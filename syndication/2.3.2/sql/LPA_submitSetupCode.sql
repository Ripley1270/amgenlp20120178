-- =============================================================================================
-- Create date: 07 August 2014
-- Description: create a table and two spored procedures for storing setup code for LPA subject
-- Table: logpad_app_setup_code
-- Stored procedure: LPA_submitSetupCode, LPA_getSetupCode    
-- ==============================================================================================

IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'LPA_submitSetupCode' AND TYPE = 'P')
   DROP PROCEDURE [dbo].[LPA_submitSetupCode]
GO

IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'LPA_getSetupCode' AND TYPE = 'P')
   DROP PROCEDURE [dbo].[LPA_getSetupCode]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM SYSOBJECTS WHERE id = object_id(N'[dbo].[logpad_app_setup_code]'))
BEGIN
	CREATE TABLE [dbo].[logpad_app_setup_code](
		[sckey] [int] IDENTITY(1,1) NOT NULL,
		[krpt] [varchar](36) NOT NULL FOREIGN KEY REFERENCES lookup_pt (krpt),
		[setup_code] [bigint] NOT NULL,
		[created] [datetime] NOT NULL CONSTRAINT [df_lpa_setup_code_created] DEFAULT (GETUTCDATE()),
	 CONSTRAINT [pk_logpad_app_setup_code] PRIMARY KEY CLUSTERED 
	(
		[sckey] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
END
GO

IF NOT EXISTS(SELECT * FROM SYS.INDEXES WHERE object_id = OBJECT_ID('logpad_app_setup_code') AND name = 'ak_logpad_app_setup_code')
BEGIN
	CREATE UNIQUE NONCLUSTERED INDEX [ak_logpad_app_setup_code] on [logpad_app_setup_code]
	([krpt] ASC) INCLUDE ([setup_code])
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-- =========================================================================================
-- Object name: LPA_submitSetupCode
-- Description: store krpt and setup code in logpad_app_setup_code table
-- If krpt exists in the table, return 'exist'
-- If inserted into the table, return 'inserted'
-- If error occurs, return error information to the caller  
-- =========================================================================================
CREATE PROCEDURE [dbo].[LPA_submitSetupCode]
	@krpt VARCHAR(36),
	@SetupCode BIGINT
AS
BEGIN

	SET NOCOUNT ON;
    SET ANSI_WARNINGS ON;
   
    DECLARE @Msg VARCHAR(30);    

	IF EXISTS(SELECT setup_code FROM [dbo].[logpad_app_setup_code] WHERE krpt = @krpt)
	BEGIN
		SET @Msg = 'exist';
	END	
	ELSE	
	BEGIN    
		BEGIN TRY
			INSERT INTO [dbo].[logpad_app_setup_code] ([krpt], [setup_code])
			VALUES (@krpt, @SetupCode);			

			IF @@ROWCOUNT > 0
			BEGIN
				SET @Msg = 'inserted';
			END		
		END TRY
		BEGIN CATCH		
			DECLARE @ErrorMessage NVARCHAR(4000);
			DECLARE @ErrorSeverity INT;
			DECLARE @ErrorState INT;

			SELECT @ErrorMessage = ERROR_MESSAGE(),
				   @ErrorSeverity = ERROR_SEVERITY(),
				   @ErrorState = ERROR_STATE();
				   
			 --return original error information that caused error to caller
			RAISERROR (@ErrorMessage, -- Message text.
					   @ErrorSeverity, -- Severity.
					   @ErrorState -- State.
					   );
					   
   			SET @Msg = 'error occured';						
		END CATCH
	END
	
	SELECT @Msg;		
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-- ====================================
-- Object name: LPA_getSetupCode 
-- Description: get setup code by krpt
-- ====================================
CREATE PROCEDURE [dbo].[LPA_getSetupCode]
	@krpt VARCHAR(36)
AS
BEGIN
	SET NOCOUNT ON;	 
	SELECT setup_code FROM [dbo].[logpad_app_setup_code] WITH (NOLOCK) WHERE krpt = @krpt;
END
GO

SET ANSI_NULLS ON 
GO
SET QUOTED_IDENTIFIER OFF 
GO
IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'LPA_submitSetupCode' AND TYPE = 'P')
BEGIN
	DECLARE @name as varchar(50)
	DECLARE @version as numeric(18,2)

	SELECT @name = 'LPA_submitSetupCode', @version = 1.00
	EXEC [dbo].[PDE_UpdateSQLVersion] @name, @version
   
END

SET ANSI_NULLS ON 
GO
SET QUOTED_IDENTIFIER OFF 
GO
IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'LPA_getSetupCode' AND TYPE = 'P')
BEGIN
	DECLARE @name as varchar(50)
	DECLARE @version as numeric(18,2)

	SELECT @name = 'LPA_getSetupCode', @version = 1.00
	EXEC [dbo].[PDE_UpdateSQLVersion] @name, @version
   
END
GO

GRANT EXECUTE ON [dbo].[LPA_submitSetupCode] TO pht_server_read_only 
GO

GRANT EXECUTE ON [dbo].[LPA_getSetupCode] TO pht_server_read_only 
GO

IF NOT EXISTS (SELECT * FROM allowed_clin_procs WHERE proc_name='LPA_submitSetupCode')
	INSERT INTO allowed_clin_procs (proc_name) values ('LPA_submitSetupCode');
GO

IF NOT EXISTS (SELECT * FROM allowed_clin_procs WHERE proc_name='LPA_getSetupCode')
	INSERT INTO allowed_clin_procs (proc_name) values ('LPA_getSetupCode');
GO

