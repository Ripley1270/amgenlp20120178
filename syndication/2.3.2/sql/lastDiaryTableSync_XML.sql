IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LPA_SplitCSV]') AND type in (N'TF'))
DROP FUNCTION [dbo].[LPA_SplitCSV]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[lastDiaryTableSync_XML]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[lastDiaryTableSync_XML]
/*
    Author: Tom Parece 
    Modified by Tim Kumm, Tom McLoughlin, and Gulden Saricali on 07/29/2014
    Modifed by Toni Blagaic on 07/08/2015 
    Modifed by Dogus Yildiz on 12/17/2015 
    
    Data returned as varchar in XML format.
*/

GO

create function dbo.LPA_SplitCSV(
	@String nvarchar (4000),
	@Delimiter nvarchar (10)
)
returns @ValueTable table ([Value] nvarchar(4000))
begin
	declare @NextString nvarchar(4000)
	declare @Pos int
	declare @NextPos int
	declare @CommaCheck nvarchar(1)

	--Initialize
	set @NextString = ''
	set @CommaCheck = right(@String,1) 

	--Check for trailing Comma, if not exists, INSERT
	--if (@CommaCheck <> @Delimiter )
	set @String = @String + @Delimiter

	--Get position of first Comma
	set @Pos = charindex(@Delimiter,@String)
	set @NextPos = 1

	--Loop while there is still a comma in the String of levels
	while (@pos <>  0)  
	begin
		set @NextString = substring(@String,1,@Pos - 1)

		insert into @ValueTable ( [Value]) Values (@NextString)

		set @String = substring(@String,@pos +1,len(@String))

		set @NextPos = @Pos
		set @pos  = charindex(@Delimiter,@String)
	end

	return
end
GO


CREATE procedure [dbo].[lastDiaryTableSync_XML](
    @krPT varchar(36), 
    @scheduleStart DATETIME,
    @unscheduledSuListCSV1 varchar(36),
    @unscheduledSuListCSV2 varchar(36) = '',
    @unscheduledSuListCSV3 varchar(36) = '',
    @unscheduledSuListCSV4 varchar(36) = '',
    @unscheduledSuListCSV5 varchar(36) = '',
    @unscheduledSuListCSV6 varchar(36) = '',
    @unscheduledSuListCSV7 varchar(36) = '',
    @unscheduledSuListCSV8 varchar(36) = '',
    @unscheduledSuListCSV9 varchar(36) = '',
    @unscheduledSuListCSV10 varchar(36) = '',
    @unscheduledSuListCSV11 varchar(36) = '',
    @unscheduledSuListCSV12 varchar(36) = '',
    @unscheduledSuListCSV13 varchar(36) = '',
    @unscheduledSuListCSV14 varchar(36) = '',
    @unscheduledSuListCSV15 varchar(36) = '',
    @unscheduledSuListCSV16 varchar(36) = '',
    @unscheduledSuListCSV17 varchar(36) = '',
    @unscheduledSuListCSV18 varchar(36) = '',
    @unscheduledSuListCSV19 varchar(36) = '',
    @unscheduledSuListCSV20 varchar(36) = ''
)
AS

/*****************************************************************************************************************
lastDiaryTableSync_XML 
Version Date        Change
1.02    08Jul2015   [tblagaic]  Return last started date in format YYYY-MM-DDThh:mm:ss{+|-}hh:mm
1.03    27Nov2015   [dyildiz]   Get the last diary roles from ig_cg table. And provide support for backwards 
                                compatibility to get previously stored roles from LPA_LastDiaryRoles
1.04    06May2016   [tmcloughlin] - Remove references to deprecated LPA_LastDiaryRoles              
*****************************************************************************************************************/

SET NOCOUNT ON
SET ansi_warnings ON

DECLARE @unscheduledSuListCSV varchar(max)
DECLARE @unscheduledSuList Table(su varchar(36))
DECLARE @RoleNames Table(
    LPARole Numeric(1, 0), 
    RoleName Varchar(100)
)

SELECT @unscheduledSuListCSV = @unscheduledSuListCSV1 +
    @unscheduledSuListCSV2 +
    @unscheduledSuListCSV3 +
    @unscheduledSuListCSV4 +
    @unscheduledSuListCSV5 +
    @unscheduledSuListCSV6 +
    @unscheduledSuListCSV7 +
    @unscheduledSuListCSV8 +
    @unscheduledSuListCSV9 +
    @unscheduledSuListCSV10 +
    @unscheduledSuListCSV11 +
    @unscheduledSuListCSV12 +
    @unscheduledSuListCSV13 +
    @unscheduledSuListCSV14 +
    @unscheduledSuListCSV15 +
    @unscheduledSuListCSV16 +
    @unscheduledSuListCSV17 +
    @unscheduledSuListCSV18 +
    @unscheduledSuListCSV19 +
    @unscheduledSuListCSV20

INSERT INTO @RoleNames (LPARole, roleName) 
SELECT 0, 'subject' UNION
SELECT 1, 'site' UNION
SELECT 2, 'admin' UNION
SELECT 3, 'caregiver'

INSERT INTO @unscheduledSuList (su)
SELECT  Value
FROM    dbo.LPA_SplitCSV(@unscheduledSuListCSV, ',');

WITH Roles (krsu, roleName, updated) AS
(
    SELECT  DISTINCT krsu, roleName, MAX(updated) OVER (PARTITION BY krsu)
    FROM    (
                SELECT  su.krsu,
                        (SELECT MAX(s.receiptts) FROM lookup_su s (NOLOCK) WHERE s.krsu = su.krsu AND s.krpt = @krpt) updated,
                        rn.roleName
                FROM    lookup_su su (NOLOCK)
                        LEFT JOIN ig_cg cg (NOLOCK) ON cg.sigorig = su.sigorig AND cg.krpt = su.krpt
                        LEFT JOIN @RoleNames rn ON rn.LPARole = cg.LPARole 
                WHERE   cg.LPARole IS NOT NULL
                        AND su.deleted = 0
                        AND su.krpt = @krpt 
                        AND (su.orig_startts >= @scheduleStart OR su.krsu IN (SELECT su FROM @unscheduledSuList WHERE su IS NOT NULL))
            ) R
)

SELECT  krsu SU,
        Convert(Varchar(40), TODATETIMEOFFSET(RptStDt, OffSet),126) lastStartedDate,
        Convert(Varchar(40), lastCompleted, 127) + 'Z' lastCompletedDate,
        Cast(deleted As Varchar(1)) deleted,
        (SELECT DISTINCT roleName [role] FROM Roles r Where r.krsu = X.krsu FOR XML PATH(''), TYPE),
        (SELECT DISTINCT Convert(Varchar(36), updated, 127) + 'Z' FROM Roles r WHERE r.krsu = X.krsu) updated
FROM    (SELECT su.krsu,
                su.ReportStartDate_mirror As RptStDt,
                su.orig_signingts As lastCompleted,
                Cast(SU.Orig_KRTZ As BigInt) / 60000 As OffSet,
                Case 
                    When deleted = 0 Then 0 
                    Else 1 
                End As Deleted,
                DENSE_RANK() Over(Partition By su.KRSU, Case When deleted = 0 Then 0 Else 1 End Order By su.ReportStartDate_mirror Desc) As Rnk
        FROM    lookup_su su With (NoLock)
        WHERE   su.krpt = @krpt And
                su.ReportStartDate_mirror IS NOT NULL
        ) X
WHERE   Rnk = 1
ORDER BY Deleted DESC, krsu
FOR XML RAW ('diary'), type, Root('Results');


GO


IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'lastDiaryTableSync_XML' AND TYPE = 'P')
BEGIN
    declare @name as varchar(50)
    declare @version as numeric(18,2)

    select @name = 'lastDiaryTableSync_XML', @version = 1.04  -- UPDATE VERSION NUMBER HERE
    exec [dbo].[PDE_UpdateSQLVersion] @name, @version
   
END
go

IF NOT EXISTS (SELECT * FROM [allowed_clin_procs] WHERE [proc_name] = 'lastDiaryTableSync_XML') BEGIN
    INSERT [allowed_clin_procs] ([proc_name]) VALUES ('lastDiaryTableSync_XML')
END


GRANT EXECUTE ON dbo.lastDiaryTableSync_XML TO pht_server_read_only
GRANT EXECUTE ON dbo.lastDiaryTableSync_XML TO PHT
GRANT EXECUTE ON dbo.lastDiaryTableSync_XML TO LPProcess
GRANT EXECUTE ON dbo.lastDiaryTableSync_XML TO ADProcess