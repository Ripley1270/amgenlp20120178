module.exports = function (grunt) {

    // Automagically load grunt tasks.
    require('load-grunt-config')(grunt, {
        config : {

            pkg     : grunt.file.readJSON('package.json'),

            build   : grunt.file.readJSON('build.json'),

            jitgrun : true,

            core    : 'app/core',

            logpad  : 'app/logpad',

            sitepad : 'app/sitepad',

            target  : 'target'

        }
    });

    grunt.loadTasks('tasks');

    // Time how long tasks take.
    require('time-grunt')(grunt);

};
