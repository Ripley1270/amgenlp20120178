@ECHO Off
COLOR 0A
set /p platform=Enter Platform:
CMD /K grunt logpad-android ^&^& exit
set currentPath=%cd%
cd C:\Program Files (x86)\Android\android-sdk\platform-tools
adb devices
cls
ECHO Removing old Build
adb uninstall com.ert.jobname.hh
ECHO Build Uninstalled
cls
REM set /p platform=Enter Platform:
ECHO New Build Installing
adb install -r %currentPath%\target\%platform%\platforms\android\build\outputs\apk\android-debug.apk
IF %ERRORLEVEL% NEQ 0 (
	Echo Install Failed
	cls
	Echo Trying Release Package
	adb install -r %currentPath%\target\%platform%\platforms\android\build\outputs\apk\android-release.apk
	IF %ERRORLEVEL% NEQ 0 (
		Echo Install Failed
		pause
	)
)
ECHO New Build Installed