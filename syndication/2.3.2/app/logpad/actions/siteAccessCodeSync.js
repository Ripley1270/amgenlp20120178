/* eslint consistent-return:0 */
import Sites from 'core/collections/Sites';
import CurrentSubject from 'core/classes/CurrentSubject';
import syncHistoricalData from 'core/actions/syncHistoricalData';
import ELF from 'core/ELF';

/**
 * Syncs site access code with the Web Service.
 * @param {null|undefined} params Not used in this action.
 * @param {Function} callback A callback function invoked upon completion of the action.
 */
export default function siteAccessCodeSync (params, callback) {
    let sync = () => {
            return syncHistoricalData({
                collection: 'Sites',
                webServiceFunction: 'getSiteAccessCode',
                activation: false
            });
        };

    if (LF.StudyDesign.siteMode === true) {

        Q.all([Sites.fetchFirstEntry(), CurrentSubject.getSubject()])
        .spread((site, subject) => {
            if (!site || site.get('site_code') !== subject.get('site_code')) {
                return sync();
            }
        })
        .finally(() => {
            callback();
        })
        .done();
    } else {
        callback();
    }
}

ELF.action('siteAccessCodeSync', siteAccessCodeSync);

// @todo remove - reference located in SiteLoginView.js
LF.Actions.siteAccessCodeSync = siteAccessCodeSync;
