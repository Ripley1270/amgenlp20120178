import ELF from 'core/ELF';
import CurrentSubject from 'core/classes/CurrentSubject';

/**
 * This action interrupts the action list if the subject is inactive.
 * @param {(undefined|null)} param - Not used in this action.
 * @param {Function} callback - Callback function to be invoked when the process has completed.
 */
export function breakRuleForInactiveSubject (param, callback) {
    CurrentSubject.getSubject().then((subject) => {
        if (subject.get('subject_active') === 0) {
            // @todo $.mobile.loading('hide');
            $('#application').removeClass('ui-disabled');
            
            //If subject is inactive and tries to send a diary to let it pass on dashboard
            localStorage.removeItem('questionnaireCompleted');
            localStorage.setItem('questionnaireToDashboard', true);
            LF.router.navigate('dashboard', true);

            if (!LF.security.timerOn) {
                LF.security.restartSessionTimeOut();
            }
        } else {
            callback();
        }
    });
}

ELF.action('breakRuleForInactiveSubject', breakRuleForInactiveSubject);
