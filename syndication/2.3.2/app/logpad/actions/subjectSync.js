/* eslint consistent-return: 0 */
import COOL from 'core/COOL';
import ELF from 'core/ELF';
import CurrentContext from 'core/CurrentContext';
import Transmit from 'logpad/transmit';

/**
 * Syncs subject data with the Web Service.
 * @param {null|undefined} params Not used in this action.
 * @param {Function} callback A callback function invoked upon completion of the action.
 * @returns {(Q.Promise<void>|undefined)}
 */
export function sync (params, callback = $.noop) {
    // Since SW takes a while to process the diaries we need to delay the subject sync call.
    // This will allow narrowing window for sync failure.
    let promise = Q.Promise(resolve => {
        setTimeout(() => {
            LF.webService.getkrDom({})
                .done(res => {
                    // transmit.subjectSync currently still uses callbacks.
                    COOL.getService('Transmit', Transmit).subjectSync(res, () => {
                        let users = CurrentContext().get('users');
                        users.syncAdminPassword()
                        .then(() => users.updateAdminUser())
                        .then(() => callback())
                        .done(resolve);
                    });
                });
        }, 500);
    });

    return promise;

}

ELF.action('subjectSync', sync);

// @todo remove
LF.Actions.subjectSync = sync;
