import ELF from 'core/ELF';
import LoginView from 'logpad/views/LoginView';
import DashboardView from 'logpad/views/DashboardView';
import CurrentSubject from 'core/classes/CurrentSubject';

/**
 * Refresh the view to update pending reports if on login or dashboard
 * @param {Object} param optional parameters.
 * @param {Function} callback Invoked to retain the action chain.
 */
export default function checkViewForRefresh (param, callback) {
    CurrentSubject.getSubject()
    .then((subject) => {
        if (subject.get('phase') !== LF.StudyDesign.terminationPhase) {
            let currentView = LF.router.view();

            if (currentView instanceof DashboardView || currentView instanceof LoginView) {
                currentView.render();
            }
        }

        callback(true);
    });
}

ELF.action('checkViewForRefresh', checkViewForRefresh);
