/**
 * @fileOverview sv_SE preferred Language set.
 * @author <a href="mailto:smoses@phtcorp.com">Stefanie Moses</a>
 * @version 1.4
 */

LF.Preferred = {
    /**
     * Preferred language object.
     * @memberof LF
     * @readonly
     * @type {String}
     */
    language    : 'sv',

    /**
     * Preferred locale object.
     * @memberof LF
     * @readonly
     * @type {String}
     */
    locale    : 'SE'
};