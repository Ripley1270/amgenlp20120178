/**
 * @fileOverview de_DE preferred Language set.
 * @author <a href="mailto:gsaricali@phtcorp.com">Gulden Saricali</a>
 * @version 1.6
 */

LF.Preferred = {
    /**
     * Preferred language object.
     * @memberof LF
     * @readonly
     * @type {String}
     */
    language    : 'de',

    /**
     * Preferred locale object.
     * @memberof LF
     * @readonly
     * @type {String}
     */
    locale    : 'DE'
};