// TODO - This class is imported, but never used.  Instead, the code uses LF.View.QuestionnairesList
// eslint-disable-next-line no-unused-vars
import QuestionnairesList from 'logpad/views/QuestionnairesList';
import IndicatorsList from 'logpad/views/IndicatorsList';
import Schedules from 'core/collections/Schedules';

/**
 * Add available objects to the dashboard. And set up all view collection event listeners
 * @param {Object} parentView parent view that is calling this function
 * @param {Function} [callback] A callback function invoked upon the completion of the action.
 * @example LF.Schedule.DisplayItem.addToView(view, function () {...});
 */
export function addToView (parentView, callback) {
    let views = {},
        objectTypes = _.chain(LF.schedule.availableSchedules.toJSON())
            .pluck('target')
            .pluck('objectType')
            .intersection()
            .value(),
        displayItem = (objectType, schedules, callback) => {
            LF.Schedule.DisplayItem[objectType](parentView, schedules, (view) => {
                if (view) {
                    view.listenTo(LF.schedule.availableSchedules, 'add', (schedule) => {
                        view.addInOrder(schedule, (newSubview) => {
                            if (newSubview) {
                                (view.refresh || $.noop)();
                            }
                        });
                    });
                    view.listenTo(LF.schedule.availableSchedules, 'remove', view.removeSubview);
                }

                callback(view);
            });
        };

    // When a new schedule is available, create a new listView for the objectType if it does not already exist.
    parentView.listenTo(LF.schedule.availableSchedules, 'add change', (schedule) => {
        let matchedView = _.findWhere(parentView.listViews, {
            objectType : schedule.get('target').objectType
        });

        if (!matchedView) {
            displayItem(schedule.get('target').objectType, new Schedules(schedule), (view) => {

                // saving logs
                if (view) {
                    parentView.listViews[view.id] = view;
                }
            });
        }
    });

    if (parentView.resetModels) {
        parentView.listenTo(LF.schedule.availableSchedules, 'beforeEvaluate', () => {
            // When we evaluate schedules we apply custom classes, so we need to start from clean models
            parentView.resetModels();
        });
    }

    if (objectTypes.length) {
        _.each(objectTypes, (objectType) => {
            displayItem(objectType, LF.schedule.availableSchedules, (view) => {
                if (view) {
                    views[view.id] = view;
                }
            });
        });
    }

    (callback || $.noop)(views);
}

LF.Schedule.DisplayItem.addToView = addToView;

/**
 * Adds a scheduled questionnairesList view to the display
 * @param {Object} parentView - parent view that is calling this function
 * @param {Object} schedules - The list of available schedules whose items are to be rendered.
 * @param {Function} callback - The callback function invoked when the the user's input is submitted.
 */
export function questionnaire (parentView, schedules, callback) {
    let questionnaires,
        questionnaireList = parentView.$('#questionnaires');

    if (questionnaireList.length > 0 && questionnaireList.children().length === 0) {
        questionnaires = new LF.View.QuestionnairesList({
            collection : parentView.questionnaires,
            schedules  : schedules,
            container  : questionnaireList
        });
    }

    callback(questionnaires);
}

LF.Schedule.DisplayItem.questionnaire = questionnaire;

/**
 * Adds a scheduled indicatorsList view to the display
 * @param {Object} parentView - parent view that is calling this function
 * @param {Object} schedules - The list of available schedules whose items are to be rendered.
 * @param {Function} callback - The callback function invoked when the the user's input is submitted.
 */
export function indicator (parentView, schedules, callback) {
    let indicators,
        indicatorsList = parentView.$('#indicators');

    if (indicatorsList.length > 0 && indicatorsList.children().length === 0) {
        indicators = new IndicatorsList({
            collection : parentView.indicators,
            schedules  : schedules,
            container  : indicatorsList
        });
    }

    callback(indicators);
}

LF.Schedule.DisplayItem.indicator = indicator;

/**
 * Add an autodial alarm
 * @param {Object} parentView - parent view that is calling this function
 * @param {Object} schedules - The list of available schedules whose items are to be rendered.
 * @param {Function} callback - The callback function invoked when the the user's input is submitted.
 */
export function autodial (parentView, schedules, callback) {
    callback();
}

LF.Schedule.DisplayItem.autodial = autodial;
