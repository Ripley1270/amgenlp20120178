import Data from 'core/Data';
import CurrentContext from 'core/CurrentContext';
import AlarmManager from 'core/AlarmManager';
import Users from 'core/collections/Users';
import Logger from 'core/Logger';
import CoreDBUpgrade from 'core/dataaccess/CoreDBUpgrade';
import ActiveAlarms from 'core/collections/ActiveAlarms';
import { filterReceivedLastDiary } from 'core/actions/handleLastDiariesResult';
import * as Helpers from 'core/Helpers';
import Transmission from 'core/models/Transmission';
import CurrentSubject from 'core/classes/CurrentSubject';
import RestartManager from 'core/classes/RestartManager';
import { MessageRepo } from 'core/Notify';
import * as lStorage from 'core/lStorage';
import * as Utilities from 'core/utilities';
import COOL from 'core/COOL';
import trial from 'core/index';
import Subjects from 'core/collections/Subjects';

let logger = new Logger('Rules');

trial.assets.coreRules(ELF.rules);

((rules) => {

    /*
     * Display a popup if the battery level is 20% or less
     */
    rules.add({
        id: 'BatteryLevelWarning',
        trigger: 'DASHBOARD:Rendered',
        evaluate: (filter, resume) => {
            LF.Wrapper.Utils.getBatteryLevel(batteryLevel => {
                if (batteryLevel && batteryLevel <= 5) {
                    filter.key = 'CRITICAL_BATTERY';
                    resume(true);
                } else if (batteryLevel && batteryLevel <= 20) {
                    filter.key = 'LOW_BATTERY';
                    resume(true);
                } else {
                    resume(false);
                }
            });
        },
        resolve: [{
            action: (input) => {
                LF.Actions.notify({
                    key: input.key
                }).done();
            }
        }],
        reject: [{ action: 'defaultAction' }]
    });


    rules.add({
        id: 'QuestionnaireScreenSetOrientation',
        trigger: 'QUESTIONNAIRE:Before',
        evaluate: 'isWrapped',
        resolve: [{ action: 'setScreenOrientation' }],
        reject: [{ action: 'defaultAction' }]
    });

    rules.add({
        id: 'DashboardSetOrientation',
        trigger: [
            'LOGIN:Rendered',
            'DASHBOARD:Rendered'
        ],
        evaluate: 'isWrapped',
        resolve: [
            { action: 'removeMessage'},
            { action: 'setScreenOrientation', data: {setDefault: true, disablePopup: true}}]
    });


    /*
     * QuestionnaireScreenAuthenticationByRole
     * Checks to see if the user logged in has a role able to answer a quesion based on a role configured at the screen level
     * If the user does not have the role required at the screen level. A login window is displayed requiring a user with the level
     * configured at the screen level to login.
     */
    rules.add({
        id: 'QuestionnaireScreenAuthenticationByRole',
        trigger: 'QUESTIONNAIRE:Before',
        evaluate: function (filter, resume) {
            resume(true);
        },
        resolve: [{ action: 'questionnaireScreenAuthenticationByRole' }],
        reject: [{ action: 'defaultAction' }]
    });

    /*
     * backout from privacy policy activation
     * When the user attempts to logout, display a confirmation dialog.
     */
    rules.add({
        id: 'PrivacyPolicyActivationBackout',
        trigger: [
            'PRIVACYPOLICYACTIVATION:Backout',
            'REACTIVATION:Backout'
        ],
        evaluate: { expression: 'confirm', input: { key: 'REVERSE_HANDOFF_CONFIRM' } },
        resolve: [{
            action (filter, done) {
                CurrentContext().setContextLanguage(`${LF.StudyDesign.defaultLanguage}-${LF.StudyDesign.defaultLocale}`)
                .then(() => done());
            }
        }, {
            action: 'navigateTo',
            data: 'code_entry'
        }],
        reject: [{ action: 'defaultAction' }]
    });

    /*
     * WidgetValidation
     * If the diary entry contains a widget try to run the widgets validation function
     */
    rules.add({
        id: 'WidgetValidation',
        trigger: 'WIDGET:Validation',
        evaluate: function (input, done) {
            let validationObj = this.model.get('validation');

            if (this.isUserTextBox && validationObj.params) {
                validationObj.params.answers = this.questionnaire.data.answers;
            }

            (LF.Widget.ValidationFunctions.hasOwnProperty(validationObj.validationFunc)) ? done(true) : done(false);
        },
        resolve: [{ action: 'widgetValidation' }],
        reject: [{ action: 'defaultAction' }]
    });

    /**
     * Perform a user sync when AddNewUser Diary is opened.
     */
    rules.add({
        id: 'OpenNewUserSync',
        trigger: 'QUESTIONNAIRE:Open/New_User',
        evaluate: true,
        resolve: [{
            action: (params, resume) => {
                LF.spinner.show()
                .then(() => {
                    const users = new Users();

                    // consider launching syncUsers().done() to do the sync asynchronously to the UI
                    // so the user could be filling it out while the sync was in place.   Sync is there to
                    // fetch users to do dup checks.
                    return users.syncUsers();
                })
                .finally(() => {
                    LF.spinner.hide();
                    resume();
                })
                .done();
            }
        }]
    });

    /**
     * Adding diaryActive when questionnaire is opened.
     */
    rules.add({
        id: 'QuestionnaireOpened',
        trigger: 'QUESTIONNAIRE:Open',
        salience: 2,
        resolve: [{ action: () => RestartManager.setDiaryActive(true) }]
    });

    /*
     * AddNewUserOnlineCheck
     * If the subject navigates away from the Affidavit and is not online display a message.
     */
    rules.add({
        id: 'AddNewUserOnlineCheck',
        trigger: 'QUESTIONNAIRE:Navigate/New_User/AFFIDAVIT',
        salience: 2,
        evaluate: function (filter, resume) {
            let newUserRole = _(LF.StudyDesign.roles.models).findWhere({id: this.data.newUserRole});

            COOL.getClass('Utilities').isOnline((isOnline) => {
                resume(!isOnline && !newUserRole.canAddOffline() && filter.direction !== 'previous');
            });
        },
        resolve: [{
            action: 'removeMessage'
        }, {
            action: (errorMessage) => {
                MessageRepo.display(MessageRepo.Banner[errorMessage]);
            },
            data: 'CONNECTION_REQUIRED'
        }],
        reject: [{ action: 'defaultAction' }]
    });

    /**
     * NewUserSave
     */
    rules.add({
        id: 'NewUserSave',
        trigger: 'QUESTIONNAIRE:Navigate/New_User/AFFIDAVIT',
        // Only execute if the next button was clicked.
        evaluate: 'isDirectionForward',
        // Set a lower inportance on this rule, so it executes after AddNewUserOnlineCheck.
        salience: 1,
        resolve: [{
            action: 'displayMessage',
            data: 'PLEASE_WAIT'
        }, {
            action: 'newUserSave'
        }, {
            action: () => {
                // Since we avoid the QuestionnaireCompletionView, we need to manually trigger QUESTIONNAIRE:Transmit
                // to ensure a proper sync is executed.
                return ELF.trigger('QUESTIONNAIRE:Transmit', {}, this);
            }
        }, {
            action: 'removeMessage'
        }, {
            action: 'navigateTo',
            data: 'dashboard'
        }, {
            // Prevent all other rules and functionality from executing in this workflow.
            // This stops a transmitQuestionnaire record from being written.
            action: 'preventAll'
        }]
    });

    /*
     * AffidavitBackOut
     * If the subject navigates away from the Affidavit, reset its answer record.
     */
    rules.add({
        id: 'AffidavitBackOut',
        trigger: 'QUESTIONNAIRE:Navigate',
        evaluate: function (input, done) {
            done(input.screenId === 'AFFIDAVIT' && input.direction === 'previous');
        },
        resolve: [{
            action: 'resetAnswersSelected',
            data: ['AFFIDAVIT']
        }, {
            action: 'defaultAction'
        }],
        reject: [{ action: 'defaultAction' }]
    });

    rules.add({
        id: 'UserChangeCredentials',
        trigger: 'USER:ChangeCredentials',
        evaluate (filter, resume) {
            new Transmission().save(filter)
            .then(() => {
                // FIXME: Core rules should not know anything about trainer
                resume(!localStorage.getItem('trainer'));
            })
            .done();
        },
        resolve: [
            {
                action: 'displayMessage',
                data: 'PLEASE_WAIT'
            },
            { action: 'recoverOrphanDiaries' },
            { action: () => RestartManager.setTransmissionActive(true) },
            { action: 'transmitAll' },
            { action: 'subjectSync' },
            {
                action: 'syncHistoricalData',
                data: {
                    collection: 'LastDiaries',
                    webServiceFunction: 'syncLastDiary',
                    activation: false
                }
            },
            {
                action: 'syncHistoricalData',
                data: {
                    collection: 'Users',
                    webServiceFunction: 'syncUsers',
                    activation: false
                }
            }, {
                action: 'siteAccessCodeSync'
            },
            { action: 'transmitLogs' },
            { action: 'terminationCheck' },
            { action: 'updateCurrentContext' },
            { action: 'removeMessage' },
            { action: () => RestartManager.setTransmissionActive(false) }
        ]
    });

    /*
     * AffidavitSessionTimeout
     * If the session times out during a questionnaire and the affidavit has been answered, save the questionnaire and logout; Otherwise, logout.
     */
    rules.add({
        id: 'AffidavitSessionTimeout',
        trigger: 'QUESTIONNAIRE:SessionTimeout',
        evaluate: 'isAffidavitSigned',
        resolve: [{
            action (filter, done) {
                Data.Questionnaire.sessionTimeout = true;
                done();
            }
        }, {
            action: 'navigateTo',
            data: 'questionnaire-completion'
        }],
        reject: [
            { action: () => RestartManager.setDiaryActive(false) },
            { action: 'logout' }
        ]
    });

    /*
     * AffidavitQestionnaireTimeout
     * If the questionnaire times out during a questionnaire and the affidavit has been answered, save the questionnaire and render dashboard; Otherwise, render dashboard.
     */
    rules.add({
        id: 'AffidavitQuestionnaireTimeout',
        trigger: 'QUESTIONNAIRE:QuestionnaireTimeout',
        evaluate: 'isAffidavitSigned',
        resolve: [{
            action (filter, done) {
                Data.Questionnaire.questionnaireSessionTimeout = true;
                done();
            }
        }, {
            action: 'navigateTo',
            data: 'questionnaire-completion'
        }],
        reject: [{
            action: function (input, done) {
                localStorage.setItem('questionnaireToDashboard', true);
                LF.router.navigate('dashboard', true);
                done();
            }
        }, {
            action: () => RestartManager.setDiaryActive(false)
        }, {
            action: 'notify',
            data: { key: 'DIARY_TIMEOUT' }
        }]
    });

    /*
     * AlarmUpdate
     * An Alarm for a diary with reminders are updated for next schedule time frame.
     */
    rules.add({
        id: 'AlarmUpdate',
        trigger: 'QUESTIONNAIRE:Saved',
        evaluate: 'isWrapped',
        resolve: [{
            action: 'updateAlarm',
            data: function (input, done) {
                done({ id: input.questionnaire });
            }
        }]
    });

    /*
     * Logout
     * When the user attempts to logout, display a confirmation dialog.
     */
    rules.add({
        id: 'Logout',
        trigger: 'DASHBOARD:Logout',
        evaluate: { expression: 'confirm', input: { key: 'LOGOUT_CONFIRM' } },
        resolve: [{ action: 'logout' }],
        reject: [{
            action: function (input) {
                this.delegateEvents();
                localStorage.removeItem('dashboardToLogin');
            }
        }, {
            action: 'preventDefault'
        }]
    });

    /*
     * Termination
     * Handles end participation of subject
     */
    rules.add({
        id: 'Termination',
        trigger: 'Termination',
        evaluate: (input, done) => {
            let transmissions = new LF.Collection.Transmissions(),
                setMessages = (callback = $.noop) => {
                    // Decide the string to be displayed on the pop up
                    if (CurrentContext().role === 'subject' && !input.subjectActive) {
                        input.dialogKey = 'TERMINATION_REPLACED';
                    } else {
                        const dialogs = CurrentContext().get('role').get('dialogs') || CurrentContext().get('defaultDialogs');

                        input.dialogKey = input.subjectActive ? dialogs.deactivated : dialogs.activatedElsewhere;
                    }

                    callback();
                },
                endDeviceUse = () => {
                    // get the transmission count, to ensure any pending reports
                    transmissions.count({
                        onSuccess: (res) => {
                            // if no pending reports, initiate end device use
                            if (!res) {
                                LF.webService.updateSubjectData(input.deviceID, { E: true }, null, () => {
                                    setMessages(() => {
                                        done(true);
                                    });
                                }, () => {
                                    done(false);
                                });
                            } else {
                                done(false);
                            }
                        },
                        onError: (err) => {
                            // logger.log(Log4js.Level.ERROR, 'Transmissions collection count failed for Termination rule with error ' + err.toString());
                            done(false);
                        }
                    });
                };

            // Save subject active to 2 in-case of end participation
            if (input.endParticipation === true) {
                CurrentSubject.getSubject()
                .then((subject) => {
                    subject.save({subject_active: 2}, {
                        onSuccess: endDeviceUse,
                        onError: (err) => {
                            logger.error('Subject save failed upon end participation for Termination rule with error: ', err);
                            endDeviceUse();
                        }
                    });
                })
                .catch((err) => {
                    logger.error('Subjects fetch failed upon end participation for Termination rule with error : ', err);
                    endDeviceUse();
                })
                .done();
            } else {
                endDeviceUse();
            }
        },
        resolve: [{
            action: 'transmitLogs'
        }, {
            action: (input, done) => {
                LF.Wrapper.exec({
                    execWhenWrapped: () => {
                        let activeAlarms = new ActiveAlarms();

                        activeAlarms.fetch()
                        .then(() => activeAlarms.cancelAllAlarms())
                        .then(() => {
                            LF.Helpers.rescheduleAlarms();
                        })
                        .done();

                        // Done on purpose. Dont want to hold up the loading of the application. This can be done in the background.
                        done();
                    },
                    execWhenNotWrapped: done
                });
            }
        }, {
            action: 'uninstall'
        }, {
            action: (input, done = $.noop) => {
                LF.spinner.hide();

                LF.Actions.notify({
                    key: input.dialogKey
                }).finally(() => {
                    LF.Utilities.restartApplication();
                    done(true);
                });
            }
        }],
        reject: [{
            action: 'notify',
            data: { key: 'ELPU_FAILURE' }
        }]
    });

    /*
     * Pending Alarms
     * Execute all pending Alarms
     */
    rules.add({
        id: 'soundPendingAlarms',
        trigger: [
            'LOGIN:Rendered',
            'DASHBOARD:Rendered'
        ],
        evaluate: 'isWrapped',
        resolve: [{
            action: function (input, done) {
                AlarmManager.soundAllPendingAlarms(input.subject);

                done();
            }
        }]
    });

    /*
     * TransmitAll
     * Executes all transmission queue items.
     */
    rules.add({
        id: 'TransmitAll',
        trigger: 'QUESTIONNAIRE:Transmit',
        evaluate: ['AND', 'isOnline', 'isStudyOnline'],
        resolve: [{
            action: 'displayMessage',
            data: 'PLEASE_WAIT'
        }, {
            action: 'recoverOrphanDiaries'
        }, {
            action: () => RestartManager.setTransmissionActive(true)
        }, {
            action: 'transmitAll'
        }, {
            action: 'subjectSync'
        }, {
            action: 'syncHistoricalData',
            data: {
                collection: 'LastDiaries',
                webServiceFunction: 'syncLastDiary',
                activation: false
            }
        }, {
            action: 'syncHistoricalData',
            data: {
                collection: 'Users',
                webServiceFunction: 'syncUsers',
                activation: false
            }
        }, {
            action: 'siteAccessCodeSync'
        }, {
            action: 'transmitLogs'
        }, {
            action: 'breakRuleForInactiveSubject'
        }, {
            action: 'terminationCheck'
        }, {
            action: 'updateCurrentContext'
        }, {
            action: 'removeMessage'
        }, {
            action: () => RestartManager.setTransmissionActive(false)
        }],
        reject: [{
            action: 'navigateTo',
            data: 'dashboard'
        }, {
            action: () => RestartManager.setTransmissionActive(false)
        }]
    });

    /*
     * ToolboxTransmit
     * Executes all transmission queue items and receives any DCF data.
     */
    rules.add({
        id: 'ToolboxTransmit',
        trigger: 'TOOLBOX:Transmit',
        evaluate: ['AND', 'isOnline', 'isStudyOnline'],
        resolve: [{
            action: 'displayMessage',
            data: 'PLEASE_WAIT'
        }, {
            action: 'recoverOrphanDiaries'
        }, {
            action: () => RestartManager.setTransmissionActive(true)
        }, {
            action: 'transmitAll'
        }, {
            action: 'subjectSync'
        }, {
            action: 'syncHistoricalData',
            data: {
                collection: 'LastDiaries',
                webServiceFunction: 'syncLastDiary',
                activation: false
            }
        }, {
            action: 'siteAccessCodeSync'
        }, {
            action: 'transmitLogs'
        }, {
            action: 'breakRuleForInactiveSubject'
        }, {
            action: 'terminationCheck'
        }, {
            action: 'updateCurrentContext'
        }, {
            action: 'syncHistoricalData',
            data: {
                collection: 'Users',
                webServiceFunction: 'syncUsers',
                activation: false
            }
        }, {
            action: 'removeMessage'
        }, {
            action: () => RestartManager.setTransmissionActive(false)
        }],
        reject: [{
            action: function (input) {
                return this.transmitRetry(input)
                .then((res) => {
                    if (res === false) {
                        return RestartManager.setTransmissionActive(false);
                    }
                });
            },
            data: function (input, done) {
                input.message = input.isOnline ? 'TRANSMIT_RETRY_FAILED' : 'TRANSMIT_RETRY_CONNECTION_REQUIRED';
                done(input);
            }
        }]
    });

    /*
     * PendingReportTransmit
     * Executes all transmission queue items and receives any DCF data.
     */
    rules.add({
        id: 'PendingReportTransmit',
        trigger: ['LOGIN:Transmit', 'DASHBOARD:Transmit'],
        evaluate: ['AND', 'isOnline', 'isStudyOnline'],
        resolve: [{
            action: 'displayMessage',
            data: 'PLEASE_WAIT'
        }, {
            action: 'recoverOrphanDiaries'
        }, {
            action: () => RestartManager.setTransmissionActive(true)
        }, {
            action: 'transmitAll'
        }, {
            action: 'subjectSync'
        }, {
            action: 'syncHistoricalData',
            data: {
                collection: 'LastDiaries',
                webServiceFunction: 'syncLastDiary',
                activation: false
            }
        }, {
            action: 'syncHistoricalData',
            data: {
                collection: 'Users',
                webServiceFunction: 'syncUsers',
                activation: false
            }
        }, {
            action: 'siteAccessCodeSync'
        }, {
            action: 'transmitLogs'
        }, {
            action: 'terminationCheck'
        }, {
            action: 'updateCurrentContext'
        }, {
            action: 'removeMessage'
        }, {
            action: () => RestartManager.setTransmissionActive(false)
        }, {
            // TODO: In 2.4 terminationCheck action should return the { preventDefault: true } flag
            // To reduce regression the implementation will be added as separate action in 2.3.2 release
            // This will avoid re-rendering (with an errror) of loginview after termination.
            action: () => {
                return Subjects.fetchCollection()
                .then((subjects) => {
                    if (subjects.length === 0) {
                        return { preventDefault: true };
                    }
                });
            }
        }],
        reject: [{
            action: function (input) {
                return this.transmitRetry(input)
                .then((res) => {
                    if (res === false) {
                        return RestartManager.setTransmissionActive(false);
                    }
                });
            },
            data: function (input, done) {
                input.message = input.isOnline ? 'TRANSMIT_RETRY_FAILED' : 'TRANSMIT_RETRY_CONNECTION_REQUIRED';
                done(input);
            }
        }]
    });

    /*
     * PasswordTransmit and SecretQuestionTransmit
     * Executes all transmission queue items and receives any DCF data.
     */
    rules.add({
        id: 'PasswordTransmit',
        trigger: ['CHANGEPASSWORD:Transmit', 'CHANGESECRETQUESTION:Transmit'],
        evaluate: ['AND', 'isOnline', 'isStudyOnline'],
        resolve: [{
            action: 'displayMessage',
            data: 'PLEASE_WAIT'
        }, {
            action: 'recoverOrphanDiaries'
        }, {
            action: () => RestartManager.setTransmissionActive(true)
        }, {
            action: 'transmitAll'
        }, {
            action: 'subjectSync'
        }, {
            action: 'syncHistoricalData',
            data: {
                collection: 'LastDiaries',
                webServiceFunction: 'syncLastDiary',
                activation: false
            }
        }, {
            action: 'syncHistoricalData',
            data: {
                collection: 'Users',
                webServiceFunction: 'syncUsers',
                activation: false
            }
        }, {
            action: 'siteAccessCodeSync'
        }, {
            action: 'transmitLogs'
        }, {
            action: 'breakRuleForInactiveSubject'
        }, {
            action: 'terminationCheck'
        }, {
            action: 'updateCurrentContext'
        }, {
            action: 'removeMessage'
        }, {
            action: () => RestartManager.setTransmissionActive(false)
        }]
    });

    /*
     * LoginFailure
     * Use to manage failed login attempts.
     */
    rules.add({
        id: 'LoginFailure',
        trigger: 'LOGIN:Failure',
        evaluate: function (input, done) {
            if (input.allowed === 0) {
                done(false);
            } else {
                done(input.failures >= input.allowed);
            }
        },
        resolve: [{ action: 'defaultAction', data: false }]
    });

    /*
     * QuestionnaireBackOut
     * Displays a confirmation prompt when a user tries to back out of a questionnaire.
     */
    rules.add({
        id: 'QuestionnaireBackOut',
        trigger: 'QUESTIONNAIRE:BackOut',
        evaluate: {
            expression: 'confirm',
            input: { key: 'BACK_OUT_CONFIRM' }
        },
        resolve: [{
            action: function (input, done) {
                LF.security.stopQuestionnaireTimeOut();
                localStorage.setItem('questionnaireToDashboard', true);
                LF.router.navigate('dashboard', { trigger: true, replace: true });
                done();
            }
        }, {
            action: () => RestartManager.setDiaryActive(false)
        }],
        reject: [{
            action: function (input, done) {
                this.enableButton($('#back'));
                localStorage.removeItem('questionnaireToDashboard');
                done({ preventDefault: true });
            }
        }]
    });

    // Set Restart Flags
    rules.add({
        id: 'SetRestartFlags',
        trigger: 'QUESTIONNAIRE:Saved',
        salience: 1,
        resolve: [
            { action: () => RestartManager.setTransmissionActive(true) },
            { action: () => RestartManager.setDiaryActive(false) }
        ]
    });

    rules.add({
        id: 'CompletionViewSavedDiarySessionTimeout',
        trigger: 'COMPLETIONVIEW:SavedDiarySessionTimeout',
        evaluate: true,
        resolve: [
            { action: 'removeMessage' },
            { action: () => RestartManager.setTransmissionActive(false) }
        ]
    });

    // Set Restart Flags
    rules.add({
        id: 'SetRestartFlagsOnTimeout',
        trigger: 'COMPLETIONVIEW:SavedDiaryQuestionnaireTimeout',
        salience: 1,
        resolve: [
            { action: () => RestartManager.setTransmissionActive(false) }
        ]
    });

    /*
     * ChangePhase
     * Checks to change phase when a questionnaire is completed.
     */
    rules.add({
        id: 'ChangePhase',
        trigger: 'QUESTIONNAIRE:Completed',
        evaluate: function (context, done) {
            let subject = this.subject,
                triggerPhase = this.model.get('triggerPhase');

            // save off rule info into filter (so available to data
            // (data not executed with a "this" being the questionnaire )
            context.subject = subject;
            context.triggerPhase = triggerPhase;

            if (subject.get('phaseTriggered') === 'true' || triggerPhase !== undefined) {
                done(LF.StudyDesign.studyPhase[triggerPhase] !== subject.get('phase'));
            } else {
                done(false);
            }
        },
        resolve: [{
            action: 'changePhase',
            data: function (context, done) {
                let subject = context.subject,
                    triggerPhase = context.triggerPhase;

                if (subject.get('phaseTriggered') === 'true') {
                    done({
                        change: true,
                        phase: subject.get('phase'),
                        phaseStartDateTZOffset: subject.get('phaseStartDateTZOffset')
                    });
                } else if (triggerPhase !== undefined) {
                    done({
                        change: true,
                        phase: LF.StudyDesign.studyPhase[triggerPhase],
                        phaseStartDateTZOffset: LF.Utilities.timeStamp(new Date())
                    });
                }
            }
        }]
    });

    /*
     * LocalStorageSupport
     * Displays an error message when accessing local storage fails.
     */
    rules.add({
        id: 'LocalStorageSupport',
        trigger: 'LOCALSTORAGE:NoSupport',
        evaluate: function (input, done) {
            // logger.log(Log4js.Level.ERROR, 'LOCAL_STORAGE_FAILURE');

            // Close the virtual keyboard before showing the popup message
            $('input').blur();

            LF.Actions.notify({
                key: 'LOCAL_STORAGE_FAILURE'
            }, done);
        },
        resolve: [{ action: 'reload' }]
    });

    /**
     * This rule requests last diary data from the server after subject data has been received successfully.
     */
    rules.add({
        id: 'RequestLastDiaryData',
        trigger: 'ACTIVATION:SubjectDataReceived',
        evaluate: true,
        resolve: [{
            action: function (input, done) {
                return this.requestHistoricalData({
                    tempStorage: 'PHT_LastDiary',
                    webServiceFunction: 'syncLastDiary',
                    activation: true,
                    filterFunction: filterReceivedLastDiary
                }, done);
            }
        }]
    });

    /**
     * This rule installs last diary data into the model.
     */
    rules.add({
        id: 'InstallLastDiary',
        trigger: 'INSTALL:ModelsInstalled',
        evaluate: true,
        resolve: [{
            action: function (input) {
                return this.installHistoricalData({
                    model: 'LastDiary',
                    tempStorage: 'PHT_LastDiary'
                });
            }
        }]
    });

    /**
     * This rule requests krdom from the server after subject data has been received successfully.
     */
    rules.add({
        id: 'RequestkrDom',
        trigger: 'ACTIVATION:SubjectDataReceived',
        evaluate: true,
        resolve: [{
            action: function (input, done) {
                return this.requestHistoricalData({
                    tempStorage: 'PHT_site',
                    webServiceFunction: 'getkrDom',
                    activation: true
                }, done);
            }
        }]
    });

    /**
     * This rule requests site access code data from the server after subject data and last diary data have been received successfully.
     */
    rules.add({
        id: 'RequestAdminData',
        trigger: 'ACTIVATION:SubjectDataReceived',
        evaluate: function (input, done) {
            done(LF.StudyDesign.adminUser === true);
        },
        resolve: [{
            action: function (input, done) {
                return this.requestHistoricalData({
                    tempStorage: 'PHT_admin',
                    webServiceFunction: 'getAdminPassword',
                    activation: true
                }, done);
            }
        }]
    });

    // This rule syncs user data when it is received
    rules.add({
        id: 'handleUserSyncData',
        trigger: 'HISTORICALDATASYNC:Received/Users',
        evaluate: true,
        resolve: [
            { action: 'handleUserSyncData' },
            { action: 'defaultAction' }
        ]
    });

    // This rule checks core database upgrades
    rules.add({
        id: 'CheckCoreDatabaseUpgrade',
        trigger: 'INSTALL:ModelsInstalled',
        evaluate (input, resume) {
            resume(CoreDBUpgrade && !_.isEmpty(CoreDBUpgrade));
        },
        resolve: [{
            action: 'displayMessage',
            data: 'PLEASE_WAIT'
        }, {
            action: 'databaseUpgradeCheck',
            data: {
                config: CoreDBUpgrade,
                versionName: 'DB_Core'
            }
        }, {
            action: 'removeMessage'
        }]
    });

    // TODO: Make sure that this rule runs after all alarms are triggered. Probably this should be handled in startup.js
    /**
     * This rule reschedules all alarms in case of a study configuration change
     */
    rules.add({
        id: 'ReScheduleAlarms',
        trigger: 'APPLICATION:Loaded',
        salience: 3,
        resolve: [{
            action: () => {
                ActiveAlarms.fetchCollection()
                .then(activeAlarms => {
                    // Let's create an array of alarmIds to loop through.
                    // This will prevent issues when removing the alarms from the collection within a loop.
                    let alarmIdList = activeAlarms.map(alarm => alarm.id);

                    return Q.all(alarmIdList.map(alarmId => {
                        // Fix for DE16461 & DE16587: Do not cancel the alarm which has triggered the app to be started
                        // So instead of canceling All alarms, delete each alarm seperately
                        if (alarmId === Data.lastTriggeredAlarmId) {
                            return Q();
                        }

                        AlarmManager.cancelAlarm(alarmId);
                        return activeAlarms.remove(alarmId);
                    }));
                })
                .then(() => LF.Helpers.rescheduleAlarms())
                .done();
            }
        }]
    });

    /*
     * If checkForTimeSlip is set to false, prevent TimeConfirmation rule from firing.
     */
    rules.add({
        id: 'SkipTimeConfirmation',
        trigger: [
            'APPLICATION:Loaded',
            'TIMESLIP:Retry'
        ],
        salience: 2,
        evaluate: (input, resume) => {
            const timeTravel = Utilities.isTimeTravelConfigured();

            if (LF.environment.checkForTimeSlip === false) {
                logger.operational('TimeSlipCheck is skipped due to studyDesign flag "checkForTimeSlip".');
                resume(true);
            } else if (timeTravel) {
                logger.operational('TimeSlipCheck is skipped because timeTravel is configured".');
                resume(true);
            } else {
                Helpers.checkInstall()
                .then(isInstalled => {
                    resume(!isInstalled);
                })
                .catch(err => {
                    logger.error(`Error checking install`, err);
                    resume(false);
                })
                .done();
            }
        },
        resolve: [{
            action: () => ({ stopRules: true })
        }, {
            action: 'navigateTo',
            data: ''
        }]
    });

    /**
     * Time Confirmation
     */
    rules.add({
        id: 'TimeConfirmation',
        trigger: [
            'APPLICATION:Loaded',
            'TIMESLIP:Retry'
        ],
        salience: 1,
        evaluate (input, resume) {
            return Q.all([COOL.getClass('Utilities').isOnline(), Helpers.checkInstall()])
            .spread((isOnline, isInstalled) => {
                const isOfflineAndActivated = !isOnline && isInstalled;

                // Show Blind confirmation screen if the device is in offline mode and is already activated
                resume(!isOfflineAndActivated);
            })
            .catch(e => {
                logger.error(e);
                resume(false);
            })
            .done();
        },
        resolve: [{
            action: 'navigateTo',
            data: 'blank'
        }, {
            action: 'timeSlipCheck'
        }, {
            action: 'navigateTo',
            data: ''
        }],
        // US7106
        reject: [{
            action: () => {
                logger.operational('TimeConfirmation: Device is offline or no server info is available.');

                LF.content.setContextLanguage(localStorage.getItem('preferredLanguageLocale'));
                LF.Actions.navigateTo('time-confirmation');

                // This prevents the default navigate invocation in the Startup workflow.
                return { preventDefault: true };
            }
        }]
    });

    // Handle the received LastDiaryTableSync Data
    rules.add({
        id: 'SyncLastDiary',
        trigger: 'HISTORICALDATASYNC:Received/LastDiaries',
        evaluate: true,
        resolve: [{ action: 'handleLastDiariesResult' }]
    });
})(ELF.rules);
