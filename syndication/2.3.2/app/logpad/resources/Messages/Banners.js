import { Banner, MessageRepo } from 'core/Notify';
import * as MessageHelpers from 'core/resources/Messages/MessageHelpers';

/**
 * Create logpad specific banners and add them to MessageRepo.
 */
export default function setupLogpadBanners () {
    const inputErrorKeys = [
        'INVALID_CODE',

        // Currently only translated in Logpad, if needed on sitepad move to core.
        'ERROR_TITLE',
        'CODE_ENTRY_NO_SERVER',
        'CONNECTION_TIMEOUT',
        'CUSTOM_ENVIRONMENT_USAGE_ERROR'
    ];

    _.each(inputErrorKeys, MessageHelpers.createInputErrorBanner);
}
