import Logger from 'core/Logger';
const logger = new Logger('Startup');

logger.traceEnter('Import Startup');
logger.trace('Import phase I');

import trial from 'core/index';

import Router from 'logpad/Router';
import ApplicationController from 'logpad/controllers/ApplicationController';
import RegistrationController from 'logpad/controllers/RegistrationController';
import SettingsController from 'logpad/controllers/SettingsController';

logger.trace('Import phase II');
import ELF from 'core/ELF';
import COOL from 'core/COOL';
import WebService from 'core/classes/WebService';
import AlarmManager from 'core/AlarmManager';
import setupLogpadMessages from 'logpad/resources/Messages';

// Transmit is imported to bootstrap the module into COOL.
import 'logpad/transmit';

//- Core Modules
import * as Helpers from 'core/Helpers';
import * as Utilities from 'core/utilities';

logger.trace('Import phase III');
// The following modules are imported so some value, or another, can be bootstrapped.
import 'core/collections/Languages';
import 'core/collections/Templates';
import 'core/collections/StoredSchedules';
import 'core/collections/Logs';
import 'core/collections/SubjectAlarms';
import 'logpad/scheduling/DisplayItem';
logger.traceExit('Startup');

import BaseStartup from 'core/classes/BaseStartup';

export default class Startup extends BaseStartup {

    /**
     * Setup the application's router.
     * @param {Object} [options={}] - Options passed into the Router's constructor.
     * @returns {Q.Promise<void>}
     */
    backboneStartup (options = {}) {
        logger.traceEnter('backboneStartup');

        return Q.Promise(resolve => {
            const application = new ApplicationController();
            const registration = new RegistrationController();
            const settings = new SettingsController();
            const study = new trial.assets.StudyController();

            const routerOpts = _.extend({}, options.routerOpts, {
                controllers: _.extend({}, options.controllers, {
                    application, registration, settings, study
                })
            });

            LF.router = new Router(routerOpts);

            // Add the web service to the LF namespace.
            // @TODO: MOVE THIS!!!
            LF.webService = COOL.new('WebService', WebService);
            logger.traceExit('backboneStartup');

            resolve();
        })
        .catch((err) => {
            logger.error('Error on backbone startup', err);
        });
    }

    /**
     * Sets up application specific services such as alarms.
     * @returns {Q.Promise<void>}
     */
    applicationStartup () {
        logger.traceEnter('applicationStartup');

        return Q.Promise(resolve => {
            if (LF.Preferred) {
                Utilities.setLanguage(`${LF.Preferred.language}-${LF.Preferred.locale}`,
                    LF.strings.getLanguageDirection());
            }

            // Checks to see if application was started because of an alarm
            LF.Wrapper.exec({
                execWhenWrapped: () => {
                    // Setup Alarm events.
                    // TODO: Should we wait until this is resolved to continue?
                    AlarmManager.registerPluginEvents();
                }
            });

            logger.traceExit('Done with application setup');
            resolve();
        })
        .catch((err) => {
            logger.error('Error during application startup', err);
        });
    }

    /**
     * Checks localStorage support and handles orientaion change of the device.
     * @returns {Q.Promise<void>}
     */
    finalStartup () {
        return Q().then(() => {
            logger.traceEnter('finalStartup');
            Helpers.checkLocalStorage();
            Helpers.handleOrientationChange();
            logger.traceExit('finalStartup');
        })
        .catch((err) => {
            logger.error('Error on final startup', err);
        });
    }

    /**
     * Runs prior to any other startup method.
     * @returns {Q.Promise<void>}
     */
    preSystemStartup () {
        logger.traceEnter('preSystemStartup');

        return super.preSystemStartup()
        .then(() => {
            // If in browser, resize to aspect ratio approximating phone (9:16)
            // (Note: using actual pixel dimensions of even low-end phone would result
            // in window too big to fit on typical monitor)
            LF.Wrapper.exec({
                execWhenNotWrapped: () => {
                    let width  = 480,
                        height = width * 16 / 9;
                    window.resizeTo(
                        width  + (window.outerWidth  - window.innerWidth),
                        height + (window.outerHeight - window.innerHeight)
                    );
                }
            });
        })
        .finally(() => logger.traceExit('preSystemStartup'));
    }

    /**
     * Start Backbone.history.
     * @returns {Q.Promise<void>}
     */
    startUI () {
        return Q().then(() => {
            logger.traceEnter('startUI');
            let b = Backbone.history.start({ pushState: false, silent: true });
            // hack so backbone things route it changing.  Unfortunately it sets fragment to ''
            // if silent: true, even though it never actually navigated anywhere.
            Backbone.history.fragment = undefined;
            logger.trace(`startUI backbone history start: ${b}`);
            logger.traceExit('startUI');
        })
        .catch((err) => {
            logger.error('Error starting UI', err);
        });
    }

    /**
     * Startup the application.
     * @returns {Q.Promise<void>}
     */
    startup () {
        logger.trace('startup.');
        setupLogpadMessages();
        Utilities.storeUrlParams();
        LF.Wrapper.detectWrapper();

        return Q()   // Only because it makes the below nicer to look at
            .then(() => this.preSystemStartup())
            .then(() => this.jQueryStartup())
            .then(() => this.cordovaStartup())
            .then(() => this.studyStartup())
            .then(() => this.backboneStartup())
            .then(() => {
                return this.applicationStartup();
            })
            .then(() => {
                return this.usersAndContext();
            })
            .then(() => {
                return this.finalStartup();
            })
            .then(() => {
                return this.startUI();
            })
            .then(() => {
                return this.notifyStudyDesignValidation();
            })
            .then(() => ELF.trigger('APPLICATION:Loaded'))
            .catch((e) => logger.error('Exception in startup:', e));
    }
}
