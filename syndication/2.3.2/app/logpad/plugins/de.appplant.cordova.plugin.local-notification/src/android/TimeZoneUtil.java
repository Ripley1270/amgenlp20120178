package de.appplant.cordova.plugin.localnotification;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.Date;

//ERT MOD
public final class TimeZoneUtil {
    public static final String TZ_ID = "timeZoneId";
    private static final String PREF_KEY = "timezoneinfo";

    public static void saveTimeZoneID(final Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE).edit();
        editor.putString(TZ_ID, getCurrentTimeZoneID());

        if (android.os.Build.VERSION.SDK_INT < 9) {
            editor.commit();
        } else {
            editor.apply();
        }
    }

    public static String getOldTimeZoneID(final Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE);
        return prefs.getString(TZ_ID, null);
    }

    public static Boolean getTimeZoneSet(final Context context) {
        SharedPreferences prefs = context.getSharedPreferences("LogPad", Context.MODE_PRIVATE);
        return prefs.getBoolean("TIMEZONE_SET", false);
    }

    public static String getCurrentTimeZoneID () {
        return TimeZone.getDefault().getID();
    }

    /**
    * Converts the given <code>date</code> from the <code>fromTimeZone</code> to the
    * <code>toTimeZone</code>.  Since java.util.Date has does not really store time zome
    * information, this actually converts the date to the date that it would be in the
    * other time zone.
    * @param date
    * @param fromTimeZone
    * @param toTimeZone
    * @return
    */
    public static Date convertTimeZone(Date date, TimeZone fromTimeZone, TimeZone toTimeZone)
    {
        long fromTimeZoneOffset = getTimeZoneUTCAndDSTOffset(date, fromTimeZone);
        long toTimeZoneOffset = getTimeZoneUTCAndDSTOffset(date, toTimeZone);

        return new Date(date.getTime() + (fromTimeZoneOffset - toTimeZoneOffset));
    }

    /**
    * Calculates the offset of the <code>timeZone</code> from UTC, factoring in any
    * additional offset due to the time zone being in daylight savings time as of
    * the given <code>date</code>.
    * @param date
    * @param timeZone
    * @return
    */
    private static long getTimeZoneUTCAndDSTOffset(Date date, TimeZone timeZone)
    {
         long timeZoneDSTOffset = 0;
         if(timeZone.inDaylightTime(date))
         {
             timeZoneDSTOffset = timeZone.getDSTSavings();
         }

         return timeZone.getRawOffset() + timeZoneDSTOffset;
    }

    /**
     * Get a diff between two dates
     * @param date1 the oldest date
     * @param date2 the newest date
     * @param timeUnit the unit in which you want the diff
     * @return the diff value, in the provided unit
     */
    public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
    }

}
