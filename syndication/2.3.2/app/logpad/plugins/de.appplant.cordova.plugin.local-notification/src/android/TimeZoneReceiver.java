package de.appplant.cordova.plugin.localnotification;

import de.appplant.cordova.plugin.notification.Manager;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
import android.util.Log;
import java.util.TimeZone;
import android.content.BroadcastReceiver;
import org.apache.cordova.CordovaWebView;

/***
* ERT MOD:
*
* Broadcast receiver for a timezone change system event.
*/
public class TimeZoneReceiver extends RestoreReceiver {

    private final String TAG = TimeZoneReceiver.class.getSimpleName();

    private CordovaWebView mWebView = null;

    /**
     * Called on timezone change event.
     * @param context
     *      Application context
     * @param intent
     *      Received intent with content data
     */
    @Override
    public void onReceive (Context context, Intent intent) {
        super.onReceive(context, intent);
        Log.d(TAG, "Timezone changed event!");

        TimeZoneUtil.saveTimeZoneID(context);
        boolean timeZoneNotSetInApplication = !TimeZoneUtil.getTimeZoneSet(context);

        mWebView = LocalNotification.getWebView();
        if (mWebView != null) {
             if (timeZoneNotSetInApplication && android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.KITKAT) {
                mWebView.loadUrl("javascript:LF.Class.RestartManager.setRestartPending(true);");
             } else if (timeZoneNotSetInApplication && android.os.Build.VERSION.SDK_INT == android.os.Build.VERSION_CODES.KITKAT) {
                 mWebView.sendJavascript("LF.Class.RestartManager.setRestartPending(true);");
             } else {
                 mWebView.sendJavascript("LF.Class.RestartManager.refreshActiveAlarms()");
             }
        } else {
         Log.d(TAG, "Application is not running.");
        }
    }

 }
