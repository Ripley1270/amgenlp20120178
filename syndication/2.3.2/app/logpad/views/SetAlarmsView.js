import PageView from 'core/views/PageView';
import AlarmTimeView from 'logpad/views/AlarmTimeView';
import SubjectAlarms from 'core/collections/SubjectAlarms';
import * as helpers from 'core/Helpers';
import Logger from 'core/Logger';

// eslint-disable-next-line no-unused-vars
const logger = new Logger('SetAlarmsView');

export default class SetAlarmsView extends PageView {
    constructor () {
        super();

        /**
         * @property {Object<string,string>} events - The view's events.
         * @readonly
         */
        this.events = {

            // Click event for back button
            'click #back': 'back',

            //Child event alarm time changed
            'alarmChanged': 'updateAlarm'
        };

        /**
         * @property {Array} schedulesWithSubjectAlarams - Array that holds schedule models with subject alarm configuration
         * @default []
         */
        this.schedulesWithSubjectAlarms = [];

        /**
         * @property {Object<string,string> templateStrings - Strings to fetch in the render function.
         */
        this.templateStrings = {
            title               : 'SET_ALARMS',
            header              : 'APPLICATION_HEADER'
        };

        /**
         * @property {SubjectAlarms} subjectAlarms - A collection of subject alarms.
         */
        this.subjectAlarms = new SubjectAlarms();
    }

    /**
     * @property {string} id - The unique id of the root element.
     * @readonly
     * @default 'set-alarms-page'
     */
    get id () {
        return 'set-alarms-page';
    }

    /**
     * @property {string} id - Id of template to render.
     * @readonly
     * @default '#alarms-template'
     */
    get template () {
        return '#alarms-template';
    } 

    /**
     * Navigates back to the toolbox view.
     */
    back () {
        this.sound.play('click-audio');
        
        this.navigate('toolbox');
    }

    /**
     * Resolve any dependencies required for the view to render.
     * @returns {Q.Promise<void>}
     */
    resolve () {
        let scheduleModels = helpers.getScheduleModels();

        // Clear the schedules prior to resolving.
        this.schedulesWithSubjectAlarms = [];

        return this.subjectAlarms.fetch().then(() => {
            _.each(scheduleModels, (schedule) => {
                if (schedule.get('alarmParams') && schedule.get('alarmParams').subjectConfig) {
                    //_.clone is not working correctly even with second parameter set to true (deep cloning)
                    this.schedulesWithSubjectAlarms.push($.extend(true, {}, schedule));
                }
            });
        });
    }

    /**
     * Renders the view.
     * @returns {Q.Promise<void>}
     */
    render () {
        return this.buildHTML({}, true)
        .then(() => {
            _.each(this.schedulesWithSubjectAlarms, (schedule) => {
                let subjectAlarm = this.subjectAlarms.where({ schedule_id: schedule.get('id') })[0];

                if (subjectAlarm) {
                    schedule.get('alarmParams').originalTime = schedule.get('alarmParams').time;
                    schedule.get('alarmParams').time = subjectAlarm.get('time');
                }

                let view = new AlarmTimeView({
                    model : schedule,
                    id    : schedule.get('alarmParams').id
                });

                this.$('#alarms-content').append(view.el);
            }, this);

            this.delegateEvents();
        });
    }

    /**
     * Update the subject alarm record with new time.
     * @param {Event} e - Event data
     * @param {Object} params - alarm data - time, id, schedule_id
     */
    updateAlarm (e, params) {
        this.subjectAlarms.setSubjectAlarm(params);
    }
}

window.LF.View.SetAlarmsView = SetAlarmsView;