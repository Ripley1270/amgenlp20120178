const { View } = Backbone;

import ELF from 'core/ELF';
import Logger from 'core/Logger';

const logger = new Logger('IndicatorView');

export default class IndicatorView extends View {
    constructor (options) {
        super(options);

        /**
         * @property {Object<string,string>} events 0 When the view is clicked, trigger the rule.
         * @readonly
         */
        this.events = { click: 'trigger' };

        /**
         * @property {Object} options - Options provided in the constructor.
         */
        this.options = options;

        /**
         * @property {Backbone.View} - The parent view of the indicator.
         */
        this.parent = this.options.parent;

        this.listenTo(this.model, 'remove', this.remove);
        this.render(options.callback || $.noop);
    }

    /**
     * @property {string} tagName - The root element of the view.
     * @readonly
     * @default 'div'
     */
    get tagName () {
        return 'div';
    }

    /**
     * The views click event.
     * @param {Event} e Event Object
     */
    trigger (e) {
        let id = this.model.get('id');

        if (e) {
            e.preventDefault();
        }

        ELF.trigger(`DASHBOARD:TriggerIndicator/${id}`, { indicator: id }, this.parent);
    }

    /**
     * Renders the view.
     * @param {Function} callback -  A callback function invoked after rendering the indicator passing this view.
     */
    render (callback) {
        let  stringsToFetch = {},
            label = this.model.get('label'),
            image = this.model.get('image'),
            content;

        if (label) {
            stringsToFetch.label = label;
        }

        if (image) {
            stringsToFetch.image = {
                key       : image,
                namespace : 'ICONS'
            };
        }

        if (image || label) {
            LF.getStrings(stringsToFetch, _(function (strings) {
                try {
                    this.$el.html('').attr('id', this.model.get('id'));
                    this.$el.html('').addClass('indicator');
                    this.$el.html('').addClass(this.model.get('className'));

                    if (image && label) {
                        content = LF.templates.display('DEFAULT:IndicatorFull', {
                            image: strings.image,
                            label: strings.label
                        });
                    } else {
                        if (label) {
                            content = LF.templates.display('DEFAULT:IndicatorLabel', { label: strings.label });
                        }

                        if (image) {
                            content = LF.templates.display('DEFAULT:IndicatorImage', { image: strings.image });
                            this.$el.html('').addClass('top');
                        }
                    }

                    this.$el.append(content);
                    this.delegateEvents();
                    callback(this);

                } catch (err) {
                    logger.error(err.toString());
                    callback(this);
                }
            }).bind(this));
        }  else {
            logger.warn(`Indicator ${this.model.get('id')} missing label and image.`);
            callback(this);
        }
    }
}

window.LF.View.IndicatorView = IndicatorView;