import PasswordBaseView from 'core/views/PasswordBaseView';

// TODO - This view is using deprecated code and needs to be updated.
export default class ChangeTemporaryPasswordView extends PasswordBaseView {
    constructor (options) {
        super(options);

        /**
         * @property {string} id - The unique id of the root element.
         * @readonly
         * @default 'change-temporary-password-page'
         */
        this.id = 'change-temporary-password-page';

        /**
         * @property {string} templateSrc - Id of template to render
         * @type String
         * @readonly
         * @default '#change-password-template'
         */
        this.templateSrc = '#change-temporary-password-template';

        /**
         * @property {Object<string,(Object|string)>} stringsToFetchForRender - Strings to fetch in the render function
         * @type Object
         */
        this.stringsToFetchForRender = {
            activatedElsewhere: 'DEACTIVATED_LABEL',
            header: 'APPLICATION_HEADER',
            title: 'CHANGE_PASSWORD',
            newPassword: 'ENTER_NEW_PASSWORD',
            back: 'BACK',
            confirmPassword: 'CONFIRM_PASSWORD',
            submit: 'OK',
            submitIcon: {
                key: 'OK',
                namespace: 'ICONS'
            },
            pwdRules: 'PASSWORD_RULES'
        };
    }

    /**
     * Resolve any dependencies required for the view to render.
     * @returns {Q.Promise<void>}
     */
    resolve () {
        return super.resolve().then(() => {
            // Initialize...
            this.cacheTemplate(this.templateSrc, 'ChangeTemporaryPasswordView');
            this.fetchData(['Subjects'], () => {
                //@todo remove
            });
        });
    }

    /**
     * Navigates back to the toolbox view.
     */
    back () {
        LF.router.navigate('login', true);
        localStorage.removeItem('PHT_changePassword');
    }

    /**
     * Renders the view.
     * @returns {ChangeTemporaryPasswordView}
     */
    render () {
        this.template = _.template(this.templateContent);

        LF.getStrings(this.stringsToFetchForRender, _(strings => {
            this.$el.html(this.template(_.extend(strings, {
                key: this.key,
                passwordRule: (LF.StudyDesign.showPasswordRules) ? LF.templates.display('DEFAULT:PasswordRules', {
                    text : strings.pwdRules
                }) : ''
            })));

            title(strings.header);
            page(this.el);
        }).bind(this));

        return this;
    }

    /**
     * Transmit all records to the web service once the password is saved locally.
     * @example this.onPasswordSaved();
     */
    onPasswordSaved () {
        LF.security.pauseSessionTimeOut();
        try {
            if (LF.StudyDesign.askSecurityQuestion) {
                localStorage.setItem('Reset_Password_Secret_Question', true);
                this.navigate('reset_secret_question', true);
            } else {
                localStorage.removeItem('PHT_changePassword');

                LF.security.login(this.user.id, $('#txtNewPassword').val()).then(() => {
                    this.navigate('dashboard', true);
                });
            }
        }
        finally {
            LF.security.restartSessionTimeOut();
        }
    }
}

window.LF.View.ChangeTemporaryPasswordView = ChangeTemporaryPasswordView;
