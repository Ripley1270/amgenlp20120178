import * as lStorage from 'core/lStorage';
import Logger from 'core/Logger';
import CoreLogin from 'core/views/LoginView';

const logger = new Logger('ContextSwitchingView');

export default class ContextSwitchingView extends CoreLogin {
    constructor (options) {
        super(options);

        logger.trace('Constructor');

        /**
         * @property {string} viewTriggerName - trigger named used by ELF
         */
        this.viewTriggerName = 'CONTEXTSWITCH';

        logger.traceExit('Constructor');
    }

    /**
     * @property {string} id  -The ID of the view.
     * @default 'context-switching-view'
     */
    get id () {
        return 'context-switching-view';
    }

    /**
     * Render the view to the DOM.
     * example this.render();
     * @returns {Q.Promise<ContextSwitchingView>} promise resolved when render is complete
     */
    render () {
        logger.traceEnter('render');

        return this.buildHTML({}, true)
        .then(() => {
            return this.userLoginSetup();
        })
        .finally(() => {
            LF.spinner.hide();

            lStorage.removeItem('isAuthorized');
            LF.security.logout();

            logger.traceExit('render');

            return this;
        });
    }
}
