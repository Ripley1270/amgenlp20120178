import SupportBaseView from 'core/views/SupportBaseView';

export default class SupportView extends SupportBaseView {
    /**
    * @property {string} id - The ID of the view.
    * @readonly
    * @default 'support-page'
    */
    get id () {
       return 'support-page';
    }

    /**
     * @property {string} id - Id of template to render.
     * @readonly
     * @default '#support-template'
     */
    get template () {
        return '#support-template';
    }

    /**
     * Navigates back to the dashboard view.
     */
    back () {
        this.sound.play('click-audio');
        this.navigate('toolbox', true);
    }
}
