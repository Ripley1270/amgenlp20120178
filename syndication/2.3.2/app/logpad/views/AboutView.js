import PageView from 'core/views/PageView';
import Transmissions from 'core/collections/Transmissions';
import CurrentSubject from 'core/classes/CurrentSubject';
import { getCurrentProtocol } from 'core/Helpers';

/*
 * @class AboutView
 * @description Details basic information about the LPA and the device.
 * @extends PageView
 */
export default class AboutView extends PageView {
    constructor (options) {
        super(options);

        /**
         * @property {Object<string,string>} events - The view's events.
         * @readonly
         */
        this.events = {
            // Click event for back button
            'click #back': 'back'
        };

        /**
         * @property {Object<string,string>} templateStrings - Strings to fetch in the render function
         */
        this.templateStrings = {
            title: 'ABOUT',
            header: 'APPLICATION_HEADER',
            studyVersion: 'STUDY_VERSION',
            activationCode: 'ACTIVATION_CODE',
            studyURL: 'STUDY_URL',
            subjectID: 'SUBJECT_ID',
            logLevel: 'LOG_LEVEL',
            siteCode: 'SITE',
            protocol: 'PROTOCOL',
            clientName: 'SPONSOR',
            studyReports: 'STORED_REPORTS',
            coreProductVer: 'CORE_PRODUCT_VERSION_LABEL'
        };
    }

    /**
     * @property {string} id - The unique id of the root element.
     * @readonly
     * @default 'about-page'
     */
    get id () {
        return 'about-page';
    }

    /**
     * @property {string} template - Id of template to render
     * @readonly
     * @default '#about-template'
     */
    get template () {
        return '#about-template';
    }

    /**
     * Resolve any dependencies required for the view to render.
     * @returns {Q.Promise<void>}
     */
    resolve () {
        let fetchTransmissionCount = () => {
            let col = new Transmissions();

            return col.fetch().then(() => {
                return col.size();
            });
        };

        return Q.all([CurrentSubject.getSubject(), fetchTransmissionCount()])
        .spread((subject, transmissionCount) => {
            this.subject = subject;
            this.transmissionCount = transmissionCount;
        });
    }

    /**
     * Navigates back to the dashboard view.
     */
    back () {
        this.sound.play('click-audio');
        this.navigate('toolbox');
    }

    /**
     * Renders the view.
     * @returns {Q.Promise<Object>} A map of translated strings.
     */
    render () {
        return this.buildHTML({
            studyVersionValue   : LF.StudyDesign.studyVersion,
            activationCodeValue : this.subject.get('subject_id'),
            studyURLValue       : localStorage.getItem('PHT_serviceBase'),
            subjectIDValue      : this.subject.get('subject_number'),
            logLevelValue       : this.subject.get('log_level'),
            siteCodeValue       : this.subject.get('site_code'),
            protocolValue       : getCurrentProtocol(this.subject),
            clientNameValue     : LF.StudyDesign.clientName,
            studyReportCounts   : this.transmissionCount,
            coreProductVerValue : LF.coreVersion
        }, true).then(() => {
            LF.Wrapper.exec({
                execWhenWrapped : () => {
                    this.$('#about-url').css('color', '#2F3E46');
                    this.$('.hidden-wrapper').removeClass('hidden-wrapper');
                }
            });
            window.scrollTo(0,0);
            this.delegateEvents();
        });
    }
}
