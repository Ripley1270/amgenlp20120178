import PasswordBaseView from 'core/views/PasswordBaseView';
import Users from 'core/collections/Users';
import ELF from 'core/ELF';
import Logger from 'core/Logger';
import templates from 'core/resources/Templates';

let logger = new Logger('ResetPasswordActivationView');

export default class ResetPasswordActivationView extends PasswordBaseView {
    constructor (options) {
        super(options);

        /**
         * @property {Object<string,string>} templateStrings - Strings to fetch in the render function.
         */
        this.templateStrings = {
            header          : 'APPLICATION_HEADER',
            title           : 'RESET_PASSWORD',
            newPassword     : 'ENTER_NEW_PASSWORD',
            confirmPassword : 'CONFIRM_PASSWORD',
            submit          : 'OK',
            submitIcon      : {
                key       : 'OK',
                namespace : 'ICONS'
            },
            pwdRules        : 'PASSWORD_RULES'
        };
    }

    /**
     * @property {string} id - The unique id of the root element.
     * @readonly
     * @default 'reset-password-activation-page'
     */
    get id () {
        return 'reset-password-activation-page';
    }

    /**
     * @property {string} template - Id of template to render
     * @readonly
     * @default '#reset-password-template'
     */
    get template () {
        return '#reset-password-template';
    }

    /**
     * Resolve any dependencies required for the view to render.
     * @returns {Q.Promise<void>}
     */
    resolve () {
        let users = new Users();

        return users.fetch().then(() => {
            let user = users.findWhere({ id: 0 });

            this.user = user;
        }).then(() => super.resolve());
    }

    /**
     * Renders the view.
     * @returns {Q.Promise<void>}
     * @example this.render();
     */
    render () {
        let request = Q();

        if (LF.StudyDesign.showPasswordRules) {
            request = request.then(() => this.i18n('PASSWORD_RULES'))
            .then(text =>  templates.display('DEFAULT:PasswordRules', { text }));
        }

        return request.then((passwordRules = '') => {
            return this.buildHTML({
                key: this.key,
                passwordRules
            }, true);
        });
    }

    /**
     * Submits the new password to the web service.
     * @param {Event} e Event data.
     */
    submit (e) {
        let password = this.$('#txtNewPassword').val();

        e.preventDefault();

        this.$('#txtNewPassword').blur();
        this.$('#txtConfirmPassword').blur();

        if (this.validatePasswords() && !this.submitted) {
            if (LF.StudyDesign.askSecurityQuestion) {
                this.submitted = true;
                LF.Data.password = password;
                this.navigate('reset_secret_question_activation');
            } else {
                this.reactivate();
            }
        }
    }

    /**
     * Submits the new password and secret question and answer to the web service.
     */
    reactivate () {
        let password = this.$('#txtNewPassword').val();

        // Handle a successful reactivation.
        let handleResult = (res) => {
            LF.Data.unlockCode = undefined;
            LF.Data.deviceID = res.D;
            LF.Data.service_password = res.W;
            LF.Data.password = password;

            logger.operational('Password reset on activation');

            let token = `${LF.Data.code}:${LF.Data.service_password}:${LF.Data.deviceID}`;

            ELF.trigger('REACTIVATION:Transmit', { }, this).finally(() =>  this.getSubjectData(token));
        };

        // Handle a transmission error.
        let handleError = (errorCode, httpCode) => {
            this.showTransmissionError({errorCode: errorCode, httpCode: httpCode});
        };

        localStorage.removeItem('Reset_Password_Secret_Question');
        localStorage.removeItem('Reset_Password');

        this.attemptTransmission().then((online) => {
            if (online) {
                let subjectData = {
                    U: LF.Data.code,
                    W: hex_sha512(password + localStorage.getItem('krpt')),
                    code: LF.Data.unlockCode
                };

                if (localStorage.getItem('PHT_IMEI')) {
                    subjectData.S = localStorage.getItem('PHT_IMEI');
                    subjectData.M = device.model;
                }
                LF.webService.getDeviceID(subjectData, handleResult, handleError);
            }
        }).done();
    }
}

window.LF.View.ResetPasswordActivationView = ResetPasswordActivationView;
