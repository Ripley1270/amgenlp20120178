import ActivationBaseView from 'core/views/ActivationBaseView';
import Data from 'core/Data';

export default class SecretQuestionView extends ActivationBaseView {

    constructor (options = {}) {
        super(options);

        /**
         * @property {string} template - Id of template to render.
         * @readonly
         * @default '#secret-question-template'
         */
        this.template = '#secret-question-template';

        /**
         * @property {boolean} submitted - The flag prevents users from submitting the form consecutively
         * @default false
         */
        this.submitted = false;

        /**
         * @property {Object<string,string>} selectors - A list of selectors to populate.
         */
        this.selectors = {
            secretQuestion : '#secret_question',
            secretAnswer: '#secret_answer',
            submit: '#submit'
        };

        /**
         * @property {Object<string,string> events - The view's events.
         * @readonly
         */
        this.events = {

            /** Tap event for going back */
            'click #back'             : 'back',

            /** Input event for secret answer */
            'input #secret_answer'  : 'validate',

            /** Submit form event */
            'click #submit'           : 'submit',

            /** Change event for secret question drop down list */
            'change #secret_question': 'clearSecretAnswer'
        };


        /**
         * @property {Object<string,string>} templateStrings - Strings to fetch in the render function.
         * @type Object
         */
        this.templateStrings = {
            header          : 'APPLICATION_HEADER',
            title           : 'DEVICE_ACTIVATION',
            secretAnswer    : 'SECRET_ANSWER',
            secretQuestion  : 'SECRET_QUESTION',
            submit          : 'OK',
            back            : 'BACK',
            submitIcon      : {
                key       : 'OK',
                namespace : 'ICONS'
            }
        };

        // @todo Move navigation logic to RegistrationController
        this.preInstallCheck();
    }

    /**
     * @property {string} id - The unique id of the root element.
     * @readonly
     * @default 'secret-question-page'
     */
    get id () {
        return 'secret-question-page';
    }

    /**
     * Navigates back to the activation view.
     */
    back () {
        this.sound.play('click-audio');
        this.navigate('activation');
    }

    /**
     * Renders the view.
     * @returns {Q.Promise<void>}
     */
    render () {
        let questions = LF.StudyDesign.securityQuestions;

        _(questions).each(value => {
            if (value.display) {
                this.templateStrings[value.text] = value.text;
            }
        });

        return this.buildHTML({ key: this.key }, true)
        .then(strings => {
            // Append the question options to the select element.
            _(questions).each(value => {
                if (value.display) {
                    this.$secretQuestion.append(`<option value="${value.key}">${strings[value.text]}</option>`);
                }
            });

            this.$secretQuestion.select2({
                minimumResultsForSearch: Infinity,
                width: '100%'
            });
        });
    }

    /**
     * Clears the secret answer field and remove icons.
     */
    clearSecretAnswer () {
        let secretAnswer = this.$secretAnswer;

        secretAnswer.val('');
        this.clearInputState(secretAnswer);
        this.$submit.attr('disabled', 'disabled');
    }

    /**
     * Validates the secret answer field.
     * @return {boolean} Returns true if secret answer is correct, otherwise false.
     */
    validatePage () {
        let $btn = this.$submit,
            $target = this.$secretAnswer,
            codeFormat = new RegExp(LF.StudyDesign.answerFormat || '.{2,}');

        if (codeFormat.test($target.val())) {
            $btn.removeAttr('disabled');
            this.inputSuccess($target);
            return true;
        } else {
            $btn.attr('disabled', 'disabled');
            this.inputError($target);
            return false;
        }
    }

    /**
     * Validate the page.
     */
    validate () {
        this.validatePage();
    }

    /**
     * Submits the form
     * @param {(MouseEvent|TouchEvent)} e - A mouse or touch event.
     */
    submit (e) {
        e.preventDefault();
        this.$secretAnswer.blur();

        Data.answer = hex_sha512(this.$secretAnswer.val());
        Data.question = this.$secretQuestion.val();

        if (this.validatePage() && !this.submitted) {
            // Prevent the user from submitting the form again.
            this.submitted = true;
            this.activate();
        }
    }
}
