import ActivationBaseView from 'core/views/ActivationBaseView';
import templates from 'logpad/resources/Templates';
import Data from 'core/Data';
import COOL from 'core/COOL';

export default class ActivationView extends ActivationBaseView {
    constructor (options) {
        super(options);

        /**
         * @property {Object<string,string>} selectors - A list of selectors to populate.
         */
        this.selectors = {
            newPassword: '#txtNewPassword',
            confirmPassword: '#txtConfirmPassword'
        };

        /**
         * @property {Object<string,string>} events - Activation view's event list.
         * @readonly
         */
        this.events = {
            // Submit form event
            'click #submit': 'submit',

            // Input event for the new password 
            'input input#txtNewPassword': 'validateNewPassword',

            // Input event for the confirmation password
            'input input#txtConfirmPassword': 'validatePasswords',

            // Click event for going back to the privacy policy
            'click #back': 'privacyPolicy'
        };

        /**
         * @property {Object<string,(Object|string)>} templateStrings - Strings to fetch in the render function
         * @type Object
         */
        this.templateStrings = {
            header          : 'APPLICATION_HEADER',
            title           : 'DEVICE_ACTIVATION',
            password        : 'ENTER_NEW_PASSWORD',
            confirmPassword : 'CONFIRM_PASSWORD',
            submit          : 'OK',
            activationCode  : 'ACTIVATION_CODE',
            back            : 'BACK',
            submitIcon      : {
                key         : 'OK',
                namespace   : 'ICONS'
            },
            pwdRules        : 'PASSWORD_RULES'
        };

        // Initialize...
        this.preInstallCheck();
    }

    /**
     * @property {string} id - The id of the root element.
     * @readonly
     * @default 'activation-page'
     */
    get id () {
        return 'activation-page';
    }

    /**
     * @property {string} template - Id of template to render
     * @readonly
     * @default '#activation-template'
     */
    get template () {
        return '#activation-template';
    }

    /**
     * Renders the view.
     * @returns {Q.Promise<void>}
     */
    render () {
        let request = Q();

        if (LF.StudyDesign.askSecurityQuestion) {
            this.templateStrings.submit = 'NEXT';
            this.templateStrings.submitIcon.key = 'LOGIN';
        }

        if (LF.StudyDesign.showPasswordRules) {
            request = request.then(() => this.i18n('PASSWORD_RULES'))
                .then(text => templates.display('DEFAULT:PasswordRules', { text }));
        }

        return request.then((passwordRules = '') => {
            return this.buildHTML({
                code: Data.code,
                key: this.key,
                passwordRules: passwordRules
            }, true);
        });
    }

    /**
     * Validates the form, and then calls the transmit method. Routes to install if successful.
     * @param {(MouseEvent|TouchEvent)} e - A mouse or touch event.
     */
    submit (e) {
        e.preventDefault();

        if (this.validatePasswords()) {
            Data.password = this.$newPassword.val();
            if (LF.StudyDesign.askSecurityQuestion) {
                this.navigate('secret_question');
            } else {
                this.activate();
            }
        } else {
            this.sound.play('error-audio');
        }
    }

    /**
     * Navigate back to the privacy policy  view
     */
    privacyPolicy () {
        this.navigate('privacy_policy_activation');
    }
}

COOL.add('ActivationView', ActivationView);
