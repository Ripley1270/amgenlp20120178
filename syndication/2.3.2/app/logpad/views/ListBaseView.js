import { getScheduleModels } from 'core/Helpers';
import ELF from 'core/ELF';
import Logger from 'core/Logger';

const logger = new Logger('ListBaseView');

export default class ListBaseView extends Backbone.View {

    constructor (options) {
        super(options);

        /**
         * @property {Array} subviews - Array of subviews appended to this view.
         */
        this.subviews = [];
    }

    /**
     * Renders the view and create subviews from models in this.collection.
     * @param {Function} callback - A callback function invoked after rendering the items passing this.
     */
    render (callback) {
        let schedules = this.options.schedules,
            step = (index) => {
                this.add(schedules.models[index], () => {
                    if (index < schedules.length - 1) {
                        step(index + 1);
                    } else {
                        this.options.container.append(this.$el);
                        this.$el.show();
                        (callback || $.noop)(this);
                    }
                });
            };

        this.clear();
        step(0);
    }

    /**
     * Add a new subview after original render with the correct order from study.demo.
     * @param {Schedule} addedSchedule - schedule of item to be added.
     * @param {Function} callback - Callback function called after new item is added.
     */
    addInOrder (addedSchedule, callback) {
        let allSchedules,
            newView,
            availableSchedules = LF.schedule.availableSchedules,
            prevIndex = 0;

        // This function gets called for every instance of this view (QuestionnairesList and IndicatorsList in some cases).
        //  So, check if the schedule pertains to this list view before adding and rendering new item.
        if (addedSchedule.get('target').objectType !== this.objectType) {
            callback();
            return;
        }

        allSchedules = this.getSchedules(getScheduleModels());

        // Loop through all schedules, incrementing prevIndex if rendered until the addedSchedule is found. Append that view at prevIndex
        // eslint-disable-next-line consistent-return
        _.every(allSchedules, (schedule) => {
            let item,
                rendered,
                availableSchedule,
                children = this.$el.children();

            if (schedule.get('id') === addedSchedule.get('id')) {
                item = this.collection.findWhere({ id: addedSchedule.get('target').id });

                this.createSubview(item, schedule.get('id'), (view, ruleString) => {
                    if (prevIndex > 0) {
                        this.$el.children().eq(prevIndex - 1).after(view.$el);
                    } else if (children.size() !== 0) {
                        children.eq(children.size() - 1).before(view.$el);
                    } else {
                        this.$el.prepend(view.$el);
                    }

                    this.subviews.splice(prevIndex, 0, view);
                    newView = view;

                    logger.operational(`Displaying ${this.objectType} ${item.id} to user.`);

                    (this.refresh || $.noop)();
                    ELF.trigger(ruleString, {}, LF.router.view());

                    // Location to render has been found, we don't have to iterate through the rest
                    return false;
                });
            } else {
                availableSchedule = availableSchedules.findWhere({ id: schedule.get('id') });

                if (availableSchedule) {
                    rendered = _.findWhere(this.subviews, { id: schedule.get('target').id });

                    if (rendered) {
                        prevIndex++;
                    }
                }

                // Continue _.every loop
                return true;
            }
        });

        callback(newView);
    }

    /**
     * Add a new subview if the schedule's target type matches
     * @param {Schedule} schedule - schedule of item to be added
     * @param {Function} callback - A callback function invoked after rendering the questionnaire passing the newly created subview or false if none were created.
     */
    add (schedule, callback) {
        let subviews = this.subviews,
            target = schedule.get('target'),
            item = this.collection.where({
                id : target.id
            })[0];

        if (item && target.objectType === this.objectType) {

            this.createSubview(item, schedule.get('id'), (view, ruleString) => {
                this.$el.append(view.$el);
                subviews.push(view);

                ELF.trigger(ruleString, {}, LF.router.view())
                .then(() => LF.security.checkLogin())
                .catch((err) => {
                    err && logger.error('Error checking login', err);
                    return false;
                })
                .then((isLoggedIn) => {
                    if (isLoggedIn || target.objectType === 'indicator') {
                        logger.operational(`Displaying ${this.objectType} ${item.id} to user.`, {
                            objectType: this.objectType,
                            id: item.id
                        });
                    }
                })
                .then(() => callback(view))
                .done();
            });
        } else {
            callback();
        }
    }

    /**
     * Delete a subview from the list by schedule
     * @param {Schedule} schedule - schedule of item to be removed
     */
    removeSubview (schedule) {
        if (schedule.get('target').objectType === this.objectType) {
            let subview = _.filter(this.subviews, (view) => {
                return view.model.get('id') === schedule.get('target').id;
            })[0];

            if (subview) {
                subview.remove();
                this.subviews.splice(this.subviews.indexOf(subview), 1);
            }
        }
    }

    /**
     * Filters schedules to only the ones that matter to this view.
     *   Currently a passthrough made to be overwritten if filtering is needed
     * @param {Object} schedules - Collection of schedules to filter
     * @return {Array} returns filtered list of schedules
     */
    getSchedules (schedules) {
        return schedules;
    }

    /**
     * Close and remove each subview from the DOM to ensure listener cleanup
     */
    close () {
        this.clear();
        this.remove();
    }

    /**
     * Clear subviews without removing this view
     */
    clear () {
        _.each(this.subviews, (view) => {
            view.remove();
        });
        this.subviews = [];
        this.unbind();
    }
}

window.LF.View.ListBaseView = ListBaseView;
