// Legacy code exemptions:
// jscs:disable requireTemplateStrings, requireArrowFunctions

import ActivationBaseView from 'core/views/ActivationBaseView';
import * as lStorage from 'core/lStorage';
import Data from 'core/Data';
import { setServiceBase } from 'core/utilities';
import Logger from 'core/Logger';
import COOL from 'core/COOL';
import Users from 'core/collections/Users';

let logger = new Logger('CodeEntryView');

export default class CodeEntryView extends ActivationBaseView {

    constructor (options) {
        super(options);

        logger.traceEnter('Constructor');

        /**
         * @property {Object<string,string>} selectors - A list of selectors to generate.
         */
        this.selectors = {
            txtCode: '#txtCode',
            txtStudy: '#txtStudy',
            next: '#next'
        };

        /**
         * @property {Object<string,string> events - Code Entry view's event list.
         * @readonly
         */
        this.events = {
            // Submit form event
            'click #next': 'submit',

            // Input event for code entry
            'input #txtCode': 'onInput',

            // Input event for url entry
            'input #txtStudy': 'onInput',

            // Click event for scanner button
            'click #scanner': 'scan'
        };

        /**
         * @property {Object<string,(Object|string)>}Strings to fetch in the render function
         */
        this.templateStrings = {
            header: 'APPLICATION_HEADER',
            title: 'DEVICE_ACTIVATION',
            activationCode: 'ACTIVATION_CODE',
            submit: 'NEXT',
            studyUrl: 'STUDY_URL',
            submitIcon: {
                key: 'LOGIN',
                namespace: 'ICONS'
            }
        };

        // Initialize...
        lStorage.setItem('activationRole', 'subject');
        localStorage.removeItem('activationCode');
        localStorage.removeItem('activationFlag');
        logger.traceExit('Constructor');
    }

    /**
     * @property {string} id - The id of the root element.
     * @readonly
     * @default 'code-entry-page'
     */
    get id () {
        return 'code-entry-page';
    }

    /**
     * @property {string} template - Id of template to render
     * @readonly
     * @default '#codeEntry-template'
     */
    get template () {
        return '#codeEntry-template';
    }

    /**
     * Renders the view.
     * @returns {Q.Promise<void>}
     */
    render () {
        logger.traceEnter('render');
        return this.buildHTML({
            key: this.key
        }, true)
        .catch((err) => logger.error('CodeEntryView could not render', err))
        .finally(() => logger.traceExit('render async part'));
    }

    /**
     * Process a setupCode and will ask the server for validation.
     * @param {string} code - The setup code to process.
     * @returns {Q.Promise<void>}
     */
    processSetupCode (code) {
        // eslint-disable-next-line consistent-return
        const process = () => {
            if (this.isValidForm('#txtCode')) {
                let studyNameOrUrl = this.$txtStudy.val();
                let fullUrl = this.getStudyUrl(studyNameOrUrl);
                setServiceBase(fullUrl);
                return this.checkSubjectIsActive(code);
            } else {
                this.sound.play('error-audio');
                this.showInputError('#txtCode', 'INVALID_CODE');
            }
        };

        // Closes the virtual keyboard and scrolls to top of page
        window.scrollTo(0, 0);

        // only process setup code if there is no rule, or a rule returns false
        return ELF.trigger('CODEENTRY:Submit', { value: this.$txtStudy.val(), product: 'logpad' }, this)
        .then(res => {
            if (!res.preventDefault) {
                process();
            }
        })
        .catch((err) => {
            logger.error('Error processing Setup Code', err);
        });
    }

    /**
     * Submits the form and validates the setup code.
     * @param {(MouseEvent|TouchEvent)} e - A mouse or touch event.
     * @returns {Q.Promise<void>}
     */
    submit (e) {
        let input = this.$txtCode,
            code = input.val().trim();

        e.preventDefault();
        input.blur();
        return this.processSetupCode(code);
    }

    /**
     * Sets the subject language.
     * @param {Object} res - The response from the getSubjectActive API call.
     */
    setSubjectLanguage (res) {
        logger.trace('checkSubjectIsActive->handleResult->setSubjectLanguage');
        // Pick the language out of the server response, if set
        if (res && res.X) {
            let ary = res.X.split('-');
            if (ary.length === 2) {
                this.preferredLanguage = ary[0];
                this.preferredLocale = ary[1];
            }
        }

        if (!this.preferredLocale && !this.preferredLanguage) {
            this.preferredLanguage = LF.StudyDesign.defaultLanguage;
            this.preferredLocale = LF.StudyDesign.defaultLocale;
        }

        LF.content.setContextLanguage(`${LF.StudyDesign.defaultLanguage}-${LF.StudyDesign.defaultLocale}`);

        // To change the time zone we need to record this in localStorage
        localStorage.setItem('preferredLanguageLocale', `${this.preferredLanguage}-${this.preferredLocale}`);
    }

    /**
     * Checks the setup code against the server. Handles transmission result and error.
     * @param {string} code - The setup code to process.
     * @returns {Q.Promise<void>}
     */
    checkSubjectIsActive (code) {
        logger.traceEnter(`checkSubjectIsActive code:${code}`);

        let activationCode = code.toString(),
            handleResult = (res) => {
                let lsKrpt = localStorage.getItem('krpt');
                logger.trace(`checkSubjectIsActive->handleResult res:${res}`);
                if (_.isEmpty(res)) {
                    lStorage.removeItem('serviceBase');

                    this.showInputError('#txtCode', 'INVALID_CODE');
                    this.disableButton(this.$next);
                    this.sound.play('error-audio');
                    this.removeMessage();
                } else {
                    this.setSubjectLanguage(res);

                    Data.code = activationCode;

                    // This will enable changing to second user if first user's account is locked by providing
                    // wrong password and moving back to CodeEntryView (DE20655).
                    if (lsKrpt && lsKrpt !== res.K) {
                        let users = new Users();
                        users.fetch()
                        .then(() => {
                            let user = users.findWhere({ id: 0 });
                            if (user) {
                                LF.security.resetFailureCount(user);
                                user.save();
                            }
                        });
                    }

                    localStorage.setItem('krpt', res.K);

                    // The flag is true, if it is activation process; false, if it is reactivation process
                    Data.activation = !res.S;

                    // To change the time zone we need to record this in localStorage
                    localStorage.setItem('activationCode', activationCode);
                    localStorage.setItem('activationFlag', !res.S);

                    logger.operational(`Setup code is verified by server. ${!res.S ? 'Activation' : 'Reactivation'} is starting.`);

                    this.navigate('set_time_zone_activation', true);
                    this.removeMessage();
                }
            },
            handleError = (errorCode, httpCode, isSubjectActive, isTimeout) => {
                let errorMsg = 'INVALID_CODE';

                logger.trace(`checkSubjectIsActive->handleError errorCode: ${errorCode}, httpCode: ${httpCode}, isTimeout: ${isTimeout}`);

                lStorage.removeItem('serviceBase');

                this.removeMessage();

                if (isTimeout) {
                    errorMsg = 'CONNECTION_TIMEOUT';
                } else if (httpCode === 404) {
                    errorMsg = 'CODE_ENTRY_NO_SERVER';
                }

                this.showInputError('#txtCode', errorMsg);
                this.disableButton(this.$next);

                this.sound.play('error-audio');
            };

        return this.attemptTransmission()
        .then(online => {
            // If offline, the attemptTransmission method will handle display of a modal.
            if (online) {
                // FIXME: Th callbacks are used but a promise is returned. The code will continue before the callbacks are called.
                return this.webService.getSubjectActive(activationCode, handleResult, handleError);
            }

            lStorage.removeItem('serviceBase');
            return Q();
        })
        .finally(() => {
            logger.traceExit('checkSubjectIsActive');
        });
    }

    /**
     * Interpret a QR Text string
     * @param {string} text - the string to interpret
     */
    processQRText (text) {
        // Called both by a received QR scan OR if the user-setupCode looks like a json string
        // which is used for debugging or pointing the system at a different URL for testing etc.
        let qrParams, studyUrl,
            badQRCode = () => {
                this.disableButton(this.$next);
                this.showInputError('#txtCode', 'ERROR_TITLE');
            };

        logger.traceEnter(`processQRText text:${text}`);

        if (text) {
            try {
                qrParams = $.parseJSON(text);

                if (qrParams.study) {
                    studyUrl = this.getStudyUrl(qrParams.study);

                    if (qrParams.setupcode) {
                        this.$txtStudy.val(studyUrl);
                        this.$txtCode.val(qrParams.setupcode);
                        setServiceBase(studyUrl);

                        // If the language is present, let's take it (otherwise we take it from the study)
                        if (qrParams.language) {
                            let ary = qrParams.language.split('-');
                            if (ary.length === 2) {
                                this.preferredLanguage = ary[0];
                                this.preferredLocale = ary[1];
                            }
                        }

                        this.clearInputState('#txtCode');
                        this.clearInputState('#txtStudy');

                        this.enableButton(this.$next);
                    } else {
                        badQRCode();
                        this.sound.play('error-audio');
                        logger.error('Missing setupcode param in QR text');
                    }
                } else {
                    badQRCode();
                    this.sound.play('error-audio');
                    logger.error('Missing study param in QR text');
                }
            } catch (e) {
                badQRCode();
                this.sound.play('error-audio');
                logger.error(`json qrcode didnt parse; text:${text}`);
            }
        }
    }

    /**
     * Activate the QR Scanner to take picture and process it
     */
    scan () {
        cordova.plugins.barcodeScanner.scan((result) => this.processQRText(result.text));
    }

    /**
     * Enables the next button when setup code and study URL input are both filled out
     */
    onInput () {
        let validCode = this.$txtCode.val().length > 0,
            validUrl = this.$txtStudy.val().length > 0;

        if (validCode && validUrl) {
            this.enableButton(this.$next);
        } else {
            this.disableButton(this.$next);
        }
    }

    /*
     * Handles the restricted custom environment entry attempts
     */
    handleDisabledCustomEnvironment () {
        logger.error('Custom environments are disabled');
        this.sound.play('error-audio');
        this.showInputError('#txtStudy', 'CUSTOM_ENVIRONMENT_USAGE_ERROR');
    }

    /**
     * Returns the URL for the study
     * @param {string} studyName - The text entered as the study name.
     * @returns {(string|null)} The templated URL.
     */
    getStudyUrl (studyName) {
        let environment,
            disableCustomEnvironments = LF.StudyDesign.disableCustomEnvironments !== undefined ?
            LF.StudyDesign.disableCustomEnvironments :
            LF.CoreSettings.disableCustomEnvironments;

        if (studyName === 'trainer') {
            return studyName;
        }

        // if it looks like a URL
        if (/^http/i.test(studyName)) {
            // try matching the url of an environment
            environment = _.find(LF.StudyDesign.environments.toJSON(), (env) => {
                return studyName === env.url;
            });

            // Use the entered url if it matches an existing environment or Custom environments are not disabled
            if (environment || !disableCustomEnvironments) {
                return studyName;
            }

            // StudyName does not match the url of any environment and custom environments are disabled
            if (disableCustomEnvironments) {
                this.handleDisabledCustomEnvironment();
                return null;
            }
        }

        // try matching on the hostname part of an environment
        environment = _.find(LF.StudyDesign.environments.toJSON(), (env) => {
            // eslint-disable-next-line prefer-template
            return new RegExp('^https?://' + studyName + '([/.]|$)').test(env.url);
        });

        if (environment) {
            return environment.url;
        }

        // also matching on the id of an environment
        environment = _.findWhere(LF.StudyDesign.environments.toJSON(), { id: studyName });

        if (environment) {
            return environment.url;
        }

        // StudyName does not exist in any environment and custom environemnets are disabled
        if (disableCustomEnvironments) {
            this.handleDisabledCustomEnvironment();
            return null;
        }

        // If no match in environments, construct a url from template
        return _.template(LF.StudyDesign.serviceUrlFormat, {
            interpolate: /{{([\s\S]+?)}}/g
        })({ study: studyName });
    }
}

window.LF.View.CodeEntryView = CodeEntryView;
COOL.add('CodeEntryView', CodeEntryView);
