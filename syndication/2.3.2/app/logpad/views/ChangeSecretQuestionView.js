import SecretQuestionBaseView from 'core/views/SecretQuestionBaseView';
import ELF from 'core/ELF';
import Logger from 'core/Logger';
import Spinner from 'core/Spinner';

let logger = new Logger('ChangeSecretQuestionView');

export default class ChangeSecretQuestionView extends SecretQuestionBaseView {
    constructor (options) {
        super(options);

        /**
         * @property {Object<string,string>} selectors - A list of selectors to populate.
         */
        this.selectors = {
            form: '#changeSecretQuestionForm',
            inactiveMsg: '#inactiveMsg',
            secretAnswer: '#secret_answer',
            secretQuestion: '#secret_question',
            currentPassword: '#txtCurrentPassword'
        };

        /**
         * @property {Object<string,string>} templateStrings - Strings to fetch in the render function
         */
        this.templateStrings = {
            activatedElsewhere: 'DEACTIVATED_LABEL',
            header: 'APPLICATION_HEADER',
            title: 'CHANGE_SECRET_QUESTION',
            currentPassword: 'PASSWORD',
            secretAnswer: 'SECRET_ANSWER',
            secretQuestion: 'SECRET_QUESTION',
            submit: 'OK',
            back: 'BACK'
        };
    }

    /**
     * @property {string} id - The unique id of the root element.
     * @readonly
     * @default 'change-secret-question-page'
     */
    get id () {
        return 'change-secret-question-page';
    }

    /**
     * @property {string} template - Id of template to render
     * @readonly
     * @default '#change-secret-question-template'
     */
    get template () {
        return '#change-secret-question-template';
    }

    /**
     * Navigates back to the activation view.
     */
    back () {
        this.sound.play('click-audio');
        this.navigate('toolbox');
    }

    /**
     * Renders the view.
     * @returns {Q.Promise<void>}
     */
    render () {
        let questions = LF.StudyDesign.securityQuestions;

        this.templateStrings.activatedElsewhere = 'SECRET_QUESTION_CHANGE_FAILED';

        // Loop through the questions and add them to the template strings to be translated.
        _(questions).each(question => {
            if (question.display) {
                this.templateStrings[question.text] = question.text;
            }
        });

        return this.buildHTML({ key: this.key }, true).then(translations => {
            // Append the question options to the select element.
            _(questions).each(question => {
                if (question.display) {
                    this.addQuestion(question.key, translations[question.text]);
                }
            });

            this.$secretQuestion.select2({
                minimumResultsForSearch: Infinity,
                width: '100%'
            });

            let subjectActive = (this.subject != null && this.subject.get('subject_active') !== 0);

            if (!subjectActive ||
                this.subject.get('phase') === LF.StudyDesign.terminationPhase ||
                this.subject.get('subject_active') === 2) {
                this.$form.hide();
                this.$inactiveMsg.removeClass('hidden');
            } else {
                this.$inactiveMsg.hide();
                this.$form.removeClass('hidden');
            }
        });
    }

    /**
     * Called upon submission after the secret question and answer is saved to local database.
     * @returns {Q.Promise<void>}
     * @example this.onSecretQuestionSaved();
     */
    onSecretQuestionSaved () {
        LF.security.pauseSessionTimeOut();

        return ELF.trigger('CHANGESECRETQUESTION:Transmit', { subject: this.subject }, this)
        .finally(() => {
            let user = LF.security.activeUser;

            Spinner.hide();

            logger.operational(`Security question/answer reset by user ${user.get('username')} with role ${user.get('role')}`);

            this.navigate('toolbox');

            LF.security.restartSessionTimeOut();
        });
    }

    /**
     * Submits the new secret question and answer to the web service.
     * @param {(MouseEvent|TouchEvent)} e - A mouse or touch event.
     */
    submit (e) {
        let input = this.$currentPassword,
            value = input.val(),
            pwd = this.user.get('password'),
            salt = this.user.get('salt');

        e.preventDefault();

        // Closes the virtual keyboard and scrolls to top of page
        input.blur();
        this.$secretAnswer.blur();
        window.scrollTo(0,0);

        if ((value.length === 0 || pwd != null) && (pwd !== hex_sha512(value + salt))) {
            this.showInputError(input, 'PASSWORD_INVALID');

            logger.error('PASSWORD_INVALID');
        } else {
            this.clearInputState(input);
            super.submit(e);
        }
    }
}
