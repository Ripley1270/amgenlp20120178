import PasswordBaseView from 'core/views/PasswordBaseView';
import ELF from 'core/ELF';
import Logger from 'core/Logger';
import templates from 'logpad/resources/Templates';
import CurrentSubject from 'core/classes/CurrentSubject';

let logger = new Logger('ChangePasswordView');

export default class ChangePasswordView extends PasswordBaseView {
    constructor (options) {
        super(options);

        /**
         * @property {Object<string,string>} selectors - A list of selectors to populate.
         */
        this.selectors = {
            inactiveMsg: '#inactiveMsg',
            form: '#changePasswordForm',
            newPassword: '#txtNewPassword',
            currentPassword: '#txtCurrentPassword',
            confirmPassword: '#txtConfirmPassword'
        };

        /**
         * @property {Object<string,(Object|string)>} templateStrings - Strings to fetch in the render function
         */
        this.templateStrings = {
            activatedElsewhere : 'DEACTIVATED_LABEL',
            header: 'APPLICATION_HEADER',
            title: 'CHANGE_PASSWORD',
            currentPassword: 'CURRENT_PASSWORD',
            newPassword: 'ENTER_NEW_PASSWORD',
            confirmPassword: 'CONFIRM_PASSWORD',
            submit: 'OK',
            back: 'BACK',
            submitIcon: {
                key: 'OK',
                namespace: 'ICONS'
            },
            pwdRules: 'PASSWORD_RULES'
        };
    }

    /**
     * @property {string} id - The unique id of the root element.
     * @readonly
     * @default 'change-password-page'
     */
    get id () {
        return 'change-password-page';
    }

    /**
     * @property {string} template - Id of template to render
     * @readonly
     * @default '#change-password-template'
     */
    get template () {
        return '#change-password-template';
    }

    /**
     * Navigates back to the toolbox view.
     */
    back () {
        this.sound.play('click-audio');
        this.navigate('toolbox');
    }

    /**
     * Renders the view.
     * @returns {Q.Promise<void>}
     */
    render () {
        return CurrentSubject.getSubject()
        .then(subject => {
            let subjectActive = (subject != null && subject.get('subject_active') !== 0),
                request = Q();

            if (!subjectActive) {
                this.templateStrings.activatedElsewhere = 'PASSWORD_CHANGE_FAILED';
            }

            if (LF.StudyDesign.showPasswordRules) {
                request = request.then(() => this.i18n('PASSWORD_RULES'))
                    .then(text => templates.display('DEFAULT:PasswordRules', { text }));
            }

            request.then((passwordRules = '') => {
                return this.buildHTML({
                    key: this.key,
                    passwordRules: passwordRules
                }, true)
                .then(() => {
                    if (!subjectActive ||
                        subject.get('phase') === LF.StudyDesign.terminationPhase ||
                        subject.get('subject_active') === 2) {
                        this.$form.hide();
                        this.$inactiveMsg.removeClass('hidden');
                    } else {
                        this.$inactiveMsg.hide();
                        this.$form.removeClass('hidden');
                    }
                });
            });

            return request;
        });
    }

    /**
     * Transmit all records to the web service once the password is saved locally.
     * @returns {Q.Promise<void>}
     * @example this.onPasswordSaved();
     */
    onPasswordSaved () {
        LF.security.pauseSessionTimeOut();

        return CurrentSubject.getSubject()
        .then(subject =>  ELF.trigger('CHANGEPASSWORD:Transmit', { subject }, this))
        .finally(() => {
            let user = this.user;

            logger.operational(`Password changed by user ${user.get('username')}`, {
                username: user.get('username'),
                role: user.get('role')
            }, 'ChangePass');

            this.navigate('toolbox');
            LF.security.restartSessionTimeOut();
        });
    }

    /**
     * Submits the new password to the web service.
     * @param {(MouseEvent|TouchEvent)} e - A mouse or touch event.
     */
    submit (e) {
        let value = this.$currentPassword.val(),
            pwd = this.user.get('password'),
            salt = this.user.get('salt'),
            hash = (pwd.length <= 32) ? hex_md5(value + salt) : hex_sha512(value + salt);

        e && e.preventDefault();

        // Closes the virtual keyboard and scrolls to top of page
        this.$currentPassword.blur();
        this.$newPassword.blur();
        this.$confirmPassword.blur();
        window.scrollTo(0,0);


        if ((value.length === 0 || pwd != null) && (pwd !== hash)) {
            this.showInputError(this.$currentPassword, 'PASSWORD_INVALID');
            // DE14884 - https://rally1.rallydev.com/#/36503486296ud/detail/defect/47244732931/discussion
            // Current password field retains wrong password on Change Password view.
            this.$currentPassword.val('');

            logger.error('PASSWORD_INVALID');
        } else {
            this.clearInputState(this.$currentPassword);
            super.submit(e);
        }
    }
}
