import PrivacyPolicyBaseView from 'logpad/views/PrivacyPolicyBaseView';

export default class PrivacyPolicyApplicationView extends PrivacyPolicyBaseView {
    constructor (options) {
        super(options);

        /**
         * @property {Object<string,string>} events - Privacy Policy Application view's event list.
         * @readonly
         */
        this.events = {
            
            // Click event for going to the settings view
            'click #back': 'back'
        };
    }

    /**
     * @property {string} id - The id of the root element.
     * @readonly
     * @default 'privacy-policy-application-page'
     */
    get id () {
        return 'privacy-policy-application-page';
    }

    /**
     * Navigate to the Settings view
     */
    back () {
        this.navigate('toolbox');
    }
}
