import PageView from 'core/views/PageView';
import CurrentContext from 'core/CurrentContext';
import { checkTimeZoneSet } from 'core/Helpers';

export default class HandoffView extends PageView {
    constructor (options) {
        super(options);

        /**
         * @property {Object<string,string>} events - The view's events.
         * @readonly
         */
        this.events = {

            // Click event for back button
            'click #back': 'back',

            // Click event for next button
            'click #next': 'next'
        };

        /**
         * @property {Object<string,string>} templateStrings - Strings to fetch in the render function
         */
        this.templateStrings = {
            header: 'APPLICATION_HEADER',
            handoffMessage: 'HANDOFF_MESSAGE',
            back: 'BACK',
            next: 'NEXT'
        };

        // TODO - Remove and adjust controller to use this.go();
        this.render();
    }

    /**
     * @property {string} id - The unique id of the root element.
     * @readonly
     * @default 'handoff-page'
     */
    get id () {
        return 'handoff-page';
    }

    /**
     * @property {string} template - Id of template to render
     * @readonly
     * @default '#handoff-template'
     */
    get template () {
        return '#handoff-template';
    }

    /**
     * Navigates back to the dashboard view.
     */
    back () {
        this.navigate('set_time_zone_activation', true);
    }

    /**
     * Navigates forward to privacy policy or reactivation
     */
    next () {
        LF.Data.activation = JSON.parse(localStorage.getItem('activationFlag'));
        CurrentContext().setContextLanguage(localStorage.getItem('preferredLanguageLocale'));
        if (LF.Data.activation) {
            this.navigate('privacy_policy_activation', true);
        } else {
            this.navigate('reactivation', true);
        }
    }

    /**
     * Renders the view.
     */
    render () {
        // TODO - should return promise.
        this.buildHTML({}, true)

        // fix for DE17475 and DE17420
        .then(checkTimeZoneSet)
        .then(() => this.delegateEvents());
    }
}
