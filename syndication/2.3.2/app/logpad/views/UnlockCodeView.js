import PageView from 'core/views/PageView';
import DailyUnlockCode from 'core/classes/DailyUnlockCode';

//- Core Modules
import * as helpers from 'core/Helpers';
import { MessageRepo } from 'core/Notify';
import Logger from 'core/Logger';
import Users from 'core/collections/Users';
import * as lStorage from 'core/lStorage';

let logger = new Logger('UnlockCodeView');

export default class UnlockCodeView extends PageView {

    constructor (options) {
        super(options);

        /**
         * @property {Object<string,string>} events - The view's events.
         * @readonly
         */
        this.events = {
            // Click event for back button
            'click #back': 'back',

            // Tap event for submit form
            'click #submit': 'validate',

            // Input event for unlock code
            'input #unlock_code': 'onInput'
        };

        /**
         * @property {Object<string,string>} templateStrings - Strings to fetch in the render function.
         */
        this.templateStrings = {
            header: 'APPLICATION_HEADER',
            title: 'CONTACT_STUDY_COORDINATOR',
            disabledSQText: 'PASSWORD_ATTEMPT_EXCEEDED',
            contactUnlock: 'CONTACT_UNLOCK_CODE',
            unlockCode: 'TEMPORARY_UNLOCK_CODE',
            submit: 'OK',
            currentDate: 'CURRENT_DATE',
            back: 'BACK'
        };
    }

    /**
     * @property {string} id - The unique id of the root element.
     * @readonly
     * @default 'unlock-code-page'
     */
    get id () {
        return 'unlock-code-page';
    }

    /**
     * @property {string} template - Id of template to render
     * @readonly
     * @default '#unlock-code-template'
     */
    get template () {
        return '#unlock-code-template';
    }

    /**
     * @property {Object<string,string>} selectors - A list of selectors to populate upon render.
     */
    get selectors () {
        return {
            input: '#unlock_code',
            submit: '#submit'
        };
    }

    /**
     * Resolve any dependencies required for the view to render.
     * @returns {Q.Promise<void>}
     */
    resolve () {
        let users = new Users();

        return users.fetch().then(() => {
            // If there is no User_Login value set, use and id of 0 for the activation process.
            let id = parseInt(lStorage.getItem('User_Login') || '0', 10),
                user = users.findWhere({ id });

            /**
             * @property {User} user - The target user.
             */
            this.user = user;
        });
    }

    /**
     * Navigates back to the ForgotPasswordView, LoginView, or ReactivationView
     * @return {Q.Promise<void>} resolved when complete
     */
    back () {
        this.sound.play('click-audio');
        localStorage.removeItem('Unlock_Code');

        let request = helpers.checkInstall();

        if (LF.StudyDesign.askSecurityQuestion) {
            return request.then(isInstalled => {
                if (isInstalled) {
                    this.navigate('forgot-password');
                } else {
                    this.navigate('forgot_password_activation');
                }
            });
        } else {
            return request.then(isInstalled => {
                this.navigate(isInstalled ? 'login' : 'reactivation');
            });
        }
    }

    /**
     * Renders the view.
     * @returns {Q.Promise<void>}
     */
    render () {
        if (LF.StudyDesign.askSecurityQuestion) {
            this.templateStrings.disabledSQText = 'ANSWER_ATTEMPT_EXCEEDED';
        }

        return this.buildHTML({
            key : this.key
        }, true).then(() => {
            // DE8275: Adding focus to prevent blue pointer from displaying erroneously in Android
            this.$submit.removeAttr('disabled')
                .focus()
                .attr('disabled', 'disabled');
        });
    }

    /**
     * Validates the unlock code.
     * @param {(MouseEvent|TouchEvent)} e - A mouse or touch event.
     * @returns {Q.Promise<void>}
     */
    validate (e) {
        let value = this.$input.val(),
            dailyUnlockCode = new DailyUnlockCode();

        e.preventDefault();
        // Closes the virtual keyboard and scrolls to top of page
        this.$input.blur();
        window.scrollTo(0,0);

        if (value === dailyUnlockCode.getCode(new Date())) {
            logger.operational('Correct unlock code entered');

            //Reset the failure counts
            LF.security.resetFailureCount(this.user);

            // Prevent the user from revisiting the unlock code page.
            localStorage.removeItem('Unlock_Code');

            // Allow the user to visit the reset password page.
            localStorage.setItem('Reset_Password', true);

            // Allow the user to visit the reset secret question page.
            if (LF.StudyDesign.askSecurityQuestion) {
                localStorage.setItem('Reset_Password_Secret_Question', true);
            }

            LF.Data.unlockCode = value;

            return helpers.checkInstall()
                .then(isInstalled => {
                    this.navigate(isInstalled ? 'reset_password' : 'reset_password_activation');
                });
        } else {
            this.inputError(this.$input);
            logger.error('INCORRECT_UNLOCK_CODE');
            logger.error('Incorrect unlock code entered');
            // disable the OK button
            this.disableButton(this.$submit);
            return MessageRepo.display(MessageRepo.Banner.INCORRECT_UNLOCK_CODE, { delay: 500 });
        }
    }

    /**
     * Enables the OK button on unlock_code input.
     */
    onInput () {
        let value = this.$input.val();

        if (value.length === 0) {
            // disable the OK button
            this.disableButton(this.$submit);
        } else {
            // enable the OK button
            this.enableButton(this.$submit);
        }
    }
}
