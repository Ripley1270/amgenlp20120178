import ListBaseView from 'logpad/views/ListBaseView';
import UpcomingQuestionnaire from 'logpad/views/UpcomingQuestionnaire';

export default class QuestionnairesList extends ListBaseView {
    constructor (options) {
        super(options);

        /**
         * Type of this list. (used in schedule targets)
         * @type string
         * @readonly
         * @default 'questionnaire'
         */
        this.objectType = 'questionnaire';

        this.options = options;

        // Bind refresh function to preserve scope when being called from listenTo.
        _.bindAll(this, 'refresh');

        this.render();

        return this;
    }

    /**
     * @property {string} id - The unique id of the root element.
     * @readonly
     * @default 'questionnairesList'
     */
    get id () {
        return 'questionnairesList';
    }

    /**
     * @property {string} tagName - The root element of the view.
     * @readonly
     * @default 'div'
     */
    get tagName () {
        return 'div';
    }

    /**
     * @property {Object<string,string>} attributes - Attributes for the QuestionnairesList view
     * @default { class: 'list-group' }
     */
    get attributes () {
        return { 'class': 'list-group' };
    }

    /**
     * Renders view then initializes jQuery listview
     */
    render () {
        super.render();
    }

    /**
     * Filters schedules to only the ones that matter to this view.
     * @param {Object} schedules - Collection of schedules to filter
     * @returns {Array} returns the list of the schedules of which target objectType is "questionnaire"
     */
    getSchedules (schedules) {
        return LF.schedule.getDiarySchedules(schedules);
    }

    /**
     * Creates a new UpcomingQuestionnaire.
     * @param {Questionnaire} questionnaire - questionnaire model used to create new subview
     * @param {string} scheduleId - scheduleId of the schedule attached to the questionnaire
     * @param {Function} callback - A callback function invoked after appending new subview passing the newly created view and studyRule string.
     */
    createSubview (questionnaire, scheduleId, callback) {
        let studyRule = `DASHBOARD:AddedToDashboard/${questionnaire.get('id')}`;

        new UpcomingQuestionnaire({
            id         : questionnaire.get('id'),
            model      : questionnaire,
            parent     : LF.router.view(),
            scheduleId : scheduleId,
            callback   : function (view) {
                callback(view, studyRule);
            }
        });
    }

    /**
     * Refresh JQuery listview
     */
    refresh () {
        // TODO - This seems to be targeted for jquery-mobile. Do we still need this method?
        let list = $(`#${this.id}`);

        if (list.hasClass('ui-listview')) {
            list.listview('refresh');
        }

        // Deleting reference from closure to prevent memory leaks.
        list = null;
    }
}

window.LF.View.QuestionnairesList = QuestionnairesList;
