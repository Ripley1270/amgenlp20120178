import BaseController from 'core/controllers/BaseController';

import InstallView from 'logpad/views/InstallView';
import CodeEntryView from 'logpad/views/CodeEntryView';
import ActivationView from 'logpad/views/ActivationView';
import ReactivationView from 'logpad/views/ReactivationView';
import PrivacyPolicyActivationView from 'logpad/views/PrivacyPolicyActivationView';
import ForgotPasswordActivationView from 'logpad/views/ForgotPasswordActivationView';
import SecretQuestionView from 'logpad/views/SecretQuestionView';
import ResetSecretQuestionActivationView from 'logpad/views/ResetSecretQuestionActivationView';
import ResetPasswordActivationView from 'logpad/views/ResetPasswordActivationView';
import UnlockCodeView from 'logpad/views/UnlockCodeView';
import HandoffView from 'logpad/views/HandoffView';
import SetTimeZoneActivationView from 'core/views/SetTimeZoneActivationView';
import { checkInstall, checkTimeZoneSet } from 'core/Helpers';
import Data from 'core/Data';
import Logger from 'core/Logger';
import COOL from 'core/COOL';

let logger = new Logger('RegistrationController');

/**
 * Controller for LogPad App registration workflow.
 * @class RegistrationController
 * @extends BaseController
 */
export default class RegistrationController extends BaseController {
    constructor (options) {
        logger.traceSection('RegistrationController');
        logger.traceEnter('constructor');
        super(options);
        logger.traceExit('constructor');
    }

    /**
     * Displays the installation page.
     * @example /activation/#install
     */
    install () {
        logger.traceEnter('install');

        this.clear();
        Data.Subject = JSON.parse(localStorage.getItem('PHT_TEMP') || false) || false;

        if (!Data.code || !Data.Subject) {
            this.navigate('code_entry', true);
        } else {
            // TODO: Investigate if authenticateThenGo could be used instead.
            // All functions in this file should be refactored accordingly
            // If the local database is installed, navigate to the login page; Otherwise, render the page.
            checkInstall()
                .then((isInstalled) => {
                    if (isInstalled) {
                        this.navigate('login');
                        return Q();
                    } else {
                        return this.go('InstallView', InstallView);
                    }
                })
                .catch(e => logger.error(e))
                .done();
        }

        logger.traceExit('install');
    }

    /**
     * Displays the set time zone activation view
     * @example /activation/#set_time_zone_activation
     */
    setTimeZone () {
        logger.traceEnter('setTimeZone');

        checkInstall().then(isInstalled => {
            if (isInstalled) {
                this.navigate('#login');
            } else {
                this.go('SetTimeZoneActivationView', SetTimeZoneActivationView)
                    .catch(e => logger.error(e))
                    .done();
            }
        });

        logger.traceExit('setTimeZone');
    }

    /**
     * The default application route. Displays the code entry view.
     * @example /activation/#code_entry
     */
    codeEntry () {
        logger.traceEnter('codeEntry');

        // when backing from activation or reactivation page, clear url_setup_code
        if (LF.router && LF.router.view()) {
            localStorage.removeItem('url_setup_code');
        }

        Q.all([checkInstall(), checkTimeZoneSet()])
        .spread((isInstalled, timeZoneSet) => {
            if (isInstalled) {
                this.navigate('#login');
            } else if (timeZoneSet) {
                this.navigate('handoff');
            } else {
                this.go('CodeEntryView', CodeEntryView)
                .catch(e => logger.error(e))
                .done();
            }
        });

        logger.traceExit('codeEntry');
    }

    /**
     * The route for a new activation. Requires new password entry.
     * @example /activation/#activation
     */
    activation () {
        logger.traceEnter('activation');

        if (Data.code) {
            this.go('ActivationView', ActivationView)
                .catch(e => logger.error(e))
                .done();
        } else {
            checkInstall()
                .then((isInstalled) => {
                    if (isInstalled) {
                        this.navigate('login');
                    } else {
                        this.navigate('code_entry');
                    }
                });
        }

        logger.traceExit('activation');
    }

    /**
     * The route for a repeat activation. Requires entry of the old password.
     * @example /activation/#reactivation
     */
    reactivation () {
        logger.traceEnter('reactivation');

        if (Data.code) {
            this.go('ReactivationView', ReactivationView)
                .catch(e => logger.error(e))
                .done();
        } else {
            checkInstall()
                .then((isInstalled) => {
                    if (isInstalled) {
                        this.navigate('login');
                    } else {
                        this.navigate('code_entry');
                    }
                });
        }

        logger.traceExit('reactivation');
    }

    /**
     * Displays the unlock code view.
     * @example /activation/#unlock_code_activation
     */
    unlockCode () {
        logger.traceEnter('unlockCode');

        if (Data.code) {
            this.go('UnlockCodeView', UnlockCodeView)
                .catch(e => logger.error(e))
                .done();
        } else {
            this.navigate('code_entry');
        }

        logger.traceExit('unlockCode');
    }

    /**
     * The route for a Privacy Policy.
     * @example /activation/#privacy_policy
     **/
    privacyPolicy () {
        logger.traceEnter('privacyPolicy');

        if (Data.code) {
            this.go('PrivacyPolicyActivationView', PrivacyPolicyActivationView)
                .catch(e => logger.error(e))
                .done();
        } else {
            this.navigate('code_entry');
        }

        logger.traceExit('privacyPolicy');
    }

    /**
     * The route for a Handoff View .
     * @example /activation/#handoff
     **/
    handoff () {
        logger.traceEnter('handoff');

        this.clear();
        Data.code = Data.code || localStorage.getItem('activationCode');
        if (!Data.code) {
            this.navigate('code_entry', true);
        } else {
            this.view = COOL.new('HandoffView', HandoffView);
        }

        logger.traceExit('handoff');
    }

    /**
     * Display the secret question view.
     * @example /activation/#secret_question
     */
    secretQuestion () {
        logger.traceEnter('secretQuestion');

        this.clear();

        if (!Data.code || !Data.password) {
            this.navigate('code_entry');
        } else {
            this.go('SecretQuestionView', SecretQuestionView)
                .catch(e => logger.error(e))
                .done();
        }

        logger.traceExit('secretQuestion');
    }

    /**
     * Displays the forgot password activation view
     * @example /activation/#forgot-password
     **/
    forgotPassword () {
        logger.traceEnter('forgotPassword');

        if (!Data.code) {
            this.navigate('code_entry');
        } else if (!Data.question) {
            this.navigate('reactivation');
        } else {
            this.go('ForgotPasswordActivationView', ForgotPasswordActivationView)
                .catch(e => logger.error(e))
                .done();
        }

        logger.traceExit('forgotPassword');
    }

    /**
     * Display the reset secret question activation view.
     * @example /#reset_secret_question
     */
    resetSecretQuestion () {
        logger.traceEnter('resetSecretQuestion');

        if (Data.code) {
            this.go('ResetSecretQuestionActivationView', ResetSecretQuestionActivationView)
                .catch(e => logger.error(e))
                .done();
        } else {
            this.navigate('code_entry');
        }

        logger.traceExit('resetSecretQuestion');
    }

    /**
     * Display the reset password activation view.
     * @example /activation/#reset_password
     */
    resetPassword () {
        logger.traceEnter('resetPassword');

        if (Data.code) {
            this.go('ResetPasswordActivationView', ResetPasswordActivationView)
                .catch(e => logger.error(e))
                .done();
        } else {
            this.navigate('code_entry');
        }

        logger.traceExit('resetPassword');
    }
}
