import BaseController from 'core/controllers/BaseController';
import COOL from 'core/COOL';
import AboutView from '../views/AboutView';
import ChangePasswordView from '../views/ChangePasswordView';
import ChangeTemporaryPasswordView from '../views/ChangeTemporaryPasswordView';
import PrivacyPolicyApplicationView from '../views/PrivacyPolicyApplicationView';
import ChangeSecretQuestionView from '../views/ChangeSecretQuestionView';
import ToolboxView from '../views/ToolboxView';
import SetAlarmsView from '../views/SetAlarmsView';
import SetTimeZoneView from 'core/views/SetTimeZoneView';
import SupportView from '../views/SupportView';
import Logger from 'core/Logger';
import CurrentSubject from 'core/classes/CurrentSubject';

let logger = new Logger('SettingsController');

/**
 * Controller for LogPad App settings workflow.
 * @class RegistrationController
 * @extends BaseController
 */
export default class SettingsController extends BaseController {

    /**
     * Display the About Screen.
     * @example /application/#about
     */
    about () {
        // TODO - Update to use authenticateThenGo
        LF.security.checkLogin()
            // eslint-disable-next-line consistent-return
            .then(authenticated => {
                if (authenticated) {
                    return this.go('AboutView', AboutView);
                } else {
                    LF.security.logout(true);
                }
            })
            .catch(e => logger.error(e))
            .done();
    }

    /**
     * Displays the change password view.
     * @example /application/#change_password
     */
    changePassword () {
        // TODO - Update to use authenticateThenGo
        LF.security.checkLogin()
            // eslint-disable-next-line consistent-return
            .then(authenticated => {
                if (authenticated) {
                    return this.go('ChangePasswordView', ChangePasswordView);
                } else {
                    LF.security.logout(true);
                }
            })
            .catch(e => logger.error(e))
            .done();
    }

     /**
     * Displays the change password view.
     * @example /application/#change_password
     */
    changeTemporaryPassword () {
        this.clear();

        // TODO - Update to use authenticateThenGo
        LF.security.checkLogin()
            // eslint-disable-next-line consistent-return
            .then(authenticated => {
                if (authenticated) {
                    return this.go('ChangeTemporaryPasswordView', ChangeTemporaryPasswordView);
                } else {
                    LF.security.logout(true);
                }
            })
            .catch(e => logger.error(e))
            .done();
    }

    /**
     * The route for a Privacy Policy.
     * @example /application/#privacy_policy
     **/
    privacyPolicy () {
        // TODO - Update to use authenticateThenGo
        LF.security.checkLogin()
            // eslint-disable-next-line consistent-return
            .then(authenticated => {
                if (authenticated) {
                    return this.go('PrivacyPolicyApplicationView', PrivacyPolicyApplicationView);
                } else {
                    LF.security.logout(true);
                }
            })
            .catch(e => logger.error(e))
            .done();
    }

    /**
     * Display the change secret question view.
     * @example /application/#change_secret_question
     */
    changeSecretQuestion () {
        // TODO - Update to use authenticateThenGo
        LF.security.checkLogin()
            // eslint-disable-next-line consistent-return
            .then(authenticated => {
                if (authenticated) {
                    return CurrentSubject.getSubject()
                    .then(subject => this.go('ChangeSecretQuestionView', ChangeSecretQuestionView, { subject }));
                } else {
                    LF.security.logout(true);
                }
            })
            .catch(e => logger.error(e))
            .done();
    }

    /**
     * Display the reset secret question view.
     * @example /application/#reset_secret_question
     */
    resetSecretQuestion () {
        this.clear();

        if (localStorage.getItem('Reset_Password_Secret_Question')) {
            CurrentSubject.getSubject()
            .then(subject => this.go('ResetSecretQuestionView', ResetSecretQuestionView, { subject }))
            .catch(e => logger.error(e))
            .done();
        } else {
            this.navigate('login');
        }
    }

    /**
     * Display the ToolBox Screen.
     * @example /application/#Toolbox
     */
    toolbox () {
        this.authenticateThenGo('ToolboxView', ToolboxView)
            .catch(e => logger.error(e))
            .done();
    }

    /**
     * Display the customer support view.
     * @example /application/#support
     */
    support () {
        this.clear();

         this.authenticateThenGo('SupportView', SupportView)
            .catch(e => logger.error(e))
            .done();
    }


    /**
     * Display the set alarms view.
     * @example /application/#set_alarms
     */
    setAlarms () {
        this.authenticateThenGo('SetAlarmsView', SetAlarmsView)
            .catch(e => logger.error(e))
            .done();
    }

    /**
     * Displays the set time zone view
     * @example /application/#set_time_zone
     */
    setTimeZone () {
        this.authenticateThenGo('SetTimeZoneView', SetTimeZoneView)
        .catch(e => logger.error(e))
        .done();
    }
}

COOL.add('SettingsController', SettingsController);