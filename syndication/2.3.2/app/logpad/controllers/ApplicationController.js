import LoginView from 'logpad/views/LoginView';
import DashboardView from 'logpad/views/DashboardView';
import UnlockCodeView from 'logpad/views/UnlockCodeView';
import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';
import QuestionnaireView from 'core/views/QuestionnaireView';
import QuestionnaireCompletionView from 'core/views/QuestionnaireCompletionView';
import BlankView from 'core/views/BlankView';
import BaseController from 'core/controllers/BaseController';
import * as lStorage from 'core/lStorage';
import ForgotPasswordView from 'core/views/ForgotPasswordView';
import ResetPasswordView from 'core/views/ResetPasswordView';
import UniversalLogin from 'core/classes/UniversalLogin';
import Data from 'core/Data';
import Logger from 'core/Logger';
import { checkInstall, checkTimeZoneSet } from 'core/Helpers';
import COOL from 'core/COOL';
import CurrentSubject from 'core/classes/CurrentSubject';

let logger = new Logger('ApplicationController');

/**
 * Controller for LogPad App application workflow.
 * @class ApplicationController
 * @extends BaseController
 */
export default class ApplicationController extends BaseController {
    constructor (options) {
        logger.traceSection('ApplicationController');
        logger.traceEnter('constructor');
        super(options);
        logger.traceExit('constructor');
    }

    /**
     * Displays the login view.
     * @example /application/#login
     */
    login () {
        let universalLogin,
            successfulLogin = () => {
                this.navigate('dashboard');
            };

        checkInstall()
            // eslint-disable-next-line consistent-return
            .then((isInstalled) => {
                if (isInstalled) {
                    // DE16561:  Clean up the previous view before showing login.
                    this.clear();

                    return checkTimeZoneSet()
                    .then(() => CurrentSubject.getSubject())
                    .then(subject => {
                        universalLogin = new UniversalLogin({
                            loginView: LoginView,

                            // To fix the hard dependency on login view in scheduling
                            appController: this,
                            subject
                        });

                        universalLogin.newUserLogin(successfulLogin);
                    });
                } else {
                    this.navigate('code_entry', true);
                }
            })
            .done();
    }

    /**
     * Displays the dashboard view.
     * @example /application/#dashboard
     */
    dashboard () {
        logger.traceEnter('dashboard');

        // fix for DE16473 - removing this flag after user has reset their SQ and A
        // rather than doing it on core/ResetSecretQuestionView which would cause SitePad issues
        localStorage.removeItem('Reset_Password_Secret_Question');

        this.authenticateThenGo('DashboardView', DashboardView)
            .catch(e => logger.error(e))
            .done();

        logger.traceExit('dashboard');
    }

    /**
     * Display the forgot password view.
     * @example /application/#forgot_password
     */
    forgotPassword () {
        logger.traceEnter('forgotPassword');

        if (localStorage.getItem('Forgot_Password') && lStorage.getItem('User_Login')) {
            this.go('ForgotPasswordView', ForgotPasswordView)
                .catch(e => logger.error(e))
                .done();
        } else {
            this.navigate('login');
        }

        logger.traceExit('forgotPassword');
    }

    /**
     * Display the Unlock Code view.
     * @example /application/#unlock_code
     */
    unlockCode  () {
        logger.traceEnter('unlockCode');

        if (localStorage.getItem('Unlock_Code')) {
            this.go('UnlockCodeView', UnlockCodeView)
                .catch(e => logger.error(e))
                .done();
        } else {
            this.navigate('login');
        }

        logger.traceExit('unlockCode');
    }

    /**
     * Display the reset password view.
     * @example /application/#reset_password
     */
    resetPassword () {
        logger.traceEnter('resetPassword');

        if (localStorage.getItem('Reset_Password')) {
            this.go('ResetPasswordView', ResetPasswordView)
                .catch(e => logger.error(e))
                .done();
        } else {
            this.navigate('login');
        }

        logger.traceExit('resetPassword');
    }

    /**
     * Display the questionnaire completion view.
     * @example /application/#questionnaire-completion
     */
    questionnaireCompletion () {
        logger.traceEnter('questionnaireCompletion');
        this.clear();

        if (!Data.Questionnaire) {
            this.navigate('dashboard', true);
        } else {
            this.view = new QuestionnaireCompletionView();
            this.view.render();
        }

        logger.traceExit('questionnaireCompletion');
    }

    /**
     * Displays the questionnaire view.
     * @param {string} questionnaire - The id of the questionnaire to load.
     * @param {string} scheduleId - unique schedule configuration id for this questionnaire.
     * @returns {Q.Promise<void>}
     * @example /application/#questionnaire/:questionnaire_id/:schedule_id
     */
    questionnaire (questionnaire, scheduleId) {
        this.clear();

        // TODO: I don't think this should return a promise.
        return CurrentSubject.getSubject()
        .then((subject) => {
            // If the Questionnaire namespace doesn't exist, navigate back to the dashboard.
            if (!Data.Questionnaire) {
                this.navigate('dashboard', true);
                return;
            }
            let params = {
                id: questionnaire,
                schedule_id: scheduleId,
                subject
            };

            // Stakutis; see if a class name is specified in the study design
            let className = _.find(LF.StudyDesign.questionnaires.models, { id: questionnaire })
                .attributes.syndicationClass || 'QuestionnaireView';

            this.view = COOL.new(className, QuestionnaireView, params);
            Data.Questionnaire = this.view;

            // eslint-disable-next-line consistent-return
            return this.view.resolve()
            // TODO: This all should be a part of the resolve/render workflow.
            .then(() => this.view.render())
            .catch((e) => {
                // log since backbone won't handle it if backbone called this method.
                logger.error('unexpected exception in ApplicationController.questionnaire():', e);
                throw(e);
            });
        });
    }

    /**
     * Display the Time_Confirmation questionnaire.
     */
    timeConfirmation () {
        this.go('BaseQuestionnaireView', BaseQuestionnaireView, {
            id: 'Time_Confirmation'
        })
        .catch(e => logger.error(e))
        .done();
    }

    /**
     * Display the Blank page.
     */
    blank () {
        this.go('BlankView', BlankView)
        .catch(e => logger.error(e))
        .done();
    }

    /**
     * Removes the current view.
     * example: this.clear();
     */
    clear () {
        logger.traceEnter('clear');

        if (this.view) {
            if (this.view.id === 'dashboard-page') {
                LF.schedule.stopTimer();
            }

            if (this.view.close) {
                this.view.close();
            } else if (this.view.remove) {
                this.view.remove();
            }

            this.view = null;
        }

        logger.traceExit('clear');
    }
}
