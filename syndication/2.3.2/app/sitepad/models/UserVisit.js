import StorageBase from 'core/models/StorageBase';

/**
 * UserVisit model that defines user visit(s) state.
 * @augments StorageBase
 */
export default class UserVisit extends StorageBase {
    constructor (options) {
        super(options);

        /**
         * The model's name
         * @readonly
         * @type String
         * @default 'UserVisit'
         */
        this.name = 'UserVisit';
    }

    /**
     * The model's schema used for validation and storage construction.
     * @readonly
     * @type {Object}
     */
    static get schema () {
        return {
            id: {
                type: Number
            },
            visitId: {
                type: String,
                required: true
            },
            studyEventId: {
                type: String
            },
            subjectKrpt: {
                type: String,
                required: true
            },
            state: {
                type: String,
                required: true
            },
            dateModified: {
                type: String
            },
            dateStarted: {
                type: String
            },
            dateEnded: {
                type: String
            },
            expired: {
                type: String,
                required: false
            }
        };
    }
}
