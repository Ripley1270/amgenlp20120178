/**
 * A model to represent a Form row on the FormTableView.
 * @class {FormRow<T>}
 * @extends {Backbone.Model<T>}
 */
export default class FormRow extends Backbone.Model { }
