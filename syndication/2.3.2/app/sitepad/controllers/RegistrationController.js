import State from 'core/classes/State';
import BaseController from 'core/controllers/BaseController';
import Logger from 'core/Logger';
import * as lStorage from 'core/lStorage';
import { checkTimeZoneSet } from 'core/Helpers';
import ELF from 'core/ELF';

import PageView from 'core/views/PageView';
import ModeSelectionView from '../views/ModeSelectionView';
import SiteSelectionView from '../views/SiteSelectionView';
import UnlockCodeView from '../views/UnlockCodeView';
import SetTimeZoneActivationView from '../views/SetTimeZoneActivationView';
import LanguageSelectionView from '../views/LanguageSelectionView';
import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';
import QuestionnaireCompletionView from 'core/views/QuestionnaireCompletionView';

let logger = new Logger('RegistrationController');

export default class RegistrationController extends BaseController {
    constructor (options) {
        super(options);

        /**
         * @property {State} state - An instance of the State class.
         */
        this.state = new State();

        let stateValue = parseInt(lStorage.getItem('state'), 10);
        if (stateValue >= 2 && stateValue !== 6) {
            this.state.set('LANGUAGE_SELECTION');
        }
    }

    /**
     * Route based on state of application.
     */
    route () {
        let state = this.state.get();

        switch (state) {
            case 'NEW':
                this.modeSelection();
                break;
            case 'SITE_SELECTION':
                this.siteSelection();
                break;
            case 'LOCKED':
                this.unlockCode();
                break;
            case 'SET_TIME_ZONE':
                this.setTimeZone();
                break;
            case 'LANGUAGE_SELECTION':
                this.languageSelection();
                break;
            case 'SETUP_USER':
                this.setupUser();
                break;
            case 'ACTIVATED':
            default:
                this.navigate('login');
                break;
        }
    }

    /**
     * Display the ModeSelectionView.
     */
    modeSelection () {
        this.go('ModeSelectionView', ModeSelectionView)
        .catch(e => logger.error(e))
        .done();
    }

    /**
     * Display the first user creation questionnaire.
     */
    setupUser () {
        if (this.state.get() === 'SETUP_USER') {
            this.go('BaseQuestionnaireView', BaseQuestionnaireView, {
                id: 'First_Site_User'
            })
            .catch(e => logger.error(e))
            .done();
        } else {
            this.navigate('language-selection');
        }

    }

    /**
     * Display the LanguageSelectionView.
     */
    languageSelection () {
        checkTimeZoneSet()
        .then((result) => {
            if (result) {
                return ELF.trigger('SETTIMEZONE:Completed', {}, new PageView());
            } else {
                return this.go('LanguageSelectionView', LanguageSelectionView)
                .catch(e => logger.error(e))
                .done();
            }
        })
        .done();
    }

    /**
     * Display the SiteSelectionView.
     */
    siteSelection () {
        this.go('SiteSelectionView', SiteSelectionView)
        .catch(e => logger.error(e))
        .done();
    }

    /**
     * Display the UnlockCodeView.
     */
    unlockCode () {
        if (this.state.get() === 'LOCKED') {
            this.go('UnlockCodeView', UnlockCodeView)
            .catch(e => logger.error(e))
            .done();
        } else {
            this.navigate('language-selection');
        }
    }

    /**
     * Display the SetTimeZoneActivationView.
     */
    setTimeZone () {
        this.go('SetTimeZoneActivationView', SetTimeZoneActivationView)
        .catch(e => logger.error(e))
        .done();
    }

    /**
     * Display the QuestionnaireCompletionView.
     */
    questionnaireCompletion () {
        this.go('QuestionnaireCompletionView', QuestionnaireCompletionView)
        .catch(e => logger.error(e))
        .done();
    }
}
