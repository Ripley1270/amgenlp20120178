
import BaseController from 'core/controllers/BaseController';
import Sites from 'core/collections/Sites';
import Transmissions from 'core/collections/Transmissions';
import Data from 'core/Data';

export default class SitepadController extends BaseController {
    bootstrap () {
        let setTransmissions,
            setSite;

        setSite = () => {
            // If the site is already in scope...
            if (Data.site) return;

            // eslint-disable-next-line consistent-return
            return Sites.fetchFirstEntry()
            .then(site => {
                Data.site = site;

                return site;
            });
        };

        setTransmissions = () => {
            return Transmissions.fetchCollection(transmissions => {
                Data.transmissions = transmissions;
                return transmissions;
            });
        };

        return Q.all([setSite(), setTransmissions()]);

    }

}
