import SitepadController from './SitepadController';

import SettingsView from '../views/SettingsView';
import AboutView from '../views/AboutView.js';
import CustomerSupportView from '../views/CustomerSupportView';
import SetTimeZoneView from '../views/SetTimeZoneView';

import Logger from 'core/Logger';

let logger = new Logger('SettingsController');

export default class SettingsController extends SitepadController {
    /**
     * Navigate to the settings view.
     */
    index () {
        this.authenticateThenGo('SettingsView', SettingsView)
        .catch(e => logger.error(e))
        .done();
    }

    /**
     * Navigate to the about view.
     */
    about () {
        this.authenticateThenGo('AboutView', AboutView)
        .catch(e => logger.error(e))
        .done();
    }

    /**
     * Navigate to the SetTimeZone view.
     */
    timeZoneSettings () {
        this.authenticateThenGo('SetTimeZoneView', SetTimeZoneView)
            .catch(e => logger.error(e))
            .done();
    }

     /**
     * Navigate to the customer support view.
     */
    customerSupport () {
        this.authenticateThenGo('CustomerSupportView', CustomerSupportView)
        .catch(e => logger.error(e))
        .done();
    }
}
