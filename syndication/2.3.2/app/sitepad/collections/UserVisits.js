import StorageBase from 'core/collections/StorageBase';
import UserVisit from 'sitepad/models/UserVisit';

/**
 * A collection representing User Visits.
 * @extends StorageBase
 */
export default class UserVisits extends StorageBase {

    /**
     * @property {UserVisit} model - The model class that the collection contains.
     */
    get model () {
        return UserVisit;
    }
}

window.LF.Collection.UserVisits = UserVisits;
