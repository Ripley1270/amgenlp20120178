import StorageBase from 'core/collections/StorageBase';
import UserReport from 'sitepad/models/UserReport';

/**
 * A collection representing User Reports.
 * @class {UserReports<T>}
 * @extends {StorageBase<T>}
 */
export default class UserReports extends StorageBase {
    /**
     * @property {UserReport} model - The model that belongs to the collection.
     */
    get model () {
        return UserReport;
    }
}

window.LF.Collection.UserReports = UserReports;
