import ELF from 'core/ELF';
import * as utils from 'core/utilities';

/**
 * Determines if active visit matches the specified visit.
 * @param {string} visitId - Visit id.
 * @returns {Q.Promise<boolean>} - True if active visit matches the specified visit, false if not.
 */
export function isVisit (visitId) {
    let result = !!utils.getNested(this, 'questionnaire.visit.id') && (this.questionnaire.visit.id === visitId);
    return Q(result);
}

ELF.expression('isVisit', isVisit);
