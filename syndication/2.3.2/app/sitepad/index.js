// Import all Core and SitePad actions.
import 'core/actions';
import 'sitepad/actions';

// Import all Core and SitePad expressions.
import 'core/expressions';
import 'sitepad/expressions';

import COOL from 'core/COOL';
// @todo most of these are imported due to requirments of the study design.
import 'core/collections/Languages';
import 'core/collections/Templates';
import 'core/collections/StoredSchedules';
import 'core/collections/Logs';
import 'core/collections/SubjectAlarms';

import 'core/branching/branchingHelpers';
import 'sitepad/resources/Templates';
import 'sitepad/resources/Rules';
import 'sitepad/visits';

// Import SitePad Widgets
import 'sitepad/widgets/ListBase';
import 'sitepad/widgets/SkipReason';
import 'sitepad/widgets/EditReason';

import 'core/widgets/LanguageSelectWidget';
import 'core/widgets/RoleSelectWidget';
import 'core/widgets/PasswordMessageWidget';

// Import all other study assets...
import 'trial/';
import '../trainer/sitepad';

import Startup from 'sitepad/classes/Startup';

$('select').click(() => {
    this.blur();
    this.focus();
});

COOL.add('Startup', Startup);

COOL.new('Startup')
.startup();
