import Data from 'core/Data';
import ELF from 'core/ELF';

/**
 * Opens a questionnaire.
 * @param {Object} params - Parameters.
 * @return {Q.Promise<void>}
 */
export function openQuestionnaireSitePad (params) {

    let id = params.questionnaire_id,
        subject = params.subject,
        visit = params.visit;

    Data.Questionnaire = {};

    //DE12483: Delay opening questionnaire to prevent the issue of questionnaire screen display
    return Q.delay(150)
        .then(() => this.navigate(`questionnaire/${id}`, true, {
            subject,
            visit
        }));
}

ELF.action('openQuestionnaireSitePad', openQuestionnaireSitePad);
