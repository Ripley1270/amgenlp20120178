import Logger from 'core/Logger';
import ELF from 'core/ELF';
import Subjects from 'core/collections/Subjects';
import UserVisits from 'sitepad/collections/UserVisits';
import * as DateTimeUtil from 'core/DateTimeUtil';

const logger = new Logger('actions/saveUserVisitsAndReports');

/**
 * Save User Visits and Reports completed on other devices than current.
 * @param  {Object} res - procedure call results object
 * @param  {Object} collection - collection to be used from syncHistoricalData
 * @return {Q.Promise} resolves with the updated collection when all user report data is saved.
 */
export function saveUserVisitsAndReports ({ res, collection }) {
    let handleVisitStartSync,
        handleVisitEndSync,
        getVisitState,
        userVisits = new UserVisits();

    getVisitState = (statusCode) => {
        const visitStatus = LF.StudyDesign.visitStatus[statusCode];
        return LF.VisitStates[visitStatus || 'COMPLETED'];
    };

    handleVisitStartSync = (userVisits, visitModel, form, subjectId, subjectKrpt, studyEventId) => {
        let userVisit = userVisits.findWhere({
                subjectKrpt,
                visitStartSigId: form.sigOrig
            }),
            formDeleted = form.deleted && form.deleted !== '0';

        if (!formDeleted && !userVisit) {
            userVisit = userVisits.findWhere({
                subjectKrpt,
                visitId: visitModel.get('id'),
                studyEventId
            });
        }

        if (!formDeleted) {
            if (!userVisit) {
                // eslint-disable-next-line new-cap
                userVisit = new userVisits.model();

                userVisit.set({
                    dateModified: DateTimeUtil.timeStamp(new Date()),
                    state: LF.VisitStates.IN_PROGRESS,
                    visitId: visitModel.get('id'),
                    studyEventId,
                    subjectKrpt,
                    dateStarted: form.startDate,
                    visitStartSigId: form.sigOrig
                });
                logger.info(`Visit Start form received. Creating new user visit record for patient: ${subjectId} and visit: ${visitModel.get('id')} with sigorig: ${form.sigOrig}`);
                userVisits.add(userVisit, { merge: true });
            } else if (userVisit.get('studyEventId') !== studyEventId || userVisit.get('dateStarted') !== form.startDate) {
                logger.info(`Visit Start form received. Updating the existing user visit record for patient: ${subjectId} and visit: ${visitModel.get('id')} with sigorig: ${form.sigOrig}`);
                userVisit.set({
                    visitId: visitModel.get('id'),
                    studyEventId,
                    dateStarted: form.startDate
                });
            }
        } else if (!!userVisit) {
            logger.info(`Visit Start form deleted. Deleting user visit record for patient: ${subjectId} and visit: ${visitModel.get('id')} with sigorig: ${form.sigOrig}`);
            userVisit.destroy();
        }
    };

    handleVisitEndSync = (userVisits, visitModel, form, subjectId, subjectKrpt, studyEventId) => {
        let userVisit = userVisits.findWhere({
                subjectKrpt,
                visitEndSigId: form.sigOrig
            }),
            formDeleted = form.deleted && form.deleted !== '0';

        if (!formDeleted && !userVisit) {
            userVisit = userVisits.findWhere({
                subjectKrpt,
                visitId: visitModel.get('id'),
                studyEventId
            });
        }

        if (!formDeleted) {
            if (!userVisit) {
                // eslint-disable-next-line new-cap
                userVisit = new userVisits.model();

                userVisit.set({
                    dateModified: DateTimeUtil.timeStamp(new Date(form.endDate)),
                    state: getVisitState(form.status),
                    visitId: visitModel.get('id'),
                    studyEventId,
                    subjectKrpt,
                    dateEnded: form.endDate,
                    visitEndSigId: form.sigOrig
                });
                logger.info(`Visit End form received. Creating new user visit record for patient: ${subjectId} and visit: ${visitModel.get('id')} with sigorig: ${form.sigOrig}`);
                userVisits.add(userVisit, { merge: true });
            } else if (userVisit.get('studyEventId') !== studyEventId || getVisitState(form.status) !== userVisit.get('state') ||
                userVisit.get('dateEnded') !== form.endDate || !userVisit.get('visitEndSigId')) {
                logger.info(`Visit End form received. Updating the existing user visit record for patient: ${subjectId} and visit: ${visitModel.get('id')} with sigorig: ${form.sigOrig}`);
                userVisit.set({
                    dateModified: DateTimeUtil.timeStamp(new Date(form.endDate)),
                    state: getVisitState(form.status),
                    visitId: visitModel.get('id'),
                    studyEventId,
                    dateEnded: form.endDate,
                    visitEndSigId: form.sigOrig
                });
            }
        } else if (!!userVisit) {
            logger.info(`Visit End form deleted. Updating the existing user visit record for patient: ${subjectId} and visit: ${visitModel.get('id')} with sigorig: ${form.sigOrig}.
            Visit state updated to IN_PROGRESS`);
            userVisit.set({
                dateModified: DateTimeUtil.timeStamp(new Date()),
                state: LF.VisitStates.IN_PROGRESS
            });
        }
    };

    return Q.all([userVisits.fetch(), Subjects.fetchCollection()])
    .spread((visits, subjects) => {
        logger.trace('Started handling user visits and reports records.');
        res.subjects.forEach(subject => {
            if (!subject.visit) {
                return;
            }
            subject.visit.forEach(visit => {
                if (!visit.report) {
                    return;
                }
                visit.report.forEach(form => {
                    const visitModel = LF.StudyDesign.visits.findWhere({
                        studyEventId: visit.eventId
                    });

                    if (!visitModel) {
                        logger.info(`Unknown visit for record with SU: ${form.krsu} and sigorig: ${form.sigOrig}`);
                        return;
                    }

                    let subjectId = subjects.findWhere({ krpt: subject.krpt }).get('subject_id');
                    if (form.krsu === 'VisitStart') {
                        handleVisitStartSync(userVisits, visitModel, form, subjectId, subject.krpt, visit.eventId);
                    } else if (form.krsu === 'VisitEnd') {
                        handleVisitEndSync(userVisits, visitModel, form, subjectId, subject.krpt, visit.eventId);
                    } else {
                        //sync regular forms
                        const questionnaire = LF.StudyDesign.questionnaires.find(questionnaire => {
                            return questionnaire.get('SU') === form.krsu && _.contains(visitModel.get('forms'), questionnaire.get('id'));
                        });

                        if (!questionnaire) {
                            logger.info(`Unknown questionnaire or not a part of visit for report record with SU: ${form.krsu} for visit: ${visit.eventId} with sigorig: ${form.sigOrig}`);
                            return;
                        }

                        let userReportExists = collection.findWhere({
                            questionnaire_id: questionnaire.get('id'),
                            subject_krpt: subject.krpt,
                            sig_id: form.sigOrig
                        });

                        if (form.deleted !== '0') {
                            if (userReportExists) {
                                logger.info(`Deleting user report record: ${questionnaire.get('id')} for patient: ${subjectId} in visit: ${visitModel.get('id')} with sigorig: ${userReportExists.get('sig_id')}`);
                                userReportExists.destroy();
                            }
                        } else {
                            if (!userReportExists) {
                                // eslint-disable-next-line new-cap
                                let userReport = new collection.model();

                                userReport.set({
                                    questionnaire_id: questionnaire.get('id'),
                                    user_visit_id: visitModel.get('id'),
                                    subject_krpt: subject.krpt,
                                    state: LF.QuestionnaireStates.COMPLETED,
                                    date_recorded: form.completedTS,
                                    phase: parseInt(form.phaseId, 10),
                                    sig_id: form.sigOrig
                                });
                                logger.info(`Creating new user report record: ${questionnaire.get('id')} for patient: ${subjectId} in visit: ${visitModel.get('id')} with sigorig: ${userReport.get('sig_id')}`);
                                collection.add(userReport, { merge: true });
                            } else {
                                userReportExists.set({
                                    user_visit_id: visitModel.get('id'),
                                    date_recorded: form.completedTS,
                                    phase: parseInt(form.phaseId, 10)
                                });
                                logger.info(`Updating the existing user report record: ${questionnaire.get('id')} for patient: ${subjectId} in visit: ${visitModel.get('id')} with sigorig: ${userReportExists.get('sig_id')}`);
                            }
                        }
                    }
                });
            });
        });
    })
    .then(() => {
        logger.trace('Saving the User Report Collection');
        return collection.save();
    })
    .then(() => {
        logger.trace('Saving the User Visits Collection');
        return userVisits.save();
    })
    .then(() => collection)
    .catch(err => {
        logger.error('Error saving userReport data.', err);
    });
}

ELF.action('saveUserVisitsAndReports', saveUserVisitsAndReports);
