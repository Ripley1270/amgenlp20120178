import ELF from 'core/ELF';
import * as syncUsers from 'core/actions/syncUsers';
import * as getAllSubjects from 'sitepad/actions/getAllSubjects';
import * as syncUserVisitsAndReports from 'sitepad/actions/syncUserVisitsAndReports';
import * as lastRefreshTime from 'sitepad/actions/lastRefreshTime';
import Logger from 'core/Logger';

let logger = new Logger('refreshAllData');

/**
 * Refreshes all data for Subjects, User Visits and Reports, Users
 * @return {Q.Promise<void>}
 */
export default function refreshAllData () {
    return getAllSubjects.getAllSubjects(true)
    .then(() => syncUserVisitsAndReports.syncUserVisitsAndReports(true))
    .then(() => syncUsers.syncUsers(true))
    .then(() => lastRefreshTime.updateLastRefreshTime())
    .catch(err => {
        logger.error('Error during Refresh All', err);
    });
}

ELF.action('refreshAllData', refreshAllData);
