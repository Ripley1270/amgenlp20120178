import ELF from 'core/ELF';
import CurrentContext from 'core/CurrentContext';
import * as lStorage from 'core/lStorage';


/**
 * @memberOf ELF.actions
 * Sets the context language to that of the site.
 * @returns {Q.Promise<void>}
 * @example
 * resolve: [{ action: 'setLanguageToSite' }]
 */
export default function setLanguageToSite () {
    let language = lStorage.getItem('language') || LF.StudyDesign.defaultLanguage || 'en';
    let locale = lStorage.getItem('locale') || LF.StudyDesign.defaultLocale || 'US';

    return CurrentContext().setContextLanguage(`${language}-${locale}`);
}

ELF.action('setLanguageToSite', setLanguageToSite);
