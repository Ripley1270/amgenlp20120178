import * as utils from 'core/utilities';
import CurrentContext from 'core/CurrentContext';
import ELF from 'core/ELF';
import Logger from 'core/Logger';
import Users from 'core/collections/Users';
import UniversalLogin from 'core/classes/UniversalLogin';

import ContextSwitchingView from 'sitepad/views/ContextSwitchingView';
import SetPasswordView from '../views/SetPasswordView';
import SecretQuestionView from '../views/SecretQuestionView';

let logger = new Logger('action/questionnaireScreenAuthenticationByRole');

/**
 * Execute the validation function attached to a widget if present.
 * @param {Object} param - Parameters passed in via ELF rule.
 * @param {string} param.questionnaire - Questionnaire Id
 * @param {Object} param.subject - Subject model
 * @returns {Q.Promise<Boolean|Object>}
 */
export default function questionnaireScreenAuthenticationByRole (param) {
    return Q.Promise((resolve, reject) => {
        LF.router.controller.view = this;

        let screen = this.data.screens[this.screen],
            role = LF.security.activeUser ? LF.security.activeUser.get('role') : '',

            // DE16290 - If there was no accessRole configuration, and no role owner, the default access role is 'subject'.
            diaryLevelRoles = this.model.get('accessRoles') ||
                (this.questionnaireRoleOwner ? [this.questionnaireRoleOwner.get('role')] : ['subject']),

            // DE16290 - accessRoles should always be configured as an Array, but just in case it isn't...
            screenLevelRoles = (value) => {
                return _.isArray(value) ? value : [(value != null ? value : 'subject')];
            };

        screenLevelRoles = screenLevelRoles(param.screenLevelRoles || screen.get('accessRoles') || diaryLevelRoles);

        let universalLogin = new UniversalLogin({
            loginView: ContextSwitchingView,
            changeTempPasswordView: SetPasswordView,
            resetSecretQuestionView: SecretQuestionView,
            resetPasswordView: SetPasswordView,
            // DE16259 - "No role" popup not displayed.
            // Added a fallback to the questionnaire level configuration.
            noRoleFound: screen.get('noRoleFound') || this.model.get('noRoleFound'),
            subject: param.subject,
            visit: param.visit,
            appController: LF.router.controller
        });

        /**
         * This function is passed into the UniversalLogin and is invoked after a user has successfully signed in.
         * @inner
         * @returns {Q.Promise<void>}
         */
        let successfulLoginReRenderFunction = () => {
            let user = CurrentContext().get('user'),
                roleOwners = typeof this.model.get('roleOwners') === 'undefined' ? true : this.model.get('roleOwners'),
                allowOwner = roleOwners && (typeof roleOwners[user.get('role')] === 'undefined' ? true : roleOwners[user.get('role')]);

            if (allowOwner && !this.allRoleOwners[user.get('role')]) {
                this.allRoleOwners[user.get('role')] = user;
            }

            let screenToRender = _(this.screenStack).last();

            // DE16314
            // If there is no screen in the screen stack to render,
            // this is the first screen of the questionnaire, and we should reset the started attribute on the dashboard record.
            if (screenToRender == null) {
                this.data.started = new Date();
                // DE18996 this.data.dashboard can be undefined for diaries based on BaseQuestionnaireView(e.g. New_Patient)
                if (this.data.dashboard) {
                    this.data.dashboard.set('started', this.data.started.ISOStamp());
                }

                // Restart the questionnaire timeout once the context switch is complete.
                LF.security.startQuestionnaireTimeOut();
            } else {
                // Otherwise, we need to restart the paused questionnaire timeout.
                LF.security.restartQuestionnaireTimeout();
            }

            return this.render(screenToRender)
                .then(() =>  this.delegateEvents());
        };

        /**
         * This function is passed into the UniversalLogin and used to determine if a user should be available for login.
         * @inner
         * @param {User} user The collection of users to sort.
         * @returns {Q.Promise<User|Boolean>} Either returns the user to be displayed, or false.
         */
        let filterUsers = (user) => {
            let roleOwner = this.allRoleOwners[user.get('role')],
                // We use Utilities.getNested() to prevent a potential error from being thrown.
                // Instead we get undefined.
                subjectRole = utils.getNested(LF, 'StudyDesign.sitePad.subjectRole'),
                newScreenLevelRoles = _.clone(screenLevelRoles),
                idx = screenLevelRoles.indexOf(subjectRole);

            if (!roleOwner && idx > -1) {
                newScreenLevelRoles.splice(idx, 1);

                if (subjectRole === user.get('role')) {
                    if (param.subject && param.subject.get('user') === user.get('id')) {
                        return Q(user);
                    }
                }
            }

            return Q.Promise((resolve) => {
                if (newScreenLevelRoles.indexOf(user.get('role')) > -1) {
                    if (roleOwner) {
                        if (roleOwner.get('role') === user.get('role') && roleOwner.get('username') === user.get('username')) {
                            resolve(user);
                        } else {
                            resolve(false);
                        }
                    } else {
                        resolve(user);
                    }
                } else {
                    resolve(false);
                }
            });
        };

        /**
         * This function is passed into the UniversalLogin and is used to sort the users into the correct display order.
         * @inner
         * @param {Users} users The collection of users to sort.
         * @returns {Q.Promise<Users>} The sorted Users collection.
         */
        let sortUsers = (users) => {
            return Q.Promise((resolve) => {
                let sorted = (_.sortBy(users, (user) => {
                    // Its possible for a Sync'd user to be added that wont have a lastLogin time. A default date in added for sorting.
                    return user.get('lastLogin') ? user.get('lastLogin') : new Date(1986, 1, 21).toString();
                })).reverse();

                resolve(sorted);
            });
        };

        this.allRoleOwners = this.allRoleOwners || {};

        if (!this.questionnaireRoleOwner) {
            if (role && diaryLevelRoles.indexOf(role) > -1) {

                // Users are fetched here, but nothing seems to be done with the resulting collection.
                // TODO: Can we remove this call?
                // eslint-disable-next-line no-unused-vars
                Users.fetchCollection().then((users) => {
                    this.questionnaireRoleOwner = param.questionnaireRoleOwner || LF.security.activeUser;

                    // Affidavit was already configured before the role owner was determined. Remove that Affidavit and make sure the correct one is added.
                    // This could be changed so that QuestionnaireView.configureAffidavit() is not called until this point. Currently breaks Sitepad if done this way.
                    let screenAffidavitIndex = this.data.screens.indexOf(_.findWhere(this.data.screens, {id: 'AFFIDAVIT'}));
                    let questionAffidavit = this.data.questions.get('AFFIDAVIT');

                    if (questionAffidavit && screenAffidavitIndex > -1) {
                        this.data.screens.splice(screenAffidavitIndex, 1);
                        this.data.questions.remove(questionAffidavit, 1);

                        this.configureAffidavit();
                    } else if (!questionAffidavit && screenAffidavitIndex === -1) {
                        this.configureAffidavit();
                    } else {
                        logger.error('You have an affidavit screen or question, but not the other. Cannot configure affidavit correctly.');
                        return;
                    }

                    let affidavit = _.findWhere(this.data.screens, {id: 'AFFIDAVIT'});

                    if (affidavit) {
                        affidavit.accessRoles = this.questionnaireRoleOwner ? [this.questionnaireRoleOwner.get('role')] : undefined;
                    }
                })
                .done();
            }
        }

        let request = Q(),
            backArrow = undefined,
            forwardArrow = undefined;

        // If direction is not set, this is the displaying the first screen on entry of the diary.
        // We should determine the user who entered the diary.
        if (!this.direction) {
            if (!this.questionnaireEntryUser) {
                this.questionnaireEntryUser = LF.security.activeUser;
            }

            // This is the back arrow function only if the first screen of a diary requires a context switch
            backArrow = () => {
                let filterUser = (user) => {
                    return Q.Promise((resolve) => {
                        let backoutRoles = utils.getNested(LF, 'StudyDesign.sitePad.diaryBackoutRoles');

                        if (backoutRoles.indexOf(user.get('role')) > -1) {
                            resolve(user);
                        } else {
                            resolve(false);
                        }
                    });
                };

                let nextArrow = () => {
                    return this.render(_(this.screenStack).last())
                        .then(() =>  this.delegateEvents());
                };

                let successfulLogin = () => {
                    this.navigate('dashboard', true, {
                        subject: param.subject,
                        visit: param.visit
                    });
                };

                universalLogin.newUserLogin(successfulLogin, filterUser, sortUsers, null, nextArrow);
            };
        } else if (this.direction === 'forward') {
            backArrow = () => {
                return this.previous();
            };
        } else {
            forwardArrow = () => {
                return this.next();
            };
        }

        request.then(() => {
            if (screenLevelRoles.indexOf(role) === -1) {
                // During a context switch, we pause the questionnaire timeout.
                LF.security.pauseQuestionnaireTimeout();

                universalLogin.newUserLogin(successfulLoginReRenderFunction, filterUsers, sortUsers, backArrow, forwardArrow);
                resolve({ stopActions: true, preventDefault: true });
            } else {
                resolve();
            }
        })
        .catch(reject)
        .done();
    });
}

ELF.action('questionnaireScreenAuthenticationByRole', questionnaireScreenAuthenticationByRole);

LF.Actions.questionnaireScreenAuthenticationByRole = questionnaireScreenAuthenticationByRole;
