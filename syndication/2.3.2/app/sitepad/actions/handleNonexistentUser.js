import ELF from 'core/ELF';

/**
 * Deletes a nonexistent user on the device.
 * @param {Object} params - Parameters provided by the ELF rule.
 * @param {User} params.user - The nonexistent user on the device.
 * @returns {Q.Promise<Object>}
 */
export function handleNonexistentUser (params) {
    return params.user.destroy();
}

ELF.action('handleNonexistentUser', handleNonexistentUser);