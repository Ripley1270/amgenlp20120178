import ELF from 'core/ELF';
import * as lStorage from 'core/lStorage';

let time;

/**
 * Sets the localstroge value with the time of the last succesfull refresh all.
 * @return {Q.Promise<void>}
 */
export function updateLastRefreshTime () {
    time && lStorage.setItem('lastRefreshTime', time);
    return Q();
}

/**
 * Sets temp time value for the Last Refresh Time. If all sync operations are
 * successfully completed this value will be saved to localstorage
 * @param {String} refreshTime refresh Time value
 * @return {Q.Promise<void>}
 */
export function setTime (refreshTime) {
    time = refreshTime;
    return Q();
}

ELF.action('updateLastRefreshTime', updateLastRefreshTime);
