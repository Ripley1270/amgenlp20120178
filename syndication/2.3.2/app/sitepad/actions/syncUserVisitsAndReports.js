import ELF from 'core/ELF';
import * as syncHistoricalData from 'core/actions/syncHistoricalData';

/**
 * Triggers the syncHistoricalData action for syncing UserVisitsAndReports.
 * @param {boolean} [rejectOnError=false] If true rejects the promise returned
 * @return {Q.Promise<void>}
 */
export function syncUserVisitsAndReports (rejectOnError = false) {
    return syncHistoricalData.syncHistoricalData({
        collection: 'UserReports',
        webServiceFunction: 'syncUserVisitsAndReports',
        activation: false
    }, rejectOnError);
}

ELF.action('syncUserVisitsAndReports', syncUserVisitsAndReports);
