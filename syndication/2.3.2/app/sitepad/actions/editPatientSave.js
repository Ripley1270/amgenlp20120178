import ELF from 'core/ELF';
import Answer from 'core/models/Answer';
import Logger from 'core/Logger';
import Transmission from 'core/models/Transmission';
import Transmissions from 'core/collections/Transmissions';
import { MessageRepo } from 'core/Notify';
import * as lStorage from 'core/lStorage';
import trial from 'core/index';

let logger = new Logger('editPatientSave');
const design = trial.assets.studyDesign;

/**
 * Saves a new Edit Patient Diary
 * @param {Object} params - Not used in this action.
 * @param {Function} done - A callback function invoked upon completion of the action.
 * @returns {Q.Promise<void>}
 */
export function editPatientSave (params, done) {
    let deferred = Q.defer(),
        diaryAnswers = this.data.answers,
        subject = this.subject,
        user = this.user,
        dashboard = this.data.dashboard,
        reason = diaryAnswers.match({ question_id: 'EDIT_PATIENT_REASON' })[0].get('response'),
        answersToRemove = [],
        transmissionsToCheck,
        addStatus = false,
        checkAndAddAnswers = () => {
            let promise = Q.defer(),
                changes = 0,
                answer,
                subjectIdAnswer,
                subjectInitialsAnswer,
                languageAnswer;

            answer = diaryAnswers.match({ question_id: 'EDIT_PATIENT_ID' });
            subjectIdAnswer = answer[0].get('response');

            if (subjectIdAnswer === subject.get('subject_id')) {
                answersToRemove.push(answer[0]);
                if (subject.get('isDuplicate')) {
                    addStatus =  true;
                }
            } else {
                let comment = new Answer({
                        subject_id: subject.get('subject_id'),
                        response: reason,
                        SW_Alias: 'PT.Patientid.Comment',
                        questionnaire_id: this.id,
                        question_id: 'EDIT_PATIENT_ID_COMMENT'
                    }),
                    subjectNumber = subjectIdAnswer.substring(LF.StudyDesign.participantSettings.participantNumberPortion[0],
                            LF.StudyDesign.participantSettings.participantNumberPortion[1]);

                diaryAnswers.add(comment);

                // needed for check if there was any changes
                changes++;

                // setting the subject_id to subject model
                subject.set('subject_id', subjectIdAnswer);

                // setting subject number to subject model
                subject.set('subject_number', subjectNumber);

                // setting the subject_id to username of the user model (DE16517)
                user.set('username', subjectIdAnswer);

                // hacking SWAlias
                answer[0].set('SW_Alias', answer[0].get('SW_Alias').replace('.0.', '.'));
            }

            answer = diaryAnswers.match({ question_id: 'EDIT_PATIENT_INITIALS' });
            subjectInitialsAnswer = answer[0].get('response');

            if (subjectInitialsAnswer === subject.get('initials')) {
                answersToRemove.push(answer[0]);
            } else {
                let comment = new Answer({
                    subject_id: subject.get('subject_id'),
                    response: reason,
                    SW_Alias: 'PT.Initials.Comment',
                    questionnaire_id: this.id,
                    question_id: 'EDIT_PATIENT_INITIALS_COMMENT'
                });

                diaryAnswers.add(comment);

                // needed for chech if there was any changes
                changes++;

                //setting the initials to subject model
                subject.set('initials', subjectInitialsAnswer);

                //hacking SWAlias
                answer[0].set('SW_Alias', answer[0].get('SW_Alias').replace('.0.', '.'));
            }

            answer = diaryAnswers.match({ question_id: 'EDIT_PATIENT_LANGUAGE' });
            languageAnswer = answer[0].get('response');

            if (languageAnswer === user.get('language')) {
                answersToRemove.push(answer[0]);
            } else {
                let comment = new Answer({
                    subject_id: subject.get('subject_id'),
                    response: reason,
                    SW_Alias: 'Assignment.0.Language.Comment',
                    questionnaire_id: this.id,
                    question_id: 'EDIT_PATIENT_LANGUAGE_COMMENT'
                });
                diaryAnswers.add(comment);
                // needed for chech if there was any changes
                changes++;
                //setting the language to user model
                user.set('language', languageAnswer);
                answer[0].set('response', languageAnswer.replace('-', '_'));
            }
            promise.resolve(changes);
            return promise.promise;
        };

    Q.all([Transmissions.fetchCollection()])
        .spread((transmissions) => {
            transmissionsToCheck = transmissions.filter((transmission) => {
                return transmission.get('status') === 'failed';
            });

            if (transmissionsToCheck) {
                transmissionsToCheck.map((transmission) => {
                    let transmissionMethod = transmission.get('method'),
                        parsedParams = JSON.parse(transmission.get('params'));

                    switch (transmissionMethod) {
                        case 'transmitSubjectAssignment': {
                            if (parsedParams.krpt === subject.get('krpt')) {
                                let answer = diaryAnswers.match({ question_id: 'EDIT_PATIENT_ID' })[0].get('response'),
                                    codeLength = design.participantSettings.participantIDFormat.length;

                                if (parsedParams.patientId !== answer) {
                                    parsedParams.patientId = answer;
                                    parsedParams = JSON.stringify(parsedParams);
                                    transmission.unset('status');
                                    transmission.save({params: parsedParams});
                                    subject.save({subject_id: subject.get('subject_id').substr(0,codeLength)});
                                }
                            }
                            break;
                        }

                        case 'transmitEditPatient': {
                            if (parsedParams.krpt === subject.get('krpt') && !addStatus) {
                                transmission.destroy();
                            }
                            break;
                        }
                    }
                });
            }
            return checkAndAddAnswers();
        })
        .then((res) => {
            if (res > 0) {
                let removeAnswers = () => {
                    //answers to delete should be deleted here
                    let reasonAnswer = diaryAnswers.match({ question_id: 'EDIT_PATIENT_REASON' });

                    if (reasonAnswer) {
                        answersToRemove.push(reasonAnswer[0]);
                    }

                    diaryAnswers.remove(answersToRemove);
                },
                createTransmission = () => {
                    let model = new Transmission(),
                        user = LF.security.activeUser,
                        now = new Date(),
                        diaryData = _.where(diaryAnswers.toJSON(), { questionnaire_id: this.id }),
                        params = JSON.stringify({
                            krpt: subject.get('krpt'),
                            ResponsibleParty: user.get('username'),
                            dateStarted: dashboard.get('started'),
                            dateCompleted: now.ISOStamp(),
                            reportDate: dashboard.get('report_date'),
                            phase: subject.get('phase'),
                            phaseStartDate: subject.get('phaseStartDateTZOffset'),
                            sigID: `SA.${this.data.started.getTime().toString(16)}${lStorage.getItem('IMEI')}`,
                            answers: diaryData,
                            ink: this.data.ink,
                            batteryLevel: dashboard.get('battery_level')
                        }),
                       newTransmission = {
                        method: 'transmitEditPatient',
                        params: params,
                        created: now.getTime()
                    };

                    if (addStatus) {
                        newTransmission.status = 'failed';
                    }

                    return model.save(newTransmission);
                };

                Q().then(removeAnswers)
                    .then(createTransmission)
                    .then(() => {
                        return subject.save();
                    })
                    .then(() => {
                        return user.save();
                    })
                    .then(() => {
                        deferred.resolve(true);
                    })
                    .catch((e) => {
                        logger.error(`EditPatientSave removeAnswers,createTransmission failed:  ${e}`);
                        LF.spinner.hide();
                        deferred.reject(false);
                    }).done(() => {
                        done({ preventDefault: true });
                    });
            } else {
                LF.spinner.hide()
                .then(() => {
                    // Displays a modal informing the user that no changes have been made.
                    // If the user click's cancel, the promise is rejected, and nothing happens.
                    return MessageRepo.display(MessageRepo.Dialog[`EDIT_PATIENT_${this.id}`]);
                })
                .then(() => {
                    this.data.answers = undefined;
                    this.navigate('home');
                    deferred.resolve(false);
                })
                .catch(() => {
                    deferred.resolve(false);
                })
                .done(() => {
                    done({preventDefault: true, stopActions: true, stopRules: true });
                });
            }
        })
        .catch((e) => {
            logger.error(`EditPatientSave checkAndAddAnswers failed: ${e}`);
            LF.spinner.hide();
            deferred.reject(false);
        })
        .done();

    return deferred.promise;
}

ELF.action('editPatientSave', editPatientSave);
