import BaseStartup from 'core/classes/BaseStartup';
import trial from 'core/index';

import ELF from 'core/ELF';
import Logger from 'core/Logger';
import { pingStudyApi } from 'core/expressions/isStudyOnline';

// I believe this is here to be bootstraped at startup.
// eslint-disable-next-line no-unused-vars
import Transmit from 'sitepad/transmit';

import Router from 'sitepad/Router';
import RegistrationController from 'sitepad/controllers/RegistrationController';
import ApplicationController from 'sitepad/controllers/ApplicationController';
import SettingsController from 'sitepad/controllers/SettingsController';

import setupSitepadMessages from 'sitepad/resources/Messages';

let logger = new Logger('Startup');

export default class Startup extends BaseStartup {

    /**
     * Setup the application's router.
     * @param {Object} [options={}] - Options passed into the Router's constructor.
     * @returns {Q.Promise<void>}
     */
    backboneStartup (options = {}) {
        logger.traceEnter('backboneStartup');

        return Q.Promise(resolve => {
            const registration = new RegistrationController();
            const application = new ApplicationController();
            const settings = new SettingsController();
            const study = new trial.assets.StudyController();

            const routerOpts = _.extend({}, options.routerOpts, {
                controllers: _.extend({}, options.controllers, {
                    application, registration, settings, study
                })
            });

            LF.router = new Router(routerOpts);
            resolve();
        })
        .catch((err) => {
            logger.error('Error on Backbone startup.', err);
        })
        .finally(() => {
            logger.traceExit('backboneStartup');
        });
    }

    /**
     * Start Backbone.history
     */
    startUI () {
        logger.traceEnter('startUI');

        Backbone.history.start({ pushState: false, silent: true });

        // hack so backbone things route it changing.  Unfortunately it sets fragment to ''
        // if silent: true, even though it never actually navigated anywhere.
        Backbone.history.fragment = undefined;
        logger.traceExit('startUI');
    }

    /**
     * Startup the application.
     * @returns {Q.Promise<void>}
     */
    startup () {
        setupSitepadMessages();

        return this.preSystemStartup()
        .then(() => this.jQueryStartup())
        .then(() => this.cordovaStartup())
        .then(() => this.studyStartup())
        .then(() => this.backboneStartup())
        .then(() => this.usersAndContext())
        .then(() => this.startUI())
        .then(() => {
            // This block is a workaround for the transmission issue in DE20508
            // Workaround will attempt a dummy subject sync once the device goes offline
            LF.Wrapper.exec({
                execWhenWrapped: () => {
                    window.addEventListener('offline', () => {
                        pingStudyApi()
                        .done();
                    }, false);
                },
                execWhenNotWrapped: $.noop
            });
        })
        .then(() => ELF.trigger('APPLICATION:Loaded'))
        .then((res) => {
            // TODO remove the usage of this flag.
            localStorage.removeItem('questionnaireCompleted');

            if (!res.preventDefault) {
                LF.router.navigate('', true);
            }
        })
        .catch(err => err && logger.error('Error Loading Application', err));
    }
}
