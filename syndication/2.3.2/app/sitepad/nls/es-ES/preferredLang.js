/**
 * @fileOverview es_ES preferred Language set.
 * @author <a href="mailto:mkamthewala@phtcorp.com">Mansoor Kamthewala</a>
 * @version 1.5
 */

LF.Preferred = {
    /**
     * Preferred language object.
     * @memberof LF
     * @readonly
     * @type {String}
     */
    language    : 'es',

    /**
     * Preferred locale object.
     * @memberof LF
     * @readonly
     * @type {String}
     */
    locale    : 'ES'
};