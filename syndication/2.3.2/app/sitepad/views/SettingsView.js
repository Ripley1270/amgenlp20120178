import PageView from 'core/views/PageView';
import Sites from 'core/collections/Sites';
import COOL from 'core/COOL';

export default class SettingsView extends PageView {
    constructor (options) {
        super(options);

        /**
         * @property {String} template A selector that points to the template for the view.
         * @readonly
         * @default '#site-users-tpl'
         */
        this.template = '#settings-template';

        /**
         * @property {Object} events A list of DOM events.
         * @readonly
         */
        this.events = {
            'click #back': 'back',
            'click #about': 'about',
            'click #customer-support': 'customerSupport',
            'click #update': 'sync',
            'click #set-timezone': 'setTimeZone'
        };

        /**
         * @property {Object} templateStrings A list of resource keys to translate and render to the UI.
         */
        this.templateStrings = {
            title: 'SETTINGS',
            site: 'SITE',
            checkForUpdates: 'CHECK_FOR_UPDATES',
            supportTools: 'SUPPORT_TOOLS',
            about: 'ABOUT',
            editInfo: 'EDIT_MY_INFO',
            customerSupport: 'SUPPORT',
            setTimeZone: 'SET_TIMEZONE'
        };
    }

    /**
     * @property {String} id The ID of the view.
     * @readonly
     * @default 'settings-view'
     */
    get id () {
        return 'settings-view';
    }

    /**
     * Resolve any dependencies required for the view to render.
     * @returns {Q.Promise<void>}
     */
    resolve () {
        return Sites.fetchFirstEntry()
        .then(site => {
            this.site = site;
        });
    }

    /**
     * Render the view to the DOM
     * @returns {Q.Promise<void>}
     */
    render () {
        let user = LF.security.activeUser;

        return this.buildHTML({
            username: user.get('username'),
            siteNumber: this.site.get('siteCode')
        }, true);
    }

    /**
     * Execute a device sync.
     */
    sync () {
        // Using removed code from prototype which communicated to the SW NG API.
        // Sync.fetch();
    }

    /**
     * Navigate to the About view.
     */
    about () {
        this.navigate('about');
    }

    /**
     * Navigate to the SetTimeZone view.
     */
    setTimeZone () {
        this.navigate('set-time-zone');
    }

    /**
     * Navigate to the Customer Support view.
     */
    customerSupport () {
        this.navigate('customer-support');
    }

    /**
     * Navigate back to the home view.
     */
    back () {
        this.navigate('home');
    }
}

COOL.add('SettingsView', SettingsView);
