import PageView from 'core/views/PageView';
import * as lStorage from 'core/lStorage';
import State from 'core/classes/State';
import COOL from 'core/COOL';
import CurrentContext from 'core/CurrentContext';

export default class LanguageSelectionView extends PageView {

    constructor (options) {
        super(options);

        /**
         * @property {string} template - A selector that points to the template for the view.
         * @readonly
         * @default '#language-selection-tpl'
         */
        this.template = '#language-selection-tpl';

        /**
         * @property {Object<string,string>} events - A list of DOM events.
         * @readonly
         */
        this.events = {
            'click #next'      : 'next',
            'change #language' : 'changeLanguage'
        };

        /**
         * @property {Object<string,string>} selectors - A list of selectors to populate.
         */
        this.selectors = {
            language: '#language'
        };

        /**
         * @property {Object<string,string>} templateStrings - A list of resource keys to translate and render to the UI.
         */
        this.templateStrings = {
            header   : 'APPLICATION_HEADER',
            language : 'LANGUAGE',
            tagline  : 'SELECT_LANGUAGE',
            next     : 'NEXT'
        };

        /*
         * @property {State} state - An instance of the State class.
         */
        this.state = new State();
    }

    /**
     * @property {string} id - The ID of the view.
     * @readonly
     * @default 'language-selection-view'
     */
    get id () {
        return 'language-selection-view';
    }

    /**
     * Render the view to the DOM.
     * @returns {Q.Promise.<void>}
     */
    render () {
        return this.buildHTML(null, true).then(() => {
            this.renderLanguages();
            this.delegateEvents();
            this.$language.select2({
                minimumResultsForSearch: Infinity
            });
        });
    }

    /**
     * Render a list of available languages.
     */
    renderLanguages () {
        let languages = LF.strings.getLanguages();

        languages.forEach(resource => {
            let el = `<option value="${resource.language}-${resource.locale}">${resource.localized}</option>`;

            if (resource.language === LF.Preferred.language && resource.locale === LF.Preferred.locale) {
                el = $(el).prop('selected', true);
            }

            this.$language.append(el);

            // Clear out DOM reference to prevent potential memory leaks.
            el = null;
        });
    }

    /**
     * Handle a language change event.
     * @param {ChangeEvent} evt - An input change event.
     * @returns {Q.Promise<void>}
     */
    changeLanguage (evt) {
        let value = this.$language.val();

        evt && evt.preventDefault();

        return CurrentContext().setContextLanguage(value)
        .then(() => {
            this.render();
        });
    }

    /**
     * Navigate to the next view.
     * @param {(MouseEvent|TouchEvent)} evt - A mouse or touch event.
     */
    next (evt) {
        let mode = lStorage.getItem('mode'),
            value = this.$language.val(),
            split = value.split('-'),
            lang = split[0],
            locale = split[1],
            state,
            screenName;

        evt && evt.preventDefault();

        lStorage.setItem('language', lang);
        lStorage.setItem('locale', locale);

        switch (mode) {
            case 'provision':
            case 'trainer':
                state = 'LOCKED';
                screenName = 'unlockCode';
                break;
            default:
                state = 'SITE_SELECTION';
                screenName = 'siteSelection';
        }

        this.state.set(state);
        this.navigate(screenName);
    }
}

COOL.add('LanguageSelectionView', LanguageSelectionView);
