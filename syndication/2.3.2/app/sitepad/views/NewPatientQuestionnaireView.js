import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';
import Sites from 'core/collections/Sites';
import Data from 'core/Data';

export default class NewPatientQuestionnaireView extends BaseQuestionnaireView {
    /**
     * Resolves any dependencies required for the view to render.
     * @returns {Q.Promise<void>}
     */
    resolve () {
        let setBatteryLevel = () => {
            return Q.Promise(resolve => {
                LF.Wrapper.Utils.getBatteryLevel(batteryLevel => {
                    this.data.batteryLevel = batteryLevel;

                    resolve();
                });
            });
        };

        return Sites.fetchFirstEntry()
            .then(site => {
                Data.site = site;
            })
            .then(() => setBatteryLevel())
            .then(() => super.resolve());
    }
}
