import QuestionnaireView from 'core/views/QuestionnaireView';
import Data from 'core/Data';
import Logger from 'core/Logger';
import CurrentContext from 'core/CurrentContext';
import Subject from 'core/models/Subject';
import User from 'core/models/User';
import Sites from 'core/collections/Sites';

let logger = new Logger('EditPatientQuestionnaireView');

export default class EditPatientQuestionnaireView extends QuestionnaireView {
    constructor (options) {
        super(options);

        /**
         * @property {number} subjectId - The ID of the subject to edit.
         */
        this.subjectId = parseInt(options.subjectId, 10);

        /**
         *  @property {boolean} showLoginInfo - Used to indicate if the LoginInfo should be displayed.
         *  @default false
         */
        this.showLoginInfo = (options.showLoginInfo) ? options.showLoginInfo : false;
    }

    /**
     * Resolve any data required for the view to render.
     * @returns {Q.Promise<void>}
     */
    resolve () {
        /**
         * @property {Subject} subject - The subject to edit.
         */
        this.subject = new Subject({ id: this.subjectId });

        /**
         * @property {User} user - The user record associated with the subject.
         */
        this.user = new User();

        return this.subject.fetch()
            .then(() => {
                // We need to set the target subject to the DynamicText object,
                // so a few DynamicText functions have access to it.
                LF.DynamicText.subject = this.subject;

                // Set the ID of the user from the subject model's user property.
                this.user.set({ id: this.subject.get('user') });

                return this.user.fetch();
            })
            // Fetch the site
            .then(() => Sites.fetchFirstEntry())
            // And assign it.
            .then(site => {
                /**
                 * @property {Site} site - The site at which the device belongs.
                 */
                this.site = site;

                // Unfortunately...we need to have the site available on the Data service.
                Data.site = this.site;
            })
            .then(() => super.resolve());
    }

    /**
     * Renders the view.
     * @param {string} id - The screen ID to render.
     * @returns {Q.Promise<void>}
     */
    render (id) {
        // TODO: Instead of returning Q.Promise, the promise chain started with this.buildHTML could be returned
        return Q.promise((resolve, reject) => {
            this.templateStrings = {
                header: {
                    namespace: this.model.get('id'),
                    key: this.model.get('displayName')
                },
                back: 'BACK',
                next: 'NEXT'
            };

            if (this.showCancel) {
                this.templateStrings.cancel = 'CANCEL';
            } else {
                this.templateStrings.cancel = '';
            }

            if (this.showLoginInfo) {
                _.extend(this.templateStrings, {
                    loggedInAs: 'LOGGED_IN_AS',
                    site: 'SITE'
                });
            }

            this.buildHTML({
                className: this.model.get('className')
            })
            .then((translated) => {
                this.page();

                if (this.showLoginInfo) {
                    let dynamicStrings = {
                        userName: CurrentContext().get('user').get('username'),
                        siteNumber: this.site.get('siteCode'),
                        textClass: 'text-right'
                    };
                    this.loginInfo = LF.templates.display('DEFAULT:LoginInfo', _.extend(translated, dynamicStrings));
                    this.$('div.container').prepend(this.loginInfo);
                }

                // Remove the cancel button, if appropriate:
                if (!this.showCancel) {
                    this.$('#cancelItem').remove();
                }
            })
            .then(() => ELF.trigger(`QUESTIONNAIRE:Rendered/${this.id}`, { questionnaire: this.id }, this))
            .then(() => {
                return this.displayScreen(id || this.data.screens[0].get('id'));
            })
            .then(() => {
                if (!id) {
                    this.screenStack.push(this.data.screens[0].get('id'));
                }

                this.delegateEvents();
                resolve();
            })
            .catch((e) => {
                logger.error('Error in render method: ', e);
                reject(e);
            })
            .done();
        });
    }
}
