import TableView from 'sitepad/views/TableView';
import SubjectRowView from './SubjectRowView';
import Subjects from 'core/collections/Subjects';

/**
 * Displays a table of subjects.
 * @class SubjectTableView
 * @extends TableView
 */
export default class SubjectTableView extends TableView {
    constructor (options = {}) {
        super(options);

        /**
         * @property {Subjects} collection - The collection to fetch and render.
         */
        this.collection = new Subjects();

        // Set the default sort property.
        this.collection.comparator = 'subject_id';
    }

    /**
     * Render a row.
     * @param {Subject} model - The subject to render.
     * @returns {Q.Promise<DOMElement>}
     */
    renderRow (model) {
        let view = new SubjectRowView({ model, strings: this.options.translations });

        this.listenTo(view, 'select', this.select);
        this.listenTo(view, 'deselect', this.deselect);

        return view.render().then(() => view.el);
    }
}
