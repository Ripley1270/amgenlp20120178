import * as lStorage from 'core/lStorage';
import PageView from 'core/views/PageView';
import { MessageRepo } from 'core/Notify';
import Transmission from 'core/models/Transmission';
import Logger from 'core/Logger';
import CurrentContext from 'core/CurrentContext';
import COOL from 'core/COOL';

const logger = new Logger('AccountConfiguredView');

export default class AccountConfiguredView extends PageView {
    constructor (options) {
        super(options);

        /**
         * @property {string} template - The ID of the template to render.
         * @default '#account-configured-template'
         */
        this.template = '#account-configured-template';

        /**
         * @property {Object<string,string>} events - Events to bind to the view.
         */
        this.events = {
            'click #back': 'back',
            'click #submit': 'nextHandler',
            'click #cancel': 'cancel'
        };

        /**
         * Handle a mouse or touch event on the next button.
         * @returns {undefined}
         */
        this.nextHandler = _.debounce(() => {
            return this.next();
        }, 500, true);

        /**
         * @property {string} dialogKey - The key to use when displaying the cancel modal.
         */
        this.dialogKey = 'CANCEL_PERMANENT_PASSWORD';

        /**
         * @property {Object<string,string>} templateStrings - Resource keys to translate and pass into the template prior to rendering.
         */
        this.templateStrings = {
            title: 'ACCOUNT_CONFIGURED_TITLE',
            legend: 'ACCOUNT_CONFIGURED_TEXT',
            back: 'BACK',
            cancel: 'CANCEL',
            next: 'NEXT',
            ok: 'OK',
            settings: 'SETTINGS',
            help: 'HELP',
            header: 'APPLICATION_HEADER'
        };
    }

    /**
     * @property {string} id - The ID of the view.
     */
    get id () {
        return 'account-configured';
    }


    /**
     * Render the view.
     * @returns {AccountConfiguredView}
     */
    render () {
        this.buildHTML({}, true);

        // TODO: This should return a promise.
        return this;
    }

    /**
     * Cancel the form.
     * @returns {Q.Promise<void>}
     */
    cancel () {
        return MessageRepo.display(MessageRepo.Dialog && MessageRepo.Dialog[this.dialogKey])
            .then(() => {
                LF.Data.NewSecretQuestionParams = null;
                LF.Data.NewPasswordParams = null;

                this.navigate('login', true);
            }, $.noop);
    }

    /**
     * Go to the next screen.
     * @param {(MouseEvent|TouchEvent)} e - A mouse or touch event.
     */
    next (e) {
        e && e.preventDefault();

        // If the next button is disabled, do nothing.
        if (this.isDisabled('#submit')) {
            return;
        }

        let newRole = LF.StudyDesign.roles.findWhere({
            id: CurrentContext().role
        });

        COOL.getClass('Utilities').isOnline()
            .tap((isOnline) => {
                if (!isOnline && !newRole.canAddOffline()) {
                    MessageRepo.display(MessageRepo.Banner.CONNECTION_REQUIRED, { delay: 500 })
                    .done();
                    return;
                }

                this.disableButton('#submit');

                let newPassword = LF.Data.NewPasswordParams,
                    secretQuestionParams = null;

                if (LF.StudyDesign.askSecurityQuestion) {
                    secretQuestionParams = LF.Data.NewSecretQuestionParams;
                }

                localStorage.removeItem('Reset_Password');
                localStorage.removeItem('Reset_Password_Secret_Question');

                // save user credentials
                let user = CurrentContext().get('user'),
                    permanentPasswordCreated = user.get('permanentPasswordCreated');

                user.set('salt', newPassword.salt);
                user.set('password', newPassword.password);
                user.set('permanentPasswordCreated', true);

                if (secretQuestionParams) {
                    user.set('secretQuestion', secretQuestionParams.secretQuestion);
                    user.set('secretAnswer', secretQuestionParams.secretAnswer);
                }

                user.save()
                    .then(() => user.get('userType') !== 'Subject' && user.changeCredentials({}))
                    .then(() => user.getSubject())
                    .then((subject) => {
                        if (subject == null) {
                            return null;
                        }

                        this.subject = subject;

                        let transmitMethod,
                            params = {
                                password: newPassword.password,
                                krpt: subject.get('krpt')
                            };

                        if (permanentPasswordCreated) {
                            // Security question usage is enabled
                            if (LF.StudyDesign.askSecurityQuestion === true) {
                                if (secretQuestionParams) {
                                    // Subject forgot password & Forgot secret question. Reset both with unlock code
                                    transmitMethod = 'transmitResetCredentials';
                                    params.secretQuestion = secretQuestionParams.secretQuestion;
                                    params.secretAnswer = secretQuestionParams.secretAnswer;
                                } else {
                                    // Subject forgot only password. Reset using the secret QA
                                    // The API requires secret QA to be sent. These values are being appended in the transmit method.
                                    transmitMethod = 'transmitResetPassword';
                                }
                            } else {
                                // Subject forgot password. No Secret QA. Reset using unlock code.
                                transmitMethod = 'transmitResetPassword';
                            }
                        } else {
                            // "Set permenant password" path
                            transmitMethod = 'sitepadPasswordSecretQuestion';

                            // Add the secret QA to payload if askSecurityQuestion is set to true
                            if (LF.StudyDesign.askSecurityQuestion === true && !!secretQuestionParams) {
                                params.secret_question = secretQuestionParams.secretQuestion;
                                params.secret_answer = secretQuestionParams.secretAnswer;
                            }
                        }

                        return this.createTransmission(transmitMethod, params);
                    })
                    .then(() => {
                        LF.Data.NewSecretQuestionParams = null;
                        LF.Data.NewPasswordParams = null;

                        LF.security.resetFailureCount(user);

                        user.set('lastLogin', (new Date()).toString()).save();

                        LF.security.activeUser = user;
                        lStorage.setItem('isAuthorized', true);

                        this.navigate('dashboard');
                    })
                    .catch(err => logger.error(err))
                    .then(() => ELF.trigger('ACCOUNT:Configured', {
                        subject: this.subject
                    }, this))
                    .catch((err) => {
                        err && logger.error('Error processing account configured action.', err);
                    })
                    .done();
            })
            .done();
    }

    /**
     * Creates a new ResetSecretQuestion Transmission record.
     * @param {string} method - The name of the transmit method for the transmission
     * @param {Object} params - An object literal of parameters to transmit.
     * @returns {Q.Promise<void>}
     * @example
     * this.createTransmission('transmitResetPassword', {
     *     krpt: 'SA.4b00ec61d261193e1596b4f6b5e2afd7c'
     *     password: '3cfdd62161aed4a7669da58ceebb16a8a9236253d44ca842d41b8bc33a29ece30e17942f16dcccdde01548e55a9da948f0dd6805fe5d30da5d659cacb3b04159","krpt":"SA.258ca971765119426b849d356ff853207',
     *     secretQuestion: 0,
     *     secretAnswer: '91649befdbfead98474f42d9853c54621ac5c6ca25125cd9963f5f9b45012fa45ed8df8819bb99b246fdc7ca93c115279f6b2c97bdd67acc83ade70a2ce53580'
     * });
     */
    createTransmission (method, params) {
        let model = new Transmission();

        return model.save({
            method,
            params: JSON.stringify(params),
            created: new Date().getTime()
        });
    }

    /**
     * Navigate to the previous view.
     * @param {(MouseEvent|TouchEvent)} e - A mouse or touch event.
     */
    back (e) {
        let navigateTo = 'change_temporary_password';

        e && e.preventDefault();

        if (LF.StudyDesign.askSecurityQuestion && localStorage.getItem('Reset_Password_Secret_Question')) {
            navigateTo = 'reset_secret_question';
        } else if (localStorage.getItem('Reset_Password')) {
            navigateTo = 'change_temporary_password';
        }

        this.navigate(navigateTo, true);
    }
}
