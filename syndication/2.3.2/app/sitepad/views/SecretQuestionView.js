import PageView from 'core/views/PageView';
import ConfirmView from 'core/views/ConfirmView';
import Data from 'core/Data';
import CurrentContext from 'core/CurrentContext';
import COOL from 'core/COOL';
import { MessageRepo } from 'core/Notify';

export default class SecretQuestionView extends PageView {

    constructor (options) {
        super(options);

        /**
         * @property {Object<string,string>} events - The view's events.
         * @readonly
         */
        this.events = {
            // Tap event for going back
            'click #back': 'back',

            // Input event for secret answer
            'input #secret_answer': 'validate',
            'click #next': 'next',
            'click #cancel': 'cancel',

            // Change event for secret question drop down list
            'change #secret_question': 'clearSecretAnswer'
        };

        /**
         * @property {Object<string,string>} templateStrings - Strings to fetch in the render function.
         */
        this.templateStrings = {
            title: 'SECRET_QUESTION_ANSWER',
            legend: 'SECRET_QUESTION_HELP_TEXT',
            secretAnswer: 'SECRET_ANSWER',
            secretQuestion: 'SECRET_QUESTION',
            patientID: CurrentContext().get('user').isSubjectUser() ? 'PATIENT_ID' : 'USERNAME',
            back: 'BACK',
            next: 'NEXT',
            cancel: 'CANCEL',
            popup_title: 'CANCEL_PERMANENT_PASSWORD_TITLE',
            popup_text: 'CANCEL_PERMANENT_PASSWORD_TEXT',
            ok: 'OK',
            yes: 'YES',
            no: 'NO',
            settings: 'SETTINGS',
            help: 'HELP',
            header: 'APPLICATION_HEADER'
        };
    }

    /**
     * @property {string} id - The unique id of the root element.
     * @readonly
     * @default 'secret-question-page'
     */
    get id () {
        return 'secret-question-page';
    }

    /**
     * @property {string} template - Id of template to render
     * @readonly
     * @default '#secret-question-template'
     */
    get template () {
        return '#secret-question-template';
    }

    /**
     * @property {Object<string,string>} selectors - A list of selectors to populate.
     */
    get selectors () {
        return {
            secretQuestion: '#secret_question',
            secretAnswer: '#secret_answer',
            next: '#next'
        };
    }

    /**
     * Render the view to the DOM.
     * example this.render();
     * @returns {Q.Promise<void>}
     */
    render () {
        let user = CurrentContext().get('user').get('username'),
            questions = LF.StudyDesign.securityQuestions;

        _(questions).each(value => {
            if (value.display) {
                this.templateStrings[value.text] = value.text;
            }
        });

        return this.buildHTML({
            user: user
        }, true).then((translated) => {
            _(questions).each(value => {
                if (value.display) {
                    this.$('#secret_question').append(`'<option value="${value.key}">${translated[value.text]}</option>`);
                }
            });

            this.$secretQuestion.select2({
                minimumResultsForSearch: Infinity,
                width: '100%'
            });
            this.buildSelectors();
            this.delegateEvents();
        });
    }

    /**
     * Clears the secret answer field and remove icons.
     */
    clearSecretAnswer () {
        this.$secretAnswer.val('');
        this.clearInputState(this.$secretAnswer);
        this.$next.attr('disabled', 'disabled');
    }

    /**
     * Validates the secret answer field.
     * @return {boolean} Returns true if secret answer is correct, otherwise false.
     */
    validatePage () {
        let codeFormat = new RegExp(LF.StudyDesign.answerFormat || '.{2,}');

        if (codeFormat.test(this.$secretAnswer.val())) {
            this.enableButton(this.$next);
            this.inputSuccess(this.$secretAnswer);
            return true;
        } else {
            this.disableButton(this.$next);
            this.inputError(this.$secretAnswer);
            return false;
        }
    }

    /**
     * Validate the form.
     */
    validate () {
        this.validatePage();
    }

    /**
     * Submits the form
     * @param {(MouseEvent|TouchEvent)} e - A mouse or touch event.
     */
    next (e) {
        e.preventDefault();

        let newRole = LF.StudyDesign.roles.findWhere({
            id: CurrentContext().role
        });

        COOL.getClass('Utilities').isOnline()
        .then((isOnline) => {
            if (!isOnline && !newRole.canAddOffline()) {
                MessageRepo.display(MessageRepo.Banner.CONNECTION_REQUIRED, { delay: 500 })
                .done();

                return;
            }

            if (this.validatePage()) {
                Data.NewSecretQuestionParams = {
                    secretQuestion: this.$secretQuestion.val(),
                    secretAnswer: hex_sha512(this.$secretAnswer.val())
                };

                this.navigate('account_configured', true);
            }
        })
        .done();
    }


    /**
     * Navigates back to the change temporary password screen
     * @param {(MouseEvent|TouchEvent)} e - A mouse or touch event.
     */
    back (e) {
        e.preventDefault();
        this.navigate('change_temporary_password', true);
    }

    /**
     * Cancel the form.
     */
    cancel () {
        this.i18n({
            header: this.templateStrings.popup_title,
            body: this.templateStrings.popup_text,
            cancel: this.templateStrings.no,
            ok: this.templateStrings.yes
        }).then((res) => {
            let modal = new ConfirmView();

            // @TODO SCREENSHOT-SITEPAD
            modal.show(res).then(() => {
                LF.Data.NewSecretQuestionParams = null;
                LF.Data.NewPasswordParams = null;

                localStorage.removeItem('Reset_Password_Secret_Question');

                this.navigate('login', true);
            }, $.noop)
            .done();
        })
        .done();
    }
}
