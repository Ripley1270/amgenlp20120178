import ELF from 'core/ELF';
import PageView from 'core/views/PageView';
import VisitTableView from './VisitTableView';
import Logger from 'core/Logger.js';
import Data from 'core/Data';
import Sites from 'core/collections/Sites';
import Subject from 'core/models/Subject';
import { MessageRepo } from 'core/Notify';
import { getCurrentProtocol } from 'core/Helpers';

// Logger is defined, but not yet used in the class.
// eslint-disable-next-line no-unused-vars
let logger = new Logger('VisitGatewayView');

/**
 * Manage subject's visits.
 * @class VisitGatewayView
 * @extends PageView
 */
export default class VisitGatewayView extends PageView {
    constructor (options) {
        super(options);

        /**
         * @property {Object} events - A list of DOM events.
         * @readonly
         */
        this.events = {
            'click #back': 'back',
            'click #transmit': 'transmitHandler',
            'click #start-visit': _.debounce(() => {
                this.startVisit().done();
            }, 2000, true),
            'click #skip-visit': _.debounce(() => {
                this.skipVisit().done();
            }, 2000, true)
        };

        /**
         * @property {number} subjectId - The ID of the subject the visits are displayed for.
         */
        this.subjectId = options.subjectId;

        /**
         * @property {Object} templateStrings - A list of resource keys to translate and render to the UI.
         */
        this.templateStrings = {
            header: 'APPLICATION_HEADER',
            site: 'SITE',
            reports: 'REPORTS_AND_FORMS',
            transmissions: 'PENDING_TRANSMISSIONS',
            patient: 'PATIENT',
            initials: 'INITIALS',
            active: 'ACTIVE',
            editPatient: 'EDIT_PATIENT',
            deactivatePatient: 'DEACTIVATE_PATIENT',
            resetPin: 'RESET_PIN',
            manageUsers: 'MANAGE_USERS',
            addPatient: 'ADD_PATIENT',
            settings: 'SETTINGS',
            logout: 'LOGOUT',
            help: 'HELP',
            sync: 'SYNC',
            back: 'BACK',
            startVisit: 'START_VISIT',
            skipVisit: 'SKIP_VISIT',
            visits: 'VISITS',
            visitName: 'VISIT_NAME',
            status: 'STATUS',
            statusDate: 'STATUS_DATE',
            available: 'AVAILABLE',
            inProgress: 'IN_PROGRESS',
            skipped: 'SKIPPED',
            completed: 'COMPLETED',
            incomplete: 'INCOMPLETE',
            notAvailable: 'NOT_AVAILABLE',
            sponsor: 'SPONSOR',
            studyVersion: 'STUDY_VERSION',
            protocol: 'PROTOCOL'
        };
    }

    /**
     * @property {string} id - The ID of the view.
     * @readonly
     * @default 'visit-gateway'
     */
    get id () {
        return 'visit-gateway';
    }

    /**
     * @property {Object} selectors - A list of selectors to populate.
     */
    get selectors () {
        return {
            pendingTransmission: '#pending-transmission-count',
            startVisit: '#start-visit',
            skipVisit: '#skip-visit',
            transmit: '#transmit',
            wrapper: '.table-wrapper'
        };
    }

    /**
     * @property {string} template - A selector that points to the template for the view.
     * @readonly
     * @default '#visit-gateway-template'
     */
    get template () {
        return '#visit-gateway-template';
    }

    /**
     * Resolve any dependencies required for the view to render.
     * @returns {Q.Promise<void>}
     */
    resolve () {
        // Get the subject by ID, which is passed into the constructor.
        let getSubject = () => {
            let subject = new Subject({ id: this.subjectId });

            return subject.fetch()
            .then(() => {
                if (Object.keys(subject.attributes).length > 1) {
                    return subject;
                } else {
                    return undefined;
                }
            });
        };

        return Q.all([Sites.fetchFirstEntry(), getSubject()])
        .spread((site, subject) => {
            this.site = site;
            this.subject = subject;
            LF.DynamicText.subject = subject;
        });
    }

    /**
     * Render the view to the DOM.
     * @example this.render();
     * @returns {Q.Promise<void>}
     */
    render () {
        let user = LF.security.activeUser,
            role = LF.security.getUserRole();

        if (this.subject) {
            this.templateStrings.role = { namespace: 'STUDY', key: role.get('displayName') };

            return this.buildHTML({
                username: user.get('username'),
                siteCode: this.site.get('siteCode'),
                patientNumber: this.subject.get('subject_id'),
                patientInitials: this.subject.get('initials'),
                sponsorVal: LF.StudyDesign.clientName,
                protocolVal: getCurrentProtocol(this.subject),
                studyVersionVal: LF.StudyDesign.studyVersion
            }, true)
            .then(translations => this.renderVisits(translations))
            .then(() => this.getPendingReportCount())
            .then(count => {
                this.$pendingTransmission.html(count);
            });
        } else {
            return Q()
            .then(() => {
                this.navigate('home');
            })
            .then(() => this.notify({
                dialog: MessageRepo.Dialog && MessageRepo.Dialog.SUBJECT_DELETED
            }));
        }
    }

    /**
     * Render the visits to the DOM.
     * @param {Object} translations - Translated strings passed into the table.
     * @returns {Q.Promise<void>}
     */
    renderVisits (translations) {
        this.table = new VisitTableView({
            rows: [{
                header: 'VISIT_NAME',
                property: 'name'
            }, {
                header: 'STATUS',
                property: 'status'
            }, {
                header: 'STATUS_DATE',
                property: 'statusDate'
            }],
            translations,
            subject: this.subject
        });

        this.listenTo(this.table, 'select', this.select);
        this.listenTo(this.table, 'deselect', this.deselect);

        return this.table.render()
        .then(() => {
            this.$wrapper.append(this.table.$el);
            this.fetchTableData().done();
        });
    }

    /**
     * Fetches the table data.
     * @returns {Q.Promise<void>}
     */
    fetchTableData () {
        // While we care about the table rendering to the DOM in the returned promise,
        // the table's rows should populate individually.
        return this.table.fetch();
    }

    /**
     * Callback on VisitRowView select action.
     * @param {VisitRow} visitRow - The selected VisitRow.
     */
    select (visitRow) {
        let visitType = visitRow.get('visit').get('visitType'),
            visitSkippableByDesign = visitRow.get('visit').get('skippable') !== false;

		this.selectedRow = visitRow;
		this.selected = visitRow.get('visit');
		this.userVisit = visitRow.get('userVisit');

        if (visitType === 'container') {
            this.enableButton(this.$startVisit);
            this.disableButton(this.$skipVisit);
        } else if (this.userVisit != null) {
            let visitStatus = this.userVisit.get('state');

            if (visitStatus === LF.VisitStates.COMPLETED ||
                visitStatus === LF.VisitStates.SKIPPED ||
                visitStatus === LF.VisitStates.INCOMPLETE ||
                visitStatus === LF.VisitStates.NOT_AVAILABLE) {
                this.disableButton(this.$startVisit);
                this.disableButton(this.$skipVisit);
            } else if (!visitSkippableByDesign) {
                this.enableButton(this.$startVisit);
                this.disableButton(this.$skipVisit);
            } else {
                this.enableButton(this.$startVisit);
                this.enableButton(this.$skipVisit);
            }
        } else if (!visitSkippableByDesign) {
            this.enableButton(this.$startVisit);
            this.disableButton(this.$skipVisit);
        } else {
            this.enableButton(this.$startVisit);
            this.enableButton(this.$skipVisit);
        }
    }

    /**
     * Callback on VisitRowView deselect action.
     */
    deselect () {
        delete this.selected;
        delete this.selectedRow;

        this.disableButton(this.$startVisit);
        this.disableButton(this.$skipVisit);
    }

    /**
     * Triggered when the user clicks #start-visit button.
     * @returns {Q.Promise<void>}
     */
    startVisit () {
        let activeVisit = this.table.getActiveVisit() ? this.table.getActiveVisit().get('visit') : null;

        this.disableButton(this.$startVisit);
        this.disableButton(this.$skipVisit);

        Data.skippedVisits = [];
        Data.site = this.site;

        let selectedVisitType = this.selected.get('visitType');

        return ELF.trigger('VISITGATEWAY:BeforeStartVisit', {subject: this.subject, visit: this.selected, userVisit: this.userVisit}, this)
        .then((res) => {
            if (!res.preventDefault) {

                if (activeVisit && activeVisit.get('id') !== this.selected.get('id') &&
                    (activeVisit.get('visitType') === 'unscheduled' && selectedVisitType === 'ordered' || selectedVisitType === 'unscheduled')) {

                    if (selectedVisitType !== 'unscheduled') {
                        this.table.checkSkippedVisits('skipTo');
                    }

                    Data.skippedVisits.push(activeVisit);

                    this.skipConfirmView(this.selected)
                    .finally(() => {
                        this.select(this.selectedRow);
                    });
                } else if (selectedVisitType === 'ordered' && this.table.checkSkippedVisits('skipTo')) {
                    this.skipConfirmView(this.selected)
                    .finally(() => {
                        this.select(this.selectedRow);
                    });
                } else {
                    this.navigate('dashboard', true, {
                        subject: this.subject,
                        visit: this.selected,
                        disableCheckVisitExpired: true
                    });
                }
            } else {
                this.enableButton(this.$startVisit);
                this.enableButton(this.$skipVisit);
            }
        });
    }

    /**
     * Opens a modal to confirm skipping the selected visit.
     * @param {Visit} visit - The selected visit.
     * @returns {Q.Promise<void>}
     */
    skipConfirmView (visit) {
        let { Dialog, display } = MessageRepo;

        return display(Dialog && Dialog.CONFIRM_SKIP_VISIT)
        .then(() => {
            this.navigate('skip-visits', true, {
                subject: this.subject,
                visit
            });
        }, () => {
            //clear skipped visits on popup cancel
            Data.skippedVisits = undefined;
        });
    }

    /**
     * Triggered when the user clicks #skip-visit button.
     * @returns {Q.Promise<void>}
     */
    skipVisit () {
        let activeVisit = this.table.getActiveVisit() ? this.table.getActiveVisit().get('visit') : null,
            selectedVisitType = this.selected.get('visitType');

        this.disableButton(this.$startVisit);
        this.disableButton(this.$skipVisit);

        Data.site = this.site;
        Data.user = LF.security.activeUser;
        Data.skippedVisits = [];

        if (activeVisit && activeVisit.get('visitType') === 'ordered' && selectedVisitType === 'unscheduled') {
            Data.skippedVisits.push(activeVisit);
        }

        if (selectedVisitType === 'ordered') {
            this.table.checkSkippedVisits('skipOver');
        } else {
            Data.skippedVisits.push(this.selected);
        }

        if (activeVisit && activeVisit.get('visitType') === 'unscheduled' && activeVisit.get('id') !== this.selected.get('id')) {
            Data.skippedVisits.push(activeVisit);
        }

        return this.skipConfirmView().finally(() => {
            this.select(this.selectedRow);
        });
    }

    /**
     * Click handler for transmit.
     */
    transmitHandler () {
        this.transmit().done();
    }

    /**
     * Calls the transmit rule for this view.
     * @returns {Q.Promise<void>}
     */
    triggerTransmitRule () {
        LF.security.pauseSessionTimeOut();

        return ELF.trigger('VISITGATEWAY:Transmit', { subject: this.subject }, this)
        .then((evt) => {
            if (evt && evt.preventDefault) {
                return;
            }
            LF.security.restartSessionTimeOut();

            // DE16139 - Reload the view to ensure all data is up to date.
            this.reload();
        });
    }

    /*
     * Trigger VISITGATEWAY:Transmit
     * @returns {Q.Promise<void>}
     */
    transmit () {
        return this.triggerTransmitRule();
    }

    /**
     * Triggered when the user clicks #back button
     */
    back () {
        this.navigate('home');
    }
}
