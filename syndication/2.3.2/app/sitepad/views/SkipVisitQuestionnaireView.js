import SitePadQuestionnaireView from './SitePadQuestionnaireView';
import Logger from 'core/Logger';
import UserVisit from '../models/UserVisit.js';
import UserReports from '../collections/UserReports.js';
import UserVisits from '../collections/UserVisits.js';
import NotifyView from 'core/views/NotifyView';
import * as dateTimeUtil from 'core/DateTimeUtil';
import {createEndVisitReport} from 'sitepad/visits/visitUtils';
import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';

let logger = new Logger('SkipVisitQuestionnaireView');

export default class SkipVisitQuestionnaireView extends SitePadQuestionnaireView {
    constructor (options) {
        this.notify = new NotifyView();
        super(options);
    }

    /**
     * onCancel
     * Overrides the onCancel function of the BaseQuestionnaireView
     * @return {Q.Promise<void>} resolved when complete
     */
    onCancel () {
        this.visit = undefined;
        return super.onCancel();
    }

    /***
     * Saves skip visit report to UserVisits table.
     * @returns {Q.Promise<void>}
     */
    saveSkipVisit () {
        let requests = [],
            subjectKrpt = this.subject.get('krpt'),
            fetchRequests = [
                UserVisits.fetchCollection(),
                UserReports.fetchCollection()
            ];

        return Q.all(fetchRequests)
        .spread((userVisits, userReports) => {
            LF.Data.skippedVisits.forEach((visit) => {
                let visitId = visit.get('id'),
                    studyEventId = visit.get('studyEventId'),
                    visitState = LF.VisitStates.SKIPPED,
                    userVisit,
                    userVisitsRecords = userVisits.findWhere({visitId, subjectKrpt}),
                    userReportsRecords = userReports.findWhere({
                        user_visit_id: visitId,
                        subject_krpt: subjectKrpt
                    }),
                    promise = () => {
                        return Q.Promise((resolve, reject) => {
                            if (!userVisitsRecords) {
                                userVisit = new UserVisit();
                                userVisit.set({
                                    // TODO: Rename ISOLocalTZStamp() to follow coding guidelines.
                                    // eslint-disable-next-line new-cap
                                    dateStarted: new Date().ISOLocalTZStamp()
                                });
                            } else {
                                userVisit = userVisitsRecords;
                            }

                            if (!!userVisitsRecords && !!userReportsRecords) {
                                visitState = LF.VisitStates.INCOMPLETE;
                            }

                            userVisit.set({
                                visitId: visitId,
                                studyEventId: studyEventId,
                                subjectKrpt: subjectKrpt,
                                state: visitState,
                                dateModified: dateTimeUtil.timeStamp(new Date())
                            });

                            userVisit.save()
                            .then(() => {
                                if (visitState !== LF.VisitStates.INCOMPLETE) {
                                    return this.createStartVisitReport(studyEventId, true, userVisit.get('dateStarted'))
                                        .then((sig_id) => userVisit.save({ visitStartSigId: sig_id }));
                                } else {
                                    return Q();
                                }
                            })
                            .delay(1000)
                            .then(() => {
                                let visitStateCode = 3;
                                if (visitState === LF.VisitStates.SKIPPED) {
                                    visitStateCode = 1;
                                } else if (visitState === LF.VisitStates.INCOMPLETE) {
                                    visitStateCode = 2;
                                }

                                let collection = _(this.questionViews).find((view) => {
                                    return view.id === 'SKIP_VISIT_Q_1';
                                }),
                                skipResponse = collection.widget.answers.toJSON()[0].response;

                                return createEndVisitReport({
                                    visitStateCode,
                                    visit,
                                    skippedReason: skipResponse,
                                    visitStartDate: userVisit.get('dateStarted'),
                                    subject: this.subject,
                                    instanceOrdinal: BaseQuestionnaireView.getInstanceOrdinal(),
                                    ink: this.data.ink
                                })
                                .then((res) => userVisit.save({
                                    visitEndSigId: res.sig_id,
                                    dateEnded: res.eventEndDate
                                }));
                            })
                            .delay(1000)
                            .catch((e) => {
                                reject();
                                logger.error(`saveSkipVisit() failed fetching user visit. ${e}`);
                            })
                            .done(resolve);
                        });
                    };

                requests.push(promise);
            });

            return requests.reduce(function (prev, cur) {
                return prev.then(cur);
            }, Q());
        });
    }

    /**
     * Saves the skip visit record.
     * @returns {Q.Promise<void>}
     */
    save () {
        logger.traceEnter('skipVisitSave()');
        return this.saveSkipVisit()
        .finally(() => {
            //clear skipped visits after successfully saving
            LF.Data.skippedVisits = undefined;
            logger.traceExit('skipVisitSave()');
        });
    }
}
