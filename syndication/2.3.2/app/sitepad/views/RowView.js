export default class RowView extends Backbone.View {
    constructor (options) {
        super(options);

        /**
         * @property {Object} events - Events bound to the view.
         */
        this.events = { click: 'toggle' };

        /**
         * @property {Backbone.Model<T>} model - The model to display.
         */
        this.model = options.model;
    }

    /**
     * @property {string} tagName - The root tag of the view.
     * @default 'tr'
     */
    get tagName () {
        return 'tr';
    }

    /**
     * Render the row.
     * @returns {Q.Promise<RowView>}
     */
    render () {
        this.delegateEvents();

        return Q().then(() => this);
    }

    /**
     * An alias for LF.strings.display. See {@link core/collections/Languages Languages} for details.
     * @param {Object|string} strings - The translations keys to translate.
     * @param {Function} [callback=$.noop] - A callback function invoked when the strings have been translated.
     * @param {Object} [options] - Options passed into the translation.
     * @returns {Q.Promise<Object>}
     */
    i18n (strings, callback=$.noop, options=undefined) {
        return LF.strings.display.call(LF.strings, strings, callback, options);
    }

    /**
     * Toggle the row's selected state.
     * @param {(ClickEvent|TouchEvent)} evt - A click or touch event.
     */
    toggle (evt) {
        // Select the row, and deselect its siblings.
        this.$el.toggleClass('bg-info');
        $(evt.currentTarget).siblings().removeClass('bg-info');

        if (this.$el.hasClass('bg-info')) {
            this.trigger('select', this.model);
        } else {
            this.trigger('deselect', this.model);
        }
    }
}
