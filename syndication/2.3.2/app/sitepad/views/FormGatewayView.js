import PageView from 'core/views/PageView';
import Data from 'core/Data';
import ELF from 'core/ELF';
import Sites from 'core/collections/Sites';
import CurrentContext from 'core/CurrentContext';
import UserVisits from 'sitepad/collections/UserVisits';
import UserVisit from 'sitepad/models/UserVisit';
import Subject from 'core/models/Subject';
import FormTableView from 'sitepad/views/FormTableView';
import { MessageRepo } from 'core/Notify';
import { isVisitExpired } from 'sitepad/visits/evaluateVisit';
import { createEndVisitReport } from 'sitepad/visits/visitUtils';
import * as DateTimeUtil from 'core/DateTimeUtil';
import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';
import Logger from 'core/Logger';
import { getCurrentProtocol } from 'core/Helpers';

let logger = new Logger('FormGatewayView');

/**
 * Displays available forms for a given Subject & Visit.
 * @class FormGatewayView
 * @extends PageView
 */
export default class FormGatewayView extends PageView {
    constructor (options) {
        super(options);

        /**
         * @property {number} subjectId - The ID of the subject to display forms for.
         */
        this.subjectId = options.subject.get('id');

        /**
         * @property {Visit} visit - The target visit to display forms for.
         */
        this.visit = options.visit;

        LF.DynamicText.subject = options.subject;

        /**
         * @property {Object<string,string>} selectors - A list of selectors to populate.
         */
        this.selectors = {
            wrapper: '.table-wrapper',
            pendingTransmission: '#pending-transmission-count'
        };

        /**
         * @property {Object<string,string>} events - A list of DOM events.
         * @readonly
         */
        this.events = {
            'click #start-form': _.debounce(() => {
                this.start();
            }, 2000, true),
            'click #skip-form': _.debounce(() => {
                this.skip();
            }, 2000, true),
            'click #back': 'back',
            'click #transmit': 'transmit'
        };

        /**
         * @property {Object<string,string>} templateStrings - A list of resource keys to translate and render to the UI.
         */
        this.templateStrings = {
            available: 'AVAILABLE',
            notavailable: 'NOT_AVAILABLE',
            skipped: 'SKIPPED',
            completed: 'COMPLETED',
            back: 'BACK',
            assignedTo: 'ASSIGNED_TO',
            formName: 'FORM_NAME',
            header: 'APPLICATION_HEADER',
            help: 'HELP',
            lAppVersion: 'APP_VERSION',
            menu: 'MENU',
            order: 'ORDER',
            patient: 'PATIENT',
            settings: 'SETTINGS',
            site: 'SITE',
            siteConfirmHeader: 'HAND_DEVICE_SITE_TITLE',
            siteConfirmBody: 'HAND_DEVICE_SITE_BODY',
            skipForm: 'SKIP_FORM',
            startForm: 'START_FORM',
            status: 'STATUS',
            sync: 'SYNC',
            subjectConfirmHeader: 'HAND_DEVICE_SUBJECT_TITLE',
            subjectConfirmBody: 'HAND_DEVICE_SUBJECT_BODY',
            cancel: 'CANCEL',
            ok: 'OK',
            logout: 'LOGOUT',
            sponsor: 'SPONSOR',
            studyVersion: 'STUDY_VERSION',
            protocol: 'PROTOCOL'
        };

        /**
         * @property {Boolean} checkVisitExpired - flag to determine if visit expire check is necessary.
         */
        this.checkVisitExpired = !options.disableCheckVisitExpired;
    }

    /**
     * @property {string} id - The ID of the view.
     * @readonly
     * @default 'begin-visit'
     */
    get id () {
        return 'begin-visit';
    }

    /**
     * @property {string} template - A selector that points to the template for the view.
     * @readonly
     * @default '#form-gateway-template'
     */
    get template () {
        return '#form-gateway-template';
    }

    /**
     * Resolve any dependencies required for the view to render.
     * @returns {Q.Promise<void>}
     */
    resolve () {
        let getSubject = () => {
            let subject = new Subject({ id: this.subjectId });

            return subject.fetch()
            .then(() => {
                if (Object.keys(subject.attributes).length > 1) {
                    return subject;
                } else {
                    return undefined;
                }
            });
        };

        return Q.all([Sites.fetchFirstEntry(), getSubject()])
        .spread((site, subject) => {
            this.site = site;
            this.subject = subject;
        });
    }

    /**
     * Calls the transmit rule for this view.
     * @returns {Q.Promise<void>}
     */
    triggerTransmitRule () {
        LF.security.pauseSessionTimeOut();

        return ELF.trigger('FORMGATEWAY:Transmit', { subject: this.subject }, this)
        .then(evt => {
            if (evt && evt.preventDefault) {
                return;
            }

            LF.security.restartSessionTimeOut();

            // eslint-disable-next-line consistent-return
            return this.resolve()
            .then(() => this.render());

        });
    }

    /*
     * Handles transmission and sync operations
     * @returns {Q.Promise<void>}
     */
    transmit () {
        return this.triggerTransmitRule();
    }

    /**
     * Render the view to the DOM.
     * example this.render();
     * @returns {Q.Promise<void>}
     */
    render () {
        let user = LF.security.activeUser,
            subject = this.subject,
            role = CurrentContext().get('role');

        if (subject) {
            this.templateStrings.role = {
                namespace: 'STUDY',
                key: role.get('displayName')
            };

            return this.buildHTML({
                username: user.get('username'),
                patientInitials: subject.get('initials'),
                patientNumber: subject.get('subject_id'),
                siteCode: this.site.get('siteCode'),
                visitName: this.visit.get('name'),
                sponsorVal: LF.StudyDesign.clientName,
                protocolVal: getCurrentProtocol(subject),
                studyVersionVal: LF.StudyDesign.studyVersion
            }, true)
            .then(() => {
                if (user.get('role') === 'subject') {
                    this.hideElement('#site-user-image,#site-user-info,#site-user-visits');
                    this.showElement('#patient-image,#patient-info,#patient-visits');
                }

                return this.checkVisitStatus()
                .then(res => {
                    if (!res) {
                        return this.renderTable();
                    }
                    // There is no need for an else. checkVisitStatus will navigate away from the view.
                    // We still need a return value for jsdoc consistent-return.
                    return Q();
                });
            })
            .then(() => this.getPendingReportCount())
            .then(count => {
                this.$pendingTransmission.html(count);
            });
        } else {
            let { display, Dialog } = MessageRepo;

            return display(Dialog &&
                    (LF.security.activeUser.isSubjectUser() ? Dialog.SUBJECT_DELETED_PATIENT : Dialog.SUBJECT_DELETED)
            )
            .finally(() => {
                LF.security.activeUser.isSubjectUser() ? this.navigate('login') : this.navigate('home');
            });
        }
    }

    /**
     * Render the form table to the view.
     * @param {Object} translations - Translated strings to pass into the table for display.
     * @returns {Q.Promise<void>}
     */
    renderTable (translations) {
        this.table = new FormTableView({
            rows: [{
                header: 'ORDER',
                property: 'order',
                comparator: (model) => {
                    let order = parseInt(model.get('order'), 10);

                    // If the order value is not a number (NaN), assign it the max value of a number
                    // so it displays after the ordered visits.
                    if (_.isNaN(order)) {
                        return Number.MAX_VALUE;
                    }

                    return order;
                }
            }, {
                header: 'FORM_NAME',
                property: 'displayName'
            }, {
                header: 'ASSIGNED_TO',
                property: 'assignedTo'
            }, {
                header: 'STATUS',
                property: 'status'
            }],
            translations,
            subject: this.subject,
            visit: this.visit
        });

        this.listenTo(this.table, 'select', this.select);
        this.listenTo(this.table, 'deselect', this.deselect);
        this.listenTo(this.table, 'noFormsAvailable', this.noFormsAvailable);

        return this.table.render()
        .then(() => {
            this.$wrapper.append(this.table.$el);

            // While we care about the table rendering to the DOM in the returned promise,
            // the table's rows should populate individually.
            this.table.fetch().done();
        });
    }

    /**
     * Checks Visit status and displays a proper message to the active user if visit is Completed
     * or Inactive after sync process (Skipped or Incompleted on server side).
     * Also, navigation is handled.
     * @returns {Q.Promise<void>}
     */
    checkVisitStatus () {
        return UserVisits.fetchCollection()
        .then(userVisits => {
            return userVisits.findWhere({
                visitId: this.visit.id,
                subjectKrpt: this.subject.get('krpt')
            });
        })
        .then((userVisit) => {
            let visitState = userVisit ? userVisit.get('state') : undefined,
                expired = userVisit ? userVisit.get('expired') : 'false';
            if (visitState && (visitState === LF.VisitStates.COMPLETED ||
                visitState === LF.VisitStates.INCOMPLETE ||
                visitState === LF.VisitStates.SKIPPED)) {
                let msgKey = (visitState === LF.VisitStates.INCOMPLETE || visitState === LF.VisitStates.SKIPPED) ? 'VISIT_INACTIVE' : 'VISIT_COMPLETED';

                if (LF.security.activeUser.isSubjectUser()) {
                    msgKey = (visitState === LF.VisitStates.COMPLETED) ? 'VISIT_COMPLETED_SUBJECT' : 'VISIT_INACTIVE_SUBJECT';
                }

                if (expired === 'true') {
                    msgKey = LF.security.activeUser.isSubjectUser() ? 'VISIT_EXPIRED_SUBJECT' : 'VISIT_EXPIRED';
                }

                return this.displayMessageAndNavigate(msgKey, expired);
            } else {
                if (!this.checkVisitExpired || !this.visit.get('expiration')) {
                    this.checkVisitExpired = true;
                    return Q(false);
                }
                return isVisitExpired(this.visit, userVisit, this.subject).then((expired) => {
                    if (!expired) {
                        return Q(false);
                    }
                    let currentDate = new Date();
                    if (userVisit == null) {
                        userVisit = new UserVisit();
                        userVisit.set({
                            // eslint-disable-next-line new-cap
                            dateStarted: currentDate.ISOLocalTZStamp()
                        });
                    }
                    userVisit.set({
                        visitId: this.visit.get('id'),
                        studyEventId: this.visit.get('studyEventId'),
                        subjectKrpt: this.subject.get('krpt'),
                        state: LF.VisitStates.INCOMPLETE,
                        dateModified: DateTimeUtil.timeStamp(currentDate),
                        expired: 'true'
                    });

                    return userVisit.save()
                    .then(() => {
                        return createEndVisitReport({
                            visitStateCode: 2,
                            visit: this.visit,
                            skippedReason: '0',
                            visitStartDate: userVisit.get('dateStarted'),
                            subject: this.subject,
                            instanceOrdinal: BaseQuestionnaireView.getInstanceOrdinal(),
                            ink: null,
                            visitExpired: true
                        });
                    })
                    .then((res) => userVisit.save({
                        visitEndSigId: res.sig_id,
                        dateEnded: res.eventEndDate
                    }))
                    .then(() => {
                        let dialogKey = LF.security.activeUser.isSubjectUser() ? 'VISIT_EXPIRED_SUBJECT' : 'VISIT_EXPIRED';
                        return this.displayMessageAndNavigate(dialogKey);
                    });
                });
            }
        });
    }

    /**
     * Displays message and navigates away.
     * @param {string} messageKey - key that identifies MessageRepo.Dialog
     * @param {string} [visitExpired=’false’]- true if current visit expired
     * @return {Q.Promise<any>} resolves based on registered message.
     */
    displayMessageAndNavigate (messageKey, visitExpired = 'false') {
        if (visitExpired === 'true') {
            logger.operational(`Visit with id = "${this.visit.get('id')}" expired for subject ${this.subject.get('subject_id')}. Message displayed to the user: ${LF.security.activeUser.get('username')}.`);
        }
        return MessageRepo.display(MessageRepo.Dialog && MessageRepo.Dialog[messageKey])
        // We don't care if the notification is resolved or rejected, we will still navigate away.
        .finally(() => {
            if (LF.security.activeUser.isSubjectUser()) {
                this.navigateToDashboard();
            }  else {
                this.navigate(`visits/${this.subject.get('id')}`, true, { visit: this.visit });
            }
        });
    }

    /**
     * Triggered when a form row is selected.
     * @param {FormRow} formRow - The selected FormRow.
     */
    select (formRow) {
        this.selected = formRow;

        // If the selected questionnaire is available, enable the start and skip buttons.
        if (formRow.get('status') === 'available') {
            this.enableButton('#start-form, #skip-form');
        } else {
            // Otherwise, disable them.
            this.deselect();
        }
    }

    /**
     * Start a form.
     * @returns {Q.Promise<void>}
     * @example let request = this.start();
     */
    start () {
        let id = this.selected.get('id');

        // Trigger an OpenQuestionnaire event via ELF.
        return ELF.trigger(`DASHBOARD:OpenQuestionnaire/${id}`, { questionnaire: this.selected }, this)
        .then((res) => {
            // If not told to prevent default behavior, open the questionnaire.
            res.preventDefault || this.openQuestionnaire(id);
        });
    }

    /**
     * Skip a form.
     * @example this.skip();
     */
    skip () {
        //TODO: skip a form
    }

    /**
     * Open a form.
     * @param {String} questionnaireId ID of the questionnaire.
     * @returns {Q.Promise<void>}
     */
    openQuestionnaire (questionnaireId) {
        let id = questionnaireId || this.selected.get('id');

        Data.Questionnaire = {};

        //DE12483: Delay opening questionnaire to prevent the issue of questionnaire screen display
        return Q.delay(150)
        .then(() => this.navigate(`questionnaire/${id}`, true, {
            subject: this.subject,
            visit: this.visit
        }));
    }

    /**
     * Triggered when the form is deselected.
     * @example this.deselect();
     */
    deselect () {
        delete this.selected;

        this.disableButton('#start-form, #skip-form');
    }

    /**
     * Navigates back.
     * @example this.back();
     */
    back () {
        if (LF.security.activeUser.isSubjectUser()) {
            this.navigateToDashboard();
        } else {
            this.navigate(`visits/${this.subject.get('id')}`, true, { visit: this.visit });
        }
    }

    /**
     * Checks if all reports are completed.
     * @returns {Q.Promise<void>}
     * @example this.noFormsAvailable();
     */
    noFormsAvailable () {
        let { display, Dialog } = MessageRepo;

        return display(Dialog && Dialog.VISIT_COMPLETED_SUBJECT)
        .then(() => this.navigateToDashboard());
    }

    /**
     * This function is navigating to the dashboard, but saying you want the roles associated with the permission of
     * diaryBackoutRoles, to be the next set of Roles you login in with. We should fix this when we do permissions.
     */
    navigateToDashboard () {
        this.navigate('dashboard/diaryBackoutRoles', true, {
            subject: this.subject,
            visit: this.visit
        });
    }
}
