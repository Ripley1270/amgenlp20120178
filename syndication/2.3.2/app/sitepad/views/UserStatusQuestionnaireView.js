import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';
import User from 'core/models/User';

export default class UserStatusQuestionnaireView extends BaseQuestionnaireView {
    constructor (options) {
        super(options);

        this.user = new User({ id: options.userId });
    }

    /**
     * Resolves any dependencies required for the view to render.
     * @returns {Q.Promise<void>}
     */
    resolve () {
        return this.user.fetch().then(() => {
            // We are adding a selectedUser property to the DynamicText object.
            // This makes it so we have access to the user from within dynamic text functions.
            LF.DynamicText.selectedUser = this.user;

            return super.resolve();
        });
    }

    /**
     * Determines if the target user record can be updated while offline.
     * @returns {Q.Promise<boolean>}
     */
    canSyncOffline () {
        let role = LF.StudyDesign.roles.findWhere({ id: this.user.get('role') });

        // This method can be called as an ELF expression, so we need to return a promise.
        return Q(role.canAddOffline());
    }
}
