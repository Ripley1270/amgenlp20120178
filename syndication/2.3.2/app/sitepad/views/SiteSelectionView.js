import PageView from 'core/views/PageView';
import Data from 'core/Data';
import UnlockCode from 'core/classes/UnlockCode';
import WebService from 'core/classes/WebService';
import Sites from 'core/collections/Sites';
import { MessageRepo } from 'core/Notify';
import * as lStorage from 'core/lStorage';
import * as FileUtil from 'core/FileUtil.js';
import State from 'core/classes/State';
import ELF from 'core/ELF';
import Logger from 'core/Logger';
import COOL from 'core/COOL';
import { getCurrentProtocol } from 'core/Helpers';

let logger = new Logger('SiteSelectionView');

const _evt = { preventDefault: $.noop };

export default class SiteSelectionView extends PageView {
    constructor (options) {
        super(options);

        /**
         * @property {Object} events - A list of DOM events.
         * @readonly
         */
        this.events = {
            'click #next': 'nextHandler',
            'click #back': 'backHandler',
            'change #site': 'siteChanged',
            'input input#unlock-code': 'validate'
        };

        /**
         * @property {Object.<string, string>} templateStrings - A list of resource keys to translate and render to the UI.
         */
        this.templateStrings = {
            header: 'APPLICATION_HEADER',
            title: 'SITE_SELECTION',
            tagline: 'CHOOSE_A_SITE',
            next: 'NEXT',
            back: 'BACK',
            site: 'SITE',
            unlockCode: 'UNLOCK_CODE',
            enterUnlockCode: 'ENTER_UNLOCK_CODE',
            helpText: 'UNLOCK_CODE_HELP',
            siteNumber: 'SELECT_SITE_NUMBER',
            noResults: 'NO_RESULTS_FOUND',
            siteListError: 'UNABLE_TO_GET_SITE_LIST'
        };

        /**
         * @property {WebService} service - An instance of the WebService class.
         */
        this.service = COOL.new('WebService', WebService);

        /**
         * @property {Sites} sites - A collection of Sites to render to the DOM.
         */
        this.sites = new Sites();

        /**
         * @property {string} mode - The current mode of the sitepad (provision, depot, trainer...)
         */
        this.mode = lStorage.getItem('mode');

        /**
         * @property {State} state - A State instance.
         */
        this.state = new State();

        /**
         * @property {string} studyDbName - The defined study database name from a prior screen.
         */
        this.studyDbName = lStorage.getItem('studyDbName');
    }

    /**
     * @property {string} id - The ID of the view.
     * @readonly
     * @default 'site-selection-view'
     */
    get id () {
        return 'site-selection-view';
    }

    /**
     * @property {string} template - A selector that points to the template for the view.
     * @readonly
     * @default '#site-selection-tpl'
     */
    get template () {
        return '#site-selection-tpl';
    }

    /**
     * @property {Object.<string, string>} selectors - A list of selectors to populate.
     */
    get selectors () {
        return {
            site: '#site',
            unlockCode: '#unlock-code',
            btnBack: 'button#back',
            btnNext: 'button#next',
            form: '#siteSelectionForm'
        };
    }

    /**
     * Render the view.
     * @returns {Q.Promise<void>}
     * @example
     * this.render().then({ ... });
     */
    render () {
        let currentProtocol = getCurrentProtocol();

        return this.buildHTML({
            study: `${LF.StudyDesign.clientName} ${currentProtocol}`,
            key: this.key
        }, true)
        .then(translations => {
            this.initForm();

            return this.spinner.show()
            .then(() => {
                // Query for sites, and then render them to the page.
                // @todo query for sites under the correct study/stage
                return this.service.getSites(this.studyDbName);
            })
            .then(res => {
                this.renderSites(res);
            }, error => {
                MessageRepo.display(MessageRepo.Banner[this.templateStrings.siteListError], {
                    //DE14133 fix
                    timeout: false
                });

                logger.error('Getting site list failed!', error);
            })
            .finally(() => {
                this.$site.select2({
                    placeholder: translations.siteNumber,
                    language: { noResults: () => translations.noResults }
                });

                return this.spinner.hide();
            });
        });
    }

    /**
     * Prevents the form from being submitted by Enter.
     * @example
     * this.initForm();
     */
    initForm () {
        this.$form.submit(e => {
            e.preventDefault();
            return false;
        });
    }

    /**
     * Render the list of sites to the DOM
     * @param {Object[]} sites - An array of sites to add.
     * @param {string} sites[].krdom - The site's KrDOM
     * @param {string} sites[].site_code - The site's code.
     * @param {string} sites[].study_name - The name of the study the site belongs to.
     * @param {number} sites[].site_id - The ID of the site.
     * @param {string} sites[].hash - A hash value.
     * @example
     * this.renderSites([{
           krdom: 'DOM.3191309',
           site_code: '006',
           study_name: 'DemoStudy',
           hash: '0xTMNT',
           site_id: 1
     * }]);
     */
    renderSites (sites) {
        this.sites.add(sites.map((site) => {
            return {
                id: site.krdom,
                site_code: site.site_code,

                // TODO - I can't find any evidence of study_name, siteCode, hash or site_id being
                // received from the web service.  Are these POC values, or is there intent to implement
                // at a later date?
                siteCode: site.siteCode,
                study_name: site.study_name,
                hash: site.hash,
                site_id: site.site_id
            };
        }));
        this.sites.forEach((site) => this.addSite(site));
    }

    /**
     * Add an individual site to the DOM.
     * @param {Site} site - The site model to add.
     * @example
     * let site = new Site({ siteCode: '222' });
     * this.addSite(site);
     */
    addSite (site) {
        let el = `<option value="${site.get('siteCode')}">${site.get('siteCode')}</option>`;

        this.$site.append(el);
    }

    /**
     * Click event handler for the Back button.
     * @param {(MouseEvent|TouchEvent)} [evt=_evt] - A mouse or touch event.
     * @example
     * this.events = { 'click #back': 'backHandler' };
     */
    backHandler (evt = _evt) {
        evt.preventDefault();

        // We're using a handler to invoke SiteSelectionView.back() so .done() can be called on
        // the returned promise.  This prevents any errors from being swallowed
        // and the error 'Detected 1 unhandled rejections of Q promises' from being thrown.
        this.back().done();
    }

    /**
     * Navigate back to the previous view.
     * @returns {Q.Promise<void>}
     */
    back () {
        switch (this.mode) {
            case 'provision':
            case 'trainer':
                this.state.set('NEW');
                this.navigate('modeSelection');
                break;
            case 'depot':
                this.state.set('LANGUAGE_SELECTION');
                this.navigate('languageSelection');
                break;
        }

        // Returned promise is for testability alone.
        return ELF.trigger('SITESELECTION:Back', { product: 'sitepad' }, this);
    }

    /**
     * Fired when a site is selected.
     * @param {ChangeEvent} [evt=_evt] - A input change event.
     * @example
     * this.siteChanged(evt);
     */
    siteChanged (evt = _evt) {
        let value = this.$site.val();

        this.$unlockCode.val('');
        this[value ? 'enableElement' : 'disableElement'](this.$unlockCode);
        this.validate(evt);
        this.clearInputState(this.$unlockCode);
    }

    /**
     * Click event handler for the Next button.
     * @param {(MouseEvent|TouchEvent)} [evt=_evt] - A mouse or touch event.
     * @example
     * this.events = { 'click #next': 'nextHandler' };
     */
    nextHandler (evt = _evt) {
        evt.preventDefault();

        // We're using a handler to invoke SiteSelectionView.next() so .done() can be called on
        // the returned promise.  This prevents any errors from being swallowed
        // and the error 'Detected 1 unhandled rejections of Q promises' from being thrown.
        this.next().done();
    }

    /**
     * Navigate to the next view.
     * @returns {Q.Promise<void>}
     * @example
     * this.next().then(() => { ... });
     */
    next () {
        let site,
            unlockCode;

        let getDeviceUUID = () => {
            if (LF.Wrapper.platform === 'windows') {
                let ASHWID = Windows.System.Profile.HardwareIdentification.getPackageSpecificToken(null).id;

                return Windows.Storage.Streams.DataReader.fromBuffer(ASHWID).readGuid();
            } else {
                //case when running in browser
                return new Date().getTime();
            }
        };

        let getSerialNumber = () => {
            // @TODO maybe move this to Wrapper.js?
            if (LF.Wrapper.isWindows()) {
                return FileUtil.readFileFromAppFolder('serial.txt');
            }
            // If android, serial number may have already been set on deviceready
            // event fired in Wrapper.js
            let serial = lStorage.getItem('IMEI');
            serial || LF.Wrapper.Utils.deviceSerialNumber().get((serialNum) => {
                lStorage.setItem('IMEI', serialNum);
                serial = serialNum;
            });

            return serial;
        };

        // DE15850 - Incorrect message displayed if no internet connection on SitePad
        // Check connectivity before attempting to register the device.  If offline, notify and then reject.
        let checkConnectivity = () => {
            return COOL.getClass('Utilities').isOnline()
            // eslint-disable-next-line consistent-return
            .then(online => {
                if (!online) {
                    // Display a connection required notification.
                    return this.spinner.hide()
                    .then(() => {
                        return MessageRepo.display(MessageRepo.Dialog.CONNECTION_REQUIRED_ERROR);
                    })
                    // Reject this promise, so the chain doesn't continue.
                    .then(() => {
                        // DE17445 - Lack of an internet connection doesn't warrent an error-level log.
                        logger.warn('An internet connection is required to register the device.');

                        return Q.reject();
                    });
                }
            });
        };

        // Displays an error banner and logs an error
        let handleUnexpectedError = (err) => {
            MessageRepo.display(MessageRepo.Banner.DEVICE_REGISTRATION_FAILURE);

            // DE17445 - If there is an error provided, log it
            err && logger.error('Device registration failed.', err);
        };

        // Registers the device with StudyWorks/Expert.
        let registerDevice = (serialNumber, deviceUUID) => {
            let serial = serialNumber ? serialNumber.replace(/(\r\n|\n|\r)/gm,'') : 'NULL',
                register = {
                    studyName: this.studyDbName,
                    krdom: site.get('site_id'),
                    startupCode: unlockCode,
                    deviceUuid: deviceUUID,
                    imei: serial
                };

            lStorage.setItem('deviceUUID', deviceUUID);
            lStorage.setItem('IMEI', serial);

            return this.service.registerDevice(register);
        };

        let finishRegisterDevice = (res) => {
            // The API token is used as an authorization token in all transmissions onward.
            lStorage.setItem('apiToken', res.apiToken);
            lStorage.setItem('deviceId', res.regDeviceId);
            lStorage.setItem('clientId', res.clientId);

            if (this.mode !== 'provision' && this.mode !== 'trainer') {
                // The end-user just entered the unlock code, so no need to enter it again.
                return ELF.trigger('SETTIMEZONE:Completed', {}, this);
            }

            return this.spinner.hide()
            .then(() => {
                this.state.set('LANGUAGE_SELECTION');

                // Display a success notification that the device setup is complete.
                return MessageRepo.display(MessageRepo.Dialog.SETUP_COMPLETE);
            })
            .then(() => {
                this.navigate('languageSelection');
            });
        };

        let createSite = () => {
            if (!site.get('backupSiteId')) {
                // only works once!!
                site.set('backupSiteId', site.id);
            }

            site.set('site_id', site.get('backupSiteId'));
            site.unset('id', { silent: true });
        };

        unlockCode = this.$unlockCode.val();

        // ucode stands for Unlock Code. Since it is being used as the encryption key
        // in sitepad, it is better to use a rather undescriptive localstorage key.
        lStorage.setItem('ucode', unlockCode);

        site = this.sites.findWhere({ siteCode: this.$site.val() });

        return this.spinner.show()
        .then(() => checkConnectivity())
        .then(() => createSite())
        .then(() => Q.all([getSerialNumber(), getDeviceUUID()]))
        .spread((serial, uuid) => registerDevice(serial, uuid))

        // Fix for DE15832. Save site AFTER successful registration.
        .tap(() => site.save())
        .then(finishRegisterDevice)
        .then(() => {
            Data.site = site;
        })
        .catch(handleUnexpectedError)
        .finally(() => this.spinner.hide());
    }

    /**
     * Compares the entered unlock code with the expected one.
     * @param {string} unlockCode - The unlock code user entered.
     * @returns {boolean} True if codes match, false if not.
     * @example
     * if (this.validateUnlockCode('3151245')) { ... }
     */
    validateUnlockCode (unlockCode) {
        let siteCode = this.$site.val(),

            // Generate the unlock code the user must match.
            validCode = UnlockCode.createStartupUnlockCode(siteCode, this.studyDbName);

        return unlockCode === validCode;
    }

    /**
     * Validates the entered unlock code. Fired upon change.
     * @param {ChangeEvent} [evt=_evt] - An input change event.
     * @example
     * this.validate(evt);
     */
    validate (evt = _evt) {
        let unlockCode = this.$unlockCode.val();

        evt.preventDefault();

        if (unlockCode.length) {
            if (this.validateUnlockCode(unlockCode)) {
                this.inputSuccess(this.$unlockCode);
                this.enableButton(this.$btnNext);
            } else {
                this.inputError(this.$unlockCode);
                this.disableButton(this.$btnNext);
            }
        } else {
            this.clearInputState(this.$unlockCode);
            this.disableButton(this.$btnNext);
        }
    }
}

COOL.add('SiteSelectionView', SiteSelectionView);
