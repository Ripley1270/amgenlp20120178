import Sites from 'core/collections/Sites';
import Transmissions from 'core/collections/Transmissions';

import Logger from 'core/Logger';
import * as lStorage from 'core/lStorage';

import CoreLogin from 'core/views/LoginView';

// Logger is defined, but not yet in use.
// eslint-disable-next-line no-unused-vars
const logger = new Logger('Sitepad/LoginView');

export default class LoginView extends CoreLogin {
    constructor (options) {
        super(options);

        /**
         * @property {string} template - A selector that points to the template for the view.
         * @readonly
         * @default '#login-tpl'
         */
        this.template = '#login-tpl';

        /**
         * @property {Object<string,string>} events - A list of DOM events.
         * @readonly
         */
        this.events = _.defaults({
            'click #sync-button': 'transmit',
            'click .fa-question-circle': 'help',
            'click #settings': 'settings'
        }, this.events);

        /**
         * @property {Object<string,string>} templateStrings - A list of resource keys to translate and render to the UI.
         */
        this.templateStrings = _.defaults({
            header: 'APPLICATION_HEADER',
            login: 'LOGIN',
            help: 'HELP',
            sync: 'SYNC',
            settings: 'SETTINGS'
        }, this.templateStrings);

        /**
         * @property {Sites} sites - A collection of sites.
         */
        this.sites = new Sites();
    }

    //noinspection JSMethodCanBeStatic
    /**
     * @property {string} id - The ID of the view.
     * @readonly
     * @default 'sitepad-login-view'
     */
    get id () {
        return 'login-view';
    }

    //noinspection JSMethodCanBeStatic
    /**
     * @property {Object<string,string>} selectors - A list of selectors to populate.
     */
    get selectors () {
        return _.defaults({
            transmit: '#sync-button',
            pendingTransmission: '#pending-transmission-count'
        }, super.selectors);
    }

    /**
     * Resolve dependencies required for the view to render.
     * @returns {Q.Promise<void>}
     */
    resolve () {
        if (lStorage.getItem('isActivation')) {
            lStorage.removeItem('isActivation');
        }

        return super.resolve()
        .then(() => Q.all([Sites.fetchFirstEntry(), Transmissions.fetchCollection()]))
        .spread((site, transmissions) => {
            this.site = site;
            this.transmissions = transmissions;
        });
    }

    /**
     * Render the view to the DOM.
     * example this.render();
     * @returns {Q.Promise<void>} this
     */
    render () {
        return this.buildHTML({
            siteNumber: this.site.get('siteCode'),
            version: LF.coreVersion
        }, true)
        .then(() => this.getPendingReportCount())
        .then(count => {
            this.$pendingTransmission.html(count);
            if (count > 0) {
                this.enableButton(this.$transmit);
            }
        })
        .then(() => {
            return this.userLoginSetup();
        });
    }

    /**
     * Settings
     */
    settings () { }

    /**
     * Help
     */
    help () { }

    /**
     * Calls the transmit rule for this view.
     * @returns {Q.Promise<void>}
     */
    triggerTransmitRule () {
        LF.security.pauseSessionTimeOut();

        return ELF.trigger('LOGIN:Transmit', {}, this)
        .then(evt => {
            if (evt && evt.preventDefault) {
                return;
            }

            this.resolve()
            .then(() => this.render())
            .done();

            LF.security.restartSessionTimeOut();
        });
    }

    /**
     * Perform a sync with the server
     *  @returns {Q.Promise<void>} this
     */
    transmit () {
        return this.triggerTransmitRule();
    }
}
