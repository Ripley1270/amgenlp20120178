import TableView from 'sitepad/views/TableView';
import UserRowView from './UserRowView';
import Users from 'core/collections/Users';

export default class UserTableView extends TableView {
    constructor (options = {}) {
        super(options);

        /**
         * @property {Users} collection The collection to fetch and render.
         */
        this.collection = new Users();

        let row = options.rows[0];

        // Set the default sort property and comparator.
        this.collection.sortProperty = row.property;
        this.collection.comparator = row.comparator || row.property;
    }

    /**
     * Render a row.
     * @param {Subject} model The subject to render.
     * @returns {Q.Promise<DOMElement>}
     */
    renderRow (model) {
        let view = new UserRowView({ model, strings: this.options.translations });

        this.listenTo(view, 'select', this.select);
        this.listenTo(view, 'deselect', this.deselect);

        return view.render().then(() => view.el);
    }
}
