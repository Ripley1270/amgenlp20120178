import TableView from 'sitepad/views/TableView';
import VisitRowView from 'sitepad/views/VisitRowView';
import VisitRows from 'sitepad/collections/VisitRows';
import UserVisits from 'sitepad/collections/UserVisits';
import {checkVisitAvailability, isVisitExpired} from 'sitepad/visits/evaluateVisit';
import {createEndVisitReport} from 'sitepad/visits/visitUtils';
import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';
import UserVisit from 'sitepad/models/UserVisit';
import * as DateTimeUtil from 'core/DateTimeUtil';
import Logger from 'core/Logger';
import Data from 'core/Data';

let logger = new Logger('VisitTableView');

/**
 * A table to display visits available for a given subject.
 * @class VisitTableView
 * @extends TableView
 */
export default class VisitTableView extends TableView {
    constructor (options = {}) {
        super(options);

        /**
         * @property {Subject} subject - The owner of the displayed visits.
         */
        this.subject = options.subject;

        /**
         * @property {VisitRows} collection - The collection to fetch and render.
         */
        this.collection = new VisitRows();

        let row = options.rows[0];

        // Set the default sort property and comparator.
        this.collection.sortProperty = row.property;
        this.collection.comparator = row.comparator || row.property;
    }

    /**
     * Fetch the rows to display.
     * @returns {Q.Promise<void>}
     */
    fetch () {
        return UserVisits.fetchCollection()
        .then(uVisits => {
            // Find all visits that belong to the selected subject.
            let visitsChain = uVisits.chain()
                // We cannot use where here due to how collections are chained...
                .filter(uVisit => {
                    return uVisit.get('subjectKrpt') === this.subject.get('krpt');
                });

            return LF.StudyDesign.visits.chain()
                .filter(visit => !visit.get('hidden'))
                .reduce((chain, visit) => {
                    return chain
                    .then(() => {
                        let userVisit = visitsChain.find(uVisit => {
                            return uVisit.get('visitId') === visit.get('id');
                        }).value();

                        this.collection.add({ visit, userVisit, action: LF.StudyDesign.visitAction.show });
                    });
                }, Q())
                .value();
        })
        .then(() => this.evaluateVisitsExpiration())
        .then(() => {
            return ELF.trigger('VISITGATEWAY:BeforeRenderVisits', { collection: this.collection, subject: this.subject }, this);
        })
        .then(() => this.renderRows());
    }

    /**
     * Goes through the visits collection and evaluates expiration of each visit.
     * @returns {Q.Promise<void>}
     */
    evaluateVisitsExpiration () {
        return this.collection.chain().reduce((chain, model) => {
            return chain.then(() => {
                let { visit, userVisit } = model.toJSON();

                if (!this.isEvaluationNeeded(userVisit)) {
                    return Q();
                }

                if (!visit.get('expiration')) {
                    return Q();
                }

                return isVisitExpired(visit, userVisit, this.subject)
                .then((expired) => {
                    if (!expired) {
                        return Q();
                    }

                    logger.operational(`Visit with id = "${visit.id}" expired for subject ${this.subject.get('subject_id')}. Putting state to "Incomplete".`);
                    let currentDate = new Date();
                    if (userVisit == null) {
                        userVisit = new UserVisit();
                        userVisit.set({
                            // eslint-disable-next-line new-cap
                            dateStarted: currentDate.ISOLocalTZStamp()
                        });
                    }
                    userVisit.set({
                        visitId: visit.get('id'),
                        studyEventId: visit.get('studyEventId'),
                        subjectKrpt: this.subject.get('krpt'),
                        state: LF.VisitStates.INCOMPLETE,
                        dateModified: DateTimeUtil.timeStamp(currentDate),
                        expired: 'true'
                    });

                    return userVisit.save()
                    .then(() => {
                        let visitStateCode = 2;
                        return createEndVisitReport({
                            visitStateCode,
                            visit,
                            skippedReason: '0',
                            visitStartDate: userVisit.get('dateStarted'),
                            subject: this.subject,
                            instanceOrdinal: BaseQuestionnaireView.getInstanceOrdinal(),
                            ink: null,
                            visitExpired: true
                        });
                    })
                    .then((res) => userVisit.save({
                        visitEndSigId: res.sig_id,
                        dateEnded: res.eventEndDate
                    }))
                    .then(() => {
                        model.set({ visit, userVisit, action: LF.StudyDesign.visitAction.show });
                    });
                });

            });
        }, Q()).value();
    }

    /**
     * Checks if provided visit model needs evaluation.
     * @param {UserVisit} userVisit - The UserVisit model in question.
     * @returns {boolean}
     */
    isEvaluationNeeded (userVisit) {
        if (userVisit == null) {
            return true;
        }

        let visitStatus = userVisit.get('state');

        //no need for evaluation if visit is already completed, skipped, incomplete.
        return visitStatus !== LF.VisitStates.COMPLETED &&
            visitStatus !== LF.VisitStates.SKIPPED &&
            visitStatus !== LF.VisitStates.INCOMPLETE;
    }

    /**
     * Goes through the visits collection and evaluates availability of each visit.
     * @returns {Q.Promise<void>}
     */
    evaluateVisitsAvailability () {
        return this.collection.chain().reduce((chain, model) => {
            return chain.then(() => {
                let { visit, userVisit } = model.toJSON();

                if (!this.isEvaluationNeeded(userVisit)) {
                    return Q();
                }

                if (!visit.get('availability')) {
                    return Q();
                }

                //evaluate availability and what to do
                return checkVisitAvailability(visit, userVisit, this.subject)
                .then((action) => {
                    model.set(action);
                });
            });
        }, Q()).value();
    }

    /**
     * Check if there are any visits to skip.
     * @param {string} type - The type skip being attempted.
     * @returns {boolean}
     */
    checkSkippedVisits (type) {
        let skippableVisitsExist = false,
            selectedVisit = this.selected.get('visit');

        Data.skippedVisits = [];

        this.collection.forEach(visitRow => {
            let { visit, userVisit, action } = visitRow.toJSON(),
                visitSkippable = false;

            if (action !== LF.StudyDesign.visitAction.hide) {
                visitSkippable = userVisit != null ?
                    (userVisit.get('state') === LF.VisitStates.IN_PROGRESS ||
                    userVisit.get('state') === LF.VisitStates.AVAILABLE ||
                    userVisit.get('state') === LF.VisitStates.NOT_AVAILABLE) :
                    true;
            }

            let skipToVisit = type === 'skipTo' ? visit !== selectedVisit : true;

            if ((visit.get('visitType') === 'ordered' &&
                visit.get('visitOrder') <= selectedVisit.get('visitOrder')) &&
                visitSkippable && skipToVisit) {
                Data.skippedVisits.push(visit);
                skippableVisitsExist = true;
            }
        });

        return skippableVisitsExist;
    }

    /**
     * Returns currently active visit.
     * @returns {VisitRow} - User visit object if it exists, null if it does not.
     */
    getActiveVisit () {
        return this.collection.find(visitRow => {
            let userVisit = visitRow.get('userVisit');
            return userVisit && userVisit.get('state') === LF.VisitStates.IN_PROGRESS;
        });
    }

    /**
     * Render a row.
     * @param {VisitRow} model - The subject to render.
     * @returns {Q.Promise<DOMElement>}
     */
    renderRow (model) {
        let view = new VisitRowView({ model, strings: this.options.translations });

        this.listenTo(view, 'select', this.select);
        this.listenTo(view, 'deselect', this.deselect);

        return view.render()
        .then(() => view.el);
    }
}
