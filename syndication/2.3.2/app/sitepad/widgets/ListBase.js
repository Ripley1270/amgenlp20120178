import MultipleChoiceBase from 'core/widgets/MultipleChoiceBase';
import Data from 'core/Data';

export default class ListBase extends MultipleChoiceBase {
    constructor (options) {
        super(options);

        /**
         * @property {Object<string,string>} events - List of the widgets events.
         */
        this.events = {
            // select the answer click event
            'click li': 'selectAnswer'
        };

        /**
         * @property {Object} options - Options passed into the constructor.
         */
        this.options = options;
    }

    /**
     * @property {string} template - The default template to render.
     * @default 'DEFAULT:SkipReason'
     */
    get template () {
        return 'DEFAULT:SkipReason';
    } 

    /**
     * @property {string} li - The default template to use for list items.
     * @default 'DEFAULT:ListItem'
     */
    get li () {
        return 'DEFAULT:ListItem';
    }

    /**
     * @property {string} loginInfoTemplate - The default template to use for displaying login information.
     * @default 'DEFAULT:LoginInfo'
     */
    get loginInfoTemplate () {
        return 'DEFAULT:LoginInfo';
    }

    /**
     * Responsible for displaying the widget.
     * @returns {Q.Promise<void>}
     */
    render () {
        let modelId = this.model.get('id'),
            answer = (this.answer && this.answer.get('response')) ? this.answer.get('response').toString() : false;

        return this.buildHTML('li')
        .then(() => new Q.Promise((resolve) => {
            this.parent.$el.prepend(this.loginInfo).append(this.$el)
                .ready(() => {
                    resolve();
                });
        }))
        .then(() => {
            let deferred = Q.defer();
            if (answer) {
                _(this.model.get('answers')).each((item, index) => {
                    if (answer === item.value) {
                        this.$(`#${modelId}_li_${index}`)
                            .addClass('selected')
                            .ready(() => {
                                deferred.resolve();
                            });
                    }
                });
            } else {
                deferred.resolve();
            }
            return deferred.promise;
        })
        .then(() => {
            this.delegateEvents();
        });
    }

    /**
     * Build the html structure of widget
     * @param {string} type - The type of the widget.
     * @example this.buildHTML('li');
     * @returns {Q.Promise<void>} resolved when HTML is built and ready
     */
    buildHTML (type) {
        let modelId = this.model.get('id'),
            template = this.model.get('templates') || this.template,
            loginInfoTemplate = this.loginInfoTemplate,
            input = this.li,
            deferred = Q.defer(),
            dynamicStrings = {
                patientId: this.questionnaire.subject.get('subject_id'),
                patientInitials: this.questionnaire.subject.get('initials'),
                userName: LF.security.activeUser.get('username'),
                siteNumber: Data.site.get('siteCode'),
                textClass: 'text-right'
            };

        LF.getStrings(this.templateStrings, (translated) => {
            this.$el.html(LF.templates.display(template, _.extend(translated, dynamicStrings)));
            this.loginInfo = LF.templates.display(loginInfoTemplate, _.extend(translated, dynamicStrings));

            _(this.model.get('answers')).each((answer, i) => {
                LF.getStrings(answer.text, (string) => {
                    let appendOption = ($target) => {
                        return $target.find('.skip-reason-answers')
                            .first()
                            .append(LF.templates.display(input, {
                                id: `${modelId}_${type}_${i}`,
                                reasonText: string,
                                value: answer.value
                            }));
                    };
                    appendOption(this.$el).ready(deferred.resolve());
                }, { namespace: this.parent.parent.id });
            });
        }, { namespace: this.parent.parent.id });
        return deferred.promise;
    }

    /**
     * Responds to answer selection.
     * @event
     * @param {(MouseEvent|TouchEvent)} e - Mouse or touch event.
     */
    selectAnswer (e) {
        let value = this.$(e.target).val();

        this.$('li').removeClass('selected');
        this.$(e.target).addClass('selected');

        if (!this.answers.size()) {
            this.answer = this.addAnswer();
        }

        this.respondHelper(this.answer, value.toString());
    }
}

window.LF.Widget.ListBase = ListBase;
