import Data from 'core/Data';
import ListBase from './ListBase';

export default class EditReason extends ListBase {
    constructor (options) {
        super(options);

        /**
         * @property {Object<string,string>} templateStrings - A list of resource keys to translate and render to the UI.
         */
        this.templateStrings = {
            // TODO - Perhaps this should be passed in via configuration?
            reason: 'REASON',
            patientIdLabel: 'PATIENT_ID'
        };

        this.options = options;
    }

    /**
     * @property {string} id - The ID of the widget
     * @readonly
     * @default 'edit-reason'
     */
    get id () {
        return 'edit-reason';
    }

    /**
     * @property {string} template - The default template to render.
     * @default 'DEFAULT:EditReason'
     */
    get template () {
        return 'DEFAULT:EditReason';
    }

    /**
     * @property {string} li - The default template to render for list items.
     * @default 'DEFAULT:ReasonListItem'
     */
    get li () {
        return 'DEFAULT:ReasonListItem';
    }

    /**
     * Responsible for displaying the widget.
     * @returns {Q.Promise<void>}
     */
    render () {
        let modelId = this.model.get('id'),
            answer = this.answer && this.answer.get('response') ? this.answer.get('response').toString() : false;

        return Q()
        .then(() => {
            this.buildHTML('li')
            .then(() => Q.Promise((resolve) => {
                this.parent.$el.append(this.$el)
                    .ready(() => {
                        resolve();
                    });
            }))
            .then(() => {
                let deferred = Q.defer();
                if (answer) {
                    _(this.model.get('answers')).each((item, index) => {
                        if (answer === item.value) {
                            this.$(`#${modelId}_li_${index}`)
                                .addClass('selected')
                                .ready(() => {
                                    deferred.resolve();
                                });
                        }
                    });
                } else {
                    deferred.resolve();
                }
                return deferred.promise;
            })
            .then(() => {
                this.delegateEvents();
            });
        });
    }

    /**
     * Build the html structure of widget
     * @param {String} type The type of the widget
     * @example this.buildHTML('li');
     * @returns {Q.Promise} resolved when HTML is built and ready
     */
    buildHTML (type) {
        let modelId = this.model.get('id'),
            template = this.model.get('templates') || this.template,
            input = this.li,
            deferred = Q.defer(),
            dynamicStrings = {
                patientId: this.questionnaire.subject.get('subject_id'),
                patientInitials: this.questionnaire.subject.get('initials'),
                userName: LF.security.activeUser.get('username'),
                siteNumber: Data.site.get('siteCode'),
                textClass: 'text-right'
            };

        LF.getStrings(this.templateStrings, (translated) => {
            this.$el.html(LF.templates.display(template, _.extend(translated, dynamicStrings)));
            _(this.model.get('answers')).each((answer, i) => {
                LF.getStrings(answer.text, (string) => {
                    let appendOption = ($target) => {
                        return $target.find('.edit-reason-answers')
                            .first()
                            .append(LF.templates.display(input, {
                                id: `${modelId}_${type}_${i}`,
                                reasonText: string,
                                value: answer.value
                            }));
                    };
                    appendOption(this.$el).ready(deferred.resolve());
                }, { namespace: this.parent.parent.id });
            });
        }, { namespace: this.parent.parent.id });
        return deferred.promise;
    }

    /**
     * Responds to answer selection.
     * @event
     * @param {Object} e Event data
     */
    selectAnswer (e) {
        let value = this.$(e.target).attr('data-reason');

        this.$('li').removeClass('selected');
        this.$(e.target).addClass('selected');

        if (!this.answers.size()) {
            this.answer = this.addAnswer();
        }

        this.respondHelper(this.answer, value.toString());
    }
}

window.LF.Widget.EditReason = EditReason;
