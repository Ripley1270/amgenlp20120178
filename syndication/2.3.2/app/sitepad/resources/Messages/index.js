import setupCoreMessages from 'core/resources/Messages';
import setupSitepadBanners from './Banners';
import setupSitepadDialogs from './DialogBoxes';

export default function setupSitepadMessages () {
    setupCoreMessages();
    setupSitepadBanners();
    setupSitepadDialogs();
}
