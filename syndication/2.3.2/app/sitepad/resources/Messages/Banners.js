import { Banner, MessageRepo } from 'core/Notify';

export default function setupSitepadBanners () {
    MessageRepo.add({
        type: 'Banner',
        key: 'PATIENT_ADDED',
        message: ({ afterShow = $.noop }) => {
            Banner.flash('success', 'PATIENT_ADDED');
            afterShow();
        }
    }, {
        type: 'Banner',
        key: 'EDIT_SERVICE_URL_SUCCESS',
        message: ({ afterShow = $.noop }) => {
            Banner.flash('success', 'EDIT_SERVICE_URL_SUCCESS');
            afterShow();
        }
    }, {
        type: 'Banner',
        key: 'PATIENT_EDITED',
        message: ({ afterShow = $.noop }) => {
            Banner.flash('success', 'PATIENT_EDITED');
            afterShow();
        }
    }, {
        type: 'Banner',
        key: 'UNABLE_TO_GET_SITE_LIST',
        message: (options = {}) => {
            LF.getStrings('UNABLE_TO_GET_SITE_LIST')
            .then(text => Banner.show(_.extend(options, {
                text,
                type: 'error',
                afterShow: options.afterShow || $.noop
            })));
        }
    }, {
        type: 'Banner',
        key: 'DEVICE_REGISTRATION_FAILURE',
        message: ({ afterShow = $.noop }) => Banner.error('DEVICE_REGISTRATION_FAILURE', { afterShow })
    }, {
        type: 'Banner',
        key: 'USER_EDITED',
        message: ({ afterShow = $.noop }) => {
            Banner.flash('success', 'USER_EDITED');
            afterShow();
        }
    });
}
