import COOL from 'core/COOL';
import Logger from 'core/Logger';
import WebService from 'core/classes/WebService';

const logger = new Logger('Transmit.transmitSPStartDate');

/**
 * Handles the SPStartDate update transmission to the web-service.
 * @param {Object} transmissionItem - The item in the transmission queue to send.
 * @param {Function} [callback] - The callback function to called upon completion.
 */
export default function transmitSPStartDate (transmissionItem, callback = $.noop) {
    let params = JSON.parse(transmissionItem.get('params')),
        dataJSON = {
            K: params.krpt,
            M: params.ResponsibleParty,
            U: 'Assignment',
            S: params.dateStarted,
            C: params.dateCompleted,
            R: params.reportDate,
            P: params.phase,
            E: LF.coreVersion,
            V: LF.StudyDesign.studyVersion,
            T: params.phaseStartDate,
            L: params.batteryLevel,
            J: params.sigID
        },
        sendSPStartDateSuccess = (res) => {
            logger.operational(`SPStartDate transmitted: res: ${res}`);
            this.destroy(transmissionItem.get('id'))
                .then(callback)
                .done();
        },

        sendSPStartDateError = (errorCode, httpCode) => {
            logger.trace(`transmitSPStartDate: SPStartDate error: removing transmissionItem errorCode: ${errorCode} httpCode: ${httpCode}`);
            this.remove(transmissionItem);
            callback();
        };

    logger.traceEnter('transmitSPStartDate');

    dataJSON.A = [];

    dataJSON.A.push({
        G: '1',
        F: 'SPStartDateAffidavit',
        Q: 'AFFIDAVIT'
    });

    dataJSON.A.push({
        G: params.SPStartDate,
        F: 'PT.SPStartDate',
        Q: ''
    });

    //compress answers collection with JSONH
    if (LF.StudyDesign.jsonh !== false) {
        dataJSON.A = JSONH.pack(dataJSON.A);
        logger.trace('Remapped after JSONH.pack');
    }

    let service = COOL.new('WebService', WebService);
    service.sendEditPatient(dataJSON, '')
        .then((res) => {
            sendSPStartDateSuccess(res);
        })
        .catch((e) => {
            sendSPStartDateError(e);
        })
        .done();

    logger.traceExit('transmitSPStartDate');
}
