import COOL from 'core/COOL';
import Logger from 'core/Logger';
import WebService from 'core/classes/WebService';
import CurrentSubject from 'core/classes/CurrentSubject';
import Subjects from 'core/collections/Subjects';
import * as transmitResetCredentials from 'sitepad/transmit/transmitResetCredentials';
import getSetupCode from 'sitepad/transmit/getSetupCode';

const logger = new Logger('Transmit.transmitResetPassword');

// TODO: In 2.4 this method should only return a promise
/**
 * Handles the transmitResetPassword transmission to the web-service.
 * @param {Object} transmissionItem - The item in the transmission queue to send.
 * @param {function} [callback] - The callback function to called upon completion.
 */
export default function transmitResetPassword (transmissionItem, callback = $.noop) {
    let params = JSON.parse(transmissionItem.get('params'));

    Subjects.getSubjectBy({ krpt: params.krpt })
    .tap(getSetupCode)
    .then((subject) => {
        let payload = { clientPassword: params.password };

        if (LF.StudyDesign.askSecurityQuestion) {
            payload.challengeQuestions = [{
                question: subject.get('secret_question'),
                answer: subject.get('secret_answer')
            }];
        }

        let onSuccess = (res, { isSubjectActive }) => {
            subject.save({
                subject_password: params.password,
                service_password: res.password,
                subject_active: isSubjectActive
            })
            .then(() => {
                CurrentSubject.clearSubject();
                return this.destroy(transmissionItem.get('id'));
            })
            .done(callback);
        };

        // TODO: In 2.4 Web modality can not use this implementation since it generates the unlock code in client
        // This transmission could have been created days ago. We have to use today's unlockcode in API call
        let service = COOL.new('WebService', WebService);

        // TODO: This call should use promises in 2.4
        service.resetSubjectPassword(payload, params.krpt, onSuccess, (errorCode, httpCode, isSubjectActive) => {
            transmitResetCredentials.onError.call(this, errorCode, httpCode, isSubjectActive, transmissionItem, callback);
        });
    })
    .catch((err) => {
        logger.error('Error in transmitResetPassword', err);
        this.remove(transmissionItem);
        callback();
    })
    .done();
}
