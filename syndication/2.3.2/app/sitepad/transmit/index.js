import COOL from 'core/COOL';
import CoreTransmit from 'core/transmit';

import getDeviceId from './getDeviceId';
import getSetupCode from './getSetupCode';
import sitepadPasswordSecretQuestion from './sitepadPasswordSecretQuestion';
import transmitUserEdit from './transmitUserEdit';
import transmitSPStartDate from './transmitSPStartDate';
import transmitResetCredentials from './transmitResetCredentials';
import transmitResetPassword from './transmitResetPassword';

let Transmit = _.extend({}, CoreTransmit, {
    getDeviceId,
    getSetupCode,
    sitepadPasswordSecretQuestion,
    transmitUserEdit,
    transmitSPStartDate,
    transmitResetCredentials,
    transmitResetPassword
});

COOL.service('Transmit', Transmit);

LF.Transmit = Transmit;

export default Transmit;
