import COOL from 'core/COOL';
import Logger from 'core/Logger';
import WebService from 'core/classes/WebService';
import CurrentSubject from 'core/classes/CurrentSubject';
import DailyUnlockCode from 'core/classes/DailyUnlockCode';
import Subjects from 'core/collections/Subjects';
import { MessageRepo } from 'core/Notify';
import * as lStorage from 'core/lStorage';
import getSetupCode from './getSetupCode';

const logger = new Logger('Transmit.transmitResetCredentials');

/**
 * Handles the error returned by the webservice method.
 * This is exported so that it can be reused by other credential transmit methods
 * @param {number} errorCode - error code return by the webservice method
 * @param {number} httpCode - httpCode error code return by the webservice method
 * @param {number} isSubjectActive - the flag returned by the webservice method
 * @param {Object} transmissionItem - The item in the transmission that got the error.
 * @param {function} [callback] - The callback function to called upon completion.
 */
export function onError (errorCode, httpCode, isSubjectActive, transmissionItem, callback = $.noop) {
    const { Dialog } = MessageRepo;

    if (!isSubjectActive) {
        LF.spinner.hide()
        .then(() => {
            LF.Actions.notify({
                dialog: Dialog && Dialog.PASSWORD_CHANGE_FAILED
            }, () => {
                LF.spinner.show();
                this.destroy(transmissionItem.get('id'), callback);
            });
        })
        .done();
    } else if (httpCode !== LF.ServiceErr.HTTP_NOT_FOUND) {
        LF.spinner.hide()
        .then(() => {
            errorCode = errorCode || '';

            LF.Actions.notify({
                dialog: Dialog && Dialog.TRANSMISSION_ERROR_HTTP_AND_ERROR,
                options: { httpCode, errorCode }
            }, () => {
                LF.spinner.show();

                if (httpCode === LF.ServiceErr.HTTP_FORBIDDEN && errorCode === LF.ServiceErr.SUBJECT_NOT_FOUND) {
                    // If the subject is deleted on backend then destroy the transmission item
                    this.destroy(transmissionItem.get('id'), callback);
                } else {
                    this.remove(transmissionItem);
                    callback();
                }
            });
        })
        .done();
    } else {
        this.remove(transmissionItem);
        callback();
    }
}

// TODO: In 2.4 this method should only return a promise
/**
 * Handles the transmitResetCredentials transmission to the web-service.
 * @param {Object} transmissionItem - The item in the transmission queue to send.
 * @param {function} [callback] - The callback function to called upon completion.
 */
export default function transmitResetCredentials (transmissionItem, callback = $.noop) {
    let params = JSON.parse(transmissionItem.get('params'));

    Subjects.getSubjectBy({ krpt: params.krpt })
    .tap(getSetupCode)
    .then((subject) => {
        let payload = {
            setupCode: subject.get('setupCode'),
            clientPassword: params.password,
            regDeviceId: lStorage.getItem('deviceId'),
            clientId: lStorage.getItem('clientId'),
            challengeQuestions: [{
                question: params.secretQuestion,
                answer: params.secretAnswer
            }]
        };

        let onSuccess = (res, { isSubjectActive }) => {
            subject.save({
                subject_password: params.password,
                secret_question: params.secretQuestion,
                secret_answer: params.secretAnswer,
                subject_active: isSubjectActive,
                service_password: res.password
            })
            .then(() => {
                CurrentSubject.clearSubject();
                return this.destroy(transmissionItem.get('id'));
            })
            .done(callback);
        };

        // TODO: In 2.4 Web modality can not use this implementation since it generates the unlock code in client
        // This transmission could have been created days ago. We have to use today's unlockcode in API call
        let todaysUnlockCode = new DailyUnlockCode().getCode(new Date()),
            service = COOL.new('WebService', WebService);

        // TODO: This call should use promises in 2.4
        service.resetSubjectCredentials(payload, params.krpt, todaysUnlockCode, onSuccess, (errorCode, httpCode, isSubjectActive) => {
            onError.call(this, errorCode, httpCode, isSubjectActive, transmissionItem, callback);
        });
    })
    .catch((err) => {
        logger.error('Error in transmitResetCredentials', err);
        this.remove(transmissionItem);
        callback();
    })
    .done();
}
