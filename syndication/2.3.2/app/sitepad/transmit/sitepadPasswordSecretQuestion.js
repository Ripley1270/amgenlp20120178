import COOL from 'core/COOL';
import Logger from 'core/Logger';
import * as Utilities from 'core/utilities';
import WebService from 'core/classes/WebService';
import CurrentSubject from 'core/classes/CurrentSubject';
import Subjects from 'core/collections/Subjects';
import { MessageRepo } from 'core/Notify';
import getSetupCode from './getSetupCode';
import getDeviceId from './getDeviceId';
import transmitResetCredentials from 'sitepad/transmit/transmitResetCredentials';
import transmitResetPassword from 'sitepad/transmit/transmitResetPassword';
const logger = new Logger('Transmit.sitepadPasswordSecretQuestion');

/**
 * Handles the sitepadPasswordSecretQuestion transmission to the web-service.
 * @param {Object} transmissionItem - The item in the transmission queue to send.
 * @param {Function} [callback] - The callback function to called upon completion.
 */
export default function sitepadPasswordSecretQuestion (transmissionItem, callback = $.noop) {
    if (!Utilities.isSitePad()) {
        logger.error('sitepadPasswordSecretQuestion transmission function is intended to be used only for SitePad.');
        callback();
        return;
    }

    let params = JSON.parse(transmissionItem.get('params'));

    let isPermanentPasswordCreated = (subject) => {
        let service = COOL.new('WebService', WebService),
            setupCode = subject.get('setupCode');

        // in 2.4 prefer webservice.getSingleSubject method
        return service.querySubjectData(setupCode, ['Password'], '', $.noop, $.noop)
        .then((response) => !!response.W);
    };

    Subjects.getSubjectBy({ krpt: params.krpt })
    .tap(getSetupCode)
    .tap(getDeviceId)
    .then((subject) => {
        let onError = (errorCode, httpCode, isSubjectActive) => {
            const { Dialog } = MessageRepo;

            if (!isSubjectActive) {
                LF.spinner.hide()
                .then(() => {
                    LF.Actions.notify({
                        dialog: Dialog && Dialog.PASSWORD_CHANGE_FAILED
                    }, () => {
                        LF.spinner.show();
                        this.destroy(transmissionItem.get('id'), callback);
                    });
                })
                .done();
            } else if (httpCode !== LF.ServiceErr.HTTP_NOT_FOUND) {
                LF.spinner.hide()
                .then(() => {
                    errorCode = errorCode || '';

                    LF.Actions.notify({
                        dialog: Dialog && Dialog.TRANSMISSION_ERROR_HTTP_AND_ERROR,
                        options: { httpCode, errorCode }
                    }, () => {
                        LF.spinner.show();

                        if (httpCode === LF.ServiceErr.HTTP_FORBIDDEN && errorCode === LF.ServiceErr.SUBJECT_NOT_FOUND) {
                            // If the subject is deleted on backend then destroy the transmission item
                            this.destroy(transmissionItem.get('id'), callback);
                        } else {
                            this.remove(transmissionItem);
                            callback();
                        }
                    });
                })
                .done();
            } else {
                this.remove(transmissionItem);
                callback();
            }
        };

        // Intenionally creating a nested promise chain. This is to avoid the catch block at the end.
        // We don't need this nested chain 2.4 because the structure is fixed.
        isPermanentPasswordCreated(subject)
        .then((permanentPasswordCreated) => {
            // If permanent password is already set on another device then we should be calling a different API
            // The existing transmissionItem should be updated and then redirected to the right transmit method
            if (permanentPasswordCreated) {
                if (params.secret_question && params.secret_answer) {
                    transmissionItem.save({
                        method: 'transmitResetCredentials',
                        params: JSON.stringify({
                            password: params.password,
                            krpt: params.krpt,
                            secretQuestion: params.secret_question,
                            secretAnswer: params.secret_answer
                        })
                    })
                    .then(() => {
                        // In 2.4 should return a promise instead of taking callback parameter
                        transmitResetCredentials.call(this, transmissionItem, callback);
                    })
                    .done();
                } else {
                    transmissionItem.save({ method: 'transmitResetPassword' })
                    .then(() => {
                        // In 2.4 should return a promise instead of taking callback parameter
                        transmitResetPassword.call(this, transmissionItem, callback);
                    })
                    .done();
                }
                return;
            }

            let onSuccess,
                mappedJSON,
                auth = [subject.get('setupCode'), subject.get('service_password'), subject.get('device_id')].join(':');

            if (params.secret_question === null) {
                params.secret_question = subject.get('secret_question');
            }

            if (params.secret_answer === null) {
                params.secret_answer = subject.get('secret_answer');
            }

            mappedJSON = {
                Q: params.secret_question,
                A: params.secret_answer
            };

            if (params.password) {
                mappedJSON.W = params.password;
            }

            onSuccess = (res, { isSubjectActive }) => {
                let attributes = {
                    secret_question: params.secret_question,
                    secret_answer: params.secret_answer,
                    subject_active: isSubjectActive
                };

                if (params.password) {
                    attributes.subject_password = params.password;
                }

                if (res && res.W) {
                    attributes.service_password = res.W;
                }

                if (res && res.D) {
                    attributes.device_id = res.D;
                }

                subject.save(attributes)
                .then(() => {
                    CurrentSubject.clearSubject();
                    this.destroy(transmissionItem.get('id'), callback);
                })
                .done();
            };

            let service = COOL.new('WebService', WebService);

            if (!subject.get('service_password')) {
                mappedJSON.U = subject.get('setupCode');
                mappedJSON.O = 'SitePad';

                service.setSubjectData(mappedJSON, onSuccess, onError)

                // The onError callback handles the errors. Until we handle the errors with
                // the rejected promise, let's just catch and do nothing
                .catch(() => $.noop())
                .done();
                return;
            }

            service.updateSubjectData(subject.get('device_id'), mappedJSON, auth, onSuccess, onError)

            // The onError callback handles the errors. Until we handle the errors with
            // the rejected promise, let's just catch and do nothing
            .catch(() => $.noop())
            .done();
        })
        .catch(({ errorCode, httpCode, isSubjectActive }) => onError(errorCode, httpCode, isSubjectActive))
        .done();
    })
    .catch(err => {
        logger.error('Error in sitepadPasswordSecretQuestion', err);
        this.remove(transmissionItem);
        callback();
    })
    .done();
}
