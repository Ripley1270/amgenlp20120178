import COOL from 'core/COOL';
import * as Utilities from 'core/utilities';
import WebService from 'core/classes/WebService';
import Logger from 'core/Logger';

const logger = new Logger('Transmit.getDeviceId');

/**
 * Gets the device_id of a subject of which password has been set before on another device
 * Runs only for sitepad because LogPad always gets the deviceId during activation
 * @param {Subject} subject - The subject model to get the deviceId for
 * @returns {Q.Promise<void>}
 */
export default function getDeviceId (subject) {
    return Q.Promise(resolve => {
        let payload = {
            U: subject.get('setupCode'),
            W: subject.get('subject_password'),
            O: 'SitePad'
        };

        if (Utilities.isSitePad() && subject.get('service_password') && !subject.get('device_id')) {
            let service = COOL.new('WebService', WebService);

            service.getDeviceID(payload, (res) => {
                subject.save({ device_id: res.D })
                .done(resolve);
            }, (err) => {
                logger.error('Error getting device id', err);
                resolve();
            });
        } else {
            resolve();
        }
    });
}
