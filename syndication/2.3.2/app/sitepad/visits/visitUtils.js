import * as DateTimeUtil from 'core/DateTimeUtil';
import * as utils from 'core/utilities';
import * as lStorage from 'core/lStorage';
import Dashboard from 'core/models/Dashboard';
import Answer from 'core/models/Answer';
import Transmission from 'core/models/Transmission';
import * as dateTimeUtil from 'core/DateTimeUtil';

/**
 * Get the battery level of the device.
 * @returns {Q.Promise<number>}
 */
function getBatteryLevel () {
    return Q.Promise(resolve => {
        LF.Wrapper.Utils.getBatteryLevel((batteryLevel) => {
            resolve(batteryLevel);
        });
    });
}

/**
 * Creates event at entry variables.
 * @param {Object} options - options
 * @param {Number} options.eventId - Event id
 * @param {Object} options.startDate - Start date
 * @param {Number} options.offset - Timezone offset in millis
 * @param {String} options.questionnaireId - Questionnaire ID
 * @param {Number} options.instanceOrdinal - Ordinal
 * @param {Object} options.subject - Subject in question
 * @returns {Q.Promise<Number>}
 */
export function createEventAtEntryVariables ({eventId, startDate, offset, questionnaireId, instanceOrdinal, subject}) {
    let eventAtEntry = new Answer({
            subject_id: subject.get('subject_id'),
            question_id: 'EventAtEntry',
            questionnaire_id: questionnaireId,
            response: eventId,
            SW_Alias: 'Phase.0.EventAtEntry',
            instance_ordinal: instanceOrdinal
        }),
        eventStartDate = new Answer({
            subject_id: subject.get('subject_id'),
            question_id: 'EventStartDate',
            questionnaire_id: questionnaireId,
            response: startDate,
            SW_Alias: 'Phase.0.EventStartDate',
            instance_ordinal: instanceOrdinal
        }),
        eventTZOffset = new Answer({
            subject_id: subject.get('subject_id'),
            question_id: 'EventTZOffset',
            questionnaire_id: questionnaireId,
            response: offset.toString(),
            SW_Alias: 'Phase.0.EventTZOffset',
            instance_ordinal: instanceOrdinal
        });

    return eventAtEntry.save()
    .then(() => eventStartDate.save())
    .then(() => eventTZOffset.save());
}

/**
 * Calculates visit end date for expired visit: visit start date + expiration, otherwise undefined.
 * @param {Boolean} visitExpired - indicates if current visit is expired
 * @param {String} visitStartDate - date when visit is started
 * @param {number} visitExpiration - param indicates when visit should expire (in minutes) from visit start.
 * @returns {Date}
 */
export function calculateVisitEndDate (visitExpired = false, visitStartDate, visitExpiration) {
    let visitEndDate;
    if (visitExpired && visitStartDate != null && visitExpiration != null) {
        // eslint-disable-next-line new-cap
        visitEndDate = new Date(dateTimeUtil.parseDateTimeIso(visitStartDate).getTime() + visitExpiration * 60000);
    }
    return visitEndDate;
}

/**
 * Creates end visit report.
 * @param {Object} options  - options.
 * @param {number} options.visitStateCode - (1 Skipped, 2 Incomplete, 3 Complete).
 * @param {number} options.visit - current visit.
 * @param {number} options.skippedReason - reason code for skipping visits.
 * @param {Date} options.visitStartDate - The start date of the visit.
 * @param {Object} options.subject - Subject
 * @param {Number} options.instanceOrdinal - Ordinal
 * @param {String} options.ink - Ink
 * @param {Boolean} visitExpired - indicates if visit is expired
 * @returns {Q.Promise<Object>}
 */
export function createEndVisitReport ({visitStateCode, visit, skippedReason, visitStartDate, subject, instanceOrdinal, ink, visitExpired = false}) {
    let user = LF.security.activeUser,
        currentDate = new Date(),
        reportDate = DateTimeUtil.convertToDate(currentDate),
        //eventEndDate = utils.buildStudyWorksFormat(currentDate),
        visitExpiration = visit.get('expiration'),
        expirationParam = visitExpiration && visitExpiration.functionParams && visitExpiration.functionParams.timeoutMinutes,
        eventEndDate = calculateVisitEndDate(visitExpired, visitStartDate, expirationParam) || currentDate,
        studyEventId = visit.get('studyEventId'),
        dashboard = new Dashboard({
            user_id: user ? user.get('id') : '',
            subject_id: subject.get('subject_id'),
            device_id: lStorage.getItem('deviceId') || lStorage.getItem('IMEI') || subject.device_id || subject.get('device_id'),
            questionnaire_id: 'P_VisitEnd',
            SU: 'VisitEnd',
            instance_ordinal: instanceOrdinal,
            started: currentDate.ISOStamp(),
            report_date: reportDate,
            phase: subject.get('phase'),
            phaseStartDateTZOffset: subject.get('phaseStartDateTZOffset'),
            study_version: LF.StudyDesign.studyVersion,
            ink: ink ? JSON.stringify(ink) : '',

            // add extra millisecond to current date when creating sig_id. After time travel Date is updated every 1000ms
            // and same value is returned within same second. This value should be different than sig_id of VisitStart (see DE20558).
            sig_id: `${'SA'}.${(currentDate.getTime() + 1).toString(16)}${lStorage.getItem('IMEI')}`,
            krpt: subject.get('krpt'),
            responsibleParty: skippedReason ? user.get('username') : lStorage.getItem('studyDbName')
        }),
        varEndDate = new Answer({
            subject_id: subject.get('subject_id'),
            question_id: 'EventEndDate',
            questionnaire_id: 'P_VisitEnd',
            response: utils.buildStudyWorksFormat(eventEndDate),
            SW_Alias: 'VisitEndDate.0.EventEndDate',
            instance_ordinal: dashboard.get('instance_ordinal')
        }),
        varEventId = new Answer({
            subject_id: subject.get('subject_id'),
            question_id: 'EventID',
            questionnaire_id: 'P_VisitEnd',
            response: studyEventId,
            SW_Alias: 'VisitEndDate.0.EventID',
            instance_ordinal: dashboard.get('instance_ordinal')
        }),
        varReasonforSkippingVisit = new Answer({
            subject_id: subject.get('subject_id'),
            question_id: 'ReasonforSkippingVisit',
            questionnaire_id: 'P_VisitEnd',
            response: skippedReason || '',
            SW_Alias: 'VisitEndDate.0.ReasonforSkippingVisit',
            instance_ordinal: dashboard.get('instance_ordinal')
        }),
        varVisitComplete = new Answer({
            subject_id: subject.get('subject_id'),
            question_id: 'VisitComplete',
            questionnaire_id: 'P_VisitEnd',
            response: (visitStateCode === 3) ? '1' : '0',
            SW_Alias: 'VisitEndDate.0.VisitComplete',
            instance_ordinal: dashboard.get('instance_ordinal')
        }),
        varVisitStatus = new Answer({
            subject_id: subject.get('subject_id'),
            question_id: 'VisitStatus',
            questionnaire_id: 'P_VisitEnd',
            response: visitStateCode.toString(),
            SW_Alias: 'VisitEndDate.0.VisitStatus',
            instance_ordinal: dashboard.get('instance_ordinal')
        }),
        varAffidavit = new Answer({
            subject_id: subject.get('subject_id'),
            question_id: 'AFFIDAVIT',
            questionnaire_id: 'P_VisitEnd',
            response: '1',
            SW_Alias: 'CustomAffidavit',
            instance_ordinal: dashboard.get('instance_ordinal')
        }),
        varProtocol = new Answer({
            subject_id: subject.get('subject_id'),
            question_id: 'Protocol',
            questionnaire_id: 'P_VisitEnd',
            response: subject.get('custom10') != null ? subject.get('custom10') : LF.StudyDesign.studyProtocol.defaultProtocol.toString(),
            SW_Alias: 'Protocol.0.Protocol',
            instance_ordinal: dashboard.get('instance_ordinal')
        }),
        transmission = new Transmission();

    return getBatteryLevel()
    .then((batteryLevel) => {
        return dashboard.save({
            completed: currentDate.ISOStamp(),
            completed_tz_offset: currentDate.getOffset(),
            diary_id: parseInt(currentDate.getTime().toString() +
                currentDate.getMilliseconds().toString(), 10),
            battery_level: batteryLevel
        });
    })
    .then(() => {
        return Q.all([
            varEndDate.save(),
            varEventId.save(),
            varReasonforSkippingVisit.save(),
            varVisitComplete.save(),
            varVisitStatus.save(),
            varAffidavit.save(),
            varProtocol.save()
        ]);
    })
    .then(() => {
        let visitStartTimeStamp = DateTimeUtil.dateLocalTZFromISO8601(visitStartDate);
        return createEventAtEntryVariables({
            eventId: studyEventId,
            startDate: utils.buildStudyWorksFormat(visitStartTimeStamp),
            offset: DateTimeUtil.tzOffsetInMillis(visitStartTimeStamp),
            questionnaireId: 'P_VisitEnd',
            instanceOrdinal: dashboard.get('instance_ordinal'),
            subject: subject
        });
    })
    .then(() => {
        let newTransmission = {
            method: 'transmitQuestionnaire',
            params: JSON.stringify({
                dashboardId: dashboard.get('id'),
                sigId: dashboard.get('sig_id')
            }),
            created: currentDate.getTime(),
            hideCount: true
        };
        if (subject.get('isDuplicate')) {
            newTransmission.status = 'failed';
        }
        return transmission.save(newTransmission);
    })
    .then(() => {
        // TODO: Rename ISOLocalTZStamp() to meet code guidelines.
        // eslint-disable-next-line new-cap
        return { sig_id: dashboard.get('sig_id'), eventEndDate: eventEndDate.ISOLocalTZStamp()};
    });
}
