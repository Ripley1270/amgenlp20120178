import UserVisits from 'sitepad/collections/UserVisits';
import * as dateTimeUtil from 'core/DateTimeUtil';


/**
 * Utility function to check subjects phase.
 * @param {Object} params - visit context params.
 * @param {Object} params.availability config
 * @param {Object} params.visit from study design
 * @param {Object} params.user visit model object
 * @param {Object} params.subject - subject
 * @returns {Q.Promise<boolean>}
 */
export function isPhase (params) {
    return Q(params.subject.get('phase') === params.avaliability.functionParams.phaseID);
}

LF.Visits.availabilityFunctions.isPhase = isPhase;

/**
 * Utility function to check if Visit is completed.
 * @param {Object} params - visit context params.
 * @param {Object} params.availability config
 * @param {Object} params.visit from study design
 * @param {Object} params.user visit model object
 * @param {Object} params.subject - subject
 * @returns {Q.Promise<boolean>}
 */
export function isVisitCompleted (params) {
    return UserVisits.fetchCollection().then(uVisits => {

        let userVisit = uVisits.find(uVisit => {
            return (uVisit.get('subjectKrpt') === params.subject.get('krpt') &&
            uVisit.get('visitId') === params.avaliability.functionParams.visitID &&
            uVisit.get('state') === LF.VisitStates.COMPLETED);
        });

        return (userVisit != null);
    });
}

LF.Visits.availabilityFunctions.isVisitCompleted = isVisitCompleted;

/**
 * Utility function to check if Visit is expired.
 * @param {Object} params - visit context params.
 * @param {Object} params.expiration config
 * @param {Object} params.visit from study design
 * @param {Object} params.user visit model object
 * @param {Object} params.subject - subject
 * @returns {Q.Promise<boolean>}
 */
export function isVisitExpired (params) {
    return UserVisits.fetchCollection().then(uVisits => {
        let timeoutMillis = params.expiration.functionParams.timeoutMinutes * 60 * 1000,
            timePassed = 0,
            visit = uVisits.find((uVisit) => {
                return (uVisit.get('subjectKrpt') === params.subject.get('krpt') &&
                uVisit.get('visitId') === params.visit.id);
            });

        if (visit != null) {
            timePassed = new Date().getTime() - dateTimeUtil.parseDateTimeIso(visit.get('dateStarted')).getTime();
        }

        return (timePassed > 0 ? timePassed > timeoutMillis : false);
    });
}

LF.Visits.availabilityFunctions.isVisitExpired = isVisitExpired;
