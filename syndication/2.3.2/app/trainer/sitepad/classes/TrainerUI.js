import COOL from 'core/COOL';

/**
 * @class TrainerUI
 * A class that modifies the UI behavior and style for Trainer mode.
 */
export default class TrainerUI {
    /**
     * @method registerCSS
     * Registers CSS styles to distinguish trainer mode UI
     */
    static registerCSS () {
        let styleSheet = document.styleSheets[(document.styleSheets.length - 1)];

        styleSheet.insertRule(`
            .navbar-inverse {
                border: none;
                background: #024e80;
                color: #ffffff;
                font-weight: bold;
                text-shadow: none;
                background-image: -webkit-gradient(linear, left top, left bottom, from(#78549f), to(#552988));
                background-image: -webkit-linear-gradient(#78549f, #552988);
                background-image:    -moz-linear-gradient(#78549f, #552988);
                background-image:     -ms-linear-gradient(#78549f, #552988);
                background-image:      -o-linear-gradient(#78549f, #552988);
                background-image:         linear-gradient(#78549f, #552988);
                background: #024e80 repeating-linear-gradient(45deg, #024e80, #024e80 10px, #035f8f 10px, #035f8f 20px);
            }`, styleSheet.cssRules.length);

        styleSheet.insertRule(`
            .navbar-nav {
                border: none;
                background: #fc7c21; color: #ffffff;
                font-weight: bold;
                text-shadow: 0 1px 1px #444444;
                background-image: -webkit-gradient(linear, left top, left bottom, from(#fc7c21 ), to(#e36f1e));
                background-image: -webkit-linear-gradient(#fc7c21, #e36f1e);
                background-image:    -moz-linear-gradient(#fc7c21, #e36f1e);
                background-image:     -ms-linear-gradient(#fc7c21, #e36f1e);
                background-image:      -o-linear-gradient(#fc7c21, #e36f1e);
                background-image:         linear-gradient(#fc7c21, #e36f1e);
                background: #fc7c21 repeating-linear-gradient(45deg, rgba(255,255,255,0), rgba(255,255,255,0.0) 10px, rgba(255,255,255,0.2) 10px, rgba(255,255,255,0.2) 20px);
            }`, styleSheet.cssRules.length);
    }

    /**
     * @method overrideCoreStrings
     * Add TRAINER_HEADER to appropriate strings for all locales
     */
    static overrideCoreStrings () {
        let appStrings = LF.strings.match({
            namespace: 'CORE',
            resources: {}
        });

        appStrings.forEach(item => {
            let resources = item.get('resources');

            // DE17366 - If APPLICATION_HEADER or APP_NAME is undefined, do nothing. Let the i18n
            // service fallback to a language where it is defined.
            resources.APPLICATION_HEADER && (resources.APPLICATION_HEADER += ' {{ TRAINER_HEADER }}');
            resources.APP_NAME && (resources.APP_NAME += ' {{ TRAINER_HEADER }}');
        });
    }
}

COOL.add('SitepadTrainerUI', TrainerUI);
