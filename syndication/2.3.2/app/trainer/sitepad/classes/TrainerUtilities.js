import COOL from 'core/COOL';
import { Utilities } from 'core/utilities';

/**
 * @function extendCoreUtilities
 * Extends some of the core utility functions and adds the class to the COOL registry
 */
export function extendCoreUtilities () {
    /**
     * @class TrainerUtilities
     * A class that extends the core Utilities class
     */
    class TrainerUtilities extends COOL.getClass('Utilities', Utilities) {
        /**
         * Check if the device is online. For trainer this always resolve with true.
         * @param {function} callback A callback function invoked upon completion.
         */
        static isOnline (callback = $.noop) {
            callback(true);
            return Q(true);
        }
    }

    COOL.add('Utilities', TrainerUtilities);
}
