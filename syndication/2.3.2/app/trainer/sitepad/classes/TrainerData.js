import COOL from 'core/COOL';

/**
 * @class TrainerData
 * A class to provide fake data for the trainer mode
 */
export default class TrainerData {
    /**
     * @method getData
     * Return the fake data for a specific webservice function
     * @param {string} webserviceFunction The name of the webservice function to return fake data for
     * @returns {Array|object} The fake trainer data.
     */
    static getData (webserviceFunction) {
        let syncStatus = {
            syncID: null,
            isSubjectActive: 1,
            isDuplicate: false
        };

        switch (webserviceFunction) {
            case 'getSites':
                return [
                    [
                        { krdom: 'DOM.466280.195', siteCode: '0001' },
                        { krdom: 'DOM.466591.209', siteCode: '0002' },
                        { krdom: 'DOM.463029.8', siteCode: '0003' },
                        { krdom: 'DOM.466985.231', siteCode: '0004' },
                        { krdom: 'DOM.466797.219', siteCode: '0005' },
                        { krdom: 'DOM.473830.257', siteCode: '0006' },
                        { krdom: 'DOM.469130.239', siteCode: '0007' },
                        { krdom: 'DOM.466923.225', siteCode: '0008' },
                        { krdom: 'DOM.468381.18', siteCode: '0009' },
                        { krdom: 'DOM.472978.249', siteCode: '0010' },
                        { krdom: 'DOM.473794.253', siteCode: '0011' },
                        { krdom: 'DOM.478020.319', siteCode: '0012 '}
                    ],
                    syncStatus
                ];
            case 'registerDevice':
                return [
                    { apiToken: '78525C0D907EB7EF96F1CBA4A4D54C91', regDeviceId: 422 },
                    syncStatus
                ];
            case 'addUser':
                return [195, syncStatus];
            case 'getAllSubjects':
                return [];
            case 'syncUserVisitsAndReports':
                return { reports: [], lastRefreshTime: new Date().ISOLocalTZStamp() };
            case 'syncUsers':
                return [];
            case 'updateUserCredentials':
                return ['S', syncStatus];
            case 'sendLogs':
                return [null, syncStatus];
            case 'sendSubjectAssignment':
                return [{ krpt: 'SA.ff634d2f7a87eebf47eda9ae99eca813' }, syncStatus];
            case 'setSubjectData':
                return [
                    { D: '186b6970-5b57-4b51-8234-1ba70aeb8fc7', W: 'e025bf2329734303c524ab6e92f26235' },
                    syncStatus
                ];
            case 'sendDiary':
                return [null, syncStatus];
            case 'updateSubjectData':
                return [{ W: 'e025bf2329734303c524ab6e92f26235' }, syncStatus];
            case 'sendEditPatient':
                return [{ krpt: 'SA.97c321226b23cfdd2fd6f289340761ef' }, syncStatus];
            case 'getSetupCode':
                return ['84317334', syncStatus];
            case 'getDeviceID':
                return [
                    {
                        D: '818ad42a-c0f5-4bff-b8b7-0f3f08f46aee',
                        W: 'e6c91a51191b0b80a63fcc065bfa6c3b',
                        Q: 0,
                        A: 'f7c0e071db137f5ae65382041c7cef4b'
                    },
                    syncStatus
                ];
            case 'getServerTime':
                return [{
                    utc: new Date()
                }];
            case 'updateUser':
                return ['S', syncStatus];
            default:
                return [];
        }
    }
}

COOL.add('SitepadTrainerData', TrainerData);
