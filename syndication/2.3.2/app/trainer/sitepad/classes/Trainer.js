import Logger from 'core/Logger';
import * as lStorage from 'core/lStorage';
import ELF from 'core/ELF';
import * as notify from 'core/Notify';
import * as utilities from 'core/utilities';
import COOL from 'core/COOL';
import TrainerUI from './TrainerUI';
import * as trainerWebservice from './TrainerWebservice';
import * as trainerUtilities from './TrainerUtilities';
import * as trainerSiteSelectionView from 'trainer/sitepad/views/TrainerSiteSelectionView';
import * as trainerUnlockCodeView from 'trainer/sitepad/views/TrainerUnlockCodeView';
import * as trainerSettingsView from 'trainer/sitepad/views/TrainerSettingsView';
import * as trainerSetTimeZoneActivationView from 'trainer/sitepad/views/TrainerSetTimeZoneActivationView';
import * as isStudyOnline from 'trainer/sitepad/expressions/isStudyOnline';
import trainerRules from 'trainer/sitepad/resources/rules';
import messages from 'trainer/sitepad/resources/messages';

import TimeTravel from 'core/TimeTravel';

let logger = new Logger('Sitepad Trainer');

/**
 * @class Trainer
 * A class to manage sitepad trainer mode.
 */
export default class Trainer {
    /**
     * @member {boolean} enabled
     * This value is true if the trainer mode is enabled
     */
    static get enabled () {
        return localStorage.getItem('trainer') === 'true';
    }

    /**
     * @method initialize
     * Run during the application startup.
     * Registers the ELF rules to listen for trainer mode activation.
     */
    static initialize () {
        // Wait for the document to be ready before overriding anything.
        $(() => {
            let trainerMode = localStorage.getItem('trainer');

            COOL.getClass('SitepadTrainer', Trainer).registerTrainerModeListener();

            if (trainerMode === 'true') {
                COOL.getClass('SitepadTrainer', Trainer).enableTrainer();
                COOL.getClass('SitepadTrainer', Trainer).fireTrainerEnabledTrigger();
            }
        });

        // Register disableTrainer as an action in order to avoid circular dependecy and to keep things together in the rules
        ELF.action('disableTrainer', COOL.getClass('SitepadTrainer', Trainer).disableTrainer);
    }

    /**
     * @method registerTrainerModeListener
     * Registers the ELF rules to listen for trainer mode activation.
     */
    static registerTrainerModeListener () {
        ELF.rules.add({
            id: 'SitepadTrainerModeListener',
            trigger: ['MODESELECT:Submit'],
            evaluate (filter, resume) {
                resume(filter.product === 'sitepad' && filter.value === 'trainer');
            },
            resolve: [
                { action: () => COOL.getClass('SitepadTrainer', Trainer).enableTrainer() },
                { action: () => COOL.getClass('SitepadTrainer', Trainer).fireTrainerEnabledTrigger() }
            ]
        });

        // This is for disabing trainer mode after if the user backs out of the site selection view
        ELF.rules.add({
            id: 'SitepadDisableTrainerMode',
            trigger: ['SITESELECTION:Back'],
            evaluate (filter, resume) {
                resume(filter.product === 'sitepad' && COOL.getClass('SitepadTrainer', Trainer).enabled);
            },
            resolve: [{
                action: () => COOL.getClass('SitepadTrainer', Trainer).disableTrainer()
            }]
        });
    }

    /**
     * @method enableTrainer
     * Runs the methods required for enabling trainer mode
     * @returns {Q.Promise<void>}
     */
    static enableTrainer () {
        localStorage.setItem('trainer', true);
        lStorage.setItem('studyDbName', 'Trainer');
        lStorage.setItem('serviceBase', 'Trainer');

        trainerWebservice.extendCoreWebservice();
        trainerSiteSelectionView.extendSiteSelectionView();
        trainerUnlockCodeView.extendUnlockCodeView();
        trainerSettingsView.extendSettingsView();
        trainerUtilities.extendCoreUtilities();
        trainerSetTimeZoneActivationView.extendSetTimeZoneActivationView();
        trainerSetTimeZoneActivationView.extendSetTimeZoneView();
        isStudyOnline.overrideIsStudyOnline();
        TimeTravel.displayTimeTravel(true);

        COOL.getClass('SitepadTrainerUI', TrainerUI).registerCSS();
        COOL.getClass('SitepadTrainerUI', TrainerUI).overrideCoreStrings();

        COOL.getClass('SitepadTrainer', Trainer).registerMessages();
        COOL.getClass('SitepadTrainer', Trainer).unRegisterCoreRules();
        COOL.getClass('SitepadTrainer', Trainer).overrideCoreActions();
        COOL.getClass('SitepadTrainer', Trainer).registerTrainerRules();
    }

    /**
     * @method fireTrainerEnabledTrigger
     * Fires an ELF trigger to anounce that trainer mode is enabled
     * @returns {Q.Promise<void>}
     */
    static fireTrainerEnabledTrigger () {
        return ELF.trigger('SITEPADTRAINER:Enabled');
    }

    /**
     * @method registerMessages
     * Registers the messages specific for the trainer mode
     */
    static registerMessages () {
        messages.forEach(message => {
            notify.MessageRepo.add(message);
        });
    }

    /**
     * @method unRegisterCoreRules
     * Un-registers some core rules to prevent transmissions
     */
    static unRegisterCoreRules () {
        ELF.rules.remove('DuplicateSubjectHandling');
        ELF.rules.remove('GatewayTransmit');
        ELF.rules.remove('QuestionnaireTransmit');
        ELF.rules.remove('EditPatientDiarySync');
    }

    /**
     * @method registerTrainerRules
     * Registers the rules specific for the trainer mode
     */
    static registerTrainerRules () {
        trainerRules.forEach(rule => {
            ELF.rules.add(rule);
        });
    }

    /**
     * @method overrideCoreActions
     * Override some core actions to prevent their default behavior
     */
    static overrideCoreActions () {
        let noopAction = () => Q();

        ELF.action('transmitAll', noopAction);
        ELF.action('refreshAllData', noopAction);
    }

    /**
     * @method disableTrainer
     * Runs the methods required for disabling trainer mode
     * @returns {Q.Promise<void>}
     */
    static disableTrainer () {
        localStorage.removeItem('trainer');
        lStorage.removeItem('studyDbName');
        lStorage.removeItem('serviceBase');

        utilities.restartApplication();

        return Q();
    }
}

COOL.add('SitepadTrainer', Trainer);

Trainer.initialize();
