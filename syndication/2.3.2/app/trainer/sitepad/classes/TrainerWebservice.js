import COOL from 'core/COOL';
import WebService from 'core/classes/WebService';
import Logger from 'core/Logger';
import TrainerData from './TrainerData';

let logger = new Logger('Sitepad TrainerWebservice');

/**
 * @function extendCoreWebservice
 * Extends some of the functions of the core Webservice module and adds the module to the COOL registry
 */
export function extendCoreWebservice () {
    /**
     * @class SitepadTrainerWebservice
     * A class that extends the core Webservice Module.
     * The functions in this class override the core functionality to prevent backend API calls
     */
    class SitepadTrainerWebservice extends COOL.getClass('WebService', WebService) {
        /**
         * @method getSites
         * Get a list of available sites that belong to a given study.
         * @param {string} studyName The name of the study to get sites for.
         * @param {function} onSuccess A callback function invoked upon successful completion of the request.
         * @param {function} onError A callback function invoked upon failure of the request.
         * @returns {Object} A deferred promise.
         */
        getSites (studyName, onSuccess, onError) {
            logger.trace(`getSites for ${studyName}`);

            return Q.apply(this, COOL.getClass('SitepadTrainerData', TrainerData).getData('getSites'));
        }

        /**
         * @method getServerTime
         * Gets the time in utc from the server..
         * @return {Q.Promise<object>} Q Promise
         */
        getServerTime () {
            logger.trace('getServerTime');

            return Q.apply(this, COOL.getClass('SitepadTrainerData', TrainerData).getData('getServerTime'));
        }

        /**
         * @method registerDevice
         * Register a SitePad device.
         * @param {Object} payload
         */
        registerDevice (payload) {
            logger.trace('registerDevice', payload);

            return Q.apply(this, COOL.getClass('SitepadTrainerData', TrainerData).getData('registerDevice'));
        }

        /**
         * Send new user data.
         * @param {Object} params
         * @param {string} params.user_type type of the user ['admin' || null]
         * @param {string} params.user_name user's name
         * @param {string} params.language user's language
         * @param {string} params.password user's password
         * @param {string} params.key salt for the user's password hash
         * @param {string} params.role role of the new user
         * @param {string} params.syncLevel level of synchronization
         * @param {string} params.syncValue value of the sync level
         * @param {string} params.auth authorization token for the webservice.
         * @returns {Q.Promise<res, { syncId, isSubjectActive, isDuplicate }>} Q promise
         */
        addUser (params = {}) {
            logger.trace('addUser', params);

            return Q.apply(this, COOL.getClass('SitepadTrainerData', TrainerData).getData('addUser'));
        }

        /**
         * Get all subject data for a site from Middle-Tier and get all non-DCF data back, and DCF data which are changed in SW.
         * @param {Object} params
         * @param {string} params.auth AJAX authorization token.
         * @param {function} onSuccess A callback function invoked upon successful completion.
         * @param {function} onError A callback function invoked upon failure.
         */
        getAllSubjects (params = {}, onSuccess, onError) {
            let data = COOL.getClass('SitepadTrainerData', TrainerData).getData('getAllSubjects');

            logger.trace('getAllSubjects', params);

            onSuccess(data);
            return Q(data);
        }

        /**
         * Send krpt, krDom, lastUpdated, deviceId and get all visits and forms status for that site.
         * @param  {Object} params parameters
         * @param  {string} params.auth auth token
         * @param  {string} params.krDom site krDom
         * @param  {string} params.lastUpdated last update timestamp
         * @param  {string} params.krpt subject SW ID
         * @param  {string} params.deviceId device SW ID
         * @param  {function} onSuccess success callback
         * @param  {function} onError error callback
         */
        syncUserVisitsAndReports (params = {}, onSuccess, onError) {
            let data = COOL.getClass('SitepadTrainerData', TrainerData).getData('syncUserVisitsAndReports');

            logger.trace('syncUserVisitsAndReports', params);

            onSuccess(data);
            return Q(data);
        }

        /**
         * Update Get all users that have been added/updated since PHT_lastUpdatedUsers or 0
         * @param {Object} params
         * @param {function} onSuccess A callback function invoked upon successful completion.
         * @param {function} onError A callback function invoked upon failure.
         * @returns {Q.Promise<res, { syncId, isSubjectActive, isDuplicate }>} Q promise
         */
        syncUsers (params = {}, onSuccess = $.noop, onError = $.noop) {
            let data = COOL.getClass('SitepadTrainerData', TrainerData).getData('syncUsers');

            logger.trace('syncUsers', params);

            onSuccess(data);
            return Q(data);
        }

        /**
         * Update non-subject User's password and/or security question and answer
         * @param {Object} params
         * @param {string} params.auth authorization token for the webservice.
         * @param {string} params.userId unique id of user to update
         * @param {string} params.password new password we want to send
         * @param {string} params.salt salt of the new password
         * @param {string} params.secretQuestion secretQuestion of user to update
         * @param {string} params.secretAnswer secretAnswer of user to update
         * @returns {Q.Promise<res, {syncId, isSubjectActive, isDuplicate}>} q promise
         */
        updateUserCredentials (params = {}) {
            logger.trace('updateUserCredentials', params);

            return Q.apply(this, COOL.getClass('SitepadTrainerData', TrainerData).getData('updateUserCredentials'));
        }

        /**
         * Logs to be sent.
         * @param {Object} logs Logs generated to be sent.
         * @param {function} onSuccess A callback function invoked upon successful completion.
         * @param {function} onError A callback function invoked upon failure.
         * @returns {Q.Promise<res, { syncId, isSubjectActive, isDuplicate }>} Q promise
         */
        sendLogs (logs, onSuccess, onError) {
            let data = COOL.getClass('SitepadTrainerData', TrainerData).getData('sendLogs');

            logger.trace('sendLogs', logs);

            onSuccess.apply(this, data);
            return Q.apply(this, data);
        }

        /**
         * @method sendSubjectAssignment
         * Send the new Patient using Transmission Queue.
         * @param {string} assignment The subject assignment to be sent.
         * @param {string} auth Authorization token.
         * @returns {Q.Promise<res, { syncId, isSubjectActive, isDuplicate }>} Q promise
         */
        sendSubjectAssignment (assignment, auth) {
            logger.trace('sendSubjectAssignment', assignment);

            return Q.apply(this, COOL.getClass('SitepadTrainerData', TrainerData).getData('sendSubjectAssignment'));
        }

        /**
         * Used to set the subject password, secret question and secret answer for the first time.
         * @param {Object} payload
         * @param {string} payload.U Setup Code
         * @param {string} payload.W Password
         * @param {string} payload.Q Secret Question
         * @param {string} payload.A Secret Answer
         * @param {string} payload.O Client Name
         * @param {function} onSuccess A callback function invoked upon successful completion.
         * @param {function} onError A callback function invoked upon failure.
         * @returns {Q.Promise<res, { syncId, isSubjectActive, isDuplicate }>} Q promise
         */
        setSubjectData (payload, onSuccess, onError) {
            let data = COOL.getClass('SitepadTrainerData', TrainerData).getData('setSubjectData');

            logger.trace('setSubjectData', payload);

            onSuccess.apply(this, data);
            return Q.apply(this, data);
        }

        /**
         * Send the completed Diary using Transmission Queue.
         * @param {Object} diary The completed diary to be sent.
         * @param {Object} auth the authentication token
         * @param {boolean} jsonh - The value indicating whether jsonh compression is being used or not
         * @param {function} onSuccess A callback function invoked upon successful completion.
         * @param {function} onError A callback function invoked upon failure.
         * @returns {Q.Promise<res, { syncId, isSubjectActive, isDuplicate }>} Q promise
         */
        sendDiary (diary, auth, jsonh, onSuccess, onError) {
            let data = COOL.getClass('SitepadTrainerData', TrainerData).getData('sendDiary');

            logger.trace('sendDiary', diary);

            onSuccess.apply(this, data);
            return Q.apply(this, COOL.getClass('SitepadTrainerData', TrainerData).getData('sendDiary'));
        }

        /**
         * Updates the Subject's Data.
         * @param {string} deviceID The device ID for the subject.
         * @param {Object} subjectData The data needs to be updated.
         * @param {string} auth AJAX authorization token.
         * @param {function} onSuccess A callback function invoked upon successful completion.
         * @param {function} onError A callback function invoked upon failure.
         * @returns {Q.Promise<res, { syncId, isSubjectActive, isDuplicate }>} Q promise
         */
        updateSubjectData (deviceID, subjectData, auth, onSuccess, onError) {
            let data = COOL.getClass('SitepadTrainerData', TrainerData).getData('updateSubjectData');
            logger.trace(`updateSubjectData for deviceID: ${deviceID}`, subjectData);

            onSuccess.apply(this, data);
            return Q.apply(this, data);
        }

        /**
         * Sends Edit Patient Diary data
         * @param {Object} params data to be sent
         * @param {Object} auth the authentication token
         * @returns {Q.Promise<res, { syncId, isSubjectActive, isDuplicate }>} Q Promise
         */
        sendEditPatient (params, auth) {
            logger.trace('sendEditPatient', params);

            return Q.apply(this, COOL.getClass('SitepadTrainerData', TrainerData).getData('sendEditPatient'));
        }

        /**
         * Gets subject setup code
         * @param {Object} krpt krpt of the subject of which setup code is to be returned
         * @returns {Q.Promise<res, { syncId, isSubjectActive, isDuplicate }>} Q promise
         */
        getSetupCode (krpt) {
            logger.trace('getSetupCode', krpt);

            return Q.apply(this, COOL.getClass('SitepadTrainerData', TrainerData).getData('getSetupCode'));
        }

        /**
         * Get the device ID for the subject.
         * @param {Object} subjectData The Subject's data used to retrieve the device ID.
         * @param {function} onSuccess A callback function invoked upon successful completion.
         * @param {function} onError A callback function invoked upon failure.
         * @returns {Q.Promise<res, { syncId, isSubjectActive, isDuplicate }>} Q promise
         */
        getDeviceID (subjectData, onSuccess, onError) {
            let data = COOL.getClass('SitepadTrainerData', TrainerData).getData('getDeviceID');

            logger.trace('getDeviceID for subjectData', subjectData);

            onSuccess.apply(this, data);
            return Q.apply(this, data);
        }

        /**
         * Update existing user.
         * @param {Object} params - Parameters passed into the method.
         * @param {number} params.id - The ID of the user.
         * @param {string} params.userType - type of the user ['admin' || null]
         * @param {string} params.username - user's name
         * @param {string} params.language - user's language
         * @param {string} params.role - role of the new user
         * @param {string} params.syncLevel - level of synchronization
         * @param {string} params.syncValue - value of the sync level
         * @param {string} [params.auth] - authorization token for the webservice.
         * @returns {Q.Promise<res, {syncId, isSubjectActive, isDuplicate}>} q promise
         */
        updateUser (params = {}) {
            logger.trace('updateUser');

            return Q.apply(this, COOL.getClass('SitepadTrainerData', TrainerData).getData('updateUser'));
        }
    }

    COOL.add('WebService', SitepadTrainerWebservice);

    // FIXME: This should be considered a workaround.
    // We should not be using LF.studyWebService thoughout the code. It should be extended using COOL by PDEs in the study level
    LF.studyWebService = COOL.new('WebService');
}
