import COOL from 'core/COOL';
import SetTimeZoneActivationView from 'sitepad/views/SetTimeZoneActivationView';
import SetTimeZoneView from 'sitepad/views/SetTimeZoneView';

/**
 * @function extendSetTimeZoneActivationView
 * Extends SetTimeZoneActivationView to fake setting time zone on trainer
 */
export function extendSetTimeZoneActivationView () {
    /**
     * @class TrainerSetTimeZoneActivationView
     * A class that extends the sitepad SetTimeZoneActivationView view.
     */
    class TrainerSetTimeZoneActivationView extends COOL.getClass('SetTimeZoneActivationView', SetTimeZoneActivationView) {
        /**
         * Resolve any dependencies required for the view to render.
         * @returns {Q.Promise<void>}
         */
        resolve () {
            this.timeZonesDisplay = [{ id: 'America/New_York', displayName: '(GMT-05:00) America/New_York' }];
            this.currentTimeZone = { id: 'America/New_York', displayName: '(GMT-05:00) America/New_York' };

            return Q();
        }

        /**
         * Changes the Timezone. In trainer, just go to the next screen.
         * @param {string} timezone TimeZone object.
         * @returns {Q.Promise<void>}
         */
        changeTimeZone (timezone) {
            this.navigateNext();

            return Q();
        }
    }

    COOL.add('SetTimeZoneActivationView', TrainerSetTimeZoneActivationView);
}

/**
 * @function extendSetTimeZoneView
 * Extends SetTimeZoneActivationView to fake setting time zone on trainer
 */
export function extendSetTimeZoneView () {
    /**
     * @class TrainerSetTimeZoneView
     * A class that extends the sitepad SetTimeZoneView view.
     * @extends SetTimeZoneView
     */
    class TrainerSetTimeZoneView extends COOL.getClass('SetTimeZoneView', SetTimeZoneView) {
        /**
         * Resolve any dependencies required for the view to render.
         * @returns {Q.Promise<void>}
         */
        resolve () {
            this.timeZonesDisplay = [{ id: 'America/New_York', displayName: '(GMT-05:00) America/New_York' }];
            this.currentTimeZone = { id: 'America/New_York', displayName: '(GMT-05:00) America/New_York' };
            return Q();
        }

        /**
         * Overrides the render to enable the next button
         * @returns {Q.Promise<void>}
         */
        render () {
            return super.render()
            .then(() => {
                return this.enableButton(this.$set);
            });
        }

        /**
         * Changes the Timezone. In trainer, just go to the next screen.
         * @returns {Q.Promise<void>}
         */
        setTimeZone () {
            this.back();

            return Q();
        }
    }

    COOL.add('SetTimeZoneView', TrainerSetTimeZoneView);
}
