import { MessageRepo } from 'core/Notify';

export default [{
    id: 'GatewayTransmit',
    trigger: [
        'HOME:Transmit',
        'VISITGATEWAY:Transmit',
        'FORMGATEWAY:Transmit',
        'LOGIN:Transmit',
        'USERMANAGEMENT:Transmit'
    ],
    resolve: [
        { action: 'displayMessage', data: 'PLEASE_WAIT' },
        { action: () => Q.delay(500) },
        { action: 'removeMessage' }
    ]
}, {
    id: 'QuestionnaireTransmit',
    trigger: 'QUESTIONNAIRE:Transmit',
    salience: 2,
    resolve: [
        { action: 'displayMessage', data: 'PLEASE_WAIT' },
        { action: () => Q.delay(500) },
        { action: 'removeMessage' }
    ]
}, {
    id: 'ExitSitepadTrainer',
    trigger: 'SITEPADTRAINER:Exit',
    evaluate: {
        expression: (filter, resume) => {
            const { Dialog } = MessageRepo;
            return MessageRepo.display(Dialog && Dialog.TRAINER_EXIT_NOTIFICATION)
            .then(resume);
        }
    },
    resolve: [
        { action: 'uninstall', data: ['UserReport', 'UserVisit'] },
        { action: 'disableTrainer' }
    ]
}, {
    id: 'TrainerEditUserCompleted',
    trigger: 'QUESTIONNAIRE:Navigate/Edit_User/AFFIDAVIT',
    evaluate: 'isDirectionForward',
    salience: 3,
    resolve: [{
        // In normal mode this banner is displayed after a successful transmission. In trainer we don't transmit
        // anything so we are adding the banner as a trainer level action
        action: 'displayBanner',
        data: 'USER_EDITED'
    }]
}];
