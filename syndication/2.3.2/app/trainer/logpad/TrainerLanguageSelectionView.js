import Logger from 'core/Logger';
import ActivationBaseView from 'core/views/ActivationBaseView';
import CurrentContext from 'core/CurrentContext';
import COOL from 'core/COOL';

const logger = new Logger('TrainerLanguageSelectionView');

export default class TrainerLanguageSelectionView extends COOL.getClass('ActivationBaseView', ActivationBaseView) {
    constructor (trainerData) {
        /**
         * @property {Object<string,string>} events - A list of DOM events.
         * @readonly
         */
        this.events = {
            'click #next': 'next',
            'click #back': 'back'
        };

        /**
         * @property {Object<string,string>} selectors - A list of selectors to populate.
         */
        this.selectors = {
            language: '#language'
        };

        /**
         * @property {Object<string,string>} templateStrings - A list of resource keys to translate and render to the UI.
         */
        this.templateStrings = {
            header: 'APPLICATION_HEADER',
            language: 'LANGUAGE',
            tagline: 'SELECT_LANGUAGE',
            next: 'NEXT',
            back: 'BACK'
        };

        super();
    }

    /**
     * @property {string} id - The ID of the view.
     * @readonly
     * @default 'trainer-setup-page'
     */
    get id () {
        return 'trainer-setup-page';
    }

    /**
     * @property {string} template - Id of template to render
     * @readonly
     * @default '#trainer-language-selection-template'
     */
    get template () {
        return '#trainer-language-selection-template';
    }

    /**
     * Back button click handler
     * @param {(MouseEvent|TouchEvent)} e - A mouse or touch event.
     */
    back (e) {
        e.preventDefault();

        LF.Utilities.restartApplication();
    }

    /**
     * Render the view to the DOM.
     * @returns {Q.Promise.<void>}
     */
    render () {
        return this.buildHTML(null, true)
        .then(() => {
            this.renderLanguages();
            this.delegateEvents();
            this.$language.select2({
                minimumResultsForSearch: Infinity
            });
        });
    }

    /**
     * Render a list of available languages.
     */
    renderLanguages () {
        let languages = LF.strings.getLanguages(),
            preferredLanguageLocale = localStorage.getItem('preferredLanguageLocale'),
            prefferedArray = preferredLanguageLocale && preferredLanguageLocale.split('-'),
            currentLanguage = prefferedArray ? prefferedArray[0] : CurrentContext().get('language'),
            currentLocale = prefferedArray ? prefferedArray[1] :CurrentContext().get('locale');

        languages.forEach(resource => {
            let el = `<option value="${resource.language}-${resource.locale}">${resource.localized}</option>`;

            if (resource.language === currentLanguage && resource.locale === currentLocale) {
                el = $(el).prop('selected', true);
            }

            this.$language.append(el);

            // Clear out DOM reference to prevent potential memory leaks.
            el = null;
        });
    }

    /**
     * Navigate to the next view.
     * @param {(MouseEvent|TouchEvent)} evt - A mouse or touch event.
     */
    next (evt) {
        localStorage.setItem('preferredLanguageLocale', this.$language.val());

        this.navigate('set_time_zone_activation', true);
    }
}

COOL.add('TrainerLanguageSelectionView', TrainerLanguageSelectionView);