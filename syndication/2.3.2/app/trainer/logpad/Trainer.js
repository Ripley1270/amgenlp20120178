import Logger from 'core/Logger';
import {restartApplication} from 'core/utilities';
import ActiveAlarms from 'core/collections/ActiveAlarms';
import COOL from 'core/COOL';
import RestartManager from 'core/classes/RestartManager';
import TrainerController from 'trainer/logpad/TrainerController';

let logger = new Logger('trainer');

let enableTrainerOnce = false;

/**
 * @file Sets the default values for the Trainer's Activation process and allows PDE's
 * to override the default values and configure a form to set values in the field.
 * @author <a href="mailto:chris.stakutis@ert.com">Chris Stakutis</a>
 * @version Syndication 1.0
 */
export default class Trainer {
    /**
     * Strings to fetch in the render function
     * @param {Object} trainerData sets trainer data
     */
    static setData (trainerData) {
        this.data = trainerData;
    }

    /**
     * Set or get trainer Mode
     * @param {boolean=} bool Sets the mode of trainer
     * If 'bool' === true, the oneTime css/elf adjustments are made and the mode is recorded
     * If 'bool' === false, mode is disabled and ELF triggers a restart
     * If 'bool' is not set, reports-back the static setting AND enables trainer if static is set
     * @returns {boolean|Q.Promise<void>} (which is not good.)
     */
    static trainerMode (bool) {
        if (bool) {
            this.data = this.data || {};
            COOL.getClass('LogpadTrainer', Trainer).enableTrainer();
        } else if (typeof bool == 'undefined') {
            let result = localStorage.getItem('trainer');
            return result;
        } else {
            return COOL.getClass('LogpadTrainer', Trainer).disableTrainer(this);
        }
    }

    // Set (or get) simulation data (for WebServices) based on a service-name
    static trainerData (key, val) {
        if (val) {
            this.data[key] = val;
        } else if (typeof this.data[key] != 'undefined') {
            return this.data[key];
        } else {
            logger.error('trainerData: Item not in list:' + key);
        }
        return {};
    }

    // Spying on Utilities doesn't work; wrap restartApplication
    // so that unit tests can trap/spyOn it
    static restartApplication () {
        restartApplication();
    }

     /**
     * @property {Object} - The default router options for trainer
     * @readonly
     */
    static get routerOptions () {
        return {
            routes: {
                trainer_language_selection: 'trainer#trainerLanguageSelection',
                trainer_setup: 'trainer#trainerSetup'
            }
        };
    }

    /**
     * Create the new Router.
     * @returns {Q.Promise}
     */
    static createRouter () {
        const opts = {
            routerOpts: COOL.getClass('LogpadTrainer', Trainer).routerOptions,
            controllers: {
                trainer: COOL.new('TrainerController', TrainerController)
            }
        };

        // Run whichever startup sequences are registered.
        const startups = ['Startup'];

        return Q.all(_.map(startups, name => {
            const Startup = COOL.getClass(name);
            return Startup && new Startup().backboneStartup(opts);
        }))
        .catch((err) => {
            err && logger.error('Error starting backbone', err);
        });
    }

    // FIXME: Refactor this method by exporting this blob into sperate files.
    static enableTrainer () {
        logger.trace('Setting trainer mode...');

        // Avoid setting twice
        if (enableTrainerOnce) {
            return;
        }

        enableTrainerOnce = true;
        logger.trace('Enabling trainer mode');
        localStorage.setItem('trainer', true);
        //TimeTravel && TimeTravel.displayTimeTravel(true);
        let appStrings, lang, i, len;

        COOL.getClass('LogpadTrainer', Trainer).createRouter();

        document.styleSheets[(document.styleSheets.length - 1)].insertRule(
            '.navbar-inverse {' +
            'border: 1px solid #035f8f;' +
            'background: #024e80;' +
            'color: #ffffff;' +
            'font-weight: bold;' +
            'text-shadow: none;' +
            'background-image: -webkit-gradient(linear, left top, left bottom, from( #78549f), to( #552988));' +
            'background-image: -webkit-linear-gradient( #78549f, #552988);' +
            'background-image:    -moz-linear-gradient( #78549f, #552988);' +
            'background-image:     -ms-linear-gradient( #78549f, #552988);' +
            'background-image:      -o-linear-gradient( #78549f, #552988);' +
            'background-image:         linear-gradient( #78549f, #552988);' +
            'background: #024e80 repeating-linear-gradient( 45deg, #024e80, #024e80 10px, #035f8f 10px, #035f8f 20px);' +
            '}', document.styleSheets[(document.styleSheets.length - 1)].cssRules.length);
        document.styleSheets[(document.styleSheets.length - 1)].insertRule(
            '.navbar-nav {' +
            'border: 1px solid #e36f1e;' +
            'background: #fc7c21; color: #ffffff;' +
            'font-weight: bold;' +
            'text-shadow: 0 1px 1px #444444;' +
            'background-image: -webkit-gradient(linear, left top, left bottom, from( #fc7c21 ), to( #e36f1e));' +
            'background-image: -webkit-linear-gradient( #fc7c21 , #e36f1e );' +
            'background-image: -moz-linear-gradient( #fc7c21 , #e36f1e );' +
            'background-image: -ms-linear-gradient( #fc7c21 , #e36f1e );' +
            'background-image: -o-linear-gradient( #fc7c21 , #e36f1e );' +
            'background-image: linear-gradient( #fc7c21 , #e36f1e );' +
            'background: #fc7c21 repeating-linear-gradient( 45deg, rgba(255,255,255,0), rgba(255,255,255,0.0) 10px, rgba(255,255,255,0.2) 10px, rgba(255,255,255,0.2) 20px);' +
            '}', document.styleSheets[(document.styleSheets.length - 1)].cssRules.length);

        // Add TRAINER_HEADER to appropriate strings for all locales
        appStrings = LF.strings.match({
            namespace: 'CORE',
            resources: {}
        });

        for (i = 0, len = appStrings.length; i < len; i += 1) {
            let resources = appStrings[i].get('resources');
            resources.APPLICATION_HEADER += ' {{ TRAINER_HEADER }}';
            resources.SITE_GATEWAY_TITLE += ' {{ TRAINER_HEADER }}';
        }

        // Remove transmission rules
        ELF.rules.remove('TransmitAll');
        ELF.rules.remove('ToolboxTransmit');
        ELF.rules.remove('PendingReportTransmit');
        ELF.rules.remove('PasswordTransmit');
        ELF.rules.remove('Termination');

        /*
         * TrainerTransmitAll
         * Replaces TransmitAll
         * Omits all transmission queue items.
         */
        ELF.rules.add({
            id: 'TrainerTransmitAll',
            trigger: [
                'QUESTIONNAIRE:Transmit',
                'RESETPASSWORD:Transmit'
            ],
            evaluate: {
                expression: function (filter, resume) {
                    COOL.getClass('Utilities').isOnline(isOnline => {
                        logger.trace('TrainerTransmitAll');
                        resume(isOnline);
                    });
                }
            },
            resolve: [{
                action: 'displayMessage',
                data: 'PLEASE_WAIT'
            }, {
                action: () => RestartManager.setTransmissionActive(true)
            }, {
                action: () => Q.delay(500)
            }, {
                action: 'removeMessage'
            }, {
                action: () => RestartManager.setTransmissionActive(false)
            }],
            reject: [{
                action: 'navigateTo', data: 'dashboard'
            }, {
                action: () => RestartManager.setTransmissionActive(false)
            }]
        });

        /*
         * TrainerToolboxTransmit
         * Replaces ToolboxTransmit and PendingReportTransmit
         * Omits all transmission queue items including any DCF data.
         */
        ELF.rules.add({
            id: 'TrainerToolboxTransmit',
            trigger: [
                'LOGIN:Transmit',
                'DASHBOARD:Transmit',
                'TOOLBOX:Transmit'
            ],
            evaluate: {
                expression: function (filter, resume) {
                    COOL.getClass('Utilities').isOnline(isOnline => {
                        logger.trace('TrainerToolboxTransmit');
                        resume(isOnline);
                    });
                }
            },
            resolve: [{
                action: 'displayMessage',
                data: 'PLEASE_WAIT'
            }, {
                action: () => RestartManager.setTransmissionActive(true)
            }, {
                action: () => Q.delay(500)
            }, {
                action: 'removeMessage'
            }, {
                action: () => RestartManager.setTransmissionActive(false)
            }],
            reject: [{
                action: function (input) {
                    return this.transmitRetry(input)
                        .then((res) => {
                            if (res === false) {
                                return RestartManager.setTransmissionActive(false);
                            }
                        });
                },
                data: { message: 'TRANSMIT_RETRY_CONNECTION_REQUIRED' }
            }]
        });

        /*
         * TrainerPasswordTransmit
         * Replaces PasswordTransmit
         * Omits all transmission queue items including any DCF data.
         */
        ELF.rules.add({
            id: 'TrainerPasswordTransmit',
            trigger: [
                'CHANGEPASSWORD:Transmit',
                'CHANGESECRETQUESTION:Transmit'
            ],
            evaluate: {
                expression: function (filter, resume) {
                    COOL.getClass('Utilities').isOnline(isOnline => {
                        resume(isOnline);
                    });
                }
            },
            resolve: [{
                action: 'displayMessage',
                data: 'PLEASE_WAIT'
            }, {
                action: () => RestartManager.setTransmissionActive(true)
            }, {
                action: () => Q.delay(500)
            }, {
                action: 'removeMessage'
            }, {
                action: () => RestartManager.setTransmissionActive(false)
            }]
        });

        /*
         * Termination
         * Handles Trainer end use. Cancel all Alarms, clear database and navigate to
         * QR code page.
         */
        ELF.rules.add({
            id: 'TrainerTermination',
            trigger: 'Termination',
            evaluate: {
                expression: function (filter, resume) {
                    LF.Actions.confirm({ key: 'TRAINER_EXIT_NOTIFICATION' })
                        .then(resume);
                }
            },
            resolve: [{
                action: 'uninstall'
            }, {
                action: (input, done) => {
                    LF.Wrapper.exec({
                        execWhenWrapped: () => {
                            let activeAlarms = new ActiveAlarms();

                            activeAlarms.fetch()
                                .then(() => activeAlarms.cancelAllAlarms())
                                .done(done);
                        },
                        execWhenNotWrapped: done
                    });
                }
            }, {
                action: function (data, resume) {
                    // This is essential; a full-refresh means the css/barber goes away
                    //window.location = './index.html';
                    enableTrainerOnce = false;  // allows unit tests to run better
                    COOL.getClass('LogpadTrainer', Trainer).restartApplication();
                    resume(true);
                }
            }]
        });
    }

    // FIXME: Convert to a non-static method and remove/replace instance parameter with this
    static disableTrainer (instance) {
        logger.traceEnter('exitTrainerMode');

        let subject,
            doDisable = true;

        if (instance && instance.subject) {
            subject = instance.subject.get('device_id');
        }

        return ELF.trigger('Termination', {
            endParticipation: true,
            subjectActive: true,
            deviceID: subject
        }, instance)
        .catch((e)=> {
            logger.trace('Termination catch returned:' + e);
            if (e) {
                doDisable = false;
            }
        })
        .then(()=> {
            // DONT use .finally() because we need to return a promise-like thingy for unit testing
            logger.traceExit('exitTrainerMode triggered/completed');
            enableTrainerOnce = false;  // allows unit tests to run better
            return doDisable;
        });
    }
}

COOL.add('LogpadTrainer', Trainer);
