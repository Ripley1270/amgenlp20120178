import COOL from 'core/COOL';
import Logger from 'core/Logger';
import ActivationBaseView from 'core/views/ActivationBaseView';

/**
 * Used to log any errors or client metrics.
 * @readonly
 * @type Object
 * @default new {@link LF.Log.Logger}
 */
const logger = new Logger('TrainerSetupView');

// FIXME: This whole file is a good example of how a view should NOT be implemented. 
//        This screen needs to be implemented from scratch
export default class TrainerSetupView extends ActivationBaseView {

    constructor (trainerData) {
        this.trainerData = trainerData;
        this.localVariables = trainerData.Configs.localVariables || [];

        this.templateSrc = $('#trainersetup-template').html();

        /**
         * Array of questions for this form. Each item is an object
         * that defines the id, label, type, and storage. Select types
         * also have a an array of options that contain objects with the value and text.
         * This is set in the config file.
         * @type Array
         * @readonly
         */
        this.localVariables = trainerData.Configs.localVariables || [];

        /**
         * The unique id of the root element.
         * @readonly
         * @type String
         * @default 'trainer-setup-page'
         */
        this.id = 'trainer-setup-page';

        /**
         * The view's events.
         * @readonly
         * @enum {Event}
         */
        this.events = {
            'click #next': 'submit',
            'click #back': 'back'
        };

        /**
         * Strings to fetch in the render function
         * @type Object
         */
        this.stringsToFetchForRender = {
            header: 'APPLICATION_HEADER',
            title: 'DEVICE_ACTIVATION',
            language: 'LANGUAGE',
            submit: 'OK',
            submitIcon: {
                key: 'OK',
                namespace: 'ICONS'
            },
            back: 'BACK'
        };

        super();
    }

    /**
     * Initialization
     */
    initialize () {
        this.preInstallCheck();
        this.addLanguagesToSelectBox();
    }

    /**
     * Renders the view.
     * @returns {Object} this ({@link LF.View.TrainerSetupView})
     */
    render () {
        let questions = '';

        this.localVariables.forEach((val) => {
            this.stringsToFetchForRender[val.label] = val.label;
        });

        _(LF.StudyDesign.securityQuestions).each((value) => {
            if (value.display) {
                this.stringsToFetchForRender[value.text] = value.text;
            }
        });

        LF.getStrings(this.stringsToFetchForRender, (strings) => {
            this.localVariables.forEach((val) => {

                //@TODO: Use templates instead of generating HTML in the runtime
                questions += '<div class="form-group">';
                questions += '<label for="' + val.id + '" >' + strings[val.label] + '</label>';

                let itemValue;

                switch (val.id) {
                    case 'answer':
                        itemValue = undefined;
                        break;
                    case 'logging':
                        itemValue = LF.Data[val.id] || this.trainerData.Configs.defaults.log_level;
                        break;
                    default:
                        let defaultValue;
                        if (val.type === 'select') {
                            defaultValue = val.options.reduce((selectedValue, option) => {
                                if (!!selectedValue) {
                                    return selectedValue;
                                }

                                if (!!option.selected) {
                                    return option.value;
                                }

                                return null;
                            }, null);
                        } else {
                            defaultValue = val.default;
                        }

                        if (val.storage === 'LF.Data')
                            itemValue = LF.Data[val.id] || defaultValue;
                        else if (val.storage === 'localStorage') {
                            itemValue = localStorage.getItem(`Trainer_Data_${val.id}`) || defaultValue;
                        }
                }

                if (val.type === 'input') {
                    questions += '<input type="text" class="form-control" id="' + val.id + '" name="' + val.id + '"';
                    questions += (typeof itemValue !== 'undefined') ? ' value="' + itemValue + '"' : '';
                    questions += (val.disabled) ? ' DISABLED' : '';
                    questions += ' />';

                } else if (val.type === 'select') {
                    questions += '<select class="form-control"  id="' + val.id + '" name="' + val.id + '">';

                    if (val.id === 'question') {
                        // add the security questions into the select box
                        _(LF.StudyDesign.securityQuestions).each((value) => {
                            if (value.display) {
                                questions += `<option value="${value.key}"${value.key === itemValue ? ' SELECTED' : ''}>${strings[value.text]}</option>`;
                            }
                        });
                    } else {
                        val.options.forEach((o) => {
                            let optionValue = (typeof o.value !== 'undefined') ? o.value : o.text,
                                selected = optionValue.toString() === itemValue.toString() ? ' SELECTED' : '';

                            questions += '    <option value="' + optionValue + '"' + selected + '>' + o.text + '</option>';
                        });
                    }
                    questions += '</select>';
                }

                questions += '</div>';
                questions += '<br />';
            });

            this.templateSrc = this.templateSrc.replace(/(id="localVariables">)/, "$1" + questions);
            this.template = _.template(this.templateSrc);

            this.$el.html(this.template(strings));

            LF.Utilities.title(strings.header);
            LF.Utilities.page(this.el);
        }, { namespace: 'TRAINER' });

        return this;
    }

    /**
     * Adds the language options to the language select box
     */
    addLanguagesToSelectBox () {
        let langCode = localStorage.getItem('url_language') || this.trainerData.Configs.defaults.lang,
            lang = langCode.split('-'),
            language = lang[0] || '',
            locale = lang[1] || '',
            localized = LF.strings.match({ namespace: 'CORE', localized: {} }),
            langs = [],
            options = '',
            renderCodes = _.uniq(LF.strings.map((m) => `${m.get('language')}-${m.get('locale')}`));

        if (langCode !== '') {
            LF.Preferred = { language, locale };
        }

        // get the localized names from the resource string files in order of localized language name
        localized.sort((firstComparatorObject, secondComparatorObject) => {
            let firstComparator = firstComparatorObject.get('localized'),
                secondComparator = secondComparatorObject.get('localized');
            return (firstComparator > secondComparator) ? 1 : (firstComparator < secondComparator) ? -1 : 0;
        }).forEach((language) => {
            langs.push({
                localized: language.get('localized'),
                code: `${language.get('language')}-${language.get('locale')}`
            });
        });

        // Append the question options to the select element.
        langs.forEach((value) => {
            if (renderCodes.indexOf(value.code) !== -1) {
                options += '<option value="' + value.code + '"';
                options += (value.code === langCode) ? ' SELECTED' : '';
                options += '>' + value.localized + '</option>';
            }
        });

        this.templateSrc = this.templateSrc.replace(/(<\/select>)/, options + "$1");
    }

    /**
     * Submits the form
     * @param {Event} e Event data.
     */
    submit (e) {
        let lang = this.$('#language').val();

        e.preventDefault();

        if (this.validateForm()) {
            localStorage.setItem('url_language', lang);
            if (lang.length) {
                localStorage.setItem('preferredLanguageLocale', lang);
            }

            this.localVariables.forEach(val => {
                if (val.storage === 'LF.Data') {
                    LF.Data[val.id] = this.$(`#${val.id}`).val();
                    if (val.id === 'answer') {
                        LF.Data[val.id] = hex_sha512(LF.Data[val.id]);
                    }

                } else if (val.storage === 'localStorage') {
                    localStorage.setItem(`Trainer_Data_${val.id}`, this.$(`#${val.id}`).val());
                    localStorage.setItem('activationFlag', true);
                }
            });

            if (!this.submitted) {
                // Prevent the user from submitting the form again.
                this.submitted = true;

                localStorage.setItem('activationFlag', true);

                this.navigate('set_time_zone_activation', true);
            }
        }
    }

    /**
     * Back button
     * @param {Event} e Event data.
     */
    back (e) {
        e.preventDefault();

        LF.Utilities.restartApplication();
    }

    /**
     * Loops through all of the form inputs and runs the items' validation functions
     * @returns {boolean} if the form is valid, return true. Otherwise false.
     */
    validateForm () {
        let pass,
            success = true;

        this.localVariables.forEach((localVar) => {
            if (!!localVar.validation) {
                pass = this.validateInput(`#${localVar.id}`, localVar.validation.regex, localVar.validation.errorKey);

                if (!pass) {
                    success = false;
                }
            }
        });

        return success;
    }
}

COOL.add('LogPadTrainerSetupView', TrainerSetupView);
