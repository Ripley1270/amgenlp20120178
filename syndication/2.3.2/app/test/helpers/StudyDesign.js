import StudyDesign from 'core/classes/StudyDesign';
import { mergeObjects } from 'core/utilities/languageExtensions';
import ObjectRef from 'core/classes/ObjectRef';

// Delicate process of getting trial, without letting core diary settings
// in trial mess up tests
import coreDesign from 'core/assets/study-design';

// ELF tests seem to have a handle on the core version of this function
// so cache it and put it back after including trial
let cachedIsCurrentPhase = LF.Branching.branchFunctions.isCurrentPhase;
import trial from 'trial/index';

LF.Branching.branchFunctions.isCurrentPhase = cachedIsCurrentPhase;

let trialDesign = trial.assets.studyDesign;

let coreRuleLookup = {};

// Remove rules that were part of the core diaries (now in trial).  These are messing up
// some LogPad tests
_.each(coreDesign.rules, (rule) => {
    coreRuleLookup[rule.id] = true;
});

let fullDesign = mergeObjects(coreDesign, trialDesign);
fullDesign.rules = _.filter(fullDesign.rules, (rule) => {
    return typeof coreRuleLookup[rule.id] === 'undefined';
});

let design = _.extend({}, fullDesign, {

    // TEMP HACK for  model validation unit test backward compatibility
    oldStyleValidationErrorMessages: true,

    // The current study version
    studyVersion: '00.01',

    // The current study database version
    studyDbVersion: 0,

    // The default language of the study.
    defaultLanguage: 'en',

    // The default locale of the study
    defaultLocale: 'US',

    // The default log level of the study
    defaultLogLevel: 'ERROR',

    // The default option to ask secret question.
    askSecurityQuestion: true,

    // The maximum number of consecutive unsuccessful logon attempts.
    maxLoginAttempts: 3,

    // The maximum number of consecutive unsuccessful secret question attempts.
    maxSecurityQuestionAttempts: 5,

    // Number of minutes after which the account is automatically re-enabled
    // (where 0 is the account never becomes re-enabled unless a daily reset/override code is used).
    lockoutTime: 5,

    // The amount of time in minutes that a user can be inactive before being logged out.
    sessionTimeout: 30,

    // Configuration for the unlock code feature
    unlockCodeConfig: {
        // Seed code used in calculation for unlock code combined with current date.
        seedCode: 1111,
        // Sets the length of unlock code
        codeLength: 6
    },

    // The amount of time in seconds that the browser can attempt to download a file to the cache before timeout.
    cacheDownloadTimeout: 180,

    // The study Protocol
    studyProtocol: {
        defaultProtocol: 0,
        protocolList: new ObjectRef({
            0: 'TestStudy',
            1: 'Protocol1',
            2: 'Protocol2',
            3: 'Protocol3',
            4: 'Protocol4',
            5: 'Protocol5',
            6: 'Protocol6',
            7: 'Protocol7',
            8: 'Protocol8',
            9: 'Protocol9',
            10: 'Protocol10'
        })
    },

    // The build Protocol so if protocol for build
    protocolBuild: 'TestStudy',

    // The client name
    clientName: 'PHT Corp',

    // The phases used by the study
    studyPhase: {
        SCREENING: 10,
        RANDOMIZATION: 20,
        TREATMENT: 30,
        FOLLOWUP: 40,
        TERMINATION: 999
    },

    // The phase from studyPhase to be used as the phase after Subject Assignment
    subjectAssignmentPhase: 10,

    // The default phase of the study
    defaultPhase: 10,

    // The termination phase of the study
    terminationPhase: 999,

    // The setup code time to live hours
    setupCodeTTLHours: 72,

    // The privacy policy URL
    privacyPolicyBaseUrl: 'http://www.phtcorp.com/privacy-policy/nls/',

    validInputCharacters: 'a-zA-Z0-9\s\.-',

    // Configuration for participant/patient
    participantSettings: {
        participantID: {
            seed: 100,
            steps: 1,
            max: 600
        },
        participantIDFormat: '0000-0000',
        participantNumberPortion: [5, 9],
        siteNumberPortion: [0, 4],
        languageList: [{
            language: 'en',
            locale: 'US',
            localized: 'LANG_EN-US'
        }, {
            language: 'es',
            locale: 'ES',
            localized: 'LANG_ES-ES'
        }]
    },

    // IG & IT for the saving of Role Data associated with Questionnaires
    lastDiaryRole: {
        IG: 'CG',
        IT: 'LPARole'
    },

    // Indicates will we use JSONH for the JSON compression
    jsonh: false,

    environments: [
        {
            id: 'development',
            label: 'Development',
            modes: ['provision'],
            url: 'https://engcorestudy05.phtnetpro.com/development',
            studyDbName: 'engcorestudy05'
        }, {
            id: 'production',
            label: 'Production',
            modes: ['provision', 'depot'],
            url: 'https://engcorestudy05.phtnetpro.com/production',
            studyDbName: 'engcorestudy05'
        }
    ],

    sitePad: {
        loginRoles: ['admin', 'site'],

        // Reference to a filter function to query the admin users for SitePad.
        // If the config value is null, then the default filter is used which is active users with the 'admin' role
        adminUserFilter: null
    },
    roles: [
        {
            id: 'subject',
            lastDiaryRoleCode: 0,
            displayName: 'SUBJECT_ROLE',
            defaultAffidavit: 'DEFAULT',
            product: ['logpad']
        }, {
            id: 'admin',
            lastDiaryRoleCode: 1,
            displayName: 'SITE_ADMINISTRATOR',
            permissions: ['ALL'],
            defaultAffidavit: 'DEFAULT',
            product: ['logpad', 'sitepad']
        }, {
            id: 'site',
            displayName: 'SITE_ROLE',
            lastDiaryRoleCode: 2,
            defaultAffidavit: 'DEFAULT',
            product: ['logpad', 'sitepad']
        }
    ],

    adminUser: {role: 'admin'},

    securityQuestions: [
        {
            text: 'SECURITY_QUESTION_0',
            key: 0,
            display: true
        }
    ],

    visits: [{
        id: 'visit1',
        studyEventId: '10',
        displayName: 'VISIT_1',
        visitType: 'ordered',
        visitOrder: 1,
        forms: ['Daily_Diary', 'Meds'],
        delay: '0'
    }, {
        id: 'visit2',
        studyEventId: '20',
        displayName: 'VISIT_2',
        visitType: 'ordered',
        visitOrder: 2,
        forms: ['Daily_Diary'],
        delay: '0'
    }, {
        id: 'visit3',
        studyEventId: '30',
        displayName: 'VISIT_3',
        visitType: 'ordered',
        visitOrder: 3,
        forms: ['Daily'],
        delay: '0'
    }, {
        id: 'visit4',
        studyEventId: '40',
        displayName: 'VISIT_4',
        visitType: 'ordered',
        visitOrder: 4,
        forms: ['FieldTypeSamples', 'EQ5D'],
        delay: '0'
    }, {
        id: 'visit5',
        studyEventId: '50',
        displayName: 'VISIT_5_NoOrder',
        visitType: 'unscheduled',
        visitOrder: 0,
        forms: ['FieldTypeSamples', 'Daily'],
        delay: '0'
    }, {
        id: 'visit6',
        studyEventId: '60',
        displayName: 'VISIT_6_NoOrder',
        visitType: 'unscheduled',
        visitOrder: 0,
        forms: ['FieldTypeSamples', 'EQ5D'],
        delay: '0'
    }, {
        id: 'visit7',
        studyEventId: '70',
        displayName: 'VISIT_7_Container',
        visitType: 'container',
        visitOrder: 0,
        forms: ['FieldTypeSamples', 'EQ5D'],
        delay: '0'
    }],

    questionnaires: [
        {
            id: 'Daily_Diary',
            SU: 'DAILY_DIARY',
            displayName: 'DISPLAY_NAME',
            className: 'DAILY_DIARY',
            previousScreen: true,
            screens: ['DAILY_DIARY_S_1', 'DAILY_DIARY_S_2', 'DAILY_DIARY_S_3']
        }, {
            id: 'Meds',
            SU: 'MEDS',
            displayName: 'DISPLAY_NAME',
            className: 'Meds',
            previousScreen: true,
            screens: ['MEDS_S_1']
        }
    ],

    screens: [
        {
            id: 'DAILY_DIARY_S_1',
            className: 'DAILY_DIARY_S_1',
            questions: [{
                id: 'DAILY_DIARY_Q_1'
            }]
        }, {
            id: 'DAILY_DIARY_S_2',
            className: 'DAILY_DIARY_S_2',
            questions: [{
                id: 'DAILY_DIARY_Q_1'
            }]
        }, {
            id: 'DAILY_DIARY_S_3',
            className: 'DAILY_DIARY_S_3',
            questions: [{
                id: 'DAILY_DIARY_Q_1'
            }]
        }, {
            id: 'MEDS_S_1',
            className: 'MEDS_S_1',
            questions: [{
                id: 'MEDS_Q_1'
            }]
        }
    ],

    questions: [{
        id: 'DAILY_DIARY_Q_1',
        IG: 'DAILY_DIARY_Q1',
        IT: 'DailyDiary',
        text: 'DD_QUESTION_1',
        className: 'DAILY_DIARY_Q1'
    }, {
        id: 'MEDS_Q_1',
        IG: 'MEDS_Q1',
        IT: 'MedsDiary',
        text: 'MEDS_QUESTION_1',
        className: 'MEDS_Q1'
    }],

    affidavits: [{
        id: 'DEFAULT',
        text: [
            'AFFIDAVIT_HELP',
            'AFFIDAVIT'
        ],
        krSig: 'AFFIDAVIT',
        widget: {
            id: 'AFFIDAVIT_WIDGET',
            className: 'AFFIDAVIT',
            type: 'CheckBox',
            answers: [
                {
                    text: 'OK',
                    value: '0'
                }
            ]
        }
    }],

    supportOptions: [{
        text: 'SUPPORT_NAME_BELGIUM',
        number: ['SUPPORT_NUMBER_BELGIUM_1', 'SUPPORT_NUMBER_BELGIUM_2', 'SUPPORT_NUMBER_BELGIUM_3'],
        display: true
    }],

    visitStatus: {
        1: 'SKIPPED',
        2: 'INCOMPLETE',
        3: 'COMPLETED'
    },

    // Configuration for rater training
    raterTrainingConfig: {
        logpad: {
            useRaterTraining: true,
            awsKey: 'LP-AWS-KEY',
            awsSecretKey: 'LP-AWS-SECRET-KEY',
            awsRegion: 'LP-REGION',
            awsBucket: 'LP-BUCKET',
            rootDirectory: 'LP-ROOT-DIRECTORY',
            fallBackBaseURL: 'LP-URL',
            pages: {
                default: 'LP-DEFAULT'
            },
            zipFile: true
        },
        sitepad: {
            useRaterTraining: true,
            awsKey: 'SP-AWS-KEY',
            awsSecretKey: 'SP-AWS-SECRET-KEY',
            awsRegion: 'SP-REGION',
            awsBucket: 'SP-BUCKET',
            rootDirectory: 'SP-ROOT-DIRECTORY',
            fallBackBaseURL: 'SP-URL',
            pages: {
                default: 'SP-DEFAULT'
            },
            zipFile: true
        }
    }
});

export function resetStudyDesign (studyDesign = design) {
    window.LF.StudyDesign = new StudyDesign(studyDesign);
    return window.LF.StudyDesign;
}

export const defaultStudyDesign = resetStudyDesign();

export default defaultStudyDesign;
