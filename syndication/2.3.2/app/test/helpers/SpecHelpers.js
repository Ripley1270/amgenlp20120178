import Subjects from 'core/collections/Subjects';
import Sites from 'core/collections/Sites';
import SubjectAlarms from 'core/collections/SubjectAlarms';
import Users from 'core/collections/Users';
import Site from 'core/models/Site';
import StoragBase from 'core/models/StorageBase';
import PageView from 'core/views/PageView';
import Data from 'core/Data';
import Logger from 'core/Logger';
import { eCoaDB, LogDB } from 'core/dataAccess';
import DABL from 'core/dataaccess/DABL';
import * as lStorage from 'core/lStorage';
import CurrentContext from 'core/CurrentContext';
import ScheduleManager from 'core/classes/ScheduleManager';
import WebService from 'core/classes/WebService';
import PatientSession from 'core/classes/PatientSession';
import NumericRatingScale from 'core/widgets/NumericRatingScale';
import CurrentSubject from 'core/classes/CurrentSubject';
import 'core/actions';
import * as syncHistoricalData from 'core/actions/syncHistoricalData';
import 'jasmine-ajax';

Logger.setGlobalsForTesting({logToConsole: false});

_.templateSettings = {
    interpolate: /\{\{(.+?)\}\}/g
};

LF.SpecHelpers = {};

export function installDatabase (models) {
    console.warn('SpecHelpers.installDatabase is now a no-op https://goo.gl/4xAH30');
    return Q();
    //return eCoaDB = new DABL({
    //    name: 'eCoaDB'
    //});
};

LF.SpecHelpers.installDatabase = installDatabase;

export function uninstallDatabase () {
    console.warn('SpecHelpers.uninstallDatabase is now a no-op https://goo.gl/4xAH30');
    CurrentSubject.clearSubject();
    return Q();
    //return eCoaDB.drop();
};

LF.SpecHelpers.uninstallDatabase = uninstallDatabase;

let subjects;

export function createSubject() {
    subjects = new Subjects([
        {
            device_id: hex_sha512('123456'),
            initials: 'bc',
            log_level: 'ERROR',
            id: 1,
            phase: 10,
            phaseStartDateTZOffset: '2013-10-05T08:15:30-05:00',
            phaseTriggered: 'false',
            secret_answer: hex_sha512('apple'),
            secret_question: '0',
            site_code: '267',
            subject_id: '12345',
            subject_password: hex_sha512('apple' + 'krpt.39ed120a0981231'),
            service_password: hex_sha512('apple' + 'krpt.39ed120a0981231'),
            subject_number: '54321',
            subject_active: 1,
            krpt: 'krpt.39ed120a0981231',
            enrollmentDate: '2013-10-05T08:15:30-05:00',
            activationDate: '2013-10-05T08:15:30-05:00'
        },
        {
            device_id: hex_sha512('56789'),
            initials: 'fg',
            log_level: 'ERROR',
            id: 2,
            phase: 10,
            phaseStartDateTZOffset: '2013-10-05T08:15:30-05:00',
            phaseTriggered: 'false',
            secret_answer: hex_sha512('apple'),
            secret_question: '0',
            site_code: '267',
            subject_id: '98765',
            subject_password: hex_sha512('apple' + 'krpt.39ed120a0981231'),
            service_password: hex_sha512('apple' + 'krpt.39ed120a0981231'),
            subject_number: '98',
            subject_active: 1,
            krpt: 'krpt.39ed120a0981231',
            enrollmentDate: '2013-10-05T08:15:30-05:00',
            activationDate: '2013-10-05T08:15:30-05:00'
        }
    ]);
};

LF.SpecHelpers.createSubject = createSubject;

export function saveSubject() {
    return Q.all(subjects.map(subject => subject.save()))
        .then(() => subjects);
};

LF.SpecHelpers.saveSubject = saveSubject;

export function triggerDeviceReadyEvent () {
    // Spoof phonegap's device ready event: see https://github.com/ariya/phantomjs/issues/11289
    let evt = document.createEvent('CustomEvent');

    evt.initCustomEvent('deviceready', false, false, null);
    document.dispatchEvent(evt);
};

LF.SpecHelpers.triggerDeviceReadyEvent = triggerDeviceReadyEvent;

LF.SpecHelpers.createSubjectAlarms = function () {
    Data.SubjectAlarms = new SubjectAlarms([
        {
            id: 1,
            schedule_id: 'LogPadSchedule',
            time: '13:00'
        }
    ]);
};

LF.SpecHelpers.saveSubjectAlarms = function () {
    let keys = [],
        length = Data.SubjectAlarms.size(),
        response = null,
        step = function (counter) {
            let alarm = Data.SubjectAlarms.at(counter);

            alarm.save().then(function (r) {
                keys.push(r);

                if (counter + 1 < length) {
                    step(counter + 1);
                } else {
                    response = true;
                    return response;
                }
            });
        };

    step(0);

    waitsFor(function () {
        return response;
    }, 'Subject alarm(s) to be stored.', 1000);

    runs(function () {
        expect(keys.length).toEqual(length);
    });
};

export function createSite() {
    let value = '1111',
        salt = 'apple',
        pass = '0x' + hex_sha512(salt + value),
        siteInfo;

    siteInfo = {
        id: 1,
        siteCode: '2674',
        studyName: salt,
        hash: pass
    };
    Data.Sites = new Sites([siteInfo]);
    Data.site = Data.Sites.at(0);
};

LF.SpecHelpers.createSite = createSite;

export function createUsers() {
    Data.Users = new LF.Collection.Users([
        {
            id: 1,
            username: 'wsmith',
            email: 'aaa@aaa.com',
            role: 'admin',
            userType: 'Admin',
            language: 'en-US'
        },
        {
            id: 2,
            username: 'jsmith',
            email: 'bbb@bbb.com',
            role: 'site',
            userType: 'Admin',
            language: 'es-US'
        },
        {
            id: 3,
            username: 'bobsmith',
            email: 'ccc@ccc.com',
            role: 'site',
            userType: 'Admin',
            language: 'en-CA'
        },
        {
            id: 4,
            username: 'emcgregor',
            email: 'ddd@ddd.com',
            role: 'site',
            userType: 'Admin',
            language: 'fr-CA'
        }
    ]);
};

LF.SpecHelpers.createUsers = createUsers;

export function saveUsers() {
    return Q.all(Data.Users.map(siteuser => siteuser.save()));
};

LF.SpecHelpers.saveUsers = saveUsers;

export function saveSite() {
    return Q.all(Data.Sites.map(site => site.save()));
};

LF.SpecHelpers.saveSite = saveSite;

export function generateLogs (level, total) {
    let requests = [],
        log = new Logger('UnitTests');

    for (let i = 0; i < total; i += 1) {
        requests.push(log[level]('Test log count={{count}} of total={{total}} at level={{level}}', {
            count : i + 1,
            total : total,
            level : level.toUpperCase()
        }));
    }

    return Q.all(requests);
};

/**
 * fakes LF.getStrings via a spyOn() calling the callback asynchronously
 */
export function fakeGetStrings () {
    const fake = (obj, fn = $.noop) => {
        // go fully async
        return Q()
            .then(() => {
                if (typeof obj === 'string') {
                    fn(obj);
                    return obj;
                }
                if (typeof obj === 'object') {
                    if (obj.text) {
                        let newObj = { text: [] };

                        for (let i = 0, len = obj.text.length; i < len; ++i) {
                            newObj.text[i] = obj.text[i].key;
                        }

                        fn(newObj);
                        return newObj;
                    } else {
                        let keys = _.keys(obj),
                            newObj = {};

                        for (let i = 0, len = keys.length; i < len; ++i) {
                            newObj[keys[i]] = obj[keys[i]].key;
                        }
                        fn(newObj);
                        return newObj;
                    }
                }
            });
    };
    spyOn(LF, 'getStrings').and.callFake(fake);
    spyOn(LF.strings, 'display').and.callFake(fake);
}

/**
 * Simulates getStrings() via spyOn().and.callFake on the same thread, for things like error handling tests,
 * which fire off banners/alerts
 * asynchronously
 */
export function fakeSynchronousGetStrings() {
    spyOn(LF, 'getStrings').and.callFake((obj, fn) => {
        if (typeof obj === 'string') {
            fn(obj);
            return;
        }
        if (typeof obj === 'object' && obj.text) {
            let newObj = { text: [] };

            for (let i = 0, len = obj.text.length; i < len; ++i) {
                newObj.text[i] = obj.text[i].key;
            }

            fn(newObj);
            return;
        }
        if (typeof obj === 'object') {
            let keys = _.keys(obj),
                newObj = {};

            for (let i = 0, len = keys.length; i < len; ++i) {
                newObj[keys[i]] = obj[keys[i]].key;
            }
            fn(newObj);
            return;
        }
    });
}

/**
 * Returns an array that contains undefined values in unused array indexes.  Sometimes useful for testing, because:
 *  (new Array())[3] = 'test string' does not equal [undefined, undefined, undefined, 'test string']
 * @param {Array} arr input array.  Note, original array is not modified.
 * @example     expect(fillEmptyArrayValues(myArr), [undefined, undefined, true]);
 */
export function fillEmptyArrayValues(arr) {
    let newArr = [];
    for (let i = 0, len = arr.length; i < len; ++i) {
        newArr[i] = arr[i];
    }
    return newArr;
}

/**
 * Initialize LF.content
 * @return {Q.Promise}
 */
export function initializeContent () {
    if (!LF.webService) {
        LF.webService = new WebService();
    }

    if (!LF.security) {
        LF.security = new PatientSession();
    } else if (!LF.security.checkLogin) {
        LF.security.checkLogin = () => Q(true);
    }

    spyOn(syncHistoricalData, 'syncHistoricalData').and.callFake((params, callback = $.noop) => Q(callback()));
    spyOn(LF.webService, 'getAdminPassword').and.callFake((params) => Q({
        role: 'caregiver',
        hash: '0xee9dab80da2e4165abb19324df1b29a1',
        key: 'hello'
    }));

    spyOn(LF.webService, 'getkrDom').and.callFake(() => Q(null));

    spyOn(LF.webService, 'getAllSubjects').and.callFake((params) => Q([
        {
            subject_id    : '0003004',
            krpt          : 'NP.8a48afe39c534ecd852d1d50deca2add',
            initials      : 'RP',
            language      : 'en-US',
            phase         : 10,
            deleted       : false,
            status        : 'Submitted',
            updated       : '2015-08-24T22:54:35.000Z'
        }
    ]));

    uninstallAjax();
    jasmine.Ajax.install();
    jasmine.Ajax.stubRequest(`${lStorage.getItem('serviceBase')}/api/v1/SWAPI/GetDataClin`)
        .andReturn({responseText: '[]'});

    CurrentContext.init();

    return CurrentContext().fetchUsers()
        .then(() => {
            return CurrentContext().checkUserAuth();
        })
        .then(() => {
            return Users.updateUsers();
        })
        .then(() => {
            LF.schedule = new ScheduleManager();
            uninstallAjax();
        });
}

/**
 * Clear all passed collections from the database in a beforeEach and afterAll
 * @param  {...[Collection]} collections Collections to be cleared from database.
 * @example helpers.clearCollections(Users, Transmissions, Subjects);
 */
export function clearCollections (...collections) {
    let clear = (...collections) => {
        return Q.all(collections.map(c => c.clearStorage()));
    };

    Async.beforeEach(() => clear(...collections));
    Async.afterAll(() => clear(...collections));
}

/**
 * Uninstall jasmine.Ajax
 */
export function uninstallAjax () {
    try {
        // will fail if not installed, just ignore.
        jasmine.Ajax.uninstall();
    } catch (err) {}
}

export function setupAjax (url, response) {
    beforeEach(function () {

        jasmine.Ajax.install();
        jasmine.Ajax.stubRequest(url)
            .andReturn({
                responseText: typeof response === 'string' ? response : JSON.stringify(response)
            });
    });

    afterEach(function () {
        uninstallAjax();
    });
}

export function waitsFor (callback, reason, timeout = 500, interval = 50) {
    let deferred = Q.defer(),
        start = (new Date()).getTime(),
        intervalId;

    intervalId = setInterval(() => {

        try {
            if (callback()) {
                clearInterval(intervalId);
                deferred.resolve();
            } else {
                let now = (new Date()).getTime();

                if (now - start > timeout) {
                    clearInterval(intervalId);
                    deferred.reject(new Error(reason));
                }
            }
        } catch (e) {
            deferred.reject(e);
        }

    }, interval);

    return deferred.promise;
}

/**
 * Find and return an ejs template.
 * @param {string} template The path and name of the template to fetch.
 * @return {string} The stringified template.
 * @example
 * let template = fetchTemplate('logpad/templates/about.ejs');
 */
export function fetchTemplate (template) {
    let temp = window.__html__[template];

    if (temp != null) {
        return temp;
    } else {
        fail(`Unable to find template: "${template}".`);
    }
}

// use like this:
// doSomethingAsync().finally(promiseTimer())
export let promiseTimer = (start = Date.now()) => {
    return () => {
        let millis = Date.now() - start;
        console.log(`${millis}ms`);
    };
};

/**
 * Find render a template to the DOM.
 * @param {string} template The path and name of the template to render to the DOM.
 * @return {function} A function to remove the template from the DOM.
 * @example
 * let removeTemplate;
 * beforeEach(() => removeTemplate = renderTemplateToDOM('logpad/templates/about.ejs'));
 * afterEach(() => removeTemplate());
 */
export function renderTemplateToDOM (template) {
    let html = fetchTemplate(template);

    // Remove any comments and new lines in the stringified HTML.
    html = html.replace(/<!--[\s\S]*?-->/g, '');
    html = html.replace(/^\s+|\s+$/g, '');

    let $html = $(html),
        id = $html.attr('id');

    // Append the template to the <body> element.
    $('body').append($html);

    // Return function to remove template from DOM.
    return () => {
        $(`#${id}`).remove();

        // Clear out the DOM reference to prevent memory leaks.
        $html = null;
    };
}
