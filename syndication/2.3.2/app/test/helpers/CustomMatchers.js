beforeEach(() => {
    jasmine.addMatchers({
        toBePromise: () => {
            return {
                compare: promise => {
                    let result = { pass: Q.isPromise(promise) };

                    if (result.pass) {
                        result.message = 'Expected NOT to be a promise.';
                    } else {
                        result.message = 'Expected to be a promise.';
                    }

                    return result;
                }
            };
        },
        toHaveBeenFulfilled : () => {
            return {
                compare: promise => {
                    let result = { pass: promise.isFulfilled() };

                    if (result.pass) {
                        result.message = 'Expected promise NOT to be resolved';
                    } else {
                        result.message = 'Expected promise to be resolved.';
                    }

                    return result;
                }
            };
        },
        toHaveBeenRejected: () => {
            return {
                compare: promise => {
                    let result = { pass: promise.isRejected() };

                    if (result.pass) {
                        result.message = 'Expected promise NOT to be rejected';
                    } else {
                        result.message = 'Expected promise to be rejected.';
                    }

                    return result;
                }
            };
        },
        toBeArray: () => {
            return {
                compare: value => {
                    let result = { pass: Array.isArray(value) };

                    if (result.pass) {
                        result.message = 'Expected NOT to be an Array.';
                    } else {
                        result.message = 'Expected to be an Array.';
                    }

                    return result;
                }
            };
        }
    });
});
