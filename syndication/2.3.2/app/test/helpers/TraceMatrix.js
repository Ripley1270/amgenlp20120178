/**
 implements the trace matrix tag
 **/

window.TRACE_MATRIX = (traceTag) => {
    const prefix = 'TRACE-MATRIX - ',
        src = document.scripts[document.scripts.length - 1].src.split('?')[0],
        postFix = `: ${src}\r\n`;

    return {
        describe: (description, fn) => {
            return describe(`${prefix}${traceTag}${postFix}${description}`, fn);
        },
        xdescribe: (description, fn) => {
            return xdescribe(`${prefix}${traceTag}${postFix}${description}`, fn);
        },
        fdescribe: (description, fn) => {
            return fdescribe(`${prefix}${traceTag}${postFix}${description}`, fn);
        },
        TRACE_MATRIX: (traceTag1) => {
            return TRACE_MATRIX(`${traceTag}${postFix}${prefix}${traceTag1}}`);
        }
    };
};
