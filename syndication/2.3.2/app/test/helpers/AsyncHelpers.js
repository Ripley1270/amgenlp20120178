/*
 *  This file is centrally imported into every spec run.
 *  It does not need to be explicitly imported, and it can't be
 *  'opted out' of.
 */

// If you want to know whether this file is being included or not, uncomment inside here.
// This should run ONCE per entire suite (e.g. sitepad), not once per spec file.
// So essentially the same as being outside of any block, BUT, it can be asynchronous.
beforeAll(done => {
    // Q().delay(100)
    // .tap(() => console.log('\n\n---------- AsyncHelpers Loaded -----------\n\n'))
    // .done(done);

    done();
});

/*
 * Detect unhandled promise rejections, and fail tests that produce such
 * Yes, beforeEach/afterEach out here works
 * Yes, fail() in afterEach works
 * You can set global tolerateSloppyPromiseHandling = true in your spec
 *    in which case you'll get off with a warning; no ticket :-)
*/

let tolerateSloppyPromiseHandling = true;  // if true, warn to console, but do not fail test
let showQStackTraces = true; // if true print stack traces for violations (up to 3)

// clear before each test
beforeEach(() => Q.resetUnhandledRejections());

// verify after each test
afterEach(() => {
    let badness = Q.getUnhandledReasons();
    if (badness.length) {
        console.error(`Detected ${badness.length} unhandled rejections of Q promises`);
        let stacks = _.chain(badness)
            .reject((reason) => /.no stack. undefined$/i.test(reason))
            .unique()
            .take(4) // cap it at 4 stack traces
            .value();
        if (stacks.length) {
            stacks.forEach((reason, ind) => console.error(`#${ind + 1}: ${reason}`));
        } else {
            console.warn('Sorry, no stack traces available :(');
        }
        Q.resetUnhandledRejections();
    }
});

/**
 * @param {!string} text
 * @param {number} numLines
 * @param {boolean=false} ellipsis
 */
let head = (text, numLines, ellipsis) => {
    let head = text.split('\n').slice(0, numLines).join('\n');
    return ellipsis && head.length < text.length ? head + '\n...' : head;
};

let asyncInner = (kind, body, done) =>
    (done) =>
        Q()
        .then(() => {
            let res = body();
            if (!Q.isPromise(res)) {
                fail([
                    `Async.${kind} body did not return a Promise.`,
                    `Got: ${res}`,
                    `Body:`,
                    `${head(body.toString(), 10, true)}`
                ].join('\n'));
                done();
            }
            return res;
        })
        .catch(e => {
            // Do change the argument to fail(e), i.e. don't format it as a String.
            // Jasmine does special stuff if the argument is an Error,
            // and then Q does something special on top of that;
            // all of which is lost if you format the error as a string.
            fail(e);
        })
        .done(done);

let asyncBlock = (kind, body, timeout) => {
    if (body.length !== 0) {
        throw new Error('Do not use a done callback with Async.*');
    }
    let wrapped = (done) => asyncInner(kind, body, done)(done);
    wrapped.wrapped = true;
    window[kind](wrapped, timeout);
};

let asyncSpec = (kind, name, body, timeout) => {
    if (body.length !== 0) {
        throw new Error('Do not use a done callback with Async.*');
    }
    let wrapped = (done) => asyncInner(kind, body, done)(done);
    wrapped.wrapped = true;
    window[kind](name, wrapped, timeout);
};

let Async = {};

let specs = ['it', 'fit', 'xit'];
specs.forEach((kind) => {
    Async[kind] = (name, body, timeout) => asyncSpec(kind, name, body, timeout);
});

let blocks = ['beforeEach', 'afterEach', 'beforeAll', 'afterAll'];
blocks.forEach((kind) => {
    Async[kind] = (body, timeout) => asyncBlock(kind, body, timeout);
});
window.Async = Async;

const origApi = {
    specs:  {it, fit, xit},
    blocks: {beforeEach, afterEach, beforeAll, afterAll}
};

for (let kind in origApi.specs) {
    let apiFunc = origApi.specs[kind];
    window[kind] = (desc, body, millis) => {
        if (body.wrapped) {
            apiFunc(desc, body, millis);
        } else {
            if (body.length === 1) {
                //console.warn([
                //    '',
                //    `**** Please convert asynchronous spec to Async.${kind} style: '${desc}'`,
                //    `     At some point, this warning will become a test failure.`
                //].join('\n'));
                apiFunc(desc, body, millis);
            } else {
                apiFunc(desc, () => {
                    let res = body();
                    if (Q.isPromise(res)) {
                        fail(`Spec body returned a Promise; must use Async.${kind} instead`);
                    }
                }, millis);
            }
        }
    };
}

for (let kind in origApi.blocks) {
    let apiFunc = origApi.blocks[kind];
    window[kind] = (body, millis) => {
        if (body.wrapped) {
            apiFunc(body, millis);
        } else {
            if (body.length === 1) {
                //console.warn([
                //    '',
                //    `**** Please convert asynchronous ${kind} block to Async.${kind} style:`,
                //    `${head(body.toString(), 10, true)}`,
                //    `     At some point, this warning will become a test failure.`
                //].join('\n'));
                apiFunc(body, millis);
            } else {
                apiFunc(() => {
                    let res = body();
                    if (Q.isPromise(res)) {
                        fail(`${kind} block returned a Promise; must use Async.${kind} instead`);
                    }
                }, millis);
            }
        }
    };
}
