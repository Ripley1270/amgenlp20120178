xdescribe('QR code activation', function () {

    var view,
        location,
        setup_code = '12345678',
        language = 'fr-FR',
        template = '<div id="codeEntry-template">' +
            '<div class="input-textbox">' +
            '<input type="text" id="txtCode" name="txtCode_{{ key }}" />' +
            '</div>' +
            '<button disabled type="submit" id="submitNext" />' +
            '</div>';

    beforeEach(function () {
        // PhantomJS defect causes it to always return false for navigator.online: https://github.com/ariya/phantomjs/issues/10647
        // Once navigator has been replaced, it will no longer update as expected otherwise
        // Remove this workaround once PhantomJS defect is resolved
        if (!navigator.onLine) {
            let fakeNavigator = _.clone(navigator);
            fakeNavigator.onLine = true;
            navigator = fakeNavigator;
        }

        localStorage.setItem('url_wrapped', 'android');
        localStorage.setItem('url_setup_code', setup_code);
        localStorage.setItem('url_language', language);
        localStorage.setItem('url_method', 'qr');

        spyOn(LF.Helpers, 'checkInstall').andCallFake(function (callback) {
            callback(false);
        });

        LF.router = new LF.Router.ActivationRouter();

        spyOn(LF.router, 'navigate').andCallFake(function (url) {
            location = url;
        });

        LF.webService = new LF.Class.WebService();

        location = 'code_entry';

        $('body').append(template);

    });

    afterEach(function () {
        localStorage.clear();

        $('#codeEntry-template').remove();

        LF.router = undefined;
        LF.security = undefined;
        LF.webService = undefined;
        view = undefined;

        LF.Data = {};
        LF.dataAccess = undefined;

        LF.logs.reset();

    });

    it('should navigate to the privacy policy view when a good setup code is passed in from the QR scanner and subject is not active', function () {
        var response;

        fakeAjax({registrations: [
            {
                url         : '../../api/v1/subjects/' + setup_code,
                type        : 'GET',
                success     : {
                    data       : {
                        S : false
                    },
                    status  : 'success',
                    xhr     : {
                        getResponseHeader: function () {}
                    }
                }
            }
        ]});

        view = new LF.View.CodeEntryView();

        runs(function () {
            response = false;

            setTimeout(function () {
                response = true;
            }, 500);
        });

        waitsFor(function () {
            return response;
        }, 'Transmission to complete', 500);

        runs(function () {
            expect(location).toEqual('privacy_policy');
        });

    });

    it('should navigate to the reactivation view when a good setup code is passed in from the QR scanner and subject is active', function () {
        var response;

        fakeAjax({registrations: [
            {
                url         : '../../api/v1/subjects/' + setup_code,
                type        : 'GET',
                success     : {
                    data       : {
                        S : true
                    },
                    status  : 'success',
                    xhr     : {
                        getResponseHeader: function () {}
                    }
                }
            }
        ]});

        view = new LF.View.CodeEntryView();

        runs(function () {
            response = false;

            setTimeout(function () {
                response = true;
            }, 500);
        });

        waitsFor(function () {
            return response;
        }, 'Transmission to complete', 500);

        runs(function () {
            expect(location).toEqual('reactivation');
        });

    });

    it('should stay on the code entry view when a bad setup code is passed in from the QR scanner', function () {
        var response;

        fakeAjax({registrations: [
            {
                url         : '../../api/v1/subjects/' + setup_code,
                type        : 'GET',
                success     : {
                    data       : {},
                    status  : 'success',
                    xhr     : {
                        getResponseHeader: function () {}
                    }
                }
            }
        ]});

        view = new LF.View.CodeEntryView();

        runs(function () {
            response = false;

            setTimeout(function () {
                response = true;
            }, 500);
        });

        waitsFor(function () {
            return response;
        }, 'Transmission to complete', 500);

        runs(function () {
            expect(location).toEqual('code_entry');
        });

    });

});
