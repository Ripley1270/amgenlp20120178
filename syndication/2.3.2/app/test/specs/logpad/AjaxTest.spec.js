xdescribe('AJAX Test', function () {

    it('just works', function () {

        var message = 'Hello ';

        fakeAjax({
            registrations: [
                {
                    url         : '../../Controller.aspx',
                    type        : 'post',
                    data        : {
                        user    : 'dog'
                    },
                    successData : 'World!'
                }
            ]
        });

        $.post('../../Controller.aspx', {
            user    : 'dog'
        }, function (result) {
            message += result;
        });

        expect(message).toEqual('Hello World!');

    });

});
