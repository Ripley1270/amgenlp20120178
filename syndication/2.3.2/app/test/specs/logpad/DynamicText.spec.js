import Languages from 'core/collections/Languages';
import DynamicText from 'core/classes/DynamicText';
import 'test/helpers/StudyDesign';
import 'test/helpers/SpecHelpers';

describe('DynamicText', () => {
    let collection,
        testDynamicText = new DynamicText(),
        prefLanguage =  LF.Preferred.language,
        prefLocale = LF.Preferred.locale,
        defaultLanguage = LF.StudyDesign.defaultLanguage,
        defaultLocale = LF.StudyDesign.defaultLocale;

    beforeEach(() => {
        _.templateSettings = {
            evaluate: /\{\[([\s\S]+?)\]\}/g,
            interpolate : /\{\{(.+?)\}\}/g
        };

        LF.StudyDesign.defaultLanguage = 'en';
        LF.StudyDesign.defaultLocale = 'US';
        LF.Preferred.language = 'fr';
        LF.Preferred.locale = 'FR';

        testDynamicText.add({
            id: 'execute1',
            evaluate: () => 'executed'
        });

        testDynamicText.add({
            id: 'execute2',
            evaluate: () => {
                return collection.display('FETCH4', $.noop, { namespace: 'DIARY' });
            }
        });

        collection = new Languages([
            {
                namespace   : 'CORE',
                language    : 'en',
                locale      : 'US',
                direction   : 'ltr',
                resources   : {
                    FETCH2   : 'US - fetched',
                    DIARY    : 'Core Default',
                    STUDY    : 'Core Default',
                    CORE     : 'Core Default',
                    DIARY2   : 'Core NoTrans',
                    STUDY2   : 'Core NoTrans',
                    CORE2    : 'Core NoTrans'
                }
            }, {
                namespace   : 'DIARY',
                language    : 'fr',
                locale      : 'FR',
                direction   : 'ltr',
                resources   : {
                    TEST1    : 'Another string {{ FETCH1 }}.',
                    TEST2    : 'Another string {{ FETCH2 }}.',
                    TEST3    : 'Another string {{ FETCH3 }}.',
                    TEST4    : 'Function {{{ execute1 }}}.',
                    TEST5    : 'String fetched {{{ execute2 }}}.',
                    TEST6    : 'Nested string {{ FETCH5 }}.',
                    TEST7    : 'Function not found {{{ execute3 }}}.',
                    FETCH1   : 'fetched',
                    FETCH4   : 'from function',
                    FETCH5   : '{{ FETCH6 }}',
                    FETCH6   : 'correctly fetched'
                }
            }
        ], { dynamicTextOverrides: testDynamicText });
    });

    afterEach(() => {
        LF.Preferred.language = prefLanguage;
        LF.Preferred.locale = prefLocale;
        LF.StudyDesign.defaultLanguage = defaultLanguage;
        LF.StudyDesign.defaultLocale = defaultLocale;
        localStorage.clear();
        testDynamicText = new DynamicText();
    });

    Async.it('should fetch another string within same namespace.', () => {
        return collection.display('TEST1', $.noop, { namespace: 'DIARY' })
        .then((string) => {
            expect(string).toEqual('Another string fetched.');
        });
    });

    Async.it('should fallback to CORE and fetch another string.', () => {
        return collection.display('TEST2', $.noop, { namespace: 'DIARY' })
        .then((string) => {
            expect(string).toEqual('Another string US - fetched.');
        });
    });

    Async.it('should not find inner string and should display dynamic text placeholder.', () => {
        return collection.display('TEST3', $.noop, { namespace: 'DIARY' })
        .then((string) => {
            expect(string).toEqual('Another string {{ FETCH3 }}.');
        });
    });

    Async.it('should execute the function and replace the placeholder.', () => {
        return collection.display('TEST4', $.noop, { namespace: 'DIARY' })
        .then((string) => {
            expect(string).toEqual('Function executed.');
        });
    });

    Async.it('should fetch another string from the function and replace the placeholder.', () => {
        return collection.display('TEST5', $.noop, { namespace: 'DIARY' })
        .then((string) => {
            expect(string).toEqual('String fetched from function.');
        });
    });

    Async.it('should fetch nested string from the string and replace the placeholder.', () => {
        return collection.display('TEST6', $.noop, { namespace: 'DIARY' })
        .then((string) => {
            expect(string).toEqual('Nested string correctly fetched.');
        });
    });

    // @todo throws parse error. Investigate and fix.
    xit('should not find function and should display dynamic text placeholder.', () => {
        return collection.display('TEST7', $.noop, { namespace: 'DIARY' })
        .then((string) => {
            expect(string).toEqual('Function not found {{{ execute3 }}}.');
        });
    });

});
