import { syncHistoricalData } from 'core/actions/syncHistoricalData';
import Dashboards from 'core/collections/Dashboards';
import Answers from 'core/collections/Answers';
import Transmissions from 'core/collections/Transmissions';
import WebService from 'core/classes/WebService';
import transmitQuestionnaire from 'core/transmit/transmitQuestionnaire';

describe('Transmit', () => {
    TRACE_MATRIX('US7392').
    describe('transmitQuestionnaire', () => {
        let answer = {
            id: 1,
            subject_id: '12345',
            response: '7',
            SW_Alias: 'EQ5D_Q_1',
            instance_ordinal: 1,
            questionnaire_id: 'EQ5D',
            question_id: 'EQ5D_Q_1'
        };

        beforeEach(() => {
            let now = new Date();

            // jscs:disable requireArrowFunctions
            // Long hand functions used in these cases to retain the context of this to the spied upon objects.
            spyOn(Transmissions.prototype, 'fetch').and.callFake(function () {
                this.add([{
                    id: 1,
                    method: 'transmitQuestionnaire',
                    params: JSON.stringify({ dashboardId: 1, sigId: 'LA.14f7031e9cc352136068949490' }),
                    created: now.getTime()
                }]);

                return Q();
            });

            spyOn(Dashboards.prototype, 'destroy').and.resolve();
            spyOn(Dashboards.prototype, 'fetch').and.callFake(function (params) {
                this.add([{
                    id: 1,
                    subject_id: '12345',
                    instance_ordinal: 1,
                    questionnaire_id: 'EQ5D',
                    completed: now.ISOStamp(),
                    diary_id: now.getTime(),
                    completed_tz_offset: now.getOffset()
                }]);

                params.onSuccess();
                return Q();
            });
            // jscs:enable requireArrowFunctions

            spyOn(WebService.prototype, 'sendDiary').and.callFake((diary, auth, jsonh, onSuccess, onError) => {
                onSuccess(null, {
                    syncID: null,
                    isSubjectActive: 1,
                    isDuplicate: false
                });
            });
        });

        Async.it('should not send T parameter if there is no type set for any answer.', () => {
            spyOn(Answers.prototype, 'fetch').and.callFake(function (params) {
                this.add([answer]);

                params.onSuccess();
                return Q();
            });

            return Transmissions.fetchCollection()
                .then((transmissions) => Q.Promise(resolve => {
                    transmitQuestionnaire.call(transmissions, transmissions.at(0), () => {
                        let diaryPayload = WebService.prototype.sendDiary.arguments[0];

                        expect(diaryPayload.A[0].T).not.toBeDefined();
                        resolve();
                    });
                }));
        });

        Async.it('should not use JSONH compression send T parameter if there is exists type value set for any answer.', () => {
            spyOn(Answers.prototype, 'fetch').and.callFake(function (params) {
                this.add([_.extend(answer, { type: 'typeValue' })]);

                params.onSuccess();
                return Q();
            });

            return Transmissions.fetchCollection()
                .then((transmissions) => Q.Promise(resolve => {
                    transmitQuestionnaire.call(transmissions, transmissions.at(0), () => {
                        let diaryPayload = WebService.prototype.sendDiary.arguments[0],
                            jsonhParameter = WebService.prototype.sendDiary.arguments[2];

                        expect(typeof diaryPayload.A[0]).toEqual('object');
                        expect(jsonhParameter).toEqual(false);
                        resolve();
                    });
                }));
        });

        Async.it('should send T parameter if there is exists type value set for any answer.', () => {
            spyOn(Answers.prototype, 'fetch').and.callFake(function (params) {
                this.add([_.extend(answer, { type: 'typeValue' })]);

                params.onSuccess();
                return Q();
            });

            return Transmissions.fetchCollection()
                .then((transmissions) => Q.Promise(resolve => {
                    transmitQuestionnaire.call(transmissions, transmissions.at(0), () => {
                        let diaryPayload = WebService.prototype.sendDiary.arguments[0];

                        expect(diaryPayload.A[0].T).toEqual('typeValue');
                        resolve();
                    });
                }));
        });
    });
});

xdescribe('Transmit - OLD', function () {

    var answers,
        dashboards,
        now = new Date(),
        collection = new LF.Collection.Transmissions(),
        logCollection = null, // no such: new LF.Collection.BatchLogs(),
        transmissions,
        logTransmissions,
        jsonh,
        logModel = new LF.Model.Log({
            id      : 1,
            jsonrpc : '2.0',
            method  : 'logEntry',
            params  : 'abc'
        });

    // Even though the describe is x'ed out, this code runs, and causes errors
    // so commented out:
    // No such: LF.Collection.BatchLogs
    //logTransmissions = new LF.Collection.BatchLogs([{
    //    id      : 1,
    //    method  : 'logEntry'
    //}, {
    //    id      : 2,
    //    method  : 'logEntry'
    //}, {
    //    id      : 3,
    //    method  : 'logEntry'
    //}]);

    transmissions = new LF.Collection.Transmissions([{
        id          : 1,
        method      : 'transmitQuestionnaire',
        params      : JSON.stringify({ dashboardId: 1, sigId: 'LA.14f7031e9cc352136068949490'}),
        created     : now.getTime()
    }, {
        id          : 2,
        method      : 'transmitQuestionnaire',
        params      : JSON.stringify({ dashboardId: 2, sigId: 'LA.24f7031e9cc352136068949490'}),
        created     : now.getTime()
    }, {
        id          : 3,
        method      : 'transmitQuestionnaire',
        params      : JSON.stringify({ dashboardId: 3, sigId: 'LA.34f7031e9cc352136068949490'}),
        created     : now.getTime()
    }, {
        id          : 4,
        method      : 'resetPassword',
        params      : '4',
        created     : now.getTime()
    }, {
        id          : 5,
        method      : 'resetPassword',
        params      : '5',
        created     : now.getTime()
    }, {
        id          : 6,
        method      : 'resetPassword',
        params      : '6',
        created     : now.getTime()
    }, {
        id          : 7,
        method      : 'resetPassword',
        params      : '7',
        created     : now.getTime()
    }, {
        id          : 8,
        method      : 'resetSecretQuestion',
        params      : '8',
        created     : now.getTime()
    }, {
        id          : 9,
        method      : 'resetSecretQuestion',
        params      : '9',
        created     : now.getTime()
    }, {
        id          : 10,
        method      : 'resetSecretQuestion',
        params      : '10',
        created     : now.getTime()
    }, {
        id          : 11,
        method      : 'resetSecretQuestion',
        params      : '11',
        created     : now.getTime()
    }]);

    beforeEach(function () {

        LF.webService = new LF.Class.WebService();

        LF.SpecHelpers.createSubject();

        LF.security = new LF.Class.ApplicationSecurity();

        jsonh = LF.StudyDesign.jsonh;

        dashboards = new LF.Collection.Dashboards([
            {
                id                  : 1,
                subject_id          : '12345',
                instance_ordinal    : 1,
                questionnaire_id    : 'EQ5D',
                completed           : now.ISOStamp(),
                diary_id            : now.getTime(),
                completed_tz_offset : now.getOffset()
            }, {
                id                  : 2,
                subject_id          : '12345',
                instance_ordinal    : 2,
                questionnaire_id    : 'EQ5D',
                completed           : now.ISOStamp(),
                diary_id            : now.getTime(),
                completed_tz_offset : now.getOffset()
            }, {
                id                  : 3,
                subject_id          : '12345',
                instance_ordinal    : 3,
                questionnaire_id    : 'EQ5D',
                completed           : now.ISOStamp(),
                diary_id            : now.getTime(),
                completed_tz_offset : now.getOffset()
            }
        ]);

        answers = new LF.Collection.Answers([
            {
                id                  : 1,
                subject_id          : '12345',
                response            : '7',
                SW_Alias            : 'EQ5D_Q_1',
                instance_ordinal    : 1,
                questionnaire_id    : 'EQ5D',
                question_id         : 'EQ5D_Q_1'
            }, {
                id                  : 2,
                subject_id          : '12345',
                response            : '7',
                SW_Alias            : 'EQ5D_Q_1',
                instance_ordinal    : 2,
                questionnaire_id    : 'EQ5D',
                question_id         : 'EQ5D_Q_1'
            }, {
                id                  : 3,
                subject_id          : '12345',
                response            : '7',
                SW_Alias            : 'EQ5D_Q_1',
                instance_ordinal    : 3,
                questionnaire_id    : 'EQ5D',
                question_id         : 'EQ5D_Q_1'
            }
        ]);

        LF.studyWebService.getLastDiary = function (params, onSuccess, onError) {
            var ajaxConfig = {
                type    : 'GET',
                uri     : '../../api/v1/LastDiary/' + '12345678' + '?fields=Daily'
            };

            this.transmit(ajaxConfig, {}, onSuccess, onError);
        };

        spyOn(LF.webService, 'getkrDom').and.callFake(() => Q(null));
        spyOn(LF.Collection.Users.prototype, 'syncAdminPassword').and.callFake(() => Q());

    });

    afterEach(function () {

        LF.webService = undefined;
        LF.security = undefined;
        localStorage.clear();
        LF.logs.reset();
        LF.Data = {};
        LF.StudyDesign.jsonh = jsonh;
        LF.studyWebService.getLastDiary = undefined;

    });

    it('should install database', function () {
        LF.SpecHelpers.installDatabase();
    });

    it('should save all the Answer records in the database.', function () {

        var keys = [],
            length = answers.size(),
            response = null,
            step = function (counter) {

                answers.at(counter).save({}, {
                    onSuccess: function (r) {
                        keys.push(r);
                        if (counter + 1 < length) {
                            step(counter + 1);
                        } else {
                            response = true;
                        }
                    }
                });
            };

        step(0);

        waitsFor(function () {
            return response;
        }, 'Answer record(s) to be stored.', 1000);

        runs(function () {
            expect(keys.length).toEqual(length);
        });

    });

    it('should save all the Dashboard records in the database.', function () {

        var response = null,
            length = dashboards.size(),
            keys = [],
            step = function (counter) {
                dashboards.at(counter).save({ }, {
                    onSuccess: function (r) {
                        keys.push(r);
                        if (counter + 1 < length) {
                            step(counter + 1);
                        } else {
                            response = true;
                        }
                    }
                });
            };

        step(0);

        waitsFor(function () {
            return response;
        }, 'Dashboard record(s) to be stored.', 1000);

        runs(function () {
            expect(keys.length).toEqual(length);
        });

    });

    it('should save all the Transmission records in the database.', function () {

        var response = null,
            length = transmissions.size(),
            keys = [],
            step = function (counter) {

                transmissions.at(counter).save({}, {
                    onSuccess: function (r) {
                        keys.push(r);
                        if (counter + 1 < length) {
                            step(counter + 1);
                        } else {
                            response = true;
                        }
                    }
                });

            };

        step(0);

        waitsFor(function () {
            return response;
        }, 'Transmission record(s) to be stored.', 2000);

        runs(function () {
            expect(keys.length).toEqual(length);
        });

    });

    it('should save all the Batch Log records in the database.', function () {

        var response = null,
            length = logTransmissions.size(),
            keys = [],
            step = function (counter) {

                logTransmissions.at(counter).save({}, {
                    onSuccess: function (r) {
                        keys.push(r);
                        if (counter + 1 < length) {
                            step(counter + 1);
                        } else {
                            response = true;
                        }
                    }
                });

            };

        step(0);

        waitsFor(function () {
            return response;
        }, 'Batch Log record(s) to be stored.', 2000);

        runs(function () {
            expect(keys.length).toEqual(length);
        });

    });

    it('should pull all the transmissions from the database(collection).', function () {

        var response;

        collection.pullQueue(function (res) {
            response = res;
        });

        waitsFor(function () {
            return response;
        }, 'Transmission queue to be pulled.', 1000);

        runs(function () {
            expect(collection.size()).toEqual(11);
        });

    });

    it('should pull all the batch logs from the database(collection).', function () {

        var response;

        logCollection.pullQueue(function (res) {
            response = res;
        });

        waitsFor(function () {
            return response;
        }, 'Batch Log queue to be pulled.', 1000);

        runs(function () {
            expect(logCollection.size()).toEqual(3);
        });

    });

    it('should not transmit a questionnaire due to \'HTTP_SERVER_ERROR\' and remove it from the queue.', function () {

        var response;

        fakeAjax({registrations: [{
            url         : '../../api/v1/diaries/',
            type        : 'POST',
            error       : {
                status: 'error',
                xhr: {
                    getResponseHeader: function (id) { },
                    status : LF.ServiceErr.HTTP_SERVER_ERROR
                }
            }
        }]
        });

        collection.execute(0, function () {
            response = true;
        });

        waitsFor(function () {
            return response;
        }, 'the diary transmission to execute.', 1000);

        runs(function () {
            expect(collection.size()).toEqual(10);
        });

    });

    it('should successfully transmit a questionnaire and remove it from the queue.', function () {

        var response;

        LF.StudyDesign.jsonh = true;

        fakeAjax({registrations: [{
            url         : '../../api/v1/diaries/',
            type        : 'POST',
            success     : {
                data    : { },
                status  : 'success',
                xhr     : {
                    getResponseHeader: function (id) {
                        if (id === 'X-Device-Status') {
                            return '1';
                        } else if (id === 'X-Sync-ID') {
                            return '13';
                        }
                    },
                    status : 201
                }
            }
        }]
        });

        collection.execute(0, function () {
            response = true;
        });

        waitsFor(function () {
            return response;
        }, 'the diary transmission to execute.', 1000);

        runs(function () {
            expect(collection.size()).toEqual(9);
        });

    });

    it('should transmit a questionnaire without requerying the database and also update subject_active', function () {

        var response;

        fakeAjax({registrations: [{
            url         : '../../api/v1/diaries/',
            type        : 'POST',
            success     : {
                data    : { },
                status  : 'success',
                xhr     : {
                    getResponseHeader: function (id) {
                        if (id === 'X-Device-Status') {
                            return '0';
                        } else if (id === 'X-Sync-ID') {
                            return '13';
                        }
                    },
                    status : 201
                }
            }
        }]
        });

        collection.execute(0, function () {
            response = true;
        });

        waitsFor(function () {
            return response;
        }, 'the diary transmission to execute.', 2000);

        runs(function () {
            expect(collection.size()).toEqual(8);
            expect(LF.Data.Subjects.at(0).get('subject_active')).toEqual(0);
        });

    });

    it('should not do a subject sync due to subject deactivation and should trigger \'Termination\' rule.', function () {

        var response,
            trigger,
            subject = LF.Data.Subjects.at(0);

        // Listen for the Termination action to be activated
        spyOn(LF.StudyRules, 'activate').andCallFake(function (ruleTrigger) {
            trigger = ruleTrigger;
            response = true;
        });

        fakeAjax({registrations: [{
            url         : '../../api/v1/subjects/' + subject.get('subject_id') + '\\?fields=Phase,PhaseStartDateTZOffset,Initials,SiteCode,LogLevel,SubjectNumber',
            type        : 'GET',
            error       : {
                status: 'error',
                xhr: {
                    getResponseHeader: function (id) {
                        if (id === 'X-Error-Code') {
                            return '6';
                        }
                    },
                    status : LF.ServiceErr.HTTP_FORBIDDEN

                }
            }
        }]
        });

        LF.Actions.subjectSync();

        waitsFor(function () {
            return response;
        }, 'the subjectSync transmission to execute.', 1000);

        runs(function () {
            expect(collection.size()).toEqual(8);
            expect(trigger).toEqual('Termination');
            expect(LF.StudyRules.activate).toHaveBeenCalled();
        });

    });

    it('should not do a subject sync due to \'HTTP_SERVER_ERROR\' and queue size should remain same.', function () {

        var response,
            subject = LF.Data.Subjects.at(0);

        fakeAjax({registrations: [{
            url         : '../../api/v1/subjects/' + subject.get('subject_id') + '\\?fields=Phase,PhaseStartDateTZOffset,Initials,SiteCode,LogLevel,SubjectNumber',
            type        : 'GET',
            error       : {
                status: 'error',
                xhr: {
                    getResponseHeader: function (id) { },
                    status : LF.ServiceErr.HTTP_SERVER_ERROR
                }
            }
        }]
        });

        LF.Actions.subjectSync({}, function () {
            response = true;
        });

        waitsFor(function () {
            return response;
        }, 'the subjectSync transmission to execute.', 1000);

        runs(function () {
            expect(collection.size()).toEqual(8);
        });

    });

    it('should do subject sync with no resulting DCF and queue size should remain same.', function () {

        var response,
            setupCode = LF.Data.Subjects.at(0).get('subject_id');

        fakeAjax({registrations: [{
            url         : '../../api/v1/subjects/' + setupCode + '\\?fields=Phase,PhaseStartDateTZOffset,Initials,SiteCode,LogLevel,SubjectNumber',
            type        : 'GET',
            success     : {
                data    : {},
                status  : 'success',
                xhr     : {
                    getResponseHeader: function (id) {
                        if (id === 'X-Device-Status') {
                            return '1';
                        } else if (id === 'X-Sync-ID') {
                            return '13';
                        }
                    },
                    status : 200
                }
            }
        }]
        });

        LF.Actions.subjectSync({}, function () {
            response = true;
        });

        waitsFor(function () {
            return response;
        }, 'The subject sync record to be created and transmitted', 1000);

        runs(function () {
            expect(localStorage.getItem('PHT_syncID')).toEqual('13');
            expect(collection.size()).toEqual(8);

        });

    });

    it('should do subject sync with a resulting DCF change to site_code and queue size should remain same.', function () {

        var response,
            subject = LF.Data.Subjects.at(0);

        fakeAjax({registrations: [{
            url         : '../../api/v1/subjects/' + subject.get('subject_id') + '\\?fields=Phase,PhaseStartDateTZOffset,Initials,SiteCode,LogLevel,SubjectNumber',
            type        : 'GET',
            success     : {
                data    : { C: '007' },
                status  : 'success',
                xhr     : {
                    getResponseHeader: function (id) {
                        if (id === 'X-Device-Status') {
                            return '1';
                        } else if (id === 'X-Sync-ID') {
                            return '13';
                        }
                    },
                    status : 200
                }
            }
        }]
        });

        LF.Actions.subjectSync({}, function () {
            response = true;
        });

        waitsFor(function () {
            return response;
        }, 'The subject sync record to be created and transmitted', 1000);

        runs(function () {
            expect(collection.size()).toEqual(8);
            expect(subject.get('site_code')).toBeTruthy('007');
            expect(subject.get('subject_number')).toEqual('54321');
            expect(subject.get('phase')).toEqual(10);
            expect(subject.get('initials')).toEqual('bc');
            expect(localStorage.getItem('PHT_syncID')).toEqual('13');
        });

    });

    it('should not change the subject\'s password due to subject deactivation and should trigger \'Termination\' rule.', function () {

        var response,
            trigger,
            subject = LF.Data.Subjects.at(0);

        // Listen for the Termination action to be activated
        spyOn(LF.StudyRules, 'activate').andCallFake(function (ruleTrigger) {
            trigger = ruleTrigger;
            response = true;
        });

        fakeAjax({registrations: [{
            url         : '../../api/v1/devices/' + subject.get('device_id'),
            type        : 'PUT',
            error       : {
                status: 'error',
                xhr: {
                    getResponseHeader: function (id) {
                        if (id === 'X-Error-Code') {
                            return '6';
                        }
                    },
                    status : LF.ServiceErr.HTTP_FORBIDDEN

                }
            }
        }]
        });

        collection.execute(0, function () { });

        waitsFor(function () {
            return response;
        }, 'the subjectSync transmission to execute.', 1000);

        runs(function () {
            expect(collection.size()).toEqual(8);
            expect(trigger).toEqual('Termination');
            expect(LF.StudyRules.activate).toHaveBeenCalled();
        });

    });

    it('should not change the subject\'s password due to \'HTTP_SERVER_ERROR\' and remove from the queue.', function () {

        var response,
            subject = LF.Data.Subjects.at(0);

        fakeAjax({registrations: [{
            url         : '../../api/v1/devices/' + subject.get('device_id'),
            type        : 'PUT',
            error       : {
                status: 'error',
                xhr: {
                    getResponseHeader: function (id) {
                        if (id === 'X-Error-Code') {
                            return '6';
                        }
                    },
                    status : LF.ServiceErr.HTTP_SERVER_ERROR
                }
            }
        }]
        });

        collection.execute(0, function () {
            response = true;
        });

        setTimeout(function () {
            $.mobile.sdCurrentDialog.butObj[0].click();
        }, 50);

        waitsFor(function () {
            return response;
        }, 'the subjectSync transmission to execute.', 1000);

        runs(function () {
            expect(collection.size()).toEqual(7);
        });

    });

    it('should not change the subject\'s password due to device replacement and remove from the queue.', function () {

        var response,
            subject = LF.Data.Subjects.at(0);

        fakeAjax({registrations: [{
            url         : '../../api/v1/devices/' + subject.get('device_id'),
            type        : 'PUT',
            error       : {
                status: 'error',
                xhr: {
                    getResponseHeader: function (id) {
                        if (id === 'X-Device-Status') {
                            return '0';
                        }
                    },
                    status : LF.ServiceErr.HTTP_SERVER_ERROR
                }
            }
        }]
        });

        collection.execute(0, function () {
            response = true;
        });

        setTimeout(function () {
            $.mobile.sdCurrentDialog.butObj[0].click();
        }, 50);

        waitsFor(function () {
            return response;
        }, 'the subjectSync transmission to execute.', 1000);

        runs(function () {
            expect(collection.size()).toEqual(6);
        });

    });

    it('should not change the subject\'s password with no Internet connection and remove from the queue.', function () {

        var response,
            subject = LF.Data.Subjects.at(0);

        fakeAjax({registrations: [{
            url         : '../../api/v1/devices/' + subject.get('device_id'),
            type        : 'PUT',
            error       : {
                status: 'error',
                xhr: {
                    getResponseHeader: function (id) {
                        if (id === 'X-Device-Status') {
                            return '1';
                        }
                    },
                    status : LF.ServiceErr.HTTP_NOT_FOUND
                }
            }
        }]
        });

        collection.execute(0, function () {
            response = true;
        });

        waitsFor(function () {
            return response;
        }, 'the subjectSync transmission to execute.', 1000);

        runs(function () {
            expect(collection.size()).toEqual(5);
        });

    });

    it('should successfully change the subject\'s password and remove from the queue.', function () {

        var response,
            subject = LF.Data.Subjects.at(0);

        fakeAjax({registrations: [{
            url         : '../../api/v1/devices/' + subject.get('device_id'),
            type        : 'PUT',
            success     : {
                data    : {
                    W : 'asdf'
                },
                status  : 'success',
                xhr     : {
                    getResponseHeader: function (id) {
                        if (id === 'X-Device-Status') {
                            return '1';
                        } else if (id === 'X-Sync-ID') {
                            return '13';
                        }
                    },
                    status : 201
                }
            }
        }]
        });

        collection.execute(0, function () {
            response = true;
        });

        waitsFor(function () {
            return response;
        }, 'the password transmission to execute.', 1000);

        runs(function () {
            expect(collection.size()).toEqual(4);
            expect(subject.get('service_password')).toEqual('asdf');
        });

    });

    it('should not change the subject\'s Secret Question due to subject deactivation and should trigger \'Termination\' rule.', function () {

        var response,
            trigger,
            subject = LF.Data.Subjects.at(0);

        // Listen for the Termination action to be activated
        spyOn(LF.StudyRules, 'activate').andCallFake(function (ruleTrigger) {
            trigger = ruleTrigger;
            response = true;
        });

        fakeAjax({registrations: [{
            url         : '../../api/v1/devices/' + subject.get('device_id'),
            type        : 'PUT',
            error       : {
                status: 'error',
                xhr: {
                    getResponseHeader: function (id) {
                        if (id === 'X-Error-Code') {
                            return '6';
                        }
                    },
                    status : LF.ServiceErr.HTTP_FORBIDDEN

                }
            }
        }]
        });

        collection.execute(0, function () { });

        waitsFor(function () {
            return response;
        }, 'the subjectSync transmission to execute.', 1000);

        runs(function () {
            expect(collection.size()).toEqual(4);
            expect(trigger).toEqual('Termination');
            expect(LF.StudyRules.activate).toHaveBeenCalled();
        });

    });

    it('should not change the subject\'s Secret Question due to \'HTTP_SERVER_ERROR\' and remove it from the queue.', function () {

        var response,
            subject = LF.Data.Subjects.at(0);

        fakeAjax({registrations: [{
            url         : '../../api/v1/devices/' + subject.get('device_id'),
            type        : 'PUT',
            error       : {
                status: 'error',
                xhr: {
                    getResponseHeader: function (id) {
                        if (id === 'X-Error-Code') {
                            return '6';
                        }
                    },
                    status : LF.ServiceErr.HTTP_SERVER_ERROR
                }
            }
        }]
        });

        collection.execute(0, function () {
            response = true;
        });

        setTimeout(function () {
            $.mobile.sdCurrentDialog.butObj[0].click();
        }, 50);

        waitsFor(function () {
            return response;
        }, 'the subjectSync transmission to execute.', 1000);

        runs(function () {
            expect(collection.size()).toEqual(3);
        });

    });

    it('should not change the subject\'s Secret Question due to device replacement and remove from the queue.', function () {

        var response,
            subject = LF.Data.Subjects.at(0);

        fakeAjax({registrations: [{
            url         : '../../api/v1/devices/' + subject.get('device_id'),
            type        : 'PUT',
            error       : {
                status: 'error',
                xhr: {
                    getResponseHeader: function (id) {
                        if (id === 'X-Device-Status') {
                            return '0';
                        }
                    },
                    status : LF.ServiceErr.HTTP_SERVER_ERROR
                }
            }
        }]
        });

        collection.execute(0, function () {
            response = true;
        });

        setTimeout(function () {
            $.mobile.sdCurrentDialog.butObj[0].click();
        }, 50);

        waitsFor(function () {
            return response;
        }, 'the subjectSync transmission to execute.', 1000);

        runs(function () {
            expect(collection.size()).toEqual(2);
        });

    });

    it('should not change the subject\'s Secret Question with no Internet connection and remove from the queue.', function () {

        var response,
            subject = LF.Data.Subjects.at(0);

        fakeAjax({registrations: [{
            url         : '../../api/v1/devices/' + subject.get('device_id'),
            type        : 'PUT',
            error       : {
                status: 'error',
                xhr: {
                    getResponseHeader: function (id) {
                        if (id === 'X-Device-Status') {
                            return '1';
                        }
                    },
                    status : LF.ServiceErr.HTTP_NOT_FOUND
                }
            }
        }]
        });

        collection.execute(0, function () {
            response = true;
        });

        waitsFor(function () {
            return response;
        }, 'the subjectSync transmission to execute.', 1000);

        runs(function () {
            expect(collection.size()).toEqual(1);
        });

    });

    it('should successfully change the subject\'s Secret Question and remove from the queue.', function () {

        var response,
            subject = LF.Data.Subjects.at(0);

        fakeAjax({registrations: [{
            url         : '../../api/v1/devices/' + subject.get('device_id'),
            type        : 'PUT',
            success     : {
                status  : 'success',
                xhr     : {
                    getResponseHeader: function (id) {
                        if (id === 'X-Device-Status') {
                            return '1';
                        } else if (id === 'X-Sync-ID') {
                            return '13';
                        }
                    },
                    status : 201
                }
            }
        }]
        });

        collection.execute(0, function () {
            response = true;
        });

        waitsFor(function () {
            return response;
        }, 'the password transmission to execute.', 1000);

        runs(function () {
            expect(collection.size()).toEqual(0);
        });

    });

    it('should transmit logs and update subject_active', function () {

        var response;

        spyOn(LF.Actions, 'deleteLogs');

        fakeAjax({registrations: [{
            url         : '../../api/v1/logs/',
            type        : 'POST',
            success     : {
                data    : { },
                status  : 'success',
                xhr     : {
                    getResponseHeader: function (id) {
                        if (id === 'X-Device-Status') {
                            return '0';
                        } else if (id === 'X-Sync-ID') {
                            return '13';
                        }
                    },
                    status : 201
                }
            }
        }]
        });

        LF.Transmit.logEntry.call(logCollection, new LF.Model.BatchLog({
            id     : 1,
            method : 'logEntry',
            params : logModel
        }), function () {
            response = true;
        });

        waitsFor(function () {
            return response;
        }, 'the logEntry transmission to execute.', 1000);

        runs(function () {
            expect(logCollection.size()).toEqual(2);
            expect(LF.Actions.deleteLogs).toHaveBeenCalled();
            expect(LF.Data.Subjects.at(0).get('subject_active')).toEqual(0);
        });

    });

    it('should transmit logs', function () {

        var response;

        spyOn(LF.Actions, 'deleteLogs');

        fakeAjax({registrations: [{
            url         : '../../api/v1/logs/',
            type        : 'POST',
            success     : {
                data    : { },
                status  : 'success',
                xhr     : {
                    getResponseHeader: function (id) {
                        if (id === 'X-Device-Status') {
                            return '1';
                        } else if (id === 'X-Sync-ID') {
                            return '13';
                        }
                    },
                    status : 201
                }
            }
        }]
        });

        LF.Transmit.logEntry.call(logCollection, new LF.Model.BatchLog({
            id     : 2,
            method : 'logEntry',
            params : logModel
        }), function () {
            response = true;
        });

        waitsFor(function () {
            return response;
        }, 'the logEntry transmission to execute.', 1000);

        runs(function () {
            expect(logCollection.size()).toEqual(1);
            expect(LF.Actions.deleteLogs).toHaveBeenCalled();
        });

    });

    it('should pack answers collection', function () {

        var packed = JSONH.pack(answers.toJSON()),
            response = [7, 'id', 'subject_id', 'response', 'SW_Alias', 'instance_ordinal',
                'questionnaire_id', 'question_id', 1, '12345', '7', 'EQ5D_Q_1', 1, 'EQ5D',
                'EQ5D_Q_1', 2, '12345', '7', 'EQ5D_Q_1', 2, 'EQ5D', 'EQ5D_Q_1', 3,
                '12345', '7', 'EQ5D_Q_1', 3, 'EQ5D', 'EQ5D_Q_1'];

        expect(packed).toEqual(response);
    });

    it('should unpack answers collection', function () {

        var packed = [7, 'response', 'id', 'subject_id', 'SW_Alias', 'instance_ordinal',
                'questionnaire_id', 'question_id', '7', 1, '12345', 'EQ5D_Q_1', 1, 'EQ5D',
                'EQ5D_Q_1', '7', 2, '12345', 'EQ5D_Q_1', 2, 'EQ5D', 'EQ5D_Q_1', '7', 3,
                '12345', 'EQ5D_Q_1', 3, 'EQ5D', 'EQ5D_Q_1'],
            response = JSONH.unpack(packed);

        expect(response).toEqual(answers.toJSON());
    });

    it('should not transmit logs due to \'HTTP_SERVER_ERROR\'', function () {

        var response;

        spyOn(LF.Actions, 'deleteLogs');

        fakeAjax({registrations: [{
            url         : '../../api/v1/logs/',
            type        : 'POST',
            error       : {
                status: 'error',
                xhr: {
                    getResponseHeader: function (id) { },
                    status : LF.ServiceErr.HTTP_SERVER_ERROR
                }
            }
        }]
        });

        LF.Transmit.logEntry.call(logCollection, new LF.Model.BatchLog({
            id     : 3,
            method : 'logEntry',
            params : logModel
        }), function () {
            response = true;
        });

        waitsFor(function () {
            return response;
        }, 'the logEntry transmission to execute.', 1000);

        runs(function () {
            expect(logCollection.size()).toEqual(0);
        });

    });

    it('should clear all the transmissions from the database(collection).', function () {

        var response;

        collection.clear({
            onSuccess: function () {
                response = true;
            }});

        waitsFor(function () {
            return response;
        }, 'Transmission queue to be pulled.', 1000);

        runs(function () {
            expect(collection.size()).toEqual(0);
        });

    });

    it('should execute historical data sync record in the transmission queue and trigger a rule', function () {
        var response;

        fakeAjax({registrations: [{
            url         : '../../api/v1/LastDiary/' + '12345678' + '\\?fields=Daily',
            type        : 'GET',
            success     : {
                data    : { data: '12-11-2014' },
                status  : 'success',
                xhr     : {
                    getResponseHeader: function (id) {
                        if (id === 'X-Device-Status') {
                            return '0';
                        }
                    },
                    status : 200
                }
            }}]
        });

        LF.StudyRules.add({
            id          : 'TestLastDiarySync',
            trigger     : [
                'HISTORICALDATASYNC:Received/LastDiaries'
            ],
            expression  : function (filter, resume) {
                expect(filter.res.data).toEqual('12-11-2014');
                expect(filter.collection).toBeDefined();
                resume(true);
            },
            actionData  : [
                {
                    trueAction: function (filter, resume) {
                        resume();
                    }
                }
            ]
        });

        syncHistoricalData({
            collection          :  'LastDiaries',
            webServiceFunction  :  'getLastDiary'
        }, function () {
            response = true;
        });

        waitsFor(function () {
            return response;
        }, 'the historical data transmission to execute', 2000);

        runs(function () {
            expect(collection.size(0)).toEqual(0);
            LF.StudyRules.remove({id: 'TestLastDiarySync'});
        });
    });

    it('should execute historical data sync record in the transmission queue and not trigger a rule due to empty result from server', function () {
        var response;

        fakeAjax({registrations: [{
            url         : '../../api/v1/LastDiary/' + '12345678' + '\\?fields=Daily',
            type        : 'GET',
            success     : {
                data    : { },
                status  : 'success',
                xhr     : {
                    getResponseHeader: function (id) {
                        if (id === 'X-Device-Status') {
                            return '0';
                        }
                    },
                    status : 200
                }
            }}]
        });

        spyOn(LF.StudyRules, 'activate');

        syncHistoricalData({
            collection          :  'LastDiaries',
            webServiceFunction  :  'getLastDiary'
        }, function () {
            response = true;
        });

        waitsFor(function () {
            return response;
        }, 'the historical data transmission to execute', 2000);

        runs(function () {
            expect(LF.StudyRules.activate).not.toHaveBeenCalled();
            expect(collection.size(0)).toEqual(0);
        });
    });

    it('should execute historical data sync record in the transmission queue and not trigger a rule due to failed transmission', function () {
        var response;

        fakeAjax({registrations: [{
            url         : '../../api/v1/LastDiary/' + '12345678' + '\\?fields=Daily',
            type        : 'GET',
            error       : {
                status: 'error',
                xhr: {
                    getResponseHeader: function (id) { },
                    status : LF.ServiceErr.HTTP_SERVER_ERROR
                }
            }}]
        });

        spyOn(LF.StudyRules, 'activate');

        syncHistoricalData({
            collection          :  'LastDiaries',
            webServiceFunction  :  'getLastDiary'
        }, function () {
            response = true;
        });

        waitsFor(function () {
            return response;
        }, 'the historical data transmission to execute', 2000);

        runs(function () {
            expect(LF.StudyRules.activate).not.toHaveBeenCalled();
            expect(collection.size(0)).toEqual(0);
        });
    });

    it('should uninstall the database.', function () {

        LF.SpecHelpers.uninstallDatabase();
    });

});
