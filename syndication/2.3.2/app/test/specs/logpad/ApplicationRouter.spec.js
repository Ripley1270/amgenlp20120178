import COOL from 'core/COOL';

xdescribe('ApplicationRouter', function () {
    var router,
        location,
        siteMode = LF.StudyDesign.siteMode,
        templates = '<div id="templates">' +
                '<div id="dashboard-template"></div>' +
                '<div id="login-template"></div>' +
                '<div id="bluetooth-template"></div>' +
                '<div id="change-password-template"></div>' +
                '<div id="reset-password-template"></div>' +
                '<div id="toolbox-template"></div>' +
                '<div id="alarms-template"></div>' +
            '</div>';

    beforeEach(function () {
        spyOn(LF.Helpers, 'checkInstall').andCallFake(function (callback) {
            callback(true);
        });

        LF.router = new LF.Router.ApplicationRouter();

        spyOn(LF.router, 'navigate').andCallFake(function (url) {
            location = url;
        });

        spyOn(COOL.getClass('Utilities'), 'isOnline').andCallFake(function (callback) {
            callback(false);
        });

        $('body').append(templates);

        LF.security = new LF.Class.ApplicationSecurity();

        spyOn(LF.security, 'listenForActivity');
        spyOn(LF.security, 'on');
        spyOn(LF.security, 'off');
    });

    afterEach(function () {
        $('#templates').remove();
        LF.security = undefined;
        LF.router = undefined;
        LF.schedule = undefined;
        LF.Data = {};
        LF.dataAccess = undefined;
        LF.StudyDesign.siteMode = siteMode;
        localStorage.clear();
    });

    it('should display the login view', function () {
        LF.router.login();

        expect(LF.router.view.id).toEqual('login-page');
    });

    it('should display connection required notification if offline and not installed', function () {
        var callParams = {message: 'CONNECTION_REQUIRED'};
        spyOn(LF.Actions, 'notify').andCallFake(function (params, callback) {
            callback(false);
        });

        LF.Helpers.checkInstall = function (callback) {
            callback(false);
        };

        router = new LF.Router.ApplicationRouter();

        expect(LF.Actions.notify).toHaveBeenCalledWith(callParams, LF.Actions.notify.mostRecentCall.args[1]);
    });

    it('should display the Subject Gateway view', function () {
        spyOn(LF.security, 'checkSubjectLogin').andReturn(true);

        LF.router.dashboard();

        expect(LF.router.view.id).toEqual('dashboard-page');
    });

    it('should display the Site Gateway view', function () {
        spyOn(LF.security, 'checkSiteLogin').andReturn(true);
        LF.StudyDesign.siteMode = true;

        LF.router.dashboard();

        expect(LF.router.view.id).toEqual('siteGateway-page');
    });

    it('should run the before filter for questionnaire back out', function () {
        LF.router.before.view = new LF.View.QuestionnaireView({ id: 'Daily_Diary', schedule_id: 'Daily_DiarySchedule_01' });

        spyOn(LF.router.before.view, 'backoutHandler').andCallFake(function () {});

        LF.router.before.dashboard('dashboard', null);

        expect(LF.router.before.view.backoutHandler).toHaveBeenCalled();
    });

    it('shouldn\'t route to dashboard when questionnaireCompleted flag is true', function () {
        LF.router.before.view = new LF.View.QuestionnaireView({ id: 'Daily_Diary', schedule_id: 'Daily_DiarySchedule_01' });

        spyOn(LF.router.before.view, 'backoutHandler').andCallFake(function () {});

        localStorage.setItem('questionnaireCompleted', true);

        LF.router.before.dashboard('dashboard', null);

        expect(location).toNotEqual('dashboard');
    });

    it('should run the questionnaire back out when trying to navigate to login from questionnaire', function () {
        LF.router.before.view = new LF.View.QuestionnaireView({ id: 'Daily_Diary', schedule_id: 'Daily_DiarySchedule_01' });

        spyOn(LF.router.before.view, 'backoutHandler').andCallFake(function () {});

        LF.router.before.login('login', null);

        expect(LF.router.before.view.backoutHandler).toHaveBeenCalled();
        expect(location).toNotEqual('login');
    });

    it('shouldn\'t display the dashboard view', function () {
        spyOn(LF.security, 'checkLogin').andReturn(false);

        LF.router.dashboard();

        expect(location).toEqual('login');
    });

    it('shouldn\'t display the forgot password view and redirect to Login view.', function () {
        LF.router.forgotPassword();

        expect(location).toEqual('login');
    });

    it('should display the forgot password view.', function () {
        localStorage.setItem('Forgot_Password', true);

        LF.router.forgotPassword();

        expect(LF.router.view.id).toEqual('forgot-password-page');
    });

    it('shouldn\'t display the reset password view and redirect to Login view.', function () {
        LF.router.resetPassword();

        expect(location).toEqual('login');
    });

    it('should display the reset password view.', function () {
        localStorage.setItem('Reset_Password', true);

        LF.router.resetPassword();

        expect(LF.router.view.id).toEqual('reset-password-page');
    });

    it('should display the questionnaire view.', function () {
        LF.Data.Questionnaire = {};

        spyOn(LF.security, 'checkQuestionnaireAccess').andReturn(true);

        LF.router.questionnaire('Daily_Diary');

        expect(LF.router.view.id).toEqual('Daily_Diary');
    });

    it('should display the dashboard if no questionnaire data is available.', function () {
        LF.Data.Questionnaire = undefined;

        spyOn(LF.security, 'checkQuestionnaireAccess').andReturn(true);

        LF.router.questionnaire('Daily_Diary');

        expect(location).toEqual('dashboard');
    });

    it('shouldn\'t display the questionnaire view', function () {
        spyOn(LF.security, 'checkQuestionnaireAccess').andReturn(false);

        LF.router.questionnaire();

        expect(location).toEqual('login');
    });

    it('should display the change password view.', function () {
        spyOn(LF.security, 'checkLogin').andReturn(true);

        LF.router.changePassword();

        expect(LF.router.view.id).toEqual('change-password-page');
    });

    it('shouldn\'t display the change password view', function () {
        spyOn(LF.security, 'checkLogin').andReturn(false);

        LF.router.changePassword();

        expect(location).toEqual('login');
    });

    it('should display the toolbox view.', function () {
        spyOn(LF.security, 'checkLogin').andReturn(true);

        LF.router.toolbox();

        expect(LF.router.view.id).toEqual('toolbox-page');
    });

    it('shouldn\'t display the toolbox view', function () {
        spyOn(LF.security, 'checkLogin').andReturn(false);

        LF.router.toolbox();

        expect(location).toEqual('login');
    });

    it('should display the set alarms view.', function () {
        spyOn(LF.security, 'checkLogin').andReturn(true);

        LF.router.setAlarms();

        expect(LF.router.view.id).toEqual('set-alarms-page');
    });

    it('shouldn\'t display the set alarms view', function () {
        spyOn(LF.security, 'checkLogin').andReturn(false);

        LF.router.setAlarms();

        expect(location).toEqual('login');
    });

    it('should display the about view.', function () {
        spyOn(LF.security, 'checkLogin').andReturn(true);

        LF.router.about();

        expect(LF.router.view.id).toEqual('about-page');
    });

    it('shouldn\'t display the about view', function () {
        spyOn(LF.security, 'checkLogin').andReturn(false);

        LF.router.about();

        expect(location).toEqual('login');
    });

    it('should display the unlockCode view.', function () {
        localStorage.setItem('Unlock_Code', true);

        LF.router.unlockCode();

        expect(LF.router.view.id).toEqual('unlock-code-page');
    });

    it('shouldn\'t display the unlockCode view and redirect to Login view', function () {
        LF.router.unlockCode();

        expect(location).toEqual('login');
    });

    it('should display the Privacy Policy view.', function () {
        spyOn(LF.security, 'checkLogin').andReturn(true);

        LF.router.privacyPolicy();

        expect(LF.router.view.id).toEqual('privacy-policy-application-page');
    });

    it('shouldn\'t display the Privacy Policy view', function () {
        spyOn(LF.security, 'checkLogin').andReturn(false);

        LF.router.privacyPolicy();

        expect(location).toEqual('login');
    });

    it('should clear the view.', function () {
        spyOn(LF.View, 'LoginView').andReturn({close: function () {}});

        LF.router.login();
        LF.router.clear();

        expect(LF.router.view).toBeNull();
    });

    it('shouldn\'t display the change secret question view', function () {
        spyOn(LF.security, 'checkLogin').andReturn(false);

        LF.router.changeSecretQuestion();

        expect(location).toEqual('login');
    });

    it('should display the change secret question view', function () {
        spyOn(LF.security, 'checkLogin').andReturn(true);

        LF.router.changeSecretQuestion();

        expect(LF.router.view.id).toEqual('change-secret-question-page');
    });

    it('shouldn\'t display the reset secret question view and redirect to Login view', function () {
        LF.router.resetSecretQuestion();

        expect(location).toEqual('login');
    });

    it('should display the reset secret question view', function () {
        localStorage.setItem('Reset_Password_Secret_Question', true);

        LF.router.resetSecretQuestion();

        expect(LF.router.view.id).toEqual('reset-secret-question-page');
    });

    it('should display the bluetooth pairing view', function () {
        window.plugin = {};
        window.plugin.eSense = {};

        spyOn(LF.security, 'checkLogin').andReturn(true);

        LF.router.bluetooth();

        expect(LF.router.view.id).toEqual('bluetooth-page');

        delete window.plugin;
    });

    it('shouldn\'t display the bluetooth view and redirect to Login view', function () {
        LF.router.bluetooth();

        expect(location).toEqual('login');
    });

    it('should display the take messagebox screenshots view', function () {
        spyOn(LF.security, 'checkLogin').andReturn(true);

        LF.router.takeMessageBoxScreenshots();

        expect(LF.router.view.id).toEqual('take-messagebox-screenshots-page');
    });

    it('shouldn\'t display the take messagebox screenshots view and redirect to Login view', function () {
        LF.router.takeMessageBoxScreenshots();

        expect(location).toEqual('login');
    });

    it('should display the site login view', function () {
        LF.router.siteLogin();

        expect(LF.router.view.id).toEqual('site-login-page');
    });
});
