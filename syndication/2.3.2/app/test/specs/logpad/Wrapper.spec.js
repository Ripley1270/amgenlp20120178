import 'core/wrapperjs/Wrapper';
import 'core/wrapperjs/Alarms';
import * as dateTime from 'core/DateTimeUtil';
import * as specHelpers from 'test/helpers/SpecHelpers';

describe('Wrapper', () => {

    beforeEach(() => {
        LF.Wrapper.isWrapped = false;
        LF.Wrapper.platform = undefined;
        localStorage.clear();
    });

    it('should not be set to wrapped before it has been detected or initialized', () => {
        expect(LF.Wrapper.isWrapped).toBeFalsy();
        expect(LF.Wrapper.platform).toBeUndefined();
        expect(localStorage.getItem('isWrapped')).toBeNull();
    });

    it('should not be set to wrapped if there is no "wrapped" flag in the URL', () => {
        LF.Wrapper.detectWrapper();

        expect(LF.Wrapper.isWrapped).toBeFalsy();
        expect(LF.Wrapper.platform).toBeUndefined();
        expect(localStorage.getItem('isWrapped')).toBeNull();
    });

    it('should save the specified platform to localStorage', () => {
        LF.Wrapper.savePlatform('ios');

        expect(LF.Wrapper.isWrapped).toBeTruthy();
        expect(LF.Wrapper.platform).toEqual('ios');
        expect(localStorage.getItem('isWrapped')).toBeTruthy();
    });

    it('should be able to determine if it is running on ios', () => {
        LF.Wrapper.savePlatform('ios');

        expect(LF.Wrapper.isIOS()).toBeTruthy();
        expect(LF.Wrapper.isAndroid()).toBeFalsy();
        expect(LF.Wrapper.isBrowser()).toBeFalsy();
    });

    it('should be able to determine if it is running on android', () => {
        LF.Wrapper.savePlatform('android');

        expect(LF.Wrapper.isIOS()).toBeFalsy();
        expect(LF.Wrapper.isAndroid()).toBeTruthy();
        expect(LF.Wrapper.isBrowser()).toBeFalsy();
    });

    it('should be able to determine if it is running in the unwrapped browser', () => {
        expect(LF.Wrapper.isIOS()).toBeFalsy();
        expect(LF.Wrapper.isAndroid()).toBeFalsy();
        expect(LF.Wrapper.isBrowser()).toBeTruthy();
    });

    it('should execute the execWhenNotWrapped callback if it is not wrapped', (done) => {
        LF.Wrapper.exec({
            execWhenNotWrapped : () => {

                // No expectations required.  Done is called, and the test will meet pass.
                done();
            }
        });
    });

    it('should execute the execWhenWrapped callback if it is wrapped', (done) => {
        LF.Wrapper.savePlatform('ios');

        LF.Wrapper.exec({
            execWhenWrapped : () => {
                done();
            }
        });

        LF.SpecHelpers.triggerDeviceReadyEvent();
    });

    // @todo May need retired.  Code in wrapper commented out.
    xit('should initialize but not append the cordova script if it is not running in the wrapper', () => {
        let iOSScript,
            androidScript;

        LF.Wrapper.initialize();

        iOSScript = $('script[src="/LF/js/lib/cordova/ios/cordova.js"]');
        androidScript = $('script[src="/LF/js/lib/cordova/android/cordova.js"]');

        expect(iOSScript[0]).toBeUndefined();
        expect(androidScript[0]).toBeUndefined();
    });

    // @todo May need retired.  Code in wrapper commented out.
    xit('should initialize and append the cordova script if it is running in the wrapper', () => {
        let appendedScript;

        LF.Wrapper.savePlatform('ios');

        console.log('ATTN: The fake ajax plugin incorrectly assumes programmatic script loads are ajax calls, ignore the following warning:');
        LF.Wrapper.initialize();

        appendedScript = $('script[src="/LF/js/lib/cordova/ios/cordova.js"]');

        expect(appendedScript[0]).toBeDefined();
        appendedScript.remove();
    });

    it('should generate a list of 4 id numbers', () => {
        let idList = LF.Wrapper.Utils.generateIds(1, 3);

        expect(idList).toEqual([1000, 1001, 1002, 1003]);
    });

    it('should generate a list with 1 id number', () => {
        let idList = LF.Wrapper.Utils.generateIds(10, undefined);

        expect(idList).toEqual([10000]);
    });

    it('should detect the wrapper platform', () => {
        localStorage.setItem('url_wrapped', 'ios');
        LF.Wrapper.detectWrapper();

        expect(localStorage.getItem('isWrapped')).toEqual('ios');
    });

    it('should return zero when \'reminders\' is null', () => {
        let date = dateTime.convertToUtc(new Date('Wed 19 Mar 2014 08:00:00'));

        expect(LF.Wrapper.Utils.getReminders(date.getTime())).toEqual(0);
    });

    it('should return zero when \'interval\' is null', () => {
        let date = dateTime.convertToUtc(new Date('Wed 19 Mar 2014 08:00:00'));

        expect(LF.Wrapper.Utils.getReminders(date.getTime(), 4)).toEqual(0);
    });

    it('should return zero when the current time is not within the Alarm time frame', () => {
        let date = dateTime.convertToUtc(new Date('Wed 19 Mar 2014 08:00:00'));

        spyOn(dateTime, 'getNow').and.returnValue(new Date('Wed 19 Mar 2014 08:25:00'));

        expect(LF.Wrapper.Utils.getReminders(date.getTime(), 4, 1)).toEqual(0);
    });

    xit('should return a value when the current time is within the Alarm time frame', () => {
        let date = dateTime.convertToUtc(new Date('Wed 19 Mar 2014 08:00:00'));

        spyOn(dateTime, 'getNow').and.returnValue(new Date('Wed 19 Mar 2014 08:25:00'));

        expect(LF.Wrapper.Utils.getReminders(date.getTime(), 4, 15)).toEqual(3);
    });

});
