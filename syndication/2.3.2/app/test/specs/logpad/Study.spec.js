// @todo Defunct - Appears to be dead code
xdescribe('Study', function () {

    var model;

    beforeEach(function () {

        model = new LF.Model.Study({
            id : 1
        });

    });

    afterEach(function () {

        localStorage.clear();
        LF.logs.reset();

    });

    it('should have an id of 1', function () {

        expect(model.get('id')).toEqual(1);

    });

    it('should have a schema', function () {

        expect(model.schema).toBeDefined();
        expect(typeof model.schema).toEqual('object');

    });

    it('should have a storage property.', function () {

        expect(model.storage).toBeDefined();
        expect(typeof model.storage).toEqual('string');

    });

    it('should throw an error because the attribute is not defined in the schema', function () {

        expect(function () {
            model.set({
                foo : 'bar'
            }, {validate:true});
        }).toThrow('Attribute: foo not defined in schema');

    });

    it('should throw an error because invalid attribute type (expects a number)', function () {

        expect(function () {
            model.set({
                id : 'bar'
            }, {validate:true});
        }).toThrow('Attribute: id. Invalid DataType. Expected number. Received string.');

    });

    it('should throw an error because invalid attribute type (expects a string)', function () {

        expect(function () {
            model.set({
                studyName : 1
            }, {validate:true});
        }).toThrow('Attribute: studyName. Invalid DataType. Expected string. Received number.');

    });

});
