import ESenseDevice from 'core/models/ESenseDevice';
import ESenseDevices from 'core/collections/ESenseDevices';
import * as helpers from 'core/Helpers';
import notify from 'core/actions/notify';
import removeMessage from 'core/actions/removeMessage';
import displayMessage from 'core/actions/displayMessage';

import 'core/wrapperjs/Wrapper';
import 'core/wrapperjs/eSense';

import 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';

describe('eSense Utilities', () => {
    let noop, location, notify,
        deviceName = 'AM1+ 204541',
        clearDevices = () => {
            return ESenseDevices.clearStorage();
        },
        registerAM1Devices = ()  => {
            let deviceA = new ESenseDevice(),
                requests = [];

            requests.push(deviceA.save({
                name: 'AM1+ 999999',
                address: '00:00:00:00:00:00',
                type:  'PEF',
                pairDate: new Date('2001-01-01').ISOStamp(),
                serial: 'B999999'
            }));

            let deviceB = new ESenseDevice();
            requests.push(deviceB.save({
                name: 'AM1+ 888888',
                address: '00:00:00:00:00:00',
                type:  'PEF',
                pairDate: new Date('2000-01-01').ISOStamp(),
                serial: 'B888888'
            }));

            let deviceC = new ESenseDevice();
            requests.push(deviceC.save({
                name: deviceName,
                address: '00:A0:96:30:35:97',
                type: 'PEF',
                pairDate: new Date('2015-01-29').ISOStamp(),
                serial: 'B204541'
            }));

            return Q.allSettled(requests);
        };

    beforeAll((done) => {
        specHelpers.uninstallDatabase().finally(done);
    });

    beforeEach(() => {

        spyOn(helpers, 'checkInstall').and.callFake(callback => callback(false));

        // LF.router = new LF.Router.ActivationRouter();

        LF.router = { navigate: $.noop };
        LF.spinner = { hide: $.noop };

        spyOn(LF.router, 'navigate').and.callFake(function (url) {
            location = url;
        });

        notify = spyOn(LF.Actions, 'notify').and.callFake(function (params, callback) {
            callback();
        });

        spyOn(LF.Actions, 'displayMessage').and.callFake(function (params, callback) {
            callback();
        });

        spyOn(LF.Actions, 'removeMessage').and.resolve();
    });

    afterEach(function () {
        LF.router = undefined;
        location = undefined;

        //noop.reset();
        //notify.reset();
    });

    describe('method:pluginExists', () => {
        it('should determine that the plugin does not exist.', () => {
            expect(LF.Wrapper.Utils.eSense.pluginExists()).toEqual(false);
        });

        it('should determine that the plugin does exists.', () => {
            window.plugin = {};
            plugin.eSense = {};
            expect(LF.Wrapper.Utils.eSense.pluginExists()).toEqual(true);

            delete window.plugin;
        });
    });

    describe('method:checkPlugin', () => {
        it('should successfully handle the case that plugin exists @ checkPlugin().', (done) => {
            window.plugin = {};
            plugin.eSense = {};

            // The callback alone indicates the plugin exists. No need for expect().
            LF.Wrapper.Utils.eSense.checkPlugin(() => {
                delete window.plugin;
                done();
            }, undefined);
        });

        it('should successfully handle the case that plugin does not exist @ checkPlugin().', (done) => {
            delete window.plugin;
            LF.Wrapper.Utils.eSense.checkPlugin(null, () => {
                // @todo need to update wrapper.js to be a module
                //expect(LF.Actions.notify).toHaveBeenCalledWith({ message: 'PLUGIN_MISSING' }, jasmine.any(Function));
                //expect(Log4js.Logger.prototype.log).toHaveBeenCalledWith(Log4js.Level.ERROR, jasmine.any(String));
                done();
            });
        });

        xit('should successfully handle the case that plugin does not exist @ checkPlugin() (no callback).', () => {
            delete window.plugin;
            LF.Wrapper.Utils.eSense.checkPlugin();

            expect(LF.Actions.notify).toHaveBeenCalledWith({ message: 'PLUGIN_MISSING' }, jasmine.any(Function));
        });
    });

    describe('method:checkSupport', () => {
        it('should successfully handle the case that eSense API is supported @ checkSupport().', (done) => {
            window.plugin = {};
            plugin.eSense = {};
            plugin.eSense.isSupported = () => {
                return true;
            };

            //onSupported callback is provided
            LF.Wrapper.Utils.eSense.checkSupport(() => {
                delete window.plugin;
                done();
            }, undefined);
        });

        it('should successfully handle the case that eSense API is not supported @ checkSupport().', (done) => {
            LF.Wrapper.Utils.eSense.checkSupport(undefined, () => {
                done();
            });
        });

        xit('should successfully handle the case that eSense API is not supported @ checkSupport() (no callback).', () => {
            LF.Wrapper.Utils.eSense.checkSupport();

            expect(LF.Actions.notify).toHaveBeenCalledWith({ message: 'DIARY_REQUIRES_E_SENSE_SUPPORT' }, jasmine.any(Function));
            expect(LF.router.navigate).toHaveBeenCalledWith('dashboard', true);
        });
    });

    describe('method:getPairedDevice', () => {
        it('should callback with null if there are no paired eSense devices.', (done) => {
            clearDevices();
            LF.Wrapper.Utils.eSense.getPairedDevice('PEF', (device) => {
                expect(device).toBe(null);
                done();
            });
        });

        it('should log an error when fetching device data fails @ getPairedDevice().', (done) => {
            let storage = LF.Collection.ESenseDevices.prototype.storage;

            LF.Collection.ESenseDevices.prototype.storage = 'nonsense';

            LF.Wrapper.Utils.eSense.getPairedDevice('PEF', (device) => {
                expect(device).toBeNull();
                LF.Collection.ESenseDevices.prototype.storage = storage;
                done();
            });
        });

        it('should successfully get the eSense device with the latest pairDate.', (done) => {
            registerAM1Devices().then(() => {
                return Q.Promise((resolve, reject) => {
                    LF.Wrapper.Utils.eSense.getPairedDevice('PEF', (device) => {
                        expect(device.get('name')).toEqual(deviceName);
                        expect(device.get('type')).toEqual('PEF');
                        expect(device.get('serial')).toEqual('B204541');
                        resolve();
                    });
                });
            })
            .catch(e => fail(e))
            .done(done);
        });
    });

    describe('method:checkPaired', () => {
        beforeAll((done) => {
            let collection = new ESenseDevices();

            collection.clear().finally(done);
        });

        it('should successfully handle the case that a paired eSense device does not exist @ checkPaired().', (done) => {

            //onNotPaired callback is provided
            LF.Wrapper.Utils.eSense.checkPaired('PEF', null, done);

        });

        xit('should successfully handle the case that a paired eSense device does not exist @ checkPaired() (no callback).', (done) => {
            LF.Wrapper.Utils.eSense.checkPaired();

            setTimeout(() => {
                expect(LF.Actions.notify).toHaveBeenCalledWith({ message: 'E_SENSE_NOT_PAIRED' }, jasmine.any(Function));
                expect(LF.router.navigate).toHaveBeenCalledWith('dashboard', true);
                done();
            });
        });

        it('should successfully handle the case that a paired eSense device exists @ checkPaired().', (done) => {
            registerAM1Devices().then(() => {
                LF.Wrapper.Utils.eSense.checkPaired('PEF', (device) => {
                    expect(device.get('name')).not.toBeUndefined();
                    expect(device.get('type')).not.toBeUndefined();
                    expect(device.get('serial')).not.toBeUndefined();
                    done();
                }, undefined);
            });
        });

    });

    describe('method:enable', () => {

        it('should successfully handle success case for eSense.enable @ enable().', (done) => {
            window.plugin = {};
            plugin.eSense = {};
            plugin.eSense.enable = (onSuccess, onError) => {
                onSuccess();
            };

            //onSuccess callback is provided
            LF.Wrapper.Utils.eSense.enable(() => {
                delete window.plugin;
                done();
            }, undefined);
        });

        it('should successfully handle error case for eSense.enable @ enable().', (done) => {
            window.plugin = {};
            plugin.eSense = {};
            plugin.eSense.enable = function (onSuccess, onError) {
                onError();
            };

            //onError callback is provided
            LF.Wrapper.Utils.eSense.enable(null, done);
        });

        it('should successfully handle error case for eSense.enable @ enable() (no callback).', (done) => {
            //onError callback is NOT provided
            LF.Wrapper.Utils.eSense.enable();

            setTimeout(() => {
                expect(LF.Actions.notify).toHaveBeenCalledWith({ message: 'ERROR_E_SENSE_ENABLE' }, jasmine.any(Function));
                expect(LF.router.navigate).toHaveBeenCalledWith('dashboard', true);

                delete window.plugin;
                done();
            });
        });

    });

    describe('method:connect', () => {

        beforeAll((done) => registerAM1Devices().then(done));
        afterAll((done) => clearDevices().then(done));

        it('should successfully handle success case for eSense.connect @ connect().', (done) => {
            window.plugin = {};
            plugin.eSense = {};
            plugin.eSense.connect = onSuccess => {
                onSuccess();
            };

            LF.Wrapper.Utils.eSense.getPairedDevice('PEF', (device) => {
                //onSuccess callback is provided
                LF.Wrapper.Utils.eSense.connect(() => {
                    delete window.plugin;
                    done();
                }, undefined, device);
            });
        });

        it('should successfully handle error case for eSense.connect @ connect().', (done) => {
            window.plugin = {};
            plugin.eSense = {};
            plugin.eSense.connect = function (onSuccess, onError) {
                onError();
            };

            LF.Wrapper.Utils.eSense.getPairedDevice('PEF', function (device) {
                //onError callback is provided
                LF.Wrapper.Utils.eSense.connect(null, done, device);
            });
        });

        it('should successfully handle error case for eSense.connect @ connect().', (done) => {
            spyOn(LF.Wrapper.Utils.eSense, 'turnOffBluetooth').and.callFake(callback => callback());

            LF.Wrapper.Utils.eSense.getPairedDevice('PEF', (device) => {
                //onError callback is not provided
                LF.Wrapper.Utils.eSense.connect(null, null, device);

                setTimeout(() => {
                    expect(LF.Wrapper.Utils.eSense.turnOffBluetooth).toHaveBeenCalled();
                    expect(LF.Actions.notify).toHaveBeenCalled();
                    delete window.plugin;
                    done();
                });
            });
        });

    });

    describe('method:turnOffBluetooth', () => {
        it('should successfully handle success case for eSense.disable @ turnOffBluetooth().', (done) => {
            window.plugin = {};
            plugin.eSense = {};
            plugin.eSense.disable = (onSuccess) => onSuccess();

            LF.Wrapper.Utils.eSense.turnOffBluetooth(done);
        });

        it('should successfully handle success case for eSense.disable @ turnOffBluetooth() (no callback).', () => {
            let spy = spyOn($, 'noop');

            LF.Wrapper.Utils.eSense.turnOffBluetooth(undefined);

            expect(spy).toHaveBeenCalled();
        });

        it('should successfully handle error case for eSense.disable @ turnOffBluetooth().', (done) => {
            window.plugin = {};
            plugin.eSense = {};
            plugin.eSense.disable = (onSuccess, onError) => onError();

            // Even if disable fails, the callback is invoked...
            LF.Wrapper.Utils.eSense.turnOffBluetooth(done);
        });

    });

    describe('method:disconnect', () => {
        it('should successfully handle success case for eSense.disconnect @ disconnect().', (done) => {
            window.plugin = {};
            plugin.eSense = {};
            plugin.eSense.disconnect = onSuccess => onSuccess();

            LF.Wrapper.Utils.eSense.disconnect(() => {
                delete window.plugin;

                done();
            });
        });

        it('should successfully handle error case for eSense.disconnect @ disconnect().', (done) => {
            window.plugin = {};
            plugin.eSense = {};
            plugin.eSense.disconnect = function (onSuccess, onError) {
                onError();
            };

            LF.Wrapper.Utils.eSense.disconnect(() => {
                delete window.plugin;

                done();
            });
        });
    });

    describe('method:checkUpdateTime', () => {
        it('should successfully handle error case for get time of eSense device @ checkUpdateTime().', (done) => {
            window.plugin = {};
            plugin.eSense = {};
            plugin.eSense.getTime = function (onSuccess, onError, timeout) {
                onError();
            };

            //onError callback is provided
            LF.Wrapper.Utils.eSense.checkUpdateTime(undefined, () => {
                delete window.plugin;

                done();
            });
        });

        it('should not update eSense device time if time of eSense device and current time is less than threshold @ checkUpdateTime().', (done) => {
            let now = (new Date()).ISOStamp();

            window.plugin = {};
            plugin.eSense = {};
            plugin.eSense.getTime = onSuccess => onSuccess(now);
            plugin.eSense.setTime = onSuccess => onSuccess();

            spyOn(plugin.eSense, 'setTime').and.callThrough();

            //onSuccess callback is provided
            LF.Wrapper.Utils.eSense.checkUpdateTime(() => {
                expect(plugin.eSense.setTime).not.toHaveBeenCalled();

                delete window.plugin;
                done();
            }, undefined);
        });

        it('should update eSense device time if time of eSense device and current time is more than threshold @ checkUpdateTime().', (done) => {
            //1 day further than now
            let deviceTime = (new Date((new Date()).getTime() + (24 * 60 * 60 * 1000))).ISOStamp();

            window.plugin = {};
            plugin.eSense = {};
            plugin.eSense.getTime = onSuccess => onSuccess(deviceTime);
            plugin.eSense.setTime = onSuccess => onSuccess();

            spyOn(plugin.eSense, 'setTime').and.callThrough();

            //onSuccess callback is provided
            LF.Wrapper.Utils.eSense.checkUpdateTime(() => {
                expect(plugin.eSense.setTime).toHaveBeenCalled();

                delete window.plugin;
                done();
            }, undefined);
        });

        it('should try time update eSense device time if time of eSense device and current time is more than threshold and should handle error case for setting time @ checkUpdateTime().', (done) => {
            //1 day further than now
            let deviceTime = (new Date((new Date()).getTime() + (24 * 60 * 60 * 1000))).ISOStamp();

            window.plugin = {};
            plugin.eSense = {};
            plugin.eSense.getTime = onSuccess => onSuccess(deviceTime);
            plugin.eSense.setTime = (onSuccess, onError) => onError();

            spyOn(plugin.eSense, 'setTime').and.callThrough();

            //onError callback is provided
            LF.Wrapper.Utils.eSense.checkUpdateTime(undefined, function () {
                expect(plugin.eSense.setTime).toHaveBeenCalled();

                delete window.plugin;
                done();
            });
        });

    });

    describe('method:getBatteryState', () => {

        it('should successfully handle success case for get the battery state @ getBatteryState()', (done) => {
            let battery = 'B';

            window.plugin = {};
            plugin.eSense = {};
            plugin.eSense.getBatteryState = onSuccess => onSuccess(battery);

            //onSuccess callback is provided
            LF.Wrapper.Utils.eSense.getBatteryState((result) => {
                expect(result).toEqual(battery);

                delete window.plugin;
                done();
            }, undefined);
        });

        it('should successfully handle error case for get the battery state @ getBatteryState()', (done) => {
            window.plugin = {};
            plugin.eSense = {};
            plugin.eSense.getBatteryState = (onSuccess, onError) => onError();

            //onError callback is provided
            LF.Wrapper.Utils.eSense.getBatteryState(undefined, () => {
                delete window.plugin;
                done();
            });
        });

    });

    describe('method:getAllRecords', () => {
        it('should successfully handle success case for getting all records from eSense @ getAllRecords()', (done) => {
            let records = [];

            window.plugin = {};
            plugin.eSense = {};
            plugin.eSense.getAllRecords = onSuccess => onSuccess(records);

            spyOn(LF.Wrapper.Utils.eSense, 'checkUpdateTime').and.callFake(onSuccess => onSuccess());

            //onSuccess callback is provided
            LF.Wrapper.Utils.eSense.getAllRecords(function (result) {
                expect(result).toEqual(records);

                delete window.plugin;
                done();
            }, undefined);
        });

        it('should successfully handle error case for checkUpdateTime fails @ getAllRecords()', (done) => {
            window.plugin = {};
            plugin.eSense = {};

            spyOn(LF.Wrapper.Utils.eSense, 'checkUpdateTime').and.callFake((onSuccess, onError) => onError());

            //onError callback is provided
            LF.Wrapper.Utils.eSense.getAllRecords(undefined, () => {
                delete window.plugin;
                done();
            });
        });

        it('should successfully handle error case for getting all records from eSense @ getAllRecords()', (done) => {
            window.plugin = {};
            plugin.eSense = {};
            plugin.eSense.getAllRecords = (onSuccess, onError) => onError();

            spyOn(LF.Wrapper.Utils.eSense, 'checkUpdateTime').and.callFake(onSuccess => onSuccess());

            //onError callback is provided
            LF.Wrapper.Utils.eSense.getAllRecords(undefined, () => {
                delete window.plugin;
                done();
            });
        });
    });

    describe('method:getLatestRecords', () => {
        it('should successfully handle success case for getting latest records from eSense @ getLatestRecords()', (done) => {
            let records = [];

            window.plugin = {};
            plugin.eSense = {};
            plugin.eSense.getLatestRecords = onSuccess => onSuccess(records);
            spyOn(LF.Wrapper.Utils.eSense, 'checkUpdateTime').and.callFake(onSuccess => onSuccess());

            //onSuccess callback is provided
            LF.Wrapper.Utils.eSense.getLatestRecords(function (result) {
                expect(result).toEqual(records);

                delete window.plugin;
                done();
            }, undefined, 5);
        });

        it('should successfully handle error case for checkUpdateTime fails @ getLatestRecords()', (done) => {
            window.plugin = {};
            plugin.eSense = {};

            spyOn(LF.Wrapper.Utils.eSense, 'checkUpdateTime').and.callFake((onSuccess, onError) => onError());
            //onError callback is provided
            LF.Wrapper.Utils.eSense.getLatestRecords(undefined, () => {
                delete window.plugin;
                done();
            }, 5);
        });

        it('should successfully handle error case for getting latest records from eSense @ getLatestRecords()', (done) => {
            window.plugin = {};
            plugin.eSense = {};
            plugin.eSense.getLatestRecords = (onSuccess, onError) => onError();

            spyOn(LF.Wrapper.Utils.eSense, 'checkUpdateTime').and.callFake(onSuccess => onSuccess());

            //onError callback is provided
            LF.Wrapper.Utils.eSense.getLatestRecords(undefined, () => {
                delete window.plugin;
                done();
            }, 5);
        });
    });

    describe('method:getNewRecords', () => {
        it('should successfully handle success case for getting new records from eSense @ getNewRecords()', (done) => {
            var records = [];

            window.plugin = {};
            plugin.eSense = {};
            plugin.eSense.getNewRecords = onSuccess => onSuccess(records);
            spyOn(LF.Wrapper.Utils.eSense, 'checkUpdateTime').and.callFake(onSuccess => onSuccess());

            //onSuccess callback is provided
            LF.Wrapper.Utils.eSense.getNewRecords(function (result) {
                expect(result).toEqual(records);

                delete window.plugin;
                done();
            }, undefined, {});
        });

        it('should successfully handle error case for checkUpdateTime fails @ getNewRecords()', (done) => {
            window.plugin = {};
            plugin.eSense = {};

            spyOn(LF.Wrapper.Utils.eSense, 'checkUpdateTime').and.callFake((onSuccess, onError) => onError());

            //onError callback is provided
            LF.Wrapper.Utils.eSense.getNewRecords(undefined, () => {
                delete window.plugin;
                done();
            }, {});
        });

        it('should successfully handle error case for getting new records from eSense @ getNewRecords()', (done) => {
            window.plugin = {};
            plugin.eSense = {};
            plugin.eSense.getNewRecords = (onSuccess, onError) => onError();
            spyOn(LF.Wrapper.Utils.eSense, 'checkUpdateTime').and.callFake(onSuccess => onSuccess());

            //onError callback is provided
            LF.Wrapper.Utils.eSense.getNewRecords(undefined, () => {
                delete window.plugin;
                done();
            }, {});
        });
    });

    describe('method:deleteRecords', () => {
        it('should successfully handle success case for deleting records @ deleteRecords()', (done) => {
            window.plugin = {};
            plugin.eSense = {};
            plugin.eSense.deleteRecords = onSuccess => onSuccess();

            //onSuccess callback is provided
            LF.Wrapper.Utils.eSense.deleteRecords(() => {
                delete window.plugin;
                done();
            }, undefined);
        });

        it('should successfully handle error case for deleting records @ deleteRecords()', (done) => {
            window.plugin = {};
            plugin.eSense = {};
            plugin.eSense.deleteRecords = (onSuccess, onError) => onError();

            //onError callback is provided
            LF.Wrapper.Utils.eSense.deleteRecords(undefined, () => {
                delete window.plugin;
                done();
            });

        });
    });

    describe('method:onESensePaired', () => {
        it('should successfully handle the success case for sending custom command @ onESensePaired()', (done) => {
            let deviceName = 'AM1+';

            window.plugin = {};
            plugin.eSense = {};
            plugin.eSense.sendCustomCommand = onSuccess => onSuccess();

            LF.Wrapper.Utils.eSense.onESensePaired(deviceName, () => {
                delete window.plugin;
                done();
            });
        });

        it('should successfully handle the error case for sending custom command @ onESensePaired()', (done) => {
            let deviceName = 'AM1+';

            window.plugin = {};
            plugin.eSense = {};
            plugin.eSense.sendCustomCommand = (onSuccess, onError) => onError();

            LF.Wrapper.Utils.eSense.onESensePaired(deviceName, () => {
                delete window.plugin;
                done();
            });
        });

        it('should just call the callback function if the eSense device is not AM1+ @ onESensePaired()', (done) => {
            let deviceName = 'Nope';

            window.plugin = {};
            plugin.eSense = {};
            plugin.eSense.sendCustomCommand = onSuccess => onSuccess();
            spyOn(plugin.eSense, 'sendCustomCommand').and.callThrough();

            LF.Wrapper.Utils.eSense.onESensePaired(deviceName, () => {
                expect(plugin.eSense.sendCustomCommand).not.toHaveBeenCalled();

                delete window.plugin;
                done();
            });
        });

    });
});
