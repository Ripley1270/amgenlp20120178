xdescribe('Message', function () {

    var model;

    beforeEach(function () {

        model = new LF.Model.Question({
            id      : 'SF36_Q_3_A',
            text    : 'QUESTION_3_A'
        });

    });

    afterEach(function () {

        localStorage.clear();

    });

    it('should have an id of SF36_Q_3_A', function () {

        return expect(model.get('id')).toEqual('SF36_Q_3_A');

    });

    it('should have a schema', function () {

        expect(model.schema).toBeDefined();

    });

    it('should not have a widget', function () {

        return expect(model.widget).not.toBeDefined();

    });

});
