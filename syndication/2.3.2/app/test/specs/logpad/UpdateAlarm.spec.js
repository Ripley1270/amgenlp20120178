xdescribe('Update Alarm', function () {

    var schedules = LF.StudyDesign.schedules,
        activeAlarms = new LF.Collection.ActiveAlarms(),
        now = new Date();

    beforeEach(function () {

        spyOn(LF.Helpers, 'checkInstall').andCallFake(function (callback) {
            callback(true);
        });

        spyOn(LF.Wrapper.Utils, 'addAlarm');
        spyOn(LF.Wrapper.Utils, 'cancelAlarm');

        LF.dataAccess = new LF.DataAccess({
            name: 'Unit_Test_UpdateAlarm',
            version: '1.0',
            description: 'LogPad App unit test.'
        });

        LF.StudyDesign.schedules = new LF.Collection.Schedules([
            {
                id: 'Test_1',
                target: {
                    objectType: 'questionnaire',
                    id: 'LogPad1'
                },
                scheduleFunction: 'function_1',
                alarmFunction: 'function_2',
                alarmParams: {
                    id: 1,
                    time: '19:00',
                    repeat: 'daily',
                    reminders: 2,
                    reminderInterval: 15
                }
            }, {
                id: 'Test_2',
                target: {
                    objectType: 'questionnaire',
                    id: 'LogPad2'
                },
                scheduleFunction: 'function_3',
                alarmFunction: 'function_4',
                alarmParams: {
                    id: 2,
                    time: '21:00'
                }
            }, {
                id: 'Test_3',
                target: {
                    objectType: 'questionnaire',
                    id: 'LogPad3'
                },
                scheduleFunction: 'function_5'
            }
        ]);
    });

    afterEach(function () {

        LF.logs.reset();
        LF.StudyDesign.schedules = schedules;
        LF.dataAccess = undefined;

    });

    it('should install the database.', function () {

        LF.SpecHelpers.installDatabase();

    });

    it('should generate alarms and reminders and save them to the database.', function () {

        var response, length, schedule, params, newDate, timeArray,
            alarms = new LF.Collection.ActiveAlarms(),
            step = function (counter) {

                if (counter === 2) {

                    alarms.fetch({
                        onSuccess : function () {

                            length = alarms.length;

                            response = true;
                        }
                    });

                } else {

                    schedule = LF.StudyDesign.schedules.at(counter);
                    params = schedule.get('alarmParams');
                    timeArray = params.time.split(':');
                    newDate = new Date(now);
                    newDate.setHours(timeArray[0], timeArray[1], 0, 0);
                    params.date = newDate;

                    alarms.updateAlarm(params, schedule.get('id'), function () {
                        alarms.fetch({
                            onSuccess : function () {

                                step(++counter);
                            }
                        });
                    });

                }
            };

        step(0);

        waitsFor(function () {
            return response;
        }, 'save alarms and reminders to the storage object', 1000);

        runs(function () {
            expect(response).toEqual(true);
            expect(length).toEqual(4);
        });

    });

    it('should find alarm and cancel it with update action', function () {

        var response, length, date, hour,
            alarms = new LF.Collection.ActiveAlarms();

        LF.Actions.updateAlarm({id: 'LogPad1'}, function () {
            alarms.fetch({
                onSuccess : function () {

                    length = alarms.length;

                    response = true;
                }
            });
        });

        waitsFor(function () {
            return response;
        }, 'cancel alarms with updateAlarm action.', 1000);

        runs(function () {
            expect(response).toEqual(true);
            expect(length).toEqual(1);
        });

    });

    it('should not find alarm params to update alarm and should callback successfully', function () {

        var response;

        LF.Actions.updateAlarm({id: 'LogPad3'}, function () {
            response = true;
        });

        waitsFor(function () {
            return response;
        }, 'should callback successfully.', 1000);

        runs(function () {
            expect(response).toEqual(true);
        });

    });

    it('should uninstall the database.', function () {

        LF.SpecHelpers.uninstallDatabase();

    });

});
