/**
 * Created by mark.matthews on 2/23/2016.
 */

/**
 * Helper function for unit tests
 * @param {Object} branchObj - extended branch object from the configuration
 * @param {BaseQuestionnaireView} view - the view that the looping is being performed on
 * @returns {Promise<Boolean>}
 */
LF.Branching.branchFunctions.toReviewScreenLoop1 = function (branchObj, view) {
    return new Q.Promise((resolve) => {
        let currentIGR,
            answersByIGR,
            IG = 'MedicationEntry_Loop_IG';
        // Update the IGR incase navigating to review screen from edit mode.
        view.updateIGR(IG);

        //clears the stack before review screen is put on the stack.
        view.clearLoopScreenFromStack(IG);

        // get the surrent IGR
        currentIGR = view.getCurrentIGR(IG);

        // gets all answers for the current IGR
        answersByIGR = view.queryAnswersByIGR(currentIGR);
        if (answersByIGR.length) {
            // increments if the current IGR is already completed, so next available IGR becomes current
            view.incrementIGR(IG);
        }
        resolve(true);
    });
};

LF.Branching.branchFunctions.always = function (branchObj, callback = $.noop) {
    return new Q.Promise((resolve) => {
        resolve(true);
    });
};

LF.Branching.branchFunctions.addNewMedsLoop = function (view) {
    let screen = 'LOOP1_MedSelection_Screen';
    view.screenStack.push(screen);
    return view.displayScreen(screen);
};

LF.Branching.branchFunctions.selectedMotrin = function (branchObj, questionnaireView) {
    return new Q.Promise((resolve) => {
        let IG = 'MedicationEntry_Loop_IG',
            IGR = questionnaireView.getCurrentIGR(IG),
            answer = questionnaireView.queryAnswersByQuestionIDAndIGR('LOOP1_MedSelection_Question', IGR);

        if (answer[0].get('response') === 4) {
            resolve(true);
        } else {
            resolve(false);
        }
    });
};
