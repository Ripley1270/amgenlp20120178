import * as helpers from 'core/Helpers';
import { createGUID } from 'core/utilities';
import Logger from 'core/Logger';
import ForgotPasswordActivationView from 'logpad/views/ForgotPasswordActivationView';
import Data from 'core/Data';
import Users from 'core/collections/Users';
import User from 'core/models/User';
import { Banner } from 'core/Notify';
import ELF from 'core/ELF';

import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';

import ActivationBaseViewSuite from 'test/specs/core/views/ActivationBaseView.specBase';

class ForgotPasswordActivationViewSuite extends ActivationBaseViewSuite {
    beforeEach () {
        resetStudyDesign();
        LF.StudyDesign.maxSecurityQuestionAttempts = 5;

        LF.webService = jasmine.createSpyObj('webService', ['getDeviceID']);
        LF.security = jasmine.createSpyObj('security', [
            'getSecurityQuestionFailure',
            'setSecurityQuestionFailure'
        ]);

        // We want to maintain the context of the User collection, so we use a anon. function declaration.
        // jscs:disable
        spyOn(Users.prototype, 'fetch').and.callFake(function () {
            let user = new User({
                id:0,
                username: 'admin'
            });

            this.add(user);
            return Q();
        });
        // jscs:enable

        Data.question = { key: 0, text: 'SECURITY_QUESTION_0' };

        this.removeTemplate = specHelpers.renderTemplateToDOM('logpad/templates/forgot-password-activation.ejs');
        this.view = new ForgotPasswordActivationView();

        specHelpers.createSubject();

        return specHelpers.saveSubject()
            .then(() => this.view.resolve())
            .then(() => this.view.render())
            .then(() => super.beforeEach());
    }

    afterEach () {
        this.removeTemplate();

        return super.afterEach();
    }

    executeAll (context) {
        super.executeAll(context);

        let methods = Object.getOwnPropertyNames(ForgotPasswordActivationViewSuite.prototype);

        this.annotateAll(methods, context);
    }

    testBack () {
        describe('method:back', () => {
            it('should navigate to the reactivation page.', () => {
                spyOn(this.view, 'navigate').and.stub();

                this.view.back();

                expect(this.view.navigate).toHaveBeenCalledWith('reactivation');
            });
        });
    }

    testUnlockCode () {
        describe('method:unlockCode', () => {
            it('should navigate to the unlock code page.', () => {
                spyOn(this.view, 'navigate').and.stub();

                this.view.unlockCode();

                expect(this.view.navigate).toHaveBeenCalledWith('unlock_code_activation');
            });
        });
    }

    testResolve () {
        describe('method:resolve', () => {
            Async.it('should fail to resolve.', () => {
                let err = new Error('Unknown database error.');

                Users.prototype.fetch.and.callFake(() => Q.reject(err));

                let request = this.view.resolve();

                expect(request).toBePromise();

                return request.catch(e => {
                    expect(e.message).toEqual(err.message);
                });
            });

            Async.it('should resolve.', () => {
                localStorage.setItem('Forgot_Password', true);
                let request = this.view.resolve();

                expect(request).toBePromise();

                return request.tap(() => {
                    expect(this.view.user).toBeDefined();
                    expect(this.view.user.get('id')).toBe(0);
                });
            });
        });
    }

    testRender () {
        describe('method:render', () => {
            Async.it('should render the view.', () => {
                let request = this.view.render();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.$el.html()).toBeDefined();
                });
            });

            Async.it('should not render the view.', () => {
                LF.StudyDesign.showPasswordRules = false;
                spyOn(this.view, 'buildHTML').and.callFake(() => Q.reject('Error'));

                let request = this.view.render();

                expect(request).toBePromise();

                return request.then(() => fail('Expected method render to have been rejected.'))
                    .catch(e => {
                        expect(e).toBe('Error');
                    });
            });

            Async.it('should display password rules when LF.StudyDesign.showPasswordRules === true', () => {
                LF.StudyDesign.showPasswordRules = true;
                let request = this.view.render();

                return request.then(() => {
                    let displayPasswordRules = !(this.view.$passwordRules.hasClass('hidden'));
                    expect(displayPasswordRules).toBe(true);
                });
            });
        });
    }

    testSubmit () {
        let preventDefault = $.noop;

        describe('method:submit', () => {
            beforeEach(() => {
                spyOn(Banner, 'closeAll').and.stub();
            });

            it('should navigate to the unlock code view.', () => {
                LF.StudyDesign.maxSecurityQuestionAttempts = 5;
                LF.security.getSecurityQuestionFailure.and.returnValue(5);
                spyOn(this.view, 'unlockCode');
                this.view.submit({ preventDefault });

                expect(this.view.unlockCode).toHaveBeenCalled();
            });

            it('should invoke the reactivate method.', () => {
                LF.StudyDesign.maxSecurityQuestionAttempts = 5;
                LF.security.getSecurityQuestionFailure.and.returnValue(3);

                this.view.reactivate = jasmine.createSpy('reactivate');
                this.view.submit({ preventDefault });

                expect(this.view.reactivate).toHaveBeenCalled();
            });

            it('should invoke the reactivate method (maxSecurityQuestionAttempts undefined).', () => {
                LF.StudyDesign.maxSecurityQuestionAttempts = null;

                this.view.reactivate = jasmine.createSpy('reactivate');
                this.view.submit({ preventDefault });

                expect(this.view.reactivate).toHaveBeenCalled();
            });
        });
    }

    testReactivate () {
        describe('method:reactivate', () => {
            beforeEach(() => {
                Data.code = 12345678;
                this.view.$newPassword.val('apple');
                this.view.$secretAnswer.val('1');
            });

            Async.it('should do nothing.', () => {
                spyOn(this.view, 'attemptTransmission').and.reject(false);

                this.view.reactivate();

                return Q().tap(() => {
                    expect(LF.webService.getDeviceID).not.toHaveBeenCalled();
                });
            });

            Async.it('should show a transmission error.', ()  => {
                spyOn(this.view, 'attemptTransmission').and.callFake(() => Q(true));
                spyOn(this.view, 'showTransmissionError').and.stub();
                LF.webService.getDeviceID.and.callFake((data, onSuccess, onFailure) => onFailure(400, 500));

                LF.ServiceErr = { HTTP_UNAUTHORIZED: 401 };
                this.view.reactivate();

                return Q().tap(() => {
                    expect(LF.webService.getDeviceID).toHaveBeenCalledWith({
                        U: 12345678,
                        W: hex_sha512(`apple${localStorage.getItem('krpt')}`),
                        Q: 0,
                        A: hex_sha512('1')
                    }, jasmine.any(Function), jasmine.any(Function));
                    expect(this.view.showTransmissionError).toHaveBeenCalledWith({
                        errorCode: 400,
                        httpCode: 500
                    });
                });
            });

            Async.it('should show an input error and navigate to the unlock code view.', ()  => {
                spyOn(this.view, 'attemptTransmission').and.callFake(() => Q(true));
                spyOn(this.view, 'showInputError').and.stub();
                LF.webService.getDeviceID.and.callFake((data, onSuccess, onFailure) => onFailure(400, 500));

                LF.StudyDesign.maxSecurityQuestionAttempts = 5;
                LF.security.getSecurityQuestionFailure.and.returnValue(5);

                LF.ServiceErr = { HTTP_UNAUTHORIZED: 500 };
                this.view.reactivate();

                return Q().tap(() => {
                    expect(this.view.showInputError).toHaveBeenCalledWith('#secret_answer', 'INCORRECT_ANSWER');
                });
            });

            Async.it('should reactivate.', () => {
                spyOn(this.view, 'attemptTransmission').and.callFake(() => Q(true));
                spyOn(ELF, 'trigger').and.callFake(() => Q());
                spyOn(this.view, 'getSubjectData').and.stub();
                spyOn(Logger.prototype, 'operational').and.stub();
                LF.webService.getDeviceID.and.callFake((data, onSuccess, onFailure) => {
                    onSuccess({
                        Q: 0,
                        A: hex_sha512('1'),
                        D: '12345',
                        W: hex_sha512('apple')
                    });
                });

                this.view.reactivate();

                return Q().tap(() => {
                    expect(Data.question).toBe(0);
                    expect(Data.answer).toBe(hex_sha512('1'));
                    expect(Data.deviceID).toBe('12345');
                    expect(Data.service_password).toBe(hex_sha512('apple'));
                    expect(Data.password).toBe('apple');
                    expect(ELF.trigger).toHaveBeenCalledWith('REACTIVATION:Transmit', {}, this.view);
                });
            });
        });
    }
}

describe('ForgotPasswordActivationView', () => {
    let suite = new ForgotPasswordActivationViewSuite();

    suite.executeAll({
        id: 'forgot-password-activation-page',
        template: '#forgot-password-activation-tpl',
        button: '#submit',
        input: '#txtNewPassword',
        dynamicStrings: {
            code: '12345',
            key: '54321',
            passwordRules: ''
        },
        exclude: [
            // Should be tested individually
            'testProperty',
            'testAttribute',
            // PageView has no class name.
            'testClass',

            // DOM structure of view does not allow for these tests to pass.
            'testAddHelpText',
            'testRemoveHelpText',
            'testGetValidParent',
            'testInputSuccess',
            'testInputError',
            'testHideKeyboardOnEnter'
        ]
    });
});
