// TODO: US6599
xdescribe('Login', function () {

    var view,
        location,
        maxLoginAttempts = LF.StudyDesign.maxLoginAttempts,
        askSecurityQuestion = LF.StudyDesign.askSecurityQuestion,
        template = '<div id="login-template">' +
            '<a id="pending-transmission" href="#"></a>' +
            '<input type="password" name="password_{{ key }}" id="password" />' +
            '<button disabled type="submit" id="login" />' +
            '</div>';

    beforeEach(function () {

        LF.StudyDesign.maxLoginAttempts = 3;

        spyOn(LF.Helpers, 'checkInstall').andCallFake(function (callback) {
            callback(true);
        });

        LF.router = new LF.Router.ApplicationRouter();

        spyOn(LF.router, 'navigate').andCallFake(function (url) {
            location = url;
        });

        spyOn(LF.DynamicText, 'imageFolder').andCallFake(function (callback) {
            callback('../../trial/images/');
        });

        $('body').append(template);

        LF.Gateways.Subject.schedules = new LF.Collection.Schedules();

    });

    afterEach(function () {

        LF.security = undefined;
        LF.router = undefined;
        LF.schedule = undefined;
        LF.Gateways.Subject.schedules = undefined;
        LF.logs.reset();
        localStorage.clear();

        LF.StudyDesign.maxLoginAttempts = maxLoginAttempts;
        LF.StudyDesign.askSecurityQuestion = askSecurityQuestion;

        $('#login-template').remove();

    });

    it('should install database', function () {

        LF.SpecHelpers.installDatabase();

    });

    it('should create the Subjects storage object and save the record.', function () {

        LF.SpecHelpers.createSubject();
        LF.SpecHelpers.saveSubject();

    });

    it('should create the transmissions storage object and save the record.', function () {

        var response,
            model = new LF.Model.Transmission();

        model.save({
            method  : 'transmitQuestionnaire',
            params  : JSON.stringify({ dashboardId: 1, sigId: 'LA.14f7031e9cc352136068949490'}),
            created : new Date().getTime()
        }, {
            onSuccess : function (res) {
                response = res;
            }
        });

        waitsFor(function () {
            return response;
        }, 'Transmission storage object to be created and the model to save.', 1000);

        runs(function () {
            expect(response).toEqual(1);
        });

    });

    it('should have a default id of \'login-page\'', function () {

        view = new LF.View.LoginView();

        waitsFor(function () {
            return view.$el.html();
        }, 'the timeout callback to invoke.', 2000);

        runs(function () {
            expect(view.$el.attr('id')).toEqual('login-page');
        });

    });

    it('should have a pending transmission count.', function () {

        expect(view.$('#pending-transmission').html()).toEqual('1');

    });

    it('should attempt to transmit pending reports', function () {
        // PhantomJS defect causes it to always return false for navigator.online: https://github.com/ariya/phantomjs/issues/10647
        // Once navigator has been replaced, it will no longer update as expected otherwise
        // Remove this workaround once PhantomJS defect is resolved
        if (!navigator.onLine) {
            let fakeNavigator = _.clone(navigator);
            fakeNavigator.onLine = true;
            navigator = fakeNavigator;
        }

        spyOn(LF.Utilities, 'isStudyOnline').andCallFake(function (callback) {
            callback(true);
        });

        spyOn(LF.Actions, 'transmitAll');
        view.transmit({
            preventDefault : function () { }
        });

        expect(LF.Actions.transmitAll).toHaveBeenCalled();

    });

    it('should navigate to the forgot password view.', function () {

        LF.StudyDesign.askSecurityQuestion = true;

        view.forgotPassword();

        expect(location).toEqual('forgot_password');
        expect(localStorage.getItem('Forgot_Password')).toBeTruthy();

    });

    it('should navigate to the unlock code view when secret question is turned off.', function () {

        LF.StudyDesign.askSecurityQuestion = false;
        view.forgotPassword();

        expect(location).toEqual('unlock_code');
        expect(localStorage.getItem('Unlock_Code')).toBeTruthy();

    });

    it('shouldn\'t validate the password.', function () {

        expect(view.validate('apple', 'krpt.39ed120a0981231')).toBeFalsy();

    });

    it('should validate the password.', function () {

        view.$('#password').val('apple');

        expect(view.validate(hex_sha512('apple' + 'krpt.39ed120a0981231'), 'krpt.39ed120a0981231')).toBeTruthy();

    });

    it('should attempt to login to the application and fail.', function () {

        LF.SpecHelpers.createSubject();
        view.$('#password').val('1234567890');

        view.login({
            preventDefault : function () { }
        });

        expect(view.loginFailures).toEqual(1);
        expect(view.$('#password').val()).toEqual('');

    });

    it('should remove error class on focus', function () {

        view.$('#password').val('1234567890');

        view.login({
            preventDefault : function () { }
        });

        expect(view.$('input#password').hasClass('ui-state-error')).toBeTruthy();

        view.onFocus({
            preventDefault : function () { }
        });

        expect(view.$('input#password').hasClass('ui-state-error')).toBeFalsy();

    });

    it('should attempt to login to the application and succeed.', function () {

        view.$('#password').val('apple');
        view.login({
            preventDefault : function () { }
        });

        expect(view.loginFailures).toEqual(2);

    });

    it('should attempt to login after maxLoginAttempts and redirect to Security Question screen', function () {
        LF.StudyDesign.askSecurityQuestion = true;

        LF.SpecHelpers.createSubject();
        LF.security.setLoginFailure(2);

        view.$('#password').val('wrongPass');
        view.login({
            preventDefault : function () { }
        });

        expect(location).toEqual('forgot_password');
        expect(localStorage.getItem('Forgot_Password')).toBeTruthy();

    });

    it('should run the timer on init if max login attempt is exceeded', function () {

        LF.security.setLoginFailure(3);
        view = new LF.View.LoginView();

        waitsFor(function () {
            return view.$el.html();
        }, 'the timeout callback to invoke.', 2000);

        runs(function () {
            expect(LF.security.checkLock()).toBeTruthy();
        });

    });

    it('should attempt to login to the application after lock timeout is passed and succeed.', function () {

        localStorage.setItem('PHT_Enable_Device_Utc', 12345678);
        view = new LF.View.LoginView();

        waitsFor(function () {
            return view.$el.html();
        }, 'the timeout callback to invoke.', 2000);

        runs(function () {
            view.$('#password').val('apple');
            view.login({
                preventDefault : function () { }
            });

            expect(location).toEqual('dashboard');
        });
    });

    it('shouldn\'t enable login button with no value in password.', function () {

        view.$('#password').val('');

        view.onInput({
            preventDefault : function () { }
        });

        expect(view.$('#login')).toHaveAttr('disabled');

    });

    it('should enable login button with value in password.', function () {

        view.$('#password').val('abc');

        view.onInput({
            preventDefault : function () { }
        });

        expect(view.$('#login')).not.toHaveAttr('disabled');

    });

    it('should navigate to the site login view.', function () {

        view.toSiteLogin({
            preventDefault : function () { }
        });

        expect(location).toEqual('siteLogin');

    });

    // Test Teardown
    it('should uninstall database.', function () {

        LF.SpecHelpers.uninstallDatabase();

    });

});
