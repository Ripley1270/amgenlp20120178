import ReactivationView from 'logpad/views/ReactivationView';
import User from 'core/models/User';
import ELF from 'core/ELF';
import Data from 'core/Data';
import { Banner } from 'core/Notify';
import Spinner from 'core/Spinner';

import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';

import ActivationBaseViewSuite from 'test/specs/core/views/ActivationBaseView.specBase';

class ReactivationViewSuite extends ActivationBaseViewSuite {
    beforeEach () {
        resetStudyDesign();

        this.removeTemplate = specHelpers.renderTemplateToDOM('logpad/templates/reactivation.ejs');
        this.view = new ReactivationView();

        return this.view.resolve()
            .then(() => this.view.render())
            .then(() => super.beforeEach());
    }

    afterEach () {
        this.removeTemplate();

        return super.afterEach();
    }

    executeAll (context) {
        super.executeAll(context);

        let methods = Object.getOwnPropertyNames(ReactivationViewSuite.prototype);

        this.annotateAll(methods, context);
    }

    testResolve () {
        describe('method:resolve', () => {
            Async.it('should fail to resolve.', () => {
                spyOn(User.prototype, 'save').and.reject('DatabaseError');

                let request = this.view.resolve();

                expect(request).toBePromise();

                return request.then(() => fail('Expected method resolve to be rejected.'))
                    .catch(e => expect(e).toBe('DatabaseError'));
            });

            Async.it('should resolve.', () => {
                let request = this.view.resolve();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.user).toEqual(jasmine.any(User));
                    expect(this.view.user.get('id')).toBe(0);
                });
            });
        });
    }

    testRender () {
        describe('method:render', () => {
            Async.it('should render the view.', () => {
                let request = this.view.render();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.$el.html()).toBeDefined();
                });
            });

            Async.it('should not render the view.', () => {
                spyOn(this.view, 'buildHTML').and.callFake(() => Q.reject('DOMError'));

                let request = this.view.render();

                expect(request).toBePromise();

                return request.then(() => fail('Expected method render to have been rejected.'))
                    .catch(e => {
                        expect(e).toBe('DOMError');
                    });
            });
        });
    }

    testSubmit () {
        describe('method:submit', () => {
            let preventDefault = $.noop;

            beforeEach(() => {
                spyOn(Banner, 'closeAll').and.stub();
                spyOn(this.view, 'reactivate').and.stub();
                spyOn(this.view.sound, 'play').and.stub();
            });
            it('should not submit.', () => {
                spyOn(this.view, 'isValidForm').and.returnValue(false);

                this.view.submit({ preventDefault });

                expect(this.view.reactivate).not.toHaveBeenCalled();
                expect(this.view.sound.play).toHaveBeenCalledWith('error-audio');
            });

            it('should submit.', () => {
                spyOn(this.view, 'isValidForm').and.returnValue(true);

                this.view.submit({ preventDefault });

                expect(this.view.reactivate).toHaveBeenCalled();
                expect(this.view.sound.play).not.toHaveBeenCalled();
            });
        });
    }

    testForgotPassword () {
        describe('method:forgotPassword', () => {
            beforeEach(() => {
                spyOn(this.view, 'navigate').and.stub();
                spyOn(Spinner, 'show').and.resolve();
                spyOn(Spinner, 'hide').and.resolve();
            });

            it('should navigate to the unlock code view.', () => {
                LF.StudyDesign.askSecurityQuestion = false;
                spyOn(this.view, 'getSecretQuestion').and.stub();

                this.view.forgotPassword();

                expect(this.view.getSecretQuestion).not.toHaveBeenCalled();
                expect(this.view.navigate).toHaveBeenCalledWith('unlock_code_activation');
            });

            it('should invoke the getSecretQuestion method (forgotPassword).', () => {
                LF.StudyDesign.askSecurityQuestion = true;
                spyOn(this.view, 'getSecretQuestion').and.callFake(forgotPassword => forgotPassword());

                this.view.forgotPassword();

                expect(this.view.navigate).toHaveBeenCalledWith('forgot_password_activation');
            });

            it('should invoke the getSecretQuestion method (unlockCode).', () => {
                LF.StudyDesign.askSecurityQuestion = true;
                spyOn(this.view, 'getSecretQuestion').and.callFake((forgotPassword, unlockCode) => unlockCode());

                this.view.forgotPassword();

                expect(this.view.navigate).toHaveBeenCalledWith('unlock_code_activation');
            });
        });
    }

    testGetSecretQuestion () {
        beforeEach(() => {
            LF.webService = { getSubjectQuestion: $.noop };
        });

        describe('method:getSecretQuestion', () => {
            Async.it('should fail to get the secret question.', () => {
                return Q.Promise(resolve => {
                    Data.code = '12345';

                    spyOn(LF.webService, 'getSubjectQuestion').and.callFake((code, success, error) => {
                        error();
                    });

                    this.view.getSecretQuestion(null, () => {
                        expect(LF.webService.getSubjectQuestion).toHaveBeenCalledWith('12345', jasmine.any(Function), jasmine.any(Function));

                        resolve();
                    });
                });
            });

            Async.it('should get the secret question.', () => {
                return Q.Promise(resolve => {
                    Data.code = '12345';

                    spyOn(LF.webService, 'getSubjectQuestion').and.callFake((code, success) => {
                        success([{ Q: 0 }]);
                    });

                    this.view.getSecretQuestion(() => {
                        expect(Data.question).toBeDefined();
                        expect(Data.question.key).toBe(0);

                        resolve();
                    });
                });
            });

            Async.it('should get the secret question, but trigger the error callback.', () => {
                return Q.Promise(resolve => {
                    Data.code = '12345';
                    delete Data.question;

                    spyOn(LF.webService, 'getSubjectQuestion').and.callFake((code, success) => {
                        success([]);
                    });

                    this.view.getSecretQuestion(null, () => {
                        expect(Data.question).toBe(undefined);

                        resolve();
                    });
                });
            });
        });
    }

    testReactivate () {
        describe('method:reactivate', () => {
            beforeEach(() => {
                LF.security.checkLock = $.noop;
                LF.security.resetFailureCount = $.noop;
                LF.security.setLoginFailure = $.noop;
                LF.security.getLoginFailure = $.noop;

                LF.webService.getDeviceID = $.noop;
            });

            it('should invok the forgot password method.', () => {
                spyOn(LF.security, 'checkLock').and.returnValue(true);
                spyOn(this.view, 'forgotPassword').and.stub();

                this.view.reactivate();

                expect(this.view.forgotPassword).toHaveBeenCalled();
            });

            // Mocked method will display a notification, but that is tested elsewhere.
            Async.it('should do nothing.', () => {
                spyOn(LF.security, 'checkLock').and.returnValue(false);
                spyOn(LF.webService, 'getDeviceID').and.stub();
                spyOn(this.view, 'attemptTransmission').and.reject();

                this.view.reactivate();

                return Q().then(() => {
                    expect(LF.webService.getDeviceID).not.toHaveBeenCalled();
                });
            });

            Async.it('should do nothing (not online).', () => {
                spyOn(LF.security, 'checkLock').and.returnValue(false);
                spyOn(LF.webService, 'getDeviceID').and.stub();
                spyOn(this.view, 'attemptTransmission').and.resolve(false);

                this.view.reactivate();

                return Q().then(() => {
                    expect(LF.webService.getDeviceID).not.toHaveBeenCalled();
                });
            });

            Async.it('should fail to get the device ID and show a transmission error.', () => {
                spyOn(LF.security, 'checkLock').and.returnValue(false);
                spyOn(LF.webService, 'getDeviceID').and.callFake((data, success, error) => {
                    error(500, 400);
                });
                spyOn(this.view, 'attemptTransmission').and.resolve(true);
                spyOn(this.view, 'showTransmissionError');

                this.view.reactivate();

                return Q().then(() => {
                    expect(this.view.showTransmissionError).toHaveBeenCalled();
                });
            });

            Async.it('should fail to get the device ID and disable the submit button.', () => {
                LF.ServiceErr = { HTTP_UNAUTHORIZED: 401 };

                spyOn(LF.security, 'checkLock').and.returnValue(false);
                spyOn(LF.webService, 'getDeviceID').and.callFake((data, success, error) => {
                    error(500, 401);
                });
                spyOn(this.view, 'disableButton').and.stub();
                spyOn(this.view, 'attemptTransmission').and.resolve(true);
                spyOn(this.view, 'showTransmissionError');

                this.view.reactivate();

                return Q().then(() => {
                    expect(this.view.disableButton).toHaveBeenCalledWith(this.view.$submit);
                    expect(this.view.showTransmissionError).not.toHaveBeenCalled();
                });
            });

            Async.it('should succeed and trigger a transmit event.', () => {
                spyOn(LF.security, 'checkLock').and.returnValue(false);
                spyOn(LF.webService, 'getDeviceID').and.callFake((data, success, error) => {
                    success({ Q: 0, A: '12345', D: '54321', W: '123456' });
                });
                spyOn(this.view, 'attemptTransmission').and.resolve(true);
                spyOn(ELF, 'trigger').and.resolve(true);
                spyOn(this.view, 'getSubjectData').and.stub();

                this.view.reactivate();

                return Q.delay().then(() => {
                    expect(ELF.trigger).toHaveBeenCalledWith('REACTIVATION:Transmit', { }, this.view);
                    expect(this.view.getSubjectData).toHaveBeenCalled();
                });
            });
        });
    }

    testOnInput () {
        describe('method:onInput', () => {
            it('should disable the submit button.', () => {
                spyOn(this.view, 'disableButton').and.stub();

                this.view.onInput();

                expect(this.view.disableButton).toHaveBeenCalledWith(this.view.$submit);
            });

            it('should enable the submit button.', () => {
                spyOn(this.view, 'enableButton').and.stub();

                this.view.$password.val('12345');
                this.view.onInput();

                expect(this.view.enableButton).toHaveBeenCalledWith(this.view.$submit);
            });
        });
    }

    testBack () {
        describe('method:back', () => {
            it('should trigger an ELF event.', () => {
                spyOn(ELF, 'trigger').and.stub();

                this.view.back();

                expect(ELF.trigger).toHaveBeenCalledWith('REACTIVATION:Backout', {}, this.view);
            });
        });
    }
}

describe('ReactivationView', () => {
    let suite = new ReactivationViewSuite();

    suite.executeAll({
        id: 'reactivation-page',
        template: '#reactivation-template',
        button: '#submit',
        input: '#txtReCode',
        dynamicStrings: { key: '12345', code: '54321' },
        exclude: [
            // Should be tested individually
            'testProperty',
            'testAttribute',
            // PageView has no class name.
            'testClass',
            'testHideKeyboardOnEnter'
        ]
    });
});
