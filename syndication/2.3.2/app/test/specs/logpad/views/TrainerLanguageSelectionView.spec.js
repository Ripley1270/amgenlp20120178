import LanguageSelectionView from 'trainer/logpad/TrainerLanguageSelectionView';
import CurrentContext from 'core/CurrentContext';
import * as specHelpers from 'test/helpers/SpecHelpers';

TRACE_MATRIX('US5955').
describe('TrainerLanguageSelectionView', () => {
    let view,
        removeTemplate,
        preventDefault = $.noop,
        languageList = [{
            namespace: 'CORE',
            language: 'en',
            locale: 'US',
            localized: 'English (US)',
            direction: 'ltr',
            resources: {
                APPLICATION_HEADER: 'ERT',
                LANGUAGE: 'Language',
                SELECT_LANGUAGE: 'Please select a preferred language for the subject.',
                NEXT: 'Next',
                BACK: 'Back'
            }
        }, {
            namespace: 'CORE',
            language: 'es',
            locale: 'ES',
            localized: 'Español (España)',
            direction: 'ltr',
            resources: {
                APPLICATION_HEADER: 'ERT',
                LANGUAGE: 'Idioma:',
                SELECT_LANGUAGE: 'Please select a preferred language for the subject. (es-ES)',
                NEXT: 'Siguiente',
                BACK: 'Atrás'
            }
        }, {
            namespace: 'CORE',
            language: 'fr',
            locale: 'FR',
            localized: 'Français (France)',
            direction: 'ltr',
            resources: {
                APPLICATION_HEADER: 'ERT',
                LANGUAGE: 'Langue :',
                SELECT_LANGUAGE: 'Please select a preferred language for the subject. (fr-FR)',
                NEXT: 'Suivant',
                BACK: 'Précédent'
            }
        }];

    Async.beforeEach(() => {
        removeTemplate = specHelpers.renderTemplateToDOM('trainer/logpad/templates.ejs');

        CurrentContext.init();
        CurrentContext().setContextLanguage('es-ES');

        spyOn(LF.strings, 'getLanguages').and.callFake(() => languageList);

        view = new LanguageSelectionView();
        spyOn(view, 'reload').and.stub();
        spyOn($.fn, 'select2').and.stub();

        return view.resolve()
        .then(() => view.render());
    });

    afterEach(() => removeTemplate());

    it('should have the correct id.', () => {
        expect(view.id).toBe('trainer-setup-page');
    });

    it('should have the correct template value.', () => {
        expect(view.template).toBe('#trainer-language-selection-template');
    });

    describe('method:render', () => {
        Async.it('should fail to render.', () => {
            spyOn(view, 'buildHTML').and.callFake(() => Q.reject('DOMError'));

            let request = view.render();

            expect(request).toBePromise();

            return request.then(() => fail('Method render should have been rejected.'))
            .catch(e => expect(e).toBe('DOMError'));
        });

        Async.it('should call the correct functions.', () => {
            spyOn(view, 'renderLanguages').and.stub();

            let request = view.render();

            expect(request).toBePromise();

            return request.then(() => {
                expect(view.renderLanguages).toHaveBeenCalled();
                expect($.fn.select2).toHaveBeenCalled();
            });
        });
    });

    describe('method:renderLanguages', () => {
        beforeEach(() => view.$language.html(''));

        it('should render all languages.', () => {
            view.renderLanguages();

            let options = view.$language.find('option');

            expect(options.length).toBe(LF.strings.getLanguages().length);
        });

        it('should render with the default language selected', () => {
            view.renderLanguages();
            expect(view.$language.val()).toEqual('es-ES');
        });

        it('should render with the pre-selected language selected', () => {
            spyOn(localStorage, 'getItem').and.callFake((key) => {
                if (key === 'preferredLanguageLocale') {
                    return 'fr-FR';
                }
            });

            view.renderLanguages();
            expect(view.$language.val()).toEqual('fr-FR');
        });
    });

    describe('method:next', () => {
        beforeEach(() => {
            spyOn(view, 'navigate').and.stub();
        });

        it('should navigate to the "Set Time Zone Activation" screen with default language', () => {
            spyOn(localStorage, 'setItem').and.stub();

            view.next();

            expect(localStorage.setItem).toHaveBeenCalledWith('preferredLanguageLocale', 'es-ES');
            expect(view.navigate).toHaveBeenCalledWith('set_time_zone_activation', true);
        });

        it('should navigate to the "Set Time Zone Activation" screen with selected language', () => {
            spyOn(localStorage, 'setItem').and.stub();

            view.$language.val('fr-FR');

            view.next();

            expect(localStorage.setItem).toHaveBeenCalledWith('preferredLanguageLocale', 'fr-FR');
            expect(view.navigate).toHaveBeenCalledWith('set_time_zone_activation', true);
        });
    });
});
