import ChangePasswordView from 'logpad/views/ChangePasswordView';
import PatientSession from 'core/classes/PatientSession';
import User from 'core/models/User';
import Subject from 'core/models/Subject';
import Subjects from 'core/collections/Subjects';
import ELF from 'core/ELF';
import Logger from 'core/Logger';
import PasswordBaseView from 'core/views/PasswordBaseView';
import * as lStorage from 'core/lStorage';
import CurrentSubject from 'core/classes/CurrentSubject';

import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';

import PasswordBaseViewSuite from 'test/specs/core/views/PasswordBaseView.specBase';

class ChangePasswordViewSuite extends PasswordBaseViewSuite {

    beforeEach () {
        // Mock out the security object and any required methods.
        LF.security = jasmine.createSpyObj('security', [
            'checkLogin',
            'getUser',
            'logout',
            'pauseSessionTimeOut',
            'restartSessionTimeOut',
            'listenForActivity'
        ]);

        // Mock checkLogin and getUser.
        LF.security.checkLogin.and.callFake(() => Q(true));
        LF.security.getUser.and.callFake(() => {
            let user = new User({
                id: 1,
                username: 'admin',
                secretAnswer: hex_sha512('apple'),
                password: hex_sha512('appleorange'),
                salt: 'orange',
                secretQuestion: '0',
                role: 'admin'
            });
            LF.security.activeUser = user;

            return Q(user);
        });

        spyOn(CurrentSubject, 'getSubject').and.callFake(() => {
            let subject = new Subject({
                phase: '10',
                subject_active: 0
            });

            return Q(subject);
        });

        this.removeTemplate = specHelpers.renderTemplateToDOM('logpad/templates/change-password.ejs');
        LF.StudyDesign.showPasswordRules = false;

        this.view = new ChangePasswordView();

        // We need to set this so the user will be fetched from within ActivationBaseView.resolve().
        lStorage.setItem('User_Login', 1);

        return this.view.resolve()
            .then(() => this.view.render())
            .then(() => super.beforeEach());
    }

    afterEach () {
        this.removeTemplate();

        return super.afterEach();
    }

    testBack () {
        describe('method:back', () => {
            it('should navigate to the toolbox page with the back button', () => {
                spyOn(this.view, 'navigate').and.stub();

                this.view.back({ preventDefault: $.noop });

                expect(this.view.navigate).toHaveBeenCalledWith('toolbox');
            });
        });
    }

    testResolve () {
        describe('method:resolve', () => {
            Async.it('should resolve.', () => {
                let request = this.view.resolve();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.user).toEqual(jasmine.any(User));
                });
            });
        });
    }

    testOnPasswordSaved () {
        describe('method:onPasswordSaved', () => {
            Async.it('should navigate back to the toolbox.', () => {
                spyOn(this.view, 'navigate').and.stub();
                spyOn(ELF, 'trigger').and.resolve();
                spyOn(Logger.prototype, 'operational').and.resolve();

                let request = this.view.onPasswordSaved();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(LF.security.restartSessionTimeOut).toHaveBeenCalled();
                    expect(this.view.navigate).toHaveBeenCalledWith('toolbox');
                    expect(Logger.prototype.operational).toHaveBeenCalledWith(`Password changed by user admin`, {
                        username : 'admin',
                        role : 'admin'
                    }, 'ChangePass');
                });
            });
        });
    }

    testRender () {
        describe('method:render', () => {
            beforeEach(() => {
                LF.StudyDesign.showPasswordRules = false;
            });

            Async.it('should render the view.', () => {
                let request = this.view.render();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.$el.html()).toBeDefined();
                });
            });

            Async.it('should fail to render the view.', () => {
                spyOn(this.view, 'buildHTML').and.reject('Error');
                spyOn(Logger.prototype, 'error').and.stub();

                let request = this.view.render();

                expect(request).toBePromise();

                return request.catch(e => {
                    expect(Logger.prototype.error).toHaveBeenCalledWith('Error');
                });
            });
        });
    }

    testSubmit () {
        describe('method:submit', () => {
            Async.beforeEach(() => this.view.buildHTML({ key: this.view.key, passwordRules: '' }));

            it('should not submit.', () => {
                spyOn(PasswordBaseView.prototype, 'submit').and.stub();
                spyOn(Logger.prototype, 'error').and.stub();

                this.view.$currentPassword.val('pr');
                this.view.submit({ preventDefault: $.noop });

                expect(PasswordBaseView.prototype.submit).not.toHaveBeenCalled();
                expect(Logger.prototype.error).toHaveBeenCalledWith('PASSWORD_INVALID');
            });

            it('should submit.', () => {
                spyOn(PasswordBaseView.prototype, 'submit').and.stub();

                this.view.$currentPassword.val('apple');
                this.view.submit({ preventDefault: $.noop });

                expect(PasswordBaseView.prototype.submit).toHaveBeenCalled();
            });
        });
    }
}

describe('ChangePasswordView', () => {
    let suite = new ChangePasswordViewSuite();

    suite.executeAll({
        id: 'change-password-page',
        template: '#change-password-template',
        input: '#txtConfirmPassword',
        button: '#submit',
        dynamicStrings:  {
            key: '12345',
            passwordRules: ''
        },
        exclude: [
            // Should be tested individually
            'testProperty',
            'testAttribute',
            // PageView has no class name.
            'testClass',

            // DOM structure of view does not allow for these tests to pass.
            'testAddHelpText',
            'testRemoveHelpText',
            'testGetValidParent',
            'testInputSuccess',
            'testInputError',
            'testHideKeyboardOnEnter'
        ]
    });

    suite.testBack();
    suite.testOnPasswordSaved();
    suite.testResolve();
    suite.testRender();
    suite.testSubmit();
});
