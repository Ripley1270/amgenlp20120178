import InstallView from 'logpad/views/InstallView';
import DatabaseVersion from 'core/models/DatabaseVersion';
import DatabaseVersions from 'core/collections/DatabaseVersions';
import Users from 'core/collections/Users';
import User from 'core/models/User';
import Subject from 'core/models/Subject';
import Subjects from 'core/collections/Subjects';
import Role from 'core/models/Role';
import Roles from 'core/collections/Roles';
import LastDiaries from 'core/collections/LastDiaries';
import LastDiary from 'core/models/LastDiary';
import Data from 'core/Data';
import Logger from 'core/Logger';
import COOL from 'core/COOL';

import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';

import PageViewSuite from 'test/specs/core/views/PageView.specBase';

class InstallViewSuite extends PageViewSuite {

    beforeEach () {
        resetStudyDesign();

        this.removeSpinnerTemplate = specHelpers.renderTemplateToDOM('core/templates/spinner.ejs');
        this.removeTemplate = specHelpers.renderTemplateToDOM('logpad/templates/install.ejs');

        this.view = new InstallView();

        spyOn(this.view, 'doInstall').and.resolve();

        return this.view.resolve()
        .then(() => this.view.render())
        .then(() => super.beforeEach());
    }

    afterEach () {
        this.removeTemplate();
        this.removeSpinnerTemplate();

        return super.afterEach();
    }

    executeAll (context) {
        super.executeAll(context);

        let methods = Object.getOwnPropertyNames(InstallViewSuite.prototype);

        this.annotateAll(methods, context);
    }

    testDoInstall () {
        describe('method:doInstall', () => {
            it('check resolve calls doInstall()', () => {
                const view = new InstallView();

                spyOn(view, 'doInstall');

                view.resolve();
                expect(view.doInstall).toHaveBeenCalled();
            });
        });
    }

    testResolve () {
        describe('method:resolve', () => {
            Async.it('should resolve.', () => {
                delete this.view.installPromise;

                this.view.doInstall.and.resolve();
                let request = this.view.resolve();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.installPromise);
                });
            });
        });
    }

    testRender () {
        describe('method:render', () => {
            Async.it('should render the view.', () => {
                let request = this.view.render();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.$el.html()).toBeDefined();
                });
            });

            Async.it('should not render the view.', () => {
                spyOn(this.view, 'buildHTML').and.callFake(() => Q.reject('DOMError'));

                let request = this.view.render();

                expect(request).toBePromise();

                return request.then(() => fail('Expected method render to have been rejected.'))
                    .catch(e => {
                        expect(e).toBe('DOMError');
                    });
            });
        });
    }

    testInstallSubjectData () {
        describe('method:installSubjectData', () => {
            afterEach(() => localStorage.removeItem('PHT_TEMP'));

            Async.it('should fail to install the subject data.', () => {
                spyOn(Subject.prototype, 'save').and.reject('DatabaseError');

                let request = this.view.installSubjectData();

                expect(request).toBePromise();

                return request.then(() => fail('Expected method installSubjectData to have been rejected.'))
                    .catch(e => expect(e).toBe('DatabaseError'));
            });

            Async.it('should install the subject data.', () => {
                let Utilities = COOL.getClass('Utilities');

                spyOn(Subject.prototype, 'save').and.resolve();

                // These methods should be tested already, so we can just resole them.
                spyOn(Users, 'updateUsers').and.resolve();
                spyOn(Utilities, 'isOnline').and.resolve(true);

                localStorage.setItem('PHT_TEMP', JSON.stringify({ initials: 'ABC' }));

                let request = this.view.installSubjectData();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(Users.updateUsers).toHaveBeenCalled();
                    expect(Utilities.isOnline).toHaveBeenCalled();
                });
            });
        });
    }

    testInstallDatabaseVersion () {
        describe('method:installHistoricalData', () => {
            beforeEach(() => {
                spyOn(Logger.prototype, 'error').and.stub();
            });

            Async.it('should fail to save the database version.', () => {
                spyOn(DatabaseVersion.prototype, 'save').and.reject('DatabaseError');

                let request = this.view.installDatabaseVersion();

                expect(request).toBePromise();

                return request.finally(() => {
                    expect(Logger.prototype.error).toHaveBeenCalledWith(`Error saving model for databaseVersion`, 'DatabaseError');
                });
            });

            Async.it('should save the database versions.', () => {
                spyOn(DatabaseVersion.prototype, 'save').and.resolve();

                let request = this.view.installDatabaseVersion();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(DatabaseVersion.prototype.save.calls.count()).toBe(2);
                });
            });
        });
    }

    testInstallHistoricalData () {
        describe('method:installDatabaseVersion', () => {
            beforeEach(() => {
                spyOn(Logger.prototype, 'error').and.stub();
            });

            Async.it('should fail to install the historic data.', () => {
                let tempStorage = 'PHT_LastDiary';
                let model = 'LastDiary';
                let historicData = {
                    krpt: 'NP.1447382bdaad4a85bb4b4f3e33c5105b',
                    questionnaire_id: 'NRS',
                    lastStartedDate: '2013-11-14T12:32:45',
                    lastCompletedDate: '2013-11-14T17:35:07'
                };

                spyOn(LastDiary.prototype, 'save').and.reject('DatabaseError');

                localStorage.setItem('PHT_LastDiary', JSON.stringify(historicData));

                let request = this.view.installHistoricalData({ tempStorage, model });

                expect(request).toBePromise();

                return request.finally(() => {
                    expect(Logger.prototype.error).toHaveBeenCalledWith('Model could not save in installHistoricalData', jasmine.any(String));
                });
            });

            Async.it('should not install the historic data (empty object).', () => {
                let tempStorage = 'PHT_LastDiary';
                let model = 'LastDiary';
                let historicData = {};

                spyOn(LastDiary.prototype, 'save');
                localStorage.setItem('PHT_LastDiary', JSON.stringify(historicData));

                let request = this.view.installHistoricalData({ tempStorage, model });

                expect(request).toBePromise();

                return request.then(() => {
                    expect(LastDiary.prototype.save).not.toHaveBeenCalled();
                    expect(Logger.prototype.error).not.toHaveBeenCalled();
                });
            });

            Async.it('should not install the historic data (empty array).', () => {
                let tempStorage = 'PHT_LastDiary';
                let model = 'LastDiary';
                let historicData = [];

                spyOn(LastDiary.prototype, 'save');
                localStorage.setItem('PHT_LastDiary', JSON.stringify(historicData));
                let request = this.view.installHistoricalData({ tempStorage, model });

                expect(request).toBePromise();

                return request.then(() => {
                    expect(LastDiary.prototype.save).not.toHaveBeenCalled();
                    expect(Logger.prototype.error).not.toHaveBeenCalled();
                });
            });

            Async.it('should install the historic data.', () => {
                let tempStorage = 'PHT_LastDiary';
                let model = 'LastDiary';
                let historicData = {
                    krpt: 'NP.1447382bdaad4a85bb4b4f3e33c5105b',
                    questionnaire_id: 'NRS',
                    lastStartedDate: '2013-11-14T12:32:45',
                    lastCompletedDate: '2013-11-14T17:35:07'
                };

                spyOn(LastDiary.prototype, 'save').and.resolve();
                localStorage.setItem('PHT_LastDiary', JSON.stringify(historicData));

                let request = this.view.installHistoricalData({ tempStorage, model });

                expect(request).toBePromise();

                return request.then(() => {
                    expect(LastDiary.prototype.save).toHaveBeenCalled();
                    expect(Logger.prototype.error).not.toHaveBeenCalled();
                });
            });
        });
    }
}

describe('InstallView', () => {
    let suite = new InstallViewSuite();

    suite.executeAll({
        id: 'install-page',
        template: '#install-template',
        exclude: [
            // Should be tested individually
            'testProperty',
            'testAttribute',
            // PageView has no class name.
            'testClass',

            // DOM structure of view does not allow for these tests to pass.
            'testClearInputState',
            'testValidateInput',
            'testHideElement',
            'testShowElement',
            'testDisableElement',
            'testEnableElement',
            'testAddHelpText',
            'testRemoveHelpText',
            'testGetValidParent',
            'testInputSuccess',
            'testInputError',
            'testHideKeyboardOnEnter'
        ]
    });
});
