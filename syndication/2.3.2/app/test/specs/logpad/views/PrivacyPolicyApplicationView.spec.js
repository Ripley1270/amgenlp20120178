import PrivacyPolicyApplicationView from 'logpad/views/PrivacyPolicyApplicationView';
import ELF from 'core/ELF';

import * as specHelpers from 'test/helpers/SpecHelpers';

describe('PrivacyPolicyApplicationView', () => {
    let view,
        removeTemplate;

    beforeEach(done => {
        LF.StudyDesign = { };

        removeTemplate = specHelpers.renderTemplateToDOM('logpad/templates/privacy-policy.ejs');
        view = new PrivacyPolicyApplicationView();

        spyOn(view, 'navigate').and.stub();

        view.resolve()
            .then(() => view.render())
            .catch(e => fail(e))
            .done(done);
    });

    afterEach(() => {
        removeTemplate();
        view = null;
    });

    it('should render  \'privacy-policy-application-page\'', () => {
        expect(view.$el.attr('id')).toEqual('privacy-policy-application-page');
    });

    it('should redirect to \'toolbox\'', () => {
        view.back();

        expect(view.navigate).toHaveBeenCalledWith('toolbox');
    });
});
