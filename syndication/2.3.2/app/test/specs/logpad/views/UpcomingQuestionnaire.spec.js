import Questionnaire from 'core/models/Questionnaire';
import Templates from 'core/collections/Templates';
import UpcomingQuestionnaire from 'logpad/views/UpcomingQuestionnaire';

import * as specHelpers from 'test/helpers/SpecHelpers';
import { resetStudyDesign } from 'test/helpers/StudyDesign';

describe('UpcomingQuestionnaire', () => {
    let questionnaire,
        location,
        rule,
        view;

    afterAll(done => specHelpers.uninstallDatabase().done(done));

    beforeEach(done => {
        resetStudyDesign();

        location = 'dashboard';

        questionnaire = new Questionnaire({
            id              : 'LogPad',
            SU              : 'LogPad',
            displayName     : 'DISPLAY_NAME',
            className       : 'medication',
            previousScreen  : false,
            screens         : ['LogPad_S_1', 'LogPad_S_2']
        });

        LF.strings.add([
            {
                language: 'en',
                locale: 'US',
                namespace: 'LogPad',
                dir: 'ltr',
                resources: {
                    DISPLAY_NAME: 'LogPad App'
                }
            }
        ]);

        // Add the template required to render the view.
        LF.templates = new Templates([{
            name        : 'Questionnaire',
            namespace   : 'DEFAULT',
            template    : '<a class="navigate-right"><strong>{{ title }}</strong></a>'
        }]);

        // Set up a mock router to capture navigation.
        LF.router = jasmine.createSpy();
        LF.router.navigate = jasmine.createSpy().and.callFake(url => location = url);

        specHelpers.createSubject();
        specHelpers.saveSubject()
            .catch(e => fail(e))
            .done(done);

    });

    afterEach(() => {
        location = undefined;
        LF.router = undefined;
        LF.strings.remove(LF.strings.where({
            namespace: 'LogPad'
        })[0]);
    });

    it('should render the view', (done) => {

        view = new UpcomingQuestionnaire({
            model         : questionnaire,
            scheduleId    : 'Daily_LogPad'
        });

        Q.delay()
            .tap(() => {
                expect(view.$('strong').html()).toEqual('LogPad App');
            })
            .catch(e => fail(e))
            .done(done);

    });

    describe('method:open', () => {

        xit('should trigger the rule on open.', function () {

            var response;

            rule = new LF.Model.Rule(
                {
                    id: 'openQuestionnaire',
                    trigger: 'DASHBOARD:OpenQuestionnaire/LogPad',
                    expression: true,
                    actionData: [
                        {
                            trueAction: function () {
                                response = 'LogPad';
                            }
                        }
                    ]
                }
            );

            LF.StudyRules.add(rule);

            view.open({
                preventDefault: function () {
                }
            });

            waitsFor(function () {
                return response;
            }, 'the rule to trigger', 1000);

            runs(function () {
                LF.StudyRules.remove(rule);
                expect(response).toEqual('LogPad');
            });

        });

        xit('should open the questionnaire.', function () {

            view.open({
                preventDefault: function () {
                }
            });

            waitsFor(function () {
                return location !== 'dashboard';
            }, 'the questionnaire to open', 1000);

            runs(function () {
                expect(location).toEqual('questionnaire/LogPad/Daily_LogPad');
            });

        });

    });

});
