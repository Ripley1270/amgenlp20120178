import SecretQuestionView from 'logpad/views/SecretQuestionView';
import Data from 'core/Data';

import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';

describe('SecretQuestion', () => {
    let view,
        removeTemplate;

    Async.beforeEach(() => {
        resetStudyDesign();

        spyOn(SecretQuestionView.prototype, 'preInstallCheck').and.stub();
        removeTemplate = specHelpers.renderTemplateToDOM('logpad/templates/secret-question.ejs');

        Data.code = '31351894';
        Data.password = 'apple';

        view = new SecretQuestionView();

        spyOn(view, 'navigate').and.stub();

        return view.resolve()
            .then(() => view.render());
    });

    afterEach(() => removeTemplate());

    it('should have an id of \'secret-question-page\'.', () => {
        expect(view.$el.attr('id')).toEqual('secret-question-page');
    });

    it('should add the security questions to the view.', () => {
        expect(view.$secretQuestion.children('option').length).not.toEqual(0);
    });

    describe('method:back', () => {
        it('should navigate back to the activation view.', () => {
            view.back();

            expect(view.navigate).toHaveBeenCalledWith('activation');
        });
    });

    describe('method:validate', () => {
        it('should validate the page.', () => {
            spyOn(view, 'validatePage').and.stub();

            view.validate();

            expect(view.validatePage).toHaveBeenCalled();
        });
    });

    describe('method:render', () => {
        Async.it('should fail to render the view.', () => {
            spyOn(view, 'buildHTML').and.reject('DOMError');

            let request = view.render();

            expect(request).toBePromise();

            return request.then(() => fail('Method render should have been rejected.'))
                .catch(e => expect(e).toBe('DOMError'));
        });
    });

    describe('method:validatePage', () => {
        it('should return false.', () => {
            expect(view.validatePage()).toBe(false);
        });

        it('should return true.', () => {
            view.$secretAnswer.val('apple');

            expect(view.validatePage()).toBe(true);
        });
    });

    describe('method:clearSecretAnswer', () => {
        it('should clear the secret answer field.', () => {
            view.$secretAnswer.val('orange');

            view.clearSecretAnswer();

            expect(view.$secretAnswer.val()).toEqual('');
        });
    });

    describe('method:submit', () => {
        let preventDefault = $.noop;

        beforeEach(() => {
            spyOn(view, 'activate').and.stub();
        });

        it('should not activate.', () => {
            view.submit({ preventDefault });

            expect(view.activate).not.toHaveBeenCalled();
            expect(view.submitted).toBe(false);
        });

        it('should activate.', () => {
            view.$secretAnswer.val('apple');
            view.submit({ preventDefault });

            expect(view.activate).toHaveBeenCalled();
            expect(view.submitted).toBe(true);
        });
    });
});
