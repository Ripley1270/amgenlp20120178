import CodeEntryView from 'logpad/views/CodeEntryView';
import Logger from 'core/Logger';
import Environments from 'core/collections/Environments';
import CurrentContext from 'core/CurrentContext';

import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';

import ActivationBaseViewSuite from 'test/specs/core/views/ActivationBaseView.specBase';

class CodeEntryViewSuite extends ActivationBaseViewSuite {
    beforeAll () {
        CurrentContext.init();
        return super.beforeAll();
    }

    beforeEach () {
        resetStudyDesign();

        this.removeTemplate = specHelpers.renderTemplateToDOM('logpad/templates/code-entry.ejs');
        this.view = new CodeEntryView();

        // We need to wait until the view is rendered...
        return this.view.render()
        .then(() => super.beforeEach());
    }

    afterEach () {
        this.removeTemplate();
        resetStudyDesign();

        return super.afterEach();
    }

    testRender () {
        describe('method:render', () => {
            Async.it('should throw an error.', () => {
                spyOn(this.view, 'buildHTML').and.reject(new Error('DOMError'));
                spyOn(Logger.prototype, 'error').and.stub();

                let request = this.view.render();

                expect(request).toBePromise();

                return request
                .finally(() => expect(Logger.prototype.error).toHaveBeenCalledWith('CodeEntryView could not render', jasmine.any(Error)));
            });
        });
    }

    testSubmit () {
        describe('method:submit', () => {
            it('should attempt to process a setup code', () => {
                this.view.$txtCode.val('12345678');
                this.view.$txtStudy.val('https://unittest.ertstudies.com');

                spyOn(this.view, 'processSetupCode').and.stub();
                this.view.submit({ preventDefault: $.noop });

                expect(this.view.processSetupCode).toHaveBeenCalled();
            });
        });
    }

    testOnInput () {
        describe('method:onInput', () => {
            it('should enable the button (both fields filled out).', () => {
                spyOn(this.view, 'enableButton');
                this.view.$txtCode.val('12345678');
                this.view.$txtStudy.val('https://unittest.ertstudies.com');
                this.view.onInput();

                expect(this.view.enableButton).toHaveBeenCalled();
            });

            it('should disable the button (url empty).', () => {
                spyOn(this.view, 'disableButton');
                this.view.$txtCode.val('12345678');
                this.view.$txtStudy.val('');
                this.view.onInput();

                expect(this.view.disableButton).toHaveBeenCalled();
            });

            it('should disable the button (both fields empty).', () => {
                spyOn(this.view, 'disableButton');
                this.view.$txtCode.val('');
                this.view.$txtStudy.val('');
                this.view.onInput();

                expect(this.view.disableButton).toHaveBeenCalled();
            });

            it('should disable the button (setup code empty).', () => {
                spyOn(this.view, 'disableButton');
                this.view.$txtStudy.val('https://unittest.ertstudies.com');
                this.view.$txtCode.val('');
                this.view.onInput();

                expect(this.view.disableButton).toHaveBeenCalled();
            });
        });
    }

    testProcessQRCode () {
        describe('method:processQRText', () => {
            beforeEach(() => {
                spyOn(Logger.prototype, 'error');
            });

            it('should log an error (invalid JSON).', () => {
                this.view.processQRText('{invalid}');

                expect(Logger.prototype.error).toHaveBeenCalledWith(jasmine.any(String));
            });

            it('should log an error (no study).', () => {
                this.view.processQRText('{}');

                expect(Logger.prototype.error).toHaveBeenCalledWith('Missing study param in QR text');
            });

            it('should log an error (no setup code).', () => {
                spyOn(this.view, 'getStudyUrl').and.returnValue('http://localhost:3000');

                this.view.processQRText(JSON.stringify({study: 'DemoStudy'}));

                expect(Logger.prototype.error).toHaveBeenCalledWith('Missing setupcode param in QR text');
            });

            it('should process the QRCode.', () => {
                spyOn(this.view, 'getStudyUrl').and.returnValue('http://localhost:3000');
                this.view.processQRText(JSON.stringify({study: 'DemoStudy', setupcode: '12345678', language: 'en-US'}));

                expect(Logger.prototype.error).not.toHaveBeenCalled();
                expect(this.view.preferredLanguage).toBe('en');
                expect(this.view.preferredLocale).toBe('US');
            });
        });
    }

    testCheckSubjectIsActive () {
        describe('method:checkSubjectIsActive', () => {
            Async.it('should do nothing.', () => {
                spyOn(this.view.webService, 'getSubjectActive');
                spyOn(this.view, 'attemptTransmission').and.resolve(false);

                let request = this.view.checkSubjectIsActive(987654321);

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.webService.getSubjectActive).not.toHaveBeenCalled();
                });
            });

            Async.it('should activate the user.', () => {
                spyOn(this.view, 'attemptTransmission').and.resolve(true);
                spyOn(this.view.webService, 'getSubjectActive').and.stub();

                let request = this.view.checkSubjectIsActive('12345678');

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.webService.getSubjectActive)
                        .toHaveBeenCalledWith('12345678', jasmine.any(Function), jasmine.any(Function));
                });
            });
            // @TODO - There are a few more tests that can be written here.
        });
    }

    testGetStudyUrl () {
        describe('method:getStudyUrl', () => {
            beforeEach(() => {
                LF.StudyDesign.serviceUrlFormat = 'https://{{study}}.nowhere.not/{{study}}';
                LF.StudyDesign.environments = new Environments([{
                    id: 'dev',
                    label: 'Development',
                    modes: ['provision'],
                    url: 'http://localhost:3000',
                    studyDbName: 'Syndication'
                }, {
                    id: 'ust',
                    label: 'UST',
                    modes: ['provision'],
                    url: 'https://segment1.segment2.gamma.not',
                    studyDbName: 'Rhubarb'
                }]);
            });

            describe('when custom environments are disabled', () => {
                beforeEach(() => {
                    LF.StudyDesign.disableCustomEnvironments = true;
                });

                it('returns url-like strings unchanged if they exist in environments', () => {
                    let urlLikeString = 'https://segment1.segment2.gamma.not';
                    expect(this.view.getStudyUrl(urlLikeString)).toBe(urlLikeString);
                });

                it('returns null if url-like strings do not match any existing environment', () => {
                    expect(this.view.getStudyUrl('https://there.is.no.such.url.in.evironments')).toBe(null);
                });

                it('constructs url from template if hostname exists in environments', () => {
                    expect(this.view.getStudyUrl('segment1')).toBe('https://segment1.segment2.gamma.not');
                });

                it('returns null if hostname does not match any existing environment', () => {
                    expect(this.view.getStudyUrl('no-such-host-name')).toBe(null);
                });
            });

            describe('when custom environments are enabled', () => {
                beforeEach(() => {
                    LF.StudyDesign.disableCustomEnvironments = false;
                });

                it('returns url-like strings unchanged even if they do not exist in environments', () => {
                    let existingUrl = 'https://segment1.segment2.gamma.not',
                        nonExistingUrl = 'https://there.is.no.such.url.in.evironments';

                    expect(this.view.getStudyUrl(existingUrl)).toBe(existingUrl);
                    expect(this.view.getStudyUrl(nonExistingUrl)).toBe(nonExistingUrl);
                });

                it('constructs url from template even if hostname does not exist in environments', () => {
                    expect(this.view.getStudyUrl('screwball')).toBe('https://screwball.nowhere.not/screwball');
                });
            });

            it('finds host name in environments', () => {
                expect(this.view.getStudyUrl('segment1')).toBe('https://segment1.segment2.gamma.not');
                expect(this.view.getStudyUrl('segment1.segment2')).toBe('https://segment1.segment2.gamma.not');
                expect(this.view.getStudyUrl('segme')).not.toBe('https://segment1.segment2.gamma.not');
            });

            it('finds environment id in environments', () => {
                expect(this.view.getStudyUrl('dev')).toBe('http://localhost:3000');
            });
        });
    }
}

describe('CodeEntryView', () => {
    let suite = new CodeEntryViewSuite();

    suite.executeAll({
        id: 'code-entry-page',
        template: '#codeEntry-template',
        button: '#next',
        input: '#txtCode',
        dynamicStrings: { key: '12345' },
        exclude: [
            // Should be tested individually
            'testProperty',
            'testAttribute',

            // PageView has no class name.
            'testClass',
            'testHideKeyboardOnEnter'
        ]
    });

    // CodeEntryView
    suite.testRender();
    suite.testSubmit();
    suite.testOnInput();
    suite.testProcessQRCode();
    suite.testCheckSubjectIsActive();
    suite.testGetStudyUrl();
});
