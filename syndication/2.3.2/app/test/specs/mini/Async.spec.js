import Logger from 'core/Logger';
let logger = new Logger('AsyncTestingStyleSpec', {consoleLevel:'TRACE'});

describe('Async Testing Style', () => {

    // complains about this
    beforeAll((done) => done());

    it('complains about using it/done, but does not fail the test', (done) => {
        done();
    });

    it('fails non-Async.it tests that return Promise', () => {
        return Q();
    });

    Async.beforeEach(() => {
        logger.trace('beforeEach');
        return Q('whatever');
    });

    Async.afterEach(() => {
        logger.trace('afterEach');
        return Q('whatever');
    });

    Async.beforeAll(() => {
        logger.trace('beforeAll');
        return Q();
    });

    Async.afterAll(() => {
        logger.trace('afterAll');
        return Q();
    });

    // this spec passes
    Async.it('passes your test when all is well', () => {
        return Q()
        .delay(100)
        .tap(() => {
            expect(0).toEqual(0);
        });
    });

    // This spec is invalid, it won't be executed.
    // The ONLY allowed pattern is for Async.it() to call done()
    // when your Promise settles. You do not have access to the
    // done CB.
    // In fact, it prevents Jasmine from starting up at all.
    //Async.it('forbids accessing the done callback', (done) => {
    //    return Q().done(done);
    //});

    // this spec fails
    Async.it('fails your test when it expects the impossible', () => {
        return Q()
        .delay(100)
        .tap(() => {
            expect(0).toEqual(1, 'I guess not.');
        });
    });

    // this spec fails
    Async.it('handles promises rejected with a string', () => {
        return Q.reject('a message');
    });

    // this spec fails
    Async.it('handles promises rejected with an Error', () => {
        return Q.reject(new Error('Some Thrown Err'));
    });

    // this spec fails
    Async.it('fails your test if you don\'t return a Promise', () => {
        Q().then(() => true);
    });

    // this spec fails
    Async.it('handles thrown errors', () => {
        throw new Error('this was thrown');
    });

    // this spec is skipped
    Async.xit('skips specs declared with Async.xit', () => {
        throw new Error('why was this run?');
    });

    /// // this spec is focused
    // Async.fit('runs only specs with Async.fit', () => {
    //     return Q();
    // });
});
