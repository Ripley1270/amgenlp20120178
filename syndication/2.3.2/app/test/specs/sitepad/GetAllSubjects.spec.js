import StudyDesign from 'core/classes/StudyDesign';
import Logger from 'core/Logger';
import * as lStorage from 'core/lStorage';

import Subjects from 'core/collections/Subjects';

import Data from 'core/Data';
import WebService from 'core/classes/WebService';
import defaultAction  from 'core/actions/defaultAction';
import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as helpers from 'test/helpers/SpecHelpers';
import ELF from 'core/ELF';
import 'jasmine-ajax';
import 'sitepad/resources/Rules';

//jscs:disable requireArrowFunctions
describe('Synchronizing All Subjects Data:', () => {
    LF.webService = new WebService();
    let studyDesign;

    Async.beforeEach(() => {
        helpers.installDatabase();
        resetStudyDesign();
        spyOn(ELF, 'trigger').and.callThrough();
        studyDesign = window.LF.StudyDesign;
        lStorage.setItem('serviceBase', studyDesign.environments.at(0).get('url'));


        return helpers.initializeContent();
    });

    Async.afterEach(() => {
        helpers.uninstallAjax()
        resetStudyDesign();
        localStorage.clear();
        return helpers.uninstallDatabase();
    });

    Async.it('should fetch all subjects from server for this site', function () {
        let subjectSize,
            result,
            mySubjects = new Subjects();

        ELF.rules.add({
            id: 'SyncSubjectsData',
            trigger: ['HISTORICALDATASYNC:Received/Subjects'],
            evaluate ({ res: subjects, collection }, done) {
                Q.all(_.map(subjects, subjectData => {
                    let subjectModel = collection.findWhere({
                        subject_id: subjectData.subject_id
                    }) || new collection.model();

                    return subjectModel.save(subjectData)
                    .then(() => collection.add(subjectModel));
                }))
                .catch(err => {
                    done(false);
                })
                .done(() => {
                    done(true);
                });
            },
            resolve: [{ action: 'defaultAction' }]
        });

        return (new Subjects()).clear()
            .then(() => {
                return LF.webService.getAllSubjects()
            })
            .tap(subjects => {
                expect(subjects.length).toBeGreaterThan(0);
            })
            .then(subjects=> {
                result = subjects;
                return Subjects.fetchCollection();
            })
            .then(collection=> {
                return ELF.trigger(`HISTORICALDATASYNC:Received/Subjects`, {res:result, collection});
            })
            .then(()=> {
                return Subjects.fetchCollection();
            })
            .tap((collection) => {
                // assumes the fetched subjects don't overlap the subjects already there.
                expect(collection.length).toEqual(result.length);
            });
    });

});
