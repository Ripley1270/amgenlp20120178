import COOL from 'core/COOL';
import * as trainerUnlockCodeView from 'trainer/sitepad/views/TrainerUnlockCodeView';

TRACE_MATRIX('US6732').
describe('TrainerUnlockCodeView', () => {
    let originalUnlockCodeView = COOL.getClass('UnlockCodeView');

    afterEach(() => {
        COOL.add('UnlockCodeView', originalUnlockCodeView);
    });

    it('Should override the view in COOL library.', () => {
        let UnlockCodeView = COOL.getClass('UnlockCodeView');
        expect(UnlockCodeView.name).toEqual('UnlockCodeView');

        trainerUnlockCodeView.extendUnlockCodeView();

        UnlockCodeView = COOL.getClass('UnlockCodeView');
        expect(UnlockCodeView.name).toEqual('TrainerUnlockCodeView');
    });

    it('Shoud validate a 9 digit number.', () => {
        trainerUnlockCodeView.extendUnlockCodeView();

        let UnlockCodeView = COOL.getClass('UnlockCodeView'),
            view = new UnlockCodeView();

        expect(view.validateUnlockCode('123456789')).toEqual(true);
        expect(view.validateUnlockCode('187236290')).toEqual(true);
        expect(view.validateUnlockCode('111111111')).toEqual(true);
        expect(view.validateUnlockCode('000000000')).toEqual(true);
    });

    it('Shoud not validate if not a 9 digit number.', () => {
        trainerUnlockCodeView.extendUnlockCodeView();

        let UnlockCodeView = COOL.getClass('UnlockCodeView'),
            view = new UnlockCodeView();

        expect(view.validateUnlockCode('')).toEqual(false);
        expect(view.validateUnlockCode('12345678')).toEqual(false);
        expect(view.validateUnlockCode('1234567890')).toEqual(false);
        expect(view.validateUnlockCode('12345678$')).toEqual(false);
        expect(view.validateUnlockCode('12345678A')).toEqual(false);
        expect(view.validateUnlockCode('ABCDEFGH')).toEqual(false);
        expect(view.validateUnlockCode('ABCDEFGHI')).toEqual(false);
        expect(view.validateUnlockCode('ABCDEFGHIJ')).toEqual(false);
    });
});
