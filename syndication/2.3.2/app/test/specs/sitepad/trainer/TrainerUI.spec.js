import Language from 'core/models/Language';
import Languages from 'core/collections/Languages';
import TrainerUI from 'trainer/sitepad/classes/TrainerUI';

TRACE_MATRIX('US6734').
describe('Trainer UI', () => {
    let lfStrings;

    beforeEach(() => {
        lfStrings = LF.strings;

        LF.strings = new Languages();

        LF.strings.add([{
            namespace: 'CORE',
            language: 'en',
            locale: 'GB',
            localized: 'English (UK)',
            direction: 'ltr',
            resources: {
                APPLICATION_HEADER: 'ERT (en-GB)',
                APP_NAME: 'ERT (en-GB)'
            }
        }, {
            namespace: 'CORE',
            language: 'hr',
            locale: 'HR',
            localized: 'Hrvatski (Hrvatska)',
            direction: 'ltr',
            resources: {
                APPLICATION_HEADER: 'ERT (hr-HR)',
                APP_NAME: 'ERT (hr-HR)'
            }
        }, {
            namespace: 'CORE',
            language: 'ja',
            locale: 'JP',
            localized: '??? (??)',
            direction: 'ltr',
            resources: {
                APPLICATION_HEADER: 'ERT (ja-JP)',
                APP_NAME: 'ERT (ja-JP)'
            }
        }]);
    });

    afterEach(() => {
        LF.strings = lfStrings;
    });

    it('Should register css class to the document style sheets.', () => {
        let styleSheet = document.styleSheets[(document.styleSheets.length - 1)];

        spyOn(styleSheet, 'insertRule').and.callThrough();

        TrainerUI.registerCSS();

        expect(styleSheet.insertRule.calls.count()).toEqual(2);
    });

    it('Should update core strings.', () => {
        let originalStrings = JSON.parse(JSON.stringify(LF.strings.match({
            namespace: 'CORE',
            resources: {}
        })));

        TrainerUI.overrideCoreStrings();

        let appStrings = LF.strings.match({
            namespace: 'CORE',
            resources: {}
        });

        appStrings.forEach((item, i) => {
            let resources = item.get('resources'),
                originalResources = originalStrings[i].resources;

            expect(resources.APPLICATION_HEADER).toEqual(originalResources.APPLICATION_HEADER + ' {{ TRAINER_HEADER }}');
            expect(resources.APP_NAME).toEqual(originalResources.APP_NAME + ' {{ TRAINER_HEADER }}');
        });
    });
});