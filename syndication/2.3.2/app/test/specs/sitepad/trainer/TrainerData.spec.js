import TrainerData from 'trainer/sitepad/classes/TrainerData';

TRACE_MATRIX('US6733').
describe('TrainerUnlockCodeView', () => {
    let testReturnNonEmptyData = (webserviceFunction) => {
        let trainerData = TrainerData.getData(webserviceFunction);

        expect(trainerData).toBeArray();
        expect(trainerData.length).not.toEqual(0);
    };

    let testReturnEmptyData = (webserviceFunction) => {
        let trainerData = TrainerData.getData(webserviceFunction);

        expect(trainerData).toBeArray();
        expect(trainerData.length).toEqual(0);
    };

    it('Should return non empty data.', () => {
        let webserviceNames = [
            'getSites',
            'registerDevice',
            'addUser',
            'updateUserCredentials',
            'sendLogs',
            'sendSubjectAssignment',
            'setSubjectData',
            'sendDiary',
            'updateSubjectData',
            'sendEditPatient',
            'getSetupCode',
            'getDeviceID',
            'updateUser'
        ];

        webserviceNames.forEach((name) => {
            testReturnNonEmptyData(name);
        });
    });

    it('Should return empty data.', () => {
        let webserviceNames = [
            'getAllSubjects',
            'syncUsers'
        ];

        webserviceNames.forEach((name) => {
            testReturnEmptyData(name);
        });
    });

    it('Should return an Object with lastRefreshTime for "syncUserVisitsAndReports".', () => {
        let trainerData = TrainerData.getData('syncUserVisitsAndReports');

        expect(typeof trainerData).toEqual('object');
        expect(trainerData.lastRefreshTime).toBeTruthy();
    });

    it('Should return an empty array for non existing webservice function.', () => {
        testReturnEmptyData('NON-EXISTING-WEBSERVICE-NAME');
    });
});
