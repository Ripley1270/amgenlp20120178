import setLanguageToSite from 'sitepad/actions/setLanguageToSite';
import CurrentContext from 'core/CurrentContext';
import Context from 'core/models/Context';
import * as lStorage from 'core/lStorage';

import { resetStudyDesign } from 'test/helpers/StudyDesign';
import { fakeGetStrings } from 'test/helpers/SpecHelpers';

describe('setLanguageToSite', () => {
    beforeEach(() => {
        resetStudyDesign();
        fakeGetStrings();

        spyOn(Context.prototype, 'setContextLanguage').and.resolve();

        CurrentContext.init();
    });

    afterEach(() => localStorage.clear());

    it('should default to en-US.', () => {
        delete LF.StudyDesign.defaultLanguage;
        delete LF.StudyDesign.defaultLocale;

        setLanguageToSite();

        expect(Context.prototype.setContextLanguage).toHaveBeenCalledWith('en-US');
    });

    it('should use the study language.', () => {
        LF.StudyDesign.defaultLanguage = 'en';
        LF.StudyDesign.defaultLocale = 'GB';

        setLanguageToSite();

        expect(Context.prototype.setContextLanguage).toHaveBeenCalledWith('en-GB');
    });

    it('should use the site language.', () => {
        lStorage.setItem('language', 'de');
        lStorage.setItem('locale', 'DE');

        setLanguageToSite();

        expect(Context.prototype.setContextLanguage).toHaveBeenCalledWith('de-DE');
    });
});
