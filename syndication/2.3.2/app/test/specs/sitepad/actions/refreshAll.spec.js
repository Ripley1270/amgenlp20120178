import * as syncHistoricalData from 'core/actions/syncHistoricalData';
import * as lStorage from 'core/lStorage';
import * as syncUsers from 'core/actions/syncUsers';
import refreshAllData from 'sitepad/actions/refreshAllData';
import * as getAllSubjects from 'sitepad/actions/getAllSubjects';
import * as syncUserVisitsAndReports from 'sitepad/actions/syncUserVisitsAndReports';
import * as lastRefreshTime from 'sitepad/actions/lastRefreshTime';
import Logger from 'core/Logger';

TRACE_MATRIX('US6771').
describe('refreshAll', () => {
    beforeEach(() => {
        localStorage.clear();
    });

    afterAll(() => {
        localStorage.clear();
    });

    describe('Sub Actions', () => {
        beforeEach(() => {
            spyOn(syncHistoricalData, 'syncHistoricalData').and.resolve();
        });

        Async.it('should call getAllSubjects with correct parameters.', () => {
            spyOn(getAllSubjects, 'getAllSubjects').and.resolve();

            return refreshAllData()
            .then(() => {
                expect(getAllSubjects.getAllSubjects).toHaveBeenCalledWith(true);
            });
        });

        Async.it('should call syncUserVisitsAndReports with correct parameters.', () => {
            spyOn(syncUserVisitsAndReports, 'syncUserVisitsAndReports').and.resolve();

            return refreshAllData()
            .then(() => {
                expect(syncUserVisitsAndReports.syncUserVisitsAndReports).toHaveBeenCalledWith(true);
            });
        });

        Async.it('should call syncUsers with correct parameters.', () => {
            spyOn(syncUsers, 'syncUsers').and.resolve();

            return refreshAllData()
            .then(() => {
                expect(syncUsers.syncUsers).toHaveBeenCalledWith(true);
            });
        });

        Async.it('should call updateLastRefreshTime.', () => {
            spyOn(lastRefreshTime, 'updateLastRefreshTime').and.resolve();

            return refreshAllData()
            .then(() => {
                expect(lastRefreshTime.updateLastRefreshTime).toHaveBeenCalled();
            });
        });

        Async.it('should update the LastRefreshTime on localStorage.', () => {
            let now = new Date().ISOLocalTZStamp();

            spyOn(lastRefreshTime, 'updateLastRefreshTime').and.callThrough();
            spyOn(syncUserVisitsAndReports, 'syncUserVisitsAndReports').and.callFake(() => {
                return lastRefreshTime.setTime(now);
            });

            return refreshAllData()
            .then(() => {
                expect(lStorage.getItem('lastRefreshTime')).toEqual(now);
            });
        });
    });

    describe('Error cases', () => {
        let now;

        Async.beforeEach(() => {
            spyOn(Logger.prototype, 'error').and.stub();
            spyOn(syncHistoricalData, 'syncHistoricalData').and.resolve();
            spyOn(lastRefreshTime, 'updateLastRefreshTime').and.callThrough();

            now = new Date().ISOLocalTZStamp();
            return lastRefreshTime.setTime(now);
        });

        Async.it('should return a resolved promise when a sub-action fails.', () => {
            spyOn(getAllSubjects, 'getAllSubjects').and.reject();

            let result = refreshAllData();

            return result
            .finally(() => {
                expect(Logger.prototype.error).toHaveBeenCalled();
                expect(result.isFulfilled()).toBe(true);
            });
        });

        let testErrorCase = () => {
            return refreshAllData()
            .finally(() => {
                expect(Logger.prototype.error).toHaveBeenCalled();
                expect(lastRefreshTime.updateLastRefreshTime).not.toHaveBeenCalled();
                expect(lStorage.getItem('lastRefreshTime')).not.toEqual(now);
            });
        };

        Async.it('should log an error and not update LastRefreshTime when getAllSubjects fails.', () => {
            spyOn(getAllSubjects, 'getAllSubjects').and.reject();
            return testErrorCase();
        });

        Async.it('should log an error and not update LastRefreshTime when syncUserVisitsAndReports fails.', () => {
            spyOn(syncUserVisitsAndReports, 'syncUserVisitsAndReports').and.reject();
            return testErrorCase();
        });

        Async.it('should log an error and not update LastRefreshTime when syncUsers fails.', () => {
            spyOn(syncUsers, 'syncUsers').and.reject();
            return testErrorCase();
        });
    });
});
