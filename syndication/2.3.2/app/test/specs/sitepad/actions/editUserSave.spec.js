import User from 'core/models/User';
import Users from 'core/collections/Users';
import Transmission from 'core/models/Transmission';
import Transmissions from 'core/collections/Transmissions';
import Answers from 'core/collections/Answers';
import editUserSave from 'sitepad/actions/editUserSave';

const fields = [{
    SW_Alias: 'Edit_User.0.EDIT_USER_1',
    question_id: 'EDIT_USER_USERNAME',
    questionnaire_id: 'Edit_User',
    // The property on this response is determine by the 'field' property
    // on the question's widget configuration. e.g. { username: 'jsmith' }
    // Indicate that the response is JSON.
    isJSON: true,
    // Determine the field name of the JSON response. e.g. { username: ... }
    field: 'username',
    // Use the user model scoped to the UserStatusQuestionnaireView (this.user).
    model: 'user',
    // Use the user model's username property to populate the response.
    // e.g. this.user.get('username');
    property: 'username'
}, {
    SW_Alias: 'Edit_User.0.EDIT_USER_2',
    question_id: 'EDIT_USER_ROLE',
    questionnaire_id: 'Edit_User',
    // The property on this response is determine by the 'field' property
    // on the question's widget configuration. e.g. { role: 'site' }
    // Indicate that the response is JSON.
    isJSON: true,
    // Determine the field name of the JSON response. e.g. { role: ... }
    field: 'role',
    // Use the user model scoped to the UserStatusQuestionnaireView (this.user).
    model: 'user',
    // Use the user model's role property to populate the response.
    // e.g. this.user.get('role');
    property: 'role'
}, {
    SW_Alias: 'Edit_User.0.EDIT_USER_3',
    question_id: 'EDIT_USER_LANG',
    questionnaire_id: 'Edit_User',
    // The property on this response is determine by the 'field' property
    // on the question's widget configuration. e.g. { language: 'en-US' }
    // Indicate that the response is JSON.
    isJSON: true,
    // Determine the field name of the JSON response. e.g. { language: ... }
    field: 'language',
    // Use the user model scoped to the UserStatusQuestionnaireView (this.user).
    model: 'user',
    // Use the user model's language property to populate the response.
    // e.g. this.user.get('language');
    property: 'language'
}];

describe('editUserSave', () => {
    let context;
    let userData;

    beforeEach(() => {
        userData = {
            id: 1,
            username: 'System Admin',
            language: 'en-US',
            role:'admin'
        };

        context = {
            user: new User(userData),
            answers: new Answers()
        };

        _.forEach(fields, (field) => {
            let { question_id, questionnaire_id, SW_Alias } = field;
            let response = { };

            response[field.field] = field.field;

            context.answers.add({
                questionnaire_id,
                question_id,
                SW_Alias,
                response: JSON.stringify(response),
                instance_ordinal: 0
            });
        });

        spyOn(User.prototype, 'save').and.resolve();

        // Using longhand function to retain proper scope of this.
        spyOn(User.prototype, 'fetch').and.callFake(function () {
            this.set(userData);

            // Only add the spy after fetch is complete, so the call above isn't captured.
            spyOn(User.prototype, 'set').and.callThrough();
            return Q();
        });

        spyOn(Users, 'fetchCollection').and.callFake(() => {
            return Q(new Users());
        });

        spyOn(Transmissions.prototype, 'fetch').and.callFake(function () {
            this.add([{
                id: 1,
                method: 'transmitUserEdit',
                status: 'failed',
                params: JSON.stringify({ id: 1 }),
                created: new Date().getTime()
            }]);

            return Q();
        });
        spyOn(Transmission.prototype, 'save').and.resolve();
    });

    Async.it('should be true.', () => {
        let request = editUserSave.call(context, fields);

        expect(request).toBePromise();

        return request.then(() => {
            expect(User.prototype.set).toHaveBeenCalledWith('username', 'username');
            expect(User.prototype.set).toHaveBeenCalledWith('role', 'role');
            expect(User.prototype.set).toHaveBeenCalledWith('language', 'language');

            expect(Transmission.prototype.save).toHaveBeenCalled();
        });
    });

    Async.it('should fail to fetch the user.', () => {
        User.prototype.fetch.and.reject('DatabaseError');

        let request = editUserSave.call(context, fields);

        return request.then(() => fail('function editUserSave should have been rejected.'))
        .catch((e) => {
            expect(e).toBe('DatabaseError');
        });
    });

    Async.it('should fail to save the user.', () => {
        User.prototype.save.and.reject('UserSaveError');

        let request = editUserSave.call(context, fields);

        return request.then(() => fail('function editUserSave should have been rejected.'))
        .catch((e) => {
            expect(e).toBe('UserSaveError');
        });
    });

    Async.it('should fail to fetch the transmissions.', () => {
        Transmissions.prototype.fetch.and.reject('DatabaseError');

        let request = editUserSave.call(context, fields);

        return request.then(() => fail('function editUserSave should have been rejected.'))
        .catch((e) => {
            expect(e).toBe('DatabaseError');
        });
    });

    Async.it('use an existing edit user transmission.', () => {
        spyOn(Transmission.prototype, 'set').and.callThrough();

        let request = editUserSave.call(context, fields);

        return request.then(() => {
            expect(Transmission.prototype.set).toHaveBeenCalledWith('status', null);
            expect(Transmission.prototype.save).toHaveBeenCalled();
        });
    });

    Async.it('use an existing new user transmission.', () => {
        spyOn(Transmission.prototype, 'set').and.callThrough();

        let request = editUserSave.call(context, fields);

        return request.then(() => {
            expect(Transmission.prototype.set).toHaveBeenCalledWith('status', null);
            expect(Transmission.prototype.save).toHaveBeenCalled();
        });
    });

    Async.it('should fail to save the transmission.', () => {
        Transmission.prototype.save.and.reject('TransmissionSaveError');

        let request = editUserSave.call(context, fields);

        return request.then(() => fail('function editUserSave should have been rejected.'))
        .catch((e) => {
            expect(e).toBe('TransmissionSaveError');
        });
    });

    Async.it('should fail to fetch the users colleciton.', () => {
        Users.fetchCollection.and.reject('FetchCollectionFail');

        let request = editUserSave.call(context, fields);

        return request.then(() => fail('function editUserSave should have been rejected.'))
        .catch((e) => {
            expect(e).toBe('FetchCollectionFail');
        });
    });

    Async.it('shouldn\'t autocorrect duplicate username.', () => {
        userData.username = 'System Admin*';

        let request = editUserSave.call(context, fields);

        return request.then(() => {
            expect(Users.fetchCollection).not.toHaveBeenCalled();
        });
    });

    Async.it('should autocorrect duplicate username.', () => {
        spyOn(Transmission.prototype, 'set').and.callThrough();
        Users.fetchCollection.and.callFake(() => {
            let col = new Users([
                { id: 2, username: 'System Admin*', role: 'admin' }
            ]);

            return Q(col);
        });

        let request = editUserSave.call(context, fields);

        return request.then(() => {
            expect(Users.fetchCollection).toHaveBeenCalled();
            expect(User.prototype.set).toHaveBeenCalledWith('username', 'System Admin');
            
            expect(Transmission.prototype.save).toHaveBeenCalled()
            expect(Transmission.prototype.set).toHaveBeenCalledWith({
                method: 'transmitUserEdit',
                params: JSON.stringify({ id: 2}),
                created: jasmine.any(Number)
            }, jasmine.any(Object));
        });
    });
});
