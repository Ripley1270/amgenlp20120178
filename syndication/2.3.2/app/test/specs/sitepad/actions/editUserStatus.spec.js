import editUserStatus from 'sitepad/actions/editUserStatus';
import User from 'core/models/User';
import Transmission from 'core/models/Transmission';
import Transmissions from 'core/collections/Transmissions';

describe('editUserStatus', () => {
	let context;
	let transmissions;

	beforeEach(() => {
		spyOn(User.prototype, 'set').and.callThrough();
		spyOn(User.prototype, 'save').and.resolve();
		spyOn(Transmission.prototype, 'save').and.resolve();
		spyOn(Transmission.prototype, 'set').and.callThrough();
		spyOn(Transmission.prototype, 'get').and.callThrough();
		spyOn(Transmissions, 'fetchCollection').and.callFake(() => {
			return Q(new Transmissions(transmissions));
		})

		transmissions = [];
		context = {
			user: new User({ 
				id: 1, 
				username: 'Site Admin', 
				role: 'admin',
				active: 1 
			})
		};
	});

	Async.it('should fail to save the user.', () => {
		User.prototype.save.and.reject('DatabaseError');

		let request = editUserStatus.call(context, { active: 0 });

		expect(request).toBePromise();

		return request.then(() => fail('editUserStatus should have been rejected.'))
		.catch(err => {
			expect(err).toBe('DatabaseError');
			expect(User.prototype.set).toHaveBeenCalledWith('active', 0);
		});
	});

	Async.it('should fail to fetch existing transmissions.', () => {
		Transmissions.fetchCollection.and.reject('DatabaseError');

		return editUserStatus.call(context, { active: 0 })
		.then(() => fail('editUserStatus should have been rejected.'))
		.catch(err => {
			expect(err).toBe('DatabaseError');
		});
	});

	Async.it('should fail to save a new transmission.', () => {
		Transmission.prototype.save.and.reject('DatabaseError');

		return editUserStatus.call(context, { active: 0 })
		.then(() => fail('editUserStatus should have been rejected.'))
		.catch(err => {
			expect(err).toBe('DatabaseError');
			expect(Transmission.prototype.save).toHaveBeenCalled();
			expect(Transmission.prototype.set).toHaveBeenCalledWith({
				method: 'transmitUserEdit',
				params: JSON.stringify({ id: 1 }),
				created: jasmine.any(Number)
			}, {});
		});
	});

	Async.it('should save a new transmitUserEdit transmission.', () => {
		return editUserStatus.call(context, { active: 0 })
		.then(() => {
			expect(Transmission.prototype.set).toHaveBeenCalledWith({
				method: 'transmitUserEdit',
				params: JSON.stringify({ id: 1 }),
				created: jasmine.any(Number)
			}, {});
			expect(Transmission.prototype.save).toHaveBeenCalled();
		});
	});

	Async.it('should find and update an existing, failed transmitUserEdit transmission.', () => {
		transmissions = [{ 
			id: 1, 
			method: 'transmitUserEdit',
			params: JSON.stringify({ id: 1 }),
			status: 'failed'
		}];

		return editUserStatus.call(context, { active: 0 })
		.then(() => {
			expect(Transmission.prototype.set).toHaveBeenCalledWith('status', null);
			expect(Transmission.prototype.save).toHaveBeenCalled();
		});
	});

	Async.it('should find an existing transmitUserEdit transmission and do nothing.', () => {
		transmissions = [{ 
			id: 1, 
			method: 'transmitUserEdit',
			params: JSON.stringify({ id: 1 })
		}];

		return editUserStatus.call(context, { active: 0 })
		.then(() => {
			expect(Transmission.prototype.get).toHaveBeenCalledWith('status');
			expect(Transmission.prototype.save).not.toHaveBeenCalled();
		});
	});

	Async.it('should find an existing transmitUserData transmission and do nothing.', () => {
		transmissions = [{ 
			id: 1, 
			method: 'transmitUserData',
			params: JSON.stringify({ userId: 1 })
		}];

		return editUserStatus.call(context, { active: 0 })
		.then(() => {
			expect(Transmission.prototype.get).toHaveBeenCalledWith('status');
			expect(Transmission.prototype.save).not.toHaveBeenCalled();
		});
	});
});