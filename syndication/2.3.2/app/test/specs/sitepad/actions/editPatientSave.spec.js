import ELF from 'core/ELF';
import EditPatientQuestionnaireView from 'sitepad/views/EditPatientQuestionnaireView';
import Questionnaire from 'core/models/Questionnaire';
import Transmission from 'core/models/Transmission';
import Subject from 'core/models/Subject';
import Subjects from 'core/collections/Subjects';
import Answers from 'core/collections/Answers';
import Data from 'core/Data';
import Site from 'core/models/Site';
import Sites from 'core/collections/Sites';

import User from 'core/models/User';
import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as action from 'sitepad/actions/editPatientSave';
import * as DateTimeUtil from 'core/DateTimeUtil';
import * as lStorage from 'core/lStorage';
import { Dialog, MessageRepo } from 'core/Notify';
import 'sitepad/resources/Rules';
import setupSitepadMessages from 'sitepad/resources/Messages';

import * as specHelpers from 'test/helpers/SpecHelpers';

TRACE_MATRIX('US5886').
describe('editPatientSave', () => {
    let view,
        removeTemplate,
        EditPatientRenderRule = {
            id: 'EditPatientQuestionnaire',
            trigger: 'QUESTIONNAIRE:Rendered/Edit_Patient',
            resolve: [{
                // This action populates answer records based on a configured model.
                action: 'populateFieldsByModel',
                data: [{
                    SW_Alias: 'PT.0.Patientid',
                    question_id: 'EDIT_PATIENT_ID',
                    questionnaire_id: 'Edit_Patient',
                    // Use the subject model scoped to the EditPatientQuestionnaireView (this.subject).
                    model: 'subject',
                    // Use the subject model's subject_id property to populate the response.
                    // e.g. this.subject.get('subject_id');
                    property: 'subject_id'
                }, {
                    SW_Alias: 'PT.0.Initials',
                    question_id: 'EDIT_PATIENT_INITIALS',
                    questionnaire_id: 'Edit_Patient',
                    // Use the subject model scoped to the EditPatientQuestionnaireView (this.subject).
                    model: 'subject',
                    // Use the subject model's initials property to populate the response.
                    // e.g. this.subject.get('initials');
                    property: 'initials'
                }, {
                    SW_Alias: 'Assignment.0.Language',
                    question_id: 'EDIT_PATIENT_LANGUAGE',
                    questionnaire_id: 'New_Patient',
                    // Use the user model scoped to the EditPatientQuestionnaireView (this.user).
                    model: 'user',
                    // Use the user model's language property to populate the response.
                    // e.g. this.user.get('language');
                    property: 'language'
                }]
            }]
        },
        deviceId = '9876543210ABCDEF',
        siteData = { siteCode: '0001', site_id: 'DOM.287206.246' },
        subjectData = { id: 1, subject_id: '0001-0172', krpt: 'SA.1234567890', initials: 'AAA', user: 1},
        userData = { id: 1, username: 'batman', language: 'en_US' },
        answers = new Answers([{
            SW_Alias: 'PT.0.Patientid',
            question_id: 'EDIT_PATIENT_ID',
            questionnaire_id: 'Edit_Patient',
            response: subjectData.subject_id,
            subject_id: subjectData.subject_id
        }, {
            SW_Alias: 'PT.0.Initials',
            question_id: 'EDIT_PATIENT_INITIALS',
            questionnaire_id: 'Edit_Patient',
            response: subjectData.initials,
            subject_id: subjectData.subject_id
        }, {
            SW_Alias: 'Assignment.0.Language',
            question_id: 'EDIT_PATIENT_LANGUAGE',
            questionnaire_id: 'Edit_Patient',
            response: userData.language,
            subject_id: subjectData.subject_id
        }, {
            SW_Alias: 'PT.Initials.Comment',
            question_id: 'EDIT_PATIENT_REASON',
            questionnaire_id: 'Edit_Patient',
            response: 'NoReason',
            subject_id: subjectData.subject_id
        }]),
        changedAnswers = new Answers([{
            SW_Alias: 'PT.0.Patientid',
            question_id: 'EDIT_PATIENT_ID',
            questionnaire_id: 'Edit_Patient',
            response: '0001-0200',
            subject_id: subjectData.subject_id
        }, {
            SW_Alias: 'PT.0.Initials',
            question_id: 'EDIT_PATIENT_INITIALS',
            questionnaire_id: 'Edit_Patient',
            response: 'BBB',
            subject_id: subjectData.subject_id
        }, {
            SW_Alias: 'Assignment.0.Language',
            question_id: 'EDIT_PATIENT_LANGUAGE',
            questionnaire_id: 'Edit_Patient',
            response: 'es_ES',
            subject_id: subjectData.subject_id
        }, {
            SW_Alias: 'PT.Initials.Comment',
            question_id: 'EDIT_PATIENT_REASON',
            questionnaire_id: 'Edit_Patient',
            response: 'ReasonIsRelative',
            subject_id: subjectData.subject_id
        }]);

    Async.beforeAll(() => {
        resetStudyDesign();
        setupSitepadMessages();


        LF.StudyDesign.questionnaires.add(new Questionnaire({
            id: 'Edit_Patient',
            SU: 'Edit_Patient',
            displayName: 'DISPLAY_NAME',
            className: 'EDIT_PATIENT',
            affidavit: 'DEFAULT',
            previousScreen: false,
            product: ['sitepad'],
            screens: [{
                id: 'EDIT_PATIENT_S_1',
                className: 'EDIT_PATIENT_S_1',
                questions: [{
                    id: 'EDIT_PATIENT_ID',
                    mandatory: true,
                    IG: 'PT',
                    IT: 'Patientid',
                    skipIT: '',
                    title: 'QUESTION_1_MAIN_TITLE',
                    text: ['QUESTION_1'],
                    className: 'EDIT_PATIENT_ID',
                    widget: {
                        id: 'EDIT_PATIENT_W_1',
                        type: 'PatientIDTextBox',
                        templates: {},
                        answers: [],
                        // numeric from 1 to 4 characters
                        maxLength: 4,
                        allowedKeyRegex: /[\d]/,
                        validateRegex: /[\d]+/,
                        _isUnique: true,
                        _isInRange: true,
                        validationErrors: [{
                            property: 'completed',
                            errorType: 'popup',
                            errString: 'QUESTION_1_EMPTY',
                            header: 'QUESTION_1_EMPTY_HEADER',
                            ok: 'VALIDATION_OK'
                        }, {
                            property: 'isInRange',
                            errorType: 'popup',
                            errString: 'QUESTION_1_RANGE',
                            header: 'QUESTION_1_RANGE_HEADER',
                            ok: 'VALIDATION_OK'
                        }],
                        validation: {
                            validationFunc: 'checkEditPatientID',
                            params: {
                                errorStrings: {
                                    errString: 'QUESTION_1_UNIQUE',
                                    header: 'QUESTION_1_UNIQUE_HEADER',
                                    ok: 'VALIDATION_OK'
                                }
                            }
                        }
                    }
                }, {
                    // Subject initials
                    id: 'EDIT_PATIENT_INITIALS',
                    mandatory: true,
                    IG: 'PT',
                    IT: 'Initials',
                    skipIT: '',
                    title: '',
                    text: ['QUESTION_2'],
                    className: 'EDIT_PATIENT_INITIALS',
                    widget: {
                        id: 'EDIT_PATIENT_W_2',
                        type: 'TextBox',
                        templates: {},
                        answers: [{ text: 'REASON_0', value: '0' }],

                        // Disallow digits, white space, and punctuation, except quotes (for Hebrew abbreviations)
                        // jscs:disable maximumLineLength
                        disallowedKeyRegex: /[\d\s\-=_!#%&*{},.\/:;?\(\)\[\]@\\$\^*+<>~`\u00a1\u00a7\u00b6\u00b7\u00bf\u037e\u0387\u055a-\u055f\u0589\u05c0\u05c3\u05c6\u05f3\u05f4\u0609\u060a\u060c\u060d\u061b\u061e\u061f\u066a-\u066d\u06d4\u0700-\u070d\u07f7-\u07f9\u0830-\u083e\u085e\u0964\u0965\u0970\u0af0\u0df4\u0e4f\u0e5a\u0e5b\u0f04-\u0f12\u0f14\u0f85\u0fd0-\u0fd4\u0fd9\u0fda\u104a-\u104f\u10fb\u1360-\u1368\u166d\u166e\u16eb-\u16ed\u1735\u1736\u17d4-\u17d6\u17d8-\u17da\u1800-\u1805\u1807-\u180a\u1944\u1945\u1a1e\u1a1f\u1aa0-\u1aa6\u1aa8-\u1aad\u1b5a-\u1b60\u1bfc-\u1bff\u1c3b-\u1c3f\u1c7e\u1c7f\u1cc0-\u1cc7\u1cd3\u2016\u2017\u2020-\u2027\u2030-\u2038\u203b-\u203e\u2041-\u2043\u2047-\u2051\u2053\u2055-\u205e\u2cf9-\u2cfc\u2cfe\u2cff\u2d70\u2e00\u2e01\u2e06-\u2e08\u2e0b\u2e0e-\u2e16\u2e18\u2e19\u2e1b\u2e1e\u2e1f\u2e2a-\u2e2e\u2e30-\u2e39\u3001-\u3003\u303d\u30fb\ua4fe\ua4ff\ua60d-\ua60f\ua673\ua67e\ua6f2-\ua6f7\ua874-\ua877\ua8ce\ua8cf\ua8f8-\ua8fa\ua92e\ua92f\ua95f\ua9c1-\ua9cd\ua9de\ua9df\uaa5c-\uaa5f\uaade\uaadf\uaaf0\uaaf1\uabeb\ufe10-\ufe16\ufe19\ufe30\ufe45\ufe46\ufe49-\ufe4c\ufe50-\ufe52\ufe54-\ufe57\ufe5f-\ufe61\ufe68\ufe6a\ufe6b\uff01-\uff03\uff05-\uff07\uff0a\uff0c\uff0e\uff0f\uff1a\uff1b\uff1f\uff20\uff3c\uff61\uff64\uff65]/,
                        // jscs:enable

                        maxLength: 6,
                        validateRegex: /.{2,}/,
                        validationErrors: [{
                            property: 'completed',
                            errorType: 'popup',
                            errString: 'QUESTION_2_EMPTY',
                            header: 'QUESTION_2_EMPTY_HEADER',
                            ok: 'VALIDATION_OK'
                        }]
                    }
                }]
            }, {
                id: 'EDIT_PATIENT_S_2',
                className: 'EDIT_PATIENT_S_2',
                questions: [
                    {
                        id: 'EDIT_PATIENT_LANGUAGE',
                        mandatory: true,
                        IG: 'Assignment',
                        IT: 'Language',
                        skipIT: '',
                        title: 'QUESTION_3_TITLE',
                        text: ['QUESTION_3'],
                        className: '',
                        widget: {
                            id: 'EDIT_PATIENT_W_3',
                            type: 'PatientLangListRadioButton',
                            templates: {},
                            answers: []
                        }
                    }
                ]
            }, {
                id: 'EDIT_PATIENT_S_3',
                className: 'EDIT_PATIENT_S_3',
                questions: [{
                    id: 'EDIT_PATIENT_REASON',
                    mandatory: true,
                    IG: 'Edit_Patient',
                    IT: 'EDIT_PATIENT_REASON',
                    title: 'QUESTION_4_TITLE',
                    text: 'QUESTION_4',
                    className: 'EDIT_PATIENT_REASON',
                    widget: {
                        id: 'EDIT_PATIENT_REASON',
                        type: 'EditReason',
                        className: 'EDIT_PATIENT_REASON',
                        answers: [
                            { text: 'REASON_0', value: 'Correcting error by clinician' },
                            { text: 'REASON_1', value: 'Reason #1' },
                            { text: 'REASON_2', value: 'Reason #2' },
                            { text: 'REASON_3', value: 'Reason #3' }
                        ]
                    }
                }]
            }],
            branches: []
        }));
        return Q();
    });

    Async.beforeEach(() => {
        removeTemplate = specHelpers.renderTemplateToDOM('sitepad/templates/questionnaire.ejs');

        Data.Questionnaire = view;

        LF.security = LF.security || {};
        LF.security.activeUser = new User(userData);
        LF.security.startQuestionnaireTimeOut = $.noop;

        LF.spinner = LF.spinner || {};
        LF.spinner.hide = () => Q();

        // Ensure the DynamicText namespace exists.
        LF.DynamicText = {};

        spyOn(Subject.prototype, 'fetch').and.callFake(function () {
            this.set(subjectData);

            return Q();
        });

        spyOn(User.prototype, 'fetch').and.callFake(function () {
            this.set(userData);

            return Q();
        });

        spyOn(Sites.prototype, 'fetch').and.callFake(function () {
            this.add(siteData);

            return Q();
        });

        view = new EditPatientQuestionnaireView({
            id: 'Edit_Patient',
            showCancel: false,
            showLoginInfo: false,
            subjectId: subjectData.id,
            user: LF.security.activeUser
        });

        view.displayScreen = () => Q();
        lStorage.setItem('deviceId', deviceId);
        return view.resolve();
    });

    Async.afterEach(() => {
        removeTemplate();
        Data = {};
        return Q();
    });

    Async.afterAll(() => {
        MessageRepo.clear();
    });

    Async.it('should create answer record on render.', () => {
        ELF.rules.add(EditPatientRenderRule);

        return ELF.trigger(`QUESTIONNAIRE:Rendered/${view.id}`, {
            questionnaire: view.id
        }, view).then(() => {
            expect(view.data.answers.models[0].get('response')).toEqual(subjectData.subject_id);
            expect(view.data.answers.models[1].get('response')).toEqual(subjectData.initials);
            expect(view.data.answers.models[2].get('response')).toEqual(userData.language);
        });
    });

    Async.it('should show the no change dialog.', () => {
        view.data.answers = answers;

        ELF.rules.add({
            id: 'EditPatientDiaryComplete',
            trigger: `QUESTIONNAIRE:Navigate/${view.id}/AFFIDAVIT`,
            resolve: [{ action: 'editPatientSave' }]
        });

        spyOn(MessageRepo, 'display').and.resolve(false);

        return ELF.trigger(`QUESTIONNAIRE:Navigate/${view.id}/AFFIDAVIT`, {
            questionnaire: view.id
        }, view)
        .then(() => {
            expect(MessageRepo.display).toHaveBeenCalledWith(MessageRepo.Dialog[`EDIT_PATIENT_${view.id}`]);
        });
    });

    Async.it('should save Transmission record in the database.', () => {
        view.data.answers = changedAnswers;
        spyOn(Transmission.prototype, 'save').and.resolve();

        return ELF.trigger(`QUESTIONNAIRE:Navigate/${view.id}/AFFIDAVIT`, {
            questionnaire: view.id
        }, view).then().then(() => {
            expect(Transmission.prototype.save).toHaveBeenCalledWith({
                method: 'transmitEditPatient',
                params: jasmine.any(String),
                created: jasmine.any(Number)
            });
        });
    });
});
