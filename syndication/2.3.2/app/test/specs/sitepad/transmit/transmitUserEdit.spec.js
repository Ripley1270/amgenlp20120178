import transmitUserEdit from 'sitepad/transmit/transmitUserEdit';
import Transmission from 'core/models/Transmission';
import User from 'core/models/User';
import UserSync from 'core/classes/UserSync';
import Logger from 'core/Logger';
import COOL from 'core/COOL';
import WebService from 'core/classes/WebService';
import ELF from 'core/ELF';

import { resetStudyDesign } from 'test/helpers/StudyDesign';

describe('transmitUserEdit', () => {
    let context,
        model,
        newService;

    beforeEach(() => {
        resetStudyDesign();

        context = jasmine.createSpyObj('context', ['remove', 'destroy']);
        context.destroy.and.callFake(() => Q());

        model = new Transmission({
            id: 1,
            params: JSON.stringify({ id: 2 }),
            created: new Date().getTime()
        });

        spyOn(User.prototype, 'fetch').and.callFake(function () {
            this.set({
                id: 2,
                username: 'System Administrator',
                role: 'site',
                language: 'en-US'
            });

            return Q();
        });

        spyOn(Logger.prototype, 'error').and.stub();
        spyOn(Logger.prototype, 'operational').and.stub();
        spyOn(Logger.prototype, 'info').and.stub();

        LF.StudyDesign.roles.reset([{
            id: 'site',
            lastDiaryRoleCode: 1,
            displayName: 'SITE_ADMINISTRATOR',
            permissions: ['ALL'],
            defaultAffidavit: 'DEFAULT',
            syncLevel: 'site',
            product: ['logpad', 'sitepad']
        }]);

        spyOn(WebService.prototype, 'updateUser');
    });

    Async.it('should fail to fetch the user.', () => {
        User.prototype.fetch.and.reject('DatabaseError');

        let request = transmitUserEdit.call(context, model);

        expect(request).toBePromise();

        return request.then(() => {
            expect(context.remove).toHaveBeenCalledWith(model);
        });
    });

    Async.it('should not update the user (no syncLevel).', () => {
        LF.StudyDesign.roles.reset([{
            id: 'site',
            lastDiaryRoleCode: 1,
            displayName: 'SITE_ADMINISTRATOR',
            permissions: ['ALL'],
            defaultAffidavit: 'DEFAULT',
            product: ['logpad', 'sitepad']
        }]);

        let request = transmitUserEdit.call(context, model);

        expect(request).toBePromise();

        return request.then(returnValue => {
            expect(returnValue).toBe(false);
            expect(Logger.prototype.operational).toHaveBeenCalledWith('User: System Administrator with role: site does not have a syncLevel.  Edits to the user were not transmitted to SW.');
            expect(context.destroy).toHaveBeenCalledWith(model.get('id'));
        });
    });

    Async.it('should fail to resolve the syncValue.', () => {
        spyOn(UserSync, 'getValue').and.reject('UnknownError');

        let request = transmitUserEdit.call(context, model);

        expect(request).toBePromise();

        return request.then(returnValue => {
            expect(UserSync.getValue).toHaveBeenCalled();
            expect(returnValue).toBe(false);
            expect(Logger.prototype.error).toHaveBeenCalledWith('There was an error updating the user System Administrator with role site.', 'UnknownError');
            expect(context.remove).toHaveBeenCalledWith(model);
        });
    });

    Async.it('should resolve w/o a syncValue.', () => {
        spyOn(UserSync, 'getValue').and.resolve(null);

        let request = transmitUserEdit.call(context, model);

        expect(request).toBePromise();

        return request.then(returnValue => {
            expect(UserSync.getValue).toHaveBeenCalled();
            expect(returnValue).toBe(false);
            expect(Logger.prototype.operational).toHaveBeenCalledWith('User: System Administrator with role: site does not have a syncValue.  Edits to the user were not transmitted to SW.');
            expect(context.destroy).toHaveBeenCalledWith(model.get('id'));
        });
    });

    Async.it('should fail to transmit the user update.', () => {
        spyOn(UserSync, 'getValue').and.resolve('DOM.12345');
        WebService.prototype.updateUser.and.reject('HTTPError');

        let request = transmitUserEdit.call(context, model);

        expect(request).toBePromise();

        return request.then(returnValue => {
            expect(WebService.prototype.updateUser).toHaveBeenCalled();
            expect(returnValue).toBe(false);
            expect(Logger.prototype.error).toHaveBeenCalledWith('There was an error updating the user System Administrator with role site.', 'HTTPError');
            expect(context.remove).toHaveBeenCalledWith(model);
        });
    });

    Async.it('should resolve the update with "S".', () => {
        spyOn(UserSync, 'getValue').and.resolve('DOM.12345');
        WebService.prototype.updateUser.and.resolve('S');

        let request = transmitUserEdit.call(context, model);

        expect(request).toBePromise();

        return request.then(returnValue => {
            expect(WebService.prototype.updateUser).toHaveBeenCalled();
            expect(returnValue).toBe(true);
            expect(Logger.prototype.operational).toHaveBeenCalledWith('System Administrator was updated.');
            expect(context.destroy).toHaveBeenCalledWith(model.get('id'));
        });
    });

    Async.it('should resolve the update with "N".', () => {
        spyOn(UserSync, 'getValue').and.resolve('DOM.12345');
        spyOn(ELF, 'trigger').and.resolve();
        WebService.prototype.updateUser.and.resolve('N');

        let request = transmitUserEdit.call(context, model);

        expect(request).toBePromise();

        return request.then(returnValue => {
            expect(WebService.prototype.updateUser).toHaveBeenCalled();
            expect(returnValue).toBe(false);
            expect(Logger.prototype.error).toHaveBeenCalledWith('System Administrator does not exist.');
            expect(context.destroy).toHaveBeenCalledWith(model.get('id'));
            expect(ELF.trigger).toHaveBeenCalledWith('TRANSMIT:Nonexistent/User', jasmine.any(Object), context);
        });
    });

    Async.it('should resolve the update with "E".', () => {
        spyOn(UserSync, 'getValue').and.resolve('DOM.12345');
        WebService.prototype.updateUser.and.resolve('E');

        let request = transmitUserEdit.call(context, model);

        expect(request).toBePromise();

        return request.then(returnValue => {
            expect(WebService.prototype.updateUser).toHaveBeenCalled();
            expect(returnValue).toBe(false);
            expect(Logger.prototype.error).toHaveBeenCalledWith('There was an error updating the user System Administrator with role site.');
            expect(context.remove).toHaveBeenCalledWith(model);
        });
    });

    Async.it('should resolve the update with "D".', () => {
        spyOn(UserSync, 'getValue').and.resolve('DOM.12345');
        WebService.prototype.updateUser.and.resolve('D');
        spyOn(ELF, 'trigger').and.resolve({ preventDefault: true });

        let request = transmitUserEdit.call(context, model);

        expect(request).toBePromise();

        return request.then(returnValue => {
            expect(WebService.prototype.updateUser).toHaveBeenCalled();
            expect(returnValue).toBe(false);
            expect(ELF.trigger).toHaveBeenCalledWith('TRANSMIT:Duplicate/User', jasmine.any(Object), context);
            expect(Logger.prototype.info).toHaveBeenCalledWith('Transmit update user failed. User System Administrator with role site already exists.');
        });
    });
});
