import WebService from 'core/classes/WebService';
import CurrentContext from 'core/CurrentContext';
import Transmissions from 'core/collections/Transmissions';
import Subjects from 'core/collections/Subjects';
import transmitResetCredentials from 'sitepad/transmit/transmitResetCredentials';
import User from 'core/models/User';
import Subject from 'core/models/Subject';
import Logger from 'core/Logger';
import { resetStudyDesign } from 'test/helpers/StudyDesign';
import DailyUnlockCode from 'core/classes/DailyUnlockCode';
import * as lStorage from 'core/lStorage';
import CurrentSubject from 'core/classes/CurrentSubject';
import Spinner from 'core/Spinner';

TRACE_MATRIX('US7958')
.describe('Transmit:transmitResetCredentials', () => {
    let subject,
        transmissions = new Transmissions(),
        setupCode = '28105953';

    beforeAll(() => {
        resetStudyDesign();
        CurrentContext.init();
        CurrentContext().setContextByRole('admin');
        LF.appName = 'SitePad App';
        LF.security = LF.security || {};
        LF.security.activeUser = new User({ id: 1 });
        LF.spinner = Spinner;
    });

    beforeEach(() => {
        spyOn(Transmissions.prototype, 'destroy').and.callFake((id, callback = $.noop) => {
            callback();
            return Q();
        });
        spyOn(Transmissions.prototype, 'remove').and.callThrough();

        transmissions.add({
            id: 1,
            method: 'transmitResetCredentials',
            params: JSON.stringify({
                krpt: 'SA.ff634d2f7a87eebf47eda9ae99eca813',
                password: 'New EncryptedPasswordHere',
                secretQuestion: '1',
                secretAnswer: 'New EncryptedSecretAnswerHere'
            }),
            created: new Date().getTime()
        });

        subject = new Subject({
            id: 1,
            krpt: 'SA.ff634d2f7a87eebf47eda9ae99eca813',
            site_code: '267',
            subject_id: '12345',
            subject_number: '54321',
            subject_active: 1,
            secret_question: '0',
            secret_answer: 'Old EncryptedSecretAnswerHere',
            subject_password: 'Old EncryptedPasswordHere',
            service_password: 'Old EncryptedServicePasswordHere'
        });

        spyOn(Subjects.prototype, 'fetch').and.callFake(function () {
            this.add(subject);

            return Q();
        });

        spyOn(lStorage, 'getItem').and.callFake(key => {
            switch (key) {
                case 'deviceId':
                    return 'DeviceId123456';
                case 'clientId':
                    return 'ClientId123456';
                default:
                    return null;
            }
        });

        spyOn(WebService.prototype, 'getSetupCode').and.resolve(setupCode);
        spyOn(Logger.prototype, 'error').and.stub();
        spyOn(Spinner, 'hide').and.resolve();
        spyOn(Spinner, 'show').and.resolve();
        spyOn(LF.Actions, 'notify').and.callFake((params, callback = $.noop) => {
            callback();
            return Q();
        });
    });

    afterEach(() => {
        transmissions.reset();
    });

    describe('Transmission', () => {
        Async.it('should get SetupCode if subject does not have SetupCode.', () => {
            spyOn(WebService.prototype, 'resetSubjectCredentials').and.callThrough();

            return Q.Promise((resolve) => {
                transmitResetCredentials.call(transmissions, transmissions.at(0), resolve);
            })
            .then(() => {
                expect(WebService.prototype.getSetupCode).toHaveBeenCalledWith('SA.ff634d2f7a87eebf47eda9ae99eca813');
            });
        });

        Async.it('should call the webservice method with the correct payload and parameters.', () => {
            let expectedUnlockCode = '123456',
                expectedPayload = {
                    setupCode,
                    clientPassword: 'New EncryptedPasswordHere',
                    regDeviceId: 'DeviceId123456',
                    clientId: 'ClientId123456',
                    challengeQuestions: [{
                        question: '1',
                        answer: 'New EncryptedSecretAnswerHere'
                    }],
                    source: 'SitePad'
                };

            spyOn(WebService.prototype, 'resetSubjectCredentials').and.callThrough();
            spyOn(DailyUnlockCode.prototype, 'getCode').and.callFake(() => expectedUnlockCode);

            return Q.Promise((resolve) => {
                transmitResetCredentials.call(transmissions, transmissions.at(0), resolve);
            })
            .then(() => {
                expect(WebService.prototype.resetSubjectCredentials).toHaveBeenCalledWith(expectedPayload, 'SA.ff634d2f7a87eebf47eda9ae99eca813', expectedUnlockCode, jasmine.any(Function), jasmine.any(Function));
            });
        });

        describe('Unexpected error', () => {
            let transmission;

            Async.beforeEach(() => {
                transmission = transmissions.at(0);

                spyOn(WebService.prototype, 'resetSubjectCredentials').and.throwError('Unexpected error');

                return Q.Promise((resolve) => {
                    transmitResetCredentials.call(transmissions, transmission, resolve);
                });
            });

            it('should log an error.', () => {
                expect(Logger.prototype.error).toHaveBeenCalledWith('Error in transmitResetCredentials', new Error('Unexpected error'));
            });

            it('should skip transmission.', () => {
                expect(Transmissions.prototype.remove).toHaveBeenCalledWith(transmission);
                expect(transmissions.length).toBe(0);
            });
        });
    });

    describe('Transmission Success', () => {
        let transmission,
            transmissionId;

        Async.beforeEach(() => {
            transmission = transmissions.at(0);
            transmissionId = transmission.get('id');

            spyOn(CurrentSubject, 'clearSubject').and.stub();
            spyOn(WebService.prototype, 'resetSubjectCredentials').and.callFake((payload, krpt, unlockCode, onSuccess) => {
                onSuccess({
                    password: 'New EncryptedServicePasswordHere'
                }, {
                    syncID: null,
                    isSubjectActive: 1,
                    isDuplicate: false
                });
            });

            return Q.Promise((resolve) => {
                transmitResetCredentials.call(transmissions, transmission, resolve);
            });
        });

        it('should update the subject credentials', () => {
            expect(subject.get('subject_password')).toBe('New EncryptedPasswordHere');
            expect(subject.get('secret_question')).toBe('1');
            expect(subject.get('secret_answer')).toBe('New EncryptedSecretAnswerHere');
            expect(subject.get('subject_active')).toBe(1);
            expect(subject.get('service_password')).toBe('New EncryptedServicePasswordHere');
        });

        it('should invalidate CurrentSubject cache', () => {
            expect(CurrentSubject.clearSubject).toHaveBeenCalled();
        });

        it('should delete transmission.', () => {
            expect(Transmissions.prototype.destroy).toHaveBeenCalledWith(transmissionId);
        });
    });

    describe('Transmission Error', () => {
        describe('Subject is not active', () => {
            let transmission,
                transmissionId;

            Async.beforeEach(() => {
                transmission = transmissions.at(0);
                transmissionId = transmission.get('id');

                spyOn(WebService.prototype, 'resetSubjectCredentials').and.callFake((payload, krpt, unlockCode, onSuccess, onError) => {
                    onError(null, null, 0, false);
                });

                return Q.Promise((resolve) => {
                    transmitResetCredentials.call(transmissions, transmission, resolve);
                });
            });

            it('Should show notification', () => {
                // TODO: For some reason Dialog keys do not match between spec files and the application code.
                // For now the expected parameters are not used
                expect(LF.Actions.notify).toHaveBeenCalled();
            });

            it('Should delete the transmission item after notification', () => {
                expect(Transmissions.prototype.destroy).toHaveBeenCalledWith(transmissionId, jasmine.any(Function));
            });
        });

        describe('Error different than Page Not Found 404', () => {
            let transmission,
                transmissionId;

            beforeEach(() => {
                transmission = transmissions.at(0);
                transmissionId = transmission.get('id');
            });

            Async.it('should show notification', () => {
                spyOn(WebService.prototype, 'resetSubjectCredentials').and.callFake((payload, krpt, unlockCode, onSuccess, onError) => {
                    onError('6', 403, 1, false);
                });

                return Q.Promise((resolve) => {
                    transmitResetCredentials.call(transmissions, transmission, resolve);
                })
                .then(() => {
                    // TODO: For some reason Dialog keys do not match between spec files and the application code.
                    // For now the expected dialog key is replaced with anything
                    expect(LF.Actions.notify).toHaveBeenCalled();
                });
            });

            Async.it('should delete transmission item if Subject is deleted on backend', () => {
                spyOn(WebService.prototype, 'resetSubjectCredentials').and.callFake((payload, krpt, unlockCode, onSuccess, onError) => {
                    onError('6', 403, 1, false);
                });

                return Q.Promise((resolve) => {
                    transmitResetCredentials.call(transmissions, transmission, resolve);
                })
                .then(() => {
                    expect(Transmissions.prototype.destroy).toHaveBeenCalledWith(transmissionId, jasmine.any(Function));
                });
            });

            Async.it('should skip transmission item if Subject exists on backend ', () => {
                spyOn(WebService.prototype, 'resetSubjectCredentials').and.callFake((payload, krpt, unlockCode, onSuccess, onError) => {
                    onError('1', 403, 1, false);
                });

                return Q.Promise((resolve) => {
                    transmitResetCredentials.call(transmissions, transmission, resolve);
                })
                .then(() => {
                    expect(Transmissions.prototype.remove).toHaveBeenCalledWith(transmission);
                    expect(transmissions.length).toBe(0);
                });
            });
        });

        Async.it('Should skip transmission item on Page Not Found 404 error', () => {
            let transmission = transmissions.at(0);

            spyOn(WebService.prototype, 'resetSubjectCredentials').and.callFake((payload, krpt, unlockCode, onSuccess, onError) => {
                onError(null, 404, 1, false);
            });

            return Q.Promise((resolve) => {
                transmitResetCredentials.call(transmissions, transmission, resolve);
            })
            .then(() => {
                expect(Transmissions.prototype.remove).toHaveBeenCalledWith(transmission);
                expect(transmissions.length).toBe(0);
            });
        });
    });
});