import WebService from 'core/classes/WebService';
import CurrentContext from 'core/CurrentContext';
import Transmissions from 'core/collections/Transmissions';
import Subjects from 'core/collections/Subjects';
import transmitResetPassword from 'sitepad/transmit/transmitResetPassword';
import * as transmitResetCredentials from 'sitepad/transmit/transmitResetCredentials';
import User from 'core/models/User';
import Subject from 'core/models/Subject';
import Logger from 'core/Logger';
import { resetStudyDesign } from 'test/helpers/StudyDesign';
import CurrentSubject from 'core/classes/CurrentSubject';

TRACE_MATRIX('US7958')
.describe('Transmit:transmitResetPassword', () => {
    let askSecurityQuestion,
        subject,
        transmissions = new Transmissions(),
        setupCode = '28105953';

    beforeAll(() => {
        resetStudyDesign();
        CurrentContext.init();
        CurrentContext().setContextByRole('admin');
        LF.appName = 'SitePad App';
        LF.security = LF.security || {};
        LF.security.activeUser = new User({ id: 1 });
    });

    beforeEach(() => {
        askSecurityQuestion = LF.StudyDesign.askSecurityQuestion;
        spyOn(Transmissions.prototype, 'destroy').and.callFake((id, callback = $.noop) => {
            callback();
            return Q();
        });
        spyOn(Transmissions.prototype, 'remove').and.callThrough();

        transmissions.add({
            id: 1,
            method: 'transmitResetPassword',
            params: JSON.stringify({
                krpt: 'SA.ff634d2f7a87eebf47eda9ae99eca813',
                password: 'New EncryptedPasswordHere'
            }),
            created: new Date().getTime()
        });

        subject = new Subject({
            id: 1,
            krpt: 'SA.ff634d2f7a87eebf47eda9ae99eca813',
            site_code: '267',
            subject_id: '12345',
            subject_number: '54321',
            subject_active: 1,
            secret_question: '0',
            secret_answer: 'Old EncryptedSecretAnswerHere',
            subject_password: 'Old EncryptedPasswordHere',
            service_password: 'Old EncryptedServicePasswordHere'
        });

        spyOn(Subjects.prototype, 'fetch').and.callFake(function () {
            this.add(subject);

            return Q();
        });

        spyOn(WebService.prototype, 'getSetupCode').and.resolve(setupCode);
        spyOn(Logger.prototype, 'error').and.stub();
    });

    afterEach(() => {
        transmissions.reset();
        LF.StudyDesign.askSecurityQuestion = askSecurityQuestion;
    });

    describe('Transmission', () => {
        Async.it('should get SetupCode if subject does not have SetupCode.', () => {
            spyOn(WebService.prototype, 'resetSubjectPassword').and.callThrough();

            return Q.Promise((resolve) => {
                transmitResetPassword.call(transmissions, transmissions.at(0), resolve);
            })
            .then(() => {
                expect(WebService.prototype.getSetupCode).toHaveBeenCalledWith('SA.ff634d2f7a87eebf47eda9ae99eca813');
            });
        });

        Async.it('should call the webservice method with the correct payload and parameters when security questions are enabled.', () => {
            LF.StudyDesign.askSecurityQuestion = true;

            let expectedPayload = {
                clientPassword: 'New EncryptedPasswordHere',
                challengeQuestions: [{
                    question: '0',
                    answer: 'Old EncryptedSecretAnswerHere'
                }],
                source: 'SitePad'
            };

            spyOn(WebService.prototype, 'resetSubjectPassword').and.callThrough();

            return Q.Promise((resolve) => {
                transmitResetPassword.call(transmissions, transmissions.at(0), resolve);
            })
            .then(() => {
                expect(WebService.prototype.resetSubjectPassword).toHaveBeenCalledWith(expectedPayload, 'SA.ff634d2f7a87eebf47eda9ae99eca813', jasmine.any(Function), jasmine.any(Function));
            });
        });

        Async.it('should call the webservice method with the correct payload and parameters when security questions are enabled.', () => {
            LF.StudyDesign.askSecurityQuestion = false;

            let expectedPayload = {
                clientPassword: 'New EncryptedPasswordHere',
                source: 'SitePad'
            };

            spyOn(WebService.prototype, 'resetSubjectPassword').and.callThrough();

            return Q.Promise((resolve) => {
                transmitResetPassword.call(transmissions, transmissions.at(0), resolve);
            })
            .then(() => {
                expect(WebService.prototype.resetSubjectPassword).toHaveBeenCalledWith(expectedPayload, 'SA.ff634d2f7a87eebf47eda9ae99eca813', jasmine.any(Function), jasmine.any(Function));
            });
        });

        Async.it('should call transmitResetCredentials.onError if there is a transmission error', () => {
            let transmission = transmissions.at(0);

            spyOn(transmitResetCredentials, 'onError').and.callFake((errorCode, httpCode, isSubjectActive, transmissionItem, callback) => {
                callback();
            });

            spyOn(WebService.prototype, 'resetSubjectPassword').and.callFake((payload, krpt, onSuccess, onError) => {
                onError('1', 403, 1, false);
            });

            return Q.Promise((resolve) => {
                transmitResetPassword.call(transmissions, transmission, resolve);
            })
            .then(() => {
                expect(transmitResetCredentials.onError).toHaveBeenCalledWith('1', 403, 1, transmission, jasmine.any(Function));
            });
        });

        describe('Unexpected error', () => {
            let transmission;

            Async.beforeEach(() => {
                transmission = transmissions.at(0);

                spyOn(WebService.prototype, 'resetSubjectPassword').and.throwError('Unexpected error');

                return Q.Promise((resolve) => {
                    transmitResetPassword.call(transmissions, transmission, resolve);
                });
            });

            it('should log an error.', () => {
                expect(Logger.prototype.error).toHaveBeenCalledWith('Error in transmitResetPassword', new Error('Unexpected error'));
            });

            it('should skip transmission.', () => {
                expect(Transmissions.prototype.remove).toHaveBeenCalledWith(transmission);
                expect(transmissions.length).toBe(0);
            });
        });
    });

    describe('Transmission Success', () => {
        let transmission,
            transmissionId;

        Async.beforeEach(() => {
            transmission = transmissions.at(0);
            transmissionId = transmission.get('id');

            spyOn(CurrentSubject, 'clearSubject').and.stub();
            spyOn(WebService.prototype, 'resetSubjectPassword').and.callFake((payload, krpt, onSuccess) => {
                onSuccess({
                    password: 'New EncryptedServicePasswordHere'
                }, {
                    syncID: null,
                    isSubjectActive: 1,
                    isDuplicate: false
                });
            });

            return Q.Promise((resolve) => {
                transmitResetPassword.call(transmissions, transmission, resolve);
            });
        });

        it('should update the subject credentials', () => {
            expect(subject.get('subject_password')).toBe('New EncryptedPasswordHere');
            expect(subject.get('subject_active')).toBe(1);
            expect(subject.get('service_password')).toBe('New EncryptedServicePasswordHere');
        });

        it('should invalidate CurrentSubject cache', () => {
            expect(CurrentSubject.clearSubject).toHaveBeenCalled();
        });

        it('should delete transmission.', () => {
            expect(Transmissions.prototype.destroy).toHaveBeenCalledWith(transmissionId);
        });
    });
});