import ModeSelectionView from 'sitepad/views/ModeSelectionView';
import * as lStorage from 'core/lStorage';
import * as utils from 'core/utilities';
import ELF from 'core/ELF';
import Logger from 'core/Logger';

import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';

import PageViewSuite from 'test/specs/core/views/PageView.specBase';

class ModeSelectionViewSuite extends PageViewSuite {

    beforeEach () {
        resetStudyDesign();

        this.removeTemplate = specHelpers.renderTemplateToDOM('sitepad/templates/mode-selection.ejs');

        this.view = new ModeSelectionView();

        return this.view.resolve()
        .then(() => this.view.render())
        .then(() => super.beforeEach());
    }

    afterEach () {
        this.removeTemplate();

        return super.afterEach();
    }

    executeAll (context) {
        super.executeAll(context);

        let methods = Object.getOwnPropertyNames(ModeSelectionViewSuite.prototype);

        this.annotateAll(methods, context);
    }

    testRender () {
        describe('method:render', () => {
            Async.it('should render the view.', () => {
                let request = this.view.render();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.$el.html()).toBeDefined();
                });
            });

            Async.it('should fail to render the view.', () => {
                spyOn(this.view, 'buildHTML').and.reject('DOMError');

                let request = this.view.render();

                expect(request).toBePromise();

                return request
                .then(() => fail('Method render should have been rejected.'))
                .catch(e => {
                    expect(e).toBe('DOMError');
                });
            });
        });
    }

    testModeChanged () {
        describe('method:modeChanged', () => {
            beforeEach(() => {
                this.view.$description.html('');
            });

            it('should do nothing (unknown mode).', () => {
                this.view.$mode.val('unknown');
                this.view.modeChanged();

                expect(this.view.$description.html()).toBe('');
            });

            it('should show the provision description.', () => {
                this.view.$mode.val('provision');
                this.view.modeChanged();

                expect(this.view.$description.html()).toBe('The device is registered by manufacturing.');
            });

            /* DE16957 https://rally1.rallydev.com/#/36503486296ud/detail/defect/59556929940
            it('should show the depot description.', () => {
                this.view.$mode.val('depot');
                this.view.modeChanged();

                expect(this.view.$description.html()).toBe('The target site for this device is unknown.  The device will be shipped to the client to complete device registration.');
            });*/

            it('should show the trainer description.', () => {
                spyOn(this.view, 'hideElement');

                this.view.$mode.val('trainer');
                this.view.modeChanged();

                expect(this.view.$description.html()).toBe('The device will be used for training.');

                // DE14016
                expect(this.view.hideElement).toHaveBeenCalledWith(this.view.$environment.parent());
                expect(this.view.hideElement).toHaveBeenCalledWith(this.view.$custom.parent());
            });

        });
    }

    testRenderEnvironments () {
        describe('method:renderEnvironments', () => {
            it('should render all the provision environments, when custom environments are enabled', () => {
                LF.StudyDesign.disableCustomEnvironments = false;

                this.view.$mode.val('provision');
                this.view.renderEnvironments();

                let environments = this.view.$('#environment').children();

                expect(environments.length).toBe(4);
                expect(environments[0].innerHTML.trim()).toBe('-- Select an environment --');
                expect(environments[1].innerHTML.trim()).toBe('Development');
                expect(environments[2].innerHTML.trim()).toBe('Production');
                expect(environments[3].innerHTML.trim()).toBe('Custom environment...');
            });

            it('should render all the provision environments except custom environments, when custom environments are disabled', () => {
                LF.StudyDesign.disableCustomEnvironments = true;

                this.view.$mode.val('provision');
                this.view.renderEnvironments();

                let environments = this.view.$('#environment').children();

                expect(environments.length).toBe(3);
                expect(environments[0].innerHTML.trim()).toBe('-- Select an environment --');
                expect(environments[1].innerHTML.trim()).toBe('Development');
                expect(environments[2].innerHTML.trim()).toBe('Production');
            });
        });
    }

    testEnvironmentChanged () {
        describe('method:environmentChanged', () => {
            it('should disable the next button (no environment, mode not trainer or screenshot).', () => {
                this.view.$mode.val('provision');

                let spy = spyOn(this.view, 'disableButton');

                this.view.environmentChanged();

                expect(spy).toHaveBeenCalled();
            });

            it('should not disable the next button (no environment, mode screenshot).', () => {
                this.view.$mode.val('screenshot');

                let spy = spyOn(this.view, 'disableButton');

                this.view.environmentChanged();

                expect(spy).not.toHaveBeenCalled();
            });

            it('should not disable the next button (no environment, mode trainer).', () => {
                this.view.$mode.val('trainer');

                let spy = spyOn(this.view, 'disableButton');

                this.view.environmentChanged();

                expect(spy).not.toHaveBeenCalled();
            });

            it('should show the custom input (when custom environments are enabled)', () => {
                LF.StudyDesign.disableCustomEnvironments = false;

                this.view.$mode.val('');

                let spy = spyOn(this.view, 'showElement');

                spyOn(this.view, 'enableButton');
                this.view.renderEnvironments();
                this.view.$environment.val('custom');
                this.view.$customStudyDbName.val('DemoStudy');
                this.view.environmentChanged();

                expect(spy).toHaveBeenCalled();

                // DE14021
                expect(this.view.enableButton.calls.count()).toBe(1);

                // DE14149
                expect(this.view.$customStudyDbName.val()).toBe('');
            });

            it('should enable the next button (configured environment selected).', () => {
                let spy = spyOn(this.view, 'enableButton');

                this.view.$mode.val('provision');
                this.view.renderEnvironments();
                this.view.$environment.val('development');
                this.view.environmentChanged();

                expect(spy).toHaveBeenCalled();
            });
        });
    }

    testNextHandler () {
        describe('method:nextHandler', () => {
            it('should invoke the next method w/ evt.', () => {
                spyOn(this.view, 'next').and.resolve();

                this.view.nextHandler({ preventDefault: $.noop });

                expect(this.view.next).toHaveBeenCalled();
            });

            it('should invoke the next method w/o evt.', () => {
                spyOn(this.view, 'next').and.resolve();

                this.view.nextHandler();

                expect(this.view.next).toHaveBeenCalled();
            });
        });
    }

    testNext () {
        describe('method:next', () => {
            beforeEach(() => {
                spyOn(this.view, 'navigate');
            });

            Async.it('should navigate to site selection view (provision mode).', () => {
                this.view.$mode.val('provision');

                let request = this.view.next();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.navigate).toHaveBeenCalledWith('siteSelection');
                });
            });

            Async.it('should navigate to site selection view (trainer mode).', () => {
                this.view.$mode.val('trainer');

                let request = this.view.next();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.navigate).toHaveBeenCalledWith('siteSelection');
                });
            });

            Async.it('should fail to navigate to the site selection view.', () => {
                spyOn(ELF, 'trigger').and.reject('ELF Error');
                spyOn(Logger.prototype, 'error').and.stub();

                this.view.$mode.val('provision');

                let request = this.view.next();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(Logger.prototype.error).toHaveBeenCalled();
                });
            });

            /*DE16957 https://rally1.rallydev.com/#/36503486296ud/detail/defect/59556929940
            Async.it('should navigate to the language selection view.', () => {
                let translation = {
                    header  : 'Setup Complete',
                    body    : 'Setup of the SitePad App is complete.',
                    ok      : 'OK'
                };

                spyOn(ELF, 'trigger').and.callFake(() => Q({ preventDefault: false }));
                spyOn(view, 'i18n').and.callFake(() => {
                    return Q(translation);
                });

                view.$mode.val('depot');
                let request = view.next({ preventDefault: $.noop });

                expect(request).toBePromise();

                return request.then(() => {
                    expect(view.navigate).toHaveBeenCalledWith('languageSelection');
                    expect(view.notify.show).toHaveBeenCalledWith(translation);
                });
            });*/
        });
    }

    testSetEnvironmentToLocalStorage () {
        describe('method:setEnvironmentToLocalStorage', () => {
            beforeEach(() => {
                spyOn(lStorage, 'setItem').and.stub();
                spyOn(utils, 'setServiceBase').and.stub();
            });

            it('should set the defined environment to localStorage', () => {
                spyOn(this.view.$environment, 'val').and.callFake(() => 'production');

                this.view.setEnvironmentToLocalStorage();

                expect(lStorage.setItem).toHaveBeenCalledWith('environment', 'production');
                expect(utils.setServiceBase).toHaveBeenCalledWith('https://engcorestudy05.phtnetpro.com/production');
                expect(lStorage.setItem).toHaveBeenCalledWith('studyDbName', 'engcorestudy05');
            });

            it('should set the custom environment to localStorage', () => {
                spyOn(this.view.$environment, 'val').and.callFake(() => 'custom');
                spyOn(this.view.$custom, 'val').and.callFake(() => 'http://127.0.0.1:3000');
                spyOn(this.view.$customStudyDbName, 'val').and.callFake(() => 'DemoStudy');

                this.view.setEnvironmentToLocalStorage();

                expect(lStorage.setItem).toHaveBeenCalledWith('environment', 'custom');
                expect(utils.setServiceBase).toHaveBeenCalledWith('http://127.0.0.1:3000');
                expect(lStorage.setItem).toHaveBeenCalledWith('studyDbName', 'DemoStudy');
            });
        });
    }

    testSetModeToLocalStorage () {
        describe('method:setModeToLocalStorage', () => {
            beforeEach(() => spyOn(lStorage, 'setItem').and.stub());

            it('should set the mode (no argument provided).', () => {
                this.view.$mode.val('screenshot');

                this.view.setModeToLocalStorage();

                expect(lStorage.setItem).toHaveBeenCalledWith('mode', 'screenshot');
            });
        });
    }
}

describe('ModeSelectionView', () => {
    let suite = new ModeSelectionViewSuite();

    suite.executeAll({
        id: 'mode-selection-view',
        template: '#mode-selection-tpl',
        button: '#next',
        input: '#mode',
        dynamicStrings: {
            screenshot: 'Screenshot',
            tagline: 'Select a mode and environment for this device.',
            trainer: 'Trainer',
            provision: 'Provision',
            depot: 'Depot',
            next: 'Next',
            mode: 'Mode',
            environment: 'Environment',
            custom: 'Custom Environment',
            customStudyDbName: 'Custom Study Name'
        },
        exclude: [
            // Should be tested individually
            'testProperty',
            'testAttribute',
            // ModeSelectionView has no class name.
            'testClass',

            // DOM structure of view does not allow for these tests to pass.
            'testValidateInput',
            'testAddHelpText',
            'testRemoveHelpText',

            'testHideKeyboardOnEnter'
        ]
    });
});
