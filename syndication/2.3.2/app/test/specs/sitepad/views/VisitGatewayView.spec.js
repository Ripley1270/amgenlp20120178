import Data from 'core/Data';
import ELF from 'core/ELF';

import UserVisit from 'sitepad/models/UserVisit';
import VisitRow from 'sitepad/models/VisitRow';
import Visit from 'core/models/Visit';
import User from 'core/models/User';
import Site from 'core/models/Site';
import Subject from 'core/models/Subject';
import Role from 'core/models/Role';

import Sites from 'core/collections/Sites';

import { MessageRepo } from 'core/Notify';
import VisitGatewayView from 'sitepad/views/VisitGatewayView';
import VisitTableView from 'sitepad/views/VisitTableView';

import PageViewSuite from 'test/specs/core/views/PageView.specBase';
import * as specHelpers from 'test/helpers/SpecHelpers';
import { resetStudyDesign } from 'test/helpers/StudyDesign';

class VisitGatewayViewSuite extends PageViewSuite {
    beforeEach () {
        resetStudyDesign();

        // jscs:disable requireArrowFunctions
        // Long hand functions used in these cases to retain the context of this to the spied upon objects.
        spyOn(Sites.prototype, 'fetch').and.callFake(function () {
            this.add({
                id: 1,
                siteCode: '001',
                studyName: 'DemoStudy',
                hash: '0x12345'
            });

            return Q();
        });

        spyOn(Subject.prototype, 'fetch').and.callFake(function () {
            this.set({
                id: 1,
                subject_number: '100-001',
                subject_id: '100-001',
                phase: 10,
                initials: 'SA',
                lastVisit: new Date(),
                custom10: '2'
            });

            return Q();
        });
        // jscs:enable requireArrowFunctions

        LF.appName = 'SitePad App';
        LF.security = jasmine.createSpyObj('security', [
            'pauseSessionTimeOut',
            'restartSessionTimeOut',
            'getUserRole'
        ]);
        LF.security.getUserRole.and.returnValue(new Role({ displayName: 'ADMIN' }));
        LF.security.activeUser = new User({
            id: 1,
            role: 'admin'
        });

        LF.Preferred = LF.Preferred || {};
        LF.Preferred.language = 'en';
        LF.Preferred.locale = 'US';

        this.removeTemplate = specHelpers.renderTemplateToDOM('sitepad/templates/visit-gateway.ejs');

        this.view = new VisitGatewayView({ subjectId: 1 });

        return this.view.resolve()
        .then(() => this.view.render())
        .then(() => super.beforeEach());
    }

    afterEach () {
        this.removeTemplate();

        LF.security = undefined;
        Data.skippedVisits = [];

        return super.afterEach();
    }

    executeAll (context) {
        super.executeAll(context);

        let methods = Object.getOwnPropertyNames(VisitGatewayViewSuite.prototype);

        this.annotateAll(methods, context);
    }

    testResolve () {
        describe('method:resolve', () => {
            Async.it('should fail to resolve.', () => {
                Sites.prototype.fetch.and.reject('DatabaseError');

                let request = this.view.resolve();

                expect(request).toBePromise();

                return request.then(() => fail('Method resolve should have been rejected.'))
                .catch(e => expect(e).toBe('DatabaseError'));
            });

            Async.it('should resolve.', () => {
                let request = this.view.resolve();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.subject).toEqual(jasmine.any(Subject));
                    expect(this.view.site).toEqual(jasmine.any(Site));
                });
            });
        });
    }

    testRender () {
        describe('method:render', () => {
            Async.it('should fail to render (buildHTML rejected).', () => {
                spyOn(this.view, 'buildHTML').and.reject('DOMError');

                let request = this.view.render();

                expect(request).toBePromise();

                return request.then(() => fail('Method render should have been rejected.'))
                .catch(e => expect(e).toBe('DOMError'));
            });

            Async.it('should fail to render (buildHTML rejected).', () => {
                spyOn(this.view, 'renderVisits').and.reject('UnknownError');

                let request = this.view.render();

                expect(request).toBePromise();

                return request.then(() => fail('Method render should have been rejected.'))
                .catch(e => expect(e).toBe('UnknownError'));
            });

            Async.it('should render.', () => {
                spyOn(this.view, 'renderVisits').and.stub();

                let request = this.view.render();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.renderVisits).toHaveBeenCalled();
                });
            });

            Async.it('should call a visit filtering rule', () => {
                let ruleCalled = false;
                ELF.rules.add({
                    id: 'customVisitsRenderTest',
                    trigger: 'VISITGATEWAY:BeforeRenderVisits',
                    resolve: [{
                        action: () => {
                            ruleCalled = true;
                        }
                    }]
                });

                return this.view.render()
                .then(() => {
                    expect(ruleCalled).toBe(true);
                });
            });
        });

        Async.it('should navigate to the home view.', () => {
            spyOn(this.view, 'navigate').and.stub();
            spyOn(this.view, 'notify').and.resolve();

            delete this.view.subject;

            return this.view.render()
            .then(() => {
                expect(this.view.navigate).toHaveBeenCalledWith('home');
                expect(this.view.notify).toHaveBeenCalled();
            });
        });
    }

    testVisitExpiration () {
        Async.it('should call check visit expiration method.', () => {
            spyOn(VisitTableView.prototype, 'evaluateVisitsExpiration').and.resolve();

            this.view.table = new VisitTableView({
                rows: [{
                    header: 'ORDER',
                    property: 'order'
                }],
                subject: this.subject
            });

            return this.view.fetchTableData()
            .then(() => {
                expect(VisitTableView.prototype.evaluateVisitsExpiration).toHaveBeenCalled();
            });
        });
    }

    testSelect () {
        describe('method:select', () => {
            let model;

            beforeEach(() => {
                model = new VisitRow({
                    visit: LF.StudyDesign.visits.at(4)
                });
            });

            it('should enable the start and skip buttons upon selecting a visit.', () => {
                spyOn(this.view, 'enableButton').and.stub();

                this.view.select(model);

                expect(this.view.selected).toEqual(jasmine.any(Visit));
                expect(this.view.enableButton).toHaveBeenCalledWith(this.view.$startVisit);
                expect(this.view.enableButton).toHaveBeenCalledWith(this.view.$skipVisit);
            });

            it('should enable the start and skip buttons upon selecting an in-progress visit.', () => {
                spyOn(this.view, 'enableButton').and.stub();

                let userVisit = new UserVisit({
                    visitId: 'visit5',
                    subject_id: '1',
                    state: LF.VisitStates.IN_PROGRESS,
                    dateModified: new Date()
                });
                model.set('userVisit', userVisit);

                this.view.select(model);

                expect(this.view.selected).toEqual(model.get('visit'));
                expect(this.view.enableButton).toHaveBeenCalledWith(this.view.$startVisit);
                expect(this.view.enableButton).toHaveBeenCalledWith(this.view.$skipVisit);
            });

            it('disable the start and skip buttons upon selecting a completed visit.', () => {
                spyOn(this.view, 'disableButton').and.stub();

                let userVisit = new UserVisit({
                    visitId: 'visit5',
                    subject_id: '1',
                    state: LF.VisitStates.SKIPPED,
                    dateModified: new Date()
                });
                model.set('userVisit', userVisit);

                this.view.select(model);

                expect(this.view.selected).toEqual(model.get('visit'));
                expect(this.view.disableButton).toHaveBeenCalledWith(this.view.$startVisit);
                expect(this.view.disableButton).toHaveBeenCalledWith(this.view.$skipVisit);
            });

            it('should enable the start button and disable the skip button upon selecting an container visit.', () => {
                spyOn(this.view, 'enableButton').and.stub();
                spyOn(this.view, 'disableButton').and.stub();

                let containerVisit = new Visit({
                    visitType: 'container'
                });
                model.set('visit', containerVisit);

                this.view.select(model);

                expect(this.view.enableButton).toHaveBeenCalledWith(this.view.$startVisit);
                expect(this.view.disableButton).toHaveBeenCalledWith(this.view.$skipVisit);
            });
        });
    }

    testDeselect () {
        describe('method:deselect', () => {
            it('deselecting Visit should disable start and skip Visit buttons.', () => {
                spyOn(this.view, 'disableButton');

                this.view.selected = LF.StudyDesign.visits.at(4);
                this.view.deselect();

                expect(this.view.selected).toBeFalsy();
                expect(this.view.disableButton).toHaveBeenCalledWith(this.view.$startVisit);
                expect(this.view.disableButton).toHaveBeenCalledWith(this.view.$skipVisit);
            });
        });
    }

    testBack () {
        describe('method:back', () => {
            it('should navigate back.', () => {
                spyOn(this.view, 'navigate').and.stub();

                this.view.back();

                expect(this.view.navigate).toHaveBeenCalledWith('home');
            });
        });
    }

    testStartVisit () {
        describe('method:startVisit', () => {
            Async.it('triggers an event on visit start', () => {
                let visit = LF.StudyDesign.visits.at(0);

                spyOn(ELF, 'trigger').and.resolve({});
                spyOn(this.view.table, 'checkSkippedVisits').and.returnValue(true);
                spyOn(this.view, 'skipConfirmView').and.resolve();

                this.view.selected = visit;
                let request = this.view.startVisit();
                expect(request).toBePromise();

                return request.then(() => {
                    expect(ELF.trigger).toHaveBeenCalledWith('VISITGATEWAY:BeforeStartVisit', {
                        subject: this.view.subject,
                        visit: this.view.selected,
                        userVisit: this.view.userVisit
                    }, this.view);
                });
            });

            Async.it('should ask for confirmation to skip.', () => {
                let visit = LF.StudyDesign.visits.at(0);

                spyOn(ELF, 'trigger').and.resolve({ });
                spyOn(this.view.table, 'checkSkippedVisits').and.returnValue(true);
                spyOn(this.view, 'skipConfirmView').and.resolve();

                this.view.selected = visit;
                let request = this.view.startVisit();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.skipConfirmView).toHaveBeenCalled();
                    expect(Data.skippedVisits.length).toBe(0);
                });
            });

            Async.it('should start the visit.', () => {
                let visit = LF.StudyDesign.visits.at(0);

                spyOn(ELF, 'trigger').and.resolve({ });
                spyOn(this.view, 'navigate').and.stub();
                spyOn(this.view.table, 'checkSkippedVisits').and.returnValue(false);
                spyOn(this.view, 'skipConfirmView').and.resolve();

                this.view.selected = visit;
                let request = this.view.startVisit();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.skipConfirmView).not.toHaveBeenCalled();
                    expect(this.view.navigate).toHaveBeenCalledWith('dashboard', true, {
                        subject: jasmine.any(Subject),
                        visit: jasmine.any(Visit),
                        disableCheckVisitExpired: true
                    });

                    expect(Data.skippedVisits.length).toBe(0);
                });
            });
        });
    }

    testSkipConfirmView () {
        describe('method:skipConfirmView', () => {
            Async.it('should ask for confirmation and be accepted.', () => {
                spyOn(MessageRepo, 'display').and.resolve();
                spyOn(this.view, 'navigate').and.stub();

                let visit = new Visit();
                let request = this.view.skipConfirmView(visit);

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.navigate).toHaveBeenCalledWith('skip-visits', true, {
                        subject: jasmine.any(Subject),
                        visit: jasmine.any(Visit)
                    });
                });
            });

            Async.it('should ask for confirmation and be rejected.', () => {
                spyOn(MessageRepo, 'display').and.reject();
                spyOn(this.view, 'navigate').and.stub();
                spyOn(this.view, 'select').and.stub();

                Data.skippedVisits = [];
                let request = this.view.skipConfirmView();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.navigate).not.toHaveBeenCalled();
                    expect(Data.skippedVisits).toBeUndefined();
                });
            });
        });
    }

    testSkipVisit () {
        describe('method:skipVisit', () => {
            Async.it('should skip the visit (ordered).', () => {
                let visit = LF.StudyDesign.visits.at(0);

                spyOn(this.view.table, 'checkSkippedVisits').and.stub();
                spyOn(this.view, 'skipConfirmView').and.resolve();
                spyOn(this.view, 'select').and.stub();

                this.view.selected = visit;
                let request = this.view.skipVisit();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.table.checkSkippedVisits).toHaveBeenCalledWith('skipOver');
                    expect(this.view.skipConfirmView).toHaveBeenCalled();
                });
            });

            Async.it('should skip the visit (-).', () => {
                let visit = LF.StudyDesign.visits.at(4);

                spyOn(this.view.table, 'checkSkippedVisits').and.stub();
                spyOn(this.view, 'skipConfirmView').and.resolve();
                spyOn(this.view, 'select').and.stub();

                this.view.selected = visit;
                let request = this.view.skipVisit();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.table.checkSkippedVisits).not.toHaveBeenCalled();
                    expect(this.view.skipConfirmView).toHaveBeenCalled();
                    expect(Data.skippedVisits.length).toBe(1);
                });
            });
        });
    }

    testTransmit () {
        TRACE_MATRIX('US6573').
        TRACE_MATRIX('DE16139').
        describe('method:transmit', () => {
            Async.it('should trigger an ELF rule and reload the view.', () => {
                spyOn(ELF, 'trigger').and.resolve();
                spyOn(this.view, 'reload').and.stub();

                let request = this.view.transmit();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(ELF.trigger).toHaveBeenCalledWith('VISITGATEWAY:Transmit', {
                        subject: jasmine.any(Subject)
                    }, this.view);
                    expect(this.view.reload).toHaveBeenCalled();
                });
            });
        });
    }
}

describe('VisitGatewayView', () => {
    let suite = new VisitGatewayViewSuite();

    suite.executeAll({
        id: 'visit-gateway',
        template: '#visit-gateway-template',
        button: '#start-visit',
        dynamicStrings: {
            username: 'SysAdmin',
            siteCode: '0100',
            appVersion: '2.0.0',
            studyName: 'eng-dev-2017',
            patientNumber: '0100-1111',
            patientInitials: 'JT',
            studyVersionVal: '1.0.0',
            protocolVal: 'Protocol2',
            sponsorVal: 'ERT'
        },
        exclude: [
            // Should be tested individually
            'testProperty',
            'testAttribute',
            // PageView has no class name.
            'testClass',

            // DOM structure of view does not allow for these tests to pass.
            'testClearInputState',
            'testValidateInput',
            'testAddHelpText',
            'testRemoveHelpText',
            'testGetValidParent',
            'testInputSuccess',
            'testInputError',
            'testHideKeyboardOnEnter'
        ]
    });
});
