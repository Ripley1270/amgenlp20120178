import UnlockCodeView from 'sitepad/views/UnlockCodeView';
import UnlockCode from 'core/classes/UnlockCode';
import Site from 'core/models/Site';
import Sites from 'core/collections/Sites';
import ELF from'core/ELF'
import * as lStorage from 'core/lStorage';
import Data from 'core/Data';
import { Banner } from 'core/Notify';
import * as specHelpers from 'test/helpers/SpecHelpers';
import { resetStudyDesign } from 'test/helpers/StudyDesign';

TRACE_MATRIX('US5986').
describe('UnlockCodeView', () => {
    let view,
        site,
        removeTemplate;

    Async.beforeEach(() => {
        resetStudyDesign();
        removeTemplate = specHelpers.renderTemplateToDOM('sitepad/templates/unlock-code.ejs');

        // jscs:disable requireArrowFunctions
        // Long hand functions used in these cases to retain the context of this to the spied upon objects.
        spyOn(Sites.prototype, 'fetch').and.callFake(function () {
            this.add({
                id: 1,
                siteCode: '0008',
                studyName: 'engcorestudy05',
                hash: '0x12345'
            });

            return Q();
        });
        // jscs:enable requireArrowFunctions

        spyOn(lStorage, 'setItem').and.stub();
        spyOn(lStorage, 'getItem').and.stub();

        view = new UnlockCodeView();

        lStorage.getItem.and.returnValue('engcorestudy05');

        spyOn(view, 'reload').and.stub();
        spyOn(view, 'navigate').and.stub();

        return view.resolve()
            .then(() => view.render());
    });

    afterEach(() => removeTemplate());

    it('should have an id.', () => {
        expect(view.id).toBe('unlock-code-view');
    });

    it('should have a selectors property.', () => {
        expect(view.selectors).toBeDefined();
    });

    describe('method:resolve', () => {
        Async.it('should fail to resolve.', () => {
            Sites.prototype.fetch.and.callFake(() => Q.reject('DatabaseError'));

            let request = view.resolve();

            expect(request).toBePromise();

            return request.then(() => fail('Method resolve should have been rejected.'))
                .catch(e => expect(e).toBe('DatabaseError'));
        });

        Async.it('should resolve.', () => {
            let request = view.resolve();

            expect(request).toBePromise();

            return request.then(() => {
                expect(view.site).toEqual(jasmine.any(Site));
                expect(view.unlockCode).toBeDefined();
            });
        });
    });

    describe('method:render', () => {
        Async.it('should fail to render.', () => {
            spyOn(view, 'buildHTML').and.callFake(() => Q.reject('DOMError'));

            let request = view.render();

            expect(request).toBePromise();

            return request.then(() => fail('Method render should have been rejected.'))
                .catch(e => expect(e).toBe('DOMError'));
        });

        Async.it('should render.', () => {
            let request = view.render();

            expect(request).toBePromise();

            return request.then(() => {
                expect(view.$el.attr('id')).toBe('unlock-code-view');
            });
        });
    });

    describe('method:initForm', () => {
        it('should stub out submit on the form.', () => {
            spyOn($.fn, 'submit').and.stub();

            view.initForm();

            expect($.fn.submit).toHaveBeenCalledWith(jasmine.any(Function));
        });
    });

    describe('method:next', () => {
        it('should navigate to set time zone activation view', () => {
            spyOn(ELF, 'trigger').and.stub();

            view.$unlockCode.val('2562816413');
            view.next();

            expect(view.navigate).toHaveBeenCalledWith('setTimeZone');
        });

        it('should display an error banner.', () => {
            spyOn(Banner, 'show').and.stub();

            view.next();

            expect(Banner.show).toHaveBeenCalledWith({
                text : 'Invalid Unlock Code',
                type : 'error'
            });
        });
    });

    describe('method:back', () => {
        it('should navigate back to the language selection view.', () => {
            view.back();

            expect(view.navigate).toHaveBeenCalledWith('languageSelection');
        });
    });

    describe('method:validate', () => {
        it('should verify that next button gets enabled when correct unlock code is entered', () => {
            let spy = spyOn(view, 'enableButton');

            view.$unlockCode.val('2562816413');

            view.validate();

            expect(spy).toHaveBeenCalled();
        });

        it('should verify that next button is disabled when there is no unlock code entered', () => {
            let spy = spyOn(view, 'disableButton');

            view.validate();

            expect(spy).toHaveBeenCalled();
        });

        it('should verify that next button gets disabled when unlock code is wrong', () => {
            let spy = spyOn(view, 'disableButton');

            view.$unlockCode.val('12345678');

            view.validate();

            expect(spy).toHaveBeenCalled();
        });
    });
});
