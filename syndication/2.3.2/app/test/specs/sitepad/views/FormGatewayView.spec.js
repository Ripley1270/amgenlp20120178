import FormGatewayView from 'sitepad/views/FormGatewayView';

import * as lStorage from 'core/lStorage';
import Data from 'core/Data';
import ELF from 'core/ELF';
import { MessageRepo } from 'core/Notify';
import CurrentContext from 'core/CurrentContext';
import Session from 'core/classes/Session';
import ScheduleManager from 'core/classes/ScheduleManager';

import Site from 'core/models/Site';
import Visit from 'core/models/Visit';
import User from 'core/models/User';
import Subject from 'core/models/Subject';
import Questionnaire from 'core/models/Questionnaire';
import FormRow from 'sitepad/models/FormRow';

import Questionnaires from 'core/collections/Questionnaires';
import Visits from 'core/collections/Visits';
import Schedules from 'core/collections/Schedules';
import Sites from 'core/collections/Sites';
import UserReports from 'sitepad/collections/UserReports';
import UserVisits from 'sitepad/collections/UserVisits';

import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';

describe('FormGatewayView', () => {
    let view,
        removeTemplate;

    Async.beforeEach(() => {
        resetStudyDesign();

        LF.appName = 'SitePad App';
        LF.security = new Session();
        LF.security.activeUser = new User({
            id : 1,
            role: 'admin'
        });

        CurrentContext.init();
        CurrentContext().setContextByRole('admin');

        LF.schedule = new ScheduleManager();

        // jscs:disable requireArrowFunctions
        // Long hand functions used in this case to retain the context of this to the spied upon objects.
        spyOn(Sites.prototype, 'fetch').and.callFake(function () {
            this.add({
                id: 1,
                siteCode: '001',
                studyName: 'DemoStudy',
                hash: '0x12345'
            });

            return Q();
        });
        // jscs:enable requireArrowFunctions

        spyOn(UserReports.prototype, 'fetch').and.callFake(() => Q());

        spyOn(Subject.prototype, 'fetch').and.callFake(function () {
            this.set({
                id: 1,
                subject_id: '100-001',
                phase: 10,
                initials: 'SA',
                lastVisit: new Date()
            });

            return Q();
        });

        LF.StudyDesign.visits = new Visits([{
            id: 'visit1',
            displayName: 'VISIT_1',
            visitType: 'ordered',
            visitOrder: 1,
            forms: ['Daily_Diary', 'Meds'],
            delay: '0'
        }, {
            id: 'visit2',
            displayName: 'VISIT_2',
            visitType: 'ordered',
            visitOrder: 2,
            forms: ['Daily_Diary'],
            delay: '0'
        }]);

        LF.StudyDesign.questionnaires = new Questionnaires([{
            id: 'Daily_Diary',
            SU: 'Daily',
            displayName: 'DISPLAY_NAME',
            className: 'DAILY_DIARY',
            affidavit: 'CustomAffidavit',
            previousScreen: true,
            accessRoles: ['admin', 'subject'],
            screens: [
                'DAILY_DIARY_S_1'
            ]
        }, {
            id: 'Meds',
            SU: 'Meds',
            displayName: 'DISPLAY_NAME',
            className: 'medication',
            previousScreen: false,
            affidavit: 'DEFAULT',
            accessRoles: ['subject'],
            screens: [
                'MEDS_S_1',
                'MEDS_S_2'
            ]
        }]);

        LF.Schedule.schedulingFunctions.checkAlwaysAvailability = (schedule, completedQuestionnaires, value, context, callback) => {
            callback(true);
        };

        LF.StudyDesign.schedules = new Schedules([{
            id: 'Schedule_01',
            target: {
                objectType: 'questionnaire',
                id: 'Daily_Diary'
            },
            scheduleFunction: 'checkAlwaysAvailability',
            scheduleRoles: ['admin', 'subject']
        }, {
            id: 'Schedule_02',
            target: {
                objectType: 'questionnaire',
                id: 'Meds'
            },
            scheduleFunction: 'checkAlwaysAvailability'
        }]);

        let visit = LF.StudyDesign.visits.at(1);
        let subject = new Subject({
            id: 1,
            subject_id: '100-001',
            phase: 10,
            initials: 'SA',
            lastVisit: new Date()
        });

        removeTemplate = specHelpers.renderTemplateToDOM('sitepad/templates/form-gateway.ejs');
        view = new FormGatewayView({ subject, visit });

        spyOn(window.localStorage, 'setItem').and.stub();
        spyOn(window.localStorage, 'getItem').and.stub();
        spyOn(window.localStorage, 'removeItem').and.stub();
        spyOn(view, 'reload').and.stub();
        spyOn(view, 'navigate').and.stub();
        spyOn(view, 'i18n').and.callFake(value => Q(value));

        // We have to mock this out for the ScheduleManger.
        LF.router = { view: () => view };

        return view.resolve()
            .then(() => view.render());
    });

    afterEach(() => removeTemplate());

    it('should have an id.', () => {
        expect(view.id).toBe('begin-visit');
    });

    describe('method:select', () => {
        it('should enable the start and skip buttons.', () => {
            spyOn(view, 'enableButton').and.stub();

            let row = new FormRow({ status: 'available' });
            view.select(row);

            expect(view.selected).toEqual(jasmine.any(FormRow));
            expect(view.enableButton).toHaveBeenCalledWith('#start-form, #skip-form');
        });

        it('should disabled the start and skip buttons.', () => {
            spyOn(view, 'deselect').and.stub();

            let row = new FormRow({ status: 'unavailable' });
            view.select(row);

            expect(view.selected).toEqual(jasmine.any(FormRow));
            expect(view.deselect).toHaveBeenCalled();
        });
    });

    describe('method:deselect', () => {
        it('should disable the start and skip buttons.', () => {
            spyOn(view, 'disableButton');

            view.selected = LF.StudyDesign.questionnaires.at(1);

            view.deselect();

            expect(view.selected).toBeFalsy();
            expect(view.disableButton).toHaveBeenCalledWith('#start-form, #skip-form');
        });
    });

    describe('method:skip', () => {
        it('should be defined.', () => {
            expect(view.skip).toBeDefined();
        });
    });

    describe('method:resolve', () => {
        Async.it('should resolve.', () => {
            let request = view.resolve();

            expect(request).toBePromise();

            return request.then(() => {
                expect(view.site).toEqual(jasmine.any(Site));
            });
        });

        Async.it('should be rejected.', () => {
            Sites.prototype.fetch.and.callFake(() => Q.reject('DatabaseError'));

            let request = view.resolve();

            expect(request).toBePromise();

            return request.then(() => fail('Method resolve should have been rejected.'))
                .catch(e => expect(e).toBe('DatabaseError'));
        });
    });

    describe('method:render', () => {
        beforeEach(() => spyOn(view, 'renderTable').and.stub());

        Async.it('should render.', () => {
            let request = view.render();

            expect(request).toBePromise();

            return request.then(() => {
                expect(view.renderTable).toHaveBeenCalled();
            });
        });

        Async.it('should fail to render.', () => {
            spyOn(view, 'buildHTML').and.callFake(() => Q.reject('DOMError'));

            let request = view.render();

            expect(request).toBePromise();

            return request.then(() => fail('Method render should have been rejected.'))
                .catch(() => expect(view.renderTable).not.toHaveBeenCalled());
        });
    });

    describe('method:checkVisitStatus', () => {
        Async.it('should navigate to the visit gateway if visit status is complete.', () => {
            spyOn(MessageRepo, 'display').and.resolve();
            spyOn(UserVisits.prototype, 'fetch').and.callFake(function () {
                this.add({
                    visitId: view.visit.id,
                    subject_id: view.subject.get('id'),
                    subjectKrpt: view.subject.get('krpt'),
                    state: LF.VisitStates.COMPLETED,
                    dateModified: new Date()
                });
                return Q();
            });

            let request = view.checkVisitStatus();

            expect(request).toBePromise();

            return request.then(res => {
                expect(view.navigate).toHaveBeenCalledWith('visits/1', true, { visit: view.visit });
            });
        });

        Async.it('should navigate to the visit gateway (rejected).', () => {
            spyOn(UserVisits.prototype, 'fetch').and.callFake(function () {
                this.add({
                    visitId: view.visit.id,
                    subject_id: view.subject.get('id'),
                    subjectKrpt: view.subject.get('krpt'),
                    state: LF.VisitStates.COMPLETED,
                    dateModified: new Date()
                });
                return Q();
            });

            view.i18n.and.callFake(() => Q.reject());

            let request = view.checkVisitStatus();

            expect(request).toBePromise();

            return request.then(res => {
                expect(view.navigate).toHaveBeenCalledWith('visits/1', true, { visit: view.visit });
            });
        });
    });

    TRACE_MATRIX('DE16263').
    describe('method:start', () => {
        Async.it('should start the questionnaire.', () => {
            spyOn(ELF, 'trigger').and.callFake(() => Q({}));
            spyOn(view, 'openQuestionnaire').and.stub();

            view.selected = LF.StudyDesign.questionnaires.at(0);

            let request = view.start();

            expect(request).toBePromise();

            return request.then(() => {
                expect(ELF.trigger).toHaveBeenCalledWith('DASHBOARD:OpenQuestionnaire/Daily_Diary', {
                    questionnaire: view.selected
                }, view);
                expect(view.openQuestionnaire).toHaveBeenCalledWith('Daily_Diary');
            });
        });

        Async.it('should not start the questionnaire (preventDefault).', () => {
            spyOn(ELF, 'trigger').and.callFake(() => Q({ preventDefault: true}));
            spyOn(view, 'openQuestionnaire').and.stub();

            view.selected = LF.StudyDesign.questionnaires.at(0);

            let request = view.start();

            expect(request).toBePromise();

            return request.then(() => {
                expect(ELF.trigger).toHaveBeenCalledWith('DASHBOARD:OpenQuestionnaire/Daily_Diary', {
                    questionnaire : view.selected
                }, view);
                expect(view.openQuestionnaire).not.toHaveBeenCalledWith({ questionnaire_id: 'Daily_Diary' });
            });
        });

        Async.it('should not start the questionnaire (reject).', () => {
            spyOn(ELF, 'trigger').and.callFake(() => Q.reject('Error'));
            spyOn(view, 'openQuestionnaire').and.stub();

            view.selected = LF.StudyDesign.questionnaires.at(0);

            let request = view.start();

            expect(request).toBePromise();

            return request.then(() => fail('Method start should have been rejected.'))
                .catch(() => {
                    expect(view.openQuestionnaire).not.toHaveBeenCalled();
                });
        });
    });

    describe('method:openQuestionnaire', () => {
        Async.it('should open the questionnaire (w/params).', () => {
            let request = view.openQuestionnaire('Daily_Diary');

            expect(request).toBePromise();

            return request.then(() => {
                expect(view.navigate).toHaveBeenCalledWith('questionnaire/Daily_Diary', true, {
                    subject: view.subject,
                    visit: view.visit
                });
            });
        });

        Async.it('should open the questionnaire (w/o params).', () => {
            view.selected = LF.StudyDesign.questionnaires.at(0);
            let request = view.openQuestionnaire();

            expect(request).toBePromise();

            return request.then(() => {
                expect(view.navigate).toHaveBeenCalledWith('questionnaire/Daily_Diary', true, {
                    subject: view.subject,
                    visit: view.visit
                });
            });
        });
    });

    describe('method:back', () => {
        it('should navigate back to the visit gateway.', () => {
            view.back();

            expect(view.navigate).toHaveBeenCalledWith('visits/1', true, { visit: view.visit });
        });

        // TODO: US6599
        Async.xit('should navigate to the login view.', () => {
            //spyOn(ConfirmView.prototype, 'show').and.callFake(() => Q());
            LF.security.activeUser.set('role', 'subject');

            view.back();

            // .then()x2 to add an appropriate number of events to the queue...
            return Q().then().then().then(() => {
                expect(view.navigate).toHaveBeenCalledWith('login');
            });
        });
    });
});
