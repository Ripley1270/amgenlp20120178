/**
 * Created by dvukman on 2/16/2016.
 */
import SetPasswordView from 'sitepad/views/SetPasswordView.js';
import CurrentContext from 'core/CurrentContext';

import 'test/helpers/StudyDesign';

describe('SetPasswordView', () => {
    let view, user,
        template =
            '<div id="set-password-template">' +
                '<div id="content">' +
                    ' <input type="password" name="txtNewPassword" id="txtNewPassword" class="form-control" value=""/>' +
                    ' <input type="password" name="txtConfirmPassword" id="txtConfirmPassword" class="form-control" value=""/>' +
                '</div>' +
            '</div>';


    Async.beforeEach(() => {
        $('body').append(template);

        user = CurrentContext().get('users').add({
            id: 1,
            language: 'fr-CA',
            username: 'Justin Trudeau'
        });

        return user.save()
            .then(() => CurrentContext().setContextByUser(user))
            .then(() => {
                view = new SetPasswordView();
                spyOn(view, 'reload').and.callFake($.noop);
            })
            .then(() => view.resolve())
            .then(() => view.render());
    });

    afterEach(() => $('#set-password-template').remove());

    it('should have an id.', () => {
        expect(view.id).toBe('set-password');
    });

    describe('method:validate', () => {
        it('should verify that next button gets enabled when passwords match', () => {
            let spy = spyOn(view, 'inputSuccess');

            view.$('#txtNewPassword').val('1234');
            view.$('#txtConfirmPassword').val('1234');

            view.validatePasswords({preventDefault: $.noop});
            expect(spy).toHaveBeenCalled();
        });

        it('should verify that next button gets disabled when passwords match', () => {
            let spy = spyOn(view, 'inputError');

            view.$('#txtNewPassword').val('2211');
            view.$('#txtConfirmPassword').val('3344');

            view.validatePasswords({preventDefault: $.noop});
            expect(spy).toHaveBeenCalled();
        });
    });
});