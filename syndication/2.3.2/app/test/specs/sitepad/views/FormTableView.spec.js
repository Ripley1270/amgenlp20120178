
import ScheduleManager from 'core/classes/ScheduleManager';
import CurrentContext from 'core/CurrentContext';
import FormTableView from 'sitepad/views/FormTableView';
import FormRowView from 'sitepad/views/FormRowView';

import Subject from 'core/models/Subject';
import Visit from 'core/models/Visit';
import User from 'core/models/User';
import FormRow from 'sitepad/models/FormRow';

import Questionnaires from 'core/collections/Questionnaires';
import Schedules from 'core/collections/Schedules';
import UserReports from 'sitepad/collections/UserReports';
import FormRows from 'sitepad/collections/FormRows';

import { resetStudyDesign } from 'test/helpers/StudyDesign';

let rows =  [{
    header: 'ORDER',
    property: 'order'
}, {
    header: 'FORM_NAME',
    property: 'displayName'
}, {
    header: 'ASSIGNED_TO',
    property: 'assignedTo'
}, {
    header: 'STATUS',
    property: 'status'
}];

let translations = { };

class FormTableViewSuite {
    constructor () {
        this.beforeEach();
    }

    beforeEach () {
        Async.beforeEach(() => {
            resetStudyDesign();

            let subject = new Subject({
                id: 1,
                subject_id: '100-001',
                phase: 10,
                initials: 'SA',
                lastVisit: new Date()
            });

            let visit = new Visit({
                id: 'visit1',
                displayName: 'VISIT_1',
                visitType: 'ordered',
                visitOrder: 1,
                forms: ['Daily_Diary', 'Meds'],
                delay: '0'
            });

            this.view = new FormTableView({
                rows,
                translations,
                subject,
                visit
            });

            LF.security = { 
                activeUser: new User({
                    id: 1,
                    role: 'admin'
                })
            };

            CurrentContext.init();
            CurrentContext().setContextByRole('admin');

            LF.StudyDesign.questionnaires = new Questionnaires([{
                id: 'Daily_Diary',
                SU: 'Daily',
                displayName: 'DISPLAY_NAME',
                className: 'DAILY_DIARY',
                affidavit: 'CustomAffidavit',
                previousScreen: true,
                accessRoles: ['admin', 'subject'],
                screens: [
                    'DAILY_DIARY_S_1'
                ]
            }, {
                id: 'Meds',
                SU: 'Meds',
                displayName: 'DISPLAY_NAME',
                className: 'medication',
                previousScreen: false,
                affidavit: 'DEFAULT',
                accessRoles: ['subject'],
                screens: [
                    'MEDS_S_1',
                    'MEDS_S_2'
                ]
            }]);

            LF.StudyDesign.schedules = new Schedules([{
                id: 'Schedule_01',
                target: {
                    objectType: 'questionnaire',
                    id: 'Daily_Diary'
                },
                scheduleFunction: 'checkAlwaysAvailability',
                scheduleRoles: ['admin', 'subject']
            }, {
                id: 'Schedule_02',
                target: {
                    objectType: 'questionnaire',
                    id: 'Meds'
                },
                scheduleFunction: 'checkAlwaysAvailability'
            }]);

            LF.schedule = new ScheduleManager();
            LF.Schedule.schedulingFunctions.checkAlwaysAvailability = (schedule, completedQuestionnaires, value, context, callback) => {
                callback(true);
            };

            // We have to mock this out for the ScheduleManger.
            LF.router = { view: () => this.view };

            spyOn(UserReports, 'fetchCollection').and.callFake(() => {
                return Q(new UserReports());
            });

            return Q();
        });
    }

    testSubject () {
        describe('property:subject', () => {
            it('should have a subject property set.', () => {
                expect(this.view.subject).toEqual(jasmine.any(Subject));
                expect(this.view.subject.get('id')).toBe(1);
            });
        });
    }

    testVisit () {
        describe('property:visit', () => {
            it('should have a visit property set.', () => {
                expect(this.view.visit).toEqual(jasmine.any(Visit));
                expect(this.view.visit.get('id')).toBe('visit1');
            });
        });
    }

    testFormRows () {
        describe('property:formRows', () => {
            it('should have a collection of form rows.', () => {
                expect(this.view.collection).toEqual(jasmine.any(FormRows));
                expect(this.view.collection.size()).toBe(0);
            });
        });
    }

    testFetch () {
        describe('method:fetch', () => {
            Async.it('should resolve, but no fetch (visit w/o forms).', () => {
                this.view.visit.set('forms', null);

                let request = this.view.fetch();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(UserReports.fetchCollection).not.toHaveBeenCalled();
                });
            });

            Async.it('should resolve and render the forms.', () => {
                spyOn(this.view, 'renderRows').and.stub();

                let request = this.view.fetch();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.collection.size()).toBe(1);
                });
            });
        });

        Async.it('should fail to render the questionnaires.', () => {
            UserReports.fetchCollection.and.reject('DatabaseError');
            spyOn(this.view, 'renderRows').and.stub();
            let request = this.view.fetch();

            expect(request).toBePromise();

            return request.then(() => fail('Method renderQuestionnaires should have failed.'))
            .catch(e => {
                expect(e).toBe('DatabaseError');
                expect(this.view.renderRows).not.toHaveBeenCalled();
            });
        });
    }

    testCheckReportCompletion () {
        TRACE_MATRIX('US6566').
        describe('method:checkReportCompletion', () => {
            it('should return true (subject user).', () => {
                let result = this.view.checkReportCompletion();

                expect(result).toBe(true);
            });

            it('should return false.', () => {
                spyOn(User.prototype, 'isSubjectUser').and.returnValue(true)
                let result = this.view.checkReportCompletion();

                LF.schedule.availableSchedules = new Schedules();

                expect(result).toBe(false);
            });
        }); 
    }

    testRenderRow () {
        describe('method:renderRow', () => {
            Async.it('should render the row.', () => {
                spyOn(FormRowView.prototype, 'render').and.resolve();
                let model = new FormRow();

                let request = this.view.renderRow(model);

                expect(request).toBePromise();

                return request.then((row) => {
                    expect(row).toBeDefined();
                });
            });
        });
    }
}

describe('FormTableView', () => {
    let suite = new FormTableViewSuite();

    suite.testSubject();
    suite.testVisit();
    suite.testFormRows();
    suite.testFetch();
    suite.testCheckReportCompletion();
    suite.testRenderRow();
});