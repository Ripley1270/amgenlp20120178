import SettingsView from 'sitepad/views/SettingsView';

import Sites from 'core/collections/Sites';
import User from 'core/models/User';
import Site from 'core/models/Site';

import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';

describe('SettingsView', () => {
    let view,
        removeTemplate;

    Async.beforeEach(() => {
        resetStudyDesign();
        removeTemplate = specHelpers.renderTemplateToDOM('sitepad/templates/settings.ejs');

        // jscs:disable requireArrowFunctions
        // Long hand functions used in this case to retain the context of this to the spied upon objects.
        spyOn(Sites.prototype, 'fetch').and.callFake(function () {
            this.add({
                id: 1,
                siteCode: '001',
                studyName: 'DemoStudy',
                hash: '0x12345'
            });

            return Q();
        });
        // jscs:enable requireArrowFunctions

        LF.security = {
            activeUser: new User({
                id : 1,
                role: 'admin'
            })
        };

        view = new SettingsView();

        spyOn(view, 'navigate').and.stub();

        return view.resolve()
            .then(() => view.render());
    });

    afterEach(() => removeTemplate());

    it('should have an id.', () => {
        expect(view.id).toBe('settings-view');
    });

    describe('method:resolve', () => {
        Async.it('should fail to resolve.', () => {
            Sites.prototype.fetch.and.callFake(() => Q.reject('DatabaseError'));

            let request = view.resolve();

            expect(request).toBePromise();

            return request.then(() => fail('Method resolve should have been rejected.'))
                .catch(e => expect(e).toBe('DatabaseError'));
        });

        Async.it('should resolve.', () => {
            let request = view.resolve();

            expect(request).toBePromise();

            return request.then(() => {
                expect(view.site).toEqual(jasmine.any(Site));
            });
        });
    });

    describe('method:render', () => {
        Async.it('should fail to render the view.', () => {
            spyOn(view, 'buildHTML').and.callFake(() => Q.reject('DOMError'));

            let request = view.render();

            expect(request).toBePromise();

            return request.then(() => fail('Method render should have been rejected.'))
                .catch(e => expect(e).toBe('DOMError'));
        });

        Async.it('should render the view.', () => {
            let request = view.render();

            expect(request).toBePromise();

            return request.then(() => {
                expect(view.$el.html()).toBeDefined();
            });
        });
    });

    describe('method:sync', () => {
        it('should be defined.', () => {
            expect(view.sync).toBeDefined();
        });
    });

    describe('method:about', () => {
        it('should navigate to the device info view.', () => {
            view.about();

            expect(view.navigate).toHaveBeenCalledWith('about');
        });
    });
    describe('method:customerSupport', () => {
        it('should navigate to the customer support view.', () => {
            view.customerSupport();

            expect(view.navigate).toHaveBeenCalledWith('customer-support');
        });
    });
    describe('method:back', () => {
        it('should navigate to home view.', () => {
            view.back();

            expect(view.navigate).toHaveBeenCalledWith('home');
        });
    });
});
