import SetTimeZoneActivationView from 'sitepad/views/SetTimeZoneActivationView';

import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';
import Logger from 'core/Logger';

import SetTimeZoneViewSuite from '../../core/views/SetTimeZoneView.specBase';

let preventDefault = $.noop;

class SetTimeZoneActivationViewSuite extends SetTimeZoneViewSuite {
    /**
     * Invoked before each unit test.
     * @returns {Q.Promise<void>}
     */
    beforeEach () {
        resetStudyDesign();
        this.removeTemplate = specHelpers.renderTemplateToDOM('sitepad/templates/set-time-zone-activation.ejs');

        this.view = new SetTimeZoneActivationView();

        return specHelpers.initializeContent()
            .then(() => this.view.resolve())
            .then(() => this.view.render())
            .then(() => super.beforeEach());
    }

    /**
     * Execute all class unit tests and those of the parent class.
     * @param {Object} context Context used to annotate method parameters.
     */
    executeAll (context) {
        super.executeAll(context);

        let methods = Object.getOwnPropertyNames(SetTimeZoneActivationViewSuite.prototype);

        this.annotateAll(methods, context);
    }

    /**
     * Tests the back method of the SetTimeZoneActivationView.
     */
    testBack () {
        describe('method:back', () => {
            it('should navigate to the unlock code view.', () => {
                spyOn(this.view, 'navigate').and.stub();

                this.view.back({ preventDefault });

                expect(this.view.navigate).toHaveBeenCalledWith('unlockCode');
            });
        });
    }

    /**
     * Tests the chooseZone method of the SetTimeZoneActivationView.
     */
    testChooseZone () {
        describe('method:chooseZone', () => {
            beforeEach(() => {
                LF.Wrapper.platform = 'browser';
                LF.StudyDesign.browser = {
                    timeZoneOptions: [
                        { tzId: 'America/New_York', swId: 21 }
                    ]
                };
            });

            it('should enable next button.', () => {
                spyOn(this.view, 'enableButton');

                this.view.$selectTimeZone.val('America/New_York').trigger('change');

                expect(this.view.enableButton).toHaveBeenCalledWith(this.view.$next);
            });
        });
    }

    /**
     * Tests the setTimeZone method of the SetTimeZoneActivationView.
     */
    testSetTimeZoneChild () {
        describe('method:setTimeZone', () => {
            beforeEach(() => {
                LF.Wrapper.platform = 'browser';
                LF.StudyDesign.browser = {
                    timeZoneOptions: [
                        { tzId: 'America/New_York', swId: 21 }
                    ]
                };
            });

            Async.it('should cancel to use the devices Time Zone.', () => {
                spyOn(this.view, 'confirm').and.resolve(false);
                spyOn(Logger.prototype, 'operational').and.stub();

                this.view.currentTimeZone = {id: 'America/New_York', displayName: '(GMT-05:00) America/New_York'};
                this.view.$selectTimeZone.val('America/New_York').trigger('change');

                let request = this.view.setTimeZone({ preventDefault });

                expect(request).toBePromise();

                return request.then(() => {
                    expect(Logger.prototype.operational).toHaveBeenCalledWith('User canceled setting the time zone to: (GMT-05:00) America/New_York');
                });
            });

            Async.it('should trigger a rule if user choose to use the devices Time Zone.', () => {
                spyOn(this.view, 'confirm').and.resolve(true);

                spyOn(Logger.prototype, 'operational').and.stub();
                spyOn(ELF, 'trigger').and.stub();

                this.view.currentTimeZone = {id: 'America/New_York', displayName: '(GMT-05:00) America/New_York'};
                this.view.$selectTimeZone.val('America/New_York').trigger('change');

                let request = this.view.setTimeZone({ preventDefault });

                expect(request).toBePromise();

                return request.then(() => {
                    expect(Logger.prototype.operational).toHaveBeenCalledWith(`User used the device's Time zone: (GMT-05:00) America/New_York`);
                    expect(ELF.trigger).toHaveBeenCalledWith('SETTIMEZONE:Completed', {}, this.view);
                });

            });

            Async.it('should cancel to use the new Time Zone (new time zone selected).', () => {
                spyOn(this.view, 'confirm').and.resolve(false);
                spyOn(Logger.prototype, 'operational').and.stub();

                this.view.currentTimeZone = {id: 'SomeOtherZone', displayName: '(GMT-02:00) SomeOtherZone'};
                this.view.$selectTimeZone.val('America/New_York').trigger('change');

                let request = this.view.setTimeZone({ preventDefault });

                expect(request).toBePromise();

                return request.then(() => {
                    expect(Logger.prototype.operational).toHaveBeenCalledWith('User canceled setting the time zone to: (GMT-05:00) America/New_York');
                });
            });

            Async.it('should trigger a rule if user choose to use a new Time Zone (browser).', () => {
                spyOn(this.view, 'confirm').and.resolve(true);
                spyOn(ELF, 'trigger').and.stub();
                spyOn(LF.Wrapper, 'exec').and.callFake((params) => {
                    params.execWhenNotWrapped();
                });

                this.view.currentTimeZone = {id: 'SomeOtherZone', displayName: '(GMT-02:00) SomeOtherZone'};
                this.view.$selectTimeZone.val('America/New_York').trigger('change');

                let request = this.view.setTimeZone({ preventDefault });

                expect(request).toBePromise();

                return request.then(() => {
                    expect(ELF.trigger).toHaveBeenCalledWith('SETTIMEZONE:Completed', {}, this.view);
                });
            });

            Async.it('should fail to trigger a rule if user choose to use a new Time Zone (device).', () => {
                spyOn(this.view, 'confirm').and.resolve(true);
                spyOn(this.view, 'notify').and.resolve();
                spyOn(LF.Wrapper, 'exec').and.callFake((params) => {
                    params.execWhenWrapped();
                });
                spyOn(Logger.prototype, 'error').and.stub();
                window.plugin.TimeZoneUtil.setTimeZone.and.callFake((onSuccess, onError) => {
                    onError('setTimeZone failure');
                });

                this.view.currentTimeZone = {id: 'SomeOtherZone', displayName: '(GMT-02:00) SomeOtherZone'};
                this.view.$selectTimeZone.val('America/New_York').trigger('change');

                let request = this.view.setTimeZone({ preventDefault });

                expect(request).toBePromise();

                return request.then((res) => {
                    expect(Logger.prototype.error).toHaveBeenCalledWith('Failed to set the Time zone', 'setTimeZone failure');
                });
            });

            Async.it('should fail to restart the application, but still trigger a rule (device).', () => {
                spyOn(this.view, 'confirm').and.resolve(true);
                spyOn(ELF, 'trigger').and.stub();
                spyOn(LF.Wrapper, 'exec').and.callFake((params) => {
                    params.execWhenWrapped();
                });
                spyOn(Logger.prototype, 'operational').and.resolve();
                spyOn(Logger.prototype, 'error').and.resolve();
                window.plugin.TimeZoneUtil.setTimeZone.and.callFake(onSuccess => onSuccess());
                window.plugin.TimeZoneUtil.restartApplication.and.callFake((onSuccess, onError) => {
                    onError('restartApplication failure');
                });

                this.view.currentTimeZone = {id: 'SomeOtherZone', displayName: '(GMT-02:00) SomeOtherZone'};
                this.view.$selectTimeZone.val('America/New_York').trigger('change');

                let request = this.view.setTimeZone({ preventDefault });

                expect(request).toBePromise();

                return request.then(() => {
                    expect(ELF.trigger).toHaveBeenCalledWith('SETTIMEZONE:Completed', {}, this.view);
                    expect(Logger.prototype.error).toHaveBeenCalledWith('Failed to restart application on Time zone change.', 'restartApplication failure');
                });
            });

            Async.it('should trigger a rule if user choose to use a new Time Zone (device).', () => {
                spyOn(this.view, 'confirm').and.resolve(true);
                spyOn(ELF, 'trigger').and.resolve();
                spyOn(LF.Wrapper, 'exec').and.callFake((params) => {
                    params.execWhenWrapped();
                });
                spyOn(Logger.prototype, 'operational').and.resolve();
                spyOn(Logger.prototype, 'error').and.stub();
                window.plugin.TimeZoneUtil.setTimeZone.and.callFake(onSuccess => onSuccess());
                window.plugin.TimeZoneUtil.restartApplication.and.callFake(onSuccess => onSuccess());

                this.view.currentTimeZone = {id: 'SomeOtherZone', displayName: '(GMT-02:00) SomeOtherZone'};
                this.view.$selectTimeZone.val('America/New_York').trigger('change');

                let request = this.view.setTimeZone({ preventDefault });

                expect(request).toBePromise();

                return request.then(() => {
                    expect(ELF.trigger).toHaveBeenCalledWith('SETTIMEZONE:Completed', {}, this.view);
                    expect(Logger.prototype.error).not.toHaveBeenCalled();
                });
            });
        });
    }
}

TRACE_MATRIX('US5979').
describe('SetTimeZoneActivationView', () => {
    let suite = new SetTimeZoneActivationViewSuite();

    suite.executeAll({
        id: 'set-tz-activation-view',
        template: '#set-time-zone-activation-template',
        button: '#back',
        exclude: [
            // Should be tested individually
            'testProperty',
            'testAttribute',
            // PageView has no class name.
            'testClass',

            // This method doesn't apply to the child class due to DOM structure and configuration.
            'testChangeSelection',
            // This method is overidden by the child class and should not be tested.
            'testSetTimeZone',

            // DOM structure of view does not allow for these tests to pass.
            'testClearInputState',
            'testValidateInput',
            'testAddHelpText',
            'testRemoveHelpText',
            'testGetValidParent',
            'testInputSuccess',
            'testInputError',
            'testHideKeyboardOnEnter'
        ]
    });
});
