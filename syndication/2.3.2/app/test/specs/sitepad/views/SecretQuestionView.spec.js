import SecretQuestionView from 'sitepad/views/SecretQuestionView.js';
import * as helpers from 'test/helpers/SpecHelpers';
import Data from 'core/Data';
import User from 'core/models/User';
import Session from 'core/classes/Session';
import CurrentContext from 'core/CurrentContext';

describe('SecretQuestion', () => {
    let view, user,
        template = '<div id="secret-question-template">' +
            '<select id="secret_question"></select>' +
            '<input type="text" id="secret_answer" />' +
            '</div>';

    Async.beforeEach(() => {
        $('body').append(template);

        helpers.installDatabase();

        helpers.createSite();

        user = CurrentContext().get('users').add({
            id: 1,
            language: 'fr-CA',
            role: 'site',
            username: 'Justin Trudeau'
        });

        return user.save()
            .then(() => CurrentContext().setContextByUser(user))
            .then(() => {
                view = new SecretQuestionView();
            })
            .then(() => view.resolve())
            .then(() => view.render());
    });

    afterEach(() => {
        $('#secret-question-template').remove();
    });

    it('should have an id.', () => {
        expect(view.id).toBe('secret-question-page');
    });

    describe('method:back', () => {
        it('should navigate back to the password creation view', () => {
            let spy = spyOn(view, 'navigate');
            view.back({preventDefault: $.noop});
            expect(spy).toHaveBeenCalled();
        });
    });

    describe('method:validatePage', () => {
        it('should verify that next button gets enabled when correct secret question is entered', () => {
            let spy = spyOn(view, 'enableButton');

            view.$secretAnswer.val('aaa');

            view.validatePage({preventDefault: $.noop});

            expect(spy).toHaveBeenCalled();
        });

        it('should verify that next button is disabled when there is no secret question entered', () => {
            let spy = spyOn(view, 'disableButton');

            view.validatePage({preventDefault: $.noop});

            expect(spy).toHaveBeenCalled();
        });

        it('should verify that next button gets disabled when unlock code is only one character long', () => {
            let spy = spyOn(view, 'disableButton');

            view.$secretAnswer.val('a');

            view.validatePage({preventDefault: $.noop});

            expect(spy).toHaveBeenCalled();
        });
    });
});
