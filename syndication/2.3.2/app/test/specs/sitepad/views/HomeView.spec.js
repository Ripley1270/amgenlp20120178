import HomeView from 'sitepad/views/HomeView.js';

import User from 'core/models/User';
import Sites from 'core/collections/Sites';
import Transmissions from 'core/collections/Transmissions';
import Subject from 'core/models/Subject';
import * as lStorage from 'core/lStorage';
import * as helpers from 'test/helpers/SpecHelpers';
import Session from 'core/classes/Session';
import Data from 'core/Data';

import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';

describe('HomeView', () => {
    let view,
        removeTemplate,
        subject;

    LF.appName = 'SitePad App';
    Async.beforeEach(() => {
        resetStudyDesign();

        // jscs:disable requireArrowFunctions
        // Long hand functions used in these cases to retain the context of this to the spied upon objects.
        spyOn(Sites.prototype, 'fetch').and.callFake(function () {
            this.add({
                id: 1,
                siteCode: '001',
                studyName: 'DemoStudy',
                hash: '0x12345'
            });

            return Q();
        });

        spyOn(Transmissions.prototype, 'fetch').and.callFake(function () {
            this.add({ id: 1 });
            return Q();
        });


        spyOn(User.prototype, 'fetch').and.callFake(function(){
            return Q();
        });
        // jscs:enable requireArrowFunctions

        LF.security = new Session();
        LF.security.activeUser = new User({
            id : 1,
            role: 'admin'
        });

        removeTemplate = specHelpers.renderTemplateToDOM('sitepad/templates/home.ejs');

        subject = new Subject({
            id: 1,
            subject_number: '100-001',
            phase: 10,
            initials: 'SA',
            lastVisit: new Date()
        });

        view = new HomeView();

        spyOn(view, 'reload').and.stub();
        spyOn(view, 'navigate').and.stub();

        return view.resolve()
            .then(() => view.render());
    });

    afterEach(() => removeTemplate());

    it('should have an id.', () => {
        expect(view.id).toBe('home-view');
    });

    describe('method:render', () => {
        Async.it('should render the view.', () => {
            spyOn(view, 'enableButton').and.stub();

            view.getPendingReportCount = () => 3;

            let request = view.render();

            expect(request).toBePromise();

            return request;
        });

        Async.it('should fail to render the view.', () => {
            spyOn(view, 'buildHTML').and.callFake(() => Q.reject('DOMError'));

            let request = view.render();

            expect(request).toBePromise();

            return request
                .then(() => fail('Method render should have failed.'))
                .catch(e => {
                    expect(e).toBe('DOMError');
                });
        });
    });

    describe('method:logout', () => {
        it('should logout of the application.', () => {
            spyOn(LF.security, 'logout').and.stub();
            view.logout();

            expect(LF.security.logout).toHaveBeenCalledWith(true);
        });
    });

    describe('method:settings', () => {
        it('should navigate to the settings view.', () => {
            view.settings();

            expect(view.navigate).toHaveBeenCalledWith('settings');
        });
    });

    describe('method:patientVisit', () => {
        it('should navigate to the patient\'s visits.', () => {
            view.select(subject);
            view.patientVisit();

            expect(view.navigate).toHaveBeenCalledWith('visits/1');
        });
    });

    TRACE_MATRIX('DE15786').
    describe('method:transmit', () => {
        Async.it('should trigger a transmit event.', () => {
            spyOn(ELF, 'trigger').and.resolve({ preventDefault: false });
            spyOn(view, 'render').and.stub();
            spyOn(LF.security, 'restartSessionTimeOut').and.stub();

            return Q().then(() => {
                view.transmit()
                .done(() => {
                    expect(ELF.trigger).toHaveBeenCalledWith('HOME:Transmit', {}, view);
                    expect(view.render).toHaveBeenCalled();
                    expect(LF.security.restartSessionTimeOut).toHaveBeenCalled();
                });
            });
        });

        Async.it('should trigger a transmit event and not render.', () => {
            spyOn(view, 'triggerTransmitRule').and.resolve({ preventDefault: true });
            spyOn(view, 'render').and.stub();
            spyOn(LF.security, 'restartSessionTimeOut').and.stub();

            return Q().then(() => {
                view.transmit()
                .done(() => {
                    expect(ELF.trigger).toHaveBeenCalledWith('HOME:Transmit', {}, view);
                    expect(view.render).not.toHaveBeenCalled();
                    expect(LF.security.restartSessionTimeOut).not.toHaveBeenCalled();
                });
            });
        });

    });

    describe('method:editPatient', () => {
        it('should navigate to the edit patient view.', () => {
            view.select(subject);

            view.editPatient();

            expect(view.navigate).toHaveBeenCalledWith(`edit-patient/${view.selected.get('id')}`);
        });
    });

    describe('method:deactivate', () => {
        it('should navigate to the deactivate patient view.', () => {
            view.deactivate();

            expect(view.navigate).toHaveBeenCalledWith('deactivate-patient', true, view.selected);
        });
    });

    describe('method:siteUsers', () => {
        it('should navigate to the site users view.', () => {
            view.siteUsers();

            expect(view.navigate).toHaveBeenCalledWith('site-users');
        });
    });

    describe('method:addNewPatient', () => {
        it('should navigate to the add new patient view.', () => {
            view.addNewPatient();

            expect(view.navigate).toHaveBeenCalledWith('add-new-patient');
        });
    });

    describe('method:select', () => {
        it('should enable action buttons', () => {
            spyOn(view, 'enableButton').and.stub();

            view.select(subject);

            expect(view.enableButton).toHaveBeenCalled();
            expect(view.enableButton.calls.count()).toBe(4);

            expect(view.selected).toBeDefined();
            expect(view.selected.get('id')).toBe(1);
        });
    });

    describe('method:deselect', () => {
        it('should disable action buttons', () => {
            spyOn(view, 'disableButton').and.stub();

            view.deselect(subject);

            expect(view.disableButton).toHaveBeenCalled();
            expect(view.disableButton.calls.count()).toBe(4);

            expect(view.selected).toBeUndefined();
        });
    });
});
