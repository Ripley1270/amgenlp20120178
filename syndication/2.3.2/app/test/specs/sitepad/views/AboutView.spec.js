import AboutView from 'sitepad/views/AboutView';
import * as lStorage from 'core/lStorage';
import * as DateTimeUtil from 'core/DateTimeUtil';
import Sites from 'core/collections/Sites';

import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';

TRACE_MATRIX('US5955').
describe('AboutView', () => {
    let view,
        removeTemplate,
        na = 'N/A';

    beforeEach(() => {
        resetStudyDesign();
        removeTemplate = specHelpers.renderTemplateToDOM('sitepad/templates/about.ejs');

        spyOn(lStorage, 'getItem').and.callFake(key => {
            switch (key) {
                case 'serviceBase':
                    return 'https://pht-syn-study01.phtnetpro.com';
                case 'deviceId':
                    return '675';
                case 'IMEI':
                    return '990000862471854';
                case 'lastRefreshTime':
                    return '2016-12-02T13:09:01.050';
            }
        });

        spyOn(Sites, 'fetchCollection').and.resolve(new Sites({ siteCode: '0008' }));

        spyOn(DateTimeUtil, 'getLocalizedDate').and.callFake((dt, {}) => 'Friday, December 02, 2016 08:09');

        view = new AboutView();

        spyOn(view, 'getPendingReportCount').and.resolve(2);
    });

    afterEach(() => removeTemplate());

    it('should have an id.', () => {
        expect(view.id).toBe('about');
    });

    it('should navigate to settings view.', () => {
        spyOn(view, 'navigate').and.stub();
        view.back();

        expect(view.navigate).toHaveBeenCalledWith('settings');
    });

    describe('method:render', () => {
        it('should return a promise.', () => {
            let request = view.render();

            expect(request).toBePromise();
        });

        Async.it('should fail to render the view when getTemplateParameters fails.', () => {
            spyOn(view, 'getTemplateParameters').and.reject('Unexpected error');

            return view.render()
            .then(() => fail('Method render should have been rejected.'))
            .catch(e => expect(e).toBe('Unexpected error'));
        });

        Async.it('should fail to render the view when buildHTML fails.', () => {
            spyOn(view, 'buildHTML').and.reject('DOMError');

            return view.render()
            .then(() => fail('Method render should have been rejected.'))
            .catch(e => expect(e).toBe('DOMError'));
        });

        Async.it('should render the view.', () => {
            return view.render()
            .then(() => expect(view.$el.html()).toBeDefined());
        });

        Async.it('should call buildHTML with the same parameters from getTemplateParameters', () => {
            spyOn(view, 'buildHTML').and.stub();

            return view.getTemplateParameters()
            .tap(() => view.render())
            .then(parameters => expect(view.buildHTML).toHaveBeenCalledWith(parameters, true));
        });
    });

    describe('method:getTemplateParameters', () => {
        it('should return a promise.', () => {
            let request = view.getTemplateParameters();

            expect(request).toBePromise();
        });

        Async.it('should return rejected promise when getBatteryLevel fails.', () => {
            spyOn(LF.Wrapper.Utils, 'getBatteryLevel').and.callFake(() => {
                throw 'getBatteryLevel Error';
            });

            return view.getTemplateParameters()
            .then(() => fail('Method getTemplateParameters should have been rejected.'))
            .catch(e => expect(e).toBe('getBatteryLevel Error'));
        });

        Async.it('should return rejected promise when fetching Sites fails.', () => {
            Sites.fetchCollection.and.reject('DB Error');

            return view.getTemplateParameters()
            .then(() => fail('Method getTemplateParameters should have been rejected.'))
            .catch(e => expect(e).toBe('DB Error'));
        });

        Async.it('should return rejected promise when fetching the pending report count fails.', () => {
            view.getPendingReportCount.and.reject('DB Error');

            return view.getTemplateParameters()
            .then(() => fail('Method getTemplateParameters should have been rejected.'))
            .catch(e => expect(e).toBe('DB Error'));
        });

        Async.it('should return an object literal with all parameters required for the template.', () => {
            return view.getTemplateParameters()
            .then(parameters => {
                expect(parameters.studyVersion).toBeDefined();
                expect(parameters.studyURL).toBeDefined();
                expect(parameters.deviceId).toBeDefined();
                expect(parameters.deviceSerialNumber).toBeDefined();
                expect(parameters.site).toBeDefined();
                expect(parameters.protocol).toBeDefined();
                expect(parameters.sponsor).toBeDefined();
                expect(parameters.storedReports).toBeDefined();
                expect(parameters.productVersion).toBeDefined();
                expect(parameters.batteryLevel).toBeDefined();
                expect(parameters.lastRefreshTime).toBeDefined();
            });
        });

        Async.it('should return the correct studyVersion.', () => {
            return view.getTemplateParameters()
            .then(parameters => {
                expect(parameters.studyVersion).toEqual(LF.StudyDesign.studyVersion);
            });
        });

        Async.it('should return the correct studyURL.', () => {
            return view.getTemplateParameters()
            .then(parameters => {
                expect(parameters.studyURL).toEqual('https://pht-syn-study01.phtnetpro.com');
            });
        });

        Async.it('should return the correct studyURL if there is no value set in localStorage.', () => {
            lStorage.getItem.and.callFake(() => null);

            return view.getTemplateParameters()
            .then(parameters => {
                expect(parameters.studyURL).toEqual(LF.apiBase);
            });
        });

        Async.it('should return the correct deviceId.', () => {
            return view.getTemplateParameters()
            .then(parameters => {
                expect(parameters.deviceId).toEqual('675');
            });
        });

        Async.it('should return the correct deviceId if there is no value set in localStorage.', () => {
            lStorage.getItem.and.callFake(() => null);

            return view.getTemplateParameters()
            .then(parameters => {
                expect(parameters.deviceId).toEqual(na);
            });
        });

        Async.it('should return the correct deviceSerialNumber.', () => {
            return view.getTemplateParameters()
            .then(parameters => {
                expect(parameters.deviceSerialNumber).toEqual('990000862471854');
            });
        });

        Async.it('should return the correct deviceSerialNumber if there is no value set in localStorage.', () => {
            lStorage.getItem.and.callFake(() => null);

            return view.getTemplateParameters()
            .then(parameters => {
                expect(parameters.deviceSerialNumber).toEqual(na);
            });
        });

        Async.it('should return the correct site.', () => {
            return view.getTemplateParameters()
            .then(parameters => {
                expect(parameters.site).toEqual('0008');
            });
        });

        Async.it('should return the correct protocol.', () => {
            return view.getTemplateParameters()
            .then(parameters => {
                expect(parameters.protocol).toEqual('TestStudy');
            });
        });

        Async.it('should return the correct sponsor.', () => {
            return view.getTemplateParameters()
            .then(parameters => {
                expect(parameters.sponsor).toEqual(LF.StudyDesign.clientName);
            });
        });

        Async.it('should return the correct storedReports.', () => {
            return view.getTemplateParameters()
            .then(parameters => {
                expect(parameters.storedReports).toEqual(2);
            });
        });

        Async.it('should return the correct productVersion.', () => {
            return view.getTemplateParameters()
            .then(parameters => {
                expect(parameters.productVersion).toEqual(LF.coreVersion);
            });
        });

        Async.it('should return the correct batteryLevel.', () => {
            spyOn(LF.Wrapper.Utils, 'getBatteryLevel').and.callFake(callback => callback(80));

            return view.getTemplateParameters()
            .then(parameters => {
                expect(parameters.batteryLevel).toEqual(80);
            });
        });

        Async.it('should return the correct batteryLevel when batteryLevel is not available.', () => {
            spyOn(LF.Wrapper.Utils, 'getBatteryLevel').and.callFake(callback => callback(null));

            return view.getTemplateParameters()
            .then(parameters => {
                expect(parameters.batteryLevel).toEqual(na);
            });
        });

        Async.it('should return lastRefreshTime as localized date.', () => {
            return view.getTemplateParameters()
            .then(parameters => {
                expect(DateTimeUtil.getLocalizedDate).toHaveBeenCalledWith(new Date('2016-12-02T13:09:01.050Z'), { includeTime: true, useShortFormat: false });
                expect(parameters.lastRefreshTime).toEqual('Friday, December 02, 2016 08:09');
            });
        });

        Async.it('should return the correct lastRefreshTime when there is no value set in localStorage.', () => {
            lStorage.getItem.and.callFake(() => null);

            return view.getTemplateParameters()
            .then(parameters => {
                expect(parameters.lastRefreshTime).toEqual(na);
            });
        });
    });
});
