import * as DetectDevice from 'core/actions/detectDevice';
import 'core/Notify';

describe('DetectDevice', function () {

    it ('should validate the device. No specified devices argument.', function (done) {

        DetectDevice.detectDevice(null, function (res) {
            expect(res).toBeTruthy();
            done();
        });

    });

    it('should validate the device with only a name property.', function (done) {

        DetectDevice.detectDevice([
            {
                name : 'WebKit'
            }
        ], function (res) {
            expect(res).toBeTruthy();
            done();
        });

    });

    /*
     *  NOTE: non-validating device handling is currently passing.  Not sure
     *  what needs to be mocked, or if the action class has a bug. --bmj
     */
    xit('shouldn\'t validate the device.', function (done) {


        DetectDevice.detectDevice([
            {
                name    : 'LogPad'
            }
        ], function (res) {
            var response;
            response = !res;
            expect(response).toBeTruthy();
            done();
        });

    });

    it('should validate the device with a match property.', function (done) {

        DetectDevice.detectDevice([
            {
                name    : 'WebKit',
                match   : [
                    ['WebKit']
                ]
            }
        ], function (res) {
            expect(res).toBeTruthy();
            done();
        });

    });

    it('should validate the device with multiple match sets.', function (done) {

        DetectDevice.detectDevice([
            {
                name    : 'WebKit',
                match   : [
                    ['LogPad', 'LF'],
                    ['LogPad', 'APP'],
                    ['WebKit']
                ]
            }
        ], function (res) {
            expect(res).toBeTruthy();
            done();
        });

    });

    /*
     *  NOTE: non-validating device handling is currently passing.  Not sure
     *  what needs to be mocked, or if the action class has a bug. --bmj
     */
    xit('shouldn\'t validate the device.  No matches.', function (done) {


        LF.Actions.detectDevice([
            {
                name    : 'WebKit',
                match   : [['LogPad', 'APP']]
            }
        ], function (res) {
                var response;
                response = !res;
                expect(response).toBeTruthy();
                done();
        });

    });

});

