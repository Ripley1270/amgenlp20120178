import ELF from 'core/ELF';
import LastDiary from 'core/models/LastDiary';
import LastDiaries from 'core/collections/LastDiaries';
import { handleLastDiariesResult } from 'core/actions/handleLastDiariesResult';
import Logger from 'core/Logger';

// Needed while running only this spec file
import 'core/DateTimeUtil';

TRACE_MATRIX('US6572').
describe('handleLastDiariesResult', () => {
    let collection = new LastDiaries(),
        syncData = {
            lastStartedDate: '2016-03-01T09:03:22+01:00',
            lastCompletedDate: '2016-03-01T08:03:27Z', 
            deleted: '0',
            krpt: 'NP.2d5f1486e843498fb3ca4ba29dd4eee8',
            questionnaire_id: 'P_Daily_Diary',
            role: JSON.stringify(['Subect']),
            updated: '2016-03-01T08:03:27Z'
        },
        doSync = (syncResult) => {
            return Q.promise((resolve) => {
                handleLastDiariesResult({ res: syncResult, collection }, () => {
                    resolve();
                });
            });
        };

    beforeAll(() => {
        // fake the save method of LastDiary model to prevent db operations
        spyOn(LastDiary.prototype, 'save').and.callFake(function (model, params) {
            this.set(model);
            collection.add(this);
            params.onSuccess();
        });

        // fake the destroy method of LastDiary model to prevent db operations
        spyOn(LastDiary.prototype, 'destroy').and.callFake(function (params) {
            collection.remove(this);
            params.onSuccess();
        });

        spyOn(Logger.prototype, 'error');
    });

    beforeEach(() => {
        collection.reset();
    });

    describe('Save received lastDiarySync Data when there is no previous data', () => {
        Async.it('should save LastDiary', () => {
            return doSync([syncData])
                .then(() => {
                    let model = collection.at(0);

                    expect(model.get('lastStartedDate')).toBe(syncData.lastStartedDate);
                    expect(model.get('lastCompletedDate')).toBe(syncData.lastCompletedDate);
                    expect(model.get('krpt')).toBe(syncData.krpt);
                    expect(model.get('questionnaire_id')).toBe(syncData.questionnaire_id);
                    expect(model.get('role')).toBe(syncData.role);
                    expect(model.get('updated')).toBe(syncData.updated);
                });
        });

        Async.it('should save LastDiary after filtering additional fields', () => {
            let data = _.clone(syncData);

            _.extend(data, { unexpected: 'unexpectedValue', shouldNotBeHere: 9999 });

            return doSync([data])
                .then(() => {
                    let model = collection.at(0);

                    expect(model.get('lastStartedDate')).toBe(syncData.lastStartedDate);
                    expect(model.get('lastCompletedDate')).toBe(syncData.lastCompletedDate);
                    expect(model.get('krpt')).toBe(syncData.krpt);
                    expect(model.get('questionnaire_id')).toBe(syncData.questionnaire_id);
                    expect(model.get('role')).toBe(syncData.role);
                    expect(model.get('updated')).toBe(syncData.updated);
                    expect(model.get('unexpected')).not.toBeDefined();
                    expect(model.get('shouldNotBeHere')).not.toBeDefined();
                });
        });
    
        Async.it('should log an error if deleted is not a number', () => {
            let data = _.clone(syncData);

            _.extend(data, { deleted: 'Not a number' });

            return doSync([data])
                .then(() => {
                    expect(collection.size()).toBe(0);
                    expect(Logger.prototype.error).toHaveBeenCalledWith('Incorrect flag type: Not a number value of deleted flag is not a number.');
                });
        });

        Async.it('should not save LastDiary when deleted is 1', () => {
            let data = _.clone(syncData);
            
            _.extend(data, { deleted: '1' });

            return doSync([data])
                .then(() => {
                    expect(collection.size()).toBe(0);
                });
        });
    });

    describe('Save received lastDiarySync Data when there is previous data', () => {
        let syncData2 = {
            lastStartedDate: '2016-03-01T10:03:22+01:00',
            lastCompletedDate: '2016-03-01T09:03:27Z', 
            deleted: '0',
            krpt: 'NP.2d5f1486e843498fb3ca4ba29dd4eee8',
            questionnaire_id: 'P_Daily_Diary',
            role: JSON.stringify(['Subect', 'Site']),
            updated: '2016-03-01T09:03:27Z'
        };

        Async.beforeEach(() => {
            return doSync([syncData]);
        });

        Async.it('should update existing diary when sync data has a bigger lastStartedDate even if it has a different lastCompletedDate', () => {
            let data = _.clone(syncData2);
            _.extend(data, { lastCompletedDate: '2016-03-01T10:03:27Z' });

            return doSync([data])
                .then(() => {
                    let model = collection.at(0);

                    expect(model.get('lastStartedDate')).toBe(data.lastStartedDate);
                    expect(model.get('lastCompletedDate')).toBe(data.lastCompletedDate);
                    expect(model.get('krpt')).toBe(data.krpt);
                    expect(model.get('questionnaire_id')).toBe(data.questionnaire_id);
                    expect(model.get('role')).toBe(data.role);
                    expect(model.get('updated')).toBe(data.updated);
                }); 
        });

        Async.it('should update existing diary when sync data has the same lastCompletedDate even if it has a smaller(or equal) lastStartedDate', () => {
            let data = _.clone(syncData2);
            _.extend(data, { 
                lastCompletedDate: '2016-03-01T08:03:27Z',
                lastStartedDate: '2016-03-01T8:03:22+01:00' 
            });

            return doSync([data])
                .then(() => {
                    let model = collection.at(0);

                    expect(model.get('lastStartedDate')).toBe(data.lastStartedDate);
                    expect(model.get('lastCompletedDate')).toBe(data.lastCompletedDate);
                    expect(model.get('krpt')).toBe(data.krpt);
                    expect(model.get('questionnaire_id')).toBe(data.questionnaire_id);
                    expect(model.get('role')).toBe(data.role);
                    expect(model.get('updated')).toBe(data.updated);
                })
                .then(() => {
                    _.extend(data, {
                        lastCompletedDate: '2016-03-01T08:03:27Z',
                        lastStartedDate: '2016-03-01T9:03:22+01:00'
                    });
                    return doSync([data]);
                })
                .then(() => {
                    let model = collection.at(0);

                    expect(model.get('lastStartedDate')).toBe(data.lastStartedDate);
                    expect(model.get('lastCompletedDate')).toBe(data.lastCompletedDate);
                    expect(model.get('krpt')).toBe(data.krpt);
                    expect(model.get('questionnaire_id')).toBe(data.questionnaire_id);
                    expect(model.get('role')).toBe(data.role);
                    expect(model.get('updated')).toBe(data.updated);
                }); 
        });

        Async.it('should not update existing diary when sync data has a different lastCompletedDate and a smaller(or equal) lastStartedDate', () => {
            let data = _.clone(syncData2);
            _.extend(data, { 
                lastCompletedDate: '2016-03-01T15:03:27Z',
                lastStartedDate: '2016-03-01T8:03:22+01:00' 
            });

            return doSync([data])
                .then(() => {
                    let model = collection.at(0);

                    expect(model.get('lastStartedDate')).toBe(syncData.lastStartedDate);
                    expect(model.get('lastCompletedDate')).toBe(syncData.lastCompletedDate);
                    expect(model.get('krpt')).toBe(syncData.krpt);
                    expect(model.get('questionnaire_id')).toBe(syncData.questionnaire_id);
                    expect(model.get('role')).toBe(syncData.role);
                    expect(model.get('updated')).toBe(syncData.updated);
                })
                .then(() => {
                    _.extend(data, {
                        lastCompletedDate: '2016-03-01T15:03:27Z',
                        lastStartedDate: '2016-03-01T9:03:22+01:00'
                    });
                    return doSync([data]);
                })
                .then(() => {
                    let model = collection.at(0);

                    expect(model.get('lastStartedDate')).toBe(syncData.lastStartedDate);
                    expect(model.get('lastCompletedDate')).toBe(syncData.lastCompletedDate);
                    expect(model.get('krpt')).toBe(syncData.krpt);
                    expect(model.get('questionnaire_id')).toBe(syncData.questionnaire_id);
                    expect(model.get('role')).toBe(syncData.role);
                    expect(model.get('updated')).toBe(syncData.updated);
                }); 
        });

        Async.it('should not save "role" and "updated" when sync data has a smaller updated value', () => {
           let data = _.clone(syncData2);
            _.extend(data, { updated: '2016-03-01T07:03:27Z' });

            return doSync([data])
                .then(() => {
                    let model = collection.at(0);
                    expect(model.get('role')).toBe(syncData.role);
                    expect(model.get('updated')).toBe(syncData.updated);
                });
        });

        Async.it('should add as a new diary when questionnaire_id value is different', () => {
           let data = _.clone(syncData2);
            _.extend(data, { questionnaire_id: 'Meds' });

            return doSync([data])
                .then(() => {
                    expect(collection.size()).toBe(2);
                }); 
        });

        Async.it('should add as a new diary when krpt value is different', () => {
           let data = _.clone(syncData2);
            _.extend(data, { krpt: 'NP.ffffffffffffffffffffffffffffffff' });

            return doSync([data])
                .then(() => {
                    expect(collection.size()).toBe(2);
                }); 
        });

        Async.it('should delete existing diary when sync data has the same lastCompletedDate and deleted is 1', () => {
           let data = _.clone(syncData);
            _.extend(data, { deleted: '1' });

            return doSync([data])
                .then(() => {
                    expect(collection.size()).toBe(0);
                }); 
        });

        Async.it('should not delete existing diary when sync data has a different lastCompletedDate and deleted is 1', () => {
           let data = _.clone(syncData);
            _.extend(data, { deleted: '1', lastCompletedDate: '2016-03-01T09:03:27Z'});

            return doSync([data])
                .then(() => {
                    expect(collection.size()).toBe(1);
                }); 
        });
    });
});