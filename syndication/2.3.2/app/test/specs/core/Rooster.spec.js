import ELF from 'core/ELF';

class TestMethods {
    blowIt (data) {
        return Q.reject('21');
    }

    sayIt (data) {
        if (data) {
            data.response = 'response';
            if (data.timesCalled == 21) {
                return Q.reject('21');
            } else if (data.timesCalled == 11) {
                data.preventDefault = true;
            } else {
                data.timesCalled++;
            }
        }
        return Q();
    }
}


describe('ELF.listen', () => {
    let methods;
    //let ELF; // comment out

    beforeAll(() => {
        //ELF = new Rooster('test');  // Comment this out when including real ELF
        methods = new TestMethods();
    });

    beforeEach(() => {
        ELF.rules.clear();
    });

    afterAll(() => {
        ELF.rules.clear();
    });

    Async.it('Listen and trigger', () => {
        spyOn(methods, 'sayIt').and.callThrough();
        ELF.listen('EVENT_TrivialTest_I', methods.sayIt);
        return ELF.trigger('EVENT_TrivialTest_I')
            .then(()=> {
                expect(methods.sayIt).toHaveBeenCalled();
            });
    });

    Async.it('Listen and trigger via listen-array', () => {
        spyOn(methods, 'sayIt').and.callThrough();
        ELF.listen(['EVENT_TrivialTest_I'], methods.sayIt);
        return ELF.trigger('EVENT_TrivialTest_I')
            .then(()=> {
                expect(methods.sayIt).toHaveBeenCalled();
            });
    });

    Async.it('Listen and trigger-wrong-trigger', () => {
        spyOn(methods, 'sayIt').and.callThrough();
        ELF.listen('EVENT_TrivialTest_II', methods.sayIt);
        return ELF.trigger('EVENT_TrivialTest_II_not')
            .then(()=> {
                expect(methods.sayIt).not.toHaveBeenCalled();
            });
    });

    // This is significant to Rooster API, but ELF doesn't have it
    Async.xit('Listen and trigger w/data IN-and-OUT', () => {
        spyOn(methods, 'sayIt').and.callThrough();
        ELF.listen('EVENT_TrivialTest_I', methods.sayIt);
        return ELF.trigger('EVENT_TrivialTest_I',{msg:'hi'})
            .then((obj)=> {
                expect(methods.sayIt).toHaveBeenCalled();
                expect(obj.response).toBe('response');
            });
    });

    Async.it('Three-actions-in-a-row', () => {
        spyOn(methods, 'sayIt').and.callThrough();
        ELF.listen('EVENT_TrivialTest_I', [methods.sayIt, methods.sayIt, methods.sayIt]);
        return ELF.trigger('EVENT_TrivialTest_I',{timesCalled:0})
            .then((obj)=> {
                expect(methods.sayIt).toHaveBeenCalled();
                expect(methods.sayIt.calls.count()).toBe(3);
                //expect(obj.timesCalled).toBe(3);
            });
    });

    Async.it('Three-actions-in-a-row preventDefault on 2nd', () => {
        spyOn(methods, 'sayIt').and.callThrough();
        ELF.listen('EVENT_TrivialTest_I', [methods.sayIt, methods.sayIt, methods.sayIt]);
        return ELF.trigger('EVENT_TrivialTest_I',{timesCalled:10})
            .then((obj)=> {
                expect(methods.sayIt).toHaveBeenCalled();
                expect(methods.sayIt.calls.count()).toBe(3);
                //expect(obj.timesCalled).toBe(11);
            });
    });

    Async.xit('TwoTriggers', () => {
        spyOn(methods, 'sayIt').and.callThrough();
        ELF.listen(['EVENT_TrivialTest_I', 'EVENT_TrivialTest_I'], methods.sayIt);
        return ELF.trigger('EVENT_TrivialTest_I',{timesCalled:0})
            .then((obj)=> {
                expect(methods.sayIt).toHaveBeenCalled();
                expect(methods.sayIt.calls.count()).toBe(2);
                //expect(obj.timesCalled).toBe(2);
            });
    });

    Async.xit('Array-of-arrays', () => {
        spyOn(methods, 'sayIt').and.callThrough();
        ELF.listen(
            ['EVENT_TrivialTest_I', 'EVENT_TrivialTest_I', 'EVENT_TrivialTest_I'],
            [methods.sayIt, methods.sayIt, methods.sayIt]);
        return ELF.trigger('EVENT_TrivialTest_I',{timesCalled:100})
            .then((obj)=> {
                expect(methods.sayIt).toHaveBeenCalled();
                expect(methods.sayIt.calls.count()).toBe(100 + 3 + 3);
                //expect(obj.timesCalled).toBe(100 + 3 * 3);
            });
    });

    Async.it('Three-actions-in-a-row reject on 2nd', () => {
        spyOn(methods, 'sayIt').and.callThrough();
        spyOn(methods, 'blowIt').and.callThrough();
        ELF.listen('EVENT_TrivialTest_I', [methods.sayIt, methods.blowIt, methods.sayIt]);
        return ELF.trigger('EVENT_TrivialTest_I',{timesCalled:20})
            .then((obj)=> {
                expect(methods.sayIt).toHaveBeenCalled();
                expect(methods.sayIt.calls.count()).toBe(1);
                expect(methods.blowIt).toHaveBeenCalled();
                expect(methods.blowIt.calls.count()).toBe(1);
                //expect(obj.timesCalled).toBe(21);
            });
    });

    Async.xit('Named-function-by-string', () => {
        spyOn(methods, 'sayIt').and.callThrough();
        let action = jasmine.createSpy('action', (dataObject, resume) => {
            dataObject.timesCalled++;
            resume();
        });
        ELF.action('action', action);

        ELF.listen('EVENT_TrivialTest_I', 'action');
        return ELF.trigger('EVENT_TrivialTest_I', {timesCalled:0})
            .then((obj)=> {
                expect(action).toHaveBeenCalled();
                expect(action.calls.count()).toBe(1);
                //expect(obj.timesCalled).toBe(1);
            });
    });
});
