import DABL from 'core/dataaccess/DABL';

xdescribe('DABL', function () {
    let _plugins = _(DABL.plugins).clone();

    beforeEach(() => {
        DABL.plugins = { };
    });

    afterEach(() => DABL.plugins = _plugins);

    describe('Constructor', () => {

        it('should throw an error. DABL: No plugins found.', () => {
            expect(() => new DABL()).toThrow(new Error('DABL: No plugins found.'));
        });

        it('should throw an error. DABL: No plugins supported.', () => {
            DABL.plugin('WebSQL', $.noop, () => false);

            expect(() => new DABL()).toThrow(new Error('DABL: No supported plugins found.'));
        });

        it('should fallback to the next supported plugin.', () => {
            DABL.plugin('IndexedDB', $.noop, () => true);

            let store = new DABL({ prefer : 'Foo' });

            expect(store.plugin).toEqual('IndexedDB');
        });

        it('should fallback to the next supported plugin.', () => {
            DABL.plugin('WebSQL', $.noop, () => false);
            DABL.plugin('IndexedDB', $.noop, () => true);

            let store = new DABL({ prefer : 'WebSQL' });

            expect(store.plugin).toEqual('IndexedDB');
        });

        it('should select the supported plug-in.', () => {
            DABL.plugin('WebSQL', $.noop, () => true);

            let store = new DABL();

            expect(store.plugin).toEqual('WebSQL');
        });

        it('should use the selected plugin.', () => {
            DABL.plugin('WebSQLite3', $.noop, () => true);

            let store = new DABL({ prefer : 'WebSQLite3' });

            expect(store.plugin).toEqual('WebSQLite3');
        });

        it('should select the plug-in by priority.', () => {
            DABL.plugin('WebSQL', $.noop, () => false);
            DABL.plugin('IndexedDB', $.noop, () => true);
            DABL.plugin('LocalStorage', $.noop, () => false);

            let store = new DABL({ prefer : ['LocalStorage', 'IndexedDB', 'WebSQL'] });

            expect(store.plugin).toEqual('IndexedDB');
        });

        it('should fail to find the plug-in by priority and fallback to the next supported.', () => {
            DABL.plugin('WebSQL', $.noop, () => false);
            DABL.plugin('IndexedDB', $.noop, () => false);
            DABL.plugin('LocalStorage', $.noop, () => true);

            let store = new DABL({ prefer : ['WebSQL', 'IndexedDB'] });

            expect(store.plugin).toEqual('LocalStorage');
        });

    });

    describe('RecordSet', () => {

        it('should return the top 3 records.', () => {
            let records = new DABL.RecordSet([
                    { id : 1 },
                    { id : 2 },
                    { id : 3 },
                    { id : 4 },
                    { id : 5 }
                ]),
                result = records.top(3);

           expect(result.length).toEqual(3);
           expect(result[0].id).toEqual(5);
           expect(result[1].id).toEqual(4);
           expect(result[2].id).toEqual(3);
        });

        it('should return the top 5 records.', () => {
            let records = new DABL.RecordSet([
                    { id : 1 },
                    { id : 2 },
                    { id : 3 },
                    { id : 4 },
                    { id : 5 }
                ]),
                result = records.top(6);

            expect(result.length).toEqual(5);
            expect(result[0].id).toEqual(5);
            expect(result[1].id).toEqual(4);
            expect(result[2].id).toEqual(3);
        });

        it('should return the bottom 3 records.', () => {
            let records = new DABL.RecordSet([
                    { id : 1 },
                    { id : 2 },
                    { id : 3 },
                    { id : 4 },
                    { id : 5 }
                ]),
                result = records.bottom(3);

            expect(result.length).toEqual(3);
            expect(result[0].id).toEqual(1);
            expect(result[1].id).toEqual(2);
            expect(result[2].id).toEqual(3);
        });

        it('should return the correct record.', () => {
            let records = new DABL.RecordSet([
                    { id : 1 },
                    { id : 2 },
                    { id : 3 },
                    { id : 4 },
                    { id : 5 }
                ]),
                result = records.where({ id : 2 });

            expect(result[0].id).toEqual(2);
        });

        it('should return the correct records.', () => {
            let records = new DABL.RecordSet([
                    { id : 1, value : 1 },
                    { id : 2, value : 2 },
                    { id : 3, value : 2 }
                ]),
                result = records.where({ value : 2 });

            expect(result.length).toEqual(2);
            expect(result[0].value).toEqual(2);
            expect(result[1].value).toEqual(2);
        });

        it('should find the correct average.', () => {
            let records = new DABL.RecordSet([
                    { id : 1, value : 1 },
                    { id : 2, value : 2 },
                    { id : 3, value : 10 }
                ]),
                result = records.avg('value');

            expect(result).toEqual((10 + 2 + 1) / 3);
        });

        it('should find the minimum value.', () => {
            let records = new DABL.RecordSet([
                    { id : 1, value : 1 },
                    { id : 2, value : 2 },
                    { id : 3, value : 10 }
                ]),
                result = records.minValue('value');

            expect(result).toEqual(1);
        });

        it('should find the maximum value.', () => {
            let records = new DABL.RecordSet([
                    { id : 1, value : 1 },
                    { id : 2, value : 2 },
                    { id : 3, value : 10 }
                ]),
                result = records.maxValue('value');

            expect(result).toEqual(10);
        });

        it('should order the records in ascending order by value.', () => {
            let records = new DABL.RecordSet([
                    { id : 1, value : 1 },
                    { id : 2, value : 10 },
                    { id : 3, value : 5 }
                ]),
                result = records.orderBy('value');

            expect(result.at(0).value).toEqual(1);
            expect(result.at(1).value).toEqual(5);
            expect(result.at(2).value).toEqual(10);
        });

        it('should order the records in descending order by value.', () => {
            let records = new DABL.RecordSet([
                    { id : 1, value : 1 },
                    { id : 2, value : 10 },
                    { id : 3, value : 5 }
                ]),
                result = records.orderBy('value', 'DESC');

            expect(result.at(0).value).toEqual(10);
            expect(result.at(1).value).toEqual(5);
            expect(result.at(2).value).toEqual(1);
        });

    });

});
