import WebService from 'core/classes/WebService';
import getServerTime from 'core/transmit/getServerTime';
import Logger from 'core/Logger';

describe('getServerTime', () => {
    Async.it('should fail to get the server time.', () => {
        spyOn(WebService.prototype, 'getServerTime').and.reject('Server Error');

        let request = getServerTime();

        expect(request).toBePromise();

        return request.then(() => fail('Method getServerTime should have been rejected.'))
        .catch(err => {
            expect(err).toBe('Server Error');
        });
    });

    Async.it('should return false.', () => {
        spyOn(WebService.prototype, 'getServerTime').and.resolve({ });
        spyOn(Logger.prototype, 'error').and.stub();

        let request = getServerTime();

        expect(request).toBePromise();

        return request.then(res => {
            expect(res).toBe(false);
            expect(Logger.prototype.error).toHaveBeenCalledWith('Unable to establish server date and time');
        });
    });

    Async.it('should return the UTC time.', () => {
        let utc = new Date().toString();

        spyOn(WebService.prototype, 'getServerTime').and.resolve({ utc });

        let request = getServerTime();

        expect(request).toBePromise();

        return request.then(res => {
            expect(_.isDate(res)).toBe(true);
        });
    });
});
