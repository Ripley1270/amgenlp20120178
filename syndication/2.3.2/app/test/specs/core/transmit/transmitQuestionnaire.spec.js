import Dashboards from 'core/collections/Dashboards';
import Answers from 'core/collections/Answers';
import Transmissions from 'core/collections/Transmissions';
import WebService from 'core/classes/WebService';
import transmitQuestionnaire from 'core/transmit/transmitQuestionnaire';

TRACE_MATRIX('US7392').
describe('transmitQuestionnaire', () => {
    let answer = {
        id: 1,
        subject_id: '12345',
        response: '7',
        SW_Alias: 'EQ5D_Q_1',
        instance_ordinal: 1,
        questionnaire_id: 'EQ5D',
        question_id: 'EQ5D_Q_1'
    };

    beforeEach(() => {
        let now = new Date();

        // jscs:disable requireArrowFunctions
        // Long hand functions used in these cases to retain the context of this to the spied upon objects.
        spyOn(Transmissions.prototype, 'fetch').and.callFake(function () {
            this.add([{
                id: 1,
                method: 'transmitQuestionnaire',
                params: JSON.stringify({ dashboardId: 1, sigId: 'LA.14f7031e9cc352136068949490' }),
                created: now.getTime()
            }]);

            return Q();
        });

        spyOn(Dashboards.prototype, 'destroy').and.resolve();
        spyOn(Dashboards.prototype, 'fetch').and.callFake(function (params) {
            this.add([{
                id: 1,
                subject_id: '12345',
                instance_ordinal: 1,
                questionnaire_id: 'EQ5D',
                completed: now.ISOStamp(),
                diary_id: now.getTime(),
                completed_tz_offset: now.getOffset()
            }]);

            params.onSuccess();
            return Q();
        });
        // jscs:enable requireArrowFunctions
    
        spyOn(WebService.prototype, 'sendDiary').and.callFake((diary, auth, jsonh, onSuccess, onError) => {
            onSuccess(null, {
                syncID: null,
                isSubjectActive: 1,
                isDuplicate: false
            });
        });
    });

    Async.it('should not send T parameter if there is no type set for any answer.', () => {
        spyOn(Answers.prototype, 'fetch').and.callFake(function (params) {
            this.add([answer]);

            params.onSuccess();
            return Q();
        });

        return Transmissions.fetchCollection()
        .then((transmissions) => Q.Promise(resolve => {
            transmitQuestionnaire.call(transmissions, transmissions.at(0), () => {
                let diaryPayload = WebService.prototype.sendDiary.arguments[0];

                expect(diaryPayload.A[0].T).not.toBeDefined();
                resolve();
            });
        }));
    });

    Async.it('should not use JSONH compression send T parameter if there is exists type value set for any answer.', () => {
        spyOn(Answers.prototype, 'fetch').and.callFake(function (params) {
            this.add([_.extend(answer, { type: 'typeValue' })]);

            params.onSuccess();
            return Q();
        });

        return Transmissions.fetchCollection()
        .then((transmissions) => Q.Promise(resolve => {
            transmitQuestionnaire.call(transmissions, transmissions.at(0), () => {
                let diaryPayload = WebService.prototype.sendDiary.arguments[0],
                    jsonhParameter = WebService.prototype.sendDiary.arguments[2];

                expect(typeof diaryPayload.A[0]).toEqual('object');
                expect(jsonhParameter).toEqual(false);
                resolve();
            });
        }));
    });

    Async.it('should send T parameter if there is exists type value set for any answer.', () => {
        spyOn(Answers.prototype, 'fetch').and.callFake(function (params) {
            this.add([_.extend(answer, { type: 'typeValue' })]);

            params.onSuccess();
            return Q();
        });

        return Transmissions.fetchCollection()
        .then((transmissions) => Q.Promise(resolve => {
            transmitQuestionnaire.call(transmissions, transmissions.at(0), () => {
                let diaryPayload = WebService.prototype.sendDiary.arguments[0];

                expect(diaryPayload.A[0].T).toEqual('typeValue');
                resolve();
            });
        }));
    });
});