import DABL from 'core/dataaccess/DABL';
import 'core/dataaccess/WebSQL';
import 'core/dataaccess/IndexedDB';

new DABL({
    name   : 'junk',
    prefer : 'WebSQL',
    debug  : false,
    showSql: false
});

new DABL({
    name   : 'jjunkk',
    prefer : 'IndexedDB',
    debug  : false,
    showSql: false
});

xdescribe('DABL Plugins', () => {

    let testWithPlugin;
    testWithPlugin = (plugin, enable) => {

        (enable ? describe : xdescribe)(`${plugin} Plugin ${enable ? '' : ' (Not supported by browser)'}`, () => {
            // Important: do NOT move anything up and outside this innermost
            // scope, or you will end up with this kind of thing:
            //  DEBUG LOG: 'WebSQL:', 'New instance: IndexedDB_Test'
            // and spend hours debugging.
            /**
             * @type {{Books:object}}
             */
            let store;

            beforeAll((done) => {
                store = new DABL({
                    name: `${plugin}_Test`,
                    prefer: plugin,
                    debug: false,
                    showSql: false
                });
                store.collection('Books')
                    .then(() => store.Books.clear())
                    .catch((e) => fail(e))
                    .done(done);
            });

            afterAll((done) => {
                store.drop()
                    .catch(e => {
                        console.error(e);
                        fail(e); // probably not really going to fail anything in afterAll.
                    })
                    .done(done);
            });

            it(`should create a new DABL/${plugin} object.`, () => {
                expect(store.plugin).toEqual(plugin);
                expect(store.name).toEqual(`${plugin}_Test`);
            });

            it('should save the record.', (done) => {
                store.Books.save({
                        title: 'The Eye of the World',
                        author: 'Robert Jordan',
                        genre: 'Fantasy',
                        rating: 5
                    })
                    .then((id) => store.Books.get(id))
                    .then((book) => {
                        expect(book.id).toBeGreaterThan(0);
                        expect(book.title).toEqual('The Eye of the World');
                        expect(book.author).toEqual('Robert Jordan');
                        expect(book.genre).toEqual('Fantasy');
                        expect(book.rating).toEqual(5);
                    })
                    .catch(e => fail(e))
                    .done(done);
            });

            it('should add a new collection and save the record.', (done) => {
                store.collection('Authors')
                    .tap(() => expect(store.Authors).toBeDefined())

                    .then(() => store.Authors.clear())

                    .then(() => store.Authors.save({
                        firstName: 'Brandon',
                        lastName: 'Sanderson'
                    }))

                    .then(id => store.Authors.get(id))
                    .tap((author) => {
                        expect(author.id).toBeGreaterThan(0);
                        expect(author.firstName).toEqual('Brandon');
                        expect(author.lastName).toEqual('Sanderson');
                    })

                    .catch(e => fail(e))
                    .done(done);
            });

            it('should modify the record before saving.', (done) => {
                let altStore = new DABL({
                    name: 'AltStore',
                    prefer: plugin,
                    debug: false,
                    showSql: false
                });

                altStore.collection('Books', {
                        before: (data) => {
                            data.modified = new Date().getTime();

                            return data;
                        }
                    })

                    .then(() => altStore.Books.save({
                        id: 1,
                        title: 'The Eye of the World',
                        author: 'Robert Jordan',
                        genre: 'Fantasy',
                        rating: 5
                    }))
                    .then((id) => altStore.Books.get(id))
                    .then((book) => {
                        expect(book.id).toBeGreaterThan(0);
                        expect(book.title).toEqual('The Eye of the World');
                        expect(book.author).toEqual('Robert Jordan');
                        expect(book.genre).toEqual('Fantasy');
                        expect(book.rating).toEqual(5);
                        expect(book.modified).toBeDefined();
                    })
                    .catch(e => fail(e))
                    .done(done);
            });

            it('should save the record.', (done) => {
                store.Books.save({
                        title: 'The Eye of the World',
                        author: 'Robert Jordan',
                        genre: 'Fantasy',
                        rating: 5
                    })
                    .then((id) => store.Books.get(id))
                    .then((book) => {
                        expect(book.id).toBeGreaterThan(0);
                        expect(book.title).toEqual('The Eye of the World');
                        expect(book.author).toEqual('Robert Jordan');
                        expect(book.genre).toEqual('Fantasy');
                        expect(book.rating).toEqual(5);
                    })
                    .catch(e => fail(e))
                    .done(done);
            });

            it('should update the record.', (done) => {
                store.Books.save({
                        id: 1,
                        title: 'The Eye of the World',
                        author: 'Robert Jordan',
                        genre: 'Epic Fantasy',
                        rating: 5
                    })
                    .then(store.Books.all)
                    .then((res) => {
                        let book = res.at(0);

                        expect(book.id).toBeGreaterThan(0);
                        expect(book.title).toEqual('The Eye of the World');
                        expect(book.author).toEqual('Robert Jordan');
                        expect(book.genre).toEqual('Epic Fantasy');
                        expect(book.rating).toEqual(5);
                    })
                    .catch(e => fail(e))
                    .done(done);
            });

            it('should save an array of records.', (done) => {
                store.Books.save([
                        {
                            id: 3,
                            title: 'Ender\'s Game',
                            author: 'Orson Scott Card',
                            genre: 'Science Fiction',
                            rating: 5
                        }, {
                            id: 2,
                            title: 'Towers of Midnight',
                            author: ['Robert Jordan', 'Brandon Sanderson'],
                            genre: 'Epic Fantasy',
                            rating: 4
                        }
                    ])
                    .then((res) => {
                        expect(res[0].id).toEqual(3);
                        expect(res[0].error).toBeUndefined();
                        expect(res[1].id).toEqual(2);
                        expect(res[1].error).toBeUndefined();

                    })
                    .catch(e => fail(e))
                    .done(done);
            });

            xit('should fetch all records in the correct order.', (done) => {
                store.Books.all().then((res) => {
                    expect(res.size()).toEqual(3);
                    expect(res.at(1).id).toEqual(2);
                    expect(res.at(1).title).toEqual('Towers of Midnight');
                    expect(res.at(2).id).toEqual(3);
                    expect(res.at(2).title).toEqual('Ender\'s Game');
                    done();
                });
            });

            it('find the minimum book.', (done) => {
                store.Books.all()
                    .then((res) => expect(res.minValue('rating')).toEqual(4))
                    .catch(e => fail(e))
                    .done(done);
            });

            it('should find the record', (done) => {
                store.Books.query({title: 'Towers of Midnight'}).then((res) => {
                    let book = res.at(0);

                    expect(book.title).toEqual('Towers of Midnight');
                }).catch(e => fail(e))
                    .done(done);
            });

            it('should delete the record.', (done) => {
                let origCount;
                store.Books.count()
                .then((res) => origCount = res )
                .then(() => store.Books.remove(1) )
                    .tap((res) => expect(res).toBeDefined())
                    .then(() => store.Books.count())
                    .then((res) => expect(res).toEqual(origCount - 1))
                    .catch(e => fail(e))
                    .done(done);
            });

            it('should clear the storage object.', (done) => {
                store.Books.clear()
                    .then(() => store.Books.count())
                    .then((res) => expect(res).toBe(0))
                    .catch(e => fail(e))
                    .done(done);
            });

            Async.it('should save all the records without locking the database.', () => {
                let books = [];

                // DE15826 - Decreased the number of records being saved.
                // 200 is excessive when the point of the test is to verify the database doesn't
                // lock up when saves happen "concurrently".
                for (let i = 0; i < 10; i += 1) {
                    books.push({
                        title: 'The Eye of the World',
                        author: 'Robert Jordan',
                        genre: 'Fantasy',
                        rating: 5
                    });
                }

                let requestA = store.Books.save(books);

                let requestB = store.Books.save({
                    title: 'The Eye of the World',
                    author: 'Robert Jordan',
                    genre: 'Fantasy',
                    rating: 5
                });

                // DE15826 - I've added all transactions to a promise to ensure
                // the test case doesn't complete without first saving all records.
                return Q.all([requestA, requestB])
                .spread((responseA, responseB) => {
                    expect(responseA).toBeDefined();
                    expect(responseB).toBeDefined();
                });
            }, 20000);

            it('should get the count of the storage object.', (done) => {
                store.Books.save({
                        id: 1,
                        title: 'The Eye of the World',
                        author: 'Robert Jordan',
                        genre: 'Fantasy',
                        rating: 5
                    })
                    .then(store.Books.count())
                    .tap((res) => expect(res).toEqual(1))
                    .catch(e => fail(e))
                    .done(done);
            });

            // This operation is not implemented
            xit('should drop an individual table', (done) => {

                store.collection('DropThisCollection');

                store.DropThisCollection.save({one: 1})
                    .tap(id => {
                        expect(id).toBe(1);
                    })

                    .then(() => store.DropThisCollection.drop())

                    .then(res => store.DropThisCollection.all())
                    .tap(res => expect(store.DropThisCollection).toBeUndefined())

                    .catch(e => fail(e))
                    .done(done);
            });

            //instead, we have:
            it('cannot drop an indvidual table', (done) => {
                store.collection('DropThisCollection')
                    .then(() => store.DropThisCollection.drop())
                    .then(res => fail(`Should not have succeeded; got ${res}`))
                    .catch(e => {
                        expect(e).toMatch(/unimplemented/);
                    })
                    .done(done);
            });

            it('should handle special characters', (done) => {

                let data = Object.freeze({
                    title: `tick' dquote" amp& percent% comma, period. dollar$ gt> lt< bang! quest?`,
                    author: 'slash/ backtick` tilde~ at@ hash# star& closep) openp( backslash\\',
                    genre: '\u2358 \u235e', // quote symbols
                    extra: 'gt&gt; pct%20',
                    rating: 5
                });

                store.Books.save(data)
                    .then((id) => store.Books.get(id))
                    .then((res) => {
                        expect(res).toEqual(jasmine.objectContaining(data));
                    })
                    .catch(e => fail(e))
                    .done(done);
            });

            it('should drop the database.', (done) => {

                let store2 = new DABL({
                    name: 'DropDBTestDB',
                    prefer: plugin,
                    debug: false
                });
                store2.collection('SomeCollection');

                store2.drop()
                    .then(() => store2.count())
                    .then(n => fail('should not have been able to count a dropped db'))
                    .catch(e => {}) // error message will vary by plugin

                    // Make sure main store was not affected
                    .then(() => store.Books.save({x: 1})) // main store, NOT store2!
                    .tap(id => expect(id).toBeGreaterThan(0))

                    //jscs:disable maximumLineLength
                    .catch((e = '') => fail(`(Perhaps) while trying to access main store after dropping other store: ${e.message}`))
                    //jscs:enable maximumLineLength
                    .done(done);
            });
        });
    };

    // Define TWO separate and independent specs; each is either enabled or disabled
    // // as applicable for the browser we're running in
    // testWithPlugin('WebSQL', !!window.openDatabase); // enable if browser supports WebSQL
    // testWithPlugin('IndexedDB', !!window.indexedDB); // enable if browser supports IndexedDB
});
