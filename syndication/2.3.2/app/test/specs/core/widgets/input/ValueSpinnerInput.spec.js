const {Model} = Backbone;

import {ValueSpinnerInputOptions} from 'core/widgets/input/ValueSpinnerInput';
import ValueSpinnerInput from 'core/widgets/input/ValueSpinnerInput';
import BaseSpinnerInput from 'core/widgets/input/BaseSpinnerInput';

/*global describe, beforeEach, afterEach, beforeAll, afterAll, it, spyOn */

describe('ValueSpinnerInput', () => {
    let options,
        spinnerInput,
        $parent,
        oldTemplateSettings;

    beforeAll(() => {
        // Set the correct interpolation settings for templates.
        oldTemplateSettings = _.templateSettings;
        _.templateSettings = {
            evaluate: /\{\[([\s\S]+?)]}/g,
            interpolate: /\{\{(.+?)}}/g
        };
    });

    afterAll(() => {
        _.templateSettings = oldTemplateSettings;
    });

    beforeEach(() => {
        $parent = $('<div class="spinner-container">');

        document.documentElement.appendChild($parent[0]);

        let defaultItems = [];
        for (let i = 0; i <= 99; ++i) {
            defaultItems[i] = {
                value: i + 1,
                text: `value ${i + 1}`
            }
        }

        options = new ValueSpinnerInputOptions({
            items: defaultItems
        });

        options = _.extend(options, {
            parent: $parent[0],
            itemTemplate: 'DEFAULT:NumberItemTemplate',
            template: LF.templates.display('DEFAULT:NumberSpinnerControl'),
            deceleration: 0.003,
            numItems: 5,
            model: new Model()
        });
    });

    afterEach(() => {
        if (spinnerInput) {
            spinnerInput.remove();
            spinnerInput = null;
        }
        if ($parent) {
            $parent.remove();
            $parent = null;
        }
    });

    describe('.ValueSpinnerInputOptions class', () => {
        it('can be constructed, and contains properties from options', () => {
            expect(options instanceof ValueSpinnerInputOptions).toBe(true);
            expect(options.items).toEqual(options.items);
        });
    });

    describe('Tests before rendering', () => {

        describe('#constructor', () => {
            it('can be constructed as a BaseSpinnerInput with values in our model', () => {
                spinnerInput = new ValueSpinnerInput(options);
                expect(spinnerInput instanceof BaseSpinnerInput).toBe(true);
                expect(spinnerInput.model.get('items')).toEqual(options.items);
            });
        });

        describe('#itemDisplayValueFunction', () => {
            it('returns text of item that has the value passed in', () => {
                let spinnerInput = new ValueSpinnerInput(options),
                    value = 4;
                expect(spinnerInput.itemDisplayValueFunction(value)).toBe('value 4');
            });
            it('returns value unaltered if value is not found', () => {
                let spinnerInput = new ValueSpinnerInput(options),
                    value = -1;
                expect(spinnerInput.itemDisplayValueFunction(value)).toBe(-1);
            });
        });
    });

    describe('Tests after rendering', () => {
        beforeEach((done) => {
            spinnerInput = new ValueSpinnerInput(options);
            spinnerInput.render().then(() => {
                done();
            });
        });
        describe('#setValues', () => {
            it('sets all values from min/max by step intervals', () => {
                spyOn(spinnerInput, 'itemValueFunction').and.callFake((val) => {
                    return `value:${val}`;
                });

                spyOn(spinnerInput, 'itemDisplayValueFunction').and.callFake((val) => {
                    return `display value:${val}`;
                });

                // Clear items that were entered during render.
                spinnerInput.$items.remove();
                spinnerInput.clearItemCache();
                spinnerInput.setValues();

                // verify dataset begins with a blank value
                expect($(spinnerInput.$items[0]).data('value')).toBe('');

                // verify we have the default items set up.
                expect(spinnerInput.model.get('items').length).toBe(100);

                // iterate our values in a for loop and make sure they appended in the right order,
                //  utilizing the value and display value functions
                for (let i = 1; i <= spinnerInput.model.get('items').length - 1; ++i) {
                    let $item = $(spinnerInput.$items[i]);
                    expect($item.data('value')).toBe(`value:${i}`);
                    expect($item.html()).toBe(`display value:${i}`);
                }
            });
        });
    });
});