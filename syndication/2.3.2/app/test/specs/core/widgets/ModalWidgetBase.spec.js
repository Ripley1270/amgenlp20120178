import * as helpers from 'test/helpers/SpecHelpers';
import 'test/helpers/StudyDesign';

import Questionnaires from 'core/collections/Questionnaires';
import Answers from 'core/collections/Answers';
import Question from 'core/models/Question';
import Dashboard from 'core/models/Dashboard';
import QuestionnaireView from 'core/views/QuestionnaireView';
import QuestionView from 'core/views/QuestionView';
import Widget from 'core/models/Widget';
import NumberSpinner from 'core/widgets/NumberSpinner';
import User from 'core/models/User';
import Session from 'core/classes/Session';
import Subject from 'core/models/Subject';
import WidgetBaseTests from './WidgetBase.specBase';
import Templates from 'core/resources/Templates';
import Languages from 'core/collections/Languages';
import NumberSpinnerInput from 'core/widgets/input/NumberSpinnerInput';
import ModalWidgetBase from 'core/widgets/ModalWidgetBase';
import TextBox from 'core/widgets/TextBox';

const DEFAULT_WRAPPER_TEMPLATE = 'DEFAULT:FormGroup',
    DEFAULT_CLASS_NAME = 'Modal',
    DEFAULT_LABEL_TEMPLATE = 'DEFAULT:Label',
    DEFAULT_INPUT_TEMPLATE = 'DEFAULT:TextBox',
    DEFAULT_LABELS = {};

/*global describe, it, beforeEach, afterEach, beforeAll, afterAll, spyOn */
export default class ModalWidgetBaseTests extends WidgetBaseTests {
    get testValue1 () {
        return 'test1';
    }

    get displayValue1 () {
        return this.testValue1;
    }

    get testValue2 () {
        return 'test2';
    }

    get displayValue2 () {
        return this.testValue2;
    }

    constructor (model) {
        if (!(model instanceof Widget)) {
            throw 'Invalid use of ModalWidgetBaseTests.  Must be tested with an implementation of the abstract class.';
        }
        super(model);
    }

    execTests () {
        super.execTests();

        TRACE_MATRIX('US6106').
        describe('ModalWidgetBase', () => {
            let templates,
                dummyParent,
                security,
                modalWidget = null;

            beforeAll(() => {
                security = LF.security;
                LF.security = this.getDummySecurity();
                templates = LF.templates;
            });

            afterAll(() => {
                LF.security = security;
                LF.templates = templates;
            });

            beforeEach(() => {
                dummyParent = this.getMinDummyQuestion();
            });

            afterEach(() => {
                $(`#${dummyParent.getQuestionnaire().id}`).remove();

                // Remove the backdrop in case it was open while we exited.
                $('.modal-backdrop').remove();
                modalWidget = null;
            });

            describe('Base Tests', () => {
                let testValue,
                    baseWidget;
                describe('#getters', () => {
                    describe('#defaultModalTemplate', () => {
                        it('throws an error if unimplemented by instance', () => {
                            baseWidget = new ModalWidgetBase({
                                model: this.model,
                                mandatory: false,
                                parent: dummyParent
                            });
                            expect(() => {
                                testValue = baseWidget.defaultModalTemplate;
                            }).toThrow(new Error('Unimplemented defaultModalTemplate getter in a ModalWidgetBase implementation.'));
                        });
                    });
                    describe('#defaultClassName', () => {
                        it('returns correct default value', () => {
                            baseWidget = new ModalWidgetBase({
                                model: this.model,
                                mandatory: false,
                                parent: dummyParent
                            });
                            expect(baseWidget.defaultClassName).toBe(DEFAULT_CLASS_NAME);
                        });
                    });
                    describe('#defaultWrapperTemplate', () => {
                        it('returns correct default value', () => {
                            baseWidget = new ModalWidgetBase({
                                model: this.model,
                                mandatory: false,
                                parent: dummyParent
                            });
                            expect(baseWidget.defaultWrapperTemplate).toBe(DEFAULT_WRAPPER_TEMPLATE);
                        });
                    });
                    describe('#defaultLabelTemplate', () => {
                        it('returns correct default value', () => {
                            baseWidget = new ModalWidgetBase({
                                model: this.model,
                                mandatory: false,
                                parent: dummyParent
                            });
                            expect(baseWidget.defaultLabelTemplate).toBe(DEFAULT_LABEL_TEMPLATE);
                        });
                    });
                    describe('#defaultInputTemplate', () => {
                        it('returns correct default value', () => {
                            baseWidget = new ModalWidgetBase({
                                model: this.model,
                                mandatory: false,
                                parent: dummyParent
                            });
                            expect(baseWidget.defaultInputTemplate).toBe(DEFAULT_INPUT_TEMPLATE);
                        });
                    });
                    describe('#defaultLabels', () => {
                        it('returns correct default value', () => {
                            baseWidget = new ModalWidgetBase({
                                model: this.model,
                                mandatory: false,
                                parent: dummyParent
                            });
                            expect(baseWidget.defaultLabels).toEqual(DEFAULT_LABELS);
                        });
                    });
                });
                
                describe('#getModalValuesString()', () => {
                    it('throws an error from base', () => {
                        baseWidget = new ModalWidgetBase({
                            model: this.model,
                            mandatory: false,
                            parent: dummyParent
                        });
                        expect(() => {
                            testValue = baseWidget.getModalValuesString();
                        }).toThrow(new Error('Unimplemented getModalValuesString() method in a ModalWidgetBase implementation.'));
                    });
                });
                
                describe('#dialogOpened()', () => {
                    it('throws an error from base', () => {
                        baseWidget = new ModalWidgetBase({
                            model: this.model,
                            mandatory: false,
                            parent: dummyParent
                        });
                        expect(() => {
                            testValue = baseWidget.getModalValuesString();
                        }).toThrow(new Error('Unimplemented getModalValuesString() method in a ModalWidgetBase implementation.'));
                    });
                });

                describe('#dialogOpened', () => {
                    it ('returns undefined if called from a jQuery event (finishes promise).', () => {
                        baseWidget = new ModalWidgetBase({
                            model: this.model,
                            mandatory: false,
                            parent: dummyParent
                        });
                        let e = new $.Event();
                        let ret = baseWidget.dialogOpened(e);
                        expect(ret).toBe(undefined);
                    });

                    Async.it ('returns a promise (only requirement from base.  actual implementation can be handled by instance).', () => {
                        baseWidget = new ModalWidgetBase({
                            model: this.model,
                            mandatory: false,
                            parent: dummyParent
                        });
                        let p = baseWidget.dialogOpened();
                        return p.then(() => {
                            expect(Q.isPromise(p)).toBe(true);
                        });
                    });
                });

            });

            describe('#constructor', () => {
                it('creates object of type ModalWidgetBase, with everything initialized', () => {
                    modalWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: false,
                        parent: dummyParent
                    });

                    expect(modalWidget instanceof ModalWidgetBase).toBe(true);
                    expect(modalWidget.$modal).toBe(null);
                    expect(modalWidget.events['focus input[type=tel], input[type=text]']).toBe('openDialog');
                });
            });

            describe('#delegateEvents', () => {
                it('calls super delegateEvents, then removes and adds custom events', () => {
                    modalWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: false,
                        parent: dummyParent
                    });

                    spyOn(modalWidget, 'removeCustomEvents');
                    spyOn(modalWidget, 'addCustomEvents');

                    modalWidget.delegateEvents();

                    // removeCustomEvents is called by undelegate, called by Backbone super.
                    expect(modalWidget.removeCustomEvents.calls.count()).toBe(1);
                    expect(modalWidget.addCustomEvents.calls.count()).toBe(1);
                });
            });

            describe('#undelegateEvents', () => {
                it('calls super undelegateEvents, then removes custom events', () => {
                    modalWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: false,
                        parent: dummyParent
                    });

                    // probably undefined... temporarily mock a function to verify super is called
                    let oldUndelegateEvents = TextBox.prototype.undelegateEvents;
                    TextBox.prototype.undelegateEvents = () => {
                    };

                    spyOn(TextBox.prototype, 'undelegateEvents');
                    spyOn(modalWidget, 'removeCustomEvents');

                    modalWidget.undelegateEvents();

                    expect(modalWidget.removeCustomEvents.calls.count()).toBe(1);
                    expect(TextBox.prototype.undelegateEvents.calls.count()).toBe(1);

                    TextBox.prototype.undelegateEvents = oldUndelegateEvents;
                });
            });

            describe('#addCustomEvents', () => {
                Async.it('adds event handlers to modal dialog', () => {
                    modalWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: false,
                        parent: dummyParent
                    });
                    return modalWidget.render().then(() => {
                        spyOn(modalWidget.$modal, 'on');
                        spyOn(modalWidget.$input, 'on');
                        modalWidget.addCustomEvents();
                        expect(modalWidget.$modal.on.calls.count()).toBe(1);
                        expect(modalWidget.$input.on.calls.count()).toBe(1);
                        expect(modalWidget.$modal.on)
                            .toHaveBeenCalledWith('hide.bs.modal', modalWidget.dialogClosing);
                        expect(modalWidget.$input.on)
                            .toHaveBeenCalledWith('remove', modalWidget.destroy);
                    });
                });
            });

            describe('#removeCustomEvents', () => {
                Async.it('removes event handlers from modal dialog', () => {
                    modalWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: false,
                        parent: dummyParent
                    });
                    return modalWidget.render().then(() => {
                        spyOn(modalWidget.$modal, 'off');
                        spyOn(modalWidget.$input, 'off');
                        modalWidget.removeCustomEvents();
                        expect(modalWidget.$modal.off.calls.count()).toBe(1);
                        expect(modalWidget.$input.off.calls.count()).toBe(1);
                        expect(modalWidget.$modal.off)
                            .toHaveBeenCalledWith('hide.bs.modal', modalWidget.dialogClosing);
                        expect(modalWidget.$input.off)
                            .toHaveBeenCalledWith('remove', modalWidget.destroy);
                    });
                });
            });

            describe('#getModalValuesString', () => {
                it('throws an exception if unimplemented in class', () => {
                    let baseWidget = new ModalWidgetBase({
                            model: this.model,
                            mandatory: false,
                            parent: dummyParent
                        }),
                        testVal;

                    expect(() => {
                        testVal = baseWidget.getModalValuesString();
                    }).toThrow(new Error('Unimplemented getModalValuesString() method in a ModalWidgetBase implementation.'));
                });
            });

            describe('#render', () => {
                Async.it('calls setupCustomParams', () => {
                    modalWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: false,
                        parent: dummyParent
                    });
                    spyOn(modalWidget, 'setupCustomParams').and.callFake(() => Q());
                    return modalWidget.render()
                        .tap(() => {
                            expect(modalWidget.setupCustomParams.calls.count()).toBe(1);
                        });
                });

                Async.it('generates strings with i18n -> empty object if nothing defined', () => {
                    let customModel = _.extend({}, this.model),
                        i18nArgs = null;
                    customModel.attributes = _.extend({}, this.model.attributes, {
                        label: undefined,
                        placeHolder: undefined,
                        modalTitle: undefined,
                        okButtonText: undefined,
                        labels: undefined
                    });
                    modalWidget = new LF.Widget[customModel.get('type')]({
                        model: customModel,
                        mandatory: false,
                        parent: dummyParent
                    });
                    //jscs:disable requireArrowFunctions
                    spyOn(modalWidget, 'i18n').and.callFake(function (strings) {
                        i18nArgs = [].splice.call(arguments, 0);
                        return Q(_.extend({}, strings));
                    });
                    //jscs:enable requireArrowFunctions
                    return modalWidget.render()
                        .tap(() => {
                            expect(modalWidget.i18n.calls.count()).toBe(1);

                            /**
                             * Since toHaveBeenCalledWith does not work with objects unless the reference is the same,
                             * inspect the closure variable we have in the spy, to make sure it was called correctly.
                             */

                            expect(_.keys(i18nArgs[0]).length).toBe(0);
                            // function that returns null
                            expect(i18nArgs[1]()).toBe(null);
                            // namespace is correct.
                            expect(i18nArgs[2].namespace).toBe(modalWidget.getQuestion().getQuestionnaire().id);
                        });
                });

                Async.it('generates strings with i18n -> gets labels, placeholders, and text from model', () => {
                    let customModel = _.extend({}, this.model),
                        i18nArgs = null,
                        i18nReturn = null;
                    customModel.attributes = _.extend({}, this.model.attributes, {
                        label: 'testLabel',
                        placeholder: 'testPlaceHolder',
                        modalTitle: 'testModalTitle',
                        okButtonText: 'testOkButtonText',
                        labels: {
                            labelOne: 'test label 1',
                            labelTwo: 'test label 2'
                        }
                    });
                    modalWidget = new LF.Widget[customModel.get('type')]({
                        model: customModel,
                        mandatory: false,
                        parent: dummyParent
                    });
                    spyOn(modalWidget, 'i18n').and.callFake((strings) => {
                        i18nArgs = [].splice.call(arguments, 0);
                        i18nReturn = {};
                        let keys = _.keys(strings);
                        for (let i = 0; i < keys.length; ++i) {
                            i18nReturn[keys[i]] = `i18n:${strings[keys[i]]}`;
                        }
                        return Q(i18nReturn);
                    });
                    return modalWidget.render()
                        .tap(() => {
                            expect(modalWidget.i18n.calls.count()).toBe(1);

                            expect(i18nReturn.label).toBe('i18n:testLabel');
                            expect(i18nReturn.placeholder).toBe('i18n:testPlaceHolder');
                            expect(i18nReturn.modalTitle).toBe('i18n:testModalTitle');
                            expect(i18nReturn.okButtonText).toBe('i18n:testOkButtonText');
                            expect(i18nReturn.labelOne).toBe('i18n:test label 1');
                            expect(i18nReturn.labelTwo).toBe('i18n:test label 2');
                        });
                });

                Async.it('generates strings with i18n -> gets labels, placeholders, and text from model', () => {
                    let customModel = _.extend({}, this.model),
                        i18nArgs = null,
                        i18nReturn = null;
                    customModel.attributes = _.extend({}, this.model.attributes, {
                        label: 'testLabel',
                        placeholder: 'testPlaceHolder',
                        modalTitle: 'testModalTitle',
                        okButtonText: 'testOkButtonText',
                        labels: {
                            labelOne: 'test label 1',
                            labelTwo: 'test label 2'
                        }
                    });
                    modalWidget = new LF.Widget[customModel.get('type')]({
                        model: customModel,
                        mandatory: false,
                        parent: dummyParent
                    });
                    spyOn(modalWidget, 'i18n').and.callFake((strings) => {
                        i18nArgs = [].splice.call(arguments, 0);
                        i18nReturn = {};
                        let keys = _.keys(strings);
                        for (let i = 0; i < keys.length; ++i) {
                            i18nReturn[keys[i]] = `i18n:${strings[keys[i]]}`;
                        }
                        return Q(i18nReturn);
                    });
                    return modalWidget.render()
                        .tap(() => {
                            expect(modalWidget.i18n.calls.count()).toBe(1);

                            expect(i18nReturn.label).toBe('i18n:testLabel');
                            expect(i18nReturn.placeholder).toBe('i18n:testPlaceHolder');
                            expect(i18nReturn.modalTitle).toBe('i18n:testModalTitle');
                            expect(i18nReturn.okButtonText).toBe('i18n:testOkButtonText');
                            expect(i18nReturn.labelOne).toBe('i18n:test label 1');
                            expect(i18nReturn.labelTwo).toBe('i18n:test label 2');
                        });
                });

                Async.it('calls renderTemplate as expected for all UI elements', () => {
                    let customModel = _.extend({}, this.model),
                        i18nArgs = null,
                        i18nReturn = null;
                    customModel.attributes = _.extend({}, this.model.attributes, {
                        label: 'testLabel',
                        placeholder: 'testPlaceHolder',
                        modalTitle: 'testModalTitle',
                        okButtonText: 'testOkButtonText',
                        labels: {
                            labelOne: 'test label 1',
                            labelTwo: 'test label 2'
                        }
                    });
                    modalWidget = new LF.Widget[customModel.get('type')]({
                        model: customModel,
                        mandatory: false,
                        parent: dummyParent
                    });
                    spyOn(modalWidget, 'i18n').and.callFake((strings) => {
                        i18nArgs = [].splice.call(arguments, 0);
                        i18nReturn = {};
                        let keys = _.keys(strings);
                        for (let i = 0; i < keys.length; ++i) {
                            i18nReturn[keys[i]] = `i18n:${strings[keys[i]]}`;
                        }
                        return Q(i18nReturn);
                    });
                    spyOn(modalWidget, 'renderTemplate').and.callThrough();

                    // Ignore templates from the spinner items
                    spyOn(modalWidget, 'injectSpinnerInputs');
                    return modalWidget.render()
                        .tap(() => {
                            expect(modalWidget.renderTemplate.calls.count()).toBe(4);
                            expect(modalWidget.renderTemplate).toHaveBeenCalledWith(
                                DEFAULT_WRAPPER_TEMPLATE,
                                {}
                            );
                            expect(modalWidget.renderTemplate).toHaveBeenCalledWith(
                                DEFAULT_LABEL_TEMPLATE,
                                {
                                    link: modalWidget.model.get('id'),
                                    text: i18nReturn.label
                                }
                            );
                            expect(modalWidget.renderTemplate).toHaveBeenCalledWith(
                                DEFAULT_INPUT_TEMPLATE,
                                {
                                    id: modalWidget.model.get('id'),
                                    placeholder: i18nReturn.placeholder || '',
                                    name: modalWidget.model.get('id'),
                                    className: modalWidget.model.get('className') || this.defaultClassName,
                                    maxLength: modalWidget.model.get('maxLength')
                                }
                            );
                            expect(modalWidget.renderTemplate).toHaveBeenCalledWith(
                                modalWidget.defaultModalTemplate,
                                {
                                    title: 'i18n:testModalTitle',
                                    okButtonText: 'i18n:testOkButtonText',
                                    labelOne: 'i18n:test label 1',
                                    labelTwo: 'i18n:test label 2'
                                }
                            );
                        });
                });

                Async.it('generates UI elements', () => {
                    let customModel = _.extend({}, this.model),
                        i18nArgs = null,
                        i18nReturn = null;
                    customModel.attributes = _.extend({}, this.model.attributes, {
                        label: 'testLabel',
                        placeholder: 'testPlaceHolder',
                        modalTitle: 'testModalTitle',
                        okButtonText: 'testOkButtonText',
                        labels: {
                            labelOne: 'test label 1',
                            labelTwo: 'test label 2'
                        }
                    });
                    modalWidget = new LF.Widget[customModel.get('type')]({
                        model: customModel,
                        mandatory: false,
                        parent: dummyParent
                    });
                    spyOn(modalWidget, 'i18n').and.callFake((strings) => {
                        i18nArgs = [].splice.call(arguments, 0);
                        i18nReturn = {};
                        let keys = _.keys(strings);
                        for (let i = 0; i < keys.length; ++i) {
                            i18nReturn[keys[i]] = `i18n:${strings[keys[i]]}`;
                        }
                        return Q(i18nReturn);
                    });

                    // Ignore templates from the spinner items
                    spyOn(modalWidget, 'injectSpinnerInputs');
                    return modalWidget.render()
                        .tap(() => {
                            // wrapper
                            expect(modalWidget.$el.find('[data-container]').length).toBe(1);

                            // label
                            expect(modalWidget.$el.find(`label[for=${modalWidget.model.get('id')}]`).length)
                                .toBe(1);

                            // input
                            expect(modalWidget.$el.find(`input#${modalWidget.model.get('id')}`).length)
                                .toBe(1);

                            // modal
                            expect(modalWidget.$modal.length).toBe(1);
                        });
                });

                Async.it('delegates events', () => {
                    let customModel = _.extend({}, this.model);

                    customModel.attributes = _.extend({}, this.model.attributes, {
                        label: 'testLabel',
                        placeholder: 'testPlaceHolder',
                        modalTitle: 'testModalTitle',
                        okButtonText: 'testOkButtonText',
                        labels: {
                            labelOne: 'test label 1',
                            labelTwo: 'test label 2'
                        }
                    });
                    modalWidget = new LF.Widget[customModel.get('type')]({
                        model: customModel,
                        mandatory: false,
                        parent: dummyParent
                    });

                    // Ignore templates from the spinner items
                    spyOn(modalWidget, 'delegateEvents');
                    return modalWidget.render()
                        .tap(() => {
                            expect(modalWidget.delegateEvents).toHaveBeenCalled();
                        });
                });

                Async.it('handles existing answer appropriately', () => {
                    let customModel = _.extend({}, this.model);

                    customModel.attributes = _.extend({}, this.model.attributes, {
                        label: 'testLabel',
                        placeholder: 'testPlaceHolder',
                        modalTitle: 'testModalTitle',
                        okButtonText: 'testOkButtonText',
                        labels: {
                            labelOne: 'test label 1',
                            labelTwo: 'test label 2'
                        }
                    });
                    modalWidget = new LF.Widget[customModel.get('type')]({
                        model: customModel,
                        mandatory: false,
                        parent: dummyParent
                    });

                    modalWidget.completed = true;
                    modalWidget.answer = this.testValue1;

                    // fake the model so it looks like we have an answer at index 0.
                    modalWidget.answers = {
                        at: (iNdx) => {
                            if (iNdx === 0) {
                                return {
                                    get: (prop) => {
                                        if (prop === 'response') {
                                            return this.testValue1;
                                        }
                                        return null;
                                    }
                                };
                            }
                            return null;
                        }
                    };

                    // Ignore templates from the spinner items
                    spyOn(modalWidget, 'injectSpinnerInputs');
                    spyOn($.fn, 'trigger');
                    return modalWidget.render()
                        .tap(() => {
                            expect(modalWidget.$(`#${this.model.get('id')}`).val()).toBe(this.displayValue1);
                            expect($.fn.trigger).toHaveBeenCalledWith('input');
                        });
                });
            });

            describe('post-rendered methods', () => {
                beforeEach((done) => {

                    modalWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: false,
                        parent: dummyParent
                    });

                    modalWidget.render()
                        .then(() => {
                            done();
                        })
                        .catch((e) => {
                            fail(e);
                        })
                        .done();
                });

                describe('#openDialog', () => {
                    it ('returns undefined if called from a jQuery event (finishes promise).', () => {
                        let e = new $.Event();
                        let ret = modalWidget.openDialog(e);
                        expect(ret).toBe(undefined);
                    });

                    Async.it ('opens the dialog by calling "show".  Adds and cleans up event handlers for dialog, calls dialogOpened handler.', () => {
                        spyOn(modalWidget.$modal, 'on').and.callThrough();
                        spyOn(modalWidget.$modal, 'modal').and.callThrough();
                        spyOn(modalWidget.$modal, 'off').and.callThrough();
                        spyOn(modalWidget, 'dialogOpened').and.callThrough();
                        return modalWidget.openDialog().tap(() => {
                            expect(modalWidget.$modal.on.calls.count()).toBe(1);
                            expect(modalWidget.$modal.off.calls.count()).toBe(1);
                            expect(modalWidget.$modal.modal.calls.count()).toBe(1);
                            expect(modalWidget.dialogOpened.calls.count()).toBe(1);

                            expect(modalWidget.$modal.modal).toHaveBeenCalledWith('show');
                        });
                    });
                });

                describe('#dialogClosing', () => {
                    it ('returns undefined if called from a jQuery event (finishes promise).', () => {
                        let e = new $.Event();
                        let ret = modalWidget.openDialog(e);
                        expect(ret).toBe(undefined);
                    });

                    Async.it ('does nothing if active element is not modal button.', () => {
                        modalWidget.value = this.testValue1;
                        spyOn(ModalWidgetBase, 'activeElementIsModalButton').and.returnValue(false);
                        spyOn(modalWidget, 'getModalValuesString').and.returnValue(Q(this.testValue2));
                        spyOn(modalWidget.$input, 'trigger');
                        return modalWidget.dialogClosing().tap(() => {
                            expect(modalWidget.getModalValuesString.calls.count()).toBe(0);
                            expect(modalWidget.value).toBe(this.testValue1);
                            expect(modalWidget.$input.trigger.calls.count()).toBe(0);
                        });
                    });
                    
                    Async.it ('sets value and calls trigger on input if active element is modal button', () => {
                        modalWidget.value = this.testValue1;
                        spyOn(ModalWidgetBase, 'activeElementIsModalButton').and.returnValue(true);
                        spyOn(modalWidget, 'getModalValuesString').and.returnValue(Q(this.testValue2));
                        spyOn(modalWidget.$input, 'trigger');
                        return modalWidget.dialogClosing().tap(() => {
                            expect(modalWidget.getModalValuesString.calls.count()).toBe(1);
                            expect(modalWidget.value).toBe(this.testValue2);
                            expect(modalWidget.$input.trigger).toHaveBeenCalledWith('input');
                        });
                    });
                });

                describe('#setupCustomParams', () => {
                    Async.it('does not run config function if it does not exist', () => {
                        let customModel = _.extend({}, this.model);
                        customModel.attributes = _.extend({}, this.model.attributes, {
                            defaultVal: '15',
                            spinnerInputOptions: {
                                min: 0,
                                max: 0,
                                custom: true
                            },
                            configFunc: 'functionThatDoesntExist'
                        });
                        modalWidget.model = customModel;
                        return modalWidget.setupCustomParams();
                        // No expects here... if it didn't throw an error, and the promise does resolve,
                        // then this means it did not attempt to run the function that does not exist.
                    });

                    Async.it('updates defaultVal and spinnerInputOptions from functions, but not other things', () => {
                        let customModel = _.extend({}, this.model);
                        customModel.attributes = _.extend({}, this.model.attributes, {
                            defaultVal: '15',
                            spinnerInputOptions: {
                                min: 0,
                                max: 0,
                                custom: true
                            },
                            customModelThing: this.testValue2,
                            configFunc: 'spinnerInputUpdater'
                        });
                        LF.Widget.ParamFunctions.spinnerInputUpdater = () => {
                            return Q({
                                spinnerInputOptions: {
                                    min: 1,
                                    max: 100,
                                    custom: false
                                },
                                defaultVal: '16',
                                customModelThing: 'new'
                            });
                        };

                        modalWidget.model = customModel;
                        return modalWidget.setupCustomParams().tap(() => {
                            expect(modalWidget.model.attributes.spinnerInputOptions.min).toBe(1);
                            expect(modalWidget.model.attributes.spinnerInputOptions.max).toBe(100);
                            expect(modalWidget.model.attributes.spinnerInputOptions.custom).toBe(false);
                            expect(modalWidget.model.attributes.defaultVal).toBe('16');
                            expect(modalWidget.model.attributes.customModelThing).toBe(this.testValue2);
                        });
                    });
                });

                describe('#removeModal', () => {
                    it('hides and removes modal', () => {
                        let $modal = modalWidget.$modal;
                        spyOn($modal, 'modal');
                        spyOn($modal, 'remove');

                        modalWidget.removeModal();
                        expect($modal.modal.calls.count()).toBe(1);
                        expect($modal.modal).toHaveBeenCalledWith('hide');
                        expect($modal.remove.calls.count()).toBe(1);
                        expect(modalWidget.$modal).toBe(null);
                    });
                });
                
                describe('#destroy', () => {
                    it('destroys all references', () => {
                        spyOn(modalWidget, 'removeSpinnerInputs');
                        spyOn(modalWidget, 'removeModal');
                        spyOn(modalWidget, 'undelegateEvents');

                        modalWidget.destroy();

                        expect(modalWidget.removeSpinnerInputs).toHaveBeenCalled();
                        expect(modalWidget.removeModal).toHaveBeenCalled();
                        expect(modalWidget.undelegateEvents).toHaveBeenCalled();

                    });
                });
            });
        });
    }
}
