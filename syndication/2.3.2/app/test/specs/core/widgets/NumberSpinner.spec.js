import * as helpers from 'test/helpers/SpecHelpers';
import 'test/helpers/StudyDesign';

import Questionnaires from 'core/collections/Questionnaires';
import Answers from 'core/collections/Answers';
import Question from 'core/models/Question';
import Dashboard from 'core/models/Dashboard';
import QuestionnaireView from 'core/views/QuestionnaireView';
import QuestionView from 'core/views/QuestionView';
import Widget from 'core/models/Widget';
import NumberSpinner from 'core/widgets/NumberSpinner';
import User from 'core/models/User';
import Session from 'core/classes/Session';
import Subject from 'core/models/Subject';
// import Templates from 'core/collections/Templates';
import Templates from 'core/resources/Templates';
import Languages from 'core/collections/Languages';
import NumberSpinnerInput from 'core/widgets/input/NumberSpinnerInput';
import BaseSpinnerTests from './BaseSpinner.spec';

import 'core/resources/Templates';

export default class NumberSpinnerTests extends BaseSpinnerTests {
    constructor (model = NumberSpinnerTests.model) {
        super(model);
    }

    /**
     * {Widget} get the widget model.
     */
    static get model () {
        return new Widget({
            id: 'SPINNER_DIARY_W_1',
            type: 'NumberSpinner',
            className: 'SPINNER_DIARY_W_1',
            label: 'PICK_A_NUMBER',
            spinnerInputOptions: [
                {
                    min: 0,
                    max: 500
                }
            ]
        });
    }

    execTests () {
        super.execTests();

        TRACE_MATRIX('US6106').
        describe('NumberSpinner', () => {
            let templates,
                dummyParent,
                security,
                numberSpinnerWidget = null;

            beforeAll(() => {
                security = LF.security;
                LF.security = this.getDummySecurity();

                templates = LF.templates;

            });
            afterAll(() => {
                LF.security = security;
                LF.templates = templates;
            });

            beforeEach(() => {
                dummyParent = this.getMinDummyQuestion();
            });
            afterEach(() => {
                $(`#${dummyParent.getQuestionnaire().id}`).remove();
                $('.modal').remove();
                $(`.modal-backdrop`).remove();
                numberSpinnerWidget = null;
            });

            describe('Default properties', () => {
                beforeEach(() => {
                    numberSpinnerWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: false,
                        parent: dummyParent
                    });
                });

                it('#defaultModalTemplate', () => {
                    expect(numberSpinnerWidget.defaultModalTemplate).toEqual('DEFAULT:NumberSpinnerModal');
                });

                it('#defaultSpinnerTemplate', () => {
                    expect(numberSpinnerWidget.defaultSpinnerTemplate).toEqual('DEFAULT:NumberSpinnerControl');
                });

                it('#defaultSpinnerItemTemplate', () => {
                    expect(numberSpinnerWidget.defaultSpinnerItemTemplate).toEqual('DEFAULT:NumberItemTemplate');
                });
            });

            describe('Value functions', () => {

                Async.it('#getSpinnerValuesArray() with value', () => {
                    let customModel = _.extend({}, this.model);
                    customModel.attributes = _.extend({}, this.model.attributes, {
                        defaultVal: '4'
                    });

                    numberSpinnerWidget = new LF.Widget[customModel.get('type')]({
                        model: customModel,
                        mandatory: false,
                        parent: dummyParent
                    });

                    return numberSpinnerWidget.render()
                        .tap(() => {
                            expect(numberSpinnerWidget.getSpinnerValuesArray()[0]).toEqual('4');
                        });
                });

                Async.it('#getModalValuesString() with value', () => {
                    let customModel = _.extend({}, this.model);
                    customModel.attributes = _.extend({}, this.model.attributes, {
                        defaultVal: '4'
                    });

                    numberSpinnerWidget = new LF.Widget[customModel.get('type')]({
                        model: customModel,
                        mandatory: false,
                        parent: dummyParent
                    });

                    return numberSpinnerWidget.render()
                        .then(() => {
                            return numberSpinnerWidget.openDialog();
                        })
                        .then(() => {
                            // duplicating some work that is done by render, but show() and setting values
                            //  are thread safe so this is a good way to listen to the promise
                            return numberSpinnerWidget.refreshSpinners();
                        })
                        .then(() => {
                            return numberSpinnerWidget.getModalValuesString();
                        }).tap((value) => {
                            expect(value).toBe('4');
                        });
                });

                Async.it('#injectSpinnerInputs() places spinners inside modal template', () => {
                    numberSpinnerWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: false,
                        parent: dummyParent
                    });

                    return numberSpinnerWidget.render()
                        .then(() => {
                            return numberSpinnerWidget.openDialog();
                        })
                        .then(() => {
                            // duplicating some work that is done by render, but show() and setting values
                            //  are thread safe so this is a good way to listen to the promise
                            return numberSpinnerWidget.refreshSpinners();
                        })
                        .tap(() => {
                            let $containers = numberSpinnerWidget.$modal.find('.number-spinner-container');
                            expect($containers.length).toBe(1);

                            $containers.each((index, element) => {
                                expect(numberSpinnerWidget.spinners[index] instanceof NumberSpinnerInput).toBe(true);
                                expect($(element).children().length).toBe(1);
                            });
                        });
                });
            });
        });
    }
}

let tester = new NumberSpinnerTests();
tester.execTests();
