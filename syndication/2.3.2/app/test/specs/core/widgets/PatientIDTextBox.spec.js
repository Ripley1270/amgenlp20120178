/*globals describe, it, expect, beforeAll, afterAll, beforeEach, afterEach, spyOn */
import TextBox from 'core/widgets/TextBox';
import TextBoxWidgetBase from 'core/widgets/TextBoxWidgetBase';
import PatientIDTextBox from 'core/widgets/PatientIDTextBox';
import WidgetBase from 'core/widgets/WidgetBase';

import * as helpers from 'test/helpers/SpecHelpers';
import 'test/helpers/StudyDesign';

import Answers from 'core/collections/Answers';
import Dashboard from 'core/models/Dashboard';
import Question from 'core/models/Question';
import Site from 'core/models/Site';
import Questionnaires from 'core/collections/Questionnaires';
import Subjects from 'core/collections/Subjects';
import Widget from 'core/models/Widget';
import Templates from 'core/collections/Templates';

import QuestionnaireView from 'core/views/QuestionnaireView';
import QuestionView from 'core/views/QuestionView';
import TextBoxTests from './TextBox.specBase';

import Data from 'core/Data';

import 'sitepad/resources/Templates';

const dummySubject = {
        device_id: hex_sha512('123456'),
        initials: 'bc',
        log_level: 'ERROR',
        id: 1,
        phase: 10,
        phaseStartDateTZOffset: '2013-10-05T08:15:30-05:00',
        phaseTriggered: 'false',
        secret_answer: hex_sha512('apple'),
        secret_question: '0',
        site_code: '267',
        subject_id: '12345',
        subject_password: hex_sha512('applekrpt.39ed120a0981231'),
        service_password: hex_sha512('applekrpt.39ed120a0981231'),
        subject_number: '54321',
        subject_active: 1,
        krpt: 'krpt.39ed120a0981231',
        enrollmentDate: '2013-10-05T08:15:30-05:00',
        activationDate: '2013-10-05T08:15:30-05:00'
    };

class PatientIDTextBoxTests extends TextBoxTests {
    constructor (model = PatientIDTextBoxTests.model) {
        super(model);
    }

    static get model () {
        return new Widget({
            id: 'DAILY_DIARY_W_0',
            type: 'PatientIDTextBox',
            className: 'DAILY_DIARY_W_0',
            templates: {},
            answers: []
        });
    }

    beforeAll () {
        return super.beforeAll()
            .then(() => {
                LF.StudyDesign.participantSettings = {
                    participantID: {
                        seed: 100,
                        steps: 2,
                        max: 600
                    },
                    participantIDFormat: 'YYZSSSSYYZPPPPYYZ',
                    participantNumberPortion: [10, 14],
                    siteNumberPortion: [3, 7],
                    languageList: [{
                        language: 'en',
                        locale: 'US',
                        localized: 'LANG_EN-US'
                    }, {
                        language: 'es',
                        locale: 'ES',
                        localized: 'LANG_ES-ES'
                    }]
                };
                Data.site = new Site({ siteCode: '0008' });
            });
    }

    /**
     * checks to see that after a render, HTML is loaded by verifying all required elements are in the DOM
     * @returns {Q.Promise<void>} resolved when completed.  Compatible with Async.it
     */
    htmlRenderedCheck () {
        return super.htmlRenderedCheck()
            .then(() => {
                expect($(`#${this.dummyParent.id}`).find('#DAILY_DIARY_W_0').length).toEqual(1);
            });
    }

    /**
     * a valid first character.  used for tests that set the value of the control (respond, deletion tests, etc)
     */
    get validChar () {
        return '1';
    }

    /**
     * executes all the tests for this file
     */
    execTests () {
        describe('PatientIDTextBox', () => {
            super.execTests();
            describe('PatientIDTextBox Specific', () => {
                let security,
                    templates,
                    dummyParent;

                beforeAll(()=> {
                    security = LF.security;
                    LF.security = this.getDummySecurity();
                });

                afterAll(() => {
                    LF.security = security;
                });

                beforeEach(() => {
                    dummyParent = this.getMinDummyQuestion();
                });
                afterEach(() => {
                    $(`#${dummyParent.getQuestionnaire().id}`).remove();
                });

                describe('constructor', () => {
                    it('calls super', () => {
                        spyOn(TextBox.prototype, 'constructor');

                        const model = PatientIDTextBoxTests.model,
                            options = {
                                model: model,
                                mandatory: false,
                                parent: dummyParent
                            },
                            widget = new LF.Widget[model.get('type')](options);

                        expect(TextBox.prototype.constructor).toHaveBeenCalledWith(options);
                        expect(TextBox.prototype.constructor.calls.count()).toEqual(1);
                    });
                    it('starts out with validation as non-unique and not in range', () => {
                        const model = PatientIDTextBoxTests.model,
                            widget = new LF.Widget[model.get('type')]({
                                model: model,
                                mandatory: false,
                                parent: dummyParent
                            });
                        expect(widget.isUnique).toEqual(false);
                        expect(widget.isInRange).toEqual(false);
                    });
                });

                describe('#render()', () => {
                    Async.it(`when constructed with no answer, answer doesn't exist.`, () => {
                        const model = PatientIDTextBoxTests.model,
                            widget = new LF.Widget[model.get('type')]({
                                model: model,
                                mandatory: false,
                                parent: dummyParent
                            });
                        let input;
                        return widget.render()
                            .then(() => {
                                input = $($(`#${this.dummyParent.id}`).find('#DAILY_DIARY_W_0')[0]);
                                expect(widget.answer).toBeUndefined();
                                expect(input.val()).toBe('');
                            });
                    });

                    Async.it(`Respond formats the input text when saved.`, () => {
                        const model = PatientIDTextBoxTests.model,
                            widget = new LF.Widget[model.get('type')]({
                                model: model,
                                mandatory: false,
                                parent: dummyParent
                            });

                        let input;
                        return widget.render()
                            .then(() => {
                                input = $($(`#${this.dummyParent.id}`).find('#DAILY_DIARY_W_0')[0]);
                                const event = $.Event('click', {target: input, currentTarget: input});
                                input.val('123');
                                return widget.respond(event);
                            })
                            .then(() => {
                                expect(widget.answer).toBeDefined();
                                expect(widget.answer.get('response')).toBe('YYZ0008YYZ0123YYZ');
                                expect(input.val()).toBe('123');
                            });
                    });
                    Async.it(`formatted answer displays only the subject id portion.`, () => {
                        const model = PatientIDTextBoxTests.model,
                            widget = new LF.Widget[model.get('type')]({
                                model: model,
                                mandatory: false,
                                parent: dummyParent
                            });
                        let input;
                        return widget.render()
                            .then(() => {
                                input = $($(`#${this.dummyParent.id}`).find('#DAILY_DIARY_W_0')[0]);
                                const event = $.Event('click', {target: input, currentTarget: input});
                                input.val('123');
                                return widget.respond(event);
                            })
                            .then(() => {
                                expect(widget.answer).toBeDefined();
                                widget.answer.set('response', 'YYZ1234YYZ0432YYZ');
                                return widget.render();
                            })
                            .then(() => {
                                // render would have built a new one...
                                input = $($(`#${this.dummyParent.id}`).find('#DAILY_DIARY_W_0')[0]);
                                expect(widget.answer).toBeDefined();
                                expect(widget.answer.get('response')).toBe('YYZ1234YYZ0432YYZ');
                                expect(input.val()).toBe('432');
                            });
                    });
                });

                describe('#validate()', () => {
                    Async.it('if not manditory and no answer, then returns true', () => {
                        const model = PatientIDTextBoxTests.model,
                            widget = new LF.Widget[model.get('type')]({
                                model: model,
                                mandatory: false,
                                parent: dummyParent
                            });

                        // verify sassumption that there is no answer if just constructed before doing other checks
                        expect(widget.answer).toBeUndefined();
                        spyOn(TextBox.prototype, 'validate').and.callFake(() => false);
                        // Setup a widget with our fake answers collection
                        widget.isUnique = false;
                        widget.isInRange = false;

                        expect(widget.validate()).toBe(true);
                        return Q();
                    });
                    //jscs:disable maximumLineLength
                    Async.it('if manditory and noanswer, and super.validate(), isUnique & isInRange are true, validate returns false', () => {
                        //jscs:enable maximumLineLength
                        const model = PatientIDTextBoxTests.model,
                            widget = new LF.Widget[model.get('type')]({
                                model: model,
                                mandatory: true,
                                parent: dummyParent
                            });
                        expect(widget.answer).toBeUndefined();
                        spyOn(TextBox.prototype, 'validate').and.callFake(() => true);
                        widget.isUnique = true;
                        widget.isInRange = true;

                        expect(widget.validate()).toBe(false);
                        return Q();
                    });
                    //jscs:disable maximumLineLength
                    Async.it('if not manditory and answer, and super.validate(), isUnique & isInRange are true, validate returns true', () => {
                        //jscs:enable maximumLineLength
                        const model = PatientIDTextBoxTests.model,
                            widget = new LF.Widget[model.get('type')]({
                                model: model,
                                mandatory: false,
                                parent: dummyParent
                            });
                        return widget.render()
                            .then(() => {
                                const input = $($(`#${this.dummyParent.id}`).find('#DAILY_DIARY_W_0')[0]);
                                const event = $.Event('click', {target: input, currentTarget: input});
                                input.val('122');
                                return widget.respond(event);
                            })
                            .then(() => {
                                expect(widget.answer).toBeDefined();
                                spyOn(TextBox.prototype, 'validate').and.callFake(() => true);
                                widget.isInRange = true;

                                expect(widget.validate()).toBe(true);
                            });
                    });

                    //jscs:disable maximumLineLength
                    Async.it('if not manditory and answer, and super.validate(), isInRange are true & isUnique is false, validate returns true', () => {
                        //jscs:enable maximumLineLength
                        const model = PatientIDTextBoxTests.model,
                            widget = new LF.Widget[model.get('type')]({
                                model: model,
                                mandatory: true,
                                parent: dummyParent
                            });
                        return widget.render()
                            .then(() => {
                                const input = $($(`#${this.dummyParent.id}`).find('#DAILY_DIARY_W_0')[0]);
                                const event = $.Event('click', {target: input, currentTarget: input});
                                input.val('123');
                                return widget.respond(event);
                            })
                            .then(() => {
                                expect(widget.answer).toBeDefined();
                                spyOn(TextBox.prototype, 'validate').and.callFake(() => false);
                                widget.isInRange = true;

                                expect(widget.validate()).toBe(false);
                            });
                    });
                });

                describe('static #generatePatientID()', () => {
                    it('generates a patient ID with specified study design formula', () => {
                        // See studydesign above.  Tests with YYZ in between formula segments to check first, last, and length
                        // for both site and patient.
                        helpers.createSite(); // creates site 2674
                        expect(PatientIDTextBox.generatePatientID('41')).toBe('YYZ2674YYZ0041YYZ');
                    });
                });

            });
        });
    }
}

let tests = new PatientIDTextBoxTests();
tests.execTests();
