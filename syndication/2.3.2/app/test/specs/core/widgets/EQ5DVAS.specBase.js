import WidgetBaseTests from './WidgetBase.specBase';
import Widget from 'core/models/Widget';
import {resetStudyDesign} from 'test/helpers/StudyDesign';
import EQ5D from 'core/widgets/VAS/EQ5D/EQ5D';

export default class EQ5DVASTests extends WidgetBaseTests {

    htmlRenderedCheck () {
        let myModel = this.model,
            widget = new LF.Widget[myModel.get('type')]({
                model: myModel,
                mandatory: false,
                parent: this.dummyParent
            });

        return widget.render()
            .then(() => {
                expect($(`#${widget.question.id}`).find(`canvas`).length)
                    .toEqual(1);
            });
    };

    constructor (model) {
        if (model === undefined) {
            model = this.simpleModel;
        }

        super(model);
    }

    /**
     * Any tests you want executed should be added here
     */
    execTests () {
        describe('Base Widget tests', () => {
            LF.appName = "LogPad App";
            super.execTests();
        });

        describe('EQ5DVAS tests', () => {

            let templates,
                dummyParent,
                security,
                EQ5DWidget = null;

            describe('EQ5DVAS Tests', () => {
                Async.beforeAll(() => {
                    resetStudyDesign();

                    security = LF.security;
                    LF.security = this.getDummySecurity();
                    templates = LF.templates;
                    return Q();
                });

                Async.afterAll(() => {
                    LF.security = security;
                    LF.templates = templates;
                    return Q();
                });

                Async.beforeEach(() => {
                    dummyParent = this.getMinDummyQuestion();
                    $(`#${dummyParent.getQuestionnaire().id}`)[0].classList.add('EQ5D');
                    return Q();
                });

                Async.afterEach(() => {
                    $(`#${dummyParent.getQuestionnaire().id}`).remove();
                    EQ5DWidget = null;
                    return Q();
                });

                Async.it('creates object of type EQ5D', () => {
                    let _model = this.simpleModel;

                    EQ5DWidget = new LF.Widget[this.model.get('type')]({
                        model: _model,
                        mandatory: false,
                        parent: dummyParent
                    })

                    return EQ5DWidget.render()
                        .tap(()=> {
                            expect(EQ5DWidget instanceof EQ5D).toBe(true);
                        });
                });

                Async.it('creates all of the required elements to render hand held EQ5D widget', () => {
                    let _model = this.simpleModel;
                    LF.appName = 'LogPad App';

                    EQ5DWidget = new LF.Widget[this.model.get('type')]({
                        model: _model,
                        mandatory: false,
                        parent: dummyParent
                    });

                    return EQ5DWidget.render()
                        .tap(()=> {
                            expect(EQ5DWidget.cvs).toBeDefined();
                            expect($('#left').length).toBeTruthy();
                            expect($('#right').length).toBeTruthy();
                            expect($('#topper').length).toBeTruthy();
                            expect($('#middle').length).toBeTruthy();
                            expect($('#bottom').length).toBeTruthy();
                            expect($('#answerDIV').length).toBeTruthy();
                            expect($('#yhtDIV').length).toBeTruthy();
                            // This case is dependent of LogPad Templates and shouldn't be tested as part of core.
                            // If this needs to be tested, it needs to be moved to logpad specs.
                            //expect($('#logpadInstructionTextDIV').length).toBeTruthy();
                        });
                });

                Async.it('creates all of the required elements to render tablet EQ5D widget', () => {
                    let _model = this.simpleModel;
                    LF.appName = 'SitePad App';

                    EQ5DWidget = new LF.Widget[this.model.get('type')]({
                        model: _model,
                        mandatory: false,
                        parent: dummyParent
                    });

                    return EQ5DWidget.render()
                        .tap(()=> {
                            expect(EQ5DWidget.cvs).toBeDefined();
                            expect($('#left').length).toBeTruthy();
                            expect($('#right').length).toBeTruthy();
                            expect($('#topper').length).toBeTruthy();
                            expect($('#middle').length).toBeTruthy();
                            expect($('#bottom').length).toBeTruthy();
                            expect($('#answerDIV').length).toBeTruthy();
                            expect($('#yhtDIV').length).toBeTruthy();
                            // This case is dependent of LogPad Templates and shouldn't be tested as part of core.
                            // If this needs to be tested, it needs to be moved to logpad specs.
                            //expect($('#logpadInstructionTextDIV').length).toBeTruthy();
                        });
                });

                Async.it('loads the correct defaults for hand held', () =>{
                    let _model = this.simpleModel;
                    LF.appName = 'LogPad App';

                    EQ5DWidget = new LF.Widget[this.model.get('type')]({
                        model: _model,
                        mandatory: false,
                        parent: dummyParent
                    });

                    return EQ5DWidget.render()
                        .tap(()=> {
                            expect(EQ5DWidget.tickMarks.frequency).toEqual(10);
                            expect(EQ5DWidget.tickMarks.values.frequency).toEqual(50);
                            expect(EQ5DWidget.tickMarks.displayMinorTicks).toBeFalsy();
                            expect(EQ5DWidget.customProperties.tickValuePosition).toEqual('right');
                        });
                });

                Async.it('loads the correct defaults for hand held', () =>{
                    let _model = this.simpleModel;
                    LF.appName = 'SitePad App';

                    EQ5DWidget = new LF.Widget[this.model.get('type')]({
                        model: _model,
                        mandatory: false,
                        parent: dummyParent
                    });

                    return EQ5DWidget.render()
                        .tap(()=> {
                            expect(EQ5DWidget.tickMarks.frequency).toEqual(10);
                            expect(EQ5DWidget.tickMarks.values.frequency).toEqual(10);
                            expect(EQ5DWidget.tickMarks.displayMinorTicks).toBeTruthy();
                            expect(EQ5DWidget.customProperties.tickValuePosition).toEqual('left');
                        });
                });

                Async.it('sets expected answer when tapping on the scale', () =>{
                    let _model = this.simpleModel,
                        eMouseDown = jQuery.Event('mousedown', {pageX: 0, pageY: 0});

                    LF.appName = 'LogPad App';

                    EQ5DWidget = new LF.Widget[this.model.get('type')]({
                        model: _model,
                        mandatory: false,
                        parent: dummyParent
                    });

                    return EQ5DWidget.render()
                        .then(() => {

                            // PHANTOMJS HACK:  PhantomJS 1.9.8 does not have full support for flexbox.
                            //      So, for this test, we must hardcode a height and redraw the canvas if the widget is at 0 height,
                            //      which means that it is not respecting its flex styling.
                            if (EQ5DWidget.$el.height() === 0) {
                                EQ5DWidget.$middle.height(400);
                                EQ5DWidget.drawTheSelectionMark();
                            }

                            let topOffset = $('#mainVasCanvas').offset().top;

                            eMouseDown.pageX = 0;
                            eMouseDown.pageY = topOffset + EQ5DWidget.cvs.height / 2;
                            EQ5DWidget.mouseyDownHandler(eMouseDown);
                            expect(EQ5DWidget.answer.get('response')).toBe('50');

                            eMouseDown.pageX = 0;
                            eMouseDown.pageY = topOffset;
                            EQ5DWidget.mouseyDownHandler(eMouseDown);
                            expect(EQ5DWidget.answer.get('response')).toBe('100');

                            topOffset = $('#mainVasCanvas').offset().top;
                            eMouseDown.pageX = 0;
                            eMouseDown.pageY = topOffset + (EQ5DWidget.cvs.height);
                            EQ5DWidget.mouseyDownHandler(eMouseDown);
                            expect(EQ5DWidget.answer.get('response')).toBe('0');
                        });
                });


            });
        });
    };

    /**
     * {Widget} get the widget model.
     */
    get simpleModel() {
        return new Widget({
            id: 'VVAS_DIARY_W_1',
            type: 'VAS',
            displayAs: 'EQ5D',
            anchors: {
                'min': {
                    'text': 'NO_PAIN'
                },
                'max': {
                    'text': 'EXTREME_PAIN'
                },
                'customProperties': {
                    'instructionText': 'test_text',
                }
            }
        });
    }
}
