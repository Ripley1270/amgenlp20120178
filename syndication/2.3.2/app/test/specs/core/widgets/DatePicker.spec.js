// jscs:disable requireShorthandArrowFunctions
import * as helpers from 'test/helpers/SpecHelpers';
import 'test/helpers/StudyDesign';

import Widget from 'core/models/Widget';
import DatePicker from 'core/widgets/DatePicker';
import WidgetBaseTests from './WidgetBase.specBase';
import Templates from 'core/collections/Templates';

describe('DatePicker', () => {

    const template = `<div id="questionnaire-template">
            <div id="questionnaire"></div>
            </div>`;
    let security,
        templates;

    beforeAll(()=> {
        security = LF.security;
        LF.security = WidgetBaseTests.getDummySecurity();

        templates = LF.templates;
        LF.templates = new Templates([{
                name        : 'DatePicker',
                namespace   : 'DEFAULT',
                template    : `<input id="{{ id }}" class="date-input" min="{{ min }}" max="{{ max }}"
                                   data-role="datebox" data-options=\'{{ configuration }}\' />`
            }, {
            name: 'DatePickerLabel',
            namespace: 'DEFAULT',
            template: '<label for="{{ id }}_date_input">{{ dateLabel }}</label>'
        }]);

        $('body').append(template);
    });

    afterAll(() => {
        $('body').remove(template);
        LF.security = security;
        LF.templates = templates;
    });

    const model = new Widget({
        type: 'DatePicker',
        id: 'testWidgetId'
    });
    const baseTests = new WidgetBaseTests(model);

    baseTests.execTests();

    describe('Class Specific', () => {
        it('localizedDate is undefined', () => {
            const model = new Widget({
                    type: 'DatePicker'
                }),
                datepickerWidget = new LF.Widget[model.get('type')]({
                    model: model,
                    mandatory: false,
                    parent: WidgetBaseTests.getMinDummyQuestion()
                });
            expect(datepickerWidget.localizedDate).toBeUndefined();

        });

        it('should return the correct value', () => {
            let date = new Date('2015', '04', '11', '12', '23', '11');
            const model = new Widget({
                    type: 'DatePicker'
                }),
                datepickerWidget = new LF.Widget[model.get('type')]({
                    model: model,
                    mandatory: false,
                    parent: WidgetBaseTests.getMinDummyQuestion()
                });
            expect(datepickerWidget.buildStudyWorksString(date)).toEqual('11 May 2015');

        });

        it('should set the correct value', (done) => {
            const model = new Widget({
                    type: 'DatePicker'
                }),
                datepickerWidget = new LF.Widget[model.get('type')]({
                    model: model,
                    mandatory: false,
                    parent: WidgetBaseTests.getMinDummyQuestion()
                });

            $('#questionnaire').append(datepickerWidget.parent.el);
            Q()
                .then(() => {
                    return datepickerWidget.render();
                })
                .then(() => {
                    datepickerWidget.$('input.date-input').val('Friday, November 06, 2015');
                    let date = new Date();
                    date.setFullYear(2015, 10, 6); //month is index based: first month is 0
                    datepickerWidget.$('input[data-role="datebox"]').datebox('setTheDate', date);
                    datepickerWidget.respond({}, {});
                })
                .tap(() => {
                    expect(datepickerWidget.answer.get('response')).toEqual('06 Nov 2015');
                })
                .catch(e => fail(e)) // w/o this test may not fail
                .done(done); // when promise done, test is done
        }, 400);

        it('should set the correct value to datebox', (done) => {
            const model = new Widget({
                    type: 'DatePicker'
                }),
                datepickerWidget = new LF.Widget[model.get('type')]({
                    model: model,
                    mandatory: false,
                    parent: WidgetBaseTests.getMinDummyQuestion()
                });
            datepickerWidget.localizedDate = 'Friday, April 03, 2015';

            $('#questionnaire').append(datepickerWidget.parent.el);
            Q()
                .then(() => {
                    return datepickerWidget.render();
                })
                .tap(() => {
                    expect(datepickerWidget.$('input[data-role="datebox"]').val()).toEqual('Friday, April 03, 2015');
                })
                .catch(e => fail(e)) // w/o this test may not fail
                .done(done); // when promise done, test is done
        }, 400);

        it('should render labels if showLabels true', (done) => {
            const model = new Widget({
                    type: 'DatePicker',
                    showLabels: true
                }),
                datepickerWidget = new LF.Widget[model.get('type')]({
                    model: model,
                    mandatory: false,
                    parent: WidgetBaseTests.getMinDummyQuestion()
                });
            helpers.fakeGetStrings();
            $('#questionnaire').append(datepickerWidget.parent.el);
            Q()
                .then(() => {
                    return datepickerWidget.render();
                })
                .tap(() => {
                    expect(datepickerWidget.$('label').length).toEqual(1);
                })
                .catch(e => fail(e)) // w/o this test may not fail
                .done(done); // when promise done, test is done
        }, 400);

        it('shouldn\'t render labels if showLabels omitted', (done) => {
            const model = new Widget({
                    type: 'DatePicker',
                    showLabels: false
                }),
                datepickerWidget = new LF.Widget[model.get('type')]({
                    model: model,
                    mandatory: false,
                    parent: WidgetBaseTests.getMinDummyQuestion()
                });

            datepickerWidget.$('label').append(datepickerWidget.parent.el);
            Q()
                .then(() => {
                    return datepickerWidget.render();
                })
                .tap(() => {
                    expect(datepickerWidget.$('#label').length).toEqual(0);
                })
                .catch(e => fail(e)) // w/o this test may not fail
                .done(done); // when promise done, test is done
        }, 400);

    });
});
