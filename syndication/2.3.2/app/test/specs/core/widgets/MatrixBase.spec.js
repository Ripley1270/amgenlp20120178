import * as helpers from 'test/helpers/SpecHelpers';
import 'test/helpers/StudyDesign';
import QuestionView from 'core/views/QuestionView';
import Widget from 'core/models/Widget';
import WidgetBaseTests from './WidgetBase.specBase';
import MatrixBase from 'core/widgets/MatrixBase';

export default class MatrixBaseTests extends WidgetBaseTests {
    execTests () {
        super.execTests();

        describe('MatrixBase', () => {
            let templates,
                dummyParent,
                security,
                matrixWidget = null;

            beforeAll(() =>{
                security = LF.security;
                LF.security = this.getDummySecurity();
                templates = LF.templates;
            });

            afterAll(() => {
                LF.security = security;
                LF.templates = templates;
            });

            beforeEach(() => {
                dummyParent = this.getMinDummyQuestion();
            });

            afterEach(() => {
                $(`#${dummyParent.getQuestionnaire().id}`).remove();
            });

            describe('#getters', () => {});

            describe('#build functions', () => {});
        });
    }
}
