import PatientLangListRadioButton from 'core/widgets/PatientLangListRadioButton';
import WidgetBase from 'core/widgets/WidgetBase';

import * as helpers from 'test/helpers/SpecHelpers';
import 'test/helpers/StudyDesign';

import Data from 'core/Data';

import Answers from 'core/collections/Answers';
import Dashboard from 'core/models/Dashboard';
import Question from 'core/models/Question';
import Questionnaires from 'core/collections/Questionnaires';
import Widget from 'core/models/Widget';
import Templates from 'core/collections/Templates';

import QuestionnaireView from 'core/views/QuestionnaireView';
import QuestionView from 'core/views/QuestionView';

import 'sitepad/resources/Templates';

// @todo Disabled until tests can be resolved.
xdescribe('PatientLangListRadioButton widget', () => {
    let questionnaires = LF.StudyDesign.questionnaires,
        modelQuestion = new Question({
            id: 'LogPad_Q_1',
            IG: 'LogPad',
            IT: 'LogPad_Q_1',
            text: 'QUESTION_1',
            className: 'LogPad_Q_1',
            widget: {
                id: 'DAILY_DIARY_W_0',
                type: 'PatientLangListRadioButton',
                className: 'DAILY_DIARY_W_0',
                templates: {},
                answers: []
            }
        }),
        modelDashboard = new Dashboard({
            instance_ordinal: 1
        }),
        viewQuestionnaire,
        viewQuestion,
        listWidget,
        template = `<div id='questionnaire-template'>
            <div id='questionnaire'></div>
            </div>`,
        modelWidget = new Widget({
            id: 'DAILY_DIARY_W_0',
            type: 'PatientLangListRadioButton',
            className: 'DAILY_DIARY_W_0',
            templates: {},
            answers: []
        });

    LF.StudyDesign.questionnaires = new Questionnaires([
        {
            id: 'DAILY_DIARY',
            SU: 'DAILY_DIARY',
            displayName: 'DISPLAY_NAME',
            className: 'DAILY_DIARY',
            previousScreen: true,
            screens: ['DAILY_DIARY_S_1']
        }
    ]);

    beforeAll(() => {
        helpers.installDatabase();
    });

    beforeEach((done) => {
        Data.Questionnaire = {};
        viewQuestionnaire = new QuestionnaireView({
            id: 'DAILY_DIARY'
        });
        viewQuestion = new QuestionView({
            id: modelQuestion.get('id'),
            screen: 'DAILY_DIARY',
            model: modelQuestion,
            parent: viewQuestionnaire,
            mandatory: false
        }),
        listWidget = new PatientLangListRadioButton({
            model: modelWidget,
            parent: viewQuestion
        });
        Data.Questionnaire = viewQuestionnaire.data;
        viewQuestionnaire.data.dashboard = modelDashboard;
        viewQuestionnaire.data.answers = new Answers();
        helpers.createSubject();
        helpers.saveSubject().then(() => {
            $('body').append(template);
            done();
        });

        $(viewQuestionnaire.$el).append(listWidget.parent.$el);
        listWidget.render();
    });

    afterEach(() => {
        // Data = {};
        $(viewQuestionnaire.$el).find('#DAILY_DIARY_W_0').remove();
    });

    afterAll((done) => {
        helpers.uninstallDatabase().finally(done);
    });

    it('should render the language list', () => {
        expect($(viewQuestionnaire.$el).find('.DAILY_DIARY_W_0').length).toEqual(1);
    });

    it('should have the same number of options as the languageList in participantSettings in the study design',
        (done) => {
            expect(listWidget.$el.find('input:radio').length)
            .toEqual(LF.StudyDesign.participantSettings.languageList.length);

            done();
        });
});
