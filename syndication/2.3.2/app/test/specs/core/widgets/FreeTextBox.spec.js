import Widget from 'core/models/Widget';

import 'test/helpers/StudyDesign';

import 'core/resources/Templates';
import WidgetBaseTests from './WidgetBase.specBase';

class FreeTextBoxTests extends WidgetBaseTests {
    static get model () {
        return new Widget({
            id: 'FTB_1',
            type: 'FreeTextBox',
            className: 'FTB_1',
            maxLength: 100
        });
    }

    htmlRenderedCheck () {
        const myModel = this.model,
            widget = new LF.Widget[myModel.get('type')]({
                model: myModel,
                mandatory: false,
                parent: this.dummyParent
            });

        return widget.render()
            .then(() => {
                // expect($(':input[type="number"]').length).toBe(1);
                expect($(`#${this.dummyParent.id} textarea`).length).toBe(1);
            });
    }
}

TRACE_MATRIX('US6121').
describe('FreeTextBox Widget', () => {
    let security,
        templates;

    beforeAll(() => {
        security = LF.security;
        LF.security = WidgetBaseTests.getDummySecurity();

        templates = LF.templates;
    });
    afterAll(() => {
        LF.security = security;
        LF.templates = templates;
    });

    let dummyParent;

    TRACE_MATRIX('US6749').
    describe('FreeTextBox', () => {
        const buildModel = () => {
            return new Widget({
                id: 'FTB_1',
                type: 'FreeTextBox',
                className: 'FTB_1',
                maxLength: 10,
                resize: 'none',
                rows: 5,
                showCount: false
            });
        };
        const baseTests = new FreeTextBoxTests(buildModel());

        baseTests.execTests();

        describe('Constructor', () => {
            beforeEach(() => {
                dummyParent = WidgetBaseTests.getMinDummyQuestion();
            });
            afterEach(() => {
                $(`#${dummyParent.getQuestionnaire().id}`).remove();
            });

            it('should not accept a missing "maxLength" property', () => {
                const model = new Widget({
                    id: 'FTB_1',
                    type: 'FreeTextBox',
                    className: 'FTB_1'
                }),
                    options = {
                    model: model,
                    mandatory: false,
                    parent: dummyParent
                };

                expect(() => { new LF.Widget[model.get('type')](options); })
                        .toThrow();
            });

            it('should not accept a "maxLength" property value that is not an integer', () => {
                const model = new Widget({
                    id: 'FTB_1',
                    type: 'FreeTextBox',
                    className: 'FTB_1',
                    maxLength: 'not_an_int'
                }),
                    options = {
                    model: model,
                    mandatory: false,
                    parent: dummyParent
                };

                expect(() => { new LF.Widget[model.get('type')](options); }).toThrow();
            });

            it('should not accept a "rows" property value that is not an integer', () => {
                const model = new Widget({
                    id: 'FTB_1',
                    type: 'FreeTextBox',
                    className: 'FTB_1',
                    maxLength: 50,
                    rows: 'foobar'
                }),
                    options = {
                    model: model,
                    mandatory: false,
                    parent: dummyParent
                };

                expect(() => { new LF.Widget[model.get('type')](options); }).toThrow();
            });

            it('should property set the "showCount" property to false when the model is false', () => {
                const model = new Widget({
                    id: 'FTB_1',
                    type: 'FreeTextBox',
                    className: 'FTB_1',
                    maxLength: 50,
                    showCount: false
                }),
                    options = {
                    model: model,
                    mandatory: false,
                    parent: dummyParent
                };

                let widget = new LF.Widget[model.get('type')](options);

                expect(widget.showCount).toBeFalsy();
            });

            it('should property set the "showCount" property to true when the model is true', () => {
                const model = new Widget({
                    id: 'FTB_1',
                    type: 'FreeTextBox',
                    className: 'FTB_1',
                    maxLength: 50,
                    showCount: true
                }),
                    options = {
                    model: model,
                    mandatory: false,
                    parent: dummyParent
                };

                let widget = new LF.Widget[model.get('type')](options);

                expect(widget.showCount).toBeTruthy();
            });
        });

        describe('CSS/Attribute handling', () => {
            beforeEach(() => {
                dummyParent = WidgetBaseTests.getMinDummyQuestion();
            });
            afterEach(() => {
                $(`#${dummyParent.getQuestionnaire().id}`).remove();
            });

            Async.it('should set the CSS attributes based on the model (resize == none)', () => {
                const model = buildModel(),
                    options = {
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    },
                    widget = new LF.Widget[model.get('type')](options);
                let input;

                return widget.render()
                    .then(() => {
                        input = $(`#${dummyParent.id} textarea`);

                        expect(input.css('resize')).toEqual('none');
                    });
            });

            Async.it('should set the element attributes based on the model (textarea rows == 5)', () => {
                const model = buildModel(),
                    options = {
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    },
                    widget = new LF.Widget[model.get('type')](options);
                let input;

                return widget.render()
                    .then(() => {
                        input = $(`#${dummyParent.id} textarea`);

                        expect(input.attr('rows')).toEqual('5');
                    });
            });

            Async.it('should hide the character count display if showCount == false', () => {
                const model = buildModel(),
                    options = {
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    },
                    widget = new LF.Widget[model.get('type')](options);
                let countDiv;

                return widget.render()
                    .then(() => {
                        countDiv = $(`#${model.id}-counter`);
                        expect(countDiv.css('visibility')).toEqual('hidden');
                    });
            });
        });

        describe('Input handling', () => {
            beforeEach(() => {
                dummyParent = WidgetBaseTests.getMinDummyQuestion();
            });
            afterEach(() => {
                $(`#${dummyParent.getQuestionnaire().id}`).remove();
            });

            Async.it('should process the response properly and set the answer', () => {
                const model = buildModel(),
                    options = {
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    },
                    widget = new LF.Widget[model.get('type')](options);
                let $input;

                return widget.render()
                    .then(() => {
                        $input = $($(`#${dummyParent.id} textarea`)[0]);
                        $input.val('some text');

                        const event = $.Event('keypress', {
                            which: 5,
                            keycode: 5,
                            target: $input,
                            currentTarget: $input
                        });

                        return widget.respond(event);
                    })
                    .then(() => {
                        expect(widget.answer).toBeDefined();
                        expect(widget.answer.get('response')).toEqual('some text');
                    });
            });

            Async.it('should validate an answer', () => {
                const model = buildModel(),
                    options = {
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    },
                    widget = new LF.Widget[model.get('type')](options);
                let $input;

                return widget.render()
                    .then(() => {
                        $input = $($(`#${dummyParent.id} textarea`)[0]);
                        $input.val('some text');

                        const event = $.Event('keypress', {
                            which: 5,
                            keycode: 5,
                            target: $input,
                            currentTarget: $input
                        });

                        return widget.respond(event);
                    })
                    .then(() => {
                        expect(widget.validate()).toBe(true);
                    });
            });

            Async.it('setCounter should update the correct HTML element', () => {
                const model = new Widget({
                    id: 'FTB_1',
                    type: 'FreeTextBox',
                    className: 'FTB_1',
                    maxLength: 10,
                    resize: 'none',
                    rows: 5,
                    showCount: true
                }),
                    options = {
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    },
                    widget = new LF.Widget[model.get('type')](options);

                return widget.render()
                    .then(() => {
                        widget.setCounter(50);
                        expect($(`#${model.id}-char-count`).html()).toEqual('50');
                    });
            });
        });
    });
});

