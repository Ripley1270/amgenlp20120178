import QuestionnaireView from 'core/views/BaseQuestionnaireView';
import Dashboard from 'core/models/Dashboard';
import Widget from 'core/models/Widget';
import Question from 'core/models/Question';
import QuestionView from 'core/views/QuestionView';
import Language from 'core/models/Language';
import Answers from 'core/collections/Answers';
import Questionnaires from 'core/collections/Questionnaires';

import Templates from 'core/resources/Templates';

import * as Helpers from 'core/Helpers';
import * as StudyDesign from 'test/helpers/StudyDesign';
import * as SpecHelpers from 'test/helpers/SpecHelpers';
import WidgetBaseTests from './WidgetBase.specBase';

class NumericRatingScaleCoreTests extends WidgetBaseTests {
    /**
     * checks to see that after a render, HTML is loaded by verifying the input with style scannerData is in the DOM
     * @returns {Q.Promise<void>} resolved when completed.  Compatible with Async.it
     */
    htmlRenderedCheck () {
        const myModel = this.model,
            widget = new LF.Widget[myModel.get('type')]({
                model: myModel,
                mandatory: false,
                parent: this.dummyParent
            });

        return widget.render()
            .then(() => {
                expect($(`#${this.dummyParent.id}`).find(`.NumericRatingScale`).length).toEqual(1);
            });
    };
}

/**
 *    NOTE: There is some lost code coverage when running karma with PhantomJS due to an outdated webkit
 *          This should be fixed in PhantomJS v2.0
 */
describe('NumericRatingScale', () => {

    const model = new Widget({
        id:'NRS_DIARY_W1',
        type:'NumericRatingScale',
        templates: { },
        reverseOnRtl: true,
        width: '85%',
        markers: {
            left: 'LEFT',
            right: 'RIGHT',
            justification: 'center',
            position: 'below'
        },
        answers: [
            { text: 'NRS_0', value: '0'},
            { text: 'NRS_1', value: '1'},
            { text: 'NRS_2', value: '2'},
            { text: 'NRS_3', value: '3'},
            { text: 'NRS_4', value: '4'},
            { text: 'NRS_5', value: '5'},
            { text: 'NRS_6', value: '6'},
            { text: 'NRS_7', value: '7'},
            { text: 'NRS_8', value: '8'},
            { text: 'NRS_9', value: '9'},
            { text: 'NRS_10', value: '10'}
        ]
    });

    const baseTests = new NumericRatingScaleCoreTests(model);
    baseTests.execTests();

    describe('instance methods', () => {

        let viewQuestionnaire,
            questionnaires = LF.StudyDesign.questionnaires,
            template = `<div id="questionnaire-template"><div id="questionnaire"></div></div>`,
            modelDashboard = new Dashboard({
                instance_ordinal: 1
            });
        LF.Data.Questionnaire = {};
        LF.StudyDesign.questionnaires = new Questionnaires([
            {
                id: 'NRS_Diary',
                SU: 'NRS',
                displayName: 'DISPLAY_NAME',
                className: 'NRS_Diary',
                previousScreen: true,
                screens: ['NRS_DIARY_S_1', 'NRS_DIARY_S_2', 'NRS_DIARY_S_3', 'NRS_DIARY_S_4', 'NRS_DIARY_S_5']
            }
        ]);
        viewQuestionnaire = new QuestionnaireView({
            id: 'NRS_Diary'
        });
        viewQuestionnaire.data.dashboard = modelDashboard;

        //CSS styles are needed for width tests

        //This might work with version 0.10 of karma instead of the longer method
        //$('<link rel="stylesheet" href="../../css/widgets/NumericRatingScale.css" media="screen" />').appendTo('head');

        $(`<style type="text/css">
        .NumericRatingScale {
            margin      : 0;
            list-style  : none;
            display: block; text - align: center;
        }
        .NumericRatingScale [data-container] {
            border: none;
            width: 100%;
            padding: 0;
            margin: auto;
        }
        .NumericRatingScale input {
            display: none;
        }
        .NumericRatingScale label {
            border: 1px solid #ccc;
            display: block;
            padding: 5px 0;
            margin-bottom: 10px;
            float: left;
            width: 10%;
        }
        .NumericRatingScale.ui-block-a {
            float: left;
        }
        .NumericRatingScale.ui-block-b {
            float: right;
        }
    </style>`).appendTo('head');

        beforeEach(() => {
            LF.security = {
                activeUser: {
                    get: () => {
                        return '1';
                    }
                }
            };

            LF.SpecHelpers.createSubject();

            spyOn(Helpers, 'checkInstall').and.callFake((callback) => {
                callback(true);
            });
            LF.Data.Questionnaire = {};
            LF.StudyDesign.questionnaires = new Questionnaires([
                {
                    id: 'NRS_Diary',
                    SU: 'NRS',
                    displayName: 'DISPLAY_NAME',
                    className: 'NRS_Diary',
                    previousScreen: true,
                    screens: ['NRS_DIARY_S_1', 'NRS_DIARY_S_2', 'NRS_DIARY_S_3', 'NRS_DIARY_S_4', 'NRS_DIARY_S_5']
                }
            ]);
            viewQuestionnaire = new QuestionnaireView({
                id: 'NRS_Diary'
            });
            viewQuestionnaire.data.dashboard = modelDashboard;
            viewQuestionnaire.data.answers = new Answers();
            viewQuestionnaire.data.igList = {NRS: 0};
            viewQuestionnaire.data.currentIGR = {NRS: 0};
        });

        afterEach(() => {
            LF.Data = {};
            LF.schedule = undefined;
            LF.security = undefined;
            LF.dataAccess = undefined;
            LF.StudyDesign.questionnaires = questionnaires;
        });

        baseTests.execTests();

        describe('With text labels positioned below and a center justification', () => {
            let modelWidget,
                modelQuestion,
                viewQuestion,
                widgetNRS;

            describe('Tests with no default selected answer', () => {

                beforeEach((done) => {
                    modelWidget = new Widget({
                        id: 'NRS_DIARY_W1',
                        type: 'NumericRatingScale',
                        templates: {},
                        width: '85%',
                        markers: {
                            left: 'LEFT',
                            right: 'RIGHT',
                            position: 'below'
                        },
                        answers: [
                            { text: '0', value: '0' },
                            { text: '1', value: '1' },
                            { text: '2', value: '2' },
                            { text: '3', value: '3' },
                            { text: '4', value: '4' },
                            { text: '5', value: '5' }
                        ]
                    });

                    modelQuestion = new Question({
                        id: 'NRS_Q_1',
                        IG: 'NRS',
                        IT: 'NRS_Q_1',
                        text: 'QUESTION_1',
                        className: 'NRS_Q_1',
                        widget: modelWidget
                    });

                    viewQuestion = new QuestionView({
                        id: modelQuestion.get('id'),
                        screen: 'NRS_DIARY_S_1',
                        model: modelQuestion,
                        parent: viewQuestionnaire,
                        mandatory: false
                    });

                    widgetNRS = new LF.Widget.NumericRatingScale({
                        model: modelWidget,
                        answers: new Answers(),
                        parent: viewQuestion
                    });

                    $('body').append(template);
                    $('#questionnaire').append(widgetNRS.question.el);
                    widgetNRS.render()
                        .catch(e => fail(e))
                        .done(done);
                });

                afterEach(() => {
                    $('#questionnaire-template').remove();
                });

                it('should create the NRS using the render method', () => {
                    expect(widgetNRS.$('[data-container]').first()).toBeDefined();
                    expect(widgetNRS.$('[data-container]').first().children('.btn').length).toEqual(6);
                });

                it('should display text labels', () => {
                    expect(widgetNRS.$el.children('.markers')).toBeDefined();
                    expect(widgetNRS.$el.children('.markers').children.length).toEqual(2);
                });

                it('click should call respond', () => {
                    spyOn(widgetNRS, 'respond');
                    widgetNRS.delegateEvents();
                    // When triggering click through code, you must trigger twice to register it
                    $('#NRS_DIARY_W1_radio_0').trigger('click');
                    $('#NRS_DIARY_W1_radio_0').trigger('click');

                    expect(widgetNRS.respond).toHaveBeenCalled();
                });
                Async.it('respond() sets the correct item', () => {
                    return widgetNRS.respond({target: '#NRS_DIARY_W1_radio_0'})
                        .then(() => {
                            expect(widgetNRS.answer.get('response')).toEqual('0');
                        });
                });
            });
            describe('tests with default answers', () => {
                beforeEach((done) => {
                    modelWidget = new Widget({
                        id: 'NRS_DIARY_W1',
                        type: 'NumericRatingScale',
                        templates: {},
                        width: '85%',
                        markers: {
                            left: 'LEFT',
                            right: 'RIGHT',
                            position: 'below'
                        },
                        answers: [
                            { text: '0', value: '0' },
                            { text: '1', value: '1' },
                            { text: '2', value: '2' },
                            { text: '3', value: '3' },
                            { text: '4', value: '4' },
                            { text: '5', value: '5' }
                        ],
                        answer: '1'
                    });

                    modelQuestion = new Question({
                        id: 'NRS_Q_1',
                        IG: 'NRS',
                        IT: 'NRS_Q_1',
                        text: 'QUESTION_1',
                        className: 'NRS_Q_1',
                        widget: modelWidget
                    });

                    viewQuestion = new QuestionView({
                        id: modelQuestion.get('id'),
                        screen: 'NRS_DIARY_S_1',
                        model: modelQuestion,
                        parent: viewQuestionnaire,
                        mandatory: false
                    });

                    widgetNRS = new LF.Widget.NumericRatingScale({
                        model: modelWidget,
                        answers: new Answers(),
                        parent: viewQuestion
                    });

                    $('body').append(template);
                    $('#questionnaire').append(widgetNRS.question.el);
                    widgetNRS.render()
                        .catch(e => fail(e))
                        .done(done);
                });

                afterEach(() => {
                    $('#questionnaire-template').remove();
                });

                // TODO: TA37973 Fix this
                xit('should render with the default selected answer', () => {
                    expect(widgetNRS.answer.get('response')).toEqual('1');
                    expect(widgetNRS.$('.btn-NRS_DIARY_W1_radio_1').hasClass('active')).toBeTruthy();
                });

                // TODO: TA37973 Fix this
                xit('should deselect old answer when new one is selected', () => {

                    // When triggering click through code, you must trigger twice to register it
                    $('#NRS_DIARY_W1_radio_1').trigger('click');
                    $('#NRS_DIARY_W1_radio_1').trigger('click');

                    expect(widgetNRS.answer.get('response')).toEqual('1');
                    expect($('.btn-NRS_DIARY_W1_radio_0').hasClass('active')).toBeFalsy();
                    expect($('.btn-NRS_DIARY_W1_radio_1').hasClass('active')).toBeTruthy();
                });

            });

            it('should display the markers below the NRS', () => {
                expect(widgetNRS.$el.children().first().hasClass('markers')).toBeFalsy();
                expect(widgetNRS.$el.children().last().hasClass('markers')).toBeTruthy();
            });

            it('should display the text labels with a center justification', () => {
                expect(widgetNRS.$el.children('.markers').hasClass('center')).toBeTruthy();
            });

        });

        describe('With text labels positioned below and an outside justification', () => {
            let modelWidget,
                modelQuestion,
                viewQuestion,
                widgetNRS;

            beforeEach((done) => {
                modelWidget = new Widget({
                    id: 'NRS_DIARY_W1',
                    type: 'NumericRatingScale',
                    templates: {},
                    width: '85',
                    markers: {
                        left: 'LEFT',
                        right: 'RIGHT',
                        justification: 'outside',
                        position: 'below'
                    },
                    answers: [
                        { text: '0', value: '0' },
                        { text: '1', value: '1' },
                        { text: '2', value: '2' },
                        { text: '3', value: '3' },
                        { text: '4', value: '4' },
                        { text: '5', value: '5' }
                    ]
                });

                modelQuestion = new Question({
                    id: 'NRS_Q_2',
                    IG: 'NRS',
                    IT: 'NRS_Q_2',
                    text: 'QUESTION_2',
                    className: 'NRS_Q_2',
                    widget: modelWidget
                });

                viewQuestion = new QuestionView({
                    id: modelQuestion.get('id'),
                    screen: 'NRS_DIARY_S_2',
                    model: modelQuestion,
                    parent: viewQuestionnaire,
                    mandatory: false
                });

                widgetNRS = new LF.Widget.NumericRatingScale({
                    model: modelWidget,
                    answers: new Answers(),
                    parent: viewQuestion
                });

                $('body').append(template);
                $('#questionnaire').append(widgetNRS.question.el);
                widgetNRS.render()
                    .catch(e => fail(e))
                    .done(done);
            });

            afterEach(() => {
                $('#questionnaire-template').remove();
            });

            it('should display the text labels below the NRS', () => {
                expect(widgetNRS.$el.children().first().hasClass('markers')).toBeFalsy();
                expect(widgetNRS.$el.children().last().hasClass('markers')).toBeTruthy();
            });

            it('should display the text labels with an outside justification', () => {
                expect(widgetNRS.$el.children('.markers').hasClass('outside')).toBeTruthy();
            });
        });

        describe('With text labels positioned above with an inside justification', () => {
            let modelWidget,
                modelQuestion,
                viewQuestion,
                widgetNRS;

            beforeEach((done) => {
                modelWidget = new Widget({
                    id: 'NRS_DIARY_W1',
                    type: 'NumericRatingScale',
                    templates: {},
                    width: '200px',
                    markers: {
                        left: 'LEFT',
                        right: 'RIGHT',
                        justification: 'inside',
                        position: 'above'
                    },
                    answers: [
                        { text: '0', value: '0' },
                        { text: '1', value: '1' },
                        { text: '2', value: '2' },
                        { text: '3', value: '3' },
                        { text: '4', value: '4' }
                    ]
                });

                modelQuestion = new Question({
                    id: 'NRS_Q_3',
                    IG: 'NRS',
                    IT: 'NRS_Q_3',
                    text: 'QUESTION_3',
                    className: 'NRS_Q_3',
                    widget: modelWidget
                });

                viewQuestion = new QuestionView({
                    id: modelQuestion.get('id'),
                    screen: 'NRS_DIARY_S_3',
                    model: modelQuestion,
                    parent: viewQuestionnaire,
                    mandatory: false
                });

                widgetNRS = new LF.Widget.NumericRatingScale({
                    model: modelWidget,
                    answers: new Answers(),
                    parent: viewQuestion
                });

                $('<style type="text/css"> .NumericRatingScale label{ \
                border        : none; \
            }</style>').appendTo('head');

                $('body').append(template);
                $('#questionnaire').append(widgetNRS.questionnaire.el);
                widgetNRS.render()
                    .catch(e => fail(e))
                    .done(done);
            });

            afterEach(() => {
                $('#questionnaire-template').remove();
            });

            it('should display the text labels above the NRS', () => {
                expect(widgetNRS.$el.children().first().hasClass('markers')).toBeTruthy();
                expect(widgetNRS.$el.children().last().hasClass('markers')).toBeFalsy();
            });

            it('should display the text labels with an inside justification', () => {
                expect(widgetNRS.$el.children('.markers').hasClass('inside')).toBeTruthy();
            });

        });

        describe('With a center justification using exact pixels for width', () => {
            let modelWidget,
                modelQuestion,
                viewQuestion,
                widgetNRS;

            beforeEach((done) => {
                modelWidget = new Widget({
                    id: 'NRS_DIARY_W1',
                    type: 'NumericRatingScale',
                    templates: {},
                    width: '200px',
                    markers: {
                        left: 'LEFT',
                        right: 'RIGHT',
                        justification: 'center',
                        position: 'above'
                    },
                    answers: [
                        { text: '0', value: '0' },
                        { text: '1', value: '1' },
                        { text: '2', value: '2' },
                        { text: '3', value: '3' },
                        { text: '4', value: '4' }
                    ]
                });

                modelQuestion = new Question({
                    id: 'NRS_Q_4',
                    IG: 'NRS',
                    IT: 'NRS_Q_4',
                    text: 'QUESTION_4',
                    className: 'NRS_Q_4',
                    widget: modelWidget
                });

                viewQuestion = new QuestionView({
                    id: modelQuestion.get('id'),
                    screen: 'NRS_DIARY_S_4',
                    model: modelQuestion,
                    parent: viewQuestionnaire,
                    mandatory: false
                });

                widgetNRS = new LF.Widget.NumericRatingScale({
                    model: modelWidget,
                    answers: new Answers(),
                    parent: viewQuestion
                });

                $('body').prepend(template);
                $('#questionnaire').append(widgetNRS.question.el);
                widgetNRS.render()
                    .catch(e => fail(e))
                    .done(done);
            });

            afterEach(() => {
                $('#questionnaire-template').remove();
            });

            it('should set the NRS width to 200px', () => {
                let contentWidth = widgetNRS.$('[data-container]').first().width() - widgetNRS.$('.NRS-Placeholder').width() * 2;
                expect(contentWidth).toEqual(200);
            });
        });

        describe('With an invalid justification', () => {
            let modelWidget,
                modelQuestion,
                viewQuestion,
                widgetNRS;

            beforeEach((done) => {
                modelWidget = new Widget({
                    id: 'NRS_DIARY_W1',
                    type: 'NumericRatingScale',
                    templates: {},
                    width: '80%',
                    markers: {
                        left: 'LEFT',
                        right: 'RIGHT',
                        justification: 'diagonal',
                        position: 'above'
                    },
                    answers: [
                        { text: '0', value: '0' },
                        { text: '1', value: '1' },
                        { text: '2', value: '2' },
                        { text: '3', value: '3' },
                        { text: '4', value: '4' },
                        { text: '5', value: '5' },
                        { text: '6', value: '6' },
                        { text: '7', value: '7' },
                        { text: '8', value: '8' },
                        { text: '9', value: '9' },
                        { text: '10', value: '10' }
                    ]
                });

                modelQuestion = new Question({
                    id: 'NRS_Q_5',
                    IG: 'NRS',
                    IT: 'NRS_Q_5',
                    text: 'QUESTION_5',
                    className: 'NRS_Q_5',
                    widget: modelWidget
                });

                viewQuestion = new QuestionView({
                    id: modelQuestion.get('id'),
                    screen: 'NRS_DIARY_S_5',
                    model: modelQuestion,
                    parent: viewQuestionnaire,
                    mandatory: false
                });

                widgetNRS = new LF.Widget.NumericRatingScale({
                    model: modelWidget,
                    answers: new Answers(),
                    parent: viewQuestion
                });

                $('body').prepend(template);
                $('#questionnaire').append(widgetNRS.question.el);
                widgetNRS.render()
                    .catch(e => fail(e))
                    .done(done);
            });

            afterEach(() => {
                $('#questionnaire-template').remove();
            });

            it('should not apply any justification', () => {
                expect(widgetNRS.$el.children('.markers').hasClass('inside')).toBeFalsy();
                expect(widgetNRS.$el.children('.markers').hasClass('outside')).toBeFalsy();
                expect(widgetNRS.$el.children('.markers').hasClass('center')).toBeFalsy();
            });
        });

        describe('with a RTL language', () => {
            let modelWidget = new Widget({
                    id: 'NRS_DIARY_W1',
                    type: 'NumericRatingScale',
                    templates: {},
                    width: '80%',
                    markers: {
                        left: 'LEFT',
                        right: 'RIGHT',
                        position: 'above'
                    },
                    answers: [
                        { text: '0', value: '0' },
                        { text: '1', value: '1' },
                        { text: '2', value: '2' },
                        { text: '3', value: '3' },
                        { text: '4', value: '4' },
                        { text: '5', value: '5' },
                        { text: '6', value: '6' },
                        { text: '7', value: '7' },
                        { text: '8', value: '8' },
                        { text: '9', value: '9' },
                        { text: '10', value: '10' }
                    ]
                }),

                modelQuestion = new Question({
                    id: 'NRS_Q_5',
                    IG: 'NRS',
                    IT: 'NRS_Q_5',
                    text: 'QUESTION_5',
                    className: 'NRS_Q_5',
                    widget: modelWidget
                }),

                viewQuestion = new QuestionView({
                    id: modelQuestion.get('id'),
                    screen: 'NRS_DIARY_S_5',
                    model: modelQuestion,
                    parent: viewQuestionnaire,
                    mandatory: false
                }),

                language = new Language({
                    namespace: 'CORE',
                    language: 'ar',
                    locale: 'EG',
                    direction: 'rtl',
                    resources: {}
                }),

                pref = {
                    lang: LF.Preferred.language,
                    loc: LF.Preferred.locale
                };

            describe('with reverseOnRtl not set', () => {
                let widgetNRS;

                it('should render NRS with RTL language', () => {
                    LF.strings.add(language);

                    LF.Preferred = {
                        language: 'ar',
                        locale: 'EG'
                    };

                    widgetNRS = new LF.Widget.NumericRatingScale({
                        model: modelWidget,
                        answers: new Answers(),
                        parent: viewQuestion
                    });
                });

                it('should have value of 0 for the first NRS selection', () => {
                    expect(widgetNRS.model.get('answers')[0].value).toEqual('0');
                });

                it('should have value of 10 for the last NRS selection', () => {
                    expect(widgetNRS.model.get('answers')[10].value).toEqual('10');
                });

                it('should have value of 5 for the fifth NRS selection', () => {
                    expect(widgetNRS.model.get('answers')[5].value).toEqual('5');
                });

                it('should have value of 7 for the seventh NRS selection', () => {
                    expect(widgetNRS.model.get('answers')[7].value).toEqual('7');
                });

                it('should remove RTL language and reset LF.Preferred', () => {
                    LF.strings.remove(language);

                    LF.Preferred = {
                        language: pref.lang,
                        locale: pref.loc
                    };
                });
            });

            describe('with reverseOnRtl set to true', () => {
                let widgetNRS;

                it('should render NRS with RTL language', () => {
                    modelWidget.set('reverseOnRtl', true);

                    LF.strings.add(language);
                    LF.Preferred = {
                        language: 'ar',
                        locale: 'EG'
                    };

                    widgetNRS = new LF.Widget.NumericRatingScale({
                        model: modelWidget,
                        answers: new Answers(),
                        parent: viewQuestion
                    });
                });

                it('should have value of 10 for the first NRS selection', () => {
                    expect(widgetNRS.model.get('answers')[0].value).toEqual('10');
                });

                it('should have value of 0 for the last NRS selection', () => {
                    expect(widgetNRS.model.get('answers')[10].value).toEqual('0');
                });

                it('should have value of 5 for the fifth NRS selection', () => {
                    expect(widgetNRS.model.get('answers')[5].value).toEqual('5');
                });

                it('should have value of 3 for the seventh NRS selection', () => {
                    expect(widgetNRS.model.get('answers')[7].value).toEqual('3');
                });

                it('should remove RTL language and reset LF.Preferred', () => {
                    LF.strings.remove(language);

                    LF.Preferred = {
                        language: pref.lang,
                        locale: pref.loc
                    };
                });

            });

        });
    });
});
