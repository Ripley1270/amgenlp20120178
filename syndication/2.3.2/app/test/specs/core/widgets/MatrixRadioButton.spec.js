import 'test/helpers/StudyDesign';
import Widget from 'core/models/Widget';
import WidgetBaseTests from './WidgetBase.specBase';

import 'core/resources/Templates';

export default class MatrixRadioButtonTests extends WidgetBaseTests {

    constructor (model = MatrixRadioButtonTests.model) {
        super(model);
    }

    static get model () {
        return new Widget({
            id: 'MATRIX_DIARY_W_1',
            type: 'MatrixRadioButton',
            className: 'MATRIX_DIARY_W_1',
            questions: [
                {
                    IT: 'MATRIX_DIARY_W_1_IT_1',
                    text: 'TEXT'
                },
                {
                    IT: 'MATRIX_DIARY_W_1_IT_2',
                    text: 'TEXT'
                }
            ],
            matrixAnswers: [
                {
                    text: 'ANSWER_1',
                    value: '0'
                }, {
                    text: 'ANSWER_1',
                    value: '1'
                }, {
                    text: 'ANSWER_1',
                    value: '2'
                }
            ]
        });
    }

    getMinDummyQuestion () {
        let dummy = super.getMinDummyQuestion();
        $('.question').addClass(this.model.get('className'));

        return dummy;
    }

    mandatoryTests () {
        const myModel = this.model;
        let widget = new LF.Widget[myModel.get('type')]({
                model: myModel,
                mandatory: true,
                parent: this.dummyParent
            });

        return widget.render()
            .then(() => {
               expect(widget.validate()).toBeFalsy('if widget not set and mandatory = true, then would expect validate() to be false.')
            });
    }

    htmlRenderedCheck () {
        const myModel = this.model,
            widget = new LF.Widget[myModel.get('type')]({
                model: myModel,
                mandatory: false,
                parent: this.dummyParent
            });

        // Necessary class addition specific to this widget due the way the sub-questions are rendered.
        $('.question').addClass(myModel.get('className'));

        return widget.render()
            .then(() => {
                expect($('.RadioButton').length).toBeGreaterThan(0);
            });
    }

    execTests () {
        super.execTests();

        TRACE_MATRIX('US6102').
        describe('MatrixRadioButton', () => {
            let templates,
                dummyParent,
                security,
                matrixWidget = null;

            beforeAll(() => {
                security = LF.security;
                LF.security = this.getDummySecurity();
                templates = LF.templates;
            });

            afterAll(() => {
                LF.security = security;
                LF.templates = templates;
            });

            beforeEach(() => {
                let className = this.model.get('className');
                dummyParent = this.getMinDummyQuestion();
                // Update the dummy question element to have the appropriate class
                $('.question').addClass(className);
            });

            afterEach(() => {
                $(`#${dummyParent.getQuestionnaire().id}`).remove();
            });

            describe('Default properties', () => {
                beforeEach(() => {
                    matrixWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: true,
                        parent: dummyParent
                    });
                });

                it('#questionTemplate', () => {
                    expect(matrixWidget.questionTemplate).toEqual('DEFAULT:MatrixQuestion');
                });

                it('#questionContainer', () => {
                    expect(matrixWidget.rowContainer).toEqual('DEFAULT:MatrixRowContainer');
                });

                it('#questionWrapper', () => {
                    expect(matrixWidget.wrapper).toEqual('DEFAULT:MatrixItemWrapper');
                });

                it('#questionLabel', () => {
                    expect(matrixWidget.questionLabel).toEqual('DEFAULT:MatrixRadioButtonLabel');
                });
            });

            describe('#build functions', () => {
                beforeEach(() => {
                    matrixWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: true,
                        parent: dummyParent
                    });
                });

                it('#buildModels', () => {
                    let models = matrixWidget.buildModels();
                    expect(models.length).toEqual(2);
                });

                it('#setTemplates', () => {
                    let templates = matrixWidget.setTemplates();

                    expect(templates.questionTemplate).toEqual('DEFAULT:MatrixQuestion');
                    expect(templates.container).toEqual('DEFAULT:MatrixRowContainer');
                    expect(templates.wrapper).toEqual('DEFAULT:MatrixItemWrapper');
                    expect(templates.questionLabel).toEqual('DEFAULT:MatrixRadioButtonLabel');
                });

                it('#buildConfig', () => {
                    let qModel = matrixWidget.buildConfig(matrixWidget.questions[0], matrixWidget.matrixAnswers);

                    expect(qModel.id).toEqual(matrixWidget.questions[0].IT);
                    expect(qModel.widget.type).toEqual('RadioButton');

                });

                it('#buildHeaderText', () => {
                    let text = matrixWidget.buildHeaderText(matrixWidget.matrixAnswers);

                    // Should be one more than the answer array total
                    expect(text.length).toEqual(4);
                });
            });

            describe('#render', () => {
                beforeEach(() => {
                    matrixWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: true,
                        parent: dummyParent
                    });
                });

                Async.it('should render a matrix with two radiobutton questions', () => {
                    return matrixWidget.render()
                        .then(() => {
                            let rbs = matrixWidget.$('.table-row');
                            expect(rbs.length).toEqual(3);
                        });
                });

                Async.it('should render the matrix with the default header background color', () => {
                    return matrixWidget.render()
                        .then(() => {
                            let header = matrixWidget.$('.table-row.header');
                            expect(header.css('background-color')).toEqual('rgb(242, 242, 242)');
                        });
                });

                Async.it('should render the matrix with the even rows having a background color of white', () => {
                    return matrixWidget.render()
                        .then(() => {
                            let rowColor = matrixWidget.$('div.question.table-row:nth-child(even)').first().css('background-color');
                            expect(rowColor).toEqual('rgb(255, 255, 255)');
                        });
                });

                Async.it('should render the matrix with the odd/alternating rows having a background color of light gray', () => {
                    return matrixWidget.render()
                        .then(() => {
                            let rowColor = matrixWidget.$('div.question.table-row:nth-child(odd)').first().css('background-color')
                            expect(rowColor).toEqual('rgb(251, 252, 252)');
                        });
                });

                Async.it('should render the matrix with a default question column width of 50%', () => {
                    return matrixWidget.render()
                        .then(() => {
                            let colWidth = matrixWidget.$('.matrix-question').first().css('width');

                            expect(colWidth).toEqual('203px');
                        });
                });
            });

            describe('#render custom options', () => {
                Async.it('should render a single row color if only rowColor is set', () => {
                    let model = this.model;
                    model.set('rowColor', 'red');
                    matrixWidget = new LF.Widget[model.get('type')]({
                        model: model,
                        mandatory: true,
                        parent: dummyParent
                    });

                    return matrixWidget.render()
                        .then(() => {
                            let rc = matrixWidget.$('div.question.table-row:nth-child(odd)').first().css('background-color');

                            expect(rc).toEqual('rgb(255, 0, 0)');
                        });
                });

                Async.it('should render a custom header row color', () => {
                    let model = this.model;
                    model.set('headerColor', 'green');
                    matrixWidget = new LF.Widget[model.get('type')]({
                        model: model,
                        mandatory: true,
                        parent: dummyParent
                    });

                    return matrixWidget.render()
                        .then(() => {
                            let hc = matrixWidget.$('.table-row.header').css('background-color');
                            expect(hc).toEqual('rgb(242, 242, 242)');
                        });
                });
            });
        });
    }
}

let tester = new MatrixRadioButtonTests();
tester.execTests();
