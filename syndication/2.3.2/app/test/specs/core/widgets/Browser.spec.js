import Widget from 'core/models/Widget';
import WidgetBaseTests from './WidgetBase.specBase';

import 'core/resources/Templates';

export default class BrowserTests extends WidgetBaseTests {

    static get model () {
        return new Widget({
            id: 'BW_1',
            type: 'Browser',
            className: 'BW_1',
            url: 'www.ert.com',
            linkText: 'Tap'
        });
    }

    static get model_noURL () {
        return new Widget({
            id: 'BW_1',
            type: 'Browser',
            className: 'BW_1'
        });
    }

    constructor (model = BrowserTests.model) {
        super(model);
    }

    htmlRenderedCheck () {
        const myModel = this.model,
            widget = new LF.Widget[myModel.get('type')]({
                model: myModel,
                mandatory: false,
                parent: this.dummyParent
            });

        return widget.render().then(() => {
            expect($(`#${widget.linkID}`).length).toBe(1);
        });
    }

    mandatoryTests () {
        const myModel = this.model;
        let widget = new LF.Widget[myModel.get('type')]({
                model: myModel,
                mandatory: false,
                parent: this.dummyParent
            });

        return widget.render()
            .then(() => {
                return Q.Promise((resolve) => {
                    widget.$(`#${widget.linkID}`).trigger('click').ready(() => {
                        resolve();
                    });
                });
            })
            .tap(() => {
                expect(widget.validate()).toBeTruthy('widget should always validate properly');
            });
    }

    execTests () {
        super.execTests();

        TRACE_MATRIX('US7178').
        describe('Browser Widget tests', () => {
            let security,
                templates,
                dummyParent,
                browserWidget;

            beforeAll(() => {
                security = LF.security;
                LF.security = WidgetBaseTests.getDummySecurity();
                templates = LF.templates;

            });

            afterAll(() => {
                LF.security = security;
                LF.templates = templates;
            });

            beforeEach(() => {
                dummyParent = this.getMinDummyQuestion();
            });

            afterEach(() => {
                $(`#${dummyParent.getQuestionnaire().id}`).remove();
                browserWidget = null;
            });

            describe('Default properties', () => {
                beforeEach(() => {
                    browserWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: false,
                        parent: dummyParent
                    });
                });

                describe('#defaultTemplate', () => {
                    it('should be DEFAULT:Browser', () => {
                        expect(browserWidget.defaultTemplate).toEqual('DEFAULT:Browser');
                    });
                });
            });

            describe('Constructor', () => {
                it('should throw an exception if the URL property is not properly set', () => {
                    let model = this.model;
                    model.unset('url');

                    expect(() => { new LF.Widget[model.get('type')]({
                        model: model,
                        mandatory: false,
                        parent: dummyParent
                    }); }).toThrow();
                });
            });

            describe('#respond', () => {
                Async.it('should log an answer value of 1 if the link is clicked', () => {
                    const myModel = this.model;
                    let widget = new LF.Widget[myModel.get('type')]({
                            model: myModel,
                            mandatory: false,
                            parent: this.dummyParent
                        });

                    return widget.render()
                        .then(() => {
                            return Q.Promise((resolve) => {
                                widget.$(`#${widget.linkID}`).trigger('click').ready(() => {
                                    resolve();
                                });
                            });
                        })
                        .tap(() => {
                            expect(widget.answer.get('response')).toEqual('1');
                        });
                });
            });
        });
    }
}

let tests =  new BrowserTests();
tests.execTests();
