import * as helpers from 'test/helpers/SpecHelpers';
import 'test/helpers/StudyDesign';

import Questionnaires from 'core/collections/Questionnaires';
import Answers from 'core/collections/Answers';
import Question from 'core/models/Question';
import Dashboard from 'core/models/Dashboard';
import QuestionnaireView from 'core/views/QuestionnaireView';
import QuestionView from 'core/views/QuestionView';
import Widget from 'core/models/Widget';
import NumberSpinner from 'core/widgets/NumberSpinner';
import User from 'core/models/User';
import Session from 'core/classes/Session';
import Subject from 'core/models/Subject';
import WidgetBaseTests from './WidgetBase.specBase';
import Templates from 'core/resources/Templates';
import Languages from 'core/collections/Languages';
import SelectWidgetBase from 'core/widgets/SelectWidgetBase';
import WidgetBase from 'core/widgets/WidgetBase';

/*global describe, it, beforeEach, afterEach, beforeAll, afterAll, spyOn */
export default class SelectWidgetBaseTests extends WidgetBaseTests {
    constructor (model) {
        if (!(model instanceof Widget)) {
            throw 'Invalid use of SelectWidgetBaseTests.  Must be tested with an implementation of the abstract class.';
        }
        super(model);
    }

    execTests () {
        super.execTests();

        describe('SelectWidgetBase', () => {
            let templates,
                dummyParent,
                security,
                selectWidget = null;

            beforeAll(() => {
                security = LF.security;
                LF.security = this.getDummySecurity();
                templates = LF.templates;
            });

            afterAll(() => {
                LF.security = security;
                LF.templates = templates;
            });

            beforeEach(() => {
                dummyParent = this.getMinDummyQuestion();
            });

            afterEach(() => {
                $(`#${dummyParent.getQuestionnaire().id}`).remove();
                $(`.modal`).remove();
                $(`.modal-backdrop`).remove();
                selectWidget = null;
            });

            describe('getters', () => {
                let baseSelect;

                describe('#wrapper', () => {
                    it('defaults correctly', () => {
                        baseSelect = new SelectWidgetBase({
                            model: this.model,
                            mandatory: false,
                            parent: dummyParent
                        });
                        expect(baseSelect.wrapper).toBe('DEFAULT:FormGroup');
                    });
                });

                describe('#label', () => {
                    it('defaults correctly', () => {
                        baseSelect = new SelectWidgetBase({
                            model: this.model,
                            mandatory: false,
                            parent: dummyParent
                        });
                        expect(baseSelect.label).toBe('DEFAULT:Label');
                    });
                });

                describe('#input', () => {
                    it('defaults correctly', () => {
                        baseSelect = new SelectWidgetBase({
                            model: this.model,
                            mandatory: false,
                            parent: dummyParent
                        });
                        expect(baseSelect.input).toBe('DEFAULT:Input');
                    });
                });
            });
            describe('#constructor', () => {
                it('sets validation based on the model', () => {
                    let customModel = _.extend({}, this.model);

                    customModel.attributes = _.extend(customModel.attributes, {
                        validation: 'test'
                    });
                    selectWidget = new LF.Widget[customModel.get('type')]({
                        model: customModel,
                        mandatory: false,
                        parent: dummyParent
                    });
                    expect(selectWidget.validation).toBe('test');
                });

                it('creates events', () => {
                    selectWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: false,
                        parent: dummyParent
                    });
                    expect(selectWidget.events['change select']).toBe('respond');
                    expect(selectWidget.events['click select']).toBe('respond');
                });
            });
            describe('#render', () => {
                beforeEach(() => {
                    selectWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: false,
                        parent: dummyParent
                    });
                    spyOn(selectWidget, 'i18n').and.callFake((strings) => {
                        _.keys(strings).forEach((string) => {
                            strings[string] = `translated: ${strings[string]}`;
                        });
                        return Q.resolve(strings);
                    });
                });

                Async.it('inserts a translated label', () => {
                    selectWidget.model.set('label', 'test label');
                    return selectWidget.render().tap(() => {
                        expect(selectWidget.$(`label`).html()).toBe(`translated: test label`);
                    });
                });

                Async.it('inserts a select element', () => {
                    return selectWidget.render().tap(() => {
                        expect(selectWidget.$('select').length).toBe(1);
                    });
                });

                Async.it('calls renderOptions()', () => {
                    spyOn(selectWidget, 'renderOptions').and.callThrough();
                    return selectWidget.render().tap(() => {
                        expect(selectWidget.renderOptions.calls.count()).toBe(1);
                        expect(selectWidget.renderOptions).toHaveBeenCalledWith(`#${this.model.get('id')}`);
                    });
                });

                Async.it('calls initializeSelect2()', () => {
                    spyOn(selectWidget, 'initializeSelect2').and.callThrough();
                    return selectWidget.render().tap(() => {
                        expect(selectWidget.initializeSelect2.calls.count()).toBe(1);
                    });
                });
            });

            describe('#initializeSelect2', () => {
                beforeEach(() => {
                    selectWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: false,
                        parent: dummyParent
                    });
                    spyOn(selectWidget, 'i18n').and.callFake((strings) => {
                        _.keys(strings).forEach((string) => {
                            strings[string] = `translated: ${strings[string]}`;
                        });
                        return Q.resolve(strings);
                    });
                });

                Async.it('calls select2 constructor', () => {
                    spyOn($.fn, 'select2').and.callThrough();
                    spyOn(selectWidget, 'initializeSelect2').and.callThrough();
                    return selectWidget.render().tap(() => {
                        expect(selectWidget.initializeSelect2.calls.count()).toBe(1);
                        expect($.fn.select2.calls.count()).toBe(1);
                    });
                });

                Async.it('delegates events', () => {
                    selectWidget.needToRespond = 'test';

                    return selectWidget.render().then(() => {
                        spyOn(selectWidget, 'delegateEvents');
                        return selectWidget.initializeSelect2();
                    }).tap(() => {
                        expect(selectWidget.delegateEvents.calls.count()).toBe(1);
                    });
                });
            });

            describe('#respond()', () => {
                Async.beforeEach(() => {
                    selectWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: false,
                        parent: dummyParent
                    });
                    spyOn(selectWidget, 'i18n').and.callFake((strings) => {
                        _.keys(strings).forEach((string) => {
                            strings[string] = `translated: ${strings[string]}`;
                        });
                        return Q.resolve(strings);
                    });

                    return selectWidget.render();
                });

                Async.it('sets completed to true', () => {
                    let e = $.Event('change', {target: $(`#${selectWidget.id}`)});
                    return selectWidget.respond(e).then(() => {
                        expect(selectWidget.completed).toBe(true);
                    });
                });

                Async.it('adds an answer if none', () => {
                    let $input = selectWidget.$(`#${selectWidget.id}`),
                        e = $.Event('change', {target: $input});

                    // do not auto fire events (might fire respond twice)
                    selectWidget.undelegateEvents();

                    $input.val(3);
                    selectWidget.answers.clear();
                    return selectWidget.respond(e).then(() => {
                        expect(selectWidget.answers.size()).toBe(1);
                    });
                });

                Async.it('skips adding an answer if there is one', () => {
                    let $input = selectWidget.$(`#${selectWidget.id}`),
                        e = $.Event('change', {target: $input});

                    // do not auto fire events (might fire respond twice)
                    selectWidget.undelegateEvents();

                    $input.val(3);
                    selectWidget.answer = selectWidget.addAnswer();
                    spyOn(selectWidget, 'addAnswer');
                    return selectWidget.respond(e).then(() => {
                        expect(selectWidget.addAnswer.calls.count()).toBe(0);
                    });
                });

                Async.it('calls super.respondHelper()', () => {
                    let $input = selectWidget.$(`#${selectWidget.id}`),
                        e = $.Event('change', {target: $input});

                    // do not auto fire events (might fire respond twice)
                    selectWidget.undelegateEvents();

                    $input.val(3);
                    selectWidget.answer = selectWidget.addAnswer();
                    spyOn(WidgetBase.prototype, 'respondHelper').and.callThrough();
                    return selectWidget.respond(e).then(() => {
                        expect(WidgetBase.prototype.respondHelper.calls.count()).toBe(1);
                    });
                });
            });
        });
    }
}