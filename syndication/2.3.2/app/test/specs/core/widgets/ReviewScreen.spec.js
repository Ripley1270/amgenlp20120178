// function() is required rather than fat arrow functions for "this" context to be correct in the testing framework.
// jscs:disable requireArrowFunctions
/*global describe, it, beforeEach, spyOn, assert, afterEach, expect, xdescribe, beforeAll, afterAll */
//jscs:disable requireArrowFunctions
import * as helpers from 'test/helpers/SpecHelpers';
import 'test/helpers/StudyDesign';

import Widget from 'core/models/Widget';
import ReviewScreen from 'core/widgets/ReviewScreen';
import WidgetBaseTests from './WidgetBase.specBase';
import Templates from 'core/collections/Templates';

const model = new Widget({
    type: 'ReviewScreen',
    id: 'testWidgetId'
});
class ReviewScreenTests extends WidgetBaseTests {

    constructor() {
        super(model);
    }

    /**
     * disable mandatory testing.   it is always valid.
     * @returns {Q.Promise<void>}
     */
    mandatoryTests() {
        return Q();
    }

    execTexts () {
        super.execTests();

        describe('ReviewScreen specific', () => {
            const template = `<div id="questionnaire-template">
            <div id="questionnaire"></div>
            </div>`;
            let testBtns = null,
                testItems = null;

            let security, templates;

            beforeAll(function () {
                security = LF.security;
                LF.security = WidgetBaseTests.getDummySecurity();
                templates = LF.templates;
                LF.templates = new Templates([
                    {
                        name: 'ReviewScreen',
                        namespace: 'DEFAULT',
                        template: `<div id="{{ id }}"><ul id="{{ id }}_Items" data-role="list          view"></ul>
                              <hr />
                              <ul id="{{ id }}_Buttons" data-role="listview"></ul></div>`
                    }, {
                        name: 'ReviewScreenItems',
                        namespace: 'DEFAULT',
                        template: '<li><div>{{ text0 }}</div></li>'
                    }, {
                        name: 'ReviewScreenButtons',
                        namespace: 'DEFAULT',
                        template: '<a data-role="button">{{ text }}</a>'
                    }
                ]);

                $('body').append(template);
            });

            afterAll(function () {
                $('body').remove(template);
                LF.security = security;
                LF.templates = templates;
            });

            beforeEach(function () {
                testBtns = [
                    {
                        id: '0',
                        text: 'Test 0',
                        actionFunction: 'test0Func',
                        availability: 'always'
                    },
                    {
                        id: '1',
                        text: 'Test 1',
                        availability: 'sometimes?',
                        actionFunction: 'test1Func'
                    },
                    {
                        id: '2',
                        text: 'Test 2 (no action)',
                        availability: 'always'
                    },
                    {
                        id: '3',
                        text: 'Test 3',
                        actionFunction: 'test3Func',
                        availability: 'customFalseFunc'
                    },
                    {
                        id: '4',
                        text: 'Test 4',
                        actionFunction: 'test4Func',
                        availability: 'customTrueFunc'
                    },
                    {
                        id: '5',
                        text: 'Test (excluded) 5',
                        actionFunction: 'test5Func',
                        availability: 'customTrueFunc'
                    },
                    {
                        id: '6',
                        text: 'Test (null availability) 6',
                        actionFunction: 'test6Func',
                        availability: null
                    }
                ];

                testItems = [
                    {
                        id: 'test1',
                        text: ['aspirin', '300mg', 'Daily'],
                        exclude: '5'
                    },
                    {
                        id: 'test2',
                        text: ['ibuprophen', '200mg', 'Every 4 Hours'],
                        exclude: null
                    }
                ];
            });

            describe('ReviewScreen-specific tests', function () {
                it('should render an item list', function (done) {
                    const model = new Widget({
                            type: 'ReviewScreen',
                            id: 'reviewScreen'
                        }),
                        reviewScreenWidget = new LF.Widget[model.get('type')]({
                            model: model,
                            mandatory: false,
                            parent: WidgetBaseTests.getMinDummyQuestion()
                        });
                    Q()
                        .then(function () {
                            return reviewScreenWidget.render();
                        })
                        .then(function () {
                            expect(reviewScreenWidget.$(`#${model.get('id')}`)
                                .find(`#${model.get('id')}_Items`).length)
                                .toEqual(1);
                        })
                        .catch(function (e) {
                            fail(e);
                        })
                        .done(() => {
                            reviewScreenWidget.remove();
                            done();
                        });

                });

                it('should render an button list', function (done) {
                    const model = new Widget({
                            type: 'ReviewScreen',
                            id: 'reviewScreen'
                        }),
                        reviewScreenWidget = new LF.Widget[model.get('type')]({
                            model: model,
                            mandatory: false,
                            parent: WidgetBaseTests.getMinDummyQuestion()
                        });
                    Q()
                        .then(function () {
                            return reviewScreenWidget.render();
                        })
                        .then(function () {
                            expect(reviewScreenWidget.$(`#${model.get('id')}`)
                                .find(`#${model.get('id')}_Buttons`).length)
                                .toEqual(1);
                        })
                        .catch(function (e) {
                            fail(e);
                        })
                        .done(() => {
                            reviewScreenWidget.remove();
                            done();
                        });

                });

                describe('#processScreenFunction()', function () {
                    it('adds items to reviewScreenList', function (done) {
                        const model = new Widget({
                                type: 'ReviewScreen',
                                id: 'reviewScreen'
                            }),
                            reviewScreenWidget = new LF.Widget[model.get('type')]({
                                model: model,
                                mandatory: false,
                                parent: WidgetBaseTests.getMinDummyQuestion()
                            });

                        let screenFunc = () => ['item 1', 'item 2', 'item 3', 'item 4'];

                        // Just make it blank for now
                        spyOn(reviewScreenWidget, 'addItem').and.callFake((item) => {
                            return item;
                        });

                        reviewScreenWidget.render()
                            .then(() => {
                                return reviewScreenWidget.processScreenFunction(screenFunc);
                            })
                            .then(() => {
                                expect(reviewScreenWidget.reviewScreenList.length).toBe(4);
                            })
                            .catch((e) => {
                                fail(e);
                            })
                            .done(() => {
                                reviewScreenWidget.remove();
                                done();
                            });
                    });
                    it('adds result of addItem for each to DOM', function (done) {
                        const model = new Widget({
                                type: 'ReviewScreen',
                                id: 'reviewScreen'
                            }),
                            reviewScreenWidget = new LF.Widget[model.get('type')]({
                                model: model,
                                mandatory: false,
                                parent: WidgetBaseTests.getMinDummyQuestion()
                            });

                        let screenFunc = () => ['item 1', 'item 2', 'item 3', 'item 4'];

                        // Make it do something very simple and predictable.
                        spyOn(reviewScreenWidget, 'addItem').and.callFake((item) => {
                            return `<div class='item'>${item}</div>`;
                        });

                        reviewScreenWidget.render()
                            .then(() => {
                                return reviewScreenWidget.processScreenFunction(screenFunc);
                            })
                            .then(() => {
                                expect(reviewScreenWidget.$(`#${model.get('id')}_Items`).find('.item').length).toBe(4);
                            })
                            .catch((e) => {
                                fail(e);
                            })
                            .done(() => {
                                reviewScreenWidget.remove();
                                done();
                            });
                    });
                });

                describe('#addDefaultButtons', function () {
                    it('adds buttons but ignores unavailable ones', function (done) {
                        const model = new Widget({
                                type: 'ReviewScreen',
                                id: 'reviewScreen'
                            }),
                            reviewScreenWidget = new LF.Widget[model.get('type')]({
                                model: model,
                                mandatory: false,
                                parent: WidgetBaseTests.getMinDummyQuestion()
                            });

                        let addedBtnIds = [],
                            customFalseFunc = () => {
                                return Q.Promise.resolve(false);
                            },
                            customTrueFunc = () => {
                                return Q.Promise.resolve(true);
                            };

                        // Back up values of customFalseFunc and customTrueFunc so we can clean them up later
                        let oldFalse = LF.Widget[model.get('type')].customFalseFunc;
                        let oldTrue = LF.Widget[model.get('type')].customTrueFunc;

                        LF.Widget[model.get('type')].customFalseFunc = customFalseFunc;
                        LF.Widget[model.get('type')].customTrueFunc = customTrueFunc;

                        reviewScreenWidget.render()
                            .then(() => {
                                // Prepare mocked models and functions for test.
                                reviewScreenWidget.model.set('buttons', testBtns);

                                spyOn(reviewScreenWidget, 'addBtn').and.callFake((btnVal) => {
                                    addedBtnIds.push(btnVal.id);
                                    return Q.Promise.resolve();
                                });

                                return reviewScreenWidget.addDefaultButtons(['5']);
                            })
                            .then(() => {
                                expect(reviewScreenWidget.addBtn.calls.count()).toBe(3);
                            })
                            .catch((e) => {
                                fail(e);
                            })
                            .done(() => {
                                LF.Widget[model.get('type')].customTrueFunc = oldTrue;
                                LF.Widget[model.get('type')].customFalseFunc = oldFalse;
                                reviewScreenWidget.remove();
                                done();
                            });
                    });
                });

                describe('removeBtns', function () {
                    it('empties buttons array and calls remove to remove from dom', function (done) {
                        const model = new Widget({
                                type: 'ReviewScreen',
                                id: 'reviewScreen'
                            }),
                            reviewScreenWidget = new LF.Widget[model.get('type')]({
                                model: model,
                                mandatory: false,
                                parent: WidgetBaseTests.getMinDummyQuestion()
                            });

                        let
                            customFalseFunc = () => {
                                return Q.Promise.resolve(false);
                            },
                            customTrueFunc = () => {
                                return Q.Promise.resolve(true);
                            };

                        // Back up values of customFalseFunc and customTrueFunc so we can clean them up later
                        let oldFalse = LF.Widget[model.get('type')].customFalseFunc;
                        let oldTrue = LF.Widget[model.get('type')].customTrueFunc;

                        LF.Widget[model.get('type')].customFalseFunc = customFalseFunc;
                        LF.Widget[model.get('type')].customTrueFunc = customTrueFunc;

                        reviewScreenWidget.render()
                            .then(() => {
                                // Prepare mocked models and functions for test.
                                reviewScreenWidget.model.set('buttons', testBtns);
                                reviewScreenWidget.customFalseFunc = customFalseFunc;
                                reviewScreenWidget.customTrueFunc = customTrueFunc;

                                return reviewScreenWidget.addDefaultButtons(['5']);
                            })
                            .then(() => {
                                expect(reviewScreenWidget.$el.find('.button').length).toBe(3);
                                expect(reviewScreenWidget.buttons.length).toBe(3);
                                reviewScreenWidget.removeBtns();
                                expect(reviewScreenWidget.buttons.length).toBe(0);
                                expect(reviewScreenWidget.$el.find('.button').length).toBe(0);
                            })
                            .catch((e) => {
                                fail(e);
                            })
                            .done(() => {
                                LF.Widget[model.get('type')].customTrueFunc = oldTrue;
                                LF.Widget[model.get('type')].customFalseFunc = oldFalse;
                                reviewScreenWidget.remove();
                                done();
                            });
                    });
                });
                describe('#addBtn', function () {
                    it('adds a button to buttons array and dom', function (done) {
                        const model = new Widget({
                                type: 'ReviewScreen',
                                id: 'reviewScreen'
                            }),
                            reviewScreenWidget = new LF.Widget[model.get('type')]({
                                model: model,
                                mandatory: false,
                                parent: WidgetBaseTests.getMinDummyQuestion()
                            });

                        reviewScreenWidget.render()
                            .then(() => {
                                return Q.allSettled([
                                    reviewScreenWidget.addBtn(testBtns[0]),
                                    reviewScreenWidget.addBtn(testBtns[1])
                                ]);
                            })
                            .then(() => {
                                expect(reviewScreenWidget.$el.find('.button').length).toBe(2);
                                expect(reviewScreenWidget.buttons.length).toBe(2);
                            })
                            .catch((e) => {
                                fail(e);
                            })
                            .done(() => {
                                reviewScreenWidget.remove();
                                done();
                            });
                    });
                });

                describe('#addItem', function () {
                    it('adds a item to dom with correct data-id', function (done) {
                        const model = new Widget({
                                type: 'ReviewScreen',
                                id: 'reviewScreen'
                            }),
                            reviewScreenWidget = new LF.Widget[model.get('type')]({
                                model: model,
                                mandatory: false,
                                parent: WidgetBaseTests.getMinDummyQuestion()
                            });

                        let $itemContainer;

                        reviewScreenWidget.render()
                            .then(() => {
                                $itemContainer = reviewScreenWidget.$(`#${reviewScreenWidget.model.get('id')}_Items`);
                                for (let i = 0; i < testItems.length; ++i) {
                                    reviewScreenWidget.reviewScreenList.push(testItems[i]);
                                    $itemContainer.append(reviewScreenWidget.addItem(testItems[i]));
                                }
                            })
                            .then(() => {
                                let $items = $itemContainer.find('.item');
                                expect(reviewScreenWidget.$el.find('.item').length).toBe(2);
                                expect($($items[0]).data('id')).toBe('test1');
                                expect($($items[1]).data('id')).toBe('test2');
                            })
                            .catch((e) => {
                                fail(e);
                            })
                            .done(() => {
                                reviewScreenWidget.remove();
                                done();
                            });
                    });
                });

                describe('#itemSelected', function () {
                    it('calls removeBtns, selects an item, default buttons with exclusions', function (done) {
                        const model = new Widget({
                                type: 'ReviewScreen',
                                id: 'reviewScreen',
                                buttons: testBtns
                            }),
                            reviewScreenWidget = new LF.Widget[model.get('type')]({
                                model: model,
                                mandatory: false,
                                parent: WidgetBaseTests.getMinDummyQuestion()
                            });

                        let $itemContainer,
                            $target;

                        reviewScreenWidget.render()
                            .then(() => {
                                $itemContainer = reviewScreenWidget.$(`#${reviewScreenWidget.model.get('id')}_Items`);
                                for (let i = 0; i < testItems.length; ++i) {
                                    reviewScreenWidget.reviewScreenList.push(testItems[i]);
                                    $itemContainer.append(reviewScreenWidget.addItem(testItems[i]));
                                }
                            })
                            .then(() => {
                                $target = reviewScreenWidget.$el.find('.item').first();
                                let e = $.Event('click', {target: $target, currentTarget: $target});

                                spyOn(reviewScreenWidget, 'removeBtns').and.callThrough();
                                spyOn(reviewScreenWidget, 'addDefaultButtons').and.callThrough();
                                return reviewScreenWidget.itemSelected(e);
                            })
                            .then(() => {
                                expect(reviewScreenWidget.removeBtns).toHaveBeenCalled();
                                expect(reviewScreenWidget.activeItem.data('id')).toBe('test1');
                                expect(reviewScreenWidget.addDefaultButtons).toHaveBeenCalledWith(testItems[0].exclude);
                                expect($target.hasClass('selected')).toBe(true);
                            })
                            .catch((e) => {
                                fail(e);
                            })
                            .done(() => {
                                reviewScreenWidget.remove();
                                done();
                            });
                    });

                    it('adds buttons that had a null availability', function (done) {
                        const model = new Widget({
                                type: 'ReviewScreen',
                                id: 'reviewScreen',
                                buttons: testBtns
                            }),
                            reviewScreenWidget = new LF.Widget[model.get('type')]({
                                model: model,
                                mandatory: false,
                                parent: WidgetBaseTests.getMinDummyQuestion()
                            });

                        let $itemContainer,
                            $target;

                        reviewScreenWidget.render()
                            .then(() => {
                                $itemContainer = reviewScreenWidget.$(`#${reviewScreenWidget.model.get('id')}_Items`);
                                for (let i = 0; i < testItems.length; ++i) {
                                    reviewScreenWidget.reviewScreenList.push(testItems[i]);
                                    $itemContainer.append(reviewScreenWidget.addItem(testItems[i]));
                                }
                            })
                            .then(() => {
                                $target = reviewScreenWidget.$el.find('.item').first();
                                let e = $.Event('click', {target: $target, currentTarget: $target});

                                spyOn(reviewScreenWidget, 'addBtn').and.callThrough();
                                return reviewScreenWidget.itemSelected(e);
                            })
                            .then(() => {
                                expect(reviewScreenWidget.addBtn).toHaveBeenCalledWith(testBtns[6]);
                            })
                            .catch((e) => {
                                fail(e);
                            })
                            .done(() => {
                                reviewScreenWidget.remove();
                                done();
                            });
                    });

                    it('deselects a selected value on the second click, displays buttons with no exclusions', function (done) {
                        const model = new Widget({
                                type: 'ReviewScreen',
                                id: 'reviewScreen',
                                buttons: testBtns
                            }),
                            reviewScreenWidget = new LF.Widget[model.get('type')]({
                                model: model,
                                mandatory: false,
                                parent: WidgetBaseTests.getMinDummyQuestion()
                            });

                        let $itemContainer,
                            $target;

                        reviewScreenWidget.render()
                            .then(() => {
                                $itemContainer = reviewScreenWidget.$(`#${reviewScreenWidget.model.get('id')}_Items`);
                                for (let i = 0; i < testItems.length; ++i) {
                                    reviewScreenWidget.reviewScreenList.push(testItems[i]);
                                    $itemContainer.append(reviewScreenWidget.addItem(testItems[i]));
                                }
                            })
                            .then(() => {
                                $target = reviewScreenWidget.$el.find('.item').first();
                                let e = $.Event('click', {target: $target, currentTarget: $target});
                                return reviewScreenWidget.itemSelected(e);
                            })
                            .then(() => {
                                spyOn(reviewScreenWidget, 'addDefaultButtons').and.callThrough();
                                let e = $.Event('click', {target: $target, currentTarget: $target});
                                return reviewScreenWidget.itemSelected(e);
                            })
                            .then(() => {
                                expect(reviewScreenWidget.activeItem).toBe(null);
                                expect(reviewScreenWidget.addDefaultButtons).toHaveBeenCalledWith([]);
                                expect($target.hasClass('selected')).toBe(false);
                            })
                            .catch((e) => {
                                fail(e);
                            })
                            .done(() => {
                                reviewScreenWidget.remove();
                                done();
                            });
                    });

                    it('can select a new value, removes class from previously selected', function (done) {
                        const model = new Widget({
                                type: 'ReviewScreen',
                                id: 'reviewScreen',
                                buttons: testBtns
                            }),
                            reviewScreenWidget = new LF.Widget[model.get('type')]({
                                model: model,
                                mandatory: false,
                                parent: WidgetBaseTests.getMinDummyQuestion()
                            });

                        let $itemContainer,
                            $oldTarget,
                            $newTarget;

                        reviewScreenWidget.render()
                            .then(() => {
                                $itemContainer = reviewScreenWidget.$(`#${reviewScreenWidget.model.get('id')}_Items`);
                                for (let i = 0; i < testItems.length; ++i) {
                                    reviewScreenWidget.reviewScreenList.push(testItems[i]);
                                    $itemContainer.append(reviewScreenWidget.addItem(testItems[i]));
                                }
                            })
                            .then(() => {
                                $oldTarget = reviewScreenWidget.$el.find('.item').first();
                                let e = $.Event('click', {target: $oldTarget, currentTarget: $oldTarget});
                                return reviewScreenWidget.itemSelected(e);
                            })
                            .then(() => {
                                $newTarget = reviewScreenWidget.$el.find('.item:nth-child(2)');
                                let e = $.Event('click', {target: $newTarget, currentTarget: $newTarget});
                                return reviewScreenWidget.itemSelected(e);
                            })
                            .then(() => {
                                expect(reviewScreenWidget.activeItem.data('id')).toBe(testItems[1].id);
                                expect($oldTarget.hasClass('selected')).toBe(false);
                                expect($newTarget.hasClass('selected')).toBe(true);
                            })
                            .catch((e) => {
                                fail(e);
                            })
                            .done(() => {
                                reviewScreenWidget.remove();
                                done();
                            });
                    });
                });

                describe('#itemSelectedEvent', function () {
                    it('Calls itemSelected with our event param', function (done) {
                        const model = new Widget({
                                type: 'ReviewScreen',
                                id: 'reviewScreen',
                                buttons: testBtns
                            }),
                            reviewScreenWidget = new LF.Widget[model.get('type')]({
                                model: model,
                                mandatory: false,
                                parent: WidgetBaseTests.getMinDummyQuestion()
                            });

                        let $itemContainer,
                            $target,
                            eventObj;

                        reviewScreenWidget.render()
                            .then(() => {
                                $itemContainer = reviewScreenWidget.$(`#${reviewScreenWidget.model.get('id')}_Items`);
                                for (let i = 0; i < testItems.length; ++i) {
                                    reviewScreenWidget.reviewScreenList.push(testItems[i]);
                                    $itemContainer.append(reviewScreenWidget.addItem(testItems[i]));
                                }
                            })
                            .then(() => {
                                $target = reviewScreenWidget.$el.find('.item').first();
                                eventObj = $.Event('click', {target: $target, currentTarget: $target});
                                spyOn(reviewScreenWidget, 'itemSelected').and.callThrough();
                                return reviewScreenWidget.itemSelectedEvent(eventObj);
                            })
                            .then(() => {
                                expect(reviewScreenWidget.itemSelected).toHaveBeenCalledWith(eventObj);
                            })
                            .catch((e) => {
                                fail(e);
                            })
                            .done(() => {
                                reviewScreenWidget.remove();
                                done();
                            });
                    });
                });

                describe('#action', function () {

                    it('Calls function if it exists', function (done) {
                        const model = new Widget({
                                type: 'ReviewScreen',
                                id: 'reviewScreen',
                                buttons: testBtns
                            }),
                            reviewScreenWidget = new LF.Widget[model.get('type')]({
                                model: model,
                                mandatory: false,
                                parent: WidgetBaseTests.getMinDummyQuestion()
                            });

                        let $itemContainer,
                            $target;

                        reviewScreenWidget.render()
                            .then(() => {
                                $itemContainer = reviewScreenWidget.$(`#${reviewScreenWidget.model.get('id')}_Items`);
                                for (let i = 0; i < testItems.length; ++i) {
                                    reviewScreenWidget.reviewScreenList.push(testItems[i]);
                                    $itemContainer.append(reviewScreenWidget.addItem(testItems[i]));
                                }
                            })
                            .then(() => {
                                $target = reviewScreenWidget.$el.find('.button').first();
                                let funcCalled = false;
                                LF.Widget.ReviewScreen.test0Func = () => {
                                    return new Q.Promise((resolve) => {
                                        funcCalled = true;
                                        resolve();
                                    });
                                };
                                let e = $.Event('click', {target: $target, currentTarget: $target});

                                spyOn(reviewScreenWidget, 'handleAction').and.callFake(() => Q());

                                reviewScreenWidget.action(e);

                                expect(reviewScreenWidget.handleAction).toHaveBeenCalled();
                            })
                            .catch((e) => {
                                fail(e);
                            })
                            .done(() => {
                                reviewScreenWidget.remove();
                                done();
                            });
                    });
                });

                describe('#handleAction', function () {
                    it('Returns a Promise', function (done) {
                        const model = new Widget({
                                type: 'ReviewScreen',
                                id: 'reviewScreen',
                                buttons: testBtns
                            }),
                            reviewScreenWidget = new LF.Widget[model.get('type')]({
                                model: model,
                                mandatory: false,
                                parent: WidgetBaseTests.getMinDummyQuestion()
                            });

                        let $itemContainer,
                            $target;

                        reviewScreenWidget.render()
                            .then(() => {
                                $itemContainer = reviewScreenWidget.$(`#${reviewScreenWidget.model.get('id')}_Items`);
                                for (let i = 0; i < testItems.length; ++i) {
                                    reviewScreenWidget.reviewScreenList.push(testItems[i]);
                                    $itemContainer.append(reviewScreenWidget.addItem(testItems[i]));
                                }
                            })
                            .then(() => {
                                $target = reviewScreenWidget.$el.find('.button').first();
                                let funcCalled = false;
                                LF.Widget.ReviewScreen.test0Func = () => {
                                    return new Q.Promise((resolve) => {
                                        funcCalled = true;
                                        resolve();
                                    });
                                };
                                let e = $.Event('click', {target: $target, currentTarget: $target});

                                expect(typeof reviewScreenWidget.handleAction(e).then).toEqual(typeof Function);
                            })
                            .catch((e) => {
                                fail(e);
                            })
                            .done(() => {
                                reviewScreenWidget.remove();
                                done();
                            });
                    });
                });
            });
        });
   }
}


let tests = new ReviewScreenTests();
tests.execTests();
