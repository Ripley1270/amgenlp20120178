import * as helpers from 'test/helpers/SpecHelpers';
import 'test/helpers/StudyDesign';

import Questionnaires from 'core/collections/Questionnaires';
import Answers from 'core/collections/Answers';
import Question from 'core/models/Question';
import Dashboard from 'core/models/Dashboard';
import QuestionnaireView from 'core/views/QuestionnaireView';
import QuestionView from 'core/views/QuestionView';
import Widget from 'core/models/Widget';
import DateSpinner from 'core/widgets/DateSpinner';
import User from 'core/models/User';
import Session from 'core/classes/Session';
import Subject from 'core/models/Subject';
// import Templates from 'core/collections/Templates';
import Templates from 'core/resources/Templates';
import Languages from 'core/collections/Languages';
import IntervalSpinnerBaseTests from './IntervalSpinnerBase.spec';

import 'core/resources/Templates';

export default class DateSpinnerTests extends IntervalSpinnerBaseTests {
    get testValue1 () {
        return '15 Oct 1995';
    }

    get displayValue1 () {
        return moment(this.testValue1).format('LL');
    }

    get testValue2 () {
        return '29 Nov 2016';
    }

    get displayValue2 () {
        return moment(this.testValue2).format('LL');
    }

    constructor (model = DateSpinnerTests.model) {
        super(model);
    }

    /**
     * {Widget} get the widget model.
     */
    static get model () {
        return new Widget({
            id: 'SPINNER_DIARY_W_1',
            type: 'DateSpinner',
            className: 'SPINNER_DIARY_W_1',
            label: 'PICK_A_NUMBER'
        });
    }

    execTests () {
        super.execTests();

        TRACE_MATRIX('US6106').
        describe('DateSpinner', () => {
            let templates,
                dummyParent,
                security,
                dateSpinnerWidget = null;

            beforeAll(() => {
                security = LF.security;
                LF.security = this.getDummySecurity();

                templates = LF.templates;

            });
            afterAll(() => {
                LF.security = security;
                LF.templates = templates;
            });

            beforeEach(() => {
                dummyParent = this.getMinDummyQuestion();
            });
            afterEach(() => {
                $(`#${dummyParent.getQuestionnaire().id}`).remove();
                $('.modal').remove();
                $(`.modal-backdrop`).remove();
                dateSpinnerWidget = null;
            });

            describe('Default properties', () => {
                beforeEach(() => {
                    dateSpinnerWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: false,
                        parent: dummyParent
                    });
                });
                
                describe('#defaultModalTemplate', () => {
                    it('is defaulted to DEFAULT:DateSpinnerModal', () => {
                        expect(dateSpinnerWidget.defaultModalTemplate).toBe('DEFAULT:DateSpinnerModal');
                    });
                });

                describe('#defaultDisplayFormat', () => {
                    it('is defaulted to DEFAULT:DateSpinnerModal', () => {
                        expect(dateSpinnerWidget.defaultModalTemplate).toBe('DEFAULT:DateSpinnerModal');
                    });
                });

                describe('#defaultStorageFormat', () => {
                    it('is defaulted to correct format', () => {
                        expect(dateSpinnerWidget.defaultStorageFormat).toBe('DD MMM YYYY');
                    });
                });

                describe('#defaultInitialDate', () => {
                    it('is defaulted to floor of current date', () => {
                        let expectedDt = new Date();
                        expectedDt.setHours(0, 0, 0, 0);
                        expect(dateSpinnerWidget.defaultInitialDate).toEqual(expectedDt);
                    });
                });
            });

            describe('Instance methods', () => {
                beforeEach(() => {
                    dateSpinnerWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: false,
                        parent: dummyParent
                    });
                });

                describe('#validateUI', () => {
                    Async.it ('throws an error if no year, month, or day are specified in format', () => {
                        dateSpinnerWidget.model.set('dateFormat', 'HH:mm');
                        spyOn(dateSpinnerWidget, 'validateUI').and.stub();
                        return dateSpinnerWidget.render().then(() => {
                            dateSpinnerWidget.validateUI.and.callThrough();
                            expect(() => {
                                dateSpinnerWidget.validateUI();
                            }).toThrow(new Error(`Invalid template for DateSpinner widget. Expected day + month + year containers`))
                        });
                    });

                    Async.it ('throws an error if hour or minute are specified in format', () => {
                        dateSpinnerWidget.model.set('dateFormat', 'DD MMM YYYY HH:mm');
                        spyOn(dateSpinnerWidget, 'validateUI').and.stub();
                        return dateSpinnerWidget.render().then(() => {
                            dateSpinnerWidget.validateUI.and.callThrough();
                            expect(() => {
                                dateSpinnerWidget.validateUI();
                            }).toThrow(new Error(`Invalid template for DateSpinner widget. Hour and minute containers must not exist.
            Use TimeSpinner or DateTimeSpinner if you wish to include these.`))
                        });
                    });
                    
                    Async.it ('does not throw an error if only date parts are used (function is void, so returns undefined)', () => {
                        dateSpinnerWidget.model.set('dateFormat', 'DD MMM YYYY');
                        spyOn(dateSpinnerWidget, 'validateUI').and.stub();
                        return dateSpinnerWidget.render().then(() => {
                            dateSpinnerWidget.validateUI.and.callThrough();
                            expect(dateSpinnerWidget.validateUI()).toBeUndefined();
                        });
                    });
                });
            });
        });
    }
}

let tester = new DateSpinnerTests();
tester.execTests();
