import COOL from 'core/COOL';

class test1 {
    value (v) { return v; }

    add (v) { return v+1; }
}

class test2 extends COOL.getClass('test1',test1) {
    value (v) { return 2*v; }  // pure overload

    add (v) { return super.add(v)+1; }  // chain
}

describe('COOL', () => {

    beforeAll(() => {
    });

    beforeEach(() => {
    });

    afterAll(()=> {
    });

    it('add classes and exercise methods and inheritence', () => {
        let result = COOL.add('test1', test1);
        expect(result).toEqual(undefined);

        result = COOL.add('test1', test2);
        expect(result).toEqual(test1);

        let test = COOL.new('test1');
        expect(test).toBeTruthy();

        result = test.value(5);
        expect(result).toEqual(10);

        result = test.add(5);
        expect(result).toEqual(7);


    });
});
