import { Banner, MessageRepo } from 'core/Notify';
import Logger from 'core/Logger';

describe('Notify', () => {
    describe('MessageRepo', () => {
        const expectEmpty = () => {
            expect(MessageRepo.messages).toEqual({});
            expect(MessageRepo.types).toEqual([]);
        };

        beforeAll(() => {
            MessageRepo.clear();
            expectEmpty();
        });

        afterEach(() => {
            MessageRepo.clear();
            expectEmpty();
        });

        describe('method:addToRepo', () => {
            it('should log an error if a required parameter is missing', () => {
                const testAdd = (params = {}) => {
                    MessageRepo.addToRepo(params);
                    expect(Logger.prototype.warn).toHaveBeenCalled();
                    expectEmpty();
                };

                spyOn(Logger.prototype, 'warn');

                testAdd();
                testAdd({
                    type: 'Banner',
                    key: 'KEY'
                });
                testAdd({
                    key: 'KEY',
                    message: $.noop
                });
                testAdd({
                    type: 'Banner',
                    message: $.noop
                });
            });

            describe('with the correct data given', () => {
                const key = 'TEST_KEY';
                const id = 'TEST_ID';
                const message = $.noop;
                const type = 'TEST_TYPE';

                it('should save correctly while omitting the id', () => {
                    MessageRepo.addToRepo({ type, key, message });
                    expect(typeof MessageRepo.messages[key]).toBe('function');
                    expect(MessageRepo[type][key]).toBe(key);
                });

                it('should save correctly while giving a custom id', () => {
                    MessageRepo.addToRepo({ type, key, message, id });
                    expect(typeof MessageRepo.messages[id]).toBe('function');
                    expect(MessageRepo[type][key]).toBe(id);
                });

                it('should overwrite an existing entry and log a warning.', () => {
                    const test = string => {
                        let callMessage;

                        MessageRepo.addToRepo({
                            type,
                            key,
                            message () {
                                callMessage = string;
                            }
                        });

                        MessageRepo.messages[key]();
                        expect(callMessage).toBe(string);
                    };

                    spyOn(Logger.prototype, 'warn');

                    test('first');
                    test('second');

                    expect(Logger.prototype.warn).toHaveBeenCalled();

                });
            });
        });

        describe('method:add', () => {
            it('should be able to add any number of items to the system', () => {
                MessageRepo.add({
                    type: '1',
                    key: '1',
                    message: $.noop
                }, {
                    type: '2',
                    key: '2',
                    message: $.noop
                });

                expect(_.keys(MessageRepo.messages).length).toBe(2);
                expect(MessageRepo.types.length).toBe(2);

                MessageRepo.add({
                    type: '3',
                    key: '3',
                    message: $.noop
                });

                expect(_.keys(MessageRepo.messages).length).toBe(3);
                expect(MessageRepo.types.length).toBe(3);
            });
        });

        describe('method:display', () => {
            const key = 'TEST_BANNER';
            const type = 'Banner';
            const id = 'TEST_ID';

            Async.it('should log an error and display nothing if no id is given', () => {
                spyOn(Logger.prototype, 'error');

                return MessageRepo.display()
                    .then(res => {
                        expect(Logger.prototype.error).toHaveBeenCalled();
                        expect(res).not.toBeDefined();
                    });

            });

            Async.it('should display the message by passing the id', () => {
                spyOn(Logger.prototype, 'error');
                const message = jasmine.createSpy('messageSpy');

                MessageRepo.add({ id, key, type, message });

                return MessageRepo.display(id)
                    .then(() => {
                        expect(Logger.prototype.error).not.toHaveBeenCalled();
                        expect(message).toHaveBeenCalled();
                    })
                    .catch(fail);
            });

            Async.it('should display the message by using the stored key', () => {
                spyOn(Logger.prototype, 'error');
                const message = jasmine.createSpy('messageSpy');

                MessageRepo.add({ id, key, type, message });

                return MessageRepo.display(MessageRepo[type][key])
                    .then(() => {
                        expect(Logger.prototype.error).not.toHaveBeenCalled();
                        expect(message).toHaveBeenCalled();
                    })
                    .catch(fail);
            });

            Async.it('should display the message with passed parameters', () => {
                spyOn(Logger.prototype, 'error');
                const message = jasmine.createSpy('messageSpy');
                const params = {
                    one: 1
                };

                MessageRepo.add({ id, key, type, message });

                return MessageRepo.display(MessageRepo[type][key], params)
                    .then(() => {
                        expect(Logger.prototype.error).not.toHaveBeenCalled();
                        expect(message).toHaveBeenCalledWith(params);
                    })
                    .catch(fail);
            });

        });

        describe('removal methods: ', () => {
            const expectFull = () => {
                expect(_.keys(MessageRepo.messages).length).toBe(3);
                expect(MessageRepo.types.length).toBe(2);
            };

            beforeEach(() => {
                MessageRepo.add({
                    type: 'Banner',
                    key: 'TEST_1',
                    message: $.noop
                }, {
                    type: 'Banner',
                    key: 'TEST_2',
                    message: $.noop
                }, {
                    type: 'Dialog',
                    key: 'TEST_3',
                    message: $.noop
                });
            });

            describe('method:removeFromRepo', () => {
                it('should not remove anything and log a warning if a param is missing', () => {
                    spyOn(Logger.prototype, 'warn');

                    expectFull();

                    MessageRepo.removeFromRepo({});

                    expect(Logger.prototype.warn.calls.count()).toBe(2);
                    expectFull();

                    MessageRepo.removeFromRepo({ type: 'Banner' });

                    expect(Logger.prototype.warn.calls.count()).toBe(3);
                    expectFull();

                    MessageRepo.removeFromRepo({ key: 'TEST_1' });

                    expect(Logger.prototype.warn.calls.count()).toBe(4);
                    expectFull();
                });

                it('should remove the message', () => {
                    spyOn(Logger.prototype, 'warn');

                    expectFull();

                    MessageRepo.removeFromRepo({
                        type: 'Banner',
                        key: 'TEST_1'
                    });

                    expect(Logger.prototype.warn).not.toHaveBeenCalled();
                    expect(MessageRepo.messages['TEST_1']).toBeNull();
                    expect(MessageRepo.Banner['TEST_1']).toBeNull();
                });
            });

            describe('method:remove', () => {
                it('should remove any number of messages at once', () => {
                    spyOn(Logger.prototype, 'warn');

                    expectFull();

                    MessageRepo.remove({
                        type: 'Banner',
                        key: 'TEST_1'
                    }, {
                        type: 'Banner',
                        key: 'TEST_2'
                    });

                    expect(Logger.prototype.warn).not.toHaveBeenCalled();
                    expect(MessageRepo.messages['TEST_1']).toBeNull();
                    expect(MessageRepo.Banner['TEST_1']).toBeNull();
                    expect(MessageRepo.messages['TEST_2']).toBeNull();
                    expect(MessageRepo.Banner['TEST_2']).toBeNull();

                    MessageRepo.remove({
                        type: 'Dialog',
                        key: 'TEST_3'
                    });

                    expect(Logger.prototype.warn).not.toHaveBeenCalled();
                    expect(MessageRepo.messages['TEST_3']).toBeNull();
                    expect(MessageRepo.Dialog['TEST_3']).toBeNull();
                });
            });

            describe('method:clear', () => {
                it('should clear all messages from the system', () => {
                    expectFull();
                    MessageRepo.clear();
                    expectEmpty();
                });
            });
        });
    });

    xdescribe('Banner', () => {
        beforeEach(() => {
            $.noty = jasmine.createSpyObj('noty', [
                'show',
                'close',
                'closeAll'
            ]);
            window.noty = jasmine.createSpy('noty');
            spyOn(LF.strings, 'display');
            spyOn(Logger.prototype, 'error').and.stub();
        });

        describe('method:show', () => {
            it('should show a banner.', () => {
                Banner.show({ text: 'Hello World!', type: 'success'});

                expect(noty).toHaveBeenCalledWith({
                    timeout: 5000,
                    dismissQueue: false,
                    callback: jasmine.any(Object),
                    template: jasmine.any(String),
                    closeWith: ['click', 'tap'],
                    type: 'success',
                    text: 'Hello World!'
                });
            });
        });

        describe('method:success', () => {
            it('should display a success banner.', done => {
                LF.strings.display.and.callFake(() => Q('Hello World'));
                spyOn(Banner, 'show').and.returnValue('1');

                let request = Banner.success('HELLO_WORLD');

                expect(request).toBePromise();

                request.then((id) => {
                    expect(id).toBe('1');
                    expect(Banner.show).toHaveBeenCalledWith({ text: 'Hello World', type: 'success' });
                })
                .catch(e => fail(e))
                .done(done);
            });

            it('should fail to display a success banner.', done => {
                LF.strings.display.and.callFake(() => Q.reject('Translation error'));
                spyOn(Banner, 'show');

                let request = Banner.success('HELLO_WORLD');

                expect(request).toBePromise();

                request.catch(e => {
                    expect(e).toBe('Translation error');
                    expect(Banner.show).not.toHaveBeenCalled();
                }).done(done);
            });
        });

        describe('method:error', () => {
            it('should display a error banner.', done => {
                LF.strings.display.and.callFake(() => Q('Invalid access'));
                spyOn(Banner, 'show').and.returnValue('1');

                let request = Banner.error('INVALID_ACCESS');

                expect(request).toBePromise();

                request.then(() => {
                    expect(Banner.show).toHaveBeenCalledWith({ text: 'Invalid access', type: 'error' });
                })
                .catch(e => fail(e))
                .done(done);
            });

            it('should fail to display a error banner.', done => {
                LF.strings.display.and.callFake(() => Q.reject('Translation error'));
                spyOn(Banner, 'show');

                let request = Banner.error('INVALID_ACCESS');

                expect(request).toBePromise();

                request.catch(e => {
                    expect(e).toBe('Translation error');
                    expect(Banner.show).not.toHaveBeenCalled();
                }).done(done);
            });
        });

        describe('method:warn', () => {
            it('should display a warn banner.', done => {
                LF.strings.display.and.callFake(() => Q('Invalid access'));
                spyOn(Banner, 'show').and.returnValue('1');

                let request = Banner.warn('INVALID_ACCESS');

                expect(request).toBePromise();

                request.then(() => {
                    expect(Banner.show).toHaveBeenCalledWith({ text: 'Invalid access', type: 'warning' });
                })
                .catch(e => fail(e))
                .done(done);
            });

            it('should fail to display a warn banner.', done => {
                LF.strings.display.and.callFake(() => Q.reject('Translation error'));
                spyOn(Banner, 'show');

                let request = Banner.warn('INVALID_ACCESS');

                expect(request).toBePromise();

                request.catch(e => {
                    expect(e).toBe('Translation error');
                    expect(Banner.show).not.toHaveBeenCalled();
                }).done(done);
            });
        });

        describe('method:info', () => {
            it('should display a info banner.', done => {
                LF.strings.display.and.callFake(() => Q('Invalid access'));
                spyOn(Banner, 'show').and.returnValue('1');

                let request = Banner.info('INVALID_ACCESS');

                expect(request).toBePromise();

                request.then(() => {
                    expect(Banner.show).toHaveBeenCalledWith({ text: 'Invalid access', type: 'information' });
                })
                .catch(e => fail(e))
                .done(done);
            });

            it('should fail to display a info banner.', done => {
                LF.strings.display.and.callFake(() => Q.reject('Translation error'));
                spyOn(Banner, 'show');

                let request = Banner.info('INVALID_ACCESS');

                expect(request).toBePromise();

                request.catch(e => {
                    expect(e).toBe('Translation error');
                    expect(Banner.show).not.toHaveBeenCalled();
                }).done(done);
            });
        });

        describe('method:alert', () => {
            it('should display a alert banner.', done => {
                LF.strings.display.and.callFake(() => Q('Invalid access'));
                spyOn(Banner, 'show').and.returnValue('1');

                let request = Banner.alert('INVALID_ACCESS');

                expect(request).toBePromise();

                request.then(() => {
                    expect(Banner.show).toHaveBeenCalledWith({ text: 'Invalid access', type: 'alert' });
                })
                .catch(e => fail(e))
                .done(done);
            });

            it('should fail to display a alert banner.', done => {
                LF.strings.display.and.callFake(() => Q.reject('Translation error'));
                spyOn(Banner, 'show');

                let request = Banner.alert('INVALID_ACCESS');

                expect(request).toBePromise();

                request.catch(e => {
                    expect(e).toBe('Translation error');
                    expect(Banner.show).not.toHaveBeenCalled();
                }).done(done);
            });
        });

        describe('method:close', () => {
            it('should close the banner.', () => {
                Banner.close('1');

                expect($.noty.close).toHaveBeenCalledWith('1');
            });
        });

        describe('method:closeAll', () => {
            it('should close all.', () => {
                Banner.closeAll();
                expect($.noty.closeAll).toHaveBeenCalled();
            });
        });

    });

});
