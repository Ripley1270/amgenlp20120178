import Session from 'core/classes/Session';
import * as lStorage from 'core/lStorage';
import State from 'core/classes/State';
import Logger from 'core/Logger';
import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';
import ELF from 'core/ELF';
import { getNested } from 'core/utilities';

// globals LF: true
describe('Session', () => {
    LF = LF || {};
    let testSession, originalLF, mockDate;

    beforeEach(() => {
        originalLF = {};
        Object.keys(LF).forEach((key) => {
            originalLF[key] = LF[key];
        });

        mockDate = new Date();

        // fake the context
        LF.router = jasmine.createSpyObj(['view', 'navigate']);
        LF.router.view.and.returnValue('theView');
        LF.router.controller = {};
        LF.router.controller.login = jasmine.createSpy('LF.router.controller.login');
        LF.StudyDesign = LF.StudyDesign || {};
        LF.StudyDesign.questionnaireTimeout = 17;
        LF.appName = 'LogPad App';
    });

    afterEach(() => {
        Object.keys(originalLF).forEach((key) => {
            LF[key] = originalLF[key];
        });
    });

    let setFakeTimeouts = (session, fakeNow, ...timeouts) => {

        let fakeNowTs = fakeNow.getTime();
        let sessionTimeout = session.sessionTimeout || 17;
        let questionnaireTimeout = getNested('LF.StudyDesign.questionnaireTimeout') || 1000000000;

        let lastCheck = fakeNowTs - 100;
        let lastActivity = fakeNowTs - 200;
        let startQuestionnaire = fakeNowTs - 300;

        if (timeouts.indexOf('activity') !== -1) {
            lastActivity = lastActivity - sessionTimeout;
        }
        session.set('Last_Active', lastActivity);

        if (timeouts.indexOf('check') !== -1) {
            lastCheck = lastCheck - sessionTimeout;
        }
        session.set('Timeout_LastCheck', lastCheck);

        if (timeouts.indexOf('questionnaire') !== -1) {
            // the questionnaire timeout is recorded in MINUTES
            startQuestionnaire = startQuestionnaire - (questionnaireTimeout * 60 * 1000);
        }
        session.questionnaireStartTime = startQuestionnaire;
    };

    describe('constructor', () => {

        it('is created successfully by the constructor', () => {
            let postConstructor = new Session();
            expect(postConstructor).toBeDefined();
            expect(postConstructor.interval).toBe(5000);
        });
    });

    describe('localStorage interface', () => {

        let testSession;

        beforeEach(() => {
            testSession = new Session();
        });

        afterEach(() => {
            testSession = null;
        });

        it('sets items by calling setItem', () => {
            spyOn(lStorage, 'setItem');
            testSession.set('mascot', 'tiger');
            expect(lStorage.setItem).toHaveBeenCalledWith('mascot', 'tiger');
        });

        it('retrieves items by calling getItem', () => {
            spyOn(lStorage, 'getItem').and.returnValue('stripey');
            let mascotName = testSession.get('mascotName');
            expect(lStorage.getItem).toHaveBeenCalledWith('mascotName');
            expect(mascotName).toBe('stripey');
        });

        it('removes items by calling removeItem', () => {
            spyOn(lStorage, 'removeItem');
            testSession.remove('prey');
            expect(lStorage.removeItem).toHaveBeenCalledWith('prey');
        });
    });

    describe('activity tracking', () => {
        const timestampOne = 1414213562373;
        const timestampTwo = 1732050807568;

        beforeEach(() => {
            testSession = new Session();
        });

        it('does not listen for keydown events before listenForActivity() is invoked', () => {
            spyOn(Session.prototype, 'setLastActive').and.callThrough();
            $('body').trigger(jQuery.Event('keydown', {
                keyCode: 65,
                which: 65
            }));
            expect(Session.prototype.setLastActive).not.toHaveBeenCalled();
        });

        it('does not listen for mouseup events before listenForActivity() is invoked', () => {
            spyOn(Session.prototype, 'setLastActive').and.callThrough();
            $('body').trigger(jQuery.Event('mouseup', {
                which: 1
            }));
            expect(Session.prototype.setLastActive).not.toHaveBeenCalled();
        });

        it('listens for keydown events after listenForActivity() is invoked', () => {
            spyOn(Session.prototype, 'setLastActive').and.callThrough();
            testSession.listenForActivity();
            $('body').trigger(jQuery.Event('keydown', {
                keyCode: 65,
                which: 65
            }));
            expect(Session.prototype.setLastActive).toHaveBeenCalled();
        });

        it('listens for mouseup events after listenForActivity() is invoked', () => {
            spyOn(Session.prototype, 'setLastActive').and.callThrough();
            testSession.listenForActivity();
            $('body').trigger(jQuery.Event('mouseup', {
                which: 1
            }));
            expect(Session.prototype.setLastActive).toHaveBeenCalled();
        });

        it('records and returns the current timestamp when instructed', () => {
            spyOn(Date.prototype, 'getTime').and.returnValue(timestampOne);
            spyOn(Session.prototype, 'set');

            let ts = testSession.setLastActive();

            expect(Date.prototype.getTime).toHaveBeenCalled();
            expect(Session.prototype.set).toHaveBeenCalledWith('Last_Active', timestampOne);
            expect(ts).toBe(timestampOne);
        });

        it('reports the latest activity timestamp as an integer', () => {
            spyOn(Session.prototype, 'get').and.returnValue(timestampOne);

            let ts = testSession.getLastActive();
            expect(Session.prototype.get).toHaveBeenCalledWith('Last_Active');
            expect(ts).toBe(timestampOne);
        });

        it('reports false when no activity timestamp exists', () => {
            spyOn(Session.prototype, 'get').and.returnValue(null);

            let ts = testSession.getLastActive();
            expect(Session.prototype.get).toHaveBeenCalledWith('Last_Active');
            expect(ts).toBeFalsy();
        });

        it('records and returns a timeout check timestamp when instructed', () => {
            spyOn(Date.prototype, 'getTime').and.returnValue(timestampTwo);
            spyOn(Session.prototype, 'set');

            let ts = testSession.setTimeoutLastCheck();

            expect(Date.prototype.getTime).toHaveBeenCalled();
            expect(Session.prototype.set).toHaveBeenCalledWith('Timeout_LastCheck', timestampTwo);
            expect(ts).toBe(timestampTwo);
        });

        it('reports the latest timeout check timestamp as an integer', () => {
            spyOn(Session.prototype, 'get').and.returnValue(timestampTwo);

            let ts = testSession.getTimeoutLastCheck();
            expect(Session.prototype.get).toHaveBeenCalledWith('Timeout_LastCheck');
            expect(ts).toBe(timestampTwo);
        });

        it('reports false when no timeout check timestamp exists', () => {
            spyOn(Session.prototype, 'get').and.returnValue(null);

            let ts = testSession.getTimeoutLastCheck();
            expect(Session.prototype.get).toHaveBeenCalledWith('Timeout_LastCheck');
            expect(ts).toBeFalsy();
        });
    });

    describe('startSessionTimeout', () => {

        const timestampOne = 1414213562373;
        const timestampTwo = 1732050807568;

        beforeEach(() => {
            LF.StudyDesign = {
                sessionTimeout: 60
            };

            mockDate = new Date();
            testSession = new Session();
            setFakeTimeouts(testSession, mockDate, 'none');
        });

        it('uses the timeout from LF.StudyDesign.sessionTimeout if there is one', () => {
            LF.StudyDesign.sessionTimeout = 90;
            testSession.startSessionTimeOut();
            expect(LF.StudyDesign.sessionTimeout).toBe(90);
            expect(testSession.sessionTimeout).toBe(90 * 60 * 1000);
        });

        it('sets and uses a timeout of 30 minutes if there is none', () => {
            LF.StudyDesign.sessionTimeout = null;
            testSession.startSessionTimeOut();
            expect(LF.StudyDesign.sessionTimeout).toBe(30);
            expect(testSession.sessionTimeout).toBe(30 * 60 * 1000);
        });

        it('starts listening for activity', () => {
            spyOn(Session.prototype, 'listenForActivity').and.callThrough();
            testSession.startSessionTimeOut();
            expect(Session.prototype.listenForActivity).toHaveBeenCalled();
        });

        it('records that session timing has begun', () => {
            testSession.startSessionTimeOut();
            expect(testSession.timerOn).toBeTruthy();
        });

        it('clears any existing session timeout timer', () => {
            spyOn(jasmine.getGlobal(), 'clearTimeout');
            testSession.timer = (42);
            testSession.startSessionTimeOut();
            expect(clearTimeout).toHaveBeenCalledWith(42);
        });

        // we spy on Session.prototype.checkTimeOut because we expect
        // it to make calls to setLastActive and setTimeoutLastCheck
        // and using a spy lets us isolate and test the behavior of
        // startSessionTimeOut independently.

        it('sets the last activity time if it is not set', () => {
            spyOn(Session.prototype, 'get').and.returnValue(null);
            spyOn(Session.prototype, 'setLastActive');
            spyOn(Session.prototype, 'checkTimeOut');
            testSession.startSessionTimeOut();
            expect(Session.prototype.get).toHaveBeenCalledWith('Last_Active');
            expect(Session.prototype.setLastActive).toHaveBeenCalled();
        });

        it('does not touch the last activity time if it is set', () => {
            spyOn(Session.prototype, 'get').and.returnValue(timestampOne);
            spyOn(Session.prototype, 'setLastActive');
            spyOn(Session.prototype, 'checkTimeOut');
            testSession.startSessionTimeOut();
            expect(Session.prototype.get).toHaveBeenCalledWith('Last_Active');
            expect(Session.prototype.setLastActive).not.toHaveBeenCalled();
        });

        it('sets the last timestamp check time if it is not set', () => {
            spyOn(Session.prototype, 'get').and.returnValue(null);
            spyOn(Session.prototype, 'setTimeoutLastCheck');
            spyOn(Session.prototype, 'checkTimeOut');
            testSession.startSessionTimeOut();
            expect(Session.prototype.get).toHaveBeenCalledWith('Timeout_LastCheck');
            expect(Session.prototype.setTimeoutLastCheck).toHaveBeenCalled();
        });

        it('does not touch the last timestamp check time if it is set', () => {
            spyOn(Session.prototype, 'get').and.returnValue(timestampTwo);
            spyOn(Session.prototype, 'setTimeoutLastCheck');
            spyOn(Session.prototype, 'checkTimeOut');
            testSession.startSessionTimeOut();
            expect(Session.prototype.get).toHaveBeenCalledWith('Timeout_LastCheck');
            expect(Session.prototype.setTimeoutLastCheck).not.toHaveBeenCalled();
        });

        it('invokes the first timestamp check', () => {
            spyOn(Session.prototype, 'checkTimeOut');
            testSession.startSessionTimeOut();
            expect(Session.prototype.checkTimeOut).toHaveBeenCalled();
        });
    });

    describe('checkTimeOut (session timeout)', () => {

        beforeEach(() => {
            testSession = new Session();
            testSession.startSessionTimeOut();
        });

        describe('timer maintenance', () => {

            it('removes the current timer, if there is one', () => {
                spyOn(jasmine.getGlobal(), 'clearTimeout');
                testSession.timer = 1685;
                testSession.checkTimeOut();
                expect(jasmine.getGlobal().clearTimeout).toHaveBeenCalledWith(1685);
            });

            it('does not remove a timer that is not there but calls clearTimeout anyway', () => {
                spyOn(jasmine.getGlobal(), 'clearTimeout');
                delete testSession.timer;
                testSession.checkTimeOut();
                expect(jasmine.getGlobal().clearTimeout).toHaveBeenCalledWith(undefined);
            });

            it('sets a timer when the session clock should be running', () => {
                spyOn(jasmine.getGlobal(), 'setTimeout');
                testSession.timerOn = true;
                testSession.checkTimeOut();
                expect(jasmine.getGlobal().setTimeout).toHaveBeenCalled();

            });

            it('sets no timer when the session clock should not be running', () => {
                spyOn(jasmine.getGlobal(), 'setTimeout');
                testSession.timerOn = true;
                testSession.checkTimeOut();
                expect(jasmine.getGlobal().setTimeout).toHaveBeenCalled();
            });
        });

        describe('timeout evaluation', () => {

            it('does not trigger a timeout when there is no current view', () => {
                spyOn(Logger.prototype, 'operational').and.callThrough();
                LF.router = null;
                setFakeTimeouts(testSession, mockDate, 'activity', 'check');
                testSession.checkTimeOut();
                expect(Logger.prototype.operational).not.toHaveBeenCalled();
            });

            it('does not trigger a timeeout when the session has not expired', () => {
                spyOn(Logger.prototype, 'operational').and.callThrough();
                setFakeTimeouts(testSession, mockDate, 'none');
                testSession.checkTimeOut();
                expect(Logger.prototype.operational).not.toHaveBeenCalled();
            });

            it('triggers a timeeout when the session has expired', () => {
                spyOn(Logger.prototype, 'operational').and.callThrough();
                setFakeTimeouts(testSession, mockDate, 'activity', 'check');
                testSession.checkTimeOut();
                expect(Logger.prototype.operational).toHaveBeenCalledWith('Session has timed out.');
            });

            it('triggers a timeout when only the last-active interval is too long', () => {
                spyOn(Logger.prototype, 'operational').and.callThrough();
                setFakeTimeouts(testSession, mockDate, 'activity');
                testSession.checkTimeOut();
                expect(Logger.prototype.operational).toHaveBeenCalledWith('Session has timed out.');
            });

            it('triggers a timeout when only the last-check interval is too long', () => {
                spyOn(Logger.prototype, 'operational').and.callThrough();
                setFakeTimeouts(testSession, mockDate, 'check');
                testSession.checkTimeOut();
                expect(Logger.prototype.operational).toHaveBeenCalledWith('Session has timed out.');
            });
        });

        describe('timing out all app modes', () => {
            it('hides any modal panes', () => {});
            it('closes any open select boxes', () => {});
        });

        describe('timing out in login views', () => {

            beforeEach(() => {
                spyOn(Logger.prototype, 'operational').and.callThrough();
                LF.router.view.and.returnValue({
                    id: 'login-view'
                });
                testSession.startSessionTimeOut();
            });

            it('removes PHT authorization', () => {
                spyOn(jasmine.getGlobal().localStorage, 'removeItem').and.callThrough();
                setFakeTimeouts(testSession, mockDate, 'activity', 'check');
                testSession.checkTimeOut();
                expect(Logger.prototype.operational).toHaveBeenCalledWith('Session has timed out.');
                expect(localStorage.removeItem).toHaveBeenCalledWith('PHT_isAuthorized');
                expect(localStorage.getItem('PHT_isAuthorized')).toBe(null);
            });

            it('routes the user to the login route', () => {
                setFakeTimeouts(testSession, mockDate, 'activity', 'check');
                testSession.checkTimeOut();
                expect(Logger.prototype.operational).toHaveBeenCalledWith('Session has timed out.');
                expect(LF.router.controller.login).toHaveBeenCalled();
            });
        });

        describe('timing out sitepad site selection', () => {

            beforeEach(() => {
                LF.appName = 'SitePad App';
                spyOn(State.prototype, 'set');
                spyOn(Session.prototype, 'remove');
                testSession.startSessionTimeOut();
            });

            it('removes localstorage variables in state SITE_SELECTION', () => {
                spyOn(State.prototype, 'get').and.returnValue('SITE_SELECTION');
                setFakeTimeouts(testSession, mockDate, 'activity', 'check');
                testSession.checkTimeOut();
                expect(Session.prototype.remove.calls.count()).toBe(4);
                expect(Session.prototype.remove.calls.allArgs()).toEqual([['environment'], ['mode'], ['studyDbName'], ['serviceBase']]);
                expect(State.prototype.set).toHaveBeenCalledWith('NEW');
                expect(LF.router.navigate).toHaveBeenCalledWith('modeSelection', true);
            });
        });

        describe('timing out sitepad user setup', () => {

            beforeEach(() => {
                LF.appName = 'SitePad App';
                spyOn(State.prototype, 'set');
                testSession.startSessionTimeOut();
            });

            it('returns the user to the language selection screen from state LOCKED', () => {
                spyOn(State.prototype, 'get').and.returnValue('LOCKED');
                setFakeTimeouts(testSession, mockDate, 'activity', 'check');
                testSession.checkTimeOut();
                expect(State.prototype.set).toHaveBeenCalledWith('LANGUAGE_SELECTION');
                expect(LF.router.navigate).toHaveBeenCalledWith('languageSelection', true);
            });

            it('returns the user to the language selection screen from state SETUP_USER', () => {
                spyOn(State.prototype, 'get').and.returnValue('SETUP_USER');
                setFakeTimeouts(testSession, mockDate, 'activity', 'check');
                testSession.checkTimeOut();
                expect(State.prototype.set).toHaveBeenCalledWith('LANGUAGE_SELECTION');
                expect(LF.router.navigate).toHaveBeenCalledWith('languageSelection', true);
            });

            it('returns the user to the language selection screen from state LANGUAGE_SELECTION', () => {
                spyOn(State.prototype, 'get').and.returnValue('LANGUAGE_SELECTION');
                setFakeTimeouts(testSession, mockDate, 'activity', 'check');
                testSession.checkTimeOut();
                expect(State.prototype.set).toHaveBeenCalledWith('LANGUAGE_SELECTION');
                expect(LF.router.navigate).toHaveBeenCalledWith('languageSelection', true);
            });
            it('returns the user to the language selection screen from state SET_TIME_ZONE', () => {
                spyOn(State.prototype, 'get').and.returnValue('SET_TIME_ZONE');
                setFakeTimeouts(testSession, mockDate, 'activity', 'check');
                testSession.checkTimeOut();
                expect(State.prototype.set).toHaveBeenCalledWith('LANGUAGE_SELECTION');
                expect(LF.router.navigate).toHaveBeenCalledWith('languageSelection', true);
            });
        });
    });

    describe('pauseSessionTimeOut', () => {

        beforeEach(() => {
            testSession.startSessionTimeOut();
        });

        it('pauses the session timeout', () => {
            testSession.pauseSessionTimeOut();
            expect(testSession.timerOn).toBeFalsy();
        });

        it('records a log trace event', () => {
            spyOn(Logger.prototype, 'trace').and.callThrough();
            testSession.pauseSessionTimeOut();
            expect(Logger.prototype.trace).toHaveBeenCalled();
        });
    });

    describe('restartSessionTimeOut', () => {

        beforeEach(() => {
            testSession.startSessionTimeOut();
            testSession.pauseSessionTimeOut();
        });

        it('restarts the session timeout checking', () => {
            testSession.restartSessionTimeOut();
            expect(testSession.timerOn).toBeTruthy();
        });

        it('records a log trace event', () => {
            spyOn(Logger.prototype, 'trace').and.callThrough();
            testSession.restartSessionTimeOut();
            expect(Logger.prototype.trace).toHaveBeenCalled();
        });

        it('triggers a session timeout check', () => {
            spyOn(Session.prototype, 'checkTimeOut');
            testSession.restartSessionTimeOut();
            expect(Session.prototype.checkTimeOut).toHaveBeenCalled();
        });
    });

    describe('startQuestionnaireTimeOut', () => {

        beforeEach(() => {
            mockDate = new Date();
            jasmine.clock().install();
            jasmine.clock().mockDate(mockDate);

            testSession = new Session();
        });

        afterEach(() => {
            jasmine.clock().uninstall();
        });

        it('starts the questionnaire timer if there is an associated timeout', () => {
            testSession.startQuestionnaireTimeOut();

            expect(testSession.questionnaireTimerOn).toBeTruthy();
            expect(testSession.questionnaireStartTime).toBe(mockDate.getTime());
            expect(testSession.questionnaireTimeout).toBe(17 * 60 * 1000);
        });

        it('does nothing if there is no associated questionnaire timeout', () => {
            LF.StudyDesign.questionnaireTimeout = null;
            testSession.startQuestionnaireTimeOut();
            expect(testSession.questionnaireStartTime).not.toBeDefined();
            expect(testSession.questionnaireTimerOn).toBeFalsy();
        });
    });

    describe('stopQuestionnareTimeOut', () => {

        beforeEach(() => {
            testSession = new Session();
        });

        it('clears all questionnaire timeout info when calledt', () => {
            testSession.startQuestionnaireTimeOut();
            testSession.stopQuestionnaireTimeOut();

            expect(testSession.questionnaireTimerOn).toBeFalsy();
            expect(testSession.questionnaireStartTime).toBeFalsy();
        });
    });

    describe('pauseQuestionaireTimeout', () => {

        beforeEach(() => {
            mockDate = new Date();
            jasmine.clock().install();
            jasmine.clock().mockDate(mockDate);
            testSession = new Session();
        });

        afterEach(() => {
            jasmine.clock().uninstall();
        });

        it('records that the questionnaire timer is not active', () => {
            testSession.startQuestionnaireTimeOut();
            testSession.pauseQuestionnaireTimeout();

            expect(testSession.questionnaireTimerOn).toBeFalsy();
            expect(testSession.questionnaireStartTime).toBe(mockDate.getTime());
            expect(testSession.questionnaireTimeout).toBe(17 * 60 * 1000);
        });

        it('retains the start time and timeout length', () => {
            testSession.startQuestionnaireTimeOut();
            testSession.pauseQuestionnaireTimeout();

            expect(testSession.questionnaireStartTime).toBe(mockDate.getTime());
            expect(testSession.questionnaireTimeout).toBe(17 * 60 * 1000);
        });
    });

    describe('restartQuestionnaireTimeout', () => {

        beforeEach(() => {
            mockDate = new Date();
            jasmine.clock().install();
            jasmine.clock().mockDate(mockDate);

            testSession = new Session();
        });

        afterEach(() => {
            jasmine.clock().uninstall();
        });

        it('retains the questionnaire timer status and timeout', () => {
            testSession.startQuestionnaireTimeOut();
            testSession.pauseQuestionnaireTimeout();
            testSession.restartQuestionnaireTimeout();

            expect(testSession.questionnaireTimerOn).toBeTruthy();
            expect(testSession.questionnaireTimeout).toBe(17 * 60 * 1000);
        });

        it('does not alter the start time', () => {
            testSession.startQuestionnaireTimeOut();
            testSession.pauseSessionTimeOut();
            jasmine.clock().tick(1832);
            testSession.restartQuestionnaireTimeout();

            expect(testSession.questionnaireStartTime).toBe(mockDate.getTime());
            expect(testSession.questionnaireTimeout).toBe(17 * 60 * 1000);
        });
    });

    describe('checkTimeOut (questionnare timeout)', () => {
        let mockView;

        beforeEach(() => {
            testSession = new Session();
            // our mockView has to be an instanceof BaseQuestionnaireView to work.

            mockView = Object.create(BaseQuestionnaireView.prototype);
            mockView.id = 'qID';
            LF.router.view.and.returnValue(mockView);
            testSession.startSessionTimeOut();
            testSession.startQuestionnaireTimeOut();
        });

        describe('session timed-out', () => {

            beforeEach(() => {
                setFakeTimeouts(testSession, mockDate, 'activity');
            });

            it('stops the questionnaire timer if it is running', () => {
                expect(testSession.questionnaireTimerOn).toBeTruthy();
                testSession.checkTimeOut();
                expect(testSession.questionnaireTimerOn).toBeFalsy();
            });

            it('triggers a timeout event on the questionnaire', () => {
                spyOn(ELF, 'trigger');
                testSession.checkTimeOut();
                expect(ELF.trigger).toHaveBeenCalled();

                let args = ELF.trigger.calls.first().args;
                expect(args[0]).toBe('QUESTIONNAIRE:SessionTimeout/qID');
                expect(args[1]).toEqual({
                    questionnaire: 'qID'
                });
                expect(args[2]).toEqual(mockView);

            });

        });

        describe('session not timed-out', () => {

            beforeEach(() => {
                setFakeTimeouts(testSession, mockDate, 'questionnaire');
            });

            it('stops the questionnaire timer if it is running', () => {
                testSession.questionnaireTimerOn = true;
                testSession.checkTimeOut();
                expect(testSession.questionnaireTimerOn).toBeFalsy();
            });

            it('triggers a timeout event on the questionnaire', () => {

                spyOn(ELF, 'trigger');
                testSession.checkTimeOut();
                expect(ELF.trigger).toHaveBeenCalled();

                let args = ELF.trigger.calls.first().args;
                expect(args[0]).toBe('QUESTIONNAIRE:QuestionnaireTimeout/qID');
                expect(args[1]).toEqual({
                    questionnaire: 'qID'
                });
                expect(args[2]).toEqual(mockView);
            });
        });
    });
});
