import * as helpers from 'test/helpers/SpecHelpers';
import Template from 'core/models/Template';

describe('Template', function () {

    var model;

    beforeEach(function () {

        model = new Template({
            name          : 'Label',
            namespace     : 'Daily_Diary',
            template      : '<label class="en-US" for="{{ belongsTo }}">{{ text }}</label>'
        });

    });

    it ('should have the correct properties set.', function () {

        expect(model.get('name')).toEqual('Label');
        expect(model.get('namespace')).toEqual('Daily_Diary');
        expect(model.get('template')).toEqual('<label class="en-US" for="{{ belongsTo }}">{{ text }}</label>');

    });

    it ('should have a default namespace value of "DEFAULT".', function () {

        var template = new LF.Model.Template({
            name : 'Label'
        });

        expect(template.get('namespace')).toEqual('DEFAULT');

    });

    it ('should have a default language of undefined.', function () {

        var template = new LF.Model.Template({
            name        : 'Label',
            namespace   : 'Daily_Diary'
        });

        expect(template.get('language')).toBeUndefined();

    });

    it ('should have a default locale of undefined.', function () {

        var template = new LF.Model.Template({
            name        : 'Label',
            namespace   : 'Daily_Diary'
        });

        expect(template.get('language')).toBeUndefined();

    });

    it('should correctly create a template.', function () {

        var template = _.template(model.get('template'))({
            belongsTo   : 'password',
            text        : 'Password:'
        });

        expect(template).toEqual('<label class="en-US" for="password">Password:</label>');

    });

});
