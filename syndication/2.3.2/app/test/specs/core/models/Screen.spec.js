import Screen from 'core/models/Screen';
import * as helpers from 'test/helpers/SpecHelpers';
import * as lStorage from 'core/lStorage';
import 'core/utilities';

describe('Screen', function () {

    var model;

    beforeEach(function () {

        model = new Screen({
            id          : 'SF36_S_1',
            questions   : [
                {
                    id          : 'SF36_Q_1',
                    type        : 'RadioButton',
                    mandatory   : true
                }
            ]
        });

    });

    it('should have a schema.', function () {

        expect(model.schema).toBeDefined();

    });

    it('should NOT have a storage property.', function () {

        expect(model.storage).not.toBeDefined();

    });

    it('should have an ID of "SF36_S_1".', function () {

        expect(model.get('id')).toEqual('SF36_S_1');

    });

    it('should throw an error because invalid id DataType (expects string, receives number).', function () {

        model.set({ id: 1 }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: id. Invalid DataType. Expected string. Received number.');

    });

    it('should throw an error because invalid id DataType (expects string, receives boolean).', function () {

        model.set({ id: true }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: id. Invalid DataType. Expected string. Received boolean.');

    });

    it('should throw an error because invalid id DataType (expects string, receives array).', function () {

        model.set({ id: [1, 2, 3] }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: id. Invalid DataType. Expected string. Received array.');

    });

    it('should throw an error because invalid id DataType (expects string, receives object).', function () {

        model.set({ id: {} }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: id. Invalid DataType. Expected string. Received object.');

    });

    it('should have a template value of "DEFAULT:Screen".', function () {

        expect(model.get('template')).toEqual('DEFAULT:Screen');

    });

    it('should have a className value of "screen".', function () {

        expect(model.get('className')).toEqual('screen');

    });

});
