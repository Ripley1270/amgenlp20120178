import Dashboard from 'core/models/Dashboard';
import * as helpers from 'test/helpers/SpecHelpers';
import * as lStorage from 'core/lStorage';
import 'core/utilities';
import { eCoaDB } from 'core/dataAccess';

describe('Dashboard', function () {

    var model,
        now = new Date();

    beforeEach(function () {

        lStorage.setItem('krpt', 'krpt.39ed120a0981231');

        model = new Dashboard({
            id                  : 1,
            subject_id          : '9876',
            SU                  : 'DAILY_DIARY',
            questionnaire_id    : 'DAILY_DIARY',
            instance_ordinal    : 1,
            started             : now.ISOStamp(),
            completed           : now.ISOStamp(),
            diary_id            : now.getTime(),
            completed_tz_offset : now.getOffset(),
            phase               : 10,
            study_version       : '00.01',
            core_version        : '1.4',
            device_id           : 'LOL1337OMG'
        });

    });

    afterEach(function () {

        lStorage.removeItem('krpt');

    });

    it('should have a schema', function () {

        expect(model.schema).toBeDefined();
        expect(typeof model.schema).toEqual('object');

    });

    it('should have a name property', function () {

        expect(model.name).toEqual('Dashboard');

    });

    it('should have a storage property', function () {

        expect(model.storage).toEqual('Dashboard');

    });

    it('should have an id of 1', function () {

        expect(model.get('id')).toEqual(1);

    });

    it('should throw an error because invalid id data type (expects a number)', function () {

        model.set({ id: '1' }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: id. Invalid DataType. Expected number. Received string.');

    });

    it('should not encrypt the id property', function () {

        model.encrypt();

        expect(model.get('id')).toEqual(1);

    });

    it('should have a subject_id of \'9876\'', function () {

        expect(model.get('subject_id')).toEqual('9876');

    });

    it('should throw an error if the subject_id is set to null or undefined', function () {

        model.set({ subject_id: null }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: subject_id must not be null or undefined.');

    });

    it('should throw an error because invalid subject_id data type (expects a string)', function () {

        model.set({ subject_id: 9876 }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: subject_id. Invalid DataType. Expected string. Received number.');

    });

    it('should encrypt the subject_id property', function () {

        model.encrypt();

        expect(model.get('subject_id')).not.toEqual('9876');

    });

    it('should have a questionnaire_id value of DAILY_DIARY', function () {

        expect(model.get('questionnaire_id')).toEqual('DAILY_DIARY');

    });

    it('should throw an error if the questionnaire_id is set to null or undefined', function () {

        model.set({ questionnaire_id: null }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: questionnaire_id must not be null or undefined.');

    });

    it('should throw an error because invalid questionnaire_id data type (expects a string)', function () {

        model.set({ questionnaire_id: 9876 }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: questionnaire_id. Invalid DataType. Expected string. Received number.');

    });

    it('should not encrypt the questionnaire_id property', function () {

        model.encrypt();

        expect(model.get('questionnaire_id')).toEqual('DAILY_DIARY');

    });

    it('should have a SU of DAILY_DIARY', function () {

        expect(model.get('SU')).toEqual('DAILY_DIARY');

    });

    it('should throw an error if the SU is set to null or undefined', function () {

        model.set({ SU: null }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: SU must not be null or undefined.');

    });

    it('should throw an error because invalid SU data type (expects a string)', function () {

        model.set({ SU: 9876 }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: SU. Invalid DataType. Expected string. Received number.');

    });

    it('should encrypt the SU property', function () {

        model.encrypt();

        expect(model.get('SU')).not.toEqual('DAILY_DIARY');

    });

    it('should have an instance_ordinal of 1', function () {

        expect(model.get('instance_ordinal')).toEqual(1);

    });

    it('should throw an error if the instance_ordinal is set to null or undefined', function () {

        model.set({ instance_ordinal: null }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: instance_ordinal must not be null or undefined.');

    });

    it('should throw an error because invalid instance_ordinal data type (expects a number)', function () {

        model.set({ instance_ordinal: '1' }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: instance_ordinal. Invalid DataType. Expected number. Received string.');

    });

    it('should not encrypt the instance_ordinal property', function () {

        model.encrypt();

        expect(model.get('instance_ordinal')).toEqual(1);

    });

    it('should have a started entry of ' + now.ISOStamp(), function () {

        expect(model.get('started')).toEqual(now.ISOStamp());

    });

    it('should throw an error because invalid started data type (expects a string)', function () {

        model.set({ started: now.getTime() }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: started. Invalid DataType. Expected string. Received number.');

    });

    it('should not encrypt the started property', function () {

        model.encrypt();

        expect(model.get('started')).toEqual(now.ISOStamp());

    });

    it('should have a completed entry of ' + now.ISOStamp(), function () {

        expect(model.get('completed')).toEqual(now.ISOStamp());

    });

    it('should throw an error because invalid completed data type (expects a string)', function () {

        model.set({ completed: now.getTime() }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: completed. Invalid DataType. Expected string. Received number.');

    });

    it('should not encrypt the completed property', function () {

        model.encrypt();

        expect(model.get('completed')).toEqual(now.ISOStamp());

    });

    it('should have a diary_id entry of ' + now.getTime(), function () {

        expect(model.get('diary_id')).toEqual(now.getTime());

    });

    it('should throw an error because invalid diary_id data type (expects a number)', function () {

        model.set({ diary_id: now.getTime().toString() }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: diary_id. Invalid DataType. Expected number. Received string.');

    });

    it('should not encrypt the diary_id property', function () {

        model.encrypt();

        expect(model.get('diary_id')).toEqual(now.getTime());

    });

    it('should have a completed_tz_offset entry of ' + now.getOffset(), function () {

        expect(model.get('completed_tz_offset')).toEqual(now.getOffset());

    });

    it('should throw an error because invalid completed_tz_offset data type (expects a number)', function () {

        model.set({ completed_tz_offset: now.getOffset().toString() }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: completed_tz_offset. Invalid DataType. Expected number. Received string.');

    });

    it('should not encrypt the completed_tz_offset property', function () {

        model.encrypt();

        expect(model.get('completed_tz_offset')).toEqual(now.getOffset());

    });

    it('should have a phase entry of 10', function () {

        expect(model.get('phase')).toEqual(10);

    });

    it('should throw an error because invalid phase data type (expects a number)', function () {

        model.set({ phase: '10' }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: phase. Invalid DataType. Expected number. Received string.');

    });

    it('should not encrypt the phase property', function () {

        model.encrypt();

        expect(model.get('phase')).toEqual(10);

    });

    it('should have a default change_phase of \'false\'', function () {

        expect(model.get('change_phase')).toEqual('false');

    });

    it('should throw an error because invalid change_phase data type (expects a string)', function () {

        model.set({ change_phase: 10 }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: change_phase. Invalid DataType. Expected string. Received number.');

    });

    it('should not encrypt the change_phase property', function () {

        model.encrypt();

        expect(model.get('change_phase')).toEqual('false');

    });

    it('should have a study_version value of \'00.01\'', function () {

        expect(model.get('study_version')).toEqual('00.01');

    });

    it('should throw an error if the study_version is set to null or undefined', function () {

        model.set({ study_version: null }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: study_version must not be null or undefined.');

    });

    it('should throw an error because invalid study_version data type (expects a string)', function () {

        model.set({ study_version: 9876 }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: study_version. Invalid DataType. Expected string. Received number.');

    });

    it('should not encrypt the study_version property', function () {

        model.encrypt();

        expect(model.get('study_version')).toEqual('00.01');

    });

    it('should have a core_version value of \'1.4\'', function () {

        expect(model.get('core_version')).toEqual('1.4');

    });

    it('should throw an error if the core_version is set to null or undefined', function () {

        model.set({ core_version: null }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: core_version must not be null or undefined.');

    });

    it('should throw an error because invalid core_version data type (expects a string)', function () {

        model.set({ core_version: 9876 }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: core_version. Invalid DataType. Expected string. Received number.');

    });

    it('should not encrypt the core_version property', function () {

        model.encrypt();

        expect(model.get('core_version')).toEqual('1.4');

    });

    it('should have a device_id value of \'LOL1337OMG\'', function () {

        expect(model.get('device_id')).toEqual('LOL1337OMG');

    });

    it('should throw an error if the device_id is set to null or undefined', function () {

        model.set({ device_id: null }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: device_id must not be null or undefined.');

    });

    it('should throw an error because invalid device_id data type (expects a string)', function () {

        model.set({ device_id: 9876 }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: device_id. Invalid DataType. Expected string. Received number.');

    });

    it('should not encrypt the device_id property', function () {

        model.encrypt();

        expect(model.get('device_id')).toEqual('LOL1337OMG');

    });

    it('should save the model to the database.', function (done) {

        var response;

        model.save().then((res) => {

            expect(res).toBe(1);
            done();

        });

    });

    it('should ensure the proper fields are encrypted at rest', function (done) {

        eCoaDB.Dashboard.all().then((res) => {

            let model = res.at(0);

            expect(model.id).toEqual(1);
            expect(model.subject_id).not.toEqual('9876');
            expect(model.SU).not.toEqual('DAILY_DIARY');
            expect(model.questionnaire_id).toEqual('DAILY_DIARY');
            expect(model.instance_ordinal).toEqual(1);
            expect(model.started).toEqual(now.ISOStamp());
            expect(model.completed).toEqual(now.ISOStamp());
            expect(model.diary_id).toEqual(now.getTime());
            expect(model.completed_tz_offset).toEqual(now.getOffset());
            expect(model.phase).toEqual(10);
            expect(model.change_phase).toEqual('false');

            done();

        });

    });

});
