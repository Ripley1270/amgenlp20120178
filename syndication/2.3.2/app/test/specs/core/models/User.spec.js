import User from 'core/models/User';
import Role from 'core/models/Role';
import Users from 'core/collections/Users';
import { createGUID } from 'core/utilities';
import * as helpers from 'test/helpers/SpecHelpers';
import 'test/helpers/StudyDesign';

describe('User', () => {
    let model;

    helpers.clearCollections(Users);

    afterAll((done) => {
        helpers.uninstallDatabase().finally(done);
    });

    beforeEach(() => {
        let password = '12345',
            salt = createGUID();

        model = new User({
            userType   : 'Admin',
            username   : 'admin',
            email      : 'noreply@ert.com',
            language   : 'en-US',
            role       : 'admin',
            password   : hex_sha512(password + salt),
            salt       : salt
        });

    });

    it('should have a name property.', () => {
        expect(model.name).toBe('User');
    });

    it('should have a storage property.', () => {
        expect(model.storage).toBe('User');
    });

    it('should save the model to the database.', (done) => {
        model.save().then(() => {
            let col = new Users();

            return col.fetch().then(() => col.at(0));
        }).then((res) => {
            expect(model.get('id')).toBe(res.get('id'));
            expect(res.get('username')).toBe('admin');
        }).done(done);
    });

    it('should get the user\'s role.', () => {
        let role = model.getRole();

        expect(role.get('id')).toBe('admin');
    });

});
