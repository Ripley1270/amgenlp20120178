import * as helpers from 'test/helpers/SpecHelpers';
import Question from 'core/models/Question';
import 'core/utilities';

describe('Question', function () {

    var model;

    beforeEach(function () {

        model = new Question({
            id      : 'SF36_Q_3_A',
            IG      : 'SF36',
            text    : 'QUESTION_3_A',
            widget  : {
                type    : 'RadioButton'
            }
        });

    });

    it('should have an id of SF36_Q_3_A', function () {

        expect(model.get('id')).toEqual('SF36_Q_3_A');

    });

    it('should have an IG of SF36', function () {

        expect(model.get('IG')).toEqual('SF36');

    });

    it('should have a schema', function () {

        expect(model.schema).toBeDefined();
        expect(typeof model.schema).toEqual('object');

    });

    it('should not have a table', function () {

        expect(model.table).not.toBeDefined();

    });

    it('should have a className attribute value of "question".', function () {

        expect(model.get('className')).toEqual('question');

    });

    it('should have a help attribute value of false.', function () {

        expect(model.get('help')).toBeFalsy();

    });

    it('should throw an error because invalid className DataType (expects a string, received a number)', function () {

        model.set({ className: 1 }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: className. Invalid DataType. Expected string. Received number.');

    });

    it('should throw an error because invalid id DataType (expects a string, received an array)', function () {

        model.set({ id: [1, 2, 3] }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: id. Invalid DataType. Expected string. Received array.');

    });

    it('should throw an error because invalid widget DataType (expects a string, received a boolean)', function () {

        model.set({ widget: true }, { validate: true });
        expect(model.validationError.message).toBe('Attribute: widget. Invalid DataType. Expected object. Received boolean.');

    });

});
