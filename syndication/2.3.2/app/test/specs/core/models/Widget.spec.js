import Widget from 'core/models/Widget';
import * as helpers from 'test/helpers/SpecHelpers';
import 'core/utilities';

describe('Widget', function () {

    var model;

    beforeEach(function () {

        model = new Widget({
            id          : 'EQ5D_W_1',
            templates   : {
                container   : 'DEFAULT:FIELDSET_VERTICAL',
                input       : 'CUSTOM:RADIO_BUTTON',
                label       : 'CUSTOM:LABEL'
            },
            answers     : [
                {
                    text    : 'NO_PROBLEMS_WALKING',
                    value   : 0
                }, {
                    text    : 'SOME_PROBLEMS_WALKING',
                    value   : 1
                }, {
                    text    : 'UNABLE_WALKING',
                    value   : 2
                }
            ]
        });

    });

    afterEach(function () {
        model = null;
    });

    it('should have an id of EQ5D_W_1', function () {

        expect(model.get('id')).toEqual('EQ5D_W_1');

    });

    it('should have a schema', function () {

        expect(model.schema).toBeDefined();
        expect(typeof model.schema).toEqual('object');

    });

    it('should NOT have a table', function () {

        expect(model.table).not.toBeDefined();

    });

    it('should throw an error because invalid id DataType (expects string, receives number).', function () {

        model.set({ id: 1 }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: id. Invalid DataType. Expected string. Received number.');

    });

    it('should throw an error because invalid id DataType (expects string, receives boolean).', function () {

        model.set({ id: true }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: id. Invalid DataType. Expected string. Received boolean.');

    });

    it('should throw an error because invalid id DataType (expects string, receives array).', function () {

        model.set({ id: [1, 2, 3] }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: id. Invalid DataType. Expected string. Received array.');

    });

    it('should throw an error because invalid id DataType (expects string, receives object).', function () {

        model.set({ id: {} }, { validate: true });

        expect(model.validationError.message).toBe('Attribute: id. Invalid DataType. Expected string. Received object.');

    });

    it('should have a className value of \'EQ5D_W_1\'.', function () {

        expect(model.set({
            className   : 'EQ5D_W_1'
        }, {validate:true}).get('className')).toEqual('EQ5D_W_1');

    });

});

