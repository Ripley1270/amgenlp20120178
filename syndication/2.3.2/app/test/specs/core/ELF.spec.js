import ELF from 'core/ELF';

describe('ELF', () => {

    let testRule,
        resolved,
        rejected;

    class SomeView {
        get id () { return 99; }
        someMethod(evData, resume) {
            this.someMethodCalled = true;
            expect(this.constructor).toBe(SomeView);
            expect(this.id).toBe(99);
            resume(true);
        }
    }

    beforeAll(() => {
        ELF.rules.clear();
    });

    beforeEach(() => {


        resolved = false;
        rejected = false;

        testRule = {
            id       : 'testRule',
            trigger  : 'TEST:Rule',
            evaluate : true,
            resolve  : [{ action: 'resolved' }],
            reject   : [{ action: 'rejected' }]
        };

        ELF.action('resolved', (evtData, done) => {
            resolved = true;
            done();
        });

        ELF.action('rejected', (evtData, done) => {
            rejected = true;
            done();
        });

        ELF.expression('expression', (evtData, done) => {
            done(evtData.resolve);
        });

    });


    describe('namespace:rules', () => {

        afterEach(() => ELF.rules.clear());

        describe('method:add', () => {

            it('should add the rule.', () => {

                ELF.rules.add({ id: 'testRule' });

                let rules = ELF.rules.get();

                expect(rules[0].id).toBe('testRule');

            });

            // Chris decided it should not. So there.
            xit('should return an error.', () => {

                ELF.rules.add({ id: 'testRule' });

                expect(() => {
                    ELF.rules.add({ id: 'testRule' });
                }).toThrow(new Error('Rule testRule already exists.'));

            });

            it('should clear the rules.', () => {

                ELF.rules.add({ id: 'testRule' });

                expect(ELF.rules.size()).toBe(1);

                ELF.rules.clear();

                expect(ELF.rules.size()).toBe(0);

            });

            it('should add the rules.', () => {

                ELF.rules.add([
                    { id: 'testRuleOne' },
                    { id: 'testRuleTwo' }
                ]);

                expect(ELF.rules.size()).toBe(2);

            });

        });

        describe('method:remove', () => {

            beforeEach(() => ELF.rules.add([{ id: 'testRule' }, { id: 'testRuleTwo' }]));

            it('should remove the rule.', () => {

                ELF.rules.remove('testRule');

                expect(ELF.rules.size()).toBe(1);
                expect(ELF.rules.get()[0].id).toBe('testRuleTwo');

            });

        });

        describe('method:size', () => {

            it('should return the size', () => {

                expect(ELF.rules.size()).toBe(0);

                ELF.rules.add({ id: 'testRule' });

                expect(ELF.rules.size()).toBe(1);

            });

        });

        describe('method:get', () => {

            it('should get all of the rules.', () => {

                let rules = [{ id: 'testRule' }, { id: 'testRuleTwo' }];

                expect(ELF.rules.get()).toEqual([]);

                ELF.rules.add(rules);

                expect(ELF.rules.get()).toEqual(rules);

            });

        });

    });

    describe('method:trigger', () => {

        // TODO
        xdescribe('status propagation', () => {

        });

        describe('matching event names', () => {

            let matched = {};

            let mkRule = (triggersOn) => {
                let id = triggersOn;

                let rule = {
                    id       : id,
                    trigger  : triggersOn,
                    evaluate : true,
                    resolve  : [{ action: `resolved_${id}` }]
                };

                ELF.action(`resolved_${id}`, (evtData, done) => {
                    matched[id] = true;
                    done();
                });

                matched[id] = false;
                return rule;
            };

            Async.it('matches against array', () => {

                testRule.trigger = ['ONE', 'TWO', 'THREE'];
                ELF.rules.add(testRule);

                return ELF.trigger('THREE:happened').then(() => {
                    expect(resolved).toBe(true);
                })
            });

            Async.it('matches partial names, up to segment boundaries', () => {

                ELF.rules.add(mkRule('STEAK-ORDER:filet-mignon/medium-rare'));
                ELF.rules.add(mkRule('STEAK-ORDER:filet-mignon'));
                ELF.rules.add(mkRule('STEAK-ORDER'));

                ELF.rules.add(mkRule('STEAK-ORDER:filet'));
                ELF.rules.add(mkRule('STEAK-ORDER:filet-mignon/medium'));
                ELF.rules.add(mkRule('STEAK-ORDER:sirloin/medium-rare'));

                return ELF.trigger('STEAK-ORDER:filet-mignon/medium-rare')

                .tap(() => {
                    expect(matched['STEAK-ORDER:filet-mignon/medium-rare']).toBe(true, 'exact match');
                    expect(matched['STEAK-ORDER:filet-mignon']).toBe(true, 'match to / segment');
                    expect(matched['STEAK-ORDER']).toBe(true, 'match to : segment');

                    expect(matched['STEAK-ORDER:filet']).toBe(false, 'filet should not match filet-mignon');
                    expect(matched['STEAK-ORDER:filet-mignon/medium']).toBe(false, 'medium should not match medium-rare');
                    expect(matched['STEAK-ORDER:sirloin/medium-rare']).toBe(false, 'sirloin? not even close');
                });
            });
        });

        beforeEach(() => ELF.rules.clear());

        Async.it('shouldn\'t trigger a rule.', () => {

            return ELF.trigger('NO_ONE_CARES').then(() => {
                expect(resolved).toBe(false);
                expect(rejected).toBe(false);
            });

        });

        describe('expressions', () => {

            Async.it('allows new-style promise-returning expressions', ()=> {
                let evaled = false;
                testRule.evaluate = (data) => {
                    evaled = true;
                    return Q().delay(100).then(()=>true)
                };
                ELF.rules.add(testRule);

                return ELF.trigger('TEST:Rule').then(() => {
                    expect(evaled).toBe(true, 'evaled');
                    expect(resolved).toBe(true, 'resolved');
                });

            });


            Async.it('allows new-style synchronous-returning expressions', ()=> {
                let evaled = false;
                testRule.evaluate = (data) => {
                    evaled = true;
                    return true;
                };
                ELF.rules.add(testRule);

                return ELF.trigger('TEST:Rule').then(() => {
                    expect(evaled).toBe(true, 'evaled');
                    expect(resolved).toBe(true, 'resolved');
                });

            });

            Async.it('should trigger and resolve the rule (boolean expression).', () => {

                ELF.rules.add(testRule);

                return ELF.trigger('TEST:Rule').then(() => {
                    expect(resolved).toBe(true);
                });

            });

            Async.it('should trigger and reject the rule (boolean expression).', () => {

                testRule.evaluate = false;
                ELF.rules.add(testRule);

                return ELF.trigger('TEST:Rule').then(() => {
                    expect(rejected).toBe(true);
                });

            });

            Async.it('should trigger and resolve the rule (functional expression).', () => {

                testRule.evaluate = (input, done) => done(input.resolve);
                ELF.rules.add(testRule);

                return ELF.trigger('TEST:Rule', { resolve: true }).finally(() => {
                    expect(resolved).toBe(true);
                });

            });

            Async.it('should trigger and reject the rule (functional expression).', () => {

                testRule.evaluate = (input, done) => done(input.resolve);
                ELF.rules.add(testRule);

                return ELF.trigger('TEST:Rule', { resolve: false }).then(() => {
                    expect(rejected).toBe(true);
                });

            });

            // currently, this feature is backed out
            Async.xit('passes context to the expression as data.source', () => {

                let context = new SomeView(),
                    action = (evtData, resume) => {
                        expect(evtData.source).toBe(context);
                        resume();
                    };

                testRule.resolve = [{ action: (a,b)=>action(a,b) }];
                ELF.rules.add(testRule);

                return ELF.trigger('TEST:Rule', { }, context);

            });


            Async.it('should trigger and resolve the rule (string expression).', () => {

                testRule.evaluate = 'expression';
                ELF.rules.add(testRule);

                return ELF.trigger('TEST:Rule', { resolve: true }).then(() => {
                    expect(resolved).toBe(true);
                });

            });

            it('should trigger and reject the rule (string expression).', (done) => {

                testRule.evaluate = 'expression';
                ELF.rules.add(testRule);

                ELF.trigger('TEST:Rule', { resolve: false }).then(() => {

                    expect(rejected).toBe(true);
                    done();

                });

            });

            Async.it('should trigger and resolve the rule (object expression).', () => {

                testRule.evaluate = { expression: 'expression', input: { resolve: true } };
                ELF.rules.add(testRule);

                return ELF.trigger('TEST:Rule').then(() => {
                    expect(resolved).toBe(true);
                });

            });

            Async.it('should trigger and reject the rule (object expression).', () => {

                testRule.evaluate = { expression: 'expression', input: { resolve: false } };
                ELF.rules.add(testRule);

                return ELF.trigger('TEST:Rule').then(() => {
                    expect(rejected).toBe(true);
                });
            });

            Async.it('should trigger and resolve he rule (Array && expression).', () => {

                testRule.evaluate = ['AND', true, true];
                ELF.rules.add(testRule);

                return ELF.trigger('TEST:Rule').then(() => {
                    expect(resolved).toBe(true);
                });

            });

            Async.it('should trigger and reject he rule (Array && expression).', () => {

                testRule.evaluate = ['AND', true, false];
                ELF.rules.add(testRule);

                return ELF.trigger('TEST:Rule').then(() => {
                    expect(rejected).toBe(true);
                });

            });

            // we lost short-circuit evaluation
            Async.xit('should evaluate the second expression (Array && expression).', () => {

                let expression = jasmine.createSpy('expression').and.callFake((input, done) => done(true));

                testRule.evaluate = ['AND', true, expression];
                ELF.rules.add(testRule);

                return ELF.trigger('TEST:Rule').then(() => {
                    expect(expression).toHaveBeenCalled();
                });

            });

            // we lost short-circuit evaluation
            Async.xit('should not evaluate the second expression (Array && expression).', () => {

                let expression = jasmine.createSpy('expression').and.stub();

                testRule.evaluate = ['AND', false, expression];
                ELF.rules.add(testRule);

                return ELF.trigger('TEST:Rule').then(() => {
                    expect(expression).not.toHaveBeenCalled();
                });

            });

            Async.it('should trigger and resolve the rule (Array || expression).', () => {

                testRule.evaluate = ['OR', true, false];
                ELF.rules.add(testRule);

                return ELF.trigger('TEST:Rule').then(() => {
                    expect(resolved).toBe(true);
                });
            });

            Async.it('should trigger and reject the rule (Array || expression).', () => {
                testRule.evaluate = ['OR', false, false];
                ELF.rules.add(testRule);

                return ELF.trigger('TEST:Rule').then(() => {
                    expect(rejected).toBe(true);
                });
            });

            Async.it('should trigger and resolve the rule (Nested Array A).', () => {

                testRule.evaluate = ['AND', ['AND', 'expression', true], (evtData, done) => done(true)];
                ELF.rules.add(testRule);

                return ELF.trigger('TEST:Rule', { resolve: true }).then(() => {
                    expect(resolved).toBe(true);
                });

            });

            Async.it('should trigger and reject the rule (Nested Array A).', () => {

                testRule.evaluate = ['AND', ['AND', 'expression', true], (evtData, done) => done(true)];
                ELF.rules.add(testRule);

                return ELF.trigger('TEST:Rule', { resolve: false }).then(() => {
                    expect(rejected).toBe(true);
                });

            });

            Async.it('should trigger and resolve the rule (Nested Array B).', () => {

                testRule.evaluate = ['OR', ['AND', 'expression', true], (evtData, done) => done(true)];
                ELF.rules.add(testRule);

                return ELF.trigger('TEST:Rule', { resolve: false }).then(() => {
                    expect(resolved).toBe(true);
                });
            });

            Async.it('should trigger and reject the rule (Nested Array B).', () => {

                testRule.evaluate = ['OR', ['AND', 'expression', true], (evtData, done) => done(false)];
                ELF.rules.add(testRule);

                return ELF.trigger('TEST:Rule', { resolve: false }).then(() => {
                    expect(rejected).toBe(true);
                });

            });

        });

        describe('actions', () => {

            Async.it('can execute promise-based, rather than callback-based actions', () => {
                let a1 = jasmine.createSpy('a1').and.callFake((input) => {
                    return Q().delay(100).then(() => true);
                });

                let a2 = jasmine.createSpy('a2').and.callFake((input) => {
                    return Q().delay(100).then(() => true);
                });

                testRule.evaluate = true;
                testRule.resolve = [
                    { action: (x)=>a1(x) },
                    { action: (x)=>a2(x) }
                ];

                ELF.rules.add(testRule);

                return ELF.trigger('TEST:Rule')
                    .tap(() => {
                        expect(a1).toHaveBeenCalled();
                        expect(a1.calls.count()).toBe(1);
                        expect(a2).toHaveBeenCalled();
                        expect(a2.calls.count()).toBe(1);
                    });
            });

            Async.it('can execute synchronously returning actions', () => {
                let a1 = jasmine.createSpy('a1').and.callFake((input) => {
                    return true;
                });

                let a2 = jasmine.createSpy('a2').and.callFake((input) => {
                    return true;
                });

                testRule.evaluate = true;
                testRule.resolve = [
                    { action: (x)=>a1(x) },
                    { action: (x)=>a2(x) }
                ];

                ELF.rules.add(testRule);

                return ELF.trigger('TEST:Rule')
                    .tap(() => {
                        expect(a1).toHaveBeenCalled();
                        expect(a1.calls.count()).toBe(1);
                        expect(a2).toHaveBeenCalled();
                        expect(a2.calls.count()).toBe(1);
                    });
            });

            Async.it('should resolve the rule and execute the action.', () => {

                let executed = false,
                    action = jasmine.createSpy('action').and.callFake((input, done) => {
                        executed = true;
                        done();
                    });

                testRule.evaluate = true;
                testRule.resolve = [{ action: (a,b)=>action(a,b) }];
                ELF.rules.add(testRule);

                return ELF.trigger('TEST:Rule')
                    .tap(() => {
                        expect(action).toHaveBeenCalled();
                        expect(action.calls.count()).toBe(1);
                    });
            });

            Async.it('should reject the rule and execute the action.', () => {

                let executed = false,
                    action = jasmine.createSpy('action').and.callFake((input, done) => {
                        executed = true;
                        done();
                    });

                testRule.evaluate = false;
                testRule.reject = [{ action: (a,b)=>action(a,b) }];
                ELF.rules.add(testRule);

                return ELF.trigger('TEST:Rule')
                .tap(() => {
                    expect(action).toHaveBeenCalled();
                    expect(action.calls.count()).toBe(1);
                });

            });

            Async.it('should resolve the rule and execute the actions.', () => {

                let executed = false,
                    action = jasmine.createSpy('action').and.callFake((input, done) => {
                        executed = true;
                        done();
                    });

                testRule.resolve = [
                    { test: 'first',  action: (a,b)=>action(a,b) },
                    { test: 'second', action: (a,b)=>action(a,b) }];
                ELF.rules.add(testRule);

                return ELF.trigger('TEST:Rule')
                .tap(() => {
                    expect(action).toHaveBeenCalled();
                    expect(action.calls.count()).toBe(2);
                });

            });

            Async.it('terminates the action chain if an action errors out.', () => {

                let action1 = jasmine.createSpy('action').and.callFake((input) => {
                    return Q.reject('failure');
                });

                let action2 = jasmine.createSpy('action').and.callFake((input) => {
                    return Q();
                });

                testRule.resolve = [{ test: 'first', action: action1 }, { test: 'second', action: action2 }];
                ELF.rules.add(testRule);

                return ELF.trigger('TEST:Rule')
                    .tap(() => {
                        expect(action1).toHaveBeenCalled();
                        expect(action1.calls.count()).toBe(1);
                        expect(action2).not.toHaveBeenCalled();
                    });

            });

            Async.it('should reject the rule and execute the actions.', () => {

                let executed = false,
                    action = jasmine.createSpy('action').and.callFake((input, done) => {
                        executed = true;
                        done();
                    });

                testRule.evaluate = false;
                testRule.reject = [
                    { action: (a,b)=>action(a,b) },
                    { action: (a,b)=>action(a,b) }
                ];
                ELF.rules.add(testRule);

                return ELF.trigger('TEST:Rule').then(() => {
                    expect(action).toHaveBeenCalled();
                    expect(action.calls.count()).toBe(2);
                });
            });

            //currently, this feature is backed out
            Async.xit('passes context to the action as data.source', () => {

                let context = new SomeView(),
                    action = (evtData, resume) => {

                        expect(evtData.source).toBe(context);

                        resume();
                    };

                testRule.resolve = [{ action: (a,b)=>action(a,b) }];
                ELF.rules.add(testRule);

                return ELF.trigger('TEST:Rule', { }, context);

            });


            Async.it('returns an object', () => {
                return ELF.trigger('WHATEVER')
                .tap(res => {
                    expect(_.isObject(res)).toBe(true);
                });
            });

        });

    });

});

