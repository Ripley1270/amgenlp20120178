import SetTimeZoneView from 'core/views/SetTimeZoneView';
import setupLogpadMessages from 'logpad/resources/Messages';
import { MessageRepo } from 'core/Notify';
import Logger from 'core/Logger';

import * as specHelpers from 'test/helpers/SpecHelpers';
import PageViewTests from './PageView.specBase';

const preventDefault = $.noop;

export default class SetTimeZoneViewSuite extends PageViewTests {
    /**
     * Invoked before all unit tests are executed.
     * @returns {Q.Promise<void>}
     */
    beforeAll () {
        setupLogpadMessages();

        return super.beforeAll();
    }

    /**
     * Invoked after all unit tests are executed.
     * @returns {Q.Promise<void>}
     */
    afterAll () {
        MessageRepo.clear();

        return super.afterAll();
    }

    /**
     * Invoked before each unit test.
     * @returns {Q.Promise<void>}
     */
    beforeEach () {
        window.plugin = {
            TimeZoneUtil: jasmine.createSpyObj('TimeZoneUtil', [
                'setTimeZone',
                'getCurrentTimeZone',
                'getAvailableTimeZones',
                'restartApplication',
                'getOptions'
            ])
        };

        return super.beforeEach();
    }

    /**
     * Invoked after each unit test.
     * @returns {Q.Promise<void>}
     */
    afterEach () {
        this.removeTemplate && this.removeTemplate();

        return super.afterEach();
    }

    /**
     * Execute all class unit tests and those of the parent class.
     * @param {Object} context Context used to annotate method parameters.
     */
    executeAll (context) {
        super.executeAll(context);

        let methods = Object.getOwnPropertyNames(SetTimeZoneViewSuite.prototype);

        this.annotateAll(methods, context);
    }

    /**
     * Tests the back method of the SetTimeZoneView
     */
    testBack () {
        describe('method:back', () => {
            it('should navigate to the toolbox view.', () => {
                spyOn(this.view, 'navigate').and.stub();

                this.view.back({ preventDefault });

                expect(this.view.navigate).toHaveBeenCalledWith('toolbox');
            });
        });
    }

    /**
     * Tests the changeSelection method of the SetTimeZoneView
     */
    testChangeSelection () {
        describe('method:changeSelection', () => {
            it('should enable the set button.', () => {
                spyOn(this.view, 'enableButton').and.stub();

                this.view.currentTimeZone = { id: 'SomeOtherZone', displayName: '(GMT-02:00) SomeOtherZone' };
                this.view.$selectTimeZone.val('America/New_York').trigger('change');

                expect(this.view.enableButton).toHaveBeenCalled();
            });

            it('should disable the set button.', () => {
                spyOn(this.view, 'disableButton').and.stub();

                this.view.$selectTimeZone.val('America/New_York').trigger('change');

                expect(this.view.disableButton).toHaveBeenCalled();
            });
        });
    }

    /**
     * Tests the resolve method of the SetTimeZoneView
     */
    testResolve () {
        describe('method:resolve', () => {
            beforeEach(() => {
                LF.Wrapper.platform = 'browser';
                LF.StudyDesign.browser = {
                    timeZoneOptions: [
                        { tzId: 'Pacific/Honolulu', swId: 0 },
                        { tzId: 'America/Anchorage', swId: 1 },
                        { tzId: 'America/Phoenix', swId: 2 },
                        { tzId: 'America/Yellowknife', swId: 3 },
                        { tzId: 'America/Dawson_Creek', swId: 4 },
                        { tzId: 'America/Cancun', swId: 5 },
                        { tzId: 'America/Chicago', swId: 6 },
                        { tzId: 'America/New_York', swId: 21 }
                    ]
                };
            });

            Async.it('should resolve as being displayed in a browser.', () => {
                let request = this.view.resolve();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.timeZonesDisplay.length).toBe(1);
                    expect(this.view.timeZonesDisplay[0].id).toBe('America/New_York');
                });
            });

            Async.it('should fail to get all the timezones.', () => {
                spyOn(LF.Wrapper, 'exec').and.callFake((params) => {
                    params.execWhenWrapped();
                });
                spyOn(this.view, 'notify').and.resolve();
                window.plugin.TimeZoneUtil.getOptions.and.callFake(onSuccess => {
                    onSuccess();
                });
                window.plugin.TimeZoneUtil.getAvailableTimeZones.and.callFake((onSuccess, onError) => {
                    onError('getAvailableTimeZones failed');
                });

                spyOn(Logger.prototype, 'error').and.stub();

                let request = this.view.resolve();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(Logger.prototype.error).toHaveBeenCalledWith('Error occurred while obtaining the time zone list.', 'getAvailableTimeZones failed');
                });
            });

        Async.it('should fail to get the current timezone.', () => {
            spyOn(LF.Wrapper, 'exec').and.callFake((params) => {
                params.execWhenWrapped();
            });
            spyOn(this.view, 'notify').and.resolve();
            spyOn(Logger.prototype, 'error').and.stub();
            window.plugin.TimeZoneUtil.getOptions.and.callFake(onSuccess => {
                onSuccess();
            });
            window.plugin.TimeZoneUtil.getAvailableTimeZones.and.callFake(onSuccess => {
                onSuccess(['Africa/Johannesburg', 'America/Chicago']);
            });
            window.plugin.TimeZoneUtil.getCurrentTimeZone.and.callFake((onSuccess, onError) => {
                onError('getCurrentTimeZone failed');
            });

                let request = this.view.resolve();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(Logger.prototype.error).toHaveBeenCalledWith('Error occurred while obtaining the time zone list.', 'getCurrentTimeZone failed');
                });
            });

        Async.it('should resolve.', () => {
            spyOn(LF.Wrapper, 'exec').and.callFake((params) => {
                params.execWhenWrapped();
            });
            window.plugin.TimeZoneUtil.getOptions.and.callFake(onSuccess => {
                onSuccess();
            });
            window.plugin.TimeZoneUtil.getAvailableTimeZones.and.callFake(onSuccess => {
                onSuccess([{ id: 'America/Chicago', displayName: '(GMT-06:00) America/Chicago' }]);
            });
            window.plugin.TimeZoneUtil.getCurrentTimeZone.and.callFake(onSuccess => onSuccess({
                id: 'America/Chicago',
                displayName: '(GMT-06:00) America/Chicago'
            }));

                let request = this.view.resolve();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.currentTimeZone.id).toBe('America/Chicago');
                    expect(this.view.timeZonesDisplay).toEqual([{
                        id: 'America/Chicago',
                        displayName: '(GMT-06:00) America/Chicago'
                    }]);
                });
            });
        });
    }

    /**
     * Tests the setTimeZone method of the SetTimeZoneView
     */
    testSetTimeZone () {
        describe('method:setTimeZone', () => {
            Async.it('should cancel to set the Time Zone.', () => {
                spyOn(this.view, 'confirm').and.resolve(false);
                spyOn(Logger.prototype, 'operational').and.stub();

                let request = this.view.setTimeZone({ preventDefault });

                expect(request).toBePromise();

                return request.then(() => {
                    expect(Logger.prototype.operational).toHaveBeenCalledWith('User canceled setting the Time zone to: (GMT-05:00) America/New_York');
                });
            });

            Async.it('should not set the timezone (not wrapped).', () => {
                spyOn(this.view, 'confirm').and.resolve(true);
                spyOn(LF.Wrapper, 'exec').and.callFake((params) => {
                    params.execWhenNotWrapped();
                });

                let request = this.view.setTimeZone({ preventDefault });

                expect(request).toBePromise();

                return request.then(() => {
                    expect(LF.Wrapper.exec).toHaveBeenCalled();
                    expect(window.plugin.TimeZoneUtil.setTimeZone).not.toHaveBeenCalled();
                });
            });

            Async.it('should fail to set the timezone.', () => {
                spyOn(this.view, 'confirm').and.resolve(true);
                spyOn(this.view, 'notify').and.resolve();
                spyOn(LF.Wrapper, 'exec').and.callFake((params) => {
                    params.execWhenWrapped();
                });
                spyOn(Logger.prototype, 'error').and.stub();

                window.plugin.TimeZoneUtil.setTimeZone.and.callFake((onSuccess, onError) => {
                    onError('Unexpected Error');
                });

                let request = this.view.setTimeZone({ preventDefault });

                expect(request).toBePromise();

                return request.then(() => fail('method setTimeZone should have been rejected.'))
                    .catch(() => {
                        expect(LF.Wrapper.exec).toHaveBeenCalled();
                        expect(Logger.prototype.error).toHaveBeenCalledWith('Failed to set the Time zone', 'Unexpected Error');
                    });
            });

            Async.it('should fail save an operational log, but still restart the application.', () => {
                spyOn(this.view, 'confirm').and.resolve(true);
                spyOn(LF.Wrapper, 'exec').and.callFake((params) => {
                    params.execWhenWrapped();
                });
                spyOn(Logger.prototype, 'operational').and.reject('error');
                spyOn(this.view, 'reload').and.stub();

                window.plugin.TimeZoneUtil.setTimeZone.and.callFake(onSuccess => onSuccess());
                window.plugin.TimeZoneUtil.restartApplication.and.callFake(onSuccess => onSuccess());

                let request = this.view.setTimeZone({ preventDefault });

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.reload).toHaveBeenCalled();
                });
            });

            Async.it('should restart the application.', () => {
                spyOn(this.view, 'confirm').and.resolve(true);
                spyOn(LF.Wrapper, 'exec').and.callFake((params) => {
                    params.execWhenWrapped();
                });
                spyOn(Logger.prototype, 'operational').and.resolve();
                spyOn(this.view, 'reload').and.stub();

                window.plugin.TimeZoneUtil.setTimeZone.and.callFake(onSuccess => onSuccess());
                window.plugin.TimeZoneUtil.restartApplication.and.callFake(onSuccess => onSuccess());

                let request = this.view.setTimeZone({ preventDefault });

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.reload).toHaveBeenCalled();
                });
            });

            Async.it('should fail to restart the application, but still reload.', () => {
                spyOn(this.view, 'confirm').and.resolve(true);
                spyOn(LF.Wrapper, 'exec').and.callFake((params) => {
                    params.execWhenWrapped();
                });
                spyOn(Logger.prototype, 'operational').and.resolve();
                spyOn(Logger.prototype, 'error').and.resolve();
                spyOn(this.view, 'reload').and.stub();

                window.plugin.TimeZoneUtil.setTimeZone.and.callFake(onSuccess => onSuccess());
                window.plugin.TimeZoneUtil.restartApplication.and.callFake((onSuccess, onError) => {
                    onError('Unexpected Error');
                });

                let request = this.view.setTimeZone({ preventDefault });

                expect(request).toBePromise();

                return request.then(() => {
                    expect(Logger.prototype.error).toHaveBeenCalledWith('Failed to restart application on Time zone change.', 'Unexpected Error');
                    expect(this.view.reload).toHaveBeenCalled();
                });
            });
        });
    }

    /**
     * Tests the render method of the SetTimeZoneView
     */
    testRender () {
        describe('method:render', () => {
            Async.it('should render the view and have current time zone preselected.', () => {
                let request = this.view.render();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.$el.html()).toBeDefined();

                    if (this.view.selectActive) {
                        expect(this.view.$selectTimeZone.select2('data')[0].id).toEqual('America/New_York');
                    }
                });
            });

            Async.it('should not render the view.', () => {
                spyOn(this.view, 'buildHTML').and.reject('DOMError');

                let request = this.view.render();

                expect(request).toBePromise();

                return request.then(() => fail('Expected method render to have been rejected.'))
                    .catch(e => {
                        expect(e).toBe('DOMError');
                    });
            });
        });
    }
}
