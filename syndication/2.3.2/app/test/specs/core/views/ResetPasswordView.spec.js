import ResetPasswordView from 'core/views/ResetPasswordView';
import User from 'core/models/User';

import * as specHelpers from 'test/helpers/SpecHelpers';
import { resetStudyDesign } from 'test/helpers/StudyDesign';

class ResetPasswordViewSuite {
    constructor () {
        Async.beforeEach(() => this.beforeEach());

        Async.afterEach(() => this.afterEach());;
    }

    beforeEach () {
        resetStudyDesign();

        this.removeTemplate = specHelpers.renderTemplateToDOM('core/templates/reset-password.ejs');
        this.view = new ResetPasswordView();

        return this.view.render();
    }

    afterEach () {
        this.removeTemplate();
        localStorage.clear();

        return Q();
    }

    // TODO: This is a base class test in PageView, but not yet promoted to this stream.
    testId (id) {
        it(`should have an id of ${id}.`, () => {
            expect(this.view.id).toBe(id);
            expect(this.view.$el.attr('id')).toBe(id);
        });
    }

    testRender () {
        describe('method:render', () => {
            Async.it('should fail to render the view.', () => {
                spyOn(this.view, 'buildHTML').and.reject('DOMError');

                let request = this.view.render();

                expect(request).toBePromise();

                return request.then(() => fail('expected method render to be rejected.'))
                    .catch(e => expect(e).toBe('DOMError'));
            });

            Async.it('should render the view.', () => {
                let request = this.view.render();

                expect(request).toBePromise();

                return request.then(() => {
                    // Using the form element to prove render was successful.
                    let rendered = !!this.view.$('form').length;

                    expect(rendered).toBe(true);
                });
            });

            Async.it('should render the view (showPasswordRules)', () => {
                LF.StudyDesign.showPasswordRules = true;
                spyOn(this.view, 'i18n').and.callFake(strings => Q(strings));
                spyOn(LF.templates, 'display').and.returnValue('PASSWORD_RULES');

                let request = this.view.render();

                expect(request).toBePromise();

                return request.then(() => {
                    // Using the form element to prove render was successful.
                    let rendered = !!this.view.$('form').length;

                    expect(rendered).toBe(true);
                    expect(this.view.i18n).toHaveBeenCalledWith('PASSWORD_RULES');
                    expect(LF.templates.display)
                        .toHaveBeenCalledWith('DEFAULT:PasswordRules', { text:'PASSWORD_RULES' });
                });
            });
        });
    }

    testOnPasswordSaved () {
        describe('method:onPasswordSaved', () => {
            beforeEach(() => {
                spyOn(this.view, 'navigate').and.stub();
                LF.security = jasmine.createSpyObj('security', ['resetFailureCount']);
                this.view.user = new User({
                    username: 'System Administrator',
                    role: 'admin'
                });
            });

            Async.it('should navigate the the Reset Secret Question view.', () => {
                localStorage.setItem('Reset_Password_Secret_Question', true);

                let request = this.view.onPasswordSaved();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.navigate).toHaveBeenCalledWith('reset_secret_question');
                });
            });

            Async.it('should notify the user and navigate to the dashboard.', () => {
                spyOn(this.view, 'notify').and.resolve();

                let request = this.view.onPasswordSaved();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.navigate).toHaveBeenCalledWith('dashboard');
                });
            });
        });
    }
}

describe('ResetPasswordView', () => {
    let suite = new ResetPasswordViewSuite();

    suite.testId('reset-password-page');
    suite.testRender();
    suite.testOnPasswordSaved();

});

// describe('ResetPasswordView', () => {
//     let view;


//     afterEach(function () {


//         localStorage.clear();
//     });

//     it('should install the database.', function () {

//         LF.SpecHelpers.installDatabase();

//     });

//     it('should create the Subjects storage object and save the record.', function () {

//         LF.SpecHelpers.createSubject();
//         LF.SpecHelpers.saveSubject();

//     });

//     it('should redirect to \'login\'', function () {

//         LF.router.resetPassword();

//         expect(location).toEqual('login');

//     });

//     it('should render \'reset-password-page\'', function () {

//         LF.Data.resetPassword = true;
//         view = new LF.View.ResetPasswordView();

//         waitsFor(function () {
//             return view.$el.html();
//         }, 'the timeout callback to invoke.', 2000);

//         runs(function () {
//             expect(view.$el.attr('id')).toEqual('reset-password-page');
//         });

//     });

//     it('should navigate to \'dashboard\'', function () {

//         view.onPasswordSaved();
//         waits(300);

//         runs(function () {
//             // The user taps "OK" on password update success dialog box and is redirected to dashboard
//             $('#OK').trigger('click');

//             expect(location).toEqual('dashboard');
//         });

//     });

//     it('should unlock the subject\'s account and reset password and secret answer attempts and redirect to Reset Secret Question screen', function () {
//         localStorage.setItem('PHT_Login_Failure', 8);
//         localStorage.setItem('PHT_Security_Question_Failure', 7);
//         localStorage.setItem('Reset_Password_Secret_Question', true);

//         view.onPasswordSaved();

//         expect(localStorage.getItem('PHT_Login_Failure')).toEqual('0');
//         expect(localStorage.getItem('PHT_Security_Question_Failure')).toEqual('0');
//         expect(location).toEqual('reset_secret_question');

//     });

//     it('should reset the password', function () {
//         var oldPassword,
//             newPassword = 'lol123',
//             updatedPassword,
//             subject,
//             logModel = new LF.Model.Log(),
//             transmissionModel = new LF.Model.Transmission();

//         LF.SpecHelpers.createSubject();
//         subject = LF.Data.Subjects.at(0);
//         oldPassword = subject.get('subject_password');

//         $('#txtNewPassword').val(newPassword);
//         $('#txtConfirmPassword').val(newPassword);

//         view.validateNewPassword({target: $('#txtNewPassword')});

//         $('#submit').trigger('click');

//         waitsFor(function () {
//             updatedPassword = subject.get('subject_password');

//             return updatedPassword !== oldPassword;
//         }, 'subject password to be saved', 2000);

//         runs(function () {
//             expect(updatedPassword).toBeDefined();
//             expect(updatedPassword).not.toEqual(oldPassword);
//         });
//     });

//     it('should uninstall the database.', function () {

//         LF.SpecHelpers.uninstallDatabase();

//     });

// });
