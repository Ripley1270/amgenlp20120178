import SetTimeZoneViewSuite from './SetTimeZoneView.specBase';
import SetTimeZoneView from 'core/views/SetTimeZoneView';
import { resetStudyDesign } from 'test/helpers/StudyDesign';

import * as specHelpers from 'test/helpers/SpecHelpers';

class SetTimeZoneViewTests extends SetTimeZoneViewSuite {

    /**
     * Invoked before each unit test.
     * @returns {Q.Promise<void>}
     */
    beforeEach () {
        resetStudyDesign();
        this.removeTemplate = specHelpers.renderTemplateToDOM('core/templates/set-time-zone.ejs');

        this.view = new SetTimeZoneView();
        this.view.currentTimeZone = {id: 'America/New_York', displayName: '(GMT-05:00) America/New_York'};

        return specHelpers.initializeContent()
            .then(() => this.view.resolve())
            .then(() => this.view.render())
            .then(() => super.beforeEach());
    }
}

TRACE_MATRIX('US6725').
describe('SetTimeZoneView', () => {
    let suite = new SetTimeZoneViewTests();

    suite.executeAll({
        id: 'set-time-zone-page',
        template: '#set-time-zone-template',
        button: '#set_button',
        exclude: [
            // Should be tested individually
            'testProperty',
            'testAttribute',
            // PageView has no class name.
            'testClass',

            // DOM structure of view does not allow for these tests to pass.
            'testClearInputState',
            'testValidateInput',
            'testAddHelpText',
            'testRemoveHelpText',
            'testGetValidParent',
            'testInputSuccess',
            'testInputError',
            'testHideKeyboardOnEnter'
        ]
    });
});
