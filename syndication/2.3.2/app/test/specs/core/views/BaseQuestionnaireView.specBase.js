
import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';
import  'core/branching/branchingHelpers';
import Session from 'core/classes/Session';
import StudyDesign from 'core/classes/StudyDesign';
import Templates from 'core/collections/Templates';
import User from 'core/models/User';
import { Banner, MessageRepo } from 'core/Notify';
import setupCoreMessages from 'core/resources/Messages';
import Spinner from 'core/Spinner';
import ELF from 'core/ELF';
import CurrentContext from 'core/CurrentContext';

import * as helpers from 'test/helpers/SpecHelpers';

import testDesign from './BaseQuestionnaireViewTestData';
import {templates} from './BaseQuestionnaireViewTestData';
import * as lStorage from 'core/lStorage';

export default class BaseQuestionnaireViewTests {

    constructor () {
        this.location = null;
        this.rules = null;
        this.questionnaires = null;
        this.questions = null;
        this.affidavits = null;
        this.screens = null;
        this.templates = null;
        this.template = `<div id="questionnaire-template">
            <div id="questionnaire"></div>
            </div>`;

        this.Questionnaire = BaseQuestionnaireView;
        this.options = { id: 'TEST', schedule_id: 'TEST_Schedule' };
    }

    execTests () {
        describe('BaseQuestionnaireView tests', () => {
            // warning:  some of these test rely on the state of the questionnaire from the previous
            // questionnaire.  so removing or changing a step may change the functionality.

            Async.beforeEach(() => this.beforeEach());
            Async.beforeAll(() => this.beforeAll());
            Async.afterAll(() => this.afterAll());

            Async.it('test proper state when constructed.', () => this.constructorTests());
            Async.it('should fetch all the screens that belong to the questionnaire.', () => this.fetchScreensTest());
            Async.it('should fetch all the questions that belong to the questionnaire.',
                () => this.fetchQuestionsTest());

            Async.it('if affidavit == false, should not load an affidavit.',
                () => this.configureAffidavitAffidavitFalseTest());
            Async.xit('if affidavit undefined, should load the default affidavit.',
                () => this.configureAffidavitNoAffidavitTest());
            Async.it('should configure the affidavit.', () => this.configureDefaultAffidavitTest());
            Async.it('should configure a custom affidavit.', () => this.configureCustomAffidavitTest());
            Async.it('should fetch the correct question configuration.', () => this.correctQuestionTest());
            Async.it('shouldn\'t validate the screen.', () => this.validateInvalidScreenTest());
            Async.it('should validate the screen.', () => this.validataValidScreenTest());
            Async.it('should throw validation error from widget.', () => this.throwValidationErrorTest());
            Async.it('should throw default validation error.', () => this.throwDefaultValidationErrorTest());
            Async.it('ELF:QUESTIONNAIRE:SkipQuestions tests', () => this.elfSkipTest());
            Async.it('ELF:QUESTIONNAIRE:BackOut tests', () => this.elfBackoutTest());

            Async.it(['test that if the widget validation function sets the widget to not completed that the screen',
                'does not skip'].join(' '), () => this.WidgetValidateRuleTest());
            Async.it('should display the screen.', () => this.displayScreenTest());
            Async.it('should skip the question: mandatory == false.', () => this.skipSkippableQuestionTest());
            Async.it('should display the next screen.', () => this.nextShouldtransitionTest());
            Async.it('Branching tests.', () => this.conditionalBranchTest());
            Async.it('Next from last screen on questionnaire with affidavit should display the affidavit.',
                () => this.lastToAffidavitTest());
            Async.it('should attempt to skip the questions and navigate to the affidavit screen.',
                () => this.lastToAffidavitSkipTest());
            Async.it('should attempt to skip the question and fail.', () => this.skipMandatoryFailTest());
            Async.it('test answers getting cleared.', () => this.branchWithoutAndWithClear());
            Async.xit('test that clearBranchedResponses clears loops', () => this.branchAcrossLoopWithClear());
            Async.it('should clear the answers from the third and fourth screen (reverse branching).',
                () => this.branchReverseWithClear());
        });
    };

    /**
     * @returns {Q.Promise<void>} resolved when completed
     */
    beforeEach () {
        LF.StudyDesign = new StudyDesign(testDesign);
        LF.templates = new Templates(templates);

        LF.security = new Session();
        LF.security.activeUser = new User({id: 1});
        LF.spinner = Spinner;

        // spoof the router navigate();
        LF.router = {
            navigate: (url) => {
                this.location = url;
            },
            flash: () => LF.router
        };

        LF.Branching.branchFunctions.equal = function (branchObj) {
            return Q.Promise((resolve) => {
                let questionId = branchObj.branchParams.questionId,
                    response;

                if (branchObj.currentScreen === branchObj.branchFrom) {
                    response = LF.Branching.Helpers.responseForQuestionId(branchObj.answers, questionId);

                    if (typeof response !== 'undefined' && branchObj.branchParams.value === response) {
                        resolve(true);
                    } else {
                        resolve(false);
                    }
                } else {
                    resolve(false);
                }
            });
        };
        return Q();
    }

    /**
     * @returns {Q.Promise<void>} resolved when completed
     */
    beforeAll () {
        // SM: HACK: some other test file apparently starts history, but if run standalone
        // need to start it ourselves.
        try {
            Backbone.history.start({pushState: false});
        } catch (e) {
            // Guess it was already started.
        }

        setupCoreMessages();
        this.location = 'questionnaire';
        LF.SpecHelpers.installDatabase();
        LF.StudyDesign = new StudyDesign(testDesign);
        $('body').append(this.template);
        $.noty = $.noty ? $.noty : { closeAll: $.noop };

        return helpers.initializeContent();
    };

    /**
     * @returns {Q.Promise<void>} resolved when completed
     */
    afterAll () {
        MessageRepo.clear();
        return Q();
    }

    /**
     * check to verify that the constructor sets internal state correctly
     * @returns {Q.Promise<obj>} return the QuestionnaireView created
     */
    constructorTests () {
        const origInstanceOrdinal = lStorage.getItem('instance_ordinal'),
            myView = new this.Questionnaire(this.options);
        if (this.options.ordinal) {
            expect(myView.editMode).toBeFalsy('If options.ordinal is passed in, then should not be in edit mode');
            expect(myView.ordinal).toBe(this.options.ordinal,
                'If options.ordinal is passed in, then this.ordinal should be options.ordinal');
        } else {
            expect(myView.editMode).toBeTruthy('If options.ordinal is undefined, then should be in edit mode');
            expect(myView.ordinal).toBe(origInstanceOrdinal + 1,
                'If options.ordinal is undefined in, should be a new instance ordinal.');
            expect(myView.ordinal).toBe(origInstanceOrdinal + 1,
                'If options.ordinal is undefined in, saved instnce ordinal should be incremented.');
        }
        return Q(myView);
    }

    /**
     * intended to be used in a jasmine test as:
     * it('should fetch all the screens that belong to the questionnaire.', this.fetchScreensTest.bind(this));
     * @returns {Q.Promise<void>} resolved when completed
     */
    fetchScreensTest () {
         const myView = new this.Questionnaire(this.options);
         myView.prepScreens();

         expect(myView.data.screens[0].get('id')).toEqual('TEST_S_0');
         expect(myView.data.screens[1].get('id')).toEqual('TEST_S_1');
         expect(myView.data.screens[2].get('id')).toEqual('TEST_S_2');
         expect(myView.data.screens[3].get('id')).toEqual('TEST_S_3');
         expect(myView.data.screens[4].get('id')).toEqual('TEST_S_4');
         expect(myView.data.screens[5].get('id')).toEqual('TEST_S_5');
         expect(myView.data.screens[6].get('id')).toEqual('TEST_S_6');
         expect(myView.data.screens[7]).toBeUndefined();
         return Q();
     };

    /**
     * intended to be used in a jasmine test as:
     * it('should fetch all the questions that belong to the questionnaire.', this.fetchQuestionsTest.bind(this));
     * @returns {Q.Promise<void>} resolved when completed
     */
    fetchQuestionsTest () {
        const myView = new this.Questionnaire(this.options);
        myView.prepScreens();

        myView.prepQuestions();

        expect(myView.data.questions.at(0).get('id')).toEqual('TEST_Q_0');
        expect(myView.data.questions.at(1).get('id')).toEqual('TEST_Q_1');
        expect(myView.data.questions.at(2).get('id')).toEqual('TEST_Q_2');
        expect(myView.data.questions.at(3).get('id')).toEqual('TEST_Q_3');
        expect(myView.data.questions.at(4).get('id')).toEqual('TEST_Q_4');
        expect(myView.data.questions.at(5).get('id')).toEqual('TEST_Q_5');
        expect(myView.data.questions.at(6).get('id')).toEqual('TEST_Q_6');
        expect(myView.data.questions.at(7)).toBeUndefined();
        return Q();
    };

    /**
     * intended to be used in a jasmine test as:
     * it('if affidavit == false, should not load an affidavit.', this.configureAffidavitAffidavitFalseTest.bind(this));
     * @returns {Q.Promise<void>} resolved when completed
     */
    configureAffidavitAffidavitFalseTest () {
        const myView = new this.Questionnaire(this.options);
        myView.prepScreens();

        const screenCnt = myView.data.screens.length;
        myView.model.set('affidavit', false);
        myView.configureAffidavit();

        expect(myView.data.screens[screenCnt]).toBeUndefined();
        expect(myView.data.questions.match({krSig: 'AFFIDAVIT'}).length).toEqual(undefined);
        return Q();
    };

    /**
     * intended to be used in a jasmine test as:
     * it('if affidavit undefined, should load the default affidavit.', this.configureAffidavitNoAffidavitTest.bind(this));
     * @returns {Q.Promise<void>} resolved when completed
     */
    configureAffidavitNoAffidavitTest () {
        const myView = new this.Questionnaire(this.options);

        myView.model.set('affidavit', undefined);
        myView.prepScreens();
        const screenCnt = myView.data.screens.length;
        myView.configureAffidavit();
        expect(myView.data.screens[screenCnt].get('id')).toEqual('AFFIDAVIT');
        expect(myView.data.questions.match({krSig: 'AFFIDAVIT'}).length).toEqual(1);
        return Q();
    };

    /**
     * intended to be used in a jasmine test as:
     * it('should configure a custom affidavit.', this.configureCustomAffidavitTest.bind(this));
     * @returns {Q.Promise<void>} resolved when completed
     */
    configureCustomAffidavitTest () {
        const myView = new this.Questionnaire(this.options);

        myView.model.set('affidavit', 'CustomAffidavit');
        myView.prepScreens();
        const screenCnt = myView.data.screens.length;
        myView.configureAffidavit();

        expect(myView.data.screens[screenCnt].get('id')).toEqual('AFFIDAVIT');
        expect(myView.data.questions.match({krSig: 'CustomAffidavit'}).length).toEqual(1);
        return Q();
    };

    /**
     * intended to be used in a jasmine test as:
     * it('should configure the affidavit.', this.configureDefaultAffidavitTest.bind(this));
     * @returns {Q.Promise<void>} resolved when completed
     */
    configureDefaultAffidavitTest () {
        const myView = new this.Questionnaire(this.options);

        myView.model.set('affidavit', 'DEFAULT');
        myView.prepScreens();
        const screenCnt = myView.data.screens.length;
        myView.configureAffidavit();
        expect(myView.data.screens[screenCnt].get('id')).toEqual('AFFIDAVIT');
        expect(myView.data.questions.match({krSig: 'AFFIDAVIT'}).length).toEqual(1);
        return Q();
    };

    /**
     * intended to be used in a jasmine test as:
     * it('should fetch the correct question configuration.', () => this.correctQuestionTest);
     * @returns {Q.Promise<void>} resolved when completed
     */
    correctQuestionTest () {
        const myView = new this.Questionnaire(this.options);
        // build a new questionnaire and use resolve so extended classes can be set up
        // properly prior when used in extended class testing. (dtp)
        return myView.resolve()
            .then(() => myView.render())
            .then(() => {
                expect(myView.data.questions.at(0).id).toEqual('TEST_Q_0');
            });
    };

    /**
     * intended to be used in a jasmine test as:
     * it('shouldn\'t validate the screen.', this.validateInvalidScreenTest.bind(this));
     * @returns {Q.Promise<void>} resolved when completed
     */
    validateInvalidScreenTest () {
        const myView = new this.Questionnaire(this.options);
        return myView.resolve()
            .then(() => myView.render())
            .then(() => {
                myView.screenStack.push('TEST_S_1');
                return myView.displayScreen('TEST_S_1');
            })
            .tap(() => {
                expect(myView.validateScreens()).toBeFalsy();
            });
    };

    /**
     * intended to be used in a jasmine test as:
     * it('should trigger Widget the screen.', this.validataValidScreenTest.bind(this));
     * @returns {Q.Promise<void>} resolved when completed
     */
    validataValidScreenTest () {
        const myView = new this.Questionnaire(this.options);
        return myView.resolve()
            .then(() => myView.render())
            .then(() => {
                myView.screenStack.push('TEST_S_1');
                return myView.displayScreen('TEST_S_1');
            })
            .then(() => {
                let widget = myView.getQuestions()[0].widget;

                // Check the first radio button of the first question.
                widget.$('#TEST_W_1_radio_0').attr('checked', 'checked');

                // Manually call the response handler, passing fake event data.
                return widget.respond({
                    target: '#TEST_W_1_radio_0'
                });
            })
            .then(() => {
                expect(myView.validateScreens()).toBeTruthy();
            });
    };

    /**
     * intended to be used in a jasmine test as:
     * it('should throw validation error from widget.', this.throwValidationErrorTest.bind(this));
     * @returns {Q.Promise<void>} resolved when completed
     */
    throwValidationErrorTest () {
        const myView = new this.Questionnaire(this.options);
        return myView.resolve()
            .then(() => myView.render())
            .then(() => {
                myView.screenStack.push('TEST_S_1');
                return myView.displayScreen('TEST_S_1');
            })
            .then(() => {
                myView.data.invalidQuestions = [myView.getQuestions()[0]];
                let widget = myView.data.invalidQuestions[0].widget;
                spyOn(widget, 'displayError').and.returnValue(true);
                spyOn(Banner, 'show');
                myView.showValidationErrorMessage();

                // Fires from widget and does not fire default.
                expect(widget.displayError.calls.count()).toBe(1);
                expect(Banner.show.calls.count()).toBe(0);
            })
            .catch((e) => {
                fail(e);
            })
            .finally(() => {
                myView.data.invalidQuestions = [];
            });
    };

    /**
     * intended to be used in a jasmine test as:
     * it('should throw default validation error and save.', this.throwDefaultValidationErrorTest.bind(this));
     * @returns {Q.Promise<void>} resolved when completed
     */
    throwDefaultValidationErrorTest () {
        const myView = new this.Questionnaire(this.options);
        let widget;

        return myView.resolve()
            .then(() => myView.render())
            .then(() => {
                myView.screenStack.push('TEST_S_1');
                return myView.displayScreen('TEST_S_1');
            })
            .then(() => {
                widget = myView.getQuestions()[0].widget;

                spyOn(widget, 'displayError').and.returnValue(false);
                spyOn(Banner, 'show');

                myView.data.invalidQuestions = [myView.getQuestions()[0]];
                return myView.showValidationErrorMessage();
            })
            .catch((e = '') => {
                fail(`'myView.showValidationErrorMessage failed: ${e}`);
            })
            .tap(() => {
                // fires default, and not from widget
                expect(widget.displayError.calls.count()).toBe(1);
                // @TODO RTE: show is being called twice?
                expect(Banner.show.calls.count()).toBe(2);
                expect(Banner.show).toHaveBeenCalledWith(jasmine.objectContaining({
                    text: '{{ REQUIRED_ANSWER }}',
                    type: 'error'
                }));
            })
            .catch((e = '') => {
                fail(`Unexpected failure in checks: ${e}`);
            })
            .then(() => {
                let res = myView.save();
                expect(Q.isPromise(res)).toBe(true);
                return res;
            });
    }
    /**
     * intended to be used in a jasmine test as:
     * it(  'should display the screen.', this.displayScreenTest.bind(this) );
     * @returns {Q.Promise<void>} resolved when completed
     */
    displayScreenTest () {
        const myView = new this.Questionnaire(this.options);
        return myView.resolve()
            .then(() => myView.render())
            .then(() => {
                return myView.displayScreen('TEST_S_1');
            })
            .tap(() => {
                expect(myView.$('section').hasClass('TEST_S_1')).toBeTruthy('expected the screen to be "TEST_S_1"');
            })
            .catch((e) => {
                fail(e);
            })
            .then(() => {
                myView.resetAnswersSelected(['TEST_Q_1']);
                expect(myView.questionViews[1]).toBeUndefined();
            });
    };

    /**
     * intended to be used in a jasmine test as:
     * it('should skip the question: mandatory == false.', this.skipSkippableQuestionTest.bind(this) );
     * @returns {Q.Promise<void>} resolved when completed
     */
    skipSkippableQuestionTest () {
        let response,
            widgetValidationCalled = false;

        spyOn(ELF, 'trigger').and.callFake((ruleName, input = {}, context = {}) => {
            if (ruleName === 'WIDGET:Validation') {
                widgetValidationCalled = true;
            }
            return Q();
        });
        const myView = new this.Questionnaire(this.options);
        return myView.resolve()
            .then(() => myView.render())
            .then(() => {
                return myView.displayScreen('TEST_S_2');
            })
            .then(() => {
                return myView.skip();
            })
            .tap(()  => {
                expect(widgetValidationCalled)
                    .toBeFalsy('Expected WIDGET:Validation not to have been triggered since widgnet not completed');
                expect(myView.questionViews[1].widget.skipped.get('SW_Alias')).toEqual('TEST.0.TEST_Q_2_SKP');
            });
    };

    /**
     * intended to be used in a jasmine test as:
     * it('should display the next screen.', this.nextShouldtransitionTest.bind(this) );
     * @returns {Q.Promise<void>} resolved when completed
     */
    nextShouldtransitionTest () {
        let response;
        const myView = new this.Questionnaire(this.options);

        return myView.resolve()
            .then(() => myView.render())
            .then(() => {
                // this doesn't actually put TEST_S_1 in the stack, so stack should still just be
                // [ 'TEST_S_0']
                return myView.displayScreen('TEST_S_1');
            })
            .then(() => {
                return myView.next({});
            })
            .tap(() => {
                expect(myView.$('section').hasClass('TEST_S_2')).toBeTruthy('Expect to be on TEST_S_2');
            })
            .then(() => {
                return myView.previous();
            })
            .tap(() => {
                // you want to be at TEST_S_0 to verify that display screen didn't push anything into the stack
                expect(myView.$('section').hasClass('TEST_S_0')).toBeTruthy('Expect to be on TEST_S_0');
            })
            .then(() => {
                return myView.previous();
            })
            .tap(() => {
                expect(this.location).toEqual('dashboard');
            });
    };

    /**
     * intended to be used in a jasmine test as:
     * it('should branch to the fifth screen from the third screen.', this.conditionalBranchTest.bind(this) );
     * @returns {Q.Promise<void>} resolved when completed
     */
    conditionalBranchTest () {
        let response,
            questionView;

        const myView = new this.Questionnaire(this.options);

        return myView.resolve()
            .then(() => myView.render())
            .then(() => {
                myView.screenStack.push('TEST_S_3');
                return myView.displayScreen('TEST_S_3');
            })
            .then(() => {
                // Check the first radio button of the first question.

                questionView = _(myView.questionViews).find((questionView) => {
                    return questionView.id === 'TEST_Q_3';
                });
                // When triggering click through code, you must trigger twice to register it
                questionView.widget.$('#TEST_W_3_radio_2').trigger('click');
                questionView.widget.$('#TEST_W_3_radio_2').trigger('click');

                // Manually call the response handler, passing fake event data.
                const ret = questionView.widget.respond({
                    target: '#TEST_W_3_radio_2'
                });
                expect(ret.constructor.name).toBe('Promise');
                return ret;
            })
            .then(() => {
                return myView.next({});
            })
            .tap(() => {
                expect(myView.$('section').hasClass('TEST_S_5')).toBeTruthy('expected to be on Screen 5');
            })
            .catch((e = '') => {
                fail(`promise failed 1: ${e}`);
            })
            .then(() => {
                return myView.previous();
            })
            .tap(() => {
                expect(myView.$('section').hasClass('TEST_S_3')).toBeTruthy('expected to be on Screen 3');
            })
            .catch((e ='') => fail(`promise failed 2: ${e}`))
            .then(() => {
                questionView = _(myView.questionViews).find((questionView) => {
                    return questionView.id === 'TEST_Q_3';
                });
                // When triggering click through code, you must trigger twice to register it
                questionView.widget.$('#TEST_W_3_radio_3').trigger('click');
                questionView.widget.$('#TEST_W_3_radio_3').trigger('click');

                // Manually call the response handler, passing fake event data.
                const ret = questionView.widget.respond({
                        target: '#TEST_W_3_radio_3'
                    });
                expect(ret.constructor.name).toBe('Promise');
                return ret;
            })
            .then(() => {
                return myView.next({});
            })
            .tap(() => {
                expect(myView.$('section').hasClass('TEST_S_6')).toBeTruthy('expected to be on Screen 6');
            })
            .catch((e) => fail(`'promise failed 3: ${e}`))
            .then(() => {
                return myView.previous();
            })
            .tap(() => {
                expect(myView.$('section').hasClass('TEST_S_3')).toBeTruthy('expected 6 Prev to be 3');
            })
            .catch((e) => fail(`promise failed 4: ${e}`))
            .then(() => {
                let questionView;

                // Check the first radio button of the first question.
                questionView = _(myView.questionViews).find((questionView) => {
                    return questionView.id === 'TEST_Q_3';
                });
                // When triggering click through code, you must trigger twice to register it
                questionView.widget.$('#TEST_W_3_radio_0').trigger('click');
                questionView.widget.$('#TEST_W_3_radio_0').trigger('click');

                // Manually call the response handler, passing fake event data.
                questionView.widget.respond({
                    target: '#TEST_W_3_radio_0'
                });

                return myView.next({});
            })
            .catch((e) => fail(`promise failed 5: ${e}`))
            .then(() => {
                expect(myView.$('section').hasClass('TEST_S_4')).toBeTruthy('expected to be on Screen 4');
            })
            .then(() => {
                let questionView;

                // Check the first radio button of the first question.
                questionView = _(myView.questionViews).find((questionView) => {
                    return questionView.id === 'TEST_Q_4';
                });
                // When triggering click through code, you must trigger twice to register it
                questionView.widget.$('#TEST_W_4_radio_0').trigger('click');
                questionView.widget.$('#TEST_W_4_radio_0').trigger('click');

                // Manually call the response handler, passing fake event data.
                questionView.widget.respond({
                    target: '#TEST_W_4_radio_0'
                });

                return myView.next({});
            })
            .catch((e) => fail(e))
            .tap(() =>  {
                expect(myView.$('section').hasClass('AFFIDAVIT')).toBeTruthy('expected to be on Affidavit');
            })
            .then(() => {
                return myView.previous();
            })
            .catch((e) => fail(`promise failed 6: ${e}`))
            .then(() => {
                expect(myView.$('section').hasClass('TEST_S_4')).toBeTruthy();
            });
    };

    /**
     * intended to be used in a jasmine test as:
     * it('Next from last screen on questionnaire with affidavit should display the affidavit.', this.lastToAffidavitTest.bind(this) );
     * @returns {Q.Promise<void>} resolved when completed
     */
    lastToAffidavitTest () {
        const myView = new this.Questionnaire(this.options);
        return myView.resolve()
            .then(() => myView.render())
            .then(() => {
                return myView.displayScreen('TEST_S_6');
            })
            .then(() => {
                try {
                    return myView.next({});
                }
                catch (e) {
                    fail(e);
                }
            })
            .tap(() => {
                expect(myView.$('section').hasClass('AFFIDAVIT'))
                    .toBeTruthy('Expected to be at the AFFIDAVIT Screen');
            });
    };

    /**
     * intended to be used in a jasmine test as:
     * test to verify the results of QUESTIONNAIRE:Skip take the right paths
     * @returns {Q.Promise<null>} resolved when completed
     */
    elfBackoutTest () {
        const elfRule = 'QUESTIONNAIRE:BackOut';
        let elfResponse;

        const myView = new this.Questionnaire(this.options);
        spyOn(ELF, 'trigger').and.callFake((ruleName, input = {}, context = {}) => {
            if (ruleName.substring(0,elfRule.length) === elfRule) {
                return Q(elfResponse);
            }
            return Q({});
        });

        return myView.resolve()
            .then(() => myView.render())
            .then(() => {
                elfResponse = {preventDefault: true};
                return myView.previous();
            })
            .tap(()  => {
                // you want to be at TEST_S_0 to verify that display screen didn't push anything into the stack
                expect(myView.$('section').hasClass('TEST_S_0'))
                    .toBeTruthy('Expect skip to not move on and to be on TEST_S_0');
            })
            .then(() => {
                elfResponse = {preventDefault: false};
                return myView.previous();
            })
            .tap(()  => {
                expect(this.location).toEqual('dashboard');
            });
    };

    /**
     * intended to be used in a jasmine test as:
     * test to verify the results of QUESTIONNAIRE:Skip take the right paths
     * @returns {Q.Promise<null>} resolved when completed
     */
    elfSkipTest () {
        const elfRule = 'QUESTIONNAIRE:SkipQuestions';
        let elfResponse;

        const myView = new this.Questionnaire(this.options);
        spyOn(ELF, 'trigger').and.callFake((ruleName, input = {}, context = {}) => {
            if (ruleName.substring(0,elfRule.length) === elfRule) {
                return Q(elfResponse);
            }
            return Q({});
        });

        return myView.resolve()
            .then(() => myView.render())
            .then(() => {
                myView.screenStack.push('TEST_S_2');
                return myView.displayScreen('TEST_S_2');
            })
            .then(() => {
                elfResponse = {preventDefault: true};
                return myView.attemptToSkip();
            })
            .tap(()  => {
                // you want to be at TEST_S_0 to verify that display screen didn't push anything into the stack
                expect(myView.$('section').hasClass('TEST_S_2'))
                    .toBeTruthy('Expect skip to not move on and to be on TEST_S_2');
            })
            .then(() => {
                elfResponse = {preventDefault: false};
                return myView.attemptToSkip();
            })
            .tap(()  => {
                // you want to be at TEST_S_0 to verify that display screen didn't push anything into the stack
                expect(myView.$('section').hasClass('TEST_S_3'))
                    .toBeTruthy('Expect skip to move to next screen TEST_S_0');
            });
    };

    /**
     * test that if the widget validation function sets the widget to not completed that the screen does not skip
     * @returns {Q.Promise<void>} resolved when completed
     */
    WidgetValidateRuleTest () {
        let widgetValidationCalled = false;
        spyOn(ELF, 'trigger').and.callFake((ruleName, input = {}, context = {}) => {
            if (ruleName === 'WIDGET:Validation') {
                widgetValidationCalled = true;
                context.completed = false;
            }
            return Q({});
        });

        const myView = new this.Questionnaire(this.options);
        return myView.resolve()
            .then(() => myView.render())
            .then(() => {
                myView.screenStack.push('TEST_S_1');
                return myView.displayScreen('TEST_S_1');
            })
            .then(() => {
                let widget = myView.getQuestions()[0].widget;
                // dummy out validation to cause trigger
                widget.model.set('validation', () => {});

                // Check the first radio button of the first question.
                widget.$('#TEST_W_1_radio_0').attr('checked', 'checked');

                // Manually call the response handler, passing fake event data.
                return widget.respond({
                    target: '#TEST_W_1_radio_0'
                });
            })
            .then(() => {
                return myView.attemptToSkip();
            })
            .then(() => {
                expect(widgetValidationCalled)
                    .toBeTruthy('Expected WIDGET:Validation to have been triggered since widgnet was completed');
                expect(myView.$('section').hasClass('TEST_S_1')).toBeTruthy('expected to stay on Screen 1');
            });
    }

    /**
     * should attempt to skip the questions and navigate to the affidavit screen.
     * @returns {Q.Promise<void>} resolved when completed
     */
    lastToAffidavitSkipTest () {
        let widgetValidationCalled = false;

        spyOn(ELF, 'trigger').and.callFake((ruleName, input = {}, context = {}) => {
            if (ruleName === 'WIDGET:Validation') {
                widgetValidationCalled = true;
            }
            return Q({});
        });

        const myView = new this.Questionnaire(this.options);
        return myView.resolve()
            .then(() => myView.render())
            .then(() => {
                myView.screenStack.push('TEST_S_6');
                return myView.render('TEST_S_6');
            })
            .then(() => {
                return myView.attemptToSkip();
            })
            .tap(() => {
                expect(widgetValidationCalled)
                    .toBeFalsy('Expected WIDGET:Validation not to have been triggered since widgnet not completed');
                expect(myView.$('section').hasClass('AFFIDAVIT')).toBeTruthy();
                return Q();
            });
    }

    /**
     * intended to be used in a jasmine test as:
     * it('should attempt to skip the question and fail.', this.skipMandatoryFailTest.bind(this) );
     * @returns {Q.Promise<void>} resolved when completed
     */
    skipMandatoryFailTest () {
        const myView = new this.Questionnaire(this.options);
        return myView.resolve()
            .then(() => myView.render())
            .then(() => {
                return myView.displayScreen('TEST_S_1');
            })
            .then(() => {
                return myView.attemptToSkip();
            })
            .then((succeeded)  => {
                if (succeeded) {
                    fail('expected attemptToSkip() to fail');
                }
            });
    };

    /**
     * intended to be used in a jasmine test as:
     * it('should not clear the answers from the fifth screen.', this.branchWithoutAndWithClear.bind(this) );
     * @returns {Q.Promise<void>} resolved when completed
     */
    branchWithoutAndWithClear () {
        const myView = new this.Questionnaire(this.options);
        return myView.resolve()
            .then(() => myView.render())
            .then(() => {
                myView.screenStack.push('TEST_S_4');
                return myView.render('TEST_S_4');
            })
            .then(() => {
                //Push the question 5 to the question array
                myView.screenStack.push('TEST_S_5');
                return myView.render('TEST_S_5');
            })
            .then(() => {
                // branch to the sixth screen from the fourth screen.
                myView.screenStack.push('TEST_S_4');
                return myView.render('TEST_S_4');
            })
            .then(() => {
                // Check the first radio button of the first question.
                const questionView = _(myView.questionViews).find((questionView) => {
                    return questionView.id === 'TEST_Q_4';
                });
                // When triggering click through code, you must trigger twice to register it
                questionView.widget.$('#TEST_W_4_radio_1').trigger('click');
                questionView.widget.$('#TEST_W_4_radio_1').trigger('click');

                // Manually call the response handler, passing fake event data.
                const ret = questionView.widget.respond({
                    target: '#TEST_W_4_radio_1'
                });

                expect(ret.constructor.name).toBe('Promise');
                return ret;
            })
            .then(() => {
                return myView.next({});
            })
            .tap(() => {
                //it should be on screen 6
                expect(myView.$('section').hasClass('TEST_S_6'))
                    .toBeTruthy('Screen 4 Radio 1 expected to land at screen 6');
                //question 5 shouldn't be removed from question array (clearBranchedResponses : false)
                expect(myView.questionViews[2].id === 'TEST_Q_5').toBeTruthy();
            })
            .then(() => {
                //we already have question 4 and question 5 in the question array
                // branch to the sixth screen from the third screen.
                myView.screenStack.push('TEST_S_3');
                return myView.render('TEST_S_3');
            })
            .tap(() => {
                expect(myView.$('section').hasClass('TEST_S_3')).toBeTruthy('expected to be on Screen 3');
            })
            .then(() => {
                // Check the first radio button of the first question.
                const questionView = _(myView.questionViews).find((questionView) => {
                    return questionView.id === 'TEST_Q_3';
                });
                // When triggering click through code, you must trigger twice to register it
                questionView.widget.$('#TEST_W_3_radio_3').trigger('click');
                questionView.widget.$('#TEST_W_3_radio_3').trigger('click');

                // Manually call the response handler, passing fake event data.
                const ret = questionView.widget.respond({
                    target: '#TEST_W_3_radio_3'
                });

                expect(ret.constructor.name).toBe('Promise');
                return ret;
            })
            .then(() => {
                return myView.next({});
            })
            .then(() => {
                //it should be on screen 6
                expect(myView.$('section').hasClass('TEST_S_6'))
                    .toBeTruthy('Screen 3 Radio 3 expected to land at screen 6');

                //question 4 and question 5 should be removed from question array
                _.each(myView.questionViews, (question) => {
                    expect(question.id !== 'TEST_Q_4' && question.id !== 'TEST_Q_5').toBeTruthy();
                });
            });
    };

    /**
     * intended to be used in a jasmine test as:
     * it('test that clearBranchedResponses clears loops', this.branchAcrossLoopWithClear.bind(this) );
     * @returns {Q.Promise<void>} resolved when completed
     */
    branchAcrossLoopWithClear () {
        fail('TODO: test case not written.');
        return Q();
    };

    /**
     * intended to be used in a jasmine test as:
     * it('should clear the answers from the third and fourth screen (reverse branching).', this.branchReverseWithClear.bind(this) );
     * @returns {Q.Promise<void>} resolved when completed
     */
    branchReverseWithClear () {
        const myView = new this.Questionnaire(this.options);
        return myView.resolve()
            .then(() => myView.render())
            .then(() => {
                myView.screenStack.push('TEST_S_4');
                return myView.render('TEST_S_4');
            })
            .then(() => {
                //Push the question 5 to the question array
                myView.screenStack.push('TEST_S_3');
                return myView.render('TEST_S_3');
            })
            .then(() => {
                //we already have question 4 and question 3 in the question array

                // branch to the seventh screen from the third screen.
                myView.screenStack.push('TEST_S_5');
                return myView.render('TEST_S_5');
            })
            .then(() => {
                // Check the first radio button of the first question.
                const questionView = _(myView.questionViews).find((questionView) => {
                    return questionView.id === 'TEST_Q_5';
                });
                // When triggering click through code, you must trigger twice to register it
                questionView.widget.$('#TEST_W_5_radio_0').trigger('click');
                questionView.widget.$('#TEST_W_5_radio_0').trigger('click');

                // Manually call the response handler, passing fake event data.
                return questionView.widget.respond({
                    target: '#TEST_W_5_radio_0'
                });
            })
            .then(() => {
                return myView.next({});
            })
            .tap(() => {
                let questions = myView.questionViews;

                //it should be on screen 6
                expect(myView.$('section').hasClass('TEST_S_2')).toBeTruthy();

                //question 4 and question 5 should be removed from question array
                _.each(myView.questionViews, (question) => {
                    expect(question.id !== 'TEST_Q_3' && question.id !== 'TEST_Q_4').toBeTruthy();
                });
            });
    };
}
