import Transmission from 'core/models/Transmission';
import User from 'core/models/User';
import { Banner } from 'core/Notify';
import * as utils from 'core/utilities';
import Spinner from 'core/Spinner';

import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';

import ActivationBaseViewSuite from 'test/specs/core/views/ActivationBaseView.specBase';

export default class PasswordBaseViewSuite extends ActivationBaseViewSuite {

    afterEach () {
        localStorage.clear();

        return super.afterEach();
    }

    executeAll (context) {
        super.executeAll(context);

        let methods = Object.getOwnPropertyNames(PasswordBaseViewSuite.prototype);

        this.annotateAll(methods, context);
    }

    testBack () {
        describe('method:back', () => {
            it('should have a back method defined.', () => {
                expect(this.view.back).toBeDefined();
                expect(this.view.back).toEqual(jasmine.any(Function));
            });
        });
    }

    testOnPasswordSaved () {
        describe('method:onPasswordSaved', () => {
            it('should have a onPasswordSaved method defined.', () => {
                expect(this.view.onPasswordSaved).toBeDefined();
                expect(this.view.onPasswordSaved).toEqual(jasmine.any(Function));
            });
        });
    }

    testCreateTransmission () {
        let params;

        beforeEach(() => {
            params = JSON.stringify({
                password: '12345',
                secret_question: 1,
                secret_answer: 'Hello World'
            });
        });
        describe('method:createTransmission', () => {
            Async.it('should fail to create a transmission.', () => {
                spyOn(Transmission.prototype, 'save').and.reject('DatabaseError');

                let request = this.view.createTransmission(params);

                expect(request).toBePromise();

                return request.then(() => fail('Expected method createTransmission to be rejected.'))
                    .catch(e => expect(e).toBe('DatabaseError'));
            });

            Async.it('should create a transmission.', () => {
                let request = this.view.createTransmission(params);

                expect(request).toBePromise();

                return request.then(id => {
                    expect(id).toEqual(jasmine.any(Number));
                });
            });
        });
    }

    // TODO: PasswordBaseView.submit() needs to be revised and tested.
    testSubmit () {
        xdescribe('method:submit', () => {

        });
    }
}
