import { sharedBehavior } from 'test/helpers/SpecHelpers';


export default class ModalViewTests {
    constructor () {
        Async.beforeEach(() => this.beforeEach());

        Async.afterEach(() => this.afterEach());;
    }

    beforeEach () {
        return Q();
    }

    afterEach () {
        $(this.view.template).remove();

        return Q();
    }

    testId (id) {
        it(`should have an id of ${id}.`, () => {
            expect(this.view.id).toBe('modal');
            expect(this.view.$el.attr('id')).toBe('modal');
        });
    }

    testClass (className) {
        it(`should have a class of ${className}.`, () => {
            expect(this.view.className).toBe(className);
            expect(this.view.$el.attr('class')).toBe(className);
        });
    }

    testAttribute (attribute, value) {
        it(`should have an attribute ${attribute} with a value of ${value}.`, () => {
            expect(this.view.attributes[attribute]).toBe(value);
            expect(this.view.$el.attr(attribute)).toBe(value);
        });
    }

    testGetTemplate () {
        describe('method:getTemplate', () => {
            it('should return the template.', () => {
                let tpl = this.view.getTemplate();

                expect(tpl).toBeDefined();
                expect(tpl).toEqual(jasmine.any(Function));
            });
        });
    }

    testTeardown () {
        describe('method:teardown', () => {
            it('should tear down the modal.', () => {
                spyOn(this.view, 'remove').and.stub();

                this.view.teardown();

                expect(this.view.remove).toHaveBeenCalled();
            });
        });
    }

    testShow (strings) {
        describe('method:show', () => {
            beforeEach(() => {
                spyOn($.fn, 'modal');
            });

            it('should show the modal.', () => {
                spyOn(this.view, 'render').and.stub();
                spyOn(this.view, 'isShown').and.returnValue(false);

                this.view.show(strings);

                expect($.fn.modal).toHaveBeenCalled();
                expect(this.view.render).toHaveBeenCalledWith(strings);
            });

            it('should not show the modal.', () => {
                spyOn(this.view, 'isShown').and.returnValue(true);

                this.view.show(strings);

                expect($.fn.modal).not.toHaveBeenCalled();
            });
        });
    }

    testHide () {
        describe('method:hide', () => {
            Async.it('should hide the modal', () => {
                spyOn($.fn, 'modal').and.stub();

                let request = this.view.hide();

                expect(request).toBePromise();

                return request.then(() => {
                    expect($.fn.modal).toHaveBeenCalledWith('hide');
                });
            });
        });
    }

    testAddGlyphicon () {
        describe('method:addGlyphicon', () => {
            it('should prepend the provided span to the modal title.', () => {
                let span = '<span class="fa fa-minus-circle"></span>';

                spyOn($.fn, 'prepend').and.stub();

                this.view.addGlyphicon(span);

                expect($.fn.prepend).toHaveBeenCalledWith(span);
            });
        });
    }

    testFindModalElement (strings) {
        describe('method:findModalElement', () => {
            beforeEach(() => {
                this.view.render(strings);
            });

            it('should return the modal-title (no arguments).', () => {
                let $ele = this.view.findModalElement();

                expect($ele.hasClass('modal-title')).toBe(true);
            });

            it('should return the modal-title (first argument provided).', () => {
                let $ele = this.view.findModalElement('modal');

                expect($ele.hasClass('modal-title')).toBe(true);
            });

            it('should return the modal-title (both arguments provided).', () => {
                let $ele = this.view.findModalElement('modal', 'modal-title');

                expect($ele.hasClass('modal-title')).toBe(true);
            });
        });
    }

    testAddModalClass () {
        describe('method:addModalClass', () => {
            it('should add the modal class (argument).', () => {
                expect(this.view.$el.hasClass('error')).toBe(false);

                this.view.addModalClass('error');

                expect(this.view.$el.hasClass('error')).toBe(true);
            });

            it('should add the modal class (property).', () => {
                this.view.styleClass = 'error';

                expect(this.view.$el.hasClass('error')).toBe(false);

                this.view.addModalClass();

                expect(this.view.$el.hasClass('error')).toBe(true);
            });
        });
    }

    testApplyStyles (options) {
        describe('method:applyStyles', () => {
            it('should apply error styles.', () => {
                spyOn(this.view, 'addGlyphicon').and.stub();
                spyOn(this.view, 'addModalClass').and.stub();

                this.view.modalType = options.type;
                this.view.applyStyles();

                expect(this.view.addGlyphicon).toHaveBeenCalledWith(`<span class="fa fa-${options.icon}"></span>`);
                expect(this.view.addModalClass).toHaveBeenCalledWith(options.type);
            });
        });
    }
}
