import PasswordBaseView from 'core/views/PasswordBaseView';
import PasswordBaseViewSuite from './PasswordBaseView.specBase';

class Suite extends PasswordBaseViewSuite {
    beforeEach () {
        let template = `
        <script id="password-base-tpl" type="text/template">
            <form>
                <div class="form-group">
                    <input id="txtNewPassword" type="text />
                </div>
                <div class="form-group">
                    <input id="txtConfirmPassword" type="text" />
                </div>
                <button id="submit">Submit</button>
            </form>
        </script>`;

        $('body').append(template);

        this.view = new PasswordBaseView();
        this.view.template = '#password-base-tpl';

        return this.view.buildHTML({ })
            .then(() => super.beforeEach());
    }

    afterEach () {
        $('#password-base-tpl').remove();

        return super.afterEach();
    }
}

describe('PasswordBaseView', () => {
    let suite = new Suite();

    suite.executeAll({
        id: 'page',
        tagName: 'div',
        template: '#password-base-tpl',
        exclude: [
            // Should be tested individually
            'testProperty',
            'testAttribute',
            // PageView has no class name.
            'testClass'
        ]
    });
});
