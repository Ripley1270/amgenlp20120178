//jscs:disable requireArrowFunctions
import Dashboard from 'core/models/Dashboard';
import QuestionnaireCompletionView from 'core/views/QuestionnaireCompletionView';
import QuestionnaireView from 'core/views/QuestionnaireView';
import PatientSession from 'core/classes/PatientSession';
import StudyDesign from 'test/helpers/StudyDesign';
import ELF from 'core/ELF';
import Data from 'core/Data';
import * as lStorage from 'core/lStorage';
import * as helpers from 'test/helpers/SpecHelpers';
import CurrentContext from 'core/CurrentContext';

describe('QuestionnaireCompletionView', () => {

    let  data;

    Async.beforeEach(() => {
        const questionnaireDashboard = new Dashboard({
            id: 1,
            subject_id: '9876',
            SU: 'DAILY_DIARY',
            questionnaire_id: 'DAILY_DIARY',
            instance_ordinal: 1,
            started: new Date().ISOStamp(),
            phase: 10
        });

        data = {
            view: undefined,
            viewOpts: {}
        };

        CurrentContext.init();
        CurrentContext().setContextByRole('site');
        LF.security = new PatientSession();
        spyOn(ELF, 'trigger').and.callThrough();

        helpers.installDatabase();
        helpers.createSubject();

        return (helpers.saveSubject()
            .then(subjects => {
                data.viewOpts.questionnaire = new QuestionnaireView({
                    subject: subjects.at(0),
                    id: 'DAILY_DIARY',
                    ordinal: 0
                });
                Data.Questionnaire = data.viewOpts.questionnaire;
                data.viewOpts.questionnaire.data.dashboard = questionnaireDashboard;
            })
        );
    });

    afterEach(() => {
        LF.security = null;
        data.viewOpts.questionnaire.remove();
    });

    // Pending DE15671
    Async.xit(`should transmit if no timeouts have occured, then navigate to dashboard`, () => {
        data.view = new QuestionnaireCompletionView(data.viewOpts);
        spyOn(data.view, 'navigate').and.stub();

        return (data.view.render()
            .then(() => {
                const elfEvents = ELF.trigger.calls.allArgs().map(args => args[0]);

                expect(data.view.navigate).toHaveBeenCalledWith('dashboard');
                expect(elfEvents).not.toContain(`COMPLETIONVIEW:SavedDiarySessionTimeout`);
                expect(elfEvents).not.toContain(`COMPLETIONVIEW:SavedDiaryQuestionnaireTimeout`);
                expect(elfEvents).toContain(`QUESTIONNAIRE:Transmit/${data.viewOpts.questionnaire.id}`);
            })
        );
    });

    // Pending DE15671
    Async.xit(`should not transmit, and trigger COMPLETIONVIEW:SavedDiarySessionTimeout if sessionTimeout has occured, then logout`, () => {
        data.viewOpts.questionnaire.sessionTimeout = true;
        data.view = new QuestionnaireCompletionView(data.viewOpts);

        spyOn(data.view, 'navigate').and.stub();

        return (data.view.render()
            .then(() => {
                const elfEvents = ELF.trigger.calls.allArgs().map(args => args[0]);

                expect(data.view.navigate).toHaveBeenCalledWith('login');
                expect(elfEvents).toContain(`COMPLETIONVIEW:SavedDiarySessionTimeout`);
                expect(elfEvents).not.toContain(`COMPLETIONVIEW:SavedDiaryQuestionnaireTimeout`);
                expect(elfEvents).not.toContain(`QUESTIONNAIRE:Transmit/${data.viewOpts.questionnaire.id}`);
            })
        );
    });

    // Pending DE15671
    Async.xit(`should not transmit, and notify DIARY_TIMEOUT if timeout has occured, then navigate to dashboard`, () => {
        data.viewOpts.questionnaire.questionnaireSessionTimeout = true;
        data.view = new QuestionnaireCompletionView(data.viewOpts);
        spyOn(data.view, 'navigate').and.stub();

        return (data.view.render()
            .then(() => {
                const elfEvents = ELF.trigger.calls.allArgs().map(args => args[0]);

                expect(data.view.navigate).toHaveBeenCalledWith('dashboard');
                expect(elfEvents).not.toContain(`COMPLETIONVIEW:SavedDiarySessionTimeout`);
                expect(elfEvents).toContain(`COMPLETIONVIEW:SavedDiaryQuestionnaireTimeout`);
                expect(elfEvents).not.toContain(`QUESTIONNAIRE:Transmit/${data.viewOpts.questionnaire.id}`);
            })
        );
    });

    Async.it (`should thrown an error if questionnaire is not provided`, () => {
        Data.Questionnaire = null;
        data.view = new QuestionnaireCompletionView();

        return (data.view.render()
            .then(noErr => fail('Expected an error, did not get it'))
            .catch(err => {
                expect(err.message).toBe(`No Questionnaire Data found.`);
            })
            .then(() => {
                const elfEvents = ELF.trigger.calls.allArgs().map(args => args[0]);

                expect(elfEvents).not.toContain(`COMPLETIONVIEW:SavedDiarySessionTimeout`);
                expect(elfEvents).not.toContain(`COMPLETIONVIEW:SavedDiaryQuestionnaireTimeout`);
                expect(elfEvents).not.toContain(`QUESTIONNAIRE:Transmit/${data.viewOpts.questionnaire.id}`);
            })
        );
    });
});
