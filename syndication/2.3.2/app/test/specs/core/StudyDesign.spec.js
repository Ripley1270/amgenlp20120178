import Design from 'core/assets/study-design';

import StudyDesign from 'core/classes/StudyDesign';

describe('Study Design', () => {

    beforeEach(() => {

    });

    afterEach(() => {

    });

    describe('study validation', () => {

        it('should validate successfully', () => {
            let design = Design,
                res = new StudyDesign(design);

            expect(res.failedValidation).toEqual(false);
        });

        it('should fail because of enabled custom environments', () => {
            expect(Design.disableCustomEnvironments).not.toEqual(false);
            expect(LF.CoreSettings.disableCustomEnvironments).not.toEqual(false);
        });

        it('should fail because of missing question', () => {
            // switch to using _.deepClone whenever lodash is updated
            let design = JSON.parse(JSON.stringify(Design));

            design.questions = _.without(design.questions, _.findWhere(design.questions, {id: 'ADD_USERNAME'}));

            let res = new StudyDesign(design);

            expect(res.failedValidation).toEqual(true);
        });

        it('should fail due to duplicate question', () => {
            // switch to using _.deepClone whenever lodash is updated
            let design = JSON.parse(JSON.stringify(Design)),
                duplicateQuestion = _.findWhere(design.questions, {id: 'ADD_USERNAME'});

            design.questions.push(duplicateQuestion);

            let res = new StudyDesign(design);

            expect(res.failedValidation).toEqual(true);
        });

        it('should fail due to missing id', () => {
            // switch to using _.deepClone whenever lodash is updated
            let design = JSON.parse(JSON.stringify(Design)),
                question = _.findWhere(design.questions, {id: 'ADD_USERNAME'});

            question.id = undefined;

            let res = new StudyDesign(design);

            expect(res.failedValidation).toEqual(true);
        });
    });
});
