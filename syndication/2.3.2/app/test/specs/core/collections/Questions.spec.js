import * as helpers from 'test/helpers/SpecHelpers';
import Questions from 'core/collections/Questions';

describe('Questions', function () {

    var collection;

    beforeEach(function () {
        collection = new Questions([
            {
                id          : 'EQ5D_Q_1',
                text        : 'QUESTION_1',
                className   : 'EQ5D_Q_1',
                widget      : 'EQ5D_W_1'
            }, {
                id          : 'SF36_Q_3_A',
                help        : 'QUESTION_3_HELP',
                text        : 'QUESTION_3_A',
                className   : 'SF36_Q_3_A',
                widget      : 'SF36_W_3'
            }, {
                id          : 'SF36_Q_3_B',
                help        : 'QUESTION_3_HELP',
                text        : 'QUESTION_3_B',
                className   : 'SF36_Q_3_B',
                widget      : 'SF36_W_3'
            }
        ]);

    });

    it('should have a model', function () {

        expect(collection.model).toBeDefined();

    });

    it('should NOT have a table', function () {

        expect(collection.table).not.toBeDefined();

    });

    it('should return 1 model', function () {

        var models = collection.match({
                id  : 'EQ5D_Q_1'
            });

        expect(models.length).toEqual(1);

    });

    it('should return 2 models', function () {

        var models = collection.match({
                help    : 'QUESTION_3_HELP'
            });

        expect(models.length).toEqual(2);

    });

    it('should return 0 models', function () {

        var models = collection.match({
                id : 2
            });

        expect(models.length).toBeFalsy();

    });

    it('should return a missing arguments error (match)', function () {

        expect(function () {
            var models = collection.match();
        }).toThrowError('Invalid number of arguments.');

    });

});
