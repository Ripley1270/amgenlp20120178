import * as helpers from 'test/helpers/SpecHelpers';
import * as studydesign from 'test/helpers/StudyDesign';
import Languages from 'core/collections/Languages';

describe('Languages', function () {

    var collection,
        prefLanguage = LF.Preferred.language,
        prefLocale = LF.Preferred.locale,
        defaultLanguage = LF.StudyDesign.defaultLanguage,
        defaultLocale = LF.StudyDesign.defaultLocale;

    beforeEach(function () {

        LF.StudyDesign.defaultLanguage = 'en';
        LF.StudyDesign.defaultLocale = 'US';

        collection = new Languages([
            {
                namespace: 'CORE',
                language: 'en',
                locale: 'US',
                direction: 'ltr',
                resources: {
                    DIARY: 'Core Default',
                    STUDY: 'Core Default',
                    CORE: 'Core Default',
                    DIARY2: 'Core NoTrans',
                    STUDY2: 'Core NoTrans',
                    CORE2: 'Core NoTrans'
                },
                dates: {
                    CORE_DATE: 'Core Default Date'
                },
                dateConfigs: {
                    STUDY_DATE_CONFIGS: 'Core Default Date Configs'
                }
            }, {
                namespace: 'CORE',
                language: 'vi',
                locale: 'VI',
                direction: 'ltr',
                resources: {
                    DIARY: 'Core Pref',
                    STUDY: 'Core Pref',
                    CORE: 'Core Pref'
                },
                dates: {
                    CORE_DATE: 'Core Pref Date'
                },
                dateConfigs: {
                    STUDY_DATE_CONFIGS: 'Core Pref Date Configs'
                }
            }, {
                namespace: 'CORE',
                language: 'ar',
                locale: 'EG',
                direction: 'rtl',
                resources: {}
            }, {
                namespace: 'STUDY',
                language: 'vi',
                locale: 'VI',
                direction: 'ltr',
                resources: {
                    DIARY: 'Study Pref',
                    STUDY: 'Study Pref'
                },
                dateConfigs: {
                    STUDY_DATE_CONFIGS: 'Study Pref Date Configs'
                }
            }, {
                namespace: 'STUDY',
                language: 'en',
                locale: 'US',
                direction: 'ltr',
                resources: {
                    DIARY: 'Study Default',
                    STUDY: 'Study Default',
                    DIARY2: 'Study NoTrans',
                    STUDY2: 'Study NoTrans'
                },
                dateConfigs: {
                    STUDY_DATE_CONFIGS: 'Study Default Date Configs'
                }
            }, {
                namespace: 'DIARY',
                language: 'vi',
                locale: 'VI',
                direction: 'ltr',
                resources: {
                    DIARY: 'Diary Pref'
                }
            }, {
                namespace: 'DIARY',
                language: 'en',
                locale: 'US',
                direction: 'ltr',
                resources: {
                    DIARY: 'Diary Default',
                    DIARY2: 'Diary NoTrans'
                }
            }, {
                namespace: 'DIARY',
                language: 'en',
                locale: 'UK',
                direction: 'ltr',
                resources: {
                    DIARY_TEST: 'Diary with same Language'
                }
            }, {
                namespace: 'DIARY',
                language: 'fr',
                locale: 'US',
                direction: 'ltr',
                resources: {
                    DIARY_TEST: 'Diary with same Locale'
                }
            }
        ]);

    });

    afterEach(function () {
        LF.Preferred.language = prefLanguage;
        LF.Preferred.locale = prefLocale;
        LF.StudyDesign.defaultLanguage = defaultLanguage;
        LF.StudyDesign.defaultLocale = defaultLocale;
        localStorage.clear();
    });

    it('should have a model', function () {

        expect(collection.model).toBeDefined();

    });

    it('should display the correct string (Core Default). Core namespace and default language and locale', function (done) {
        var arr = [];

        collection.fetch('CORE', {}).then(
            function (result) {
                arr.push(result);

                return collection.fetch('CORE', {namespace: 'STUDY'});
            }
        ).then(
            function (result) {
                arr.push(result);

                return collection.fetch('CORE', {namespace: 'DIARY'});
            }
        ).then(
            function (result) {
                arr.push(result);

                expect(arr[0]).toEqual('Core Default');
                expect(arr[1]).toEqual('Core Default');
                expect(arr[2]).toEqual('Core Default');
                done();
            }
        );

    });

    it('should display the correct string (Study Default). Study namespace and default language and locale', function (done) {

        var arr = [];

        collection.fetch('STUDY', {}).then(
            function (result) {
                arr[0] = result;

                return collection.fetch('STUDY', {namespace: 'STUDY'});
            }
        ).then(
            function (result) {
                arr[1] = result;

                return collection.fetch('STUDY', {namespace: 'DIARY'});
            }
        ).then(
            function (result) {
                arr[2] = result;

                expect(arr[0]).toEqual('Study Default');
                expect(arr[1]).toEqual('Study Default');
                expect(arr[2]).toEqual('Study Default');
                done();
            }
        );
    });

    it('should display the correct string (Diary Default). Diary namespace and default language and locale', function (done) {
        collection.fetch('DIARY', {namespace: 'DIARY'}).then(
            function (result) {
                expect(result).toEqual('Diary Default');
                done();
            }
        );
    });

    it('should display the correct string (Core Pref). Core namespace and preferred language and locale', function (done) {

        var arr = [];

        LF.Preferred.language = 'vi';
        LF.Preferred.locale = 'VI';

        collection.fetch('CORE', {}).then(
            function (result) {
                arr[0] = result;

                return collection.fetch('CORE', {namespace: 'STUDY'});
            }
        ).then(function (result) {
                arr[1] = result;

                return collection.fetch('CORE', {namespace: 'DIARY'});
            }
        ).then(function (result) {
                arr[2] = result;

                expect(arr[0]).toEqual('Core Pref');
                expect(arr[1]).toEqual('Core Pref');
                expect(arr[2]).toEqual('Core Pref');
                done();
            }
        );
    });

    it('should display the correct string (Core NoTrans). Missing Translation. Core namespace and default language and locale', function (done) {
        var arr = [];

        LF.Preferred.language = 'vi';
        LF.Preferred.locale = 'VI';

        collection.fetch('CORE2', {}).then(
            function (result) {
                arr[0] = result;

                return collection.fetch('CORE2', {namespace: 'STUDY'});
            }
        ).then(
            function (result) {
                arr[1] = result;

                return collection.fetch('CORE2', {namespace: 'DIARY'});
            }
        ).then(
            function (result) {
                arr[2] = result;

                expect(arr[0]).toEqual('Core NoTrans');
                expect(arr[1]).toEqual('Core NoTrans');
                expect(arr[2]).toEqual('Core NoTrans');
                done();
            }
        );
    });

    it('should display the correct string (Study Pref). Study namespace and preferred language and locale', function (done) {
        var arr = [];

        LF.Preferred.language = 'vi';
        LF.Preferred.locale = 'VI';

        collection.fetch('STUDY', {}).then(
            function (result) {
                arr[0] = result;

                return collection.fetch('STUDY', {namespace: 'DIARY'});
            }
        ).then(
            function (result) {
                arr[1] = result;

                expect(arr[0]).toEqual('Study Pref');
                expect(arr[1]).toEqual('Study Pref');
                done();
            });
    });

    it('should display the correct string (Study NoTrans). Missing Translation. Study namespace and default language and locale', function (done) {

        var arr = [],
            Q;

        LF.Preferred.language = 'vi';
        LF.Preferred.locale = 'VI';

        collection.fetch('STUDY2', {}).then(
            function (result) {
                arr[0] = result;

                return collection.fetch('STUDY2', {namespace: 'DIARY'});
            }
        ).then(
            function (result) {
                arr[1] = result;

                expect(arr[0]).toEqual('Study NoTrans');
                expect(arr[1]).toEqual('Study NoTrans');
                done();
            }
        );
    });

    it('should display the correct string (Diary Pref). Diary namespace and preferred language and locale', function (done) {
        LF.Preferred.language = 'vi';
        LF.Preferred.locale = 'VI';

        collection.fetch('DIARY', {namespace: 'DIARY'}).then(
            function (result) {
                expect(result).toEqual('Diary Pref');
                done();
            }
        );
    });

    it('should display the correct string (Diary NoTrans). Missing Translation. Diary namespace and default language and locale', function (done) {
        LF.Preferred.language = 'vi';
        LF.Preferred.locale = 'VI';

        collection.fetch('DIARY2', {namespace: 'DIARY'}).then(
            function (result) {
                expect(result).toEqual('Diary NoTrans');
                done();
            }
        );
    });

    it('should display the correct string (Diary with same Locale). Diary Locale is same as default Locale', function (done) {

        var arr = [];

        LF.Preferred.language = 'fr';
        LF.Preferred.locale = 'US';

        collection.fetch('DIARY_TEST', {namespace: 'DIARY'}).then(
            function (result) {
                arr[0] = result;
                return collection.fetch('DIARY', {namespace: 'DIARY'});
            }
        ).then(
            function (result) {
                arr[1] = result;

                expect(arr[0]).toEqual('Diary with same Locale');
                expect(arr[1]).toEqual('Diary Default');
                done();
            }
        );
    });

    it('should display the correct string (Diary with same Language). Diary Language is same as default Language', function (done) {

        var arr = [];

        LF.Preferred.language = 'en';
        LF.Preferred.locale = 'UK';

        collection.fetch('DIARY_TEST', {namespace: 'DIARY'}).then(
            function (result) {
                arr[0] = result;
                return collection.fetch('DIARY', {namespace: 'DIARY'});
            }
        ).then(function (result) {
                arr[1] = result;

                expect(arr[0]).toEqual('Diary with same Language');
                expect(arr[1]).toEqual('Diary Default');
                done();
            }
        );
    });

    it('should display the correct string. Unknown locale. Reverting to Default language and locale', function (done) {
        var arr = [];

        LF.Preferred.language = 'es';
        LF.Preferred.locale = 'ES';

        collection.fetch('CORE', {}).then(
            function (result) {
                arr[0] = result;
                return collection.fetch('STUDY', {});
            }
        ).then(
            function (result) {
                arr[1] = result;
                return collection.fetch('CORE', {namespace: 'STUDY'});
            }
        ).then(
            function (result) {
                arr[2] = result;
                return collection.fetch('STUDY', {namespace: 'STUDY'});
            }
        ).then(
            function (result) {
                arr[3] = result;
                return collection.fetch('CORE', {namespace: 'DIARY'});
            }
        ).then(
            function (result) {
                arr[4] = result;
                return collection.fetch('STUDY', {namespace: 'DIARY'});
            }
        ).then(
            function (result) {
                arr[5] = result;
                return collection.fetch('DIARY', {namespace: 'DIARY'});
            }
        ).then(
            function (result) {
                arr[6] = result;

                expect(arr[0]).toEqual('Core Default');
                expect(arr[1]).toEqual('Study Default');
                expect(arr[2]).toEqual('Core Default');
                expect(arr[3]).toEqual('Study Default');
                expect(arr[4]).toEqual('Core Default');
                expect(arr[5]).toEqual('Study Default');
                expect(arr[6]).toEqual('Diary Default');
                done();
            }
        );
    });

    it('should display the correct string. No preferred locale. Reverting to Default language and locale', function (done) {
        var arr = [];

        LF.Preferred.language = null;
        LF.Preferred.locale = null;

        collection.fetch('CORE', {}).then(
            function (result) {
                arr[0] = result;

                return collection.fetch('STUDY', {});
            }
        ).then(
            function (result) {
                arr[1] = result;
                return collection.fetch('CORE', {namespace: 'STUDY'});
            }
        ).then(
            function (result) {
                arr[2] = result;
                return collection.fetch('STUDY', {namespace: 'STUDY'});
            }
        ).then(
            function (result) {
                arr[3] = result;
                return collection.fetch('CORE', {namespace: 'DIARY'});
            }
        ).then(
            function (result) {
                arr[4] = result;
                return collection.fetch('STUDY', {namespace: 'DIARY'});
            }
        ).then(
            function (result) {
                arr[5] = result;
                return collection.fetch('DIARY', {namespace: 'DIARY'});
            }
        ).then(
            function (result) {
                arr[6] = result;

                expect(arr[0]).toEqual('Core Default');
                expect(arr[1]).toEqual('Study Default');
                expect(arr[2]).toEqual('Core Default');
                expect(arr[3]).toEqual('Study Default');
                expect(arr[4]).toEqual('Core Default');
                expect(arr[5]).toEqual('Study Default');
                expect(arr[6]).toEqual('Diary Default');
                done();
            }
        );
    });

    it('it should display the resource key. No string found.', function (done) {

        collection.fetch('INVALID_PASSWORD', {namespace: 'DERP'}).then(
            function (result) {
                expect(result).toEqual('{{ INVALID_PASSWORD }}');
                done();
            }
        );
    });

    it('should not find a Language model.', function () {
        LF.Preferred.language = 'es';
        LF.Preferred.locale = 'US';

        expect(collection.find()).toBeFalsy();
    });

    it('should not find a Language model.', function (done) {
        LF.Preferred.language = 'en';
        LF.Preferred.locale = 'US';

        collection.fetch('TEST', {
            namespace: 'STUDY',
            language: '',
            locale: ''
        }).then(
            function (result) {
                expect(result).toEqual('{{ TEST }}');
                done();
            }
        );
    });

    it('should throw an error. Missing argument key.', function () {

        expect(function () {
            collection.fetch();
        }).toThrow(new Error('Error: Missing argument.'));
    });

    it('should throw an error. Invalid argument type.', function () {

        expect(function () {
            collection.fetch(1);
        }).toThrow(new Error('Error: Invalid argument type.'));
    });

    it('should return a string when string is the argument for display function.', function (done) {
        collection.display('TEST', function (result) {
            expect(typeof(result)).toEqual('string');
            expect(result).toEqual('{{ TEST }}');
            done();
        });
    });

    it('should return an object when object is the argument for display function.', function (done) {
        collection.display({
            key: 'TEST'
        }, function (result) {
            expect(typeof(result)).toEqual('object');
            expect(result.key).toEqual('{{ TEST }}');
            done();
        });
    });

    it('should invoke callback when empty object is the argument for display function.', function (done) {
        var response;

        collection.display({}, function (result) {
            response = 'callback invoked';
            expect(response).toEqual('callback invoked');
            done();
        });
    });

    it('should resolve the string within the object when passed as an object in display function.', function (done) {
        collection.display({
            key1: {
                key: 'TEST',
                namespace: 'SOME_NAMESPACE'
            }
        }, function (result) {
            expect(typeof(result)).toEqual('object');
            expect(typeof(result.key1)).toEqual('string');
            expect(result.key1).toEqual('{{ TEST }}');
            done();
        });
    });

    it('should return an array when object argument has array for display function.', function (done) {
        collection.display({
            key1: ['TEST1', {
                key: 'TEST2',
                namespace: 'SOME_NAMESPACE'
            }]
        }, function (result) {
            expect(typeof(result)).toEqual('object');
            expect(result.key1.length).toBe(2);
            expect(result.key1).toEqual(['{{ TEST1 }}', '{{ TEST2 }}']);
            done();
        });
    });

    it('should return correct string with CORE and DIARY namespace combination.', function (done) {
        collection.display(['CORE', 'DIARY'], function (result) {
            expect(result.length).toBe(2);
            expect(result).toEqual(['Core Default', 'Diary Default']);
            done();
        }, {namespace: 'DIARY'});

    });

    it('should return correct string with STUDY and DIARY namespace combination.', function (done) {
        collection.display({str1: 'STUDY', str2: 'DIARY'}, function (result) {
            expect(_.isObject(result)).toBeTruthy();
            expect(result.str1).toEqual('Study Default');
            expect(result.str2).toEqual('Diary Default');
            done();
        }, {namespace: 'DIARY'});
    });

    it('should return correct string with STUDY and CORE namespace combination.', function (done) {
        LF.Preferred.language = 'vi';
        LF.Preferred.locale = 'VI';

        collection.display({str1: {key: 'CORE', namespace: 'CORE'}, str2: 'STUDY'}, function (result) {
            expect(_.isObject(result)).toBeTruthy();
            expect(result.str1).toEqual('Core Pref');
            expect(result.str2).toEqual('Study Pref');
            done();
        }, {namespace: 'STUDY'});
    });

    it('should contain the correct string (Core Default Date). Default language and locale', function () {

        var results = collection.dates({dates: {}});

        expect(results.CORE_DATE).toEqual('Core Default Date');

    });

    it('should contain the correct string (Core Pref Date).', function () {

        LF.Preferred.language = 'vi';
        LF.Preferred.locale = 'VI';

        var results = collection.dates({dates: {}});

        expect(results.CORE_DATE).toEqual('Core Pref Date');

    });

    it('should contain the correct string (Study Default Date Configs). Default language and locale', function () {

        var results = collection.dates({dateConfigs: {}});

        expect(results.STUDY_DATE_CONFIGS).toEqual('Study Default Date Configs');

    });

    it('should contain the correct string (Study Pref Date Configs).', function () {

        LF.Preferred.language = 'vi';
        LF.Preferred.locale = 'VI';

        var results = collection.dates({dateConfigs: {}});

        expect(results.STUDY_DATE_CONFIGS).toEqual('Study Pref Date Configs');

    });

    it('should contain the correct string (Core Default Date). Reverting to default language and locale', function () {

        LF.Preferred.language = 'es';
        LF.Preferred.locale = 'ES';

        var results = collection.dates({dates: {}});

        expect(results.CORE_DATE).toEqual('Core Default Date');

    });

    it('should contain the correct string (Core Default Date). Reverting to default locale and namespace', function () {

        var results = collection.dates({namespace: 'STUDY', dates: {}});

        expect(results.CORE_DATE).toEqual('Core Default Date');

    });

    it('should contain the correct string (Core Default Date). No namespace. Reverting to default locale and namespace', function () {

        var results = collection.dates({namespace: null, dates: {}});

        expect(results.CORE_DATE).toEqual('Core Default Date');

    });

    it('should get direction of "ltr" when LF.Preferred is English', function () {
        LF.Preferred = {
            language: 'en',
            locale: 'US'
        };

        expect(collection.getLanguageDirection()).toEqual('ltr');
    });

    it('should get direction of "rtl" when LF.Preferred is a RTL language such as Arabic.', function () {
        LF.Preferred = {
            language: 'ar',
            locale: 'EG'
        };

        expect(collection.getLanguageDirection()).toEqual('rtl');
    });

});
