import Templates from 'core/collections/Templates';
import * as helpers from 'test/helpers/SpecHelpers';

describe('Templates', function () {

    var collection,
        prefLanguage =  LF.Preferred.language,
        prefLocale = LF.Preferred.locale;

    _.templateSettings = {
        interpolate : /\{\{(.+?)\}\}/g
    };

    beforeEach(function () {

        collection = new Templates([
            {
                name: 'Label',
                namespace: 'P_Daily_Diary',
                template: '<label for="{{ belongsTo }}">{{ text }}</label>'
            }, {
                name: 'Label',
                namespace: 'P_Daily_Diary',
                language: 'en',
                template: '<label class="en" for="{{ belongsTo }}">{{ text }}</label>'
            }, {
                name: 'Label',
                namespace: 'P_Daily_Diary',
                language: 'en',
                locale: 'US',
                template: '<label class="en-US" for="{{ belongsTo }}">{{ text }}</label>'
            }, {
                name: 'Label',
                namespace: 'P_Daily_Diary',
                language: 'ko',
                locale: 'KR',
                template: '<label class="ko-KR" for="{{ belongsTo }}">{{ text }}</label>'
            }, {
                name: 'Label',
                namespace: 'Welcome',
                template: '<label class="welcome" for="{{ belongsTo }}">{{ text }}</label>'
            }, {
                name: 'Label',
                namespace: 'DEFAULT',
                template: '<label class="default" for="{{ belongsTo }}">{{ text }}</label>'
            }
        ]);
    });

    afterEach(function () {

        LF.Preferred.language = prefLanguage;
        LF.Preferred.locale = prefLocale;
        // LF.logs.reset();

    });

    it('should throw a missing arguments error.', function () {

        expect(function () {
            var match = collection.match();
        }).toThrow(new Error('Missing argument: params is null or undefined'));

    });

    it('should return false.', function () {

        var match = collection.match({
            name        : 'Label',
            namespace   : 'SF36'
        });

        expect(match).toBeFalsy();

    });

    it('should match the first template.' , function () {

        var match = collection.match({
            name        : 'Label',
            namespace   : 'P_Daily_Diary'
        });

        expect(match).toBeDefined();
        expect(match.get('name')).toEqual('Label');
        expect(match.get('namespace')).toEqual('P_Daily_Diary');
        expect(match.get('language')).toBeUndefined();
        expect(match.get('locale')).toBeUndefined();
        expect(match.get('template')).toEqual('<label for="{{ belongsTo }}">{{ text }}</label>');

    });

    it('should match the second template.' , function () {

        var match = collection.match({
            name        : 'Label',
            namespace   : 'P_Daily_Diary',
            language    : 'en'
        });

        expect(match).toBeDefined();
        expect(match.get('name')).toEqual('Label');
        expect(match.get('namespace')).toEqual('P_Daily_Diary');
        expect(match.get('language')).toEqual('en');
        expect(match.get('locale')).toBeUndefined();
        expect(match.get('template')).toEqual('<label class="en" for="{{ belongsTo }}">{{ text }}</label>');

    });

    it('should match the third template.' , function () {

        var match = collection.match({
            name        : 'Label',
            namespace   : 'P_Daily_Diary',
            language    : 'en',
            locale      : 'US'
        });

        expect(match).toBeDefined();
        expect(match.get('name')).toEqual('Label');
        expect(match.get('namespace')).toEqual('P_Daily_Diary');
        expect(match.get('language')).toEqual('en');
        expect(match.get('locale')).toEqual('US');
        expect(match.get('template')).toEqual('<label class="en-US" for="{{ belongsTo }}">{{ text }}</label>');

    });

    it('should match the fourth template.' , function () {

        var match = collection.match({
            name        : 'Label',
            namespace   : 'P_Daily_Diary',
            language    : 'ko',
            locale      : 'KR'
        });

        expect(match).toBeDefined();
        expect(match.get('name')).toEqual('Label');
        expect(match.get('namespace')).toEqual('P_Daily_Diary');
        expect(match.get('language')).toEqual('ko');
        expect(match.get('locale')).toEqual('KR');
        expect(match.get('template')).toEqual('<label class="ko-KR" for="{{ belongsTo }}">{{ text }}</label>');

    });

    it('should throw a missing argument error.', function () {

        expect(function () {
            collection.display();
        }).toThrow(new Error('Missing argument: key is null or undefined'));

    });

    it('should throw a invalid data type error.', function () {

        expect(function () {
            collection.display(1);
        }).toThrow(new Error('Invalid DataType: key.  Expected string, received number.'));

    });

    it('should match the default template.', function () {

        var template = collection.display('Label', {
            belongsTo   : 'password',
            text        : 'Password:'
        });

        expect(template).toEqual('<label class="default" for="password">Password:</label>');

    });

    it('should match and return the third template.', function () {

        LF.Preferred.language = 'en';
        LF.Preferred.locale = 'US';

        var template = collection.display('P_Daily_Diary:Label', {
            belongsTo   : 'password',
            text        : 'Password:'
        });

        expect(template).toEqual('<label class="en-US" for="password">Password:</label>');

    });

    it('should match and return the fourth template.', function () {

        LF.Preferred.language = 'ko';
        LF.Preferred.locale = 'KR';

        var template = collection.display('P_Daily_Diary:Label', {
            belongsTo   : 'password',
            text        : 'Password:'
        });

        expect(template).toEqual('<label class="ko-KR" for="password">Password:</label>');

    });

    it('should match and return the third template.', function () {

        LF.Preferred.language = 'en';
        LF.Preferred.locale = 'US';

        var template = collection.display('P_Daily_Diary:Label', {
            belongsTo   : 'password',
            text        : 'Password:'
        });

        expect(template).toEqual('<label class="en-US" for="password">Password:</label>');

    });

    it('shoudn\'t find a match, and throw an error.', function () {

        expect(function () {

            collection.display('P_Daily_Diary:Input');

        }).toThrow(new Error('No matching template found for "P_Daily_Diary:Input".'));

    });

});
