import Answers from 'core/collections/Answers';
import * as helpers from 'test/helpers/SpecHelpers';

describe('Answers', function () {

    var collection;

    beforeEach(function () {

        collection = new Answers([
            {
                id                  : 1,
                questionnaire_id    : 'EQ5D',
                instance_ordinal    : 1,
                response            : '1'
            }
        ]);

    });

    afterEach(function () {
    });

    it('should have a model', function () {

        expect(collection.model).toBeDefined();

    });

    it('should have a storage property', function () {

        expect(collection.storage).toBeDefined();

    });

    it('should return 1 model', function () {

        var models = collection.match({
                id : 1
            });

        expect(models.length).toEqual(1);

    });

    it('should return 0 models', function () {

        var models = collection.match({
            id : 2
        });

        expect(models.length).toBeFalsy();

    });

    it('should return a missing arguments error (match)', function () {

        expect(function () {
            var models = collection.match();
        }).toThrowError('Invalid number of arguments.');

    });

});
