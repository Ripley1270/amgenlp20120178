import Studies from 'core/collections/Studies';

describe('Studies', function () {

    var collection;

    beforeEach(function () {

        collection = new Studies([
            {
                id : 1
            }
        ]);

    });

    it('should have a model', function () {

        expect(collection.model).toBeDefined();

    });

    it('should have a storage property', function () {

        expect(collection.storage).toBeDefined();

    });

});

