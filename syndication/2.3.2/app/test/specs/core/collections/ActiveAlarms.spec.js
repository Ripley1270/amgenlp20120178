import ActiveAlarms from 'core/collections/ActiveAlarms';
import * as helpers from 'test/helpers/SpecHelpers';
import * as studydesign from 'test/helpers/StudyDesign';
import 'core/utilities';

xdescribe('ActiveAlarms', function () {
    it('should install the database.', function () {
        helpers.installDatabase();
    });

    // This type of error logging/handling is not implemented.  This test will fail.
    xit('should log an error when collection fetch fails in addAlarm method', function (done) {
        var response,
            alarms = new ActiveAlarms();

        alarms.storage = 'fakeStorage';

        spyOn(alarms.logger, 'log');

        alarms.addAlarm({id: 1, date: new Date()}, 'DailySchedule', function () {
            response = true;
            done();
        });
        expect(alarms.logger.log).toHaveBeenCalledWith(Log4js.Level.ERROR, '[object SQLError]');
    });

    // This type of error logging/handling is not implemented.  This test will fail.
    xit('should log an error when collection fetch fails in cancelAlarm method', function (done) {
        var alarms = new ActiveAlarms();

        alarms.storage = 'fakeStorage';

        spyOn(alarms.logger, 'log');

        alarms.cancelAlarm({id: 1}, function () {
            done();
        });

        expect(alarms.logger.log).toHaveBeenCalledWith(Log4js.Level.ERROR, '[object SQLError]');
    });

    // This type of error logging/handling is not implemented.  This test will fail.
    xit('should log an error when clearing collection fails in cancelAllAlarms method', function (done) {
        var alarms = new ActiveAlarms();

        alarms.storage = 'fakeStorage';

        spyOn(alarms, 'cancelAllAlarms');

        spyOn(alarms.logger, 'log');

        alarms.cancelAllAlarms(function () {
            done();
        });

        expect(alarms.logger.log).toHaveBeenCalledWith(Log4js.Level.ERROR, '[object SQLError]');
    });

    it('should add an alarm and 3 reminders', function (done) {
        var alarms = new ActiveAlarms();

        spyOn(LF.Wrapper.Utils, 'addAlarm');

        alarms.addAlarm({
            id: 1,
            date: new Date(),
            reminders: 3,
            reminderInterval: 15
        }, 'DailySchedule', function () {
            alarms.fetch({
                onSuccess: function () {
                    expect(alarms.size()).toEqual(4);
                    expect(alarms.at(0).get('id')).toEqual(1000);
                    expect(alarms.at(1).get('id')).toEqual(1001);
                    expect(alarms.at(2).get('id')).toEqual(1002);
                    expect(alarms.at(3).get('id')).toEqual(1003);
                    expect(LF.Wrapper.Utils.addAlarm.calls.count()).toEqual(4);

                    done();
                }
            });
        });
    });

    it('should not add already existing alarm', function () {
        var alarms = new ActiveAlarms();

        spyOn(LF.Wrapper.Utils, 'addAlarm');

        alarms.addAlarm({id: 1, date: new Date()}, 'DailySchedule', function () {
            alarms.fetch({
                onSuccess: function () {
                    expect(alarms.size()).toEqual(4);
                    expect(LF.Wrapper.Utils.addAlarm).not.toHaveBeenCalled();
                    done();
                }
            });
        });
    });

    it('should throw an error when updating an alarm because alarm id is null or undefined', function () {
        var alarms = new ActiveAlarms();

        expect(function () {
            alarms.updateAlarm({date: new Date()}, 'DailySchedule');
        }).toThrow(new Error('Missing Argument: alarm id is null or undefined.'));
    });

    it('should throw an error when adding an alarm because alarm id is null or undefined', function () {
        var alarms = new ActiveAlarms();

        expect(function () {
            alarms.addAlarm({date: new Date()}, 'DailySchedule');
        }).toThrow(new Error('Missing Argument: alarm id is null or undefined.'));
    });

    it('should throw an error because alarm date is null or undefined', function () {
        var alarms = new ActiveAlarms();

        expect(function () {
            alarms.updateAlarm({id: 1}, 'DailySchedule');
        }).toThrow(new Error('Missing Argument: alarm date is null or undefined.'));
    });

    it('should throw an error because schedule id is null or undefined', function () {
        var alarms = new ActiveAlarms();

        expect(function () {
            alarms.updateAlarm({id: 1, date: new Date()});
        }).toThrow(new Error('Missing Argument: schedule id is null or undefined.'));
    });

    it('should throw an error when canceling an alarm because alarm id is null or undefined', function () {
        var alarms = new ActiveAlarms();

        expect(function () {
            alarms.cancelAlarm({});
        }).toThrow(new Error('Missing Argument: alarm id is null or undefined.'));
    });

    it('should cancel the alarm and reminders', function (done) {
        var alarms = new ActiveAlarms();
        spyOn(LF.Wrapper.Utils, 'cancelAlarm');

        alarms.cancelAlarm({id: 1, reminders: 3, reminderInterval: 15}, function () {
            alarms.fetch({
                onSuccess: function () {
                    expect(alarms.size()).toEqual(0);
                    expect(LF.Wrapper.Utils.cancelAlarm).toHaveBeenCalledWith(1000);
                    expect(LF.Wrapper.Utils.cancelAlarm).toHaveBeenCalledWith(1001);
                    expect(LF.Wrapper.Utils.cancelAlarm).toHaveBeenCalledWith(1002);
                    expect(LF.Wrapper.Utils.cancelAlarm).toHaveBeenCalledWith(1003);
                    done();
                }
            });
        });
    });

    it('should call callback function if there is no alarm to cancel', function (done) {
        var alarms = new ActiveAlarms();

        spyOn(LF.Wrapper.Utils, 'cancelAlarm');

        alarms.cancelAlarm({id: 5}, function () {
            alarms.fetch({
                onSuccess: function () {
                    expect(alarms.size()).toEqual(0);
                    expect(LF.Wrapper.Utils.cancelAlarm).not.toHaveBeenCalledWith({id: 5000});
                    done();
                }
            });
        });
    });

    it('should add an alarm without repeat', function (done) {
        var alarms = new ActiveAlarms();

        spyOn(LF.Wrapper.Utils, 'addAlarm');

        alarms.addAlarm({id: 2, date: new Date()}, 'DailySchedule', function () {
            alarms.fetch({
                onSuccess: function () {
                    expect(alarms.size()).toEqual(1);
                    expect((alarms.where({id: 2000})[0]).get('repeat')).toEqual('');
                    expect(LF.Wrapper.Utils.addAlarm).toHaveBeenCalled();
                    done();
                }
            });
        });
    });

    it('should update the alarm', function (done) {
        var alarms = new ActiveAlarms();

        spyOn(LF.Wrapper.Utils, 'addAlarm');

        alarms.updateAlarm({id: 2, date: new Date(), repeat: 'daily'}, 'DailySchedule', function () {
            alarms.fetch({
                onSuccess: function () {
                    expect(alarms.size()).toEqual(1);
                    expect((alarms.where({id: 2000})[0]).get('repeat')).toEqual('daily');
                    expect(LF.Wrapper.Utils.addAlarm).toHaveBeenCalled();
                    done();
                }
            });
        });
    });

    it('should add an alarm', function (done) {
        var alarms = new ActiveAlarms();

        spyOn(LF.Wrapper.Utils, 'addAlarm');

        alarms.addAlarm({id: 3, date: new Date()}, 'DailySchedule', function () {
            alarms.fetch({
                onSuccess: function () {
                    expect(alarms.size()).toEqual(2);
                    expect(LF.Wrapper.Utils.addAlarm).toHaveBeenCalled();
                    done();
                }
            });
        });
    });

    it('should cancel all the alarms', function (done) {
        var alarms = new ActiveAlarms();

        spyOn(LF.Wrapper.Utils, 'cancelAllAlarms');

        alarms.cancelAllAlarms(function () {
            alarms.fetch({
                onSuccess: function () {
                    expect(alarms.size()).toEqual(0);
                    expect(LF.Wrapper.Utils.cancelAllAlarms).toHaveBeenCalled();
                    done();
                }
            });
        });
    });

    it('should not add one time reminders if the one time reminder number is 0', function (done) {
        var alarms = new ActiveAlarms();

        spyOn(LF.Wrapper.Utils, 'addAlarm');
        spyOn(alarms, 'addOneTimeReminders');

        alarms.addAlarm({id: 1, date: new Date(), time: '13:00', oneTimeReminders: 0}, 'DailySchedule', function () {
            alarms.fetch({
                onSuccess: function () {
                    expect(alarms.size()).toEqual(1);
                    expect(alarms.addOneTimeReminders).not.toHaveBeenCalled();
                    done();
                }
            });
        });
    });

    it('should not add one time reminders if the one time reminder is not defined', function (done) {
        var alarms = new ActiveAlarms();

        spyOn(LF.Wrapper.Utils, 'addAlarm');
        spyOn(alarms, 'addOneTimeReminders');

        alarms.addAlarm({id: 2, date: new Date(), time: '13:00'}, 'DailySchedule', function () {
            alarms.fetch({
                onSuccess: function () {
                    expect(alarms.size()).toEqual(2);
                    expect(alarms.addOneTimeReminders).not.toHaveBeenCalled();
                    done();
                }
            });
        });
    });

    it('should add one time reminders', function () {
        var alarms = new ActiveAlarms();

        spyOn(LF.Wrapper.Utils, 'addAlarm');
        spyOn(alarms, 'addOneTimeReminders').and.callThrough();

        alarms.addAlarm({id: 3, date: new Date(), time: '13:00', oneTimeReminders: 3}, 'DailySchedule', function () {
            alarms.fetch({
                onSuccess: function () {
                    expect(alarms.size()).toEqual(6);
                    expect((alarms.where({id: 3000000})[0]).get('repeat')).toEqual('');
                    expect(alarms.addOneTimeReminders).toHaveBeenCalled();
                    expect(LF.Wrapper.Utils.addAlarm).toHaveBeenCalled();
                    done();
                }
            });
        });
    });

    it('should uninstall the database.', function () {
        LF.SpecHelpers.uninstallDatabase();
    });
});
