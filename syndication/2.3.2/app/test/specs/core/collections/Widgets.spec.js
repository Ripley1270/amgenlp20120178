import Widgets from 'core/collections/Widgets';
import Widget from 'core/models/Widget';

describe('Widgets', function () {

    var collection;

    beforeEach(function () {

        collection = new Widgets();

        collection.add([
            {
                id: 'EQ5D_W_1',
                className: 'EQ5D_W',
                templates: {
                    container: 'DEFAULT:FIELDSET_VERTICAL',
                    input: 'CUSTOM:RADIO_BUTTON',
                    label: 'CUSTOM:LABEL'
                },
                answers: [
                    {
                        text: 'NO_PROBLEMS_WALKING',
                        value: 0
                    },
                    {
                        text: 'SOME_PROBLEMS_WALKING',
                        value: 1
                    },
                    {
                        text: 'UNABLE_WALKING',
                        value: 2
                    }
                ]
            },
            {
                id: 'EQ5D_W_2',
                className: 'EQ5D_W',
                templates: {
                    container: 'DEFAULT:FIELDSET_VERTICAL',
                    input: 'CUSTOM:RADIO_BUTTON',
                    label: 'CUSTOM:LABEL'
                },
                answers: [
                    {
                        text: 'NO_PROBLEMS_WALKING',
                        value: 0
                    },
                    {
                        text: 'SOME_PROBLEMS_WALKING',
                        value: 1
                    },
                    {
                        text: 'UNABLE_WALKING',
                        value: 2
                    }
                ]
            }
        ], {
            silent: false
        });

    });

    afterEach(function () {
        collection = null;
    });

    it('should have a model', function () {

        expect(collection.model).toBeDefined();

    });

    it('should NOT have a table', function () {

        expect(collection.table).not.toBeDefined();

    });

    it('should return 1 model with an id of EQ5D_W_1', function () {

        var model = collection.match({
            id: 'EQ5D_W_1'
        });

        expect(model.length).toEqual(1);
        expect(model[0].get('id')).toEqual('EQ5D_W_1');

    });

    it('should return 2 models with className\'s of \'EQ5D_W\'.', function () {

        var models = collection.match({
            className: 'EQ5D_W'
        });

        expect(models.length).toEqual(2);
        expect(models[0].get('className')).toEqual('EQ5D_W');
        expect(models[1].get('className')).toEqual('EQ5D_W');

    });

    it('should return 0 models.', function () {

        var models = collection.match({
            className: 'EQ5D_W_3'
        });

        expect(models.length).toBeFalsy();

    });

    it('should throw an invalid number of arguments error.', function () {

        expect(function () {
            collection.match();
        }).toThrowError('Invalid number of arguments.');

    });

    // NOTE: schema validation is not supported in the base -- bmj
    xit('should throw an error: Attribute: foo not defined in schema', function () {

        expect(function () {
            collection.at(0).set({
                foo: 0
            }, {validate:true});
        }).toThrowError('Attribute: foo not defined in schema');

    });

});

