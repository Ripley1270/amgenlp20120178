import WebService from 'core/classes/WebService';
import Transmissions from 'core/collections/Transmissions';

import COOL from 'core/COOL';
import Transmit from 'core/transmit';
import * as specHelpers from 'test/helpers/SpecHelpers';

describe('Transmissions', () => {
    let now = Date.now(),
        oldTransmit,
        transmissions;

    Async.beforeEach(() => {
        transmissions = new Transmissions();
        return transmissions.clear()
            .then(() => {
                LF.webService = new WebService();

                oldTransmit = COOL.getService('Transmit');

                Transmit.testAction = function (transmissionItem, callback) {
                    transmissionItem.destroy(transmissionItem.get('id'))
                        .then(callback)
                        .catch(e => fail(e))
                        .done();
                };
                COOL.service('Transmit', Transmit);

                transmissions = new Transmissions([{
                    id          : 1,
                    method      : 'testAction',
                    params      : '1',
                    created     : now
                }, {
                    id          : 2,
                    method      : 'testAction',
                    params      : '2',
                    created     : now
                }, {
                    id          : 3,
                    method      : 'transmitNothing',
                    params      : '3',
                    created     : now
                }]);
            });
    });

    afterEach(() => {
        localStorage.clear();
        LF.webService = undefined;

        COOL.service('Transmit', oldTransmit);
        oldTransmit = null;
    });

    describe('method:save', () => {
        Async.it('should save all the Transmission records in the database.', () => {
            let origCount;
            return transmissions.count()
                .then((res) => {

                    origCount = res;
                    return transmissions.save();
                })
                .then(() => {
                    return transmissions.count();
                })
                .then(res => {
                    expect(res).toBe(3);
                });
        });
    });

    describe('method:execute', () => {
        let col;

        beforeEach(() => col = new Transmissions());

        Async.it('should not execute the transmission.  No transmissions pulled from the queue.', () => {
            expect(() => {
                col.execute(0, $.noop);
            }).toThrow(new Error('Transmission queue is empty.'));
            return Q();
        });

        Async.it('shouldn\'t execute a transmission. Action doesn\'t exist.', () => {
            col.add({
                id          : 3,
                method      : 'transmitNothing',
                params      : '3',
                created     : now
            });

            expect(() => {
                col.execute(0, $.noop);
            }).toThrow(new Error('Nonexistent transmission type.'));
            return Q();
        });

        Async.it('should not execute the transmission.  Non-existent transmission.', () => {
            col.add({
                id          : 3,
                method      : 'transmitNothing',
                params      : '3',
                created     : now
            });

            return col.pullQueue()
                .then(() => {
                    expect(() => {
                        col.execute(3, $.noop);
                    }).toThrow(new Error('Nonexistent transmission record.'));
                });
        });
    });

    describe('method:destroy', () => {
        let col;

        beforeEach(() => col = new Transmissions());

        it('it shouldn\'t delete the transmission. No transmissions pulled from the queue.', () => {
            expect(() => {
                col.destroy(0, $.noop);
            }).toThrow(new Error('Cannot destroy a nonexistent record.'));
        });

        it('should throw an error.  No transmission specified.', () => {
            expect(() => {
                col.destroy();
            }).toThrow(new Error('Missing Argument: id is null or undefined.'));
        });

        Async.it('should remove one transmission from the queue.', () => {
            let origSize;

            return transmissions.save()
                .then(() => {
                    return col.pullQueue();
                })
                .then(() => {
                    expect(col.size()).toEqual(3);
                })
                .then(() => {
                    return col.destroy(3);
                })
                .then(() => {
                    expect(col.size()).toEqual(2);
                });
        });

        it('should throw an error upon attempted removal. Non-existent Transmission.', () => {
            expect(() => {
                col.destroy(3, $.noop);
            }).toThrow(new Error('Cannot destroy a nonexistent record.'));
        });
    });

    describe('method:pullQueue', () => {
        let col;

        beforeEach(() => col = new Transmissions());

        Async.it('should pull all the transmissions from the database(collection).', () => {
            let col = new Transmissions();

            return transmissions.save()
                .then(() => {
                    return col.pullQueue();
                })
                .then(() => {
                    expect(col.size()).toEqual(3);
                });
        });

        Async.it('should pull the queue and have no transmissions.', () => {
            return transmissions.save()
                .then(() => {
                    return col.clear();
                })
                .then(() => col.pullQueue())
                .then(() => {
                    expect(col.size()).toEqual(0);
                });
        });
    });

    describe('method:executeAll', () => {
        it('should execute every transaction.', (done) => {
            let col = new Transmissions([{
                method      : 'testAction',
                params      : '1',
                created     : now
            }, {
                method      : 'testAction',
                params      : '2',
                created     : now
            }]);

            col.save().then(() => {
                let deferred = Q.defer();

                col.executeAll(deferred.resolve);

                return deferred.promise;
            }).then(() => {
                    expect(col.size()).toBe(0);
                })
                .catch(e => fail(e))
                .done(done);
        });
    });

});
