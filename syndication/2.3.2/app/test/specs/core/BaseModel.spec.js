import Base from 'core/models/Base';

class BadTestModel extends Base {
    static get schema () {
        return {
            id: {
                type: String,
                required: true
            },
            badRelationship: {
                name: 'NotUsed',
                relationship: {

                }
            },
            group: {
                name: 'NotNeeded',
                relationship: {
                    to: 'groupCollection',
                    multiplicity: '1..*'
                }
            }
        };
    }
}

class TestModel extends Base {
    static get schema () {
        return {
            group: {
                name: 'NotNeeded',
                relationship: {
                    to: 'groupCollection',
                    multiplicity: '0..*'
                }
            }
        };
    }
}

class TestModel2 extends Base {
    static get schema () {
        return {
            group: {
                name: 'NotNeeded',
                relationship: {
                    to: 'groupCollection',
                    multiplicity: '2..*'
                }
            }
        };
    }
}

class TestModel3 extends Base {
    static get schema () {
        return {
            group: {
                name: 'NotNeeded',
                relationship: {
                    to: 'groupCollection',
                    multiplicity: '1..2'
                }
            }
        };
    }
}

class ValidateTestModel extends Base {
    static get schema () {
        return {
            id: {
                type: String,
                required: true,
                width: 4
            }
        };
    }
}

describe('Base Model', () => {

    describe('method:validatePropDef', () => {
        it('requires a property to have a name', () => {
            expect(() => Base.validatePropDef(BadTestModel.schema.id))
                .toThrow(new Error('Property in schema is missing name'));
        });

        it('required rels to have to property', () => {
            expect(() => Base.validatePropDef(BadTestModel.schema.badRelationship))
                .toThrow(new Error('Property NotUsed: relationship.to not specified'));
        });

        it('requires rel multiplicity to be valid', () => {
            expect(() => Base.validatePropDef(BadTestModel.schema.group))
                .not.toThrow();
        });
    });

    describe('method:validateRelationshipSpec', () => {
        it('sets type to array if upper bound > 1', () => {
            let testModel = new TestModel(),
                result = {
                name: 'NotNeeded',
                relationship: {
                    to: 'groupCollection',
                    multiplicity: '1..*'
                }
            };

            testModel.validateRelationshipSpec('id', result, [], []);

            expect(result.type).toEqual(Array);
        });

        it('sets type to object if embedded and upper bound is 1', () => {
            let testModel = new TestModel(),
                result = {
                name: 'NotNeeded',
                relationship: {
                    embedded: true,
                    to: 'groupCollection',
                    multiplicity: '1..1'
                }
            };

            testModel.validateRelationshipSpec('id', result, {}, []);

            expect(result.type).toEqual(Object);
        });

        it('sets type to string if not embedded and upper bound is 1', () => {
            let testModel = new TestModel(),
                result = {
                name: 'NotNeeded',
                relationship: {
                    to: 'groupCollection',
                    multiplicity: '1..1'
                }
            };

            testModel.validateRelationshipSpec('id', result, '', []);

            expect(result.type).toEqual(String);
        });

    });

    describe('method:displayName', () => {
        it('gives ModelName[ID].propName', () => {
            let testModel = new TestModel();

            testModel.id = '123';

            expect(testModel.displayName('id')).toEqual(`TestModel[${testModel.id}].id`);
        });

        it('uses <#cid> if no id', () => {
            let testModel = new TestModel(),
                cid = testModel.cid;

            expect(testModel.displayName('blank')).toEqual(`TestModel[<#${cid}>].blank`);
        });
    });

    describe('method:validate', () => {

        it('tolerates void value if not required', () => {
            let testModel = new TestModel(),
                data = {
                    group: null
                },
                res = testModel.validate(data);

            expect(res).toEqual(null);
        });

        it('rejects void rel value if required, a special case of lowerBound', () => {
            let testModel = new TestModel2(),
                data = {
                    group: null
                },
                cid = testModel.cid,
                res = testModel.validate(data);

            expect(res)
                .toEqual(new Error(`TestModel2[<#${cid}>].group (rel to groupCollection) is required (2..*), but is null\nAttribute: group must not be null or undefined.`));
        });

        it('enforces multiplicity lower bound', () => {
            let testModel = new TestModel2(),
                data = {
                    group: [{}]
                },
                cid = testModel.cid,
                res = testModel.validate(data);

            expect(res)
                .toEqual(new Error(`TestModel2[<#${cid}>].group too few; minimum 2, actual 1`));
        });

        it('enforces multiplicity upper bound', () => {
            let testModel = new TestModel3(),
                data = {
                    group: [{}, {}, {}]
                },
                cid = testModel.cid,
                res = testModel.validate(data);

            expect(res)
                .toEqual(new Error(`TestModel3[<#${cid}>].group too many; maximum 2, actual 3`));
        });

        it('rejects void values if required', () => {
            let testModel = new ValidateTestModel(),
                cid = testModel.cid,
                data = {
                    id: null
                },
                res = testModel.validate(data);

            expect(res)
                .toEqual(new Error(`Attribute: id must not be null or undefined.`));
        });

        // unimplemented
        xit('checks union types', () => {

        });

        it('enforces max width, if so specified', () => {
            let testModel = new ValidateTestModel(),
                cid = testModel.cid,
                data = {
                    id: '12345'
                },
                res = testModel.validate(data);

            expect(res)
                .toEqual(new Error(`Attribute: id Invalid Length`));
        });

        it('rejects value of wrong type', () => {
            let testModel = new ValidateTestModel(),
                cid = testModel.cid,
                data = {
                    id: 1234
                },
                res = testModel.validate(data);

            expect(res)
                .toEqual(new Error(`Attribute: id. Invalid DataType. Expected string. Received number.`));
        });

        it('returns null if no problems identified', () => {
            let testModel = new ValidateTestModel(),
                cid = testModel.cid,
                data = {
                    id: '1234'
                },
                res = testModel.validate(data);

            expect(res)
                .toEqual(null);
        });

        it('returns multiple violations in a single Error return', () => {
            let testModel = new ValidateTestModel(),
                cid = testModel.cid,
                data = {
                    id: 123456
                },
                res = testModel.validate(data);

            expect(res)
                .toEqual(new Error(`Attribute: id Invalid Length\nAttribute: id. Invalid DataType. Expected string. Received number.`));
        });
    });

    describe('method:getMultiplicityBounds', () => {
        it('handles unspecified as 0..Inf', () => {
            expect(Base.getMultiplicityBounds({}))
                .toEqual([0, Infinity]);
        });

        it('handles * as 0..Inf', () => {
            expect(Base.getMultiplicityBounds({multiplicity: '*'}))
                .toEqual([0, Infinity]);
        });

        it('handles 1..1 as 1..1', () => {
            expect(Base.getMultiplicityBounds({multiplicity: '1..1'}))
                .toEqual([1, 1]);
        });

        it('handles 1 as 1..1', () => {
            expect(Base.getMultiplicityBounds({multiplicity: '1'}))
                .toEqual([1, 1]);
        });

        it('handles numeric 1 as 1..1', () => {
            expect(Base.getMultiplicityBounds({multiplicity: 1}))
                .toEqual([1, 1]);
        });

        it('handles 0..* as 0..Inf', () => {
            expect(Base.getMultiplicityBounds({multiplicity: '0..*'}))
                .toEqual([0, Infinity]);
        });

        it('handles 0..1 as 0..1', () => {
            expect(Base.getMultiplicityBounds({multiplicity: '0..1'}))
                .toEqual([0, 1]);
        });

        it('handles 5..6 as 5..6', () => {
            expect(Base.getMultiplicityBounds({multiplicity: '5..6'}))
                .toEqual([5, 6]);
        });

        it('rejects foo', () => {
            expect(() => Base.getMultiplicityBounds({multiplicity: 'foo'}))
                .toThrow(new Error('Invalid multiplicity spec: foo'));
        });

        it('rejects 1..n', () => {
            expect(() => Base.getMultiplicityBounds({multiplicity: '1..n'}))
                .toThrow(new Error('Invalid multiplicity spec: 1..n'));
        });

        it('rejects 1..0', () => {
            expect(() => Base.getMultiplicityBounds({multiplicity: '1..0'}))
                .toThrow(new Error('Invalid multiplicity spec: 1..0'));
        });

    });
});
