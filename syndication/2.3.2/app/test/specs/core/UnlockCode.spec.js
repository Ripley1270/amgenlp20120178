import UnlockCode from 'core/classes/UnlockCode';

describe('UnlockCode', function () {

    it('should generate the unlock code', function () {

        var unlockCode = UnlockCode.createStartupUnlockCode('0008', 'SitePadAutomation_052215');

        expect(unlockCode).toEqual('1170372055');

    });

});
