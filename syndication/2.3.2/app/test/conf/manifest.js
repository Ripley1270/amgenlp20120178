'use strict';

var optimist = require('optimist'),
    opts = optimist.argv,
    _ = require('underscore');

// The point of the following crazy stuff is:
// convert environment variable '--name=value' to
// equivalent command line argument.
// Why? because WebStorm does not support passing in args
// but does support passing in env vars.
function env2opts() {
    var envArgs = _(process.env).keys() // Get all the env var names
        .filter(function (name) {return  /^--/.test(name); }) // extract out env names that start with --
        .map(function (name) {return name + '=' + process.env[name]; }); // build array of --name=value, argv style
    var envOpts = optimist(envArgs).argv; // use optimist to parse out that fake argv
    _.extend(opts, envOpts); // and combine result with previously parsed CLI opts
}
env2opts();

var manifest = {

    core        : [
        'core/application.js',
        'core/coreSettings.js',
        'core/Errors.js',
        'core/wrapperjs/*.js',
        { pattern: 'media/images/**/*.png', watched: false, included: false, served: true }
    ],

    mini        : ['test/specs/mini/**/*.spec.js'],

    coreSpecs   : [
        'test/specs/core/**/*.spec.js',
        'core/templates/*.ejs',
        'logpad/templates/*.ejs',
        'sitepad/templates/*.ejs',
        'core/less/**/*.less'
    ],

    logpad      : ['test/specs/logpad/**/*.spec.js', 'core/templates/*.ejs', 'logpad/templates/*.ejs', 'trainer/logpad/*.ejs'],

    sitepad     : ['test/specs/sitepad/**/*.spec.js', 'core/templates/*.ejs', 'sitepad/templates/*.ejs'],

    widgets: [
        'test/specs/core/widgets/**/*.spec.js',
        'core/templates/*.ejs',
        'core/less/**/*.less'
    ],

    coreExclude: ['test/specs/core/widgets/**/*.js'],

    exclude     : [
        '**/node_modules/**'
    ],

    thirdparty  : [
        'lib/jQuery/jquery-2.0.3.js',
        'lib/jQuery/jquery-migrate-1.2.1.js',
        'lib/q/q.ERTMOD.js',
        'lib/bootstrap/js/bootstrap.js',
        'lib/md5-min.js',
        'lib/sha-512.js',
        'lib/gibberish-aes-1.0.0.min.js',
        'lib/xregex-min.js',
        'lib/jsonh.js',
        'lib/underscore/underscore.min.js',
        'lib/backbone/backbone.js',
        'lib/backbone/plugins/backbone-route-control.js',
        'lib/momentjs/moment-with-locales.js',
        'lib/select2/js/select2.ERTMOD.js',
        'lib/jSignature.ERTMOD.js',
        'lib/jQueryUI/js/jquery.ui.widget.js',
        'lib/datebox/js/datebox-bootstrap.ERTMOD.js',
        'lib/iscroll-master/build/iscroll.js',
        { pattern: 'lib/select2/css/select2.css', watched: false, included: true, served: true }
    ],

    // Helpers listed here are injected into ALL test specs
    helpers     : [
        'test/helpers/TraceMatrix.js',
        'test/helpers/JasmineWrappers.js',
        'test/helpers/AsyncHelpers.js',
        'test/helpers/CustomMatchers.js',
        'test/helpers/SpyStrategies.js'
    ]

};

if (exports) {
    var specs = opts['spec'] || opts['specs'];

    exports.files = manifest;

    exports.mergeFilesFor = function () {

        var files = [];

        _.forEach(arguments, function (filegroup) {

            // If --spec (or --specs) option is given on command line,
            // a filegroup requested with tilde prefix is satisfied with
            // the --spec value, ignoring what's actually in this manifest
            // Multiple specs may be given, e.g. Trainer,ELF
            // .spec.js suffix is optional
            if (specs && /^~/.test(filegroup)) {
                files = files.concat(specs
                    .split(',')  // allow multiple patterns, comma separated
                    .map(function(spec) {
                        // .spec.js suffix is optional; append it if not given
                        return /\.spec\.js$/.test(spec) ? spec : spec + '.spec.js';
                    })
                    .map(function(spec) {
                        // path prefix test/specs/** is automatic
                        // this allows --spec=Foo or --spec=views/Foo if former matches unintended files
                        return 'test/specs/**/' + spec;
                    })
                );
            } else {
                filegroup = filegroup.replace(/^~/, '');

                manifest[filegroup].forEach(function (file) {

                    // replace @ref; mind that file may actually be obj, not string!
                    var match = _.isString(file) && file.match(/^@(.*)/);

                    if (match) {
                        files = files.concat(manifest[match[1]]);
                    } else {
                        files.push(file);
                    }

                });
            }

        });

        return files;
    };
}
