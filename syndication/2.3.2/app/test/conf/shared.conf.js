'use strict';

// jscs:disable disallowKeywords

var istanbul = require('browserify-istanbul'),
    pathmodify = require('pathmodify'),
    caseVerify = require('dep-case-verify'),
    path = require('path'),
    _ = require('underscore'),
    optimist = require('optimist'),
    opts = optimist.argv;

// The point of the following crazy stuff is:
// convert environment variable '--name=value' to
// equivalent command line argument.
// Why? because WebStorm does not support passing in args
// but does support passing in env vars.
function env2opts() {
    var envArgs = _(process.env).keys() // Get all the env var names
        .filter(function (name) {return  /^--/.test(name); }) // extract out env names that start with --
        .map(function (name) {return name + '=' + process.env[name]; }); // build array of --name=value, argv style
    var envOpts = optimist(envArgs).argv; // use optimist to parse out that fake argv
    _.extend(envOpts); // and combine result with previously parsed CLI opts
}
env2opts();

/*
 * This configuration file contains all common/shared configuration options.
 *
 *  Special flags implemented here:
 *
 *   --debug leave the browser open, for interactive debugging of tests, implies --chrome
 *   --chrome  test with GoogleChrome
 *   --suppress=pass,fail,skip,console  (console represents output captured from browser console)
 *   --reporters=a,b,c  use reporters a, b, and c (e.g. dots,coverage)
 *   --quiet Bare minimum output, basically just test failures
 *              ~= --reporters=spec --suppress=pass,skip,console
 *              also sets logLevel:'WARN'
 *   --no-color turn off ANSI escapes
 *              used for Jenkins; note: attempted auto-detect did't work. ???
 */
module.exports = function (config) {
    var workspace    = path.join(__dirname, '../../..'),
        appDir       = path.join(workspace, 'app'),
        reporters    = ['spec', 'junit', 'coverage'],
        browsers     = ['PhantomJS'],
        suppress     = '',
        slow         = 500,
        logLevel     = 'INFO',
        isJenkins    = !!process.env.BUILD_NUMBER,
        option       = function (name) { return opts[name]; },
        colors       = !isJenkins && option('color') !== false, // treat undefined as true
        singleRun    = !option('debug');

    if (option('show-args')) {
        console.log(JSON.stringify(opts));
    }

    // handle quiet first; specifc other options then take precedence
    if (option('quiet')) {
        reporters = _.difference(reporters, ['coverage']);
        suppress = 'pass,skip,console';
        logLevel = 'WARN';
    }

    suppress = option('suppress') || suppress;
    slow = option('slow') || slow;

    if (option('reporters')) {
        reporters = option('reporters').split(',');
    }

    if (option('chrome') || option('debug')) {
        browsers = ['ChromeDebug'];
    }

    config.set({
        basePath     : appDir, // All other conf files use paths relative to this basePath
        logLevel     : logLevel,
        browsers     : browsers,
        reporters    : reporters,
        colors       : colors,
        singleRun    : singleRun,
        specReporter : {
            suppressSkipped: /skip/.test(suppress) || option('grep'),
            suppressPassed:  /pass/.test(suppress),
            suppressFailed:  /fail/.test(suppress)
        },

        customLaunchers: {

            // Defines a custom Chrome launcher to auto open devtools for the debug tab.
            ChromeDebug: {
                base: 'Chrome',
                flags: ['--auto-open-devtools-for-tabs']
            }
        },

        reportSlowerThan           : Number(slow),
        autoWatch                  : option('debug'),
        // Determines how long Karma waits for a browser to reconnect (in ms).
        browserDisconnectTimeout   : 20000,
        // The number of disconnections tolerated before failure.
        browserDisconnectTolerance : 3,
        // How long Karma will wait for a message from a browser before disconnecting.
        // Increase wait time due to disconnect failures on Windows
        // e.g. [PhantomJS 1.9.8 (Windows 8 0.0.0)]: Disconnected (3 times), because no message in 20000 ms.
        browserNoActivityTimeout   : 60000,
        frameworks                 : ['browserify', 'jasmine'],
        port                       : 9877,

        // Resolves 404 on image HTTP requests.
        proxies :  { '/media/images/': '/base/media/images/' },

        browserify : {
            debug     : true,
            plugin    : [
                caseVerify,
                [pathmodify(), {
                    mods : [
                        pathmodify.mod.dir('core',    path.join(workspace, 'app/core')),
                        pathmodify.mod.dir('sitepad', path.join(workspace, 'app/sitepad')),
                        pathmodify.mod.dir('logpad',  path.join(workspace, 'app/logpad')),
                        pathmodify.mod.dir('trainer', path.join(workspace, 'app/trainer')),
                        pathmodify.mod.dir('test',    path.join(workspace, 'app/test')),
                        pathmodify.mod.dir('trial',   path.join(workspace, 'trial'))
                    ]
                }]
            ],
            transform : option('debug') ? ['babelify'] : [
                'babelify',
                istanbul({
                    ignore : [
                        '**/node_modules/**',
                        '.**/.gitignore',
                        '**/bower_components/**',
                        '**/test/**'
                    ]
                })
            ]
        }
    });

};
