 /**
 * Class Oriented Object Locator
 *
 * This is the Service Locator pattern.
 *
 * Basically, a bit of inversion-of-control.  Allow engineers to create upperlevel classes
 * that likely subclass core classes, and then 'register' those by name.  The system (core)
 * will "new" by that name and get an instance of the registered class without need to know
 * what it is or what it offers beyond to the base implementation it depends on.
 */
import Logger from 'core/Logger';
let logger = new Logger('COOL');

// registry has form:
// { SomeKey: {konstructor: <Function>, singleton: <Boolean>, instance: <Object>|undefined}, ... }
// currently only the konstructor field is used.
let _registry = {};

export default class COOL {

    /**
     * Register a service.
     * @static
     * @param {string} name - The name of the service to register.
     * @param {Object} srv - The service to register.
     * @returns {Object} The registered service.
     * @example
     * COOL.service('Transmit', { ... });
     */
    static service (name, srv) {
        let previous = _registry[name];

        _registry[name] = { name, service: srv, singleton: true };

        if (previous) {
            logger.trace(`Replaced service ${name}.`);
        } else {
            logger.trace(`Registered service ${name}.`);
        }

        return _registry[name];
    }

    /**
     * Fetch a service by name.
     * @static
     * @param {string} name - The name of the service to get.
     * @param {Object} [deflt] - If no service exists, return this service.
     * @returns {Object} The service requested, or the default provided.
     * @example
     * import Transmit from 'core/transmit';
     * let srv = COOL.getService('Transmit', Transmit);
     */
    static getService (name, deflt) {
        let registered = _registry[name];

        if (registered) {
            logger.trace(`Found service ${name}.`);

            return registered.service;
        } else if (deflt) {
            logger.trace(`Service ${name} not registered; returning supplied default.`);

            return deflt;
        } else {
            logger.trace(`Service ${name} not registered and no default was supplied.`);

            return null;
        }
    }

    /**
     * Determines if the provided class is a valid class/constructor function.
     * @static
     * @param {(Object|Function)} konstructor - The class/function to check.
     * @returns {boolean} True if valid, false if not.
     */
    static isValidConstructor (konstructor) {
        if (typeof konstructor === 'object' && _.isEmpty(konstructor)) {
            // eslint-disable-next-line no-console
            console.warn('It looks like we may have this situation: http://stackoverflow.com/a/30390378/1092324');
        }
        return konstructor instanceof Function;
    }

    /*
     * Register a class (by its constructor), under some name.
     * @static
     * @param {string} name - The name of the class to register.
     * @param {(Object|Function)} konstructor - The class or constructor function to register.
     * @returns {(Object|Function)} The registered class.
     * @example
     * class WebService { };
     * COOL.register('WebService', WebService);
     */
    static register (name, konstructor) {
        let previous = _registry[name];
        if (!COOL.isValidConstructor(konstructor)) {
            logger.fatal(`Attempted to register ${name} with something that is not a constructor: ${konstructor}`);
            return previous;
        }
        _registry[name] = {konstructor, singleton: false, instance: undefined};
        if (previous) {
            logger.trace(`Replaced ${name} ${previous.name} with ${konstructor.name}`);
        } else {
            logger.trace(`Registered ${name} as ${konstructor.name}`);
        }
        return previous ? previous.konstructor : undefined;
    }

    /**
     * Alias for COOL.register.
     * @static
     * @param {string} name - The name of the class to register.
     * @param {(Object|Function)} konstructor - The class or constructor function to register.
     * @returns {(Object|Function)} The registered class.
     * @example
     * class WebService { };
     * COOL.add('WebService', WebService);
     */
    static add (name, konstructor) {
        return COOL.register(name, konstructor);
    }

    /**
     * Alias for COOL.new, but provided only a name.
     * @static
     * @param {string} name - The name of the registered class to instantiate.
     * @returns {Object} The instantiated class.
     * @example
     * let webService = COOL.getInstance('WebService');
     */
    static getInstance (name) {
        // TODO evolve to handle singletons etc.
        return COOL.new(name);
    }

    /*
     * Caller can pass in the string-name of a class already added to COOL,
     * and if not defined in cool, the 'default' (a class constructor) will be used instead.
     * @static
     * @param {string} name - The name of the class to instantiate.
     * @param {(Object|Function)} [deflt] - The default class to instantiate if none is registered.
     * @param {Object} [options] - Options passed into the constructor function.
     * @returns {Object} The instantiated class.
     * @example
     * COOL.new('WebService', WebService, { ... });
     */
    static new (name, deflt, options) {
        let registration = _registry[name];
        let konstructor = registration ? registration.konstructor : undefined;
        if (!registration && !deflt) {
            logger.fatal(`Asked to instantiate a ${name} which is not registered; nor was a default given`);
            return undefined;
        } else if (!registration && deflt) {
            if (!COOL.isValidConstructor(deflt)) {
                logger.fatal([
                    `Nothing registered for ${name},`,
                    `and supplied default is not a valid constructor: ${deflt}`
                ].join(' '));
                return undefined;
            }
            konstructor = deflt;
            logger.trace(`Instantiating supplied default class ${deflt.name} for ${name}`);
        } else {
            logger.trace(`Instantiating a ${name} of type ${konstructor.name}`);
        }
        // jscs:disable requireCapitalizedConstructors
        // eslint-disable-next-line new-cap
        return new konstructor(options);
        // jscs:enable requireCapitalizedConstructors
    }

    /*
     * This method does not instantiate the requested service, it only returns its class (constructor).
     * This is outside the Service Locator pattern. In general, only the 'new' method should be used.
     * @static
     * @param {string} name - The name of the registered class to get.
     * @param {(Object|Function)} - A default class/constructor function to provide.
     * @returns {(Object|Function)} - The requested class/constructor function.
     * @example
     * import WebService from 'core/classes/WebService';
     * let WService = COOL.getClass('WebService', WebService);
     * let service = new WebService();
     */
    static getClass (name, deflt) {
        let registration = _registry[name];
        if (registration) {
            logger.trace(`Found ${name} ${registration.konstructor.name}`);
            return registration.konstructor;
        } else if (deflt) {
            if (!COOL.isValidConstructor(deflt)) {
                logger.error(`Name ${name} not registered, and supplied default looks to be invalid: ${deflt}`);
            }
            logger.trace(`Name ${name} not registered; returning supplied default ${deflt.name}`);
            return deflt;
        } else {
            logger.trace(`Name ${name} not registered and no default was supplied.`);
            return undefined;
        }
    }
}
