import StorageBase from './StorageBase';
import Dashboard from '../models/Dashboard';

/**
 * A class that creates a collection of Dashboards.
 * @class Dashboards
 * @extends StorageBase
 * @example let collection = new Dashboards();
 */
export default class Dashboards extends StorageBase {

    /**
     * @property {Dashboard} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link Dashboard}'
     */
    get model () {
        return Dashboard;
    }
}

window.LF.Collection.Dashboards = Dashboards;
