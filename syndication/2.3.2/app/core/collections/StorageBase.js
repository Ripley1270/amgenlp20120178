import BaseCollection from './BaseCollection';
import Logger from 'core/Logger';
import { eCoaDB } from '../dataAccess';

let logger = new Logger('collections/StorageBase', {consoleOnly:true});

/**
 * Base class for all storage-based collections.
 * @class {StorageBase<T>} 
 * @extends {BaseCollection<T>} BaseCollection
 */
export default class StorageBase extends BaseCollection {
    constructor (options) {
        super(options);

        this.storage = this.model.name;

        // handle browsers that don't support constructor names
        if (this.storage === undefined) {
            let funcNameRegex = /function\s([^(]{1,})\(/;
            this.storage = (funcNameRegex).exec(this.model.toString())[1].trim();
        }

        // If this collection doesn't already exist in the data access layer, create it.
        if (!eCoaDB[this.storage]) {
            eCoaDB.collection(this.storage);
        }
    }

    /**
     * The Model that this collection manages
     * @returns {Function} the class (constructor) for the model
     */
    // TODO: this is backward; this.model should call static get model
    static get model () {
        return new this().model;
    }

    /**
     * The name of the storage for this collection
     * @returns {string}
     */
    static get storageName () {
        return this.model.name;
    }

    /**
     * The DABL db that backs this collection
     * @returns {Object}
     */
    static get db () {
        return eCoaDB; // bit hacky. Logs collection overrides this.
    }

    /**
     * The DABL storage object
     * @returns {Object}
     */
    static get storageObject () {
        return this.db[this.storageName];
    }

    /**
     * Drop all records in the storage that backs this collection.
     * Warning! destructive! Dangerous!
     * @returns {Q.Promise<void>}
     */
    static clearStorage () {
        if (!this.storageObject || !this.storageObject.clear) {
            logger.warn(`${this.name}.clearStorage(): no storageObject`);
            return Q();
        }
        
        logger.info(`Clearing ${this.name} storage!`);
        return this.storageObject.clear();
    }

    /**
     * Save the collection to the local database.
     * @returns {Q.Promise}
     */
    save () {
        return Q.all(this.models.map(it => it.save()));
    }
}
