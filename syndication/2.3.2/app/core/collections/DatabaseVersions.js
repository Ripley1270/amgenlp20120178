import StorageBase from './StorageBase';
import DatabaseVersion from '../models/DatabaseVersion';

/**
 * A class that creates a collection of DatabaseVersions.
 * @class DatabaseVersions
 * @augments StorageBase
 * @example let collection = new DatabaseVersions();
 */
export default class DatabaseVersions extends StorageBase {

    /**
     * @property {DatabaseVersion} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link DatabaseVersion}'
     */
    get model () {
        return DatabaseVersion;
    }
}

window.LF.Collection.DatabaseVersions = DatabaseVersions;
