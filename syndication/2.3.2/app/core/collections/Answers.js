import StorageBase from './StorageBase';
import Answer from '../models/Answer';

/**
 * A class that creates a collection of Answers.
 * @class {Answers} 
 * @extends {StorageBase<Answer>}
 * @example let collection = new Answers();
 */
export default class Answers extends StorageBase {
    /**
     * @property {Answer} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link Answer}'
     */
    get model () {
        return Answer;
    }
}

window.LF.Collection.Answers = Answers;
