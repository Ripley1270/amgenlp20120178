
import BaseCollection from './BaseCollection';
import Visit from '../models/Visit';

/**
 * A collection of patient visits.
 * @class Visits
 * @extends BaseCollection
 */
export default class Visits extends BaseCollection {

    /**
     * @property {Visit} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link Visit}'
     */
    get model () {
        return Visit;
    }
}

window.LF.Collection.Visits = Visits;
