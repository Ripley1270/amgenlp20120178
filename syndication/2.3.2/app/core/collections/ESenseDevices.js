import StorageBase from './StorageBase';
import ESenseDevice from '../models/ESenseDevice';

/**
 * A class that creates a collection of ESenseDevices.
 * @class ESenseDevices
 * @extends StorageBase
 * @example let collection = new ESenseDevices();
 */
export default class ESenseDevices extends StorageBase {

    /**
     * @property {ESenseDevice} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link ESenseDevice}'
     */
    get model () {
        return ESenseDevice;
    }
}

window.LF.Collection.ESenseDevices = ESenseDevices;
