import StorageBase from './StorageBase';
import StoredSchedule from '../models/StoredSchedule';

/**
 * A class that creates a collection of Schedules.
 * @class StoredSchedules
 * @extends StorageBase
 * @example let collection = new StoredSchedules();
 */
export default class StoredSchedules extends StorageBase {
    constructor (models, options) {
        super(models);

        if (options) {
            this.options = options;
        }
    }

    /**
     * @property {StoredSchedule} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link StoredSchedule}'
     */
    get model () {
        return StoredSchedule;
    }

    /**
     * Converts all Models in this collection to a Schedule
     * @returns {Array} array of converted Schedules
     */
    toSchedule () {
        return this.map((model) => model.toSchedule());
    }

    /**
     * Return JSON object of this.attributes, calling toJSON of each submodel
     * @returns {Array} of models in the collection after .toJSON is called on each
     */
    toJSON () {
        return this.map((model) => model.toJSON());
    }
}

window.LF.Collection.StoredSchedules = StoredSchedules;
