import BaseCollection from './BaseCollection';
import Screen from '../models/Screen';

/**
 * A class that creates a collection of Screens.
 * @class Screens
 * @extends BaseCollection
 * @example let collection = new Screens();
 */
export default class Screens extends BaseCollection {

    /**
     * @property {Screen} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link Screen}'
     */
    get model () {
        return Screen;
    }
}

window.LF.Collection.Screens = Screens;
