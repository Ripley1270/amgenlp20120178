import BaseCollection from './BaseCollection';
import AnswerOption from '../models/AnswerOption';

/**
 * A class that creates a collection of AnswerOptions.
 * @class AnswerOptions
 * @extends BaseCollection
 * @example let collection = new AnswerOptions();
 */
export default class AnswerOptions extends BaseCollection {

    /**
     * @property {AnswerOption} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link AnswerOption}'
     */
    get model () {
        return AnswerOption;
    }
}

window.LF.Collection.AnswerOptions = AnswerOptions;
