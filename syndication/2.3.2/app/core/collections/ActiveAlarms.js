import StorageBase from './StorageBase';
import ActiveAlarm from '../models/ActiveAlarm';
import Logger from 'core/Logger';
import AlarmManager from 'core/AlarmManager';

const logger = new Logger('ActiveAlarms');

/**
 * A class that creates a collection of ActiveAlarms.
 * @class ActiveAlarms
 * @extends StorageBase
 * @example let collection = new ActiveAlarms();
 */
export default class ActiveAlarms extends StorageBase {

    /**
     * @property {ActiveAlarm} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link ActiveAlarm}'
     */
    get model () {
        return ActiveAlarm;
    }

    /**
     * Add an alarm and associated reminders
     * @param {Object} params Alarm parameters including id, date, title, message and repeat
     * @param {String} scheduleId unique id for schedule
     * @param {Function} [callback] Callback function to be invoked when alarm is added
     */
    addAlarm (params, scheduleId, callback) {
        if (params.id == null) {
            throw new Error('Missing Argument: alarm id is null or undefined.');
        }

        ActiveAlarms.fetchCollection().then((collection) => {
            let id = _.first(AlarmManager.generateIds(params.id, params.reminders)),
                alarmDate = LF.Utilities.convertToUtc(new Date(params.date)),
                thisDate = null,
                alarm = collection.get(id);

            if (alarm) {
                thisDate = LF.Utilities.convertToUtc(new Date(alarm.get('date')));
            }

            // If the alarm is not in the active alarm list or date is changed, set the alarm
            if (!alarm || (thisDate !== alarmDate && params.repeat !== 'once')) {
                if (params.oneTimeReminders) {
                    this.addOneTimeReminders(params, scheduleId, () => {
                        this.updateAlarm(params, scheduleId, callback);
                    });
                } else {
                    this.updateAlarm(params, scheduleId, callback);
                }
            } else {
                (callback || $.noop)();
            }
        }).catch((err) => {
            logger.error(`Error adding alarm: ${scheduleId}`, err);
            (callback || $.noop)();
        }).done();
    }

    /**
     * Add one time reminders for today.
     * @param {Object} alarmParams Alarm parameters including id, date, title, message and repeat
     * @param {String} scheduleId unique id for schedule
     * @param {Function} callback Callback function to be invoked when one time reminders are added
     */
    addOneTimeReminders (alarmParams, scheduleId, callback) {
        let timeDiff,
            reminderParams = JSON.parse(JSON.stringify(alarmParams)),
            reminderDiff = alarmParams.reminders - alarmParams.oneTimeReminders;

        reminderParams.date = new Date(LF.Utilities.parseTime(alarmParams.time, true));
        reminderParams.id = alarmParams.id * 1000;
        reminderParams.reminders = alarmParams.oneTimeReminders - 1;
        reminderParams.reminderInterval = alarmParams.reminderInterval;
        reminderParams.repeat = '';

        timeDiff = reminderParams.date.getTime() + ((reminderDiff + 1) * alarmParams.reminderInterval * 60000);
        reminderParams.date.setTime(timeDiff);
        this.updateAlarm(reminderParams, scheduleId, callback);
    }

    /**
     * Update an existing alarm and associated reminders. If they don't exist, adds them.
     * @param {Object} params Alarm parameters including id, date, title, message, repeat, reminders, and reminderInterval
     * @param {String} scheduleId unique id for the schedule
     * @param {Function} [callback] Callback function to be invoked when alarm is added
     */
    updateAlarm (params, scheduleId, callback = $.noop) {
        let alarmStrings,
            alarm = new ActiveAlarm(),
            nowUTCMillis = new Date().getTime(),
            alarmUTCMillis = new Date(params.date).getTime(),
            stringsToFetch = {
                title: params.title || 'APPLICATION_HEADER',
                message: params.message || 'NEXT_ACTION'
            },
            interval = params.reminderInterval || 0,
            idList = AlarmManager.generateIds(params.id, params.reminders),
            step = (counter) => {
                let id = idList[counter],
                    newDate = new Date(params.date.getTime() + (60000 * counter * interval));

                // TODO: Check that alarmVolumeConfig, from params works
                if (!(params.repeat === 'once' && nowUTCMillis > alarmUTCMillis)) {
                    AlarmManager.addAlarm(id, newDate, alarmStrings.title, alarmStrings.message, params.repeat, scheduleId, params.alarmVolumeConfig);
                }

                alarm.save({
                    id,
                    schedule_id: scheduleId,
                    date: newDate.toString(),
                    repeat: params.repeat ? params.repeat.toString() : '',
                    alarmTrigger: params.onAlarm || ''
                })
                .catch((err) => {
                    logger.error('Error saving alarm', err);
                })
                .then(() => {
                    if (counter < idList.length - 1) {
                        step(counter + 1);
                    } else {
                        callback();
                    }
                });
            };

        if (params.id == null) {
            throw new Error('Missing Argument: alarm id is null or undefined.');
        } else if (params.date == null) {
            throw new Error('Missing Argument: alarm date is null or undefined.');
        } else if (scheduleId == null) {
            throw new Error('Missing Argument: schedule id is null or undefined.');
        }

        LF.getStrings(stringsToFetch)
        .then((strings) => {
            alarmStrings = strings;
            step(0);
        })
        .catch(err => {
            logger.error('Error updating alarms', err);
        })
        .done();
    }

    /**
     * Cancel an alarm and associated reminders
     * @param {Object} params Alarm parameters including id, date, title, message, repeat, reminders, and reminderInterval
     * @returns {Q.Promise<void>}
     */
    cancelAlarm (params) {
        if (params.id == null) {
            throw new Error('Missing Argument: alarm id is null or undefined.');
        }

        return this.fetch()
            .then(() => {
                const idList = [
                    ...AlarmManager.generateIds(params.id, params.reminders),
                    ...AlarmManager.generateIds(params.id * 1000, params.reminders)
                ];

                idList.forEach(id => {
                    const item = this.findWhere({ id });

                    if (item) {
                        AlarmManager.cancelAlarm(id);
                        item.destroy();
                    }
                });
            })
            .catch(err => {
                logger.error('Error canceling alarms', err);
            });
    }

    /**
     * Cancel all alarms
     * @returns {Q.Promise<void>}
     */
    cancelAllAlarms () {
        AlarmManager.cancelAllAlarms();

        return this.clear();
    }

    /**
     * static call to cancel all alarms
     * @returns {Q.Promise<void>}
     */
    static cancelAllAlarms () {
        return new this().cancelAllAlarms();
    }

    /**
     * Static call to Cancel an alarm and associated reminders
     * @param {Object} params Alarm parameters including id, date, title, message, repeat, reminders, and reminderInterval
     * @return {Q.Promise<void>}
     */
    static cancelAlarm (params) {
        return new this().cancelAlarm(params);
    }
}

window.LF.Collection.ActiveAlarms = ActiveAlarms;
