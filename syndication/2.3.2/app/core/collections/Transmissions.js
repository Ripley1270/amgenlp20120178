import QueueBase from './QueueBase';
import Transmission from '../models/Transmission';
import Transmit from 'core/transmit';
import COOL from 'core/COOL';

/**
 * A class that creates a collection of Transmissions.
 * @class Transmissions
 * @extends QueueBase
 * @example let collection = new Transmissions();
 */
export default class Transmissions extends QueueBase {

    /**
     * @property {Transmission} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link Transmission}'
     */
    get model () {
        return Transmission;
    }

    /* eslint-disable consistent-return */
    /**
     * Executes the action.
     * @param {Number} ordinal The ordinal of the action to execute within the collection.
     * @param {Function} onSuccess Invoked when the action is finished executing.
     * @throws {Object} If the transmission is not found to execute.
     * @returns {undefined}
     */
    execute (ordinal, onSuccess = $.noop) {
        let transmitAction,
            transmission = this.at(ordinal);

        if (!this.size()) {
            throw new Error('Transmission queue is empty.');
        } else if (!transmission) {
            throw new Error('Nonexistent transmission record.');
        } else if (transmission.get('status') === 'failed') {
            this.remove(transmission);

            // DE16474 - No return statement was creating duplicate transmissions attempts.
            return onSuccess();
        }

        transmitAction = COOL.getService('Transmit', Transmit)[transmission.get('method')];

        if (transmitAction) {
            transmitAction.call(this, transmission, onSuccess);
        } else {
            throw new Error('Nonexistent transmission type.');
        }
    }
    /* eslint-enable consistent-return */

    /**
     * Counts transmission items in the transmission queue.
     * @returns {Q.Promise<number>}
     */
    getCount () {
        return this.pullQueue(() => _.countBy(this.models, item => item.get('method')));
    }
}

window.LF.Collection.Transmissions = Transmissions;
