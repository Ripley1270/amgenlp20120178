import BaseCollection from 'core/collections/BaseCollection';
import Language from 'core/models/Language';
import dynamicText from 'core/dynamicText';
import ELF from 'core/ELF';
import Logger from 'core/Logger';
import {deepExtend} from 'core/utilities/languageExtensions';

let logger = new Logger('Languages');

/**
 * A class that creates a collection of Languages.
 * @class Languages
 * @extends BaseCollection
 * @example
 * let collection = new Languages([
 *     {
 *         language : 'en',
 *         locale   : 'us',
 *         resource : {
 *             UPCOMING: 'Upcoming',
 *             COMPLETED: 'Completed'
 *         }
 *     }
 * ]);
 */
export default class Languages extends BaseCollection {
    constructor (models, options = {}) {
        super(models, options);
        // Override dynamicText with provided dynamicText class
        this.dynamicText = options.dynamicTextOverrides || dynamicText;
    }

    /**
     * @property {Language} model - The collection's associated model.
     * @readonly
     * @default '{@link Language}'
     */
    get model () {
        return Language;
    }

    /**
     * Finds and returns the appropriate resource string.
     * @param {Object} [options] - Options provided to the method.
     * @param {string} [options.language] - Query by language.
     * @param {string} [options.locale] - Query by locale.
     * @param {string} [options.namespace] - Query by namespace.
     * @return {Object|Boolean} Returns the model else returns false.
     */
    find (options = {}) {
        let model;

        if (!options.language && !options.locale) {
            //TODO: Remove if statement when site user locale and language are implemented
            //Check site content to use the default locale and language for site
            if (!(LF.security && LF.security.checkSiteContent && LF.security.checkSiteContent())) {
                options.language = LF.Preferred.language;
                options.locale = LF.Preferred.locale;
            }
        }

        options.namespace || (options.namespace = 'CORE');

        if (typeof options.language === 'undefined' || (options.language === null && typeof options.locale === 'undefined') || options.locale === null) {
            options.language = LF.StudyDesign.defaultLanguage;
            options.locale = LF.StudyDesign.defaultLocale;
        }

        model = this.match(options);

        return !!model && model[0];
    }

    /**
     * Resolves the Object with string keys and invoke the callback with the resolved strings.
     * @param {String|Object|Array} strings The key or collection of keys of the resource to find.
     * @param {Function} [callback] Invoked when all the keys are resolved.
     * @param {Object} [options] Namespace option for the strings.
     * @returns {Q.Promise}
     */
    display (strings, callback = $.noop, options) {
        let deferred = Q.defer(),
            resolve = (results) => {
                callback(results);
                deferred.resolve(results);
            },
            keys,
            stringsToFetch = _.clone(strings, true),
            stepThroughObject = (counter) => {
                let currentKey,
                stepThroughArray = (index, arrayCallback) => {
                    let arrayItem = currentKey[index];

                    if (index === currentKey.length) {
                        // done iterating through the array
                        arrayCallback();
                    } else if (_.isObject(arrayItem)) {
                        // object with the key and namespace
                        this.fetch(arrayItem.key, {
                            namespace: arrayItem.namespace
                        })
                        .then((model) => {
                            currentKey[index] = model;
                            stepThroughArray(index + 1, arrayCallback);
                        })
                        .catch(e => deferred.reject(e))
                        .done();
                    } else {
                        // arrayItem is single key to resolve
                        this.fetch(arrayItem, _.clone(options))
                        .then((model) => {
                            currentKey[index] = model;
                            stepThroughArray(index + 1, arrayCallback);
                        })
                        .catch(e => deferred.reject(e))
                        .done();
                    }
                };

                if (counter === keys.length) {
                    // done iterating through all keys
                    resolve(stringsToFetch);
                } else {
                    currentKey = stringsToFetch[keys[counter]];

                    if (_.isArray(currentKey)) {
                        // current key is an array
                        stepThroughArray(0, () => {
                            stepThroughObject(counter + 1);
                        });
                    } else if (_.isObject(currentKey)) {
                        // current key is an object with key and namespace
                        this.fetch(currentKey.key, {namespace: currentKey.namespace})
                        .then((model) => {
                            stringsToFetch[keys[counter]] = model;
                            stepThroughObject(counter + 1);
                        })
                        .catch(e => deferred.reject(e))
                        .done();
                    } else {
                        // currentKey is single key to be resolved
                        this.fetch(currentKey, _.clone(options))
                        .then((model) => {
                            stringsToFetch[keys[counter]] = model;
                            stepThroughObject(counter + 1);
                        })
                        .catch(e => deferred.reject(e))
                        .done();
                    }
                }
            };

        if (_.isObject(stringsToFetch)) {
            // the stringsToFetch is an object with multiple keys
            keys = _.map(stringsToFetch, (value, key) => {
                return key;
            });

            if (keys.length) {
                stepThroughObject(0);
            } else {
                resolve(stringsToFetch);
            }
        } else {
            // stringsToFetch is just a single key to be resolved
            this.fetch(stringsToFetch, _.clone(options))
                .then(resolve)
                .catch(e => deferred.reject(e))
                .done();
        }

        return deferred.promise;
    }

    /**
     * Fetch string for given key
     * @param {String} key The key of the resource to find.
     * @param {Object=} options Namespace option for the strings.
     * @param {Function=} callback Invoked when the key is resolved. (DEPRECATED; use returned Promise instead)
     * @throws {Error} If key is not found.
     * @returns {Q.Promise}
     */
    fetch (key, options = {}, callback) {

        if (key == null) {
            throw new Error('Error: Missing argument.');
        }

        if (typeof key !== 'string') {
            throw new Error('Error: Invalid argument type.');
        }

        return Q().then(() => {
            let model, result, namespace, dynamicKeys,
                search = (searchKey, searchParams) => {
                    result = this.find(searchParams);
                    model = !!result && result.get('resources')[searchKey];

                    if (model) {
                        return model;
                    } else if (searchParams.namespace !== 'STUDY' && searchParams.namespace !== 'CORE') {
                        // Fallback to the Study
                        searchParams.namespace = 'STUDY';
                        result = this.find(searchParams);
                        model = !!result && result.get('resources')[searchKey];
                    }

                    if (model) {
                        return model;
                    } else if (searchParams.namespace !== 'CORE') {
                        // Fallback to the Core
                        searchParams.namespace = 'CORE';
                        result = this.find(searchParams);
                        model = !!result && result.get('resources')[searchKey];
                    }

                    return model;
                };

            options.namespace = (options.namespace || 'STUDY');
            namespace = options.namespace;
            options.resources = (options.resources || {});

            model = search(key, options);

            if (!model) {
                // Try falling back to the default language.
                if (
                    options.language !== LF.StudyDesign.defaultLanguage ||
                    options.locale !== LF.StudyDesign.defaultLocale
                ) {
                    options.language = LF.StudyDesign.defaultLanguage;
                    options.locale = LF.StudyDesign.defaultLocale;
                    options.namespace = namespace;
                    model = search(key, options);
                } else {
                    model = false;
                }
            }

            if (model) {
                dynamicKeys = model.match(/\{\{([^}|^{]*)\}\}|\{\{\{([^}|^{]*)\}\}\}/g);

                if (dynamicKeys) {
                    return this.evaluateDynamicText(dynamicKeys, model, options);
                } else {
                    return model;
                }
            } else {
                return `{{ ${key} }}`;
            }
        })
        .tap((result) => {
            if (typeof(callback) === 'function') {
                logger.warn('.fetch() callback is deprecated.  Please use the returned promise instead');
                callback(result);
            }
        });
    }

    /**
     * evaluates all dynamic text placeholders and invoke the callback with the
     * evaluated resource string to display.
     * @param {String} dynamicKeys All dynamic placeholders in a string.
     * @param {String} model The string in which the dynamic text placeholders are being replaced.
     * @param {Object} params Namespace option for the strings.
     * @returns {Q.Promise}
     */
    evaluateDynamicText (dynamicKeys, model, params) {
        let evaluatedTexts = {},
            evaluateString = (key, params) => {
                return this.fetch(key, params);
            };

        // Loop through the dynamicText keys and evaluate them.
        return dynamicKeys.reduce((chain, currentKey) => {
            // eslint-disable-next-line consistent-return
            return chain.then(() => {
                if ((/\{\{\{.*\}\}\}/).test(currentKey)) {
                    let key = currentKey.replace(/\{|\}|\s/gi, '');
                    let dTextFunc = this.dynamicText.list[key];

                    if (dTextFunc) {
                        // Trigger ELF Rule so that Screenshot mode can intercept.
                        return ELF.trigger('DYNAMICTEXT:Trigger', { key,  dynamicText: dTextFunc })
                        .then(res => {
                            // We can override dynamic text using an ELF rule, seen in the Screenshot tool.
                            if (res.text) {
                                return res.text;
                            }

                            if (!res.preventDefault) {
                                return dTextFunc.evaluate();
                            }

                            return '';
                        })
                        .then((response) => {
                            evaluatedTexts[key] = response;
                        });

                    } else {
                        // Provide backwards compatibility with LF.DynamicText functions.
                        if (LF.DynamicText[key]) {
                            logger.warn('LF.DynamicText will not provide screenshots, please update to the newer syntax.');

                            return Q.Promise(resolve => {
                                let promise = LF.DynamicText[key](response => {
                                    evaluatedTexts[key] = response;
                                    resolve();
                                });

                                // Just in case a promise is returned with the old syntax.
                                if (promise && promise.done) {
                                    promise.done();
                                }
                            });
                        }

                        // dynamic text function not found
                        evaluatedTexts[key] = currentKey;
                    }
                } else {
                    // the dynamic text placeholder is key to another string
                    let key = currentKey.replace(/\{|\}|\s/gi, '');

                    return evaluateString(key, params)
                    .then((result) => {
                        if (result) {
                            evaluatedTexts[key] = result;
                        }
                    });
                }
            });
        }, Q())
        .then(() => {
            // done iterating through all dynamic text placeholders
            model = model.replace(/\{{3}/gi, '{{').replace(/\}{3}/gi, '}}');
            model = _.template(model)(evaluatedTexts);
            return model;
        });
    }

    /**
     * Returns the dates resources for the current language
     * @param {Object} [options] Namespace option for the strings.
     * @return {Object|Boolean} Returns the date resources for the current language else returns false.
     */
    dates (options = {}) {
        let results, name, namespace, returnedDates,
            search = (searchName, searchParams) => {
                results = this.find(searchParams);
                returnedDates = !!results && results.get(searchName);
                if (returnedDates) {
                    return returnedDates;
                } else if (searchParams.namespace !== 'STUDY' && searchParams.namespace !== 'CORE') {
                    // Fallback to the Study
                    searchParams.namespace = 'STUDY';
                    results = this.find(searchParams);
                    returnedDates = !!results && results.get(searchName);
                }

                if (returnedDates) {
                    return returnedDates;
                } else if (searchParams.namespace !== 'CORE') {
                    // Fallback to the Core
                    searchParams.namespace = 'CORE';
                    results = this.find(searchParams);
                    returnedDates = !!results && results.get(searchName);
                }

                return returnedDates;
            };

        options.namespace = (options.namespace || 'STUDY');
        namespace = options.namespace;

        if (options.dates) {
            name = 'dates';
        } else if (options.dateConfigs) {
            name = 'dateConfigs';
        } else {
            throw new Error('Error: Unsupported date resource type.');
        }

        returnedDates = search(name, options);

        if (!returnedDates) {
            if (options.language !== LF.StudyDesign.defaultLanguage && options.locale !== LF.StudyDesign.defaultLocale) {
                options.language = LF.StudyDesign.defaultLanguage;
                options.locale = LF.StudyDesign.defaultLocale;
                options.namespace = namespace;
                returnedDates = search(name, options);
            } else {
                returnedDates = false;
            }
        }

        return returnedDates;
    }

    /**
     * Finds the direction of the LF.Preferred language/locale, if that is not found it falls back to LF.StudyDesign.default...
     * @param {Object} [langloc] language and locale to search for. Searches for LF.Preferred if either language or locale is undefined
     * @param {String} [langloc.language] language to search for
     * @param {String} [langloc.locale] locale to search for
     * @returns {String} If found, returns a string of the direction ('ltr' or 'rtl'), otherwise returns 'ltr'.
     */
    getLanguageDirection (langloc) {
        let resource;

        //  If language or locale is not defined in the object, undefined param will search for LF.Preferred
        if (!langloc || !langloc.language || !langloc.locale) {
            langloc = undefined;
        }

        resource = this.find(langloc) || this.find({
                language: LF.StudyDesign.defaultLanguage,
                locale: LF.StudyDesign.defaultLocale
            });

        return resource ? resource.get('direction') : 'ltr';
    }

    /**
     * Returns a list of available languages.
     * @returns {Array<string>}
     */
    getLanguages () {
        return this.chain()

            // Pick only the language, local, and formatted name for each resource.
            .map((res) => res.pick('language', 'locale', 'localized'))

            // We only require languages with a localized property.
            .filter((res) => res.localized != null)

            // Reduce to unique languages/locale resources.
            .uniq((res) => res.language + res.locale)

            .value()

            .sort((a, b) => {
                let sortStringA = `${a.language} ${a.locale}`,
                    sortStringB = `${b.language} ${b.locale}`;
                return +(sortStringA > sortStringB) || +(sortStringA === sortStringB) - 1;
            });
    }

    /**
     * Adds or extends a model in the collection.
     * This will either add a new model, or update an existing model by doing _.extend()
     * @param {object} content content of addition.
     * @returns {*} add or save operation.
     */
    addLanguageResources (content) {
        let namespace = content.namespace || '',
            language = content.language || '',
            locale = content.locale || '',
            toInsert = _.extend({}, content, {
                id: `${language} ${locale} ${namespace}`
            }),
            oldVal = this.get(toInsert.id);
        if (oldVal) {
            return oldVal.set(deepExtend(oldVal.attributes, toInsert));
        } else {
            return this.add(toInsert);
        }
    }
}

// @todo phase out this namespace...
window.LF.Collection.Languages = Languages;

/**
 * Collections of languages object.
 * @memberof LF
 */
window.LF.strings = new Languages();

/**
 * Shortcut to the display function.
 */
window.LF.getStrings = _(LF.strings.display).bind(LF.strings);
