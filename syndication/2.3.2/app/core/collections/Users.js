import StorageBase from './StorageBase';
import User from 'core/models/User';
import Subjects from 'core/collections/Subjects';
import { isSitePad } from 'core/utilities';
import * as lStorage from 'core/lStorage';
import { syncUsers as syncUsersAction } from 'core/actions/syncUsers';
import Logger from 'core/Logger';

const logger = new Logger('Users');
/**
 * @class Users
 * @extends {StorageBase<User>}
 */
export default class Users extends StorageBase {

    /**
     * @property {User} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link User}'
     */
    get model () {
        return User;
    }

    /**
     * Update the users by creating users for each subject and creating an admin user.
     * @returns {Q.Promise}
     */
    static updateUsers () {
        return this.fetchCollection()
            .tap(users => users.updateAllSubjectUsers())
            .tap(users => users.syncAdminPassword())
            .tap(users => users.updateAdminUser())
            .tap(users => users.syncUsers());
    }

    /**
     * Create a user from each Subject.
     * @param {Object} [userConfig] Configuration to pass to a newly created user.
     * @returns {Q.Promise}
     */
    updateAllSubjectUsers (userConfig = {}) {
        const update = (subject) => this.updateSubjectUser(subject, userConfig);

        return Q()
            .then(() => Subjects.fetchCollection())
            .then(subjects => {
                return Q.all(subjects.map(update));
            })
            .catch(err => logger.error('Error updating subject Users', err));
    }

    /**
     * Update subject user. If subject user does not exist, create subject user.
     * @param {Subject} subject - The subject to create user form.
     * @param {Object} [userConfig] - optional params to pass to the new User.
     * @return {Q.Promise}
     */
    updateSubjectUser (subject, userConfig = {}) {
        const subjectUser = this.findWhere({
            id: subject.get('user')
        });

        return Q()
        .then(() => {
            if (!subjectUser) {
                return this.model.createUserFromSubject(subject, userConfig);
            } else {
                return subjectUser;
            }
        })
        .tap(user => {
            const subjectRole = user.getRole(),
                hasServicePassword = !!subject.get('service_password');

            return user.save({
                userType: 'Subject',
                username: isSitePad() ? subject.get('subject_id') : subjectRole.get('displayName'),
                language: subject.get('language') || `${LF.Preferred.language}-${LF.Preferred.locale}`,
                password: (hasServicePassword && subject.get('subject_password')) || user.get('password'),
                secretQuestion: (hasServicePassword && subject.get('secret_question'))|| user.get('secretQuestion'),
                secretAnswer: (hasServicePassword && subject.get('secret_answer')) || user.get('secretAnswer'),
                salt: (hasServicePassword && subject.get('krpt')) || user.get('salt'),
                role: subjectRole.get('id'),
                permanentPasswordCreated: hasServicePassword || user.get('permanentPasswordCreated'),
                active: 1
            });
        })
        .tap(user => subject.save({
            user: user.get('id')
        }))
        .catch(err => {
            logger.error(`Error creating user for subject: ${subj.get('username')}`, err);
        });
    }

    /**
     * Update admin user. If admin user does not exist, create admin user.
     *   Promise is rejected if the user is not saved.
     * @returns {Q.Promise}
     */
    updateAdminUser () {
        return Q().then(() => {
            let adminRole,
                userType = 'Admin',
                adminUser = this.findWhere({userType}),
                admin;

            if (!LF.StudyDesign.adminUser) {
                logger.operational('No admin user configuration found!');

                return adminUser && adminUser.deactivate();
            } else {
                try {
                    admin = JSON.parse(lStorage.getItem('admin'));
                } catch (e) {
                    logger.warn('updateAdminUser: No admin item');
                }

                adminRole = LF.StudyDesign.roles.findWhere({id: LF.StudyDesign.adminUser.role});
                if (!adminRole || adminRole.get('id') === 'subject') {
                    logger.error('Admin role configuration is not valid!');

                    return adminUser && adminUser.deactivate();
                }
            }

            if (!admin) {
                logger.info('No admin password synced from server');
                // eslint-disable-next-line consistent-return
                return;
            }

            if (!adminUser) {
                adminUser = new User();
            }

            return adminUser.save({
                userType,
                username: adminRole.get('displayName'),
                language: `${LF.StudyDesign.defaultLanguage}-${LF.StudyDesign.defaultLocale}`,
                password: admin.hash.substring(2),
                salt: admin.key,
                role: adminRole.get('id'),
                active: 1
            }).then(() => {
                lStorage.removeItem('admin');
                return adminUser;
            });
        }).catch((err) => {
            logger.error('Error saving admin user', err);
        });
    }

    /**
     * Update all users. If the user's role no longer exists they are marked as inactive.
     * @returns {Q.Promise}
     */
    updateUserStatus () {
        return Q().then(() => {
            let roles = LF.StudyDesign.roles.pluck('id');

            return Q.all(this.map(user => {
                if (!(_.contains(roles, user.get('role'))) || user.get('active') === 0) {
                    return user.deactivate();
                } else {
                    return user.checkActive();
                }
            }));
        }).catch(err => {
            err && logger.error('Error updating user status', err);
        });
    }

    /**
     * Static function to provide a shortut to calling syncUsers
     * @return {Q.Promise} promise
     */
    static syncUsers () {
        return new this().syncUsers();
    }

    /**
     * Synchronize the Users table with SW
     * @returns {Q.Promise}
     */
    syncUsers () {
        return Q.all(this.map(user => user.checkActive()))
            .then(() => syncUsersAction())
            .catch(err => {
                logger.error('Error syncing users', err);
            });
    }

    /**
     * Syncs admin password if needed.
     * @returns {Q.Promise}
     */
    syncAdminPassword () {
        let syncAdminPassword = () => {
            return LF.webService.getAdminPassword()
                .then(res => {
                    lStorage.setItem('admin', JSON.stringify(res));
                });
        };

        if (!lStorage.getItem('site')) {
            return LF.webService.getkrDom()
                .then(res => {
                    lStorage.setItem('site', JSON.stringify(res));
                })
                .then(syncAdminPassword());
        } else {
            return syncAdminPassword();
        }
    }
}

window.LF.Collection.Users = Users;
