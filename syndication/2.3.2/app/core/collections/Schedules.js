import BaseCollection from './BaseCollection';
import Schedule from '../models/Schedule';

/**
 * A class that creates a collection of Schedules.
 * @class Schedules
 * @extends BaseCollection
 * @example let collection = new Schedules();
 */
export default class Schedules extends BaseCollection {

    /**
     * @property {Schedule} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link Schedule}'
     */
    get model () {
        return Schedule;
    }

    /**
     * @property {Object} schema - Defines model attributes, and used in validation.
     * @static
     * @readonly
     */
    static get schema () {
        return {
            id: {
                type: String,
                required: true
            },

            target: {
                type: Object,
                required: true
            },

            scheduleFunction: {
                type: String,
                required: true
            },

            scheduleParams: {
                type: Object,
                required: false
            },

            scheduleRoles: {
                relationship: {
                    to: 'Roles',
                    multiplicity: '0..*'
                }
            },

            alarmFunction: {
                type: String,
                required: false
            },

            alarmParams: {
                type: Object,
                required: false
            }
        };
    }
}

window.LF.Collection.Schedules = Schedules;
