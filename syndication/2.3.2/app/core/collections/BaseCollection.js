import Logger from 'core/Logger';

let logger = new Logger('BaseCollection', {consoleOnly: true});

/**
 * A class that creates a Base collection.
 * BaseCollection constructor function.
 * @extends {Backbone.Collection<T>}
 * @class {BaseCollection<T>} 
 */
export default class BaseCollection extends Backbone.Collection {

    //noinspection JSDuplicatedDeclaration,JSCheckFunctionSignatures
    /**
     * Fetch a collection
     * @param {Backbone.CollectionFetchOptions|Object=} options Fetch options (see backbone)
     * but also supports our custom where clauses.
     * @returns {Q.Promise}
     */
    fetch (options = undefined) {
        //noinspection JSValidateTypes
        return super.fetch(options);
    }

    /**
     * Creates a new instance of the collection and fetches from the database.
     * @return {Q.Promise<BaseCollection<T>>} returns a promise passing the fetched collection.
     */
    static fetchCollection () {
        let collection = new this();

        // The superfluous Q wrapper here squelches a WebStorm static check warning
        return Q(collection.fetch()).then(() => collection);
    }

    /**
     * Fetches an instance of this collection from the database, and returns the first entry.
     * @return {Q.Promise<BaseModel<T>>} returns a promise passing the fetched collection's first entry.
     */
    static fetchFirstEntry () {
        return this.fetchCollection()
        .then(collection => collection.at(0));
    }

    /**
     * Matches and returns models based on passed attributes.
     * @param {Object} attributes A hash-map of attributes to match.
     * @returns {Backbone.Model[] | boolean} An array of matched models.
     * @throws {Error} If attributes object is not defined.
     * @example
     * var matched = dashboards.match({
     *   questionnaire_id : 'EQ5D',
     *   instance_ordinal : 5
     * });
     */
    // Return all matching models. If 0, return false.'
    match (attributes) {
        let models;

        if (attributes == null) {
            throw new Error('Invalid number of arguments.');
        } else if (attributes.id) {
            logger.warn('Use Collection.get(id) instead of .match({id:id})', {at: new Error().stack});
        }

        // For each model in the collection.
        models = this.filter((model) => {
            let matchCount = 0,
                length = _.size(attributes);

            _(attributes).every((value, key) => {
                // If the field matches the value increase the match counter.
                if ((model.get(key) === value) || ((_.isObject(value)) && model.has(key))) {
                    matchCount += 1;
                    return true;
                } else {
                    return false;
                }
            });

            // If the match counter and total attributes length match, return the model.
            return matchCount === length;
        });

        if (models.length > 0) {
            return models;
        } else {
            return false;
        }
    }

    /**
     * Removes the entire collection from the database.
     * @param {Object} [options] Options for deleting the collection.
     * @returns {Q.Promise<void>}
     */
    destroy (options) {
        return Backbone.sync('delete', this, options);
    }

    /**
     * Clears out the entire storage object not just those in the collection.
     * @param {Object} [options] Options for clearing the collection's storage.
     * @returns {Q.Promise<void>}
     */
    clear (options) {
        return Backbone.sync('clear', this, options);
    }

    /**
     * Returns the count of the storage object.
     * @param {Object} [options] Options for returning the count.
     * @returns {Q.Promise<void>}
     */
    count (options) {
        return Backbone.sync('count', this, options);
    }

    /**
     * Formats as: 'Thingies collection containing 2 items'
     * @returns {string}
     */
    toString () {
        return `${this.constructor.name} collection containing ${this.size()} items`;
    }
}

window.LF.Collection.BaseCollection = BaseCollection;
