import StorageBase from './StorageBase';

/**
 * A class that creates a collection of QueueBase.
 * @class QueueBase
 * @extends StorageBase
 * @example var collection = new QueueBase();
 */
export default class QueueBase extends StorageBase {

    /**
     * Pull the transmission queue from the database.
     * @param {Function} onSuccess Callback function invoked upon a successful transaction.
     * @returns {Q.Promise}
     */
    pullQueue (onSuccess = $.noop) {
        return this.fetch().then(onSuccess);
    }

    /**
     * Executes all action items until none are left.
     * @param {Function} onSuccess A callback function invoked upon success.
     */
    executeAll (onSuccess) {
        let step = () => {
            this.execute(0, () => {
                // If there any transmissions in the queue...
                if (this.size()) {
                    step();
                } else {
                    onSuccess();
                }
            });
        };

        if (this.size()) {
            step();
        } else  {
            onSuccess();
        }
    }

    /**
     * Remove an item from the queue.
     * @param {String} id The id of the item to remove.
     * @param {Function} onSuccess Callback function invoked upon completion of the transaction.
     * @param {Function} [onError] A callback invoked upon errant action.
     * @throws {Object} If id or queue items are not found.
     * @returns {Q.Promise}
     */
    destroy (id, onSuccess = $.noop, onError = $.noop) {
        let item,
            deferred = Q.defer(),
            resolve = (res) => {
                onSuccess(res);
                deferred.resolve(res);
            },
            reject = (err) => {
                onError(err);
                deferred.reject(err);
            };

        // If no id has been specified.
        if (id == null) {
            throw new Error('Missing Argument: id is null or undefined.');
        }

        // Find the record in memory.
        item = this.get(parseInt(id, 10));

        if (item) {
            // Attempt to destroy the queue item.
            item.destroy().then(resolve, reject);
        } else {
            throw new Error('Cannot destroy a nonexistent record.');
        }

        return deferred.promise;
    }
}

window.LF.Collection.QueueBase = QueueBase;
