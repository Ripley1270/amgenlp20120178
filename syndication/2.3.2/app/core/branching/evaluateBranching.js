/**
 * @file
 * @author <a href="mailto:snovakovic@phtcorp.com">Stefan Novakovic</a>
 * @version 1.5.1
 */

/**
 * Loop through each branchObj form branchArray and call the PDE's or core branch function from configuration to
 * evaluate it.  Before calling the branch function extend branchObject with currentScreen ID and answer collection
 * @param {Branch[]} branchArray - array of branchObj properties for the current questionnaire.
 * @param {String} screenId - Id of the current screen.
 * @param {Answers} answers - collection of the user responses
 * @param {object} [view] The interface which triggered the event.
 * @returns {Q.Promise<Branch | boolean>} branchObj - first branchObject from branchArray that evaluates to true
 *                               false - none of the branchObjects from branchArray evaluate to true
 **/
export function evaluateBranching (branchArray, screenId, answers, view) {
    return Q.Promise((resolve) => {
        const length = typeof branchArray !== 'undefined' ? branchArray.length : false;
        let step, nextStep;

        step = function (counter) {
            const branchObj = branchArray[counter];

            _.extend(branchObj, {
                currentScreen : screenId,
                answers : answers
            });
            if (typeof branchObj.branchTo === 'undefined' || typeof branchObj.branchFrom === 'undefined' ||
                typeof branchObj.branchFunction === 'undefined') {
                nextStep(counter);
            } else if (screenId === branchObj.branchFrom) {
                if (LF.Branching.branchFunctions[branchObj.branchFunction]) {
                    // If branch function exist call it
                    LF.Branching.branchFunctions[branchObj.branchFunction](branchObj, view)
                        .then((response) => {
                            if (response) {
                                if(response.clearBranchedResponses !== undefined && response.branchTo) {
                                    resolve(response);
                                } else {
                                    //branch object evaluate to true, return it
                                    resolve(branchObj);
                                }
                            } else {
                                nextStep(counter);
                            }
                        });
                } else {
                    nextStep(counter);
                }
            } else {
                nextStep(counter);
            }
        };

        nextStep = function (counter) {
            if (counter + 1 < length) {
                step(counter + 1);
            } else {
                resolve(false);
            }
        };

        if (length) {
            step(0);
        } else {
            resolve(false);
        }
    });
}

LF.Branching.evaluateBranching =  evaluateBranching;

export default LF.Branching.evaluateBranching;