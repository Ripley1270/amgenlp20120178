/**
 * @file
 * @author <a href="mailto:drakocevic@phtcorp.com">Davor Rakocevic</a>
 * @version 1.6
 */

/**
 * Evaluate if user response is the same as value from configuration
 * @param {Object} branchObj - extended branch object from the configuration
 * @param {Object} [view] The interface which invoked this method.
 * @return {Q.Promise<Boolean>} true - if passed in phase is the current one
 *                    false - if passed in phase is not the current one
 */
LF.Branching.branchFunctions.isCurrentPhase = function (branchObj, view) {
    return Q.Promise((resolve) => {
        let configPhase = LF.StudyDesign.studyPhase[branchObj.branchParams.value],
            phase = view.subject.get('phase');

        if (configPhase === phase) {
            resolve(true);
        } else {
            resolve(false);
        }
    });
};

export default LF.Branching.branchFunctions.isCurrentPhase;