/**
 * Holds all branch helper functions
 * @namespace
 */
LF.Branching.Helpers =  { };

/**
 * Get the user response for the selected question from the answer collection
 * @param {Collection} answers - collection of the user responses
 * @param {String} questionId - Id of the question for which we want to get the user response
 * @returns {Undefined | String | Array} undefined - there is no user response for the questionId
 *                                       string - user response is one value
 *                                       array - user response is an array
 */
export function responseForQuestionId (answers, questionId) {
    let questionAnswers, responseArray;

    questionAnswers = answers.filter(function (answerItem) {
        return answerItem.get('question_id') === questionId;
    });

    responseArray = _(questionAnswers).map(function (ansObj) {
        return ansObj.get('response');
    });

    if (responseArray.length === 0) {
        return undefined;
    } else if (responseArray.length === 1) {
        return responseArray[0];
    } else {
        return responseArray;
    }
}

LF.Branching.Helpers.responseForQuestionId = responseForQuestionId;

export default LF.Branching.Helpers;