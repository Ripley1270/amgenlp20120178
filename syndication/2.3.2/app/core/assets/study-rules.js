import RestartManager from 'core/classes/RestartManager';
import StudyDBUpgrade from './db-upgrade';

// Add rules, actions, and expressions here...

export default (rules) => {

    // Remove rules from core, so we can re-define
    rules.remove('CheckStudyDatabaseUpgrade');
    rules.remove('SkipQuestions');
    rules.remove('AutoDial');

    /*
     * Checks study database upgrades
     */
    rules.add({
        id: 'CheckStudyDatabaseUpgrade',
        trigger: 'INSTALL:ModelsInstalled',
        evaluate (filter, resume) {
            resume(StudyDBUpgrade && !_.isEmpty(StudyDBUpgrade));
        },
        resolve: [{
            action: 'displayMessage',
            data: 'PLEASE_WAIT'
        }, {
            action: 'databaseUpgradeCheck',
            data: {
                config: StudyDBUpgrade,
                versionName: 'DB_Study'
            }
        }, {
            action: 'removeMessage'
        }]
    });

    /*
     * Example: Skipped question confirmation
     * Displays a confirmation when attempting to skip a question
     */
    rules.add({
        id: 'SkipQuestions',
        trigger: 'QUESTIONNAIRE:SkipQuestions',
        evaluate: { expression: 'confirm', input: { key: 'SKIP_QUESTION_CONFIRM' } },
        resolve: [{
            action () {
                return this.skip()
                .then(() => this.next());
            }
        }, {
            action: 'preventDefault'
        }],
        reject: [{
            action: 'preventDefault'
        }]
    });

    /*
     * Example: AutoDial
     * Transmits Data to server when alarm with ScheduleId of AutoDial goes off
     */
    rules.add({
        id: 'AutoDial',
        trigger: 'ALARM:Trigger/AutoDial',
        evaluate: ['AND', 'isOnline', 'isStudyOnline'],
        resolve: [{
            action: 'displayMessage',
            data: 'PLEASE_WAIT'
        }, {
            action: () => RestartManager.setTransmissionActive(true)
        }, {
            action: 'transmitAll'
        }, {
            action: 'subjectSync'
        }, {
            action: 'syncHistoricalData',
            data: {
                collection: 'LastDiaries',
                webServiceFunction: 'syncLastDiary',
                activation: false
            }
        }, {
            action: 'siteAccessCodeSync'
        }, {
            action: 'transmitLogs'
        }, {
            action: 'checkViewForRefresh'
        }, {
            action: 'terminationCheck'
        }, {
            action: 'recoverOrphanDiaries'
        }, {
            action: 'removeMessage'
        }, {
            action: 'silenceAlarm'
        }, {
            action: () => RestartManager.setTransmissionActive(false)
        }],
        reject: [{
            action: 'silenceAlarm'
        }]
    });
};