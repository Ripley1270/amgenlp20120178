// CORE FILES
import studyDesign from './study-design';
import dbUpgrade from './db-upgrade';
import loggingConfig from './logging-config';
import studyRules from './study-rules';
import StudyController from './study-controller';
import studyRoutes from './study-routes';
import studyWebService from './study-web-service';
// END CORE FILES

export default {studyDesign, dbUpgrade, loggingConfig, studyRules,
    StudyController, studyRoutes, studyWebService};
