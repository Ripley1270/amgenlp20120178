import WebService from 'core/classes/WebService';
import COOL from 'core/COOL';

export default class StudyWebService extends COOL.getClass('WebService', WebService) {
    constructor () {
    }

// An example REST web service method
/**
 * Gets hello world from server by sending the stored procedure name and the required parameters
 * @param params Includes the authentication token
 * @param {Function} onSuccess A callback function invoked upon successful completion.
 * @param {Function} onError A callback function invoked upon failure.
 */
/* syncHelloWorld (params, onSuccess, onError) {
    let ajaxConfig = {
            type    : 'POST',
            uri     : '../../api/v1/SWAPI/GetDataClin',
            auth    : params ? params.auth : null
        },
        data = {
            StoredProc    : 'HelloWorld',
            Params        : ['Hello', 'World']
        };

    return this.transmit(ajaxConfig, data, onSuccess, onError);
};  */

}

COOL.add('WebService', StudyWebService);
