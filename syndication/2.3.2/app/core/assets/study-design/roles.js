/**
 * @fileOverview This is an example roles study configuration file.
 * @author <a href='mailto:bill.calderwood@ert.com'>William A. Calderwood</a>
 * @version 2.1
 */

import ArrayRef from 'core/classes/ArrayRef';

export default {
    sitePad: {
        // What Roles are allowed to login to sitepad
        loginRoles: new ArrayRef(['admin', 'site']),
        // What Roles are allowed to access the visits view
        visitRoles: new ArrayRef(['admin', 'site']),
        // What Roles are allowed to access the homeview after backing out of a diary that has a context switch on diary entry.
        diaryBackoutRoles: new ArrayRef(['admin', 'site']),
        // What role is the 'subject' role only used in context switching right now...
        subjectRole: 'subject',

        // Reference to a filter function to query the admin users for SitePad.
        // If the config value is null, then the default filter is used which is active users with the 'admin' role
        adminUserFilter: null
    },
    // IG & IT for the saving of Role Data associated with Questionnaires
    lastDiaryRole: {
        IG: 'CG',
        IT: 'LPARole'
    },
    //if passwordFormat is not defined for specific role these settings will be used.
    defaultPasswordFormat: {
        max: 20,
        min: 4,
        lower: 0,
        upper: 0,
        alpha: 0,
        numeric: 0,
        special: 0,
        custom: [],
        allowRepeating: true,
        allowConsecutive: true
    },
    roles: new ArrayRef([
        {
            id: 'subject',
            displayName: 'SUBJECT_ROLE',
            lastDiaryRoleCode: 0,
            defaultAffidavit: 'P_SignatureAffidavit',
            userSelectIconClass: 'patient-icon',
            unlockCodeConfig: {
                seedCode: 1111,
                codeLength: 6
            },
            addPermissionsList: ['ALL'],
            passwordFormat: {
                min: 4,
                max: 8
            },
            product: ['logpad']
        }, {
            id: 'site',
            displayName: 'SITE_ROLE',
            lastDiaryRoleCode: 1,
            permissions: [],
            defaultAffidavit: 'SignatureAffidavit',
            syncLevel: 'site',
            offlineSync: false,
            userSelectIconClass: 'doctor-icon',
            dialogs: {
                deactivated: 'SITE_DEACTIVATED',
                activatedElsewhere: 'SITE_ACTIVATED_ELSEWHERE'
            },
            text: {
                deactivated: 'SITE_DEACTIVATED_LABEL',
                header: 'SITE_GATEWAY_TITLE',
                activatedElsewhere: 'SITE_ACTIVATED_ELSEWHERE'
            },
            addPermissionsList: [],
            passwordFormat: {
                min: 4,
                max: 8
            },
            product: ['logpad', 'sitepad']
        }, {
            id: 'admin',
            displayName: 'SITE_ADMINISTRATOR',
            lastDiaryRoleCode: 2,
            permissions: ['ALL'],
            defaultAffidavit: 'SignatureAffidavit',
            syncLevel: 'site',
            offlineSync: false,
            addPermissionsList: ['ALL'],
            userSelectIconClass: 'admin-icon',
            dialogs: {
                deactivated: 'SITE_DEACTIVATED',
                activatedElsewhere: 'SITE_ACTIVATED_ELSEWHERE'
            },
            text: {
                deactivated: 'SITE_DEACTIVATED_LABEL',
                header: 'APPLICATION_HEADER',
                activatedElsewhere: 'SITE_ACTIVATED_ELSEWHERE'
            },
            passwordFormat: {
                min: 4,
                max: 8
            },
            product: ['logpad', 'sitepad']
        }
        /*, {
            id: 'caregiver',
            displayName: 'CAREGIVER_ROLE',
            lastDiaryRoleCode: 3,
            permissions: ['ALL'],
            defaultAffidavit: 'SignatureAffidavit',
            syncLevel: 'subject',
            offlineSync: true,
            dialogs: {
                deactivated: 'SITE_DEACTIVATED',
                activatedElsewhere: 'SITE_ACTIVATED_ELSEWHERE'
            },
            text: {
                deactivated: 'SITE_DEACTIVATED_LABEL',
                header: 'APPLICATION_HEADER',
                activatedElsewhere: 'SITE_ACTIVATED_ELSEWHERE'
            },
            product: ['logpad']
        }, {
            id: 'caregiver2',
            displayName: 'CAREGIVER2_ROLE',
            lastDiaryRoleCode: 4,
            permissions: ['ALL'],
            defaultAffidavit: 'SignatureAffidavit',
            syncLevel: 'subject',
            offlineSync: true,
            dialogs: {
                deactivated: 'SITE_DEACTIVATED',
                activatedElsewhere: 'SITE_ACTIVATED_ELSEWHERE'
            },
            text: {
                deactivated: 'SITE_DEACTIVATED_LABEL',
                header: 'APPLICATION_HEADER',
                activatedElsewhere: 'SITE_ACTIVATED_ELSEWHERE'
            },
            product: ['logpad']
        }*/
    ]),
    adminUser: { role: 'admin' },
    defaultUserValues: {
        password: 'temp',
        role: 'site',
        language: 'en-US'
    }
};
