// This is a core questionnaire required for the Deactivate User workflow on the SitePad App.
// Please do not remove it, but configure it as needed.
// All resource strings referenced in this questionnaire are core (namespace:CORE) resources,
// but can be overwritten at either the questionnaire (namespace:Deactivate_User)
// or study (namespace:STUDY) levels.
export default {
    questionnaires: [{
        id: 'Deactivate_User',
        SU: 'Deactivate_User',
        displayName: 'DEACTIVATE_USER',
        className: 'DEACTIVATE_USER',
        affidavit: 'DeactivateUserAffidavit',
        screens: ['DEACTIVATE_USER_S_1'],
        product: ['sitepad'],
        branches: [],
        accessRoles: ['admin']
    }],

    screens: [{
        id: 'DEACTIVATE_USER_S_1',
        className: 'DEACTIVATE_USER_S_1',

        // Disable the back button on this screen to prevent a questionnaire back out.
        disableBack: true,
        questions: [
            { id: 'DEACTIVATE_USER_USERNAME', mandatory: true },
            { id: 'DEACTIVATE_USER_ROLE', mandatory: true },
            { id: 'DEACTIVATE_USER_LANG', mandatory: false }
        ]
    }],

    questions: [{
        id: 'DEACTIVATE_USER_USERNAME',
        IG: 'Deactivate_User',
        IT: 'DEACTIVATE_USER_1',
        skipIT: '',
        title: '',
        text: ['DEACTIVATE_USER_CONFIRM_INFORMATION'],
        className: 'DEACTIVATE_USER',
        widget: {
            id: 'DEACTIVATE_USER_W_1',
            type: 'StaticField',
            label : 'FULL_NAME',
            field : 'username'
        }
    }, {
        id: 'DEACTIVATE_USER_ROLE',
        IG: 'Deactivate_User',
        IT: 'DEACTIVATE_USER_2',
        skipIT: '',
        title: '',
        text: [],
        className: 'ROLE',
        widget: {
            id: 'DEACTIVATE_USER_W_2',
            type: 'StaticRoleField',
            label: 'ROLES',
            field: 'role'
        }
    }, {
        id: 'DEACTIVATE_USER_LANG',
        IG: 'Deactivate_User',
        IT: 'DEACTIVATE_USER_3',
        skipIT: '',
        title: '',
        text: [],
        className: 'LANGUAGE',
        widget: {
            id: 'DEACTIVATE_USER_W_3',
            type: 'StaticLanguageField',
            label: 'LANGUAGE',
            field: 'language'
        }
    }],

    affidavits: [{
        id: 'DeactivateUserAffidavit',
        text: ['DEACTIVATE_USER_AFF', 'DEACTIVATE_USER_SIG'],
        krSig: 'SubmitForm',
        widget: {
            id: 'SIGNATURE_AFFIDAVIT_WIDGET',
            className: 'AFFIDAVIT',
            type: 'SignatureBox'
        }
    }],

    rules: [
        // DeactivateUserOpen
        // This rule is required for SPA's Deactivate User workflow.
        // Prior to the Deactivate_User questionnaire openning, this rule is evaluated.
        // If the user cannot access the questionnaire, a notification is displayed.
        {
            id: 'DeactivateUserOpen',
            trigger: 'USERMANAGEMENT:DeactivateUser',
            evaluate: { expression: 'canAccessQuestionnaire', input: 'Deactivate_User' },
            reject: [{
                action: 'notify',
                data: { key: 'INVALID_PERMISSION' }
            }, {
                action: 'preventDefault'
            }]
        },

        // DeactivateUserQuestionnaire
        // This rule is required for SPA's Deactivate User workflow.
        // When the Deactivate_User questionnaire is rendered, configured fields will be populated
        // based on the selected user.  The DeactivateUserQuestionnaire view resolves the selected user,
        // assigning it to this.user.  See the Deactivate_User questionnaire configuration for which fields to populate.
        {
            id: 'DeactivateUserQuestionnaire',
            trigger: 'QUESTIONNAIRE:Rendered/Deactivate_User',
            resolve: [{
                // This action populates answer records based on a configured model.
                action: 'populateFieldsByModel',
                data: [{
                    SW_Alias: 'Deactivate_User.0.DEACTIVATE_USER_1',
                    question_id: 'DEACTIVATE_USER_USERNAME',
                    questionnaire_id: 'Deactivate_User',
                    // The property on this response is determine by the 'field' property
                    // on the question's widget configuration. e.g. { username: 'jsmith' }
                    // Indicate that the response is JSON.
                    isJSON: true,
                    // Determine the field name of the JSON response. e.g. { username: ... }
                    field: 'username',
                    // Use the user model scoped to the UserStatusQuestionnaireView (this.user).
                    model: 'user',
                    // Use the user model's username property to populate the response.
                    // e.g. this.user.get('username');
                    property: 'username'
                }, {
                    SW_Alias: 'Deactivate_User.0.DEACTIVATE_USER_2',
                    question_id: 'DEACTIVATE_USER_ROLE',
                    questionnaire_id: 'Deactivate_User',
                    // The property on this response is determine by the 'field' property
                    // on the question's widget configuration. e.g. { role: 'site' }
                    // Indicate that the response is JSON.
                    isJSON: true,
                    // Determine the field name of the JSON response. e.g. { role: ... }
                    field: 'role',
                    // Use the user model scoped to the UserStatusQuestionnaireView (this.user).
                    model: 'user',
                    // Use the user model's role property to populate the response.
                    // e.g. this.user.get('role');
                    property: 'role'
                }, {
                    SW_Alias: 'Deactivate_User.0.DEACTIVATE_USER_3',
                    question_id: 'DEACTIVATE_USER_LANG',
                    questionnaire_id: 'Deactivate_User',
                    // The property on this response is determine by the 'field' property
                    // on the question's widget configuration. e.g. { language: 'en-US' }
                    // Indicate that the response is JSON.
                    isJSON: true,
                    // Determine the field name of the JSON response. e.g. { language: ... }
                    field: 'language',
                    // Use the user model scoped to the UserStatusQuestionnaireView (this.user).
                    model: 'user',
                    // Use the user model's language property to populate the response.
                    // e.g. this.user.get('language');
                    property: 'language'
                }]
            }]
        },

        // DeactivateUserOfflineSyncCheck
        // This rule is required for the SPA's deactivate user workflow.
        // After clicking the next button on the first screen, if the device is online,
        // and the user record being deactivated is set to not allow offline sync,
        // display a connection required banner.
        {
            id: 'DeactivateUserOfflineSyncCheck',
            trigger: 'QUESTIONNAIRE:Navigate/Deactivate_User/DEACTIVATE_USER_S_1',
            // canSyncOffline is an expression that exists on the context of the questionnaire.
            // see UserStatusQuestionnaireView.js.
            evaluate: ['AND', 'isDirectionForward', '!isOnline', '!canSyncOffline'],
            resolve: [{
                action: 'displayBanner',
                data: 'CONNECTION_REQUIRED'
            }, {
                action: 'preventDefault'
            }]
        },

        // DeactivateUserCompleted
        // This rule is required for the SPA's deactivate user workflow.
        // Upon signing the affidavit, instead of navigating to the QuestionnaireCompletionView,
        // execute the deactivateUser action, navigate to the Manage Site Users view (SiteUserView), and prevent other rules from executing.
        {
            id: 'DeactivateUserCompleted',
            trigger:  'QUESTIONNAIRE:Navigate/Deactivate_User/AFFIDAVIT',
            evaluate: 'isDirectionForward',
            // Set this rule to a high importance, preventing other rules from executing before it.
            salience: 2,
            resolve: [{
                // Displays a loading message.
                action: 'displayMessage',
                data: 'PLEASE_WAIT'
            }, {
                // Deactivates the user, and creates a transmission record to update the user in StudyWorks.
                action: 'editUserStatus',
                data: { active: 0 }
            }, {
                action: function () {
                    return ELF.trigger('DeactivateUserCompleted:Transmit', {}, this);
                }
            }, {
                action: 'updateCurrentContext'
            }, {
                // Remove the loading message.
                action: 'removeMessage'
            }, {
                // Navigate back to the Manage Site Users view.
                action: 'navigateTo',
                data: 'site-users'
            }, {
                // Prevent all other rules and functionality from executing.
                // This prevents a dashbord record from being written to the database.
                action: 'preventAll'
            }],
            // Prevent the default functionality from executing.
            // This prevents a dashboard record from being written to the database.
            reject: [{ action: 'preventDefault' }]
        },

        // DeactivateUserQuestionnaireTimeout
        // This rule is required for the SPA's deactivate user workflow.
        // Upon signing the affidavit, instead of navigating to the QuestionnaireCompletionView,
        // execute the deactivateUser action, navigate to the Manage Site Users view (SiteUserView), and prevent other rules from executing.
        {
            id: 'DeactivateUserQuestionnaireTimeout',
            trigger: 'QUESTIONNAIRE:QuestionnaireTimeout/Deactivate_User',
            evaluate: 'isAffidavitSigned',
            salience: 3,
            resolve: [{
                // Displays a loading message.
                action: 'displayMessage',
                data: 'PLEASE_WAIT'
            }, {
                // Deactivates the user, and creates a transmission record to update the user in StudyWorks.
                action: 'editUserStatus',
                data: { active: 0 }
            }, {
                // Remove the loading message.
                action: 'removeMessage'
            }, {
                // Navigate back to the Manage Site Users view.
                action: 'navigateTo',
                data: 'site-users'
            }, {
                // Prevent all other rules and functionality from executing.
                // This prevents a dashbord record from being written to the database.
                action: 'preventAll'
            }],
            // Prevent the default functionality from executing.
            // This prevents a dashboard record from being written to the database.
            reject: [{
                action: 'navigateTo',
                data: 'site-users'
            }, {
                action: 'notify',
                data: { key: 'DIARY_TIMEOUT' }
            }, {
                action: 'preventAll'
            }]
        },

        // DeactivateUserSessionTimeout
        // NOTE: This rule is required for SPA's Deactivate User workflow.
        {
            id: 'DeactivateUserSessionTimeout',
            trigger: 'QUESTIONNAIRE:SessionTimeout/Deactivate_User',
            evaluate: 'isAffidavitSigned',
            salience: 3,
            resolve: [{
                // Displays a loading message.
                action: 'displayMessage',
                data: 'PLEASE_WAIT'
            }, {
                // Deactivates the user, and creates a transmission record to update the user in StudyWorks.
                action: 'editUserStatus',
                data: { active: 0 }
            }, {
                action: 'logout'
            }, {
                // Prevent all other rules and functionality from executing.
                // This prevents a dashbord record from being written to the database.
                action: 'preventAll'
            }]
        },

        // DeactivateUserAffidavitBackout
        // This rule is required for the Deactivate User workflow.
        // Due to the DeactivateUserCompleted rule, we need this rule to
        // reinstate the back button behavior on the affidavit screen of the questionnaire.
        {
            id: 'DeactivateUserAffidavitBackout',
            // Trigger only on a navigation event on the affidavit screen of the Deactivate User questionnaire.
            trigger: 'QUESTIONNAIRE:Navigate/Deactivate_User/AFFIDAVIT',
            // If the back button was pressed, evaluate as true.
            evaluate: 'isDirectionBackward',
            // Set the importance of this rule below that of DeactivateUserCompleted.
            salience: 1,
            // Trigger the navigation handler of the BaseQuestionnaireView to navigate back to the previous screen.
            resolve: [{ action: 'navigationHandler', data: 'back' }]
        },

        // DeactivateUserCancel
        // This rule is required for the SPA's deactivate user workflow.
        // When attempting to cancel the Deactivate_User workflow, a confirmation dialog will appear.
        // Clicking 'Cancel' will cancel out the form and navigate to the Manage Site Users (SiteUsersView) view.
        // Clicking 'No' will close the dialog.
        {
            id: 'DeactivateUserCancel',
            trigger: 'QUESTIONNAIRE:Canceled/Deactivate_User',
            resolve: [
                { action: 'navigateTo', data: 'site-users' },
                // Prevent the default action, which is to navigate to the dashboard view.
                { action: 'preventDefault' }
            ]
        }
    ]
};
