// This is a core questionnaire required for the New Patient workflow on the SitePad App.
// Please do not remove it, but configure it as needed.
// All resource strings referenced in this questionnaire are core (namespace:CORE) resources,
// but can be overwritten at either the questionnaire (namespace:New_Patient)
// or study (namespace:STUDY) levels.
export default {
    questionnaires :[{
        id              : 'New_Patient',
        SU              : 'New_Patient',
        displayName     : 'ADD_PATIENT',
        className       : 'ADD_PATIENT',
        // Use a custom affidavit designed for this questionnaire.
        // The affidavit configuration can be found below.
        affidavit       : 'NewPatientAffidavit',
        // This questionnaire is only available on the SitePad App.
        product: ['sitepad'],
        screens         : [
            'ADD_PATIENT_S_1',
            'ADD_PATIENT_S_2'
        ],
        branches        : [],
        // Only Site Users and System Adminstrators may access this questionnaire.
        accessRoles     : ['site', 'admin']
    }],

    screens : [{
        id          : 'ADD_PATIENT_S_1',
        className   : 'ADD_PATIENT_S_1',

        // Prevent the back button from displaying on the first screen.
        disableBack : true,
        questions   : [
            { id: 'ADD_PATIENT_INFO' },
            { id: 'ADD_PATIENT_ID', mandatory: true },
            { id: 'ADD_PATIENT_INITIALS', mandatory: true }
        ]
    }, {
        id          : 'ADD_PATIENT_S_2',
        className   : 'ADD_PATIENT_S_2',
        questions   : [
            { id: 'ADD_PATIENT_LANGUAGE_INFO' },
            { id: 'ADD_PATIENT_LANGUAGE', mandatory: true }
        ]
    }],

    questions : [{
        id          : 'ADD_PATIENT_INFO',
        IG          : 'Add_Patient',
        IT          : 'ADD_PATIENT_INFO',
        skipIT      : '',
        title       : '',
        text        : ['NEW_PATIENT_QUESTION_1_MAIN_TITLE'],
        className   : 'ADD_PATIENT_INFO'
    }, {
        id          : 'ADD_PATIENT_ID',
        IG          : 'Add_Patient',
        IT          : 'ADD_PATIENT_ID',
        skipIT      : '',
        title       : '',
        text        : ['NEW_PATIENT_QUESTION_1'],
        className   : 'ADD_PATIENT_ID',
        widget      : {
            id                  : 'ADD_PATIENT_W_1',
            type                : 'PatientIDTextBox',
            templates           : {},
            answers             : [],
            // numeric from 1 to 4 characters
            maxLength           : 4,
            allowedKeyRegex     : /[\d]/,
            validateRegex       : /[\d]+/,
            validationErrors    : [{
                property: 'completed',
                errorType: 'popup',
                errString: 'NEW_PATIENT_QUESTION_1_EMPTY',
                header: 'NEW_PATIENT_QUESTION_1_EMPTY_HEADER',
                ok: 'OK'
            }],
            validation: {
                validationFunc: 'checkPatientID',
                params: {
                    errorStrings: {
                        isInRange: {
                            errString: 'NEW_PATIENT_QUESTION_1_RANGE',
                            header: 'NEW_PATIENT_QUESTION_1_RANGE_HEADER',
                            ok: 'OK'
                        },
                        isUnique: {
                            errString: 'NEW_PATIENT_QUESTION_1_UNIQUE',
                            header:'NEW_PATIENT_QUESTION_1_UNIQUE_HEADER',
                            ok:'OK'
                        }
                    }
                }
            }
        }
    }, {
        id          : 'ADD_PATIENT_INITIALS',
        IG          : 'Add_Patient',
        IT          : 'ADD_PATIENT_INITIALS',
        skipIT      : '',
        title       : '',
        text        : ['NEW_PATIENT_QUESTION_2'],
        className   : 'ADD_PATIENT_INITIALS',
        widget      : {
            id          : 'ADD_PATIENT_W_2',
            type        : 'TextBox',
            templates   : {},
            answers     : [],
            // Disallow digits, white space, and punctuation, except quotes (for Hebrew abbreviations)
            // jscs:disable maximumLineLength
            disallowedKeyRegex: /[\d\s\-=_!#%&*{},.\/:;?\(\)\[\]@\\$\^*+<>~`\u00a1\u00a7\u00b6\u00b7\u00bf\u037e\u0387\u055a-\u055f\u0589\u05c0\u05c3\u05c6\u05f3\u05f4\u0609\u060a\u060c\u060d\u061b\u061e\u061f\u066a-\u066d\u06d4\u0700-\u070d\u07f7-\u07f9\u0830-\u083e\u085e\u0964\u0965\u0970\u0af0\u0df4\u0e4f\u0e5a\u0e5b\u0f04-\u0f12\u0f14\u0f85\u0fd0-\u0fd4\u0fd9\u0fda\u104a-\u104f\u10fb\u1360-\u1368\u166d\u166e\u16eb-\u16ed\u1735\u1736\u17d4-\u17d6\u17d8-\u17da\u1800-\u1805\u1807-\u180a\u1944\u1945\u1a1e\u1a1f\u1aa0-\u1aa6\u1aa8-\u1aad\u1b5a-\u1b60\u1bfc-\u1bff\u1c3b-\u1c3f\u1c7e\u1c7f\u1cc0-\u1cc7\u1cd3\u2016\u2017\u2020-\u2027\u2030-\u2038\u203b-\u203e\u2041-\u2043\u2047-\u2051\u2053\u2055-\u205e\u2cf9-\u2cfc\u2cfe\u2cff\u2d70\u2e00\u2e01\u2e06-\u2e08\u2e0b\u2e0e-\u2e16\u2e18\u2e19\u2e1b\u2e1e\u2e1f\u2e2a-\u2e2e\u2e30-\u2e39\u3001-\u3003\u303d\u30fb\ua4fe\ua4ff\ua60d-\ua60f\ua673\ua67e\ua6f2-\ua6f7\ua874-\ua877\ua8ce\ua8cf\ua8f8-\ua8fa\ua92e\ua92f\ua95f\ua9c1-\ua9cd\ua9de\ua9df\uaa5c-\uaa5f\uaade\uaadf\uaaf0\uaaf1\uabeb\ufe10-\ufe16\ufe19\ufe30\ufe45\ufe46\ufe49-\ufe4c\ufe50-\ufe52\ufe54-\ufe57\ufe5f-\ufe61\ufe68\ufe6a\ufe6b\uff01-\uff03\uff05-\uff07\uff0a\uff0c\uff0e\uff0f\uff1a\uff1b\uff1f\uff20\uff3c\uff61\uff64\uff65]/,
            // jscs:enable
            maxLength       : 6,
            validateRegex   : /.{2,}/,
            validationErrors: [{
                property: 'completed',
                errorType: 'popup',
                errString: 'NEW_PATIENT_QUESTION_2_EMPTY',
                header: 'NEW_PATIENT_QUESTION_2_EMPTY_HEADER',
                ok: 'OK'
            }]
        }
    }, {
        id          : 'ADD_PATIENT_LANGUAGE_INFO',
        IG          : 'Add_Patient',
        IT          : 'ADD_PATIENT_LANGUAGE_INFO',
        skipIT      : '',
        title       : '',

        text        : ['NEW_PATIENT_QUESTION_3_TITLE'],
        className   : 'ADD_PATIENT_LANGUAGE_INFO'
    }, {
        id          : 'ADD_PATIENT_LANGUAGE',
        IG          : 'Add_Patient',
        IT          : 'ADD_PATIENT_LANGUAGE',
        skipIT      : '',
        title       : '',
        text        : ['NEW_PATIENT_QUESTION_3'],
        className   : '',
        widget      : {
            id          : 'ADD_PATIENT_W_3',
            type        : 'PatientLangListRadioButton',
            label       : 'Language',
            templates   : {},
            answers     : []
        }
    }],

    affidavits: [{
        id: 'NewPatientAffidavit',
        text: [
            'NEW_PATIENT_AFFIDAVIT_HEADER',
            'SITE_REPORT_AFF'
        ],
        templates: { question: 'AffidavitText' },
        krSig: 'SubmitForm',
        widget: {
            id: 'SIGNATURE_AFFIDAVIT_WIDGET',
            className: 'AFFIDAVIT',
            type: 'SignatureBox'
        }
    }],

    rules: [
        // NewPatientCancel
        // NOTE: This rule is required for the New Patient workflow. Do not remove.
        // This rule ensures that when the questionnaire is cancelled, the correct view is displayed.
        {
            id: 'NewPatientCancel',
            trigger: 'QUESTIONNAIRE:Canceled/New_Patient',
            resolve: [
                // Instead of navigating to 'dashboard', we want to navigate to 'home'.
                { action: 'navigateTo', data: 'home' },

                // Prevent the default navigate call.
                { action: 'preventDefault' }
            ]
        },

        // QuestionnaireTransmitNewPatient
        // NOTE: This rule is required for the New Patient workflow. Do not remove.
        // This rule ensures that the newly created patient is transmitted after the questionnaire is completed.
        //
        {
            id: 'QuestionnaireTransmitNewPatient',
            trigger: 'QUESTIONNAIRE:Transmit/New_Patient',
            salience: 1,
            resolve: [
                { action: 'displayMessage', data: 'PLEASE_WAIT' },
                { action: 'navigateTo', data: 'home' },
                { action: 'removeMessage' },
                { action: 'preventDefault' }
            ]
        },

        // NewPatientAffidavitBackout
        // NOTE: This rule is required for the New Patient workflow. Do not remove.
        // Due to the NewPatientSave we need this rule to
        // reinstate the back button behavior on the affidavit screen of the questionnaire.
        {
            id: 'NewPatientAffidavitBackout',
            // Trigger only on a navigation event on the affidavit screen of the New Patient questionnaire.
            trigger: 'QUESTIONNAIRE:Navigate/New_Patient/AFFIDAVIT',
            // If the back button was pressed, evaluate as true.
            evaluate: 'isDirectionBackward',
            // Set the importance of this rule below that of NewPatientSave.
            salience: 2,
            // Trigger the navigation handler of the BaseQuestionnaireView to navigate back to the previous screen.
            resolve: [
                { action: 'navigationHandler', data: 'back' },
                { action: 'preventDefault' }
            ]
        },

        // NewPatientSave
        // NOTE: This rule is required for the New Patient workflow. Do not remove.
        // After the user clicks 'Next' on the affidavit screen, save the new patient
        // and queue a transmission record to transmit the patient.
        {
            id: 'NewPatientSave',
            trigger: 'QUESTIONNAIRE:Navigate/New_Patient/AFFIDAVIT',
            // Only resolved on 'next' click.
            evaluate: 'isDirectionForward',
            salience: 1,
            resolve: [
                { action: 'displayMessage', data: 'PLEASE_WAIT' },
                { action: 'newPatientSave' }
            ]
        },

        // NewPatientSaveOnTimeout
        // NOTE: This rule is required for the New Patient workflow. Do not remove.
        {
            id: 'NewPatientQuestionnaireTimeout',
            trigger:'QUESTIONNAIRE:QuestionnaireTimeout/New_Patient',
            evaluate: 'isAffidavitSigned',
            salience: 3,
            resolve: [
                { action: 'displayMessage', data: 'PLEASE_WAIT' },
                { action: 'newPatientSave' },
                { action: 'removeMessage' },
                { action: 'navigateTo', data: 'home' },
                { action: 'preventAll'}
            ],
            reject: [{
                action: 'navigateTo',
                data: 'home'
            }, {
                action: 'notify',
                data: { key: 'DIARY_TIMEOUT' }
            }, {
                action: 'preventAll'
            }]
        },

        // NewPatientSaveOnTimeout
        // NOTE: This rule is required for the New Patient workflow. Do not remove.
        {
            id: 'NewPatientSessionTimeout',
            trigger: 'QUESTIONNAIRE:SessionTimeout/New_Patient',
            salience: 3,
            resolve: [
                { action: 'displayMessage', data: 'PLEASE_WAIT' },
                { action: 'newPatientSave' },
                { action: 'removeMessage' }
            ]
        },

        // NewPatientPreventNavigation
        // NOTE: This rule is required for the New Patient workflow
        {
            id: 'NewPatientDefect',
            trigger: 'NAVIGATE:application/addNewPatient',
            evaluate: 'isQuestionnaireCompleted',
            resolve : [{ action: 'preventDefault' }]
        }
    ]
};
