// This is a core questionnaire required for the Aactivate User workflow on the SitePad App.
// Please do not remove it, but configure it as needed.
// All resource strings referenced in this questionnaire are core (namespace:CORE) resources,
// but can be overwritten at either the questionnaire (namespace:Edit_User)
// or study (namespace:STUDY) levels.

// These are form field configurations used in several expressions and actions throughout
// the questionnaire's workflow. This governs the how each form field is populated, how to
// determine if the field has changed from it's original value, and how to save the updated user.
let fields = [{
    SW_Alias: 'Edit_User.0.EDIT_USER_1',
    question_id: 'EDIT_USER_USERNAME',
    questionnaire_id: 'Edit_User',
    // The property on this response is determine by the 'field' property
    // on the question's widget configuration. e.g. { username: 'jsmith' }
    // Indicate that the response is JSON.
    isJSON: true,
    // Determine the field name of the JSON response. e.g. { username: ... }
    field: 'username',
    // Use the user model scoped to the UserStatusQuestionnaireView (this.user).
    model: 'user',
    // Use the user model's username property to populate the response.
    // e.g. this.user.get('username');
    property: 'username'
}, {
    SW_Alias: 'Edit_User.0.EDIT_USER_2',
    question_id: 'EDIT_USER_ROLE',
    questionnaire_id: 'Edit_User',
    // The property on this response is determine by the 'field' property
    // on the question's widget configuration. e.g. { role: 'site' }
    // Indicate that the response is JSON.
    isJSON: true,
    // Determine the field name of the JSON response. e.g. { role: ... }
    field: 'role',
    // Use the user model scoped to the UserStatusQuestionnaireView (this.user).
    model: 'user',
    // Use the user model's role property to populate the response.
    // e.g. this.user.get('role');
    property: 'role'
}, {
    SW_Alias: 'Edit_User.0.EDIT_USER_3',
    question_id: 'EDIT_USER_LANG',
    questionnaire_id: 'Edit_User',
    // The property on this response is determine by the 'field' property
    // on the question's widget configuration. e.g. { language: 'en-US' }
    // Indicate that the response is JSON.
    isJSON: true,
    // Determine the field name of the JSON response. e.g. { language: ... }
    field: 'language',
    // Use the user model scoped to the UserStatusQuestionnaireView (this.user).
    model: 'user',
    // Use the user model's language property to populate the response.
    // e.g. this.user.get('language');
    property: 'language'
}];

export default {
    questionnaires: [{
        id: 'Edit_User',
        SU: 'Edit_User',
        displayName: 'EDIT_USER',
        className: 'EDIT_USER',
        // Use a custom affidavit designed for this questionnaire.
        // The affidavit configuration can be found below.
        affidavit: 'EditUserAffidavit',
        screens: ['EDIT_USER_S_1'],
        branches: [],
        // This questionnaire is only available on the SitePad App.
        product: ['sitepad'],
        // This questionnaire can only be accessed by a Site Administrator.
        accessRoles: ['admin']

    }],

    screens: [{
        id: 'EDIT_USER_S_1',
        className: 'EDIT_USER_S_1',
        // Hide the back button on the first screen.
        // The only way to exit the questionnaire is via cancel.
        disableBack: true,
        questions: [
            { id: 'EDIT_USER_USERNAME', mandatory: true },
            { id: 'EDIT_USER_ROLE', mandatory: true },
            { id: 'EDIT_USER_LANG', mandatory: false }
        ]
    }],

    questions: [{
        // Field to edit the user's username.
        id: 'EDIT_USER_USERNAME',
        IG: 'Edit_User',
        IT: 'EDIT_USER_1',
        skipIT: '',
        title: '',
        text: [],
        className: 'EDIT_USER',
        widget: {
            id: 'EDIT_USER_W_1',
            type: 'AddUserTextBox',
            label: 'FULL_NAME',
            templates: {},
            answers: [],
            field: 'username',
            maxLength: 25,
            allowedKeyRegex: /[\w\W ]/,
            validateRegex: /[\w\W ]{3,}/,
            validation: {
                validationFunc: 'checkUserNameField',
                params: {
                    maxLength: 25,
                    minLength: 3
                }
            }
        }
    }, {
        // Field to edit the user's role.
        id: 'EDIT_USER_ROLE',
        IG: 'Edit_User',
        IT: 'EDIT_USER_2',
        skipIT: '',
        title: '',
        text: [],
        className: 'ROLE',
        widget: {
            id: 'EDIT_USER_W_2',
            type: 'RoleSelectWidget',
            label: 'ROLES',
            field: 'role',
            templates: {},
            answers: [],
            params: {
                language: 'LANGUAGE',
                password: 'PASSWORD'
            },
            validation: {
                validationFunc: 'checkRoleFieldWidget',
                params: {
                    errorString: 'CONNECTION_REQUIRED'
                }
            }
        }
    }, {
        // Field to edit the user's lanuage.
        id: 'EDIT_USER_LANG',
        IG: 'Edit_User',
        IT: 'EDIT_USER_3',
        skipIT: '',
        title: '',
        text: [],
        className: 'LANGUAGE',
        widget: {
            id: 'EDIT_USER_W_3',
            type: 'LanguageSelectWidget',
            label: 'LANGUAGE',
            field: 'language',
            templates: {},
            answers: []

        }
    }],

    // This is a custom affidavit to be used only by this questionnaire.
    affidavits: [{
        id: 'EditUserAffidavit',
        text: ['EDIT_USER_AFF'],
        krSig: 'SubmitForm',
        widget: {
            id: 'SIGNATURE_AFFIDAVIT_WIDGET',
            className: 'AFFIDAVIT',
            type: 'SignatureBox'
        }
    }],

    rules: [

        // EditUserOpen
        // NOTE: This rule is required for SPA's Edit User workflow.
        // Prior to the Edit_User questionnaire openning, this rule is evaluated.
        // If the user cannot access the questionnaire, an 'Invalid Permissions' notification is displayed.
        {
            id: 'EditUserOpen',
            trigger: 'USERMANAGEMENT:EditUser',
            evaluate: { expression: 'canAccessQuestionnaire', input: 'Edit_User' },
            reject: [{
                action: 'notify',
                data: { key: 'INVALID_PERMISSION' }
            }, {
                action: 'preventDefault'
            }]
        },

        // EditUserQuestionnaire
        // NOTE: This rule is required for SPA's Edit User workflow.
        // When the Edit_User questionnaire is rendered, configured fields will be populated
        // based on the selected user.  The EditUserQuestionnaire view resolves the selected user,
        // assigning it to this.user.  See the Edit_User questionnaire configuration for which fields to populate.
        {
            id: 'EditUserQuestionnaire',
            trigger: 'QUESTIONNAIRE:Rendered/Edit_User',
            resolve: [{
                // This action populates answer records based on a configured model.
                action: 'populateFieldsByModel',
                data: fields
            }]
        },

        // EditUserChange
        // NOTE: This rule is required for SPA's Edit User workflow.
        // When the user navigates forward on the first screen of the questionnaire, a check is executed
        // to see if the form has changed.
        {
            id: 'EditUserChange',
            // Triggers when a navigation event is triggered on the first screen of the questionnaire.
            trigger: 'QUESTIONNAIRE:Navigate/Edit_User/EDIT_USER_S_1',
            // Only resolves if the direction is forward, and the form has changed.
            evaluate: ['AND', 'isDirectionForward', {
                expression: 'hasFormChanged',
                input: fields
            }],
            // Set to a higher importance than EditUserConfirmation, so this rule executes first.
            salience: 2,
            resolve: [{
                // Navigate to the next screen, which in our case is the affidavit.
                action: 'navigationHandler',
                data: 'next'
            }, {
                // Prevents EditUserConfirmation from being evaluated.
                action: 'preventAll'
            }]
        },

        // EditUserConfirmation
        // NOTE: This rule is required for SPA's Edit User workflow.
        // If the EditUserChange rule is rejected, this rule is executed.
        // Displays a confirmation informing the user that no changes have been made to the user.
        {
            id: 'EditUserConfirmation',
            // Triggers when a navigation event is triggered on the first screen of the questionnaire.
            trigger: 'QUESTIONNAIRE:Navigate/Edit_User/EDIT_USER_S_1',
            // Displays a confirmation. If confirmed, the questionnaire is cancelled
            // and the Manage Site Users (SiteUsersView) view is displayed.  If denied, the confirmation
            // closes and nothing happens.
            evaluate: [{
                expression: 'confirm',
                input: { key: 'EDIT_USER_NO_CHANGES' }
            }],
            // Set to a lower importance than EditUserChange, so it executes last.
            salience: 1,
            resolve: [
                { action: 'navigateTo', data: 'site-users' },
                { action: 'preventDefault' }
            ],
            reject: [{ action: 'preventAll' }]
        },

        // EditUserCompleted
        // NOTE: This rule is required for SPA's Edit User workflow.
        // Upon signing the affidavit, instead of navigating to the QuestionnaireCompletionView,
        // execute the editUserSave action, navigate to the Manage Site Users view (SiteUserView), and prevent other rules from executing.
        {
            id: 'EditUserCompleted',
            trigger: 'QUESTIONNAIRE:Navigate/Edit_User/AFFIDAVIT',
            evaluate: 'isDirectionForward',
            salience: 2,
            resolve: [{
                // Displays a loading message.
                action: 'displayMessage',
                data: 'PLEASE_WAIT'
            }, {
                // Saves the user updates, and creates a transmission record to update the user in StudyWorks.
                action: 'editUserSave',
                data: fields
            }, {
                action: function () {
                    $('#nextItem').attr('disabled', 'disabled');
                    $('#cancelItem').attr('disabled', 'disabled');
                    $('#prevItem').attr('disabled', 'disabled');
                    return ELF.trigger('EditUserCompleted:Transmit', {}, this);
                }
            }, {
                action: 'updateCurrentContext'
            }, {
                // Remove the loading message.
                action: 'removeMessage'
            }, {
                // Navigate back to the Manage Site Users view.
                action: 'navigateTo',
                data: 'site-users'
            }, {
                // Prevent all other rules and functionality from executing.
                // This prevents a dashbord record from being written to the database.
                action: 'preventAll'
            }],
            // Prevent the default functionality from executing.
            // This prevents a dashboard record from being written to the database.
            reject: [{ action: 'preventDefault' }]
        },

        // EditUserQuestionnaireTimeout
        // NOTE: This rule is required for SPA's Edit User workflow.
        {
            id: 'EditUserQuestionnaireTimeout',
            trigger: 'QUESTIONNAIRE:QuestionnaireTimeout/Edit_User',
            evaluate: 'isAffidavitSigned',
            salience: 3,
            resolve: [{
                // Displays a loading message.
                action: 'displayMessage',
                data: 'PLEASE_WAIT'
            }, {
                // Saves the user updates, and creates a transmission record to update the user in StudyWorks.
                action: 'editUserSave',
                data: fields
            }, {
                action: 'updateCurrentContext'
            }, {
                // Remove the loading message.
                action: 'removeMessage'
            }, {
                // Creates a flash (success) banner for the user being updated.
                action: 'displayBanner',
                data: 'USER_EDITED'
            }, {
                // Navigate back to the Manage Site Users view.
                action: 'navigateTo',
                data: 'site-users'
            }, {
                // Prevent all other rules and functionality from executing.
                // This prevents a dashbord record from being written to the database.
                action: 'preventAll'
            }],
            // Prevent the default functionality from executing.
            // This prevents a dashboard record from being written to the database.
            reject: [{
                action: 'navigateTo',
                data: 'site-users'
            }, {
                action: 'notify',
                data: { key: 'DIARY_TIMEOUT' }
            }, {
                action: 'preventAll'
            }]
        },


        // EditUserSessionTimeout
        // NOTE: This rule is required for SPA's Edit User workflow.
        {
            id: 'EditUserSessionTimeout',
            trigger: 'QUESTIONNAIRE:SessionTimeout/Edit_User',
            evaluate: 'isAffidavitSigned',
            salience: 3,
            resolve: [{
                // Displays a loading message.
                action: 'displayMessage',
                data: 'PLEASE_WAIT'
            }, {
                // Saves the user updates, and creates a transmission record to update the user in StudyWorks.
                action: 'editUserSave',
                data: fields
            }, {
                action: 'logout'
            }, {
                // Prevent all other rules and functionality from executing.
                // This prevents a dashbord record from being written to the database.
                action: 'preventAll'
            }]
        },

        // EditUserAffidavitBackout
        // NOTE: This rule is required for SPA's Edit User workflow.
        // Due to the EditUserCompleted rule, we need this rule to
        // reinstate the back button behavior on the affidavit screen of the questionnaire.
        {
            id: 'EditUserAffidavitBackout',
            // Trigger only on a navigation event on the affidavit screen of the Edit User questionnaire.
            trigger: 'QUESTIONNAIRE:Navigate/Edit_User/AFFIDAVIT',
            // If the back button was pressed, evaluate as true.
            evaluate: 'isDirectionBackward',
            // Set the importance of this rule below that of EditUserCompleted.
            salience: 1,
            // Trigger the navigation handler of the BaseQuestionnaireView to navigate back to the previous screen.
            resolve: [{ action: 'navigationHandler', data: 'back' }]
        },

        // EditUserCancel
        // NOTE: This rule is required for SPA's Edit User workflow.
        // When attempting to cancel the Edit_User workflow, a confirmation dialog will appear.
        // Clicking 'Yes' will cancel out the form and navigate to the Manage Site Users (SiteUsersView) view.
        // Clicking 'No' will close the dialog.
        {
            id: 'EditUserCancel',
            trigger: 'QUESTIONNAIRE:Canceled/Edit_User',
            resolve: [
                { action: 'navigateTo', data: 'site-users' },
                // Prevent the default action, which is to navigate to the dashboard view.
                { action: 'preventDefault' }
            ]
        }
    ]
};
