// CORE FILES
import Add_User_Diary from './add-user';
import Deactivate_User from './deactivate-user';
import Activate_User from './activate-user';
import Edit_User from './edit-user';
import New_Patient_Diary from './new-patient';

import Skip_Visit_Diary from './skip-visit';
import Edit_Patient_Diary from './edit-patient-diary';
import First_Site_User from './first-site-user';
import Time_Confirmation from './time-confirmation';
// CORE FILES

import {mergeObjects} from 'core/utilities/languageExtensions';

export default mergeObjects ({}, Add_User_Diary, New_Patient_Diary, Skip_Visit_Diary, Edit_Patient_Diary,
	First_Site_User, Deactivate_User, Activate_User, Edit_User, Time_Confirmation);