// This is a core questionnaire required for the Edit Patient workflow on the SitePad App.
// Please do not remove it, but configure it as needed.
// All resource strings referenced in this questionnaire are core (namespace:CORE) resources,
// but can be overwritten at either the questionnaire (namespace:Edit_Patient)
// or study (namespace:STUDY) levels.
export default {
    questionnaires: [{
        id: 'Edit_Patient',
        SU: 'Edit_Patient',
        displayName: 'EDIT_PATIENT',
        className: 'EDIT_PATIENT',
        // Use a custom affidavit designed for this questionnaire.
        // The affidavit configuration can be found below.
        affidavit: 'EditPatientSignatureAffidavit',
        // This questionnaire is only available on the SitePad App.
        product: ['sitepad'],
        screens: [
            'EDIT_PATIENT_S_1',
            'EDIT_PATIENT_S_2',
            'EDIT_PATIENT_S_3'
        ],
        branches: [],
        // Only Site Users and System Adminstrators may access this questionnaire.
        accessRoles: ['site', 'admin']
    }],

    screens: [{
        id: 'EDIT_PATIENT_S_1',
        className: 'EDIT_PATIENT_S_1',

        // DE16954 - Disable the back button on this screen only.
        disableBack: true,
        questions: [
            { id: 'EDIT_PATIENT_S1_INFO' },
            { id: 'EDIT_PATIENT_ID', mandatory: true },
            { id: 'EDIT_PATIENT_INITIALS', mandatory: true }
        ]
    }, {
        id: 'EDIT_PATIENT_S_2',
        className: 'EDIT_PATIENT_S_2',
        questions: [
            { id: 'EDIT_PATIENT_LANGUAGE', mandatory: true }
        ]
    }, {
        id: 'EDIT_PATIENT_S_3',
        className: 'EDIT_PATIENT_S_3',
        questions: [
            { id: 'EDIT_PATIENT_REASON_INFO' },
            { id: 'EDIT_PATIENT_REASON', mandatory: true }
        ]
    }],

    questions: [{
        id: 'EDIT_PATIENT_S1_INFO',
        IG: 'Add_Patient',
        IT: 'EDIT_PATIENT_S1_INFO',
        skipIT: '',
        title: '',
        text: ['EDIT_PATIENT_QUESTION_1_MAIN_TITLE'],
        className: 'EDIT_PATIENT_S1_INFO'
    }, {
        id: 'EDIT_PATIENT_ID',
        IG: 'PT',
        IT: 'Patientid',
        skipIT: '',
        title: '',
        text: ['EDIT_PATIENT_QUESTION_1'],
        className: 'EDIT_PATIENT_ID',
        widget: {
            id: 'EDIT_PATIENT_W_1',
            type: 'PatientIDTextBox',
            templates: {},
            answers: [],
            // numeric from 1 to 4 characters
            maxLength: 4,
            allowedKeyRegex: /[\d]/,
            validateRegex: /[\d]+/,
            _isUnique: true,
            _isInRange: true,
            validationErrors: [{
                property: 'completed',
                errorType: 'popup',
                errString: 'EDIT_PATIENT_QUESTION_1_EMPTY',
                header: 'EDIT_PATIENT_QUESTION_1_EMPTY_HEADER',
                ok: 'OK'
            }, {
                property: 'isInRange',
                errorType: 'popup',
                errString: 'EDIT_PATIENT_QUESTION_1_RANGE',
                header: 'EDIT_PATIENT_QUESTION_1_RANGE_HEADER',
                ok: 'OK'
            }],
            validation: {
                validationFunc: 'checkEditPatientID',
                params: {
                    errorStrings: {
                        errString: 'EDIT_PATIENT_QUESTION_1_UNIQUE',
                        header:'EDIT_PATIENT_QUESTION_1_UNIQUE_HEADER',
                        ok:'OK',
                        isInRange: {
                            errString: 'EDIT_PATIENT_QUESTION_1_RANGE',
                            header: 'EDIT_PATIENT_QUESTION_1_RANGE_HEADER',
                            ok: 'OK'
                        },

                        isUnique: {
                            errString: 'EDIT_PATIENT_QUESTION_1_UNIQUE',
                            header:'EDIT_PATIENT_QUESTION_1_UNIQUE_HEADER',
                            ok:'OK'
                        }
                    }
                }
            }
        }
    }, {
        id: 'EDIT_PATIENT_INITIALS',
        IG: 'PT',
        IT: 'Initials',
        skipIT: '',
        title: '',
        text: ['EDIT_PATIENT_QUESTION_2'],
        className: 'EDIT_PATIENT_INITIALS',
        widget: {
            id: 'EDIT_PATIENT_W_2',
            type: 'TextBox',
            templates: {},
            answers: [{ text: 'REASON_0', value: '0' }],

            // Disallow digits, white space, and punctuation, except quotes (for Hebrew abbreviations)
            // jscs:disable maximumLineLength
            disallowedKeyRegex: /[\d\s\-=_!#%&*{},.\/:;?\(\)\[\]@\\$\^+<>~`\u00a1\u00a7\u00b6\u00b7\u00bf\u037e\u0387\u055a-\u055f\u0589\u05c0\u05c3\u05c6\u05f3\u05f4\u0609\u060a\u060c\u060d\u061b\u061e\u061f\u066a-\u066d\u06d4\u0700-\u070d\u07f7-\u07f9\u0830-\u083e\u085e\u0964\u0965\u0970\u0af0\u0df4\u0e4f\u0e5a\u0e5b\u0f04-\u0f12\u0f14\u0f85\u0fd0-\u0fd4\u0fd9\u0fda\u104a-\u104f\u10fb\u1360-\u1368\u166d\u166e\u16eb-\u16ed\u1735\u1736\u17d4-\u17d6\u17d8-\u17da\u1800-\u1805\u1807-\u180a\u1944\u1945\u1a1e\u1a1f\u1aa0-\u1aa6\u1aa8-\u1aad\u1b5a-\u1b60\u1bfc-\u1bff\u1c3b-\u1c3f\u1c7e\u1c7f\u1cc0-\u1cc7\u1cd3\u2016\u2017\u2020-\u2027\u2030-\u2038\u203b-\u203e\u2041-\u2043\u2047-\u2051\u2053\u2055-\u205e\u2cf9-\u2cfc\u2cfe\u2cff\u2d70\u2e00\u2e01\u2e06-\u2e08\u2e0b\u2e0e-\u2e16\u2e18\u2e19\u2e1b\u2e1e\u2e1f\u2e2a-\u2e2e\u2e30-\u2e39\u3001-\u3003\u303d\u30fb\ua4fe\ua4ff\ua60d-\ua60f\ua673\ua67e\ua6f2-\ua6f7\ua874-\ua877\ua8ce\ua8cf\ua8f8-\ua8fa\ua92e\ua92f\ua95f\ua9c1-\ua9cd\ua9de\ua9df\uaa5c-\uaa5f\uaade\uaadf\uaaf0\uaaf1\uabeb\ufe10-\ufe16\ufe19\ufe30\ufe45\ufe46\ufe49-\ufe4c\ufe50-\ufe52\ufe54-\ufe57\ufe5f-\ufe61\ufe68\ufe6a\ufe6b\uff01-\uff03\uff05-\uff07\uff0a\uff0c\uff0e\uff0f\uff1a\uff1b\uff1f\uff20\uff3c\uff61\uff64\uff65]/,
            // jscs:enable

            maxLength: 6,
            validateRegex: /.{2,}/,
            validationErrors: [{
                property: 'completed',
                errorType: 'popup',
                errString: 'EDIT_PATIENT_QUESTION_2_EMPTY',
                header: 'EDIT_PATIENT_QUESTION_2_EMPTY_HEADER',
                ok: 'OK'
            }]
        }
    }, {
        id: 'EDIT_PATIENT_LANGUAGE',
        IG: 'Assignment',
        IT: 'Language',
        skipIT: '',
        title: 'EDIT_PATIENT_QUESTION_3_TITLE',
        text: ['EDIT_PATIENT_QUESTION_3'],
        className: '',
        widget: {
            id: 'EDIT_PATIENT_W_3',
            type: 'PatientLangListRadioButton',
            templates: {},
            answers: []
        }
    }, {
        id: 'EDIT_PATIENT_REASON_INFO',
        IG: 'Edit_Patient',
        IT: 'EDIT_PATIENT_REASON_INFO',
        title: '',
        text: 'EDIT_PATIENT_QUESTION_4_TITLE',
        className: 'EDIT_PATIENT_REASON_INFO'
    }, {
        id: 'EDIT_PATIENT_REASON',
        IG: 'Edit_Patient',
        IT: 'EDIT_PATIENT_REASON',
        title: '',
        text: 'EDIT_PATIENT_QUESTION_4',
        className: 'EDIT_PATIENT_REASON',
        widget: {
            id: 'EDIT_PATIENT_REASON',
            type: 'EditReason',
            className: 'EDIT_PATIENT_REASON',
            answers: [
                { text: 'EDIT_PATIENT_REASON_0', value: 'Correcting error by clinician' },
                { text: 'EDIT_PATIENT_REASON_1', value: 'Attribute data correction' },
                { text: 'EDIT_PATIENT_REASON_2', value: 'ID change' },
                { text: 'EDIT_PATIENT_REASON_3', value: 'Incorrect language selected' }
            ]
        }
    }],

    affidavits: [{
        id: 'EditPatientSignatureAffidavit',
        text: [
            'EDIT_PATIENT_AFFIDAVIT_HEADER',
            'EDIT_PATIENT_AFF'
        ],
        templates: { question: 'AffidavitText' },
        krSig: 'AddSubject',
        widget: {
            id: 'EDIT_PATIENT_SIGNATURE_AFFIDAVIT_WIDGET',
            className: 'AFFIDAVIT',
            type: 'SignatureBox',
            configuration: {
                sizeRatio: 3
            }
        }
    }],

    rules: [
        // EditPatientQuestionnaire
        // NOTE: This rule is required for SPA's Edit Patient workflow. Do not remove.
        // When the Edit_Patient questionnaire is rendered, the configured fields will be populated
        // based on the selected patient.  The EditPatientQuestionnaireView resolves the selected patient (this.subject)
        // and it's associated user (this.user) model.,
        // See the Edit_Patient questionnaire configuration for which fields to populate.
        {
            id: 'EditPatientQuestionnaire',
            trigger: 'QUESTIONNAIRE:Rendered/Edit_Patient',
            resolve: [{
                // This action populates answer records based on a configured model.
                action: 'populateFieldsByModel',
                data: [{
                    SW_Alias: 'PT.0.Patientid',
                    question_id: 'EDIT_PATIENT_ID',
                    questionnaire_id: 'Edit_Patient',
                    // Use the subject model scoped to the EditPatientQuestionnaireView (this.subject).
                    model: 'subject',
                    // Use the subject model's subject_id property to populate the response.
                    // e.g. this.subject.get('subject_id');
                    property: 'subject_id'
                }, {
                    SW_Alias: 'PT.0.Initials',
                    question_id: 'EDIT_PATIENT_INITIALS',
                    questionnaire_id: 'Edit_Patient',
                    // Use the subject model scoped to the EditPatientQuestionnaireView (this.subject).
                    model: 'subject',
                    // Use the subject model's initials property to populate the response.
                    // e.g. this.subject.get('initials');
                    property: 'initials'
                }, {
                    SW_Alias: 'Assignment.0.Language',
                    question_id: 'EDIT_PATIENT_LANGUAGE',
                    questionnaire_id: 'New_Patient',
                    // Use the user model scoped to the EditPatientQuestionnaireView (this.user).
                    model: 'user',
                    // Use the user model's language property to populate the response.
                    // e.g. this.user.get('language');
                    property: 'language'
                }]
            }]
        },

        // EditPatientDiaryComplete
        // NOTE: This rule is required for SPA's Edit Patient workflow. Do not remove.
        // This rule is triggered when attempting to navigate on the affidavit screen.
        // If the 'next' button was pressed, display a message, and save the patient edits.
        {
            id: 'EditPatientSave',
            trigger: 'QUESTIONNAIRE:Navigate/Edit_Patient/AFFIDAVIT',
            evaluate: 'isDirectionForward',
            // High importance so this rule runs before any others.
            salience: 3,
            resolve: [
                { action: 'displayMessage', data: 'PLEASE_WAIT' },
                { action: 'editPatientSave' }
            ]
        },

        // EditPatientQuestionnaireTimeout
        // NOTE: This rule is required for SPA's Edit Patient workflow. Do not remove.
        // This rule is triggered when a questionnaire timeout occurs in the diary.
        // If the 'next' button was pressed, display a message, and save the patient edits.
        {
            id: 'EditPatientQuestionnaireTimeout',
            trigger: 'QUESTIONNAIRE:QuestionnaireTimeout/Edit_Patient',
            evaluate: 'isAffidavitSigned',
            salience: 3,
            resolve: [{
                action: 'displayMessage',
                data: 'PLEASE_WAIT'
            }, {
                action: 'editPatientSave'
            }, {
                action: 'updateCurrentContext'
            }, {
                action: 'removeMessage'
            }, {
                action: 'navigateTo',
                data: 'home'
            }, {
                action: 'preventAll'
            }],
            reject: [{
                action: 'navigateTo',
                data: 'home'
            }, {
                action: 'notify',
                data: { key: 'DIARY_TIMEOUT' }
            }, {
                action: 'preventAll'
            }]
        },

        // EditPatientSessionTimeout
        // NOTE: This rule is required for SPA's Edit Patient workflow.
        {
            id: 'EditPatientSessionTimeout',
            trigger: 'QUESTIONNAIRE:SessionTimeout/Edit_Patient',
            evaluate: 'isAffidavitSigned',
            salience: 3,
            resolve: [{
                action: 'displayMessage',
                data: 'PLEASE_WAIT'
            }, {
                action: 'editPatientSave'
            }, {
                action: 'logout'
            }, {
                // Prevent all other rules and functionality from executing.
                // This prevents a dashbord record from being written to the database.
                action: 'preventAll'
            }]
        },

        // EditPatientDiarySync
        // NOTE: This rule is required for SPA's Edit Patient workflow. Do not remove.
        // This rule is triggered when attempting to navigate on the affidavit screen.
        // If the 'next' button was pressed and the device is online,
        // transmit all queued transmissions and sync subjects.
        {
            id: 'EditPatientDiarySync',
            trigger: 'QUESTIONNAIRE:Navigate/Edit_Patient/AFFIDAVIT',
            evaluate: ['AND', 'isOnline', 'isDirectionForward'],
            // Importance below that of EditPatientDiaryComplete, so this is triggered following it.
            salience: 2,
            resolve: [{
                action: function () {
                    return ELF.trigger('EditPatientDiarySync:Transmit', {}, this);
                }
            }]
        },

        // EditPatientDiaryNavigate
        // NOTE: This rule is required for SPA's Edit Patient workflow. Do not remove.
        // This rule is triggered when attempting to navigate on the affidavit screen.
        // If the 'next' button was pressed, update the current context,
        // and then navigate to the Subject list (HomeView) view.
        {
            id: 'EditPatientDiaryNavigate',
            trigger: 'QUESTIONNAIRE:Navigate/Edit_Patient/AFFIDAVIT',
            evaluate: 'isDirectionForward',
            salience: 1,
            resolve: [
                { action: 'updateCurrentContext' },
                { action: 'removeMessage' },
                { action: 'navigateTo', data: 'home' },
                { action: 'preventDefault' }
            ]
        },

        // EditPatientCancel
        // NOTE: This rule is required for SPA's Edit Patient workflow. Do not remove.
        // Navigates to the Subject list (HomeView) view after the questionnaire has been cancelled.
        {
            id: 'EditPatientCancel',
            trigger: 'QUESTIONNAIRE:Canceled/Edit_Patient',
            resolve: [
                { action: 'navigateTo', data: 'home' },
                { action: 'preventDefault' }
            ]
        }
    ]
};
