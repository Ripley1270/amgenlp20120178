// This is a core questionnaire required for the Time Confirmation workflow on the eCoa App.
// Please do not remove it, but configure it as needed.
// All resource strings referenced in this questionnaire are core (namespace:CORE) resources,
// but can be overwritten at either the questionnaire (namespace:Time_Confirmation)
// or study (namespace:STUDY) levels.
const year = new Date().getFullYear();

export default {
    questionnaires : [{
        id: 'Time_Confirmation',
        SU: 'TC',
        displayName: 'TIME_CONFIRMATION_HEADER',
        className: 'TIME_CONFIRMATION',
        affidavit: false,
        screens: ['TIME_CONFIRMATION_S_1']
    }],

    screens : [{
        id: 'TIME_CONFIRMATION_S_1',
        className: 'TIME_CONFIRMATION_S_1',
        disableBack: true,
        questions: [
            { id: 'TIME_CONFIRMATION_Q_1', mandatory: true },
            { id: 'TIME_CONFIRMATION_Q_2', mandatory: true }
        ]
    }],

    questions : [{
        id          : 'TIME_CONFIRMATION_Q_1',
        IG          : 'Time_Confirmation',
        IT          : 'DATE_TIME',
        text        : ['TIME_CONFIRMATION_INSTRUCTIONS'],
        className   : 'TIME_CONFIRMATION_Q_1',
        widget      : {
            id          : 'TIME_CONFIRMATION_W_1',
            type        : 'DateTimePicker',
            className   : 'TIME_CONFIRMATION_W_1',
            showLabels  : true,
            configuration: {
                defaultValue: `${year}-01-01`,
                timeConfiguration: { }
            }
        }
    }, {
        id          : 'TIME_CONFIRMATION_Q_2',
        IG          : 'Time_Confirmation',
        IT          : 'TIME_ZONE',
        skipIT      : '',
        title       : '',
        text        : [],
        className   : 'TIME_ZONE',
        widget      : {
            id          : 'TIME_CONFIRMATION_W_2',
            type        : 'SelectTimeZone',
            label       : 'SET_TIMEZONE_MESSAGE',
            field       : 'timezone',
            templates   : {},
            answers     : []
        }
    }],

    rules: [
        // TimeConfirmationComplete
        // NOTE: This rule is required for the Time Confirmation workflow.
        // After the user clicks 'Next' on the time confirmation screen, compare the
        // local time with the entered time.
        {
            id: 'TimeConfirmationComplete',
            trigger: 'QUESTIONNAIRE:Navigate/Time_Confirmation',
            // Only resolved on 'next' click.
            evaluate: 'isDirectionForward',
            salience: 1,
            resolve: [
                { action: 'timeSlipCheck', data: { isBlind: true } },
                { action: 'navigateTo', data: '' },
                // Prevent all other actions, such as navigating to the QuestionnaireCompletionView.
                { action: 'preventAll' }
            ]
        },
        // TimeConfirmationTimeouts
        // NOTE: This rule is required for the Time Confirmation workflow.
        // Session and questionnaire timeouts should not be run on this screen.
        {
            id: 'TimeConfirmationTimeouts',
            trigger: 'QUESTIONNAIRE:Rendered/Time_Confirmation',
            evaluate: true,
            resolve: [{
                action: () => {
                    LF.security.stopQuestionnaireTimeOut();
                    LF.security.pauseSessionTimeOut();
                }
            }]
        },
        // TimeConfirmationTimeouts
        // NOTE: This rule is required for the Time Confirmation workflow.
        // Because time confirmation can be required without a user logged in or in existence,
        // the role based screen authentication should not be done.
        {
            id: 'TimeConfirmationStopAuthentication',
            trigger: 'QUESTIONNAIRE:Before/Time_Confirmation',
            salience: 3,
            resolve: [{ action: () => ({ stopRules: true }) }]
        },
        // TimeConfirmationQuestionnaireOpened
        // Time confirmation should not prevent the restart triggered by time zone change
        {
            id: 'TimeConfirmationQuestionnaireOpened',
            trigger: 'QUESTIONNAIRE:Open/Time_Confirmation',
            salience: 1,
            resolve: [{ action: () => LF.Class.RestartManager.setDiaryActive(false) }]
        }
    ]
};
