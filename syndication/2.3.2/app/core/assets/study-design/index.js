// CORE FILES
import affidavits from './affidavits';
import visits from './visits';
import supportOptions from './support-options';
import diaries from './diaries';
import roles from './roles';
import timeZoneConfig from './time-zone-config';
import timeZoneWindowsConfig from './time-zone-windows-config';
import timeZoneIosConfig from './time-zone-ios-config';
import toolBoxConfig from './toolbox-config';
import securityQuestions from './security-questions-config';
import studyDesign from './study-design';
// CORE FILES

import {mergeObjects} from 'core/utilities/languageExtensions';

export default mergeObjects({}, studyDesign, affidavits, visits, supportOptions,
    diaries, roles, timeZoneConfig, timeZoneWindowsConfig, timeZoneIosConfig, toolBoxConfig, securityQuestions);
