/**
 * @fileOverview This is an example affidavits study configuration file.
 * @author <a href='mailto:bill.calderwood@ert.com'>William A. Calderwood</a>
 * @version 2.1
 */
export default {
    affidavits      : [

        // Do NOT remove this configuration. Please refer to Affidavit_Study_Design_1_4 for more information.
        {
            // This is the same as CheckBoxAffidavit
            id: 'DEFAULT',
            text: [
                'P_AFFIDAVIT_HEADER',
                'AFFIDAVIT_HELP',
                'AFFIDAVIT'
            ],
            templates: { question: 'AffidavitText' },
            krSig: 'AFFIDAVIT',
            widget: {
                id: 'AFFIDAVIT_WIDGET',
                className: 'CheckBox',
                type: 'CheckBox',
                answers: [{
                    text: 'OK',
                    value: '0'
                }]
            }
        },

        // Do NOT remove this configuration. Please refer to Affidavit_Study_Design_1_4 for more information.
        {
            id: 'CheckBoxAffidavit',
            text: [
                'P_AFFIDAVIT_HEADER',
                'AFFIDAVIT_HELP',
                'AFFIDAVIT'
            ],
            templates: { question: 'AffidavitText' },
            krSig: 'AFFIDAVIT',
            widget: {
                id: 'AFFIDAVIT_WIDGET',
                className: 'CheckBox',
                type: 'CheckBox',
                answers: [{
                    text: 'OK',
                    value: '0'
                }]
            }
        },

        // Do NOT remove this configuration. Please refer to Affidavit_Study_Design_1_4 for more information.
        {
            id: 'P_CheckBoxAffidavit',
            text: [
                'P_AFFIDAVIT_HEADER',
                'AFFIDAVIT_HELP',
                'AFFIDAVIT'
            ],
            templates: { question: 'AffidavitText' },
            krSig: 'AFFIDAVIT',
            widget: {
                id: 'AFFIDAVIT_WIDGET',
                className: 'CheckBox',
                type: 'CheckBox',
                answers: [{
                    text: 'OK',
                    value: '0'
                }]
            }
        }, {
            id: 'CustomAffidavit',
            text: [
                'P_AFFIDAVIT_HEADER',
                'AFFIDAVIT_HELP',
                'AFFIDAVIT'
            ],
            templates: { question: 'AffidavitText' },
            krSig: 'CustomAffidavit',
            widget: {
                id: 'CUSTOM_AFFIDAVIT_WIDGET',
                className: 'CheckBox',
                type: 'CheckBox',
                answers: [{
                    text: 'OK',
                    value: '0'
                }]
            }
        }, {
            id: 'SignatureAffidavit',
            text: [
                'P_AFFIDAVIT_HEADER',
                'SITE_REPORT_AFF'
            ],
            templates: { question: 'AffidavitText' },
            krSig: 'SubmitForm',
            widget: {
                id: 'SIGNATURE_AFFIDAVIT_WIDGET',
                className: 'AFFIDAVIT',
                type: 'SignatureBox'
            }
        }, {
            id: 'P_SignatureAffidavit',
            text: [
                'P_AFFIDAVIT_HEADER',
                'PATIENT_REPORT_AFF'
            ],
            templates: { question: 'AffidavitText' },
            krSig: 'SubmitForm',
            widget: {
                id: 'P_SIGNATURE_AFFIDAVIT_WIDGET',
                className: 'AFFIDAVIT',
                type: 'SignatureBox'
            }
        }, {
            id: 'FirstUserSignatureAffidavit',
            text: [
                'FIRST_USER_AFFIDAVIT'
            ],
            templates: { question: 'AffidavitText' },
            krSig: 'SubmitForm',
            widget: {
                id: 'P_SIGNATURE_AFFIDAVIT_WIDGET',
                className: 'AFFIDAVIT',
                type: 'SignatureBox'
            }
        }, {
            id: 'NewUserAffidavit',
            text: ['SITE_REPORT_AFF'],
            templates: { question: 'AffidavitText' },
            krSig: 'SubmitForm',
            widget: {
                id: 'SIGNATURE_AFFIDAVIT_WIDGET',
                className: 'AFFIDAVIT',
                type: 'SignatureBox'
            }
        }
    ]
};
