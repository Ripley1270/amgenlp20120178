import ArrayRef from 'core/classes/ArrayRef';

export default {
    ios: {
        timeZoneOptions: new ArrayRef([{
            tzId: 'America/New_York',
            swId: 21
        }])
    }
};
