/**
 * The list of study upgrade configurations.
 * You must either include custom function per version upgrade.
 * Supports use of callback or returning a promise.
 * @example
 * LF.StudyDBUpgrade = [{
 * 	   version: 1,
 *     upgradeFunction (callback) {
 * 	       callback(isCompleted);
 * 	   }
 * 	 }, {
 *     version: 10,
 *     upgradeFunction () {
 *       return Q()
 *           .then(...)
 *           .then(() => isCompleted);
 *     }
 * }];
 * @type {*[]}
 */
const StudyDBUpgrade = [{
    version: 1,
    upgradeFunction () {
        return Q(true);
    }
}];

export default StudyDBUpgrade;
LF.StudyDBUpgrade = StudyDBUpgrade;
