/**
 * The purpose of this file is to load all core and trial-specific runtime artifacts for
 * the trial.  First grabs all the core stuff needed for load (that isn't loaded by another means like a grunt task),
 * and then merges the resulting object with the contents of the "trial" directory.  trial/index.js should assemble the
 * object to merge.
 */

import 'core/alarm';
import assets from 'core/assets';
import 'core/branching';

import 'core/schedule';
import 'core/screenshot';

import 'core/widgets';

import trial from 'trial/index';
import {mergeObjects} from 'core/utilities/languageExtensions';

// split rules out into separate properties, so they can be executed individually along with the platform-specific rules
assets.coreRules = assets.studyRules;
assets.trialRules = trial.assets && trial.assets.studyRules ? trial.assets.studyRules : () => {};

delete assets.studyRules;
if (trial.assets) {
    delete trial.assets.studyRules;
}

export default mergeObjects({assets}, trial);
