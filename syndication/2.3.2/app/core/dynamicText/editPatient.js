/**
 * Get patient ID.
 */
export const getPatientIdNumber = {
    id: 'getPatientIdNumber',
    evaluate: () =>  {
        return LF.DynamicText.subject.get('subject_id');
    },
    screenshots: {
        values: ['100']
    }
};

/**
 *  Get patient initials.
 */
export const getPatientInitials = {
    id: 'getPatientInitials',
    evaluate: () =>  {
        return LF.DynamicText.subject.get('initials');
    },
    screenshots: {
        values: ['AAA']
    }
};