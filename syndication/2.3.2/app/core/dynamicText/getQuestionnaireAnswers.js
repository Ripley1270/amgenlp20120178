import Data from 'core/Data';
import Answers from 'core/collections/Answers';

/**
 * Create the rows for the table using the given question views.
 * @param {array} questionViews - Array of questionViews for the answered questions.
 * @returns {string} The string value of the created rows.
 */
const createRows = (questionViews) => {
    let retString = '';

    for (let i = 0, q = questionViews.length -1; i < q; i += 1) {
        retString += `<tr><td>Answer ${i + 1}</td><td>`;
        if (questionViews[i].widget) {
            let answers = questionViews[i].widget.answers;
            for (let j = 0; j < answers.length; j += 1) {
                retString += `<div>${questionViews[i].id}: ${answers.at(j).get('response')}</div>`;
            }
        }
        retString += '</td> </tr>';
    }

    return retString;
};

/**
 * Create the table from the given question views.
 * @param {array} questionViews - Array of questionViews for the answered questions.
 * @returns {string} The string value of the created table.
 */
const createTable = (questionViews) => {
    return `
        <table border="1" cellpadding="0,3">
            <tr> <th>Question</th> <th>Answers</th> </tr>
            ${createRows(questionViews)}
        </table>
    `;
};

/**
 * Makes the assumption that this is the last question, so doesn't show the answers
 * for the last question in the set. s
 */
export const getQuestionnaireAnswers = {
    id: 'getQuestionnaireAnswers',
    evaluate: () => createTable(Data.Questionnaire.questionViews),

    screenshots: {
        // Answers collection fails if constructed before database is set up.
        //  This cannot use the static "values".
        getValues: () => {
            const createQuestion = (i, response) => {
                return {
                    id: `ScreenshotTest_${i}`,
                    widget: {
                        answers: new Answers([{ response }])
                    }
                };
            };

            return [createTable([
                createQuestion(1, 'first answer'),
                createQuestion(2, 'second answer'),
                // An empty question is needed because in the real app the screen calling this is the last question.
                {}
            ])];
        }
    }
};

