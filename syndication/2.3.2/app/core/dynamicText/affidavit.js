import Data from 'core/Data';
import Logger from 'core/Logger';

let logger = new Logger('DynamicText.affidavit');

export const getParticipantCodeForSubject = {
    id: 'getParticipantCodeForSubject',
    evaluate: () => {
        return LF.DynamicText.subject.get('subject_number');
    },
    screenshots: {
        values: ['100']
    }
};

export const getSiteCodeForSubject = {
    id: 'getSiteCodeForSubject',
    evaluate: () => {
        return LF.DynamicText.subject.get('site_code');
    },
    screenshots: {
        values: ['101']
    }
};

export const showPatientInitials = {
    id: 'showPatientInitials',
    evaluate: () => {
        let initials = LF.DynamicText.subject.get('subject_number');
        return initials != null && initials !== '---' ? 'show' : 'show';
    },
    screenshots: {
        values: ['AAA']
    }
};

export const getActiveUserName = {
    id: 'getActiveUserName',
    evaluate: () =>  {
        return Q().then(() => {
            if (LF && LF.security && LF.security.activeUser) {
                switch (LF.security.activeUser.get('userType')) {
                    case 'Subject':
                        // Per DE16341 subject initial
                        return LF.DynamicText.subject.get('initials');
                    default:
                        return LF.security.activeUser.get('username');
                }
            } else {
                logger.error('Attempt to get active username without active user session.');
                return null;
            }
        }).then((authorName) => {
            return authorName || '---';
        });
    },
    screenshots: {
        values: ['John Doe']
    }
};

export const getSelectedUsername = {
    id: 'getSelectedUsername',
    evaluate: () =>  {
        return LF.DynamicText.selectedUser.get('username') || '---';
    },
    screenshots: {
        values: ['johnDoe']
    }
};

export const getFirstUsername = {
    id: 'getFirstUsername',
    evaluate: () =>  {
        return Q().then(() => {
            let requiredResponses = _.invoke(Data.Questionnaire.answers.models, 'get', 'response')
                .toString().match(/\{("username"):.+?\}/g);

            let parsedResponses = _.map(requiredResponses, (res) => JSON.parse(res)),
                answers = _.extend.apply(Object, _.filter(parsedResponses, (res) => _.isObject(res)));

            return answers.username;
        }).then((authorName) => {
            return authorName || '---';
        });
    },
    screenshots: {
        values: ['johnDoe']
    }
};