/**
 * Get participant code.
 */
export const getParticipantCode = {
    id: 'getParticipantCode',
    evaluate: () =>  {
        return LF.DynamicText.subject.get('subject_number');
    },
    screenshots: {
        values: ['1']
    }
};