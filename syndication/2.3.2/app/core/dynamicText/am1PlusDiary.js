import * as Utilities from 'core/utilities';

/**
 * Get the date of the latest AM1 Plus record retrieved, for AM1PLUS_Q2 from eSense Diary.
 */
export const getLatestAM1PlusRecordDate = {
    id: 'getLatestAM1PlusRecordDate',
    evaluate: () =>  {
        let time,
            timeString = '',
            record = localStorage.getItem('PREVIOUS_LATEST_AM1PLUS_RECORD'),
            pad = Utilities.zeroPad;

        if (!!record) {
            record = JSON.parse(record);
            time = new Date(record.TIME);
            timeString = `${pad(time.getMonth() + 1, 2)}/${pad(time.getDate(), 2)}/${time.getFullYear()} ${pad(time.getHours(), 2)}:${pad(time.getMinutes(), 2)}`;
        }

        return timeString;
    },
    screenshots: {
        values: ['01/01/2017 12:00']
    }
};

/**
 * Get the appropriate string for the explanation of displayed records, for AM1PLUS_Q2 from eSense Diary.
 */
export const getDisplayExplanation = {
    id: 'getDisplayExplanation',
    evaluate: () =>  {
        let stringsToFetch,
            buttonId = localStorage.getItem('AM1PLUS_CLICKED_BUTTON');

        localStorage.removeItem('AM1PLUS_CLICKED_BUTTON');

        if (!!buttonId) {
            stringsToFetch = {
                explanation: {
                    namespace: 'P_AM1PlusDiary',
                    key: `QUESTION_2_${buttonId}`
                }
            };

            return LF.getStrings(stringsToFetch)
                .then(strings => {
                    return strings.explanation;
                });
        } else {
            return '';
        }
    },
    screenshots: {
        getValues: () => {
            const button = (buttonId) => {
                return {
                    namespace: 'P_AM1PlusDiary',
                    key: `QUESTION_2_${buttonId}`
                };
            };

            return LF.getStrings({
                buttonx: button('X'),
                button1: button(1),
                button2: button(2),
                button3: button(3)
            })
            .then(strings => [strings.buttonx, strings.button1, strings.button2, strings.button3]);
        }
    }
};

/**
 * Get the name of the paired 'PEF' type eSense device name.
 */
export const eSenseDeviceName = {
    id: 'eSenseDeviceName',
    evaluate: () => 'PEF',
    screenshots: {
        values: ['PEF']
    }
};

/**
 * Get the serial number of the paired 'PEF' type eSense device name.
 */
export const eSenseSerialNumber = {
    id: 'eSenseSerialNumber',
    evaluate: () =>  {
        const getPairedDevice = Q.Promise(resolve => LF.Wrapper.Utils.eSense.getPairedDevice('PEF', resolve));

        return getPairedDevice()
            .then(device => {
                let serial = !!device ? device.get('serial') :  '';
                return serial;
            });
    },
    screenshots: {
        values: ['1']
    }
};

/**
 * Get the serial number of the paired AM1 device.
 */
export const getAM1SerialNumber = {
    id: 'getAM1SerialNumber',
    evaluate: () =>  {
        return LF.Data.Questionnaire.serialNumber;
    },
    screenshots: {
        values: ['1']
    }
};