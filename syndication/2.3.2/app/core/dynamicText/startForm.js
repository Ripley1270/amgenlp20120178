/**
 * Get the name of the report that is about to be started.
 */
export const formTitle = {
    id: 'formTitle',
    evaluate: () =>  {
        return localStorage.getItem('START_FORM_TITLE');
    },
    screenshots: {
        values: ['Form Title']
    }
};

/**
 * Get the patient id or role name.
 */
export const patientOrRole = {
    id: 'patientOrRole',
    evaluate: () =>  {
        return localStorage.getItem('START_FORM_SUBJECT_OR_ROLE');
    },
    screenshots: {
        values: ['100', 'site']
    }
};