/**
 * Get the countdown text for the restart.
 */
export const timeZoneChangeRestartCountdown = {
    id: 'timeZoneChangeRestartCountdown',
    evaluate: () =>  {
        let countdown = LF.StudyDesign.timeZoneChangeRestartCountdown || LF.CoreSettings.timeZoneChangeRestartCountdown,
            countdownInSeconds = countdown / 1000,
            stringsToFetch = { duration: 'SECONDS' };

        return LF.getStrings(stringsToFetch)
        .then((strings) => {
            return `${countdownInSeconds} ${strings.duration}`;
        });
    },
    screenshots: {
        getValues: () => {
            return LF.getStrings('SECONDS')
            .then(string => `10 ${string}`);
        }
    }
};
