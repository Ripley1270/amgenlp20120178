/**
 * Get the image folder location.
 */
export const imageFolder = {
    id: 'imageFolder',
    evaluate: () => 'trial/images/',
    screenshots: {
        values: ['trial/images/']
    }
};
