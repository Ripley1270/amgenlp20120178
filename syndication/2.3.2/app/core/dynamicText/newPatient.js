import Data from 'core/Data';
import * as Helpers from 'core/Helpers';

/**
 *  Gets the site name for display in a question template.
 *  This assumes that the current site will be in LF.Data.
 */
export const getSiteCode = {
    id: 'getSiteCode',
    evaluate: () =>  {
        return Data.site ? Data.site.get('siteCode') : '';
    },
    screenshots: {
        values: ['101']
    }
};

/**
 *  Retrieves the patient ID from the questionnaire (ADD_PATIENT_ID).
 */
export const getPatientID = {
    id: 'getPatientID',
    evaluate: () =>  {
        return Helpers.getResponses('ADD_PATIENT_ID')[0].response;
    },
    screenshots: {
        values: ['1']
    }
};

/**
 *  Returns the user name of the current user for screen display.
 */
export const getUserName = {
    id: 'getUserName',
    evaluate: () =>  {
        return LF.security.activeUser.get('username');
    },
    screenshots: {
        values: ['John Doe']
    }
};