/**
 * Get the name of selected time zone.
 */
export const selectedTimeZone = {
    id: 'selectedTimeZone',
    evaluate: () => LF.DynamicText.selectedTZName,
    screenshots: {
        values: ['Africa/Abidjan', 'America/New_York']
    }
};