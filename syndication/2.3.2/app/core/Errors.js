/** #depends application.js
 * @fileOverview Set of error codes
 * @author <a href="mailto:gsaricali@phtcorp.com">Gulden Saricali</a>
 * @version 1.4
 */

/**
 * Web-Service error object.
 * @memberof LF
 * @type {Object}
 */
LF.ServiceErr = {

    /**
     * The web service error that is received when the security question answer is incorrect.
     * @type String
     * @readonly
     * @default '2'
     */
    INCORRECT_SECURITY_ANSWER: '2',

    /**
     * The web service error for server error.
     * @type String
     * @readonly
     * @default '5'
     */
    SERVER_ERROR: '5',

    /**
     * The web service error that is received when the subject is not found.
     * @type String
     * @readonly
     * @default '6'
     */
    SUBJECT_NOT_FOUND: '6',

    /**
     * The HTTP error for timeout.
     * @type Number
     * @readonly
     * @default 0
     */
    HTTP_TIMEOUT: 0,
    /**
     * The HTTP error for bad request like improperly formatted header, JSON Parse error.
     * @type Number
     * @readonly
     * @default 400
     */
    HTTP_BAD_REQUEST: 400,

    /**
     * The HTTP error for unauthorized request.
     * @type Number
     * @readonly
     * @default 401
     */
    HTTP_UNAUTHORIZED: 401,

    /**
     * The HTTP error for forbidden request.
     * @type Number
     * @readonly
     * @default 403
     */
    HTTP_FORBIDDEN: 403,

    /**
     * The HTTP error for not found resources in server or internet connection error.
     * @type Number
     * @readonly
     * @default 404
     */
    HTTP_NOT_FOUND: 404,

    /**
     * The HTTP error for methods not allowed on a resource .
     * @type Number
     * @readonly
     * @default 405
     */
    HTTP_METHOD_NOT_ALLOWED: 405,

    /**
     * The HTTP error for server error.
     * @type Number
     * @readonly
     * @default 500
     */
    HTTP_SERVER_ERROR: 500

};