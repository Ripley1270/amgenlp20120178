import ConfirmView from './views/ConfirmView';
import { getGlyphicon } from 'core/utilities';
import Logger from 'core/Logger';

let logger = new Logger('Notify');
let cache;

const requiredParam = param => logger.warn(`Missing param ${param}`);

/**
 * Registry for Messages. Any dialog or banner registered here will be available to display and also for the screenshot tool.
 * @type {Object}
 */
export const MessageRepo = {
    messages: {},
    types: [],

    /**
     * Display the dialog by passing an ID.
     * @param  {string} id String value of the key used to register the message.
     * @param  {[Object]} options Runtime options to pass to the registered message function.
     * @return {Promise<any>} resolves based on registered message.
     */
    display (id, options = {}) {
        if (!id) {
            logger.error('ID not provided. Cannot display message');
            return Q();
        }

        const message = MessageRepo.messages[id];

        if (!message) {
            logger.error(`No message found with id: ${id}`);
            return Q();
        }

        return Q().then(() => {
            return message(options);
        });
    },

    /**
     * Adds any number of messages to the registry.
     * @param {[object]} messages Messages to add to the registry.
     * @example MessageRepo.add(messageObj1, messageObj2, messageObj3);
     */
    add (...messages) {
        messages.forEach(MessageRepo.addToRepo);
    },

    /**
     * Function called that renders the message.
     * @typedef {function} NotificationMessage
     * @param {Object} options - options
     * @param {function} options.afterShow - message function must call the afterShow callback if it exists in order for the screenshot tool to work.
     */

    /**
     * Add a single message to the registry.
     * @param {object}   options options
     * @param {string}   options.type The type of message to add (Dialog, Banner, etc.).
     * @param {NotificationMessage} options.message The function to run to display the dialog.
     * @param {string}   options.key The key to store the message under.
     * @param {[string]} options.id The unique value to represent the message in the registry. Defaults to the same as "params.key"
     */
    addToRepo ({
        type = requiredParam('type'),
        key = requiredParam('key'),
        message = requiredParam('message'),
        id
    }) {
        if (!type || !key || !message) {
            return;
        }

        const value = id || key;
        const registeredMessage = MessageRepo.messages[value];

        if (registeredMessage) {
            logger.warn(`Duplicate message key used: ${value}. Overwriting entry.`);
        }

        MessageRepo.messages = _.extend({}, MessageRepo.messages, {
            [value]: message
        });

        MessageRepo[type] = _.extend({}, MessageRepo[type], {
            [key]: value
        });

        if (!_.contains(MessageRepo.types, type)) {
            MessageRepo.types.push(type);
        }
    },

    /**
     * Removes any number of messages from the registry.
     * @param {[object]} messages Messages to remove from the registry.
     * @example MessageRepo.remove(messageObj1, messageObj2, messageObj3);
     */
    remove (...messages) {
        messages.forEach(MessageRepo.removeFromRepo);
    },

    /**
     * Remove a single message from the registry.
     * @param {object} options options
     * @param {string} options.type The type of message to remove (Dialog, Banner, etc.).
     * @param {string} options.key The key the message is stored under.
     */
    removeFromRepo ({
        type = requiredParam('type'),
        key = requiredParam('key')
    }) {
        if (!type || !key) {
            return;
        }

        MessageRepo[type][key] = null;
        MessageRepo.messages[key] = null;
    },

    /**
     * Clear the MessageRepo
     */
    clear () {
        _.each(MessageRepo.types, type => {
            MessageRepo[type] = null;
        });
        MessageRepo.types = [];
        MessageRepo.messages = {};
    }
};

/**
 * Holds all banner notification functions.
 * @namespace
 */
LF.Notify.Banner = ((LF && LF.Notify.Banner) || {

    /**
     * Creates and displays a banner notification.
     * @param {Object} config is a noty configuration object.
     * @returns {String} id of given notification element.
     * @example var error = LF.Notify.Banner.show({
     *     text: 'Some error',
     *     type: 'error'
     * }); // "noty_error_1343321605126"
     */
    show: function (config) {
        //Workaround for jquery defect on ios with the banner staying
        //in the middle of the screen in some cases
        // @todo fix.
        //$.mobile.silentScroll(0);
        let span = config.type ? getGlyphicon(config.type)[0] || '' : '';

        return noty(_({
            timeout:  5000,
            dismissQueue:  false,
            callback:  {
                onClose: () => {
                    $.noty.clearQueue();
                },
                afterShow: () => {
                    (config.afterShow || $.noop)();
                }
            },
            template: `<div class="noty_message">
                    ${span}<span class="noty_text"></span>
                    <div class="noty_close"></div>
                </div>`,
            closeWith:  ['click', 'tap']
        }).extend(_.omit(config, 'afterShow')));
    },

    /**
     * Display a success (green) banner.
     * @param {string} key The translation key of the message to display.
     * @param {object} options  extra options to pass into noty
     * @returns {Q.Promise<string>} id of the notification.
     * @example Banner.success('Password changed.');
     */
    success: function (key, options = {}) {
        return LF.strings.display.call(LF.strings, key).then(text => {
            return this.show(_.extend(options, { text, type: 'success' }));
        });
    },

    /**
     * Display an error (red) banner.
     * @param {string} key The translation key of the message to display.
     * @param {object} options  extra options to pass into noty
     * @returns {Q.Promise<string>} id of the notification.
     * @example Banner.error('Illegal access attempt.');
     */
    error: function (key, options = {}) {
        return LF.strings.display.call(LF.strings, key).then(text => {
            return this.show(_.extend(options, { text, type: 'error' }));
        });
    },

    /**
     * Display a warning (yellow) banner.
     * @param {string} key The translation key of the message to display.
     * @param {object} options  extra options to pass into noty
     * @returns {Q.Promise<string>} id of the notification.
     * @example Banner.warn('Login required.');
     */
    warn: function (key, options = {}) {
        return LF.strings.display.call(LF.strings, key).then(text => {
            return this.show(_.extend(options, { text, type: 'warning' }));
        });
    },

    /**
     * Display a alert (white) banner.
     * @param {string} key The translation key of the message to display.
     * @param {object} options  extra options to pass into noty
     * @returns {Q.Promise<string>} id of the notification.
     * @example Banner.alert('Hello World!');
     */
    alert: function (key, options = {}) {
        return LF.strings.display.call(LF.strings, key).then(text => {
            return this.show(_.extend(options, { text, type: 'alert' }));
        });
    },

    /**
     * Display a information (blue) banner.
     * @param {string} key The translation key of the message to display.
     * @param {object} options  extra options to pass into noty
     * @returns {Q.Promise<string>} id of the notification.
     * @example Banner.info('Hello World!');
     */
    info: function (key, options = {}) {
        return LF.strings.display.call(LF.strings, key).then(text => {
            return this.show(_.extend(options, { text, type: 'information' }));
        });
    },

    /**
     * Flash a banner on the next page display.
     * @param {string} type The type of banner to display.
     * @param {string} key The translation key of the message to display.
     * @example Banner.flash('warn', 'Login required.');
     */
    flash: function (type, key) {
        cache = { type, key };
    },

    /**
     * Execute and clear the flash cache, if any.
     * @returns {Q.Promise}
     * @example Banner.flush();
     */
    flush: function () {
        if (cache != null) {
            return Q.Promise(resolve => {
                this[cache.type](cache.key, { afterShow: resolve });
                cache = null;
            });
        }
        return Q();
    },

    /**
     * Closes the notification banner of the given id.
     * @param {String} notifyId is the id of the banner to close.
     * @example LF.Notify.Banner.close("noty_error_1343321605126");
     */
    close: function (notifyId) {
        $.noty && $.noty.close(notifyId);
    },

    /**
     * Closes all banner notifications.
     * @example LF.Notify.Banner.closeAll();
     */
    closeAll: function () {
        $.noty && $.noty.closeAll();
    }

});

export let Banner = LF.Notify.Banner;

/**
 * Holds all dialog notification functions.
 * @namespace
 */
export let Dialog = {

    show: function (config) {
        let modal = new ConfirmView();

        return modal.show(config);
    }

};

LF.Notify.Dialog = Dialog;

/**
 * Holds all fullscreen notification functions.
 * @namespace
 */
export let Fullscreen = {

    /**
     * Displays a fullscreen notification
     * @param {String} message is a key for a localized string to be displayed in the alert.
     * @example LF.Notify.Fullscreen.show('PORTRAIT_ONLY');
     */
    show: function (message) {

        LF.Notify.Banner.closeAll();
        $('#application').hide();
        $('.ui-simpledialog-container').hide();
        $('.ui-simpledialog-screen').hide();

        LF.getStrings(message, (string) => {
            $('.fullscreen-alert').css('display', 'block').find('h1').html(string);
        });

    },

    /**
     * Closes the current fullscreen notification.
     * @example LF.Notify.Fullscreen.close();
     */
    close: function () {
        $('#application').show();
        $('.ui-simpledialog-container').show();
        $('.ui-simpledialog-screen').show();
        $('.fullscreen-alert').css('display', 'none');
    }

};

LF.Notify.Fullscreen = Fullscreen;
