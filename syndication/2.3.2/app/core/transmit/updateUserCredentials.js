import Logger from 'core/Logger';

const logger = new Logger('Transmit.updateUserCredentials');

/**
 * Handles updateUserCredentials transmission for non-subject users.
 * @param  {Object} transmissionItem - The item in the transmission queue to send.
 * @param {Function} [callback] - The callback function to called upon completion.
 * @return {Q.Promise<Object>} Q Promise
 */
export default function updateUserCredentials (transmissionItem, callback = $.noop) {
    let success = (res, data) => {
        logger.trace(`updateUserCredentials -> SUCCESS: ${res}`);
        if (res === 'E') {
            this.remove(transmissionItem);

            logger.error('Error updating credentials for user');
        } else {
            this.destroy(transmissionItem.get('id'));
        }

        callback(data);
        return Q(data);
    };

    let error = (errorCode) => {
        this.remove(transmissionItem);

        errorCode && logger.error('User credentials change failed with error code: {{errorCode}}', {errorCode});

        callback(false);
        return Q.reject();
    };

    return Q(JSON.parse(transmissionItem.get('params')))
    .catch(e => {
        // Something went wrong building transmission. Remove transmission from queue and collection.
        this.destroy(transmissionItem.get('id'));

        logger.error('transmissionItem.params is not valid JSON!', e);
        return Q.reject();
    })
    .then(transmitData => LF.studyWebService.updateUserCredentials(transmitData))
    .then((res, data) => success(res, data))
    .catch((res) => error(res.errorCode));
}
