import COOL from 'core/COOL';
import CurrentSubject from 'core/classes/CurrentSubject';
import ELF from 'core/ELF';
import Subjects from 'core/collections/Subjects';
import { MessageRepo } from 'core/Notify';
import WebService from 'core/classes/WebService';
import Spinner from 'core/Spinner';

/**
 * Handles the resetPassword transmission to the web-service.
 * @param {Object} transmissionItem - The item in the transmission queue to send.
 * @param {Function} [callback] - The callback function to called upon completion.
 */
export default function resetPassword (transmissionItem, callback = $.noop) {
    let resetPasswordSuccess,
        resetPasswordError,
        params = JSON.parse(transmissionItem.get('params')),
        mappedJSON = {
            W: params.password,
            Q: params.secret_question,
            A: params.secret_answer
        };

    Subjects.getSubjectBy({ krpt: params.krpt })
    .then(subject => {
        // eslint-disable-next-line no-unused-vars
        resetPasswordSuccess = (res, { syncID, isSubjectActive }) => {
            subject.save({
                service_password: res.W,
                subject_active: isSubjectActive
            })
            .then(() => {
                CurrentSubject.clearSubject();

                return this.destroy(transmissionItem.get('id'), callback);
            })
            .done();
        };

        resetPasswordError = (errorCode, httpCode, isSubjectActive) => {
            const { Dialog } = MessageRepo;

            if (httpCode === LF.ServiceErr.HTTP_FORBIDDEN && errorCode === LF.ServiceErr.SUBJECT_NOT_FOUND) {
                // Delete clinical data
                ELF.trigger('Termination', {
                    endParticipation: true,
                    subjectActive: !!isSubjectActive,
                    deviceID: subject.get('device_id')
                }, this)
                .finally(() => {
                    return this.destroy(transmissionItem.get('id'), callback);
                })
                .done();
            } else if (!isSubjectActive) {
                Spinner.hide()
                .then(() => {
                    LF.Actions.notify({
                        dialog: Dialog && Dialog.PASSWORD_CHANGE_FAILED
                    }, () => {
                        Spinner.show();
                        this.destroy(transmissionItem.get('id'), callback);
                    });
                })
                .done();
            } else if (httpCode !== LF.ServiceErr.HTTP_NOT_FOUND) {
                LF.spinner.hide().then(() => {
                    errorCode = errorCode ? `-${errorCode}` : '';

                    LF.Actions.notify({
                        dialog: Dialog && Dialog.TRANSMISSION_ERROR_HTTP_AND_ERROR,
                        options: { httpCode, errorCode }
                    }, () => {
                        LF.spinner.show();
                        this.remove(transmissionItem);
                        callback();
                    });
                }).done();
            } else {
                this.remove(transmissionItem);
                callback();
            }
        };

        let service = COOL.new('WebService', WebService);

        service.updateSubjectData(subject.get('device_id'), mappedJSON, null, resetPasswordSuccess, resetPasswordError);
    })
    .done();
}