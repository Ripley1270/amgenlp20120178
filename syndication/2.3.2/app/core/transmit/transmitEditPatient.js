import COOL from 'core/COOL';
import ELF from 'core/ELF';
import Logger from 'core/Logger';
import WebService from 'core/classes/WebService';
import { MessageRepo } from 'core/Notify';
import Spinner from 'core/Spinner';

const logger = new Logger('Transmit.transmitEditPatient');

/**
 * Handles the transmitEditPatient transmission to the web-service.
 * @param {Object} transmissionItem - The item in the transmission queue to send.
 * @param {Function} [callback] - The callback function to called upon completion.
 */
export default function transmitEditPatient (transmissionItem, callback = $.noop) {
    let params = JSON.parse(transmissionItem.get('params')),
        editPatientJSON = {
            K: params.krpt,
            M: params.ResponsibleParty,
            U: 'Assignment',
            S: params.dateStarted,
            C: params.dateCompleted,
            R: params.reportDate,
            P: params.phase,
            E: LF.coreVersion,
            V: LF.StudyDesign.studyVersion,
            T: params.phaseStartDate,
            L: params.batteryLevel,
            J: params.sigID
        },
        sendAssignmentSuccess = (res) => {
            logger.operational(`EditPatient transmitted: res: ${res}`);
            this.destroy(transmissionItem.get('id'))
                .then(callback);
        },
        // eslint-disable-next-line consistent-return
        sendAssignmentError = (errorCode, httpCode) => {
            logger.trace(`transmitEditPatient: EditPatientError: removing transmissionItem errorCode: ${errorCode} httpCode: ${httpCode}`);
            if (errorCode.errorCode === '19') {
                let responseKrpt = JSON.parse(transmissionItem.attributes.params).krpt;

                return ELF.trigger('TRANSMIT:Duplicate/Subject', {
                    krpt: responseKrpt
                }, this)
                .then(() => {
                    const { Dialog } = MessageRepo;

                    logger.info(`Transmisson for Edit Subject failed. Subject ${LF.DynamicText.duplicateSubjectId} already exists`);
                    Spinner.hide();
                    MessageRepo.display(Dialog && Dialog.SUBJECT_ALREADY_EXISTS_ERROR)
                    .then(() => {
                        Spinner.show();
                        transmissionItem.save({ status: 'failed'}). then(() => {
                            this.remove(transmissionItem);
                            callback();
                        });
                    });
                });
            } else {
                this.remove(transmissionItem);
                callback();
            }

        };

    logger.traceEnter('transmitEditPatient');

    editPatientJSON.A = _.map(params.answers, (answer) => {
        return {
            G: answer.response,
            F: answer.SW_Alias,
            Q: answer.question_id
        };
    });

    if (!!params.ink) {
        editPatientJSON.N = {
            D: params.ink.signatureData,
            X: params.ink.xSize,
            Y: params.ink.ySize,
            H: params.ink.author
        };
    }

    //compress answers collection with JSONH
    if (LF.StudyDesign.jsonh !== false) {
        editPatientJSON.A = JSONH.pack(editPatientJSON.A);
        logger.trace('Remapped after JSONH.pack');
    }

    let service = COOL.new('WebService', WebService);

    service.sendEditPatient(editPatientJSON, '')
        .then((res) => {
            sendAssignmentSuccess(res);
        })
        .catch((e) => {
            sendAssignmentError(e);
        })
        .done();

    logger.traceExit('transmitEditPatient');
}
