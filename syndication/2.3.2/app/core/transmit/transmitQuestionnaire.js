import COOL from 'core/COOL';
import Logger from 'core/Logger';
import * as lStorage from 'core/lStorage';
import Subjects from 'core/collections/Subjects';
import Dashboards from 'core/collections/Dashboards';
import Answers from 'core/collections/Answers';
import WebService from 'core/classes/WebService';

const logger = new Logger('Transmit.transmitQuestionnaire');

let dataCache = null;

/**
 * Bundles a questionnaire to be transmitted to the Web-Service.
 * @param {Object} transmissionItem - The item in the transmission queue to send.
 * @param {Function} [callback] - The callback function to called upon completion.
 */
export default function transmitQuestionnaire (transmissionItem, callback = $.noop) {
    // Boolean used to determine if the device is a sitepad app.
    let isSitePad = !!lStorage.getItem('apiToken');

    let subjectKrpt,
        diaryJSON = {},
        params = JSON.parse(transmissionItem.get('params'));

    let resume = () => {
        let isLastItem = !this.filter((item) => item.get('method') === 'transmitQuestionnaire').length;

        if (isLastItem) {
            dataCache = null;
        }

        callback();
    };

    // eslint-disable-next-line no-unused-vars
    let sendDiarySuccess = (res, { syncID, isSubjectActive, isDuplicate }) => {
        logger.operational(`Diary transmitted: ${diaryJSON.SU} with Dashboard ID: ${params.dashboardId}, sigID: ${diaryJSON.sig_id}`);

        if (isDuplicate) {
            logger.error(`Duplicate transmission: ${diaryJSON.SU} with Dashboard ID: ${params.dashboardId}, sigID: ${diaryJSON.sig_id}`);
        }

        // Destroy the corresponding dashboard and answers records
        logger.trace(`transmitQuestionnaire: sendDiarySuccess: about delete item from database ${dataCache.dashboards.get(diaryJSON.id)}`);
        Backbone.sync('delete', dataCache.dashboards.get(diaryJSON.id));

        _.each(diaryJSON.Answers, (answer) => {
            logger.trace(`transmitQuestionnaire: sendDiarySuccess: _each: delete answer ${dataCache.answers.get(answer.id)}`);
            Backbone.sync('delete', dataCache.answers.get(answer.id));
        });

        this.destroy(transmissionItem.get('id'), $.noop);

        if (!isSubjectActive) {
            logger.trace('transmitQuestionnaire: sendDiarySuccess: !isSubjectActive saving subject not active');

            Subjects.getSubjectBy({ krpt: subjectKrpt })
            .then((subject) => {
                subject.save({
                    subject_active: 0
                }, {
                    onSuccess: resume,
                    onError: resume
                });
            })
            .done();
        } else {
            resume();
        }
    };

    let sendDiaryError = () => {
        logger.trace('transmitQuestionnaire: sendDiaryError: removing transmissionItem');
        this.remove(transmissionItem);
        resume();
    };

    let processData = () => {
        let mappedJSON = {
                I: diaryJSON.diary_id,
                U: diaryJSON.SU,
                S: diaryJSON.started,
                C: diaryJSON.completed,
                R: diaryJSON.report_date,
                O: diaryJSON.completed_tz_offset,
                P: diaryJSON.phase,
                T: diaryJSON.phaseStartDateTZOffset,
                B: diaryJSON.change_phase,
                D: diaryJSON.device_id,
                E: diaryJSON.core_version,
                V: diaryJSON.study_version,
                L: diaryJSON.battery_level,
                J: diaryJSON.sig_id
            },
            useJSONH = (LF.StudyDesign.jsonh !== false);

        // If the device is a SitePad App, we need to modify the JSON.
        if (isSitePad) {
            mappedJSON.K = diaryJSON.krpt;
            mappedJSON.M = diaryJSON.responsibleParty;
        }

        // TODO: It would be better to put krpt into questionnaire transmission paramas
        subjectKrpt = diaryJSON.krpt || localStorage.getItem('krpt');

        logger.traceEnter('transmitQuesionnaire: processData');

        mappedJSON.A = _.map(diaryJSON.Answers, (answer) => {
            let answerPayload = {
                G: answer.response,
                F: answer.SW_Alias,
                Q: answer.question_id
            };

            if (answer.type) {
                answerPayload.T = answer.type;
                useJSONH = false;
            }

            return answerPayload;
        });

        logger.trace(`Setting mappedJSON.A: ${mappedJSON.A}`);

        if (!!diaryJSON.ink) {
            let ink = JSON.parse(diaryJSON.ink);
            mappedJSON.N = {
                D: ink.signatureData,
                X: ink.xSize,
                Y: ink.ySize,
                H: ink.author
            };
        }

        // compress answers collection with JSONH
        if (useJSONH) {
            mappedJSON.A = JSONH.pack(mappedJSON.A);
            logger.trace('Remapped after JSONH.pack');
        }

        let service = COOL.new('WebService', WebService);

        service.sendDiary(mappedJSON, transmissionItem.get('token'), useJSONH, sendDiarySuccess, sendDiaryError);
        logger.traceExit('transmitQuesionnaire: processData');
    };
    
    let fetchData = () => {
        // Find the correct dashboard record to transmit.
        let dashboardRecord = _.where(dataCache.dashboards.toJSON(), {
            id: params.dashboardId
        })[0];

        logger.trace(`transmitQuestionnaire: fetchData: dashboardRecord: ${dashboardRecord}`);

        if (dashboardRecord) {

            // Find the answer records that belong to the dashboard item.
            let diaryAnswers = _.where(dataCache.answers.toJSON(), {
                questionnaire_id: dashboardRecord.questionnaire_id,
                instance_ordinal: dashboardRecord.instance_ordinal
            });

            // If there are no answers...
            if (diaryAnswers.length === 0) {
                // StudyWorks will fail if "A" is []
                logger.error('transmitQuestionnaire failed to find answers');
            }

            // Copy the dashboard record data and the diary answers to the diaryJSON object
            _.extend(diaryJSON, dashboardRecord);
            diaryJSON.Answers = diaryAnswers;
            processData();
        } else {
            logger.error(`Dashboard record does not exist. Dashboard ID: ${params.dashboardId}, sigID: ${params.sigId}`);
            logger.trace('transmitQuestionnaire: no dashboard record, destroying transmission item');
            this.destroy(transmissionItem.get('id'), resume);
        }
    };

    // Optimize transmissions by avoiding having to decrypt the database for every transmission item.
    if (!dataCache) {
        dataCache = {};
        dataCache.dashboards = new Dashboards();
        dataCache.dashboards.fetch({
            onSuccess: () => {
                dataCache.answers = new Answers();
                dataCache.answers.fetch({
                    onSuccess: fetchData
                });
            }
        });
    } else {
        fetchData();
    }

    logger.traceExit('transmitQuestionnaire');
}
