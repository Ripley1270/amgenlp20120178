import COOL from 'core/COOL';
import ELF from 'core/ELF';
import Spinner from 'core/Spinner';
import Subjects from 'core/collections/Subjects';
import CurrentSubject from 'core/classes/CurrentSubject';
import WebService from 'core/classes/WebService';
import { MessageRepo } from 'core/Notify';

/**
 * Handles the resetSecretQuestion transmission to the web-service.
 * @param {Object} transmissionItem - The item in the transmission queue to send.
 * @param {Function} [callback] - The callback function to called upon completion.
 */
export default function resetSecretQuestion (transmissionItem, callback = $.noop) {
    let resetSecretQuestionSuccess,
        resetSecretQuestionError,
        params = JSON.parse(transmissionItem.get('params')),
        mappedJSON = {
            Q: params.secret_question,
            A: params.secret_answer
        };

    Subjects.getSubjectBy({ krpt: params.krpt })
    .then(subject => {
        // eslint-disable-next-line no-unused-vars
        resetSecretQuestionSuccess = (res, { syncID, isSubjectActive }) => {
            subject.save({ subject_active: isSubjectActive })
            .then(() => {
                CurrentSubject.clearSubject();
                this.destroy(transmissionItem.get('id'), callback);
            })
            .done();
        };

        resetSecretQuestionError = (errorCode, httpCode, isSubjectActive) => {
            const { Dialog } = MessageRepo;

            if (httpCode === LF.ServiceErr.HTTP_FORBIDDEN && errorCode === LF.ServiceErr.SUBJECT_NOT_FOUND) {
                // Delete clinical data
                ELF.trigger('Termination', {
                    endParticipation: true,
                    subjectActive: !!isSubjectActive,
                    deviceID: subject.get('device_id')
                }, this)
                .finally(() => {
                    this.destroy(transmissionItem.get('id'), callback);
                });
            } else if (!isSubjectActive) {
                Spinner.hide().then(() => {
                    LF.Actions.notify({
                        dialog: Dialog && Dialog.SECRET_QUESTION_CHANGE_FAILED
                    }, () => {
                        Spinner.show();
                        this.destroy(transmissionItem.get('id'), callback);
                    });
                }).done();
            } else if (httpCode !== LF.ServiceErr.HTTP_NOT_FOUND) {
                Spinner.hide().then(() => {
                    errorCode = errorCode ? `-${errorCode}` : '';

                    LF.Actions.notify({
                        dialog: Dialog && Dialog.TRANSMISSION_ERROR_HTTP_AND_ERROR,
                        options: { httpCode, errorCode }
                    }, () => {
                        Spinner.show();
                        if (subject.get('isDuplicate')) {
                            transmissionItem.save({status: 'failed'});
                        }
                        this.remove(transmissionItem);
                        callback();
                    });
                }).done();
            } else {
                this.remove(transmissionItem);
                callback();
            }
        };

        let service = COOL.new('WebService', WebService);
        service.updateSubjectData(subject.get('device_id'), mappedJSON, null, resetSecretQuestionSuccess, resetSecretQuestionError);
    });
}
