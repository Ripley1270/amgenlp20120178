import ELF from 'core/ELF';
import Logger from 'core/Logger';
import UserSync from 'core/classes/UserSync';
import { MessageRepo } from 'core/Notify';

const logger = new Logger('Transmit.transmitUserData');

/**
 * Bundles user data to be transmitted to the Web-Service.
 * @param {Object} transmissionItem - The item in the transmission queue to send.
 * @param {Function} [callback] - A callback invoked upon completion. DEPRECATED
 * @return {Q.Promise<Object>}
 */
export default function transmitUserData (transmissionItem, callback = $.noop) {
    // eslint-disable-next-line consistent-return
    return Q.Promise((resolve) => {
        // Resolves the transmission with the provided value. Legacy support via callback.
        let resolved = (val) => {
            resolve(val);
            callback(val);

            return val;
        };

        if (transmissionItem.get('status') === 'failed') {
            return resolved(false);
        }

        // Parse out the transmission item's parameters.
        let params = JSON.parse(transmissionItem.get('params'));

        // LF.Model.User may be used here to resolve a compile issue with import.
        let user = new LF.Model.User({ id: params.userId });

        // Fetch the user by ID.
        user.fetch().then(() => {
            let syncLevel = LF.StudyDesign.roles.findWhere({ id: user.get('role') }).get('syncLevel');

            if (!syncLevel) {
                // If no syncLevel, save user locally but not to SW.
                logger.operational(`User: ${user.get('username')} with role: ${user.get('role')} does not have a syncLevel and was not transmitted to SW`);

                // Destroy the transmission record and resolve with false.
                return this.destroy(transmissionItem.get('id'))
                    .then(() => resolved(false));
            } else {
                return UserSync.getValue(syncLevel).then(syncValue => {
                    let userRole = user.getRole();

                    if (!syncValue && syncValue !== '') {
                        // When there is no syncLevel, save user locally but do not transmit.
                        logger.operational(`User: ${user.get('username')} with role: ${user.get('role')}
                                does not have a syncLevel and is only being stored locally`);

                        return this.destroy(transmissionItem.get('id'))
                            .then(() => resolved(false));
                    } else {
                        let userJSON = _.extend({},
                                user.pick('username', 'userType', 'language', 'secretQuestion', 'secretAnswer', 'role', 'password', 'salt', 'active'), {
                                    syncValue,
                                    syncLevel
                                });

                        return LF.studyWebService.addUser(userJSON).then((res, data) => {
                            // 'E' = error
                            if (res === 'E') {
                                logger.error(`There was an error adding the user ${user.get('username')} ` +
                                        `with role ${user.get('role')}`);

                                if (userRole && !userRole.canAddOffline()) {

                                    // Destroy the transmission record...
                                    return this.destroy(transmissionItem.get('id'))

                                        // Then destroy the user.
                                        .then(() => user.destroy())
                                        .then(() => {
                                            return resolved(false);
                                        });
                                }

                                this.remove(transmissionItem);

                                return resolved(false);
                            }

                            // 'D' = Duplicate
                            if (res === 'D') {
                                logger.info(`Transmit User failed. User ${user.get('username')} with role ${user.get('role')} already exists`);

                                return ELF.trigger('TRANSMIT:Duplicate/User', { user, transmissionItem }, this)
                                .then(evt => {
                                    if (!evt.preventDefault) {
                                        const { Dialog } = MessageRepo;
                                        return MessageRepo.display(Dialog && Dialog.USER_ALREADY_EXISTS_ERROR)
                                            .then(() => {
                                                return this.destroy(transmissionItem.get('id'));
                                            })
                                            .then(() => {
                                                return resolved(data);
                                            });
                                    } else {
                                        return resolved(data);
                                    }
                                });
                            } else {
                                return Q(parseInt(res, 10))
                                .catch(err => {
                                    logger.error('Error parsing result', err);
                                })
                                .then(userId => user.save({ userId }))
                                .then(() => {
                                    logger.operational(`userId ${user.get('userId')} assigned to user ${user.get('username')}`);

                                    return this.destroy(transmissionItem.get('id')).then(() => {
                                        return resolved(data);
                                    });
                                })
                                .catch(err => {
                                    err && logger.error('Error saving user', err);
                                    this.remove(transmissionItem);

                                    return resolved(false);
                                });
                            }
                        });
                    }
                });
            }
        })
        .catch(err => {
            err && logger.error('Error Transmitting user data', err);

            this.remove(transmissionItem);

            return resolved(false);
        })
        .done();
    });
}
