import COOL from 'core/COOL';
import ELF from 'core/ELF';
import Logger from 'core/Logger';
import WebService from 'core/classes/WebService';
import { MessageRepo } from 'core/Notify';

const logger = new Logger('Transmit.transmitSubjectAssignment');

/**
 * Handles the transmitSubjectAssignment transmission to the web-service.
 * @param {Object} transmissionItem - The item in the transmission queue to send.
 * @param {Function} [callback] - The callback function to called upon completion.
 */
export default function transmitSubjectAssignment (transmissionItem, callback = $.noop) {
    let params = JSON.parse(transmissionItem.get('params')),
        assignment = {
            K: params.krpt,
            M: params.responsibleParty,
            U: 'Assignment',
            S: params.dateStarted,
            C: params.dateCompleted,
            R: params.reportDate,
            P: params.subjectAssignmentPhase,
            E: LF.coreVersion,
            V: LF.StudyDesign.studyVersion,
            T: params.phaseStartDate,
            L: params.batteryLevel,
            J: params.sigID,
            A: [{
                G: params.initials,
                F: 'PT.Initials',
                Q: ''
            }, {
                G: params.patientId,
                F: 'PT.Patientid',
                Q: ''
            }, {
                G: params.enrollDate,
                F: 'PT.EnrollDate',
                Q: ''
            }, {
                G: params.TZValue,
                F: 'SU.TZValue',
                Q: ''
            }, {
                G: params.language,
                F: 'Assignment.0.Language',
                Q: ''
            }, {
                G: '',
                F: params.affidavit,
                Q: 'AFFIDAVIT'
            }, {
                G: params.SPStartDate,
                F: 'PT.SPStartDate',
                Q: ''
            }]
        },
        sendAssignmentSuccess = () => {
            logger.operational(`SubjectAssignment transmitted: PatientID: ${params.patientId}, sigID: ${params.sigID}`);

            this.destroy(transmissionItem.get('id'))
            .then(callback)
            .done();
        },
        // eslint-disable-next-line consistent-return
        sendAssignmentError = ({ errorCode, httpCode, isSubjectActive }) => {
            let subjectId = JSON.parse(transmissionItem.attributes.params).patientId,
                responseKrpt = JSON.parse(transmissionItem.attributes.params).krpt;

            logger.error(`transmitSubjectAssignment: sendAssignmentError. removing transmissionItem. errorCode: ${errorCode}, httpCode: ${httpCode}, isSubjectActive: ${isSubjectActive}`);

            if (errorCode === '19') {
                LF.DynamicText.duplicateSubjectId = subjectId;

                logger.info(`Transmit Subject failed. Subject ${subjectId} already exists`);

                return ELF.trigger('TRANSMIT:Duplicate/Subject', { subjectId: subjectId, krpt: responseKrpt }, this)
                .then(() => {
                    const { Dialog } = MessageRepo;
                    LF.Actions.removeMessage();
                    return MessageRepo.display(Dialog && Dialog.SUBJECT_ALREADY_EXISTS_ERROR);
                })
                .then(() => {
                    LF.Actions.displayMessage();

                    transmissionItem.save({ status: 'failed' })
                    .then(() => {
                        this.remove(transmissionItem);
                        callback();
                    })
                    .done();
                });
            } else if (errorCode === '20') {
                logger.error(`Transmit Subject failed. Subject ${subjectId} was already successfully transmitted, deleting transmissionItem.`);

                this.destroy(transmissionItem.get('id'))
                .then(callback)
                .done();
            } else {
                this.remove(transmissionItem);
                callback();
            }
        };

    logger.traceEnter('transmitSubjectAssignment');

    if (params.ink) {
        assignment.N = {
            D: params.ink.signatureData,
            X: params.ink.xSize,
            Y: params.ink.ySize,
            H: params.ink.author
        };
    }

    if (params.customSysVars) {
        _.each(params.customSysVars, (sysVar) => {
            assignment.A.push({
                G: sysVar.response,
                F: sysVar.SW_Alias,
                Q: ''
            });
        });
    }

    //compress answers collection with JSONH
    if (LF.StudyDesign.jsonh !== false) {
        assignment.A = JSONH.pack(assignment.A);
        logger.trace('Remapped after JSONH.pack');
    }

    let service = COOL.new('WebService', WebService);

    service.sendSubjectAssignment(assignment, transmissionItem.get('token'))
    .then(sendAssignmentSuccess)
    .catch(sendAssignmentError)
    .done();

    logger.traceExit('transmitSubjectAssignment');
}
