import Logger from 'core/Logger';
import ELF from 'core/ELF';
import WebService from 'core/classes/WebService';
import COOL from 'core/COOL';

const logger = new Logger('HistoricalDataSync');

/**
 * Handles historical data sync transmission to the web service
 * @param {Object} transmissionItem - The item in the transmission queue to send.
 * @param {Function} [callback] - The callback function to called upon completion.
 */
export default function historicalDataSync (transmissionItem, callback = $.noop) {
    let params = JSON.parse(transmissionItem.get('params')),
        historicalDataSyncSuccess = res => {
            // Don't update data unless we get a response
            if (!_.isEmpty(res)) {
                let collection = new LF.Collection[params.collection]();

                collection.fetch()
                    .then(() => ELF.trigger(`HISTORICALDATASYNC:Received/${params.collection}`, {res, collection}))
                    .then(() => {
                        this.destroy(transmissionItem.get('id'), callback);
                    })
                    .catch(err => {
                        err && logger.error('Error destroying transmission', err);
                    })
                    .done();
            } else {
                this.destroy(transmissionItem.get('id'), callback);
            }
        },
        historicalDataSyncError = () => {
            // Remove from queue
            this.destroy(transmissionItem.get('id'), () => {
                callback({ error: true });
            });
        };

    COOL.new('WebService', WebService)[params.webServiceFunction](params, historicalDataSyncSuccess, historicalDataSyncError);
}
