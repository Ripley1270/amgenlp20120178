import Base from './Base';

/**
 * Log model that defines stored log data.
 * @class Log
 * @extends Base
 * @example let model = new Log();
 */
export default class Log extends Base {

    /**
     * @property {string} name - The model's name
     * @readonly
     * @default 'Log'
     */
    get name () {
        return 'Log';
    }

    /**
     * @property {string} storage - Since we aren't extending the StorageBase, we need to manually add in the storage property.
     * @readonly
     * @default 'Log'
     */
    get storage () {
        return 'Log';
    }

    /**
     * @property {Object} schema - The model's schema used for validation and storage construction.
     * @static
     * @readonly
     */
    static get schema () {
        return {
            id: {
                type: Number
            },
            type: {
                type: String,
                required: true,
                width: 32
            },
            level: {
                type: String,
                required: true,
                width: 12
            },
            message: {
                type: String,
                required: true,
                width:512
            },
            source: {
                type: String,
                width: 512
            },
            location: {
                type: String,
                width: 512
            },
            clientTime: {
                type: String,
                required: true,
                width:32
            }
        };
    }

    /**
     * Counts the characters
     * @return {Number} character count
     * @example var charCount = model.countChars();
     */
    countChars () {
        return ((JSON.stringify(this)).length);
    }
}

window.LF.Model.Log = Log;
