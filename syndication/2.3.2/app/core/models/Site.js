import StorageBase from './StorageBase';

/**
 * A model that defines site data.
 * @class Site
 * @extends StorageBase
 * @example let model = new Site();
 */
export default class Site extends StorageBase {

    /**
     * @property {string} name - The model's name
     * @readonly
     * @default 'Site'
     */
    get name () {
        return 'Site';
    }

    /**
     * @property {Object} schema - The model's schema used for validation and storage construction.
     * @static
     * @readonly
     */
    static get schema () {
        return {
            id: {
                type        : Number
            },
            site_code: {
                type        : String,
                encrypt     : true
            },

            // Two different APIs return two different values? Or, does the LPA assign it as site_code?

            siteCode: {
                type        : String,
                encrypt     : true
            },
            study_name: {
                type        : String,
                encrypt     : true
            },
            hash: {
                type        : String,
                encrypt     : true
            },
            site_id: {
                type        : String,
                encrypt     : true
            }
        };
    }
}

window.LF.Model.Site = Site;

