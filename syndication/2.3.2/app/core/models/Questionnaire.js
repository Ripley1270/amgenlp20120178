import Base from './Base';

/**
 * A model that defines a questionnaire configuration.
 * @class Questionnaire
 * @extends Base
 * @example let model = new Questionnaire();
 */
export default class Questionnaire extends Base {

    /**
     * @property {Object} defaults - Default values of every questionnaire model.
     * @readonly
     */
    get defaults () {
        return {

            /**
             * @property {string} defaults.className - The default class assigned to the questionnaire's root element.
             */
            className: 'questionnaire',

            /**
             * @property {boolean} defaults.previousScreen - Allows the user to use the back button while taking the questionnaire.
             */
            previousScreen: true
        };
    }

    /**
     * @property {Object} schema - The model's schema used for validation and storage construction.
     * @static
     * @readonly
     */
    static get schema () {
        return {
            id: {
                type: String,
                width: 48,
                required: true
            },

            // The Signing Unit (SU) of the questionnaire. e.g. 'Daily'
            // This value is stored in each Dashboard record and transmitted to StudyWorks.
            SU: {
                type: String,
                required: true
            },

            // The display name of the questionnaire.
            // This is a translation key that should be in the questionnaire's resource strings.
            displayName: {
                type: String,
                required: true
            },

            // A class appended to the questionnaire view's root DOM node. e.g. 'questionnaire'
            className: {
                type: String,
                required: false
            },

            // Determines if the user can navigate back to the previous question.
            previousScreen: {
                type: Boolean,
                required: false
            },

            // The ID of the affidavit to display. e.g. 'CustomAffidavit'
            affidavit: {
                // affidavit can be set to false to indicate that no affidavit should be shown.
                // A null or undefined value will fallback a role's default affidavit.
                type: [Boolean, String],
                relationship: {
                    to: 'Affidavits',
                    multiplicity: '0..1',
                    // We have to inform the validator to ignore any case where the affidavit might be set to false.
                    // Any value other than null or undefined will be matched against affidavit IDs.
                    ignore: [false]
                }
            },

            // An array of Screen ID's that belong to the questionnaire. e.g. ['DAILY_S1', 'DAILY_S2']
            // See core/models/Screen for details.
            screens: {
                relationship: {
                    to: 'Screens',
                    multiplicity: '1..*'
                }
            },

            // The configuration of the questionnaire's schedules.
            // See core/models/Schedule for details.
            schedules: {
                relationship: {
                    to: 'Schedules',
                    multiplicity: '0..*'
                }
            },

            // If set, will trigger a phase change for the subject upon completion of the questionnaire.
            // See the study design's studyPhase property for available options. e.g. 'FOLLOWUP'
            triggerPhase: {
                type: String,
                required: false
            },

            // Branching configuration.
            // [
            //     {
            //         branchFrom: 'DAILY_DIARY_S_4',
            //         branchTo: 'AFFIDAVIT',
            //         clearBranchedResponses: true,
            //         branchFunction: 'isCurrentPhase',
            //         branchParams: {
            //             value: 'SCREENING'
            //         }
            //     }
            // ]
            branches: {
                relationship : {
                    to: 'Branches',
                    multiplicity: '0..*',
                    embedded: true
                }
            },

            // Configure the role that have access to the questionnaire. e.g. ['site', 'subject']
            accessRoles: {
                relationship: {
                    to: 'Roles',
                    multiplicity: '0..*'
                }
            },

            // Key to configured modal for display if none of the access roles are found.
            //  If blank, no dialog is displayed.
            // noRoleFound: 'NO_ROLE_FOUND_DEFAULT'
            noRoleFound: {
                type: String,
                required: false
            },

            // The Product(s) which should display the diary. e.g. ['sitepad']
            product: {
                required: false,
                type: Array
            }
        };
    }
}

window.LF.Model.Questionnaire = Questionnaire;
