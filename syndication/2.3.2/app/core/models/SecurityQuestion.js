import Base from 'core/models/Base';

/**
 * A model that defines a security question configuration.
 * @class SecurityQuestion
 * @extends Base
 * @example let model = new SecurityQuestion({
 *    text: 'SECURITY_QUESTION_1',
 *    key: 0,
 *    display: true
 * });
 */
export default class SecurityQuestion extends Base {

    /**
     * @property {string} idAttribute - Determines which attribute should be used as the ID on the model.
     * @readonly
     * @default 'key'
     */
    get idAttribute () {
        return 'key';
    }

    /**
     * @property {Object} defaults - Default attributes of the SecurityQuestion model.
     * @readonly
     */
    get defaults () {
        return {
            display: true
        };
    }

    /**
     * @property {Object} schema - The model's schema used for validation.
     * @static
     * @readonly
     */
    static get schema () {
        return {

            // A translation key that determines the display text for the security question.
            // e.g. 'SECURITY_QUESTION_1'
            text : {
                type: String,
                required: true
            },

            // The response stored with the User record. e.g. 0
            key: {
                type: Number,
                required: true
            },

            // If true, the question will be displayed.
            display: {
                type: Boolean,
                required: false
            }
        };
    }
}

window.LF.Model.SecurityQuestion = SecurityQuestion;
