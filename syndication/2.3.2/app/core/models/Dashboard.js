import StorageBase from './StorageBase';

/**
 * A model defining completed questionnaire meta data.
 * @class Dashboard
 * @description
 * Dashboard models, as its name doesn't imply, represent a completed questionnaire/form/diary.
 * Each dashboard record has associated answers, which can be queried by matching the questionnaire_id and instance_ordinal.
 * @extends StorageBase
 * @example let model = new Dashboard();
 */
export default class Dashboard extends StorageBase {

    /**
     * @property {string} name - The model's name
     * @readonly
     * @default 'Dashboard'
     */
    get name () {
        return 'Dashboard';
    }

    /**
     * @property {Object} defaults - Default values of every Dashboard model.
     * @readonly
     */
    get defaults () {
        return {

            /**
             * @property {string} defaults.change_phase - Defaults change_phase to 'false'
             */
            change_phase: 'false',

            /**
             * @property {string} defaults.core_version - Defaults core_version to LF.coreVersion
             */
            core_version: LF.coreVersion

        };
    }

    /**
     * @property {Object} schema - The model's schema used for validation and storage construction.
     * @static
     * @readonly
     */
    static get schema () {
        return {
            id: {
                type        : Number
            },

            // This is the subject_id field from a Subject.  Note that this is not the Subject's actual ID.
            // TODO - We should eliminate this field if possible.  This value can change on a Subject record.
            subject_id: {
                type        : String,
                required    : true,
                encrypt     : true
            },

            // This is the ID of the Questionnaire completed.
            // This field is used in combination with the instance_ordinal to query for answer records.
            // This is NOT transmitted to NetPro/Expert.  See SU.
            // e.g. 'P_DAILY_DIARY'
            questionnaire_id: {
                type        : String,
                required    : true
            },

            // The Signing Unit (SU) of the completed questionnaire. e.g. 'Daily'
            // Configured on the Questionnaire as SU.
            // Used by NetPro/Expert to identify the questionnaire.
            // Maps to 'U' in questionnaire transmissions.
            SU: {
                type        : String,
                required    : true,
                encrypt     : true
            },

            // This is used in unison with the questionnaire_id to map a Dashboard to Answer records. e.g. 1
            instance_ordinal: {
                type        : Number,
                required    : true
            },

            // The start date/time of the questionnaire. e.g. '2012-07-25T17:39:43Z'
            // See DateTimeUtil.ISOStamp();
            // Maps to 'S' in questionnaire transmissions.
            started: {
                type        : String
            },

            // The completed date/time of the questionnaire. e.g. '2012-07-25T17:39:43Z'
            // See DateTimeUtil.ISOStamp();
            // Maps to 'C' in questionnaire transmissions.
            completed: {
                type        : String
            },

            // The timezone offset at the time of questionnaire completion. e.g. -4
            // See DateTimeUtil.getOffset();
            // Maps to 'O' in questionnaire transmissions.
            completed_tz_offset: {
                type        : Number
            },

            // This seems to be an random number passed along to NetPro/Expert as a unique identifier.
            // TODO - Add a method to the model to generate the diary_id.
            // parseInt(currentDate.getTime().toString() + currentDate.getMilliseconds().toString(), 10)
            diary_id: {
                type        : Number
            },

            // A formatted start date string (yyyy-mm-dd). ex. '2015-01-15'
            // See DateTimeUtil.convertToDate();
            // Maps to 'R' in questionnaire transmissions.
            report_date: {
                type        : String
            },

            // The phase of the subject at questionnaire completion. e.g. 10.
            // A map of phases can be found in the study design configuration as studyPhase.
            // Maps to 'P' in questionnaire transmissions.
            phase: {
                type        : Number
            },

            // See Subject.phaseStartDateTZOffset. e.g. -4
            // Maps to 'T' in questionnaire transmissions.
            phaseStartDateTZOffset: {
                type        : String
            },

            // Determines if this diary should change the subject's phase. e.g. 'false'
            // Maps to 'B' in questionnaire transmissions.
            change_phase: {
                type        : String
            },

            // The core version of the application at the time of questionnaire completion. e.g. '2.0.0'
            // Maps to 'E' in questionnaire transmissions.
            // TODO - It seems that this value is being hardcoded for questionnaire transmissions.
            core_version: {
                type        : String,
                required    : true
            },

            // The study version at time of completion. e.g. '00.01'
            // See StudyDesign.studyVersion.
            // Maps to 'V' in questionnaire transmissions.
            study_version: {
                type        : String,
                required    : true
            },

            // The ID of the device the questionnaire was completed on.
            // Either pulled from localStorage property 'deviceId' or 'IMEI', or the Subject.device_id.
            // Maps to 'D' in questionnaire transmissions.
            device_id: {
                type        : String,
                required    : true
            },

            // The battery level at the time the questionnaire was taken.
            // Maps to 'L' in questionnaire transmissions.
            battery_level: {
                type        : String
            },

            // Affidavit signature data as stringified JSON. See SignatureBox widget.
            // JSON.stringify({
            //     X: 438, The width of the signature.
            //     Y: 120, The height of the signature.
            //     H: 'John Smith', // The responsible party or author
            //     D: '3.150.39S@69.37@67...' // Studyworks signature data
            // })
            // Maps to 'N' in questionnaire transmissions.
            ink: {
                type        : String,
                encrypt     : true
            },

            // A generated identifier that differs by product.
            // e.g. 'SA.5a5cbfe24deef09cd2c3d898f4ebc5e1'
            // Maps to 'J' in questionnaire transmissions.
            // Generated as seen below:
            // `${isSitePad ? 'SA' : 'LA'}.${this.data.started.getTime().toString(16)}${lStorage.getItem('IMEI')}`
            sig_id: {
                type        : String
            },

            // The krpt of the subject.
            // This is required for SPA transmissions as there is no Subject bound authentication token like in LPA.
            // Maps to 'K' in questionnaire transmissions.
            krpt: {
                type        : String
            },

            // The name of the party that signed the questionnaire. e.g. 'John Snow'
            // Required for SPA questionnaire transmissions.
            // Maps to 'M' in questionnaire transmissions.
            responsibleParty: {
                type        : String
            }
        };
    }
}

window.LF.Model.Dashboard = Dashboard;
