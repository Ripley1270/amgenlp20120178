import StorageBase from './StorageBase';

/**
 * A model that defines a single response record.
 * @class AnswerBase
 * @extends StorageBase
 * @example var model = new AnswerBase();
 */
export default class AnswerBase extends StorageBase {

    /**
     * @property {string} name - The Model's name
     * @readonly
     * @default 'AnswerBase'
     */
    get name () {
        return 'AnswerBase';
    }

    /**
     * @property {Object} defaults - Default values of every Answer model.
     * @readonly
     */
    get defaults () {
        return {

            /**
             * @property {(null|string|number)} defaults.response - A default response of null
             */
            response    : null
        };
    }

    /**
     * @property {Object} schema - The model's schema used for validation and storage construction.
     * @static
     * @readonly
     */
    static get schema () {
        return {
            id: {
                type        : Number,
                primary     : true,
                increment   : true
            },

            // The ID of the Question. e.g. 'DAILY_DIARY_Q_1'
            question_id: {
                type        : String,
                required    : true
            },

            // The ID of the Questionnaire. e.g. 'P_Daily_Diary'
            // Maps to 'Q' in questionnaire transmissions.
            questionnaire_id: {
                type        : String,
                required    : true
            },

            // The user's response to the question.
            // Maps to 'G' in questionnaire transmissions.
            response: {
                type        : String,
                encrypt     : true
            },

            // A key generated by `${IG}.${IGR}.${IT}` e.g. Daily.0.DAILY_Q1
            // IG (Item Group) is a configured property on a question.
            // IGR (Item Group Repetition) indicates the repetition of the question, and is generated.
            // IT (Item) is also configured on the question.
            // Maps to 'F' in questionnaire transmissions.
            SW_Alias: {
                type        : String,
                required    : true
            },

            // The instance of the ordinal of the questionnaire. e.g. 0
            instance_ordinal: {
                type        : Number,
                required    : true
            }
        };
    }
}

