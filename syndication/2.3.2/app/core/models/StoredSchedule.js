import StorageBase from './StorageBase';
import Schedule from './Schedule';

/**
 *  A model that defines a Schedule configuration.
 * @class StoredSchedule
 * @extends StorageBase
 * @example let model = new StoredSchedule();
 */
export default class StoredSchedule extends StorageBase {

    /**
     * @property {string} name - The model's name
     * @readonly
     * @default 'Schedule'
     */
    get name () {
        return 'Schedule';
    }

    /**
     * @property {Object} schema - The model's schema used for validation.
     * @static
     * @readonly
     */
    static get schema () {
        return {
            id: {
                type        : Number
            },
            scheduleId: {
                type        : String,
                width      : 30,
                required    : true
            },
            target: {
                type        : String,
                required    : true
            },
            scheduleFunction: {
                type        : String,
                required    : true
            },
            scheduleParams: {
                type        : String,
                required    : false

            },
            alarmFunction: {
                type        : String,
                required    : false
            },
            alarmParams: {
                type        : String,
                required    : false
            },
            phase: {
                type        : String,
                required    : false
            }
        };
    }

    /**
     * @description
     * StoredSchedule constructor function.
     * Sets listener to destroy and remove from database when removed from LF.schedules collection
     * @param {Object} attributes - Attributes to set on the model.
     * @param {Object} options - See Backbone.Model documentation.
     * @example
     * var schedule = new StoredSchedule();
     */
    initialize (attributes, options) {
        this.options = options;

        if (this.options && this.options.parseSchedule) {
            attributes.scheduleId = this.get('id');
            this.set(attributes);
            this.unset('id');
        }
    }

    /**
     * Converts this model to an Schedule
     * @returns {Schedule} converted schedule
     */
    toSchedule () {

        // perform deep clone
        let attr = JSON.parse(JSON.stringify(this.attributes));

        attr.id = this.attributes.scheduleId;
        attr.dbid = this.attributes.id;

        return new Schedule(attr);
    }


    /**
     * Converts this model to an LF.Model.StoredSchedule to be saved in the database
     * @param {Schedule} schedule - The schedule model to convert.
     * @returns {StoredSchedule} converted schedule
     */
    static fromSchedule (schedule) {
        return new this(JSON.parse(JSON.stringify(schedule.attributes)));
    }

    /**
     * Override parse to stringify Parameter Objects before saving to database
     * @param {string} response - attributes to be modified
     * @returns {*} parsed attributes
     */
    parse (response) {
        let attr = response ? JSON.parse(JSON.stringify(response)) : JSON.parse(JSON.stringify(this.attributes));

        if (attr.target && typeof attr.target === Object) {
            attr.target = JSON.stringify(attr.target);
        }

        if (attr.scheduleParams && typeof attr.scheduleParams === Object) {
            attr.scheduleParams = JSON.stringify(attr.scheduleParams);
        }

        if (attr.alarmParams && typeof attr.alarmParams === Object) {
            attr.alarmParams = JSON.stringify(attr.alarmParams);
        }

        if (attr.phase && typeof attr.phase === Array) {
            attr.phase = JSON.stringify(attr.phase);
        }

        return attr;
    }

    /**
     * Parse Parameter Objects into JSON objects
     * @returns {Object} new, parsed attributes object
     */
    toJSON () {
        let attr = JSON.parse(JSON.stringify(this.attributes));

        if (attr.target && typeof attr.target !== Object) {
            attr.target = JSON.parse(attr.target);
        }

        if (attr.scheduleParams && typeof attr.scheduleParams !== Object) {
            attr.scheduleParams = JSON.parse(attr.scheduleParams);
        }

        if (attr.alarmParams && typeof attr.alarmParams !== Object) {
            attr.alarmParams = JSON.parse(attr.alarmParams);
        }

        if (attr.phase && typeof attr.phase !== Array) {
            attr.phase = JSON.parse(attr.phase);
        }

        return attr;
    }

    /**
     * Validate the parsed values of this model.
     */
    validate () {
        super.validate(this.parse());
    }
}

window.LF.Model.StoredSchedule = StoredSchedule;
