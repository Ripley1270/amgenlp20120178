import Base from './Base';

/**
 * A model that defines a visit configuration.
 * @class Visit
 * @extends Base
 * @example let model = new Visit({
 *   id    : 'visit3',
 *   studyEventId: '30',
 *   displayName  : 'VISIT_3',
 *   visitType : 'ordered',
 *   visitOrder : 3,
 *   forms : ['P_Welcome_SitePad', 'P_Meds_SitePad'],
 *   delay: '0'
 * });
 */
export default class Visit extends Base {

    /**
     * @property {Object} schema - The model's schema used for validation and storage construction.
     * @static
     * @readonly
     */
    static get schema () {
        return {
            id: {
                type        : String,
                required    : true
            },

            // This is sent as a response for visit start and end forms.
            // Matches up with a visit attribute in the study design. e.g. 10
            studyEventId: {
                type: String
            },

            // The display name for the visit.
            // Must be a translation key. e.g. 'VISIT_01'
            displayName: {
                type: String
            },

            // Determines if the visit is unscheduled or ordered. e.g. 'unscheduled'
            visitType: {
                type: String
            },

            // If the visitType is 'ordered', this determines the display order on the gateway. e.g. 0
            visitOrder: {
                type: Number
            },

            // An array of Questionnaire IDs that belong to the form. e.g. ['EQ5D', 'FieldTypeSamples']
            forms: {
                relationship: {
                    to: 'Questionnaires',
                    multiplicity: '0..*'
                }
            },

            // e.g. '0'
            delay: {
                type: String
            }
        };
    }
}

window.LF.Model.Visit = Visit;
