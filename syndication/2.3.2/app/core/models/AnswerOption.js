import Base from './Base';

/**
 * @class AnswerOption
 * @description
 * AnswerOptions are usually constructed by each widget and represent an array of possible answers for a question.
 * Depending on the widget in question, the properties on the model may differ; However, certain properties must exist as seen in the schema.
 * @extends Base
 * @example let model = new AnswerOption({
 *     text: 'UNABLE_WALKING',
 *     value: '2',
 *     type: 'number'
 * });
 */
export default class AnswerOption extends Base {

    /**
     * @property {string} name - The Model's name
     * @readonly
     * @default 'Answer'
     */
    get name () {
        return 'AnswerOption';
    }

    /**
     * @property {Object} schema - The model's schema used for validation and storage construction.
     * @static
     * @readonly
     */
    static get schema () {
        return {
            // A translation key to use for display of the answer option. e.g. 'UNABLE_WALKING'
            text: {
                type        : String,
                required    : true
            },

            // The value to be stored if this answer option is selected.
            value: {
                type        : String,
                required    : true
            },

            // Can be used to parse the response to a type, such as a number. e.g. 'number'
            type: {
                type: String,
                required: false
            },

            // In some cases, such as a checkbox, where multiple answers may be selected, multiple answers need to be recorded.
            // As such, each recorded answer requires it's own IT to construct the SWAlias (IG.IGR.IT).
            IT: {
                type        : String,
                required    : false
            },

            exclude: {
                type        : Array,
                required    : false
            }
        };
    }
}

window.LF.Model.AnswerOption = AnswerOption;
