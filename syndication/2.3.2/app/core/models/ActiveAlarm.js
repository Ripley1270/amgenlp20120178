import StorageBase from './StorageBase';

/**
 * A model that defines the active alarm.
 * @class ActiveAlarm
 * @extends StorageBase
 * @example let model = new ActiveAlarm();
 */
export default class ActiveAlarm extends StorageBase {

    /**
     * @property {string} name - The model's name
     * @readonly
     * @default 'ActiveAlarm'
     */
    get name () {
        return 'ActiveAlarm';
    }

    /**
     * @property {Object} schema - The model's schema used for validation and storage construction.
     * @static
     * @readonly
     */
    static get schema () {
        return {
            id: {
                type: Number,
                required: true
            },
            schedule_id: {
                required: true
            },
            date: {},
            repeat: {},
            alarmTrigger: {}
        };
    }
}

window.LF.Model.ActiveAlarm = ActiveAlarm;
