import Base from './Base';

/**
 * A model that defines a question configuration.
 * @class Question
 * @extends Base
 * @example
 * let model = new Question({
 *     id          : 'DAILY_DIARY_Q_1',
 *       IG          : 'Daily',
 *       IT          : 'DAILY_Q1',
 *       skipIT      : 'DAILY_Q1_SKP',
 *       title       : 'QUESTION_1_TITLE',
 *       text        : [
 *
 *           'HELP_TEXT',
 *           'QUESTION_1'
 *       ],
 *       className   : 'DAILY_DIARY_Q_1',
 *       widget      : { }
 * });
 */
export default class Question extends Base {

    /**
     * @property {Object} schema - The model's schema used for validation and storage construction.
     * @static
     * @readonly
     */
    static get schema () {
        return {
            id: {
                type        : String,
                required    : true
            },

            // The Item Group (IG) is the first part of the SW_Alias. e.g. 'Daily'
            IG: {
                type        : String,
                required    : false
            },

            // Item (IT) is the last part of the SW_Alias. e.g. 'Daily_Q1'
            IT: {
                type        : String,
                required    : false
            },

            // This value is only used for Affidavits. e.g. 'SubmitForm''
            krSig: {
                type        : String,
                required    : false
            },

            // If a question is skipped, this value will replace the IT in the SW_Alias. e.g. 'DAILY_Q1_SKP'
            skipIT: {
                type        : String,
                required    : false
            },

            // The question's display title. e.g. 'QUESTION_1_TITLE'
            // This is a translation key that should be in the questionnaire's resource strings.
            title: {
                type        : String,
                required    : false
            },

            // Template configuration for the question view.
            // e.g. { question: 'Welcome:Question' }
            templates: {
                type: Object,
                required: false
            },

            // Determine if the question can be looped.
            repeating: {
                type: Boolean,
                required: false
            },

            // The question's text. Either a translation key, or an array of translation keys.
            // e.g. ['QUESTION_1', 'QUESTION_1A']
            text: {
                type        : [String, Array],
                required    : true
            },

            // Help text to display for the question. e.g. 'QUESTION_1_HELP'
            // This is a translation key that should be in the questionnaire's resource strings.
            help: {
                type        : String,
                required    : false
            },

            // An optional class appended to the question view's root DOM node. e.g. 'question'
            className: {
                type        : String,
                required    : false
            },

            // The question's widget configuration.
            // See each widget for configuration options.
            widget: {
                relationship : {
                    to: 'Widgets',
                    multiplicity: '0..1',
                    embedded: true
                }
            }
        };
    }

    /**
     * @property {Object} defaults - Default values of every Question model.
     * @readonly
     */
    get defaults () {
        return {

            /**
             * @property {string} defaults.className - The default class assigned to the question's root element.
             */
            className: 'question'
        };
    }
}

window.LF.Model.Question = Question;
