import StorageBase from './StorageBase';

/**
 * A model that defines esense device data
 * @class ESenseDevice
 * @extends StorageBase
 * @example let model = new ESenseDevice();
 */
export default class ESenseDevice extends StorageBase {

    /**
     * @property {string} name - The model's name
     * @readonly
     * @default 'ESenseDevice'
     */
    get name () {
        return 'ESenseDevice';
    }

    /**
     * @property {Object} schema - The model's schema used for validation and storage construction.
     * @static
     * @readonly
     */
    static get schema () {
        return {
            id: {
                type        : Number
            },
            name: {
                type        : String
            },
            type: {
                type        : String
            },
            address: {
                type        : String
            },
            serial: {
                type        : String
            },
            pairDate: {
                type        : String
            }
        };
    }
}

window.LF.Model.ESenseDevice = ESenseDevice;
