import AnswerBase from './AnswerBase';

/**
 * A model that defines a single response record.
 * @class Answer
 * @extends AnswerBase
 * @example let model = new Answer();
 */
export default class Answer extends AnswerBase {

    /**
     * @property {string} name - The Model's name
     * @readonly
     * @default 'Answer'
     */
    get name () {
        return 'Answer';
    }

    /**
     * @property {Object} defaults - Default values of every Answer model.
     * @readonly
     */
    get defaults () {
        return {

            /**
             * @property {(null|string|number)} defaults.response - A default response of null.
             */
            response: null
        };
    }

    /**
     * @property {Object} schema - The model's schema used for validation and storage construction.
     * @static
     * @readonly
     */
    static get schema () {
        return _.extend({
            // App level code seems to be confused about subject vs subject_id.
            // TODO: Resolve which is actually used, make it required, remove the other.
            subject: {
                type        : String,
                encrypt     : true
            },
            subject_id: {
                type        : String,
                encrypt     : true
            }

        }, AnswerBase.schema);
    }
}

window.LF.Model.Answer = Answer;
