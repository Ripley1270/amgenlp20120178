import * as Utils from 'core/utilities';
import Schedules from 'core/collections/Schedules';
import StoredSchedules from 'core/collections/StoredSchedules';
import ActiveAlarms from 'core/collections/ActiveAlarms';
import Subjects from 'core/collections/Subjects';
import Logger from 'core/Logger';
import * as lStorage from 'core/lStorage';

const logger = new Logger('Context');

export default class Context extends Backbone.Model {
    constructor (options) {
        super(options);

        this.set({
            defaultLanguage: `${LF.StudyDesign.defaultLanguage}-${LF.StudyDesign.defaultLocale}`,

            defaultLanguageModels: LF.strings.filter(model => {
                const { language, locale } = model.toJSON();
                const { defaultLanguage, defaultLocale } = LF.StudyDesign;

                return language === defaultLanguage && locale === defaultLocale;
            }),

            // Cache an instance of Users so we don't have to fetch everywhere.
            // TODO: Usage of LF.Collection.Users is due to circular dependency. This has to be resolved
            users: new LF.Collection.Users(),

            // Cache an instance of Subjects so we don't have to fetch everywhere.
            subjects: new Subjects(),

            defaultContextRole: 'subject',

            theme: 'a',

            defaultDialogs: {
                deactivated: 'DEACTIVATED',
                activatedElsewhere: 'ACTIVATED_ELSEWHERE'
            },

            defaultText: {
                header: 'APPLICATION_HEADER',
                noDiariesTitle: 'NO_DIARIES_TITLE',
                noDiariesAvailable: 'SCHED_NO_DIARIES_AVAILABLE',
                deactivated: 'DEACTIVATED_LABEL',
                activatedElsewhere: 'ACTIVATED_ELSEWHERE'
            }
        });

        this.setLanguageProperties(this.get('defaultLanguage'));
    }

    /**
     * @property {string} role - Shortcut getter to get the string "id" of the current context role.
     */
    get role () {
        const role = this.get('role');

        return role ? role.get('id') : false;
    }

    /**
     * @property {string} langLoc - Create language-locale string
     */
    get langLoc () {
        return `${this.get('language')}-${this.get('locale')}`;
    }

    /**
     * fetch the context users collection.
     * @returns {Q.Promise} resolves with the current users collection after fetch is done.
     */
    fetchUsers () {
        this.get('users').reset();
        return this.get('users').fetch()
        .then(() => this.get('users'));
    }

    /**
     * Check if the logged in user is still active and still logged in,
     *  then set context accordingly
     * @returns {Q.Promise}
     */
    checkUserAuth () {
        return LF.security.checkLogin()
        .then(authenticated => {
            let loggedUser = parseInt(lStorage.getItem('User_Login'), 10),
                user = this.get('users').findWhere({ id: loggedUser });

            if (user && user.get('active') && authenticated) {
                return this.setContextByUser(user);
            } else {
                logger.operational('Logged user is inactive or cannot be found.');
                lStorage.removeItem('isAuthorized');
                lStorage.removeItem('User_Login');

                this.setLanguageProperties(this.get('defaultLanguage'));
                return this.setContextByRole();
            }
        });
    }

    /**
     * Set application context based on user role and language
     * @param {LF.Model.User} [user] The user model that will be used to set application context
     * @returns {Q.Promise}
     */
    setContextByUser (user) {
        this.set('user', user);

        return user.fetch()
        .then(() => this.setContextLanguage(user.get('language')))
        .then(() => this.setContextByRole(user.get('role')));
    }

    /**
     * Set application context based on user role and language
     * @param {string} [userRole] - String id of role to set context as. Defaults to this.defaultContextRole
     * @returns {Q.Promise}
     */
    setContextByRole (userRole = this.get('defaultContextRole')) {
        const role = LF.StudyDesign.roles.findWhere({ id: userRole });

        if (!role) {
            logger.error('Role not found in studyDesign');
            return Q();
        }

        return Q()
        .then(() => {
            this.set({ role });

            return this.loadContextSchedules();
        })
        .catch(err => {
            logger.error(`Error while setting up context by user with role: ${role}`, err);
        });
    }
    /**
     * Sets the language and locale properties of the class
     * @param {String} lang - The String containing language and locale in this format 'language-locale'
     */
    setLanguageProperties (lang) {
        const [language, locale] = lang.split('-');

        this.set({ language, locale });
    }

    /**
     * Set application context language
     * @param {String} lang - The String containing language and locale in this format 'language-locale'
     * @returns {Q.Promise<void>}
     * @example this.setContextLanguage('en-US');
     */
    setContextLanguage (lang) {
        return Q()
        .then(() => {
            const [language, locale] = lang.split('-');

            this.setLanguageProperties(lang);

            LF.Preferred = { language, locale };
            Utils.setLanguage(lang, LF.strings.getLanguageDirection(LF.Preferred));

            this.set('contextLanguageModels', LF.strings.filter(model => {
                return model.get('language') === language && model.get('locale') === locale;
            }));
        });
    }

    /**
     * Loads context schedules for context role
     * @returns {Q.Promise}
     */
    loadContextSchedules () {
        const addToGateway = schedule => {
            let scheduleRole = schedule.get('scheduleRoles') || ['subject'];

            if (_.contains(scheduleRole, this.role)) {
                this.get('schedules').add(schedule, { merge: true });
            }
        };

        this.set('schedules', new Schedules());

        return StoredSchedules.fetchCollection()
        .then(storedSchedules => {
            if (LF.schedules) {
                LF.schedules.reset(storedSchedules.toJSON());
            } else {
                LF.schedules = storedSchedules;
            }

            return LF.Helpers.getScheduleModels();
        })
        .then(schedules => Q.all(schedules.map(schedule => addToGateway(schedule))))
        .then(() => {
            this.stopListening(LF.schedules);

            // When a schedule is added, add it to the application context schedule collection.
            this.listenTo(LF.schedules, 'add', schedule => addToGateway(schedule.toSchedule()));

            // When a schedule is removed, remove it from the application context then reevaluate alarms.
            this.listenTo(LF.schedules, 'remove', schedule => {
                if (this.role === schedule.get('target').role) {
                    const scheduleId = schedule.get('scheduleId');
                    const contextSchedule = this.get('schedules').findWhere({ scheduleId });
                    this.get('schedules').remove(contextSchedule);
                }

                LF.Wrapper.exec({
                    execWhenWrapped: () => {
                        if (schedule.get('alarmParams')) {
                            ActiveAlarms.cancelAlarm(schedule.get('alarmParams'));
                        }
                    }
                });
            });
        })
        .catch(err => {
            logger.error('Error loading context schedules', err);
        });
    }
}
