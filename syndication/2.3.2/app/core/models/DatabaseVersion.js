import StorageBase from './StorageBase';

/**
 * A model that defines the database version.
 * @class DatabaseVersion
 * @extends StorageBase
 * @example let model = new DatabaseVersion();
 */
export default class DatabaseVersion extends StorageBase {

    /**
     * @property {string} name - The model's name
     * @readonly
     * @default 'DatabaseVersion'
     */
    get name () {
        return 'DatabaseVersion';
    }

    /**
     * @property {Object} schema - The model's schema used for validation and storage construction.
     * @static
     * @readonly
     */
    static get schema () {
        return {
            id: {
                type        : Number
            },
            versionName: {
                type        : String,
                required    : true
            },
            version: {
                type        : Number,
                required    : true
            }
        };
    }
}

window.LF.Model.DatabaseVersion = DatabaseVersion;
