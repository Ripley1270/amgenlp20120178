import Base from 'core/models/Base';

/**
 * @class Branch
 * @extends Base
 * @description
 * A branch is used to jump from one screen to another.
 * Upon leaving the branchFrom screen, the configured branching function is invoked.
 * Each branching function must return a boolean value.  A value of true will navigate to the branchTo screen.
 * Any falsy value will execute normal questionaire workflow.
 * Branching functions sit in the LF.Branching.branchFunctions[branchFunction] namespace.
 * @example let model = new Branch({
 *    branchFrom: 'Question01',
 *    branchTo: 'Question03',
 *    branchFunction: 'myBranchingFunction',
 *    branchParams: { title: 'HELLO_WORLD' },
 *    clearBranchedResponses: true
 * });
 */
export default class Branch extends Base {

    /**
     * @property {Object} schema - The model's schema used for validation.
     * @static
     * @readonly
     */
    static get schema () {
        return {

            // The screen to initiate branching from.
            // e.g. 'DailyScreen01'
            branchFrom: {
                relationship : {
                    to: 'Screens',
                    multiplicity: '1..1',
                    ignore: ['AFFIDAVIT']
                }
            },

            // The screen to branch to.
            // e.g. 'DailyScreen03'
            branchTo: {
                relationship : {
                    to: 'Screens',
                    multiplicity: '1..1',
                    ignore: ['AFFIDAVIT']
                }
            },

            // This is the name of a function to execute on the LF.Branching.branchFunctions namespace.
            // Each function should return a boolean value. If true, navigates to the branchTo screen.
            // If false, normal questionnaire workflow is executed.
            branchFunction: {
                type: String,
                required: true
            },

            // Parameters passed into the branchFunction. Could be any type.
            branchParams: {
                type: [Object, String, Boolean, Number],
                required: false
            },

            // If set to true, clears any branched responses.
            clearBranchedResponses: {
                type: Boolean,
                required: false
            }
        };
    }
}

window.LF.Model.Branch = Branch;
