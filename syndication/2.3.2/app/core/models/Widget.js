import Base from './Base';

/**
 * A model that defines a widget configuration.
 * @class Widget
 * @extends Base
 * @example let model = new Widget();
 */
export default class Widget extends Base {

    /**
     * @property {Object} schema - The model's schema used for validation and storage construction.
     * @static
     * @readonly
     */
    static get schema () {
        return {
            // The ID of the widget. e.g. 'DailyWidget01'
            id: {
                type        : String,
                required    : true
            },

            // An optional class appended to the widget's root DOM node. e.g. 'daily-widget'
            className: {
                type        : String,
                required    : false
            },

            // A map of templates to use with the configured widget.
            // Each widget type will have it's own template configuration options.
            templates: {
                type        : Object,
                required    : false
            },

            // The available answer options for the widget.
            // See core/models/AnswerOption for configuration.
            answers: {
                relationship : {
                    to: 'AnswerOptions',
                    multiplicity: '0..*',
                    embedded: true
                }
            },

            markers: {
                type        : Object,
                required    : false
            },
            configuration: {
                type        : Object,
                required    : false
            },

            // Validation configuration for the widget's response.
            validation: {
                type        : Object,
                required    : false
            }
        };
    }
}

window.LF.Model.Widget = Widget;
