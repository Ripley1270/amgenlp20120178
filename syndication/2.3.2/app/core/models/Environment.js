import Base from 'core/models/Base';

/**
 * A model that defines a environment configuration.
 * @class Environment
 * @extends Base
 * @example let model = new Environment({
 *
 * });
 */
export default class Environment extends Base {

    /**
     * @property {Object} defaults - Default attributes to populate.
     */
    get defaults () {
        /**
         * @property {boolean} defaults.checkForTimeSlip - Determine if time slip should be checked.
         */
        return { checkForTimeSlip: true };
    }

    /**
     * @property {Object} schema - The model's schema used for validation.
     * @static
     * @readonly
     */
    static get schema () {
        return {

            // A unique ID stored in localStorage (PHT_environment) to track the selected environment.
            // e.g. 'development'
            id: {
                type: String,
                required: true
            },

            // The display text of the environment. e.g. 'SPA Development'
            label: {
                type: String,
                required: true
            },

            // A list of modes the environment is available in. e.g. ['depot', 'provision']
            modes: {
                type: Array,
                required: true
            },

            // The service URL associated with the environment.
            // All API calls from the client will use this URL.
            url: {
                type: String,
                required: true
            },

            // The name of the study, or study database name.
            studyDbName: {
                type: String,
                required: true
            },

            // enable/disable Time travel
            timeTravel: {
                type: Boolean,
                required: false
            },

            // enable/disable timeSlip check. Defaults to true.
            checkForTimeSlip: {
                type: Boolean,
                required: false
            }

        };
    }
}

window.LF.Model.Environment = Environment;
