import StorageBase from './StorageBase';

/**
 * A model that defines the subject configurable alarm.
 * @class SubjectAlarm
 * @extends StorageBase
 * @example let model = new SubjectAlarm();
 */
export default class SubjectAlarm extends StorageBase {

    /**
     * @property {string} name - The model's name
     * @readonly
     * @default 'SubjectAlarm'
     */
    get name () {
        return 'SubjectAlarm';
    }

    /**
     * @property {Object} schema - The model's schema used for validation and storage construction.
     * @static
     * @readonly
     */
    static get schema () {
        return {
            id: {
                type        : Number
            },
            schedule_id: {
                type        : String,
                required    : true
            },
            time: {
                type        : String,
                required    : true
            }
        };
    }
}

window.LF.Model.SubjectAlarm = SubjectAlarm;
