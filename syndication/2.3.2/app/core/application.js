/* eslint-disable */
var LF, Application;

/**
 * The global namespace of the application.
 * @namespace
 */
LF = {
    /**
     * Core Product version
     * @readonly
     * @type String
     * @default '2.0.0'
     */
    coreVersion: '2.0.0',

    /**
     * Core database version
     * @readonly
     * @type Number
     * @default 0
     */
    coreDbVersion: 0,

    /**
     * Holds all extended Backbone Models.
     * @namespace
     * @example LF.Model.Example = Backbone.Model.extend({});
     * @example var model = new LF.Model.Example({ title : 'untitled' });
     */
    Model: {},

    /**
     * Holds all extended Backbone Collections.
     * @namespace
     * @example LF.Collection.Example = Backbone.Collection.extend({});
     * @example var collection = new LF.Collection.Example({});
     */
    Collection: {},

    /**
     * Holds all application widgets.
     * @namespace
     * @example LF.Widget.Example = Backbone.View.extend({});
     * @example var widget = new LF.Widget.Example({});
     */
    Widget: {
        ValidationFunctions: {},
        ParamFunctions: {},
        ReviewScreen: {}
    },

    /**
     * Holds all extended Backbone Views.
     * @namespace
     * @example LF.View.ExampleView = Backbone.View.extend({});
     * @example var view = new LF.View.Example({});
     */
    View: {},

    /**
     * Holds all extended Backbone Routers.
     * @namespace
     * @example LF.Router.Example = Backbone.Router.extend({});
     * @example var router = new LF.Router.Example();
     */
    Router: {},

    /**
     * Holds all extended Backbone Classes.
     * @namespace
     * @example LF.Class.Example = Backbone.Class.extend({});
     * @example var example = new LF.Class.Example();
     */
    Class: {},

    /**
     * Holds all in memory application data.
     * @namespace
     * @example LF.Data.currentDate = new Date();
     */
    Data: {},

    /**
     * Holds all application utility functions.
     * @namespace
     * @example LF.Utilities.add = function (a, b) { }
     * @example var result = LF.Utilities.add(1, 1);
     */
    Utilities: {
        localStorage: {}
    },

    /**
     * Holds all application helpers functions.
     * @namespace
     * @example LF.Helpers.getResponse = function (questionID) { };
     * @example var response = LF.Helpers.getResponse(DAILY_DIARY_Q_1);
     */
    Helpers: {},

    /**
     * Holds all application actions for use by the rules engine.
     * @namespace
     * @example LF.Actions.confirm = function ({message : msg}, callback ) { };
     * @example LF.Actions.confirm({message : 'RESOURCE_KEY'}, function (response) { });
     */
    Actions: {},

    /**
     * Holds all transmission methods to be called by the transmission queue.
     * @namespace
     * @example LF.Transmit.syncSubject = function (params, callback) { };
     */
    Transmit: {},

    /**
     * Holds all transmission response methods to be called by the transmission queue.
     * @namespace
     * @example LF.Respond.syncSubject = function (params, callback) { };
     */
    Respond: {},

    /**
     * Holds all application Resources
     * @namespace
     * @example = LF.Resources.Icons = { };
     */
    Resources: {},

    /**
     * Holds all preferences
     * @namespace
     * @example = LF.Preferred.language = 'en';
     */
    Preferred: {},

    /**
     * Holds all application notification functions.
     * @namespace
     * @example LF.Notify.Banner.show({});
     */
    Notify: {},

    /**
     * Holds all application branching.
     * @namespace
     * @example LF.Branching.evaluateBranching = function (branchArray, screenId, answers) {};
     */
    Branching: {
        branchFunctions: {}
    },

    /**
     * Holds all application schedule functions.
     * @namespace
     * @example LF.Schedule.schedulingFunctions.checkAlwaysAvailability(schedule, completedQuestionnaires, callback);
     */
    Schedule: {
        DisplayItem: {},
        schedulingFunctions: {},
        alarmFunctions: {}
    },

    /**
     * Holds application Gateways.
     * @namespace
     * @example LF.Gateways.Subject.schedules
     */
    Gateways: {
        Subject: {},
        Site: {}
    },

    /**
     * Holds all application dynamic text functions.
     * @namespace
     * @example LF.DynamicText.currentDate(callback);
     */
    DynamicText: {},

    /**
     * Holds all phonegap wrapper related functionality.
     * @namespace
     * @example LF.Wrapper.isAndroid();
     */
    Wrapper: {
        Utils: {
            eSense: {}
        }
    },

    /**
     * Holds the all the setting variables used in core.
     * @namespace
     * @example LF.CoreSettings.eSense.apiCallTimeout();
     */
    CoreSettings: {
        eSense: {}
    },

    /**
     * Holds the all the Error settings for the application.
     * @namespace
     */
    Error: {},

    /**
     * Holds all possible visit states for SitePad.
     * @namespace
     */
    VisitStates: {
        AVAILABLE: 'Available',
        NOT_AVAILABLE: 'Not Available',
        IN_PROGRESS: 'In Progress',
        SKIPPED: 'Skipped',
        COMPLETED: 'Completed',
        INCOMPLETE: 'Incomplete'
    },

    /**
     * Holds all possible Questionnaire states.
     * @namespace
     */
    QuestionnaireStates: {
        AVAILABLE: 'available',
        NOT_AVAILABLE: 'notavailable',
        SKIPPED: 'skipped',
        COMPLETED: 'completed'
    },

    /**
     * Holds all application visits functions.
     * @namespace
     * @example LF.visits.availabilityFunctions.isPhase = (visit, userVisit);
     */
    Visits: {
        availabilityFunctions: {}
    }
};

Application = LF;
