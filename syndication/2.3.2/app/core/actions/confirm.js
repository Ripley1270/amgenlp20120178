import ELF from 'core/ELF';
import { MessageRepo } from 'core/Notify';
import * as MessageHelpers from 'core/resources/Messages/MessageHelpers';
import Logger from 'core/Logger';
import Spinner from 'core/Spinner';

const logger = new Logger('Confirm');

/**
 * Displays a confirmation dialog pop-up.
 * @param {Object} params The header and message to display.
 * @param {Function} callback The callback function invoked when the the user's input is submitted.
 * @returns {Q.Promise<void>}
 */
export function confirm (params, callback = false) {
    const dialogVal = params.dialog || (MessageRepo.Dialog && MessageRepo.Dialog[params.key]);

    if (!dialogVal) {
        if (params.key) {
            logger.warn(`Dialog "${params.key}" not found in registry.`);

            callback && callback(true);
            return Q(true);
        }

        logger.info('Confirm dialog being shown without being registered, message will not be available for screenshots.');

        const dialog = MessageHelpers.confirmDialogCreator(params);

        return Spinner.hide()
        .then(() => {
            return dialog();
        })
        .then(() => {
            callback && callback(true);
        });
    }

    return Spinner.hide()
    .then(() => MessageRepo.display(dialogVal, params.options))
    .then(() => {
        callback && callback(true);
        return true;
    })
    .catch(() => {
        // DE16990 - Removed Q.reject(false).  It wasn't being returned, so remained unhandled.
        callback && callback(false);

        return false;
    });
}

ELF.action('confirm', confirm);

// Adding this as an expression as well...
ELF.expression('confirm', confirm);

// @todo remove
LF.Actions.confirm = confirm;
