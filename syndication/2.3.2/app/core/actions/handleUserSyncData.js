import ELF from 'core/ELF';
import Logger from 'core/Logger';

let logger = new Logger('handleUserSyncData');

/**
 * Handle user sync data from a historical sync.
 * @param {Array<Object>} users - A collection of user data from the service.
 * @param {Users} collection - A collection of existing users.
 * @returns {Q.Promise<(number|object)>}
 */
export default function handleUserSyncData ({ res: users, collection }) {
    return Q.all(_.map(users, userData => {
        let userModel = collection.findWhere({
                // Matching by username creates a superfluous user record on cases of a duplicate user
                // existing on the server.
                // username: userData.username
                userId: userData.userId
            // eslint-disable-next-line new-cap
            }) || new collection.model();

        return userModel.save(userData)
            .then(() => collection.add(userModel));
    }))
    .then(() => collection.updateUserStatus())
    .catch((err) => {
        logger.error('Unexpected error', err);
        return { stopActions: true };
    });
}

ELF.action('handleUserSyncData', handleUserSyncData);
