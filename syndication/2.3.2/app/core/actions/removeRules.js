import ELF from '../ELF';

/**
 * Remove ELF rules by ID.
 * @param {Object} data - Action configuration.
 * @param {Array<string>} data.rules - An array of rule ID's to remove.
 * @param {Function} done - Invoked upon completion of the action.
 */
export function removeRules (data, done) {
    if (data.rules) {
        _.forEach(data.rules, function (id) {

            ELF.rules.remove(id);

        });
    }

    done();
}

ELF.action('removeRules', removeRules);