import ELF from 'core/ELF';

/**
 * Prevents the default functionality and any more rules from executing.
 * @return {Q.Promise<Object>}
 */
export default function preventAll () {
    return Q({ preventDefault: true, stopRules: true });
}

ELF.action('preventAll', preventAll);
