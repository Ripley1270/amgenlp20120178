import ELF from 'core/ELF';

/**
 * Navigate to a defined view.
 * @param {String} destination The destined view.
 * @param {Function} callback Invoked to retain the action chain.
 */
export function navigateTo (destination, callback) {
    LF.router.navigate(destination, true);

    (callback || $.noop)();
}

ELF.action('navigateTo', navigateTo);

// @todo remove
LF.Actions.navigateTo = navigateTo;