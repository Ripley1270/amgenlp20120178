import { Banner } from 'core/Notify';
import Spinner from 'core/Spinner';
import ELF from 'core/ELF';

/**
 * Disables the entire view and displays a message.
 * @param {string} param A resource string key.
 * @param {Function} callback A callback function invoked upon the action's completion.
 */
export function displayMessage (param, callback) {
    // Close any banners displayed and then show the spinner.
    Banner.closeAll();

    Spinner.show();
    (callback || $.noop)();
}

ELF.action('displayMessage', displayMessage);

// @todo remove
LF.Actions.displayMessage = displayMessage;
