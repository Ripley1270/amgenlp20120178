import Logs from 'core/collections/Logs';
import ELF from 'core/ELF';

/**
 * Deletes all the log entries included in the batch log provided.
 * @param {LF.Model.BatchLog} deleteBatch BatchLog containing logs to be deleted
 * @param {Function} callback Callback function to be invoked when the process has completed.
 */
export function deleteLogs (deleteBatch, callback) {
    let deleteLength,
        deleteLogs = new Logs(),
        step = function (counter) {
            // Test if logs.at(counter).id is zero, if so, skip
            if (deleteLogs.at(counter).id !== 0) {
                deleteLogs.destroy(deleteLogs.at(counter).id,
                    function () {
                        if (deleteLogs.length > counter) {
                            step(counter);
                        } else {
                            if (callback) {
                                callback();
                            }
                        }
                    }
                );
            } else {
                if (deleteLogs.length > counter + 1) {
                    step((counter + 1));
                } else {
                    if (callback) {
                        callback();
                    }
                }
            }
        };

    deleteLogs = deleteBatch.attributes.params;
    deleteLength = deleteLogs.length;

    if (deleteLength) {
        step(0);
    }
}

ELF.action('deleteLogs', deleteLogs);

LF.Actions.deleteLogs = deleteLogs;