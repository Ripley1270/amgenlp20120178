import ELF from 'core/ELF';
import { getDatabaseVersion } from 'core/Helpers';
import Logger from 'core/Logger';
import DatabaseVersion from 'core/models/DatabaseVersion';
import DatabaseVersions from 'core/collections/DatabaseVersions';

const logger = new Logger('databaseUpgradeCheck');

/**
 * Check for an update to the database.
 * @param  {Object}   params   The  database version type and upgrade configuration to check upgrades.
 * @returns {Q.Promise<void>}
 * @example
 * databaseUpgradeCheck({
 *     config      : StudyDBUpgrade,
 *     versionName : 'DB_Study'
 *  }, function () { });
 */
function databaseUpgradeCheck (params) {
    const { versionName, config } = params;

    const doUpgrades = (upgrades, dbVersion) => {

        // Do all upgrades sequentially.
        return upgrades.reduce((chain, upgrade) => {
            const { version, upgradeFunction, sql } = upgrade;

            logger.info(`Data Upgrade is detected for version ${version} of ${versionName}`);

            // eslint-disable-next-line consistent-return
            return chain.then(() => {
                if (sql) {
                    logger.warn(`SQL upgrades are no longer needed. Upgrade version: ${version}`);

                } else if (upgradeFunction) {

                    // Support upgradeFunctions that use callbacks or promises.
                    return Q.Promise((resolve) => {
                        let callbackDone, promiseDone;

                        const promise = upgradeFunction((completed) => {
                            callbackDone = true;

                            if (!promiseDone) {
                                resolve(completed);
                            }
                        });

                        if (promise && promise.then && !callbackDone) {
                            promiseDone = true;
                            promise.tap(resolve).done();
                        }
                    })
                        .then((completed) => {
                            if (!completed) {
                                return Q.reject(new Error(`Version ${version} of ${versionName}`));
                            } else {
                                return dbVersion.save({versionName, version});
                            }
                        });

                } else {
                    return Q.reject(
                        new Error(`No upgrade function configured for version ${version} of ${versionName}`));
                }
            });

        }, Q())
        .catch((err) => {
            err && logger.error('Database upgrade failed:', err);
        });
    };

    return getDatabaseVersion(versionName)
        // eslint-disable-next-line consistent-return
        .then((versionNumber = 0) => {
            const upgrades = _(config)
                .sortBy('version')
                .filter((item) => item.version > versionNumber);

            if (upgrades.length && versionNumber != null) {

                return DatabaseVersions.fetchCollection()
                    .then(versions => {
                        const dbVersion = versions.findWhere({versionName}) || new DatabaseVersion();
                        return doUpgrades(upgrades, dbVersion);
                    });
            }
        })
        .catch((err) => {
            err && logger.error('Database upgrade failed:', err);
            reject(err);
        });
}

ELF.action('databaseUpgradeCheck', databaseUpgradeCheck);

// @todo remove
LF.Actions.databaseUpgradeCheck = databaseUpgradeCheck;

export default databaseUpgradeCheck;
