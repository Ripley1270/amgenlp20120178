import ELF from '../ELF';

/**
 * Logout of the application
 * @param {null|undefined} params Not used in this action.
 * @param {Function} callback Invoked to retain the action chain.
 */
export function logout (params, callback) {
    LF.security.logout(true);
    callback();
}

ELF.action('logout', logout);

// @todo remove
LF.Actions.logout = logout;