import ELF from 'core/ELF';

/**
 * Calls the function that is passed as the parameter
 * @param {Function} param - Evaluates a condition and calls the the callback function to resume the action chain if the action chain execution should be continued.
 * @param {Function} callback - Callback function to be invoked when the process has completed.
 */
export function breakActionChain (param, callback) {
    if (_(param).isFunction()) {
        param(callback);
    }
}

ELF.action('breakActionChain', breakActionChain);

// @todo remove
LF.Actions.breakActionChain = breakActionChain;