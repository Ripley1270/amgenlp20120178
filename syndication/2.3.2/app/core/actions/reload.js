import ELF from 'core/ELF';

/**
 * Reload the view.
 * @returns {Q.Promise}
 */
export function reload () {
    document.location.reload();

    return Q();
}

ELF.action('reload', reload);