import { MessageRepo } from 'core/Notify';
import * as MessageHelpers from 'core/resources/Messages/MessageHelpers';
import ELF from 'core/ELF';
import Logger from 'core/Logger';
import Spinner from 'core/Spinner';

const logger = new Logger('actions/notify.js');

/**
 * Displays a notification modal with a button.
 * @param {Object} params The header, message to display and theme of popup.
 * @param {Function} [callback] The callback function invoked when the the user's input is submitted.
 * @returns {Q.Promise<void>}
 * @example
 * LF.Actions.notify({
 *     key  : 'DEACTIVATED_DIALOG'
 * });
 */
export default function notify (params, callback = $.noop) {
    const dialogVal = params.dialog || (MessageRepo.Dialog && MessageRepo.Dialog[params.key]);

    if (!dialogVal) {
        if (params.key) {
            logger.warn(`Dialog "${params.key}" not found in registry.`);
            return true;
        }

        logger.info('Notify dialog being shown without being registered, message will not be available for screenshots.');

        const dialog = MessageHelpers.notifyDialogCreator(params);

        return dialog()
        .then(() => {
            callback(true);
        });
    }
    return Spinner.hide()
    .then(()  => MessageRepo.display(dialogVal, params.options))
    .then(() => {
        callback(true);
        return true;
    });
}

ELF.action('notify', notify);

// @todo remove
LF.Actions.notify = notify;
