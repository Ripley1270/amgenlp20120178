import ELF from 'core/ELF';
import { isSitePad } from 'core/Utilities';
import { MessageRepo } from 'core/Notify';
import Logger from 'core/Logger';

const logger = new Logger('actions/setScreenOrientation.js');

/**
 * Sets screen orientation specified in questionnaire config. Supported values are
 * 'portrait' and 'landscape'.
 * @param {object} param specifies if default orientation should be used.
 * @return {Q.Promise<void>}
 */
export function setScreenOrientation (param) {
    let defaultOrientation = isSitePad() ? 'landscape' : 'portrait',
        newOrientation,
        screen,
        questionnaireOrientation,
        nativeScreen = window.screen,
        dialogKey,
        orientationChanged,
        showPopup = () => {
            if (!param.disablePopup && orientationChanged) {
                dialogKey = (newOrientation.indexOf('landscape') > -1) ?
                    MessageRepo.Dialog.SCREEN_ORIENTATION_LANDSCAPE_SET_CONFIRM : MessageRepo.Dialog.SCREEN_ORIENTATION_PORTRAIT_SET_CONFIRM;
                return MessageRepo.display(dialogKey);
            } else {
                return Q();
            }
        };

    if (param.setDefault) {
        newOrientation = defaultOrientation;
    } else {
        screen = this.data.screens[this.screen];
        questionnaireOrientation = this.model.get('orientation');
        newOrientation = screen.get('orientation') || questionnaireOrientation || defaultOrientation;
    }

    orientationChanged = nativeScreen.orientation && nativeScreen.orientation.indexOf(newOrientation) === -1;

    return showPopup()
    .then (() => {
        if (orientationChanged) {
            nativeScreen.lockOrientation(newOrientation);
        }
    })
    .catch(err => {
        logger.error(`error while setting screen orientation: ${err}`);
    });
}

ELF.action('setScreenOrientation', setScreenOrientation);
