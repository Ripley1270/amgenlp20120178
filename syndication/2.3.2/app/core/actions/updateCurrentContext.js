import CurrentContext from 'core/CurrentContext';

/**
 * Updates the current context data.
 * @returns {Q.Promise}
 */
export function updateCurrentContext () {
    return Q.all([CurrentContext().get('subjects').fetch(), CurrentContext().get('users').fetch()]);
}

ELF.action('updateCurrentContext', updateCurrentContext);
