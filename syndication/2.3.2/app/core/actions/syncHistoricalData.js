import Transmission from 'core/models/Transmission';
// TODO - Transmissions is defined, but the code uses LF.Collection.Transmissions
// eslint-disable-next-line no-unused-vars
import Transmissions from 'core/collections/Transmissions';
import ELF from 'core/ELF';
import Logger from 'core/Logger';

// Logger is defined, but not yet used.
// eslint-disable-next-line no-unused-vars
let logger = new Logger('syncHistoricalData');

/**
 * Syncs historical data with the Web Service using transmission queue.
 * @param {Object} params Includes collection and web service method names.
 * @param {boolean} [rejectOnError=false] If true rejects the promise returned
 * @return {Q.Promise<void>}
 */
export function syncHistoricalData (params, rejectOnError = false) {
    let method = 'historicalDataSync',
        model = new Transmission(),
        subjectKrpt = params.subject && params.subject.get('krpt');

    if (params.collection === 'LastDiaries' && subjectKrpt) {
        params.krpt = subjectKrpt;
    }

    return model.save({
        method,
        params: JSON.stringify(params),
        created: new Date().getTime()
    })
    // TODO: should we change LF.Collection.Transmissions to transmissions?
    // May be it is there to prevent a circular dependency issue.
    .then(() => LF.Collection.Transmissions.fetchCollection())
    .then(transmissions => {
        let syncTransmission = transmissions.findWhere({ method });

        return Q.Promise((resolve, reject) => {
            transmissions.execute(transmissions.indexOf(transmissions.get(syncTransmission)), (response) => {
                if (response && response.error && rejectOnError) {
                    reject();
                } else {
                    resolve();
                }
            });
        });
    });
}

ELF.action('syncHistoricalData', syncHistoricalData);
