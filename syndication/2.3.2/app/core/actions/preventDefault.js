import ELF from '../ELF';

/**
 * Prevent the default action from executing.
 * @returns {Q.Promise<{preventDefault:true}>} flag to ELF.trigger'er
 */
export function preventDefault () {
    return { preventDefault: true };
}

ELF.action('preventDefault', preventDefault);
