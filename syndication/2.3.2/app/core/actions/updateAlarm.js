import { getScheduleModels } from 'core/Helpers';
import { query } from 'core/utilities';
import ActiveAlarms from 'core/collections/ActiveAlarms';
import ELF from 'core/ELF';
import AlarmManager from 'core/AlarmManager';
import Logger from 'core/Logger';

const logger = new Logger('actions/updateAlarm');

/**
 * Update an Alarm and all reminders.
 * @param {Object} params The target id in the schedule.
 * @param {Function} callback The callback function invoked when the the user's input is submitted.
 * @returns {Q.Promise<void>}
 */
export function updateAlarm (params) {

    return ActiveAlarms.fetchCollection()
    // eslint-disable-next-line consistent-return
    .then(activeAlarms => {
        const matchedSchedule = _.find(getScheduleModels(), schedule => {
            const target = schedule.get('target');
            return target && target.id === params.id;
        });

        if (matchedSchedule && matchedSchedule.get('alarmParams')) {
            const currentAlarms = query(activeAlarms.toJSON(), {
                where: { schedule_id: matchedSchedule.get('id') },
                order: { field: 'id' }
            });

            // eslint-disable-next-line consistent-return
            return Q.all(currentAlarms.map(alarm => {
                const item = activeAlarms.get(parseInt(alarm.id, 10));
                AlarmManager.cancelAlarm(alarm.id);

                // Not removing from ActiveAlarms if the repeat is 'once' to prevent from being rescheduled again.
                if (item.get('repeat') !== 'once') {
                    return item.destroy();
                }
            }));
        }

    })
    .catch(err => {
        logger.error('Error updating alarms', err);
    });
}

ELF.action('updateAlarm', updateAlarm);

// @todo remove
LF.Actions.updateAlarm = updateAlarm;
