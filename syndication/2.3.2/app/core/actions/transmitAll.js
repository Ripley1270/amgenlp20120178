
// TODO - Transmissions is imported, by the method uses LF.Collection.Transmission.
// eslint-disable-next-line no-unused-vars
import Transmissions from 'core/collections/Transmissions';
import ELF from 'core/ELF';
import Logger from 'core/Logger';

const logger = new Logger('transmitAll');

/**
 * Transmits every item in the transmission queue.
 * @param {null|undefined} params Additional parameters passed to the action from ELF. NOT used in this action.
 * @param {Function} callback Callback function to be invoked when the process has completed.
 */
export function transmitAll (params, callback) {
    let connectionType,
        transmissions = new LF.Collection.Transmissions(), // stakutis hack
        pullQueue = () => {
            transmissions.pullQueue(() => {
               let transmissionsToRemove = transmissions.filter((transmission) => {
                    return !!transmission.get('status');
                });
                transmissionsToRemove.map((transmission) => {
                    transmissions.remove(transmission);
                });
                transmissions.executeAll(callback);

            });
        };

    // @TODO: Extract this block into utilities. This block needs major refactoring
    LF.Wrapper.exec({
        execWhenWrapped: () => {
            let resolveType = (type) => {
                let none,
                    unknown,
                    Connection = window.Connection,
                    plugin = window.plugin,
                    platform = LF.Wrapper.platform;

                if (platform === 'windows') {
                    none = Windows.Networking.Connectivity.NetworkConnectivityLevel.none;
                    unknown = 'Unknown connection';
                } else if (plugin && plugin.networkType) {
                    // evaluate strings if running on wrapper version 1.8 and up
                    none = plugin.networkType.connections.NONE;
                    unknown = plugin.networkType.connections.UNKNOWN;
                } else if (Connection != null) {
                    // evaluate strings if running on wrapper version 1.7 and older
                    none = Connection.NONE;
                    unknown = Connection.UNKNOWN;
                }

                connectionType = (type === none) ? unknown : type;
                logger.info(`Starting a transmission using ${connectionType}`);
                pullQueue();
            },
            netWorkType = LF.Wrapper.getConnectionType();

            // if running on 1.8 wrapper, this will be undefined
            if (netWorkType) {
                resolveType(netWorkType);
            }
        },
        execWhenNotWrapped: pullQueue
    });
}

ELF.action('transmitAll', transmitAll);

// @todo remove
LF.Actions.transmitAll = transmitAll;
