import ELF from 'core/ELF';
import State from 'core/classes/State';

/**
 * Set the state of the application.
 * @param {string} toState The state to set. See core/classes/State for a list of avialable states.
 * @returns {Q.Promise<void>}
 */
export default function setState (toState) {
    return Q.Promise(resolve => {
        let curState = new State();

        curState.set(toState);

        resolve();
    });
}

ELF.action('setState', setState);
