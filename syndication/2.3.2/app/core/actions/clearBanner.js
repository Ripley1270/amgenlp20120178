import { Banner } from 'core/Notify';
import ELF from 'core/ELF';

/**
 * Clears any displayed banners.
 * @returns {Q.Promise<void>}
 */
export default function clearBanner () {
    Banner.closeAll();

    return Q();
}

ELF.action('clearBanner', clearBanner);
