import Transmission from 'core/models/Transmission';
import Transmissions from 'core/collections/Transmissions';
import ELF from 'core/ELF';

/**
 * Transmit with the Web Service using transmission queue.
 * @param {String} params The transmit method name that is executed by the transmission queue.
 * @param {Function} callback A callback function invoked upon completion of the action.
 */
export function transmit (params, callback) {
    let model = new Transmission(),
        transmission = new Transmissions();

    model.save({
        method  : params,
        created : new Date().getTime()
    }, {
        onSuccess: function () {
            transmission.pullQueue(() => {
                transmission.executeAll(callback);
            });
        }
    });
}

ELF.action('transmit', transmit);

// @todo remove
LF.Actions.transmit =  transmit;