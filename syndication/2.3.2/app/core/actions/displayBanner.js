import { MessageRepo } from 'core/Notify';

/**
 * Displays a banner by key.
 * @param {string} key - The key of the banner to display.
 * @returns {Q.Promise<void>}
 * @example
 *  resolve :[{
 *      action: 'displayBanner',
 *      data: 'USER_EDITED'
 *  }]
 */
export default function displayBanner (key) {
    MessageRepo.display(MessageRepo.Banner[key]);

    return Q();
}

ELF.action('displayBanner', displayBanner);
