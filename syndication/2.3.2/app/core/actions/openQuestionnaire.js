import Data from '../Data';
import ELF from '../ELF';

/**
 * Open a specified questionnaire.
 * @param {Object} params Includes the id of the questionnaire and schedule id
 * @param {Function} callback Callback invoke to retain the action chain.
 * @example LF.Actions.openQuestionnaire({
 *               questionnaire_id: 'Daily_Diary',
 *               schedule_id     : 'Daily_DiarySchedule1'
 *           }, function () {});
 */
export function openQuestionnaire (params, callback) {
    // TODO: This is only used in LogPad and won't work for SitePad. Move it from core to logpad

    //DE12483: Delay opening questionnaire to prevent the issue of questionnaire screen display
    _.delay(function () {
        Data.Questionnaire = {};
        
        LF.router.navigate(`questionnaire/${params.questionnaire_id}/${params.schedule_id}`, true);

        callback();
    }, 150);
}

ELF.action('openQuestionnaire', openQuestionnaire);

// @todo remove
LF.Actions.openQuestionnaire = openQuestionnaire;