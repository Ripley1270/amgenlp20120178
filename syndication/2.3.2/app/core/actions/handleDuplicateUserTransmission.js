import Logger from 'core/Logger';
import ELF from 'core/ELF';

// Logger is declared, but not yet used in file.
// eslint-disable-next-line no-unused-vars
let logger = new Logger('handleDuplicateUserTransmission');

/**
 * Handles the user transmission with the duplicate error.
 * @param {Object} params - Parameters provided by the ELF rule.
 * @param {User} params.user - The duplicate user of which transmission has failed.
 * @param {Object} params.transmissionItem - The failed transmission item.
 * @returns {Q.Promise<Object>}
 */
export function handleDuplicateUserTransmission (params) {
    return params.user.save({
        username: `${params.user.get('username')}*`,
        isDuplicate: true
    })
    .then(() => params.transmissionItem.save({ status: 'failed' }))
    .then(() => ({ preventDefault: true }));
}

ELF.action('handleDuplicateUserTransmission', handleDuplicateUserTransmission);