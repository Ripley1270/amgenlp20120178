import ReviewScreen from 'core/widgets/ReviewScreen';
import ELF from 'core/ELF';

/**
 * Performs an action when button is clicked on the Review Screen.
 * @param {Object} params An object that contains button and item selected on the Review Screen.
 * @param {Function} callback Invoked to retain the action chain.
 */
export function reviewScreenButtonAction (params, callback) {
    ReviewScreen[params.button.actionFunction](params, callback);
}

ELF.action('reviewScreenButtonAction', reviewScreenButtonAction);

// @todo remove
LF.Actions.reviewScreenButtonAction = reviewScreenButtonAction;