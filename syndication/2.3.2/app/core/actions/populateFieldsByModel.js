import ELF from 'core/ELF';
import Answer from 'core/models/Answer';
import Answers from 'core/collections/Answers';
import { getNested } from 'core/utilities';

/**
 * @memberOf ELF.actions
 * @description
 * Populates answers based on configuration.  Should only be used
 * in the context of a questionnaire.
 * @param {Array<Object>} config - Configuration of fields to populate.
 * @returns {Q.Promise<Answers>}
 * @example
 *  resolve: [{
 *      // This action populates answer records based on a configured model.
 *      action: 'populateFieldsByModel',
 *      data: [{
 *          SW_Alias: 'Deactivate_User.0.DEACTIVATE_USER_1',
 *          question_id: 'DEACTIVATE_USER_USERNAME',
 *          questionnaire_id: 'Deactivate_User',
 *          // The property on this response is determine by the 'field' property
 *          // on the question's widget configuration. e.g. { username: 'jsmith' }
 *          // Indicate that the response is JSON.
 *          isJSON: true,
 *          // Determine the field name of the JSON response. e.g. { username: ... }
 *          field: 'username',
 *          // Use the user model scoped to the UserStatusQuestionnaireView (this.user).
 *          model: 'user',
 *          // Use the user model's username property to populate the response.
 *          // e.g. this.user.get('username');
 *          property: 'username'
 *      }]
 *  }]
 */
export default function populateFieldsByModel (config) {
    let answers = new Answers();

    // Loop through each configured answer.
    _.forEach(config, (answer) => {
        // Populate the static, configured fields
        let model = new Answer(_.pick(answer, 'SW_Alias', 'question_id', 'questionnaire_id'));

        // Use getNested to parse out the configured model from the context.
        // This allows for configuring a model deep within the context.
        // e.g. model: 'parent.child.model'
        let source = getNested(this, answer.model);

        // DE17443 - If the source has not been found, the response will be an empty string.
        // TODO: Make a default response configurable?
        let response = source != null ? source.get(answer.property) : '';

        // If the response should be JSON, format it as such.
        if (answer.isJSON) {
            // Use the provided field name, of if none is configured, use the property name.
            model.set('response', `{ "${answer.field || answer.property}" : "${response}" }`);
        } else {
            model.set('response', response);
        }

        // Add the newly created answer record to the answers collection.
        answers.add(model);
    });

    // Here we override the original answers collection.
    // This might have to be modified to be a merge, or add.
    this.data.answers = answers;

    return Q(answers);
}

ELF.action('populateFieldsByModel', populateFieldsByModel);
