import ELF from 'core/ELF';

/**
 * Do nothing.
 * @returns {Q.Promise<void>}
 */
export function doNothing () {
    return Q();
}

ELF.action('doNothing', doNothing);