import ELF from 'core/ELF';
import * as syncHistoricalData from 'core/actions/syncHistoricalData';

/**
 * Triggers the syncHistoricalData action for syncing Users.
 * @param {boolean} [rejectOnError=false] If true rejects the promise returned
 * @return {Q.Promise<void>}
 */
export function syncUsers (rejectOnError = false) {
    return syncHistoricalData.syncHistoricalData({
        collection: 'Users',
        webServiceFunction: 'syncUsers',
        activation: false
    }, rejectOnError);
}

ELF.action('syncUsers', syncUsers);
