
import SpinnerView from './views/SpinnerView';

let instance = null;

const Spinner = {
    get instance () {
        if (!instance) {
            instance = new SpinnerView();
        }
        return instance;
    },

    hide () {
        return instance ? instance.hide() : Q();
    }
};

Spinner.show = (...args) => Spinner.instance.show(...args);

export default Spinner;
