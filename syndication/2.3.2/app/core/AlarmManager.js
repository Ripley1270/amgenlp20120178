import Data from 'core/Data';
import Logger from 'core/Logger';
import Schedules from 'core/collections/Schedules';
import ELF from 'core/ELF';

/**
 * This module is used as an API to the Alarms on the OS
 */
let AlarmManager = { },
    logger = new Logger('AlarmManager');

/**
 * Pending alarms array
 */
AlarmManager.pendingAlarms = [];

/**
 * Triggers the study rule based on the alarm
 * @param {Object} alarmConfig - configuration of an alarm
 */
AlarmManager.triggerAlarmRule = alarmConfig => {
    let scheduleId = alarmConfig.scheduleId ?  `/${alarmConfig.scheduleId}` : '',
        isTimeoutPaused = !LF.security.timeOn;

    logger.operational(`Alarm ID of ${alarmConfig.id} with Schedule ID of '${alarmConfig.scheduleId} Triggered`);

    if (!isTimeoutPaused) {
        LF.security.pauseSessionTimeOut();
    }

    // TODO: Use join?!?!?!
    ELF.trigger(`ALARM:Trigger${scheduleId}`, { alarmConfig }, window)
    .then(res => {
        if (!res.preventDefault) {
            AlarmManager.soundAlarm(alarmConfig);
        }
    })
    .finally(() => {
        // If we were paused prior to the alarm sounding, we want to keep it paused.
        if (!isTimeoutPaused) {
            LF.security.restartSessionTimeOut();
        }

        // Alarm successfully triggered. Release the WakeLock that keeps the screen on. All rules for this alarm have been run.
        window.cordova && cordova.plugins.notification.local.releaseLock();
    })
    .done();
};

/**
 * Add an alarm config to the pending alarms array
 * @param {Object} alarmConfig - configuration of an alarm
 */
AlarmManager.addPendingAlarm = alarmConfig => {
    AlarmManager.pendingAlarms.push(alarmConfig);
};

/**
 * Sounds all of the pending alarms
 * @param {Object} subject - subject model for the alarms
 */
AlarmManager.soundAllPendingAlarms = (subject) => {
    let schedules = LF.Helpers.getScheduleModels();

    _(AlarmManager.pendingAlarms).forEach((alarmConfig) => {
        // eslint-disable-next-line consistent-return
        let foundSchedule = _.find(schedules, (scheduleModel) => {
            if (alarmConfig.scheduleId === scheduleModel.id) {
                return scheduleModel;
            }
        });

        let scheduleCollection = new Schedules([foundSchedule]);

        LF.schedule.determineAlarmSchedules(scheduleCollection, { subject }, (availableSchedules) => {
            if (availableSchedules.length > 0) {
                AlarmManager.triggerAlarmRule(alarmConfig);
            } else {
                // Alarm not triggered. Release the WakeLock for this alarm. Schedule is no longer valid.
                window.cordova && cordova.plugins.notification.local.releaseLock();
            }
        });
    });

    AlarmManager.pendingAlarms = [];
};

/**
 * Sounds the alarm on the native OS
 * @param {Object} alarmConfig - configuration of an alarm
 */
AlarmManager.soundAlarm = alarmConfig => {
    window.cordova && cordova.plugins.notification.local.displayNotification(alarmConfig);
};

/**
 * Alarm trigger function. Entry point in JavaScript when an alarm is triggered.
 * @param {Object} notification - configuration of the alarm currently being triggered
 */
AlarmManager.alarmTrigger = notification => {
    let triggerObj = JSON.parse(notification.data),
        currentView = LF.router.view(),
        addAlarm = true;

    if (currentView instanceof LF.View.QuestionnaireView) {
        AlarmManager.pendingAlarms.forEach((alarmConfig) => {
            if (alarmConfig.scheduleId && alarmConfig.scheduleId === triggerObj.alarmConfig.scheduleId) {
                addAlarm = false;
            }
        });

        if (addAlarm) {
            AlarmManager.addPendingAlarm(triggerObj.alarmConfig);
        } else {
            // Alarm not triggered. Release the WakeLock for this alarm. This is a duplicate of a pending alarm.
            window.cordova && cordova.plugins.notification.local.releaseLock();
        }
    } else {
        AlarmManager.triggerAlarmRule(triggerObj.alarmConfig);
    }
};

/**
 * Add an alarm
 * @param {Number} id - unique id of the alarm
 * @param {Date} date - when the alarm should first appear
 * @param {String} title - title of the message
 * @param {String} message - the message that is displayed
 * @param {String|Number} repeat - interval at which the alarm repeats
 * @param {String} scheduleId - the scheduleId of the alarm
 * @param {Object} alarmVolumeConfig - path to the sound file for the pending alarm
 */
AlarmManager.addAlarm = (id, date, title, message, repeat, scheduleId, alarmVolumeConfig) => {
    let volumeConfig = alarmVolumeConfig,
        alarmObj = {},
        preferred = LF.Preferred || { language: LF.StudyDesign.defaultLanguage, locale: LF.StudyDesign.defaultLocale };

    if (!volumeConfig) {
        volumeConfig = LF.StudyDesign.alarmVolumeConfig || volumeConfig;
    }

    alarmObj.alarmConfig = {
        scheduleId,
        title,
        id: id.toString(),
        at: date,
        text: message,
        every: repeat,
        sound: (volumeConfig && volumeConfig.sound) ? volumeConfig.sound : null,
        direction: LF.strings.getLanguageDirection(preferred),
        language: [preferred.language, preferred.locale].join('-')
    };

    if (volumeConfig) {
        _.extend(alarmObj.alarmConfig, volumeConfig);
    }

    window.cordova && cordova.plugins.notification.local.schedule({
        title,
        id: id.toString(),
        at: date,
        text: message,
        every: repeat,
        data: JSON.stringify(alarmObj)
    });
};

/**
 * Cancel one alarm
 * @param {String} id - unique id of the alarm to cancel
 */
AlarmManager.cancelAlarm = id => {
    window.cordova && cordova.plugins.notification.local.cancel(id);
};

/**
 * Cancel all alarms
 */
AlarmManager.cancelAllAlarms = () => {
    window.cordova && cordova.plugins.notification.local.cancelAll();
};

/**
* Generate a list of ids for alarms and reminders
* @param {Number} start - a unique seed id
* @param {Number} reminders - the number of reminders
* @returns {Number[]} array of numbers to use for alarm ids
*/
AlarmManager.generateIds = (start, reminders) => {
    return _.range((reminders || 0) + 1).reduce((idList, i) => {
        idList.push(start * 1000 + i);
      	return idList;
    }, []);
};

/**
 * Gets the number of reminders in future for an Alarm on assignment.
 * @param {Number} initialTime Initial time of an Alarm in milliseconds
 * @param {Number} reminders Number of reminders with the Alarm
 * @param {Number} interval Interval in minutes for the reminders
 * @returns {Number} Number of reminders in future.
 * @example var reminders = LF.Wrapper.Utils.getReminders(now.getTime(), 4, 15);
 */
AlarmManager.getReminders = (initialTime, reminders, interval) => {
    let startTime, endTime, now, minLeft,
        milliSecInMin = 60000;

    if (reminders == null || interval == null) {
        return 0;
    }

    startTime = Math.floor(initialTime / milliSecInMin);
    endTime = startTime + (reminders * interval);
    now = Math.floor(LF.Utilities.getNow().getTime() / milliSecInMin);

    if (now > endTime) {
        return 0;
    } else {
        minLeft = endTime - now;
        return Math.ceil(minLeft / interval);
    }
};

/*
 * Register Trigger event with local notification plugin
 */
AlarmManager.registerPluginEvents = () => {
    document.addEventListener('deviceready', () => {
        window.cordova && cordova.plugins.notification.local.on('trigger', (notification) => {
            // TODO: Instead of using the Data namespace pass the alarmId through an ELF trigger.
            Data.lastTriggeredAlarmId = parseInt(JSON.parse(notification.data).alarmConfig.id, 10);
            AlarmManager.alarmTrigger(notification);
        });

        window.cordova && cordova.plugins.notification.local.ready();
    });
};

export default AlarmManager;
