import PageView from 'core/views/PageView';
import NotifyView from 'core/views/NotifyView';
import Subjects from 'core/collections/Subjects';

import Logger from 'core/Logger';
import { restartApplication } from 'core/utilities';


const logger = new Logger('ScreenshotView');

/**
 * @class ScreenshotView
 * @description Screenshot Configuration view.
 */
export default class ScreenshotView extends PageView {
    constructor (options) {
        this.id = 'screenshot-config';

        /**
        * The view's events.
        * @readonly
        * @enum {Event}
        */
        this.events = {
            'click .check-all': 'selectAll',

            'click .uncheck-all': 'deselectAll',

            /** Event handlers for checkbox selection */
            'click li.list-item > span': 'handleSelection',

            'change input[type=checkbox]': 'handleSelection',

            /** Click event for next button */
            'click #next': 'nextHandler',

            /** Click event for back button */
            'click #back': 'back'
        };

        super(options);

        this.template = '#screenshot-template';

        /**
         * Strings to fetch in the render function
         * @type Object
         */
        this.templateStrings = _.defaults({}, options.strings, {
            header: 'APPLICATION_HEADER'
        });
    }

    /**
     * Restarts application, navigating to code entry.
     * @param {Event} e Event object.
     */
    back () {
        // DE18816 - Logpad Only - Navigation was broken when clicking the back button.
        // A 'screenshot' variable is set in localStorage when submitting the CodeEntryView with a Study URL of 'screenshot'.
        // This value needs to be removed before restarting the application, so the correct view will display.
        localStorage.removeItem('screenshot');

        // DE17565 - A fake subject is created when screenshot mode is initialized.
        // This subject has to be removed for the LPA or the application will attempt
        // to navigate to the login view.
        Subjects.clearStorage()
        .finally(() => {
            restartApplication();
        })
        .done();
    }

    /**
     * Go to the given next screen.
     * @param {string} url - The url we are navigating to.
     */
    next (url) {
        this.navigate(url);
    }

    /**
     * Determine where to go next, or display an error message.
     * @returns {Q.Promise|undefined}
     */
    nextHandler () {
        let languages = this.findCheckedItems();

        if (!(languages.length)) {
            return new NotifyView().show({
                header: '',
                body: 'Please select one or more languages',
                ok: 'OK',
                type: 'error'
            });
        }

        this.model.get('selection').set('languages', languages);

        return this.next('screenshot_mode');
    }

    /**
     * If we are navigating back to this screen, display what was chosen previously.
     * @returns {Q.Promise}
     */
    showPrevCheckedItems () {
        return Q().then(() => {
            let itemsChecked = this.model.get('selection').get('languages');

            if (itemsChecked.length) {

                let allListItems = Array.prototype.slice.call(this.$checkboxGroup.find('li.list-item'));

                itemsChecked.map(item => {
                    return allListItems.map(listItem => {
                        // this.selectItem(listItem);
                        return item === $(listItem).data('value') ? this.selectItem($(listItem)) : false;

                    });
                });

            }
        })
        .catch(e => logger.error(e));
    }

    /**
     * Get the selected items.
     * @returns {string[]} The selected Items.
     */
    findCheckedItems () {
        let selectedAnswers = this.$el.find('li.checked');

        selectedAnswers = Array.prototype.slice.call(selectedAnswers);

        return selectedAnswers.map(answer => $(answer).data('value') );
    }

    /**
     * Render the view
     * @return {Q.Promise}
     */
    render () {
        return this.buildHTML({
            title: 'Take Screenshots',
            mode: 'language'
        })
        .then(() => {
            this.page();
            this.delegateEvents();

            this.$checkboxContainer = this.$('.screenshot-checkbox-container');
            this.$checkboxGroup = this.$('#screenshot-checkbox-group');
            this.$screenshotBtnContainer = this.$('.screenshot-btn-container');

        })
        //add check for new strings here
        .then(() => this.addLanguagesList())
        .then(() => this.showPrevCheckedItems())
        .catch(err => {
            err && logger.error('An error occurred rendering the screenshot view', err);
        });
    }

    /**
     * Add the list of languages
     * @returns {Q.Promise}
     */
    addLanguagesList () {
        let langcodes = _.uniq(LF.strings.map((m) => {
            return `${m.attributes.language}-${m.attributes.locale}`;
        })),
        unorderedList = $('<div>');

        // Display the languages as checkbox options.
        return Q.all(langcodes.map(langcode => {
            return LF.getStrings(langcode.toUpperCase(), (langName) => {

                let listItem = $(`<li class="list-item"><input type="checkbox"><span>${langName}</span></li>`)
                    .attr('data-value', langcode);

                return unorderedList.append(listItem);
                //deal w/ multiple namespaces
            }) || Q(unorderedList.append($(`<li class="list-item"><input type="checkbox"><span>${langcode.toUpperCase}</span></li>`)
                    .attr('data-value', langcode)));
        }))
        .then(() => {
            this.renderListItems(unorderedList);
        })
        .catch(err => {
            logger.error('Error occurred rendering languages', err);
        });
    }

    /**
     * Render the list to the screen.
     * @param {any} list - jQuery object of the list to display.
     */
    renderListItems (list) {
        this.$checkboxGroup.html(list.html());
    }

    /**
     * React to input event to select a list item.
     * @param {Object} e - input event
     */
    handleSelection (e) {
        e.preventDefault();
        e.stopImmediatePropagation();

        let $parentListItem = $(e.target).closest('li');
        $parentListItem.hasClass('checked') ? this.deselectItem($parentListItem) : this.selectItem($parentListItem);

    }

    /**
     * Select the item.
     * @param {any} listItem - jQuery object for the item to select
     */
    selectItem (listItem) {
        listItem.context || (listItem = $(listItem));
        listItem.not('.disabled')
            .addClass('checked')
            .attr('selected', true)
            .children('input:not(:disabled)')
            .attr('checked', true);
    }

    /**
     * Deselect the item.
     * @param {any} listItem - jQuery object for the item to deselect
     */
    deselectItem (listItem) {
        listItem.context || (listItem = $(listItem));
        listItem.not('.disabled')
            .removeClass('checked')
            .attr('selected', false)
            .children('input:not(:disabled)')
            .removeAttr('checked');
    }

    /**
     * Checks all checkboxes
     */
    selectAll () {
        this.$checkboxGroup
            .find('li.list-item').not('.disabled')
            .map((idx, listItem) => {
                this.selectItem(listItem);
            });
    }

    /**
     * Unchecks all checkboxes
     */
    deselectAll () {
        this.$checkboxGroup
            .find('li.list-item').not('.disabled')
            .map((idx, listItem) => {
                this.deselectItem(listItem);
            });
    }
}
