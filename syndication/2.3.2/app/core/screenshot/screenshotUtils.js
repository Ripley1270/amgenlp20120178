import Logger from 'core/Logger';
import Language from 'core/models/Language';
import NotifyView from 'core/views/NotifyView';

const logger = new Logger('TakeScreenshot');

export default class ScreenshotUtils {
    /**
     * Takes a screenshot of what is currently being displayed and saves it to the device
     * @param {object} [options] - options
     * @param {string} options.filename - filename
     * @param {string} options.foldername - foldername
     * @param {string} options.extension - extension
     * @param {number} options.quality - ßquality
     * @returns {Q.Promise<*>}
     */
    static takeScreenshot ({ filename, foldername, extension = 'png', quality = 100 } = {}) {
        return Q.Promise((resolve, reject) => {
            if (!navigator.screenshot) {
                logger.trace('No screenshot plugin detected');
                resolve(false);
            }

            navigator.screenshot.save((error, res) => {
                if (error) {
                    reject(error);
                } else {
                    logger.info(`Screenshot saved to: ${res.filePath}`);
                    resolve(res);
                }
            }, extension, quality, filename, foldername);
        });
    }

    /**
     * Takes language formatted json files from the ./Download/nls directory of the phone and adds them the LF.strings for use in screenshot tool
     * @param {Object} view      first view to be rendered post loading the strings
     */
    static addScreenShotStrings (view) {
        let path = `${cordova.file.externalRootDirectory}Download/nls`,
            fail = () => {
                logger.info('Unable to add Screen Shot Strings. nls folder may be missing. Continuing screenshot mode with the default strings.');
                return view.render();
            },

            /**
             * reads the contents from all of the file objects
             * @param {Array} files all of the FileEntry file objects
             */
            readMultiFiles = (files) => {
                let readFile = (index) => {
                    let reader = new FileReader();

                    if (index >= files.length || typeof files[index] === 'undefined') {
                        return view.render();
                    }

                    reader.onloadend = (evt) => {
                        // actually get the file contents
                        let data = evt.target.result,
                            complete = () => {
                                readFile(index + 1);
                            },
                            result,
                            languageMatch;

                        try {
                            result = JSON.parse(data);
                            languageMatch = LF.strings.match({ namespace: result.namespace, locale: result.locale, language: result.language });

                            if (languageMatch[0]) {
                                languageMatch[0].set('resources', result.resources);
                                languageMatch[0].set('dates', result.dates);
                                complete();
                            } else {
                                let startSize = LF.strings.models.length,
                                    check = () => {
                                        if (LF.strings.models.length === startSize) {
                                            setTimeout(check, 8000);
                                        } else {
                                            complete();
                                        }
                                    },
                                    newLang = new Language(result);

                                LF.strings.add(newLang);
                                check();
                            }
                        } catch (e) {
                            let localURL = evt.target._localURL,
                                fileName = localURL.substr(localURL.search('nls')),
                                errorMsg = `${fileName} is improperly formatted JSON and will not be added`;

                            logger.error(errorMsg, e);

                            return new NotifyView().show({
                                header : '',
                                body   : errorMsg,
                                ok     : 'OK',
                                type   : 'error'
                            }).then(complete);
                        }
                        return Q.resolve();
                    };

                    return reader.readAsText(files[index]);
                };

                readFile(0);
            },

            /**
             * Gets all File and directory entries from the directoryReader
             * @param {Array} entries File and Diretory Entries
             */
            gotDirectoryEntries = (entries) => {
                let files = [];

                entries = entries.filter((entry) => entry.isFile && entry.name.substr(-4) === 'json');

                Q.all(entries.map((entry) => {
                    return Q.promise((resolve) => {
                        entry.file((file) => {
                            files.push(file);
                            resolve();
                        });
                    });
                }))
                .then(() => readMultiFiles(files))
                .done();
            },

            /**
             * creates and triggers the directoryReader
             * @param {Object} directoryEntry the directory to read from
             */
            gotDirectoryEntry = (directoryEntry) => {
                let directoryReader = directoryEntry.createReader();
                directoryReader.readEntries(gotDirectoryEntries, fail);
            };

        window.resolveLocalFileSystemURL(path, gotDirectoryEntry, fail);
    }
}
