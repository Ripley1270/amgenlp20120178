import DropDownListDetector from './DetectDropdownList';

/**
 * Add an ELF Rule to intercept dynamic text.
 * @param {object} options - The options
 * @param {string} options.id - The id for the elf rule
 * @param {string} options.salience - The salience for the elf rule
 * @param {function|string} options.action - The action called when DynamicText is triggered.
 */
export function addDynamicTextTriggerRule ({ id, action, salience = 1 }) {
    if (ELF.rules.find(id)) {
        ELF.rules.remove(id);
    }

    ELF.rules.add({
        id,
        salience,
        trigger: 'DYNAMICTEXT:Trigger',
        evaluate: true,
        resolve: [{ action }]
    });
}

/**
 * A detector that will detect when dynamic text is used,
 *  then loop through the provided screenshot values and re-render the screen with each value.
 */
export class DynamicTextDetector {
    constructor (renderFunc, takeScreenshot, cleanup = $.noop) {
        this.ELFId = 'DynamicTextScreenshot';

        this.renderFunc = renderFunc;
        this.takeScreenshot = takeScreenshot;
        this.cleanup = cleanup;

        this.isDetected = false;
        this.namingIndex = 1;
    }

    /**
     * Function that sets up a listener that will call "detected" when the feature is detected.
     * @returns {*}
     */
    setupDetectionListener () {
        return addDynamicTextTriggerRule({
            id: this.ELFId,
            salience: 1,
            action: ({ key, dynamicText }) => this.detected(key, dynamicText)
        });
    }

    /**
     * Parse the dynamic text's screenshot options and retrieve the list of values.
     * @param {object} dynamicText - The dynamic Text object
     * @return {Q.Promise<Array<string>>} Array of all screenshot values.
     */
    parseDynamicTextValues (dynamicText = {}) {
        _.defaults(dynamicText, {
            screenshots: {}
        });

        _.defaults(dynamicText.screenshots, {
            values: [],
            getValues: $.noop
        });

        const { values, getValues, keys } = dynamicText.screenshots;

        // Start with static values
        return Q(values)

        // Fetch provided strings, omitting options which is provided in the same object for convenience.
        .then((valuesList = []) => {
            if (keys) {
                return LF.getStrings(_.omit(keys, 'options'), $.noop, keys.options || {})
                .then(strings => {
                    // Add fetched strings to return list.
                    return valuesList.concat(_.values(strings));
                });
            }

            return valuesList;
        })

        // Get values from provided function and add to list.
        .then((valuesList = []) => {
            return Q(getValues())
            .then((runtimeValues = []) => {
                return valuesList.concat(runtimeValues);
            });
        })

        // Add message if still empty.
        .then((valuesList = []) => {
            if (valuesList.length === 0) {
                valuesList.push('{{ No dynamic text screenshot values provided }}');
            }

            return valuesList;
        });
    }

    /**
     * Increment the current value of an item to use for the next screenshot.
     * @param {string} key - The key of the item to increment current value.
     * @returns {string}
     */
    increment (key) {
        const item = this.list.get(key);
        const text = item.get('values')[item.get('index')];

        if (item.get('index') < item.get('values').length - 1) {
            item.set('index', item.get('index') + 1);
        } else {
            item.set('completed', true);
        }

        return text;
    }

    /**
     * Run when DynamicText is detected, then sets up or continues the dynamic text screenshot loop, resolving the next value to display for screenshots.
     * @param {String} key - The key of the dynamicText function
     * @param {object} dynamicText - The dynamicText object.
     * @returns {Q.Promise<Object>} object containing the evaluated dynamic text.
     */
    detected (key, dynamicText) {
        return Q().then(() => {
            this.isDetected = true;

            if (!this.list) {
                // Used to track multiple dynamic text on a screen.
                this.list = new Backbone.Collection();
            }
        })
        .then(() => {
            if (!this.list.has(key)) {
                return this.parseDynamicTextValues(dynamicText)
                .then((values = []) => {
                    this.list.add({
                        values,
                        id: key,
                        index: 0,
                        completed: false
                    });
                });
            }
            return Q();
        })
        .then(() => this.increment(key))
        .then(text => {

            // ELF Trigger expects an object.
            return {
                text
            };
        });
    }

    /**
     * Render using the provided renderFunc, then run provided takeScreenshot function
     * and determine if there are more values to display for the dynamic text on this screen.
     * @returns {Q.Promise<any>}
     */
    run () {
        if (!this.isDetected || !this.list || this.list.isEmpty()) {
            ELF.rules.remove(this.ELFId);
            return Q();
        }

        return Q()
        .then(() => this.takeScreenshot({ dText: this.namingIndex}))
        .then(() => this.cleanup())
        .then(() => {
            if (!this.list.findWhere({ completed: false })) {
                ELF.rules.remove(this.ELFId);
                return Q();
            }

            this.namingIndex++;

            return Q()
            .then(() => this.renderFunc())
            .then(() => this.run());
        });
    }
}

/**
 * Creates an array of instantiated detectors, then runs them in sequence.
 * @param {Object} options - options
 * @param {function} options.render - The function that will render the screen.
 * @param {function} options.takeScreenshot - The function that will take the screenshots.
 * @param {function} [options.cleanup] - A function run to cleanup after rendering and taking screenshot.
 * @param {object[]} [options.detectors] - The detectors to run. Defaults to all of the current detectors.
 * @returns {Q.Promise<any>}
 */
export const runDetectors = (options = {}) => {
    _.defaults(options, {
        render: $.noop,
        takeScreenshot: $.noop,
        cleanup: $.noop,
        detectors: [],
        defaultDetectorClasses: [
            DynamicTextDetector,
            DropDownListDetector
        ]
    });
    const { render, takeScreenshot, cleanup, detectors } = options;

    // So that Dynamic Text functions are not run while checking other detectors.
    addDynamicTextTriggerRule({
        id: 'BlankDynamicText',
        salience: 2,
        action: () => ({ text: '' })
    });

    if (!detectors.length) {
        detectors.push(..._.map(options.defaultDetectorClasses, Detector => {
            return new Detector(render, takeScreenshot, cleanup);
        }));
    }

    const detectorFound = (detectorList) => _.contains(_.pluck(detectorList, 'isDetected'), true);

    return detectors.reduce((detectionChain, detector) => {
        return detectionChain
        .then(() => {
            return detector.setupDetectionListener ? detector.setupDetectionListener() : Q();
        })
        .then(() => render())
        .then(() => detector.run())
        .then(() => !detectorFound([detector]) && cleanup());
    }, Q())
    .then(() => {
        // if no detectors were found, render, take a screenshot, and cleanup
        if (detectorFound(detectors)) {
            return Q();
        }

        return Q()
        .then(() => render())
        .then(() => takeScreenshot())
        .then(() => cleanup());
    });
};
