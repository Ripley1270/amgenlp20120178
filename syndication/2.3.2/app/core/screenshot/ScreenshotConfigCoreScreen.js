import PageView from 'core/views/PageView';
import Logger from 'core/Logger';
import NotifyView from 'core/views/NotifyView';

const logger = new Logger('ScreenshotConfigCoreScreen');

/**
 * @class ScreenshotConfigCoreScreen
 * @description Screenshot Configuration view.
 */
/**
 *
 *
 * @export
 * @class ScreenshotConfigCoreScreen
 * @extends {PageView}
 */
export default class ScreenshotConfigCoreScreen extends PageView {
    /**
     * Creates an instance of ScreenshotConfigCoreScreen.
     * @param {object} options options for this object constructor
     */
    constructor (options) {
        this.id = 'screenshot-config-core-screens';

        /**
        * The view's events.
        * @readonly
        * @enum {Event}
        */
        this.events = {
            'click .check-all': 'selectAll',
            'click .uncheck-all': 'deselectAll',

            // Click event for next button
            'click #next': 'nextHandler',

            // Click event for back button
            'click #back': 'back',

            // Event handlers for checkbox selection
            'click li.list-item > span': 'handleSelection',

            'change input[type=checkbox]': 'handleSelection'
        };

        super(options);

        this.template = '#screenshot-template-core-screen-selection';

        /**
         * Strings to fetch in the render function
         * @type Object
         */
        this.templateStrings = _.defaults({}, options.strings, {
            header: 'APPLICATION_HEADER'
        });
    }

    /**
     * Navigate back to the previous screen.
     */
    back () {
        let coreScreens = this.findCheckedItems();

        if (coreScreens.length) {
            this.model.get('selection').set('coreScreens', coreScreens);
        }

        this.model.prevMode();
        this.navigate('screenshot_next_mode');
    }

    /**
     * Go to the given next screen.
     * @param {string} url - The url we are navigating to.
     */
    next (url) {
        this.model.nextMode();
        this.navigate(url);
    }

    /**
     * Determine where to go next, or display an error message.
     * @returns {Q.Promise|undefined}
     */
    nextHandler () {
        let coreScreens = this.findCheckedItems();

        if (!(coreScreens.length)) {
            return new NotifyView().show({
                header: '',
                body: 'Please select one or more core screens',
                ok: 'OK',
                type: 'error'
            });
        }

        this.model.get('selection').set('core_screens', coreScreens);

        return this.next('screenshot_next_mode');
    }

    /**
     * If we are navigating back to this screen, display what was chosen previously.
     * @returns {Q.Promise}
     */
    showPrevCheckedItems () {
        return Q().then(() => {
            let itemsChecked = this.model.get('selection').get('core_screens');

            if (itemsChecked.length) {
                let allListItems = Array.prototype.slice.call(this.$checkboxGroup.find('li.list-item'));

                itemsChecked.map(item => {
                    return allListItems.map(listItem => {
                        return item === $(listItem).data('value') ? this.selectItem($(listItem)) : false;
                    });
                });
            }
        })
        .catch(e => logger.error(e));
    }

    /**
     * Get the selected items.
     * @returns {string[]} The selected Items.
     */
    findCheckedItems () {
        let selectedAnswers = this.$el.find('li.checked');

        selectedAnswers = Array.prototype.slice.call(selectedAnswers);

        return selectedAnswers.map(answer => $(answer).data('value'));
    }

    /**
     * Render the view
     * @returns {Q.Promise}
     */
    render () {
        return this.buildHTML({
            title: 'Take Screenshots',
            mode: 'Core Screens'
        })
        .then(() => {
            this.page();
            this.delegateEvents();

            this.$checkboxContainer = this.$('.screenshot-checkbox-container');
            this.$checkboxGroup = this.$('#screenshot-checkbox-group');
            this.$screenshotBtnContainer = this.$('.screenshot-btn-container');
        })
        .then(() => {
            let coreScreens = _.findWhere(this.model.get('screens'), {
                name: 'core_screen'
            }).data;
            return this.createCheckboxListItems(coreScreens);
        })
        .then(() => this.showPrevCheckedItems())
        .catch(err => {
            logger.error('An error occurred rendering the screenshot view', err);
        });
    }

    /**
     * Render the checkbox list.
     */
    createCheckboxListItems () {
        let unorderedList = $('<div>'),
            displayName = 'Customer Support',
            id = 'support';

        let listItem = $(`<li class="list-item"><input type="checkbox"><span>${displayName}</span></li>`)
                    .attr('data-template-data', JSON.stringify(id))
                    .attr('id', id)
                    .attr('data-value', id);

        unorderedList.append(listItem);

        this.renderListItems(unorderedList);
    }

    /**
     * Render the list to the screen.
     * @param {any} list - jQuery object of the list to display.
     */
    renderListItems (list) {
        this.$checkboxGroup.html(list.html());
    }

    /**
     * React to input event to select a list item.
     * @param {Object} e - input event
     */
    handleSelection (e) {
        e.preventDefault();
        e.stopImmediatePropagation();

        let $parentListItem = $(e.target).closest('li');
        $parentListItem.hasClass('checked') ? this.deselectItem($parentListItem) : this.selectItem($parentListItem);
    }

    /**
     * Select the item.
     * @param {any} listItem - jQuery object for the item to select
     */
    selectItem (listItem) {
        listItem.context || (listItem = $(listItem));
        listItem.not('.disabled')
            .addClass('checked')
            .attr('selected', true)
            .children('input:not(:disabled)')
            .attr('checked', true);
    }

    /**
     * Deselect the item.
     * @param {any} listItem - jQuery object for the item to deselect
     */
    deselectItem (listItem) {
        listItem.context || (listItem = $(listItem));
        listItem.not('.disabled')
            .removeClass('checked')
            .attr('selected', false)
            .children('input:not(:disabled)')
            .removeAttr('checked');
    }

    /**
     * Checks all checkboxes
     */
    selectAll () {
        this.$checkboxGroup
            .find('li.list-item').not('.disabled')
            .map((idx, listItem) => {
                this.selectItem(listItem);
            });
    }

    /**
     * Unchecks all checkboxes
     */
    deselectAll () {
        this.$checkboxGroup
            .find('li.list-item').not('.disabled')
            .map((idx, listItem) => {
                this.deselectItem(listItem);
            });
    }
}
