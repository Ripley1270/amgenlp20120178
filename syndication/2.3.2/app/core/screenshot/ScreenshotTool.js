import QuestionnaireView from 'core/views/QuestionnaireView';
import NotifyView from 'core/views/NotifyView';
import CustomerSupportView from 'sitepad/views/CustomerSupportView';
import SupportView from 'logpad/views/SupportView';
import BaseVas from 'core/widgets/VAS/BaseVas';
import Answer from 'core/models/Answer';
import User from 'core/models/User';
import Logger from 'core/Logger';
import Data from 'core/Data';
import CurrentContext from 'core/CurrentContext';
import * as utils from 'core/utilities';

import { MessageRepo, Banner } from 'core/Notify';

import ScreenshotUtils from './screenshotUtils';
import * as DetectFeatures from './DetectFeatures';

const logger = new Logger('ScreenshotTool');
const screenIdCache = {};

/**
 * Handles setting up and looping through screens for screenshots
 * @class ScreenshotTool
 */
export default class ScreenshotTool {
    constructor ({ controller, delay = 200 }) {
        _.extend(this, { controller, delay });
    }

    /**
     * Shows the completion notification.
     * @returns {Q.Promise<void>}
     */
    showNotification () {
        return new NotifyView().show({
            header : '',
            body   : 'Screenshots capture is complete.',
            ok     : 'OK',
            type   : 'success'
        });
    }

    /**
     * Loops through each selected Diary for each selected Language
     * @returns {Q.Promise<void>}
     */
    takeDiaryScreenshots () {
        const diaryList = this.controller.model.get('selection').get('diaries');
        const languages = this.controller.model.get('selection').get('languages');

        const reduceDiaries = language => (diaryChain, diary) => {
            return diaryChain
                .then(() => {
                    CurrentContext().setContextLanguage(language);
                })
                .then(() => {
                    return this.prepDiary(diary);
                })
                .then(() => {
                    return this.loopDiaryScreens(diary, language);
                })
                .catch(err => {
                    logger.error(`Error taking screenshot of ${diary}`, err);
                });
        };

        const reduceLanguages = (languageChain, language) => diaryList.reduce(reduceDiaries(language), languageChain);

        return languages.reduce(reduceLanguages, Q())
            .catch(err => {
                logger.error('Error completing screenshot capture', err);
            });
    }

    /**
     * Prep data needed to render the Diary.
     * @param  {string} id the string identifier for the diary.
     * @returns {Q.Promise<void>}
     */
    prepDiary (id) {
        let viewsWithCancel = ['Edit_Patient', 'Edit_User', 'Activate_User','Deactivate_User', 'New_Patient','Skip_Visits'],
            options = {id, subject: Data.subject };

        this.controller.clear();

        if( viewsWithCancel.indexOf(id) > -1 ){
            options = { id, subject: Data.subject, showCancel: true };
         }

        this.controller.view  = new QuestionnaireView(options);
        this.controller.view.Answer = Answer;
        Data.Questionnaire = this.controller.view;

        LF.security.activeUser = new User();

        return Q()
            .then(() => {
                return this.controller.view.prepScreens();
            })
            .then(() => {
                return this.controller.view.prepQuestions();
            })
            .then(() => {
                return this.controller.view.configureAffidavit();
            })
            .then(() => {
                return this.controller.view.prepAnswerData(null, true);
            });
    }

    /**
     * Loop through all the screens configured for the diary in the given language.
     * @param {string} id the string identifier for the diary to render
     * @param {string} lang the language to render the diary screen in.
     * @param {number} [delay=this.delay] the amount of time in milliseconds to wait between screens to account for async problems.
     * @returns {Q.Promise<void>}
     */
    loopDiaryScreens (id, lang, delay = this.delay) {
        let numberOfScreensInDiary = this.controller.view.data.screens.length - 1;

        const renderScreen = (screenId, questionIndex) => {
            const { view } = this.controller;
            LF.security.set('Last_Active', new Date().getTime());
            logger.trace(`Rendering screen: ${screenId} of Diary: ${this.controller.view.id}`);

            return Q(view.render(screenId))
            .then(() => {
                // Fix for DE18951.
                // Convert canvases to an <img> tag before taking a screenshot.
                // https://github.com/gitawego/cordova-screenshot/issues/87
                const questionView = view.questionViews && view.questionViews[questionIndex];
                const widget = questionView && questionView.widget;

                if (widget && widget instanceof BaseVas) {
                    const canvases = widget.$('canvas');
                    if (canvases.length > 0) {

                        _.each(canvases, (canvas) => {
                            const imageMask = document.createElement('img');
                            const canvasEl = widget.$(canvas);

                            canvasEl.wrapAll('<div id="img-wrap"></div>');

                            imageMask.classList.add('image-mask');
                            imageMask.src = canvas.toDataURL('png');

                            $(imageMask).attr({
                                style: canvasEl.attr('style'),
                                height: canvasEl.attr('height'),
                                width: canvasEl.attr('width'),
                                id: canvasEl.attr('id')
                            })
                            .css({
                                display: 'block'
                            });

                            widget.$('#img-wrap').replaceWith(imageMask);
                        });
                    }
                }
                return Q();
            })
            // Async problems =(
            .delay(delay);
        };

        const takeScreenshot = (screenId, indx, dText, subScreenId) => {
            return Q().then(() => {
                return this.createFileAndFolderName({ diaryId: id, lang, fileOrder: indx + 1, screenId, dText, subScreenId})
                    .then(names => {
                        const [filename, foldername] = names;

                        logger.trace(`Taking screenshot of screen: ${screenId} from questionnaire: ${id}`);

                        return ScreenshotUtils.takeScreenshot({ filename, foldername });
                    })
                    .catch(err => logger.error('An error occured', err));
            })
            .catch(err => {
                logger.error(`Error taking screenshot of screen: ${screenId} from questionnaire: ${id}`, err);
            });
        };

        //US6879: Take screenshots of expanded DatePicker widget and the DropDown list in diary
        const takeDatePickerScreenshots = (screenId, indx, subScreenId) => {
            let selectTag, dateBoxClass,
                screenIdOfLastScreen = `DATEPICKER_DIARY_S_${numberOfScreensInDiary}`;
            return renderScreen(screenId)
            .then(() => {
                return takeScreenshot(screenId, indx, null, subScreenId++);
            })
            .then(() => {
                if (screenId !== 'AFFIDAVIT'){
                    dateBoxClass = $('input.date-input');
                    dateBoxClass.datebox('open');
                    return Q.delay(1000)
                    .then(() => {
                        return takeScreenshot(screenId, indx, null, subScreenId++);
                    })
                    .then(() => {
                        selectTag = this.controller.view.getQuestions()[0].widget.$('select');
                        $(selectTag[0]).select2('open');
                    })
                    .delay(1000)
                    .then(() => {
                        return takeScreenshot(screenId, indx, null, subScreenId++);
                    })
                    .then(() => {
                        $(selectTag[0]).select2('close');
                        $(selectTag[1]).select2('open');
                    })
                    .delay(1000)
                    .then(() => {
                        return takeScreenshot(screenId, indx, null, subScreenId++);
                    })
                    .then(() => {
                        $(selectTag[1]).select2('close');
                    })
                    .then(() => {
                        dateBoxClass.datebox('close');
                        if (screenId === screenIdOfLastScreen) {
                            let countOfDays = [1, 1, 1, 1, 1, 1, 1];

                            return countOfDays.reduce((chain, currentDay) => {
                                return chain
                                .then(() => {
                                    let dateToSet = dateBoxClass.datebox('getTheDate');

                                    if(!(dateToSet instanceof Date)) {
                                        dateToSet = new Date();
                                    }

                                    dateToSet.setDate(dateToSet.getDate() + currentDay);
                                    dateBoxClass.datebox('setTheDate', dateToSet);
                                    return Q.delay(1000)
                                    .then(() => {
                                        return takeScreenshot(screenId, indx, null, subScreenId++);
                                    });
                                });
                            }, Q());
                        }
                        dateBoxClass = null;
                        return Q();
                    });
                }
                return Q();
            });
        };

        return this.controller.view.data.screens.reduce((chain, screen, indx) => {
            return chain
            .then(() => {
                const screenId = screen.get('id');

                // US6879: Added to check if fake DatePicker diary is selected.
                if (id.indexOf('DatePicker_Screenshot') !== -1) {
                    return takeDatePickerScreenshots(screenId, indx, 1);
                }

                return DetectFeatures.runDetectors({
                    render: () => renderScreen(screenId, indx),
                    takeScreenshot: ({ dText, subScreenId } = {}) => takeScreenshot(screenId, indx, dText, subScreenId)
                });
            });
        }, Q())
        .then(() => {
            this.controller.clear();
        });
    }

    /**
     * Decide the name of the screenshot file and folder to place the screenshot in.
     * @param {object} options object containing options
     * @param {string} [options.diaryId] - The id of the diary rendered, if we are not rendering a diary leave empty.
     * @param {string} [options.messageName] - The name to use for the file if we are not rendering a diary.
     * @param {string} options.lang - The language that the screen was rendered in.
     * @param {number} [options.fileOrder] - The order number of the file to be used for naming. Optional.
     * @param {number} [options.dText] - The order number of the Dynamic Text entry. Optional.
     * @param {string} options.screenId - The id of the screen.
     * @returns {Q.Promise<void>}
     */
    createFileAndFolderName (options) {
        let { diaryId, messageName, lang, fileOrder, screenId, dText, subScreenId } = options,
            maxStrLength = 3,
            prependZeros = (fileOrder, numOfZeros) => {
                return (numOfZeros === 1) ? `0${fileOrder}` : `00${fileOrder}`;
            },
            fetchDiaryDisplayName = diaryId => {
                let cachedId = screenIdCache[diaryId];

                // DE18894 - String was being fetched w/key DISPLAY_NAME, which isn't the key
                // used for the display name for all diaries.  Instead, get the displayName
                // property from the questionnaire's configuration to determine the correct key to use.
                let displayName = utils.getNested(this, 'controller.view.model.attributes.displayName') || 'DISPLAY_NAME';

                return cachedId || LF.getStrings(displayName, name => {
                    screenIdCache[diaryId] = (() => {
                        return Q().then(() => name);
                    })();
                    return name;

                }, {
                    namespace: diaryId,
                    language: 'en',
                    locale: 'US'
                });
            };

        return Q().then(() => {
            if (diaryId) {
                return fetchDiaryDisplayName(diaryId);
            } else {
                return messageName;
            }
        }).then(name => {
            let file, folder, fileOrderLength;

            if (fileOrder) {
                fileOrderLength = fileOrder.toString().length;

                if (fileOrderLength < maxStrLength) {
                    fileOrder = prependZeros(fileOrder, (maxStrLength - fileOrderLength));
                }
            }

            if (fileOrder && screenId) {
                file = `${name.replace(/[\s+\/]/g, '')}_${fileOrder}_${screenId}`;
            } else {
                file = `${name.replace(/[\s+\/]/g, '')}`;
            }

            folder = lang.replace('-', '_');

            if (dText) {
                file += `_DynamicText${dText}`;
            }

            if (subScreenId) {
                // case: if there're more than 26(the 26 alphabet) screen shots, keep adding Alphabet to the end of the filename.
                let suffix = '';
                while (subScreenId > 0) {
                    let remainder = subScreenId % 26;
                    subScreenId = Math.floor(subScreenId / 26);
                    if (remainder === 0) {
                        remainder = 26;
                        subScreenId--;
                    }
                    remainder = String.fromCharCode(64 + remainder);
                    suffix = `${remainder}${suffix}`;
                }
                file += suffix;
            }

            return [file, folder];
        })
        .catch(e => logger.error('An error occured fetching diary display name', e));

    }

    /**
     * Loop through all banners and notifications and take a screenshot.
     * @param {number} [delay=1000] The amount of time, in milliseconds, to wait between messages to account for async issues.
     * @returns {Q.Promise<void>}
     */
    takeMessageScreenshots (delay = 1000) {
        const languages = this.controller.model.get('selection').get('languages');

        const cleanup = () => {
            Banner && Banner.closeAll();
            // triggers 'hidden.bs.modal': 'teardown'
            $('.modal').modal('hide');
        };

        const renderMessage = (language, message, type) => {
            CurrentContext().setContextLanguage(language);

            switch (type) {
                case 'Dialog': {
                    // We can't wait for dialogs to render because they don't resolve until closed.
                    MessageRepo.display(message);
                    return Q.delay(delay);
                }
                case 'Banner': {
                    // noty uses callbacks, wrap in a resolved Promise.
                    return Q.Promise(resolve => {
                        MessageRepo.display(message, { afterShow: resolve });
                    })
                    .then(() => Banner.flush());
                }
                default: {
                    logger.error(`Unsupported message type: ${type}`);
                }

            }
            return Q.resolve();
        };

        const takeScreenshot = (key, type, language, dText) => {
            return this.createFileAndFolderName({ messageName: key, lang: language, dText })
            .then(names => {
                const [filename, foldername] = names;
                const name = `${type}_${filename}`;

                logger.trace(`Taking screenshot of message: ${key} of type: ${type}`);

                return ScreenshotUtils.takeScreenshot({ filename: name, foldername });
            })
            .catch(err => {
                logger.error(`Error taking screenshot of message: ${key} of type: ${type}`, err);
            });
        };

        const reduceMessageTypes = language => (messageChain, type) => {
            const messageKeys = MessageRepo[type];

            // Go through each message of this message type.
            return Object.keys(messageKeys).reduce((chain, key) => {
                return chain
                .then(() => {
                    return DetectFeatures.runDetectors({
                        cleanup,
                        render: () => renderMessage(language, messageKeys[key], type),
                        takeScreenshot: ({ dText } = {}) => takeScreenshot(key, type, language, dText)
                    });
                });
            }, messageChain);
        };

        const reduceLanguages = (languageChain, language) => {
            return MessageRepo.types.reduce(reduceMessageTypes(language), languageChain);
        };

        $('#application').hide();
        return languages.reduce(reduceLanguages, Q())
        .then(() => {
            $('#application').show();
        })
        .catch((err) => {
            logger.error('Error taking screenshots.', err);
        });
    }

    /**
     * Loops through each selected core screen for each selected Language
     * @param {number} [delay=1000] the amount of time in milliseconds to wait between screens to account for async problems.
     * @returns {Q.Promise<void>}
     */
    takeCoreScreenshots (delay = 1000) {
        const coreScreenList = this.controller.model.get('selection').get('core_screens');
        const languages = this.controller.model.get('selection').get('languages');

        const loopSupportOptions = (language) => {
            let deferred = Q.defer(),
                length = this.controller.view.$('#support_dropdown option').length,
                step = (counter) => {
                    if (counter >= (length - 1)) {
                        deferred.resolve();
                    } else {
                        let foldername = language.replace('-', '_'),
                            filename = `CustomerSupport${counter}`;

                        $('#support_dropdown').val(counter).trigger('change');

                        // Extra delay for DE19018
                        _.delay(() => {
                            ScreenshotUtils.takeScreenshot({ filename: filename, foldername })
                            .delay(delay)
                            .then(() => {
                                step(counter + 1);
                            });
                        }, 500);
                    }
                };

            step(0);

            return deferred.promise;
        };

        const loopCoreScreens = language => (screenChain, screen) => {
            return screenChain
            .then(() => {
                CurrentContext().setContextLanguage(language);
            })
            .then(() => {
                this.controller.clear();
                this.controller.view  = utils.isSitePad() ? new CustomerSupportView() : new SupportView({});

                LF.security.set('Last_Active', new Date().getTime());

                return Q(this.controller.view.render())
                .delay(delay);
            })
            .then(() => {
                return loopSupportOptions(language);
            })
            .catch(err => {
                logger.error(`Error taking screenshot of ${screen}`, err);
            });
        };

        const reduceLanguages = (languageChain, language) => coreScreenList.reduce(loopCoreScreens(language), languageChain);

        return languages.reduce(reduceLanguages, Q())
        .catch(err => {
            logger.error('Error completing screenshot capture', err);
        });

    }

    /**
     * Take screenshots for selected mode.
     * @param {string[]} modes The selected modes to take screenshots of.
     * @returns {Q.Promise<void>}
     */
    takeModeScreenshot (modes = []) {
        const takeSSForMode = mode => {
            switch (mode) {
                case 'Diaries':
                    return this.takeDiaryScreenshots();
                case 'Messagebox':
                    return this.takeMessageScreenshots();
                case 'Core Screens':
                    return this.takeCoreScreenshots();
                default:
                    return Q();
            }
        };

        return modes.reduce((chain, mode) => chain.then(() => takeSSForMode(mode)), Q())
        .then(() => this.controller.model.trigger('Model:ScreenshotsCompleted'))
        .then(() => this.showNotification())
        .catch(err => {
            logger.error('Error taking screenshots', err);
        });
    }
}
