import ScreenshotController from './ScreenshotController';
import COOL from 'core/COOL';
import ELF from 'core/ELF';
import Data from 'core/Data';
import Subjects from 'core/collections/Subjects';
import Subject from 'core/models/Subject';
import Site from 'core/models/Site';
import { getProductName } from 'core/utilities';

/**
 * Create fake data in order to render certain screens.
 * @returns {Q.Promise}
 */
export const fakeData = () => {
    /**
     * Create a fake function for a widget
     * @param {string} text - The text to display instead.
     * @param {number} [count=1] - The amount of fake values to create, for use in lists.
     * @returns {function} fake widget function
     */
    function fakeWidgetObj (text, count = 1) {
        return () => {
            let arr = _.range(1, count + 1);

            return Q([{
                id: '1',
                text: _.map(arr, (value) => `--${text}:${value}--`)
            }]);
        };
    }

    /**
     * Create a fake function for the given object.
     * @param {object} obj - The object that needs a fake function applied to it.
     * @param {function} [fake] - The fake function to apply, defaults to $.noop
     */
    function fakeObjectFunctions (obj, fake = $.noop) {
        _.chain(obj)
            .keys()
            .each(key => {
                // DE17599 - A few functions need to be singled out to prevent errors from
                // being thrown and crashing the application on Windows.
                switch (key) {
                    case 'barCodeList':
                        return obj[key] = fakeWidgetObj('fakeReviewScreenText', 3);
                    case 'medsLoop1':
                    case 'painNRS':
                        return obj[key] = fakeWidgetObj('fakeReviewScreenText', 2);
                    case 'dropDownListTest':
                        // dropDownListTest expects an Object returned, not an Array.
                        return obj[key] = () => {
                            return Q({
                                items : [{
                                    value: 1,
                                    text: 'fakeParamFunctionsText'
                                }]
                            });
                        };
                    default:
                        return obj[key] = fake;
                }
            });
    }

    fakeObjectFunctions(LF.Widget.ReviewScreen, fakeWidgetObj('fakeReviewScreenText'));
    fakeObjectFunctions(LF.Widget.ParamFunctions, fakeWidgetObj('fakeParamFunctionsText'));
    fakeObjectFunctions(LF.DynamicText, callback => callback('{{ No dynamic text screenshot values provided }}'));

    let krpt = 'fake';

    return Subjects.fetchCollection()
    .then((subjects) => {
        let subject = subjects.findWhere({ krpt });
        Data.subject = subject || new Subject({ id: 1, subject_id: '123', krpt });
        return Data.subject.save();
    })
    .then(() => {
        Data.site = new Site();
        ELF.rules.remove('QuestionnaireScreenAuthenticationByRole');
    });
};

/**
 * Create the new Router.
 * @param {string} product - The current product ('logpad' or 'sitepad')
 * @returns {Q.Promise}
 */
function createRouter (product) {
    const opts = {
        routerOpts: {
            routes: {
                screenshot: 'screenshot#screenshot',
                screenshot_take_screenshots: 'screenshot#takeScreenshots',
                screenshot_mode: 'screenshot#screenshotModeSelection',
                screenshot_diary: 'screenshot#screenshotDiary',
                screenshot_core_screen: 'screenshot#screenshotCoreScreen',
                screenshot_messagebox: 'screenshot#screenshotMessagebox',
                screenshot_next_mode: 'screenshot#nextMode'
            }
        },
        controllers: {
            screenshot: new ScreenshotController({ product })
        }
    };

    // Run whichever startup sequences are registered.
    const startups = ['Startup'];

    return Q.all(_.map(startups, name => {
        const Startup = COOL.getClass(name);
        return Startup && new Startup().backboneStartup(opts);
    }))
    .catch((err) => {
        err && logger.error('Error starting backbone', err);
    });
}

/**
 * Run to add Screenshot rule listeners.
 */
export default function addRules () {
    let activateScreenShot = (product) => {
        return createRouter(product)
        .then(() => fakeData())
        .then(() => {
            LF.security.pauseSessionTimeOut();
            localStorage.setItem('screenshot', true);
        });
    };

    // Skip Code Entry and resume to Screenshot mode if screenshot has been activated before
    ELF.rules.add({
        id: 'Screenshot_Resume',
        trigger: 'NAVIGATE:registration/codeEntry',
        evaluate (filter, resume) {
            resume(localStorage.getItem('screenshot') === 'true');
        },
        resolve: [
            { action: () => activateScreenShot(getProductName()) },
            { action: 'navigateTo', data: 'screenshot' },
            { action: 'preventDefault' }
        ]
    });

    // Add screenshot router and route to screenshot view.
    ELF.rules.add({
        id: 'Screenshot_Tool',
        trigger: [
            'CODEENTRY:Submit',
            'MODESELECT:Submit'
        ],
        evaluate (filter, resume) {
            resume(filter.value === 'screenshot');
        },
        resolve: [
            { action: (filter) => activateScreenShot(filter.product) },
            { action: 'navigateTo', data: 'screenshot' },
            { action: 'preventDefault' }
        ]
    });
}
