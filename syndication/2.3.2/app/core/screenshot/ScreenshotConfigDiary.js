import PageView from 'core/views/PageView';
import Logger from 'core/Logger';
import NotifyView from 'core/views/NotifyView';

const logger = new Logger('ScreenshotConfigDiary');

/**
 * @class ScreenshotConfigDiary
 * @description Screenshot Configuration view.
 */
/**
 * 
 * 
 * @export
 * @class ScreenshotConfigDiary
 * @extends {PageView}
 */
export default class ScreenshotConfigDiary extends PageView {
    /**
     * Creates an instance of ScreenshotConfigDiary.
     * @param {object} options options for this object constructor
     */
    constructor (options) {
        this.id = 'screenshot-config-diary';

        /**
        * The view's events.
        * @readonly
        * @enum {Event}
        */
        this.events = {
            'click .check-all': 'selectAll',
            'click .uncheck-all': 'deselectAll',

            // Click event for next button
            'click #next': 'nextHandler',

            // Click event for back button
            'click #back': 'back',

            // Event handlers for checkbox selection
            'click li.list-item > span': 'handleSelection',

            'change input[type=checkbox]': 'handleSelection'
        };

        super(options);

        this.template = '#screenshot-template-diary-selection';

        /**
         * Strings to fetch in the render function
         * @type Object
         */
        this.templateStrings = _.defaults({}, options.strings, {
            header: 'APPLICATION_HEADER'
        });
    }

    /**
     * Navigate back to the previous screen.
     */
    back () {
        let diaries = this.findCheckedItems();

        if (diaries.length) {
            this.model.get('selection').set('diaries', diaries);
        }

        this.model.prevMode();
        this.navigate('screenshot_next_mode');
    }

    /**
     * Go to the given next screen.
     * @param {string} url - The url we are navigating to.
     */
    next (url) {
        this.model.nextMode();
        this.navigate(url);
    }

    /**
     * Determine where to go next, or display an error message.
     * @returns {Q.Promise|undefined}
     */
    nextHandler () {
        let diaries = this.findCheckedItems();

        if (!(diaries.length)) {
            return new NotifyView().show({
                header: '',
                body: 'Please select one or more diaries',
                ok: 'OK',
                type: 'error'
            });
        }

        this.model.get('selection').set('diaries', diaries);

        return this.next('screenshot_next_mode');
    }

    /**
     * If we are navigating back to this screen, display what was chosen previously.
     * @returns {Q.Promise}
     */
    showPrevCheckedItems () {
        return Q().then(() => {
            let itemsChecked = this.model.get('selection').get('diaries');

            if (itemsChecked.length) {

                let allListItems = Array.prototype.slice.call(this.$checkboxGroup.find('li.list-item'));

                itemsChecked.map(item => {
                    return allListItems.map(listItem => {
                        return item === $(listItem).data('value') ? this.selectItem($(listItem)) : false;
                    });
                });
            }
        })
        .catch(e => logger.error(e));
    }

    /**
     * Get the selected items.
     * @returns {string[]} The selected Items.
     */
    findCheckedItems () {
        let selectedAnswers = this.$el.find('li.checked');

        selectedAnswers = Array.prototype.slice.call(selectedAnswers);

        return selectedAnswers.map(answer => $(answer).data('value'));
    }

    /**
     * Render the view
     * @returns {Q.Promise}
     */
    render () {
        return this.buildHTML({
            title: 'Take Screenshots',
            mode: 'diaries'
        })
        .then(() => {
            this.page();
            this.delegateEvents();

            this.$checkboxContainer = this.$('.screenshot-checkbox-container');
            this.$checkboxGroup = this.$('#screenshot-checkbox-group');
            this.$screenshotBtnContainer = this.$('.screenshot-btn-container');
        })
        .then(() => {
            let diaries = _.findWhere(this.model.get('screens'), {
                displayName: 'Diaries'
            }).data;
            return this.createCheckboxListItems(diaries);
        })
        .then(() => this.showPrevCheckedItems())
        .catch(err => {
            logger.error('An error occurred rendering the screenshot view', err);
        });
    }

    /**
     * Render the checkbox list.
     * @param {string[]} list - List of diaries to display
     * @returns {Q.Promise}
     */
    createCheckboxListItems (list) {
        let unorderedList = $('<div>');

        return Q.all(list.map(item => {
            // not sure translation is needed, but for now...
            return LF.getStrings(item.get('displayName'), (diaryName) => {
                let displayName = diaryName || item.displayName || item.get('displayName') || item,
                    id = item.id || '',
                    listItem = $(`<li class="list-item"><input type="checkbox"><span>${displayName}</span></li>`)
                        .attr('data-template-data', JSON.stringify(item))
                        .attr('id', id)
                        .attr('data-value', id);

                unorderedList.append(listItem);
            }, { namespace: item.get('id')});
        }))
        .then(() => this.renderListItems(unorderedList))
        .catch(err => logger.error('An error occurred', err));
    }

    /**
     * Render the list to the screen.
     * @param {any} list - jQuery object of the list to display.
     */
    renderListItems (list) {
        this.$checkboxGroup.html(list.html());
    }

    /**
     * React to input event to select a list item.
     * @param {Object} e - input event
     */
    handleSelection (e) {
        e.preventDefault();
        e.stopImmediatePropagation();

        let $parentListItem = $(e.target).closest('li');
        $parentListItem.hasClass('checked') ? this.deselectItem($parentListItem) : this.selectItem($parentListItem);
    }

    /**
     * Select the item.
     * @param {any} listItem - jQuery object for the item to select
     */
    selectItem (listItem) {
        listItem.context || (listItem = $(listItem));
        listItem.not('.disabled')
            .addClass('checked')
            .attr('selected', true)
            .children('input:not(:disabled)')
            .attr('checked', true);
    }

    /**
     * Deselect the item.
     * @param {any} listItem - jQuery object for the item to deselect
     */
    deselectItem (listItem) {
        listItem.context || (listItem = $(listItem));
        listItem.not('.disabled')
            .removeClass('checked')
            .attr('selected', false)
            .children('input:not(:disabled)')
            .removeAttr('checked');
    }

    /**
     * Checks all checkboxes
     */
    selectAll () {
        this.$checkboxGroup
            .find('li.list-item').not('.disabled')
            .map((idx, listItem) => {
                this.selectItem(listItem);
            });
    }

    /**
     * Unchecks all checkboxes
     */
    deselectAll () {
        this.$checkboxGroup
            .find('li.list-item').not('.disabled')
            .map((idx, listItem) => {
                this.deselectItem(listItem);
            });
    }
}
