import BaseController from 'core/controllers/BaseController';

import ScreenshotView from './ScreenshotView';
import ScreenshotConfigModel from './ScreenshotConfigModel';
import ScreenshotConfigDiary from './ScreenshotConfigDiary';
import ScreenshotConfigCoreScreen from './ScreenshotConfigCoreScreen';
import ScreenshotConfigModeSelection from './ScreenshotConfigModeSelection';
import ScreenshotTool from './ScreenshotTool';
import ScreenshotUtils from './screenshotUtils';

import Logger from 'core/Logger';
import CurrentContext from 'core/CurrentContext';

const logger = new Logger('ScreenshotController');

/**
 * Navigation controller for the screenshot mode.
 * @class ScreenshotController
 * @extends {BaseController}
 */
export default class ScreenshotController extends BaseController {
    constructor (options) {
        super(options);
        this.product = options.product;
        this.model = new ScreenshotConfigModel({ product: this.product });
        this.screenshotTool = new ScreenshotTool({ controller: this });

        this.strings = {
            header: 'APPLICATION_HEADER'
        };
    }

    /**
     * Navigate to ScreenshotView.
     */
    screenshot () {
        const { model, strings } = this;

        this.clear();
        this.view = new ScreenshotView({ model, strings });

        LF.Wrapper.exec({
            execWhenWrapped: () => {
                ScreenshotUtils.addScreenShotStrings(this.view);
            },
            execWhenNotWrapped: () => {
                this.view.render();
            }
        });
    }

    /**
     * Navigate to the next mode in the list of modes.
     */
    nextMode () {
        const modes = this.model.get('selection').get('modes');
        const modeIndex = this.model.get('modeIndex');

        if (modeIndex < 0) {
            this.model.set('modeIndex', 0);
            this.navigate('screenshot_mode');

        } else if (modeIndex === modes.length) {
            this.model.set('modeIndex', 0);
            this.navigate('screenshot_take_screenshots');

        } else {
            const modeName = _.findWhere(this.model.get('screens'), {
                displayName: modes[modeIndex]
            }).name;

            this.navigate(`screenshot_${modeName}`);
        }
    }

    /**
     * Navigate to the ModeSelection View.
     */
    screenshotModeSelection () {
        const { model, strings } = this;

        this.clear();
        this.view = new ScreenshotConfigModeSelection({ model, strings });
        this.view.render();
    }

    /**
     * Navigate to the diary selection view.
     */
    screenshotDiary () {
        const { model, strings } = this;

        this.clear();
        this.view = new ScreenshotConfigDiary({ model, strings });
        this.view.render();
    }

    /**
     * Navigate to the core screen seletion view.
     */
    screenshotCoreScreen () {
        const { model, strings } = this;

        this.clear();
        this.view = new ScreenshotConfigCoreScreen({ model, strings });
        this.view.render();
    }

    /**
     * Navigate to the MessageBox selection view.
     */
    screenshotMessagebox () {
        const { model } = this;

        model.nextMode();
        this.navigate('screenshot_next_mode');
    }

    /**
     * Start the screenshot taking process based on selections.
     * @returns {Q.Promise}
     */
    takeScreenshots () {
        const originalLang = CurrentContext().langLoc;

        return this.screenshotTool.takeModeScreenshot(this.model.get('selection').get('modes'))
            .then(() => {
                CurrentContext().setContextLanguage(originalLang);
                this.navigate('screenshot');
            })
            .catch(err => {
                logger.error('Error calling to screenshotTool', err);
            });
    }
}
