import PageView from 'core/views/PageView';
import NotifyView from 'core/views/NotifyView';
import Logger from 'core/Logger';

const logger = new Logger('ScreenshotConfigModeSelection');

/**
 * @class ScreenshotConfigModeSelection
 * @description Screenshot Configuration view.
 */
export default class ScreenshotConfigModeSelection extends PageView {
    constructor (options) {

        this.id = 'screenshot-config-mode-selection';

        /**
        * The view's events.
        * @readonly
        * @enum {Event}
        */
        this.events = {
            'click .check-all': 'selectAll',

            'click .uncheck-all': 'deselectAll',

            /** Click event for next button */
            'click #next': 'nextHandler',

            /** Click event for back button */
            'click #back': 'back',

            /** Event handlers for checkbox selection */
            'click li.list-item > span': 'handleSelection',

            'change input[type=checkbox]': 'handleSelection'
        };

        super(options);

        this.template = '#screenshot-template-mode-selection';

        /**
         * Strings to fetch in the render function
         * @type Object
         */
        this.templateStrings = _.defaults({}, options.strings, {
            header: 'APPLICATION_HEADER'
        });
    }

    /**
     * Go back
     */
    back () {
        let modes = this.findCheckedItems();

        if (modes.length) {
            this.model.get('selection').set('modes', modes);
        }
        this.navigate('screenshot');
    }

    /**
     * Go to the next config mode.
     * @param {string} url - The url to navigate to.
     */
    next (url) {
        this.navigate(url);
    }

    /**
     * Determine where to go next, or display an error message.
     * @returns {Q.Promise|undefined}
     */
    nextHandler () {
        let modes = this.findCheckedItems();

        if (!(modes.length)) {
            return new NotifyView().show({
                header: '',
                body: 'Please select one or more modes',
                ok: 'OK',
                type: 'error'
            });
        }

        this.model.get('selection').set('modes', modes);

        return this.next('screenshot_next_mode');
    }

    /**
     * When returning to this page, select items that were selected previously.
     * @returns {Q.Promise}
     */
    showPrevCheckedItems () {
        return Q().then(() => {
            let itemsChecked = this.model.get('selection').get('modes');

            if (itemsChecked.length) {

                let allListItems = Array.prototype.slice.call(this.$checkboxGroup.find('li.list-item'));

                itemsChecked.map(item => {
                    return allListItems.map(listItem => {
                        return item === $(listItem).data('value') ? this.selectItem($(listItem)) : false;
                    });
                });
            }

        })
        .catch(e => logger.error(e));
    }

    /**
    * Get the selected items.
    * @returns {string[]} The selected Items.
    */
    findCheckedItems () {
        let selectedAnswers = this.$el.find('li.checked');

        selectedAnswers = Array.prototype.slice.call(selectedAnswers);

        return selectedAnswers.map(answer => $(answer).data('value'));
    }

    /**
     * Render the view
     * @return {Q.Promise}
     */
    render () {
        return this.buildHTML({
            title: 'Take Screenshots',
            mode: 'mode'
        })
        .then(() => {
            this.page();
            this.delegateEvents();

            this.$checkboxContainer = this.$('.screenshot-checkbox-container');
            this.$checkboxGroup = this.$('#screenshot-checkbox-group');
            this.$screenshotBtnContainer = this.$('.screenshot-btn-container');
        })
        .then(() => {
            this.showModes();
        })
        .then(() => this.showPrevCheckedItems())
        .catch(err => {
            logger.error('An error occurred rendering the screenshot view', err);
        });
    }

    /**
     * Render the modes list.
     */
    showModes () {
        this.renderListItems(this.createCheckboxList(this.model.get('modeToDisplay')));
    }

    /**
     * Create the list.
     * @param {object[]} list - List of modes
     * @returns {any} jQuery object of the created list.
     */
    createCheckboxList (list) {
        let unorderedList = $('<div>');

        list.forEach(item => {
            let displayName = item.displayName || item,
                id = item.id || '',
                listItem = $(`<li class="list-item"><input type="checkbox"><span>${displayName}</span></li>`)
                    .attr('data-template-data', JSON.stringify(item))
                    .attr('id', id)
                    .attr('data-value', displayName);
            unorderedList.append(listItem);
        });

        return unorderedList;
    }

    /**
     * Add the list to the DOM.
     * @param {any} list - The jQuery object of the list to add to the dom.
     */
    renderListItems (list) {
        this.$checkboxGroup.html(list.html());
    }

    /**
     * React to input event to select a list item.
     * @param {Object} e - input event
     */
    handleSelection (e) {
        e.preventDefault();
        e.stopImmediatePropagation();

        let $parentListItem = $(e.target).closest('li');
        $parentListItem.hasClass('checked') ? this.deselectItem($parentListItem) : this.selectItem($parentListItem);
    }

    /**
     * Select the item.
     * @param {any} listItem - jQuery object for the item to select
     */
    selectItem (listItem) {
        listItem.context || (listItem = $(listItem));
        listItem.not('.disabled')
            .addClass('checked')
            .attr('selected', true)
            .children('input:not(:disabled)')
            .attr('checked', true);

    }

    /**
     * Deselect the item.
     * @param {any} listItem - jQuery object for the item to deselect
     */
    deselectItem (listItem) {
        listItem.context || (listItem = $(listItem));
        listItem.not('.disabled')
            .removeClass('checked')
            .attr('selected', false)
            .children('input:not(:disabled)')
            .removeAttr('checked');

    }

    /**
     * Checks all checkboxes
     */
    selectAll () {
        this.$checkboxGroup
            .find('li.list-item').not('.disabled')
            .map((idx, listItem) => {
                this.selectItem(listItem);
            });
    }

    /**
     * Unchecks all checkboxes
     */
    deselectAll () {
        this.$checkboxGroup
            .find('li.list-item').not('.disabled')
            .map((idx, listItem) => {
                this.deselectItem(listItem);
            });
    }
}
