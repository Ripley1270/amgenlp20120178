/**
 * A Model representing the various selections in screenshot mode.
 * @class ScreenshotSelectionModel
 * @extends {Backbone.Model}
 */
export class ScreenshotSelectionModel extends Backbone.Model {
    /**
     * Default everything to an empty array.
     */
    get defaults () {
        return {
            languages: [],
            modes: [],
            diaries: [],
            core_screens: [],
            messageboxes: []
        };
    }
}

/**
 * The model representing the navigation state and other data of the screenshot mode.
 * @class ScreenshotConfigModel
 * @extends {Backbone.Model}
 */
export default class ScreenshotConfigModel extends Backbone.Model {
    constructor (options = {}) {
        super(options);

        this.listenTo(this, 'Model:ScreenshotsCompleted', this.resetSelection);

        this.fakeDatePickerDiaryConfig();

        this.diaryList = LF.StudyDesign.questionnaires.filter(questionnaire => {
            const product = questionnaire.get('product');
            return product ? _.contains(product, options.product) : true;
        });

        this.defaults = {
            screenshotModeData: ['Diaries', 'Core Pages', 'Messagebox'],

            languages: ['English'],

            modeIndex: 0,

            modeToDisplay : [{
                data: this.diaryList,
                id: 'diary',
                displayName: 'Diaries',
                screen_id: 1
            }, {
                data: [],
                id: 'messagebox',
                displayName: 'Messagebox'
            }, {
                data: [],
                id: 'core_screen',
                displayName: 'Core Screens'
            }],

            screens: [{
                name: 'language',
                displayName: 'Languages',
                screen_id: 0,
                data: [],
                skipScreen: false
            }, {
                name: 'mode',
                displayName: 'Mode',
                screen_id: 1,
                data: [],
                skipScreen: false
            }, {
                name: 'diary',
                displayName: 'Diaries',
                screen_id: 2,
                data: this.diaryList,
                skipScreen: false
            }, {
                name: 'core_screen',
                displayName: 'Core Screens',
                screen_id: 3,
                data: [],
                skipScreen: false
            }, {
                name: 'messagebox',
                displayName: 'Messagebox',
                screen_id: 4,
                data: [],
                skipScreen: false
            }],

            selection: new ScreenshotSelectionModel()
        };

        this.set(this.defaults);
    }

    /**
     * Change the current mode to the previous mode in the workflow.
     */
    prevMode () {
        this.set('modeIndex', this.get('modeIndex') - 1);
    }

    /**
     * Change the current mode to the next mode in the workflow.
     */
    nextMode () {
        const modeIndex = this.get('modeIndex');

        this.set('modeIndex', modeIndex + 1);
    }

    /**
     * Reset all selections to default (empty).
     * @returns {Q.Promise}
     */
    resetSelection () {
        return Q().then(() => {
            this.set('selection', new ScreenshotSelectionModel());
        });
    }

    /**
     * US6879: Fake diary config for DatePicker screen-shots.
     */
    fakeDatePickerDiaryConfig () {

        //Questionnaire for DatePicker screen-shots
        LF.StudyDesign.questionnaires.add([{
            id: 'P_DatePicker_Screenshot_Diary',
            SU: 'Date',
            displayName: 'DISPLAY_NAME',
            className: 'DATEPICKER_WIDGET_DIARY',
            affidavit: undefined,
            product: ['logpad'],
            screens: [
                'DATEPICKER_DIARY_S_1',
                'DATEPICKER_DIARY_S_2'
            ]
        }, {
            id: 'P_DatePicker_Screenshot_Diary_SitePad',
            SU: 'Date',
            displayName: 'DISPLAY_NAME',
            className: 'DATEPICKER_WIDGET_DIARY',
            accessRoles: ['admin','site'],
            affidavit: undefined,
            product: ['sitepad'],
            screens: [
                'DATEPICKER_DIARY_S_1',
                'DATEPICKER_DIARY_S_2'
            ]
        }]);

        //Question for DatePicker screen-shots
        LF.StudyDesign.questions.add([{
            id: 'DATEPICKER_DIARY_Q_1',
            IG: 'Date',
            IT: 'DATE_1',
            text: ['QUESTION_1'],
            className: 'DATEPICKER_DIARY_Q_1',
            widget: {
                id: 'DATEPICKER_DIARY_W_1',
                type: 'DatePicker',
                className: 'DATEPICKER_DIARY_W_1',
                configuration: {
                    maxDays: 180,
                    useAnimation: false,
                    defaultValue: '2014-01-01'
                }
            }
        }, {
            id: 'DATEPICKER_DIARY_Q_2',
            IG: 'Date',
            IT: 'DATE_2',
            skipIT: 'Date_2_SKP',
            text: ['QUESTION_2'],
            className: 'DATEPICKER_DIARY_Q_2',
            widget: {
                id: 'DATEPICKER_DIARY_W_2',
                type: 'DatePicker',
                className: 'DATEPICKER_DIARY_W_1',
                configuration: {
                    minDays:0,
                    maxDays: 1460,
                    useAnimation: false,
                    defaultValue: '2015-07-01',
                    useClearButton: true
                }
            }
        }]);

        //Screens for DatePicker screen-shots
        LF.StudyDesign.screens.add([{
            id: 'DATEPICKER_DIARY_S_1',
            className: 'DATEPICKER_DIARY_S_1',
            questions: [
                { id: 'DATEPICKER_DIARY_Q_1', mandatory: true }
            ]
        }, {
            id: 'DATEPICKER_DIARY_S_2',
            className: 'DATEPICKER_DIARY_S_2',
            questions: [
                { id: 'DATEPICKER_DIARY_Q_2', mandatory: false }
            ]
        }]);

        //Translated strings for DatePicker screen-shots
        LF.strings.add([{
            'namespace' : 'P_DatePicker_Screenshot_Diary',
            'language'  : 'en',
            'locale'    : 'US',
            'direction' : 'ltr',
            'resources' : {
                'DISPLAY_NAME'      : 'DatePicker Screenshot Diary',
                'DATE_PICKER_HELP'  : 'Tap a date to select it.',
                'QUESTION_1'        : 'On which day did you take your study medicine?',
                'QUESTION_2'        : 'When would you like to schedule your appointment?'
            }
        }, {
            'namespace' : 'P_DatePicker_Screenshot_Diary_SitePad',
            'language'  : 'en',
            'locale'    : 'US',
            'direction' : 'ltr',
            'resources' : {
                'DISPLAY_NAME'      : 'DatePicker Screenshot Diary',
                'DATE_PICKER_HELP'  : 'Tap a date to select it.',
                'QUESTION_1'        : 'On which day did you take your study medicine?',
                'QUESTION_2'        : 'When would you like to schedule your appointment?'
            }
        }]);
    }

}
