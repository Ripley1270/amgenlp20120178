/**
 * A detector that will detect when DropDownList is used,
 *  then loop through the provided choices in DropDownList and re-render the screen with each value.
 */
export default class DropDownListDetector {
    constructor (renderFunc, takeScreenshot, cleanup = $.noop) {
        this.ELFId = 'DropDownListScreenshot';

        this.renderFunc = renderFunc;
        this.takeScreenshot = takeScreenshot;
        this.cleanup = cleanup;

        this.isDetected = false;
        this.isLast = false;
        this.namingIndex = 1;
        this.isFirstTimeDetected = true;

        this.maxHeight = 0;
    }

    /**
     * Calculate the max height of the current dropdownlist items.
     *  DE19112 - All items in the dropdownlist have the height of the largest in the case of word wrapping.
     */
    setMaxHeight () {
        if ($('.select2-results__option').length) {
            const currMax = _.max($('.select2-results__option').map((i, item) => $(item).innerHeight()));
            this.maxHeight = _.max([currMax, this.maxHeight]);
        } else {
            this.maxHeight = this.maxHeight || 0;
        }

    }

    /**
     * Function that sets up a listener that will call "detected" when the feature is detected.
     */
    setupDetectionListener () {
        if (ELF.rules.find(this.ELFId)) {
            ELF.rules.remove(this.ELFId);
        }

        ELF.rules.add({
            id: this.ELFId,
            trigger: 'WIDGET:Rendering',
            evaluate: (options) => {
                if (options.widget.model.get('type') === 'DropDownList') {
                    return true;
                } else {
                    return false;
                }
            },
            resolve: [{ action: (options) => this.detected(options) }]
        });
    }

    /**
     * After ELF trigger has been triggered in render(QuestionView.js) function, it calls this detected function.
     * calculate how many screenshots will be taken for this DropDownList
     * and fill the DropDownList with items that are going to display on the next screen
     * @param {options} options contains the DropDownList widget.
     * @param {options} options.widget the DropDownList widget.
     */
    detected (options) {
        this.isDetected = true;
        this.widget = options.widget;
        if (this.isFirstTimeDetected) {
            if (!this.startItems || !this.startItems.length) {
                this.startItems = this.widget.model.get('items') || [];
            }
            this.items = [...this.startItems];

            this.rowsNum = this.widget.model.get('visibleRows');

            if (this.rowsNum === null) {
                // set the default visible rows to 6 if it's not set up in the diary.
                this.widget.model.set('visibleRows', 6);
                this.rowsNum = 6;
            }

            this.screenshotNum = Math.ceil(this.items.length / this.rowsNum);
            this.isFirstTimeDetected = false;
        } else {
            this.screenshotNum--;
            this.widget.model.set('items', this.items.slice(0, this.rowsNum));
            if (this.screenshotNum <= 0) {
                // it's the last screen for this DropDownList
                this.isLast = true;
            } else {
                this.items = this.items.slice(this.items.length < (2 * this.rowsNum) ?
                        (this.items.length - this.rowsNum) : this.rowsNum);
            }
        }
    }

    /**
     * Loop through dropdown options and either take screenshots or caluculate heights.
     * @param {boolean} takeScreenshots - if true, loop through and take screenshots. If false, loop through to calculate height values.
     * @returns {Q.Promise<void>}
     */
    loopDropdownOptions (takeScreenshots = false) {
        const renderOpenContinue = () => {
            return this.renderFunc()
            .then(() => {
                if (this.widget.$select2) {
                    this.widget.$select2.select2('open');
                }
            })
            .delay(1000)
            .then(() => this.loopDropdownOptions(takeScreenshots));
        };

        if (!takeScreenshots) {
            return Q()
            .then(() => this.cleanup())
            .then(() => this.setMaxHeight())
            .then(() => !this.isLast && renderOpenContinue());
        }

        return Q()
        .then(() => this.cleanup())
        .then(() => {
            // Allow container to properly grow
            $('.select2-results__options').css('max-height', this.maxHeight * this.rowsNum);

            // Set all items to the size of max (to simulate case of text wrapping making one item larger)
            $('.select2-results__option').innerHeight(this.maxHeight);

            return Q.delay(1000)
            .then(() => {
                return this.takeScreenshot({ subScreenId: this.namingIndex });
            })
            .then(() => this.namingIndex++);
        })
        .then(() => !this.isLast && renderOpenContinue());
    }

    /**
     * Keep rendering and taking screenshots for the DropDownList till all the options are displayed.
     * @returns {Q.Promise<void>}
     */
    run () {
        if (!this.isDetected) {
            return Q();
        }

        // First screenshot is unopened, take a screenshot before continuing.
        return this.takeScreenshot({ subScreenId: this.namingIndex })
        .then(() => this.namingIndex++)

        // Loop once to calculate height, then again to take the screenshots
        .then(() => this.loopDropdownOptions(false))
        .then(() => {
            // reset flags so that we can start over.
            this.isLast = false;
            this.isFirstTimeDetected = true;

            // To initialize counts and flags
            return this.detected({ widget: this.widget });
        })
        .then(() => this.renderFunc())
        .then(() => {
            if (this.widget.$select2) {
                this.widget.$select2.select2('open');
            }
        })
        .delay(1000)
        .then(() => this.loopDropdownOptions(true))
        .then(() => {
            ELF.rules.remove(this.ELFId);
        });
    }
}