import ELF from 'core/ELF';

/**
 * @memberOf ELF.expressions
 * @description
 * Determines if the navigation direction is forward.
 * This should only be used in the context of a questionnaire,
 * when a QUESTIONNAIRE:Navigate event is triggered.
 * @param {object} input Event data passed along by the triggered event.
 * @returns {Q.Promise<boolean>}
 */
export default function isDirectionForward (input) {
    return Q(input.direction === 'next');
}

ELF.expression('isDirectionForward', isDirectionForward);
