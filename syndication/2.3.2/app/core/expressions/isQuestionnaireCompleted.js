import ELF from 'core/ELF';

/**
 * @memberOf ELF.expressions
 * @description
 * Determines if the questionnaire has been completed.  This is only really
 * useful to determine if the questionnaire is running the questionnaire completion workflow.
 * @returns {Q.Promise<boolean>}
 */
export default function isQuestionnaireCompleted () {
    return Q(!!localStorage.getItem('questionnaireCompleted'));
}

ELF.expression('isQuestionnaireCompleted', isQuestionnaireCompleted);
