import ELF from '../ELF';

/**
 * Determines if the application is wrapped via cordova.
 * @param {null} input - Not used in this expression.
 * @param {Function} done - Invoked upon completion of expression.
 */
export function isWrapped (input, done) {
    LF.Wrapper.exec({
        execWhenWrapped : function () {
            done(true);
        },
        execWhenNotWrapped : function () {
            done(false);
        }
    });
}

ELF.expression('isWrapped', isWrapped);