
import ELF from 'core/ELF';

/**
 * Determines if today is in the provided list of days.
 * @param {Object} input - Expression input.
 * @param {string[]} input.days - The list of days.
 * @param {Function} done - Invoked when the expression is complete.
 * @example
 * {
 *    expression : 'isDayOfTheWeek',
 *    input      : { days : ['Monday', 'Wednesday', 'Friday'] }
 * }
 */
export function isDayOfTheWeek (input, done) {
    let date = new Date(),
        days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
        today = days[date.getDay()];

    done(input.days.indexOf(today) !== -1);
}

ELF.expression('isDayOfTheWeek', isDayOfTheWeek);