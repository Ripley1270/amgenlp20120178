import ELF from 'core/ELF';

/**
 * Determines if the running application is a LogPad.
 * @param {null} input - Not used with this expression.
 * @param {Function} done - Invoked upon completion of the expression.
 */
export function isLogPad (input, done) {
    done(LF.appName === 'LogPad App');
}

ELF.expression('isLogPad', isLogPad);