import ELF from 'core/ELF';
import COOL from 'core/COOL';

/**
 * @memberOf ELF.expressions
 * @description
 * An ELF expression that determines if the device is online.
 * @param {Object} input Input provided by ELF.
 * @returns {Q.Promise<boolean>} True if online, false if not.
 */
export function online (input) {
    return Q.Promise(resolve => {
        COOL.getClass('Utilities').isOnline((status) => {
            input.isOnline = status;

            resolve(status);
        })
        .done();
    });
}

ELF.expression('isOnline', online);
