import ELF from 'core/ELF';

/**
 * Determines if the current time is between a given range.
 * @param {Object} input - The provided time range.
 * @param {string} input.start - The start of the time range.
 * @param {string} input.end - The end of the time range.
 * @param {Function} done - Invoked upon completion of the expression.
 */
export function isBetweenTimeRange (input, done) {
    let date = new Date(),
        hours = date.getHours(),
        minutes = date.getMinutes(),
        current = (hours * 60) + minutes,
        convert = function (time) {
            time = time.split(':');

            return (parseInt(time[0], 10) * 60) + parseInt(time[1], 10);
        }, start, end;

    start = convert(input.start);
    end = convert(input.end);

    done(current >= start && current <= end);
}

ELF.expression('isBetweenTimeRange', isBetweenTimeRange);