import ELF from 'core/ELF';
import { getNested } from 'core/utilities';

/**
 * Determines if a form has been changed from it's original state.
 * @param {Object[]} [config] An optional configuration for comparing original form values.
 * @returns {Q.Promise<boolean>}
 * @example
 * evaluate: ['AND', 'isDirectionForward', {
 *      expression: 'hasFormChanged',
 *      input: [{
 *          SW_Alias: 'Edit_User.0.EDIT_USER_1',
 *          question_id: 'EDIT_USER_USERNAME',
 *          questionnaire_id: 'Edit_User',
 *          // The property on this response is determine by the 'field' property
 *          // on the question's widget configuration. e.g. { username: 'jsmith' }
 *          // Indicate that the response is JSON.
 *          isJSON: true,
 *          // Determine the field name of the JSON response. e.g. { username: ... }
 *          field: 'username',
 *          // Use the user model scoped to the UserStatusQuestionnaireView (this.user).
 *          model: 'user',
 *          // Use the user model's username property to populate the response.
 *          // e.g. this.user.get('username');
 *          property: 'username'
 *      }, ...]
 *  }]
 */
export default function hasFormChanged (config) {
    let changed = false;

    // If there is no configuration provided, just loop through the answers collection
    // and determine if any of the models has changed.
    // NOTE: This isn't reliable as providing a configuration.  Reverting a field back
    // to the original value will still register as a change.
    if (config == null) {
        changed = this.answers.any(answer => {
            return answer.hasChanged();
        });
    } else {
        changed = _.any(config, (field) => {
            // Find the answer model the matches the configuration item's.
            let answer = this.answers.findWhere({ SW_Alias: field.SW_Alias });

            // Use getNested to parse out the configured model from the context.
            // This allows for configuring a model deep within the context.
            // e.g. model: 'parent.child.model'
            let source = getNested(this, field.model);

            let response;

            // If the response should be JSON, format it as such.
            if (field.isJSON) {

                // Fetch the response from the answer model.
                let parsed = JSON.parse(answer.get('response'));

                response = parsed[field.field || field.property];
            } else {
                response = answer.get('response');
            }

            // Determine if the anser record differs from the original source.
            return source.get(field.property) !== response;
        });
    }

    return Q(changed);
}

ELF.expression('hasFormChanged', hasFormChanged);
