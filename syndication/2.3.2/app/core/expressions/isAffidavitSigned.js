import ELF from 'core/ELF';

/**
 * @memberOf ELF.expressions
 * @description
 * Determines if the affidavit has been signed
 * This should only be used in the context of a questionnaire
 * @returns {Q.Promise<boolean>}
 */
export default function isAffidavitSigned () {
	let affidavitWidget = LF.Helpers.getWidget('AFFIDAVIT');
	
    return Q(!!affidavitWidget && affidavitWidget.isAffidavitAnswered());
}

ELF.expression('isAffidavitSigned', isAffidavitSigned);
