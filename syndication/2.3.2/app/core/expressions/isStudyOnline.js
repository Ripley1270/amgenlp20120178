import ELF from '../ELF';
import Spinner from 'core/Spinner';
import COOL from 'core/COOL';
import WebService from 'core/classes/WebService';

/**
 * Check if study is online by pinging ping.ashx for for the study.
 * This funciton must not be called anywhere when running LPA in trainer mode.
 * Since trainer can run w/o NetPRO, ping.ashx will not be available.
 * @param {function} callback A callback function invoked upon completion.
 * @returns {Q.Promise<void>}
 */
export function pingStudyApi (callback = $.noop) {
    // TODO: Moved this function from utilities as is. It will be refactored in the project's main stream.
    // This is a quick fix for ping.
    // We don't really care what the response is, only that there is one.
    // @todo check xhr status...
    // Stakutis Fixed during Trainer work
    return COOL.new('WebService', WebService).getSubjectActive(1)
    .then(() => callback(true))
    .catch(err => callback(false, err))
    .finally(() => {
    });
}

/**
 * Determines if the study is online.
 * @param {Object} input - ELF context.
 * @param {Function} done - Invoked upon completion of the expression.
 */
export function studyOnline (input, done) {
    Spinner.show()
    .then(() => {
        pingStudyApi((status, code) => {
            input.httpRespCode || (input.httpRespCode = code);

            Spinner.hide()
            .done();

            done(status);
        });
    })
    .done();
}

ELF.expression('isStudyOnline', studyOnline);
