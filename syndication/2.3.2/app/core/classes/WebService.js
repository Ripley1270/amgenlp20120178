// jscs:disable disallowKeywords, requireTemplateStrings, requireArrowFunctions
import { timeStamp, parseTime } from 'core/DateTimeUtil';
import * as lStorage from 'core/lStorage';
import Logger from 'core/Logger';
import UserSync from 'core/classes/UserSync';
import Sites from 'core/collections/Sites';
import Subjects from 'core/collections/Subjects';
import { convertXmlToJson, filterCastModelData, zeroPad, isLogPad, getProductName } from 'core/utilities';
import COOL from 'core/COOL';
import CurrentSubject from 'core/classes/CurrentSubject';

let logger = new Logger('WebService');

/**
 * A class that creates the WebService object.
 * @class WebService
 * @example LF.webService = new LF.Class.WebService();
 */
export default class WebService {
    /**
     * Does the AJAX RESTful call to the Server.
     * @param {Object} ajaxConfig The data required to make AJAX call.
     * @param {Object} data The data sent with AJAX call.
     * @param {Function} onSuccess A callback function invoked upon successful completion.
     * @param {Function} onError A callback function invoked upon failure.
     * @returns {Q.Promise<*>}
     * @example this.transmit({
     *      type: 'GET',
     *      uri: '/subjects/12345678'
     *  }, {}, onSuccess, onError);
     */
    transmit (ajaxConfig, data, onSuccess = $.noop, onError = $.noop) {
        let authorization,
            deferred = Q.defer(),
            timeout = (LF.StudyDesign.transmissionTimeout || LF.CoreSettings.transmissionTimeout) * 1000,
            now = new Date();

        if (ajaxConfig.type == null) {
            throw new Error('Missing Argument: type is null or undefined.');
        } else if (ajaxConfig.uri == null) {
            throw new Error('Missing Argument: uri is null or undefined.');
        }

        // TODO: Getting the current subject will work for logpad only.
        // This should be fixed by saving the authorization key somewhere in an upper level and using that here.
        // Core code should not care about which product(sitepad/logpad) is running
        CurrentSubject.getSubject()
        .then(subject => {
            let storedProc = data && data.StoredProc ? ` with stored procedure "${data.StoredProc}"` : '',
                syncLevel = data && data.level ? ` for syncLevel "${data.level}"` : '';

            if (ajaxConfig.auth || ajaxConfig.auth === '') {
                authorization = ajaxConfig.auth;
            } else {
                if (subject && isLogPad()) {
                    authorization = [
                        subject.get('subject_id'),
                        subject.get('service_password'),
                        subject.get('device_id')
                    ].join(':');
                } else {
                    authorization = '';
                }
            }

            logger.operational(`Attempt to transmit ${ajaxConfig.type} ${ajaxConfig.uri}${storedProc}${syncLevel} started.`, {
                storedProc: data.StoredProc,
                syncLevel: data.level,
                method: ajaxConfig.type,
                uri: ajaxConfig.uri
            }, 'CommStart');

            $.ajax({
                type: ajaxConfig.type,
                url: ajaxConfig.uri,
                data: ajaxConfig.type === 'GET' ? '' : JSON.stringify(data),
                dataType: ajaxConfig.dataType || 'json',
                contentType: 'application/json',
                timeout: timeout,
                cache: false,

                // jQuery is setting the context of this... Fat arrows don't work properly.
                success: _((res, status, jqXHR) => {
                    let isSubjectActive = jqXHR.getResponseHeader('X-Device-Status') == null ? 1 : +jqXHR.getResponseHeader('X-Device-Status'),
                        syncID = jqXHR.getResponseHeader('X-Sync-ID'),
                        isDuplicate = !!parseInt(jqXHR.getResponseHeader('X-Diary-Status'), 10);

                    let path = ajaxConfig.uri.replace(/https?:\/\/[^\/]+\//, '');
                    // Only log up to 100 chars of response at OPERATIONAL level;
                    // keep in mind all this data gets POST'ed back to server
                    logger.operational(`Successful request ${ajaxConfig.type} ${path}`, {
                        uri: ajaxConfig.uri,
                        method: ajaxConfig.type,
                        response: (typeof res === 'string' ? res : JSON.stringify(res)).slice(0,100)
                    }, 'CommEnd');
                    // But log the whole thing at TRACE level (goes to console only)
                    logger.trace(`Successful request ${ajaxConfig.type} ${path}`, {
                        response: typeof res === 'string' ? res : JSON.stringify(res)
                    });

                    onSuccess(res, {syncID, isSubjectActive, isDuplicate});

                    deferred.resolve(res, {syncID, isSubjectActive, isDuplicate});
                }).bind(this),

                // jQuery is setting the context of this... Fat arrows don't work properly.
                error: _((jqXHR, status) => {
                    let isSubjectActive = jqXHR.getResponseHeader('X-Device-Status') == null ? 1 : +jqXHR.getResponseHeader('X-Device-Status'),
                        errorCode = jqXHR.getResponseHeader('X-Error-Code'),
                        httpCode = jqXHR.status,
                        swisError = errorCode ? ` and SWIS error: ${errorCode}` : '',
                        isTimeout = (status === 'timeout');

                    logger.operational(`Unsuccessful transmission ${ajaxConfig.type} ${ajaxConfig.uri} with HTTP code: ${httpCode} ${swisError}`, {}, 'CommFail');

                    onError(errorCode, httpCode, isSubjectActive, isTimeout);
                    deferred.reject({errorCode, httpCode, isSubjectActive});
                }).bind(this),
                beforeSend: (xhr) => {
                    if (xhr) {
                        // If set as blank it will fail!
                        if (authorization !== '') {
                            xhr.setRequestHeader('Authorization', authorization);
                        }
                        xhr.setRequestHeader('Clientdate', timeStamp(now));
                        if (lStorage.getItem('apiToken')) {
                            xhr.setRequestHeader('X-Sync-API-Token', lStorage.getItem('apiToken'));
                        }
                        if (ajaxConfig.syncID && ajaxConfig.syncID !== 'null') {
                            xhr.setRequestHeader('X-Sync-ID', ajaxConfig.syncID);
                        }
                        if (ajaxConfig.jsonh) {
                            xhr.setRequestHeader('Diary-Encoding', 'jsonm');
                        }
                        if (ajaxConfig.unlockCode) {
                            xhr.setRequestHeader('Unlock-Code', ajaxConfig.unlockCode);
                        }
                    }
                }
            });
        }).done();

        return deferred.promise;
    }

    /**
     * Build and return URL string.
     * @param {string} fragment - URL fragment to append to the end of the string.
     * @returns {string}
     */
    buildUri (fragment) {
        let serviceBase = lStorage.getItem('serviceBase');

        return ((serviceBase != null) ? `${serviceBase}/api` : LF.apiBase) + fragment;
    }

    // TODO: This should go into webservice class of each modality in 2.4
    /**
     * Returns the source string required by some of the APIs.
     * @returns {string}
     */
    getSource () {
        switch (getProductName()) {
            case 'sitepad':
                return 'SitePad';
            case 'logpad':
                return 'LogPadApp';
            default:
                return null;
        }
    }

    /**
     * Register a SitePad device.
     * @param {object} data - Data passed into the transmit method.
     * @returns {Q.Promise<Object>}
     */
    registerDevice (data) {
        logger.trace('registerDevice', data);
        let ajaxConfig = {
            type: 'POST',
            uri: this.buildUri('/v1/devices2')
        };
        logger.trace('registerDevice',data);

        return this.transmit(ajaxConfig, data, $.noop, $.noop, 'registerDevice');
    }

    /**
     * Get a list of available sites that belong to a given study.
     * @param {String} studyName The name of the study to get sites for.
     * @param {function} onSuccess A callback function invoked upon successful completion of the request.
     * @param {Function} onError A callback function invoked upon failure of the request.
     * @returns {Q.Promise<Object>} A deferred promise.
     */
    getSites (studyName, onSuccess, onError) {
        let uri = this.buildUri(`/v1/sites/${studyName}`);
        logger.trace(`getSites for ${studyName}`);

        return this.transmit({
            type: 'GET',
            uri: uri
        }, {}, onSuccess, onError, 'getSites');
    }

    /**
     * Send the new Patient using Transmission Queue.
     * @param {String} assignment The subject assignment to be sent.
     * @param {String} auth Authorization token.
     * @returns {Q.Promise<res, { syncId, isSubjectActive, isDuplicate }>} Q promise
     */
    sendSubjectAssignment (assignment, auth) {
        let ajaxConfig = {
            type: 'POST',
            uri: this.buildUri('/v1/subjects/'),
            auth: auth,
            jsonh: LF.StudyDesign.jsonh !== false ? true : false
        };

        return this.transmit(ajaxConfig, assignment, $.noop, $.noop, 'sendSubjectAssignment');
    }

    /**
     * Retrieve Subject's active status
     * @param {String} setupCode The Setup code of the subject.
     * @param {Function} onSuccess A callback function invoked upon successful completion.
     * @param {Function} onError A callback function invoked upon failure.
     * @returns {Q.Promise<Object>}
     * @example LF.webservice.getSubjectActive('12345678', onSuccess, onError);
     */
    getSubjectActive (setupCode, onSuccess = $.noop, onError) {
        let ajaxConfig;

        logger.trace(`getSubjectActive for setupCode:${setupCode}`);

        if (setupCode == null) {
            throw new Error('Missing Argument: setupCode is null or undefined.');
        }

        ajaxConfig = {
            type: 'GET',
            uri: this.buildUri(`/v1/subjects/${setupCode}`)
        };

        return this.transmit(ajaxConfig, {}, onSuccess, onError, 'getSubjectActive');
    }

    /**
     * Retrieve Subject's security question
     * @param {String} setupCode The Setup code of the subject.
     * @param {Function} onSuccess A callback function invoked upon successful completion.
     * @param {Function} onError A callback function invoked upon failure.
     * @returns {Q.Promise<Object>}
     * @example LF.webservice.getSubjectQuestion('12345678', onSuccess, onError);
     */
    getSubjectQuestion (setupCode, onSuccess, onError) {
        let ajaxConfig;
        logger.trace(`getSubjectQuestion for ${setupCode}`);

        if (setupCode == null) {
            throw new Error('Missing Argument: setupCode is null or undefined.');
        }

        ajaxConfig = {
            type: 'GET',
            uri: this.buildUri(`/v1/devices?UserID=${setupCode}`)
        };

        return this.transmit(ajaxConfig, { }, onSuccess, onError, 'getSubjectQuestion');
    }

    /**
     * Do subject sync with Middle-Tier and get all non-DCF data back, and DCF data which are changed in SW.
     * @param {String} setupCode The Setup code of the subject.
     * @param {String} auth AJAX authorization token.
     * @param {Function} onSuccess A callback function invoked upon successful completion.
     * @param {Function} onError A callback function invoked upon failure.
     * @returns {Q.Promise<Object>}
     * @example LF.webservice.doSubjectSync('12345678', '12345678:3bb7b3b1631b95534:4f4-17bd-38134c', onSuccess, onError);
     */
    doSubjectSync (setupCode, auth, onSuccess, onError) {
        let ajaxConfig;

        logger.trace(`doSubjectsync for ${setupCode}`);

        if (setupCode == null) {
            throw new Error('Missing Argument: setupCode is null or undefined.');
        }

        ajaxConfig = {
            type: 'GET',
            uri: this.buildUri(`/v1/subjects/${setupCode}`),
            auth: auth,
            syncID: localStorage.getItem('PHT_syncID')
        };

        return this.transmit(ajaxConfig, {}, onSuccess, onError, 'doSubjectSync');
    }

    /**
     * Get subject data from Middle-Tier and get all non-DCF data back, and DCF data which are changed in SW.
     * @param {string} krpt - The krpt of the subject.
     * @param {string} auth - AJAX authorization token.
     * @returns {Q.Promise<Object>}
     * @example webservice.getSingleSubject('12345678', '12345678:3bb7b3b1631b95534:4f4-17bd-38134c');
     */
    getSingleSubject (krpt, auth) {
        let ajaxConfig;

        logger.trace(`getSingleSubject for ${krpt}`);

        if (krpt == null) {
            throw new Error('Missing Argument: krpt is null or undefined.');
        }

        ajaxConfig = {
            type: 'GET',
            uri: this.buildUri(`/v1/subjects/${krpt}`),
            auth
        };

        return this.transmit(ajaxConfig, {}, $.noop, $.noop, 'getSingleSubject');
    }

    /**
     * Get all subject data for a site from Middle-Tier and get all non-DCF data back, and DCF data which are changed in SW.
     * @param {Object} params - Parameters passed into the method.
     * @param {String} params.auth AJAX authorization token.
     * @param {Function} onSuccess A callback function invoked upon successful completion.
     * @param {Function} onError A callback function invoked upon failure.
     * @returns {Q.Promise<Array>}
     * @example LF.webservice.getAllSubjects({ auth: '12345678:3bb7b3b1631b95534:4f4-17bd-38134c' }, onSuccess, onError);
     */
    getAllSubjects (params = {}, onSuccess, onError) {
        let ajaxConfig = {
                type: 'GET',
                uri: this.buildUri('/v1/subjects'),
                auth: params.auth || ''
            },
            convertSubjectIDFormat = function (patientID, siteCode) {
                let idFormat = LF.StudyDesign.participantSettings.participantIDFormat,
                    patientPortion = LF.StudyDesign.participantSettings.participantNumberPortion,
                    sitePortion = LF.StudyDesign.participantSettings.siteNumberPortion,
                    formattedSite,
                    formattedPatient,
                    fullFormatted;

                formattedSite = zeroPad(siteCode, sitePortion[1] - sitePortion[0]);
                formattedPatient = zeroPad(patientID, patientPortion[1] - patientPortion[0]);
                fullFormatted = '';

                // Walk the string and build our code from the idFormat with patient and site ranges spliced in.
                for (let i = 0, len = idFormat.length; i < len; ++i) {
                    if (i >= sitePortion[0] && i < sitePortion[1]) {
                        fullFormatted += formattedSite.charAt(i - sitePortion[0]);
                    } else if (i >= patientPortion[0] && i < patientPortion[1]) {
                        fullFormatted += formattedPatient.charAt(i - patientPortion[0]);
                    } else {
                        fullFormatted += idFormat.charAt(i);
                    }
                }

                return fullFormatted;
            };

        logger.trace('getAllSubjects');

        return Q.all([
            Sites.fetchCollection(),
            Subjects.fetchCollection(),
            this.transmit(ajaxConfig, {}, $.noop, $.noop, 'getAllSubjects')
        ])
        // eslint-disable-next-line consistent-return
        .spread((sites, subjects, incomingSubjects) => {
            let site = sites.at(0),
                filteredSubjects = [];

            if (incomingSubjects.length) {
                for (let i = 0, len = incomingSubjects.length; i < len; i++) {
                    let incomingSubject = incomingSubjects[i],
                        subject = subjects.findWhere({ krpt: incomingSubject.krpt });

                    incomingSubject.subject_active = 1;
                    incomingSubject.site_code = site.get('siteCode');
                    incomingSubject.subject_number = incomingSubject.subjectId.substring(
                        LF.StudyDesign.participantSettings.participantNumberPortion[0],
                        LF.StudyDesign.participantSettings.participantNumberPortion[1]
                    );
                    incomingSubject.subject_id = convertSubjectIDFormat(incomingSubject.subject_number, incomingSubject.site_code);
                    incomingSubject.service_password = incomingSubject.password;
                    incomingSubject.subject_password = incomingSubject.clientPassword;
                    incomingSubject.secret_answer = incomingSubject.securityAnswer;

                    // If the incoming subject exists in the client and the phaseStartDateTZOffset is greater than
                    // the incoming timestamp then we ignore phase data for the subject. See DE19851
                    if (subject && new Date(subject.get('phaseStartDateTZOffset')) > new Date(incomingSubject.phaseStartDate)) {
                        delete incomingSubject.phase;
                    } else {
                        incomingSubject.phase = JSON.parse(incomingSubject.phase);
                        incomingSubject.phaseStartDateTZOffset = incomingSubject.phaseStartDate;
                    }

                    if (incomingSubject.setupCode) {
                        incomingSubject.setupCode = `${incomingSubject.setupCode}`;
                    }

                    if (incomingSubject.securityQuestion != null) {
                        incomingSubject.secret_question = `${incomingSubject.securityQuestion}`;
                    }

                    filteredSubjects.push(filterCastModelData(incomingSubject, 'Subject'));
                }

                return filteredSubjects;
            }
        })
        .done(subjects => {
            onSuccess(subjects);
        }, err => {
            err && logger.error('Error while syncing Subjects', err);
            onError();
        });
    }

    /**
     * Query the subject Data and always gets the data back if it is non-DCF data.
     * The Middle-Tier will return only data changed in SW if it is DCF data.
     * @param {String} setupCode The Setup code of the subject.
     * @param {String []} queryList An array of strings listing the subject's query fields.
     * @param {String} auth AJAX authorization token.
     * @param {Function} onSuccess A callback function invoked upon successful completion.
     * @param {Function} onError A callback function invoked upon failure.
     * @returns {Q.Promise<Object>}
     * @example LF.webservice.querySubjectData('12345678', ['Phase', 'LogLevel', 'SiteCode'],
     *          '12345678:3bb7b3b1631b95534:4f4-17bd-38134c', onSuccess, onError);
     */
    querySubjectData (setupCode, queryList, auth, onSuccess, onError) {
        let ajaxConfig,
            index = 1,
            query;

        logger.trace(`querySubjectData for ${setupCode} list:${queryList}`);
        if (setupCode == null) {
            throw new Error('Missing Argument: setupCode is null or undefined.');
        }

        if (queryList == null) {
            throw new Error('Missing Argument: queryList is null or undefined.');
        }

        query = queryList.length ? `?fields=${queryList[0]}` : '';

        for (; index < queryList.length; index++) {
            query +=  `,${queryList[index]}`;
        }

        ajaxConfig = {
            type: 'GET',
            uri: this.buildUri(`/v1/subjects/${setupCode}${query}`),
            auth: auth,
            syncID: localStorage.getItem('PHT_syncID')
        };

        return this.transmit(ajaxConfig, { }, onSuccess, onError, 'querySubjectData');
    }

    /**
     * Updates the Subject's Data.
     * @param {String} deviceID The device ID for the subject.
     * @param {Object} subjectData The data needs to be updated.
     * @param {String} [auth] AJAX authorization token.
     * @param {Function} onSuccess A callback function invoked upon successful completion.
     * @param {Function} onError A callback function invoked upon failure.
     * @returns {Q.Promise<res, { syncId, isSubjectActive, isDuplicate }>} Q promise
     * @example LF.webservice.updateSubjectData('4f4-17bd-38134c', {
     *  question  : 0,
     *  answer    : 'abcd',
     *  password  : 'qqqq'
     *  }, '12345678:3bb7b3b1631b95534:4f4-17bd-38134c', onSuccess, onError);
     */
    updateSubjectData (deviceID, subjectData, auth, onSuccess, onError) {
        let ajaxConfig;

        logger.trace(`updateSubjectData for deviceID:${deviceID}`, subjectData);
        if (deviceID == null) {
            throw new Error('Missing Argument: deviceID is null or undefined.');
        }

        ajaxConfig = {
            type: 'PUT',
            uri: this.buildUri(`/v1/devices/${deviceID}`),
            auth
        };

        return this.transmit(ajaxConfig, subjectData, onSuccess, onError, 'updateSubjectData');
    }

    /**
     * Used to set the subject password, secret question and secret answer for the first time.
     * @param {Object} payload - Payload configuration
     * @param {String} payload.U Setup Code
     * @param {String} payload.W Password
     * @param {String} payload.Q Secret Question
     * @param {String} payload.A Secret Answer
     * @param {String} payload.O Client Name
     * @param {Function} onSuccess A callback function invoked upon successful completion.
     * @param {Function} onError A callback function invoked upon failure.
     * @returns {Q.Promise<res, { syncId, isSubjectActive, isDuplicate }>} Q promise
     */
    setSubjectData (payload, onSuccess, onError) {
        logger.trace('setSubjectData', payload);

        let ajaxConfig = {
            type: 'POST',
            uri: this.buildUri('/v1/devices/')
        };

        return this.transmit(ajaxConfig, payload, onSuccess, onError, 'setSubjectData');
    }

    /*
     * Gets the subject credentials from backend
     * @param {string} krpt - krpt of the subject to get the credentials of
     * @param {function} onSuccess - A callback function invoked upon successful completion.
     * @param {function} onError - A callback function invoked upon failure.
     * @returns {Q.Promise<Object>}
     */
    getSubjectCredentials (krpt, onSuccess, onError) {
        logger.trace('getSubjectCredentials', krpt);

        let ajaxConfig = {
            type: 'GET',
            uri: this.buildUri(`/v1/subjects/${krpt}/credentials`)
        };

        return this.transmit(ajaxConfig, {}, onSuccess, onError, 'getSubjectCredentials');
    }

    /**
     * Resets the subject password and secret QA with unlock code.
     * @param {Object} payload - Payload to be sent with the API request
     * @param {number} payload.setupCode - Setup Code a.k.a pin
     * @param {string} payload.clientPassword - Password to be set
     * @param {number} payload.regDeviceId - regDeviceId returned by Netpro during registration
     * @param {number} payload.clientId - clientId returned by Netpro during registration
     * @param {Array} payload.challengeQuestions - an array of an object for secret question and answers to be set
     * @param {Object} payload.challengeQuestions[0] - An object literal for secret question and answer
     * @param {number} payload.challengeQuestions[0].question - Secret question
     * @param {string} payload.challengeQuestions[0].answer - Secret answer
     * @param {string} krpt - krpt of the subject to change the credentials of
     * @param {string} unlockCode - Daily unlock code
     * @param {function} onSuccess - A callback function invoked upon successful completion.
     * @param {function} onError - A callback function invoked upon failure.
     */
    resetSubjectCredentials (payload, krpt, unlockCode, onSuccess, onError) {
        logger.trace('resetSubjectCredentials', payload);

        payload.source = this.getSource();

        let ajaxConfig = {
            type: 'POST',
            unlockCode,
            uri: this.buildUri(`/v1/subjects/${krpt}/credentials`)
        };

        // TODO: Transmit method resolves the promise with only the result.
        // But this API call should also return the isSubjectActive flag. So we have to use onsuccess callback.
        // This issue is fixed in 2.4
        this.transmit(ajaxConfig, payload, onSuccess, onError, 'resetSubjectCredentials')
        .done();
    }

    /**
     * Resets the subject password
     * @param {Object} payload - Payload to be sent with the API request
     * @param {string} payload.clientPassword - Password to be set
     * @param {Array} payload.challengeQuestions - an array of an object for secret question and answers to be set
     * @param {Object} payload.challengeQuestions[0] - An object literal for secret question and answer
     * @param {number} payload.challengeQuestions[0].question - Secret question
     * @param {string} payload.challengeQuestions[0].answer - Secret answer
     * @param {string} krpt - krpt of the subject to change the credentials of
     * @param {function} onSuccess - A callback function invoked upon successful completion.
     * @param {function} onError - A callback function invoked upon failure.
     */
    resetSubjectPassword (payload, krpt, onSuccess, onError) {
        logger.trace('resetSubjectPassword', payload);

        payload.source = this.getSource();

        let ajaxConfig = {
            type: 'PUT',
            uri: this.buildUri(`/v1/subjects/${krpt}/credentials`)
        };

        // TODO: Transmit method resolves the promise with only the result.
        // But this API call should also return the isSubjectActive flag. So we have to use onsuccess callback.
        // This issue is fixed in 2.4
        this.transmit(ajaxConfig, payload, onSuccess, onError, 'resetSubjectPassword')
        .done();
    }

    /**
     * Gets subject setup code
     * @param {Object} krpt krpt of the subject of which setup code is to be returned
     * @returns {Q.Promise<res, { syncId, isSubjectActive, isDuplicate }>} Q promise
     */
    getSetupCode (krpt) {
        return Q.Promise((resolve, reject) => {
            let ajaxConfig = {
                    type: 'POST',
                    uri: this.buildUri('/v1/SWAPI/GetDataClin')
                },
                data = {
                    StoredProc: 'LPA_getSetupCode',
                    Params: [krpt]
                },
                handleResult = (res) => {
                    if (res) {
                        resolve(res);
                    } else {
                        reject(`No setup code could be found for the krpt: ${krpt}`);
                    }
                };

            this.transmit(ajaxConfig, data, handleResult, reject);
        });
    }

    /**
     * Get the device ID for the subject.
     * @param {Object} subjectData The Subject's data used to retrieve the device ID.
     * @param {Function} onSuccess A callback function invoked upon successful completion.
     * @param {Function} onError A callback function invoked upon failure.
     * @returns {Q.Promise<res, { syncId, isSubjectActive, isDuplicate }>} Q promise
     * @example LF.webservice.getDeviceID({
     *      U: '12345678',
     *      W: 'qqqq'
     *  }, onSuccess, onError);
     * @example LF.webservice.getDeviceID({
     *      U: '12345678',
     *      W: 'qqqq',
     *      code: 123456
     *  }, onSuccess, onError);
     */
    getDeviceID (subjectData, onSuccess, onError) {
        let ajaxConfig = {
            type: 'POST',
            uri: this.buildUri('/v1/devices'),
            unlockCode: subjectData.code ? subjectData.code : false
        };

        logger.trace('getDeviceID for subjectData', subjectData);

        if (subjectData.code) {
            subjectData.code = undefined;
        }

        return this.transmit(ajaxConfig, subjectData, onSuccess, onError, 'getDeviceID');
    }

    /**
     * Send the completed Diary using Transmission Queue.
     * @param {Object} diary - The completed diary to be sent.
     * @param {string} auth - The authentication token
     * @param {boolean} jsonh - The value indicating whether jsonh compression is being used or not
     * @param {Function} onSuccess - A callback function invoked upon successful completion.
     * @param {Function} onError - A callback function invoked upon failure.
     * @returns {Q.Promise<res, { syncId, isSubjectActive, isDuplicate }>} Q promise
     * @example LF.webservice.sendDiary({
     *  "id"        : 1,
     *  "SU"        :'Meds',...,
     *  "Answers"   :[{
     *      "response"  : "1",
     *      "SW_Alias"  : "Meds.0.MEDS_Q_1",..., {...},{...}]
     *  }, true, onSuccess, onError);
     */
    sendDiary (diary, auth, jsonh, onSuccess, onError) {
        let ajaxConfig = {
            type: 'POST',
            uri: this.buildUri('/v1/diaries/'),
            jsonh
        };

        // TODO: This looks like to be cecking if it is logpad. Change to isLogPad() or
        // better refocator it so that there is no need to check the running product
        if (!lStorage.getItem('apiToken')) {
            ajaxConfig.auth = auth;
        }

        return this.transmit(ajaxConfig, diary, onSuccess, onError, 'sendDiary');
    }

    /**
     * Logs to be sent.
     * @param {Object} logs Logs generated to be sent.
     * @param {Function=} onSuccess A callback function invoked upon successful completion.
     * @param {Function=} onError A callback function invoked upon failure.
     * @returns {Q.Promise<void>}
     * @example LF.webservice.sendLogs({
     *  "id"        : 1,...,
     *  "params"   :[{...},{...}]
     *  }, onSuccess, onError);
     */
    sendLogs (logs, onSuccess, onError) {
        let ajaxConfig = {
            type: 'POST',
            uri: this.buildUri('/v1/logs/')
        };

        return this.transmit(ajaxConfig, logs, onSuccess, onError, 'sendLogs');
    }

    /**
     * Gets last diary data from server by sending the stored procedure name and the required parameters
     * @param {Object} params Includes the authentication token
     * @param {Function} onSuccess A callback function invoked upon successful transmission.
     * @param {Function} onError A callback function invoked upon transmission failure.
     * @returns {Q.Promise<void>}
     */
    syncLastDiary (params = {}, onSuccess, onError) {
        logger.trace('syncLastDiary');
        let ajaxConfig = {
                type: 'POST',
                uri: this.buildUri('/v1/SWAPI/GetDataClin'),
                auth: params.auth || null
            },
            krpt = params.krpt || localStorage.getItem('krpt'),
            scheduleStart = new Date(parseTime('00:00', true)).toISOString(),
            unscheduledSuList = (LF.StudyDesign.lastDiarySyncUnscheduledSUList || []).join(',').match(/.{1,36}/g),
            payload = {
                StoredProc: 'lastDiaryTableSync_XML',
                Params: [krpt, scheduleStart].concat(unscheduledSuList)
            },
            handleResult = (res) => {
                let xml, data;

                try {
                    if (res !== 'null') {
                        xml = $($.parseXML(res)).children()[0];
                        data = convertXmlToJson(xml).diary;
                    } else {
                        data = [];
                    }
                } catch (err) {
                    logger.error(`Error converting lastDiary XML data to JSON. res: ${res}, err: ${err.toString()}`);
                }

                if (_.isArray(data)) {
                    if (params && params.activation) {
                        data = _.reject(data, (item) => item.deleted === '1');
                    }

                    _.each(data, (item) => {
                        let SU = item.SU,
                            questionnaire = LF.StudyDesign.questionnaires.where({ SU: SU })[0];

                        if (params && params.activation) {
                            delete item.deleted;
                        }

                        if (item.role) {
                            item.role = JSON.stringify(item.role);
                        }

                        item.krpt = krpt;
                        item.questionnaire_id = questionnaire ? questionnaire.get('id') : SU;
                        delete item.SU;
                    });
                } else {
                    throw new Error('Incorrect result type: Result type should be an array');
                }

                onSuccess(data);
            };

        return this.transmit(ajaxConfig, payload, handleResult, onError, 'syncLastDiary');
    }

    /**
     * Gets site access code hash from server by sending the stored procedure name and the required parameters
     * @param {Object} params Includes the authentication token
     * @param {Function} onSuccess A callback function invoked upon successful transmission.
     * @param {Function} onError A callback function invoked upon transmission failure.
     * @returns {Q.Promise<Object>}
     */
    getSiteAccessCode (params = {}, onSuccess, onError) {
        let subjectData,
            ajaxConfig = {
                type: 'POST',
                uri: this.buildUri('/v1/SWAPI/GetDataClin'),
                auth: params.auth || null
            },
            handleResult = (res) => {
                let xml,
                    record,
                    resData;

                try {
                    if (res) {
                        xml = $($.parseXML(res));
                        record = xml.find('UAC')[0];
                    }

                    resData = {
                        site_code: $(record).attr('SiteCode'),
                        study_name: $(xml.find('UACs')).attr('Stdy'),
                        hash: $(record).attr('Hash')
                    };

                    if (resData.site_code && resData.study_name && resData.hash) {
                        onSuccess(resData);
                    } else {
                        onError();
                    }
                } catch (err) {
                    onError();
                }
            },
            // eslint-disable-next-line consistent-return
            getSiteCode = () => {
                if (params.activation) {
                    subjectData = JSON.parse(localStorage.getItem('PHT_TEMP'));
                    return Q(subjectData ? subjectData.site_code : '');
                } else {
                    // FIXME: This is a logpad specific code and the cuurent subject is always available and is always the same
                    // This will work but webservice should not know or care about the cuurent subject.
                    // Instead pass the sitecode as a parameter
                    CurrentSubject.getSubject()
                    .then(subject => subject.get('site_code'));
                }
            };

        return getSiteCode()
        .then (siteCode => {
            let data = {
                StoredProc: 'PDE_LPA_SiteUserAccessCodes',
                Params: [siteCode]
            };

            return this.transmit(ajaxConfig, data, handleResult, onError);
        });
    }

    /**
     * Send new user data.
     * @param {Object} params - Parameters passed into the method.
     * @param {String} params.user_type type of the user ['admin' || null]
     * @param {String} params.user_name user's name
     * @param {String} params.language user's language
     * @param {String} params.password user's password
     * @param {String} params.key salt for the user's password hash
     * @param {String} params.role role of the new user
     * @param {String} params.syncLevel level of synchronization
     * @param {String} params.syncValue value of the sync level
     * @param {String} [params.auth] authorization token for the webservice.
     * @returns {q.promise<res, {syncId, isSubjectActive, isDuplicate}>} q promise
     * @example LF.webservice.addUser({
     *    auth      : subject.getAuth(),
     *    userType  : user.get('user_type'),
     *    userName  : user.get('user_name'),
     *    language  : user.get('language'),
     *    password  : user.get('password'),
     *    key       : user.get('key'),
     *    syncLevel : role.get('syncLevel'),
     *    syncValue : getSyncValue()
     *  });
     */
    addUser (params = {}) {
        logger.trace('addUser',params);
        let ajaxConfig = {
            type: 'POST',
            uri: this.buildUri('/v1/SWAPI/GetDataClin'),
            auth: params.auth || null
        }, data = {
            StoredProc: 'LPA_AddUser',
            Params: [
                params.userType,
                params.username,
                params.language,
                params.password,
                params.salt,
                params.secretQuestion || '',
                params.secretAnswer || '',
                params.role,
                params.syncLevel,
                params.syncValue,
                params.active
            ]
        };

        return this.transmit(ajaxConfig, data, $.noop, $.noop, 'addUser');
    }


    /**
     * Update existing user.
     * @param {Object} params - Parameters passed into the method.
     * @param {Number} params.id - The ID of the user.
     * @param {String} params.userType - type of the user ['admin' || null]
     * @param {String} params.username - user's name
     * @param {String} params.language - user's language
     * @param {String} params.role - role of the new user
     * @param {String} params.syncLevel - level of synchronization
     * @param {String} params.syncValue - value of the sync level
     * @param {String} [params.auth] - authorization token for the webservice.
     * @returns {q.promise<res, {syncId, isSubjectActive, isDuplicate}>} q promise
     * @example LF.webservice.updateUser({
     *    userId: user.get('userId'),
     *    userType: user.get('userType'),
     *    username: user.get('username'),
     *    role: user.get('role'),
     *    language: user.get('language'),
     *    active: user.get('active'),
     *    syncLevel,
     *    syncValue
     *  });
     */
    updateUser (params = {}) {
        logger.trace('updateUser', params);

        let ajaxConfig = {
            type: 'POST',
            uri: this.buildUri('/v1/SWAPI/GetDataClin'),
            auth: params.auth || null
        }, data = {
            StoredProc: 'synd_UpdateUser',
            Params: [
                params.userId,
                params.userType,
                params.username,
                params.language,
                params.role,
                params.syncLevel,
                params.syncValue,
                params.active
            ]
        };

        return this.transmit(ajaxConfig, data, $.noop, $.noop, 'updateUser');
    }
    /**
     * Update non-subject User's password and/or security question and answer
     * @param {Object} params - Parameters passed into the method.
     * @param {String} [params.auth] authorization token for the webservice.
     * @param {String} params.userId unique id of user to update
     * @param {String} params.password new password we want to send
     * @param {String} params.salt salt of the new password
     * @param {String} params.secretQuestion secretQuestion of user to update
     * @param {String} params.secretAnswer secretAnswer of user to update
     * @returns {q.promise<res, {syncId, isSubjectActive, isDuplicate}>} q promise
     * @example LF.webservice.updateUserCredentials({
     *    userId         : '2',
     *    password       : 'newPassword'
     *    salt           : 'jash2u329udisf93',
     *    secretQuestion : '0',
     *    secretAnswer   : 'newAnswer'
     *  }).then(onSuccess, onError);
     */
    updateUserCredentials (params = {}) {
        let user = new LF.Model.User({ id: params.id });
        // Fetch the user by ID, fetch added as fix for DE18864.
        return user.fetch()
        .then(() => {
            logger.trace('updateUserCredentials');
            let ajaxConfig = {
                type: 'POST',
                uri: this.buildUri('/v1/SWAPI/GetDataClin'),
                auth: params.auth || null
            }, data = {
                StoredProc: 'LPA_UpdateUserCredentials',
                Params: [
                    params.userId || user.get('userId'),
                    params.password,
                    params.salt,
                    params.secretQuestion,
                    params.secretAnswer
                ]
            };
            return this.transmit(ajaxConfig, data, $.noop, $.noop, 'updateUserCredentials');
        });
    }

    /**
     * Update Get all users that have been added/updated since PHT_lastUpdatedUsers or 0
     * @param {Object} params - Parameters passed into the method.
     * @param {string} params.auth - Authorization token.
     * @param {Function=} onSuccess A callback function invoked upon successful completion.
     * @param {Function=} onError A callback function invoked upon failure.
     * @example LF.webservice.syncUsers(params);
     */
    syncUsers (params = {}, onSuccess = $.noop, onError = $.noop) {
        logger.trace('syncUsers',params);
        let getData, newLastUpdated,
            lastUpdated = lStorage.getItem('lastUpdatedUsers') || null,
            syncLevels = LF.StudyDesign.roles.getSyncLevels();

        getData = (level, value) => {
            let ajaxConfig = {
                type: 'POST',
                uri: this.buildUri('/v1/SWAPI/GetDataClin'),
                auth: params.auth || null
            },
            data = {
                level,
                StoredProc: 'LPA_SyncUsers_XML',
                Params: [lastUpdated, level, value]
            };

            return this.transmit(ajaxConfig, data, $.noop, $.noop, 'syncUsers')
            // eslint-disable-next-line consistent-return
            .then(res => {
                // Stored Proc returns string 'null' if no users are found
                if (res && res !== 'null') {
                    let i, len, xml, result, users,
                        filteredUsers = [];

                    try {
                        // The actual code is the else block. If block is a workaround for unit tests.
                        if (typeof res !== 'string') {
                            result = res;
                        } else {
                            xml = $($.parseXML(res)).children()[0];
                            result = convertXmlToJson(xml);
                        }

                        users = result.user;
                        newLastUpdated = result.lastUpdated;
                    } catch (e) {
                        logger.error('User data retrieved is not a valid XML', res);
                        // eslint-disable-next-line no-throw-literal
                        throw '';
                    }

                    if (users) {
                        for (i = 0, len = users.length; i < len; i++) {
                            filteredUsers.push(filterCastModelData(users[i], 'User'));
                        }
                    }

                    // users.map(user => {
                    //     user.active = 1;
                    // });

                    return filteredUsers;
                }
            });
        };

        // eslint-disable-next-line consistent-return
        Q().then(() => {
            if (syncLevels.length === 0) {
                logger.operational('No SyncLevels found, skipping sync.');
            } else {
                return Q.all(_.map(syncLevels, syncLevel =>
                    // eslint-disable-next-line consistent-return
                    UserSync.getValue(syncLevel).then(syncVal => {
                        if (syncVal === false) {
                            logger.operational(`No value for syncLevel: ${syncLevel}. Skipping sync`);

                            // Exit Q.all
                            // eslint-disable-next-line no-throw-literal
                            throw '';
                        } else if (syncVal || syncVal === '') {
                            return getData(syncLevel, syncVal)
                            .catch(() => {
                                // Exit Q.all
                                // eslint-disable-next-line no-throw-literal
                                throw '';
                            });
                        }
                    })
                ))
                .then(users => {
                    lStorage.setItem('lastUpdatedUsers', newLastUpdated);

                    return _(users).chain().flatten().compact().value();
                });
            }
        })
        .done(users => {
            onSuccess(users);
        }, err => {
            err && logger.error('Error while syncing users', err);
            onError();
        });
    }

    /**
     * Gets admin password hash from server by sending the stored procedure name and the required parameters
     * @param {Object} params - Parameters passed into the method.
     * @param {string} params.auth - Authorization token.
     * @param {Function=} onSuccess A callback function invoked upon successful transmission.
     * @param {Function=} onError A callback function invoked upon transmission failure.
     * @returns {Q.Promise<string>}
     */
    getAdminPassword (params = {}, onSuccess = $.noop, onError = $.noop) {
        logger.trace('getAdminPassword');
        let ajaxConfig = {
                type: 'POST',
                uri: this.buildUri('/v1/SWAPI/GetDataClin'),
                auth: params.auth || null
            };

        return Q(JSON.parse(localStorage.getItem('PHT_site')))
        .then(siteData => {
            let data = {
                StoredProc: 'LPA_getRoleAccessCodes_XML',
                Params: [siteData.krdom]
            };

            LF.StudyDesign.useAcccessCodesFor1dot10 && data.Params.push(1);

            return data;
        })
        .then(data => this.transmit(ajaxConfig, data, $.noop, $.noop, 'getAdminPassword'))
        .then(res => {
            let xml, adminRes;

            // The actual code is the else block. If block is a workaround for unit tests.
            if (typeof res !== 'string') {
                adminRes = res[0];
            } else {
                xml = $($.parseXML(res)).children()[0];
                adminRes = convertXmlToJson(xml).accessCode[0];
            }

            onSuccess(adminRes);
            return adminRes;
        })
        .catch(err => {
            err && logger.error('Error retrieving admin password', err);
            onError();
        });
    }

    /**
     * Gets krdom from server by sending the stored procedure name and the required parameters
     * @param {Object} params - Parameters passed into the method.
     * @param {string} params.auth - Optional authorization token.
     * @param {Function} onSuccess A callback function invoked upon successful transmission.
     * @param {Function} onError A callback function invoked upon transmission failure.
     * @returns {Q.Promise<string>}
     */
    getkrDom (params = {}, onSuccess = $.noop, onError = $.noop) {
        logger.trace('getkrDom');
        let ajaxConfig = {
            type: 'POST',
            uri: this.buildUri('/v1/SWAPI/GetDataClin'),
            auth: params.auth || null
        },
        data = {
            StoredProc: 'LPA_getKrdom_XML',
            Params: [localStorage.getItem('krpt')]
        };

        return this.transmit(ajaxConfig, data, $.noop, $.noop, 'getkrDom')
        .then(res => {
            let xml, siteRes;

            // The actual code is the else block. If block is a workaround for unit tests.
            if (typeof res !== 'string') {
                siteRes = res[0];
            } else {
                xml = $($.parseXML(res)).children()[0];
                siteRes = convertXmlToJson(xml).site[0];
            }

            onSuccess(siteRes);
            return siteRes;
        })
        .catch(err => {
            err && logger.error('Error retrieving krdom', err);
            onError();
        });
    }

    /**
     * Sends Edit Patient Diary data
     * @param {Object} params data to be sent
     * @param {Object} auth the authentication token
     * @returns {Q.Promise<res, { syncId, isSubjectActive, isDuplicate }>} Q promise
     */
    sendEditPatient (params, auth) {
        let ajaxConfig = {
            type: 'PUT',
            uri: this.buildUri(`/v1/subjects/${params.K}`),
            auth: auth,
            jsonh: LF.StudyDesign.jsonh !== false
        };

        return this.transmit(ajaxConfig, params, $.noop, $.noop, 'sendEditPatient');
    }

    /**
     * Send krpt, krDom, lastUpdated, deviceId and get all visits and forms status for that site.
     * @param  {object} params parameters
     * @param  {string} [params.auth] auth token
     * @param  {string} [params.krDom] site krDom
     * @param  {string} [params.lastUpdated] last update timestamp
     * @param  {string} [params.krpt] subject SW ID
     * @param  {string} [params.deviceId] device SW ID
     * @param  {function} onSuccess success callback
     * @param  {function} onError error callback
     */
    syncUserVisitsAndReports (params = {}, onSuccess, onError) {
        const ajaxConfig = {
                type: 'POST',
                uri: this.buildUri('/v1/SWAPI/GetDataClin'),
                auth: params.auth || null
            },
            krpt =  params.krpt || null,
            lastUpdated = params.lastUpdated || null,
            deviceId = params.deviceId || lStorage.getItem('deviceId');

        const handleResult = (res) => {
            let result;

            try {
                result = !res ? [] : convertXmlToJson($($.parseXML(res)).children()[0]);
            } catch (err) {
                logger.error('Error parsing Form result.', err);
            }

            onSuccess({ subjects: result.subject || [], lastRefreshTime: result.lastUpdated });
        };

        Sites.fetchFirstEntry()
        .then(site => {
            const payload = {
                StoredProc: 'synd_SyncVisitsAndForms',
                Params: [params.krDom || site.get('site_id'), deviceId, krpt, lastUpdated]
            };
            return this.transmit(ajaxConfig, payload);
        })
        .then(handleResult)
        .catch(onError)
        // return and remove .done if historicalDataSync is ever promisified.
        .done();
    }

    /**
     * Get the time in utc from the server.
     * @param  {object} params parameters
     * @param  {string} [params.auth] auth token
     * @return {Q.Promise<Object>} Q Promise
     */
    getServerTime (params = {}) {
        const ajaxConfig = {
            type: 'GET',
            uri: this.buildUri('/v1/timezones/utc'),
            auth: params.auth || null
        };

        return this.transmit(ajaxConfig, params, $.noop, $.noop, 'getServerTime');
    }
}

COOL.add('WebService', WebService);
