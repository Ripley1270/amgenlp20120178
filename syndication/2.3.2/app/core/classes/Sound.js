
export default class Sound {
    constructor () {
        this.mute();
    }

    /**
     * Plays a given sound.
     * @param {String} sound The id of a audio file.
     * @example
     * var sound = new LF.Class.Sound;
     * sound.play('click-audio');
     */
    play (sound) {
        if (localStorage.getItem('PHT_Sound') !== 'true' && document.getElementById(sound)) {
            document.getElementById(sound).play();
        }
    }

    /**
     * Mutes the application.
     * @return {Boolean} returns true on mute.
     * @example
     * var sound = new LF.Class.Sound;
     * sound.mute();
     */
    mute () {
        localStorage.setItem('PHT_Sound', true);
        return true;
    }

    /**
     * Un-mutes the application.
     * @return {Boolean} returns false on unmute.
     * @example
     * var sound = new LF.Class.Sound;
     * sound.unmute();
     */
    unmute () {
        localStorage.setItem('PHT_Sound', false);
        return false;
    }
}

window.LF.Class.Sound = Sound;
