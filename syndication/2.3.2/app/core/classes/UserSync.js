import Sites from 'core/collections/Sites';
import * as lStorage from 'core/lStorage';
import Logger from 'core/Logger';
import { isSitePad } from 'core/utilities';
import CurrentSubject from 'core/classes/CurrentSubject';

const logger = new Logger('UserSync');

/**
 * SyncValue computed by the corresponding SyncLevel's function. False to stop synchronization with the server
 * @typedef {string|boolean} SyncValue
 */

/**
 * All sync functions provided by core.
 * @type {Object}
 */
const coreSyncFunctions = {
    /**
     * Get Value for "subject" synchronization level.
     * @return {Q.Promise<SyncValue>} A promise, resolved with subject's krpt to be used for the user's synchronization level.
     */
    subject () {
        // TODO: Getting the current subject will work for logpad only
        // This has to be changed when implementing subject level user sync for sitepad
        return CurrentSubject.getSubject().then(subject => {
            if (subject) {
                return subject.get('krpt');
            } else {
                logger.error('Subject not found, skipping user sync');

                return false;
            }
        });
    },

    /**
     * Get Value for "site" synchronization level.
     * @return {Q.Promise<SyncValue>} A promise, resolved with site's krdom to be used for the user's synchronization level.
     */
    site () {
        // TODO: Core code should not be aware of which product is being run.
        if (isSitePad()) {
            return Sites.fetchFirstEntry()
            .then(site => site && site.get('backupSiteId'));
        } else {
            const storedSite = lStorage.getItem('site');
            const siteData = JSON.parse(storedSite.toString());

            return CurrentSubject.getSubject()
            .then(subject => {
                // If site_codes don't match,  something went wrong.
                if (subject && siteData && subject.get('site_code') === siteData.sitecode) {
                    return siteData.krdom;
                } else {
                    logger.error(`Subject's site code (${subject.get('site_code')}) and stored site code (${siteData.sitecode}) do not match`);
                    return false;
                }
            });
        }
    },

    /**
     * Get Value for "study" synchronization level.
     * @return {Q.Promise<SyncValue>} A promise, resolved with empty string for the user's synchronization level.
     *      Empty string is passed because databases are per-study.
     */
    study () {
        return Q('');
    }
};

/**
 * Class that holds and modifies the available functions to get sync value.
 * @class
 */
class UserSync {
    constructor (syncFunctions = {}) {
        this.syncFunctions = _.extend({}, syncFunctions);
    }

    /**
     * Run the function to retrieve the value for the given Sync Level.
     * @param  {string} syncLevel Key of sync function to run.
     * @return {Q.Promise<SyncValue>} Promise that will resolve with the syncValue.
     */
    getValue (syncLevel) {
        if (syncLevel === 'subject' && isSitePad()) {
            // TODO: To support sitepad we need to pass the current subject to this function
            // and use that subject to get the syncValue
            logger.error('Subject level user sync is not currentl;y supported for sitepad');
            throw new Error('Subject level user sync is not currentl;y supported for sitepad');
        }

        let syncFunc = this.syncFunctions[syncLevel];

        return Q().then(() => {
            if (syncFunc) {
                return syncFunc.call(this)
                    .catch(err => {
                        err && logger.error(`Error calling sync Function ${syncLevel}`, err);
                        // eslint-disable-next-line no-throw-literal
                        throw '';
                    });
            } else {
                logger.error('Sync Level has no sync value function');
                // eslint-disable-next-line no-throw-literal
                throw '';
            }
        });
    }

    /**
     * Add a single function to the available sync functions.
     * @param {string} syncLevel - The sync level of the user.
     * @param {function} newSyncFunction function that is run by core to find the syncValue for the given syncLevel.
     * @example UserSync.addSyncFunction('protocol', () => {... deferred.resolve(protocolValue); });
     */
    addSyncFunction (syncLevel, newSyncFunction) {
        this.syncFunctions = _.defaults({}, this.syncFunctions, {
            [syncLevel]: newSyncFunction
        });
    }

    /**
     * Add multiple sync functions using key-value pairs
     * @param {Object} newSyncFunctions Object containing key value pairs of { syncLevel: syncFunction}
     * @example UserSync.addSyncFunctions({ prototcol : getPrototcol, newSyncLevel () => {... deferred.resolve(value);} });
     */
    addSyncFunctions (newSyncFunctions = {}) {
        this.syncFunctions = _.defaults({}, this.syncFunctions, newSyncFunctions);
    }
}

/** singleton instance of UserSync with all current sync functions. LF.GetUserSyncValue is for backwards compatability in PDE code. */
export default new UserSync(_.defaults({}, coreSyncFunctions, LF.GetUserSyncValue));
