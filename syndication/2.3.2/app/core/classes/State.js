import * as lStorage from 'core/lStorage';

export default class State {
    constructor () {
        let localState = lStorage.getItem('state');

        this.states = [
            // The device
            'NEW',

            // A site must be selected.
            'SITE_SELECTION',

            // The device has been registered.
            'LOCKED',

            //Time Zone needs to be selected.
            'SET_TIME_ZONE',

            // The preferred site language needs to be selected.
            'LANGUAGE_SELECTION',

            // The inital super user must be created.
            'SETUP_USER',

            // The device is fully registered/activated and ready for use.
            'ACTIVATED'
        ];

        // If there is no state, that means this is a first time use...
        if (localState == null) {
            this.set('NEW');
        }
    }

    get () {
        let index = lStorage.getItem('state');

        return this.states[parseInt(index, 10)];
    }

    set (state) {
        let index = this.states.indexOf(state);

        if (index !== -1) {
            lStorage.setItem('state', index);
        } else {
            throw new Error('Error: State not found.');
        }
    }

    is (state) {
        let storedIndex = lStorage.getItem('state'),
        givenIndex = this.states.indexOf(state);

        return parseInt(storedIndex, 10) === givenIndex;
    }
}
