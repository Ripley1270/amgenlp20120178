import * as lStorage from 'core/lStorage';
import Users from 'core/collections/Users';
import Logger from 'core/Logger';
import { Banner } from 'core/Notify';
import CurrentContext from 'core/CurrentContext';
import ELF from 'core/ELF';
import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';
import State from 'core/classes/State';
import { isSitePad } from 'core/utilities';

const logger = new Logger('Session');

export default class Session {
    constructor () {
        // Used to check for session timeout.
        this.interval = 5000;
    }

    // Set an item in localStorage.
    set (name, value) {
        lStorage.setItem(name, value);
    }

    // Get an item from localStorage.
    get (name) {
        return lStorage.getItem(name);
    }

    // Remove an item from localStorage.
    remove (name) {
        return lStorage.removeItem(name);
    }

    // Listen for user activity and then update the last active timestamp.
    listenForActivity () {
        // If off() is not called, the listeners will be added multiple times.
        $('body')
            .off('mouseup keydown')
            .on('mouseup keydown', () => this.setLastActive());
    }

    // Sets a localStorage['PHT_Last_Active'] to the current time in milliseconds.
    setLastActive () {
        let now = (new Date()).getTime();

        this.set('Last_Active', now);

        return now;
    }

    // Return the time the user was last active.
    getLastActive () {
        let lastActive = this.get('Last_Active');

        if (lastActive) {
            return new Date().setTime(lastActive);
        } else {
            return false;
        }
    }

    // Sets a localStorage['PHT_Timeout_LastCheck'] to the current time in milliseconds.
    setTimeoutLastCheck () {
        let now = (new Date()).getTime();

        this.set('Timeout_LastCheck', now);

        return now;
    }

    /**
     * Return the time that the timeout was last checked
     * @return {Object|Boolean}
     */
    getTimeoutLastCheck () {
        let lastCheck = this.get('Timeout_LastCheck');

        return lastCheck ? (new Date()).setTime(lastCheck) : false;
    }

    /* Start the Timer for activation and application session timeout. */
    startSessionTimeOut () {
        if (_(LF.StudyDesign.sessionTimeout).isUndefined() || _(LF.StudyDesign.sessionTimeout).isNull()) {
            // TODO: Move this default value to app/core/coreSettings.js
            // Default to 30 minutes
            LF.StudyDesign.sessionTimeout = 30;
        }

        this.sessionTimeout = (LF.StudyDesign.sessionTimeout * 60) * 1000;
        this.listenForActivity();
        this.timerOn = true;
        clearTimeout(this.timer);

        if (!this.get('Last_Active')) {
            this.setLastActive();
        }

        if (!this.get('Timeout_LastCheck')) {
            this.setTimeoutLastCheck();
        }

        this.checkTimeOut();
    }

    /**
     * Checks for timeout every 5 seconds and logs out if there is a timeout.
     * @example this.checkTimeOut();
     */
    checkTimeOut () {
        let now = new Date(),
            lastActive = new Date(),
            lastCheck = new Date(),

            // State is only used in SitePad
            state = new State(),
            stateName = state.get(),

            // SM: Generally unit tests don't set LF.router.view,
            //     then, on timeout, an error occurs here.
            //     Karma exits if error is thrown during timeout (why is a mystery);
            //     consequently, test runs terminate randomly based on timing.
            //     Hence all these seemingly paranoid checks:
            currentView = LF.router && LF.router.view ? LF.router.view() : undefined,
            lastCheckElapsedTime,
            lastActiveElapsedTime,
            defaultSessionTimeoutHandler;

        if (currentView) {
            lastActive.setTime(this.getLastActive());
            lastCheck.setTime(this.getTimeoutLastCheck());

            lastActiveElapsedTime = now.getTime() - lastActive.getTime();
            lastCheckElapsedTime = now.getTime() - lastCheck.getTime();

            if (this.timerOn && (lastActiveElapsedTime > this.sessionTimeout || lastCheckElapsedTime > this.sessionTimeout)) {
                this.setLastActive();

                $('.modal').modal('hide');

                let $dateBox = $('input.date-input');
                $dateBox.datebox('instance') && $dateBox.datebox('close');

                let $timeBox = $('input.time-input');
                $timeBox.datebox('instance') && $timeBox.datebox('close');

                let $select2 = $('.select2-hidden-accessible');
                $select2.length && $select2.select2('close');

                $dateBox = null;
                $timeBox = null;
                $select2 = null;

                let removeModeSelectionLStorageValues = () => {
                    this.remove('environment');
                    this.remove('mode');
                    this.remove('studyDbName');
                    this.remove('serviceBase');
                };

                defaultSessionTimeoutHandler = () => {
                    if (currentView instanceof BaseQuestionnaireView) {
                        // if session timeout occurs before questionnaire timeout
                        if (this.questionnaireTimerOn) {
                            this.stopQuestionnaireTimeOut();
                        }

                        //Saves the report if Affidavit is checked.
                        ELF.trigger(`QUESTIONNAIRE:SessionTimeout/${currentView.id}`, {
                            questionnaire: currentView.id
                        }, currentView);
                    } else {
                        this.logout(true);
                    }
                };

                if (currentView.id === 'login-view') {
                    localStorage.removeItem('PHT_isAuthorized');

                    LF.router.controller.login();
                } else if (isSitePad()) {
                    switch (stateName) {
                        case 'NEW':
                            removeModeSelectionLStorageValues();
                            Backbone.history.loadUrl(undefined);
                            break;
                        case 'SITE_SELECTION':
                            Banner.closeAll();

                            removeModeSelectionLStorageValues();

                            state.set('NEW');
                            LF.router.navigate('modeSelection', true);

                            ELF.trigger('SITESELECTION:Back', { product: 'sitepad' }, this)
                            .done();
                            break;
                        case 'LOCKED':
                        case 'SETUP_USER':
                        case 'LANGUAGE_SELECTION':
                        case 'SET_TIME_ZONE':
                            state.set('LANGUAGE_SELECTION');
                            LF.router.navigate('languageSelection', true);
                            break;
                        case 'ACTIVATED':
                            // These initializations are done to disable the date/time picker
                            // when session timeout occurs, see DE19403
                            let timeTravelDateBox = $('#timeTravelDateBox');
                            let timeTravelTimeBox = $('#timeTravelTimeBox');
                            timeTravelDateBox.length && timeTravelDateBox.datebox().datebox('close');
                            timeTravelTimeBox.length && timeTravelTimeBox.datebox().datebox('close');
                            $('.modal-backdrop').remove();
                            timeTravelDateBox = null;
                            timeTravelTimeBox = null;
                            defaultSessionTimeoutHandler();
                            break;
                        default:
                            defaultSessionTimeoutHandler();
                    }
                } else {
                    defaultSessionTimeoutHandler();
                }

                logger.operational('Session has timed out.');
            } else if (this.questionnaireTimerOn && (now.getTime() - this.questionnaireStartTime) > this.questionnaireTimeout) {
                this.stopQuestionnaireTimeOut();

                if (currentView instanceof BaseQuestionnaireView) {
                    // Close all modal backgrounds
                    $('.modal-backdrop').remove();
                    $('.mbsc-jqm').remove();

                    // Close modal.
                    $('.modal').modal('hide');

                    logger.operational(`Questionnaire ${currentView.id} has timed out.`);

                    //Saves the report if Affidavit is checked.
                    ELF.trigger(`QUESTIONNAIRE:QuestionnaireTimeout/${currentView.id}`, {
                        questionnaire: currentView.id
                    }, currentView);
                }
            }
        }

        clearTimeout(this.timer);

        if (this.timerOn) {
            this.timer = setTimeout(_.bind(this.checkTimeOut, this), this.interval /* this.frequency */);
            this.setTimeoutLastCheck();
        }
    }

    /**
     * Pause the Timer for application session timeout.
     * @example LF.security.pauseSessionTimeOut();
     */
    pauseSessionTimeOut () {
        logger.trace('Session timeout has been paused.');

        this.timerOn = false;
    }

    /**
     * Restart the Timer for application session timeout.
     * @example LF.security.restartSessionTimeOut();
     */
    restartSessionTimeOut () {
        logger.trace('Session timeout has been restarted.');

        if (!this.timerOn) {
            this.timerOn = true;
            this.checkTimeOut();
        }
    }

    /**
     * Start the Timer for questionnaire timeout.
     * @example LF.security.startQuestionnaireTimeOut();
     */
    startQuestionnaireTimeOut () {
        if (LF.StudyDesign.questionnaireTimeout && LF.StudyDesign.questionnaireTimeout !== 0) {
            this.questionnaireTimerOn = true;
            this.questionnaireStartTime = new Date().getTime();
            this.questionnaireTimeout = (LF.StudyDesign.questionnaireTimeout * 60) * 1000;

            logger.trace('Questionnaire timeout has been started.');
        }
    }

    /**
     * Stops the Timer for questionnaire timeout.
     * @example LF.security.stopQuestionnaireTimeOut();
     */
    stopQuestionnaireTimeOut () {
        if (this.questionnaireTimerOn) {
            this.questionnaireTimerOn = false;
            this.questionnaireStartTime = undefined;

            logger.trace('Questionnaire timeout has been stopped.');
        }
    }

    /**
     * Pause the questionnaire timeout.
     */
    pauseQuestionnaireTimeout () {
        this.questionnaireTimerOn = false;

        logger.trace('Questionnaire timeout has been paused.');
    }

    /**
     * Restart the questionaire timeout.
     */
    restartQuestionnaireTimeout () {
        if (!this.questionnaireTimerOn) {
            this.questionnaireTimerOn = true;

            logger.trace('Questionnaire timeout has been restarted.');
        }
    }

    /*
     * Checks if max login attempts is exceeded
     * @param {Object} user The current user
     * @return (Boolean) true - max login attempts is exceeded, false - max login attempts is not exceeded
     */
    checkLock (user) {
        let maxAttemptsExceeded = LF.StudyDesign.maxLoginAttempts && (LF.security.getLoginFailure(user) >= LF.StudyDesign.maxLoginAttempts),
            nowUtcTimestamp = new Date().getTime(),
            userEnableTimestamp = user.get('enableDeviceUtc'),
            lockoutExpired = userEnableTimestamp && (nowUtcTimestamp >= userEnableTimestamp),
            userLocked = maxAttemptsExceeded ? !lockoutExpired : false;

        if (lockoutExpired) {
            logger.operational(`User lockout expired for user ${user.get('username')}`);

            this.resetFailureCount(user);
        }

        return userLocked;
    }

    /*
     * Sets a localStorage['PHT_Login_Failure'] to the current number of consecutive unsuccessful logon attempts.
     * @param {Number} number current number of consecutive unsuccessful logon attempts to be saved
     * @param {Object} user The current user
     */
    setLoginFailure (number, user) {
        user.set('failedLoginAttempts', number).save();
    }

    /**
     * Return the current number of consecutive unsuccessful logon attempts stored in localStorage, if not return 0
     * @param {Object} user The current user
     * @return {number}
     */
    getLoginFailure (user) {
        let attempts = user.get('failedLoginAttempts');

        return attempts != null ? parseInt(attempts, 10) : 0;
    }

    /*
     * Resets a localStorage['PHT_Login_Failure'], localStorage['PHT_Security_Question_Failure'] and ['PHT_Enable_Device_Utc'] to 0
     * @param {Object} user The current user
     */
    resetFailureCount (user) {
        user.set('failedLoginAttempts', 0);
        user.set('failedQuestionAttempts', 0);
        user.set('enableDeviceUtc', 0);
        user.save();
    }

    /**
     * Sets a localStorage['PHT_Security_Question_Failure'] to the current number of consecutive unsuccessful secret question attempts.
     * @param {Number} number current number of consecutive unsuccessful logon attempts to be saved
     * @param {Object} user The current user d
     */
    setSecurityQuestionFailure (number, user) {
        user.set('failedQuestionAttempts', number).save();
    }

    logout (navigate) {
        delete this.activeUser;

        if (navigate) {
            LF.router.navigate('login', true);
        }
    }

    /**
     * Checks if a user is logged in.
     * @returns {Q.Promise<Boolean>} True if logged in, false if not.
    */
    checkLogin () {
        return Q.Promise((resolve, reject) => {
            if (lStorage.getItem('isActivation')) {
                resolve(true);
            } else if (lStorage.getItem('isAuthorized') && lStorage.getItem('User_Login')) {
                // DE15020 - After a refresh, the activeUser property is undefined.
                if (this.activeLogin != null) {
                    resolve(true);
                } else if (!this.activeUser) {
                    let userId = parseInt(lStorage.getItem('User_Login'), 10);

                    this.getUser(userId).then((user) => {
                        this.activeUser = user;
                    })
                    .then(() => resolve(true))
                    .catch(reject)
                    .done();
                } else {
                    resolve(true);
                }
            } else {
                resolve(false);
            }
        });
    }

    /**
     * Return the current number of consecutive unsuccessful security question attempts stored in localStorage, if not return 0
     * @param {Object} user The current user
     * @return {Number} number of security question failures
     */
    getSecurityQuestionFailure (user) {
        let questionFailure = user.get('failedQuestionAttempts');

        return questionFailure != null ? parseInt(questionFailure, 10) : 0;
    }

    /*
     * Redirects the user to Security Question Screen
     * Starts the checkLockTimer function
     * @example this.accountDisable();
     * @param {Object} user The current user
     * @return {Boolean} Returns true
     */
    accountDisable (user) {
        let nowUtcTimestamp,
            enableDeviceUtcTimeStamp;

        if (_(LF.StudyDesign.lockoutTime).isUndefined() || _(LF.StudyDesign.lockoutTime).isNull()) {
            //Default to 5 minutes
            LF.StudyDesign.lockoutTime = 5;
        }

        if (LF.StudyDesign.lockoutTime === 0) {
            //User will be able to re-enable disabled account with "Forgot Password" process described in US1610
            return true;
        } else {
            nowUtcTimestamp = new Date().getTime();
            enableDeviceUtcTimeStamp = nowUtcTimestamp + LF.StudyDesign.lockoutTime * 60000;
            user.set('enableDeviceUtc', enableDeviceUtcTimeStamp).save();

            return true;
        }
    }

    // Fetch all users from the database.
    fetchUsers () {
        let collection = new Users();

        return collection.fetch().then(() => {
            this.users = collection;

            return collection;
        });
    }

    getUser (id, fetchCollection = false) {
        if (fetchCollection) {
            return this.fetchUsers()
            .then((users) => {
                return users.findWhere({
                    id: id
                });
            });
        } else {
            return Q(CurrentContext().get('users').findWhere({
                id: id
            }));
        }
    }

    /*
     * @method validate
     * Validate a user login.
     */
    login (id, password) {
        return this.getUser(id, true)
        // eslint-disable-next-line consistent-return
        .then((user) => {
            if (user) {
                let now = new Date(),
                    userPassword = user.get('password'),
                    isAdminUser = user.get('userType') === 'Admin',
                    hash,
                    changePassword;

                if (isAdminUser) {
                    hash = (userPassword.length > 32) ? hex_sha512(user.get('salt') + password) : hex_md5(user.get('salt') + password);
                } else {
                    hash = (userPassword.length > 32) ? hex_sha512(password + user.get('salt')) : hex_md5(password + user.get('salt'));
                }

                if (hash === userPassword) {
                    logger.operational('User {{ username }} with role {{ role }} successfully logged in.', {username: user.get('username'), role: user.get('role')});

                    (user.get('salt') === 'temp') ? lStorage.setItem('changePassword', true) : lStorage.removeItem('changePassword');

                    this.activeUser = user;

                    lStorage.setItem('isAuthorized', true);

                    if (this.timerOn) {
                        this.setLastActive();
                        this.setTimeoutLastCheck();
                    }

                    // Record the time the user logged in
                    if (userPassword.length <= 32 && !isAdminUser) {
                        changePassword = hex_sha512(password + user.get('salt'));

                        user.changeCredentials({ password: changePassword, salt: user.get('salt') });
                    }
                    user.set('lastLogin', now.toString()).save();

                    this.resetFailureCount(user);

                    // Resolve the promise, passing in the current user.
                    return user;
                }
            }
        });
    }

    getUserRole () {
        if (this.activeUser == null) {
            return false;
        }

        return LF.StudyDesign.roles.findWhere({ id: this.activeUser.get('role') });
    }
}
