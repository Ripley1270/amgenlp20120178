/**
 * An simple extension of a JS Object.  This object is used, specifically for functions like mergeObjects
 * to indicate that this object is meant to be a final reference, not to be appended or mutated.
 */
export default class ObjectRef extends Object {
    constructor (fromObject) {
        super();
        _.each(_.keys(fromObject), (key) => {
            this[key] = fromObject[key];
        });
    }
}