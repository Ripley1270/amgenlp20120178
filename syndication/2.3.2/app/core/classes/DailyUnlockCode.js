import CurrentContext from 'core/CurrentContext';

/**
 * Defines the class for daily unlock code.
 * @class DailyUnlockCode
 * @example let dailyUnlockCode = new DailyUnlockCode();
 */
export default class DailyUnlockCode {

    /**
     * Generates DailyUnlockCode
     * @param {Date} date - The current date.
     * @return {number} Returns calculated Daily Unlock Code
     * @example 
     * let unlockCode = new DailyUnlockCode()
     * unlockCode.getCode(new Date());
     */
    // Code based on LogPad 5.x Unlock Code Generation.
    getCode (date) {
        let seed, magnitude, code,
            config = CurrentContext().get('role').get('unlockCodeConfig') || LF.StudyDesign.unlockCodeConfig;

        seed = config.seedCode + date.getMonth() + 1 + date.getDate();
        seed = Math.pow(seed, 2);

        magnitude = seed.toString().length;

        if (magnitude > config.codeLength + 1) {
            // Trim the code to the required significant digits (codeLength + 1)
            seed = seed / Math.pow(10, magnitude - (config.codeLength + 1));

            // Retrieve the base-10 integer value of the result, dropping decimals.
            seed = parseInt(seed, 10);
        }

        code = Math.pow(date.getMonth() + 1, 2);
        code += Math.pow(date.getDate(), 3);
        code += Math.pow(date.getMonth() + 1 + date.getDate(), 4);

        // Limit the number of digits in sum of the seed and calculated code to that
        // specified in codeLength by trimming the excess digits from the left of the code.
        code = (seed + code) % Math.pow(10, config.codeLength);

        // Get the string value of the sum.
        code = code.toString();

        // Pad code with leading 0's to meet specified length.
        while (code.length < config.codeLength) {
            code = `0${code}`;
        }

        return code;
    }
}

window.LF.Class.DailyUnlockCode = DailyUnlockCode;
