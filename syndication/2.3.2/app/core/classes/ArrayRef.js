/**
 * An extension of an Array object.  This object is used, specifically for functions like mergeObjects
 * to indicate that this array is meant to be a final reference, not to be appended or mutated.
 */
export default class ArrayRef extends Array {
    constructor (fromArray) {
        super();
        for (let i = 0; i < fromArray.length; ++i) {
            this.push(fromArray[i]);
        }
    }
}

// Redefine _.isArray to pass when the object is an instance of an ArrayRef (which should also be considered an array)
let oldIsArray = _.isArray;
_.isArray = (test) => {
    return oldIsArray(test) || (test instanceof ArrayRef);
};