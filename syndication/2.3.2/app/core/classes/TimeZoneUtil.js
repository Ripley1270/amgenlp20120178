import Logger from 'core/Logger';

/**
 * An object literal representation of a time zone option.
 * @typedef {Object} TimeZoneOption
 * @property {string} id - The ID of the time zone. e.g. 'America/New_York'
 * @property {string} displayName - The display name of the time zone. e.g. '(GMT-05:00) America/New_York'
 */

let logger = new Logger('TimeZoneUtil');

export default class TimeZoneUtil {

    /**
     * Gets all available timezones from the device.
     * @returns {Q.Promise<TimeZoneOption[]>}
     * @example
     * TimeZoneUtil.getAllTimeZones().then(res => {
     *     // res === [{id: 'America/New_York', displayName: '(GMT-05:00) America/New_York'}]
     * });
     */
    static getAllTimeZones () {
        return Q.Promise((resolve, reject) => {
            // If the TimeZoneUtil plugin exists, get all available time zones.
            if (window.plugin && window.plugin.TimeZoneUtil) {
                window.plugin.TimeZoneUtil.getAvailableTimeZones(resolve, (err) => {
                    logger.error('Failed to fetch time zones.', err);

                    reject(err);
                });
            } else {
                let err = new Error('TimeZoneUtil plugin is not available.');

                reject(err);
            }
        });
    }

    /**
     * Get the current timezone.
     * @returns {Q.Promise<TimeZoneOption>}
     * @example
     * TimeZoneUtil.getCurrentTimeZone().then(res => {
     *    res === { id: 'America/New_York', displayName: '(GMT-05:00) America/New_York' }
     * });
     */
    static getCurrentTimeZone () {
        return Q.Promise((resolve, reject) => {
            if (window.plugin && window.plugin.TimeZoneUtil) {
                window.plugin.TimeZoneUtil.getCurrentTimeZone((tz) => {
                    logger.operational(`Current Time Zone is: ${tz.displayName}`);

                    resolve(tz);
                }, (err) => {
                    logger.error('Failed to fetch current time zone.', err);

                    reject(err);
                });
            } else {
                let err = new Error('TimeZoneUtil plugin is not available.');

                reject(err);
            }
        });
    }

    /**
     * Filters out the timezones from the StudyDesign, matching those of the device.
     * @param {TimeZoneOption[]} allTimeZones - The result of TimeZone.getAllTimeZones().
     * @returns {TimeZoneOption[]}
     * @example
     * let filtered = TimeZone.filterTimeZones(allTimeZones);
     */
    static filterTimeZones (allTimeZones) {
        return _.filter(allTimeZones, (tz) => {
            return _.findWhere(LF.StudyDesign[LF.Wrapper.platform].timeZoneOptions, { tzId: tz.id });
        });
    }

    /**
     * Set the device's time zone.
     * @param {string} timeZone - The time zone to set.
     * @returns {Q.Promise<void>}
     */
    static setTimeZone (timeZone) {
        return Q.Promise((resolve, reject) => {
            if (window.plugin && window.plugin.TimeZoneUtil) {
                window.plugin.TimeZoneUtil.setTimeZone(() => {
                    logger.operational(`User changed Time zone to: ${timeZone.displayName}`)
                    .finally(resolve);
                }, (err) => {
                    logger.error('Failed to set the Time zone', err);

                    reject(err);
                }, timeZone.id);
            } else {
                let err = new Error('TimeZoneUtil plugin is not available.');

                reject(err);
            }
        });
    }

    /**
     * Restarts the application on Android < 5.
     * @returns {Q.Promise<void>}
     * @example
     * TimeZoneUtil.restartApplication().then(() => ...);
     */
    static restartApplication () {
        return Q.Promise((resolve, reject) => {
            if (window.plugin && window.plugin.TimeZoneUtil) {
                window.plugin.TimeZoneUtil.restartApplication(resolve, (err) => {
                    // It seems like even if the restart fails, we should still move forward.
                    // So, we log an error and resolve.
                    logger.error('Failed to restart application on Time zone change.', err)
                    // In the case of a reload, we have to wait for the error log to be saved before resolving.
                    .finally(resolve);
                });
            } else {
                let err = new Error('TimeZoneUtil plugin is not available.');

                reject(err);
            }
        });
    }
}
