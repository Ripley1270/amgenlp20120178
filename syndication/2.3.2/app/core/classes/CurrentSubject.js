import Subjects from 'core/collections/Subjects';
import Logger from 'core/Logger';

let cachedSubject;
let logger = new Logger('CurrentSubject');

export default class CurrentSubject {
    /**
     * Static function that fetches then resolves with subject user
     * In order to support .listenTo(model, 'change') monitoring, we always
     * return the same instance.
     * @return {Q.Promise} Promise that resolves with the subject.
     */
    static getSubject () {
        let krpt = localStorage.getItem('krpt'),
            activeUser = LF.security.activeUser,
            cache = (subjects) => {
                let criteria = krpt ? { krpt } : { user: activeUser && activeUser.get('id') },
                    subject = subjects.where(criteria)[0];

                if (!!subject) {
                    cachedSubject = subject;
                    logger.trace('Current subject is now:', cachedSubject.toJSON());

                    // Note! Logger static, not logger instance method
                    Logger.setSubjectLoggingLevel(subject.get('log_level'));

                    // Here we set up a watch on subject.log_level, and inform
                    // Logger when this is changed (Logger global setting)
                    subject.on('change:log_level', (subject, level) => {
                        logger.info(`Changing subject logging level to ${level}`);
                        Logger.setSubjectLoggingLevel(level);
                    });
                } else {
                    logger.trace('No subject found');
                }
            };

        // Clear the cache if it has expired
        if (cachedSubject && !krpt && activeUser && cachedSubject.get('user') !== activeUser.get('id')) {
            CurrentSubject.clearSubject();
        }

        if (!!cachedSubject) {
            return Q(cachedSubject);
        } else {
            return Subjects.fetchCollection()
            .then(cache)
            .then(() => cachedSubject)
            .catch(err => {
                logger.error('getSubject exception', err);
            });
        }
    }

    /**
     * Clear the cached subject.
     */
    static clearSubject () {
        cachedSubject = null;
    }
}
