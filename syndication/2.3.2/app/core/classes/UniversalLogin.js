/* eslint-disable new-cap */
import Logger from 'core/Logger';
import LoginView from 'core/views/LoginView';
import ChangeTemporaryPasswordView from 'core/views/ChangeTemporaryPasswordView';
import ResetSecretQuestionView from 'core/views/ResetSecretQuestionView';
import ResetPasswordView from 'core/views/ResetPasswordView';
import ForgotPasswordView from 'core/views/ForgotPasswordView';
import UnlockCodeView from 'core/views/UnlockCodeView';
import AccountConfiguredView from 'sitepad/views/AccountConfiguredView';

let logger = new Logger('UniversalLogin');

export default class UniversalLogin {
    constructor (options = {}) {
        logger.traceEnter('constructor');

        // Allow for optional views to be used instead of the standard core views
        this.optLoginView = options.loginView;
        this.optChangeTempPasswordView = options.changeTempPasswordView;
        this.optResetSecretQuestionView = options.resetSecretQuestionView;
        this.optResetPasswordView = options.resetPasswordView;
        this.optForgotPasswordView = options.forgotPasswordView;
        this.optUnlockCodeView = options.unlockCodeView;
        this.noRoleFound = options.noRoleFound;
        this.optAcctConfiguredView = options.acctConfiguredView;

        // This is to fix a bad hard dependency in scheduling
        this.optAppController = options.appController;

        // Don't loose track of the subject and visit
        this.subject = options.subject;
        this.visit = options.visit;

        logger.traceExit('constructor');
    }

    newUserLogin (successfulLogin, filterUsers, sortUsers, backArrow, forwardArrow) {
        logger.traceEnter('newUserLogin');

        if (!successfulLogin) {
            // throw error we need to have this function at a minimum
        }

        this.successfulLogin = successfulLogin;
        this.backArrow = backArrow;
        this.forwardArrow = forwardArrow;
        this.filterUsers = filterUsers;
        this.sortUsers = sortUsers;

        this.universalLoginNavigator = (route) => {
            switch (route) {
                case 'dashboard': {
                    // fix for DE16593 - removing this flag after user has reset their SQ and A
                    // rather than doing it on core/ResetSecretQuestionView which would cause SitePad issues
                    localStorage.removeItem('Reset_Password_Secret_Question');

                    this.successfulLogin();
                    this.clear();
                    break;
                }
                case 'forgot-password': {
                    this.goToForgotPassword();
                    break;
                }
                case 'change_temporary_password': {
                    this.goToChangeTempPassword();
                    break;
                }
                case 'reset_secret_question': {
                    this.goToResetSecretQuestion();
                    break;
                }
                case 'reset_password': {
                    this.goToResetPassword();
                    break;
                }
                case 'login': {
                    this.goToLogin();
                    break;
                }
                case 'unlock_code': {
                    this.goToUnlockCode();
                    break;
                }
                case 'account_configured': {
                    this.goToAccountConfigured();
                    break;
                }
            }
        };

        this.goToLogin();

        logger.traceExit('newUserLogin');
    }

    renderView (newView) {
        // Clear the View
        this.clear();

        this.view = newView;

        return this.view.resolve()
            .then(() => this.view.render())
            .then(() => {
                LF.Notify.Banner.closeAll();
            })
            .catch((err) => {
                err && logger.error(err);
            });
    }

    clear () {
        if (this.view) {
            this.view.remove();
        }
    }

    goTo (view) {
        view.navigate = this.universalLoginNavigator;
        this.renderView(view);
    }

    goToLogin () {
        let viewOptions = {
            backArrow: this.backArrow,
            forwardArrow: this.forwardArrow,
            filterUsers: this.filterUsers,
            sortUsers: this.sortUsers,
            noRoleFound: this.noRoleFound
        };

        let view = this.optLoginView ? new this.optLoginView(viewOptions) : new LoginView(viewOptions);
        view.subject = this.subject;
        view.visit = this.visit;

        // To fix a bad hard dependency in scheduling
        if (this.optAppController) {
            this.optAppController.view = view;
        }

        this.goTo(view);
    }

    goToForgotPassword () {
        let view = this.optForgotPasswordView ? new this.optForgotPasswordView() : new ForgotPasswordView();
        this.goTo(view);
    }

    goToChangeTempPassword () {
        let view = this.optChangeTempPasswordView ? new this.optChangeTempPasswordView() : new ChangeTemporaryPasswordView();
        this.goTo(view);
    }

    goToResetSecretQuestion () {
        let view = this.optResetSecretQuestionView ? new this.optResetSecretQuestionView() : new ResetSecretQuestionView();
        view.subject = this.subject;
        this.goTo(view);
    }

    goToResetPassword () {
        let view = this.optResetPasswordView ? new this.optResetPasswordView() : new ResetPasswordView();
        this.goTo(view);
    }

    goToUnlockCode () {
        let view = this.optUnlockCodeView ? new this.optUnlockCodeView() : new UnlockCodeView();
        this.goTo(view);
    }

    goToAccountConfigured () {
        let view = this.optAcctConfiguredView ? new this.optAcctConfiguredView() : new AccountConfiguredView();
        this.goTo(view);
    }
}
