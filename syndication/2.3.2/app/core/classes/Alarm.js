import Logger from '../Logger';
import ActiveAlarms from 'core/collections/ActiveAlarms';
import Users from 'core/collections/Users';

import AlarmManager from 'core/AlarmManager';

let logger = new Logger('Alarm');

export default class Alarm {

    constructor (schedule) {
        this.alarmParams = _.clone(schedule.get('alarmParams'));

        /**
         * The flag to be used to turn off the alarm
         * @type Boolean
         * @default false
         */
        this.turnOffAlarm = false;
    }

    /**
     * This method evaluates the Alarm if the application wrapped, and sets or cancels the alarm accordingly.
     * @param {LF.Model.Schedules} schedule The schedule object including schedule and alarm configurations
     * @param {LF.Collection.LastDiaries} completedQuestionnaires list of all completed diaries
     * @param {Object} parentView The view to append this scheduled item to.
     */
    evaluateAlarm (schedule, completedQuestionnaires, parentView) {
        LF.Wrapper.exec({
            execWhenWrapped: () => {
                let alarmFunction = LF.Schedule.alarmFunctions[schedule.get('alarmFunction')];

                if (alarmFunction && this.alarmParams) {
                    if (this.alarmParams.repeat === undefined) {
                        this.alarmParams.repeat = 'once';
                    }

                    if (this.alarmParams.time == null) {
                        throw new Error('Missing alarm time: alarm time is null or undefined.');

                    } else if (this.alarmParams.repeat && this.alarmParams.repeat !== 'daily' && this.alarmParams.repeat !== 'once') {
                        throw new Error(`Incorrect repeat type: repeat type "${this.alarmParams.repeat}" is not supported`);
                    }

                    alarmFunction(schedule, this.alarmParams, completedQuestionnaires, parentView, (setAlarm) => {
                        let alarms = new ActiveAlarms();

                        if (_.isNumber(setAlarm) || setAlarm) {
                            this.evaluateSubjectConfigAlarm(schedule.get('id'), () => {
                                const completedDiary = _.first(completedQuestionnaires.where({
                                    questionnaire_id: schedule.get('target').id
                                }));

                                this.setAlarmDate(schedule, completedDiary, setAlarm, alarms).then(() => {
                                    if (this.turnOffAlarm) {
                                        alarms.cancelAlarm(this.alarmParams);
                                    } else {
                                        alarms.addAlarm(this.alarmParams, schedule.get('id'));
                                    }
                                })
                                .catch(err => {
                                    logger.error('Error setting alarm', err);
                                })
                                .done();
                            });
                        } else {
                            alarms.cancelAlarm(this.alarmParams);
                        }

                    });
                }
            }
        });
    }

    /**
     * This method evaluates the subject selected alarm , and set the alarm time accordingly.
     * @param {String} scheduleId The unique schedule id that the alarm associated with.
     * @param {Function} callback The callback function to be invoked upon completion.
     */
    evaluateSubjectConfigAlarm (scheduleId, callback = $.noop) {
        let subjectAlarms = new LF.Collection.SubjectAlarms();

        if (this.alarmParams.subjectConfig) {
            subjectAlarms.fetch()
                .then(() => {
                    let subjectAlarm;

                    if (subjectAlarms.length !== 0) {
                        subjectAlarm = _.first(subjectAlarms.where({ id: this.alarmParams.id }));

                        if (subjectAlarm) {
                            this.alarmParams.time = subjectAlarm.get('time');
                            this.turnOffAlarm = this.alarmParams.time === '';
                        }
                    }

                    callback();
                })
                .catch((err) => {
                    logger.error('Failed evaluating subjectConfigAlarm', err);
                    callback();
                })
                .done();
        } else {
            subjectAlarms.removeSubjectAlarm(scheduleId, callback);
        }
    }

    /**
     * This method sets the date of the alarm.
     * @param {Schedule} schedule - Schedule configuration.
     * @param {LastDiary} completedDiary - The model of the completed diary of the alarm.
     * @param {boolean|Integer} dayOffset - The number of day from now to schedule the alarm or boolean value to set the alarm
     * @returns {Q.Promise<void>}
     */
    setAlarmDate (schedule, completedDiary, dayOffset) {

        return Q.all([Users.fetchCollection(), ActiveAlarms.fetchCollection()])
        .spread((users, alarms) => {
            let { time: alarmTime } = this.alarmParams,
                parseTime = time => new Date(LF.Utilities.parseTime(time, true)),
                //@todo: remove getFictionalUtc
                getFictionalUtc = date => LF.Utilities.convertToUtc(date ? new Date(date) : new Date()),
                nowFicUtcDate = getFictionalUtc(),
                nowUtcMillis = new Date().getTime(),
                target = schedule.get('target'),
                alarmDate = typeof alarmTime === 'string' ? parseTime(alarmTime) : alarmTime,
                // eslint-disable-next-line consistent-return
                accessRoles = (() => {
                    if (target.id && target.objectType === 'questionnaire') {
                        const targetQuestionnaire = LF.StudyDesign.questionnaires.findWhere({
                            id: target.id
                        });

                        return targetQuestionnaire && targetQuestionnaire.get('accessRoles');
                    }
                })(),
                scheduleOrAccessRoles = accessRoles || schedule.get('scheduleRoles') || ['subject'],
                userRoles = _.uniq(_.invoke(users.models, 'get', 'role')),
                isShared = !!schedule.get('sharedAcrossRoles'),
                isDiaryCompletedToday = false,
                isAlarmMissedToday,
                isRoleIntersectionEqual,
                isRoleInUse,
                alarmTimeFicUtcDate,
                alarmTimeUtcMillis,
                diaryRoles,
                startedFicUtcDate,
                isFirstOfRole,
                isNewRoleOnlyRole,
                isAlarmWithinMinute,
                isScheduleSameTimeAsAlarm;

            if (completedDiary) {
                const role = completedDiary.get('role');

                startedFicUtcDate = getFictionalUtc(completedDiary.get('lastStartedDate'));
                diaryRoles = (typeof role === 'string') ? JSON.parse(role) : role;
                isDiaryCompletedToday = nowFicUtcDate.toDateString() === startedFicUtcDate.toDateString();
            }

            if (!(alarmDate instanceof Date)) {
                let randomMilliseconds,
                    startUTCOffset,
                    endUTCOffset,
                    startTime = alarmDate.startTimeRange ? parseTime(alarmDate.startTimeRange) : null,
                    endTime = alarmDate.endTimeRange ? parseTime(alarmDate.endTimeRange) : null,
                    alarmSchedule = alarms.findWhere({ schedule_id: schedule.id }),
                    strAlarmDateWithNoTZ = alarmSchedule && alarmSchedule.get('date').substring(0, alarmSchedule.get('date').indexOf('GMT')).trim(),
                    hours = alarmSchedule && new Date(strAlarmDateWithNoTZ).getHours(),
                    minutes = alarmSchedule && new Date(strAlarmDateWithNoTZ).getMinutes();

                if (startTime && endTime) {
                    if (hours != null && minutes != null) {
                        alarmDate = new Date(startTime.getTime());
                        alarmDate.setHours(hours);
                        alarmDate.setMinutes(minutes);
                    } else {
                        randomMilliseconds = Math.random() * (endTime.getTime() - startTime.getTime());
                        alarmDate = new Date(startTime.getTime() + randomMilliseconds);

                        startUTCOffset = startTime.getTimezoneOffset();
                        endUTCOffset = alarmDate.getTimezoneOffset();
                        alarmDate = new Date(alarmDate.getTime() - ((startUTCOffset - endUTCOffset) * 60 * 1000));
                    }

                    alarmDate.setSeconds(0);
                } else {
                    logger.error('\'Time\' key for \'alarmParams\' is an Object, but is missing the \'startTimeRange\' or \'endTimeRange\' keys in the time object');
                }
            }

            alarmTimeFicUtcDate = LF.Utilities.convertToUtc(alarmDate);
            alarmTimeUtcMillis = alarmDate.getTime();
            isFirstOfRole = localStorage.getItem('isFirstOfRole');
            isAlarmMissedToday = nowUtcMillis > alarmTimeUtcMillis;

            if (schedule.get('scheduleParams')) {
                isAlarmWithinMinute = nowFicUtcDate.getHours() === alarmTimeFicUtcDate.getHours() && nowFicUtcDate.getMinutes() <= alarmTimeFicUtcDate.getMinutes() + 1;
                isScheduleSameTimeAsAlarm = schedule.get('scheduleParams').startAvailability === alarmTime;
            }

            if (isScheduleSameTimeAsAlarm && isAlarmWithinMinute) {
                isAlarmMissedToday = false;
            }

            if (isFirstOfRole) {
                userRoles.push(isFirstOfRole);
                isNewRoleOnlyRole = scheduleOrAccessRoles === isFirstOfRole.toString().split();
                LF.Data.scheduleCount++;
            }

            isRoleIntersectionEqual = _.isEqual(_.intersection(scheduleOrAccessRoles, diaryRoles).sort(), _.intersection(scheduleOrAccessRoles, userRoles).sort());
            isRoleInUse = _.intersection(scheduleOrAccessRoles, userRoles).length !== 0;

            this.turnOffAlarm = !isRoleInUse || (this.alarmParams.time === '');

            if (!_.isNumber(dayOffset) || dayOffset === 0) {

                // Starting Alarm time is missed and diary is not completed today
                // add remaining reminders
                if ((isAlarmMissedToday && !isDiaryCompletedToday) || (isDiaryCompletedToday && !isRoleIntersectionEqual && !isShared && isAlarmMissedToday) || (!isAlarmMissedToday && isFirstOfRole && isNewRoleOnlyRole)) {
                    switch (this.alarmParams.repeat) {
                        case 'daily':
                            if (isFirstOfRole && !isAlarmMissedToday && nowFicUtcDate.toDateString() !== alarmTimeFicUtcDate.toDateString() && _.contains(scheduleOrAccessRoles, isFirstOfRole)) {
                                alarmDate.setDate(alarmDate.getDate() - 1);
                                alarmTimeFicUtcDate = LF.Utilities.convertToUtc(alarmDate);
                                alarmTimeUtcMillis = alarmDate.getTime();
                            }
                            this.alarmParams.oneTimeReminders = AlarmManager.getReminders(alarmTimeUtcMillis, this.alarmParams.reminders, this.alarmParams.reminderInterval);
                            break;
                        case 'once':
                            break;
                        default:
                            break;
                    }
                }

                // Starting Alarm time is missed or diary is completed today
                if ((isAlarmMissedToday) || (isDiaryCompletedToday && (isShared || (!isShared && isRoleIntersectionEqual)))) {
                    switch (this.alarmParams.repeat) {
                        case 'daily':
                            alarmDate.setDate(alarmDate.getDate() + 1);
                            break;
                        case 'once':
                            break;
                        case undefined:
                            this.turnOffAlarm = true;
                            break;
                        default:
                            throw new Error(`Incorrect repeat type: ${this.alarmParams.repeat}`);
                    }
                }
            } else {
                alarmDate.setDate(alarmDate.getDate() + dayOffset);
            }

            if (LF.Data.scheduleCount === LF.Data.numberOfAlarmSchedules) {
                localStorage.removeItem('isFirstOfRole');
            }

            this.alarmParams.date = alarmDate;
        })
        .catch(err => {
            logger.error('Error setting alarm date', err);
        });
    }

    /**
     * This method cancels the alarm if the application is wrapped.
     */
    cancelAlarm () {
        LF.Wrapper.exec({
            execWhenWrapped: () => {
                let alarms = new LF.Collection.ActiveAlarms();

                if (this.alarmParams) {
                    alarms.cancelAlarm(this.alarmParams);
                }
            }
        });
    }
}

window.LF.Class.Alarm = Alarm;
