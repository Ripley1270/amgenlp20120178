import Logger from 'core/Logger';

let logger = new Logger('Cookie');

/**
 * A class that sets and gets cookie.
 * @class Cookie
 * @example var cookie = new Cookie()
 */
export default class Cookie {

    /**
     * @deprecated
     */
    constructor () {
        logger.warn('Instantiated use of Cookie is deprecated. Use as static, instead', new Error('Stack'));
    }

    /**
     * Use static version, Cookie.set instead
     * @param {string} name - The name of the cookie.
     * @param {string} value - The value of the cookie as a JSON string.
     * @param {number=} exdays - The number of days before the cookie expires.
     * @deprecated
     */
    setCookie (name, value, exdays) {
        Cookie.set(name, value, exdays);
    }

    /**
     * Use static version, Cookie.get instead
     * @param {string} name - The name of the desired cookie.
     * @returns {string} The value of the cookie.
     * @deprecated
     */
    getCookie (name) {
        return Cookie.get(name);
    }

    /**
     * Sets a cookie.
     * @param {String} cookieName The name of the cookie.
     * @param {String} value The value of the cookie as a JSON string.
     * @param {Number=} exdays The number of days before the cookie expires.
     * @example
     * Cookie.set('phtCookie', JSON.stringify({
     *     study : 1,
     *     language : en,
     *     locale : us
     * }));
     */
    static set (cookieName, value, exdays) {
        let expires = '';

        if (exdays) {
            let expireDt = new Date(Date.now() + exdays * 7 * 24 * 60 * 60 * 1000); // millis to days
            expires = `expires=${expireDt.toUTCString()};`; // including the semicolon
        }

        let cookieData = `${encodeURIComponent(value)}; ${expires} path=/`;

        document.cookie = `${cookieName}=${cookieData}`;
    }

    /**
     * Gets a cookie
     * @param {String} cookieName The name of the desired cookie.
     * @returns {String} The value of the cookie.
     */
    // eslint-disable-next-line consistent-return
    static get (cookieName) {
        let arrCookies = document.cookie.split(';');

        for (let i = 0; i < arrCookies.length; i += 1) {
            let cookie = arrCookies[i].trim();

            // no worky; why? name="" after this. Babel bug?
            // yet all and value get set properly.
            // let [all, name, value] = cookie.match(/([^=]+)=(.+)/);

            let parts = cookie.match(/([^=]+)=(.+)/);
            let name = parts[1];
            let value = parts[2];
            if (name === cookieName) {
                return decodeURIComponent(value);
            }
        }
    }
}

window.LF.Class.Cookie = Cookie;
