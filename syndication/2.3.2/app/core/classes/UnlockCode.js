
const LENGHT_OF_UNLOCK_CODES = 8;

let hashString = function (input) {
    return hex_md5(input).substring(0, LENGHT_OF_UNLOCK_CODES);
};

let padString = function (source, length, padChar, padRight) {
    let split = source.split(''),
        diff = length - split.length;

    if (diff > 0) {
        for (let i = 0; i < diff; i += 1) {
            split[padRight ? 'push' : 'unshift'](padChar);
        }
    }

    return split.join('');
};

let convertHextoDec = function (input) {
    let unlockCode =  (parseInt(input, 16)).toString();

    if (unlockCode.length < 10) {
        unlockCode = padString(unlockCode, 10, '0', false);
    }

    return unlockCode;
};

export default class UnlockCode {
    static createStartupUnlockCode (siteCode, databaseName) {
        let output;

        if (siteCode == null) {
            throw new Error('Missing arugment (siteCode) to createStartupUnlockCode.');
        }

        if (databaseName == null) {
            throw new Error('Missing argument (databaseName) to createStartupUnlockCode.');
        }

        output = hashString(databaseName + siteCode);
        output = convertHextoDec(output);

        return output;
    }
}
