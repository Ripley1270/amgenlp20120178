import Logger from 'core/Logger';

const logger = new Logger('DynamicText');

/**
 * Stores and updates dynamic text entries
 * @class DynamicText
 */
export default class DynamicText {
    /**
     * Creates an instance of DynamicText.
     */
    constructor () {
        this.list = [];
    }

    /**
     * Update the screenshot values for the item with the given key.
     * @param {string} key - The key to store the dynamic text entry under.
     * @param {Object} [screenshots] - Object containing screenshot settings.
     * @param {Array<string>} [screenshots.values] - List of static values to use in screenshots.
     * @param {Object} [screenshots.keys] - Object containing string keys and namespace.
     * @param {Strings} [screenshots.keys.namespace] - Namespace to use for obtaining translations.
     * @param {function} [screenshots.getValues] - Function used to get values to use for screenshots.
     */
    updateScreenshots (key, screenshots = {}) {
        if (this.list[key]) {
            this.list[key].screenshots = _.extend({}, this.list[key].screenshots, screenshots);
        } else {
            logger.error(`No dynamic text entry found for key: ${key}`);
        }
    }

    /**
     * Add dynamic text entry to the list.
     * @param {Object} dText - object containing dynamic text.
     * @param {string} dText.id - id to use as a key for the dynamic text.
     * @param {function} dText.evaluate - function run to get the dynamic text value.
     * @param {function} [dText.screenshots] - optional object to provide screenshot settings.
     */
    addDynamicText (dText) {
        if (this.list[dText.id]) {
            logger.info(`Dynamic text entry for ${dText.id} already exists, overriding entry.`);
        }

        this.list[dText.id] = {
            id: dText.id,
            evaluate: dText.evaluate || $.noop
        };

        this.updateScreenshots(dText.id, dText.screenshots || {});
    }

    /**
     * Add multiple dynamic text entries at once.
     * @param {...object} entries - Dynamic text entries to add to the list.
     */
    add (...entries) {
        entries.forEach(entry => this.addDynamicText(entry));
    }

    /**
     * Add a dynamic text entry from an object containing entries
     * @param {object} entriesObject - object containing multiple entries to add.
     * @example
     * dynamicText.addFromObject({
     *     getSubjectNumber: { id, evaluate, screenshots },
     *     getName: { id, evaluate, screenshots }
     * });
     */
    addFromObject (entriesObject) {
        Object.keys(entriesObject).map(key => this.addDynamicText(entriesObject[key]));
    }

    /**
     * Add multiple dynamic text entries that are objects containing entries.
     * @param {...object} entriesObjects - the objects containing entries to add.
     */
    addFromObjects (...entriesObjects) {
        entriesObjects.forEach(obj => this.addFromObject(obj));
    }
}