// Import the study design, templates and rules.
import trial from 'core/index';

import COOL from 'core/COOL';
import Logger from 'core/Logger';
import NotifyView from 'core/views/NotifyView';
import StudyDesign from 'core/classes/StudyDesign';
import CurrentContext from 'core/CurrentContext';
import { setServiceBase } from 'core/utilities';


let logger = new Logger('BaseStartup');

export default class BaseStartup {

    /**
     * Runs prior to any other startup method.
     * @returns {Q.Promise<void>}
     */
    preSystemStartup () {
        return Q.Promise((resolve) => {
            logger.traceEnter('preSystemStartup');

            // From Q docs:
            // This feature does come with somewhat-serious performance and memory overhead, however.
            // If you're working with lots of promises, or trying to scale a server to many users, you
            // should probably keep it off. But in development, go for it!
            Q.longStackSupport = false;

            // This code periodically asks Q if it has detected any
            // unhandled rejections.  If yes, it prints them out to console.
            // No point in reporting the ELF.trigger rejection, so suppress it.
            window.qMonitorTimer =  window.setInterval(() => {
                let badness = Q.getUnhandledReasons();
                if (badness.length) {
                    logger.error(`Detected ${badness.length} unhandled rejections of Q promises`);
                    let stacks = _.chain(badness)
                        .reject((reason) => /.no stack. undefined$/i.test(reason))
                        .unique()
                        .take(4) // cap it at 4 stack traces
                        .value();
                    if (stacks.length) {
                        logger.traceEnter('Available stack traces');
                        stacks.forEach((reason, ind) => logger.error(`#${ind + 1}: ${reason}`));
                        logger.traceExit();
                    } else {
                        logger.warn('Sorry, no stack traces available :(');
                    }
                }
                Q.resetUnhandledRejections();
            }, 1000);

            logger.traceExit('preSystemStartup');
            resolve();
        });
    }

    /**
     * Ensures that the device is ready
     * @returns {Q.Promise<void>}
     */
    cordovaStartup () {
        logger.traceEnter('cordovaStartup');

        LF.Wrapper.detectWrapper();

        // Without this, app is never fully initialized (Cordova stuff, etc)
        return LF.Wrapper.initialize()
        .catch((err) => {
            logger.error('Error on cordova startup', err);
        })
        .finally(() => {
            logger.traceExit('cordovaStartup');
        });
    }

    /**
     * Ensures the DOM is ready.
     * @returns {Q.Promise<void>}
     */
    jQueryStartup () {
        return Q.Promise((resolve) => {
            logger.traceEnter('jQueryStartup');

            // Wait until jQuery starts
            $(() => {
                logger.trace('jQuery is satisfied');
                resolve();
            });
        })
        .catch((err) => {
            logger.error('Error on jQuery startup', err);
        })
        .finally(() => {
            logger.traceExit('jQueryStartup');
        });
    }

    /**
     * If validation of the Study Design failed, display a notification.
     * @returns {Q.Promise<void>}
     */
    notifyStudyDesignValidation () {
        if (LF.StudyDesign.failedValidation) {
            return new NotifyView().show({
                header: 'Validation Error',
                body: 'Study Design failed validation.\nCheck console logs.',
                ok: 'OK',
                type: 'error'
            });
        } else {
            return Q();
        }
    }

    /**
     * Setup the local users and context.
     * @returns {Q.Promise<void>}
     */
    usersAndContext () {
        logger.traceEnter('usersAndContext');
        LF.security.startSessionTimeOut();

        CurrentContext.init();

        return CurrentContext().fetchUsers()
        .then(() => CurrentContext().checkUserAuth())
        .catch(err => {
            logger.error('Error setting up users and context', err);
        })
        .finally(() => {
            logger.traceExit('usersAndContext');
        });
    }

    /**
     * Constructs key study components such as the Study Design, Templates and Web Service.
     * @returns {Q.Promise<void>}
     */
    studyStartup () {
        return Q.Promise((resolve) => {
            logger.traceEnter('studyStartup');

            // Set the study design in the global namespace.
            logger.trace(`Processing study design: ${trial.assets.studyDesign}`);
            LF.StudyDesign = new StudyDesign(trial.assets.studyDesign);

            setServiceBase();

            // Add the custom trial-specific study rules
            trial.assets.trialRules(ELF.rules);

            // Add the custom study templates to the global collection.
            logger.trace('Adding templates...');
            LF.templates.add(trial.assets.studyTemplates);

            // Add the custom study web service to the global namespace.
            LF.studyWebService = COOL.new('WebService', trial.assets.studyWebService);

            logger.traceExit('studyStartup');
            resolve();
        });
    }
}
