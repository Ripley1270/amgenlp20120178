import ELF from 'core/ELF';
import BackboneRouteControl from './BackboneRouteControl';

let flashParameters = {};

export default class RouterBase extends BackboneRouteControl {
    constructor (options) {
        super(options);

        // Whenever the route is changed, we update a local reference to the current controller.
        // We use this when LF.router.view() is called to get the correct view.
        this.on('route', (route) => {
            let controller = route.split('#')[0];

            this.controller = this.controllers[controller];
        });
    }

    /**
     * Manually bind a single named route to a callback.
     * NOTE: The majority of this method is abstracted from backbone's Router.route method.
     * It's been modified to account for cases where RouterBase.execute returns a promise with falsy value.
     * Backbone's default Router.route method only allows for an immediate return of false.
     * @param {string} route The route to bind.
     * @param {string} name The name of the route.
     * @param {Function} callback A callback function invoked when the route is hit.
     * @returns {RouterBase}
     * @example
     * this.route('search/:query/p:num', 'search', function(query, num) {
     *     ...
     * });
     */
    route (route, name, callback) {
        if (!_.isRegExp(route)) {
            route = this._routeToRegExp(route);
        }

        if (_.isFunction(name)) {
            callback = name;
            name = '';
        }

        if (!callback) {
            callback = this[name];
        }

        let router = this;

        Backbone.history.route(route, (fragment) => {
            let args = router._extractParameters(route, fragment);

            router.execute(args, name)
            .then(res => {
                if (res) {
                    callback && callback.apply(this, args);

                    // eslint-disable-next-line prefer-spread
                    router.trigger.apply(router, [`route:${name}`].concat(args));
                    router.trigger('route', name, args);

                    Backbone.history.trigger('route', router, name, args);
                }
            })
            .done();
        });

        return this;
    }

    /**
     * Called prior to a controller's method being ivoked.
     * Triggers an ELF event 'NAVIGATE:${controller}/${route}' which can be used to determine if the view
     * should be displayed or not.  This can also be used to reroute.
     * @param {Array<String>} args Arguments passed in by the route.
     * @param {String} name The string value defined in the router for the url
     * @returns {Q.Promise<boolean>}
     */
    execute (args, name) {
        let [controller, route] = name.split('#');

        if (!_.isEmpty(flashParameters)) {
            args.splice(args.length - 1, 0, flashParameters);
            flashParameters = {};
        }

        return ELF.trigger(`NAVIGATE:${controller}/${route}`, args, this)
            .then(evt => {
                return evt.preventDefault ? false : true;
            });
    }

    /**
     * Returns a reference to the current view.
     * @returns {Backbone.View}
     */
    view () {
        let controller = this.controller || {};

        return controller.view;
    }

    /**
     * Sets flash parameters to be passed into the next route.
     * Flash parameters are cleared after each route is executed.
     * @param {Object} [params={}] The parameter object to be passed wih the router navigation.
     * @returns {RouterBase}
     * @example
     * router.flash({ patient }).navigate('edit-patient');
     */
    flash (params = {}) {
        if (!_.isEmpty(params)) {
            flashParameters = params;
        }

        return this;
    }
}
