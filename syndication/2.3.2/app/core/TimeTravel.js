/* eslint-disable no-use-before-define */
// TODO - This module needs to be restructured.
import AlarmManager from 'core/AlarmManager';
import ActiveAlarms from 'core/collections/ActiveAlarms';
import Logger from 'core/Logger';

const logger = new Logger('TimeTravel');

/**
 * Time Travel Module
 * @returns {Object} - returns object containing exposed Time Travel methods
 */

export default (() => {
    // TODO: rename variables to remove _ prefix. This is against coding conventions
    let _now,
        _savedAddAlarm,
        _timeTravelTimer,
        _defaultDateConfig,
        _defaultTimeConfig,
        _timeTraveled,
        _nativeDate = Date,
        _timeTraveling = false,
        _displayTimeTravel = true,

        /**
         * Pad a value with a 0 if less than 10
         * @param {number} n - is the value you wish to pad
         * @returns {string} The padded number as a string.
         * @example pad(dateString);
         */
        pad = n => {
            return (n < 10) ? `0${n}` : `${n}`;
        },

        /**
         * Opens the date picker modal
         * @example changeTime();
         */
        changeTime = () => {
            let date = new Date(),
                defaultDate = `${date.getFullYear()}-${(parseInt(date.getMonth(), 10) + 1)}-${date.getDate()}`;

            $('#timeTravelDateBox').datebox({ closeCallback: selectedDate, defaultValue: defaultDate }).datebox('open');
        },

        /**
         * Callback once date is selected in date picker modal
         * @example selectedDate();
         */
        selectedDate = () => {
            let newDate = $('#timeTravelDateBox').val(),
                date = new Date(),
                defaultTime = `${date.getHours()}:${date.getMinutes()}`;

            if (newDate) {
                $('#timeTravelTimeBox').datebox({ closeCallback: selectedTime, defaultValue: defaultTime }).datebox('open');
            }
        },

        /**
         * Callback once time is selected in time picker modal
         * @example selectedTime();
         */
        selectedTime = () => {
            let dateBox = $('#timeTravelDateBox'),
                timeBox = $('#timeTravelTimeBox'),
                newDate = dateBox.val(),
                newTime = timeBox.val();

            if (!!newDate && !!newTime) {
                applyNewTimeTravelDate(newDate, newTime);
            }

            dateBox.val('');
            timeBox.val('');
        },

        /**
         * Callback once time is selected in time picker modal
         * @param {String} strDate the date selected from the date picker modal
         * @param {String} strTime the time selected from the time picker modal
         * @example selectedDate(dateString, timeString);
         */
        applyNewTimeTravelDate = (strDate, strTime) => {
            let newDate = strDate.split('-'),
                newTime = strTime.split(':'),
                newDateTime = new Date(newDate[0],
                        parseInt(newDate[1], 10) - 1, newDate[2], newTime[0], newTime[1], 0, 0).getTime(),
                currentTime = new _nativeDate().getTime();

            if (newDateTime <= currentTime) {
                stopTimeTravel();
            } else {
                setTimeTravelDate(newDateTime);
            }
        },

        /**
         * Update the time travel clock as well as displays the 'Set Time' button and Time Travel time if on the dashboard
         * @example updateTimeTravelClock();
         */
        updateTimeTravelClock = () => {
            // TODO: SitePad
            let timeTravelRef = $('#timeTravel'),
                setTimeTravelRef = $('#setTimeTravel'),
                dashboardHeaderClock = $('#timeTravel span.ui-title');

            clearTimeout(_timeTravelTimer);

            _timeTravelTimer = setTimeout(() => {
                let now = new Date();

                if (_timeTraveling) {
                    let realTime = new _nativeDate();
                    now.setTime(realTime.getTime() + _timeTraveled);

                    overrideDate(now.getTime());
                }

                if (_displayTimeTravel) {
                    if (timeTravelRef.length) {
                        if (!setTimeTravelRef.length) {
                            timeTravelRef.append('<div><button id="setTimeTravel" type="button" class="btn btn-primary">Set Time</button></div>')
                                .trigger('create')
                                .on('click', '#setTimeTravel', changeTime);
                        }

                        dashboardHeaderClock.html(`${pad(now.getMonth() + 1, 2)}/` +
                                `${pad(now.getDate(), 2)}/` +
                                `${now.getFullYear()} ` +
                                `${pad(now.getHours(), 2)}:` +
                                `${pad(now.getMinutes(), 2)}`);
                    }
                }

                updateTimeTravelClock();
            }, 1000);
        },

        /**
         * Resets the application security timers
         * @example resetSecurity();
         */
        resetSecurity = () => {
            LF.security.pauseSessionTimeOut();
            LF.security.setTimeoutLastCheck();
            LF.security.setLastActive();
            LF.security.restartSessionTimeOut();
        },

        /**
         * Sets the newly entered date to Time Travel to
         * @param {number} newDate the timestamp to Time Travel to
         * @example LF.TimeTravel.setTimeTravelDate(newTimeTravelDate);
         */
        setTimeTravelDate = newDate => {
            let nativeNow = new _nativeDate();

            // adjust the seconds on the selected time.
            newDate = newDate + (nativeNow.getSeconds() * 1000);

            _timeTraveled = newDate - nativeNow;
            localStorage.setItem('PHT_Time_Traveled', _timeTraveled);

            clearTimeout(_timeTravelTimer);

            overrideDate(newDate);

            if (!_timeTraveling) {
                hijackAddAlarm();
            }

            resetSecurity();

            // Cancel and reschedule Alarms on LogPad
            if (LF.appName === 'LogPad App') {
                LF.Wrapper.exec({
                    execWhenWrapped: () => {
                        rescheduleAlarms()
                        .then(() => {
                            LF.Helpers.rescheduleAlarms();
                        })
                        .catch(err => {
                            logger.error('Error rescheduling Alarms', err);
                        })
                        .done();
                    }
                });
            }

            _timeTraveling = true;
            LF.router.view().render();

            // Start updating the clock
            updateTimeTravelClock();
        },

        /**
         * Stop Time Travel and go back to current date and time
         * @example LF.TimeTravel.stopTimeTravel();
         */
        stopTimeTravel = () => {
            clearTimeout(_timeTravelTimer);

            Date = _nativeDate; // eslint-disable-line

            if (_timeTraveling) {
                AlarmManager.addAlarm = _savedAddAlarm;
            }

            resetSecurity();

            // Cancel and reschedule Alarms
            LF.Wrapper.exec({
                execWhenWrapped: () => {
                    rescheduleAlarms()
                    .then(() => {
                        LF.Helpers.rescheduleAlarms();
                    })
                    .catch(err => {
                        logger.error('Error rescheduling alarms', err);
                    })
                    .done();
                }
            });

            localStorage.removeItem('PHT_Time_Traveled');

            _timeTraveling = false;
            _timeTraveled = undefined;

            updateTimeTravelClock();
            LF.router.view().render();
        },

        /**
         * Display Time Travel time as well as the 'Set Time' button
         * @param {boolean} enable - True to display time travel, false if not.
         * @example LF.TimeTravel.displayTimeTravel(true|false);
         */
        displayTimeTravel = enable => {
            _displayTimeTravel = enable;

            if ($('#timeTravel').length && LF.router) {
                Q().then(() => {
                    return LF.router.view().render();
                })
                .catch(err => {
                    logger.error('Error rendering view', err);
                })
                .done();
            }
        },

        /**
         * Overrides the native Date() object
         * @param {Date} newDate a date to override the native date with
         * @example overrideDate(Date);
         */
        overrideDate = (newDate) => {
            // Override the native Date
            // eslint-disable-next-line
            Date = function () {
                // eslint-disable-next-line prefer-rest-params
                if (arguments.length === 1) {
                    // eslint-disable-next-line prefer-rest-params
                    return new _nativeDate(arguments[0]);
                // eslint-disable-next-line prefer-rest-params
                } else if (arguments.length === 7) {
                    // eslint-disable-next-line prefer-rest-params
                    return new _nativeDate(arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], arguments[5], arguments[6]);
                } else {
                    return new _nativeDate(newDate);
                }
            };

            Date.prototype = _nativeDate.prototype;

            // Added following functions' reference manually because they are
            // not prototype functions
            Date.UTC = _nativeDate.UTC;
            Date.parse = _nativeDate.parse;
            Date.now = _nativeDate.now;
        },

        /**
         * Add an alarm
         * @param {Number} id - unique id of the alarm
         * @param {Date} date - when the alarm should first appear
         * @param {string} title - title of the message
         * @param {string} message - the message that is displayed
         * @param {(string|number)} repeat - interval at which the alarm repeats
         * @param {string} scheduleId - The ID of the scheduled alarm.
         * @param {Object} alarmVolumeConfig - The configuration for the alarms volume.
         */
        addTimeTravelAlarm = (id, date, title, message, repeat, scheduleId, alarmVolumeConfig) => {
            let now = new Date(),
                systemTime = new _nativeDate().getTime(),
                offsetTime = date.getTime() - now.getTime(),
                setDate = new Date(systemTime + offsetTime);

            if (offsetTime > 0) {
                _savedAddAlarm(id, setDate, title, message, repeat, scheduleId, alarmVolumeConfig);
            }
        },

        /**
         * Takes control of the AlarmManager.addAlarm while time traveling
         * @example hijackAddAlarm();
         */
        hijackAddAlarm = () => {
            _savedAddAlarm = AlarmManager.addAlarm;
            AlarmManager.addAlarm = addTimeTravelAlarm;
        },

        rescheduleAlarms = () => {
            return ActiveAlarms.fetchCollection().then(alarms => {
                const repeatingAlarms = alarms.filter(alarm => {
                    const repeat = alarm.get('repeat');
                    return repeat !== 'once' && repeat !== undefined;
                });

                _.each(repeatingAlarms, alarm => {
                    AlarmManager.cancelAlarm(alarm.get('id'));
                    alarm.destroy();
                });
            })
            .catch(err => {
                logger.error('Error rescheduling alarms', err);
            });

        };

    // Elements added to the DOM for DateBox and TimeBox
    $(document).ready(() => {
        // Date Modal configuration
        _defaultDateConfig = JSON.stringify({
            mode: 'calbox',
            calUsePickers: true,
            useButton: false,
            calNoHeader: true,
            pickerMinYears: 1900,
            pickerMaxYears: 2100,
            useSetButton: true,
            calHighToday: false,
            calHighPick: false,
            useAnimation: false,
            themeDatePick: 'z',
            themeHeader: 'b',
            useHeader: true,
            centerHoriz: true,
            centerVert: true,
            useModal: true,
            useNewStyle: true
        });

        $('body').prepend(
            `<input id="timeTravelDateBox" style="display:none;" type="text" data-role="datebox" data-options='${_defaultDateConfig}'></input>`
        ).trigger('create');

        // Time Modal configuration
        _defaultTimeConfig = JSON.stringify({
            mode: 'timebox',
            useSetButton: true,

            // Dup-key here.  Assuming the second instance is correct...
            // useHeader: true,
            rolloverMode: {
                h: false
            },
            useAnimation: false,
            themeDatePick: 'z',
            themeHeader: 'b',
            useHeader: true,
            centerHoriz: true,
            centerVert: true,
            useModal: true,
            useNewStyle: true,
            useButton: false
        });

        $('body').prepend(`<input id="timeTravelTimeBox" style="display:none;" type="text" data-role="datebox" data-options='
                ${_defaultTimeConfig}'></input>`).trigger('create');
    });

    if (localStorage.getItem('PHT_Time_Traveled')) {
        _now = new _nativeDate();
        // If still Time Traveling continue Time Traveling!
        _timeTraveled = parseInt(localStorage.getItem('PHT_Time_Traveled'), 10);

        hijackAddAlarm();
        _timeTraveling = true;

        overrideDate(_now.getTime() + _timeTraveled);
    }

    // Starts updating Time Travel (or native date if not time traveling) clock
    updateTimeTravelClock();

    // Exposed Time Travel methods
    return {
        setTimeTravelDate,
        stopTimeTravel,
        displayTimeTravel
    };
})();
