const {View} = Backbone;

// quarter second
const AUTO_SCROLL_MILLISECONDS = 250,
    DEFAULT_DECELERATION = 0.0006;

const MOUSE_DOWN_EVENTS = ['MSPointerDown', 'mousedown', 'pointerdown', 'touchstart'];
const MOUSE_UP_EVENTS = ['MSPointerUp', 'mouseup', 'pointerup', 'touchend'];

/**
 * An input control that works as a spinner.
 * This is essentially a scrollable area that keeps track of a "value" property, which is the item that is in the
 * center of the area.
 *
 * This scrolls with momentum and also allows selecting an item by clicking it.
 */
export default class BaseSpinnerInput extends View {
    /**
     * Items cache.  Only includes available items (not ones that are hidden, because we cannot select them)
     * @returns {jQuery|null}
     */
    get $items () {
        if (this._$items === null && this.$itemContainer) {
            this._$items = this.$itemContainer.find('.item');
        }
        return this._$items;
    }

    get value () {
        if (this.model) {
            return this.model.get('value');
        }
        return null;
    }

    set value (val) {
        this.model.set('value', val);
    }

    /**
     * Whether or not a finger or mouse is currently down.  If so, cancel any end events.
     * NOTE:  Array is searched starting at second to last item, because this will be called during another event
     *              , and we want to ignore the ongoing event.
     * @type {boolean}
     */
    get mouseDown () {
        for (let i = this.spinnerEventQueue.length - 2; i >= 0; --i) {
            switch(this.spinnerEventQueue[i]) {
                case 'mousedown':
                    return true;
                case 'mouseup':
                    return false;
            }
        }
        return false;
    }

    /**
     * Whether or not we are currently in a scroll event
     * NOTE:  Array is searched starting at second to last item, because this will be called during another event
     *              , and we want to ignore the ongoing event.
     * @type {boolean}
     */
    get inScroll () {
        for (let i = this.spinnerEventQueue.length - 2; i >= 0; --i) {
            switch(this.spinnerEventQueue[i]) {
                case 'scrollStart':
                    return true;
                case 'scrollEnd-success':
                    return false;
            }
        }
        return false;
    }

    /**
     * Whether or not a scroll started from the user (if false during a scroll event, we are in an auto-scroll).
     * NOTE:  Array is searched starting at second to last item, because this will be called during another event
     *              , and we want to ignore the ongoing event.
     * @type {boolean}
     */
    get userInitiatedScroll () {
        for (let i = this.spinnerEventQueue.length - 2; i >= 0; --i) {
            switch (this.spinnerEventQueue[i]) {
                case 'beforeScrollStart':
                    return true;
                case 'scrollEnd-success':
                    return false;
            }
        }
        return false;
    }

    /**
     * Get previous spinner event.
     * NOTE:  The item returned is the second to last item, because this will be called during another event
     *              , and we want to ignore the ongoing event.
     * @type {string|null}
     */
    get previousSpinnerEvent () {
        if (this.spinnerEventQueue.length >= 2) {
            return this.spinnerEventQueue[this.spinnerEventQueue.length - 2];
        }
        return null;
    }

    /**
     * Construct the spinner input object.
     * @param {Object} options options for this spinner.
     * @param {Model} options.model Backbone model to bind attributes.
     * @param {Element} options.parent parent element for our input control.
     * @param {string} options.itemTemplate template for an individual item in our input control.  (Note: this is the
     * lookup key to for template resources.)
     * @param {string} options.template template for our entire input control.  (Note, this should be expanded into
     * HTML, not a lookup in Templates).
     * @param {string} [options.deceleration] deceleration of our spinner.
     * @param {string} [options.autoScrollMilliseconds] number of milliseconds required to do an auto scroll (i.e. after
     * a click, or after scrolling approximately to the location of an item).
     * @param {string} [options.numItems] number of items to be displayed.  Passing a number here will change the
     * height of the item-container element to accomodate N number of elements to be displayed at a time.
     * Otherwise, the height remains whatever it was from CSS.
     */
    constructor (options) {
        super(options);

        // Parameter validation
        if (!options.parent) {
            throw new Error('Invalid configuration for Spinner.  "parent" is undefined.');
        }
        if (!options.itemTemplate) {
            throw new Error('Invalid configuration for Spinner.  "itemTemplate" is undefined.');
        }
        if (!options.template) {
            throw new Error('Invalid configuration for Spinner.  "template" is undefined.');
        }

        this.parent = options.parent;

        this.model.set('itemTemplate', options.itemTemplate);
        this.model.set('template', options.template);
        this.model.set('deceleration', options.deceleration || DEFAULT_DECELERATION);
        this.model.set('numItems', options.numItems || null);
        this.model.set('autoScrollMilliseconds',
            typeof options.autoScrollMilliseconds === 'number' ?
                options.autoScrollMilliseconds :
                AUTO_SCROLL_MILLISECONDS
        );

        this.model.set('value', options.value || null);

        /**
         * IScroll object.
         * @type {IScroll}
         */
        this.scroller = null;

        /**
         * Whether or not the widget was previously rendered.  Decides whether or not to do full render, or just
         * refreshing the currently selected value on show().
         * @type {boolean}
         */
        this.isRendered = false;

        this._renderPromise = Q.Promise.resolve();

        /**
         * Item container JQuery selector.
         * @type {jQuery}
         */
        this.$itemContainer = null;
        
        /**
         * Reference to window
         * @type {jQuery}
         */
        this.$win = $(window);

        /**
         * Keep track of spinner events that have occurred.  Gets reset after every thing.
         * @type {Array}
         */
        this.spinnerEventQueue = [];

        /**
         * Item cache
         * @private
         */
        this._$items = null;

        /**
         * Selected item by this control
         * @type {Element|null}
         */
        this.selectedItem = null;
    }

    /**
     * Parse the display value (shown in spinner) from the actual value that gets stored.
     * Default is for them to be identical.  One can override if deemed necessary
     * For instance, a number should do some fixed precision, and an item spinner (e.g. "Sunday-Saturday")
     * should do some sort of lookup based on a numeric value.
     * @param {string|number} value The value
     * @returns {string}
     */
    itemDisplayValueFunction (value) {
        return value;
    }

    /**
     * Parse the item value (stored value in spinner) from the value used to establish the template.
     * Default is for them to be identical.  One reason to override this is to create a fixed precision for numeric
     * values so that roundoff error mess up the appearance in the textbox in the NumberSpinner control.
     * @param {string|number} value The value
     * @returns {string}
     */
    itemValueFunction (value) {
        return value;
    }

    /**
     * Set our values.  Values can be passed into the model in a way that makes sense to the varation of the spinner.
     * @abstract
     */
    setValues () {
        throw new Error('setValues() is not defined in a BaseSpinnerInput extension.');
    }

    /**
     * Show our spinner.  Either by rendering or just refreshing the scroller (if it's already rendered).
     * @returns {Promise<{jQuery}>} promise containing this.$el for convenience, in case the caller is interested
     * in listening to ready().
     */
    show () {
        if (!this.isRendered) {
            this._renderPromise = this.render();
        } else {
            this._renderPromise = this._renderPromise.then(() => {
                this.scroller.refresh();
                return this.$el;
            });
        }
        return this._renderPromise;
    }

    /**
     * Render the spinner.
     * @returns {Promise<{jQuery}>} promise containing this.$el for convenience, in case the caller is interested
     * in listening to ready().
     */
    render () {
        let that = this;

        that.isRendered = true;
        return new Q.Promise((resolve) => {
            that.$el.append(this.model.get('template'));
            //jscs:disable requireArrowFunctions
            $(that.parent).append(this.$el)
                .ready(function () {
                    that.$itemContainer = that.$el.find('.item-container');
                    that.setValues();
                }).ready(function () {
                that.setHeights();
                that.setEmptyAreas();
                that.scroller = new IScroll(that.$itemContainer.parent()[0],
                    {
                        deceleration: that.model.get('deceleration'),
                        tap: true
                    });
                that.clearItemCache();
            }).ready(function () {
                that.delegateEvents();
                resolve(that.$el);
            });
            //jscs:enable requireArrowFunctions
        });
    }

    /**
     * Overridden to have handlers for scroller as well.
     * (in the events object) so create and destroy manually
     */
    delegateEvents () {
        super.delegateEvents();
        this.removeCustomEvents();
        this.addCustomEvents();
    }

    /**
     * Overridden to remove our custom delegated events.
     */
    undelegateEvents () {
        super.undelegateEvents();
        this.removeCustomEvents();
    }

    /**
     * Add custom events to our spinner.
     */
    addCustomEvents () {
        if (this.scroller) {
            this.handleScrollEnd = _.bind(this.handleScrollEnd, this);
            this.handleScrollCancel = _.bind(this.handleScrollCancel, this);
            this.handleBeforeScrollStart = _.bind(this.handleBeforeScrollStart, this);
            this.handleScrollStart = _.bind(this.handleScrollStart, this);
            this.scroller.on('scrollEnd', this.handleScrollEnd);
            this.scroller.on('scrollCancel', this.handleScrollCancel);
            this.scroller.on('beforeScrollStart', this.handleBeforeScrollStart);
            this.scroller.on('scrollStart', this.handleScrollStart);
        }
        if (this.$itemContainer) {
            this.handleTap = _.bind(this.handleTap, this);
            this.handleMouseDown = _.bind(this.handleMouseDown, this);
            this.handleMouseUp = _.bind(this.handleMouseUp, this);

            this.$itemContainer.on('tap', this.handleTap);
            
            _.each(MOUSE_DOWN_EVENTS, (eventName) => {
                this.$itemContainer.on(eventName, this.handleMouseDown);
            });

            _.each(MOUSE_UP_EVENTS, (eventName) => {
                this.$win.on(eventName, this.handleMouseUp);
            });
        }
    }

    /**
     * Removing custom events from our spinner.
     */
    removeCustomEvents () {
        if (this.scroller) {
            this.scroller.off('scrollEnd', this.handleScrollEnd);
            this.scroller.off('beforeScrollStart', this.handleBeforeScrollStart);
            this.scroller.off('scrollStart', this.handleScrollStart);
            this.scroller.off('scrollCancel', this.handleScrollCancel);
        }

        if (this.$itemContainer) {
            this.$itemContainer.off('tap', this.handleTap);

            _.each(MOUSE_DOWN_EVENTS, (eventName) => {
                this.$itemContainer.off(eventName, this.handleMouseDown);
            });

            _.each(MOUSE_UP_EVENTS, (eventName) => {
                this.$win.off(eventName, this.handleMouseUp);
            });

        }
    }

    /**
     * Cache heights of item and field.
     * If the user passed in a set number of items to be shown at a time, set a new field height.
     * Assumes all items are the same height (so ensure item template CSS guarantees this).
     */
    setHeights () {
        let $firstItem = this.$items.first(),
            numItems = this.model.get('numItems');
        this.itemHeight = $firstItem.outerHeight();

        if (numItems) {
            this.fieldHeight = this.itemHeight * numItems;
            this.$el.children().first().height(this.fieldHeight).children().first().height(this.fieldHeight);
            this.$el.height(this.fieldHeight);
            
            this.$('div.overlay-top').height();
        } else {
            this.fieldHeight = this.$el.height();
        }
    }

    /**
     * Creates buffer zones at the top and bottom of the spinner, so that the first and last object can be in the
     * middle of the scrollable area, and can be selected.
     */
    setEmptyAreas () {
        let bufferHeight = (this.fieldHeight / 2.0) - (this.itemHeight / 2.0);

        // remove old buffers.
        this.$itemContainer.find('.buffer-item').remove();
        this.$itemContainer.prepend(`<div class="buffer-item" style="height:${bufferHeight}px;">&nbsp;</div>`);
        this.$itemContainer.append(`<div class="buffer-item" style="height:${bufferHeight}px;">&nbsp;</div>`);
    }

    itemValueComparison (currentIndex, value) {
        let valueA = value,
            valueB = this.itemValueFunction($(this.$items[currentIndex]).data('value'));

        // Blank should be first item.  First 2 cases are to coerce the search that direction.
        if (valueA === '' && valueB !== '') {
            return -1;
        } else if (valueB === '' && valueA !== '') {
            return 1;
        } else if (valueA < valueB) {
            return -1;
        } else if (valueA > valueB) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * Get an item by its value.  First, attempt to do a binary search.  If that is unsuccessful (i.e. items are
     * not sorted), then fall back to a jQuery selector (iterative scan).
     * If includeHiddenItems is true, search both $hiddenItems as well as $items before trying a scan of both.
     * @param {number|string} value value to search for
     * @returns {Element|null} element found or null
     */
    getItemByValue (value) {
        // If selectedItem is null, we are either spinning (and forced to choose an answer because the widget is
        // closing), or we are finished spinning.
        // Either way, find item in the middle, set it as the new value, and update the UI.
        // Perform binary search to determine which element we want to select.
        let minIndex = 0,
            maxIndex = this.$items.length - 1,
            currentIndex;
        while (minIndex <= maxIndex) {
            // eslint-disable-next-line no-bitwise
            currentIndex = (minIndex + maxIndex) / 2 | 0;
            let comp = this.itemValueComparison(currentIndex, value);
            if (comp < 0) {
                maxIndex = currentIndex - 1;
            } else if (comp > 0) {
                minIndex = currentIndex + 1;
            } else {
                return this.$items[currentIndex];
            }
        }

        // If it gets here, we need to get it based on a selector.
        let retValue = null;
        //jscs:disable requireArrowFunctions
        this.$items.filter(`.item[data-value='${value}']`).each(function () {
            retValue = this;
            // break out since we found one
            return false;
        });
        //jscs:enable requireArrowFunctions
        // Will be null if everything fell through.
        return retValue;
    }

    /**
     * Immediately move scroller by 'height' pixels
     * @param {number} height number of pixels to scroll.
     */
    adjustScroll (height) {
        if (height !== 0) {
            this.scroller.scrollBy(0, height, 0);
        }
        this.scroller.refresh();
    }

    /**
     * Remove item by value.  Optionally rescroll so our position remains the same
     * if it was above the scroll and we are setting it to display:none.
     * @param {number|string} value value to remove.
     * @param {boolean} [adjustScroller=true] if item is above the scroll line
     * force a rescroll to put us back in the same position, and refresh() the scroller as well.
     * @returns {Promise<Number>} Promise containing the amount that the container would need to be scrolled to remain in the same position
     *                          Note, this number will always be zero if adjustScroller is true (because the scroll will happen immediately)
     */
    hideItem (value, adjustScroller = true) {
        let item = this.getItemByValue(value),
            remainingScrollAmount = 0;
        if (item !== null) {
            let $item = $(item),
                height = $item.outerHeight(),
                top = $item.position().top,
                shouldReScroll;

            shouldReScroll = (top - ((this.fieldHeight / 2.0) - (height / 2.0)) < (-this.scroller.y));

            if ($item.hasClass('hidden-item')) {
                shouldReScroll = false;
            } else {
                $item.addClass('hidden-item');

                // Don't rescroll if we just changed it to gray or something.
                shouldReScroll = shouldReScroll && $item.css('display') === 'none';
            }

            remainingScrollAmount = shouldReScroll ? height : 0;

            if (adjustScroller) {
                this.adjustScroll(remainingScrollAmount);
                remainingScrollAmount = 0;
            }

            if (this.value === value) {
                if (this.selectedItem) {
                    this.selectedItem.classList.remove('selected');
                }
                this.selectedItem = null;
                adjustScroller && this.pushValue();
            }
        }
        return Q(remainingScrollAmount);
    }

    /**
     * Unhide item.  Optionally rescroll so our position remains the same
     * if it was above the scroll and used to be display:none.
     * @param {number|string} value value of item to unhide.
     * @param {boolean} [adjustScroller=true] if item is above the scroll line
     * force a rescroll to put us back in the same position, and refresh() the scroller as well.
     * @returns {Promise<Number>} Promise containing the amount that the container would need to be scrolled to remain in the same position
     *                          Note, this number will always be zero if adjustScroller is true (because the scroll will happen immediately)
     */
    unHideItem (value, adjustScroller = true) {
        let that = this,
            remainingScrollAmount = 0;

        return new Q.Promise((resolve) => {
            let item = that.getItemByValue(value),
                $item;

            $item = $(item);

            if ($item.length > 0 && $item.hasClass('hidden-item')) {
                let shouldReScroll = $item.css('display') === 'none';
                //jscs:disable requireArrowFunctions
                $item.removeClass('hidden-item').ready(function () {
                    let height = $item.outerHeight(),
                        top = $item.position().top;

                    // item now takes up space so subtract its height from this equation as well.
                    shouldReScroll = shouldReScroll &&
                        top - height - ((that.fieldHeight / 2.0) - (height / 2.0)) < (-that.scroller.y);

                    remainingScrollAmount = shouldReScroll ? -height : 0;
                    if (adjustScroller) {
                        that.adjustScroll(remainingScrollAmount);
                        remainingScrollAmount = 0;
                    }

                    resolve(remainingScrollAmount);
                });
                //jscs:enable requireArrowFunctions
            } else {
                resolve(remainingScrollAmount);
            }
        });
    }

    /**
     * Stop scrolling.  Usually used when the window is closed so we can calculate a value.
     */
    stop () {
        this.scroller.scrollBy(0, 0, 0);
    }

    updateSelectedItemUI () {
        if (!this.selectedItem) {
            return;
        }

        if (!this.selectedItem.classList.contains('selected')) {
            this.selectedItem.classList.add('selected');
        }

        if (this.value !== '' && this.value !== null) {
            this.hideItem('');
        }

        // clear spinnerEventQueue, since we're setting the UI and are done with user interaction for the moment.
        this.spinnerEventQueue.splice(0, this.spinnerEventQueue.length);
    }

    /**
     * Set the value.  Also calls out to update the UI.
     * @param {string|number} val The value
     * @returns {Promise<void>} promise resolving when value is unhidden and set.
     */
    setValue (val) {
        this.value = val;
        return this.unHideItem(val, false)
            .then(() => {
                let selectedItem = this.getItemByValue(val);
                // save some time by setting the selectedItem and readying the UI here.
                // so that pullValue() will not need to look it up.
                if (this.selectedItem) {
                    this.selectedItem.classList.remove('selected');
                }
                this.selectedItem = selectedItem;
                this.pullValue();
            });
    }

    /**
     * Pull the value from the "value" property, and use it to update the UI.
     */
    pullValue () {
        let value = this.value;

        if (value === null || value === undefined) {
            if (this.selectedItem) {
                this.selectedItem.classList.remove('selected');
            }
            this.selectedItem = null;
            return;
        }

        if (!(this.selectedItem && $(this.selectedItem).data('value') === this.value)) {
            if (this.selectedItem) {
                this.selectedItem.classList.remove('selected');
            }
            this.selectedItem = this.getItemByValue(value);
        }

        if (this.selectedItem !== null) {
            let $item = $(this.selectedItem);
            let topPos = $item.position().top,
                height = $item.height(),
                curScrollY = this.scroller.y,
                targetScrollTop;

            targetScrollTop = parseInt(-Math.round(topPos - ((this.fieldHeight / 2.0) - (height / 2.0))), 10);
            if (targetScrollTop !== curScrollY) {
                this.scroller.scrollTo(0, targetScrollTop, this.model.get('autoScrollMilliseconds'));
            } else {
                this.updateSelectedItemUI();
            }
        }
    }

    /**
     * Comparison function that determines an item's position, relative to the selected item.
     * For a spinner, an item is considered selected if it is in the middle of the field.
     * @param {number} index index of the element to compare
     * @returns {number|null} comparison number (null if the item is set to not display).
     * (-1 if above than the selected item in the list, 1 if it is below, and 0 if this item is selected).
     */
    itemPositionComparison (index) {
        let $item = $(this.$items[index]),
            relY,
            top,
            height;

        if ($item.css('display') === 'none') {
            return null;
        }

        relY = this.fieldHeight / 2.0;
        top = $item.position().top + this.scroller.y;
        height = $item.height();

        // Edge case... last item.  Make sure our scroller is at LEAST that far, and we're good.
        //  Possible that scroller is even further and this item is out of range if we are past the end
        //  and bouncing.
        if (index === this.$items.length - 1) {
            if (top <= relY) {
                return 0;
            }
        }

        // Similar to bottom.  Just make sure that this item is low enough in the scroll list to count as
        //  selected.  Might fail the other case because of bouncing.
        if (index === 0) {
            if (top + height > relY) {
                return 0;
            }
        }

        if (top + height <= relY) {
            // scroll is too low (item too high)... return 1 to find a higher indexed item
            return 1;
        } else if (top > relY) {
            // scroll is too high (item too low)... return -1 to find a lower indexed item
            return -1;
        } else {
            // if it gets here, item is right in range on the middle of the scroller.  Return 0 for equality.
            return 0;
        }
    }

    /**
     * Push the current selected value into the "value" property.  And call "pullValue()" if the UI isn't updated yet.
     * If "selectedItem" exists, use that (came from a click of a specific item).
     */
    pushValue () {
        if (this.selectedItem === null) {
            // If selectedItem is null, we are either spinning (and forced to choose an answer because the widget is
            // closing), or we are finished spinning.
            // Either way, find item in the middle, set it as the new value, and update the UI.
            // Perform binary search to determine which element we want to select.
            let minIndex = 0,
                maxIndex = this.$items.length - 1,
                currentIndex,
                selectedIndex = null,
                comp = null;
            while (minIndex <= maxIndex) {
                // eslint-disable-next-line no-bitwise
                currentIndex = (minIndex + maxIndex) / 2 | 0;
                comp = null;

                // Next section is a bit of wiggling.
                //  If the comparison function returns null, this means that the position was not calculable,
                //  which means that it is display:none.  Not to fear, just keep moving higher until you find one
                //  that is calculable... then go down until you find one.
                do {
                    comp = this.itemPositionComparison(currentIndex);
                } while (comp === null && ++currentIndex <= maxIndex);

                if (comp === null) {
                    // eslint-disable-next-line no-bitwise
                    currentIndex = (minIndex + maxIndex) / 2 | 0;
                }
                while (comp === null && --currentIndex >= minIndex) {
                    comp = this.itemPositionComparison(currentIndex);
                }

                // Null at this point means all items within this range are disabled.
                if (comp === null) {
                    break;
                }

                if (comp < 0) {
                    maxIndex = currentIndex - 1;
                } else if (comp > 0) {
                    minIndex = currentIndex + 1;
                } else {
                    selectedIndex = currentIndex;
                    break;
                }
            }

            if (selectedIndex !== null) {
                currentIndex = selectedIndex;
                // Now that we know the position of the selected item, choose the next one after it that is not
                // disabled.  If none, go the other direction.
                do {
                    if (!this.$items[currentIndex].classList.contains('hidden-item')) {
                        this.selectedItem = this.$items[currentIndex];
                    }
                } while (this.selectedItem === null && ++currentIndex < this.$items.length);

                if (this.selectedItem === null) {
                    currentIndex = selectedIndex;
                }
                while (this.selectedItem === null && --currentIndex >= 0) {
                    if (!this.$items[currentIndex].classList.contains('hidden-item')) {
                        this.selectedItem = this.$items[currentIndex];
                    }
                }
            }
        }
        if (this.selectedItem !== null) {
            this.value = $(this.selectedItem).data('value');
        }
        this.pullValue();
    }

    /**
     * Event after the scroll has ended.
     */
    handleScrollEnd () {
        this.spinnerEventQueue.push('scrollEnd');

        // Maybe just tapping or changing directions.  Wait it out.
        if (this.mouseDown) {
            return;
        }

        if (!this.userInitiatedScroll) {
            // Auto scroll to snap.  Style when finished.
            this.updateSelectedItemUI();
            this.spinnerEventQueue.push('scrollEnd-success');
            return;
        }

        this.pushValue();
        this.spinnerEventQueue.push('scrollEnd-success');
    }

    /**
     * Handle when scroll is cancelled.  When this occurs, mouse is always up,
     * so set that and forward to handleScrollEnd
     */
    handleScrollCancel () {
        this.spinnerEventQueue.push('scrollCancel');
        this.handleScrollEnd();
    }

    /**
     * Event when a scroll is about to be started by a user.  Updates the userInitiatedScroll property so we know
     * this is not an auto-scroll to center the selected value
     */
    handleBeforeScrollStart () {
        this.spinnerEventQueue.push('beforeScrollStart');
    }

    /**
     * If the user started this scroll... set our value and selectedItem to null, awaiting a new value.
     */
    handleScrollStart () {
        this.spinnerEventQueue.push('scrollStart');

        if (!this.userInitiatedScroll) {
            return;
        }
        
        this.value = null;
        this.pullValue();
    }

    handleMouseDown () {
        this.spinnerEventQueue.push('mousedown');
        return true;
    }

    handleMouseUp () {
        this.spinnerEventQueue.push('mouseup');

        if (this.previousSpinnerEvent === 'scrollEnd') {
            // Must have finished scrolling, then lifted finger (possible on Edge).
            //  Set an answer.
            this.handleScrollEnd();
        }

        return true;
    }

    /**
     * Handle tap of an element.  Make that element the "selectedItem" and call pushValue() to push it into the value
     * property (and update the UI).
     * @param {Object} e event object, contains the target of our click.
     */
    handleTap (e) {
        let that = this;

        this.spinnerEventQueue.push('tap');

        // A tap when scrolling in a user initiated scroll should stop the scroller.  Fire the scrollend event.
        if (this.inScroll) {
            // False because we just tapped.
            this.handleScrollEnd();
            return;
        }

        // jscs:disable requireArrowFunctions
        $(e.target).closest('.item').each(function () {
            that.value = $(this).data('value');
            that.pullValue();

            // Do not keep iterating after finding one item node.
            return false;
        });
        // jscs:enable requireArrowFunctions
    }

    /**
     * clear our cache (jQuery selector) of items.
     * Set private value to null so getter will recalculate next time
     */
    clearItemCache () {
        this._$items = null;
    }

    destroy () {
        this.clearItemCache();
        this.undelegateEvents();
        this.scroller.destroy();
        this.$itemContainer = null;
        this.$win = null;
        this.scroller = null;
    }
}
