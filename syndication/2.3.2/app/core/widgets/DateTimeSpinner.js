import IntervalSpinnerBase from './IntervalSpinnerBase';

const DEFAULT_MODAL_TEMPLATE = 'DEFAULT:DateTimeSpinnerModal';
const DEFAULT_DISPLAY_FORMAT = 'LLL';
const DEFAULT_STORAGE_FORMAT = 'DD MMM YYYY HH:mm:ss';

/**
 * Variation of Interval Spinner for Date and Time.
 */
export default class DateTimeSpinner extends IntervalSpinnerBase {
    /*constructor (options) {
        const dateOptions = _.extend({}, LF.strings.dates({dates: {}}), LF.strings.dates({dateConfigs: {}}));
        super(options);
    }*/
    /**
     * default modal template
     * @returns {string}
     */
    get defaultModalTemplate () {
        return DEFAULT_MODAL_TEMPLATE;
    }

    get defaultDisplayFormat () {
        return DEFAULT_DISPLAY_FORMAT;
    }

    get defaultStorageFormat () {
        return DEFAULT_STORAGE_FORMAT;
    }

    /**
     * Verify all necessary containers exist.
     */
    validateUI () {
        if (this.yearIndex === null || this.monthIndex === null || this.dayIndex === null || this.hourIndex === null || this.minuteIndex === null) {
            throw new Error('Invalid template for DateTimeSpinner widget. Expected day + month + year + hour + minute containers');
        }
    }

    render () {
        let minDate = this.model.get('minDate'),
            maxDate = this.model.get('maxDate'),
            answer = _.last(this.answers.models);

        if(answer && (new Date(answer.get('response')) < new Date(minDate).getTime() || new Date(answer.get('response')) > new Date(maxDate).getTime())){
            this.removeAnswer(answer);
        }
        return super.render();
    }
}

window.LF.Widget.DateTimeSpinner = DateTimeSpinner;
