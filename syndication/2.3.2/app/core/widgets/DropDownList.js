import SelectWidgetBase from 'core/widgets/SelectWidgetBase';
const DEFAULT_ITEM_TEMPLATE = 'DEFAULT:SelectItemTemplateTrans';
const DEFAULT_INPUT_TEMPLATE = 'DEFAULT:SelectWidget';
const DEFAULT_CLASS_NAME = 'DropDownList';
const DEFAULT_RESULTS_CLASS_NAME = 'DropDownList-Results';
const DEFAULT_UNIFORM_OPTION_HEIGHT = true;

class DropDownList extends SelectWidgetBase {
    /**
     * default item template
     * @returns {string}
     */
    get defaultItemTemplate () {
        return DEFAULT_ITEM_TEMPLATE;
    }

    /**
     * default input template
     * @returns {string}
     */
    get input () {
        return DEFAULT_INPUT_TEMPLATE;
    }

    /**
     * default class name
     * @returns {string}
     */
    get defaultClassName () {
        return DEFAULT_CLASS_NAME;
    }

    get defaultResultsClassName () {
        return DEFAULT_RESULTS_CLASS_NAME;
    }

    /**
     * Constructs a DropDownList
     * @param {object} options The properties for constructing the widget.
     * @param {Object} options.model The model containing the specific configuration for the widget.  This is represented by the "widget" property of the question in the study design.
     * @param {Array<{text: string, value: string}>} options.items Array of objects with value and text keys setup as the
     * value of the item and the text for the item, respectively.  In the itemTemplate, you can use "text" for the
     * literal text or "localized" for translated text.
     * @param {string} [options.model.label=null] lookup (translated) for the label, no label if not defined.
     * @param {Object} [options.model.templates] templates
     * @param {Object} [options.model.templates.wrapper=DEFAULT:FormGroup] template for the wrapper
     * @param {Object} [options.model.templates.input=DEFAULT:Input] template for the input control (select)
     * @param {string} [options.model.itemTemplate=DEFAULT:SelectItemTemplateNoTrans] item template.  Use "text" for the
     * literal text or "localized" for translated text.
     * @param {string} [options.model.className=DropDownList] CSS class name
     * @param {string} [options.model.defaultVal=null] Default value to select, if none already selected.
     * @param {string} [options.model.configFunc=null] Name of function in LF.Widget.ParamFunctions namespace.  This function
     * will return a Promise containing either a new options array, or a new defaultVal (in keys with the same name).
     * @param {boolean} [options.model.uniformOptionHeight=true] whether or not to automatically set the uniform option
     * height.
     * @param {number} [options.model.visibleRows=null] Number of rows to be visible at one time when selecting.
     * Note, this property (when true) assumes that uniformOptionHeight is true, and will force it to be true
     * if it is undefined or false.
     * @param {string} options.model.id {dev-only} The ID associated with the widget.
     */
    constructor (options) {
        super(options);

        /**
         * Whether or not this control is valid and completed
         * @type {boolean}
         */
        this.completed = false;

        /**
         * Reference to answer for this control
         * @type {Answer}
         */
        this.answer = null;

        /**
         * overridden from base.  Object that specifies the value to display when no value is selected.  This item
         * goes away upon first selection.
         * @type {{value: string}}
         */
        this.needToRespond = {value: ''};

        /**
         * reference to select2
         * @type {jQuery}
         */
        this.$select2 = null;

        this.model.set('itemTemplate', this.model.get('itemTemplate') || this.defaultItemTemplate);

        this.model.set('className', this.model.get('className') || this.defaultClassName);

        this.model.set('resultsClassName', this.model.get('resultsClassName') || this.defaultResultsClassName);

        this.model.set('uniformOptionHeight',
            (
                typeof(this.model.get('uniformOptionHeight')) === 'boolean' ?
                    this.model.get('uniformOptionHeight') : DEFAULT_UNIFORM_OPTION_HEIGHT
            ) || typeof(this.model.get('visibleRows')) === 'number'
        );

        this.model.set('visibleRows',
            typeof(this.model.get('visibleRows')) === 'number' ?
                this.model.get('visibleRows') :
                null
        );
    }

    delegateEvents () {
        super.delegateEvents();
        this.addCustomEvents();
    }

    undelegateEvents () {
        this.removeCustomEvents();
        super.undelegateEvents();
    }

    addCustomEvents () {
        let id = `#${this.model.get('id')}`;
        if (this.$select2) {
            this.setHeights = _.bind(this.setHeights, this);
            this.$(id).on('select2:open', this.setHeights);
        }
    }

    removeCustomEvents () {
        let id = `#${this.model.get('id')}`;
        if (this.$select2) {
            this.$(id).off('select2:open', this.setHeights);
        }
    }

    /**
     * Render the options for this control
     * @param {string} divToAppendTo ID string for the div on which to append the control.
     * @returns {Promise<void>}
     */
    renderOptions (divToAppendTo) {
        return Q()
            .then(() => {
                let itemTemplate = this.model.get('itemTemplate'),
                    items = this.model.get('items'),
                    $targetDiv = this.$(divToAppendTo),
                    addItemPromiseFactories,
                    itemTemplateFactory;
                itemTemplateFactory = LF.templates.getTemplateFromKey(itemTemplate);

                $targetDiv.empty();
                addItemPromiseFactories = items.map((item) => {
                    return () => {
                        return LF.strings.display.call(LF.strings, item.text, () => null, { namespace: this.getQuestion().getQuestionnaire().id }).then((string) => {
                            item.localized = string;
                            let el = $(itemTemplateFactory(item));
                            $targetDiv.append(el);
                        });
                    };
                });
                return addItemPromiseFactories.reduce(Q.when, Q());
            });
    }

    /**
     * initialize the select2 control, register events, and choose the initially selected value.
     * @returns {Promise<void>}
     */
    initializeSelect2 () {
        let id = `#${this.model.get('id')}`,
        checkForRTLNumbers = (option) => {
            if ($('body').attr('dir') === 'rtl' && !option.reversed) {
                let original = option.text,
                    regEx = /(\d[\d\.]*)/g,
                    numbers = original.match(regEx),
                    indices,
                    newResult;

                if (numbers !== null) {
                    indices = [];
                    for (let i = 0; i < numbers.length; i++) {
                        indices.push(original.search(numbers[i]));
                    }

                    numbers.map((element) => {
                        return (element.length > 1) ? element.split('').reverse().join('') : element;
                    });

                    newResult = original.split('');

                    numbers.forEach((number, index, array) => {
                        newResult.splice(indices[indices.length - index - 1], array[indices.length - index - 1].length, number);
                    });

                    option.text = newResult.join('');
                    option.reversed = true;
                }
            }
            return option.text;
        };
        return new Q.Promise((resolve) => {
            $.when(this.$(id).select2({ minimumResultsForSearch: Infinity, width: '', templateResult: checkForRTLNumbers, templateSelection: checkForRTLNumbers }))
            .done(() => {
                this.$select2 = this.$(id);
                resolve();
            });
        })
        .then(() => {
            let answer;

            this.delegateEvents();

            if (!!(this.answer)) {
                answer = this.answer.get('response');
            } else if (this.model.get('defaultVal') !== null) {
                answer = this.model.get('defaultVal');
            } else if (this.needToRespond) {
                answer = this.needToRespond.value;
            }
            this.$select2.val(answer).trigger('change');
        });
    }

    /**
     * Set the heights of options to be the same, if necessary... and set height of option container to be the right
     * size for the configured number of items.
     * @returns {Q.Promise<void>}
     */
    setHeights () {
        let ret = Q.resolve(),
            uniformOptionHeight = this.model.get('uniformOptionHeight'),
            visibleRows = this.model.get('visibleRows'),
            that = this,
            maxOptionHeight = 0,
            $optionContainer = $(`#select2-${that.model.get('id')}-results`);

        // add our class name to the results container
        $optionContainer.addClass(this.model.get('resultsClassName'));

        if (uniformOptionHeight) {
            ret = ret.then(() => {
                return new Q.Promise((resolve) => {
                    let $options = $optionContainer.find('.select2-results__option');
                    //jscs:disable requireArrowFunctions
                    $options.each(function () {
                        maxOptionHeight = Math.max(maxOptionHeight, $(this).outerHeight());
                    });

                    $options.outerHeight(maxOptionHeight).ready(() => {
                        resolve();
                    });
                    //jscs:disable requireArrowFunctions
                });
            });
        }

        if (typeof visibleRows === 'number') {
            ret = ret.then(() => {
                return new Q.Promise((resolve) => {
                    //jscs:disable requireArrowFunctions
                    $optionContainer.each(function () {
                        $(this).css(
                            'maxHeight',
                            maxOptionHeight * Math.min(visibleRows, that.model.get('items').length)
                        );
                    }).ready(() => {
                        resolve();
                    });
                    //jscs:enable requireArrowFunctions
                });
            });
        }

        ret = ret.then(() => {
            return new Q.Promise((resolve) => {
                $optionContainer.trigger('ensureHighlightVisible');
                resolve();
            });
        });

        return ret;
    }

    /**
     * Respond to changes in the select control.
     * Updated in this subclass to just store the value
     * (not do any of the specialized stuff with fields, default values, etc.)
     * @param {Event} e The event arguments
     * @returns {Q.Promise}
     */
    respond (e) {
        let value = this.$(e.target).val();

        this.completed = value !== null;

        if (!this.completed) {
            this.removeAllAnswers();
            return Q.resolve();
        }

        if (!this.answers.size()) {
            this.answer = this.addAnswer();
        }

        return this.respondHelper(this.answer, value, this.completed);
    }
}

window.LF.Widget.DropDownList = DropDownList;
export default DropDownList;
