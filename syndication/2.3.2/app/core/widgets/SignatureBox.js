import WidgetBase from './WidgetBase';
import * as Utilities from 'core/utilities';
import Logger from 'core/Logger';
import { MessageRepo } from 'core/Notify';

let logger = new Logger('SignatureBox');

// Constant for the length of time for a press to be considered "long"
const longpress = 1000;

class SignatureBox extends WidgetBase {
    /**
     * Constructs the signature box widget.
     * Note that this documentation is for the actual widget used to capture a signature, not the entire affidavit form.
     * @param {Object} options The options used to render the widget.
     * @param {Object} options.model The model containing the specific widget configuration.
     * @param {Object} [options.model.templates] {dev-only} Contains any specific templates to be used when rendering the widget
     * @param {string} [options.model.templates.container] {dev-only} The template to be used when rendering the container element for the widget.
     * @param {Object} [options.model.configuration] {dev-only} Specific configuration settings for the widget (not currently implemented)
     */
    constructor (options) {
        /**
         * The template of the container
         * @type String
         * @default 'DEFAULT:SignatureBox'
         */
        this.container = 'DEFAULT:SignatureBox';

        /**
         * Ink Signature Data to be written in Dashboard
         * @type Object
         * @default {}
         */
        this.ink = {};

        /**
         * The Native Signature Data to be stored within the widget
         * @type Array
         * @default []
         */
        this.nativeSignatureData = [];

        /**
         * author's name who performs the signature.
         * @type String
         * @default ''
         */
        this.author = '---';

        /**
         * The data size limit for the ink signature in bytes
         * @type Number
         * @readonly
         * @default 4 * 1024
         */
        this.maxDataSize = 4 * 1024;

        /**
         * List of the widgets events.
         * @readonly
         * @enum {Event}
         */
        this.events = {
            /** signature box input change event */
            'change .signature-box': 'respond',
            'click #signature-clear': 'clear',
            'mousedown #signature-clear': 'clearMouseDown',
            'mouseup #signature-clear': 'clearMouseUp',
            'mouseleave #signature-clear': 'clearMouseLeave',
            'touchstart #signature-clear': 'clearMouseDown',
            'touchend #signature-clear': 'clearMouseUp',
            'remove': 'removeJSignature'
        };

        /**
         * Responds to clear event. Clear event fires when user clicks the clear signature button.
         * @param {Event} e Event data
         */
        this.clear = _.debounce((e) => {
            e.preventDefault();
            this.clearSignature();
        }, 300, true);

        this.options = options;
        super(options);

        //Initialize...
        this.config = this.model.get('configuration') || {};
        this.templates = this.model.get('templates') || {};
        this.container = this.templates.container || this.container;

        this.longpressTimer = 0;
    }

    /**
     * Handles the mouseleave event on the signature-clear button.
     */
    clearMouseLeave () {
        this.longpressTimer = 0;
    }

    /**
     *  Handles the mousedown event for the signature-clear button
     */
    clearMouseDown () {
        this.longpressTimer = new Date().getTime();
    }

    /**
     *  Handles the mousup event on the signature-clear button.
     */
    clearMouseUp () {
        if (new Date().getTime() >= (this.longpressTimer + longpress)) {
            this.clearSignature();
        }
    }

    /**
     *  Clears the ink signature control, and resets the answer information.
     */
    clearSignature () {
        this.$('.signature-box').jSignature('clear');
        this.nativeSignatureData = [];
        this.clearInkData(this.nativeSignatureData);
        this.createAnswer();
        this.respondHelper(this.answer, '', false);
    }

    /**
     * Constructs StudyWorks formatted signature
     * @param {Array} value A jSignature native signature array
     * @returns {String} a studyworks formatted signature string
     * @example this.buildStudyWorksString([{x: [81, 87], y: [63, 62]}, {x: [66, 69, 74], y: [94, 92, 92]}]);
     */
    // jscs:disable requireTemplateStrings
    buildStudyWorksString (value) {
        let canvas = $('.jSignature', this.$el),
            header = [value.length, canvas.width(), canvas.height()].join('.');

        return header + value.map((stroke) => {
            let i = 0;
            // eslint-disable-next-line prefer-template
            return 'S' + stroke.x.map((x) => {
                // eslint-disable-next-line prefer-template
                let val = '@' + [x, stroke.y[i]].join('.');
                i += 1;
                return val;
            }).join('');
        }).join('');
    }

    // jscs:enable requireTemplateStrings

    /**
     * Constructs jSignature formatted signature object
     * @param {String} value A studyworks signature string
     * @returns {Array} a jSignature formatted signature object
     * @example this.buildSignatureObject('604.302S@81.63@87.62@90.60@94.59S@66.94@69.92@74.92');
     */
    buildSignatureObject (value) {
        let body = value.split('S@');
        body.shift();

        return body.map((swStroke) => {
            let stroke = { x: [], y: [] };
            swStroke.split('@').forEach((point) => {
                let coordinate = point.split('.');
                stroke.x.push(parseInt(coordinate[0], 10));
                stroke.y.push(parseInt(coordinate[1], 10));
            });
            return stroke;
        });
    }

    /**
     * If the widget is used as an affidavit returns if the affidavit is answered
     * @return {Boolean} Returns true if the affidavit is answered
     * @example this.isAffidavitAnswered();
     */
    isAffidavitAnswered () {
        return (this.nativeSignatureData.length > 0);
    }

    /**
     * Responsible for displaying the widget.
     * @returns {Q.Promise<void>}
     */
    render () {
        let stringsToFetch = {
            clear: 'CLEAR_SIGNATURE'
        };

        return new Q.Promise((resolve) => {
            LF.getStrings(stringsToFetch, (strings) => {
                let widgetView = LF.Resources.Templates.display(this.container, {clear: strings.clear});
                let sizeRatio = (Utilities.isSitePad()) ? 10 : 4;

                this.$el.html(widgetView);
                this.$el.appendTo(this.parent.$el);

                // Adjust size here, maybe based on modality?
                this.$('.signature-box').jSignature({
                    'sizeRatio': sizeRatio,
                    'lineWidth': 1,
                    'background-color': '#DEDEDE',
                    'decor-color': '#DEDEDE'
                });

                if (!!this.nativeSignatureData && this.nativeSignatureData.length && this.completed) {
                    this.setSignaturePluginData(this.nativeSignatureData);
                }

                this.$el.trigger('create');
                this.delegateEvents();

                resolve();
            });
        })
        .then(() => {
            if (LF && LF.security && LF.security.activeUser) {
                return LF.security.activeUser.getSubject()
                .then(subject => {
                    if (subject) {
                        return subject.get('initials');
                    } else {
                        return LF.security.activeUser.get('username');
                    }
                });
            } else {
                logger.info('Invalid SignatureBox invocation. No active user session.');
                return null;
            }
        })
        .then((authorName) => {
            this.setAuthor(authorName);
        });
    }

    /**
     * Responds to user signature input event. The event fires when user takes his finger off the screen after signing.
     * @returns {Q.Promise<void>}
     */
    respond () {
        let value = this.getSignaturePluginData();

        if (!!value && value.length) {
            this.nativeSignatureData = value;
            this.writeInkData(this.nativeSignatureData);
            this.createAnswer();

            return this.respondHelper(this.answer, '', true);
        } else {
            return Q();
        }
    }

    /**
     * Override the base validation function to allow for operational logs
     * @returns {bool}
     */
    validate () {
        let isValid = super.validate();

        if (isValid) {
            logger.operational('Signature Capture: Success');
        }

        return isValid;
    }

    /**
     * Populates the json object for ink and writes the object to Dashboard
     * @param {Array} nativeSignatureData - The native widget's signature data.
     */
    writeInkData (nativeSignatureData) {
        let canvas = this.$('.jSignature');

        this.ink.xSize = canvas.width();
        this.ink.ySize = canvas.height();
        this.ink.signatureData = this.buildStudyWorksString(nativeSignatureData);
        this.ink.author = this.author;

        if (this.validateDataSize()) {
            // This is specific to patient questionnaires, so we need this workaround in case it is used in a non-patient questionnaire
            if (this.questionnaire.data) {
                this.questionnaire.data.ink = this.ink;
            }
        }
    }

    /**
     * Clears the ink object and clears the ink data in the Dashboard
     */
    clearInkData () {
        this.ink = {};
        if (this.questionnaire.data) {
            delete this.questionnaire.data.ink;
        }
    }

    /**
     * Validates the data size of the ink signature. Trims the signature data if validation fails.
     * @returns {boolean} Returns true if the data size do not exceed the data size limit. Returns false otherwise
     */
    validateDataSize () {
        let dataSize = Utilities.getByteLength(JSON.stringify(this.ink)),
            surplus = dataSize - this.maxDataSize;

        if (surplus > 0) {
            const { Dialog } = MessageRepo;

            logger.warn('Signature Capture: Failed - max size exceeded');

            MessageRepo.display(Dialog && Dialog.SIGNATURE_DATA_LIMIT_EXCEEDED)
            .then(() => {
                this.clearSignature();
            })
            .done();

            return false;
        }

        return true;
    }

    /**
     * Trims the signature data to fit the data size limit. Repopulates the widget with the trimmed data.
     * @param {Number} surplus The number of bytes that exceed the data size limit
     */
    trimSignature (surplus) {
        let trimmedStrokeSize,
            trimmed = this.ink.signatureData.slice(0, -1 * surplus),
            lastSIndex = trimmed.lastIndexOf('S'),
            lastAtIndex = trimmed.lastIndexOf('@'),
            firstDotIndex = trimmed.indexOf('.'),
            endIndex = lastAtIndex - 1 > lastSIndex ? lastAtIndex : lastSIndex;

        trimmed = trimmed.substring(0, endIndex);
        trimmedStrokeSize = (trimmed.match(/S@/g) || []).length;
        trimmed = trimmedStrokeSize + trimmed.substring(firstDotIndex, endIndex);

        this.nativeSignatureData = this.buildSignatureObject(trimmed);
        this.setSignaturePluginData(this.nativeSignatureData);
    }

    /**
     * Sets the data  the jSignature plugin
     * @param {Object} data The signature data in the native format of jSignature
     */
    setSignaturePluginData (data) {
        this.$('.signature-box').jSignature('setData', data, 'native');
    }

    /**
     *  Returns the native data in the jSignature plugin
     *  @returns {Object} The native data
     */
    getSignaturePluginData () {
        return this.$('.signature-box').jSignature('getData', 'native');
    }

    /**
     * Sets value for the member 'author'.
     * This function is provided for giving the flexibility to set the author name for the widget
     * @param {String} author Author's name
     */
    setAuthor (author) {
        this.author = author || 'unknown';
    }

    /**
     * Adds answer to the this.answers collection and assigns it to this.answer if this.answers is empty.
     */
    createAnswer () {
        if (!this.answers.size()) {
            this.answer = this.addAnswer();
        }
    }

    /**
     * Override remove to also get rid of canvas
     * @param {Event} e - The event args
     */
    removeJSignature (e) {
        logger.trace('Removing JSignature. ', e);
        this.$('.signature-box').jSignature('destroy');
    }
}

/**
 * The class of the root element.
 * @type String
 * @readonly
 * @default 'SignatureBox'
 */
SignatureBox.prototype.className = 'SignatureBox';

window.LF.Widget.SignatureBox = SignatureBox;
export default SignatureBox;
