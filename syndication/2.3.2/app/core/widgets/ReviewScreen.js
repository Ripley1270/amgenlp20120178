/** #depends Base.js
 * @file Defines the review screen widget
 * @author <a href="mailto:mkamthewala@phtcorp.com">Mansoor Kamthewala</a>
 * @version 1.7.0
 */

import WidgetBase from './WidgetBase';

class ReviewScreen extends WidgetBase {

    constructor (options) {

        /**
         * The template for the container
         * @type String
         * @default 'DEFAULT:ReviewScreen'
         */
        this.container = 'DEFAULT:ReviewScreen';

        /**
         * The template of the Review Screen Items
         * @type String
         * @default 'DEFAULT:ReviewScreenItems'
         */
        this.itemsTemplate = 'DEFAULT:ReviewScreenItems';

        /**
         * The template of the Review Screen Buttons
         * @type String
         * @default 'DEFAULT:ReviewScreenButtons'
         */
        this.buttonsTemplate = 'DEFAULT:ReviewScreenButtons';

        /**
         * List of the widgets events
         * @readonly
         * @enum {Event}
         */
        this.events = {

            /** click event for selecting Review Screen Item */
            'click .item': 'itemSelectedEvent',

            /** click event for for performing an action when a button is clicked */
            'click .button': 'action'
        };

        /**
         * Calls the configured function when an button is clicked.
         * Function must be available in LF.Widget.ReviewScreen namespace
         * @param {Event} e
         */
        this.action = _.debounce((e) => {
            this.handleAction(e)
                .done();
        }, 500, true);

        this.options = options;
        super(options);

        // Initialize...
        this.completed = true;
        this.reviewScreenList = [];
        this.buttons = [];
        this.activeItem = null;
        this.selectable = false;

        // determine the templates for container, items and buttons
        this.templates = this.model.get('templates') || {};
        this.container = this.templates.container || this.container;
        this.reviewScreenItems = this.templates.itemsTemplate || this.itemsTemplate;
        this.reviewScreenButtons = this.templates.buttonsTemplate || this.buttonsTemplate;

    }

    /**
     * Calls the configured function when an button is clicked.
     * Function must be available in LF.Widget.ReviewScreen namespace
     * @param {Event} e The event args.
     * @returns {Q.Promise<void>}
     */
    handleAction (e) {
        let id = this.$(e.currentTarget).data('id'),
            selectedBtn = _.find(this.model.get('buttons'), (btn) => btn.id.toString() === id.toString()),
            func = selectedBtn.actionFunction;

        return Q()
            .then(() => {
                if (func) {
                    // This will/should return a Promise
                    return LF.Widget.ReviewScreen[func]({
                        question: this.parent,
                        button: selectedBtn,
                        item: this.activeItem
                    });
                } else {
                    throw new Error('Button Function not found in the widget Configuration.');
                }

            });
    }

    /**
     *  Sets up the configured screenFunction property for the widget.
     *  @param {Function} screenFunc The screenFunction to be called
     *  @returns {Q.Promise<array>} Resolved Promise with an array of items to display.
     */
    processScreenFunction (screenFunc) {
        let that = this;

        return Q()
            .then(() => screenFunc.call(that))
            .then((items) => {
                if (items !== null) {
                    _(items).each((item) => {
                        that.reviewScreenList.push(item);
                        that.$(`#${that.model.get('id')}_Items`).append(that.addItem(item));
                    });
                }
            });
    }

    /**
     * Responsible for displaying the widget.
     * @returns {Q.Promise} resolved when the element has been rendered.
     */
    render () {
        // ID was originally being set to 'reviewScreen' rather the ID in the model.  Changed to allow the unit tests to pass --bmj
        let widgetView = LF.templates.display(this.container, {id: this.model.get('id')}),
            screenFunc = LF.Widget.ReviewScreen[this.model.get('screenFunction')] || (() => null);

        return Q()
            .then(() => {

                this.$el.empty();
                this.$el.append(widgetView);
            })
            .then(() => {
                // passing empty array as nothing to exclude since no item is selected at this point.
                return this.addDefaultButtons([]);
            })
            .then(() => {
                this.$el.appendTo(this.parent.$el);
                this.$el.trigger('create');
            })
            .then(() => this.processScreenFunction(screenFunc))
            .then(() => {
                this.delegateEvents();
            });

    }

    /**
     * Add all buttons which has availability property.
     * @param {Array} exclude An array with Button Ids that should not be rendered.
     * @example this.addDefaultButtons([3, 4]);
     * @returns {Q.Promise<void>}
     */
    addDefaultButtons (exclude) {

        let _this = this,
            allBtns = this.model.get('buttons') || [],
            btnAddPromiseFactories = [];

        // create and append default buttons
        _(allBtns).each((btn) => {
            if (btn.availability === 'always' && _.indexOf(exclude, btn.id) === -1) {
                _this.selectable = true;
                btnAddPromiseFactories.push(() => {
                    return _this.addBtn(btn);
                });

            // } else if (_.isFunction(_this[btn.availability]) && _.indexOf(exclude, btn.id) === -1) {
            } else if (LF.Widget.ReviewScreen[btn.availability] && _.indexOf(exclude, btn.id) === -1) {
                btnAddPromiseFactories.push(() => {
                    return new Q.Promise((resolve) => {
                        LF.Widget.ReviewScreen[btn.availability](_this)
                            .then((result) => {
                                if (result) {
                                    _this.selectable = true;
                                    _this.addBtn(btn).then(resolve).done();
                                } else {
                                    resolve();
                                }
                            });
                    });
                });
            }
        });

        // Run these promises in order.
        return btnAddPromiseFactories.reduce(Q.when, Q());
    }

    /**
     * Remove all buttons which are not default buttons.
     * @example this.removeBtns();
     */
    removeBtns () {

        this.buttons = _.filter(this.buttons, (btn) => {
            btn.remove();
            return false;
        });

    }

    /**
     * Add a single button to the Review Screen.
     * @param {Object} btn A single button object configured for the widget.
     * @example this.addBtn({
     *          "id"            : 2,
     *          "text"          : "ADD_NEW",
     *          "actionFunction": "addNew"
     *      }, false);
     * @returns {Q.Promise<void>} Promise that is resolved when the button is added.
     */
    addBtn (btn) {
        let that = this;
        return new Q.Promise((resolve) => {
            LF.getStrings(btn.text, _((string) => {
                let newBtn = $(LF.templates.display(this.reviewScreenButtons, {text: string}))
                    .addClass('button')
                    .attr('data-id', btn.id);

                that.buttons.push(newBtn);
                that.$(`#${that.model.get('id')}_Buttons`).append(newBtn);
                that.$el.trigger('create');
                // Disable fat arrow check since this is a jQuery object and the context of this is important.
                // jscs:disable requireArrowFunctions
                that.$el.ready(function () {
                    resolve();
                });
                // jscs:enable requireArrowFunctions
            }).bind(this), {namespace: this.getQuestion().getQuestionnaire().id});
        });
    }

    /**
     * Add an Item to the Review Screen.
     * @param {Object} item A single item object to be displayed.
     * @return {Q.Promise<object>} Returns a resolved Promise with JQuery Mobile object which needs to be appended
     * @example this.addItem({
     *      id      : '2',
     *      text    : ['Aspirin', '300mg', 'Daily'],
     *      exclude : [3]
     *  });
     */
    addItem (item) {

        let strings = {};

        if (_.isArray(item.text)) {
            // create strings object which needs to be passed to the template
            _(item.text).each((str, index) => {
                strings[`text${index}`] = str;
            });
        } else {
            throw new Error('Text for the Review Screen Item is not configured in an Array.');
        }

        return $(LF.templates.display(this.reviewScreenItems, strings))
            .addClass('item')
            .attr('data-id', item.id);

    }

    /**
     * Selects an item on the screen and sets it as an active item.
     * Displays all buttons configured for the item.
     * @param {Event} e The event args
     * @returns {Q.Promise<jQuery>} Promise resolving with containing reference to jQuery container for $el
     */
    itemSelected (e) {

        let targetItem = this.$(e.currentTarget),
            id = targetItem.data('id'),
            item = _.find(this.reviewScreenList, (listItem) => {
                return (listItem.id ? listItem.id.toString() : null) === (id != null ? id.toString() : null);
            }),
            allBtns = this.model.get('buttons') || [],
            exclude = item.exclude || [],
            retPromise;

        // remove all buttons.  correct ones (with proper exclusions) will be added later
        this.removeBtns();

        if (targetItem.hasClass('selected')) {
            // deselect an item
            targetItem.removeClass('selected');
            this.selectable = false;
            this.activeItem = null;
            // deseleting, so no buttons needs to be excluded
            retPromise = this.addDefaultButtons([]);
        } else {
            // deselect any previously selected item
            if (this.activeItem) {
                this.activeItem.removeClass('selected');
            }
            // set new active item
            this.activeItem = targetItem;

            // add custom button (null/undefined/false availability) for newly selected item
            let btnAddPromiseFactories = [];
            _(allBtns).each(_((button) => {
                if (!button.availability && _.indexOf(exclude, button.id) === -1) {
                    if (this.selectable === false) {
                        this.selectable = true;
                    }
                    btnAddPromiseFactories.push(() => {
                        return this.addBtn(button);
                    });
                }
            }).bind(this));
            retPromise = this.addDefaultButtons(exclude)
                .then(() => {
                    return btnAddPromiseFactories.reduce(Q.when, Q());
                })
                .then(() => {
                    // selects an item
                    if (this.selectable) {
                        targetItem.addClass('selected');
                    }
                });
        }

        return retPromise.then(() => {
            return this.$el.trigger('create');

        });
    }

    /**
     * Event wrapper for itemSelected.  Since this comes from a click event, nothing listens
     * to our promise, so mark it "done"
     * @param {Event} e The event args
     * @returns {Q.Promise<jQuery>} Finished Promise resolving with containing reference to jQuery container for $el
     */
    itemSelectedEvent (e) {
        return this.itemSelected(e).done();
    }

}

/**
 * The default class of review screen widget.
 * @readonly
 * @type String
 * @default 'reviewScreen'
 */
ReviewScreen.prototype.className = 'reviewScreen';

/**
 * The root element of the widget.
 * @readonly
 * @type String
 * @default 'div'
 */
ReviewScreen.prototype.tagName = 'div';

window.LF.Widget.ReviewScreen = ReviewScreen;
export default ReviewScreen;
