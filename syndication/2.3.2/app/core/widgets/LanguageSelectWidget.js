import SelectWidgetBase from './SelectWidgetBase';

const INPUT = 'DEFAULT:SelectWidget';

export default class LanguageSelectWidget extends SelectWidgetBase {

    get input () {
        return INPUT;
    }

    /**
     * Extension of SelectWidgetBase for a particular instance of a Select Widget control.
     * @param {String} divToAppendTo the select ID that the options should be appended to
     */

    renderOptions (divToAppendTo) {
        //@todo change languages to be only those configured for a role. Part of US6150
        let languages = LF.strings.getLanguages(),
            answer = (this.answer) ? JSON.parse(this.answer.get('response'))[this.model.get('field')] : false,
            template = _.template('<option id="{{ language }}-{{ locale }}" value="{{ language }}-{{ locale }}">{{ localized }}</option>');
        $(divToAppendTo).empty();
        this.needToRespond = {value: `${languages[0].language}-${languages[0].locale}`};
        let addItemPromiseFactories = languages.map((resource) => {
            return () => {
                resource.localized = (`${resource.language}-${resource.locale}`).toLocaleUpperCase();
                return LF.strings.display.call(LF.strings, resource.localized)
                    .then((string) => {
                        resource.localized = string;
                        let el = template(resource);

                        if ((answer && answer === (`${resource.language}-${resource.locale}`)) || (this.$(divToAppendTo).children().size() === 0)) {
                            el = $(el).attr('selected', 'selected');
                        }
                        this.$(divToAppendTo).append(el);
                    });
            };
        });
        return addItemPromiseFactories.reduce(Q.when, Q());
    }

    /**
     * @param {String} roleID the id of the role that has been set
     */
    refreshLanguages (roleID) {
        // eslint-disable-next-line
        let role = _.find(LF.StudyDesign.roles.models, (role)=> role.id === roleID);
        //with implementation of US6150 this will need to call renderOptions('#' + this.model.id) and will need to remove the answer.
        return;
    }
}

window.LF.Widget.LanguageSelectWidget = LanguageSelectWidget;
