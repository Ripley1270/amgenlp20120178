import WidgetBase from './WidgetBase';

export default class RadioButtonMatrix extends WidgetBase {

    get defaultModel () {
        return {
            id: 0,
            type: 'RadioButton',
            templates: {},
            label: '',
            answer: 0,
            answers: []
        };
    }

    constructor (options) {
        this.options = options;
        super(options);

        // Set up the widget model here.
    }

    render () {
        // For each question:
        // Get the question text

        // Create the widget (create the object and call render())

        // Resolve our promise
    }

    respond () {}

    generateModel () {

    }
}

window.LF.Widget.RadioButtonMatrix = RadioButtonMatrix;
