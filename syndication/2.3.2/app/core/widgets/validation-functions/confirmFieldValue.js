/**
 * Widget Validation Function for a Confirmation Textbox Widget
 * @param {Object} answer the answer object from the widget
 * @param {Object} params the validation object form the widget model
 */

import { Banner } from 'core/Notify';

/**
 * Confirms a field value.
 * @param {Object} answer The answer object
 * @param {Object} params The params
 * @param {Function} callback The callback to be executed
 */
export function confirmFieldValue (answer, params, callback) {
    let response = JSON.parse(answer.get('response')) ,
        value = _(response).values()[0] ,
        key = response.fieldToConfirm;
    if (value === $(`#${answer.attributes.questionnaire_id}`).find(`#${key}`).val()) {
        callback(true);
    } else {
        let errorString = params ? params.errorString : 'ERROR';
        LF.getStrings(errorString,  (string) => {
            Banner.show({
                text    : string,
                type    : 'error'
            });
        });
        $(`#${answer.attributes.question_id}`).find('input').addClass('ui-state-error-border');

        callback(false);
    }
}

LF.Widget.ValidationFunctions.confirmFieldValue = confirmFieldValue;

export default confirmFieldValue;
