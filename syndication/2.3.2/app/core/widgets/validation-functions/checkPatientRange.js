/**
 *  Checks the given patient ID against the configured range.
 *  @param {string} patientID - The patient ID to check
 *  @returns {boolean} true if given ID is in the configured range.
 */
export function checkPatientRange (patientID) {
    let step = LF.StudyDesign.participantSettings.participantID.steps,
                minValue = LF.StudyDesign.participantSettings.participantID.seed,
                maxValue = LF.StudyDesign.participantSettings.participantID.max,
                format = LF.StudyDesign.participantSettings.participantNumberPortion,
                isInRange,
                subjNum;

    if (patientID == null) {
        return false;
    }

    subjNum = parseInt(patientID.substring(format[0], format[1]), 10);

    isInRange = ((subjNum - minValue) % step) === 0 && minValue <= subjNum && subjNum <= maxValue;
    return isInRange;
}

LF.Widget.ValidationFunctions.checkPatientRange = checkPatientRange;

export default checkPatientRange;