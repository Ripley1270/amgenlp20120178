/**
 * Widget Validation Function for a Confirmation Textbox Widget
 * @param {Object} answer the answer object from the widget
 * @param {Object} params the validation object form the widget model
 * @param {Function} callback The callback function to called upon completion.
 */
import NotifyView from 'core/views/NotifyView';

/**
 * @param {Object} answer The answer
 * @param {Object} params The params
 * @param {Function} callback The function to execute as the callback
 */
export function checkPatientID (answer, params, callback) {
    let response = answer.get('response'),
        namespace = this.questionnaire.id,
        errorPopup = function (errorString) {
            let notifyStrings = {};

            notifyStrings.header = {
                key: errorString.header || 'VALIDATION_HEADER',
                namespace: namespace
            };

            notifyStrings.body = {
                key: errorString.errString,
                namespace: namespace
            };

            notifyStrings.ok = {
                key: errorString.ok || 'VALIDATION_OK',
                namespace: namespace
            };

            LF.getStrings(notifyStrings).then((strings) => {
                (new NotifyView()).show(strings);
            });

            $(`#${answer.attributes.question_id}`).find('input').addClass('ui-state-error-border');
        };

    LF.Collection.Subjects.fetchCollection()
        .then((subjects) => {
            let subjectExists = subjects.find((user) => user.get('subject_id') === response),
                isInRange = LF.Widget.ValidationFunctions.checkPatientRange(response);
            if (!isInRange) {
                errorPopup(params.errorStrings.isInRange);
                callback(isInRange);
            } else {
                subjectExists && errorPopup(params.errorStrings.isUnique);
                callback(!subjectExists);
            }

        });
}

LF.Widget.ValidationFunctions.checkPatientID = checkPatientID;

export default checkPatientID;
