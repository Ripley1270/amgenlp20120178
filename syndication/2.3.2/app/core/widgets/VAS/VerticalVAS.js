
import BaseVas from './BaseVas';
import SelectedValue from './CommonProperties/SelectedValue';

export default class VerticalVAS extends BaseVas {

    get events () {
        return {
            /** mouse down event */
            'mousedown canvas': 'mouseyDownHandler',
            /** mouse up event */
            'mouseup canvas': 'mouseUpHandler',
            /** mouse move event */
            'mousemove canvas': 'mouseMoveHandler',
            
            'touchmove canvas': 'mouseTouchHandler',
            'touchstart canvas': 'mouseTouchHandler'
        };
    }

    get defaultVasLineHeight () {
        let otherItemsHeight = this.anchors.height + (this.cursorHeight / 2),
            remaining_space = this.getVasCanvasHeight() - otherItemsHeight;

        return remaining_space;
    }

    get defaultVasLineWidth () {
        return 6;
    }

    get defaultStrikeMarkWidth () {
        return this.anchors.width * 0.9;
    }

    get defaultStrikeMarkHeight () {
        return 7;
    }

    get defaultPointerWidth () {
        if (this.selectedValue.location !== 'static') {
            // set the pointer font to the customized font from the config (if specified)
            this.ctxVas.font = this.fontString(this.selectedValue.font);
            return this.ctxVas.measureText('99999').width;
        } else {
            // set the pointer font to the customized font from the config (if specified)
            this.ctxVas.font = this.fontString(SelectedValue.defaultSelectedAnswerFont);
            return this.ctxVas.measureText('99999').width;
        }
    }

    get defaultPointerHeight () {

    }

    get defaultAnchorHeight () {
        return 6;
    }

    get defaultAnchorWidth () {
        return 37;
    }

    get defaultPointerLocation () {
        return 'left';
    }

    constructor (options) {
        super(options);

        let pAnchorTextHeight,
            pTickMarkHeight,
            pAnswerTextHeight;

        this.textDirection = LF.strings.getLanguageDirection();
        this.defaultTextDirection = 'ltr';

        this.mouseDown = false;

        Object.defineProperty(this, 'anchorTextHeight', {
            get: () => {
                if (pAnchorTextHeight === undefined) {
                    // eslint-disable-next-line new-cap
                    pAnchorTextHeight = this.MeasureText(this.minAnchorText,
                        false,
                        this.anchors.font.name,
                        this.anchors.font.size).height;
                }

                return pAnchorTextHeight;
            }
        });

        Object.defineProperty(this, 'tickSize', {
            get: () => {
                if (pTickMarkHeight === undefined) {
                    pTickMarkHeight = ((this.vasLineEnd - this.vasLineStart) / (this.anchors.max.value - this.anchors.min.value));
                }

                return pTickMarkHeight;
            }
        });

        Object.defineProperty(this, 'answerTextHeight', {
            get: () => {
                if (pAnswerTextHeight === undefined) {
                    // eslint-disable-next-line new-cap
                    pAnswerTextHeight = this.MeasureText('999', false, this.selectedValue.font.name, this.selectedValue.font.size).height;
                }

                return pAnswerTextHeight;
            }
        });

    }

    get vasLineStart () {
        let rslt = (this.anchors.height / 2) + (this.cursorHeight / 2);

        return Math.floor(rslt);
    }

    get vasLineEnd () {
        return this.vasLine.height;
    }

    /**
     * Gets the height of the entire vas canvas.  This includes removing padding information
     * @returns {number}
     */
    getVasCanvasHeight () {
        return this.$('.vvas-canvas-container').height();
    }

    /**
     * Gets the height in pixels of the arrow cursor
     * @returns {*}
     */
    get cursorHeight () {
        let val;

        if (this.answer === undefined) {
            val = '0';
        } else {
            val = this.answer.get('response');
        }

        let ht = this.selectedValue.location === 'static' ?
                // eslint-disable-next-line new-cap
                this.MeasureText(val, false, SelectedValue.defaultSelectedAnswerFont.name, SelectedValue.defaultSelectedAnswerFont.size).height :
                // eslint-disable-next-line new-cap
                this.MeasureText(val, false, this.selectedValue.font.name, this.selectedValue.font.size).height,
            padding = ht * 0.15;

        return ht + padding;
    }

    saveAnswer (responseValue, canvasYCoordinate) {
        super.saveAnswer(responseValue);

        if (canvasYCoordinate !== undefined) {
            this.answer.set('canvasYCoord', canvasYCoordinate);
        }
    }

    setup () {
        this.$el.empty();

        let topAnchorText = this.minAnchorText,
            bottomAnchorText = this.maxAnchorText;

        if (this.anchors.swapMinMaxLocation === true) {
            topAnchorText = this.maxAnchorText;
            bottomAnchorText = this.minAnchorText;
        }

        let container = this.renderTemplate('VVAS', {
            id: this.model.get('id'),
            top: topAnchorText,
            bottom: bottomAnchorText
        });

        return Q().then(() => {
            return Q.Promise((resolve) => {
                this.$el.append(container);
                this.$el.appendTo(this.parent.$el);
                this.$el.ready(() => {
                    resolve();
                });
            });
        }).then(() => {
            return Q.Promise((resolve) => {
                this.$topText = this.$el.find('#topAnchor');
                this.$bottomText = this.$el.find('#bottomAnchor');
                this.vasCanvas = this.$el.find(`#${this.model.get('id')}`)[0];
                this.vasCanvas.width = $('.widget').width();
                this.ctxVas = this.vasCanvas.getContext('2d');

                // Make sure the canvas has been sized and is ready to go.
                this.$el.ready(() => {
                    resolve();
                });
            });
        });
    }

    /**
     * Override of render() to unhide the canvas after it is finished.
     * @returns {Q.Promise<void>} promise resolving after render is finished
     */
    render () {
        return super.render().then(() => {
            return this.revealCanvas();
        });
    }

    /**
     * Method to disable invisibility of canvas.
     * Deferred until after the create event to address defect
     * DE17680
     * @returns {Q.Promise<void>} promise resolving after canvas is revealed
     */
    revealCanvas () {
        return Q.delay(0).then(() => {
            $(this.vasCanvas).css('visibility', 'visible');
        });
    }

    /**
     * Responsible for displaying the widget.
     * @returns {Q.Promise} resolved when element is finished being rendered
     */
    drawTheVas () {
        return this.translateStrings()
            .then(() => {
                return this.setup();
            }).then(() => {
                this.renderMinAndMaxAnchors();
                this.renderScale();
                this.renderMinMaxValues();
                this.renderThePointerAndStrikeOnLoad();

                if (this.answer !== undefined) {
                    this.renderAnswerValue(this.answer.get('canvasYCoord'));
                }

            });
    }

    setCanvasDimensions () {
        return new Q.Promise((resolve) => {
            $(this.vasCanvas).width($('.widget').width());
            this.vasCanvas.width = $('.widget').width();

            resolve(true);
        });
    }

    /**
     * This method renders a scale containing top and bottom anchors
     * @example this.renderScale();
     */
    renderScale () {
        this.ctxVas.strokeStyle = '#000000';
        this.vasCanvas.height = this.getVasCanvasHeight();

        // top anchor line
        this.ctxVas.beginPath();
        this.ctxVas.moveTo(this.centerX - (this.anchors.width / 2), this.vasLineStart);
        this.ctxVas.lineTo(this.centerX + (this.anchors.width / 2), this.vasLineStart);
        //noinspection JSSuspiciousNameCombination
        this.ctxVas.lineWidth = this.anchors.height;
        this.ctxVas.stroke();
        this.ctxVas.closePath();

        // bottom anchor line
        this.ctxVas.beginPath();
        this.ctxVas.moveTo(this.centerX - (this.anchors.width / 2), this.vasLineEnd);
        this.ctxVas.lineTo(this.centerX + (this.anchors.width / 2), this.vasLineEnd);
        //noinspection JSSuspiciousNameCombination
        this.ctxVas.lineWidth = this.anchors.height;
        this.ctxVas.stroke();
        this.ctxVas.closePath();

        // vertical vas line itself
        this.ctxVas.beginPath();
        this.ctxVas.moveTo(this.centerX, this.vasLineStart);
        this.ctxVas.lineTo(this.centerX, this.vasLineEnd);
        this.ctxVas.lineWidth = this.vasLine.width;
        this.ctxVas.stroke();
        this.ctxVas.closePath();
    }

    renderMinMaxValues () {
        let topValString = this.anchors.min.value,
            bottomValString = this.anchors.max.value;

        if (this.anchors.swapMinMaxLocation === true) {
            topValString = this.anchors.max.value;
            bottomValString = this.anchors.min.value;
        }

        this.ctxVas.beginPath();

        if (this.pointer.location === 'right') {
            this.ctxVas.textAlign = 'end';
            this.ctxVas.textBaseline = 'middle';

            this.ctxVas.font = `${this.anchors.font.size}px ${this.anchors.font.name}`;

            this.ctxVas.fillText(bottomValString,
                this.centerX - (this.anchors.width / 2) - 10,
                this.vasLineEnd);

            this.ctxVas.font = `${this.anchors.font.size}px ${this.anchors.font.name}`;

            this.ctxVas.fillText(topValString,
                this.centerX - (this.anchors.width / 2) - 10,
                this.vasLineStart);
        } else {
            this.ctxVas.textAlign = 'start';
            this.ctxVas.textBaseline = 'middle';

            this.ctxVas.font = `${this.anchors.font.size}px ${this.anchors.font.name}`;

            this.ctxVas.fillText(topValString,
                this.centerX + (this.anchors.width / 2) + 10,
                this.vasLineStart);

            this.ctxVas.font = `${this.anchors.font.size}px ${this.anchors.font.name}`;

            this.ctxVas.fillText(bottomValString,
                this.centerX + (this.anchors.width / 2) + 10,
                this.vasLineEnd);
        }

        this.ctxVas.closePath();
    }

    /**
     * Handles the styling of the top and bottom anchor text elements, and renders
     * the min and max values.
     */
    renderMinAndMaxAnchors () {

        this.$topText.css('font-family', this.anchors.font.name);
        this.$topText.css('font-size', `${this.anchors.font.size}px`);
        this.$topText.css('text-align', 'center');

        this.$bottomText.css('font-family', this.anchors.font.name);
        this.$bottomText.css('font-size', `${this.anchors.font.size}px`);
        this.$bottomText.css('text-align', 'center');

    }

    /**
     * Renders the pointer when the screen loads if necessary
     */
    renderThePointerAndStrikeOnLoad () {

        // if the answer is already defined, we always render the strike mark and the pointer...
        // ONLY worry about rendering the pointer on display if the answer has not yet been defined.
        if (this.answer !== undefined) {
            this.renderTheStrikeMark();
            this.renderThePointer();
        } else {
            // the answer has not yet been defined...check what the config says to do about rendering the POINTER on load
            if (this.pointer.displayInitially) {
                this.renderThePointer();
            }

            if (this.initialCursorDisplay) {
                this.renderTheStrikeMark();
            }
        }
    }

    /**
     * gets the coordinates required to render the pointer that 'points' to the selected value
     * @returns {{pointerStart: number, endOfPointerTip: number, pointerWidth: number}}
     */
    get pointerOffsets () {
        let pointerStartXOffset = this.centerX - 5 - (this.anchors.width / 2),
            pointerTipOffset = this.centerX - 5 - (this.anchors.width / 2) - this.pointer.width / 3,
            pointerWidthOffset = this.centerX - 5 - (this.anchors.width / 2) - (this.pointer.width) - 10;

        if (this.pointer.location === 'right') {
            pointerStartXOffset = this.centerX + 5 + (this.anchors.width / 2);
            pointerTipOffset = this.centerX + 5 + (this.anchors.width / 2) + this.pointer.width / 3;
            pointerWidthOffset = this.centerX + 5 + (this.anchors.width / 2) + (this.pointer.width) + 10;
        }

        return {
            pointerStart: pointerStartXOffset,
            endOfPointerTip: pointerTipOffset,
            pointerWidth: pointerWidthOffset
        };
    }

    /**
     * Gets the vertical center of the widget
     * @returns {number}
     */
    get centerY () {
        return this.vasLineStart + ((this.vasLineEnd - this.vasLineStart) / 2);
        // return $(this.vasCanvas).height() / 2;
    }

    /**
     * Renders the pointer (arrow pointer) on the screen based on the passed in Y Coordinate
     */
    renderThePointer () {

        let yCoord,
            pointerCoords = this.pointerOffsets,
            rightLeftOffset = 1;

        // if the pointer is not supposed to be rendered based on the configuration, then just return a resolved promise
        if (!this.pointer.isVisible) {
            return;
        }

        // if the user selects 0 or 100, 'snap' the strike to the top or bottom anchor respectively
        if (this.answer !== undefined) {
            if (this.answer.get('response') === this.anchors.min.value.toString()) {
                if (this.anchors.swapMinMaxLocation === true) {
                    yCoord = this.vasLineEnd;
                } else {
                    yCoord = this.vasLineStart;
                }
            } else if (this.answer.get('response') === this.anchors.max.value.toString()) {
                if (this.anchors.swapMinMaxLocation === true) {
                    yCoord = this.vasLineStart;
                } else {
                    yCoord = this.vasLineEnd;
                }
            } else {
                yCoord = this.answer.get('canvasYCoord');
            }
        } else {
            yCoord = this.centerY;
        }

        if (this.pointer.location === 'right') {
            rightLeftOffset = -1;
        }

        // draws the blue pointer
        this.ctxVas.beginPath();
        this.ctxVas.strokeStyle = '#000';
        this.ctxVas.lineWidth = 1;
        this.ctxVas.fillStyle = this.pointer.color;
        this.ctxVas.moveTo(pointerCoords.pointerStart, yCoord);
        this.ctxVas.lineTo(pointerCoords.endOfPointerTip, yCoord - (this.cursorHeight / 2));
        this.ctxVas.lineTo(pointerCoords.pointerStart - this.pointer.width * (rightLeftOffset), yCoord - (this.cursorHeight / 2));
        this.ctxVas.lineTo(pointerCoords.pointerStart - this.pointer.width * (rightLeftOffset), yCoord + (this.cursorHeight / 2));
        this.ctxVas.lineTo(pointerCoords.endOfPointerTip, yCoord + (this.cursorHeight / 2));
        this.ctxVas.lineTo(pointerCoords.pointerStart, yCoord);
        this.ctxVas.fill();
        this.ctxVas.stroke();
        this.ctxVas.closePath();
    }

    renderTheStrikeMark () {
        let yCoord;

        // if the user selects 0 or 100, 'snap' the strike to the top or bottom anchor respectively
        if (this.answer !== undefined) {
            if (this.answer.get('response') === this.anchors.min.value.toString()) {
                if (this.anchors.swapMinMaxLocation === true) {
                    yCoord = this.vasLineEnd;
                } else {
                    yCoord = this.vasLineStart;
                }
            } else if (this.answer.get('response') === this.anchors.max.value.toString()) {
                if (this.anchors.swapMinMaxLocation === true) {
                    yCoord = this.vasLineStart;
                } else {
                    yCoord = this.vasLineEnd;
                }
            } else {
                yCoord = this.answer.get('canvasYCoord');
            }
        } else {
            yCoord = this.centerY;
        }

        this.ctxVas.beginPath();
        this.ctxVas.fillStyle = '#000';
        this.ctxVas.fillRect(this.centerX - (this.anchors.width / 2), yCoord - (this.anchors.height / 2),
            this.anchors.width, this.anchors.height);
        this.ctxVas.closePath();

        this.ctxVas.beginPath();
        this.ctxVas.fillStyle = '#fff';
        this.ctxVas.fillRect(this.centerX - (this.anchors.width / 2), yCoord - (this.anchors.height / 4),
            this.anchors.width, this.anchors.height / 2);
        this.ctxVas.closePath();
        //
        //
        //
        // this.ctxVas.beginPath();
        // this.ctxVas.strokeStyle = '#fff';
        // this.ctxVas.lineWidth = this.anchors.height / 2;
        // this.ctxVas.moveTo(this.centerX - (this.anchors.width / 2), yCoord);
        // this.ctxVas.lineTo(this.centerX + (this.anchors.width / 2), yCoord);
        // this.ctxVas.stroke();
        // this.ctxVas.closePath();
    }

    isWithinTouchableArea (xCoord) {
        let pointerWidth = 0;

        if (this.pointer.width !== undefined) {
            pointerWidth = this.pointer.width;
        }

        if ((xCoord < (this.centerX - (this.anchors.width / 2) - pointerWidth)) ||
            (xCoord > (this.centerX + (this.anchors.width / 2) + pointerWidth))) {
            return false;
        }

        return true;
    }

    /**
     * handles the mouse down event for the VVAS to save the answer and
     * force a redraw of the pointer and selected value
     * @param {Event} e - The mouse event args
     */
    mouseyDownHandler (e) {
        this.mouseDown = true;

        let canvasOffset = $(this.vasCanvas).offset();

        let canvasY = this.getPageY(e) - canvasOffset.top;

        if (canvasY < this.vasLineStart) {
            canvasY = this.vasLineStart;
        }

        if (canvasY > this.vasLineEnd) {
            canvasY = this.vasLineEnd;
        }

        //Disable the default browser behaviour
        e.preventDefault();

        const respondValue = this.getVasNumericValue(canvasY);
        this.saveAnswer(respondValue, canvasY);
        this.redrawTheVas(canvasY, true);
    }

    /**
     * If the user drags the slider, update the answer and move the pointer as they're sliding
     * @param {Event} e - The event
     * @param {boolean} [mouseDown=this.mouseDown] whether or not the mouse is down.  Can be overridden, but defaults to the state of the object.
     * @returns {boolean}
     */
    mouseMoveHandler (e, mouseDown = this.mouseDown) {
        let canvasY = this.getPageY(e) - $(this.vasCanvas).offset().top;

        //Disable the default browser behaviour
        e.preventDefault();

        if (!mouseDown) {
            return false;
        }

        if (canvasY < this.vasLineStart) {
            canvasY = this.vasLineStart;
        }

        if (canvasY > this.vasLineEnd) {
            canvasY = this.vasLineEnd;
        }

        const respondValue = this.getVasNumericValue(canvasY);
        this.saveAnswer(respondValue, canvasY);

        this.redrawTheVas(canvasY, true);

        return true;
    }

    /**
     * Flag that the mouse button is no longer pressed
     */
    mouseUpHandler () {
        this.mouseDown = false;
    }

    /**
     *  Draw the appropriate stuff when the mouse/pointer is moved.
     *  @param {Number} pointerYCoordinate - The Y coordinate of the pointer
     */
    redrawTheVas (pointerYCoordinate) {
        this.renderMinAndMaxAnchors();
        this.renderScale();
        this.renderMinMaxValues();
        this.renderTheStrikeMark();//this.getCursorPoint(this.answer.get('response')));
        this.renderThePointer();
        this.renderAnswerValue(pointerYCoordinate);
    }

    /**
     * Draws the current slider value to the screen
     * @param {Number} ypos The y position
     */
    renderAnswerValue (ypos) {
        let val = this.getVasNumericValue(ypos),
            that = this,
            currentAnswerSizeInPixels = this.answerBoxSize(),

            /**
             * renders the selected value at a static/non-moving location to the right or left of the VAS line
             */
            renderStatic = () => {

                that.ctxVas.beginPath();
                that.ctxVas.font = that.fontString(that.selectedValue.font);
                that.ctxVas.textBaseline = 'middle';
                that.ctxVas.textAlign = 'center';
                that.ctxVas.fillStyle = that.selectedValue.selectionBox.fillColor;
                that.ctxVas.lineWidth = that.selectedValue.selectionBox.borderWidth;

                let rectX = this.pointer.location === 'right' ?
                    that.centerX - that.anchors.width - 5 - currentAnswerSizeInPixels :
                    that.centerX + that.anchors.width + 5,
                    rectY = that.centerY - currentAnswerSizeInPixels / 2,
                    answerX = this.pointer.location === 'right' ?
                    that.centerX - that.anchors.width - 5 - currentAnswerSizeInPixels / 2 :
                    that.centerX + that.anchors.width + 5 + currentAnswerSizeInPixels / 2;

                if (!isNaN(val) && val !== undefined) {
                    that.ctxVas.fillRect(rectX, rectY, currentAnswerSizeInPixels, currentAnswerSizeInPixels);
                    that.ctxVas.closePath();

                    that.ctxVas.beginPath();
                    that.ctxVas.strokeStyle = that.selectedValue.selectionBox.borderColor;
                    that.ctxVas.fillStyle = that.selectedValue.font.color;
                    that.ctxVas.rect(rectX, rectY, currentAnswerSizeInPixels, currentAnswerSizeInPixels);
                    that.ctxVas.fillText(val, answerX, that.centerY);
                    that.ctxVas.stroke();
                    that.ctxVas.closePath();
                }
            },

            /**
             * renders the selected value to the left or right of the strike mark
             */
            renderDynamic = () => {
                that.ctxVas.beginPath();
                that.ctxVas.font = that.fontString(that.selectedValue.font);
                that.ctxVas.fillStyle = that.selectedValue.selectionBox.fillColor;
                that.ctxVas.lineWidth = that.selectedValue.selectionBox.borderWidth;

                if (this.pointer.location === 'right') {
                    that.ctxVas.textAlign = 'start';
                    // the rendered value moves with and is centered inside of the pointer
                    let xvalue = that.centerX + 5 + (that.anchors.width / 2) + (that.pointer.width / 3);
                    that.ctxVas.fillText(val, xvalue, ypos);
                } else {
                    that.ctxVas.textAlign = 'end';
                    // the rendered value moves with and is centered inside of the pointer
                    let xvalue = that.centerX - 5 - (that.anchors.width / 2) - (that.pointer.width / 3);
                    that.ctxVas.fillText(val, xvalue, ypos);
                }
                that.ctxVas.closePath();
            };

        if (this.answerTextWidth === undefined) {
            this.answerTextWidth = this.ctxVas.measureText(val);
        }

        if (this.selectedValue.isVisible === true) {
            if (this.selectedValue.location === 'static') {
                renderStatic();
            } else if (this.selectedValue.location === 'dynamic') {
                renderDynamic();
            } else {
                // render both
                renderStatic();
                renderDynamic();
            }
        }

        if (ypos !== undefined) {
            this.saveAnswer(val, ypos);
        }
    }

    /**
     * Calculates the current selected value on the VAS scale
     * @param {number} y - The Y coordinate used to find the value.
     * @returns {number}
     */
    getVasNumericValue (y) {
        let yval = y - this.vasLineStart;
        let val = Math.round(yval / this.tickSize) + this.anchors.min.value;

        if (this.anchors.swapMinMaxLocation === true) {
            val = this.anchors.max.value - val;
        }

        return val;
    }

    translateStrings () {
        let maxAnchorModelText = this.model.has('anchors') ? this.model.get('anchors').max.text : '',
            minAnchorModelText = this.model.has('anchors') ? this.model.get('anchors').min.text : '',
            stringsToFetch = {
                maxText: {
                    key: maxAnchorModelText,
                    namespace: this.getQuestion().getQuestionnaire().id
                },
                minText: {
                    key: minAnchorModelText,
                    namespace: this.getQuestion().getQuestionnaire().id
                }
            };

        return new Q.Promise((resolve) => {
            stringsToFetch.maxText.key === '' && delete stringsToFetch.maxText;
            stringsToFetch.minText.key === '' && delete stringsToFetch.minText;

            LF.getStrings(stringsToFetch, (strings) => {

                this.minAnchorText = strings.minText || '';
                this.maxAnchorText = strings.maxText || '';

                resolve();
            });
        });
    }

}

window.LF.Widget.VerticalVAS = VerticalVAS;
