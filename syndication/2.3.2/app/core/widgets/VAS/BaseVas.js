import Logger from '../../Logger';
import WidgetBase from '../WidgetBase';
import SelectedValue from './CommonProperties/SelectedValue';
import TickMarks from './CommonProperties/TickMarks';
import Anchors from './CommonProperties/Anchors';
import Pointer from './CommonProperties/Pointer';

export const logger = new Logger('BaseVAS');
export const defaultPointerColor = '#00ACE6';
export const defaultAnchorTextColor = '#000000';
export const defaultStrikeColor = '#000000';

/**
 * This is a base class that any VAS classes can choose to extend from.  The point of this class
 * is to provide some commonly shared properties loaded between VAS controls along with defaults which
 * get loaded to provide uniformity across different VAS controls.
 *
 * Note defaults that should be overridden along with the "drawTheVas" function the must be overridden.
 */
export default class BaseVas extends WidgetBase {
    get defaultAnchorFont () {
        return {
            name: 'Courier',
            size: 15,
            color: '#000'
        };
    }

    constructor (options) {
        super(options);
        let that = this;

        /**
         * This method binds questionnaire:resize event for VAS
         */
        this.bindResize = () => {
            //If browser is resized, render the canvas again
           that.$('#questionnaire').bind('questionnaire:resize', _(that.resizeHandler).bind(that));
        };

        /**
         * The handler triggered when resize events are fired on the application.
         */
        this.resizeHandler = () => {

            if (this.resizeTimeout) {
                clearTimeout(this.resizeTimeout);
            }

            this.resizeTimeout = setTimeout(() => {
                this.render();
            }, 100);

        };

        //SETUP DEFAULT VALUES and define private variables for the public getters defined below.
        let _InitialCursorDisplay = false,
            _DisplayAs = 'Vertical',
            _VasLine = {
                get height () {
                    return that.defaultVasLineHeight;
                },
                get width () {
                    return that.defaultVasLineWidth;
                }
            },
            _Pointer = new Pointer(this.model),
            _SelectedValue = new SelectedValue(this.model),
            _Anchors = new Anchors(this.model),
            _StrikeMark = {
                get height () {
                    if (that.defaultStrikeMarkHeight === -1) {
                        throw(new Error('defaultStrikeMarkHeight is undefined.  Must be overridden'));
                    }

                    return that.defaultStrikeMarkHeight;
                },
                get width () {
                    if (that.defaultStrikeMarkWidth === -1) {
                        throw(new Error('defaultStrikeMarkWidth is undefined.  Must be overridden'));
                    }

                    return that.defaultStrikeMarkWidth;
                }
            },
            _TickMarks = new TickMarks(this.model),
            _CustomProperties = {};

        if (this.model.has('initialCursorDisplay')) {
            _InitialCursorDisplay = this.model.get('initialCursorDisplay');
        }

        if (this.model.has('customProperties')) {
            _CustomProperties = this.model.get('customProperties');
        }

        // attempts to load the displayAs setting from the configuration.  If undefined, this will default to 'Vertical'
        if (this.model.has('displayAs')) {
            _DisplayAs = this.model.get('displayAs');
        }

        //SETUP GETTERS FOR PROPERTIES IN THE CONFIGURATION
        Object.defineProperty(this, 'initialCursorDisplay', {
            get: () => {
                return _InitialCursorDisplay;
            }
        });

        Object.defineProperty(_Anchors, 'height', {
            get: () => {
                return this.defaultAnchorHeight;
            }
        });

        Object.defineProperty(_Anchors, 'width', {
            get: () => {
                return this.defaultAnchorWidth;
            }
        });

        Object.defineProperty(_Pointer, 'height', {
            get: () => {
                return this.defaultPointerHeight;
            }
        });

        Object.defineProperty(_Pointer, 'width', {
            get: () => {
                return this.defaultPointerWidth;
            }
        });

        /**
         * The following section of code defines getter properties for the baseVas class.  This is intended to hide
         * the default and read in values from the configuration.
         */
        Object.defineProperty(this, 'displayAs', {
            get: () => {
                return _DisplayAs;
            }
        });

        /**
         * the top and bottom min/max anchors
         */
        Object.defineProperty(this, 'anchors', {
            get: () => {
                if (_Anchors.font === undefined) {
                    _Anchors.font = {
                        name: 'Arial',
                        size: 20
                    };
                }

                return _Anchors;
            }
        });

        /**
         * the pointer that's shown on the screen
         */
        Object.defineProperty(this, 'pointer', {
            get: () => {
                return _Pointer;
            }
        });

        /**
         * How to display the answer that is shown on the screen
         */
        Object.defineProperty(this, 'selectedValue', {
            get: () => {
                return _SelectedValue;
            },
            set: (val) => {
                _SelectedValue = val;
            }
        });

        /**
         * Defines the strike mark that appears on screen when user strikes
         */
        Object.defineProperty(this, 'strikeMark', {
            get: () => {
                return _StrikeMark;
            }
        });

        /**
         * Object defining the line where the user will strike.
         */
        Object.defineProperty(this, 'vasLine', {
            get: () => {
                return _VasLine;
            }
        });

        /**
         * Object defining the line where the user will strike.
         */
        Object.defineProperty(this, 'tickMarks', {
            get: () => {
                return _TickMarks;
            },
            set: (val) => {
                _TickMarks = val;
            }
        });

        /**
         * Any extra properties defined for the specific vas being rendered
         */
        Object.defineProperty(this, 'customProperties', {
            get: () => {
                return _CustomProperties;
            }
        });
    }

    /**
     * Gets the top of the widget container
     * @returns {*|jQuery}
     */
    get widgetContainerTop () {
        return $('.widget').position().top;
    }

    /**
     * Gets the width of the widget container
     * @returns {*|jQuery}
     */
    get widgetContainerWidth () {
        return $('.widget').width();
    }

    /**
     * Gets the horizontal center of the widget
     * @returns {number}
     */
    get centerX () {
        return (this.widgetContainerWidth / 2);
    }

    get isPortrait () {
        return window.innerHeight > window.innerWidth;
    }

    get isLandscape () {
        return window.innerWidth > window.innerHeight;
    }

    /**
     * accepts a VVAS font object and returns a css like font string
     * @param {Object} fontObject - The font object used to generate the string
     * @returns {string}
     */
    fontString (fontObject) {
        return `${fontObject.size}px ${fontObject.name}`;
    }

    /**
     * Handles the rendering of the widget.
     * @returns {Q.Promise<void>}
     */
    render () {
        return Q()
            .then(() => {
                return this.drawTheVas();
            })
            .tap(() => {
                this.$el.trigger('create');
                this.delegateEvents();
                return Q();
            });
    }

    /**
     * override this to perform any setup work for drawing the VAS such as creating canvas elements,
     * canvas contexts, etc...
     */
    setup () {
    }

    /**
     * must be overridden in rendering class
     */
    drawTheVas () {
        throw(new Error('draw method must be implemented for the vas that is being rendered.'));
    }

    get defaultVasLineHeight () {
        throw(new Error('BaseVas.js -> defaultVasLineHeight function must be overridden in the extended VAS class'));
    }

    get defaultVasLineWidth () {
        throw(new Error('BaseVas.js -> defaultVasLineWidth function must be overridden in the extended VAS class'));
    }

    getWidgetAvailableHeight () {
        return this.$el.height();
    }

    get defaultAnchorHeight () {
    }

    get defaultAnchorWidth () {
    }

    get defaultPointerLocation () {
        throw(new Error('BaseVas.js -> defaultPointerLocation getter must be overridden in the extended VAS class'));
    }

    get defaultPointerHeight () {
        return -1;
    }

    get defaultPointerWidth () {
        return -1;
    }

    get defaultStrikeMarkWidth () {
        return -1;
    }

    get defaultStrikeMarkHeight () {
        return -1;
    }

    /**
     *  Handles the save of the selected value to the answer model
     *  @param {string} respondValue - the value to be saved.
     */
    saveAnswer (respondValue) {
        if (!this.answers.size()) {
            this.answer = this.addAnswer();
        }
        $(this.vasCanvas).attr('data-selectedValue', respondValue.toString());

        this.respondHelper(this.answer, respondValue.toString());
    }

    /////////////////////////
    // RENDERING FUNCTIONS //
    /////////////////////////

    /**
     * UTILITY HELPER FUNCTIONS
     */

    /**
     * Measure the height in pixels of a given text and font*
     *
     * @param {string} val - The string to measure
     * @param {boolean} bold - Indicates if the text is bolded
     * @param {string} font - The font family
     * @param {number} size - The font size of the text
     * @returns {*}
     * @constructor
     */
    MeasureText (val, bold, font, size) {

        // This function works as follows:
        // 1.  Creates a temporary div.
        // 2.  Sets the innerHTML of the div to the string that needs measured.
        // 3.  Sets the font of the DIV to that which is passed in.
        // 4.  Adds the DIV to the dom off screen.
        // 5.  The DIV is measured (giving the width of the text).
        // 6.  DIV is then removed.

        let div = document.createElement('DIV');
        div.innerHTML = val;
        div.style.position = 'absolute';
        div.style.top = '-100px';
        div.style.left = '-100px';
        div.style.fontFamily = font;
        div.style.fontWeight = bold ? 'bold' : 'normal';
        div.style.fontSize = `${size}px`;
        document.body.appendChild(div);

        let rslt = {width: $(div).innerWidth(), height: $(div).innerHeight()};

        document.body.removeChild(div);

        return rslt;
    }

    answerBoxSize () {
        // eslint-disable-next-line new-cap
        let ht = this.MeasureText(this.anchors.max.value, false, this.selectedValue.font.name,
            this.selectedValue.font.size).height,
            // eslint-disable-next-line new-cap
            wdt = this.MeasureText(this.anchors.max.value, false, this.selectedValue.font.name,
                this.selectedValue.font.size).width,
            sizeInPixels = ht > wdt ? ht + 10: wdt + 10;

        return sizeInPixels;
    }

    /**
     * Handle touchstart/touchmove events.  Convert them to normal mousemove events, and forward them to the mouse movehandler.
     * @param {Event} e - The event
     * @returns {boolean}
     */
    // eslint-disable-next-line consistent-return
    mouseTouchHandler (e) {
        if (e.originalEvent.changedTouches.length > 0) {
            return this.mouseMoveHandler(e.originalEvent, true);
        }
    }
}
