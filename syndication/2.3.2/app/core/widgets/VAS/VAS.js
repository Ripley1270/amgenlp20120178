/**
 * Created by mark.matthews on 6/7/2016.
 */

import VerticalVAS from './VerticalVAS';
import HorizontalVAS from './HorizontalVAS';
import EQ5D from './EQ5D/EQ5D';

class Vas {

    /**
     * Constructs a Vas object.
     * @param {Object} options Contains the configuration options for the widget.
     * @param {Object} options.model The model containing the specific configuration for thw widget, as represented by the "widget" property in the parent question's study design.
     * @param {string} options.model.id {dev-only} The ID to be associated with the widget.
     * @param {string} [options.model.displayAs=Horizontal] Indicates the type of VAS to create.  Valid options are "Vertical", "Horizontal", and "EQ5D".
     * @param {boolean} options.model.initialCursorDisplay Indicates whether the "cursor" or strike mark should be displayed initially.
     * @param {Object} options.model.pointer Configuration properties for the pointer which can be used to display the selected value to the user.
     * @param {boolean} [options.model.pointer.isVisible=false] Indicates whether the pointer should be visible.
     * @param {string} [options.model.pointer.location=left] Indicates where the pointer should be drawn in relation to the scale.  For the Vertical VAS, the valid values are "left" (default) and "right."  For the Horizontal VAS, the valid values are "above" and "below."
     * @param {boolean} [options.model.pointer.displayInitially=false] Indicates if the pointer should be displayed initially.  <strong>Not applicable to the Horizontal VAS</strong>.
     * @param {Object} [options.model.pointer.font] Contains font information for the pointer.
     * @param {string} [options.model.pointer.font.Name] The name of the font.
     * @param {string} [options.model.pointer.font.Size] The size of the font.
     * @param {string} options.model.pointer.color The fill color for the pointer.  Should be a valid hex color.
     * @param {Object} options.model.anchors Contains the configuration information for the scale anchors.
     * @param {Object} [options.model.anchors.font] Contains the font information for rendering the anchors.
     * @param {string} options.model.anchors.font.Name The font name.
     * @param {string} options.model.anchors.font.Size The size of the font.
     * @param {boolean} options.model.anchors.swapMinMaxLocation Indicates whether the minimum and maximum anchors should be swapped on the scale.
     * @param {Object} options.model.anchors.min Contains the configuration for the minimum value anchor.
     * @param {string} options.model.anchors.min.Text The text to display on the min anchor.
     * @param {number} [options.model.anchors.min.Value=0] The minimum value available on the sccale.
     * @param {Object} options.model.anchors.max Contains the configuration for the maximum value anchor.
     * @param {string} options.model.anchors.max.Text The text to display on the max anchor.
     * @param {number} [options.model.anchors.max.Value=100] The maximum value available on the sccale.
     * @param {Object} options.model.selectedValue Contains configuration information for rendering the selected value.
     * @param {boolean} options.model.selectedValue.isVisible Indicates whether the selected value should be displayed.
     * @param {string} options.model.selectedValue.location Indicates the location of the displayed selected value.  Valid values are "Dynamic", "Static", and "Both".
     * @param {Object} options.model.selectedValue.font Contains the configuration for the font to be used to display the selected value.
     * @param {string} options.model.selectedValue.font.name The name of the font.
     * @param {string} options.model.selectedValue.font.Size The size of the font.
     * @param {Object} options.model.selectedValue.selectionBox Contains configuration for rendering the box around the displayed selected value.
     * @param {string} options.model.selectedValue.selectionBox.borderColor The border color for the box.
     * @param {string} options.model.selectedValue.selectionBox.fillColor The fill color for the box.
     * @param {number} options.model.selectedValue.selectionBox.borderWidth The width of the border, in pixels (omit 'px')
     * @param {Object} options.model.customProperties A collection of custom properties for the scale.
     * @param {number} [options.model.customProperties.topBottomMargin=20] (<strong>EQ5D only</strong>) The margin to be added to the top and bottom of the scale.
     * @param {string} [options.model.customProperties.instructionText=Error: Instruction text must be defined] (<strong>EQ5D only</strong>) Text resource for block of instructional text on the screen.
     * @param {string} [options.model.customProperties.answerLabelText] (<strong>EQ5D only</strong>) Text resource for block of text underneath the answer box.
     * @param {string} [options.model.customProperties.footerText] (<strong>EQ5D only</strong>) Text resource for block of text on bottom left of the EQ5D (only exists in tablet template).
     * @param {boolean} [options.model.reverseOnRtl=false] Indicates whether to reverse the display of the scale.
     */
    constructor (options) {

        // determine which type of vas we are creating
        let displayAs = options.model.attributes.displayAs,
            _vasToCreate;

        switch (displayAs) {
            case 'Vertical':
                _vasToCreate = new VerticalVAS(options);
                break;
            case 'Horizontal':
                _vasToCreate = new HorizontalVAS(options);
                break;
            case 'EQ5D':
                _vasToCreate = new EQ5D(options);
                break;
            default:
                _vasToCreate = new HorizontalVAS(options);
        }

        return _vasToCreate;
    }
}

window.LF.Widget.VAS = Vas;
export default Vas;
