import BaseVas from '../BaseVas';
import EQ5DTickMarks from './EQ5DTickMarks';
import EQ5DSelectedValue from './EQ5DSelectedValue';

export default class EQ5D extends BaseVas {

    get events () {
        return {
            /** mouse down event */
            'mousedown #mainVasCanvas': 'mouseyDownHandler',
            /** mouse up event */
            'mouseup #mainVasCanvas': 'mouseUpHandler',
            /** mouse move event */
            'mousemove #mainVasCanvas': 'mouseMoveHandler',
            
            'touchmove #mainVasCanvas': 'mouseTouchHandler',
            'touchstart #mainVasCanvas': 'mouseTouchHandler'
        };
    }

    get defaultVasLineHeight () {
        let otherItemsHeight = this.anchors.height + (this.cursorHeight / 2) + this.anchorTextHeight,
            remaining_space = this.getVasCanvasHeight() - otherItemsHeight;

        return remaining_space;
    }

    get defaultVasLineWidth () {
        return 6;
    }

    get defaultStrikeMarkWidth () {
        return this.anchors.width * 0.9;
    }

    get defaultStrikeMarkHeight () {
        return 7;
    }

    get defaultPointerWidth () {
        // set the pointer font to the customized font from the config (if specified)
        this.ctxVas.font = this.fontString(this.selectedValue.font);
        return this.ctxVas.measureText('99999').width;
    }

    get defaultPointerHeight () {

    }

    get template () {
        return 'DEFAULT:EQ5D';
    }

    get defaultAnchorHeight () {
        return 6;
    }

    get defaultAnchorWidth () {
        return 37;
    }

    get defaultPointerLocation () {
        return 'left';
    }

    constructor (options) {
        super(options);

        if (this.customProperties.topBottomMargin === undefined) {
            this.customProperties.topBottomMargin = 20;
        }
    }

    /**
     * Perform the setup of all HTML elements needed to render the EQ5D
     * @returns {*|promise}
     */
    setup () {
        return new Q.Promise((resolve) => {
            // get references to the major players in rendering the EQ5D to the screen.
            // comments included to make it easier to determine what's what...

            // the main widget container.  Look for <div class='widget'> in the DOM
            this.$widget = $('.widget');

            // this is the canvas where the VAS scale itself is drawn to
            this.cvs = document.getElementById('mainVasCanvas');
            this.mainVasCanvasContext = this.cvs.getContext('2d');

            // the DIVs that handle the main layout of the entire widget
            this.leftDIV = $('#left');
            this.rightDIV = $('#right');

            // these three are located inside of this.rightDIV.
            // topper = 'Worst Health You Can Imagine' DIV
            // middle = contains the mainVasCanvas
            // bottom = 'Best Health You Can Imagine' DIV
            this.$topper = this.$('#topper');
            this.$middle = this.$('#middle');
            this.$bottom = this.$('#bottom');
            this.$logPadInstructionTextDIV = this.$('#logpadInstructionTextDIV');

            // the selected answer DIV's.
            // this is the where the black (default) answer box is drawn to
            this.$answerDiv = this.$('#answerDIV');
            this.yhtDIV = this.$('#yhtDIV');

            // spacerDIV is used to line up the answer box with the center of the mainVasCanvas
            this.spacerDIV = this.$('#spacer');

            this.innerDIVs = this.$('#innerDIVS');

            this.widgetHeight = this.getWidgetAvailableHeight();
            this.widgetWidth = this.$widget.width();

            // FIXME: Instead use the utility function isSitepad()
            if (LF.appName === 'SitePad App' && this.customProperties.tickValuePosition === undefined) {
                this.customProperties.tickValuePosition = 'left';
            } else if (this.customProperties.tickValuePosition === undefined) {
                this.customProperties.tickValuePosition = 'right';
            }

            // overwrite the default VAS values for the EQ5D
            this.tickMarks = new EQ5DTickMarks(this.model);
            this.selectedValue = new EQ5DSelectedValue(this.model);

            this.$el.ready(() => {
                resolve();
            });
        });
    }

    /**
     * Responsible for displaying the widget.
     * @returns {Q.Promise} resolved when element is finished being rendered
     */
    drawTheVas () {
        return new Q.Promise((resolve) => {
            this.buildTheHTML()
                .then(() => {
                    this.$el.append(this.widgetHtml)
                        .ready(() => {
                            this.setup()
                                .then(() => {
                                    this.drawTheSelectionMark();
                                    resolve();
                                });
                        });
                });
        });
    }

    drawTheEQ5D () {
        let middle_rect,
            middle_height,
            middle_width,
            vasCanvasWidth,
            vasLineLength;

        // Use getBoundingClientRect, because simple jQuery $.width() rounds and messes things up.
        middle_rect = this.$middle.get(0).getBoundingClientRect();
        middle_height = middle_rect.height;
        middle_width = middle_rect.width;

        // grab a reference to the actual canvas that gets drawn to and set it's initial height and width
        this.cvs.height = Math.floor(middle_height);
        this.cvs.width = Math.floor(middle_width);

        vasCanvasWidth = this.cvs.width;
        this.theCenterX = vasCanvasWidth / 2;

        // this is the length of the line itself.  Give a values.margin of 20 on each end.
        vasLineLength = middle_height - (this.customProperties.topBottomMargin * 2);

        this.tatmp = parseInt(this.anchors.max.value, 10) - parseInt(this.anchors.min.value, 10);

        // very simple calculation to get the location to draw each tick
        this.tickFreq = (vasLineLength / this.tatmp);

        this.firstOne = this.customProperties.topBottomMargin;
        this.lastOne = (this.tickFreq * (this.anchors.max.value)) + this.customProperties.topBottomMargin;

        this.mainVasCanvasContext.beginPath();

        if (this.customProperties.tickValuePosition === 'left') {
            this.mainVasCanvasContext.font = this.fontString(this.tickMarks.values.font);
            this.mainVasCanvasContext.textAlign = 'right';
            this.mainVasCanvasContext.textBaseline = 'middle';

            this.mainVasCanvasContext.fillText(this.anchors.max.value, this.theCenterX - this.tickMarks.majorSize,
                this.customProperties.topBottomMargin);

            this.mainVasCanvasContext.fillText(this.anchors.min.value, this.theCenterX - this.tickMarks.majorSize,
                (this.tickFreq * this.anchors.max.value) + this.customProperties.topBottomMargin);
        } else {
            this.mainVasCanvasContext.font = this.fontString(this.tickMarks.values.font);
            this.mainVasCanvasContext.textAlign = 'left';
            this.mainVasCanvasContext.textBaseline = 'middle';

            this.mainVasCanvasContext.fillText(this.anchors.max.value, this.theCenterX + this.tickMarks.majorSize,
                this.customProperties.topBottomMargin);

            this.mainVasCanvasContext.fillText(this.anchors.min.value, this.theCenterX + this.tickMarks.majorSize,
                (this.tickFreq * this.anchors.max.value) + this.customProperties.topBottomMargin);
        }

        this.mainVasCanvasContext.closePath();

        this.mainVasCanvasContext.lineWidth = this.tickMarks.thickness;

        this.mainVasCanvasContext.beginPath();
        this.mainVasCanvasContext.moveTo(this.theCenterX - (this.tickMarks.majorSize / 2), this.customProperties.topBottomMargin);
        this.mainVasCanvasContext.lineTo(this.theCenterX + (this.tickMarks.majorSize / 2), this.customProperties.topBottomMargin);
        this.mainVasCanvasContext.stroke();
        this.mainVasCanvasContext.closePath();

        this.mainVasCanvasContext.beginPath();
        this.mainVasCanvasContext.moveTo(this.theCenterX - (this.tickMarks.majorSize / 2),
            (this.tickFreq * this.anchors.max.value) + this.customProperties.topBottomMargin);
        this.mainVasCanvasContext.lineTo(this.theCenterX + (this.tickMarks.majorSize / 2),
            (this.tickFreq * this.anchors.max.value) + this.customProperties.topBottomMargin);
        this.mainVasCanvasContext.stroke();
        this.mainVasCanvasContext.closePath();

        if (this.tickMarks.isVisible) {
            for (let i = this.anchors.max.value - 1; i >= this.anchors.min.value + 1; i--) {
                if (i % this.tickMarks.frequency === 0) {
                    this.mainVasCanvasContext.beginPath();
                    this.mainVasCanvasContext.moveTo(this.theCenterX - (this.tickMarks.majorSize / 2),
                        (this.tickFreq * i) + this.customProperties.topBottomMargin);
                    this.mainVasCanvasContext.lineTo(this.theCenterX + (this.tickMarks.majorSize / 2),
                        (this.tickFreq * i) + this.customProperties.topBottomMargin);

                    this.mainVasCanvasContext.stroke();
                    this.mainVasCanvasContext.closePath();
                } else {
                    if (this.tickMarks.displayMinorTicks) {
                        this.mainVasCanvasContext.beginPath();
                        this.mainVasCanvasContext.moveTo(this.theCenterX - (this.tickMarks.minorSize / 2),
                            (this.tickFreq * i) + this.customProperties.topBottomMargin);
                        this.mainVasCanvasContext.lineTo(this.theCenterX + (this.tickMarks.minorSize / 2),
                            (this.tickFreq * i) + this.customProperties.topBottomMargin);

                        this.mainVasCanvasContext.stroke();
                        this.mainVasCanvasContext.closePath();
                    }
                }

                if (i % this.tickMarks.values.frequency === 0) {
                    let tickValue = Math.abs(this.anchors.max.value - i);

                    if (this.customProperties.tickValuePosition === 'left') {
                        this.mainVasCanvasContext.beginPath();
                        this.mainVasCanvasContext.textAlign = 'right';
                        this.mainVasCanvasContext.textBaseline = 'middle';
                        this.mainVasCanvasContext.font = this.fontString(this.tickMarks.values.font);
                        this.mainVasCanvasContext.fillText(tickValue.toString(),
                            this.theCenterX - this.tickMarks.majorSize,
                            (this.tickFreq * i) + this.customProperties.topBottomMargin);
                        this.mainVasCanvasContext.closePath();
                    } else {
                        this.mainVasCanvasContext.beginPath();
                        this.mainVasCanvasContext.textAlign = 'left';
                        this.mainVasCanvasContext.textBaseline = 'middle';
                        this.mainVasCanvasContext.font = this.fontString(this.tickMarks.values.font);
                        this.mainVasCanvasContext.fillText(tickValue.toString(),
                            this.theCenterX + this.tickMarks.majorSize,
                            (this.tickFreq * i) + this.customProperties.topBottomMargin);
                        this.mainVasCanvasContext.closePath();
                    }
                }

                if (parseInt(i, 10) === 0) {
                    this.firstOne = (this.tickFreq * i) + this.customProperties.topBottomMargin;
                }

                if (parseInt(i, 10) === parseInt(this.anchors.max.value, 10)) {
                    this.lastOne = (this.tickFreq * i) + this.customProperties.topBottomMargin;
                }
            }
        }

        this.mainVasCanvasContext.beginPath();
        this.mainVasCanvasContext.lineWidth = this.tickMarks.thickness;
        this.mainVasCanvasContext.moveTo(this.theCenterX, this.firstOne);
        this.mainVasCanvasContext.lineTo(this.theCenterX, this.lastOne);
        this.mainVasCanvasContext.stroke();
        this.mainVasCanvasContext.closePath();

        this.vasScaleVerticalCenter = (this.$topper.height() + (this.$middle.height() / 2)) - this.$logPadInstructionTextDIV.outerHeight();
    }

    drawTheSelectionMark (selectedYCoordinate) {
        this.drawTheEQ5D();

        let selectionYVal = selectedYCoordinate,
            swappedVal = this.anchors.max.value - this.val;

        this.mainVasCanvasContext.font = '26px Arial';

        if (selectionYVal === undefined) {
            selectionYVal = this.val * this.tickFreq;
            selectionYVal = selectionYVal + this.customProperties.topBottomMargin;
        }

        // draws the black 'dot' selection circle
        this.mainVasCanvasContext.beginPath();
        this.mainVasCanvasContext.fillStyle = 'black';
        this.mainVasCanvasContext.arc(this.theCenterX, selectionYVal, 8, 0, 2 * Math.PI);
        this.mainVasCanvasContext.fill();
        this.mainVasCanvasContext.closePath();

        // draws the red 'dot' selection circle
        this.mainVasCanvasContext.beginPath();
        this.mainVasCanvasContext.fillStyle = 'red';
        this.mainVasCanvasContext.arc(this.theCenterX, selectionYVal, 7, 0, 2 * Math.PI);
        this.mainVasCanvasContext.fill();
        this.mainVasCanvasContext.closePath();

        if (!isNaN(swappedVal)) {
            this.$answerDiv.html(swappedVal.toString());

            this.saveAnswer(swappedVal.toString());
        }
    }

    /**
     * handles the mouse down event for the VVAS to save the answer and
     * force a redraw of the pointer and selected value
     * @param {Event} e - The event
     */
    mouseyDownHandler (e) {
        this.mouseDown = true;

        let yval = this.getPageY(e) - $(this.cvs).offset().top;

        this.val = (Math.round((yval - this.customProperties.topBottomMargin) / this.tickFreq)) + this.anchors.min.value;

        if (this.val < this.anchors.min.value) {
            this.val = this.anchors.min.value;
        }

        if (this.val > this.anchors.max.value) {
            this.val = this.anchors.max.value;
        }

        let selectionYVal = this.val * this.tickFreq;
        selectionYVal = selectionYVal + this.customProperties.topBottomMargin;


        this.drawTheSelectionMark(selectionYVal);
    }

    /**
     * If the user drags the slider, update the answer and move the pointer as they're sliding
     * @param {Event} e - The event
     * @param {boolean} [mouseDown=this.mouseDown] whether or not the mouse is down.  Can be overridden, but defaults to the state of the object.
     * @returns {boolean}
     */
    mouseMoveHandler (e, mouseDown = this.mouseDown) {

        //Disable the default browser behaviour
        e.preventDefault();

        if (!mouseDown) {
            return false;
        }

        let yval = this.getPageY(e) - $(this.cvs).offset().top;

        this.val = (Math.round((yval - this.customProperties.topBottomMargin) / this.tickFreq)) + this.anchors.min.value;

        if (this.val < this.anchors.min.value) {
            this.val = this.anchors.min.value;
        }

        if (this.val > this.anchors.max.value) {
            this.val = this.anchors.max.value;
        }

        let selectionYVal = this.val * this.tickFreq;
        selectionYVal = selectionYVal + this.customProperties.topBottomMargin;

        this.drawTheSelectionMark(selectionYVal);

        return true;
    }

    /**
     * Flag that the mouse button is no longer pressed
     */
    mouseUpHandler () {
        this.mouseDown = false;
    }

    /**
     * Override if you have any additional translations that you'd like performed in your VAS
     *  @returns {Q.Promise}
     */
    buildTheHTML () {
        let maxAnchorModelText = this.model.has('anchors') ? this.model.get('anchors').max.text : '',
            minAnchorModelText = this.model.has('anchors') ? this.model.get('anchors').min.text : '',
            instructionModelText,
            answerLabelText,
            footerText,
            stringsToFetch;

        if (this.customProperties.instructionText !== undefined) {
            instructionModelText = this.customProperties.instructionText;
        } else {
            instructionModelText = 'Error: Instruction text must be defined';
        }

        answerLabelText = this.customProperties.answerLabelText || '';
        footerText = this.customProperties.footerText || '';

        stringsToFetch = {
            maxText: {
                key: maxAnchorModelText,
                namespace: this.getQuestion().getQuestionnaire().id
            },
            minText: {
                key: minAnchorModelText,
                namespace: this.getQuestion().getQuestionnaire().id
            },
            instructionText: {
                key: instructionModelText,
                namespace: this.getQuestion().getQuestionnaire().id
            },
            answerLabelText: {
                key: answerLabelText,
                namespace: this.getQuestion().getQuestionnaire().id
            },
            footerText: {
                key: footerText,
                namespace: this.getQuestion().getQuestionnaire().id
            }
        };

        answerLabelText === '' && delete stringsToFetch.answerLabelText;
        minAnchorModelText === '' && delete stringsToFetch.minText;
        maxAnchorModelText === '' && delete stringsToFetch.maxText;
        instructionModelText === '' && delete stringsToFetch.instructionText;
        footerText === '' && delete stringsToFetch.footerText;

        return LF.getStrings(stringsToFetch, (strings) => {
            this.widgetHtml = this.renderTemplate(this.template, {
                topText: strings.maxText || '',
                botText: strings.minText || '',
                answerLabelText: strings.answerLabelText || '',
                instructionText: strings.instructionText || '',
                footerText: strings.footerText || ''
            });

            this.$el.empty();
            this.$el.appendTo(this.parent.$el);
        });
    }
}

window.LF.Widget.EQ5D = EQ5D;
