import TickMarks from '../CommonProperties/TickMarks';
/**
 * Created by mark.matthews on 8/20/2016.
 */
export default class EQ5DTickMarks extends TickMarks {

    constructor (vasModel) {
        super(vasModel);

        // setup the defaults for the EQ5D tick marks based on whether it's a tablet or hand-held
        this.frequency = 10;
        this.values.frequency = LF.appName === 'SitePad App' ? 10 : 50;
        this.displayMinorTicks = LF.appName === 'SitePad App';

        // if the configuration overrides the defaults, load them up
        if (vasModel !== undefined && vasModel.has('tickMarks')) {
            let tmpTickMarks = vasModel.get('tickMarks');

            if (tmpTickMarks.frequency !== undefined) {
                this.frequency = tmpTickMarks.frequency;
            }

            if (tmpTickMarks.displayMinorTicks !== undefined) {
                this.displayMinorTicks = tmpTickMarks.displayMinorTicks;
            }

            if (tmpTickMarks.values !== undefined && tmpTickMarks.values.frequency !== undefined) {
                this.values.frequency = tmpTickMarks.values.frequency;
            }
        }
    }
}
