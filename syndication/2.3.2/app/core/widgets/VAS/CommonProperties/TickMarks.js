/**
 * Created by mark.matthews on 8/20/2016.
 */
export default class TickMarks {
    static get defaultTickMarkFont () {
        return {
            name: 'Arial',
            size: 16,
            color: '#000'
        };
    }

    constructor (vasModel) {
        let _isVisible = true,
            _frequency = 1,
            _thickness = 1,
            _majorSize = 20,
            _minorSize = 10,
            _displayMinorTicks = false,
            _values = {
                font: TickMarks.defaultTickMarkFont,
                frequency: 5,
                margin: 40
            };

        Object.defineProperty(this, 'isVisible', {
            get: () => {
                return _isVisible;
            },
            set: (val) => {
                _isVisible = val;
            }
        });

        Object.defineProperty(this, 'frequency', {
            get: () => {
                return _frequency;
            },
            set: (val) => {
                _frequency = val;
            }
        });

        Object.defineProperty(this, 'thickness', {
            get: () => {
                return _thickness;
            },
            set: (val) => {
                _thickness = val;
            }
        });

        Object.defineProperty(this, 'minorSize', {
            get: () => {
                return _minorSize;
            },
            set: (val) => {
                _minorSize = val;
            }
        });

        Object.defineProperty(this, 'majorSize', {
            get: () => {
                return _majorSize;
            },
            set: (val) => {
                _majorSize = val;
            }
        });

        Object.defineProperty(this, 'displayMinorTicks', {
            get: () => {
                return _displayMinorTicks;
            },
            set: (val) => {
                _displayMinorTicks = val;
            }
        });

        Object.defineProperty(this, 'values', {
            get: () => {
                return _values;
            },
            set: (val) => {
                _values = val;
            }
        });

        if (vasModel !== undefined) {
            if (vasModel.has('tickMarks')) {
                let tmpTickMarks = vasModel.get('tickMarks'),
                    _modelIsVisible = tmpTickMarks.isVisible,
                    _modelFrequency = tmpTickMarks.frequency,
                    _modelThickness = tmpTickMarks.thickness,
                    _modelMinorSize = tmpTickMarks.minorSize,
                    _modelMajorSize = tmpTickMarks.majorSize,
                    _modelDisplayMinorTicks = tmpTickMarks.displayMinorTicks,
                    _modelValues = tmpTickMarks.values;

                _isVisible = _modelIsVisible === undefined ? _isVisible : _modelIsVisible;
                _frequency = _modelFrequency === undefined ? _frequency : _modelFrequency;
                _thickness = _modelThickness === undefined ? _thickness : _modelThickness;
                _minorSize = _modelMinorSize === undefined ? _minorSize : _modelMinorSize;
                _majorSize = _modelMajorSize === undefined ? _majorSize : _modelMajorSize;
                _displayMinorTicks = _modelDisplayMinorTicks === undefined ? _displayMinorTicks : _modelDisplayMinorTicks;

                if (_modelValues !== undefined) {
                    _values.frequency = _modelValues.frequency === undefined ?
                        _values.frequency : _modelValues.frequency;

                    _values.margin = _modelValues.margin === undefined ?
                        _values.margin : _modelValues.margin;

                    if (_modelValues.font !== undefined) {
                        _values.font.name = _modelValues.font.name === undefined ?
                            _values.font.name : _modelValues.font.name;

                        _values.font.size = _modelValues.font.size === undefined ?
                            _values.font.size : _modelValues.font.size;

                        _values.font.color = _modelValues.font.color === undefined ?
                            _values.font.color : _modelValues.font.color;
                    }
                }
            }
        }
    }
}
