import WidgetBase from './WidgetBase';

export default class PasswordMessageWidget extends WidgetBase {
    /**
     * Extension of WidgetBase.
     * Adds rendering capabilities
     * @param {Object} options The options for the widget.
     */
    constructor (options) {
        this.options = options;
        super(options);
        this.render();
        this.completed;
        this.data = {};
    }

    /**
     *  Responsible for displaying the widget
     * @returns {Q.Promise<void>}
     */
    render () {
        let parsedResponses = _.map(_.invoke(this.questionnaire.data.answers.models, 'get', 'response'), (res) => res = JSON.parse(res)),
            templates = this.model.get('templates') || {},
            message = templates.input || this.input,
            roleToCheck,
            thisRole,
            roleName,
            thisLang ,
            messageElement,
            stringsForRender;

        this.data = _.extend.apply(Object, _.filter(parsedResponses, (res) => _.isObject(res))) || {};
        roleToCheck =  this.data.role || LF.StudyDesign.defaultUserValues.role;
        thisRole = _.find(LF.StudyDesign.roles.models, (role) => role.get('id') === roleToCheck);
        roleName = thisRole ? thisRole.get('displayName') : '';
        thisLang = this.data.language || LF.StudyDesign.defaultUserValues.language;
        stringsForRender = {
            tempPasswordMessage : this.model.get('text').tempPasswordMessage,
            userName: this.model.get('text').userName,
            role: this.model.get('text').role,
            tempPassword: this.model.get('text').tempPassword,
            language: this.model.get('text').language,
            displayRole: roleName,
            displayLanguage: thisLang.toUpperCase()
        };

        return LF.getStrings(stringsForRender).then((strings) => {
            messageElement = LF.templates.display(message, {
                tempPasswordMessage : strings.tempPasswordMessage,
                userName: strings.userName,
                role: strings.role,
                tempPassword: strings.tempPassword,
                language: strings.language,
                displayUsername: this.data.username,
                displayRole: strings.displayRole,
                displayTempPass: this.data.password,
                displayLanguage: strings.displayLanguage

            });
        }).then(() => {
            this.$el.empty();
            this.$el.append(messageElement).appendTo(this.parent.$el).trigger('create');
            this.delegateEvents();
            this.completed = true;
            if (!this.answer) {
                this.answer = this.addAnswer();
            }
            super.respondHelper(this.answer,JSON.stringify('PasswordMessageWidget'),this.completed);
        });
    }
}

PasswordMessageWidget.prototype.input = 'DEFAULT:PasswordMessage';
window.LF.Widget.PasswordMessageWidget = PasswordMessageWidget;
