import StaticField from './StaticField';

/**
 * Display an already populated language response.
 * Note: The answer record must be pre-populated prior to render.
 * @example
 *  widget      : {
 *      id          : 'ACTIVATE_USER_W_3',
 *      type        : 'StaticLanguageField',
 *      label       : 'LANGUAGE',
 *      field       : 'language'
 *  }
 */
export default class StaticLanguageField extends StaticField {
    constructor (options) {
        super(options);

        // We always want the languages's display name to be translated.
        this.model.set('translatable', true);
    }

    /**
     * The text to display when rendering the widget.
     * @returns {string} The text to display.
     */
    get displayText () {
        return super.displayText.toUpperCase();
    }
}

window.LF.Widget.StaticLanguageField = StaticLanguageField;
