import TextBox from './TextBox';

export default class SecretAnswerTextBox extends TextBox {

    /**
     *  @constructs SecretAnswerTextBox
     *  @param {Object} [options={}] The options for the class.
     */
    constructor (options = {}) {
        super(_.extend({

            /**
             * @property {Object} events A map of events to delegate.
             */
            events: {
                'keypress input[type=text]': 'keyPressed',
                'input input[type=text]': 'sanitize',
                'focus input[type=text]' : 'onFocus'
            }
        }, options));

        super.render();
    }

    /**
     * @property {String} input The input template to use when rendering the widget.
     */
    get input () {
        return 'DEFAULT:TextBox';
    }

    /**
     * @property {String} displayText Text to display when rendering the widget.
     */
    get displayText () {
        if (this.answer != null) {
            return JSON.parse(this.answer.get('response'))[this.model.get('field')];
        }

        return '';
    }

    /**
     *  Handles user input on the widget.
     *  @param {Event} e Event data
     */
    respond (e) {
        let validateRegex = new RegExp(LF.StudyDesign.answerFormat || '.{2,}'),
            field =  this.model.get('field'),
            value = this.$(e.target).val(),
            response = JSON.stringify({[field]: value});

        // If the is no answer yet, add one.
        if (!this.answers.size()) {
            this.answer = this.addAnswer();
        }

        this.respondHelper(this.answer, response, validateRegex.test(value));
    }

    /**
    * clears the textbox and removes the answer
    */
    clear () {
        this.completed = false;
        this.removeAnswer(this.answer);
        this.$(`#${this.model.get('id')}`).val('');
    }
}

window.LF.Widget.SecretAnswerTextBox = SecretAnswerTextBox;
