import BaseSpinner from './BaseSpinner';
import NumberSpinnerInput from './input/NumberSpinnerInput';

let {Model} = Backbone;

const DEFAULT_MODAL_TEMPLATE = 'DEFAULT:NumberSpinnerModal',
    DEFAULT_SPINNER_TEMPLATE = 'DEFAULT:NumberSpinnerControl',
    DEFAULT_SPINNER_ITEM_TEMPLATE = 'DEFAULT:NumberItemTemplate',
    DEFAULT_CLASS_NAME = 'NumberSpinner';

/**
 * The NumberSpinner input control implements the BaseSpinner interface, to create a numeric control.
 * <ul>
 *      <li>NumberSpinnerInput is the input.</li>
 *      <li>Spinners are injected into containers with the 'number-spinner-container' CSS class in the modal template.</li>
 *      <li>Creating an array to send to spinners splits the string on '.' and sends an array of size 2 to a decimal spinner.</li>
 *      <li>Values are concatenated together when returning from the spinner.</li>
 * </ul>
 * <p><i>Note: 3 things passed in from study-design.js must be arrays in this widget.  They depend
 * on there being the same number of HTML elements with the class of 'number-spinner-container' in the modal template.
 * Each index of these arrays will apply to each spinner, respectively, based on the order they occur in the template.
 * <ul>
 *     <li>spinnerInputOptions</li>
 *     <li>templates.spinnerTemplates</li>
 *     <li>templates.spinnerItemTemplates</li>
 * </ul></i>
 */
class NumberSpinner extends BaseSpinner {
    /**
     * default modal template
     * @returns {string}
     */
    get defaultModalTemplate () {
        return DEFAULT_MODAL_TEMPLATE;
    }

    /**
     * default spinner template
     * @returns {string}
     */
    get defaultSpinnerTemplate () {
        return DEFAULT_SPINNER_TEMPLATE;
    }

    /**
     * default spinner item template
     * @returns {string}
     */
    get defaultSpinnerItemTemplate () {
        return DEFAULT_SPINNER_ITEM_TEMPLATE;
    }

    /**
     * default class name.
     */
    get defaultClassName () {
        return DEFAULT_CLASS_NAME;
    }

    /**
     * Construct the number spinner
     * @param {Object} options options for this widget (see super class for option definitions).
     * @param {Object} options.model The model containing the widget configuration.  This represents the "widget" node in the study-design for this question.
     * @param {string} options.model.label The text label for the spinner
     * @param {string} options.model.modalTitle The title string for the modal dialog.
     * @param {Array<NumberSpinnerInputOptions>} options.model.spinnerInputOptions Array of options for each spinner in the template.
     * @param {number} options.model.spinnerInputOptions[].min min value for this spinner instance.
     * @param {number} options.model.spinnerInputOptions[].max max value for this spinner instance.
     * @param {number} options.model.spinnerInputOptions[].precision number of decimal places to show in this spinner instance.
     * @param {boolean} options.model.spinnerInputOptions[].showLeadingZeroes whether or not to show leading zeroes in this spinner instance.
     * @param {boolean} options.model.spinnerInputOptions[].step step intervals for this spinner instance.
     * @param {string} options.model.id {dev-only} The ID property for this widget.
     * @param {string} [options.model.type=NumberSpinner] {dev-only} The widget type.
     * @param {Object} [options.model.labels={labelOne: '', labelTwo: ''}] {dev-only} The label strings.  Used to replace the label placeholders in the modal dialog template.
     * @param {string} [options.model.okButtonText=OK] {dev-only} OK button text.
     * @param {string} [options.model.className=NumberSpinner] {dev-only} The CSS classname for the widget.
     */
    constructor (options) {
        super(options);

        /**
         * Array of number spinners.
         * @type {Array<NumberSpinnerInput>}
         */
        this.spinners = [];
    }

    /**
     * Get the string value for the text box from an array of spinner values.
     * @returns {Promise<string>} promise returning a string to be used for our textbox.
     */
    getModalValuesString () {
        let valueArray = [];
        for (let i = 0; i < this.spinners.length; ++i) {
            this.spinners[i].stop();
            this.spinners[i].pushValue();
        }

        return Q().then(() => {
            for (let i = 0; i < this.spinners.length; ++i) {
                valueArray.push(this.spinners[i].itemDisplayValueFunction(this.spinners[i].value));
            }
            if (isNaN(parseFloat(valueArray[0]))) {
                return '';
            }
            return valueArray.reduce((x, y) => (x.toString() + y.toString()));
        });
    }

    /**
     * Get the array of spinner values from the textbox entry.
     * @returns {Array<Number>} the array to be passed along to our spinners.
     */
    getSpinnerValuesArray () {
        let textBox = this.$(`#${this.model.get('id')}`),
            valArray,
            defaultVal = this.model.get('defaultVal') || '',
            val;

        val = textBox.val();
        if (val === '' || val === undefined) {
            val = defaultVal.toString();
        }
        valArray = val.split('.');
        if (valArray.length > 1) {
            valArray[1] = parseFloat(`.${valArray[1]}`);
        }
        return valArray;
    }

    /**
     * Place spinners into place.  For the NumberSpinner implementation, they go into elements
     * with the CSS class 'number-spinner-container'.
     */
    injectSpinnerInputs () {
        let foundSpinner = false,
            that = this;

        //jscs:disable requireArrowFunctions
        that.$modal.find('.number-spinner-container').each(function (i) {
            let spinnerTemplate = that.getSpinnerTemplate(i),
                spinnerItemTemplate = that.getSpinnerItemTemplate(i),
                spinnerElement,
                spinnerInputOptions;

            spinnerElement = that.renderTemplate(spinnerTemplate, {});

            // For Item, delay evaluate, so we can replace placeholders with actual values and display values
            spinnerInputOptions = that.model.get('spinnerInputOptions') || [];

            foundSpinner = true;
            that.spinners[i] = new NumberSpinnerInput(
                _.extend({
                    model: new Model(),
                    parent: this,
                    template: spinnerElement,
                    itemTemplate: spinnerItemTemplate
                }, spinnerInputOptions[i] || {}
                )
            );
        });

        //jscs:enable requireArrowFunctions
        if (!foundSpinner) {
            throw new Error(`Invalid template for NumberSpinner widget.  Expected an element with
                "number-spinner-container" class`);
        }
    }

    render () {
        let minDate = _.first(this.model.get('spinnerInputOptions')).min,
            maxDate = _.first(this.model.get('spinnerInputOptions')).max,
            answer = _.last(this.answers.models);

        if(answer && (answer.get('response') < minDate || answer.get('response') > maxDate)){
            this.removeAnswer(answer);
        }
        return super.render();
    }
}

window.LF.Widget.NumberSpinner = NumberSpinner;
export default NumberSpinner;
