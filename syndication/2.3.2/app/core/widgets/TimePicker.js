/** #depends DateTimeWidgetBase.js
 * @file Defines a time picker widget.
 * @author <a href="mailto:ante.burilovic@ert.com">Ante Burilovic</a>
 * @version 1.0
 */

import DateTimeWidgetBase from './DateTimeWidgetBase';
import { zeroPad } from 'core/utilities';

class TimePicker extends DateTimeWidgetBase {
    /**
     * LF.Widget.TimePicker constructor function
     * @param {Object} options The options used to construct the class.
     * @param {Object} options.model The model that contains the widget configuration, as represented by the "widget" property for the question in the study design.
     * @param {string} options.model.id {dev-only} The ID to be associated with the widget.
     * @param {string} [options.model.min] The minimum time accepted by the widget.
     * @param {string} [options.model.max] The maximum time accepted by the widget.
     * @param {boolean} [options.model.showLabels=false] Indicates whether the picker labels are rendered.
     * @param {Object} [options.model.templates] {dev-only} Container to override default templates for rendering the widget.
     * @param {string} [options.model.templates.timePicker=DEFAULT:TimePicker] {dev-only} Container template for the time picker.
     * @param {string} [options.model.templates.timePickerLabel=DEFAULT:TimePickerLabel] {dev-only} Template for the time picker label.
     * @param {Object} [options.model.configuration] {dev-only} Contains specific configuration settings for the DateBox library.  The full API is available to via <a href="http://dev.jtsage.com/jQM-DateBox/api/">here</a>.
     */
    constructor (options) {
        super(options);
        this.defaultConfig = {
            mode: 'timebox',
            useSetButton: true,
            useHeader: true,
            setTimeLabel: 'SET_TIME',
            useFocus: true
        };
    }

    /**
     * Responsible for displaying the widget.
     * @returns {Q.Promise<void>}
     */
    render () {
        const showLabels = this.model.get('showLabels') && this.model.get('showLabels') === true;

        _.defaults(this.config, this.defaultConfig);

        this.resetConfig();

        return Q().then(() => {
            const timeLblKey = 'TIME_LBL';
            return showLabels ? LF.getStrings({ timeLbl: timeLblKey, setTimeLabel: this.config.setTimeLabel }) : Q();
        })
        .then((result) => {
            const templates = this.model.get('templates') || {},
                timePicker = templates.timePicker || this.timePicker;

            this.$el.empty();
            if (showLabels) {
                const timePickerLabel = templates.timePickerLabel || this.timePickerLabel,
                    timePickerElementLabel = LF.templates.display(timePickerLabel, {
                        id: this.model.get('id'),
                        timeLabel: result.timeLbl
                    });
                this.$el.append(timePickerElementLabel);
            }

            const timePickerElement = LF.templates.display(timePicker, {
                id: this.model.get('id')
            });

            this.$el.append(timePickerElement)
                .appendTo(this.parent.$el);

            if (this.localizedTime) {
                this.$('input[id$="_time_input"]').val(this.localizedTime);
            }
            this.delegateEvents();
            this.$el.trigger('create');

            this.inputTime = this.$('input.time-input');
            this.inputTime.datebox(this.config);
        });
    }

    /**
     * Constructs StudyWorks formatted date string
     * @returns {String} a studyworks formatted date string HH:mm:ss
     * @example this.buildStudyWorksString();
     */
    buildStudyWorksString () {
        let swHours,
            swMinutes,
            dateVal = this.inputTime.datebox('getTheDate');

        if (!dateVal) {
            return '';
        }
        swHours = zeroPad(dateVal.getHours());
        swMinutes = zeroPad(dateVal.getMinutes());

        return `${swHours}:${swMinutes}:00`;
    }

    /**
     * fired when value is set. saves the UI Control value into the answer associated with the widget
     * @return {Q.Promise<void>} promise resolved when complete.
     */
    respond () {
        let value = this.$('input.time-input').val(),
            answered = value !== '';

        //Store localized input into widget property to load back in
        this.localizedTime = value;

        if (!answered) {
            this.removeAllAnswers();
            return Q();
        }

        value = this.buildStudyWorksString();
        !this.answers.size() && this.addAnswer();
        return this.respondHelper(this.answer, value, answered);
    }

    /**
     * Add an answer record to the collection.
     * @param {Object} params An object literal
     * @returns {Object}
     */
    addAnswer (params = {}) {
        this.answer = super.addAnswer(params);
        return this.answer;
    }
}

window.LF.Widget.TimePicker = TimePicker;
export default TimePicker;
