/**
 *    @file Base class for text entry controls
 *    @author <a href="mailto:brian.janaszek@ert.com">bjanaszek</a>
 *    @version 1.0.0
 */

import WidgetBase from './WidgetBase';

const LABEL = 'DEFAULT:Label';
const INPUT = 'DEFAULT:Input';
const WRAPPER = 'DEFAULT:FormGroup';

export default class SelectWidgetBase extends WidgetBase {

    get wrapper () {
        return WRAPPER;
    }

    get label () {
        return LABEL;
    }

    get input () {
        return INPUT;
    }

    /**
     * @param {Object} options The options for the widget
     */
    constructor (options) {
        super(_.extend({

            /**
             * List of widget events
             */
            events: {
                'change select': 'respond',
                'click select': 'respond'
            }

        }, options));

        let validation = this.model.get('validation');
        (validation) ? this.validation = validation : '';
    }

    /**
     *  Responsible for displaying the widget
     * @returns {Q.Promise<void>}
     */
    render () {
        let templates = this.model.get('templates') || {},
            selectTemplate = templates.input || this.input,
            id = `#${this.model.get('id')}`,
            toTranslate = {};

        this.$el.empty();

        this.model.get('label') && (toTranslate.label = this.model.get('label'));

        return this.i18n(
            toTranslate,
            () => null,
            {namespace: this.getQuestion().getQuestionnaire().id}
        ).then(
            strings => {
                let wrapperElement,
                    labelElement,
                    selectElement;

                wrapperElement = this.renderTemplate(templates.wrapper || this.wrapper);

                // If a label has been set.
                if (strings.label) {
                    labelElement = this.renderTemplate(templates.label || this.label, {
                        link: this.model.get('id'),
                        text: strings.label
                    });
                }

                selectElement = this.renderTemplate(selectTemplate, {
                    id: this.model.get('id'),
                    name: this.model.get('id'),
                    className: this.model.get('className')
                });

                // Append the wrapper to the local DOM and find where to append other elements.
                let $wrapper = this.$el.append(wrapperElement).find('[data-container]');

                if (labelElement) {
                    $wrapper.append(labelElement);
                }

                $wrapper.append(selectElement)
                    .appendTo(this.$el);

                // Append the widget to the parent element and trigger a create event.
                this.$el.appendTo(this.parent.$el)
                    .trigger('create');
            })
            .then(() => {
                return this.renderOptions(id);
            })
            .then(() => {
                return this.initializeSelect2();
            });
    }

    /**
     * Initializes the select 2 plugin on our select box.
     * Also setup the initial answer.
     * @param {Object} [options={}] The options for the select2
     * @returns {Q.Promise<jQuery>} promise resolving with jQuery selector for element, for chaining purposes.
     */
    initializeSelect2 (options = {}) {
        return new Q.Promise((resolve) => {
            let id = `#${this.model.get('id')}`;

            $.when(this.$(id).select2(_.extend({ minimumResultsForSearch: Infinity, width: '' }, options)))
            .done(() => {
                if (!!(this.answer) && this.completed) {
                    let answer = JSON.parse(this.answer.get('response'))[this.model.get('field')];

                    $(id).val(answer);

                    if (this.model.get('disabled')) {
                        $(id).attr('disabled', 'disabled');
                    }
                } else if (this.needToRespond && !this.answer) {
                    this.respond(this.needToRespond);
                }

                this.delegateEvents();

                resolve(this.$el);
            });
        });
    }

    /**
     * Creates the response for the widget
     * @param {Event} e The event args
     */
    respond (e) {
        let value = (e.target) ? this.$(e.target).val() : e.value,
            defaults = LF.StudyDesign.defaultUserValues,
            field = this.model.get('field');

        this.completed = true;

        if (!this.answers.size()) {
            this.answer = this.addAnswer();
        }

        if (!value && defaults[field]) {
            value = defaults[field];
        }

        let response = JSON.stringify({[field]: value});

        super.respondHelper(this.answer, response, this.completed);
    }
}

window.LF.Widget.SelectWidgetBase = SelectWidgetBase;
