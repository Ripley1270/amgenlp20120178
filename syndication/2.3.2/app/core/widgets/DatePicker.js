/** #depends DateTimeWidgetBase.js
 * @file Defines a date picker widget.
 * @author <a href="mailto:dimitar.vukman@ert.com">Dimitar Vukman</a>
 * @version 1.0
 */

import DateTimeWidgetBase from './DateTimeWidgetBase';

class DatePicker extends DateTimeWidgetBase {

    /**
     * Constructs a DatePicker.
     * @param {Object} options The configuration options for the widget.
     * @param {Object} options.model The model containing the specific configuration for the widget, as represented by the "widget" property in the study design for the question.
     * @param {string} options.model.id {dev-only} The ID assigned to the widget.
     * @param {boolean} options.model.showLabels Indicates whether to display the picker labels.
     * @param {string} options.model.min The minimum date allowed by the widget.
     * @param {string} options.model.max The maximum date allowed by the widget.
     * @param {Object} [options.model.templates] Container for the templates that can be used to render the widget's elements.
     * @param {string} [options.model.templates.datePicker=DEFAULT:DatePicker] The template for the widget's main container element.
     * @param {string} [options.model.templates.datePickerLabel=DEFAULT:DatePickerLabel] The template for the widget's label element.
     * @param {Object} [options.model.configuration] {dev-only} Contains specific configuration settings for the DateBox library.  The full API is available to via <a href="http://dev.jtsage.com/jQM-DateBox/api/">here</a>.
     * 
     */
    constructor (options) {
        super(options);
        this.defaultConfig = {
            mode: 'calbox',
            calUsePickers: 'true',
            calNoHeader: true,
            useFocus: true,
            calHighToday: false,
            calHighPick: false
        };
    }

    /**
     * Responsible for displaying the widget.
     * @returns {Q.Promise<void>}
     */
    render () {
        let showLabels,
            datePickerElementLabel,
            datePickerElement,
            templates = this.model.get('templates') || {},
            datePicker = templates.datePicker || this.datePicker,
            datePickerLabel = templates.datePickerLabel || this.datePickerLabel,
            attributes = this.model.attributes;

        _.defaults(this.config, this.defaultConfig);

        this.resetConfig();

        this.$el.empty();

        return Q()
            .then(() => {
                showLabels = this.model.get('showLabels') && this.model.get('showLabels') === true;

                return showLabels ? LF.getStrings('DATE_LBL') : Q();
            })
            .then((label) => {
                if (showLabels) {
                    datePickerElementLabel = LF.templates.display(datePickerLabel, {
                        id: `${this.model.get('id')}_date_input`,
                        dateLabel: label
                    });
                    this.$el.append(datePickerElementLabel);
                }

                datePickerElement = LF.templates.display(datePicker, {
                    id: this.model.get('id'),
                    min: this.config.min || attributes.min,
                    max: this.config.max || attributes.max,
                    configuration: _.escape(JSON.stringify(this.config))
                });
                this.$el.append(datePickerElement)
                    .appendTo(this.parent.$el);

                if (this.localizedDate) {
                    //this.$('#' + this.model.get('id') ).val(this.localizedDate);
                    this.$('input.date-input').val(this.localizedDate);
                }
                this.delegateEvents();
                this.$el.trigger('create');

                //call datebox on input
                //this.$('#' + this.model.get('id') ).datebox(this.config);
                this.$('input.date-input').datebox(this.config);
            });
    }

    /**
     * Constructs StudyWorks formatted date string
     * @param {Date} date A date object to build StudyWorks string from
     * @returns {String} a StudyWorks formatted date string dd Mmm yyyy
     * @example this.buildStudyWorksString(new Date());
     * @example this.buildStudyWorksString(new Date(1995,11,17));
     */
    buildStudyWorksString (date) {

        if (!(date instanceof Date)) {
            throw new Error(`Expected a Date, got ${date}`);
        }
        let swDate = (date.getDate() < 10) ? `0${date.getDate()}` : date.getDate(),
            swMonth = this.swMonths[date.getMonth()],
            swYear = date.getFullYear();

        return (`${swDate} ${swMonth} ${swYear}`);

    }

    /**
     * Responds to user input.
     * @return {Q.Promise<void>} promise resolved when complete.
     */
    respond () {
        let value = this.$('input.date-input').val(),
            date = this.$('input.date-input').datebox('getTheDate'),
            strSWDate,
            answered = value !== '';

        //Store localized input into widget property to load back in
        this.localizedDate = value;

        if (answered) {
            strSWDate = this.buildStudyWorksString(date);
            value = strSWDate;
            if (!this.answers.size()) {
                this.answer = this.addAnswer();
            }
        } else {
            this.removeAllAnswers();
            return Q();
        }
        return this.respondHelper(this.answer, value, value);
    }
}

window.LF.Widget.DatePicker = DatePicker;
export default DatePicker;
