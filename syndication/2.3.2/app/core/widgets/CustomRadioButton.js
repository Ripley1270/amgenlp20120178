/** #depends RadioButton.js
 * @file Defines a custom radio button widget.
 * @author <a href="mailto:gsaricali@phtcorp.com">Gulden Saricali</a>
 * @version 1.4
 */
/**
 * LF.Widget.CustomRadioButton's constructor function
 * @class Initialize a new CustomRadioButton widget
 * @augments LF.Widget.RadioButton
 */

import RadioButton from './RadioButton';

export default class CustomRadioButton extends RadioButton {
    //noinspection JSMethodCanBeStatic
    /**
     * The class of the root element.
     * @returns {String}
     */
    get className () {
        return 'CustomRadioButton';
    }
}

window.LF.Widget.CustomRadioButton = CustomRadioButton;
