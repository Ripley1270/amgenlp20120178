import TextBox from './TextBox';
import Data from 'core/Data';
import {zeroPad} from 'core/utilities';

/**
 *    @file Defines a text box for entering a patient ID.  This is essentially the textbox widget with just a bit of extra validation.
 *    @author <a href="mailto:brian.janaszek@ert.com">brian janaszek</a>.
 *    @version 1.0
 */
class PatientIDTextBox extends TextBox {

    /**
     * Constructor
     * @param {Object} options flags for this widget.
     * @param {Object} options.model The model containing the widget configuration (from the "widget" property of the question).
     * @param {int} [options.model.maxLength] Maximum length of the input field.  Null or blank will make it infinite.
     * @param {string} options.model.id The ID of the widget.
     * @param {string} options.model.className {dev-only} The CSS classname to be applied to the widget.
     * @param {string} [options.model.validateRegex] {dev-only} The regular expression used to validate user input.
     * @param {string} [options.model.disallowedKeyRegex] {dev-only} The regular expression to filter characters that cannot be entered into the textbox.
     * @param {string} [options.model.defaultVal] The default value for the textbox.
     * @param {string} options.model.placeholder Placeholder text in the textbox itself.  This will be deleted as soon as the user interacts with the textbox.  Can be used as "hint" text.
     * @param {string} options.model.label The text label to be attached to the textbox.
     * @param {boolean} [options.model.disabled] Indicates whether the textbox should be disabled.
     */
    constructor (options) {
        super(options);
        if (this.model) {
            this.validation = this.model.get('validation');
        }

        /**
         * Options for this widget, including any template params
         * @type {Object}
         */
        this._isUnique = false;
        this._isInRange = false;
        this.options = options;
    }
    /**
     * Whether or not this widget currently contains a unique value.
     * Used for validation.
     * @type {boolean}
     */
    get isUnique ()  {
        return this._isUnique;
    }

    /**
     * Whether or not this widget currently contains a unique value.
     * Used for validation.
     * @param {boolean} val the value to set
     */
    set isUnique (val) {
        return (this._isUnique = !!val);
    }

    /**
     * Whether or not this widget currently contains a value that is within the specified patient range.
     * Used for validation.
     * @type {boolean}
     */
    get isInRange ()  {
        return this._isInRange;
    }
    /**
     * @param {boolean} val the value to set
     */
    set isInRange (val) {
        return (this._isInRange = !!val);
    }

    /**
     * the default input
     * @returns {string}
     */
    get input () {
        return 'DEFAULT:NumberBox';
    }

    get displayText () {
        let answer;

        $('#prevItem').hide();

        if (this.answer && this.completed) {
            const format = LF.StudyDesign.participantSettings.participantNumberPortion;

            answer = parseInt(this.answer.get('response').substring(format[0], format[1]), 10);
        }

        return answer ? answer : '';
    }

    /**
     *    Handles user input on the widget.
     *    @param {Event} e Event data
     *    @returns {Q.Promise<void>}
     */
    respond (e) {
        return super.respond(e)
            .then(() => {
                if (this.answer) {
                    // Patient ID needs to be formatted before we run our validation
                    this.answer.set('response', PatientIDTextBox.generatePatientID(this.answer.get('response')));

                }
            });
    }

    /**
     * Get state of the control's validation.
     * If mandatory, validation includes super, isUnique, and isInRange.
     * @returns {boolean}
     */
    validate () {
        // if we have an answer, verify that it is valid
        if (this.answer) {
            return super.validate();
        } else {
            return !this.mandatory;
        }
    }

    /**
     *    Generates the formatted patient ID based on the study design configuration.
     *    @param {string} patientID The ID entered by the user.
     *    @returns {string} The formatted ID
     */
    static generatePatientID (patientID) {
        let idFormat = LF.StudyDesign.participantSettings.participantIDFormat,
            patientPortion = LF.StudyDesign.participantSettings.participantNumberPortion,
            sitePortion = LF.StudyDesign.participantSettings.siteNumberPortion,
            siteCode = Data.site.get('siteCode'),
            formattedSite,
            formattedPatient,
            fullFormatted;

        if (patientID == null) {
            return '';
        }

        formattedSite = zeroPad(siteCode, sitePortion[1] - sitePortion[0]);
        formattedPatient = zeroPad(patientID, patientPortion[1] - patientPortion[0]);
        fullFormatted = '';

        // Walk the string and build our code from the idFormat with patient and site ranges spliced in.
        for (let i = 0, len = idFormat.length; i < len; ++i) {
            if (i >= sitePortion[0] && i < sitePortion[1]) {
                fullFormatted += formattedSite.charAt(i - sitePortion[0]);
            } else if (i >= patientPortion[0] && i < patientPortion[1]) {
                fullFormatted += formattedPatient.charAt(i - patientPortion[0]);
            } else {
                fullFormatted += idFormat.charAt(i);
            }
        }

        return fullFormatted;
    }

}
window.LF.Widget.PatientIDTextBox = PatientIDTextBox;
export default PatientIDTextBox;
