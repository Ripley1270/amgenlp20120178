import AddUserTextBox from './AddUserTextBox';
import { isValidPassword } from '../utilities';

/**
  * @file Defines a text box for entering user information.
  * @version 1.0
  */

export default class TempPasswordTextBox extends AddUserTextBox {
    /**
     *  Constructor
     *  @param {Object} options The options for the widget.
     */
    constructor (options) {
        this.options = options;
        super(options);
        this.events = {
            'input input': 'keyPressed',
            'focus input' : 'onFocus'
        };
        this.hasError = false;
        this.isValid = true;

    }

    /**
     *  Responds to a key press event.
     *  @param {Event} e Event data
     */
    keyPressed (e) {
        let keyCode = e.which || e.keyCode;

        if ((keyCode === 13) || (keyCode === 10)) {
            super.keyPressed(e);
        } else {
            let $target = $(e.target),
                passwordValue = $target.val(),
                nonDateboxInputs = $(':input').not('[data-role = "datebox"]'),
                confirmationTextBox = $(nonDateboxInputs[nonDateboxInputs.index($target) + 1]),
                confirmationValue = (confirmationTextBox) ? confirmationTextBox.val() : '',
                parent = $target.parent('.input-group'),
                roleWidget = this.questionnaire.questionViews.filter((q) => q.widget.model.get('field') === 'role')[0].id,
                roleValue = JSON.parse(this.questionnaire.queryAnswersByID(roleWidget)[0].get('response')).role,
                rolePasswordFormat = LF.StudyDesign.roles.find({ id: roleValue }).get('passwordFormat'),
                defaultPasswordFormat = (LF.StudyDesign.defaultPasswordFormat) ? _.defaults(LF.StudyDesign.defaultPasswordFormat, LF.CoreSettings.defaultPasswordFormat) : LF.CoreSettings.defaultPasswordFormat,
                passwordFormat = _.defaults(rolePasswordFormat, defaultPasswordFormat),
                isPasswordValid = isValidPassword(passwordValue,passwordFormat),
                icon = (isPasswordValid) ? '<span class="glyphicon glyphicon-ok-sign form-control-feedback" aria-hidden="true"></span>' : '<span class="glyphicon glyphicon-minus-sign form-control-feedback" aria-hidden="true"></span>',
                classesToAdd = (isPasswordValid) ? 'has-feedback has-success' : 'has-feedback has-error';

            if (parent.hasClass('has-feedback')) {
                $target.siblings('.form-control-feedback').remove();
                $target.removeClass('ui-state-error-border');
                $($target.parentsUntil('screen')).removeClass('ui-state-error-border');
                parent.removeClass('has-feedback has-success has-warning has-error');
            }

            this.hasError = !isPasswordValid;
            this.completed = isPasswordValid && !this.hasError;
            this.isValid = isPasswordValid || passwordValue === '';
            parent.addClass(classesToAdd).append(icon);

            if (confirmationValue !== '') {
                let  confirmationWidget = this.questionnaire.questionViews[this.questionnaire.questionViews.indexOf(this.question) + 1].widget,
                     passInValue = {
                        keyCode: e.keyCode,
                        target: confirmationTextBox
                    };
                confirmationWidget.keyPressed(passInValue);
            }


            super.sanitize(e);
            parent = null;
        }
    }
}
TempPasswordTextBox.prototype.input = 'DEFAULT:LogpadPasswordTextBox';
window.LF.Widget.TempPasswordTextBox = TempPasswordTextBox;
