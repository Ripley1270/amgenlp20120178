/**
 * This function determines the items to be displayed on the Review Screen for AM1+ diary.
 * @param {Object} widget Review screen widget option.
 * @param {Function} callback The function to be invoked with array of Review screen
 * item.
 */
LF.Widget.ReviewScreen.AM1PlusScreenFunction1 = function (widget, callback) {
    // let logger = Log4js.loggers['Widget.ReviewScreen.AM1Plus'] || new LF.Log.Logger('Widget.ReviewScreen.AM1Plus');

    if (!LF.Wrapper.Utils.eSense.pluginExists()) {
        //    logger.log(Log4js.Level.ERROR, 'eSense plugin is missing');

        LF.Actions.notify({ message: 'PLUGIN_MISSING' }, function () {
            localStorage.setItem('questionnaireToDashboard', true);
            LF.router.navigate('dashboard', true);
        });

        return;
    }

    LF.Wrapper.Utils.eSense.checkPaired('PEF', function () {
        let stringsToFetch = {
            text0: {
                namespace: 'P_AM1PlusDiary',
                key: 'AM1_INSTRUCTION_TURN_OFF'
            },
            text2: {
                namespace: 'P_AM1PlusDiary',
                key: 'AM1_INSTRUCTION_ENVELOPE'
            },
            text4:  {
                namespace: 'P_AM1PlusDiary',
                key: 'AM1_INSTRUCTION_TAP'
            }
        };

        LF.getStrings(stringsToFetch, function (strings) {
            let items = [];
            items.push({ text: [] });
            items[0].text[0] = strings.text0;
            items[0].text[1] = 'icon-am1-turn-off';
            items[0].text[2] = strings.text2;
            items[0].text[3] = 'icon-am1-envelope';
            items[0].text[4] = strings.text4;

            callback(items);
        });
    });
};

/**
 * This function determines the items to be displayed on the Review Screen for AM1+ diary.
 * @param {Object} widget Review screen widget option.
 * @param {Function} callback The function to be invoked with array of Review screen
 * item.
 */
LF.Widget.ReviewScreen.AM1PlusScreenFunction2 = function (widget, callback) {
    let i, j,
        result = [],
        IG = 'eS2_PEF',
        currentIGR = LF.Utilities.getCurrentIGR(IG),
        battery = LF.Utilities.queryAnswersByIGITAndIGR('eS2_Perf', 'BATLVL1N', 0)[0].get('response'),
        pad = LF.Utilities.zeroPad,
        formatDate = function (date) {
            // eslint-disable-next-line prefer-template
            return pad(date.getMonth() + 1, 2) + '/' + pad(date.getDate(), 2) + '/' + date.getFullYear() + ' ' + pad(date.getHours(), 2) + ':' + pad(date.getMinutes(), 2);
        };

    for (i = 1; i < currentIGR; i ++) {
        j = i - 1;
        result[j] = { text: [] };
        result[j].text[0] = i;
        // Record Time
        result[j].text[1] = formatDate(new Date(LF.Utilities.queryAnswersByIGITAndIGR(IG, 'PEF_RD1S', i)[0].get('response')));
        // Battery Level
        result[j].text[2] = battery;
        // PEF value
        result[j].text[3] = LF.Utilities.queryAnswersByIGITAndIGR(IG, 'PEFVAL1N', i)[0].get('response');
        // FEV1 Value
        result[j].text[4] = LF.Utilities.queryAnswersByIGITAndIGR(IG, 'FEVVAL1N', i)[0].get('response');
    }

    // callback with results to be displayed on the review screen.
    callback(result);
};

/**
 * Populates several objects of the widget of the question 'AM1PLUS_Q1' with the new set of data from AM1+ device.
 * @param {Object} records The records retrieved from AM1+ device.
 * @param {Number} battery The battery level.
 * @param {Function} callback The callback function to continue app life cycle
 */
LF.Widget.ReviewScreen.populateWithAM1PlusRecords = function (records, battery, callback) {
    let i, length, eS2_Perf, eS2_PEF, eS2_PEF_Perf,
        timeWindow = LF.Utilities.getNested('LF.StudyDesign.eSenseConfig.AM1Plus.timeWindow') || LF.CoreSettings.eSense.AM1Plus.timeWindow,
        now = new Date(),
        maxPEF = 0;

    for (i = 0, length = records.length; i < length; i ++) {
        maxPEF = (records[i].PEF > maxPEF) ? records[i].PEF : maxPEF;
    }

    LF.Wrapper.Utils.eSense.getPairedDevice('PEF', function (device) {
        eS2_Perf = {
            SERNUM1C: device.get('serial'),
            TRANMIT1S: now.ISOStamp(),
            BATLVL1N: battery,
            DEVTYP1C: device.get('type')
        };

        eS2_PEF = [];
        for (i = 0, length = records.length; i < length; i ++) {
            eS2_PEF.push({
                PEF_RD1S: new Date(records[i].TIME).ISOStamp(),
                PEFVAL1N: records[i].PEF,
                FEVVAL1N: records[i].FEV1,
                //true equivalent for Study Works
                VALID_1B: '1'
            });
        }

        eS2_PEF_Perf = {
            PEFBLW1N: records.length,
            // eslint-disable-next-line prefer-template
            PEFSTR1S: LF.Utilities.zeroPad(timeWindow.startHour) + ':' + LF.Utilities.zeroPad(timeWindow.startMinute),
            // eslint-disable-next-line prefer-template
            PEFEND1S: LF.Utilities.zeroPad(timeWindow.endHour) + ':' + LF.Utilities.zeroPad(timeWindow.endMinute),
            MAXPEF1N: maxPEF
        };

        !!records[0] && localStorage.setItem('LATEST_AM1PLUS_RECORD', JSON.stringify(records[0]));

        LF.Widget.ReviewScreen.populateAnswers(eS2_Perf, eS2_PEF, eS2_PEF_Perf);

        callback();
    });
};

/**
 * Populates the AM1+ diary answers with the appropriate IGs and ITs
 * @param {Object} eS2_Perf The data to be sent to Study Works with the eS2_Perf IG
 * @param {Array} eS2_PEF The data to be sent to Study Works with the eS2_PEF IG
 * @param {Object} eS2_PEF_Perf The data to be sent to Study Works with the eS2_PEF_Perf IG
 */
LF.Widget.ReviewScreen.populateAnswers = function (eS2_Perf, eS2_PEF, eS2_PEF_Perf) {
    let i, l, currentIGR,
        questionnaireId = 'P_AM1PlusDiary',
        pefIG = 'eS2_PEF';

    _(eS2_Perf).each(function (value, key) {
        LF.Utilities.addIT({
            question_id: 'AM1PLUS_Q1',
            questionnaire_id: questionnaireId,
            IG: 'eS2_Perf',
            IGR: 0,
            IT: key,
            response: value
        });
    });

    for (i = 0, l = eS2_PEF.length; i < l; i ++) {
        currentIGR = LF.Utilities.getCurrentIGR(pefIG);

        _(eS2_PEF[i]).each(function (value, key) {
            LF.Utilities.addIT({
                question_id: 'AM1PLUS_Q2',
                questionnaire_id: questionnaireId,
                IG: pefIG,
                IGR: currentIGR,
                IT: key,
                response: value
            });
        });

        LF.Utilities.incrementIGR(pefIG);
    }

    _(eS2_PEF_Perf).each(function (value, key) {
        LF.Utilities.addIT({
            question_id: 'AM1PLUS_Q3',
            questionnaire_id: questionnaireId,
            IG: 'eS2_PEF_Perf',
            IGR: 0,
            IT: key,
            response: value
        });
    });
};

/**
 * Removes the looping answers for the IG: eS2_PEF
 */
LF.Widget.ReviewScreen.removePEFAnswers = function () {
    let IG = 'eS2_PEF',
        currentIGR = LF.Utilities.getCurrentIGR(IG) - 1;

    while (currentIGR > 0) {
        LF.Utilities.removeLoopEvent(IG, currentIGR);
        LF.Utilities.decrementIGR(IG);
        currentIGR --;
    }

};

/**
 * Helper function for the nested function calls for connecting to eSense device
 * @param {Function} callback The callback function to continue app life cycle
 */
LF.Widget.ReviewScreen.connectESense = function (callback) {
    LF.Wrapper.Utils.eSense.checkSupport(function () {
        LF.Wrapper.Utils.eSense.enable(function () {
            LF.Wrapper.Utils.eSense.checkPaired('PEF', function (device) {
                LF.Wrapper.Utils.eSense.connect(function () {
                    (callback || $.noop)();
                }, null, device);
            });
        });
    });
};

/**
 * Helper function for the nested function calls for disconnecting from and disabling eSense device
 * @param {Function} callback The callback function to continue app life cycle
 */
LF.Widget.ReviewScreen.disconnectESense = function (callback) {
    LF.Wrapper.Utils.eSense.disconnect(function () {
        LF.Wrapper.Utils.eSense.turnOffBluetooth(callback);
    });
};

/**
 * Converts the battery state of AM1+ to number.
 * B stands for Bad and is converted to 0. G stands for Good and is converted to 100.
 * @param {String} battery state
 * @returns {Number} battery level
 */
LF.Widget.ReviewScreen.convertBatteryToNumber = function (battery) {
    // let logger = Log4js.loggers['Widget.ReviewScreen.convertBatteryToNumber'] || new LF.Log.Logger('Widget.ReviewScreen.convertBatteryToNumber');
    switch (battery) {
        case 'B':
            return 0;
        case 'G':
            return 100;
        default:
            //    logger.log(Log4js.Level.ERROR, 'Unexpected battery value received: ' + battery);
            return 0;
    }
};

/**
 * Click handler for the 'Get all records' button. Gets all records and navigates the app to screen 'AM1PLUS_S_2'
 * @param {Object} params An Object which contains configuration for the button clicked
 * @param {Function} callback The function needs to be called to finish the Rule which triggered this action.
 */
LF.Widget.ReviewScreen.getAllRecords = function (params, callback) {
    LF.Widget.ReviewScreen.removePEFAnswers();

    localStorage.setItem('AM1PLUS_CLICKED_BUTTON', 0);

    LF.Actions.displayMessage('PLEASE_WAIT', function () {
        LF.Widget.ReviewScreen.connectESense(function () {
            let onError = function () {
                LF.Widget.ReviewScreen.disconnectESense(function () {
                    LF.Actions.removeMessage();

                    LF.Actions.notify({
                        header: 'ERROR_TITLE',
                        message: ['BLUETOOTH_SYNC_FAILURE_TOP', 'BLUETOOTH_SYNC_FAILURE_MIDDLE']
                    }, callback);
                });
            };

            LF.Wrapper.Utils.eSense.getBatteryState(function (battery) {
                LF.Wrapper.Utils.eSense.getAllRecords(function (records) {
                    LF.Widget.ReviewScreen.disconnectESense(function () {
                        battery = LF.Widget.ReviewScreen.convertBatteryToNumber(battery);

                        LF.Widget.ReviewScreen.populateWithAM1PlusRecords(records, battery, function () {
                            LF.Actions.removeMessage();
                            LF.Utilities.navigateToScreen('AM1PLUS_S_2', callback);
                        });
                    });
                }, onError);
            }, onError);
        });
    });
};

/**
 * Click handler for the 'Get latest records' button. Gets 5 latest records and navigates the app to screen 'AM1PLUS_S_2'
 * @param {Object} params An Object which contains configuration for the button clicked
 * @param {Function} callback The function needs to be called to finish the Rule which triggered this action.
 */
LF.Widget.ReviewScreen.getLatestRecords = function (params, callback) {
    LF.Widget.ReviewScreen.removePEFAnswers();

    localStorage.setItem('AM1PLUS_CLICKED_BUTTON', 1);

    LF.Actions.displayMessage('PLEASE_WAIT', function () {
        LF.Widget.ReviewScreen.connectESense(function () {
            let onError = function () {
                LF.Widget.ReviewScreen.disconnectESense(function () {
                    LF.Actions.removeMessage();

                    LF.Actions.notify({
                        header: 'ERROR_TITLE',
                        message: ['BLUETOOTH_SYNC_FAILURE_TOP', 'BLUETOOTH_SYNC_FAILURE_MIDDLE']
                    }, callback);
                });
            };

            LF.Wrapper.Utils.eSense.getBatteryState(function (battery) {
                LF.Wrapper.Utils.eSense.getLatestRecords(function (records) {
                    LF.Widget.ReviewScreen.disconnectESense(function () {
                        battery = LF.Widget.ReviewScreen.convertBatteryToNumber(battery);

                        LF.Widget.ReviewScreen.populateWithAM1PlusRecords(records, battery, function () {
                            LF.Actions.removeMessage();
                            LF.Utilities.navigateToScreen('AM1PLUS_S_2', callback);
                        });
                    });
                }, onError, 5);
            }, onError);
        });
    });
};

/**
 * Click handler for the 'Get new records' button. Gets the new records since the last time records have been retrieved
 * and navigates the app to screen 'AM1PLUS_S_2'
 * @param {Object} params An Object which contains configuration for the button clicked
 * @param {Function} callback The function needs to be called to finish the Rule which triggered this action.
 */
LF.Widget.ReviewScreen.getNewRecords = function (params, callback) {
    let latestRecord = localStorage.getItem('LATEST_AM1PLUS_RECORD');

    LF.Widget.ReviewScreen.removePEFAnswers();

    if (!!latestRecord) {
        localStorage.setItem('AM1PLUS_CLICKED_BUTTON', 2);
        //This localStorage value is used in the dynamic text which is displayed in the 2nd Review Screen
        localStorage.setItem('PREVIOUS_LATEST_AM1PLUS_RECORD', latestRecord);

        LF.Actions.displayMessage('PLEASE_WAIT', function () {
            LF.Widget.ReviewScreen.connectESense(function () {
                let onError = function () {
                    LF.Widget.ReviewScreen.disconnectESense(function () {
                        LF.Actions.removeMessage();

                        LF.Actions.notify({
                            header: 'ERROR_TITLE',
                            message: ['BLUETOOTH_SYNC_FAILURE_TOP', 'BLUETOOTH_SYNC_FAILURE_MIDDLE']
                        }, callback);
                    });
                };

                LF.Wrapper.Utils.eSense.getBatteryState(function (battery) {
                    LF.Wrapper.Utils.eSense.getNewRecords(function (records) {
                        LF.Widget.ReviewScreen.disconnectESense(function () {
                            battery = LF.Widget.ReviewScreen.convertBatteryToNumber(battery);

                            LF.Widget.ReviewScreen.populateWithAM1PlusRecords(records, battery, function () {
                                LF.Actions.removeMessage();

                                LF.Utilities.navigateToScreen('AM1PLUS_S_2', callback);
                            });
                        });
                    }, onError, JSON.parse(latestRecord));
                }, onError);
            });
        });
    } else {
        localStorage.setItem('AM1PLUS_CLICKED_BUTTON', 0);

        LF.Widget.ReviewScreen.getAllRecords(params, callback);
    }
};

/**
 * Click handler for the 'Delete records' button. Deleted all records from eSense device.
 * @param {Object} params An Object which contains configuration for the button clicked
 * @param {Function} callback The function needs to be called to finish the Rule which triggered this action.
 */
LF.Widget.ReviewScreen.deleteRecords = function (params, callback) {
    LF.Widget.ReviewScreen.removePEFAnswers();

    LF.Actions.displayMessage('PLEASE_WAIT', function () {
        LF.Widget.ReviewScreen.connectESense(function () {
            LF.Wrapper.Utils.eSense.deleteRecords(function () {
                LF.Widget.ReviewScreen.disconnectESense(function () {
                    LF.Actions.removeMessage();

                    LF.Actions.notify({
                        message: {
                            namespace: 'P_AM1PlusDiary',
                            key: 'ALL_RECORDS_DELETED'
                        }
                    }, callback);
                });
            }, function () {
                LF.Widget.ReviewScreen.disconnectESense(function () {
                    LF.Actions.removeMessage();

                    LF.Actions.notify({
                        header: 'ERROR_TITLE',
                        message: ['BLUETOOTH_SYNC_FAILURE_TOP', 'BLUETOOTH_SYNC_FAILURE_MIDDLE']
                    }, callback);
                });
            });
        });
    });
};

/**
 * Validate AM1+ Review Screen to make sure at least one record is retrieved from the eSense device.
 * @param {String} answer - Not useful for Review screen. Responses are fetched in the function
 * @param {Object} params - An object containing configuration parameters for the validation function
 * @param {Function} callback true - At least one record is retrieved from AM1+
 *                      false - No records available
 */
LF.Widget.ValidationFunctions.checkESenseData = function (answer, params, callback) {
    let answers = LF.Utilities.queryAnswers({ where: { question_id: 'AM1PLUS_Q2' } });
    // logger = Log4js.loggers['Widget.ValidationFunctions.checkESenseData'] || new LF.Log.Logger('Widget.ValidationFunctions.checkESenseData');

    if (!!answers && !!answers.length) {
        callback(true);
    } else {
        //logger.log(Log4js.Level.ERROR, 'RECORDS_REQUIRED');

        LF.getStrings('RECORDS_REQUIRED', function (string) {
            LF.Notify.Banner.show({
                text    : string,
                type    : 'error'
            });
        }, { namespace: 'P_AM1PlusDiary' });

        callback(false);
    }
};

/* Search for and display AM1 devices
 * @param {Object} params An Object which contains configuration for the button clicked
 * @param {Function} callback The function needs to be called to finish the Rule which triggered this action.
 */
LF.Widget.ReviewScreen.getAM1Devices = function (params, callback) {
    //  let logger = Log4js.loggers['Widget.ReviewScreen.getAM1Devices'] || new LF.Log.Logger('Widget.ReviewScreen.getAM1Devices');

    LF.Actions.displayMessage('PLEASE_WAIT', function () {
        LF.Wrapper.Utils.eSense.enable(function () {
            plugin.eSense.findDevices(
                function (devices) {
                    LF.Wrapper.Utils.eSense.turnOffBluetooth(function () {
                        LF.Actions.removeMessage();
                        if (!devices.length) {
                            LF.Actions.notify({
                                header  : 'ERROR_TITLE',
                                message : 'BLUETOOTH_REG_NO_DEVICES_FOUND'
                            });
                            //  logger.log(Log4js.Level.ERROR, 'No devices of the supported types were found');
                        }
                        LF.Utilities.removeScreenFromStack('PAIRING_DIARY_S_3');
                        LF.Data.Questionnaire.am1DeviceList = devices;
                        LF.Utilities.navigateToScreen('PAIRING_DIARY_S_3', callback);
                    });
                }, function () {
                    //logger.log(Log4js.Level.ERROR, 'Error searching for devices: '  + error);
                    LF.Actions.removeMessage();
                }, {
                    deviceType: 'AM1',
                    timeout: LF.Utilities.getNested('LF.StudyDesign.eSenseConfig.findDevicesTimeout') || LF.CoreSettings.eSense.findDevicesTimeout,
                    firstOnly: false
                }
            );
        }, function () {
            localStorage.setItem('questionnaireToDashboard', true);
            LF.router.navigate('dashboard', true);
        });
    });
};

/**
 * Pair with the selected device
 * @param {Object} params An Object which contains configuration for the button clicked
 * @param {Function} callback The function needs to be called to finish the Rule which triggered this action.
 */
LF.Widget.ReviewScreen.pairAM1Device = function (params, callback) {
    let am1Device, am1DeviceName, am1DeviceAddress, continueRegistration,
       // logger = Log4js.loggers['Widget.ReviewScreen.pairAM1Device'] || new LF.Log.Logger('Widget.ReviewScreen.pairAM1Device'),
        selectedDeviceName = params.item.text().trim(),
        showError = function () {
            LF.Wrapper.Utils.eSense.turnOffBluetooth(function () {
                LF.Actions.removeMessage();

                LF.Actions.notify({
                    header  : 'ERROR_TITLE',
                    message : 'BLUETOOTH_PAIRING_FAILED'
                }, function () {
                    LF.Wrapper.Utils.eSense.unPairDevice(am1DeviceAddress, $.noop);
                });
            });
        },
        convertBatteryLevelToNumber = function (level) {
            // AM1 returns only B or G value for battery state so to be consistent we map it to 0 and 100
            switch (level) {
                case 'B':
                    return '0';
                case 'G':
                    return '100';
                default:
                    if (level === parseInt(level, 10)) {
                        return level.toString();
                    } else {
                        //      logger.log(Log4js.Level.ERROR, 'Unexpected battery value received: ' + level);
                        return '0';
                    }
            }
        };

    am1Device = _.find(params.question.widget.reviewScreenList, function (listItem) {
        return listItem.text[0] === selectedDeviceName;
    });

    if (am1Device) {
        am1DeviceName = am1Device.text[0];
        am1DeviceAddress = am1Device.text[1];

        LF.Actions.displayMessage('PLEASE_WAIT');

        LF.Wrapper.Utils.eSense.enable(function () {
            plugin.eSense.connect(function () {
                let now;

                plugin.eSense.setTime(function () {
                    plugin.eSense.getSerialNumber(function (serialNumber) {
                        LF.Data.Questionnaire.serialNumber = serialNumber;

                        continueRegistration = function () {
                            LF.Actions.displayMessage('PLEASE_WAIT');

                            LF.Wrapper.Utils.eSense.getBatteryState(function (batteryLevel) {
                                // eSense Device Serial Number
                                LF.Utilities.addIT({
                                    question_id : 'PAIRING_DIARY_Q_3',
                                    questionnaire_id : 'Pairing_Diary',
                                    response : serialNumber,
                                    IG : 'eS2_Perf',
                                    IGR : 0,
                                    IT : 'SERNUM1C'
                                });

                                // eSense Device Battery Level
                                LF.Utilities.addIT({
                                    question_id : 'PAIRING_DIARY_Q_3',
                                    questionnaire_id : 'Pairing_Diary',
                                    response : convertBatteryLevelToNumber(batteryLevel),
                                    IG : 'eS2_Perf',
                                    IGR : 0,
                                    IT : 'BATLVL1N'
                                });

                                // eSense Device Type
                                LF.Utilities.addIT({
                                    question_id : 'PAIRING_DIARY_Q_3',
                                    questionnaire_id : 'Pairing_Diary',
                                    response : 'PEF',
                                    IG : 'eS2_Perf',
                                    IGR : 0,
                                    IT : 'DEVTYP1C'
                                });

                                // eSense Device Serial Number
                                LF.Utilities.addIT({
                                    question_id : 'PAIRING_DIARY_Q_3',
                                    questionnaire_id : 'Pairing_Diary',
                                    response : serialNumber,
                                    IG : 'eS2_Registration',
                                    IGR : 0,
                                    IT : 'CURSER1C'
                                });

                                // If the device is pre-paired. LogPad App does not support pre-pairing, so this is 0 (false).
                                LF.Utilities.addIT({
                                    question_id : 'PAIRING_DIARY_Q_3',
                                    questionnaire_id : 'Pairing_Diary',
                                    response : '0',
                                    IG : 'eS2_Registration',
                                    IGR : 0,
                                    IT : 'ISPAIR1B'
                                });

                                now = new Date().ISOStamp();

                                LF.Data.Questionnaire.pairedDevice = {
                                    name       : am1DeviceName,
                                    address    : am1DeviceAddress,
                                    type       : 'PEF',
                                    pairDate   : now,
                                    serial     : serialNumber
                                };

                                LF.Utilities.addIT({
                                    question_id : 'PAIRING_DIARY_Q_3',
                                    questionnaire_id : 'Pairing_Diary',
                                    response : now,
                                    IG : 'eS2_Perf',
                                    IGR : 0,
                                    IT : 'TRANMIT1S'
                                });

                                // eSense Registration Time
                                LF.Utilities.addIT({
                                    question_id : 'PAIRING_DIARY_Q_3',
                                    questionnaire_id : 'Pairing_Diary',
                                    response : now,
                                    IG : 'eS2_Registration',
                                    IGR : 0,
                                    IT : 'REGIST1S'
                                });

                                LF.Wrapper.Utils.eSense.onESensePaired(am1DeviceName, function () {
                                    let onComplete = function () {
                                        LF.Data.Questionnaire.devicePaired = true;

                                        LF.Actions.notify({
                                            header: 'BLUETOOTH_REG_TITLE',
                                            message: 'BLUETOOTH_PAIRING_SUCCESS'
                                        }, function () {
                                            LF.Utilities.navigateToScreen('SITEUSER', callback);
                                        });
                                    };

                                    // logger.log(Log4js.Level.OPERATIONAL, 'Successfully paired with eSense device: ' + am1DeviceName);

                                    plugin.eSense.disconnect(
                                        function () {
                                            logger.log(Log4js.Level.OPERATIONAL, `Disconnected from eSense device: ${am1DeviceName}`);
                                            LF.Wrapper.Utils.eSense.turnOffBluetooth(onComplete);
                                        }, function () {
                                            //      logger.log(Log4js.Level.ERROR, 'Disconnecting from eSense device ' + am1DeviceName + ' failed: ' + error);
                                            LF.Wrapper.Utils.eSense.turnOffBluetooth(onComplete);
                                        }
                                    );
                                });
                            }, function () {
                                showError();
                                //   logger.log(Log4js.Level.ERROR, 'Error getting ESenseDevice battery level.');
                            });
                        };

                        LF.Actions.confirm({
                            message: 'CONFIRM_DEVICE_SERIAL',
                            ok: 'YES',
                            cancel: 'NO'
                        }, function (result) {
                            if (result) {
                                continueRegistration();
                            } else {
                                LF.Wrapper.Utils.eSense.unPairDevice(am1DeviceAddress, function () {
                                    LF.Wrapper.Utils.eSense.turnOffBluetooth(LF.Actions.removeMessage);
                                });
                            }
                        });
                    }, function () {
                        showError();
                        //   logger.log(Log4js.Level.ERROR, 'Error getting serial number: ' + error);
                    },
                    LF.Utilities.getNested('LF.StudyDesign.eSenseConfig.apiCallTimeout') || LF.CoreSettings.eSense.apiCallTimeout);
                }, function () {
                    showError();
                    //  logger.log(Log4js.Level.ERROR, 'Error setting time on device: ' + error);
                },
                new Date().ISOStamp(),
                LF.Utilities.getNested('LF.StudyDesign.eSenseConfig.apiCallTimeout') || LF.CoreSettings.eSense.apiCallTimeout);
            }, function () {
                showError();
                // logger.log(Log4js.Level.ERROR, 'Error pairing with device: ' + error);
            },
            am1DeviceAddress,
            true,
            LF.Utilities.getNested('LF.StudyDesign.eSenseConfig.apiCallTimeout') || LF.CoreSettings.eSense.apiCallTimeout);
        }, function () {
            showError();
            //logger.log(Log4js.Level.ERROR, 'Error enabling Bluetooth ' + error);
        });
    }
};

/**
 * This function determines the items to be displayed on the Review Screen for AM1+ diary.
 * @param {Object} widget Review screen widget option.
 * @param {Function} callback The function to be invoked with array of Review screen
 * item.
 */
LF.Widget.ReviewScreen.AM1PlusPairingScreenFunction1 = function (widget, callback) {
    LF.Widget.ReviewScreen.updateAM1DeviceList();

    LF.getStrings('PAIRED', function (pairedText) {
        let i, len, result,
            devices = LF.Data.Questionnaire.am1DeviceList,
            pairedDevice = LF.Data.Questionnaire.pairedDevice;

        if (devices && devices.length) {
            result = [];

            for (i = 0, len = devices.length; i < len; i ++) {
                result[i] = {text: []};
                result[i].text[0] = devices[i].name;
                result[i].text[1] = devices[i].address;

                if (pairedDevice && devices[i].address === pairedDevice.address) {
                    result[i].text[0] += ` - ${pairedText}`;
                }
            }
        }

        callback(result);
    }, { namespace: 'P_Pairing_Diary' });
};

/**
 * This function adds paired device to LF.Data.Questionnaire.am1DeviceList
 * if the list is empty or the paired device is not in the list
 */
LF.Widget.ReviewScreen.updateAM1DeviceList = function () {
    let i, length,
        isPairedDeviceInTheList = false,
        pairedDevice = LF.Data.Questionnaire.pairedDevice,
        devices = LF.Data.Questionnaire.am1DeviceList,
        addPairedDevice = function () {
            LF.Data.Questionnaire.am1DeviceList.unshift(JSON.parse(JSON.stringify(pairedDevice)));
        };

    if (devices && devices.length && LF.Data.Questionnaire.devicePaired && pairedDevice) {
        for (i = 0, length = devices.length; i < length; i ++) {
            if (devices[i].address === pairedDevice.address) {
                isPairedDeviceInTheList = true;
                break;
            }
        }

        if (!isPairedDeviceInTheList) {
            addPairedDevice();
        }
    } else if (LF.Data.Questionnaire.devicePaired && pairedDevice) {
        addPairedDevice();
    }
};

/**
 * Validate AM1+ Review Screen to make sure at least one AM1 device has been paired
 * @param {String} answer - Not useful for Review screen. Responses are fetched in the function
 * @param {Object} params - An object containing configuration parameters for the validation function
 * @param {Boolean} callback - At least one device has been paired
 *                      false - No devices have been paired
 */
LF.Widget.ValidationFunctions.checkForPairedDevice = function (answer, params, callback) {
    //let logger = Log4js.loggers['Widget.ValidationFunctions.checkForPairedDevice'] || new LF.Log.Logger('Widget.ValidationFunctions.checkForPairedDevice');

    if (LF.Data.Questionnaire.devicePaired) {
        callback(true);
    } else {
        // logger.log(Log4js.Level.ERROR, 'NO_PAIRED_DEVICE');

        LF.getStrings('NO_PAIRED_DEVICE', function (string) {
            LF.Notify.Banner.show({
                text    : string,
                type    : 'error'
            });
        }, { namespace: 'P_Pairing_Diary' });

        callback(false);
    }
};
