
import ELF from 'core/ELF';
import Sound from 'core/classes/Sound';
import Answers from 'core/collections/Answers';
import { MessageRepo, Banner } from 'core/Notify';
import NotifyView from 'core/views/NotifyView';
import Logger from 'core/Logger';

const { View } = Backbone;
const logger = new Logger('WidgetBase');

export default class WidgetBase extends View {
    constructor (options) {
        let className,
            swAlias,
            answers;

        /**
         * Handles all application sounds.
         * @type  LF.Class.Sound
         */
        this.sound = new Sound();

        super(options);

        className = this.model.get('className') || this.className;

        this.options = options;
        this.parent = this.options.parent;
        this.answers = new Answers();
        this.mandatory = this.options.mandatory || false;
        this.completed = false;
        this.paramsSetup = false;

        LF.Widget.ParamFunctions.subject = this.questionnaire.subject;

        // If the questionnaire is in edit mode...
        if (this.questionnaire.editMode) {
            let curIGR = this.parent.ig;
            let IGRNumber = this.questionnaire.data.currentIGR[curIGR];
            swAlias = `${this.getQuestion().model.get('IG')}.${IGRNumber}.${(this.getQuestion().model.get('IT'))}`;

            answers = this.questionnaire.data.answers.match({
                SW_Alias: swAlias
            });

            if (answers) {
                this.answers.add(answers);

                this.answers.each((answer) => {
                    this.respondHelper(answer, answer.get('response'), true);
                });
            }
        }

        //Set the class name of the widget
        this.$el.attr('class', className);
    }

    get questionnaire () {
        return this.question.getQuestionnaire();
    }

    toString () {
        return `${this.constructor.name}:${this.id}`;
    }

    /**
     * This function is a workaround for an MS Edge issue.
     * See https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/4983269/
     * @param {Event} e - The event args
     * @returns {number} - The appropriate pageX for the platform.
     */
    getPageX (e) {
        let ev = this.getClickOrTouchEvent(e);
        let pageX = ev.pageX;

        if (ev.clientX != null && window.scrollX != null) {
            pageX = ev.clientX + window.scrollX;
        }

        return pageX;
    }

    getClickOrTouchEvent (e) {
        return e.touches ? e.touches[e.touches.length - 1] : e;
    }

    /**
     * This function is a workaround for an MS Edge issue.
     * See https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/4983269/
     * @param {Event} e - The event args
     * @returns {number} - The appropriate pageY for the platform.
     */
    getPageY (e) {
        let ev = this.getClickOrTouchEvent(e);
        let pageY = ev.pageY;

        if (ev.clientY != null && window.scrollY != null) {
            pageY = ev.clientY + window.scrollY;
        }

        return pageY;
    }

    /**
     * Iterates through the widget's model looking for functions used to set
     * the model attributes.
     * @returns {Q.Promise}
     */
    setupModel () {
        // This is a temporary workaround for deprecating the answerFunc attribute for multiple choice widgets.
        if (this.model.has('answerFunc')) {
            logger.warn('answerFunc has been deprecated.  Please set the answers attribute to be your answers setup function.');
            // Copy to answers:
            this.model.set('answers', this.model.get('answerFunc'));
            // Remove the answerFunc:
            this.model.unset('answerFunc', { silent: true });
        }

        // This is a temporary workaround for deprecating the configFunc attribute for multiple choice widgets.
        if (this.model.has('configFunc') && !this.paramsSetup) {
            logger.warn('configFunc has been deprecated.  Please set individual attributes to functions.');
            // Copy to configuration (for time widgets and things that use 'configuration' model property):
            this.model.set('configuration', this.model.get('configFunc'));
            // DO NOT remove configFunc from model.  Just escape if you run into it in processAttributes.
        }

        let attrs = this.model.attributes;

        // Iterate through the attributes.  If any of the attributes are functions, add them to the promise factory,
        // which will be executed at the end of the function:
        let aFactory = this.processAttributes(attrs);

        // Reduce the array and execute the promises:
        return aFactory.reduce(Q.when, Q()).then(() => {
            this.paramsSetup = true;
        });
    }

    /**
     * Process the given array of model attributes, running any functions
     * to set the attribute value, or recursing on any attribute that is an object.
     * We'll let any errors from the functions bubble up the core, which will halt the rendering of the widget.
     * @param {Array} attrs - Array of attributes to parse
     * @param {string} [namespace=''] - dot-delimited path to the current point in the object.  Used to create a unique
     * string for caching functions.
     * @returns {Array} - An array of Promises
     */
    processAttributes (attrs, namespace = '') {
        let aFactory = [],
            attrKeys = _.keys(attrs);

        this.cachedFunctions = this.cachedFunctions || {};

        for (let i = 0; i < attrKeys.length; i++) {
            let key = attrKeys[i],
                fullKeyPath = namespace === '' ? key : `${namespace}.${key}`,
                attribute = attrs[key];

            if (key === 'validationFunc' || key === 'configFunc') {
                continue;
            }

            let runner;
            if (this.paramsSetup) {
                // If the screen has already been rendered, look for a cached function and run it if it exists
                // Only run cached functions, in case objects and other functions were stored in the model by the widget
                runner = this.cachedFunctions[fullKeyPath];
            } else if (typeof attribute === 'function') {
                runner = attribute;
            } else if (typeof attribute === 'string') {
                runner =  LF.Widget.ParamFunctions[attribute];
            }

            if (typeof runner === 'function') {
                // cache the property in cachedFunctions object to be run if the screen is re-rendered.
                this.cachedFunctions[fullKeyPath] = runner;
                aFactory.push(() => {
                    return Q()
                    .then(() => {
                        return runner.call(this);
                    })
                    .then((val) => {
                        attrs[key] = val;
                    });
                });
            }
            // use constructor to ensure that this is a simple object, not a constructed model or RegExp or something
            // which may happen to the model when returning to a screen that has already been rendered.
            else if (attribute && (_.isArray(attribute) || attribute.constructor === Object)) {
                aFactory = aFactory.concat(this.processAttributes(attribute, fullKeyPath));
            }
        }

        return aFactory;

    }

    /**
     * Gets the question (parent) of this widget.
     * @returns {QuestionView}
     */
    getQuestion () {
        return this.question;
    }

    /**
     * gets the parent question of this widget
     * @returns {*}
     */
    get question () {
        return this.parent;
    }

    /**
     * fired when value is set. saves the UI Control value into the answer associated with the widget
     * @abstract
    * @return {Q.Promise<void>} promise resolved when complete.
    */
    respond () {
        logger.error('unexpected call to WidgetBase.respond().  Should be overridden by extending class.');
        return Q();
    }

    /**
     * Responsible for displaying the widget.
     * @abstract
     * @returns {Q.Promise<void>} resolved when draw is complete
     */
    render () {
        logger.error('unexpected call to WidgetBase.render().  Should be overridden by extending class.');
        return Q();
    }

    /**
     * Respond to user input and answer the question.
     * @param {Object} model The answer record to modify.
     * @param {String} [response=null] The response to be stored.
     * @param {Boolean} [completed=true] True if the response should set the widget to a completed state.
     * @return {Q.Promise<void>} promise resolved when complete.
     * @example LF.Widget.Base.prototype.respond.call(this, this.answer, "25", true);
     * @protected
     */
    respondHelper (model, response = null, completed = true) {
        // Set the answers numeric value to the target's value.
        model.set({ response });

        this.saveAnswerType(model);

        //prepopulate widget internal answer
        this.answer = model;

        if (model.get('question_id') === 'AFFIDAVIT') {
            if (this.isAffidavitAnswered()) {
                logger.operational(`Diary ${model.get('questionnaire_id')} signed.`, {
                    diary: model.get('questionnaire_id')
                });
            } else {
                logger.operational(`Diary ${model.get('questionnaire_id')} unsigned.`, {
                    diary: model.get('questionnaire_id')
                });
            }
        }

        this.completed = completed;

        if (this.completed && this.skipped) {
            this.skipped = false;
        }

        this.trigger('response');

        return ELF.trigger(`QUESTIONNAIRE:Answered/${this.getQuestion().getQuestionnaire().id}/${this.question.id}`, {
            questionnaire: this.questionnaire.id,
            screen: this.options.screen,
            question: this.question.id,
            answer: this.answers
        }, this.question.getQuestionnaire());
    }

    /**
     * Validate the widget.
     * @return {Boolean} Returns true if the widget is valid, otherwise false
     * @example this.validate();
     */
    validate () {
        return !this.mandatory || this.completed;
    }

    /**
     * Figures out which validation errors have failed, and throws an error for the proper one.
     * @returns {Q.Promise<boolean>} true if an error message was actually displayed here.
     */
    displayError () {
        // eslint-disable-next-line consistent-return
        return Q.Promise((resolve) => {
            let validationErrors = this.model.attributes.validationErrors,
                currentError = null,
                notifyStrings = {};

            if (this.validate()) {
                resolve(false);
            }

            // Validation errors not in the model.  Create an empty array so it will act like it didn't find one.
            if (!_.isArray(validationErrors)) {
                validationErrors = [];
            }

            for (let i = 0, length = validationErrors.length; i < length; ++i) {
                // Must be an actual boolean false.  Ignore undefined.
                if (this[validationErrors[i].property] === false) {
                    currentError = validationErrors[i];
                    break;
                }
            }

            // Error doesn't match any of our passed in validation errors.  Throw a generic message instead.
            if (currentError === null) {
                logger.warn(`REQUIRED_ANSWER QuestionnaireID: ${this.getQuestion().getQuestionnaire().id}, QuestionID: ${this.getQuestion().id}`);
                return MessageRepo.display(MessageRepo.Banner.REQUIRED_ANSWER)
                .then(() => {
                    resolve(true);
                });
            }

            notifyStrings.header = {
                key: currentError.header || 'VALIDATION_HEADER',
                namespace: this.questionnaire.id
            };

            notifyStrings.body = {
                key: currentError.errString,
                namespace: this.questionnaire.id
            };

            notifyStrings.ok = {
                key: currentError.ok || 'VALIDATION_OK',
                namespace: this.questionnaire.id
            };

            logger.error(currentError.errString);

            LF.getStrings(notifyStrings)
            // eslint-disable-next-line consistent-return
            .then((strings) => {
                const getErrorMessage = type => {
                    const { messageKey, errString } = currentError;

                    // Can either pass a custom message key or use ${errString}_${questionnaire.id}
                    const key = messageKey || `${errString}_${this.questionnaire.id}`;

                    return MessageRepo[type] && MessageRepo[type][key];
                };

                switch (currentError.errorType) {
                    case 'alert':
                        alert(strings.body);
                        break;
                    case 'popup':
                        const dialog = getErrorMessage('Dialog');

                        // Since the strings are configured in the study,
                        //  core may or may not have a registered message here.
                        if (dialog) {
                            return MessageRepo.display(dialog);
                        } else {
                            return (new NotifyView()).show(strings);
                        }
                    default:
                        const banner = getErrorMessage('Banner');

                        // Since the strings are configured in the study,
                        //  core may or may not have a registered message here.
                        if (banner) {
                            return MessageRepo.display(banner);
                        } else {
                            return Banner.show({
                                text: strings.body,
                                type: 'error'
                            });
                        }
                }
            })
            .then(() => {
                resolve(true);
            });
        });
    }

    /**
     * Set a record as skipped.
     * @param {String} swAlias Skipped IT
     * @return {Boolean} Returns true if the skipped record is set, otherwise false.
     */
    skip (swAlias) {
        if (this.completed) {
            return false;
        }

        this.skipped = new (this.question.getQuestionnaire()).Answer();

        try {
            this.skipped.set({
                question_id: this.question.model.get('id'),
                questionnaire_id: this.question.getQuestionnaire().model.get('id'),
                response: '1',
                SW_Alias: swAlias
            });
        } catch (err) {
            // eslint-disable-next-line no-console
            console.log(err);
        }

        return true;
    }

    /**
     * Returns the value to be set as Type
     * @returns {null}
     */
    getAnswerType () {
        return null;
    }

    /**
     * Sets the answer type.
     * @param {Answer} model - The answer model
     */
    saveAnswerType (model) {
        let type = this.getAnswerType();

        if (type != null) {
            model.set({ type });
        }
    }

    /**
     * Add an answer record to the collection.
     * @param {Object} [params] An object literal
     * @example this.addAnswer();
     * @returns {Object}
     */
    addAnswer (params = {}) {
        let model,
            IG = this.question.model.get('IG'),
            swAlias;

        if (this.question.model.get('id') === 'AFFIDAVIT') {
            swAlias = params.krSig || this.question.model.get('krSig');
        } else {
            swAlias = `${IG}.${this.question.getQuestionnaire().getCurrentIGR(IG)}.${(params.IT ||
                this.question.model.get('IT'))}`;
        }

        try {
            let questionnaire = this.question.getQuestionnaire();

            model = new questionnaire.Answer({
                user_id: LF.security.activeUser ? LF.security.activeUser.get('id') : null,
                question_id: this.question.model.get('id'),
                questionnaire_id: questionnaire.model.get('id'),
                response: params.response || null,
                SW_Alias: swAlias
            });

            this.saveAnswerType(model);
            this.answers.add(model);
            this.questionnaire.data.answers.add(model);
        } catch (e) {
            // eslint-disable-next-line no-console
            console.log(e);
        }
        return model;
    }

    /**
     * remove an answer record from the collections.
     * @param {Object} model An answer model to be removed from the collection.
     * @example this.removeAnswer(model);
     */
    removeAnswer (model) {
        this.answers.remove(model);
        this.questionnaire.data.answers.remove(model);
        this.completed = false;
    }

    /**
     * remove all answer records from the collections.
     */
    removeAllAnswers () {
        for (let i = this.answers.length - 1; i >= 0; i--) {
            this.removeAnswer(this.answers.at(i));
        }
    }

    /**
     * Save the entire answer collection to the database
     * @return {Q.Promise<void>}
     */
    save () {
        // If a skipped record exists, save it.
        if (this.skipped) {
            return this.skipped.save();
        } else {
            return Q();
        }
    }

    /**
     * If the widget is used as an affidavit, returns whether the affidavit is answered or not
     * @return {Boolean} Returns true if the affidavit is answered
     * @example this.isAffidavitAnswered();
     * Moved the decision making logic to the widget space where it makes more sense.
     * By doing so evey widget can implement their own logic by overriding this function.
     * The behaviour in this function is the behaviour of the CheckBox widget/affidavit
     * and it was being used as the default behaviour for all widgets/affidavits so it is kept that way.
     */
    isAffidavitAnswered () {
        let res = this.answers.toJSON();
        return res && res[0].response === '1';
    }

    /**
     * An alias for LF.strings.display. See {@link core/collections/Languages Languages} for details.
     * @param {object|Array|string} strings The translations keys to translate.
     * @param {function} callback A callback function invoked when the strings have been translated.
     * @param {object} options Options passed into the translation.
     * @returns {Q.Promise}
     */
    i18n (strings, callback = (() => {}), options = {}) {
        return LF.strings.display.call(LF.strings, strings, callback, options);
    }

    /**
     * An alias for LF.templates.display. See {@link core/collections/Templates Templates} for details.
     * @param {string} name The name of the template to render.
     * @param {object} options The options passed to the template.
     * @returns {string} The rendered template.
     */
    renderTemplate (name, options) {
        return LF.templates.display(name, options);
    }
}

WidgetBase.prototype.tagName = 'div';
WidgetBase.prototype.className = 'widget';

window.LF.Widget.WidgetBase = WidgetBase;
