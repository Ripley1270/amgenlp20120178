/** #depends MultipleChoiceBase.js
 * @file Defines a radio button widget.
 * @author
 * <a href="mailto:chopkins@phtcorp.com">Corey Hopkins</a>
 * <a href="mailto:gsaricali@phtcorp.com">Gulden Saricali</a>
 * @version 1.4
 */

import MultipleChoiceBase from './MultipleChoiceBase';

class RadioButton extends MultipleChoiceBase {

    //noinspection JSMethodCanBeStatic
    get className () {
        return 'RadioButton';
    }

    //noinspection JSMethodCanBeStatic
    get wrapper () {
        return 'DEFAULT:RadioButtonWrapper';
    }

    //noinspection JSMethodCanBeStatic
    get input () {
        return 'DEFAULT:RadioButton';
    }

    //noinspection JSMethodCanBeStatic
    get label () {
        return 'DEFAULT:RadioButtonLabel';
    }

    /**
     * Constructs a RadioButton class.
     * @param {Object} options The options used to create the RadioButton
     * @param {Object} options.model The model containing the specific configuration for the widget.  This is represented by the "widget" property in the question definition in the study design.
     * @param {string} options.model.id {dev-only} The ID associated with the model.
     * @param {string} [options.model.className=RadioButton] {dev-only} The CSS classname applied to the widget.
     * @param {Array} options.model.answers The list of answer options used to render the widget.  Each element of the array should an object of the form <code>{ text: 'ONE', value: '1' }</code>
     * @param {Object} options.model.templates {dev-only} Contains any display templates to override the widget's defaults.
     * @param {string} [options.model.templates.wrapper=DEFAULT:RadioButtonWrapper] {dev-only} The template used to render the wrapper element for the widget.
     * @param {string} [options.model.templates.input=DEFAULT:RadioButton] {dev-only} The template used to render the input elements for the widget.
     * @param {string} [options.model.templates.label=DEFAULT:RadioButtonLabel] {dev-only} The template used to render the labels attached to the inputs of the widget.
     */
    constructor (options) {
        /**
         * List of the widgets events.
         * @readonly
         * @enum {Event}
         */
        this.events = {
            /** radio button input change event */
            'change input[type=radio]': 'respond'
        };

        this.options = options;
        super(options);
    }

    /**
     * Set up our answers.  Then figure out the configured default selected value and select it.
     * @returns {Q.Promise}
     */
    setupAnswers () {
        return Q()
        .then(() => {
            let answerOptions = this.model.get('answers'),
                curAnswer = this.completed && this.answers && this.answers.size() ?
                    this.answers.at(0).get('response') :
                    this.options.model.get('answer');

            this.removeAllAnswers();

            // curAnswer is either the cached answer from previously or the default answer.
            //      Verify it still exists in the answers collection, and select it if it does.
            if (curAnswer != null) {
                for (let i = 0, len = answerOptions.length; i < len; ++i) {
                    if (curAnswer === answerOptions[i].value) {
                        this.answer = this.addAnswer();
                        this.respondHelper(this.answer, curAnswer);
                        break;
                    }
                }
            }
        });
    }

    /**
     * Responsible for displaying the widget.
     * @returns {Q.Promise} resolved when element is finished being rendered
     */
    render () {
        return Q()
            .then(() => this.setupAnswers())
            .then(() => this.buildHTML('radio'))
            .then(() => Q.Promise((resolve) => {
                this.$el.appendTo(this.parent.$el)
                    .ready(() => {
                        resolve();
                    });
            }))
            .then(() => {
                this.setLabelHeight();
            })
            .then(() => {
                this.delegateEvents();
            })
            .then(() => {
                let promises = [],
                    answer = this.answer ? this.answer.get('response') : false,
                    modelId = this.model.get('id');

                if (answer) {
                    _(this.model.get('answers')).each((item, index) => {
                        if (answer === item.value) {
                            promises.push(
                                new Q.Promise((resolve) => {
                                    this.$(`input[id=${modelId}_radio_${index}]`)
                                        .attr('checked', true)
                                        .closest('.btn')
                                        .addClass('active')
                                        .ready(() => {
                                            resolve();
                                        });
                                })
                                .then(() => {
                                    // flush queue so trigger handler will have run
                                })
                            );
                        }
                    });
                }

                return Q.allSettled(promises);
            });
    }

    /**
     * Responds to user input.
     * @param {Event} e Event data
     * @returns {Q.Promise<void>}
     */
    respond (e) {
        let value = this.$(e.target).val();

        if (!this.answers.size()) {
            this.answer = this.addAnswer();
        }

        return this.respondHelper(this.answer, value);
    }
}

window.LF.Widget.RadioButton = RadioButton;
export default RadioButton;
