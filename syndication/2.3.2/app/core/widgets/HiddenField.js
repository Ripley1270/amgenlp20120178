import WidgetBase from './WidgetBase';

export default class HiddenField extends WidgetBase {
    constructor (options) {
        super(options);

        this.answer = this.addAnswer();

        let {field, value} = this.model.toJSON();

        let response = JSON.stringify({[field]: value});

        this.respondHelper(this.answer, response, true);
    }

    /**
     * The class of the widget.
     * @returns {string}
     */
    get className () {
        return 'hidden';
    }

    /**
     * There is nothing to render for this widget.  We just need an empty render method
     * to prevent WidgetBase from logging an error.
     * @returns {Q.Promise<void>}
     */
    render () {
        return Q();
    }
}

window.LF.Widget.HiddenField = HiddenField;

