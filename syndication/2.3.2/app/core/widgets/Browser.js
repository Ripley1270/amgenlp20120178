import WidgetBase from './WidgetBase';

const DEFAULT_TEMPLATE = 'DEFAULT:Browser';
const ANSWER_VALUE = '1';

class Browser extends WidgetBase {

    /**
     * The default template.
     * @returns {string}
     */
    get defaultTemplate () {
        return DEFAULT_TEMPLATE;
    }

    /**
     * Constructs the browser widget.
     * @param {Object} options - The widget options
     * @param {Object} options.model - The Backbone model class containing the widget attributes.
     * @param {string} options.model.url - The URL to set as the iframe source.
     * @param {string} [options.model.className] {dev-only} - The CSS classname applied to the widget container element.
     * @param {Object} options.model.templates - Collection of templates to be used to render the widget.
     * @param {string} options.model.linkText - The key for the NLS text string to be used as the link text for opening the new browser window.
     * @param {string} options.model.id {dev-only} - The ID of the widget.
     * @param {string} [options.model.templates.container=DEFAULT:Browser] - The container used to render the iframe.
     */
    constructor (options) {
        super(options);

        this.validateModel();

        /**
         * Indicates if the widget is completed.  This will be set to true when the provided URL renders.
         * @type Boolean
         * @default true
         */
        this.completed = false;

        this.events = {
            'click a': 'respond'
        };
    }

    /**
     * Renders the widget.
     * @returns {Q.Promise}
     */
    render () {
        /**
         * The templates used by the widget.
         * @type Object
         */
        this.templates = this.model.get('templates') || { container: this.defaultTemplate };

        /**
         * The text to use for the link to open the browser.
         * @type String
         */
        this.linkText = this.model.get('linkText');

        /**
         * The URL to provide to the iframe as its src attribute.
         * @type String
         */
        this.url = this.model.get('url');

        /**
         * The ID of the widget.
         * @type String
         */
        this.id = this.model.get('id');

        /**
         * The ID applied to the anchor element.
         * @type String
         */
        this.linkID = `${this.id}_a`;

        return LF.getStrings(this.linkText, $.noop, { namespace: this.question.getQuestionnaire().id })
            .then((link) => {
                return new Q.Promise((resolve) => {
                    let $a;
                    let divEl = this.renderTemplate(this.templates.container, {
                        linkText: link,
                        id: this.linkID
                    });

                    this.$el.empty();
                    this.$el.append(divEl);

                    this.$el.appendTo(this.parent.$el);
                    this.$el.trigger('create');

                    $a = this.$(`#${this.linkID}`);
                    $a.on('click', () => {
                        this.trigger('response');
                    });

                    $a.ready(() => { resolve(); });
                });
            })
            .then(() => {
                this.delegateEvents();
            });
    }

    /**
     * Validate the model, specifically the URL attribute.
     * If this does not exist, throw an exception.
     */
    validateModel () {
        if (!this.model.has('url')) {
            throw new Error('Browser Widget must have a URL attribute defined.');
        }
    }

    /**
     * Handles the user's interact with the widget.
     * @returns {Q.Promise}
     */
    respond () {        
        // If we are running on a device, us the InAppBrowser
        if (LF.Wrapper.isWrapped) {
            cordova.InAppBrowser.open(this.url, '_blank', 'location=no');
        } else {
            window.open(this.url, '_blank', 'location=no');
        }

        // If the user clicks on the link, consider the question "done"
        this.completed = true;

        if (!this.answers.size()) {
            this.answer = this.addAnswer();
        }

        return this.respondHelper(this.answer, ANSWER_VALUE);
    }

    /**
     * Determines if the widget is valid.
     * @returns {boolean}
     */
    validate () {
        return this.completed;
    }
}

window.LF.Widget.Browser = Browser;
export default Browser;
