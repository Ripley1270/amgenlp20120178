import TextBox from './TextBox';

const DEFAULT_WRAPPER_TEMPLATE = 'DEFAULT:FormGroup',
    DEFAULT_CLASS_NAME = 'Modal',
    DEFAULT_LABEL_TEMPLATE = 'DEFAULT:Label',
    DEFAULT_INPUT_TEMPLATE = 'DEFAULT:TextBox',
    DEFAULT_LABELS = {};

class ModalWidgetBase extends TextBox {

    /**
     * default modal template
     * @abstract
     */
    get defaultModalTemplate () {
        throw new Error('Unimplemented defaultModalTemplate getter in a ModalWidgetBase implementation.');
    }

    /**
     * default class name for this widget, unless overridden by model.
     * @type {string}
     */
    get defaultClassName () {
        return DEFAULT_CLASS_NAME;
    }

    /**
     * default template for the wrapper of this widget, unless overidden by model.
     * @type {string}
     */
    get defaultWrapperTemplate () {
        return DEFAULT_WRAPPER_TEMPLATE;
    }

    /**
     * default template for the label of this widget, unless overidden by model.
     * @type {string}
     */
    get defaultLabelTemplate () {
        return DEFAULT_LABEL_TEMPLATE;
    }

    /**
     * default template for the input of this widget, unless overidden by model.
     * @type {string}
     */
    get defaultInputTemplate () {
        return DEFAULT_INPUT_TEMPLATE;
    }

    /**
     * default labels for modal dialog (keys to be translated)
     * @type {Object}
     */
    get defaultLabels () {
        return DEFAULT_LABELS;
    }

    /**
     * @abstract Method to convert the form elements/widgets in the modal dialog
     *      into a string to place into the textbox.
     */
    getModalValuesString () {
        throw new Error('Unimplemented getModalValuesString() method in a ModalWidgetBase implementation.');
    }


    /**
     * @abstract
     * @param {Event} e The event args
     * Overrideable hook for after the dialog is fully opened.
     * @returns {Q.Promise<void>}
     */
    dialogOpened (e) {
        let ret = Q();

        // If called directly from an event handler, close the promise,
        // and return undefined (value of ret.done())
        //  Otherwise return the promise to be handled by the caller.
        return e instanceof $.Event ? ret.done() : ret;
    }

    constructor (options) {
        super(options);

        /**
         * Modal dialog jQuery selector.
         * @type {jQuery}
         */
        this.$modal = null;

        /**
         * Input element jQuery selector
         * @type {jQuery}
         */
        this.$input = null;

        _.extend(this.events, {
            'focus input[type=tel], input[type=text]': 'openDialog'
        });
    }

    /**
     * Static method for determining if the active page element is the primary button of the modal dialog.
     * @returns {boolean}
     */
    static activeElementIsModalButton () {
        return document.activeElement.classList.contains('btn-primary');
    }

    /**
     * Overridden to have handler for modal dialog.  Doesn't work as a regular event
     * (in the events object) so create and destroy manually
     */
    delegateEvents () {
        super.delegateEvents();
        this.addCustomEvents();
    }

    /**
     * Overridden to also cancel custom events.
     */
    undelegateEvents () {
        super.undelegateEvents();
        this.removeCustomEvents();
    }

    /**
     * Add custom events that could not be set the Backbone way
     */
    addCustomEvents () {
        if (this.$modal) {
            this.dialogClosing = _.bind(this.dialogClosing, this);
            this.destroy = _.bind(this.destroy, this);
            this.$modal.on('hide.bs.modal', this.dialogClosing);
            this.$input.on('remove', this.destroy);
        }
    }
    
    /**
     * Remove custom events that could not be set the Backbone way
     */
    removeCustomEvents () {
        if (this.$modal) {
            this.$modal.off('hide.bs.modal', this.dialogClosing);
            this.$input.off('remove', this.destroy);
        }
    }

    /**
     * Launch the dialog.
     * @param {Event} e The event args
     * @returns {Q.Promise<void>} undefined if called from an event handler.
     * Otherwise, returns a promise, resolving when the dialog is fully opened.
     * After opened, this function will launch the afterDialogOpened hook.
     */
    openDialog (e) {
        //jscs:disable requireArrowFunctions
        this.$el.find('input[type=tel], input[type=text]').each(function () {
            this.blur();
        });
        //jscs:enable requireArrowFunctions

        let ret = new Q.Promise((resolve) => {
            let isShown = () => {
                this.$modal && this.$modal.off('shown.bs.modal', isShown);
                resolve();
            };
            if (this.$modal) {
                this.$modal.on('shown.bs.modal', isShown);
                this.$modal.modal('show');
            } else {
                resolve();
            }
        }).then(() => {
            return this.dialogOpened();
        });


        // If called directly from an event handler, close the promise,
        // and return undefined (value of ret.done())
        //  Otherwise return the promise to be handled by the caller.
        return e instanceof $.Event ? ret.done() : ret;
    }

    /**
     * Overrideable hook for when the dialog is closing.
     * @param {Event} e The event args
     * @returns {Q.Promise<void>} the promise if not coming from an event handler
     *      , or undefined if it is.
     */
    dialogClosing (e) {
        let ret = Q();
        if (ModalWidgetBase.activeElementIsModalButton()) {
            ret = this.getModalValuesString().then((answer) => {
                this.value = answer;
                this.$input.trigger('input');
            });
        }

        // If called directly from an event handler, close the promise,
        // and return undefined (value of ret.done())
        //  Otherwise return the promise to be handled by the caller.
        return e instanceof $.Event ? ret.done() : ret;
    }

    render () {
        let templates = this.model.get('templates') || {},
            textBox = templates.input || this.defaultInputTemplate,
            modalTemplate = templates.modal || this.defaultModalTemplate,
            toTranslate = {},
            answer = null,
            that = this;

        return this.setupCustomParams()
            .then(
                () => {
                    that.$el.empty();

                    // Optionally populate the toTranslate object.
                    that.model.get('label') && (toTranslate.label = that.model.get('label'));
                    that.model.get('placeholder') && (toTranslate.placeholder = that.model.get('placeholder'));
                    that.model.get('modalTitle') && (toTranslate.modalTitle = that.model.get('modalTitle'));
                    that.model.get('okButtonText') && (toTranslate.okButtonText = that.model.get('okButtonText'));

                    toTranslate = _.extend(toTranslate, that.model.get('labels'));

                    return that.i18n(
                        toTranslate,
                        () => null,
                        {namespace: that.getQuestion().getQuestionnaire().id}
                    );
                }
            ).then(strings => {
                let wrapperElement,
                    labelElement,
                    textBoxElement,
                    modalElement;

                strings.label = strings.label || '';
                strings.placeholder = strings.placeholder || '';
                strings.modalTitle = strings.modalTitle || '';
                strings.okButtonText = strings.okButtonText || '';

                wrapperElement = that.renderTemplate(templates.wrapper || this.defaultWrapperTemplate, {});

                // If a template label is configured, render it.
                if (strings.label) {
                    labelElement = that.renderTemplate(templates.label || this.defaultLabelTemplate, {
                        link: that.model.get('id'),
                        text: strings.label
                    });
                }

                textBoxElement = that.renderTemplate(textBox, {
                    id: that.model.get('id'),
                    placeholder: strings.placeholder || '',
                    name: that.model.get('id'),
                    className: that.model.get('className') || this.defaultClassName,
                    maxLength: that.model.get('maxLength')
                });

                // Append the wrapper to the local DOM and find where to append other elements.
                let $wrapper = that.$el.append(wrapperElement).find('[data-container]');

                if (labelElement) {
                    $wrapper.append(labelElement);
                }

                let modalTemplateOptions = {
                    title: strings.modalTitle || '&nbsp;',
                    okButtonText: strings.okButtonText || '&nbsp;'
                };

                let labelKeys = _.keys(that.model.get('labels') || that.defaultLabels);
                for (let i = 0; i < labelKeys.length; ++i) {
                    modalTemplateOptions[labelKeys[i]] = strings[labelKeys[i]] || '';
                }

                modalElement = that.renderTemplate(modalTemplate, modalTemplateOptions);

                // Add the textBox to the wrapper, then add the wrapper to the widget's DOM.
                $wrapper.append(textBoxElement)
                    .append(modalElement)
                    .appendTo(that.$el);

                that.$modal = $(modalElement);
                that.$input = $wrapper.find('input');

                $wrapper.append(modalElement);

                // Append the widget to the parent element and trigger a create event.
                that.$el.appendTo(that.parent.$el)
                    .trigger('create');

                if (!!(that.answer) && that.completed) {
                    answer = (that.isUserTextBox) ?
                        JSON.parse(that.answers.at(0).get('response'))[that.model.get('field')] :
                        that.answers.at(0).get('response');
                    that.value = answer;
                }

                that.delegateEvents();

                if (answer !== null) {
                    // Trigger the key-up event so that the answer is properly formatted:
                    that.$(`#${that.model.get('id')}`).trigger('input');
                }
            });
    }

    
    /**
     * Setup params in our current model from a custom function (passed to the diary as configFunc)
     * Note:   The only keys we allow here are "spinnerInputOptions" and "defaultVal"
     * @returns {Promise.<void>}
     */
    setupCustomParams () {
        return Q()
            .then(
                () => {
                    let customSpinnerConfigFn = this.model.get('configFunc');
                    if (LF.Widget.ParamFunctions[customSpinnerConfigFn]) {
                        return LF.Widget.ParamFunctions[customSpinnerConfigFn].call(this);
                    } else {
                        return {};
                    }
                }
            ).then((options) => {
                let optionKeys = _.keys(options);
                for (let i = 0; i < optionKeys.length; ++i) {
                    // ensure that we only set spinnerInputOptions or defaultVal here.
                    // TODO:  Override this with generic widget property function.
                    switch (optionKeys[i]) {
                        case 'spinnerInputOptions':
                        case 'defaultVal':
                            this.model.set(optionKeys[i], options[optionKeys[i]]);
                            break;
                    }
                }
            });
    }
    
    removeModal () {
        if (this.$modal) {
            this.$modal.modal('hide');
            this.$modal.remove();
            this.$modal = null;
        }
    }

    destroy () {
        this.undelegateEvents();
        this.removeModal();
    }
}

export default ModalWidgetBase;
