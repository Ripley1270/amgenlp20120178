/**
 * Renders a radio button specific matrix question.
 * @author <a href="mailto:brian.janaszek@ert.com">bjanaszek</a>
 * @version 1.0
 */
import MatrixBase from './MatrixBase';

class MatrixRadioButton extends MatrixBase {

    /**
     * Get the default question template.
     * @returns {string}
     */
    get questionTemplate () {
        return 'DEFAULT:MatrixQuestion';
    }

    /**
     * Get the default question container template.
     * @returns {string}
     */
    get rowContainer () {
        return 'DEFAULT:MatrixRowContainer';
    }

    /**
     * Gets the default question wrapper template.
     * @returns {string}
     */
    get wrapper () {
        return 'DEFAULT:MatrixItemWrapper';
    }

    /**
     * Gets the default question wrapper template.
     * @returns {string}
     */
    get questionLabel () {
        return 'DEFAULT:MatrixRadioButtonLabel';
    }

    /**
     * Construct the MatrixRadioButton widget.
     * @param {Object} options - The widget options.
     * @param {Object} options.model - The Backbone model class for the widget.
     * @param {Array} options.model.questions - The array of question information used to render each question of the matrix.  Each element is { IT: QUESTION_IT, text: Question_Text }
     * @param {Array} options.model.matrixAnswers - The array of answers to be used for each question in the matrix.  Each element is of the format { text: 'Answer_Text', value: 'answer_value'}
     * @param {string} [options.model.rowColor=#fff] - Hex value (or valid color string like 'black') for the standard row color.  If this value is provided, but alternatingRowColor is not, this will be used for all matrix rows.
     * @param {string} [options.model.alternatingRowColor=#FBFCFC] - Hex value (or valid color string like 'black') for the alternating row color.
     * @param {string} [options.model.questionWidth=50%] - The default width for the question column in the matrix.
     * @param {string} [options.model.headerRowColor=#F2F2F2] - The hex value (or value color string) for the header row background.
     * @param {string} options.model.className {dev-only} The CSS classname for the widget.
     * @param {Object} options.model.templates - Collection of templates used to render the matrix.
     * @param {string} [options.model.templates.questionTemplate=DEFAULT:MatrixQuestion] {dev-only} The question template for the widget.
     * @param {string} [options.model.rowContainer=DEFAULT:MatrixRowContainer] {dev-only} Used to render the question row in the matrix.
     * @param {string} [options.model.wrapper=DEFAULT:MatrixItemWrapper] {dev-only} The matrix item wrapper template.
     * @param {string} [options.model.questionLabel=DEFAULT:MatrixRadioButtonLabel] {dev-only} The template used to render the individual question labels.
     */
    constructor (options) {
        super(options);

        /**
         * Array of question information for individual questions.
         * [{ IT: 'x', text: 'y' }]
         */
        this.questions = this.model.get('questions');

        /**
         * Array of answers, of the form { text: 'x', value: y }
         */
        this.matrixAnswers = this.model.get('matrixAnswers');

        /**
         * Collection of question models to pass to the base control.
         */
        this.questionModels = [];

        /**
         * Templates used for rendering the matrix
         */
        this.templates = this.setTemplates();

    }

    /**
     * Properly configures the template property based on the given model.
     * @returns {Object} Template definition.
     */
    setTemplates () {
        let templates = this.model.get('templates') || {};
        let defaultTemplates = {
            questionTemplate: this.questionTemplate,
            container: this.rowContainer,
            wrapper: this.wrapper,
            questionLabel: this.questionLabel
        };

        return _.defaults(templates, defaultTemplates);
    }

    /**
     * Renders the widget.
     * @returns {Q.Promise<void>} Resolved when rendering is complete.
     */
    render () {
        // Reset the questions model for the base:
        this.model.set('questions', this.buildModels());
        this.model.set('header', this.buildHeaderText(this.matrixAnswers));

        return super.render();
    }

    /**
     * Generates the configuration/model objects for the questions.
     * @returns {Array} Contains question configurations.
     */
    buildModels () {
        let models = [];

        for (let i = 0; i < this.questions.length; i += 1) {
            models.push(this.buildConfig(this.questions[i], this.matrixAnswers));
        }

        return models;
    }

    /**
     * Build the question/widget configurations for the given question.
     * @param {String} question - The question text.
     * @param {Array} answers - The answers for the question.
     * @returns {Object} A properly configured question object.
     */
    buildConfig (question, answers) {

        let qModel = {
            id: question.IT,
            IT: question.IT,
            IG: this.getQuestion().model.get('IG'),
            text: [question.text],
            className: `${question.IT} table-row`,
            templates: {
                question: this.templates.questionTemplate
            },
            mandatory:true,
            widget: {
                id: `${question.IT}_WIDGET`,
                type: 'RadioButton',
                templates: {
                    container: this.templates.container,
                    wrapper: this.templates.wrapper,
                    label: this.templates.questionLabel
                },
                answers: answers
            }
        };

        return qModel;
    }

    /**
     * Builds the header text array based on the answers provided.
     * @param {Array} answers - The array of answers.
     * @returns {Array} The array of answer text.
     */
    buildHeaderText (answers) {
        let headerText = [''];
        _.each(answers, (item) => {
            headerText.push(item.text);
        });

        return headerText;
    }

}

window.LF.Widget.MatrixRadioButton = MatrixRadioButton;

export default MatrixRadioButton;
