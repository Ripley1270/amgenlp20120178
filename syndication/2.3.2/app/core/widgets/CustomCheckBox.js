/** #depends CheckBox.js
 * @file Defines a custom checkbox widget.
 * @author <a href="mailto:gsaricali@phtcorp.com">Gulden Saricali</a>
 * @version 1.4
 */
/**
 * LF.Widget.CustomCheckBox's constructor function
 * @class Initialize a new CustomCheckBox widget
 * @augments LF.Widget.CheckBox
 */
import CheckBox from './CheckBox';

export default class CustomCheckBox extends CheckBox {
    //noinspection JSMethodCanBeStatic
    get className () {
        return 'CustomCheckBox';
    }
}

window.LF.Widget.CustomCheckBox = CustomCheckBox;
