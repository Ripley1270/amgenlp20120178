import TemplateCollection from 'core/collections/Templates';

let Templates = new TemplateCollection([
    {
        name        : 'SignatureBox',
        namespace   : 'DEFAULT',
        template    : '<div class="signature-frame">' +
        '<div class="signature-box"></div>' +
        '<button type="submit" id="signature-clear" data-role="button" data-inline="true" data-theme="a">' +
        '{{ clear }}' +
        '</button>' +
        '</div>'
    },

    {
        name: 'NRSButtonGroup',
        namespace: 'DEFAULT',
        template: '<div class="NRS-Container" data-toggle="buttons" data-container></div>'
    },

    {
        name: 'VerticalButtonGroup',
        namespace: 'DEFAULT',
        template: '<div class="vertical-buttons" data-toggle="buttons" data-container></div>'
    },

    {
        name: 'HorizontalButtonGroup',
        namespace: 'DEFAULT',
        template: '<div class="horizontal-buttons" data-toggle="buttons" data-container></div>'
    },

    // Label wrapper for radio buttons.
    {
        name: 'RadioButtonWrapper',
        namespace: 'DEFAULT',
        template: '<div class="btn btn-default btn-block btn-{{ id }}" data-container></div>'
    },

    // Wrapper for horizontal choices (CustomRadioButton or CustomCheckBox)
    {
        name: 'HorizontalWrapper',
        namespace: 'DEFAULT',
        template: '<div class="text-center btn btn-default btn-block btn-{{id}}" data-container></div>'
    },

    {
        name: 'TimePicker',
        namespace: 'DEFAULT',
        template: `<span class="ui-helper-hidden-accessible"><input type="text"/>
        </span><input id="{{ id }}_time_input" class="form-control time-input"  data-role="datebox" />`
    },

    {
        name: 'TimePickerLabel',
        namespace: 'DEFAULT',
        template: '<label for="{{ id }}_time_input">{{ timeLabel }}</label>'
    },

    {
        name: 'DatePicker',
        namespace: 'DEFAULT',
        template: `<span class="ui-helper-hidden-accessible"><input type="text"/>
        </span><input id="{{ id }}" min="{{ min }}" max="{{ max }}" class="form-control date-input" data-role="datebox"  data-options='{{ configuration }}'  />`
    },
    {
        name: 'DatePickerLabel',
        namespace: 'DEFAULT',
        template: '<label for="{{ id }}_date_input">{{ dateLabel }}</label>'
    },

    // Default radio button input.
    {
        name: 'RadioButton',
        namespace: 'DEFAULT',
        template: '<input type="radio" id="{{ id }}" name="{{ name }}" value="{{ value }}"/>'
    },

    // Default template for custom RadioButton input that does not include jquery mobile css style.
    {
        name: 'CustomRadioButton',
        namespace: 'DEFAULT',
        template: '<input type="radio" id="{{ id }}" name="{{ name }}" value="{{ value }}" data-role="none"/>'
    },

    // Default template for RadioButton label.
    {
        name: 'CustomRadioButtonLabel',
        namespace: 'DEFAULT',
        template: '<label for="{{ link }}" class="radio-text {{ className }}"> <div>{{ text }}</div> </label>'
    },

    // Default template for RadioButton label.
    {
        name: 'RadioButtonLabel',
        namespace: 'DEFAULT',
        template: '<div class="icon"></div><label for="{{ link }}" class="radio-text {{ className }}">{{ text }}</label>'
    },

    // Default TextBox
    {
        name: 'TextBox',
        namespace: 'DEFAULT',
        template: `<input id="{{ id }}" placeholder="{{ placeholder }}" name="{{ name }}" class="{{ className }} form-control" maxlength="{{ maxLength }}" type="text" autocapitalize="off"/>`
    },

    // Default free text box
    {
        name: 'FreeTextBox',
        namespace: 'DEFAULT',
        template: `<div class="form-group">
                        <textarea id="{{ id }}" placeholder="{{ placeholder }}" name="{{ name }}" class="{{ className }} form-control"></textarea>
                        <div id="{{ id }}-counter" class="freetext-counter"><span id="{{ id }}-char-count"></span> {{ text }}</div>
                    </div>`
    },

    // Default Password TextBox
    {
        name        : 'PasswordTextBox',
        namespace   : 'DEFAULT',
        template    : '<div class="input-group"><span class="input-group-addon"><span class="fa fa-asterisk"></span></span> <input class ="form-control password-form" id="{{ id }}" placeholder="{{ placeholder }}" name="{{ name }}" class="{{ className }}" maxlength="{{ maxLength }}" type="password" autocapitalize="off"/></div>'
    },

    // Default numeric TextBox
    {
        name: 'NumericTextBox',
        namespace: 'DEFAULT',
        template: `<input id="{{ id }}" placeholder="{{ placeholder }}" name="{{ name }}" class="{{ className }} form-control" type="number" min="{{ min }}" max="{{ max }}" step="{{ step }}" />`
    },

    {
        name: 'FormGroup',
        namespace: 'DEFAULT',
        template: '<div class="form-group" data-container></div>'
    },

    // NumberBox
    {
        name: 'NumberBox',
        namespace: 'DEFAULT',
        template: '<div class="form-group"><input id="{{ id }}" placeholder="{{ placeholder }}" name="{{ name }}" class="{{ className }}" maxlength="{{ maxLength }}" type="tel" /></div>'
    },
    // Label wrapper for radio buttons.
    {
        name: 'CheckBoxWrapper',
        namespace: 'DEFAULT',
        template: '<div class="btn btn-default btn-block btn-{{ id }}" data-container></div>'
    },

    // Default template for Checkbox input that allows jquery mobile css style
    {
        name: 'CheckBox',
        namespace: 'DEFAULT',
        template: '<input type="checkbox" id="{{ id }}" name="{{ name }}" value="{{ value }}"/>'
    },

    // Default template for Checkbox input that does not include jquery mobile css style
    {
        name: 'CustomCheckBox',
        namespace: 'DEFAULT',
        template: '<input type="checkbox" id="{{ id }}" name="{{ name }}" value="{{ value }}" data-role="none"/>'
    },

    // Default template for Checkbox label.
    {
        name: 'CheckboxLabel',
        namespace: 'DEFAULT',
        template: '<div class="icon"></div><label for="{{ link }}" class="checkbox-text {{ className }}">{{ text }}</label>'
    },
    {
        name: 'NRSMarkers',
        namespace: 'DEFAULT',
        template: '<div class="markers">' +
        '<div class="marker marker-left"><div class="arrow-container up"></div><div class="marker-text">{{ left }}</div><div class="arrow-container down"></div></div>' +
        '<div class="marker marker-right"><div class="arrow-container up"></div><div class="marker-text">{{ right }}</div><div class="arrow-container down"></div></div>' +
        '</div>'
    },

    {
        name: 'AffidavitText',
        namespace: 'DEFAULT',
        template: '<div class="{{ label }} affidavit-text">{{ text }}</div>'
    },

    {
        name: 'Label',
        namespace: 'DEFAULT',
        template: '<label for="{{ link }}">{{ text }}</label>'
    },

    // Default Vertical Fieldset template that does not include jquery css mobile style. Used as a container for CustomRadioButton and CustomCheckBox widget.
    {
        name: 'CustomVerticalFieldset',
        namespace: 'DEFAULT',
        template: '<fieldset data-container data-role="none" ></fieldset>'
    }, {
        name: 'BarcodeScanner',
        namespace: 'DEFAULT',
        template: '<form class="scannerInputForm" data-ajax="false" autocomplete="off" autocorrect="off">' +
        '<input type="text" class="scannerData" />' +
        '</form>' +
        '<div class="scanBtn" data-role="button"></div>'
    },
    {
        name: 'NumberSpinnerModal',
        namespace: 'DEFAULT',
        template: `<div class="modal fade number-spinner-modal" tabindex="-1" role="dialog" aria-hidden="true" style="margin: auto">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel">
                                                {{title}}
                                            </h4>
                                        </div>

                                        <!-- Modal Body -->
                                        <div class="modal-body">
                                            <div class="number-spinner-label">{{labelOne}}</div>
                                            <div class="whole number-spinner-container">
                                            </div>
                                        </div>

                                        <!-- Modal Footer -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">
                                                {{okButtonText}}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>`
    },
    {
        name: 'DecimalSpinnerModal',
        namespace: 'DEFAULT',
        template: `<div class="modal fade number-spinner-modal" tabindex="-1" role="dialog" aria-hidden="true" style="margin: auto">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel">
                                                {{title}}
                                            </h4>
                                        </div>

                                        <!-- Modal Body -->
                                        <div class="modal-body decimal">
                                            <div class="modal-horizontal-label-container">
                                                <div class="number-spinner-label">{{labelOne}}</div>
                                                <div class="number-spinner-label">{{labelTwo}}</div>
                                            </div>
                                            <div class="modal-horizontal-container">
                                                <div class="number-spinner-container"></div>
                                                <div class="number-spinner-container"></div>
                                            </div>
                                        </div>


                                        <!-- Modal Footer -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">
                                                {{okButtonText}}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>`
    },
    {
        name: 'NumberSpinnerControl',
        namespace: 'DEFAULT',
        template: `<div class="number-spinner-control">
                                                    <div class="container-wrapper">
                                                        <ul class="item-container">
                                                        </ul>

                                                        <div class="overlay-top">&nbsp;</div>
                                                        <div class="overlay-bottom">&nbsp;</div>
                                                        <div class="overlay-middle">&nbsp;</div>
                                                    </div>
                                                </div>`
    }, {
        name        : 'SelectWidget',
        namespace   : 'DEFAULT',
        template    : `<div class="form-group">
            <select id="{{ id }}" name="{{ name }}" class="{{ className }} form-control select-widget"></select>
        </div>`
    }, {
        name: 'NumberItemTemplate',
        namespace: 'DEFAULT',
        template: `<li class="item-template item" data-value="{{value}}">{{displayValue}}</li>`
    },
    {
        name        : 'SelectWidget',
        namespace   : 'DEFAULT',
        template    : `<div class="form-group">
            <select id="{{ id }}" name="{{ name }}" class="{{ className }} form-control select-widget"></select>
        </div>`
    },
    {
        name: 'SelectItemTemplateNoTrans',
        namespace: 'DEFAULT',
        template: `<option value="{{ value }}">{{ text }}</option>`
    },
    {
        name: 'SelectItemTemplateTrans',
        namespace: 'DEFAULT',
        template: `<option value="{{ value }}">{{ localized }}</option>`
    },
    {
        name: 'VVAS',
        namespace: 'DEFAULT',
        template: `<div class='vvas-container'>
                    <div id='topAnchor'>{{ top }}</div>
                    <div class='vvas-canvas-container'>
                        <canvas id='{{ id }}' class='vvas' />
                    </div>
                    <div id='bottomAnchor'>{{ bottom }}</div>
                   </div>`
    },

    {
        name        : 'LogpadPasswordTextBox',
        namespace   : 'DEFAULT',
        template    : `<div class="input-group password-form">
            <span class="input-group-addon">
                <span class="fa fa-asterisk"></span>
            </span>
            <input class ="form-control" id="{{ id }}" placeholder="{{ placeholder }}" name="{{ name }}" class="{{ className }}" maxlength="{{ maxLength }}" type="text" autocapitalize="off"/>
        </div>`
    }, {
        name: 'PasswordMessage',
        namespace: 'DEFAULT',
        template: `<div id="passwordMessage">{{ tempPasswordMessage }}
            <br><br>
            <strong>{{ userName }}</strong> {{ displayUsername }}
            <br>
            <strong>{{ role }}</strong> {{ displayRole }}
            <br>
            <strong>{{ tempPassword }}</strong> {{ displayTempPass }}
            <br>
            <strong>{{ language }}</strong> {{ displayLanguage }}
        <div>`
    },
    {
        name: 'StaticField',
        namespace: 'DEFAULT',
        template: '<p class="form-control-static">{{ response }}</p>'
    }, {
        name: 'MatrixContainer',
        namespace: 'DEFAULT',
        template: '<div class="{{ className }} container-fluid matrix-container"></div>'
    }, {
        name: 'MatrixHeaderContainer',
        namespace: 'DEFAULT',
        template: '<div class="table-row header"></div>'
    }, {
        name: 'MatrixHeaderItem',
        namespace: 'DEFAULT',
        template: '<div class="matrix-header {{ className }}">{{ text }}</div>'
    }, {
        name: 'MatrixRowContainer',
        namespace: 'DEFAULT',
        template: '<div class="matrix-wrapper item" data-toggle="buttons" data-container></div>'
    }, {
        name: 'MatrixItemWrapper',
        namespace: 'DEFAULT',
        template: '<div class="item btn btn-{{ id }}" data-container></div>'
    }, {
        name: 'MatrixQuestion',
        namespace: 'DEFAULT',
        template: '<div class="matrix-question item">{{ text }}</div>'
    }, {
        name: 'MatrixRadioButtonLabel',
        namespace: 'DEFAULT',
        template: '<div class="icon"></div><label for="{{ link }}" class="matrix-text {{ className }}">{{ text }}</label>'
    },
    {
        name: 'DateSpinnerModal',
        namespace: 'DEFAULT',
        template: `<div class="modal fade date-spinner-modal" tabindex="-1" role="dialog" aria-hidden="true" style="margin: auto">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel">
                                                {{title}}
                                            </h4>
                                        </div>

                                        <!-- Modal Body -->
                                        <div class="modal-body date">
                                            <div class="modal-horizontal-container date-container"></div>
                                        </div>


                                        <!-- Modal Footer -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">
                                                {{okButtonText}}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>`
    },
    {
        name: 'TimeSpinnerModal',
        namespace: 'DEFAULT',
        template: `<div class="modal fade date-spinner-modal" tabindex="-1" role="dialog" aria-hidden="true" style="margin: auto">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel">
                                                {{title}}
                                            </h4>
                                        </div>

                                        <!-- Modal Body -->
                                        <div class="modal-body">
                                            <div class="modal-horizontal-container date-time-time-container time-container"></div>
                                        </div>


                                        <!-- Modal Footer -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">
                                                {{okButtonText}}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>`
    },
    {
        name: 'TimeSpinnerModal_24Hour',
        namespace: 'DEFAULT',
        template: `<div class="modal fade date-spinner-modal" tabindex="-1" role="dialog" aria-hidden="true" style="margin: auto">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel">
                                                {{title}}
                                            </h4>
                                        </div>

                                        <!-- Modal Body -->
                                        <div class="modal-body">
                                            <div class="modal-horizontal-container date-time-time-container time-container" data-format="HH:mm">
                                            </div>
                                        </div>


                                        <!-- Modal Footer -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">
                                                {{okButtonText}}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>`
    },
    {
        name: 'TimeSpinnerModal_12Hour',
        namespace: 'DEFAULT',
        template: `<div class="modal fade date-spinner-modal" tabindex="-1" role="dialog" aria-hidden="true" style="margin: auto">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel">
                                                {{title}}
                                            </h4>
                                        </div>

                                        <!-- Modal Body -->
                                        <div class="modal-body">
                                            <div class="modal-horizontal-container date-time-time-container time-container" data-format="hh:mm A">
                                            </div>
                                        </div>


                                        <!-- Modal Footer -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">
                                                {{okButtonText}}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>`

    }, {
        name: 'Browser',
        namespace: 'DEFAULT',
        template: `<div class="Browser-Link">
                        <a id="{{ id }}">{{ linkText }}</a> 
                   </div>`
    }
]);

export default Templates;

LF.Resources.Templates = Templates;
// A shortcut to make displaying templates easier.
LF.templates = LF.Resources.Templates;
