import Logger from 'core/Logger';
import { Dialog, MessageRepo } from 'core/Notify';
import * as MessageHelpers from './MessageHelpers';

const logger = new Logger('Dialog');

export default function setupCoreDialogs () {

    MessageRepo.add({
        type: 'Dialog',
        key: 'BACK_OUT_CONFIRM',
        message: MessageHelpers.confirmDialogCreator({
            message: 'BACK_OUT_CONFIRM',
            type: 'warning'
        })
    }, {
        type: 'Dialog',
        key: 'USER_ALREADY_EXISTS_ERROR',
        message: MessageHelpers.notifyDialogCreator({
            message: 'USER_ALREADY_EXISTS',
            type: 'error'
        })
    }, {
        type: 'Dialog',
        key: 'CONNECTION_REQUIRED_ERROR',
        message: MessageHelpers.notifyDialogCreator({
            message: 'CONNECTION_REQUIRED',
            type: 'error'
        })
    }, {
        type: 'Dialog',
        key: 'TRANSMISSION_ERROR_ERR',
        message: (params = {}) => {
            return LF.getStrings({ err: 'ERROR' })
                .then(strings => {
                    return MessageHelpers.notifyDialogCreator({
                        header: 'ERROR_TITLE',
                        coreString: strings.err,
                        type: 'error'
                    })();
                });
        }
    }, {
        type: 'Dialog',
        key: 'TRANSMISSION_ERROR_HTTP_TIMEOUT',
        message: (params = {}) => {
            return LF.getStrings({
                connectionTimeout: 'CONNECTION_TIMEOUT',
                tryOrContact: 'PLEASE_TRY_OR_CONTACT_SUPPORT'
            })
            .then(strings => {
                return MessageHelpers.notifyDialogCreator({
                    header: 'ERROR_TITLE',
                    coreString: `${strings.connectionTimeout}<br>${(strings.tryOrContact)}`,
                    type: 'error'
                })();
            });
        }
    }, {
        type: 'Dialog',
        key: 'TRANSMISSION_ERROR_HTTP_AND_ERROR',
        message: ({ httpCode = '{{httpCode}}', errorCode = '{{errorCode}}'}) => {
            return LF.getStrings({
                contact: 'PLEASE_CONTACT',
                errCode: 'ERROR_CODE'
            })
            .then(strings => {
                return MessageHelpers.notifyDialogCreator({
                    header: 'ERROR_TITLE',
                    coreString: `${strings.errCode} ${httpCode}-${errorCode}<br>${(strings.contact)}`,
                    type: 'error'
                })();
            });
        }
    }, {
        type: 'Dialog',
        key: 'TRANSMISSION_ERROR_HTTP_AND_ERROR_ACTIVATION_SUCCESSFUL',
        message: ({ httpCode = '{{httpCode}}', errorCode = '{{errorCode}}'}) => {
            return LF.getStrings({
                errMsg: 'ERROR_AFTER_ACTIVATION_SUCCESSFUL',
                errCode: 'ERROR_CODE'
            })
            .then(strings => {
                return MessageHelpers.notifyDialogCreator({
                    header: 'ERROR_TITLE',
                    coreString: `${strings.errCode} ${httpCode}-${errorCode}<br>${(strings.errMsg)}`,
                    type: 'error'
                })();
            });
        }
    }, {
        type: 'Dialog',
        key: 'TRANSMISSION_ERROR_HTTP_CODE',
        message: ({ httpCode = '{{httpCode}}' }) => {
            return LF.getStrings({
                contact: 'PLEASE_CONTACT',
                errCode: 'ERROR_CODE'
            })
            .then(strings => {
                return MessageHelpers.notifyDialogCreator({
                    header: 'ERROR_TITLE',
                    coreString: `${strings.errCode} ${httpCode}<br>${(strings.contact)}`,
                    type: 'error'
                })();
            });
        }
    }, {
        type: 'Dialog',
        key: 'TRANSMISSION_ERROR_ERROR_CODE',
        message: ({ errorCode = '{{errorCode}}' }) => {
            return LF.getStrings({
                tryOrContact: 'PLEASE_TRY_OR_CONTACT_SUPPORT'
            })
            .then(strings => {
                return MessageHelpers.notifyDialogCreator({
                    header: 'ERROR_TITLE',
                    coreString: strings.tryOrContact,
                    type: 'error'
                })();
            });
        }
    }, {
        type: 'Dialog',
        key: 'RETRY_OR_CONTACT_SUPPORT',
        message: () => {
            return LF.getStrings({
                tryOrContact: 'PLEASE_TRY_OR_CONTACT_SUPPORT'
            })
            .then(strings => {
                return MessageHelpers.notifyDialogCreator({
                    ok: 'RETRY',
                    header: 'ERROR_TITLE',
                    coreString: strings.tryOrContact,
                    type: 'error'
                })();
            });
        }
    }, {
        type: 'Dialog',
        key: 'TIMESLIP_ERROR',
        message: () => {
            return LF.getStrings({
                error: 'TIMESLIP_ERROR'
            })
            .then(strings => {
                return MessageHelpers.notifyDialogCreator({
                    ok: 'RETRY',
                    header: 'ERROR_TITLE',
                    coreString: strings.error,
                    type: 'error'
                })();
            });
        }
    }, {
        type: 'Dialog',
        key: 'TRANSMIT_RETRY_FAILED',
        message: MessageHelpers.notifyDialogCreator({
            header: 'UPDATE_FAILURE',
            message: 'UPDATE_FAILED',
            type: 'error'
        })
    }, {
        type: 'Dialog',
        key: 'PASSWORD_CHANGE_SUCCESS',
        message: MessageHelpers.notifyDialogCreator({
            header: 'UPDATE_SUCCESS',
            message: 'PASSWORD_CHANGE_SUCCESS',
            type: 'success'
        })
    }, {
        type: 'Dialog',
        key: 'SECRET_QUESTION_CHANGE_SUCCESS',
        message: MessageHelpers.notifyDialogCreator({
            header  : 'UPDATE_SUCCESS',
            message : 'SECRET_QUESTION_CHANGE_SUCCESS',
            type: 'success'
        })
    }, {
        type: 'Dialog',
        key: 'SIGNATURE_DATA_LIMIT_EXCEEDED',
        message: MessageHelpers.notifyDialogCreator({
            message: 'SIGNATURE_DATA_LIMIT_EXCEEDED'
        })
    }, {
        type: 'Dialog',
        key: 'PASSWORD_CHANGE_FAILED',
        message: MessageHelpers.notifyDialogCreator({
            type: 'error',
            message: 'PASSWORD_CHANGE_FAILED',
            header: 'ERROR_TITLE'
        })
    }, {
        type: 'Dialog',
        key: 'CONFIRM_TRANSMIT_RETRY_CONNECTION_REQUIRED',
        message: MessageHelpers.confirmDialogCreator({
            header: 'UPDATE_FAILURE',
            message: 'CONNECTION_REQUIRED',
            ok: 'RETRY',
            type: 'error'
        })
    }, {
        type: 'Dialog',
        key: 'CONFIRM_TRANSMIT_RETRY',
        message: MessageHelpers.confirmDialogCreator({
            header: 'UPDATE_FAILURE',
            message: 'PLEASE_TRY_OR_CONTACT_SUPPORT',
            ok: 'RETRY',
            type: 'error'
        })
    }, {
        type: 'Dialog',
        key: 'SKIP_QUESTION_CONFIRM',
        message: MessageHelpers.confirmDialogCreator({
            message: 'SKIP_QUESTION'
        })
    }, {
        type: 'Dialog',
        key: 'TIME_ZONE_SET_CONFIRM',
        message: MessageHelpers.confirmDialogCreator({
            header: 'TIME_ZONE_SET_HEADER',
            message: 'TIME_ZONE_SET_CONFIRM'
        })
    }, {
        type: 'Dialog',
        key: 'TIME_ZONE_SET_CONFIRM_NOCHANGE',
        message: MessageHelpers.confirmDialogCreator({
            header: 'TIME_ZONE_SET_HEADER',
            message: 'TIME_ZONE_SET_CONFIRM_NOCHANGE'
        })
    },  {
        type: 'Dialog',
        key: 'TIME_ZONE_ERROR',
        message: MessageHelpers.notifyDialogCreator({
            header: 'TIME_ZONE_ERROR_HEADER',
            message: 'TIME_ZONE_ERROR_MSG'
        })
    }, {
        type: 'Dialog',
        key: 'TIME_ZONE_SET_ERROR',
        message: MessageHelpers.notifyDialogCreator({
            header: 'TIME_ZONE_ERROR_HEADER',
            message: 'TIME_ZONE_SET_ERROR_MSG',
            type: 'error'
        })
    }, {
        type: 'Dialog',
        key: 'CRITICAL_BATTERY',
        message: MessageHelpers.notifyDialogCreator({
            message: 'CRITICAL_BATTERY',
            type: 'warning'
        })
    }, {
        type: 'Dialog',
        key: 'LOW_BATTERY',
        message: MessageHelpers.notifyDialogCreator({
            message: 'LOW_BATTERY',
            type: 'warning'
        })
    }, {
        type: 'Dialog',
        key: 'NONEXISTENT_USER',
        message: MessageHelpers.notifyDialogCreator({
            header: 'ERROR_TITLE',
            message: 'NONEXISTENT_USER',
            type: 'error'
        })
    });
}
