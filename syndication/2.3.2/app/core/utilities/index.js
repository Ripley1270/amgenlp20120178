import * as coreUtilities from './coreUtilities';
import * as languageExtensions from './languageExtensions';
import * as localizationUtils from './localizationUtils';

export default _.extend(coreUtilities, languageExtensions, localizationUtils);