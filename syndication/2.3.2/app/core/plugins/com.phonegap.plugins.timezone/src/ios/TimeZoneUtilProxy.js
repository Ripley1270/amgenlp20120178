module.exports = {

    /**
     * Get the user's current time zone
     * TODO: Find the corresponding functionality on iOS
     * @param {Function} onSuccess Invoked on successful get current time zone, containing a result in a callback value
     * @param {Function} onError Invoked if there is an error.
     */
    getCurrentTimeZone: function (onSuccess, onError) {
        onSuccess({
            id: 'America/New_York',
            displayName: '(GMT-05:00) America/New_York'
        });
    },

    /**
     * Get the list of system installed time zones
     * TODO: Find the corresponding functionality on iOS
     * @param {Function} onSuccess Invoked on successful get, containing a result in a callback value
     * @param {Function} onError Invoked if there is an error.
     */
    getAvailableTimeZones: function (onSuccess, onError) {
        onSuccess([{
            id: 'America/New_York',
            displayName: '(GMT-05:00) America/New_York'
        }]);
    },

    /**
     * Get the list of system installed time zone IDs
     * TODO: Find the corresponding functionality on iOS
     * @param {Function} onSuccess Invoked on successful get, containing a result in a callback value
     * @param {Function} onError Invoked if there is an error.
     */
    getAutomaticTimezone: function (onSuccess, onError) {
        onError('Not yet developed on iOS platform');
    },

    /**
     * Set the OS time zone
     * TODO: needs to be done on iOS
     * @param {Function} onSuccess Invoked on successful set.
     * @param {Function} onError Invoked if there is an error.
     * @param {string}   tzId Time zone ID to set.
     */
    setTimeZone: function (onSuccess, onError, tzId) {
        onSuccess();
    },

    /**
     * Get the options for timezone
     * No info for this on iOS platform
     * @param {Function} onSuccess Invoked on successful set.
     * @param {Function} onError Invoked if there is an error.
     */
    getOptions: function (onSuccess, onError) {
        onSuccess();
    },

    /**
     * Restart the application if Android version less than 5
     * No info for this on iOS platform
     * @param {Function} onSuccess Invoked on successful execution
     * @param {Function} onError Invoked if there is an error.
     */
    restartApplication: function (onSuccess, onError) {
        onSuccess();
    }
}; // exports

require("cordova/exec/proxy").add("TimeZoneUtil", module.exports);
