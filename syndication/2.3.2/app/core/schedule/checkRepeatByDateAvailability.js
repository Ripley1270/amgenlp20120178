/**
 * Checks the repeating diary schedule.
 * @param {LF.Model.Schedules} schedule object containing schedule configuration.
 * @param {LF.Collection.Dashboards} completedQuestionnaires list of all completed diaries.
 * @param {Object} parentView The view to append this scheduled item to.
 * @param {Object} context an object that contains subject model and visit model (sitepad only)
 * @param {Function} callback A callback function invoked upon.
 * @param {Boolean} isAlarm States if this scheduling is for an alarm
 */
LF.Schedule.schedulingFunctions.checkRepeatByDateAvailability = function (schedule, completedQuestionnaires, parentView, context, callback) {
    let nowUtcMillis        = new Date().getTime(),
        startTimeUtc        = LF.Utilities.parseTime(schedule.get('scheduleParams').startAvailability, true),
        endTimeUtc          = LF.Utilities.parseTime(schedule.get('scheduleParams').endAvailability, true),
        daysSinceActivation = LF.Utilities.dateDiffInDays(new Date(), new Date(LF.Data.Questionnaire.subject.get('activationDate'))),
        questionnaireID     = schedule.get('target').id,
        diary;
    if(startTimeUtc >= endTimeUtc){
        if(nowUtcMillis <= startTimeUtc && daysSinceActivation !== 0){
            startTimeUtc = startTimeUtc - (24 * 60 * 60 * 1000);
        }else{
            endTimeUtc = endTimeUtc + (24 * 60 * 60 * 1000);
        }
    }
    if(nowUtcMillis >= startTimeUtc && nowUtcMillis < endTimeUtc){
        diary = _.first(completedQuestionnaires.where({questionnaire_id : questionnaireID}));
        if(!diary){
            callback(true);
            return;
        }
        let startedDate;
        startedDate = new Date(LF.Utilities.shiftToNewLocal(diary.get('lastStartedDate'))).getTime();
        if(!(startTimeUtc < startedDate && endTimeUtc > startedDate)){
            callback(true);
        }else{
            callback(false);
        }
    }else{
        callback(false);
    }
};
export default LF.Schedule.schedulingFunctions.checkRepeatByDateAvailability;
