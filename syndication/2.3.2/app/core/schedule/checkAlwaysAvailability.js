/**
 * @file The LogPad LF Core Schedule function.
 * @author <a href="mailto:tblagaic@phtcorp.com">Toni Blagaic</a>
 * @version 1.5.1
 */
/**
 * true for Always Available Diary.
 * @param {LF.Model.Schedules} schedule object containing schedule configuration.
 * @param {LF.Collection.Dashboards} completedQuestionnaires list of all completed diaries.
 * @param {any} value the value
 * @param {Object} context an object that contains subject model and visit model (sitepad only)
 * @param {Function} callback A callback function invoked upon.
 */
LF.Schedule.schedulingFunctions.checkAlwaysAvailability = function (schedule, completedQuestionnaires, value, context, callback) {
    callback(true);
};

export default LF.Schedule.schedulingFunctions.checkAlwaysAvailability;