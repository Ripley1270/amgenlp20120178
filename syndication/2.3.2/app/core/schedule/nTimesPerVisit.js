/**
 *
 * @param {LF.Model.Schedules} schedule object containing schedule configuration.
 * @param completedQuestionnaires
 * @param value
 * @param {Function} callback A callback function invoked upon.
 */

import UserReports from 'sitepad/collections/UserReports';

// There are too many parameters. Pass them as a single object instead.
LF.Schedule.schedulingFunctions.nTimesPerVisit = function (schedule, completedQuestionnaires, value, context, callback) {
    let scheduleParams = schedule.get('scheduleParams') || {},
        questionnaireID = schedule.get('target').id,
        currentSubject = context.subject,
        currentVisit = context.visit,
        maxReports,
        visitID;

    if (!currentVisit || _.isEmpty(scheduleParams)) {
        callback(false);
        return;
    }

    let currentVisitParams = _.select(scheduleParams.visits, (visitParams) => {
        let result = false;

        if (visitParams.visitID === currentVisit.id) {
            if (!!visitParams.phase) {
                let phase = _.select(visitParams.phase, (phaseID) => {
                    // eslint-disable-next-line eqeqeq
                    return phaseID == currentSubject.get('phase');
                });

                result = phase.length > 0;
            } else {
                result = true;
            }
        }

        return result;
    });

    if (currentVisitParams.length > 0) {
        let userReports = new UserReports();

        // Should be only one
        currentVisitParams = currentVisitParams[0];
        maxReports = parseInt(currentVisitParams.maxReports, 10);
        visitID = currentVisitParams.visitID;

        if (maxReports === 0) {
            // 0 means always available
            callback(true);
        } else {
            userReports.fetch().then((reports) => {
                let result = reports.length === 0,
                    filteredReports;

                if (!result) {
                    filteredReports = _.filter(reports, (report) => {
                        return report.get('user_visit_id') === visitID &&
                            report.get('questionnaire_id') === questionnaireID &&
                            report.get('subject_krpt') === currentSubject.get('krpt');
                    });
                    result = !!filteredReports ? (filteredReports.length < maxReports) : false;
                }

                callback(result);
            });
        }
    } else {
        callback(false);
    }
};

export default LF.Schedule.schedulingFunctions.nTimesPerVisit;