import DABL from './DABL';

/* We shouldn't need these lines anymore unless we want to support older browsers.
 * window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
 * window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction;
 * window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;
 */

DABL.plugin('IndexedDB', (options) => {

    let indexedDB = window.indexedDB,
        transactionType = { READ_ONLY: 'readonly', READ_WRITE: 'readwrite' },
        queue = [],
        working = false,
        executeAll, addToQueue, db, request, open,
        dbName = options && options.name || 'DEFAULT',
        pluginName = 'IndexedDB',

        debug = (...args) => {
            if (options.debug) {
                console.debug ? console.debug(`${pluginName}:`, ...args) : console.log(`${pluginName}:`, ...args);
            }
        },

        error = (...args) => {
            // Special handling of SQLError, which has no useful .string()
            args = args.map(arg => arg.message ? arg.message : arg);
            console.error(`${pluginName}:${dbName}`, ...args); // no ${name} available at this level
        },

        logAndReject = (deferred) => (event) => {
            error(event);
            deferred.reject(new Error(event.toString()));
        },

        createObjectStore = (name) => {
            let exists = _(db.objectStoreNames).indexOf(name);

            if (exists === -1) {
                db.createObjectStore(name, {
                    keyPath : 'id',
                    autoIncrement : true
                });
            }
        };

    // Cannot use => as the context of this is lost :(
    open = function () {
        let request,
            deferred = Q.defer();

        if (this.openPending) {
            debug(`An open of ${this.name} is already pending; wait for that to finish`);
            return Q().delay(100)
                .then(() => open.call(this));
        } else if (db != null) {
            debug(`${this.name} was already open; probably by a parallel open request`);
            return Q();
        }

        this.openPending = true;

        if (!this.version) {
            debug(`Opening ${this.name} with unspecified version`);
            request = indexedDB.open(dbName);
        } else {
            debug(`Opening ${this.name} with version ${this.version}`);
            request = indexedDB.open(dbName, this.version);
        }

        request.onblocked = (evt) => {
            // retry
            debug(`Open ${this.name} was blocked; retrying in 100ms`, evt);
            return Q().delay(100)
                .then(() => open.call(this));
        };

        // When an upgrade is required, we need to ensure that every collection has an object store.
        request.onupgradeneeded = (evt) => {
            debug(`Upgrade of ${this.name} needed`, evt);
            db = evt.target.result;

            // Create an object store for each collection.
            _(this.collections).each((collection, name) => createObjectStore(name));
        };

        request.onsuccess = (evt) => {
            debug(`Opened ${this.name}`, evt);
            let matches = 0,
                version;

            db = evt.target.result;
            this.openPending = false;

            // this is suspect... db is not nulled out?
            db.onversionchange = (event) => event.target.close();

            // Loop through each collection to determine if it has an object store.
            // If one is missing, then an upgrade is required.
            _(this.collections).forEach((collection, name) => {
                _(db.objectStoreNames).forEach((store) => {
                    if (store === name) {
                        matches += 1;
                    }
                });
            });

            // if there are new collections, need to create them
            if (matches < _(this.collections).size()) {
                // tick the database version...
                this.version = parseInt(db.version, 10) + 1;
                debug(`Detected new collection not in ${this.name}; initiating upgrade to version ${this.version}`);
                debug(`Closing currently open ${this.name}`);
                db.close();
                db = null;
                open.call(this)
                    .then(() => deferred.resolve());
            } else {
                deferred.resolve();
            }
        };

        request.onerror = (evt, err) => {
            debug(`Failed to open ${this.name}`, evt, err);
            deferred.reject(err);
        };

        return deferred.promise;

    };

    addToQueue =  (action) => {
        queue.push(action);

        if (!working) {
            working = true;
            executeAll();
        }
    };

    executeAll = () => {
        let step = () => {
            let action = queue.shift();

            if (action) {
                action(() => {
                    if (queue.length) {
                        step();
                    } else {
                        working = false;
                    }
                });
            } else {
                working = false;
            }
        };

        if (queue.length) {
            step();
        } else {
            working = false;
        }
    };

    debug(`New IndexedDB ${dbName}`);

    return {

        name: dbName,

        supported   : !!indexedDB,

        collections : { },

        collection  : function (name, options = {}) {

            let error = (...args) => {
                // Special handling of SQLError, which has no useful .string()
                args = args.map(arg => arg.message ? arg.message : arg);
                console.error(`${pluginName}:${dbName}:${name}`, ...args);
            };

            // NOTE! this method does NOT call createObjectStore (?)
            //       that happens indirectly via re-opening db

            debug(`${this.name}.collection( "${name}", ${JSON.stringify(options)} )`);

            // A list interface methods to extend on the collection.
            let whitelist = ['save', 'get', 'remove', 'clear', 'count', 'query', 'all', 'drop'];

            // If this collection doesn't already exist.
            if (this[name] == null) {
                this.collections[name] = options;

                // Add the collection to the plugin.
                this[name] = {
                    name      : name,
                    options   : options,
                    installed : false
                };

                // Loop through each property and method on the plugin.
                _(this).each((method, index) => {

                    // If the method is whitelisted...
                    if (_(whitelist).include(index)) {
                        let _options  = options; // expose options to the inner function
                        // Add the method onto the collection.  Can't use arrow shortcut... compiler issue?
                        this[name][index] = _.bind(function (...args) {
                            try {
                                debug(`${dbName}.${name}.${index}( ${JSON.stringify(args).slice(1,-1)} )`);
                                return method.apply(this, [name].concat(args));
                            } catch (e) {
                                error(e);
                                // convert DOMError to Error with statck
                                return Q.reject(e.message ? new Error(e.message) : e);
                            }
                        }, this);
                    }
                });

                if (db) {
                    addToQueue(done => {
                        debug('Closing currently open db');
                        db.close();
                        db = null;
                        Q.delay(100).then(open.call(this).done(done));
                    });
                }
            }

            debug(`After adding collection("${name}"), ${this.name}.collections=${_(this.collections).keys()}`);
            return Q(); // required by DABL API
        },

        drop : function (oops) {

            if (!db) {
                return open.call(this)
                    .then(() => this.drop(oops));
            }

            if (oops) {
                return Q.reject(`IndexedDB: drop individual collection unimplemented. (${oops}). clear() is supported.`);
            }
            let deferred = Q.defer();

            debug(`DROPPING DB: ${this.name}; tables: ${_(this.collections).keys()}`);

            let dbName = this.name; // put it in closure
            addToQueue((next) => {
                // 'this' is null here
                let request = indexedDB.deleteDatabase(dbName /* from closure; this==null*/);

                request.onerror = logAndReject(deferred);
                request.onblocked = logAndReject(deferred);
                request.onsuccess =  (evt) => {
                    next();
                    debug(`${this.name} DROP RESULT:`, '(void)');
                    deferred.resolve();
                };
            });

            this.collections = { };
            return deferred.promise;
        },

        all : function (name) {
            let deferred = Q.defer();

            if (!db) {
                return open.call(this)
                    .then(() => this.all(name));
            }

            addToQueue((next) => {
                let transaction = db.transaction([name], transactionType.READ_ONLY),
                    store = transaction.objectStore(name),
                    results = [],
                    request = store.openCursor();

                request.onsuccess = (res) => {
                    let cursor = res.target.result;

                    if (cursor) {
                        results.push(cursor.value);

                        // Using [] since continue is a reserved keyword.
                        cursor['continue']();
                    } else {
                        let res = new DABL.RecordSet(results);
                        debug(`${this.name}.${name}.all() RESULT:`, res);
                        deferred.resolve(res);
                        next();
                    }
                };

                request.onerror = logAndReject(deferred);
                request.onblock = logAndReject(deferred);
            });

            return deferred.promise;
        },

        clear   : function (name) {
            let deferred = Q.defer();

            if (!db) {
                return open.call(this)
                    .then(() => this.clear(name));
            }

            addToQueue((next) => {
                let transaction = db.transaction([name], transactionType.READ_WRITE),
                    store = transaction.objectStore(name),
                    request = store.clear();

                // Used transaction.oncomplete, instead of request.onsuccess to ensure all data is up-to-date.
                transaction.oncomplete = () => {
                    debug(`${this.name}.${name}.clear()  RESULT:`, '(void)');
                    deferred.resolve();
                    next();
                };

                request.onerror = (err) => {
                    deferred.reject(err);
                    next();
                };
            });

            return deferred.promise;
        },

        count : function (name) {
            let deferred = Q.defer();

            if (!db) {
                return open.call(this)
                    .then(() => this.count(name));
            }

            addToQueue((next) => {
                let transaction = db.transaction([name], transactionType.READ_ONLY),
                    store = transaction.objectStore(name),
                    request = store.count();

                transaction.oncomplete = () => {
                    debug(`${this.name}.${name}.count()  RESULT:`, request.result);
                    deferred.resolve(request.result);
                    next();
                };

                request.onerror = (err) => {
                    deferred.reject(err);
                    next();
                };
            });

            return deferred.promise;
        },

        query : function (name, params) {
            if (!db) {
                return open.call(this)
                    .then(() => this.query(name, params));
            }

            return this.all(name)
                .then((res) => {
                    let r = new DABL.RecordSet(res.where(params));
                    debug(`${this.name}.${name}.query(...)  RESULT:`, r);
                    return r;

                });
        },

        get : function (name, id) {
            let deferred = Q.defer();

            if (!db) {
                return open.call(this)
                    .then(() => this.get(name, id));
            }

            addToQueue(next => {
                let transaction = db.transaction([name], transactionType.READ_ONLY),
                    store = transaction.objectStore(name);

                let request = store.get(id);

                request.onsuccess = (evt) => {
                    let result = evt.target.result;

                    deferred.resolve(result);
                    next();
                };

                request.onerror = logAndReject(deferred);
                request.onblock = logAndReject(deferred);
            });

            return deferred.promise;
        },

        remove  : function (name, id) {
            let deferred = Q.defer();

            if (!db) {
                return open.call(this)
                    .then(() => this.remove(name, id));
            }

            addToQueue((next) => {
                let transaction = db.transaction([name], transactionType.READ_WRITE),
                    store = transaction.objectStore(name),

                    // Using [] notation to prevent lint errors with a reserved keyword.
                    request = store['delete'](id);

                // Used transaction.oncomplete in place of request.onsuccess.
                transaction.oncomplete = (evt) => {
                    debug(`${this.name}.${name}.remove( ${id} )  RESULT:`, id);
                    deferred.resolve(id);
                    next();
                };

                request.onerror = (err) => {
                    deferred.reject(err);
                    next();
                };
            });

            return deferred.promise;
        },

        save : function (name, params) {
            if (!db) {
                return open.call(this)
                    .then(() => this.save(name, params));
            }

            if (_(params).isArray()) {
                return Q.allSettled(params.map((param) => this.save(name, param)))
                    .then(results => results.map((res) => {
                        if (res.value) {
                            return {id: res.value};
                        } else {
                            return {error: res.reason};
                        }
                    }
                ));
            }

            let deferred = Q.defer();

            addToQueue(done => {
                try { // db.transaction() *can* throw immediately
                    let transaction = db.transaction([name], transactionType.READ_WRITE),
                        store       = transaction.objectStore(name),
                        data        = this[name].options.before ? this[name].options.before(params) : params,
                        request     = store.put(data);

                    transaction.oncomplete = (res) => {
                        debug(`${this.name}.${name}.save(...)  RESULT:`, request.result);
                        done();
                        deferred.resolve(request.result);
                    };

                    request.onerror = (err) => {
                        done();
                        deferred.reject(err);
                    };
                } catch (e) {
                    done();
                    deferred.reject(e);
                }
            });

            return deferred.promise;
        }
    };

}, () => !!window.indexedDB);
