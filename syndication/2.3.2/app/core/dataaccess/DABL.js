/*
* DABL.js
*/

class DABL {

    constructor (options = { }) {
        let models = { },
            findSupportedPlugin,
            plugin;

        // Find a the first supported plugin.
        findSupportedPlugin = () => {
            let plugin = _(DABL.plugins).find((plugin) => plugin.supported);

            if (!plugin) {
                throw new Error('DABL: No supported plugins found.');
            }

            return plugin;
        };

        // If there are no plugins available, throw an error.
        if (!_(DABL.plugins).size()) {
            throw new Error('DABL: No plugins found.');
        }

        // If there is no preferred plugin, find the first supported plugin.
        if (!options.prefer) {
            plugin = findSupportedPlugin();
        } else {

            // A list of preferred plugins has been supported.
            if (_(options.prefer).isArray()) {

                // Loop through each one, and use the first supported one.
                _(options.prefer).find((value) => {

                    if (DABL.plugins[value].supported) {
                        plugin = DABL.plugins[value];
                        return true;
                    }

                });

                // A preferred plugin couldn't be found, so try to find another.
                // There is bound to be cross checking with the preferred list, but it's just a boolean check, so who cares...
                if (!plugin) {
                    plugin = findSupportedPlugin();
                }

            // There is just one preferred plugin...
            } else {

                // If the plugin exists and is not supported... try and find another.
                if (DABL.plugins[options.prefer] == null || !DABL.plugins[options.prefer].supported) {
                    plugin = findSupportedPlugin();
                } else {

                    // Else, set the preferred selection as the plugin to use.
                    plugin = DABL.plugins[ options.prefer ];
                }

            }

        }

        // Call create on the plugin
        plugin = plugin.create(options);

       _(plugin).extend(_.pick(options, 'database', 'size', 'version') );

        return plugin;

    }
};

DABL.VERSION = '0.1.0';

class RecordSet {

    constructor ( records = []) {
        this.records = records;
    }

    /**
     * Returns the record at the specified index.  If no index is provided, defaults to the first record.
     * @param {number} index The index of the record to return.
     * @returns {object} The target record.
     */
    at (index = 0) {
        return this.records[index];
    }

    /**
     * Returns n records from the top of the array.
     * @param {number} count The number of records to return.
     * @returns {array} A list of records from the top of the record set.
     * @example
     * let topFive = recordSet.top(5);
     */
    top (count) {
        let output = [];

        for (let i = 0; i < count; i += 1 ) {
            let record = this.records.pop();

            if (record != null) {
                output.push(record);
            }
        }

        return output;
    }

    /**
     * returns n records from the bottom of the array.
     * @param {number} count The number of records to return from the bottom of the array.
     * @returns {array} A list of records from the bottom of the record set.
     * @example
     * let bottom5 = recordSet.bottom(5);
     */
    bottom (count) {
        let output = [];

        for (let i = 0; i < count; i += 1) {
            if (this.records[i] != null) {
                output.push(this.records.shift());
            }
        }

        return output;
    }

    /**
     * Returns the average value of a numeric field.
     * @param {string} The name of the field to average.
     * @returns {number} The average value of the field.
     * @example
     * let avg = recordSet.avg('score');
     */
    avg (field) {
        let props = _(this.records).pluck(field),
            length = props.length;

        return _(props).reduce((memo, value) => {
            return memo + value;
        }, 0) / length;
    }

    /**
     * Returns the lowest value of a field.
     * @param {string} The field to get the lowest value from.
     * @returns {number}
     * @example
     * let lowestScore = recordSet.minValue('score');
     */
    minValue ( field ) {
        let output = _(this.records).pluck(field);

        return _(output).min();
    }

    /**
     * Returns the highest value of a field.
     * @param {string} The name of the field get the highest value from.
     * @returns {number}
     * @example highestScore = recordSet.maxValue('score');
     */
    maxValue (field) {
        let output = _(this.records).pluck(field);

        return _(output).max();
    }

    /**
     * Returns a new, ordered record set.
     * @param {string} field The name of the field to order by.
     * @param {stirng} order Either ASC or DESC order.
     * @returns {RecordSet}
     * @example
     * var ordered = recordSet.orderBy('score', 'ASC');
     */
    orderBy (field, order) {
        let output = _(this.records).sortBy((item) => item[field]);

        return new DABL.RecordSet(order !== 'DESC' ? output : output.reverse());
    }

};

// Adds each individual underscore method to the record set.
_.each([
    'each', 'forEach', 'reduce', 'reduceRight', 'find', 'filter',
    'where', 'reject', 'all', 'any', 'contains', 'invoke', 'pluck',
    'max', 'min', 'sortBy', 'groupBy', 'countBy', 'shuffle', 'toArray', 'size'
], function (method) {
    RecordSet.prototype[method] = function () {
        return _[method].apply(_, [this.records].concat(_.toArray(arguments)));
    };
});

DABL.RecordSet = RecordSet;

DABL.plugins || (DABL.plugins = { });

DABL.plugin = (name, Plugin, supported) => {

    // Add the plugin to the list of plugins.
    DABL.plugins[name] = {

        // Create a new instance of the plugin.
        create : (options = {}) => {

            if (!options.name) {
                options.name = 'DEFAULT';
            }
            let output = new Plugin(options);

            // yeah this is confusing....
            // since we now have multiple open stores,
            // and one plugin instance per opened store,
            // store.name should be the db name;
            // but we need the plugin name as well for testability
            // so move that to a new field, store.plugin
            output.name = options.name;
            output.plugin = name;

            return output;
        },

        // Determines if the plugin is supported.
        supported : supported()
    };

};

export default DABL;
