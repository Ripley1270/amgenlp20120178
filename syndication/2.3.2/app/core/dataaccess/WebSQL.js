/* websql.plugin.js */
import DABL from './DABL';

DABL.plugin('WebSQL', (options) => {
    let working = false,
        noop = () => { },
        open, execute, executeAll, database, insertRecord,
        createTable, isInstalled, install,
        name = options && options.name || 'DEFAULT',
        pluginName = 'WebSQL',
        escapeSqlString = str => str.replace(/'/g, `''`),

        showSql = (query) => {
            if (options.showSql) {
                console.debug ? console.debug(`${pluginName}: ${name}: ${query}`) : console.log(`${pluginName}: ${name}: ${query}`);
            }
        },
        debug = (...args) => {
            if (options.debug) {
                console.debug ? console.debug(`${pluginName}:`, ...args) : console.log(`${pluginName}:`, ...args);
            }
        },
        error = (...args) => {
            // Special handling of SQLError, which has no useful .string()
            args = args.map(arg => arg.message ? arg.message : arg);
            console.error(`${pluginName}:', '${name}:`, ...args);
        };

    open = () => {
        // If the application is running as a native Windows app,
        // use the sqlitePlugin instead of the native WebSQL API
        if (window.cordova && window.cordova.platformId === 'windows') {
            database = window.sqlitePlugin.openDatabase({
                name,
                location: 'default'
            });
        } else {
        	database = window.openDatabase(
        		name,
            	'1.0',
            	'DABL Database',
        		(2 * 1024 * 1024)
        	);

        }

        return Q(); // right now, not asynch, but may be asynch in the future
    };

    // @todo should be able to transaction around a batch of inserts
    execute = (query) => {
        if (!database) {
            return open().then(() => execute(query));
        }

        return Q.Promise((resolve, reject) => {
            database.transaction((tx) => {
                showSql(query);

                try {
                    // Execute the database query.
                    tx.executeSql(query, [], (evt, result) => {
                        resolve(result);  // This is the promise to the API's client
                    }, (evt, err) => {
                        error(err, `while executing ${query}`);
                        reject(err);  // Disappoint the API client
                    });
                } catch (e) {
                    reject(e);
                }
            });
        });
    };

    createTable = (table) =>  `CREATE TABLE IF NOT EXISTS ${table} (id INTEGER PRIMARY KEY AUTOINCREMENT, data BLOB);`;

    insertRecord = (table, attributes) => {

        var query = `INSERT OR REPLACE INTO ${table} (id, data) VALUES (`;

        query += (attributes.id != null) ? attributes.id : 'NULL';

        // SM: uncool to modify incoming object
        //delete attributes.id;

        try {
            query += `, '${escapeSqlString(JSON.stringify(attributes))}' );`;
        } catch (e) {
            console.error(`WebSQL: Unable to stringify`, attributes);
        }
        return query;

    };

    debug(`New instance: ${name}`);

    return {

        name: name,

        collections : { },

        collection  : function (name, options = {}) {

            debug(`${this.name}.collection( "${name}", ${JSON.stringify(options)} )`);

            // A list interface methods to extend on the collection.
            let whitelist = ['save', 'get', 'remove', 'clear', 'count', 'query', 'all', 'drop'],
                nonexistent = !this[name];

            this.collections[name] = options;

            // Create the collection, but don't register it until it's persisted
            this[name] = 'pending';
            let newCollection = {
                name      : name,
                options   : options,
                installed : false
            };

            // Loop through each member on the plugin.
            _(this).each((func, funcName) => {

                // If the method name is whitelisted...
                if (_(whitelist).include(funcName)) {
                    let _options  = options; // expose options to the inner function
                    // Add the method onto the collection.  Can't use arrow shortcut... compiler issue?
                    newCollection[funcName] = _.bind(function (...args) {
                        try {
                            let options = _options;
                            debug(`${this.name}.${name}.${funcName}( ${JSON.stringify(args).slice(1,-1)} )`);
                            return func.apply(this, [name].concat(args));
                        } catch (e) {
                            error(e);
                            throw e;
                        }
                    }, this);
                }
            });

            // If the collection is nonexistent, then add it as a table.
            if (nonexistent) {
                return execute(createTable(name)).then(() => {
                    // Only now that the storage is built do we register the collection into the plugin
                    this[name] = newCollection;
                    debug(`After adding collection('${name}'); ${this.name}.collections=${_(this.collections).keys()}`);
                });
            } else {
                debug(`Collection('${name}') already present; ${this.name}.collections=${_(this.collections).keys()}`);
                return Q();
            }
        },

        // @todo should be able to transaction around a batch of inserts
        save : function (name, params) {
            if (_(params).isArray()) {
                return Q.allSettled(params.map((param) => this.save(name, param)))
                    .then(results => results.map((res) => {
                        if (res.value) {
                            return {id: res.value};
                        } else {
                            return {error: res.reason};
                        }
                    }));
            }

            return execute(insertRecord(name, params)).then(res => res.insertId);
        },

        drop : function (oops) {

            if (oops) {
                return Q.reject(`drop individual collection unimplemented. (${oops}). clear() is supported.`);
            }
            let tables = this.collections,
                operations = [],
                step;

            debug(`Dropping DB: ${this.name}; tables: ${_(this.collections).keys()}`);

            _(tables).each((table, name) => {
                operations.push({table: name, query: `DROP TABLE IF EXISTS ${name};`});
            });

            return Q.allSettled(operations.map(op =>
                execute(op.query)
                .then((res) => {
                    delete this[op.table];
                    delete this.collections[op.table];
                })
            ))
            // don't expose the Q structure to client
            .then(() => undefined);
        },

        remove : function (name, id) {
            let query = `DELETE FROM ${name} WHERE id = ${id};`;

            return execute(query).then((ignored) => id);
        },

        clear : function (name) {
            let query = `DELETE FROM ${name};`;

            return execute(query);
        },

        count : function (name) {
            let query = `SELECT COUNT(*) AS c FROM ${name};`;

            return execute(query).then((result) => {
                return result.rows.item(0).c;
            });
        },

        query : function (name, map) {
            return this.all(name).then((recordSet) => {
                let recordSet = recordSet.where(map);

                return new DABL.RecordSet(recordSet);
            });
        },

        get : function (name, id) {
            let query = `SELECT * FROM ${name} WHERE id = ${id};`;
            return execute(query).then((result) => {
                if (result.rows.length == 0) {
                    // DE17950 - instead of throwing error, empty object is returned.
                    return {};
                } else {
                    let row = result.rows.item(0);
                    row =  _.extend(JSON.parse(row.data), { id: row.id });
                    debug(`Retrieved ${name} #${id}`, JSON.stringify(row));

                    if (this[name].options.before != null) {
                        row = this[name].options.before(row);
                        debug(`Possibly modified ${name} #${id}`);
                    }

                    return row;
                }
            });
        },

        all : function (name) {
            let query = `SELECT * FROM ${name};`;

            return execute(query).then((result) => {

                var recordSet = [],
                    rowLength = result.rows.length;

                for (let i = 0; i < rowLength; i += 1) {
                    let row = result.rows.item(i);

                    row =  _.extend({ id: row.id }, JSON.parse(row.data));

                    if (this[name].options.before != null) {
                        row = this[name].options.before(row);
                    }

                    recordSet.push(row);
                }

                return new DABL.RecordSet(recordSet);
            });
        }

    };

}, () => {
	// If running as a native Windows application, WebSQL is always supported.
    if (window.cordova && window.cordova.platformId === 'windows') {
    	return true;
    } else {
    	return !!window.openDatabase;
    }
});
