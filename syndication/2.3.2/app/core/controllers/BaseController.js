import Logger from 'core/Logger';
import { Banner } from 'core/Notify';
import COOL from 'core/COOL';
import Data from 'core/Data';
import Spinner from 'core/Spinner';

// eslint-disable-next-line no-unused-vars
let logger = new Logger('BaseController');

export default class BaseController {

    /**
     * Navigate to another view.
     * @param {string} fragment - URL fragment to navigate to.
     * @param {(Object|boolean)} options - to pass to Backbone.history.navigate().
     * @param {Object} flashParams - Parameters to pass into the route's view.
     */
    navigate (fragment, options = true, flashParams) {
        LF.router.flash(flashParams).navigate(fragment, options);
    }

    /**
     * Construct, resolve and render a view.
     * @param {String} strView The name of the view to render.
     * @param {Object} clsView The default class to use in case one isn't registered with COOL.
     * @param {Object} options Options to pass to the constructor.
     * @returns {Q.Promise<void>}
     * @example this.go('ForgotPasswordView', ForgotPasswordView);
     */
    go (strView, clsView, options = {}) {
        let view = COOL.new(strView, clsView, options);

        // If resolve takes longer than 500 ms display a spinner.
        let timeoutId = window.setTimeout(() => {
            Spinner.show();
        }, 500);

        // Questionnaire views need the view assigned to Data.Questionnaire.
        Data.Questionnaire = view;

        // Resolve the views async dependencies....
        // We are passing in the timeoutId of the spinner, so resolve methods can clear the timeout if needed.
        return view.resolve(timeoutId)
            .finally(() => {
                window.clearTimeout(timeoutId);

                // DE17662 - close all banners prior displaying the next view.
                Banner.closeAll();
            })
            .then(() => {
                // Clear the existing view, and assign the new one.
                this.clear();
                this.view = view;

                return this.view.render();
            })

            // We use finally, because we always want the Spinner to be removed regardless of promise status.
            .finally(() => Spinner.hide())
            .then(() => {
                // Flush any pending banner message, if any.
                Banner.flush().done();
            });
    }

    /**
     * Authenticate, then go to the view. If not authenticated, navigates to the login view.
     * @param {String} strView The name of the view to render.
     * @param {Object} clsView The default class to use in case one isn't registered with COOL.
     * @param {Object} options Options to pass to the constructor.
     * @returns {Q.Promise<void>}
     * @example this.authenticateThenGo('EditSubjectView', EditSubjectView, { subjectId : 1 });
     */
    authenticateThenGo (...args) {
        return LF.security.checkLogin()
        .then(authenticated => {
            if (authenticated) {
                return this.go(...args);
            } else {
                return this.navigate('login');
            }
        });
    }

    /**
     * Clear the current view.
     */
    clear () {
        if (this.view) {
            this.view[this.view.close ? 'close' : 'remove']();

            this.view = null;
        }
    }
}
