import Data from './Data';
import ELF from './ELF';
import { Banner, Fullscreen } from './Notify';
import { isSitePad } from './utilities';
import ScheduleManager from './classes/ScheduleManager';
import DatabaseVersions from './collections/DatabaseVersions';
import Subjects from './collections/Subjects';
import Logger from 'core/Logger';
import CurrentSubject from 'core/classes/CurrentSubject';

const logger = new Logger('Helpers');
/**
 * Gets the string representing the current protocol
 * @param {String} subject The ID of the question you want to fetch.
 * @returns {Array|boolean}
 * @example LF.Helpers.getCurrentProtocol();
 */
export function getCurrentProtocol (subject) {
    let currentProtocolValue = subject && subject.get('custom10') ? subject.get('custom10') : LF.StudyDesign.studyProtocol.defaultProtocol;

    return LF.StudyDesign.studyProtocol.protocolList[currentProtocolValue];
}

LF.Helpers.getCurrentProtocol = getCurrentProtocol;

/**
 * Gets the answer record(s) of the current questionnaire.
 * @param {String} question The ID of the question you want to fetch.
 * @returns {Array|boolean}
 * @example LF.Helpers.getResponses('DAILY_DIARY_Q_1');
 */
export function getResponses (question) {
    let collection, views;

    if (Data.Questionnaire) {
        views = Data.Questionnaire.questionViews;
        collection = _(views).find(view => {
            return view.id === question;
        });

        if (collection && collection.widget) {
            return collection.widget.answers.toJSON();
        } else {
            return false;
        }
    } else {
        return false;
    }
}

LF.Helpers.getResponses = getResponses;

/**
 * Gets the widget of the specified question of the current questionnaire.
 * @param {String} question The ID of the question you want to fetch the widget of.
 * @returns {Object|null} returns widget of the question. Returns null if there is no widget found
 * @example LF.Helpers.getWidget('DAILY_DIARY_Q_1');
 */
export function getWidget (question) {
    let questionViews,
        view;

    if (!Data.Questionnaire) {
        return null;
    }

    questionViews = Data.Questionnaire.questionViews;
    view = _(questionViews).find(view => {
        return view.id === question;
    });

    if (view && view.widget) {
        return view.widget;
    } else {
        return null;
    }
}

LF.Helpers.getWidget = getWidget;

/**
 * Checks install of subjects in local db. Only for LogPad.
 * @return {Q.Promise<bool>} true if installed
 * @example LF.Helpers.checkInstall();
 */
export function checkInstall () {
    // eslint-disable-next-line consistent-return
    return Q.Promise((resolve) => {
        if (localStorage.getItem('krpt')) {
            const collection = new Subjects();

            return collection.fetch()
            .then(() => {
                collection.size() >= 1 ? resolve(true) : resolve(false);
            })
            .done();
        } else {
            resolve(false);
        }
    });
}

LF.Helpers.checkInstall = checkInstall;

/**
 * Checks if Time Zone was set during activation. Only for Android.
 * @return {Q.Promise<bool>} true if installed
 * @example LF.Helpers.checkTimeZoneSet();
 */
export function checkTimeZoneSet () {
    return Q.Promise((resolve, reject) => {
        LF.Wrapper.exec({
            execWhenWrapped: () => {
                plugin.TimeZoneUtil.getOptions(resolve, reject);
            },
            execWhenNotWrapped: resolve
        });
    });
}

LF.Helpers.checkTimeZoneSet = checkTimeZoneSet;

/**
 * Adds event handler for orientation change events
 * @example LF.Helpers.handleOrientationChange();
 */
export function handleOrientationChange () {
    LF.Wrapper.exec({
        execWhenNotWrapped: function () {
            let restrictOrientation;

            if (!LF.StudyDesign.disableOrientationLock) {
                restrictOrientation = function (e) {
                    Banner.closeAll();

                    $('.ui-focus').addClass('preFocus');

                    if (e.orientation === 'landscape') {
                        //The timeout is here because orientation change and keyboard dismissal can't happen at the
                        //same time on iOS without breaking things.
                        _.delay(function () {
                            //If it is landscape, remove focus from focused element for disabling virtual keyboard.
                            $('.ui-focus').blur();

                            //DE10167: Remove focus to close question list (iOS)
                            $('select').blur();

                            Fullscreen.show('PORTRAIT_ONLY');
                        }, 300);
                    } else {
                        Fullscreen.close();

                        //If it is portrait, focus the element that was focused before the landscape mode.
                        $('.preFocus').focus();
                        $('.preFocus').removeClass('preFocus');
                    }

                    // 600ms after orientation change event, reset width of footer to fill page on Android browser.
                    _.delay(function () {
                        $('.ui-footer').width($(window).width());
                    }, 600);
                };

                $(window).bind('orientationchange', restrictOrientation)
                    .trigger('orientationchange');
            }
        }
    });
}

LF.Helpers.handleOrientationChange = handleOrientationChange;

/**
 * Checks if the local storage is accessible
 * @example  LF.Helpers.handleLocalStorage();
 */
export function checkLocalStorage () {
    let handleLocalStorageErr = function () {
        $('#application').removeClass('ui-disabled');

        ELF.trigger('LOCALSTORAGE:NoSupport', {}, LF.Actions);
    };

    try {
        localStorage.setItem('LocalStorageSupport', true);
        localStorage.removeItem('LocalStorageSupport');
    } catch (err) {
        handleLocalStorageErr();
    }

    window.onerror = function (msg) {
        if (/QuotaExceededError\.*/.test(msg)) {
            handleLocalStorageErr();
        }
    };
}

LF.Helpers.checkLocalStorage = checkLocalStorage;

/**
 * Retreive an array of all schedule models from studydesign and localdatabase.
 * @returns {Array} an array of all schedules without duplicate target ids
 */
export function getScheduleModels () {
    return [
        ...(LF.StudyDesign.schedules ? LF.StudyDesign.schedules.models : []),
        ...(LF.schedules ? LF.schedules.toSchedule() : [])
    ];
}

LF.Helpers.getScheduleModels = getScheduleModels;

/**
 * Gets the current study or core database version
 * @param {String} versionName The database version. It should be 'DB_Study' or 'DB_Core'
 * @returns {Q.Promise<number>} The current database version.
 */
export function getDatabaseVersion (versionName) {
    if (versionName == null) {
        logger.error('Missing version type: version type is null or undefined');
        return Q();
    }

    return DatabaseVersions.fetchCollection()
    .then((dbVersions) => {
        const dbVersion = dbVersions.findWhere({ versionName });

        if (dbVersion) {
            return dbVersion.get('version');
        } else {
            return 0;
        }
    })
    .catch((err) => {
        err && logger.error('Error fetching database versions', err);
    });
}

LF.Helpers.getDatabaseVersion = getDatabaseVersion;

/**
 * Evaluate all alarms and re-schedule them as needed. If there is no subject, nothing is done.
 * @returns {Q.Promise<void>}
 */
export function rescheduleAlarms () {
    if (isSitePad()) {
        throw new Error('Helpers.rescheduleAlarms is not compatible with SitePad.');
        // eslint-disable-next-line no-unreachable
        return;
    }

    let schedules = getScheduleModels(),
        schedule = new ScheduleManager(),
        currentRoles,
        alarmSchedules,
        alarmFilter = schedule => {
            return !!schedule.get('alarmFunction');
        },
        roleFilter = schedule => {
            let scheduleRoles = schedule.get('scheduleRoles') || ['subject'];

            return _.intersection(scheduleRoles, currentRoles).length !== 0;
        },
        reschedule = (subject) => {
            if (Data.Users && Data.Users.length > 0) {
                currentRoles = _.invoke(Data.Users.models, 'get', 'role');
                if (localStorage.getItem('isFirstOfRole')) {
                    currentRoles = localStorage.getItem('isFirstOfRole').toString().split();
                }
            } else {
                currentRoles = ['subject'];
            }

            alarmSchedules = _.intersection(_.filter(schedules, roleFilter), _.filter(schedules, alarmFilter));
            Data.numberOfAlarmSchedules = alarmSchedules.length;
            Data.scheduleCount = 0;

            return Q.all(alarmSchedules.map(model => schedule.evaluateAlarmForSchedule(model, subject, false)));
        };

    // eslint-disable-next-line consistent-return
    return CurrentSubject.getSubject()
    .then(subject => subject && reschedule(subject));
}

LF.Helpers.rescheduleAlarms = rescheduleAlarms;
