import ModalView from './ModalView';

export default class ConfirmView extends ModalView {

    constructor () {
        super({

            /**
             * @property {Object} events A list of DOM events.
             * @readonly
             */
            events : {
                'click #cancel': 'reject',
                'click #ok': 'resolve',
                'hidden.bs.modal': 'teardown'
            }
        });

        /**
         * @property {String} template A selector that points to the template, within the DOM, to render.
         */
        this.template = '#confirm-template';

        /**
         * Invoked when the user clicks cancel.
         */
        this.reject = _.debounce(() => {
            this.hide()
            .then(() => {
                // @todo This should resolve(false).
                this.deferred.reject();
            })
            .done();
        }, 500, true);

        /**
         * Invoked when the user accepts the confirmation.
         */
        this.resolve = _.debounce(() => {
            this.hide()
            .then(() => {
                this.deferred.resolve(true);
            })
            .done();
        }, 500, true);
    }

    /**
     * Show the notification dialog.
     * @param {Object} options Strings to pass into the modal's template.
     * @param {String} options.header The header text to display.
     * @param {String} options.body The body text to display.
     * @param {String} options.ok The OK button text.
     * @param {String} options.cancel The Cancel button text.
     * @param {String} options.type The type of notification to display.
     * @returns {Q.Promise<void>}
     */
    show (options) {
        this.deferred = Q.defer();

        super.show(options);

        return this.deferred.promise;
    }

}
