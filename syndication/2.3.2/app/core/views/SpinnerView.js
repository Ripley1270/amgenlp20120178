import ModalView from './ModalView';

export default class SpinnerView extends ModalView {
    constructor () {
        super({
            /**
             * @property {Object} events A list of DOM events.
             * @readonly
             */
            events: { 'hidden.bs.modal': 'teardown' }
        });

        /**
         * @property {String} template A selector that points to the template, within the DOM, to render.
         */
        this.template = '#spinner-template';
    }

    /**
     * Show the spinner modal.
     * @param {Object} [translationKeys] Translation keys to be translated and displayed.
     * @returns {Q.Promise<void>}
     */
    show (translationKeys = { text: 'PLEASE_WAIT' }) {
        return this.i18n(translationKeys).then(text => super.show(text));
    }
}
