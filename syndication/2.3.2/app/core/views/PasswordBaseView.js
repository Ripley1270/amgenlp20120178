import ActivationBaseView from 'core/views/ActivationBaseView';
import Transmission from 'core/models/Transmission';
import { MessageRepo } from 'core/Notify';
import Logger from 'core/Logger';
import Spinner from 'core/Spinner';
import COOL from 'core/COOL';
import Data from '../Data';

// eslint-disable-next-line no-unused-vars
const logger = new Logger('PasswordBaseView');

/**
 * Defines the base view for reset and change password functionality.
 * @class PasswordBaseView
 * @extends ActivationBaseView
 * @example
 * export default class ChangePasswordView extends PasswordBaseView { }
 */
export default class PasswordBaseView extends ActivationBaseView {
    /**
     * The view's events.
     * @readonly
     * @enum {Event}
     */
    get events () {
        return {
            // Click event for back button
            'click #back': 'back',

            // Input event for new password
            'input #txtNewPassword': 'validateNewPassword',

            // Input event for confirm password
            'input #txtConfirmPassword': 'validatePasswords',

            // Submit password event
            'click #submit': 'submit'
        };
    }

    set events (events) {
        return events;
    }

    /**
     * Back button event
     */
    back () {}

    /**
     * Creates a new ResetPassword Transmission record.
     * @param {Object} params An object literal of parameters to transmit.
     * @param {Function} callback A callback function invoked upon.
     * @returns {Q.Promise<Number>} The ID of the new transmission record.
     * @example
     * this.createTransmission(JSON.stringify({
     *     password: hex_sha512(password + krpt),
     *     secret_question: subject.get('secret_question'),
     *     secret_answer: subject.get('secret_answer')
     * }}, function () {
     *   // transmission record created...
     * }):
     */
    createTransmission (params, callback = $.noop) {
        let model = new Transmission();

        return model.save({
            method: 'resetPassword',
            params: params,
            created: new Date().getTime()
        }, {
            onSuccess: callback
        });
    }

    /**
     * Submits the new password to the web service.
     * @param {Event} e Event data.
     * @returns {Q.Promise<void>}
     */
    submit (e) {
        let newPassword = this.$('#txtNewPassword').val(),
            passwordChangeParams = this.getPasswordChangeParams(newPassword);

        e.preventDefault();

        this.$('#txtNewPassword').blur();
        this.$('#txtConfirmPassword').blur();

        // Invoked upon success of form submission.
        let onSuccess = () => {
            return Spinner.hide()
            .then(() => {
                // Login as the current user with the new password.
                LF.security.login(this.user.get('id'), newPassword);

                return this.onPasswordSaved();
            });
        };

        // Displays a notification that an internet connection is required.
        let connectionRequired = () => {
            return Spinner.hide()
            .then(() => {
                return MessageRepo.display(MessageRepo.Banner.CONNECTION_REQUIRED, { delay: 900 });
            });
        };

        // Changes the subject user's credentials.
        let changeCredentials = (params) => {
            return this.user.changeCredentials(params)
            .then(() => this.user.getSubject())
            .then(subject => {
                // If the user is not a subject user, then complete the promise.
                if (!subject) {
                    return onSuccess();
                }

                let krpt = subject.get('krpt');
                let subjectData = {
                    krpt,
                    password: hex_sha512(newPassword + krpt)
                };

                params.auth = subject.getAuth();

                if (LF.StudyDesign.askSecurityQuestion) {
                    subjectData.secret_question = subject.get('secret_question');
                    subjectData.secret_answer = subject.get('secret_answer');
                }

                return this.createTransmission(JSON.stringify(subjectData))
                .then(() => {
                    return subject.save({ subject_password: hex_sha512(newPassword + subject.get('krpt')) });
                })
                .then(() => onSuccess());
            });
        };

        return Spinner.show()
        .then(() => COOL.getClass('Utilities').isOnline())
        // eslint-disable-next-line consistent-return
        .then((isOnline) => {
            if (!isOnline && !this.user.getRole().canAddOffline()) {
                return connectionRequired();
            } else {
                // If the passwords are valid, and the form is not yet submitted...
                if (this.validatePasswords(false) && !this.submitted) {
                    let params = {
                        password: passwordChangeParams.password,
                        salt: passwordChangeParams.salt
                    };

                    this.submitted = true;

                    // When changing the temporary password, we don't transmit right away
                    if (this.id === 'change-temporary-password-page') {
                        Data.changeCredentialParams = params;
                        return Spinner.hide()
                        .then(() => {
                            return this.onPasswordSaved();
                        });
                    } else {
                        return changeCredentials(params);
                    }
                }
            }
        });
    }

    /**
     * Called upon submission after the password is saved to local database.
     * @example this.onPasswordSaved();
     */
    onPasswordSaved () {}
}
