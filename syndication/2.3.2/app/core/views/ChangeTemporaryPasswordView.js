import PasswordBaseView from 'core/views/PasswordBaseView';
import { page, title } from 'core/utilities';
import Data from 'core/Data';
import Logger from 'core/Logger';

let logger = new Logger('ChangeTemporaryPasswordView');

export default class ChangeTemporaryPasswordView extends PasswordBaseView {
    constructor (options) {
        super(options);

        /**
         * The unique id of the root element.
         * @readonly
         * @type String
         * @default 'change-password-page'
         */
        this.id = 'change-temporary-password-page';

        /**
         * Id of template to render
         * @type String
         * @readonly
         * @default '#change-password-template'
         */
        this.templateSrc = '#change-temporary-password-template';

        /**
         * Strings to fetch in the render function
         * @type Object
         */
        this.stringsToFetchForRender = {
            activatedElsewhere: 'DEACTIVATED_LABEL',
            header: 'APPLICATION_HEADER',
            title: 'CHANGE_PASSWORD',
            newPassword: 'ENTER_NEW_PASSWORD',
            back: 'BACK',
            confirmPassword: 'CONFIRM_PASSWORD',
            submit: 'OK',
            submitIcon: {
                key: 'OK',
                namespace: 'ICONS'
            },
            pwdRules: 'PASSWORD_RULES'
        };
    }

    resolve () {
        return super.resolve().then(() => {
            // Initialize
            this.cacheTemplate(this.templateSrc, 'ChangeTemporaryPasswordView');
            this.fetchData(['Subjects'], () => {
                //@todo remove
            });
        });
    }

    /**
     * Navigates back to the toolbox view.
     */
    back () {
        this.navigate('login', true);
        localStorage.removeItem('PHT_changePassword');
    }

    /**
     * Renders the view.
     * @returns {Object} this ({@link LF.View.ChangeTemporaryPasswordView})
     */
    render () {
        this.template = _.template(this.templateContent);

        LF.getStrings(this.stringsToFetchForRender, (strings) => {
            this.$el.html(this.template(_.extend(strings, {
                key: this.key,
                passwordRules: LF.StudyDesign.showPasswordRules ? LF.templates.display('DEFAULT:PasswordRules', {
                    text: strings.pwdRules
                }) : ''
            })));

            title(strings.header);
            page(this.el);
        });

        return this;
    }

    /**
     * Transmit all records to the web service once the password is saved locally.
     * @example this.onPasswordSaved();
     */
    onPasswordSaved () {
        let user = this.user;

        logger.operational(`Password set by ${user.get('role')} ${user.get('username')}`, {
            username: user.get('username'),
            role: user.get('role')
        });

        LF.security.pauseSessionTimeOut();
        try {
            if (LF.StudyDesign.askSecurityQuestion) {
                localStorage.setItem('Reset_Password_Secret_Question', true);
                this.navigate('reset_secret_question', true);
            } else {
                localStorage.removeItem('PHT_changePassword');

                user.changeCredentials(Data.changeCredentialParams)
                .then(() => LF.security.login(this.user.id, $('#txtNewPassword').val()))
                .then(() => {
                    Data.changeCredentialParams = undefined;
                    this.navigate('dashboard', true);
                });
            }
        }
        finally {
            LF.security.restartSessionTimeOut();
        }
    }
}

window.LF.View.ChangeTemporaryPasswordView = ChangeTemporaryPasswordView;
