import * as Utilities from 'core/utilities';
import PageView from 'core/views/PageView';
import { Banner, MessageRepo } from 'core/Notify';
import Users from 'core/collections/Users';
import * as lStorage from 'core/lStorage';
import CurrentContext from 'core/CurrentContext';
import Logger from 'core/Logger';
import Spinner from 'core/Spinner';
import COOL from 'core/COOL';

const logger = new Logger('Core/LoginView');

export default class LoginView extends PageView {
    constructor (options = {}) {
        super(options);

        /**
         * @property {string} template A selector that points to the template for the view.
         * @readonly
         * @default '#context-tpl'
         */
        this.template = '#context-tpl';

        /**
         * @property {object} events A list of DOM events.
         * @readonly
         */
        this.events = {
            'click #login': 'login',
            'input #password': 'validate',
            'click #forgot-password': 'forgotPassword',
            'focus #password': 'passwordFocused',
            'change #user': 'userChanged',
            'click #sync-button': 'transmit',
            'change #languages': 'updateLanguage',
            'click #back': 'back',
            'click #next': 'forward'
        };

        /**
         * @property {object} templateStrings A list of resource keys to translate and render to the UI.
         */
        this.templateStrings = {
            header: 'APPLICATION_HEADER',
            name: 'NAME',
            user: 'USER',
            role: 'USER_ROLE',
            password: 'PASSWORD',
            login: 'LOG_IN',
            language: 'LANGUAGE',
            forgotPassword: 'FORGOT_PASSWORD',
            welcome: 'WELCOME',
            invalidPass: 'PASSWORD_INVALID',
            noResults: 'NO_RESULTS_FOUND',
            userMenuRefresh: 'BLUETOOTH_REG_REFRESH_BUTTON',
            back: 'BACK',
            next: 'NEXT'
        };

        logger.traceEnter('Constructor');

        /**
         * @property {viewTriggerName} users A collection of users.
         */
        this.viewTriggerName = 'LOGIN';

        /**
         * @property {Users} users A collection of users.
         */
        this.users = CurrentContext().get('users');

        /**
         * @property backArrow function called when backArrow is clicked
         */
        this.backArrow = options.backArrow;

        /**
         * @property forwardArrow function called when forwardArrow is clicked
         */
        this.forwardArrow = options.forwardArrow;

        /**
         * @property usersFilter function called when filtering users that are allowed to login
         */
        this.filterUsers = options.filterUsers;

        /**
         * @property sortUsers function called to sort the list of users after they have been filtered
         */
        this.sortUsers = options.sortUsers;

        this.noRoleFound = options.noRoleFound;

        /**
         * @property back function called when back button is clicked
         */
        this.back = _.debounce((evt) => {
            evt.preventDefault();

            if (typeof this.backArrow === 'function') {
                this.disableButton(this.$back);
                this.backArrow();
            } else {
                // TODO: err
            }
        }, 1000, true);

        /**
         * @property forward function called when forward button is clicked
         */
        this.forward = _.debounce((evt) => {
            evt.preventDefault();

            if (typeof this.forwardArrow === 'function') {
                this.disableButton(this.$next);
                this.forwardArrow();
            } else {
                // TODO: err
            }
        }, 1000, true);

        this.subject = options.subject;
        this.visit = options.visit;

        logger.traceExit('Constructor');
    }

    /**
     * @property {string} id The ID of the view.
     * @readonly
     * @default 'login-view'
     */
    get id () {
        return 'core-login-view';
    }

    /**
     * @property {object} an object literal of key value pairs for the JQuery selectors to be cached.
     * @readonly
     */
    get selectors () {
        return {
            user: '#user',
            password: 'input#password',
            login: 'button#login',
            forgotPassword: '#forgot-password',
            languages: '#languages',
            back: '#back',
            next: '#next',
            footer: '#footer'
        };
    }

    /**
     * @property {string} id of the template for the user dropdown selected option
     * @readonly
     * @default '#user-name-login-selection-option'
     */
    get selectMenuSelectionTemplate () {
        return '#user-name-login-selection-option';
    }

    /**
     * @property {string} id of the template for the user dropdown options
     * @readonly
     * @default '#language-login-option'
     */
    get selectMenuOptionTemplate () {
        return '#user-name-login-option';
    }

    /**
     * @property {string} id of the template for the language select options
     * @readonly
     * @default '#language-login-option'
     */
    get languageSelectionTemplate () {
        return '#language-login-option';
    }

    /**
     * performs any asynchronous initialization
     * @param {number} timeoutId A timeout ID of a spinner.
     * @returns {Q.Promise<void>}
     */
    resolve (timeoutId) {
        // Clear the spinner timeout so it doesn't display, disrupting the modal.
        window.clearTimeout(timeoutId);
        Spinner.show();

        // DE16896 - Deactivated users are displaying in user list.
        // The user's on this view are cached, and this.users.fetch() doesn't merge
        // the existing collection with the new data fetched.  Resetting the
        // collection allows for fresh data from the database.
        this.users.reset();
        CurrentContext().get('subjects').reset();

        return Q.all([this.users.fetch(), CurrentContext().get('subjects').fetch()])
        .then(() => {
            if (this.filterUsers) {
                let allFilteredUsers = [];

                return Q.all(this.users.map((user) => {
                    return this.filterUsers(user)
                    .then((user) => {
                        if (user && user.get('active') === 1) {
                            allFilteredUsers.push(user);
                        }
                    })
                    .catch(() => {
                        // TODO: Doo err heeerrre
                    });
                }))
                .then(() => {
                    return allFilteredUsers;
                });
            } else {
                return this.users;
            }
        })
        .then((filteredUsers) => {
            if (this.sortUsers) {
                return this.sortUsers(filteredUsers)
                .catch(() => {
                    // TODO: Errorssss
                });
            } else {
                return filteredUsers;
            }
        })
        // eslint-disable-next-line consistent-return
        .then((sortedUsers) => {
            if (sortedUsers && sortedUsers.length) {
                this.user = sortedUsers[0];
                return CurrentContext().setContextByUser(this.user);
            }

            // If no users of the assignable roles exist in the system, display a notification...
            // and then navigate back to the login view.
            if (this.noRoleFound) {
                return Spinner.hide()
                .then(() => {
                    const { Dialog } = MessageRepo;
                    return MessageRepo.display(Dialog && Dialog[this.noRoleFound])
                    .then(() => {
                        LF.router.navigate('login', true);

                        return Q.reject();
                    });
                });
            } else {
                logger.error('No Users found for the roles required. Unable to display error message to user because \'noRoleFound\' is not configured at the questionnaire level');
                LF.router.navigate('login', true);
                return Q.reject();
            }

            // Hide any spinners that may remain.
        })
        .finally(() => Spinner.hide());
    }

    /**
     * Render the view to the DOM.
     * example this.render();
     * @returns {Q.Promise<LoginView>} promise resolved when render is complete
     */
    render () {
        return this.buildHTML({ version: LF.coreVersion }, true)
        .then(() => this.userLoginSetup());
    }

    // TODO: Add JS Docs
    userLoginSetup () {
        return Q()
        .then(() => {
            if (this.user && this.user.get('userType') === 'Admin') {
                this.$forgotPassword.hide();

                this.showElement('#admin-language-selection');

                this.$languages.data('select2') && this.$languages.select2('destroy').html('');
                this.$languages.select2({
                    escapeMarkup: (markup) => markup,
                    data: this.createLanguageSelectionData(),
                    templateSelection: (item) => this.getTemplate(this.languageSelectionTemplate)({ item }),
                    templateResult: (item) => this.getTemplate(this.languageSelectionTemplate)({ item }),
                    minimumResultsForSearch: Infinity,

                    // Defer width of select2 in favor of the CSS preferences for this element.
                    width: ''
                });
            }
        })
        .then(() => this.renderUsers())
        .then(() => {
            if (this.backArrow && !this.forwardArrow) {
                this.hideElement(this.$next);
            } else if (!this.backArrow && this.forwardArrow) {
                this.hideElement(this.$back);
            }

            if (this.backArrow || this.forwardArrow) {
                this.showElement(this.$footer);
            }
        })
        .then(() => this.translateToUserLanguage())
        .then(res => {
            if (res) {
                this.undelegateEvents();
                return this.render();
            } else {
                return ELF.trigger(`${this.viewTriggerName}:Rendered`, { subject: this.subject }, this);
            }
        })
        .catch(err => {
            logger.error('render Exception', err);
        })
        .finally(() => {
            this.delegateEvents();
            this.buildSelectors();

            // Validate after everything is drawn.  Fixes edge case where auto-population of the password
            //  from browser history disables everything if done before the user was initialized.
            this.validate();
            logger.trace('Finished rendering LoginView');
            return this;
        });
    }

    /**
     * Render the users to the view.
     * @example this.renderUsers();
     * @returns {Q.Promise}
     */
    renderUsers () {
        let allFilteredUsers = [];

        this.users = this.users || new Users();
        this.userSelectData = [];

        return Q.Promise((resolve) => {
            // eslint-disable-next-line consistent-return
            return Q.all(this.users.map(user => {
                if (user.get('active') === 1) {
                    if (this.filterUsers) {
                        return this.filterUsers(user)
                        .then((user) => {
                            if (user) {
                                allFilteredUsers.push(user);
                            }
                        })
                        .catch(() => {
                            // TODO: log an error if passed in filtering users unction sucks
                        });
                    } else {
                        allFilteredUsers.push(user);
                    }
                }
            }))
            .then(() => {
                resolve(allFilteredUsers);
            });
        })
        .then((filteredUsers) => {
            if (this.sortUsers) {
                return this.sortUsers(filteredUsers)
                .then((sortedUsers) => {
                    return Utilities.promiseChain(sortedUsers, (previousResult, user) => {
                        return this.addUser(user);
                    });
                })
                .catch(() => {
                    // TODO: log error if sort users function blows
                });
            } else {
                return Utilities.promiseChain(filteredUsers, (previousResult, user) => {
                    return this.addUser(user);
                });
            }
        })
        .then(() => this.i18n(_.pick(this.templateStrings, ['noResults', 'userMenuRefresh'])))
        .then((text) => Q.Promise(resolve => {
            $.fn.select2.amd.require([
                'select2/utils',
                'select2/dropdown',
                'select2/dropdown/attachBody',
                'select2/dropdown/button',
                'select2/dropdown/closeOnSelect'
            ], (utils, dropdownAdapter, attachBody, button, closeOnSelect) => {
                // eslint-disable-next-line new-cap
                let decorated = utils.Decorate(dropdownAdapter, attachBody);
                // eslint-disable-next-line new-cap
                decorated = utils.Decorate(decorated, button);
                // eslint-disable-next-line new-cap
                decorated = utils.Decorate(decorated, closeOnSelect);

                this.$user.data('select2') && this.$user.select2('destroy').html('');
                this.$user.select2({
                    language: { noResults: () => text.noResults },
                    escapeMarkup: (markup) => markup,
                    data: this.userSelectData,
                    templateSelection: (item) => this.getTemplate(this.selectMenuSelectionTemplate)({ item }),
                    templateResult: (item) => this.getTemplate(this.selectMenuOptionTemplate)({ item }),
                    dropdownAdapter: decorated,
                    buttonText: text.userMenuRefresh,
                    onButtonClick: () => {
                        this.refreshUsers().done();
                    },

                    // Defer width of select2 in favor of the CSS preferences for this element.
                    width: ''
                });

                resolve();
            });
        }));
    }

    /**
     * Sync Users from server when refresh button of User select menu is pressed.
     * @returns {Q.Promise}
     */
    refreshUsers () {
        this.$user.select2('close');
        this.spinner.show();

        return COOL.getClass('Utilities').isOnline()
        .then(onlineStatus => {
            if (onlineStatus) {
                return Users.syncUsers()
                .then(() => this.users.reset())
                .then(() => this.users.fetch())
                .then(() => this.spinner.hide())
                .then(() => {
                    let selectedUserId = parseInt(this.$user.val(), 10);
                    this.user = this.users.findWhere({ id: selectedUserId });
                    return this.translateToUserLanguage();
                })
                .then(() => {
                    this.undelegateEvents();
                    return this.resolve();
                })
                .then(() => this.render())
                .then(() => {
                    this.$user.select2('open');
                })
                .catch(err => {
                    err && logger.error('Error refreshing Users: ', err);
                });
            } else {
                return Q.promise(resolve => {
                    this.spinner.hide()
                    .then(() => {
                        this.confirm({
                            dialog: MessageRepo.Dialog && MessageRepo.Dialog.CONFIRM_TRANSMIT_RETRY_CONNECTION_REQUIRED
                        }, (res) => {
                            res && setTimeout(() => {
                                this.refreshUsers();
                            }, 1000);
                            resolve();
                        });
                    })
                    .done();
                });
            }
        });
    }

    /**
     * Add the user record to the view.
     * @param {User} user The user to add.
     * @returns {Q.Promise<Object>} resolves with an object representing the select2 data for the added user.
     */
    addUser (user) {
        //noinspection JSUnresolvedFunction
        let roleSettings,
            { username, id, language, lastLogin } = user.toJSON(),
            role = user.getRole(),
            userLanguage = language.split('-')[0],
            userLocale = language.split('-')[1],
            contentLanguageModel = LF.strings.filter((model) => model.get('language') === userLanguage &&
                model.get('locale') === userLocale)[0] ||
                CurrentContext().get('defaultLanguageModels')[0];

        if (!role) {
            return Q();
        }

        roleSettings = LF.StudyDesign.roles.find((item) => item.id === role.id);

        // Translate the role
        return this.i18n({
            displayName: role.get('id') === 'subject' ? 'SUBJECT' : role.get('displayName'),
            userName: username,
            roleLabel: 'ROLES'
        }, $.noop, {
            language: userLanguage,
            locale: userLocale
        })
        .then((res) => {
            let uName = (res.userName.substring(0, 2) !== '{{') ? res.userName : username;

            // Used for displaying the initials of the subjects on Sitepad
            if (role.get('id') === 'subject') {
                let subject = CurrentContext().get('subjects').findWhere({ user: user.get('id') }),
                    initials = subject ? subject.get('initials') : undefined;

                if (initials) {
                    uName = `${uName} (${initials})`;
                }
            }

            let userData = {
                id: id,
                lastLogin: lastLogin,
                userName: uName,
                roleLabel: res.roleLabel,
                roleDisplayName: res.displayName,
                fontFamily: Utilities.getFontFamily(language),
                dir: contentLanguageModel.get('direction'),

                // may not be needed anymore
                cssClass: contentLanguageModel.get('direction') === 'rtl' ? 'right-direction' : 'left-direction',
                iconClass: roleSettings && roleSettings.get('userSelectIconClass'),
                selected: id === this.user.id
            };

            this.userSelectData.push(userData);
            return userData;
        });
    }

    // TODO: JS Docs...
    //selectMenuUserEntry ()

    /**
     * Triggered when the selected user has changed.
     * @param {object} evt onChange event data.
     * @example this.userChanged();
     */
    userChanged (evt) {
        this.translateToUserLanguage().done(() => {
            evt.preventDefault();
            this.undelegateEvents();
            this.render();
        });
    }

    /**
     * Translate the UI to the selected user's preferred language.
     * @returns {Q.Promise<boolean>}
     * @example this.translateToUserLanguage();
     */
    translateToUserLanguage () {
        let selectedUserId = parseInt(this.$user.val(), 10),
            user = this.users.findWhere({ id: selectedUserId }),
            oldLanguage = LF.Preferred.language,
            oldLocale = LF.Preferred.locale;

        !_.isNaN(selectedUserId) ? lStorage.setItem('User_Login', selectedUserId) : '';

        return Q()
        .then(() => {
            if (!user) {
                return false;
            }

            return CurrentContext().setContextByUser(user)
            .then(() => {
                let language = user.get('language').split('-');

                this.stopListening(this.user);
                this.user = user;
                this.listenTo(this.user, 'change:language', (user, language) => {
                    CurrentContext().setContextLanguage(language)
                    .then(() => this.users.add(user, { merge: true }))
                    .then(() => this.users.save())
                    .then(() => {
                        this.undelegateEvents();
                        return this.render();
                    })
                    .done();
                });

                // DE20539 fix
                if (LF.Preferred.language !== oldLanguage || LF.Preferred.locale !== oldLocale) {
                    // Fix for DE17606: should apply to LogPad Product only
                    localStorage.setItem('preferredLanguageLocale', `${language[0]}-${language[1]}`);
                    return true;
                }

                return false;
            });
        });
    }

    /**
     * Validate the login form.
     * @example this.validate();
     */
    validate () {
        if (this.$user.val()) {
            let username = this.$user.val(),
                password = this.$password.val();

            this.enableButton(this.$password);

            if (username && password) {
                this.enableButton(this.$login);
            } else {
                this.disableButton(this.$login);
            }
        } else {
            this.disableButton(this.$password);
            this.disableButton(this.$login);
        }
    }

    /**
     * Login to the App.
     * @param {object} e Event data.
     * @returns {(boolean|undefined)}
     * @example this.login();
     */
    login (e) {
        let id = parseInt(this.$user.val(), 10),
            password = this.$password.val(),
            isAdminUser = this.user ? this.user.get('userType') === 'Admin' : false,
            setSecurityQuestion = !isAdminUser && this.user && (this.user.get('secretQuestion') === '' || this.user.get('secretQuestion') == null) &&
                LF.StudyDesign.askSecurityQuestion === true,
            failedAttempt = () => {
                logger.error('Failed login attempt by username={{ username }} with role {{ role }}.',
                { username: this.user ? this.user.get('username') : '', role: this.user ? this.user.get('role') : '' });

                if (!isAdminUser) {
                    LF.security.setLoginFailure(LF.security.getLoginFailure(this.user) + 1, this.user);
                }

                this.inputError(this.$password);
                this.$password.val('');
                this.disableButton(this.$login);

                // DE17426 - If the user is already locked out, we don't need to display the banner.
                if (!LF.security.checkLock(this.user)) {
                    MessageRepo.display(MessageRepo.Banner[this.templateStrings.invalidPass]);
                }
            };

        logger.traceEnter('login');

        e && e.preventDefault();

        if (!password) {
            return false;
        }

        if (!isAdminUser && LF.security.checkLock(this.user)) {
            failedAttempt();
            this.forgotPassword();
        } else {
            LF.security.login(id, password)
            .then(user => {
                if (!!user) {
                    Banner.closeAll();

                    if (setSecurityQuestion) {
                        localStorage.setItem('Reset_Password_Secret_Question', true);
                    }

                    if (lStorage.getItem('changePassword')) {
                        this.navigate('change_temporary_password', true);
                    } else if (setSecurityQuestion) {
                        this.navigate('reset_secret_question', true);
                    } else {
                        let params = {};
                        if (this.subject) {
                            params.subject = this.subject;
                            params.visit = this.visit;
                        }
                        this.navigate('dashboard', true, params);
                    }
                } else {
                    failedAttempt();

                    if (!isAdminUser) {
                        if (LF.StudyDesign.maxLoginAttempts === LF.security.getLoginFailure(this.user)) {
                            logger.error('user={{ username }} has been locked out.', { username: this.user && this.user.get('username') });

                            if (LF.security.accountDisable(this.user)) {
                                this.forgotPassword();
                            }
                        }
                    }
                }
            })
            .catch(err => {
                logger.error('Error checking login from LoginView', err);
            })
            .done();
        }

        logger.traceExit('login');

        // Return is required to pass lint.
        return false;
    }

    /**
     * Clear the input state on the password field.
     * @example this.passwordFocused();
     */
    passwordFocused () {
        this.clearInputState(this.$password);
    }

    /**
     * Navigate to the forgot password view.
     */
    forgotPassword () {
        if (this.user) {
            let userQuestion = parseInt(this.user.get('secretQuestion'), 10);
            if (LF.StudyDesign.askSecurityQuestion && !_.isNaN(userQuestion)) {
                localStorage.setItem('Forgot_Password', true);
                this.navigate('forgot-password', true);
            } else {
                localStorage.setItem('Unlock_Code', true);
                this.navigate('unlock_code', true);
            }
        }
    }

    /**
     * Display an input error.
     * @param {object} input The input to set to an errant state.
     * @returns {Object} jQuery element
     * @example this.inputError($('#password'));
     */
    inputError (input) {
        let $parent = input.parent('.input-group'),
            icon = '<span class="glyphicon glyphicon-minus-sign form-control-feedback" aria-hidden="true"></span>';

        this.clearInputState(input);

        return $parent.addClass('has-feedback has-error').append(icon);
    }

    /**
     * Clear an input of its validation state.
     * @param {object} input The input to clear of it's state.
     * @example this.clearInputState($('#password'));
     */
    clearInputState (input) {
        let $parent = input.parent('.input-group'),
            $icon = input.siblings('.form-control-feedback');

        if ($parent.hasClass('has-feedback')) {
            $icon.remove();

            // Remove any state-related classes from the input.
            $parent.removeClass('has-feedback has-success has-warning has-error');
        }
    }

    /**
     * Creates and returns language data
     * @return {Array} data to populate the language dropdown list
     */
    createLanguageSelectionData () {
        let selectedRoleSupportedLanguages,
            localized = LF.strings.match({ namespace: 'CORE', localized: {} }),
            langs = [];

        logger.traceEnter('createLanguageSelectionData');

        if (this.user) {
            selectedRoleSupportedLanguages = this.user.get('supportedLanguages') || 'ALL';
        }

        // get the localized names from the resource string files in order of localized language name
        localized.sort((firstComparatorObject, secondComparatorObject) => {
            let firstComparator = firstComparatorObject.get('localized'),
                secondComparator = secondComparatorObject.get('localized');
            return (firstComparator > secondComparator) ? 1 : (firstComparator < secondComparator) ? -1 : 0;
        }).forEach((language) => {
            let langCode = `${language.get('language')}-${language.get('locale')}`,
                lang = {
                    id: langCode,
                    localized: language.get('localized'),
                    fontFamily: Utilities.getFontFamily(langCode),
                    dir: language.get('direction'),
                    cssClass: language.get('direction') === 'rtl' ? 'right-direction text-right-absolute' : 'left-direction text-left-absolute',
                    selected: langCode === this.user.get('language')
                };

            if (selectedRoleSupportedLanguages.toString() === 'ALL') {
                langs.push(lang);
            } else if (_.contains(selectedRoleSupportedLanguages, langCode)) {
                langs.push(lang);
                selectedRoleSupportedLanguages = _.without(selectedRoleSupportedLanguages, langCode);
            }
        });

        logger.traceExit('createLanguageSelectionData');

        return langs;
    }

    /**
     * Updates the view with the selected language-locale strings
     * when the user selects a new language
     */
    updateLanguage () {
        let lang = this.$('#languages').val();

        if (this.user.get('language') !== lang) {
            this.user.save({
                language: lang
            }, {
                onSuccess: () => {
                    CurrentContext().setContextLanguage(lang);
                    logger.operational('AdminUserLanguage ={{ language }} has been saved.', { language: this.user.get('language') });
                    this.undelegateEvents();
                    this.render();
                },
                onError: (err) => {
                    logger.error('AdminUser save language Exception', err);
                }
            });
        }
    }

    /**
     * When the view is closed, make sure select menu and spinner also close.
     */
    close () {
        this.$user && this.$user.data('select2') && this.$user.select2('destroy').html('');
        this.spinner.hide();
    }
}
window.LF.View.LoginView = LoginView;
