import {Banner} from '../Notify';
import Widget from '../models/Widget';

// Ensure the widgets are available.
import '../widgets/BarcodeScanner';
import '../widgets/CheckBox';
import '../widgets/CustomCheckBox';
import '../widgets/CustomRadioButton';
import '../widgets/VAS/VAS';
import '../widgets/NumericRatingScale';
import '../widgets/PatientIDTextBox';
import '../widgets/FreeTextBox';
import '../widgets/TextBox';
import '../widgets/PatientLangListRadioButton';
import '../widgets/AddUserTextBox';
import '../widgets/TempPasswordTextBox';
import '../widgets/ConfirmationTextBox';
import '../widgets/LanguageSelectWidget';
import '../widgets/RoleSelectWidget';
import '../widgets/PasswordMessageWidget';
import '../widgets/SignatureBox';
import '../widgets/TimePicker.js';
import '../widgets/DatePicker.js';
import '../widgets/DateTimePicker.js';
import '../widgets/NumberSpinner.js';
import '../widgets/NumericTextBox.js';
import '../widgets/DropDownList';
import '../widgets/HiddenField';
import '../widgets/SecretAnswerTextBox';
import '../widgets/SecretQuestionList';
import '../widgets/StaticField';
import '../widgets/StaticRoleField';
import '../widgets/StaticLanguageField';
import '../widgets/SelectTimeZone';
import '../widgets/DateSpinner';
import '../widgets/DateTimeSpinner';
import '../widgets/TimeSpinner';
import '../widgets/MatrixBase';
import '../widgets/MatrixRadioButton';
import '../widgets/Browser';

import Logger from 'core/Logger';

const logger = new Logger('QuestionView');

export default class QuestionView extends Backbone.View {

    constructor (options) {
        /**
         * The root element of the view.
         * @readonly
         * @type String
         * @default 'div'
         */
        this.tagName = 'div';

        /**
         * The class of the root element
         * @readonly
         * @type String
         * @default 'question'
         */
        this.className = 'question';

        /**
         * The template of the Site User of the Affidavit
         * @type String
         * @default 'DEFAULT:SiteUser'
         */
        this.siteUserTemplate = 'DEFAULT:SiteUser';

        /**
         * Strings to fetch in the render function
         * @type Object
         */
        this.stringsToFetchForRender = {
            site: 'SITE',
            subjectId: 'SUBJECT_ID',
            siteUserLabel: 'SITE_SIGNATURE'
        };

        this.options = options;

        // TODO - Fix 'super before this' error.
        super(options);

        // Pointer to parent view (Questionnaire)
        this.parent = this.options.parent;

        // Store the ig, igr and screen ID for the current instance
        this.ig = this.options.ig;
        this.igr = this.options.igr;
        this.screenID = this.options.screen;

        this.templates = this.model.get('templates') || {};
        this.siteUserTemplate = this.templates.siteUserTemplate || this.siteUserTemplate;

        this.widgetModel = false;
        if (this.model.get('widget') instanceof Widget) {
            this.widgetModel = this.model.get('widget');
        } else if (this.model.get('widget')) {
            this.widgetModel = new Widget(this.model.get('widget'));
        }

        // Error check here to ensure the configured widget actually exists
        if (this.widgetModel) {
            if (LF.Widget[this.widgetModel.get('type')] === undefined) {
                let msg = `Widget ${this.widgetModel.get('type')} not found.`,
                    e = new Error(msg);
                logger.error(msg, e);
                throw e;
            }
        }

        this.widget = (this.widgetModel) ? new LF.Widget[this.widgetModel.get('type')]({
            model: this.widgetModel,
            mandatory: this.options.mandatory,
            parent: this
        }) : false;

    }

    /**
     * Renders the view.
     * @param {String} appendTo The selector of the container to append the question to.
     * @returns {Q.Promise<void>} resolved when complete
     */
    render (appendTo) {
        let stringsToFetchForRender = _.clone(this.stringsToFetchForRender),
            text = this.model.get('text'),
            i = 0,
            title = this.model.get('title'),
            templates = (this.model.get('templates') || {});

        return Q().then(() => {
            this.$el.appendTo(`${appendTo}`);

            // If the question has a title, fetch the correct resource string to display.
            if (title) {
                stringsToFetchForRender.title = {
                    key: title,
                    namespace: this.parent.id
                };
            }

            if (_.isArray(text)) {
                stringsToFetchForRender.text = [];

                for (; i < text.length; i++) {
                    stringsToFetchForRender.text[i] = {
                        key: text[i],
                        namespace: this.parent.id
                    };
                }
            } else {
                stringsToFetchForRender.text = {
                    key: text,
                    namespace: this.parent.id
                };
            }

            return LF.getStrings(stringsToFetchForRender);
        }).then((strings) => {
            logger.info(`Displaying question ${this.model.get('id')} to user.`);

            this.$el.html('')
                .attr('id', this.model.get('id'))
                .addClass(this.model.get('className'));

            if (strings.title) {
                this.$el.append(LF.templates.display('DEFAULT:QuestionHeader', {
                    left: strings.title || '',
                    right: ''
                }));
            }

            if (_(text).isArray()) {
                for (i = 0; i < text.length; i++) {
                    this.$el.append(LF.templates.display(templates.question || 'DEFAULT:Question', {
                        label: `${text[i]} text`,
                        text: strings.text[i]
                    }));
                }
            } else {
                this.$el.append(LF.templates.display(templates.question || 'DEFAULT:Question', {
                    text: strings.text,
                    label: `${text} text`
                }));
            }
            /*
             if (affidavitWidgetType === signatureAffidavitWidgetType) {
             siteUser = this.parent.getSiteUser();
             if (!!siteUser) {
             this.$el.append(LF.templates.display(this.siteUserTemplate, {
             siteUserLabel   : strings.siteUserLabel,
             siteUser        : siteUser
             }));
             }
             }
             */

            if (this.widget) {
                //This is fix for DE8102: double vas issue on Jellybean
                if (this.widget.model.get('type') === 'HorizontalVAS') {
                    $('.ui-content').addClass('vas-overflow-visible');
                } else {
                    $('.ui-content').removeClass('vas-overflow-visible');
                }

                if (this.widget.bindResize) {
                    this.widget.bindResize();
                }

                // Run the setup on the model first, which will execute any functions
                // that will provide values for widget attributes.
                return this.widget.setupModel()
                .then(() => {
                    return ELF.trigger('WIDGET:Rendering',{widget: this.widget});
                })
                .then(() => {
                    return this.widget.render();
                })
                .then(() => {
                    this.widget.on('response', () => this.clearErrors());
                })
                .catch((reason) => {
                    const msg = `Error thrown during widget setup: ${reason}`;
                    logger.error(msg);
                    throw new Error(msg);
                });
            } else {
                this.completed = true;
                this.options.mandatory = false;
                return Q.resolve();
            }
        });
    }

    /**
     * Adds a red border around the root element.
     * @returns {boolean} true if an error message was displayed by the widget
     * @example let errorShown = this.displayError();
     */
    displayError () {
        let errorShown = false;

        if (this.widget && this.widget.displayError) {
            errorShown = this.widget.displayError();
        }
        this.$el.addClass('ui-state-error-border');

        return errorShown;
    }

    /**
     * Clears all errors.
     * @example this.clearErrors();
     */
    clearErrors () {
        if (this.$el.hasClass('ui-state-error-border')) {
            setTimeout(Banner.closeAll, 500);
            this.$el.removeClass('ui-state-error-border');
        }
    }

    /**
     * Save all of the question's answer records.
     * @returns {Q.Promise<void>} - rejected if there was a save error
     */
    save () {
        let deferred = Q.defer();

        if (this.widget) {
            this.widget.save()
                .then(deferred.resolve)
                .catch(deferred.reject);
        } else {
            deferred.resolve();
        }

        return deferred.promise;
    }

    /**
     * Attempts to skip the question.
     * @return {Boolean} Returns true if it is skippable, otherwise false.
     */
    attemptToSkip () {
        let IG, IGR, IT;

        if (!this.options.mandatory) {
            if (this.completed) {
                return true;
            } else {
                IG = this.model.get('IG');
                IGR = this.parent.getCurrentIGR(IG);
                IT = this.model.get('skipIT');

                return this.widget.skip(`${IG}.${IGR}.${IT}`);
            }
        } else {
            return false;
        }
    }

    /**
     * Check if the question is not mandatory and the question hasn't been answered.
     * @return {Boolean} Returns true if the question is not mandatory and the question hasn't been answered, otherwise false.
     */
    isSkippable () {
        if (this.widget) {
            return !this.options.mandatory && !this.widget.completed;
        } else {
            return !this.options.mandatory;
        }
    }

    /**
     * Validate the questions widget.
     * @return {Boolean} Return true if the question widget valid, otherwise false.
     */
    validate () {
        if (this.widget && !this.widget.validate()) {
            return false;
        }

        return true;
    }

    /**
     * Gets the questionnaire (parent) of this question
     * @returns {QuestionnaireView}
     */
    getQuestionnaire () {
        return this.parent;
    }
}

window.LF.View.QuestionView = QuestionView;
