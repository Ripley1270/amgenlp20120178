import PageView from 'core/views/PageView';
import Data from 'core/Data';
import CurrentContext from 'core/CurrentContext';
import { MessageRepo } from 'core/Notify';
import Transmission from 'core/models/Transmission';
import * as lStorage from 'core/lStorage';
import COOL from 'core/COOL';

/**
 * Defines the base view for reset and change secret question functionality.
 * @class SecretQuestionBaseView
 * @extends PageView
 * @example
 * var view = LF.View.SecretQuestionBaseView.extend({
 *    // Options here...
 * });
 */
export default class SecretQuestionBaseView extends PageView {
    constructor (options) {
        super(options);
        /**
         * The flag prevents users from submitting the form consecutively
         * @type Boolean
         * @default false
         */
        this.submitted = false;

        /**
         * The view's events.
         * @readonly
         * @enum {Event}
         */
        this.events = {
            // Tap event for going back
            'click #back': 'back',

            // Input event for secret answer
            'input #secret_answer': 'validate',

            // Submit form event
            'click #submit': 'submit',

            // Change event for secret question drop down list
            'change #secret_question': 'clearSecretAnswer'
        };

        this.subject = options.subject;

        // TODO: Move to a resolve method.
        LF.security.getUser(parseInt(lStorage.getItem('User_Login'), 10), true)
        .then(user => {
            this.user = user;
        })
        .done();
    }

    /**
     * Back button event
     */
    back () {}

    /**
     * Creates a new ResetSecretQuestion Transmission record.
     * @param {Object} params An object literal of parameters to transmit.
     * @returns {Q.Promise<number>} The ID of the new transmission record.
     * @example
     * this.createTransmission(JSON.stringify({
     *     secret_question: LF.Data.question,
     *     secret_answer: LF.Data.answer
     * });
     */
    createTransmission (params) {
        let model = new Transmission();

        return model.save({
            method: 'resetSecretQuestion',
            params: params,
            created: new Date().getTime()
        });
    }

    /**
     * Submits the new secret question and answer to the web service.
     * @param {Event} e Event data.
     */
    submit (e) {
        let user = this.user,
            updateUser,
            createTransmission,
            setCookie;

        e.preventDefault();

        this.$('#secret_answer').blur();

        LF.spinner.show()
        .then(() => COOL.getClass('Utilities').isOnline())
        // eslint-disable-next-line consistent-return
        .then((isOnline) => {
            let newRole = LF.StudyDesign.roles.findWhere({
                id: CurrentContext().role
            });

            if (!isOnline && !newRole.canAddOffline()) {
                LF.spinner.hide();

                MessageRepo.display(MessageRepo.Banner.CONNECTION_REQUIRED, { delay: 500 })
                .done();
            } else {
                Data.answer = hex_sha512(this.$('#secret_answer').val());
                Data.question = this.$('#secret_question').val();

                if (this.validate() && !this.submitted) {
                    this.submitted = true;

                    updateUser = () => {
                        return user.changeCredentials(_.extend({}, Data.changeCredentialParams, {
                            secretQuestion: Data.question,
                            secretAnswer: Data.answer
                        }));
                    };

                    createTransmission = () => {
                        return this.createTransmission(JSON.stringify({
                            krpt: this.subject.get('krpt'),
                            secret_question: Data.question,
                            secret_answer: Data.answer
                        }));
                    };

                    setCookie = () => {
                        Data.changeCredentialParams = undefined;

                        this.onSecretQuestionSaved();
                    };

                    if (user.get('role') === 'subject') {
                        return createTransmission()
                        .then(() => {
                            return this.subject.save({
                                secret_question: Data.question,
                                secret_answer: Data.answer
                            });
                        })
                        .then(updateUser)
                        .then(setCookie);
                    } else {
                        return updateUser()
                        .then(setCookie);
                    }
                }
            }
        });
    }

    /**
     * Called upon submission after the secret question and answer is saved to local database.
     * @example this.onSecretQuestionSaved();
     */
    onSecretQuestionSaved () {}

    /**
     * Clears the secret answer field and remove icons.
     */
    clearSecretAnswer () {
        let $secretAnswer = this.$('#secret_answer');

        $secretAnswer.val('');
        this.clearInputState($secretAnswer);

        this.disableButton('#submit');
    }

    /**
     * Validates the secret answer field.
     * @return {boolean} Returns true if secret answer is correct, otherwise false.
     */
    validate  () {
        let $target = this.$('#secret_answer'),
            codeFormat = new RegExp(LF.StudyDesign.answerFormat || '.{2,}');

        if (codeFormat.test($target.val())) {
            this.enableButton('#submit');
            this.inputSuccess($target);
            return true;
        } else {
            this.disableButton('#submit');
            this.inputError($target);
            return false;
        }
    }

    /**
     * Add the security question to the select.
     * @param {String} key The key of the question to add.
     * @param {String} value The value of the question to add.
     */
    addQuestion (key, value) {
        let $secretQuestion = this.$('#secret_question'),
            $option = $(`<option value="${key}">${value}</option>`);

        $secretQuestion.append($option);

        // Null out $option to prevent any memory leaks.
        $option = null;
    }
}
