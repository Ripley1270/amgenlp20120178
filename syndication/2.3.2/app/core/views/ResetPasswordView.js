import PasswordBaseView from './PasswordBaseView';
import Logger from 'core/Logger';
import Data from 'core/Data';
import Spinner from 'core/Spinner';
import templates from 'core/resources/Templates';
import { MessageRepo } from 'core/Notify';

let logger = new Logger('ResetPasswordView');

export default class ResetPasswordView extends PasswordBaseView {
    constructor () {
        super();

        /**
         * @property {Object} templateStrings - Strings to fetch in the render function.
         */
        this.templateStrings = {
            header          : 'APPLICATION_HEADER',
            title           : 'RESET_PASSWORD',
            newPassword     : 'ENTER_NEW_PASSWORD',
            confirmPassword : 'CONFIRM_PASSWORD',
            submit          : 'OK',
            submitIcon      : {
                key       : 'OK',
                namespace : 'ICONS'
            },
            pwdRules        : 'PASSWORD_RULES'
        };

        // Initialize...
        Data.unlockCode = undefined;
    }

    /**
     * @property {string} id - The unique id of the root element.
     * @readonly
     * @default 'reset-password-page'
     */
    get id () {
        return 'reset-password-page';
    }

    /**
     * @property {string} template - Id of template to render.
     * @readonly
     * @default '#reset-password-template'
     */
    get template () {
        return '#reset-password-template';
    }

    /**
     * Renders the view.
     * @returns {Q.Promise<void>}
     * @example this.render();
     */
    render () {
        let request = Q();

        if (LF.StudyDesign.showPasswordRules) {
            request = request.then(() => this.i18n('PASSWORD_RULES'))
                .then(text =>  templates.display('DEFAULT:PasswordRules', { text }));
        }

        return request.then((passwordRules = '') => {
            return this.buildHTML({
                key: this.key,
                passwordRules
            }, true);
        });
    }

    /**
     * Submit the reset password request.
     * @param {(MouseEvent|TouchEvent)} e - The Mouse/Touch event.
     */
    submit (e) {
        this.disableButton('#submit');

        super.submit(e)
        .finally(() => {
            this.enableButton('#submit');
        })
        .done();
    }

    /**
     * Display success message once the password is saved locally and redirect to dashboard.
     * @returns {Q.Promise<void>}
     * @example this.onPasswordSaved();
     */
    onPasswordSaved () {
        return Q.Promise(resolve => {
            let user = this.user;

            logger.operational(`Password reset by ${user.get('role')} ${user.get('username')}`, {
                username: user.get('username'),
                role: user.get('role')
            });

            LF.security.resetFailureCount(user);

            localStorage.removeItem('Reset_Password');

            if (localStorage.getItem('Reset_Password_Secret_Question')) {
                this.navigate('reset_secret_question');
                resolve();
            } else {
                //Close the virtual keyboard before showing the popup message
                $('input').blur();

                Q.delay(200).then(() => {
                    Spinner.hide();

                    return this.notify({
                        dialog : MessageRepo.Dialog && MessageRepo.Dialog.PASSWORD_CHANGE_SUCCESS
                    }).then(() => {
                        localStorage.setItem('Subject_Login', true);
                        this.navigate('dashboard');
                    });
                }).done(resolve);
            }
        });
    }
}
