import Data from 'core/Data';
import Sound from 'core/classes/Sound';
import Spinner from 'core/Spinner';
import { MessageRepo } from 'core/Notify';
import { confirm } from 'core/actions/confirm';
import notify from 'core/actions/notify';
import Transmissions from 'core/collections/Transmissions';
import Logger from 'core/Logger';
import { isLogPad } from 'core/utilities';

// Imported so LF.displayStrings is defined.
import 'core/collections/Languages';

let { history } = Backbone,
    { defer } = Q,
    templateCache = {};

const logger = new Logger('PageView');

export default class PageView extends Backbone.View {
    //noinspection JSDuplicatedDeclaration
    /**
     * @param {Backbone.ViewOptions=} options Backbone view options
     */
    constructor (options) {
        super(options);

        /**
         * The default pageType of a page view.
         * @readonly
         * @type String
         * @default 'page'
         */
        this.pageType = 'page';

        /**
         * A list of events applied to the view's DOM.
         * @type Object
         */
        this.events = this.events || {};

        /**
         * Attributes for the page view
         * @enum {String}
         * @default <ul>
         *     <li>data-theme: a</li>
         *     <li>lang: en-US</li>
         *     <li>dir: ltr</li>
         *     <li>screen: auto-gen</li>
         * </ul>
         */
        this.attributes = {
            /** screen dimension */
            screen: `${screen.width}x${screen.height}`
        };

        /**
         * Template content to render
         * @type String
         * @readonly
         * @default ''
         */
        this.templateContent = '';

        /**
         * Responsible for finding and playing sounds.
         * @readonly
         * @type Object
         * @default new {@link LF.Class.Sound}
         */
        this.sound = new Sound();

        this.spinner = Spinner;

        this.data = Data;

        /**
         * Scoped notify action
         * @type notify
         */
        this.notify = notify;

        /**
         * Scoped confirm action
         * @type confirm
         */
        this.confirm = confirm;

        // This will log the name of the actual view that is being loaded, not this base class
        // However, the logger name will be this base class.
        logger.traceSection(this.constructor.name);

        /**
         * @property {string} key A randomly generated alphanumeric key generated as each page is constructed.
         */
        this.key = (() => {
            let chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghiklmnopqrstuvwxyz',
                length = chars.length,
                output = '';

            for (let i = 0; i < 10; i += 1) {
                let value = Math.floor(Math.random() * length);
                output += chars.substring(value, value + 1);
            }

            return output;
        })();
    }

    toString () {
        return `${this.constructor.name}:${this.id}`;
    }

    //noinspection JSDuplicatedDeclaration,JSCheckFunctionSignatures
    /**
     * Render the page. Unlike the Backbone.View method it overrides, this
     * method is expected to return a Promise which resolves when the page
     * is rendered.
     *
     * @abstract
     * @returns {Q.Promise<*>}
     */
    render () {
        throw new Error('must be implemented by subclass!');
    }

    /**
     * Cache the template content in templateContent
     * using the View name as the key in the caching object.
     * @example this.cacheTemplate();
     * @param {string} source The ID associated with the template.
     * @param {string} view The name of the View associated with the template.
     */
    cacheTemplate (source, view) {
        !!LF.cachedTemplates || (LF.cachedTemplates = {});
        !!LF.cachedTemplates[view] || (LF.cachedTemplates[view] = $(source).html());
        this.templateContent = LF.cachedTemplates[view];
    }

    /**
     * Remove the view and prevent zombies(living dead event listeners).
     * @example this.close();
     */
    close () {
        $('#noty_top_layout_container').css('display', 'none');
        this.undelegateEvents();
        this.remove();
    }

    //noinspection JSUnusedLocalSymbols
    /**
     * Disables the entire view and displays a message.
     * @returns {Q.Promise<void>}
     */
    displayMessage () {
        this.spinner.show();
        return Q();
    }

    //noinspection JSUnusedLocalSymbols
    /**
     *
     * Removes the loading message and enables the view.
     * @returns {Q.Promise<void>}
     */
    removeMessage () {
        this.spinner.hide();
        return Q();
    }

    /**
     * Fetches any data required for the view to render.
     * @param {String[]} collections An array of strings listing the collections to fetch.
     * @param {Function} onSuccess A callback function invoked upon success.
     * @returns {Q.Promise<void>}
     * @example
     * this.fetchData([
     *     'Subjects'
     * ], function () {
     *     // Data fetched...
     * });
     */
    fetchData (collections, onSuccess) {
        let deferred = defer(),
            length = collections.length,
            step = (counter) => {
                let item = collections[counter],
                    tempModel;

                // Define a new model based on the model type and data.
                tempModel = new LF.Collection[item]();

                //noinspection JSUnusedLocalSymbols
                tempModel.fetch().then(() => {
                    Data[item] = tempModel;

                    if ((counter + 1) === length) {
                        (onSuccess || $.noop)();
                        deferred.resolve();
                    } else {
                        step(counter + 1);
                    }
                });
            };

        // Begin stepping through the data sets.
        step(0);

        return deferred.promise;
    }

    /**
     * Retry the transmission action based on the user input.
     * @param {Object} params An object that contains the message and connectivity status.
     * @param {Function} callback A callback function to continue remaining actions.
     * @returns {Q.Promise<void>}
     */
    transmitRetry (params, callback = $.noop) {
        const { Dialog } = MessageRepo;
        let resume = (result) => {
            if (!result) {
                callback(false);
                return Q(false);
            }

            // Let the user know that we are retrying by making sure that
            // the please wait message displays for at least 1 sec
            return this.spinner.show()
            .delay(1000)
            .then(() => {
                return this.triggerTransmitRule();
            })
            .then(() => this.spinner.hide())
            .then(() => callback());
        };

        LF.security.restartSessionTimeOut();

        if (params.isOnline) {
            // if the device has connectivity, but study was not reachable
            //noinspection JSValidateTypes
            return this.notify({
                dialog: Dialog && Dialog[params.message],
                options: { httpRespCode: params.httpRespCode || '' }
            })
            .then(callback);
        } else {
            const dialog = (() => {
                if (Dialog && Dialog[`CONFIRM_${params.message}`]) {
                    return Dialog[`CONFIRM_${params.message}`];
                } else {
                    return Dialog && Dialog[params.message];
                }
            })();

            // Device has no internet connection
            //noinspection JSValidateTypes
            return this.confirm({
                dialog,
                options: { httpRespCode: params.httpRespCode || '' }
            })
            .then((res) => resume(res));
        }
    }

    /**
     * Switch the theme for site use.
     */
    setTheme () { }

    /**
     * Execute all transmissions and DCF requests.
     */
    transmit () {}

    /**
     * Calls the transmit rule.
     */
    triggerTransmitRule () {}

    /**
     * Display a view.
     * @example
     * this.page();
     */
    page () {
        let $page = $('#application');

        $page.html(this.el);
        $page = null;
    }

    /**
     * Set the title of the window.
     * @param {string} title The title to set.
     */
    title (title) {
        document.title = title;
    }

    /**
     * Find and return an underscore template.
     * @param {string=} selector The selector for the template to fetch and wrap.
     * @returns {_.TemplateExecutor}
     */
    getTemplate (selector) {
        selector = (selector || this.template);

        // If there is no selector, throw an error.
        if (!selector) {
            throw new Error('No template selector provided.');
        }

        // If the template is cached, use it.
        if (templateCache[selector]) {
            return templateCache[selector];

        // Otherwise, find and create the template.
        } else {
            let template = _.template($(selector).html());
            templateCache[selector] = template;
            return template;
        }
    }

    /**
     * Clears the private template cache. Use more for unit testing.
     */
    clearTemplateCache () {
        templateCache = {};
    }

    /**
     * Build the view's HTML.
     * @param {object} dynamicStrings Any strings that are not to be translated, but are pulled from other sources. ex. A site's ID.
     * @param {boolean} renderToView If set to true, calls page() to render the view to the DOM.
     * @returns {Q.Promise} The translated strings.
     */
    buildHTML (dynamicStrings = {}, renderToView = false) {
        let template = this.getTemplate();

        this.templateStrings = this.templateStrings || {};

        // Before constructing the view, remove any existing event listeners.
        this.undelegateEvents();

        // DE15951 - Extend default events onto the current event list.
        this.events = _.extend(this.events, {
            'keypress input': 'hideKeyboardOnEnter',
            'submit': 'skipSubmit'
        });

        return this.i18n(this.templateStrings).then(translated => {
            let strings = _.extend(translated, dynamicStrings),
                compiled;

            // Try and compile the template with the translated and dynamic strings.
            try {
                compiled = template(strings);
            } catch (e) {
                logger.error(e);
                return Q.reject(e.message);
            }

            // Pass the combined strings into the template, and set it as the view's HTML.
            this.$el.html(compiled);

            // Build any predefined selectors.
            this.buildSelectors();

            // If renderToView is set to true, render the view to the page.
            if (renderToView) {
                this.page();
            }

            // Ensure that all defined event listeners are once again enabled.
            this.delegateEvents();

            return strings;
        });
    }

    /**
     * Generate shortcut selectors from this.selectors. Called with buildHTML().
     * @example
     * this.selectors = { submit : '#submit' };
     * this.buildSelectors();
     * this.$submit.attr('disabled', 'disabled');
     */
    buildSelectors () {
        let selectors = this.selectors || {};
        _(selectors).forEach((value, name) => {
            this[`$${name}`] = this.$(value);
        });
    }

    /**
     * Clear any generated shortcut selectors. Called on view removal.
     * @example
     * this.clearSelectors();
     */
    clearSelectors () {
        let selectors = this.selectors || {};
        _(selectors).forEach((value, name) => {
            this[`$${name}`] = null;
        });
    }

    /**
     * An alias for Backbone.history.navigate (or LF.router.navigate).
     * @param {string} fragment See Backbone docs.
     * @param {object|boolean} options See Backbone docs.
     * @param {object} flashParams The parameters to be passed to the navigated controller function
     * @example this.navigate(login, true);
     */
    navigate (fragment, options = true, flashParams) {
        LF.router.flash(flashParams).navigate(fragment, options);
    }

    /**
     * An alias for Backbone.history.loadUrl(). Essentially reloads the route without navigating away, or refreshing the page.
     * @example this.reload();
     */
    reload () {
        history.loadUrl(undefined);
    }

    /**
     * Default resolve method for in cases where a view doesn't have one defined.
     * @returns {Q.Promise<void>}
     */
    resolve () {
        return Q();
    }

    /**
     * An alias for LF.strings.display. See {@link core/collections/Languages Languages} for details.
     * @param {object|string} strings The translations keys to translate.
     * @param {function} callback A callback function invoked when the strings have been translated.
     * @param {object} options Options passed into the translation.
     * @returns {Q.Promise}
     */
    i18n (strings, callback=$.noop, options=undefined) {
        return LF.strings.display.call(LF.strings, strings, callback, options);
    }

    /**
     * Returns a jQuery object based on the provided selector.
     * @param {string|object} selector The selector of the element to get.
     * @returns {Object}
     */
    getElement (selector) {
        return (typeof selector === 'string') ? this.$(selector) : selector;
    }

    /**
     * Enable an element (scoped to the view).
     * @param {string|object} selector Either a string selector, or a jQuery object, of the element to enable.
     * @returns {object} The enabled element.
     */
    enableElement (selector) {
        let $ele = this.getElement(selector);

        return $ele.removeAttr('disabled');
    }

    /**
     * Disable an element (scoped to the view).
     * @param {string|object} selector Either a string selector, or a jQuery object of the element to disable.
     * @returns {object} The disabled element.
     */
    disableElement (selector) {
        let $ele = this.getElement(selector);

        return $ele.attr('disabled', 'disabled');
    }

    /**
     * Determines if an element is disabled.
     * @param {string|object} selector Either a string selector, or a jQuery object of the element to check.
     * @returns {Boolean}
     */
    isDisabled (selector) {
        let $ele = this.getElement(selector);

        return !!$ele.attr('disabled');
    }

    /**
     * A shortcut for enableElement.
     * @param {string|object} button Either a string selector, or a jQuery object of the button to enable.
     * @returns {object} The enabled button.
     */
    enableButton (button) {
        return this.enableElement(button);
    }

    /**
     * A shortcut for disableButton.
     * @param {string|object} button Either a string selector, or a jQuery object of the button to disable.
     * @returns {object} The disabled button.
     */
    disableButton (button) {
        return this.disableElement(button);
    }

    /**
     * Show an element in the view.
     * @param {string|object} selector Either a string selector, or a jQuery object of the element to show.
     * @returns {JQuery} The shown element.
     */
    showElement (selector) {
        let $ele = this.getElement(selector);

        return $ele.removeClass('hidden');
    }

    /**
     * Hide an element in the view.
     * @param {string|object} selector Either a string selector, or a jQuery object of the element to hide.
     * @returns {object} The hidden element.
     */
    hideElement (selector) {
        let $ele = this.getElement(selector);

        return $ele.addClass('hidden');
    }

    /**
     * Clear an input of any validation state.
     * @param {string|object} input Either a string selector, or a jQuery object of the input to clear state.
     * @returns {object} The cleared input element.
     */
    clearInputState (input) {
        let $input = (typeof input === 'string') ? this.$(input) : input,
            $parent = this.getValidParent($input),
            $icon = $input.siblings('.form-control-feedback');

        // If the input has feedback...
        if ($parent.hasClass('has-feedback')) {
            // Remove the icon.
            $icon.remove();

            // Remove any state-related classes from the input.
            $parent.removeClass('has-feedback has-success has-warning has-error');
        }

        $parent = null;
        $icon = null;

        return $input;
    }

    /**
     * Validate an input element within the view.
     * @param {string|object} input Either a string selector, or a jQuery object of the input to validate.
     * @param {RegExp} expression A regular expression used to validate the input element's value.
     * @param {string} errorKey A translation key to display in an invalid state.
     * @returns {boolean} True if valid, false if not.
     */
    validateInput (input, expression, errorKey) {
        let $input = (typeof input === 'string') ? this.$(input) : input,
            value = $input.val();

        $input = null;

        // Test the value from the input against the provided expression.
        if (expression.test(value)) {
            // The input is valid...
            this.inputSuccess(input);

            if (errorKey != null) {
                // Remove any existing help text.
                this.removeHelpText(input);
            }

            return true;
        } else {
            // The field is not valid...
            this.inputError(input);

            if (errorKey != null) {
                // If there is an error key provided, display help text below the input element.
                this.addHelpText(input, errorKey);
            }

            return false;
        }
    }

    /**
     * Add help text to an input element.
     * @param {string|object} input Either a string selector, or a jQuery object of the input to add the help text to.
     * @param {string} key A translation key for the help text to add.
     * @returns {object} The input the help text was added to.
     */
    addHelpText (input, key) {
        let $input = (typeof input === 'string') ? this.$(input) : input,
            $parent = this.getValidParent($input),
            exists = $parent.has('.help-block').length;

        if (!exists) {
            this.i18n(key).then((text) => {
                $parent.append(`<p class="help-block">${text}</p>`);
                $parent = null;
            }).done();
        } else {
            $parent = null;
        }

        return $input;
    }

    /**
     * Remove help text from an input element.
     * @param {string|object} input Either a string selector, or a jQuery object of the input to remove the help text from.
     * @returns {object} The input the help text was removed from.
     */
    removeHelpText (input) {
        let $input = (typeof input === 'string') ? this.$(input) : input,
            $parent = this.getValidParent($input);

        // Remove any prior text.
        $parent.children('.help-block').remove();
        $parent = null;

        return $input;
    }

    /**
     * Select correct parent for target element
     * @param  {string|object} input Either a string selector, or a jQuery object of the input whose parent is needed.
     * @return {object} The parent object of the input element
     */
    getValidParent (input) {
        let $input = (typeof input === 'string') ? this.$(input) : input,
            $parent = $input.parent('.input-group');

        return $parent.length ? $parent : $input.closest('.form-group');
    }

    /**
     * Set an input element to a valid state.
     * @param {string|object} input Either a string selector, or a jQuery object of the input to set to a valid state.
     * @returns {object} The provided input element.
     */
    inputSuccess (input) {
        let $input = (typeof input === 'string') ? this.$(input) : input,
            $parent = this.getValidParent($input),
            icon = '<span class="glyphicon glyphicon-ok-sign form-control-feedback" aria-hidden="true"></span>';

        this.clearInputState(input);
        $parent.addClass('has-feedback has-success').append(icon);
        $parent = null;

        return $input;
    }

    /**
     * Set an input element to an invalid state.
     * @param {string|object} input Either a string selector, or a jQuery object of the input to set to an invalid state.
     * @returns {object} The provided input element.
     */
    inputError (input) {
        let $input = (typeof input === 'string') ? this.$(input) : input,
            $parent = this.getValidParent($input),
            icon = '<span class="glyphicon glyphicon-minus-sign form-control-feedback" aria-hidden="true"></span>';

        this.clearInputState(input);
        $parent.addClass('has-feedback has-error').append(icon);
        $parent = null;

        return $input;
    }

    /**
     * Displays an error banner with the error icon next to input text box.
     * @param {String} elementName The name of the element to show the error icon, preceded with a #
     * @param {String} errorKey The resource key for the error string that will be displayed on the banner
     * @return {Q.Promise<null>}
     */
    showInputError (elementName, errorKey) {
        this.inputError(elementName);
        logger.error(errorKey);

        return Q().then(() => {
            const banner = (() => {

                if (MessageRepo.Banner[errorKey]) {
                    return MessageRepo.Banner[errorKey];
                } else {
                    return MessageRepo.Banner[`INPUT_ERROR_${errorKey}`];
                }
            })();

            return MessageRepo.display(banner);
        })
        .catch((err) => {
            logger.error('Error displaying message', err);
            return Q.reject(err);
        });
    }

    //noinspection JSDuplicatedDeclaration,JSCheckFunctionSignatures
    /**
     * @returns {Backbone.View<TModel>}
     * Remove the view from the DOM.
     */
    remove () {
        this.clearSelectors();
        this.undelegateEvents();
        return super.remove();
    }

    /**
     * We don't want the soft-keyboard 'go' or 'enter' to do an html submit of the page!
     * @param {(MouseEvent|TouchEvent)} e - A mouse or touch event.
     * @returns {boolean}
     */
    skipSubmit (e) {
        e.preventDefault();
        return false;
    }

    /**
     * Hides keyboard from the active element, when enter key is pressed.
     * @param {Event} evt Event data.
     */
    hideKeyboardOnEnter (evt) {
        if (evt.keyCode === 13 && document.activeElement) {
            if (document.activeElement.tagName === 'INPUT') {
                document.activeElement.setSelectionRange(0, 0);
            }

            document.activeElement.blur();
        }
    }

    /**
     * Gets the number of pending reports that should be displayed
     * @param  {Object} [filter] - transmission items to be included or excluded from display
     * @example getPendingReportCount({
     *      exclude: ['resetPassword'],
     *      include ['transmitEditPatient', 'updateUserCredentials']
     *  })
     * @return {Q.Promise<number>}
     */
    getPendingReportCount (filter = {}) {
        let transmissionTypes = {
                core: [],
                logpad: [
                    'resetPassword',
                    'resetSecretQuestion',
                    'transmitQuestionnaire',
                    'updateUserCredentials',
                    'transmitUserData'
                ],
                sitepad: [
                    'transmitUserData',
                    'transmitSubjectAssignment',
                    'transmitEditPatient',
                    'transmitQuestionnaire',
                    'createFirstUser',
                    'sitepadPasswordSecretQuestion',
                    'transmitResetCredentials',
                    'transmitResetPassword',
                    'updateUserCredentials',
                    'transmitUserEdit'
                ]
            },

            filterTransmission = models => {
                return _.reject(models, model => {
                    return model.get('hideCount') === true;
                });
            },

            collection = new Transmissions(),

            getCount = (models) => {
                return _.countBy(models, model => model.get('method'));
            },

            addUpCount = (param) => {
                let total = 0,
                    typesToDisplay = transmissionTypes[isLogPad() ? 'logpad' : 'sitepad'],
                    filterTypesToDisplay = (types) => {
                        let transmissionKeys = Object.keys(LF.Transmit),
                            filterOut = filter.exclude,
                            filterIn = filter.include,
                            // performs the filtering of transmission
                            execute = (filter, op) => {
                                let testValidKeys = filter => {
                                        let validKeys = _.intersection(transmissionKeys, filter),
                                            invalidKeys = validKeys.length === filter.length ? [] : _.difference(filter, validKeys);
                                        return { validKeys, invalidKeys };
                                    },
                                    keys = testValidKeys(filter);

                                // add or remove valid transmission types from the types to display
                                if (keys.validKeys && keys.validKeys.length) {
                                    types = (op === 'out' ? _.difference : _.union)(types, keys.validKeys);
                                }
                                // log error if invalid transmission type was passed into filter object
                                if (keys.invalidKeys && keys.invalidKeys.length) {
                                    logger.error('Invalid transmission item(s)', keys.invalidKeys);
                                }
                            };


                        if (filterOut && !_.isEmpty(filterOut)) {
                            execute(filterOut, 'out');
                        }

                        if (filterIn && !_.isEmpty(filterIn)) {
                            execute(filterIn, 'in');
                        }

                        return types;
                    };

                // if filters were passed in, run this to perform filtering
                if (!_.isEmpty(filter)) {
                    typesToDisplay = filterTypesToDisplay(typesToDisplay);
                }

                _.each(typesToDisplay, type => {
                    total = param[type] ? total + param[type] : total;
                });

                return total;
            };

        return collection.fetch()
            .then(() => filterTransmission(collection.models))
            .then(models => getCount(models))
            .then(param => addUpCount(param))
            .catch(err => logger.error('Error getting pending transmission count', err));
    }

}

/*
 * Due to ES6's spec, you cannot set properties outside of the constructor!
 * This creates a condition, where class properties will be overwritten by the super class.
 * Since ES6 classes are syntactic sugar onto of prototypal inheritance, we still have direct access to the prototype chain...
 * This isn't the cleanest solution, but it provides what we need.
 */
PageView.prototype.id = 'page';
PageView.prototype.tagName = 'div';

window.LF.View.PageView = PageView;
