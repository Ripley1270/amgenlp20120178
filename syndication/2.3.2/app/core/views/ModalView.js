import { getGlyphicon } from 'core/utilities';

export default class ModalView extends Backbone.View {

    /**
     * @property {string} id The ID of the modal.
     * @readonly
     * @default 'modal'
     */
    get id () {
        return 'modal';
    }

    /**
     * @property {string} className The class of the modal.
     * @readonly
     * @default 'modal'
     */
    get className () {
        return 'modal';
    }

    /**
     * @property {Object} attributes Additional attributes appended to the top-level DOM node.
     * @readonly
     */
    get attributes () {
        return {
            'role': 'dialog',
            'aria-hidden': 'true',
            'data-backdrop': 'static'
        };
    }

    /**
     * Find and return an underscore template.
     * @returns {String}
     */
    getTemplate () {
        return _.template($(this.template).html());
    }

   /**
     * Render the modal to the DOM.
     * @param {Object} strings Strings to pass into the modal's template.
     * @returns {ModalView}
     */
    render (strings) {
        let template = this.getTemplate();

        this.$el.html(template(strings));

        this.styleClass = strings.styleClass || '';

        this.modalType = strings.type || '';

        (this.modalType || this.styleClass) && this.applyStyles();

        this.delegateEvents();
        // strings.styleClass ? this.$el.addClass(strings.styleClass) : $.noop;

        return this;
    }

    /**
     * Show the modal, if not already shown.
     * @param {Object} strings Strings to pass into the modal's template.
     */
    show (strings) {
        if (!this.isShown()) {
            this.render(strings);
            this.$el.modal('show');
        }
    }

    /**
     * Determines if the modal is displayed.
     * @returns {boolean} True if displayed, false if not.
     */
    isShown () {
        return $('#modal').hasClass('in');
    }

    /**
     * Hides the modal.
     * @returns {Q.Promise<void>}
     */
    hide () {
        let request = Q.Promise(resolve => {
            let timer;
            let complete = () => {
                // If the promise hasn't already been resolved, do so.
                if (request && !request.isFulfilled()) {
                    // Remove the time and event listener from the modal.
                    window.clearTimeout(timer);
                    this.$el.off('hidden.bs.modal', complete);

                    resolve();
                }
            };

            // We need to set a timeout in case the modal is removed from the DOM
            // externally.  This will prevent the 'hidden.bs.modal' event from triggering.
            timer = window.setTimeout(complete, 500);

            // Add an event listener for the modal to close, then call hide.
            this.$el.on('hidden.bs.modal', complete).modal('hide');
        });

        return request;
    }

    /**
     * Clean up the modal's events.
     */
    teardown () {
        this.$el.data('modal', null);
        this.remove();
    }

    /**
     * An alias for LF.strings.display. See {@link core/collections/Languages Languages} for details.
     * @param {Object|string} strings The translations keys to translate.
     * @param {Function} callback A callback function invoked when the strings have been translated.
     * @param {Object} options Options passed into the translation.
     * @returns {Q.Promise<Object>}
     */
    i18n (strings, callback, options) {
        return LF.strings.display.call(LF.strings, strings, callback, options);
    }

    /**
     * Prepends an icon onto the modal.
     * @param {string} span The icon to prepend.
     */
    addGlyphicon (span) {
        let elem = this.findModalElement();
        span && elem && elem.prepend(span);
    }

    /**
     * Find and return a modal element.
     * @param {string} [popover=modal] The element to find.
     * @param {string} [popoverClass] A class used to narrow down the search of the element.
     * @returns {HTMLElement|boolean}
     */
    findModalElement (popover = 'modal', popoverClass) {
        let elemClass = (popover === 'modal') ? popoverClass || 'modal-title'
                : popoverClass || '';

        return elemClass ? this.$el.find(`.${elemClass}`) : false;
    }

    /**
     * Add a class to the modal.
     * @param {string} [headerClass] An additional class to add to the header element.
     */
    addModalClass (headerClass) {
        let styleClass = this.styleClass;

        if (headerClass) {
            // concat headerClass
            styleClass = styleClass ? `${styleClass} ${headerClass}` : `${headerClass}`;
        }

        styleClass ? this.$el.addClass(styleClass) : $.noop;
    }

    /**
     * Apply styles upon the modal based on modal type.
     */
    applyStyles () {
        let [span, headerClass] = getGlyphicon(this.modalType);

        // add glyphicon only if modal has 'type' param
        span && this.addGlyphicon(span);

        this.addModalClass(headerClass);
    }
}
