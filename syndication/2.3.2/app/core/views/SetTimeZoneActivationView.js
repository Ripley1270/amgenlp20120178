import SetTimeZoneView from 'core/views/SetTimeZoneView';
import Logger from 'core/Logger';
import { MessageRepo } from 'core/Notify';
import TimeZoneUtil from 'core/classes/TimeZoneUtil';
import * as lStorage from 'core/lStorage';
import COOL from 'core/COOL';

let logger = new Logger('setTimeZoneActivation');

/*
 * @class SetTimeZoneActivationView
 * @description View for setting the time zone during activation
 * @extends SetTimeZoneView
 */
export default class SetTimeZoneActivationView extends SetTimeZoneView {
    constructor (options) {
        super(options);

        /**
         * @property {boolean} [selectActive=false] - If set to true, selects the device's active timezone by default.
         */
        this.selectActive = false;

        /**
         * @property {Object<string,string>} events - The view's events.
         * @readonly
         */
        this.events = {
            'click #back': 'back',
            'click #next': 'nextHandler',
            'change #selectTimeZone': 'chooseZone'
        };

        /**
         * @property {Object<string,string>} templateStrings - Strings to fetch in the render function
         */
        this.templateStrings = {
            header: 'APPLICATION_HEADER',
            timezone_msg: 'SET_TIMEZONE_MESSAGE',
            back: 'BACK',
            next: 'NEXT'
        };
    }

    /**
     * @property {string} id - The unique id of the root element.
     * @readonly
     * @default 'set-time-zone-activation-page'
     */
    get id () {
        return 'set-time-zone-activation-page';
    }

    /**
     * @property {string} template - Id of template to render
     * @readonly
     * @default '#set-time-zone-activation-template'
     */
    get template () {
        return '#set-time-zone-activation-template';
    }

    /**
     * @property {Object<string,string>} selectors - A list of selectors to populate upon render.
     */
    get selectors () {
        return {
            next: '#next',
            selectTimeZone: '#selectTimeZone'
        };
    }

    /**
     * Navigates back to the code entry view.
     * @event
     * @param {Object} e Event object.
     */
    back (e) {
        e.preventDefault();
        this.sound.play('click-audio');

        lStorage.removeItem('serviceBase');
        this.navigate('code_entry');
    }

    /**
     * Enable the next button based on selection.
     */
    chooseZone () {
        this.enableButton(this.$next);
    }

    /**
     * Sets the Timezone selected.
     * @returns {Q.Promise<void>}
     */
    setTimeZone () {
        // Returning a Promise is entirely for testability.
        // This method is invoked via a click event, and doesn't live within a promise chain.
        return Q.Promise((resolve, reject) => {
            let selected = this.$selectTimeZone.select2('data')[0];

            // Navigates to the handoff view, and resolves the returned promise.
            let navigateToHandoff = () => {
                this.navigate('handoff');
                resolve();
            };

            // Handles case where the end user cancels the timezone change, or lack thereof.
            let cancel = () => {
                logger.operational(`User canceled setting the Time zone to: ${selected.displayName}`);
                resolve();
            };

            LF.DynamicText.selectedTZName = selected.displayName;

            // If the device's current timezone is different than the one selected...
            if (this.currentTimeZone.id !== selected.id) {
                this.confirm({ dialog: MessageRepo.Dialog.TIME_ZONE_SET_CONFIRM })
                // eslint-disable-next-line consistent-return
                .then((res) => {
                    if (!res) {
                        return cancel();
                    }
                    LF.Wrapper.exec({
                        execWhenWrapped: () => {
                            // If the application is run on a device, set the device's active timezone.
                            TimeZoneUtil.setTimeZone(selected).then(() => {
                                return TimeZoneUtil.restartApplication()
                                .finally(navigateToHandoff);
                            }, (err) => {
                                return this.notify({
                                    dialog: MessageRepo.Dialog && MessageRepo.Dialog.TIME_ZONE_SET_ERROR
                                }).then(() => reject(err));
                            })
                            .done();
                        },
                        execWhenNotWrapped: () => {
                            // The application is being run in a web browser, and doesn't have access to the
                            // TimeZoneUtil plugin.  Instead of changing the timezone, just navigate to the handoff view.
                            navigateToHandoff();
                        }
                    });
                })
                // confirm() returns a promise, so we should wrap it up with a .done();
                .done();
            } else {
                // The time zone hasn't been changed. Display a message informing the user of no change.
                this.confirm({ dialog: MessageRepo.Dialog.TIME_ZONE_SET_CONFIRM_NOCHANGE })
                .then((res) => {
                    if (res) {
                        logger.operational(`User used the device's Time zone: ${selected.displayName}`);
                        navigateToHandoff();
                    } else {
                        cancel();
                    }
                })
                // confirm() returns a promise, so we should wrap it up with a .done();
                .done();
            }
        });
    }
}

COOL.add('SetTimeZoneActivationView', SetTimeZoneActivationView);