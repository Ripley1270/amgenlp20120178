/* eslint-disable consistent-return */

import PageView from 'core/views/PageView';
import * as lStorage from 'core/lStorage';

import Logger from 'core/Logger';

import { removeEmoji } from 'core/utilities';

let logger = new Logger('ForgotPasswordView');

export default class ForgotPasswordView extends PageView {
    constructor (options = {}) {
        super(options);

        /**
         * @property {string} template - Id of template to render
         * @readonly
         * @default '#forgot-password-template'
         */
        this.template = '#forgot-password-template';

        /**
         * @property {Object} selectors - A list of selectors to be created.
         * @readonly
         */
        this.selectors = {
            submit: '#submit',
            back: '#back',
            secretAnswer: '#secret_answer',
            unlockCode: '#unlock-code'
        };

        /**
         * @property {Object} events - The view's events.
         * @readonly
         */
        this.events = {
            /** Click event for back button */
            'click #back'             : 'back',

            /** Tap event for submit form */
            'click #submit'           : 'validate',

            /** Tap event for Unlock Code link */
            'click #unlock-code'  : 'unlockCode',

            /** Input event for secret answer */
            'input #secret_answer'  : 'onInput'
        };

        /**
         * @property {Object} templateStrings - Strings to fetch in the render function
         */
        this.templateStrings = {
            header              : 'APPLICATION_HEADER',
            title               : 'FORGOT_PASSWORD',
            disabledLoginText   : 'PASSWORD_ATTEMPT_EXCEEDED',
            pleaseAnswerText    : 'PLEASE_ANSWER_SQ',
            unlockCode          : 'FORGOT_SECRET_ANSWER',
            secretQuestion      : '',
            submit              : 'OK',
            back                : 'BACK',
            submitIcon          : {
                key       : 'OK',
                namespace : 'ICONS'
            }
        };

        // TODO: Fix this
        this.backCallback = options.backCallback;
    }

    /**
     * @property {string} id - The unique id of the root element.
     * @readonly
     * @default 'forgot-password-page'
     */
    get id () {
        return 'forgot-password-page';
    }

    /**
     * Resolve any dependencies of the view rendering.
     * @returns {Q.Promise<void>}
     */
    resolve () {
        let userId = parseInt(lStorage.getItem('User_Login'), 10);

        return LF.security.getUser(userId, true).then((user) => {
            this.user = user;
        });
    }

    /**
     * Navigates back to the dashboard view.
     */
    back () {
        this.sound.play('click-audio');
        this.navigate('login');
    }

    /**
     * Renders the view.
     * @returns {Q.Promise<void>}
     */
    render () {
        this.templateStrings.secretQuestion = this.getSecretQuestion(parseInt(this.user.get('secretQuestion'), 10));

        return this.buildHTML({ key: this.key }, true).then(() => {
            // DE8275: Adding focus to prevent blue pointer from displaying erroneously in Android
            this.$submit.removeAttr('disabled')
                .focus()
                .attr('disabled', 'disabled');
        }).catch(e => logger.error(e));
    }

    /**
     * Validates the secret answer.
     * @param {Event} e - Event data.
     * @return {boolean} Returns false if the maximum Security Questions is exceeded. This way it stops executing the validate function
     */
    validate (e) {
        let input = this.$secretAnswer,
            value = input.val(),
            onFailedAttempt = () => {
                let questionFailures = LF.security.getSecurityQuestionFailure(this.user) + 1;

                LF.security.setSecurityQuestionFailure(questionFailures, this.user);
                logger.error(`Incorrect secret answer entered, attempt #${questionFailures}/${LF.StudyDesign.maxSecurityQuestionAttempts} for user ${this.user.get('username')}`);
            },
            secretAnswer = this.user.get('secretAnswer'),
            hash = (secretAnswer.length <= 32) ? hex_md5(value) : hex_sha512(value);

        e && e.preventDefault();

        // Closes the virtual keyboard and scrolls to top of page
        input.blur();
        window.scrollTo(0,0);

        if (!!LF.StudyDesign.maxSecurityQuestionAttempts && LF.security.getSecurityQuestionFailure(this.user) >= LF.StudyDesign.maxSecurityQuestionAttempts) {
            onFailedAttempt();

            this.unlockCode();

            return false;
        }

        if (hash === secretAnswer) {
            logger.operational('Correct secret answer entered');
            if (secretAnswer.length <= 32) {
                this.user.changeCredentials({secretAnswer: hex_sha512(value)});
            }

            localStorage.setItem('Reset_Password', true);

            this.navigate('reset_password');
        } else {
            this.showInputError(input, 'INCORRECT_ANSWER');

            logger.error('INCORRECT_ANSWER');

            onFailedAttempt();

            if (LF.StudyDesign.maxSecurityQuestionAttempts === LF.security.getSecurityQuestionFailure(this.user)) {
                this.unlockCode();
            }
        }
    }

    /**
     * Fetch the Security Question's key selected by user.
     * @param {string} questionValue - question value related to the question selected.
     * @returns {string} KEY to Security Question string in the Study Strings.
     * @throws {Error} If the Security Question not found.
     */
    getSecretQuestion (questionValue) {
        let question,
            collection = LF.StudyDesign.securityQuestions;

        _(collection).each(value => {
            if (value.key === questionValue) {
                question = value.text;
            }
        });

        if (!question) {
            throw new Error('Security Question not found in the Study Configuration.');
        }

        return question;
    }

    /**
     * Navigates to the UnlockCodeView
     * @returns {Q.Promise<void>}
     */
    unlockCode () {
        // DE17666 - The Banner notification is on a delay of 500 ms via Q.delay().  Added a slight delay
        // so the banner can display prior to navigating.
        this.disableButton(this.$back);
        return Q.delay(600)
        .then(() => {
            localStorage.setItem('Unlock_Code', true);
            this.navigate('unlock_code');
        });
    }

    /**
     * Enables the OK button on secret_answer input.
     */
    onInput () {
        let input = this.$secretAnswer,
            value = removeEmoji(input.val());

        if (value.length === 0) {
            // disable the OK button
            this.$submit.attr('disabled', 'disabled');
        } else {
            // enable the OK button
            this.$submit.removeAttr('disabled');
        }

        // Reset the value, in case any emoticons were stripped
        if (value !== input.val()) {
            input.val(value);
        }
    }

}

window.LF.View.ForgotPasswordView = ForgotPasswordView;
