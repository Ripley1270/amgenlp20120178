'use strict';

module.exports = function (grunt) {

    var util = require('util');

    grunt.registerMultiTask('scriptbuilder', 'Generates HTML script tags.', function () {

        var options = this.options({
            start       : '<!-- START SCRIPT -->',
            end         : '<!-- END SCRIPT -->',
            template    : '<script type="text/javascript" src="%s"></script>',
            cwd         : ''
        });

        this.files.forEach(function (file) {

            var src = '\n' + file.src.filter(function (filepath) {

                if (!grunt.file.exists(filepath)) {

                    grunt.log.warn('Source File "' + filepath + '" not found.');

                    return false;

                } else {

                    return true;

                }

            }).map(function (filepath) {

                grunt.log.writeln(filepath);
                return util.format(options.template, filepath.replace(options.cwd, ''));

            }).join('\n'), file, start, end, newFile;

            // Add a new line to the end of the src string.
            src += '\n';

            if (!grunt.file.exists(file.target)) {

                grunt.log.warn('Target file "' + file.target + '" not found.');

            } else {

                // Read in the target file.
                newFile = grunt.file.read(file.target);

                // Get the index of the start tag.
                start = newFile.indexOf(options.start);

                // Get the index of the end tag.
                end = newFile.indexOf(options.end);

                if (start === -1 || end === -1) {

                    // Either the start or end tag wasn't found.
                    grunt.log.warn('Start and/or end tag is not found.');

                } else {

                    newFile = newFile.substr(0, start + options.start.length) + src + newFile.substr(end);
                    grunt.file.write(file.dest, newFile);
                    grunt.log.writeln('File "' + file.dest + '" updated.');

                }

            }

        });

    });

};