module.exports = {

    logpadCSS : {
        options : {
            start    : '<!-- START:Stylesheets -->',
            end      : '<!-- END:Stylesheets -->',
            template : '<link rel="stylesheet" type="text/css" href="./%s" />',
            cwd      : './'
        },
        files : [{
            target : '<%= target %>/logpad/www/index.html',
            dest   : '<%= target %>/logpad/www/index.html',
            src    : ['trial/css/*.css']
        }]
    },

    sitepadCSS : {
        options : {
            start    : '<!-- START:Stylesheets -->',
            end      : '<!-- END:Stylesheets -->',
            template : '<link rel="stylesheet" type="text/css" href="./%s" />',
            cwd      : './'
        },
        files : [{
            target : '<%= target %>/sitepad/www/index.html',
            dest   : '<%= target %>/sitepad/www/index.html',
            src    : ['trial/css/*.css']
        }]
    }

};
