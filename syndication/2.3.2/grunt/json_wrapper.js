module.exports = {
    logpad: {
        options: {
            wrapper: 'LF.strings.addLanguageResources({content});',
            raw: true
        },
        files: {
            'target/logpad/www/resources/nls.js': [
                'app/core/nls/**/*.json',
                'app/logpad/nls/**/*.json',
                'trial/nls/**/*.json',
                'trial/**/nls/**/*.json'
            ]
        }
    },
    sitepad: {
        options: {
            wrapper: 'LF.strings.addLanguageResources({content});',
            raw: true
        },
        files: {
            'target/sitepad/www/resources/nls.js': [
                'app/core/nls/**/*.json',
                'app/sitepad/nls/**/*.json',
                'trial/nls/**/*.json'
            ]
        }
    }
};
