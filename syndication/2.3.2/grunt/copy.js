module.exports = {

    logpad2server : {
        files : [
            // Copy over the trial...
            {
                expand :true,
                src : ['./trial/**/*'],
                dest : 'server/public/'
            }, {
                expand : true,
                cwd : '<%= target %>/logpad/www',
                src : ['**/*'],
                dest : 'server/public/'
            }
        ]
    },

    sitepad2server : {
        files : [
            // Copy over the ./trial...
            {
                expand :true,
                src : ['./trial/**/*'],
                dest : 'server/public/'
            }, {
                expand : true,
                cwd : '<%= target %>/sitepad/www',
                src : ['**/*'],
                dest : 'server/public/'
            }
        ]
    },

    logpad : {
        files : [
          { src: './app/core/application.js', dest: '<%= target %>/logpad/www/application.js' },
          { src: './app/core/coreSettings.js', dest: '<%= target %>/logpad/www/coreSettings.js' },
          { src: './app/core/Errors.js', dest: '<%= target %>/logpad/www/Errors.js' },
          { src: './app/core/TimeTravel.js', dest: '<%= target %>/logpad/www/TimeTravel.js' },
          { src: './app/core/taco.json', dest: '<%= target %>/logpad/taco.json' },

          { src: './app/logpad/config.xml', dest: '<%= target %>/logpad/config.xml' },
          { expand: true, cwd: './app/logpad', src: 'platforms/**', dest: '<%= target %>/logpad/' },
          { expand: true, cwd: './app/logpad', src: 'plugins/**', dest: '<%= target %>/logpad/' },
          { expand: true, cwd: './app/logpad', src: 'hooks/**', dest: '<%= target %>/logpad/' },

          // Copy "core" plugins
          { expand: true, cwd: './app/core', src: 'plugins/**', dest: '<%= target %>/logpad/' },

          // Copy "trial" plugins
          { expand: true, cwd: './trial', src: 'plugins/**', dest: '<%= target %>/logpad/' },

          // Copy over the trial...
          {
              expand :true,
              src : ['./trial/**/*', '!./trial/plugins/**'],
              dest : '<%= target %>/logpad/www/'
          },

          {
              expand :true,
              cwd : './app',
              src : [
                './media/**/*',
                './lib/**/*'
              ],
              dest : '<%= target %>/logpad/www/'
          },

          {
              expand :true,
              cwd : './app/core',
              src : [
                'wrapperjs/*.js',
                'dataaccess/**/*.js',
                'log/*.js'
              ],
              dest : '<%= target %>/logpad/www/'
          },

          {
              expand :true,
              cwd    : './app/logpad',
              src    : ['bower_components/**/*'],
              dest : '<%= target %>/logpad/www/'
          }
        ]
    },

    bundle_logpad: {
        files: [{
            src: '<%= target %>/logpad/www/runtime.bundle.js',
            dest: 'server/public/runtime.bundle.js'
        }]
    },

    css_logpad: {
        files: [{
            expand: true,
            cwd: '<%= target %>/logpad/www/css/',
            src: '*.css',
            dest: 'server/public/css'
        }]
    },

    sitepad : {
        files : [
          { src: './app/core/application.js', dest: '<%= target %>/sitepad/www/application.js' },
          { src: './app/core/coreSettings.js', dest: '<%= target %>/sitepad/www/coreSettings.js' },
          { src: './app/core/Errors.js', dest: '<%= target %>/sitepad/www/Errors.js' },
          { src: './app/core/TimeTravel.js', dest: '<%= target %>/sitepad/www/TimeTravel.js' },

          { src: './app/sitepad/config.xml', dest: '<%= target %>/sitepad/config.xml' },
          { expand: true, cwd: './app/sitepad', src: 'platforms/**', dest: '<%= target %>/sitepad/' },
          { expand: true, cwd: './app/sitepad', src: 'plugins/**', dest: '<%= target %>/sitepad/' },
          { expand: true, cwd: './app/sitepad', src: 'hooks/**', dest: '<%= target %>/sitepad/' },
          { src: './app/core/taco.json', dest: '<%= target %>/sitepad/taco.json' },

          // Copy "core" plugins
          { expand: true, cwd: './app/core', src: 'plugins/**', dest: '<%= target %>/sitepad/' },

          // Copy over the trial...
          {
              expand :true,
              cwd : './',
              src : ['trial/**/*'],
              dest : '<%= target %>/sitepad/www/'
          },

          {
              expand :true,
              cwd : './app',
              src : [
                './media/**/*',
                './lib/**/*'
              ],
              dest : '<%= target %>/sitepad/www/'
          },

          {
              expand :true,
              cwd : './app/core',
              src : [
                'wrapperjs/*.js',
                'dataaccess/**/*.js',
                'log/*.js'
              ],
              dest : '<%= target %>/sitepad/www/'
          },

          {
              expand :true,
              cwd    : './app/sitepad',
              src    : ['bower_components/**/*'],
              dest : '<%= target %>/sitepad/www/'
          }
        ]
    },

    bundle_sitepad: {
          files: [{
              src: '<%= target %>/sitepad/www/runtime.bundle.js',
              dest: 'server/public/runtime.bundle.js'
          }]
      },

    css_sitepad: {
          files: [{
              expand: true,
              cwd: '<%= target %>/sitepad/www/css/',
              src: '*.css',
              dest: 'server/public/css'
          }]
      },

    'unlock-code' : {
        files : [{
            expand : true,
            cwd    : './tools/unlock-code',
            src    : [
                'manifest.json',
                'popup.html',
                'css/*.css'
            ],
            dest   : '<%= target %>/unlock-code'
        }, {
            src  : './app/lib/bootstrap/css/bootstrap.min.css',
            dest : '<%= target %>/unlock-code/css/bootstrap.min.css'
        }, {
            src  : './app/lib/jQuery/jquery-2.0.3.js',
            dest : '<%= target %>/unlock-code/src/jquery.js'
        }, {
            src  : './app/lib/md5-min.js',
            dest : '<%= target %>/unlock-code/src/md5-min.js'
        }, {
            src  : './app/media/images/logo-25x25.png',
            dest : '<%= target %>/unlock-code/images/logo-25x25.png'
        }]
    }

};
