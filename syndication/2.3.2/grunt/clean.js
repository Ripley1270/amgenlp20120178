module.exports = {

    karma_html    : ['karma_html/*/**'],

    junit         : ['junit/**'],

    coverage      : ['coverage/**'],

    target        : ['<%= target %>/'],

    logpad        : ['<%= target %>/logpad'],

    sitepad       : ['<%= target %>/sitepad'],

    server        : ['server/public/*'],

    doc           : ['doc'],

    'unlock-code' : ['<%= target %>/unlock-code']

};
