

module.exports = {

    logpad : {
        options : {
            appName : 'LogPad App',
            version : '<%= pkg.version %>',
            apiBase : '<%= build.serverURL %>/<%= build.apiBase %>'
        },
        files   : [{
            src     : ['<%= logpad %>/index.ejs'],
            dest    : '<%= target %>/logpad/www/index.html'
        }]
    },

    sitepad : {
        options : {
            appName : 'SitePad App',
            version : '<%= pkg.version %>',
            apiBase : '<%= build.serviceURL %>/<%= build.apiBase %>'
        },
        files   : [{
            src     : ['<%= sitepad %>/index.ejs'],
            dest    : '<%= target %>/sitepad/www/index.html'
        }]
    }

};
