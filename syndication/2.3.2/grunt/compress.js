module.exports = {

    sitepad : {
        options : {
            archive : '<%= target %>/sitepad/www/sitepad.zip',
            mode    : 'zip'
        },
        cwd : '<%= target %>/sitepad/www',
        expand : true,
        src : ['**'],
        dest : './'
    },

    compress_windows_sitepad : {
        options : {
            archive : 'sitepad_windows.zip',
            mode    : 'zip'
        },
        cwd : '<%= target %>/sitepad/platforms/windows/AppPackages',
        expand : true,
        src : ['*/**'],
        dest : './'
    }

};
