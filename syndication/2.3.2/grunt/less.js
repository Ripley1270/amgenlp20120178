module.exports = {

    logpad : {
        options : {
            paths : ['<%= logpad %>less/']
        },
        files : {
            '<%= target %>/logpad/www/css/base.css' : [
                'app/core/less/**/*.less',
                'app/core/screenshot/*.less',
                '<%= logpad %>/less/style.less',
                '<%= logpad %>/less/widgets/*.less',
                'trial/**/*.less'
            ]
        }
    },

    sitepad : {
        options : {
            paths : ['<%= sitepad %>less/']
        },
        files : {
            '<%= target %>/sitepad/www/css/base.css' : [
                'app/core/less/**/*.less',
                'app/core/screenshot/*.less',
                '<%= sitepad %>/less/base.less',
                '<%= sitepad %>/less/widgets/*.less',
                'trial/**/*.less'
            ]
        }
    }

};
