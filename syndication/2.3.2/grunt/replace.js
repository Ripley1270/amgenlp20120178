module.exports = {
replace_sitepad_windows : {
        options : {
            patterns : [
            {
                match: '<Capability Name="internetClient" />',
                replace: '<Capability Name="internetClient" />\n\u0020\u0020\u0020\u0020<Capability Name="picturesLibrary"/>'
            }
            ],
                usePrefix: false
        },
        files: [
        {expand: true, flatten: true, src: ['./target/sitepad/platforms/windows/package.windows10.appxmanifest'], dest: './target/sitepad/platforms/windows'}
        ]
       }
};
