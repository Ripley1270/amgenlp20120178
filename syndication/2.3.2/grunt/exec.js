var path = require('path');

module.exports = {
    taco_android_logpad: {
        command: '' +
            'taco platform add android && ' +
            'taco build android && ' +
            'taco run android --livereload',
        cwd: './target/logpad'
    },

    taco_android_sitepad: {
        command: '' +
            'taco platform add android && ' +
            'taco build android && ' +
            'taco run android --livereload',
        cwd: './target/sitepad'
    },

    start_server: {
        command: 'node ./server/bin/www &',
        stdout: false,
        stderr: true
    },

    start_server_with_logs: {
        command: 'node ./server/bin/www &',
        stdout: true,
        stderr: true
    },

    install_android_logpad: {
        command: 'adb install -r android-debug.apk',
        cwd: './target/logpad/platforms/android/build/outputs/apk'
    },

    install_android_sitepad: {
        command: 'adb install -r android-debug.apk',
        cwd: './target/sitepad/platforms/android/build/outputs/apk'
    },

    add_android_sitepad : {
        command : 'cd ./target/sitepad && cordova platform add android',
        stdout  : true,
        stderr  : true
    },

    add_ios_sitepad : {
        command : 'cd ./target/sitepad && cordova platform add ios',
        stdout : true,
        stderr : true
    },

    add_windows_sitepad : {
        command : 'cd ./target/sitepad && cordova platform add windows',
        stdout  : true,
        stderr  : true
    },

    build_android_sitepad : {
        command : 'cd ./target/sitepad && cordova build android',
        stdout  : true,
        stderr  : true
    },

    build_ios_sitepad : {
        command : 'cd ./target/sitepad && cordova build ios --device --release --force',
        stdout : true,
        stderr : true
    },

    build_android_sitepad_release : {
        command : 'cd ./target/sitepad && cordova plugin add cordova-plugin-ignore-lint-translation --save && cordova build android --release -- --keystore=../../syndication.keystore --storePassword=IlovePht --alias=PHT --password=IlovePht',
        //command : 'cd ./target/sitepad && cordova plugin add cordova-plugin-ignore-lint-translation --save && cordova build android --release',
        stdout  : true,
        stderr  : true
    },

    build_windows_sitepad : {
        command : 'cd ./target/sitepad && cordova build windows -- --win --archs="x64"',
        stdout  : true,
        stderr  : true
    },


    run_windows_sitepad : {
        command : 'cd ./target/sitepad && cordova run windows -- --win --archs="x64"',
        stdout  : true,
        stderr  : true
    },

    add_android_logpad : {
        command : 'cd ./target/logpad && cordova platform add android',
        stdout  : true,
        stderr  : true
    },

    build_android_logpad : {
        command : 'cd ./target/logpad && cordova build android',
        stdout  : true,
        stderr  : true
    },

    build_android_logpad_release : {
        command : 'cd ./target/logpad && cordova plugin add cordova-plugin-ignore-lint-translation --save && cordova build android --release -- --keystore=../../syndication.keystore --storePassword=IlovePht --alias=PHT --password=IlovePht',
        //command : 'cd ./target/logpad && cordova plugin add cordova-plugin-ignore-lint-translation --save && cordova build android --release',
        stdout  : true,
        stderr  : true
    },

    add_ios_logpad : {
        command : 'cd ./target/logpad && cordova platform add ios',
        stdout  : true,
        stderr  : true
    },

    build_ios_logpad : {
        command : 'cd ./target/logpad && cordova build ios —-device —-release --force',
        stdout  : true,
        stderr  : true
    },

    lint_sitepad : {
        command : path.relative('', './node_modules/.bin/eslint') + ' app/sitepad --max-warnings 0',
        stdout  : true,
        stderr  : true
    },

    lint_core : {
        command : path.relative('', './node_modules/.bin/eslint') + ' app/core --max-warnings 0',
        stdout  : true,
        stderr  : true
    },

    lint_logpad : {
        command : path.relative('', './node_modules/.bin/eslint') + ' app/logpad --max-warnings 0',
        stdout  : true,
        stderr  : true
    }

};
