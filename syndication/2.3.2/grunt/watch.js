module.exports = {
    options: {
        interrupt: true
    },
    mini: {
        // watching all allows you to work on core/lp/sp watching just mini
        files: ['app/**/*.js'],
        tasks: ['karma:mini']
    },
    coreSpecs: {
        files: ['app/test/specs/core/**/*.js'],
        // Core test files don't invalidate logpad and sitepad
        tasks: ['karma:core']
    },
    core: {
        files: ['app/core/**/*.js'],
        // core sources invalidate everything
        tasks: ['karma:core', 'karma:logpad', 'karma:sitepad']
    },
    logpad: {
        files: ['app/logpad/**/*.js', 'app/test/specs/logpad/**/*.js', 'trial/assets/**'],
        tasks: ['karma:logpad']
    },
    logpad_js: {
        files: ['<%= target %>/logpad/www/runtime.bundle.js'],
        tasks: ['copy:bundle_logpad']
    },
    // @TODO: device livereload
    // logpad_device_js: {
    //     files: ['server/public/platforms/android/assets/www/runtime.bundle.js'],
    //     tasks: ['exec:reload_browsersync']
    // },
    logpad_less: {
        files: ['app/**/*.less', 'trial/**/*.less'],
        tasks: ['less:logpad', 'copy:css_logpad']
    },
    // @TODO: device livereload
    // logpad_device_less: {
    //     files: ['app/**/*.less'],
    //     tasks: ['less:logpad', 'copy:css_logpad_2android']
    // },
    sitepad: {
        files: ['app/sitepad/**/*.js', 'app/test/specs/sitepad/**/*.js', 'trial/assets/**'],
        tasks: ['karma:sitepad']
    },
    sitepad_js: {
        files: ['<%= target %>/sitepad/www/runtime.bundle.js'],
        tasks: ['copy:bundle_sitepad']
    },
    // @TODO: device livereload
    // sitepad_device_js: {
    //     files: ['server/public/platforms/android/assets/www/runtime.bundle.js'],
    //     tasks: ['exec:reload_browsersync']
    // },
    sitepad_less: {
        files: ['app/**/*.less', 'trial/**/*.less'],
        tasks: ['less:sitepad', 'copy:css_sitepad']
    }
    // @TODO: device livereload
    // ,
    // sitepad_device_less: {
    //     files: ['app/**/*.less'],
    //     tasks: ['less:sitepad', 'copy:css_sitepad_2android']
    // }
};
