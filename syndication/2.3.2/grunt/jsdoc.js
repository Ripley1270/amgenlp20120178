module.exports = function (grunt) {
    return {
        dist : {
            options: {
                source: 'app',
                destination: 'doc'
            },

        }, 'widget-json': {
            options: {
                configure: './jsdoc-json.widget.conf.json'
            }
        }, widget : {
            options: {
                configure: './jsdoc.widget.conf.json'
            }
        }, core: {
            options: {
                source: 'app/core',
                destination: 'doc/core'
            }
        }
    };
};
