﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace ERTService
{
    public partial class ERTService : ServiceBase
    {
        private const string TagSource = "ERT Settings Service";
        private const string TagLogs = "ERT Settings Logs";

        private System.Diagnostics.EventLog mEventLog;
        private NancyConsole.Program mNancy = null;


        public ERTService()
        {
            InitializeComponent();
            mEventLog = new System.Diagnostics.EventLog();
            if (!System.Diagnostics.EventLog.SourceExists(TagSource))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    TagSource, TagLogs);
            }
            mEventLog.Source = TagSource;
            mEventLog.Log = TagLogs;

            // This is needed for this window service to detect session changes
            base.CanHandleSessionChangeEvent = true;
        }

        protected override void OnStart(string[] args)
        {                       
            mNancy = new NancyConsole.Program();
            mNancy.Start();
            mEventLog.WriteEntry("ERTService started.");

            bool result = NancyConsole.SettingsHelper.setLoopbackPermissions();
            mEventLog.WriteEntry("Setting SitePad permissions succeedded: " + result);
        }

        protected override void OnStop()
        {
            mEventLog.WriteEntry("ERTService stopped.");
            mNancy.Stop();
        }

        protected override void OnSessionChange(SessionChangeDescription changeDescription)
        {          
            switch (changeDescription.Reason)
            {
                case SessionChangeReason.SessionLogon:                    
                    mEventLog.WriteEntry("Login detected. Setting loopback permissions.");
                    bool result = NancyConsole.SettingsHelper.setLoopbackPermissions();
                    mEventLog.WriteEntry("Setting SitePad permissions succeedded: " + result);
                    break;

                case SessionChangeReason.SessionLogoff:                
                    //nothing for now
                    break;
            }

            // Inform the base class of the change as well
            base.OnSessionChange(changeDescription);
        }
    }
}
