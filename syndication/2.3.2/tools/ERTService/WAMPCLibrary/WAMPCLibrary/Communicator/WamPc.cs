﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using invivodata.Interop.WAMPC.Communicator;
using Mortara.ELIxCtrl.API;
using System.Reflection;
using Mortara.ELIx.ECGRecord;

namespace invivodata.Interop.WAMPC
{
    /// <summary>
    /// WamPc is a wrapper class that interfaces with EliXAPI, and provides generic hardware operations such as to get info, pair, program, collect, delete, to consuming objects.
    /// </summary>
    public class WamPc
    {
        #region private variables
        //FIXME: Update logging...
        private static readonly Log _log = new Log();
        private class Log
        {
            public Log()
            {
            }

            public void Verbose(string message)
            {
            }
        }

        private const string COLLECTION_DATE_FORMAT_STRING = "yyyy-MM-dd";
        private const string COLLECTION_TIME_FORMAT_STRING = "HH:mm:ss";
        #endregion

        #region events

        /// <summary>
        /// Event handler to udpate WAM status.
        /// </summary>
        public virtual event EventHandler<WamEventArgs> WamStatusUpdateEvent;

        /// <summary>
        /// Event handler to update task process bar.
        /// </summary>
        public event EventHandler TaskInProgressEvent;

        /// <summary>
        /// Raises status update event.
        /// </summary>
        /// <param name="e">Wam event arg</param>
        protected virtual void OnWamStatusUpdate(WamEventArgs e)
        {
            if (WamStatusUpdateEvent != null)
                WamStatusUpdateEvent(this, e);
        }

        /// <summary>
        /// Raises task progress event.
        /// </summary>
        protected virtual void OnTaskInProgress()
        {
            if (TaskInProgressEvent != null)
                TaskInProgressEvent(this, EventArgs.Empty);
        }

        public static void writeLog(string message)
        {
            if (Directory.Exists(@"c:\WAMPCLogs"))
            {
                string path = "c:\\WAMPCLogs\\WamPcLog.log";
                if (!File.Exists(path))
                {
                    using (StreamWriter text = File.CreateText(path))
                        text.WriteLine(message);
                }
                else
                    File.AppendAllText(path, Environment.NewLine + message);
            }
        }

        #endregion

        #region public methods

        /// <summary>
        /// Retrieves all pertinent information regarding the state of the current connected Wam device
        /// </summary>
        public virtual WampcCommunicationResult<WampcDeviceInformation> GetDeviceInformation()
        {
            WampcDeviceInformation result = new WampcDeviceInformation();
            try
            {
                IDeviceInformation deviceInformation = ELIxAPIFacade.Controller.GetDeviceInformation();
                result.AcquisitionId = deviceInformation.AcquisitionID;
                result.AcquisitionTimeStamp = deviceInformation.RecordingInfo == null ? new DateTime?() : deviceInformation.RecordingInfo.AcquisitionTimestamp;
                result.ClockResetTimeStamp = deviceInformation.RecordingInfo == null ? new DateTime?() : deviceInformation.RecordingInfo.ClockResetTimestamp;
                result.AcquisitionState = deviceInformation.RecordingInfo == null ? AcquisitionState.OtherError : deviceInformation.RecordingInfo.State;
                result.Model = deviceInformation.Model;
                result.Serial = deviceInformation.Serial;
                result.Version = deviceInformation.Version;
                switch (deviceInformation.BatteryLevel)
                {
                    case BatteryLevel.NA:
                        result.BattLevel = WamBatteryLevel.NA;
                        break;
                    case BatteryLevel.Low:
                        result.BattLevel = WamBatteryLevel.Low;
                        break;
                    case BatteryLevel.Critical:
                        result.BattLevel = WamBatteryLevel.Critical;
                        break;
                    default:
                        result.BattLevel = WamBatteryLevel.OK;
                        break;
                }
                switch (deviceInformation.DevState)
                {
                    case DeviceState.Empty:
                        result.DevState = WamDeviceState.Empty;
                        break;
                    case DeviceState.Ready:
                        result.DevState = WamDeviceState.Ready;
                        break;
                    case DeviceState.OperationInProgress:
                        result.DevState = WamDeviceState.OperationInProgress;
                        break;
                    case DeviceState.Off:
                        result.DevState = WamDeviceState.Off;
                        break;
                    case DeviceState.NoUTK:
                        result.DevState = WamDeviceState.NoUTK;
                        break;
                    case DeviceState.HasData:
                        result.DevState = WamDeviceState.HasData;
                        break;
                    default:
                        result.DevState = WamDeviceState.Unknown;
                        break;
                }
                result.PairingCode = deviceInformation.PairingCode;
                return new WampcCommunicationResult<WampcDeviceInformation>(result);
            }
            catch (ReflectionTypeLoadException ex)
            {
                StringBuilder stringBuilder = new StringBuilder();
                foreach (Exception loaderException in ex.LoaderExceptions)
                {
                    stringBuilder.AppendLine(loaderException.Message);
                    FileNotFoundException notFoundException = loaderException as FileNotFoundException;
                    if (notFoundException != null && !string.IsNullOrEmpty(notFoundException.FusionLog))
                    {
                        stringBuilder.AppendLine("Fusion Log:");
                        stringBuilder.AppendLine(notFoundException.FusionLog);
                    }
                    stringBuilder.AppendLine();
                }
                WamPc.writeLog("LOADER EXCEPTION ERROR HAS SHOWN: " + stringBuilder.ToString());
                return new WampcCommunicationResult<WampcDeviceInformation>(new Exception("LOADER EXCEPTIONS THROWN!"));
            }
            catch (Exception ex)
            {
                WamPc.writeLog("***WamPc - Exception while getting device information: " + ex.Message);
                WamPc.writeLog("***WamPc - Exception while getting device information (Stack Trace): " + ex.StackTrace);
                return new WampcCommunicationResult<WampcDeviceInformation>(ex);
            }
        }

        /// <summary>
        /// Pairs the WAM device to the USB Utk dongle
        /// </summary>
        /// <param name="settings">Settings that are to be applied to the WAM device</param>
        /// <param name="useDefaults">Indicates the default WAM settings should be used</param>
        /// <returns>new pairing ID if successful, -1 if not</returns>
        public virtual WampcCommunicationResult<int?> PairDeviceToUtkDongle(WamPairingSettings settings, bool useDefaults)
        {
            _log.Verbose("***WamPc - Pairing Device to dongle");
            return PairDeviceToUtkDongle(settings, useDefaults, 30000);
        }

        /// <summary>
        /// Pairs the WAM device to the USB Utk dongle
        /// </summary>
        /// <param name="settings">Settings that are to be applied to the WAM device</param>
        /// <param name="useDefaults">Indicates the default WAM settings should be used</param>
		/// <param name="timeout">Timeout for the pairing operation in milliseconds</param>
        /// <returns>new pairing ID if successful, -1 if not</returns>
        public virtual WampcCommunicationResult<int?> PairDeviceToUtkDongle(WamPairingSettings settings, bool useDefaults, int timeout)
        {
            int? newPairingId = null;

            var cfg = new DeviceConfiguration();

            if (useDefaults)
            {
                cfg = DeviceConfiguration.ELIX_DEFAULTS;
                _log.Verbose("***WamPc - pairing using default values: " + cfg.CapturedSeconds + "," + cfg.MinValidSeconds + "," + cfg.SelfPoweroffTimeoutMinutes + "," + cfg.TestAcquisitionSeconds);
            }
            else
            {
                if (settings.MaxDuration + settings.MinDuration + settings.TestAcq + settings.SelfPowerOff != 0)
                {
                    try
                    {
                        cfg = new DeviceConfiguration(settings.MaxDuration, settings.MinDuration, settings.TestAcq,
                            settings.SelfPowerOff);
                    }
                    catch (ArgumentOutOfRangeException e)
                    {
                        return new WampcCommunicationResult<int?>(e) { Result = -1 };
                    }
                }
            }

            AsyncOperationManager operationManager = new AsyncOperationManager(timeout);
            using (var sync = new AutoResetEvent(false))
            {
                var cApi = ELIxAPIFacade.Controller;

                var ps = new PairingSettings(
                    dpp =>
                    {
                        operationManager.Start();
                        OnWamStatusUpdate(new WamEventArgs("Pairing device with UTK dongle", WamEventArgs.WamStatus.Pairing));

                        if (dpp.AsyncOperationResult == OperationResult.Success)
                        {
                            //success...set the Communicator to the new pairing code and store device configuration.
                            newPairingId = dpp.PairingCode;
                            OnWamStatusUpdate(new WamEventArgs("Pairing successful.  New pairing ID is: " + newPairingId, WamEventArgs.WamStatus.Success));
                        }
                        operationManager.EvaluateStop(dpp);
                    },
                    false, cfg, sync);

                cApi.StartDevicePairing(ps);

                sync.WaitOne();
                cApi.WaitForAsyncOperation(2000);
            }

            return new WampcCommunicationResult<int?>(newPairingId);
        }

        /// <summary>
        ///  Programs the WAM with a new AcquisitionID
        /// </summary>
        /// <param name="newAcquisitionId">New acquisition ID to be programmed on the device</param>
        /// <returns>WampcCommunicationResult.Result = true if successful, false if now</returns>
        public virtual WampcCommunicationResult<bool> ProgramDevice(int newAcquisitionId)
        {
            var returnValue = new WampcCommunicationResult<bool>(true);

            try
            {
                var acquisitionId = newAcquisitionId;

                using (var sync = new AutoResetEvent(false))
                {
                    var cApi = ELIxAPIFacade.Controller;
                    var devState = cApi.GetDeviceInformation().DevState;

                    var aji = new AcquisitionJobInfo(acquisitionId);

                    AsyncOperationManager operationManager = new AsyncOperationManager(30000);
                    var acqs = new AcquisitionSettings(
                        aji,
                        aop =>
                        {
                            operationManager.Start();

                            if (devState == DeviceState.NoUTK || devState == DeviceState.Off)
                            {
                                operationManager.Stop(aop);
                                returnValue.Result = false;
                                returnValue.Exception =
                                    new Exception("Unable to communicate with the WAM PC -> " + devState);
                            }
                            else
                            {
                                if (aop.AsyncOperationResult == OperationResult.Success)
                                {

                                    OnWamStatusUpdate(
                                        new WamEventArgs(
                                            "Device prepared successfully with new acquisition id: " + aji.AcquisitionId,
                                            WamEventArgs.WamStatus.Success));
                                }
                            }

                            OnWamStatusUpdate(new WamEventArgs("Preparing", WamEventArgs.WamStatus.Preparing));

                            operationManager.EvaluateStop(aop);
                        },
                        sync);

                    cApi.StartDevicePrepare(acqs);
                    sync.WaitOne();
                    cApi.WaitForAsyncOperation(2000);
                }
            }
            catch (Exception exc)
            {
                returnValue.Result = false;
                returnValue.Exception = exc;
            }

            return returnValue;
        }

        /// <summary>
        /// Deletes all memory from Wam device
        /// </summary>
        /// <returns>true if successfull, false if failure</returns>
        public virtual WampcCommunicationResult<bool> DeleteDeviceMemory()
        {
            var cApi = ELIxAPIFacade.Controller;

            // empty device, trying to delete an empty device will actually throw an exception in they WAM API.  Handle it and return true.
            if (cApi.GetDeviceInformation().DevState == DeviceState.Empty)
            {
                OnWamStatusUpdate(new WamEventArgs("Device is empty.", WamEventArgs.WamStatus.None));
                return new WampcCommunicationResult<bool>(true);
            }

            // if device is off or no UTK, return an exception to the user that device communication cannot take place
            if (cApi.GetDeviceInformation().DevState == DeviceState.NoUTK || cApi.GetDeviceInformation().DevState == DeviceState.Off)
            {
                OnWamStatusUpdate(new WamEventArgs("Unable to communicate with device.  Please ensure device is powered on and try again.", WamEventArgs.WamStatus.Failure));
                return new WampcCommunicationResult<bool>(new Exception("Unable to communicate with device.  Please ensure device is powered on and try again."));
            }

            OnWamStatusUpdate(new WamEventArgs("Deleting Device Memory", WamEventArgs.WamStatus.Deleting));

            try
            {
                // delete the memory on the WAM
                cApi.DeleteDeviceMemory();
            }
            catch (Exception ex) // handle and return any exceptions
            {
                OnWamStatusUpdate(new WamEventArgs("Failure occurred while deleting WAM memory", ex, WamEventArgs.WamStatus.None));
                return new WampcCommunicationResult<bool>(ex) { Result = false };
            }

            // successful deletion
            OnWamStatusUpdate(new WamEventArgs("Memory deletion successful", WamEventArgs.WamStatus.DeleteSuccess));
            return new WampcCommunicationResult<bool>(true);
        }

        /// <summary>
        /// Asynchronously downloads the Ecg data from Wam device
        /// </summary>
        /// <param name="callback">Called once ecg data is downloaded and returns Pdf byte array</param>
        /// <param name="acquisitionJobRecord">Contains all information needed for generating the ELIX_RECORD</param>
        public virtual void DownloadWamEcgDataAsync(WampcCallbacks.PdfLoadedCallback callback, AcquisitionJobRecord acquisitionJobRecord)
        {
            WamPc.writeLog("DownloadWamEcgDataAsync initiated!");
            ITransferDataProgress saved = (ITransferDataProgress)null;
            byte[] pdfByteArray = new byte[0];
            writeLog("1");
            string elixSerializedString = string.Empty;
            writeLog("2");
            Task.Factory.StartNew((Action)(() =>
            {
                try
                {
                    writeLog("3");
                    IWamPCControl controller = ELIxAPIFacade.Controller;
                    writeLog("4");           
                    using (AutoResetEvent autoResetEvent = new AutoResetEvent(false))
                    {
                        writeLog("5");
                        AsyncOperationManager operationManager = new AsyncOperationManager(120000);
                        writeLog("6");
                        DataXferSettings dxfs = new DataXferSettings((TransferProgressNotifier)(o =>
                        {
                            //writeLog("7");
                            operationManager.Start();
                            //writeLog("8");

                            if (o.DeviceInformation == null)
                            {
                                this.OnWamStatusUpdate(new WamEventArgs("Failed to locate ecg data.  Please make sure device is turned on and try again.", WamEventArgs.WamStatus.Failure));
                                writeLog("Unable to locate ecg data. ");
                                operationManager.EvaluateStop((IAsyncOperationProgress)o);
                                writeLog("9");
                            }
                            else
                            {
                                this.OnWamStatusUpdate(new WamEventArgs(o.ProgressPercentage.ToString(), WamEventArgs.WamStatus.DownloadingEcg));
                                if (o.AsyncOperationResult != OperationResult.Success && o.AsyncOperationResult != OperationResult.InProgress)
                                {
                                    this.OnWamStatusUpdate(new WamEventArgs("Failure occurred while downloading ecg data.  Please power cycle the device and try again.", WamEventArgs.WamStatus.Failure));
                                    writeLog("WAMPC reported failure as AsyncOperationResult");
                                    operationManager.Stop((IAsyncOperationProgress)o);
                                }
                                else
                                {
                                    saved = o;
                                    writeLog("reporting progress");
                                    writeLog("progress Percentage: " + saved.ProgressPercentage);
                                    if (saved.FaultObject != null)
                                    {
                                        if (saved.FaultObject.Message != null)
                                        {
                                            writeLog("An exception has occured during progress reporting: " + saved.FaultObject.Message);
                                            writeLog("Stacktrace is: ");
                                            writeLog("   --- " + saved.FaultObject.StackTrace);
                                            if (saved.FaultObject.InnerException != null)
                                            {
                                                writeLog("    An exception has occured during progress reporting - Innerexception: " +
                                                    saved.FaultObject.InnerException.Message);
                                                writeLog("        Stacktrace is: ");
                                                writeLog("              --- " + saved.FaultObject.InnerException.StackTrace);                                            
                                            }
                                        } else
                                        {
                                            writeLog("An exception has occured during progress reporting BUT MESSAGE IS NULL");
                                        }    
                                    }

                                    writeLog("TransferredSeconds: " + saved.TransferredSeconds);
                                    
                                    operationManager.EvaluateStop((IAsyncOperationProgress)o);
                                }
                            }
                        }), (object)autoResetEvent);
                        controller.StartDataTransfer(dxfs);
                        autoResetEvent.WaitOne();
                        controller.WaitForAsyncOperation(2000);

                        writeLog("a");

                        if (saved == null)
                        {
                            writeLog("saved == null");
                            return;
                        }

                        this.OnWamStatusUpdate(new WamEventArgs("Rendering...", WamEventArgs.WamStatus.RenderingPdf));
                        int lengthInMilliseconds = acquisitionJobRecord.AcquisitionLengthInMilliseconds;
                        ACFilter acFilter = this.ToACFilter(acquisitionJobRecord.ACFilter);
                        AgeInfo eliXpatientAge = acquisitionJobRecord.EliXPatientAge;
                        PatientGender eliXpatientGender = acquisitionJobRecord.EliXPatientGender;
                        WamPc.writeLog("ACFilter: " + (object)acquisitionJobRecord.ACFilter);
                        WamPc.writeLog("ac: " + acFilter.ToString());
                        WamPc.writeLog("Analyzing input: " + eliXpatientAge.AgeValue.ToString());
                        string str1 = "startMs: ";
                        int num = 0;
                        string str2 = num.ToString();
                        WamPc.writeLog(str1 + str2);
                        WamPc.writeLog("endMs: " + lengthInMilliseconds.ToString());
                        string str3 = "age: ";
                        num = eliXpatientAge.AgeValue;
                        string str4 = num.ToString();
                        WamPc.writeLog(str3 + str4);
                        WamPc.writeLog("gender: " + eliXpatientGender.ToString());
                        AnalysisInput input = new AnalysisInput(saved.TransferredSamples, 0, lengthInMilliseconds, acFilter, eliXpatientAge, eliXpatientGender, 0, true);
                        WamPc.writeLog("Initializing processing API...");
                        IELIxProcessing processingApi = ELIxAPIFacade.ProcessingAPI;
                        WamPc.writeLog("Analyzing...");
                        IAnalysisResult ar = processingApi.RunAnalysis(input);

                        if (saved.DeviceInformation.RecordingInfo.AcquisitionTimestamp.HasValue)
                            acquisitionJobRecord.CollectionTimestampUTC = saved.DeviceInformation.RecordingInfo.AcquisitionTimestamp.Value;

                        try
                        {
                            acquisitionJobRecord.InvestigatorComments = "investigator comments";

                            writeLog("acquisitionJobRecord.ApplicationId: " + acquisitionJobRecord.ApplicationId);
                            writeLog("acquisitionJobRecord.ApplicationName: " + acquisitionJobRecord.ApplicationName);
                            writeLog("acquisitionJobRecord.ApplicationVersion: " + acquisitionJobRecord.ApplicationVersion);
                            writeLog("acquisitionJobRecord.ProcessingDeviceId: " + acquisitionJobRecord.ProcessingDeviceId);
                            writeLog("acquisitionJobRecord.ProcessingDeviceModel: " + acquisitionJobRecord.ProcessingDeviceModel);
                            writeLog("acquisitionJobRecord.SubjectID: " + acquisitionJobRecord.SubjectID);
                            writeLog("acquisitionJobRecord.SubjectInitials: " + acquisitionJobRecord.SubjectInitials);
                            writeLog("acquisitionJobRecord.SubjectDOB: " + acquisitionJobRecord.SubjectDOB);
                            writeLog("acquisitionJobRecord.SubjectDOBKind: " + acquisitionJobRecord.SubjectDOBKind);
                            writeLog("acquisitionJobRecord.SubjectGender: " + acquisitionJobRecord.SubjectGender);
                            writeLog("acquisitionJobRecord.InvestigatorAddress: " + acquisitionJobRecord.InvestigatorAddress);
                            writeLog("acquisitionJobRecord.InvestigatorComments: " + acquisitionJobRecord.InvestigatorComments);
                            writeLog("acquisitionJobRecord.InvestigatorIDAsInt: " + acquisitionJobRecord.InvestigatorIDAsInt);
                            writeLog("acquisitionJobRecord.InvestigatorName: " + acquisitionJobRecord.InvestigatorName);
                            writeLog("acquisitionJobRecord.ProtocolIDAsInt: " + acquisitionJobRecord.ProtocolIDAsInt);
                            writeLog("acquisitionJobRecord.ProtocolName: " + acquisitionJobRecord.ProtocolName);
                            writeLog("acquisitionJobRecord.SponsorIDAsInt: " + acquisitionJobRecord.SponsorIDAsInt);
                            writeLog("acquisitionJobRecord.SponsorName: " + acquisitionJobRecord.SponsorName);
                            writeLog("acquisitionJobRecord.SiteID: " + acquisitionJobRecord.SiteID);
                            writeLog("acquisitionJobRecord.UserName: " + acquisitionJobRecord.UserName);
                            writeLog("acquisitionJobRecord.VisitName: " + acquisitionJobRecord.VisitName);
                            writeLog("acquisitionJobRecord.VisitAbbreviation: " + acquisitionJobRecord.VisitAbbreviation);
                            writeLog("acquisitionJobRecord.CollectionTimestamp: " + acquisitionJobRecord.CollectionTimestamp.ToString("yyyy -MM-dd"));
                            writeLog("acquisitionJobRecord.CollectionTimestamp: " + acquisitionJobRecord.CollectionTimestamp.ToString("HH:mm:ss"));
                            writeLog("acquisitionJobRecord.VisitSubjectBP: " + acquisitionJobRecord.VisitSubjectBP);
                            writeLog("acquisitionJobRecord.VisitSubjectWeight: " + acquisitionJobRecord.VisitSubjectWeight);

                            var visitElix = EliXRecordsTools.GET_ELIX_RECORDVISIT_DEMO(1,
                                acquisitionJobRecord.VisitName,
                                acquisitionJobRecord.VisitAbbreviation,
                                acquisitionJobRecord.CollectionTimestamp.ToString("yyyy-MM-dd"),
                                acquisitionJobRecord.CollectionTimestamp.ToString("HH:mm:ss"),
                                acquisitionJobRecord.VisitSubjectBP,
                                acquisitionJobRecord.VisitSubjectWeight);

                            writeLog("visitElix.ABBREVIATION: " + visitElix.ABBREVIATION);
                            writeLog("visitElix.NAME: " + visitElix.NAME);
                            writeLog("visitElix.VISIT_DEMO_FIELD Length: " + visitElix.VISIT_DEMO_FIELD.Length.ToString());
                            writeLog("visitElix.VISIT_ID: " + visitElix.VISIT_ID);

                            ELIX_RECORD r = EliXRecordsTools.MakeEliXRecord(EliXRecordsTools.GET_ELIX_RECORDRECORDING_DATA(ar, saved.DeviceInformation, EliXRecordsTools.GET_ELIX_RECORDRECORDING_DATASOURCE(saved.DeviceInformation, acquisitionJobRecord.ApplicationId, acquisitionJobRecord.ApplicationName, acquisitionJobRecord.ApplicationVersion, acquisitionJobRecord.ProcessingDeviceId, acquisitionJobRecord.ProcessingDeviceModel)),
                            EliXRecordsTools.GET_ELIX_RECORDSUBJECT_DEMO(acquisitionJobRecord.SubjectID, acquisitionJobRecord.SubjectInitials, acquisitionJobRecord.SubjectDOB, acquisitionJobRecord.SubjectDOBKind, acquisitionJobRecord.SubjectGender),
                            EliXRecordsTools.GET_ELIX_RECORDSUBMISSION_DATA(acquisitionJobRecord.InvestigatorAddress, acquisitionJobRecord.InvestigatorComments, acquisitionJobRecord.InvestigatorIDAsInt, acquisitionJobRecord.InvestigatorName, acquisitionJobRecord.ProtocolIDAsInt, acquisitionJobRecord.ProtocolName, 0, false, string.Empty, acquisitionJobRecord.SponsorIDAsInt, acquisitionJobRecord.SponsorName, acquisitionJobRecord.SiteID, acquisitionJobRecord.UserName),
                            EliXRecordsTools.GET_ELIX_RECORDVISIT_DEMO(1, acquisitionJobRecord.VisitName, acquisitionJobRecord.VisitAbbreviation, acquisitionJobRecord.CollectionTimestamp.ToString("yyyy-MM-dd"), acquisitionJobRecord.CollectionTimestamp.ToString("HH:mm:ss"), acquisitionJobRecord.VisitSubjectBP, acquisitionJobRecord.VisitSubjectWeight));

                            r.RECORDING_DATA.SOURCE.ACQUISITION_TIME.ToString();
                            StringBuilder sb = new StringBuilder();
                            new XmlSerializer(r.GetType()).Serialize((TextWriter)new StringWriter(sb), (object)r);
                            elixSerializedString = sb.ToString();
                            string str5 = "ERT " + r.RECORDING_DATA.SOURCE.APPLICATION_NAME + " v." + r.RECORDING_DATA.SOURCE.APPLICATION_VERSION;
                            WamPc.writeLog("Version is..." + str5);
                            WamPc.writeLog("Generating PDF...");
                            WamPc.writeLog("Executing assembly: " + Assembly.GetExecutingAssembly().FullName);
                            pdfByteArray = processingApi.GeneratePDF(r, "English", "v.5.0.0.0");
                            WamPc.writeLog("PDF has been generated...");
                            if (pdfByteArray.Length == 0)
                                callback(new WampcCommunicationResult<ECGDownloadData>(new Exception("No PDF data loaded.")));
                            else
                                this.OnWamStatusUpdate(new WamEventArgs("ECG Data Rendered", WamEventArgs.WamStatus.ECGDataRendered));
                        }
                        catch (Exception ex)
                        {
                            Exception innerException = ex.InnerException;
                            string message = ex.Message;
                            string str5;
                            if (innerException != null)
                                str5 = message + Environment.NewLine + "   " + innerException.Message + Environment.NewLine + "          StackTrack: " + innerException.StackTrace;
                            else
                                str5 = message + Environment.NewLine + "   Inner exception is null";
                            WamPc.writeLog("An exception has been thrown: " + str5);
                            this.OnWamStatusUpdate(new WamEventArgs("Failure generating PDF - " + str5, ex, WamEventArgs.WamStatus.Failure));
                            callback(new WampcCommunicationResult<ECGDownloadData>(ex));
                        }
                    }
                }
                catch (Exception ex)
                {
                    Exception innerException = ex.InnerException;
                    string message = ex.Message;
                    string str5;
                    if (innerException != null)
                        str5 = message + Environment.NewLine + "   " + innerException.Message + Environment.NewLine + "          StackTrack: " + innerException.StackTrace;
                    else
                        str5 = message + Environment.NewLine + "   Inner exception is null";
                    WamPc.writeLog("An exception has been thrown: " + str5);
                    this.OnWamStatusUpdate(new WamEventArgs("Failure generating PDF - " + str5, ex, WamEventArgs.WamStatus.Failure));
                    callback(new WampcCommunicationResult<ECGDownloadData>(ex));
                }
            })).ContinueWith((Action<Task>)(cb => callback(new WampcCommunicationResult<ECGDownloadData>(new ECGDownloadData()
            {
                ECGData = pdfByteArray,
                Serialized_ELIX_Record = elixSerializedString
            }))));
        }

        #endregion

        #region Helpers
        /// <summary>
        /// Converts integer filterValue to ELiX API APIFilter enum.
        /// </summary>
        /// <param name="filterValue">value to convert</param>
        /// <returns>filter as enum type</returns>
        private ACFilter ToACFilter(int filterValue)
        {
            if (filterValue == 50)
            {
                return ACFilter.AC_50Hz;
            }
            else if (filterValue == 60)
            {
                return ACFilter.AC_60Hz;
            }
            else
            {
                return ACFilter.Off;
            }
        }
        #endregion
    }
}
