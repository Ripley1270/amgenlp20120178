﻿using System;
using System.Diagnostics;
using System.Runtime.Remoting.Messaging;
using invivodata.Interop.WAMPC.Communicator;

namespace invivodata.Interop.WAMPC
{
    /// <summary>
    /// Represents a hardware abstraction for the WAMPC to enable us to run the core without having the actual hardware.
    /// </summary>
    public class WAMPCMock : WamPc, IMockedWamCommunicator
    {
        #region IMockedWamCommunicator members

        /// <summary>
        /// Get the power state of the device - on/off
        /// </summary>
        public MockedWamPowerState PowerState { get; private set; }

        /// <summary>
        /// Get the current battery level
        /// </summary>
        public WamBatteryLevel CurrentMockBatteryLevel { get; private set; }

        /// <summary>
        /// Get the state of the device (HasData, off, etc...)
        /// </summary>
        public WamDeviceState CurrentMockDeviceState { get; private set; }
        
        /// <summary>
        /// Simulate pressing the power button on the WAM
        /// </summary>
        public void MockTogglePower()
        {
            switch (PowerState)
            {
                case MockedWamPowerState.WamOff:
                    PowerState = MockedWamPowerState.WamOn;
                    break;
                case MockedWamPowerState.WamOn:
                    PowerState = MockedWamPowerState.WamOff;
                    break;
            }
        }

        /// <summary>
        /// Sets the state of the device
        /// </summary>
        /// <param name="newState">new state that you are setting the device to</param>
        public void MockSetState(WamDeviceState newState)
        {
            CurrentMockDeviceState = newState;
        }

        /// <summary>
        /// Sets the battery level
        /// </summary>
        /// <param name="newBatteryLevel">new battery level</param>
        public void MockSetBatteryLevel(WamBatteryLevel newBatteryLevel)
        {
            CurrentMockBatteryLevel = newBatteryLevel;
        }

        /// <summary>
        /// Simulates pressing the ECG button on the device
        /// </summary>
        public void MockTakeECGPressed()
        {
            MockTogglePower();
            PowerState = MockedWamPowerState.WamOff;
        }

        #endregion

        /// <summary>
        /// Creates a new instance of the WAMPC hardware mock
        /// </summary>
        public WAMPCMock()
        {
            CurrentMockDeviceState = WamDeviceState.NoUTK;
            PowerState = MockedWamPowerState.WamOff;
        }

        /// <summary>
        /// Simulates getting device information from the device
        /// </summary>
        /// <returns>Communication result containing Simulated Device information</returns>
        public override WampcCommunicationResult<WampcDeviceInformation> GetDeviceInformation()
        {
            var curState = CurrentMockDeviceState;
            
            // when the device is on we will always assume it's been programmed
            if (PowerState == MockedWamPowerState.WamOn)
                curState = WamDeviceState.Ready;

            if (CurrentMockDeviceState == WamDeviceState.HasData)
                curState = WamDeviceState.HasData;

            // if it has data, but the device is off...override the HasData state and return Off
            if (CurrentMockDeviceState == WamDeviceState.HasData && PowerState == MockedWamPowerState.WamOff)
                    curState = WamDeviceState.Off;

            var rslt = new WampcDeviceInformation
            {
                DevState = curState,
                BattLevel = CurrentMockBatteryLevel,
                PairingCode = 12345
            };

            return new WampcCommunicationResult<WampcDeviceInformation>(rslt);
        }

        /// <summary>
        /// Pairs the device
        /// </summary>
        /// <param name="settings">unused pass null</param>
        /// <param name="useDefaults">unused pass anything</param>
        /// <returns>successful pairing result</returns>
        public override WampcCommunicationResult<int?> PairDeviceToUtkDongle(WamPairingSettings settings, bool useDefaults)
        {
            return new WampcCommunicationResult<int?>(12345);
        }

        /// <summary>
        /// Simulates programming the device
        /// </summary>
        /// <param name="newAcquisitionId">fake acquisition Id</param>
        /// <returns>communication result</returns>
        public override WampcCommunicationResult<bool> ProgramDevice(int newAcquisitionId)
        {
            PowerState = MockedWamPowerState.WamOff;
            return new WampcCommunicationResult<bool>(true);
        }

        /// <summary>
        /// Simulates deleting the device's memory
        /// </summary>
        /// <returns>communication result</returns>
        public override WampcCommunicationResult<bool> DeleteDeviceMemory()
        {
            return new WampcCommunicationResult<bool>(true);
        }

        /// <summary>
        /// Simulates downloading data from the wam by using a stopwatch and calling the WamStatusUpdate event
        /// </summary>
        /// <param name="callback">Callback to be invoked when download is complete</param>
        /// <param name="acquisitionJobRecord">pass null - unused</param>
        public override void DownloadWamEcgDataAsync(WampcCallbacks.PdfLoadedCallback callback, AcquisitionJobRecord acquisitionJobRecord)
        {
            var sw = new Stopwatch();
            sw.Start();

            do // simulate downloading a 10 second recording
            {
                var msg = (sw.ElapsedMilliseconds/100).ToString();
                OnWamStatusUpdate(new WamEventArgs(msg, WamEventArgs.WamStatus.DownloadingEcg));
            } while (sw.ElapsedMilliseconds <= 10000);

            // return the WAMSAMPLE.pdf included in the project
            var EcgData = System.IO.File.ReadAllBytes(@"WAMSAMPLE.pdf");

            var ecgDownloadData = new ECGDownloadData();
            ecgDownloadData.ECGData = EcgData;
            ecgDownloadData.Serialized_ELIX_Record = string.Empty;

            // fire off the callback
            callback(new WampcCommunicationResult<ECGDownloadData>(ecgDownloadData));
        }

        public override event EventHandler<WamEventArgs> WamStatusUpdateEvent;
        protected override void OnWamStatusUpdate(WamEventArgs e)
        {
            if (WamStatusUpdateEvent != null)
                WamStatusUpdateEvent(this, e);
        }
    }
}
