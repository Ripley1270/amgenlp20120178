﻿using invivodata.Interop.WAMPC.Utilities;
using Mortara.ELIxCtrl.API;
using System;

namespace invivodata.Interop.WAMPC
{
    public class AcquisitionJobRecord
    {
        private const int UNDEFINED_ID = 0;
        private const string GENDER_MALE = "male";
        private const string GENDER_FEMALE = "female";

        public int AcquisitionLengthInMilliseconds { get; set; }

        public bool HasBeenSubmitted { get; set; }

        public int ACFilter { get; set; }

        public int JobID { get; set; }

        public static int NewID
        {
            get
            {
                return (int)(DateTime.UtcNow.Ticks & 16777215L);
            }
        }

        public DateTime CollectionTimestampUTC { get; set; }

        public DateTime CollectionTimestamp
        {
            get
            {
                return this.CollectionTimestampUTC;
            }
        }

        public DateTime ClockResetTimestampUTC { get; set; }

        public DateTime JobStartTimestampUTC
        {
            get
            {
                return this.ClockResetTimestampUTC;
            }
        }

        public string SponsorID { get; set; }

        public int SponsorIDAsInt
        {
            get
            {
                int result;
                return string.IsNullOrEmpty(this.SponsorID) || !int.TryParse(this.SponsorID, out result) ? 0 : result;
            }
        }

        public string SponsorName { get; set; }

        public string ProtocolID { get; set; }

        public int ProtocolIDAsInt
        {
            get
            {
                int result;
                return string.IsNullOrEmpty(this.ProtocolID) || !int.TryParse(this.ProtocolID, out result) ? 0 : result;
            }
        }

        public string ProtocolName { get; set; }

        public string InvestigatorID { get; set; }

        public int InvestigatorIDAsInt
        {
            get
            {
                int result;
                return string.IsNullOrEmpty(this.InvestigatorID) || !int.TryParse(this.InvestigatorID, out result) ? 0 : result;
            }
        }

        public string InvestigatorName { get; set; }

        public string InvestigatorAddress { get; set; }

        public string InvestigatorComments { get; set; }

        public string SiteID { get; set; }

        public string UserName { get; set; }

        public string VisitID { get; set; }

        public string VisitName { get; set; }

        public string VisitAbbreviation { get; set; }

        public string VisitSubjectBP { get; set; }

        public string VisitSubjectWeight { get; set; }

        public string VisitSubjectHeight { get; set; }

        public string SubjectID { get; set; }

        public string SubjectInitials { get; set; }

        public string SubjectDOB { get; set; }

        public string SubjectDOBKind { get; set; }

        public string SubjectGender { get; set; }

        public string SubjectEthnicity { get; set; }

        public string SiteName { get; set; }

        public string SequenceID { get; set; }

        public string UserID { get; set; }

        public string ApplicationId
        {
            get
            {
                return "2A395C51-F155-4C01-964D-BB683F421604";
            }
        }

        public string ApplicationName
        {
            get
            {
                return "SitePAD";
            }
        }

        public string ApplicationVersion
        {
            get
            {
                return "1.0";
            }
        }

        public string ProcessingDeviceId
        {
            get
            {
                return "92";
            }
        }

        public string ProcessingDeviceModel
        {
            get
            {
                return "ELIX-API";
            }
        }

        public AgeInfo EliXPatientAge
        {
            get
            {
                try
                {
                    if (string.IsNullOrEmpty(this.SubjectDOBKind))
                    {
                        WamPc.writeLog("SubjectDOBKind is null or empty");
                        return new AgeInfo(0, AgeInfo.AgeUnitKind.Unknown);
                    }
                    string lower = this.SubjectDOBKind.ToLower();
                    if (!(lower == "y"))
                    {
                        if (lower == "ym")
                        {
                            WamPc.writeLog("YEAR-MONTH Age: " + this.EliXPatientAgeFromFullDOB(this.SubjectDOB).AgeValue.ToString());
                            return this.EliXPatientAgeFromYearMonthDOB(this.SubjectDOB);
                        }
                        WamPc.writeLog("DEFAULT Age: " + this.EliXPatientAgeFromFullDOB(this.SubjectDOB).AgeValue.ToString());
                        return this.EliXPatientAgeFromFullDOB(this.SubjectDOB);
                    }
                    WamPc.writeLog("YEAR Age: " + this.EliXPatientAgeFromYearDOB(this.SubjectDOB).AgeValue.ToString());
                    return this.EliXPatientAgeFromYearDOB(this.SubjectDOB);
                }
                catch (Exception ex)
                {
                    WamPc.writeLog("Error parsing Date Of Birth in AcquisitionJobRecord.cs - " + ex.Message);
                    throw;
                }
            }
        }

        public PatientGender EliXPatientGender
        {
            get
            {
                if (string.IsNullOrEmpty(this.SubjectGender))
                    return PatientGender.Unknown;
                return this.SubjectGender.Equals("male", StringComparison.InvariantCultureIgnoreCase) ? PatientGender.Male : PatientGender.Female;
            }
        }

        public AcquisitionJobRecord()
        {
            this.JobID = 0;
            this.SponsorID = string.Empty;
            this.SponsorName = string.Empty;
            this.ProtocolID = string.Empty;
            this.ProtocolName = string.Empty;
            this.InvestigatorID = string.Empty;
            this.InvestigatorName = string.Empty;
            this.InvestigatorAddress = string.Empty;
            this.InvestigatorComments = string.Empty;
            this.SiteID = string.Empty;
            this.UserName = string.Empty;
            this.VisitID = string.Empty;
            this.VisitName = string.Empty;
            this.VisitAbbreviation = string.Empty;
            this.VisitSubjectBP = string.Empty;
            this.VisitSubjectWeight = string.Empty;
            this.VisitSubjectHeight = string.Empty;
            this.SubjectID = string.Empty;
            this.SubjectInitials = string.Empty;
            this.SubjectDOB = string.Empty;
            this.SubjectDOBKind = string.Empty;
            this.SubjectGender = string.Empty;
            this.SubjectEthnicity = string.Empty;
        }

        private AgeInfo EliXPatientAgeFromYearDOB(string subjectDOB)
        {
            WamPc.writeLog("EliXPatientAgeFromYearDOB subjectDOB ---> " + subjectDOB);
            int age = DateTimeHelper.UtcNow.Year - int.Parse(subjectDOB);
            WamPc.writeLog("EliXPatientAgeFromYearDOB ---> " + age.ToString());
            return this.ConvertToEliXPatientAge(age);
        }

        private AgeInfo EliXPatientAgeFromYearMonthDOB(string subjectDOB)
        {
            DateTime utcNow = DateTimeHelper.UtcNow;
            int num1 = int.Parse(subjectDOB.Substring(0, 4));
            int num2 = int.Parse(subjectDOB.Substring(5, 2));
            int age = utcNow.Year - num1;
            if (utcNow.Month < num2)
                --age;
            return this.ConvertToEliXPatientAge(age);
        }

        private AgeInfo EliXPatientAgeFromFullDOB(string subjectDOB)
        {
            WamPc.writeLog("subjectDOB = " + subjectDOB);
            DateTime utcNow = DateTimeHelper.UtcNow;
            WamPc.writeLog("Step 1");
            int num1 = int.Parse(subjectDOB.Substring(0, 4));
            WamPc.writeLog("Step 2");
            int num2 = int.Parse(subjectDOB.Substring(5, 2));
            WamPc.writeLog("Step 3");
            int num3 = int.Parse(subjectDOB.Substring(8, 2));
            WamPc.writeLog("Step 4");
            int age = utcNow.Year - num1;
            if (utcNow.Month < num2 || utcNow.Month == num2 && utcNow.Day < num3)
                --age;
            WamPc.writeLog("Before ConvertToEliX");
            return this.ConvertToEliXPatientAge(age);
        }

        private AgeInfo ConvertToEliXPatientAge(int age)
        {
            if (age < 0)
                return new AgeInfo(0, AgeInfo.AgeUnitKind.Unknown);
            if (age >= 16)
                return new AgeInfo(age, AgeInfo.AgeUnitKind.Years);
            age = age * 365 + 1;
            return new AgeInfo(age, AgeInfo.AgeUnitKind.Days);
        }
    }
}
