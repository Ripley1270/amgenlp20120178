﻿// -------------------------------------------------------------------
// $Date: 2014-06-27 18:47:24 +0200 (ven, 27 giu 2014) $
// $Author: maarek $
// $Revision: 1108 $
// $HeadURL: http://vm-mastersvn-svr.local.mortara.it:18080/svn/ELI-X/trunk/ELI-X/Mortara.ElixAPI.TestApp/EliXRecordsTools.cs $
// -------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Xml.Serialization;
using Mortara.ELIx.ECGRecord;
using Mortara.ELIxCtrl.API;
using Mortara.ELIxCtrl.API.Veritas;

namespace invivodata.Interop.WAMPC
{
    /// <summary>
    /// Helper class provided by Mortara to abstract away the details of building ELIX_RECORD data.
    /// </summary>
    static class EliXRecordsTools
	{
		public static Byte[] SerializeELIXRecord(ELIX_RECORD elix, out String checksum)
		{
			var	xml_serializer	= new XmlSerializer(typeof(ELIX_RECORD));
			Byte[] res;
			using(var xml_stream = new MemoryStream())
			{
				xml_serializer.Serialize(xml_stream, elix);
				res = xml_stream.ToArray();
			}

			//Calculate MD5 checksum
			MD5 md5_checksum = MD5.Create();
			checksum = Convert.ToBase64String(md5_checksum.ComputeHash(res));
			return res;
		}

        #region Build ELIX record data

        /// <summary>
        /// Creates an elix_record to be passed to WAM API when generating a PDF.
        /// </summary>
        /// <param name="recordingData">ecg recording data</param>
        /// <param name="subjectDemo">subject demographics</param>
        /// <param name="submissionData">investigator, sponsor, etc... information</param>
        /// <param name="visitDemo">contains information defining the current visit</param>
        /// <returns></returns>
        public static ELIX_RECORD MakeEliXRecord(ELIX_RECORDRECORDING_DATA recordingData,
                ELIX_RECORDSUBJECT_DEMO subjectDemo, ELIX_RECORDSUBMISSION_DATA submissionData, ELIX_RECORDVISIT_DEMO visitDemo)
        {
            var res = new ELIX_RECORD
            {
                RECORDING_DATA = recordingData,
                SUBJECT_DEMO = subjectDemo,
                SUBMISSION_DATA = submissionData,
                VISIT_DEMO = visitDemo
            };

            return res;
        }

        internal static ELIX_RECORDRECORDING_DATA GET_ELIX_RECORDRECORDING_DATA(IAnalysisResult ar, IDeviceInformation devInfo,
                ELIX_RECORDRECORDING_DATASOURCE dataSource)
        {
            var reData = new ELIX_RECORDRECORDING_DATA
            {
                AUTOMATIC_INTERPRETATION = ConvertStatements(ar),
                CHANNEL = ConvertLeads(ar),
                GLOBAL_MEASUREMENTS = ConvertMeasures(ar),
                QUALITY_INFO = new ELIX_RECORDRECORDING_DATAQUALITY_INFO
                {
                    ELECTRODES_FAIL_MASK = ConvertElectrodesArray(ar.LFMask.ToArray()),
                    OVERRIDE_LF = devInfo.RecordingInfo.State == AcquisitionState.Test
                },
                TYPICAL_CYCLE = ConvertAverages(ar),
                SOURCE = dataSource
            };

            return reData;
        }

        internal static ELIX_RECORDSUBMISSION_DATA GET_ELIX_RECORDSUBMISSION_DATA(string address, string comment, int investigator_id,
                string investigator_name, int protocol_id, string protocol_name, int reason_code_id, bool reason_code_idspecified, string reason_code_text,
                int sponsor_id, string sponsor_name, string sponsor_site_id, string user_name)
        {
            var res = new ELIX_RECORDSUBMISSION_DATA
            {
                INVESTIGATOR_ADDRESS = address,
                INVESTIGATOR_COMMENT = comment,
                INVESTIGATOR_ID = investigator_id,
                INVESTIGATOR_NAME = investigator_name,
                PROTOCOL_ID = protocol_id,
                PROTOCOL_NAME = protocol_name,
                REASON_CODE_ID = reason_code_id,
                REASON_CODE_IDSpecified = reason_code_idspecified,
                REASON_CODE_TEXT = reason_code_text,
                SPONSOR_ID = sponsor_id,
                SPONSOR_NAME = sponsor_name,
                SPONSOR_SITE_ID = sponsor_site_id,
                USER_NAME = user_name
            };

            return res;
        }

        internal static ELIX_RECORDSUBJECT_DEMO GET_ELIX_RECORDSUBJECT_DEMO(string subjectNumber, string initials, string dob, string dobKind, string gender)
        {
            // SUBJECT_INDEX will not be used in ELiX API version.
            var res = new ELIX_RECORDSUBJECT_DEMO { SUBJECT_INDEX = 0, SUBJECT_INDEXSpecified = false };

            var flds = new List<ELIX_RECORDSUBJECT_DEMOSUBJECT_DEMO_FIELD>();

            // Add subject number -- required.
            flds.Add(new ELIX_RECORDSUBJECT_DEMOSUBJECT_DEMO_FIELD
            {
                ENTRY_DATA_TYPE = "STRING",
                ENTRY_FIELD_LABEL = "ID",
                ENTRY_FIELD_TYPE = 3,
                Value = subjectNumber
            });

            // Add subject initials
            if (!string.IsNullOrEmpty(initials))
            {
                flds.Add(new ELIX_RECORDSUBJECT_DEMOSUBJECT_DEMO_FIELD
                {
                    ENTRY_DATA_TYPE = "STRING",
                    ENTRY_FIELD_LABEL = "Initials",
                    ENTRY_FIELD_TYPE = 1,
                    Value = initials
                });
            }

            var reorderedDOB = dob;

            // reorder the pased in DOB to ensure it matches what the Mortara api is expecting which is:  mm/dd/yyyy
            if (dob.Length > 0)
            {
                // year only passed in so prepent 01/01
                if (dobKind == "Y")
                    reorderedDOB = "01/01/" + dob;

                // year and month only passed in.  add in a day of '01'...also reorder the strings so they read mm/dd/yyyy
                if (dobKind == "YM" && dob.Length > 6)
                {
                    string dobYear = dob.Substring(0, 4);
                    string dobMonth = dob.Substring(5, 2);

                    reorderedDOB = dobMonth + "/01/" + dobYear;
                }

                // reorder the dob string to be in the format mm/dd/yyyy
                if (dobKind == "YMD" && dob.Length > 9)
                {
                    string dobYear = dob.Substring(0, 4);
                    string dobMonth = dob.Substring(5, 2);
                    string dobDay = dob.Substring(8, 2);

                    reorderedDOB = dobMonth + "/" + dobDay + "/" + dobYear;
                }
            }

            // Add subject dob
            if (!string.IsNullOrEmpty(dob))
            {
                flds.Add(new ELIX_RECORDSUBJECT_DEMOSUBJECT_DEMO_FIELD
                {
                    ENTRY_DATA_TYPE = "STRING",
                    ENTRY_FIELD_LABEL = "DOB",
                    ENTRY_FIELD_TYPE = 2,
                    Value = reorderedDOB
                });
            }

            // Add gender.
            if (!string.IsNullOrEmpty(gender))
            {
                flds.Add(new ELIX_RECORDSUBJECT_DEMOSUBJECT_DEMO_FIELD
                {
                    ENTRY_DATA_TYPE = "STRING",
                    ENTRY_FIELD_LABEL = "Gender",
                    ENTRY_FIELD_TYPE = 6,
                    Value = gender
                });
            }

            res.SUBJECT_DEMO_FIELD = flds.ToArray();

            return res;
        }

        internal static ELIX_RECORDRECORDING_DATASOURCE GET_ELIX_RECORDRECORDING_DATASOURCE(IDeviceInformation devInfo,
                string applicationID, string applicationName, string applicationVersion, string processingDeviceId, string processingDeviceModel)
        {
            var res = new ELIX_RECORDRECORDING_DATASOURCE
            {
                ACQUISITION_DEVICE_ID = devInfo.Serial,
                ACQUISITION_DEVICE_MODEL = devInfo.Model,
                ACQUISITION_DEVICE_VERSION = devInfo.Version,
                APPLICATION_ID = applicationID,
                APPLICATION_NAME = applicationName,
                APPLICATION_VERSION = applicationVersion,
                ECG_UPLOAD_TIME = DateTime.UtcNow
            };

            if (devInfo.RecordingInfo.AcquisitionTimestamp.HasValue)
            {
                res.ACQUISITION_TIME = devInfo.RecordingInfo.AcquisitionTimestamp.Value;
            }

            if (devInfo.RecordingInfo.ClockResetTimestamp.HasValue)
            {
                res.ID_DOWNLOAD_TIME = devInfo.RecordingInfo.ClockResetTimestamp.Value;
            }

            res.PROCESSING_DEVICE_ID = processingDeviceId; // HOST APP: ELI-PC uses APPLICATION_ID 
            res.PROCESSING_DEVICE_MODEL = processingDeviceModel; // HOST APP (although this is a reasonable value)

            var mainVersion = typeof(IELIxProcessing).Assembly.GetName().Version;

            res.PROCESSING_DEVICE_VERSION = mainVersion.ToString(); // get actual version from assembly
            res.MEPA_START_LISTEN_TIME = new DateTime();
            // when the acquisition started (is it really needed? - check with EXPERT2 guys)

            return res;
        }

        internal static ELIX_RECORDVISIT_DEMO GET_ELIX_RECORDVISIT_DEMO(int visit_id, string name, string abbreviation, string collectionDate,
                string collectionTime, string bloodPressure, string weight)
        {
            var res = new ELIX_RECORDVISIT_DEMO { VISIT_ID = visit_id, NAME = name, ABBREVIATION = abbreviation };

            var flds = new List<ELIX_RECORDVISIT_DEMOVISIT_DEMO_FIELD>();
            flds.Add(new ELIX_RECORDVISIT_DEMOVISIT_DEMO_FIELD
            {
                ENTRY_DATA_TYPE = "STRING",
                ENTRY_FIELD_LABEL = "Collection Date",
                ENTRY_FIELD_TYPE = 7,
                Value = collectionDate
            });

            flds.Add(new ELIX_RECORDVISIT_DEMOVISIT_DEMO_FIELD
            {
                ENTRY_DATA_TYPE = "STRING",
                ENTRY_FIELD_LABEL = "Collection Time",
                ENTRY_FIELD_TYPE = 8,
                Value = collectionTime
            });

            if (!string.IsNullOrEmpty(bloodPressure))
            {
                flds.Add(new ELIX_RECORDVISIT_DEMOVISIT_DEMO_FIELD
                {
                    ENTRY_DATA_TYPE = "STRING",
                    ENTRY_FIELD_LABEL = "BP",
                    ENTRY_FIELD_TYPE = 10,
                    Value = bloodPressure
                });
            }

            if (!string.IsNullOrEmpty(weight))
            {
                flds.Add(new ELIX_RECORDVISIT_DEMOVISIT_DEMO_FIELD
                {
                    ENTRY_DATA_TYPE = "STRING",
                    ENTRY_FIELD_LABEL = "Weight (lb)",
                    ENTRY_FIELD_TYPE = 11,
                    Value = weight
                });
            };

            WamPc.writeLog("flds.length = " + flds.ToArray().Length.ToString());

            res.VISIT_DEMO_FIELD = flds.ToArray();

            return res;
        }

        #endregion


		static TElectrodes[] ConvertElectrodesArray(Electrodes10W[] input)
		{
			var res = new TElectrodes[input.Length];

			for(var i = 0; i < input.Length; ++i)
			{
				switch(input[i])
				{
				case Electrodes10W.LA:
					res[i] = TElectrodes.LA_L;
					break;

				case Electrodes10W.LL:
					res[i] = TElectrodes.LL_F;
					break;

				case Electrodes10W.RA:
					res[i] = TElectrodes.RA_R;
					break;

				case Electrodes10W.RL:
					res[i] = TElectrodes.RL_N;
					break;

				case Electrodes10W.V1:
					res[i] = TElectrodes.V1_C1;
					break;

				case Electrodes10W.V2:
					res[i] = TElectrodes.V2_C2;
					break;

				case Electrodes10W.V3:
					res[i] = TElectrodes.V3_C3;
					break;

				case Electrodes10W.V4:
					res[i] = TElectrodes.V4_C4;
					break;

				case Electrodes10W.V5:
					res[i] = TElectrodes.V5_C5;
					break;

				case Electrodes10W.V6:
					res[i] = TElectrodes.V6_C6;
					break;
				}

			}
			return res;
		}

		static ELIX_RECORDRECORDING_DATASTATEMENT[] ConvertStatements(IAnalysisResult ar)
		{
			var list = new List<ELIX_RECORDRECORDING_DATASTATEMENT>();
			foreach(var stmt in ar.Statements)
			{
				var itm = new ELIX_RECORDRECORDING_DATASTATEMENT()
				{
					STATEMENT_NUMBER = (UInt32)stmt.LineNumber,
					TEXT = stmt.Statement,
					REASON = stmt.Reasons
				};

				list.Add(itm);
			}

			return list.ToArray();
		}

		static ELIX_RECORDRECORDING_DATACHANNEL[] ConvertLeads(IAnalysisResult ar)
		{
			var result = new ELIX_RECORDRECORDING_DATACHANNEL[ar.Best10Samples.LeadsCount];
			for(var wave_idx = 0; wave_idx < ar.Best10Samples.LeadsCount; ++wave_idx)
			{
				result[wave_idx] = new ELIX_RECORDRECORDING_DATACHANNEL();

				result[wave_idx].BITS = ELIX_RECORDRECORDING_DATATYPICAL_CYCLEBITS.Item16;
				result[wave_idx].FORMAT = ELIX_RECORDRECORDING_DATATYPICAL_CYCLEFORMAT.SIGNED;

				result[wave_idx].SAMPLE_FREQ = (UInt32)ar.Best10Samples.SamplingRateHz;
				result[wave_idx].DURATION = (UInt32)ar.Best10Samples.SamplesCount;
				result[wave_idx].SAMPLE_LSB_nV = (UInt32)ar.Best10Samples.SamplesLSB_nV;

				result[wave_idx].BASELINE_FILTER = ar.Best10Samples.BaselineFilterFrequencyHz;
				result[wave_idx].BASELINE_FILTERSpecified = true;

				result[wave_idx].LOWPASS_FILTER = ar.Best10Samples.LowPassFilterFrequencyHz;
				result[wave_idx].LOWPASS_FILTERSpecified = true;
	

				result[wave_idx].NAME = (TECGLead)wave_idx;
				result[wave_idx].OFFSET = 0;
                result[wave_idx].POWERLINE_FILTER = AcFilterFreqHztoPowerLineFilter(ar.Best10Samples.AcFilterFrequencyHz);
                result[wave_idx].POWERLINE_FILTERSpecified = true;

				var lead_samples = new Byte[2 * ar.Best10Samples.SamplesCount];
				for(var i = 0; i < ar.Best10Samples.SamplesCount; ++i)
				{
					lead_samples[i * 2 + 0] = (Byte)(ar.Best10Samples.Data[i].Lead[wave_idx] & 0xFF);
					lead_samples[i * 2 + 1] = (Byte)((ar.Best10Samples.Data[i].Lead[wave_idx] & 0xFF00) >> 8);
				}

				result[wave_idx].ENCODING = ELIX_RECORDRECORDING_DATATYPICAL_CYCLEENCODING.BASE64;
				result[wave_idx].DATA = Convert.ToBase64String(lead_samples);
			}

			return result;
		}

		private static ELIX_RECORDRECORDING_DATACHANNELPOWERLINE_FILTER AcFilterFreqHztoPowerLineFilter(Int32 acf)
		{
			if (acf == 50)
			{
				return ELIX_RECORDRECORDING_DATACHANNELPOWERLINE_FILTER.Item50;
			}
			if (acf == 60)
			{
				return ELIX_RECORDRECORDING_DATACHANNELPOWERLINE_FILTER.Item60;
			}
			return ELIX_RECORDRECORDING_DATACHANNELPOWERLINE_FILTER.NONE;
		}

		static ELIX_RECORDRECORDING_DATATYPICAL_CYCLE ConvertAverages(IAnalysisResult ar)
		{
			var result = new ELIX_RECORDRECORDING_DATATYPICAL_CYCLE();

			result.BITS = ELIX_RECORDRECORDING_DATATYPICAL_CYCLEBITS.Item16;	// these are constant: FilteredSample.Lead[] is a short
			result.FORMAT = ELIX_RECORDRECORDING_DATATYPICAL_CYCLEFORMAT.SIGNED;

			result.DURATION = (UInt32)ar.Medians.Data.Length;

			result.ENCODING = ELIX_RECORDRECORDING_DATATYPICAL_CYCLEENCODING.BASE64;	// constant: encoding implemented is base64
			result.SAMPLE_FREQ = (UInt32)ar.Medians.SamplingRateHz;
			result.SAMPLE_LSB_nV = (UInt32)ar.Medians.SamplesLSB_nV;

			if(result.DURATION == 0)
			{
				return result;	// nothing else
			}

			// Fill median cycle channels
			result.TYPICAL_CYCLE_CHANNEL = new ELIX_RECORDRECORDING_DATATYPICAL_CYCLETYPICAL_CYCLE_CHANNEL[ar.Medians.LeadsCount];
			for(int wave_idx = 0; wave_idx < ar.Medians.LeadsCount; ++wave_idx)
			{
				result.TYPICAL_CYCLE_CHANNEL[wave_idx] = new ELIX_RECORDRECORDING_DATATYPICAL_CYCLETYPICAL_CYCLE_CHANNEL();

				result.TYPICAL_CYCLE_CHANNEL[wave_idx].NAME = (TECGLead)wave_idx;
				var lead_median = new Byte[2 * ar.Medians.SamplesCount];
				var i = 0;
                foreach (var s in ar.Medians.Data)
                {
                    int median_sample = s.Lead[wave_idx];
                    lead_median[i * 2 + 0] = (Byte)(median_sample & 0xFF);
                    lead_median[i * 2 + 1] = (Byte)((median_sample & 0xFF00) >> 8);
                    ++i;
                }
                result.TYPICAL_CYCLE_CHANNEL[wave_idx].DATA = Convert.ToBase64String(lead_median);
            }

			return result;
		}


		static ELIX_RECORDRECORDING_DATAGLOBAL_MEASUREMENTS ConvertMeasures(IAnalysisResult ar)
		{
			var res = new ELIX_RECORDRECORDING_DATAGLOBAL_MEASUREMENTS();

			res.AVERAGE_RR = ar.GlbMeasurements.AvgRR ?? 0;
			res.NUM_QRS = (UInt32)(ar.GlbMeasurements.NumQRS ?? 0);
			res.P_AXIS = ar.GlbMeasurements.PAx ?? 999;
			res.P_DURATION = ar.GlbMeasurements.Pdur ?? 0;
			res.P_OFFSET = ar.GlbMeasurements.POffset ?? 0;
			res.P_OFFSETSpecified = ar.GlbMeasurements.POffset.HasValue;
			res.P_ONSET = ar.GlbMeasurements.POnset ?? 0;
			res.P_ONSETSpecified = ar.GlbMeasurements.POnset.HasValue;
			res.PR_DURATION = ar.GlbMeasurements.PRdur ?? 0;

			res.Q_OFFSET = ar.GlbMeasurements.QRSOffset ?? 0;
			res.Q_OFFSETSpecified = ar.GlbMeasurements.QRSOffset.HasValue;
			res.Q_ONSET = ar.GlbMeasurements.QRSOnset ?? 0;
			res.Q_ONSETSpecified = ar.GlbMeasurements.QRSOnset.HasValue;

			res.QA_FLAG = ar.GlbMeasurements.QualityOverride ? ELIX_RECORDRECORDING_DATAGLOBAL_MEASUREMENTSQA_FLAG.Item1 : ELIX_RECORDRECORDING_DATAGLOBAL_MEASUREMENTSQA_FLAG.Item0;
			res.QRS_AXIS = ar.GlbMeasurements.QRSAx ?? 999;
			res.QRS_DURATION = ar.GlbMeasurements.QRSdur ?? 0;
			res.QT = ar.GlbMeasurements.QT ?? 0;
			res.QTC = ar.GlbMeasurements.QTcL ?? 0;
			res.QTCB = ar.GlbMeasurements.QTcB ?? 0;
			res.QTCF = ar.GlbMeasurements.QTcF ?? 0;
			res.R_PEAK = ar.GlbMeasurements.RPeak ?? 0;
			res.R_PEAKSpecified = ar.GlbMeasurements.RPeak.HasValue;
			res.T_AXIS = ar.GlbMeasurements.TAx ?? 999;
			res.T_OFFSET = ar.GlbMeasurements.TOffset ?? 0;
			res.T_OFFSETSpecified = ar.GlbMeasurements.TOffset.HasValue;
			res.VENT_RATE = ar.GlbMeasurements.VRate ?? 0;
			res.VENT_RATESpecified = ar.GlbMeasurements.VRate.HasValue;

			return res;
		}
	}
}
