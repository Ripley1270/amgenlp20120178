$key='HKCU:\SOFTWARE\Classes\Local Settings\Software\Microsoft\Windows\CurrentVersion\AppContainer\Mappings'
$appID=(Get-ChildItem $key -Recurse | ForEach-Object { Get-ItemProperty $_.pspath } | Where-Object {$_.DisplayName -like "SitePad App*"}).PSChildName
CheckNetIsolation.exe LoopbackExempt -a -p="$appID"