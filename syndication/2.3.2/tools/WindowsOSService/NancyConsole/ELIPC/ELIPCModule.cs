﻿using invivodata.Interop.WAMPC;
using invivodata.Interop.WAMPC.Communicator;
using Mortara.ELIxCtrl.API;
using Nancy.ModelBinding;
using NancyConsole.ELIPC;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace NancyConsole
{
    public class GetDeviceInfoJSONResult
    {
        public string Model;
        public string Serial;
        public string Version;
        public WamBatteryLevel BattLevel;
        public string DeviceState;
        public int? PairingCode;
        public int? AcquisitionId;
        public DateTime? AcquisitionTimeStamp;
        public DateTime? ClockResetTimeStamp;
        public AcquisitionState AcquisitionState;
    }

    public class ELIXResult
    {
        public string elix;
        public string elixNON64;
    }

    public class DownloadECGStatus
    {
        public bool downloadComplete;
        public int TransferPercentageComplete;
        public string ECGDataBase64;
        public string ElixRecord;
        public double p1H, p1W, p2H, p2W, p3H, p3W;
    }

    public class DeviceStatePost
    {
        public string DeviceState;
    }

    public class ECGDownloadResult
    {
        public int TransferPercentage;
        public string ECGBase64 = "undefined";
    }

    public class DeleteDataResult
    {
        public bool DeleteSuccessful;
    }

    public class PairingSettings
    {
        public string AcquisitionDuration;
        public string AutoPwrOffLength;
    }

    public class PairingResult
    {
        public string PairingCode;
    }

    public class ELIPCModule : Nancy.NancyModule
    {
        private static DeviceState demoDeviceState;

        private static bool _demoMode = false;
        public static int counter;
        public static bool downloading = false;
        public static int TransferCompletionPercentage;
        public static string DownloadedEcgData;
        public static string SerializedElixRecord;
        public static string SerializedElixRecordNONBase64;
        public static bool ECGDataDownloadComplete;

        private static bool _hasUTKBeenInsertedYet = false;
        private static bool _isDeviceProgrammedYet = false;
        private static bool _doesDeviceHaveDataYet = false;
        private static bool _hasEcgBeenRecordedYet = false;

        private static AcquisitionJobRecord currentPatientAcquisitionJobRecord;

        private static string replaceECGLeadData(string val)
        {
            try
            {
                XmlDocument doc = new XmlDocument();

                doc.LoadXml(val);

                var leadNodes = doc.SelectNodes("ELIX_RECORD//CHANNEL");

                foreach (var nd in leadNodes)
                {
                    ((XmlElement)nd).SetAttribute("DATA", "ChDataHere");
                }

                using (var stringWriter = new StringWriter())
                using (var xmlTextWriter = XmlWriter.Create(stringWriter))
                {
                    doc.WriteTo(xmlTextWriter);
                    xmlTextWriter.Flush();
                    return stringWriter.GetStringBuilder().ToString();
                }

            }
            catch (Exception ex)
            {
                writeLog("Exception while changing XML: " + ex.Message);
                return "";
            }
        }

        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, 1)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static void writeLog(string message)
        {
            if (Directory.Exists(@"c:\WAMPCLogs"))
            {
                string path = @"c:\WAMPCLogs\ELIPCModule.log";
                if (!File.Exists(path))
                {   // Create a file to write to.
                    using (StreamWriter sw = File.CreateText(path))
                    {
                        sw.WriteLine(message);
                    }
                }
                else
                {
                    // Open the stream and write to it.
                    File.AppendAllText(path, Environment.NewLine + message);
                }
            }
        }

        public static void TestDownloadAsync()
        {
            TransferCompletionPercentage = 0;
            ECGDataDownloadComplete = false;
            DownloadedEcgData = null;

            Task.Factory.StartNew((() =>
            {
                try
                {
                    Stopwatch stopwatch = new Stopwatch();
                    stopwatch.Start();
                    do
                    {
                        TransferCompletionPercentage = stopwatch.Elapsed.Seconds * 10;
                    } while (TransferCompletionPercentage < 100);
                    stopwatch.Stop();
                }
                catch (Exception excx)
                {
                    writeLog("Exception thrown while simulating transfer: " + excx.Message);
                    if (excx.InnerException != null)
                    {
                        writeLog("      InnerException: " + excx.InnerException.Message);
                    }
                }
            })).ContinueWith((arg =>
            {
                try
                {
                    downloading = false;

                    #region samplePDF
                    // load the sample ecg pdf and convert it to Base64 to return to the widget
                    var ecgBytes = File.ReadAllBytes(@"c:\elipcDemo\ecg_pdf_sample.pdf");
                    #endregion

                    #region sampleElix_Record
                    // load the sample elix_record
                    XmlDocument elix_xmlDoc = new XmlDocument();

                    // because they encode the XML as UTF-16 cannot use XmlDoc.Load(filepath).  Must load
                    // it with a StreamReader first...
                    using (StreamReader sr = new StreamReader(@"c:\elipcDemo\elix_record.xml", true))
                    {
                        elix_xmlDoc.Load(sr);
                    }

                    byte[] elix_bytes = Encoding.Default.GetBytes(elix_xmlDoc.OuterXml);
                    var elixBase64 = Convert.ToBase64String(elix_bytes);
                    #endregion

                    DownloadedEcgData = Convert.ToBase64String(ecgBytes);
                    SerializedElixRecord = elixBase64;

                    ECGDataDownloadComplete = true;

                    writeLog("transfer complete...elix record length = " + SerializedElixRecord.Length.ToString());
                }
                catch (Exception ex)
                {
                    writeLog("Exception while generating sample: " + ex.Message);
                    if (ex.InnerException != null)
                    {
                        writeLog("      InnerException: " + ex.InnerException.Message);
                    }
                }
            }));
        }

        public static void DownloadECGDataAsync()
        {
            writeLog("Transfer Started");
            TransferCompletionPercentage = 0;

            WamPc wamPc = new WamPc();
            DownloadedEcgData = null;
            downloading = true;
            ECGDataDownloadComplete = false;

            EventHandler<WamEventArgs> handler = (sender, wamEventArgs) =>
            {
                if (wamEventArgs.Status == WamEventArgs.WamStatus.RenderingPdf)
                {
                    writeLog("Rendering..." + wamEventArgs.Message);
                }

                if (wamEventArgs.Status == WamEventArgs.WamStatus.ECGDataRendered)
                {
                    writeLog("ECGDataRendered..." + wamEventArgs.Message);
                }

                if (wamEventArgs.Status == WamEventArgs.WamStatus.Analyzing)
                {
                    writeLog("Analyzing..." + wamEventArgs.Message);
                }

                if (wamEventArgs.Status == WamEventArgs.WamStatus.Failure)
                {
                    writeLog("Failure..." + wamEventArgs.Message);
                }

                if (wamEventArgs.Status == WamEventArgs.WamStatus.None)
                {
                    writeLog("None..." + wamEventArgs.Message);
                }

                if (wamEventArgs.Status == WamEventArgs.WamStatus.RenderingPdf)
                {
                    writeLog("RenderingPdf..." + wamEventArgs.Message);
                }

                if (wamEventArgs.Status == WamEventArgs.WamStatus.Success)
                {
                    writeLog("Success..." + wamEventArgs.Message);
                }

                if (wamEventArgs.Status == WamEventArgs.WamStatus.DownloadingEcg)
                {
                    writeLog("STATUS MESSAGE: " + wamEventArgs.Message);
                    TransferCompletionPercentage = wamEventArgs.Message.ToInt();
                    writeLog("Transfer % - " + TransferCompletionPercentage.ToString());
                }
            };

            wamPc.WamStatusUpdateEvent += handler;

            try
            {
                writeLog("TRANSFERRING WITH THE NEW ACQUISITIONJOBRECORD");
                writeLog("Gender: " + currentPatientAcquisitionJobRecord.SubjectGender);
                writeLog("DateOfBirth: " + currentPatientAcquisitionJobRecord.SubjectDOB);
                writeLog("DOBKind: " + currentPatientAcquisitionJobRecord.SubjectDOBKind);
                writeLog("ACFilter: " + currentPatientAcquisitionJobRecord.ACFilter);

                wamPc.DownloadWamEcgDataAsync((callback =>
                {
                    try
                    {
                        writeLog(Environment.NewLine+Environment.NewLine+callback.Result.Serialized_ELIX_Record);
                        var replacedData = callback.Result.Serialized_ELIX_Record;

                        writeLog("The replaced data is: " + replacedData);

                        var elixBytes = Encoding.UTF8.GetBytes(replacedData);
                        var elixBase64 = Convert.ToBase64String(elixBytes);

                        SerializedElixRecord = elixBase64;

                        ECGDataDownloadComplete = true;

                        DownloadedEcgData = Convert.ToBase64String(callback.Result.ECGData);
                    }
                    catch (Exception elixException)
                    {
                        writeLog("Exception serializing: " + elixException.Message);
                    }
                }), currentPatientAcquisitionJobRecord);
            }
            catch (Exception e)
            {
                writeLog("Exception downloading: " + e.Message);
            }
        }

        public ELIPCModule()
        {
            After.AddItemToEndOfPipeline((ctx) =>
            {
                ctx.Response.Headers.Add("Access-Control-Allow-Origin", "*");
                ctx.Response.Headers.Add("Access-Control-Allow-Methods", "POST,GET");
                ctx.Response.Headers.Add("Access-Control-Allow-Headers", "Accept, Origin, Content-type");
            }
            );

            Get["/SetDemoMode/{val}"] = parameters =>
            {
                //this is set statically...if you want to change this mid session, make sure you make a call to 
                try
                {

                    if (((string)parameters.val == "true"))
                    {
                        _demoMode = true;
                        demoDeviceState = DeviceState.NoUTK;
                        _isDeviceProgrammedYet = false;
                        _doesDeviceHaveDataYet = false;
                        _hasEcgBeenRecordedYet = false;

                        writeLog("switching to DEMO MODE");

                    }
                    else
                    {
                        _demoMode = false;

                        writeLog("switching to HARDWARE MODE");
                    }

                    return null;
                }
                catch (Exception ex)
                {
                    writeLog("Exception thrown switching to demo mode: " + ex.Message);
                    if (ex.InnerException != null)
                    {
                        writeLog("    InnerException: " + ex.InnerException.Message);
                    }

                    return 202;
                }
            };

            Post["/SetDeviceState"] = _ =>
            {
                try
                {
                    var id = Request.Body;
                    var length = Request.Body.Length;
                    var data = new byte[length];
                    id.Read(data, 0, (int)length);

                    var body = Encoding.Default.GetString(data);

                    var eqIndex = body.IndexOf('=');
                    var newState = body.Substring(eqIndex + 1);

                    writeLog("Body is: " + body);
                    writeLog("New State is: " + newState);

                    // if the user has clicked the POWER ON button in demo mode, we have to keep track
                    // of what the previous state was...and update to the new state in a linear fashion.
                    if (newState == "On")
                    {
                        // THESE ONLY APPLY TO DEMO MODE

                        // if the device is turned on and the UTK hasn't been inserted yet,
                        // we know that the UTK must have been inserted.  Flag it.
                        if (!_hasUTKBeenInsertedYet)
                        {
                            // the utk is inserted.  Simulate that the device is OFF
                            _hasUTKBeenInsertedYet = true;
                            demoDeviceState = DeviceState.Off;

                            writeLog("Turning the device off after UTK was inserted");

                            return 200;
                        }

                        if (!_isDeviceProgrammedYet)
                        {
                            // the utk has already been inserted and the device is in an off
                            // state.  They've clicked the "Power On" button in demo mode.  Turn it on and
                            // program it.
                            _isDeviceProgrammedYet = true;
                            demoDeviceState = DeviceState.Empty;

                            writeLog("Device was just programmed...setting it to EMPTY");
                            return 200;
                        }

                        // if the UTK is in and the device is programmed, check to see if the ecg has been
                        // recorded yet.  If it hasn't been recorded yet, the set the simulated device into a
                        // 'Ready' state.  This will flag the widget to tell the user to 'Press the ECG button' 
                        if (!_hasEcgBeenRecordedYet)
                        {
                            demoDeviceState = DeviceState.Ready;

                            writeLog("Device is ready for an ECG.  Set the state to READY");
                            return 200;
                        }

                        if (_hasEcgBeenRecordedYet)
                        {
                            demoDeviceState = DeviceState.HasData;

                            writeLog("Device was turned on.  It has data...set state to HASDATA");
                            return 200;
                        }
                    }

                    if (newState == "Off")
                    {
                        if ((_hasUTKBeenInsertedYet && _isDeviceProgrammedYet) && (!_hasEcgBeenRecordedYet))
                        {
                            // the utk is in, the device is programmed, the ecg must have been recorded.
                            _hasEcgBeenRecordedYet = true;

                            writeLog("Device was turned off without an ECG having been recorded.  The ECG is now being recorded");
                        }

                        demoDeviceState = DeviceState.Off;
                        return 200;
                    }

                    demoDeviceState = (DeviceState)Enum.Parse(typeof(DeviceState), newState);

                    writeLog("Setting device by default to: " + newState);
                    return 200;
                }
                catch (Exception ex)
                {
                    writeLog("Exception thrown setting device state: " + ex.Message);
                    if (ex.InnerException != null)
                    {
                        writeLog("    InnerException: " + ex.InnerException.Message);
                    }

                    return 404;
                }
            };

            Get["/GetDeviceInformation"] = parameters =>
            {
                try
                {
                    if (_demoMode)
                    {
                        //writeLog("DEMO MODE!");
                        GetDeviceInfoJSONResult rslt = new GetDeviceInfoJSONResult();
                        rslt.DeviceState = demoDeviceState.ToString();
                        return new JSONHelper<GetDeviceInfoJSONResult>(rslt).ToJSON();
                    }
                    else
                    {
                        WamPc wamPc = new WamPc();

                        var devInfo = wamPc.GetDeviceInformation().Result;
                        GetDeviceInfoJSONResult rslt = new GetDeviceInfoJSONResult();

                        if (devInfo == null)
                        {
                            writeLog("devInfo is null");
                        }

                        rslt.AcquisitionId = devInfo.AcquisitionId;
                        rslt.AcquisitionState = devInfo.AcquisitionState;
                        rslt.AcquisitionTimeStamp = devInfo.AcquisitionTimeStamp;
                        rslt.BattLevel = devInfo.BattLevel;
                        rslt.ClockResetTimeStamp = devInfo.ClockResetTimeStamp;
                        rslt.DeviceState = devInfo.DevState.ToString();
                        rslt.Model = devInfo.Model;
                        rslt.PairingCode = devInfo.PairingCode;
                        rslt.Serial = devInfo.Serial;

                        return new JSONHelper<GetDeviceInfoJSONResult>(rslt).ToJSON();
                    }
                }
                catch (Exception ex)
                {
                    writeLog("Exception thrown in GetDeviceInformation: " + ex.Message);
                    if (ex.InnerException != null)
                    {
                        writeLog("         InnerException is: " + ex.InnerException.Message);
                    }
                    writeLog("                   StackTrace: " + ex.StackTrace);
                    return null;
                }
            };

            Get["/DeleteDeviceData"] = _ =>
            {
                var dataResult = new DeleteDataResult();

                if (_demoMode)
                {
                    var rslt = new DeleteDataResult() { DeleteSuccessful = true };
                    demoDeviceState = DeviceState.Off;
                    return new JSONHelper<DeleteDataResult>(rslt).ToJSON();
                }
                else
                {
                    WamPc wamPc = new WamPc();

                    var communicationResult = wamPc.DeleteDeviceMemory();
                    var exc = communicationResult.Exception;

                    if (communicationResult.Exception != null)
                    {
                        writeLog("Result: " + communicationResult.Exception.Message);
                    }
                    else
                    {
                        writeLog("Deletion occurred...no exception.");
                    }

                    if (exc != null)
                    {
                        return new JSONHelper<DeleteDataResult>(dataResult).ToJSON();
                    }
                    else
                    {
                        var rslt = new DeleteDataResult() { DeleteSuccessful = communicationResult.Result };
                        return new JSONHelper<DeleteDataResult>(rslt).ToJSON();
                    }
                }
            };

            Get["/BeginDownloadingECGData"] = _ =>
            {
                try
                {
                    if (_demoMode)
                    {
                        TestDownloadAsync();
                        return null;
                    }
                    else
                    {
                        DownloadECGDataAsync();
                        return new JSONHelper<DownloadECGStatus>(new DownloadECGStatus() { downloadComplete = false }).ToJSON();
                    }
                }
                catch (Exception exc)
                {
                    writeLog("Exception thrown in DownloadECG: " + exc.Message);
                    return false;
                }
            };

            Get["/ElixRecord"] = _ =>
            {
                writeLog("Elix record = " + SerializedElixRecord);

                var rslt = new ELIXResult();
                rslt.elix = SerializedElixRecord;

                return new JSONHelper<ELIXResult>(rslt).ToJSON();
            };

            Get["/QueryECGDownloadStatus"] = _ =>
            {
                try
                {
                    var rslt = new DownloadECGStatus();

                    rslt.downloadComplete = ECGDataDownloadComplete;
                    rslt.TransferPercentageComplete = TransferCompletionPercentage;

                    rslt.ECGDataBase64 = DownloadedEcgData;

                    return new JSONHelper<DownloadECGStatus>(rslt).ToJSON();
                }
                catch (Exception ex)
                {
                    writeLog("Exception thrown in GetDeviceInformation: " + ex.Message);
                    if (ex.InnerException != null)
                    {
                        writeLog("         InnerException is: " + ex.InnerException.Message);
                    }
                    writeLog("                   StackTrace: " + ex.StackTrace);

                    var rslt = new DownloadECGStatus();

                    rslt.downloadComplete = ECGDataDownloadComplete;
                    rslt.TransferPercentageComplete = TransferCompletionPercentage;

                    rslt.ECGDataBase64 = "error";

                    return new JSONHelper<DownloadECGStatus>(rslt).ToJSON();
                }
            };

            Post["/ProgramDevice"] = parameters =>
            {
                try
                {
                    var id = Request.Body;
                    long length = Request.Body.Length;
                    byte[] data = new byte[length];
                    id.Read(data, 0, (int)length);
                    string body = Encoding.Default.GetString(data);

                    writeLog("Body Is: " + body);

                    JSONHelper<AcquisitionPOSTObject> helper = new JSONHelper<AcquisitionPOSTObject>();
                    var newObj = helper.FromJSON(body);

                    if (newObj != null)
                    {
                        writeLog("newObj is NOT NULL");
                        writeLog("AcquisitionLengthInMilliseconds: " + newObj.AcquisitionLengthInMilliseconds);
                        writeLog("Gender: " + newObj.Gender);
                        writeLog("SponsorID: " + newObj.SponsorID);
                        writeLog("SponsorName: " + newObj.SponsorName);
                        writeLog("ProtocolID: " + newObj.ProtocolID);
                        writeLog("ProtocolName: " + newObj.ProtocolName);
                        writeLog("SiteID: " + newObj.SiteID);
                        writeLog("SiteName: " + newObj.SiteName);
                        writeLog("SubjectID: " + newObj.SubjectID);
                        writeLog("SubjectInitials: " + newObj.SubjectInitials);
                        writeLog("SubjectDOB: " + newObj.SubjectDOB);
                        writeLog("SubjectDOBKind: " + newObj.SubjectDOBKind);
                        writeLog("Gender: " + newObj.Gender);
                        writeLog("VisitSubjectBP: " + newObj.VisitSubjectBP);
                        writeLog("VisitSubjectWeight: " + newObj.VisitSubjectWeight);
                        writeLog("InvestigatorID: " + newObj.InvestigatorID);
                        writeLog("InvestigatorName: " + newObj.InvestigatorName);
                        writeLog("InvestigatorAddress: " + newObj.InvestigatorAddress);
                        writeLog("VisitID: " + newObj.VisitID);
                        writeLog("VisitName: " + newObj.VisitName);
                        writeLog("VisitAbbreviation: " + newObj.VisitAbbreviation);
                        writeLog("UserID: " + newObj.UserID);
                        writeLog("UserName: " + newObj.UserName);
                    }
                    else
                    {
                        writeLog("newObj IS NULL");
                    }

                    WamPc wamPc = new WamPc();
                    currentPatientAcquisitionJobRecord = new AcquisitionJobRecord()
                    {
                        AcquisitionLengthInMilliseconds = newObj.AcquisitionLengthInMilliseconds.ToInt(),
                        ACFilter = 50,
                        JobID = AcquisitionJobRecord.NewID,
                        SponsorID = newObj.SponsorID.ToString(),
                        SponsorName = newObj.SponsorName,
                        ProtocolID = newObj.ProtocolID.ToString(),
                        ProtocolName = newObj.ProtocolName,
                        SiteID = newObj.SiteID,
                        SiteName = newObj.SiteName,
                        SubjectID = newObj.SubjectID,
                        SubjectInitials = newObj.SubjectInitials,
                        SubjectDOB = newObj.SubjectDOB,
                        SubjectDOBKind = newObj.SubjectDOBKind,
                        SubjectGender = newObj.Gender,
                        VisitSubjectBP = newObj.VisitSubjectBP,
                        VisitSubjectWeight = newObj.VisitSubjectWeight.ToString(),
                        InvestigatorID = newObj.InvestigatorID.ToString(),
                        InvestigatorName = newObj.InvestigatorName,
                        InvestigatorAddress = newObj.InvestigatorAddress,
                        VisitID = newObj.VisitID.ToString(),
                        VisitName = newObj.VisitName,
                        VisitAbbreviation = newObj.VisitAbbreviation,
                        UserID = newObj.UserID.ToString(),
                        UserName = newObj.UserName
                    };

                    WampcCommunicationResult<bool> communicationResult = wamPc.ProgramDevice(currentPatientAcquisitionJobRecord.JobID);
                    //};
                }
                catch (Exception ex)
                {
                    writeLog("Exception thrown in ProgramDevice: " + ex.Message);
                    return null;
                }
                return null;
            };

            Post["/PairDeviceToUTKDongle"] = parameters =>
            {
                try
                {
                    var rslt = new PairingResult();
                    rslt.PairingCode = "ERROR";

                    var id = this.Request.Body;
                    long length = this.Request.Body.Length;
                    byte[] data = new byte[length];
                    id.Read(data, 0, (int)length);
                    string body = System.Text.Encoding.Default.GetString(data);
                    var p = body.Split('&')
                        .Select(s => s.Split('='))
                        .ToDictionary(k => k.ElementAt(0), v => v.ElementAt(1));

                    var maxDur = p["MaxDuration"];
                    var minDur = p["MinDuration"];
                    var testAcq = p["TestAcq"];
                    var selfPowerOff = p["SelfPowerOff"];

                    writeLog("PairDeviceToUTKDongle has been called with the following settings: " +
                        Environment.NewLine +
                        "   MaxDuration - " + maxDur + Environment.NewLine +
                        "   MinDuration - " + minDur + Environment.NewLine +
                        "   TestAcq - " + testAcq + Environment.NewLine +
                        "   SelfPowerOff - " + selfPowerOff);

                    WamPc wamPc = new WamPc();
                    WamPairingSettings pairingSettings = new WamPairingSettings();
                    pairingSettings.MaxDuration = maxDur.ToInt();
                    pairingSettings.MinDuration = minDur.ToInt();
                    pairingSettings.TestAcq = testAcq.ToInt();
                    pairingSettings.SelfPowerOff = selfPowerOff.ToInt();

                    writeLog("Settings have been established...making the call to PairDeviceToUtkDongle");

                    WampcCommunicationResult<int?> utkDongle =
                        wamPc.PairDeviceToUtkDongle(pairingSettings, false);

                    writeLog("Call to PairDeviceToUtkDongle has returned");

                    int? result;
                    int num;
                    if (utkDongle != null)
                    {
                        result = utkDongle.Result;
                        num = result.HasValue ? 1 : 0;
                    }
                    else
                        num = 0;
                    if (num == 0)
                    {
                        writeLog("Pairing failed - Unable to pair WAMPC with the UTK.  The result of pairing attempt is NULL.");
                        return new JSONHelper<PairingResult>(rslt).ToJSON();
                    }
                    Thread.Sleep(1000);

                    writeLog("Getting device information after pairing");

                    WampcCommunicationResult<WampcDeviceInformation> deviceInformation = wamPc.GetDeviceInformation();

                    if (deviceInformation != null)
                    {
                        if (string.IsNullOrEmpty(deviceInformation.Result.Serial))
                        {
                            writeLog("WAMPCApplet-->The WAM returned an empty serial number.  This warrants a call to Mortara to figure out what the deal is.  This should really never happen.");
                            return new JSONHelper<PairingResult>(rslt).ToJSON();
                        }

                        result = utkDongle.Result;
                        string str = result.ToString();

                        rslt.PairingCode = utkDongle.Result.ToString();

                        return new JSONHelper<PairingResult>(rslt).ToJSON();
                    }
                }
                catch (Exception ex)
                {
                    writeLog("Exception thrown in PairDeviceToUTKDongle: " + ex.Message);
                    return null;
                }
                return null;
            };

            Get["/FullECGTest"] = _ =>
            {
                var flname = @"c:\elipcDemo\WAMSAMPLE.pdf";
                var flBytes = File.ReadAllBytes(flname);
                var rslt = new DownloadECGStatus();

                rslt.downloadComplete = true;
                rslt.TransferPercentageComplete = 100;
                rslt.ECGDataBase64 = Convert.ToBase64String(flBytes);

                return new JSONHelper<DownloadECGStatus>(rslt).ToJSON();
            };
        }
    }

    internal class AcquisitionPOSTObject
    {
        public int AcquisitionLengthInMilliseconds;
        public int ACFilter;
        public int SponsorID;
        public string SponsorName;
        public int ProtocolID;
        public string ProtocolName;
        public string SiteID;
        public string SiteName;
        public string SubjectID;
        public string SubjectInitials;
        public string SubjectDOB;
        public string SubjectDOBKind;
        public string Gender;
        public string VisitSubjectBP;
        public int VisitSubjectWeight;
        public int InvestigatorID;
        public string InvestigatorName;
        public string InvestigatorAddress;
        public int VisitID;
        public string VisitName;
        public string VisitAbbreviation;
        public int UserID;
        public string UserName;
    }
}