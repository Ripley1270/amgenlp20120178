﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NancyConsole.log
{
    class Constants
    {
        public const string TagSource = "ERT Settings Service";
        public const string TagLogs = "ERT Settings Logs";
    }
}
