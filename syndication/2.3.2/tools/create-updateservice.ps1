﻿if (Get-Service "ERTService" -ErrorAction SilentlyContinue) 
{
     Stop-Service -Name "ERTService" -Force
     & sc.exe delete "ERTService"
} 
If (Test-Path "c:\ERTService") {
        Remove-Item -Force -Recurse "C:\ERTService"
}
New-Item -ItemType Directory -Force -Path C:\ERTService
Move-Item c:\temp\release\* c:\ERTService
& c:\Windows\Microsoft.NET\Framework\v4.0.30319\InstallUtil.exe c:\ERTService\ERTService.exe
Start-Service "ERTService"