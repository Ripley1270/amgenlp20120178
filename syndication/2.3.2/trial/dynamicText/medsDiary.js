import dynamicText from 'core/dynamicText';
import * as Helpers from 'core/Helpers';
import * as utilities from 'core/utilities';
/**
 * Get the response for MEDS_Q_1 from Meds current diary.
 */
dynamicText.add({
    id          : 'getPills',
    evaluate    : () => {
        let response = Helpers.getResponses('MEDS_Q_1')[0].response;
        if(response === '0'){
            return '1/2';
        }else{
            return response;
        }
    },
    screenshots : {
        values : ['1/2', '2']
    }
});
/**
 * Return the string based on the response to MEDS_Q_1.
 * @param {string} namespace - The namespace to use in translating strings.
 * @returns {object} dynamic text settings.
 */
const pills = namespace => {
    return {
        id          : 'pills',
        evaluate    : () => {
            let response = Helpers.getResponses('MEDS_Q_1')[0].response;
            let stringKey = response === '2' ? 'PILLS' : 'PILL';
            return LF.getStrings(stringKey, $.noop, {namespace});
        },
        screenshots : {
            keys : {
                options : {namespace},
                pill    : 'PILL',
                pills   : 'PILLS'
            }
        }
    };
};
// Sitepad has it's own meds namespace.
let pillsNamespace = 'P_Meds';
// utilities.isSitePad throws an error in unit tests, ignore it.
try{
    if(utilities.isSitePad()){
        pillsNamespace = `${pillsNamespace}_SitePad`;
    }
    // eslint-disable-next-line no-empty
}catch(err){
}
dynamicText.add(pills(pillsNamespace));
/**
 * Get the image to display.
 */
dynamicText.add({
    id          : 'medsImage',
    evaluate    : () => '<img src=\"media/images/valid.png\" height="15px/">',
    screenshots : {
        values : ['<img src=\"media/images/valid.png\" height="15px/">']
    }
});
dynamicText.add({
    id          : 'getMed1',
    evaluate    : () => {
        return LF.Utilities.medDynamicText('1');
    },
    screenshots : {
        values : ['Medication1']
    }
});
dynamicText.add({
    id          : 'getMed2',
    evaluate    : () => {
        return LF.Utilities.medDynamicText('2');
    },
    screenshots : {
        values : ['Medication2']
    }
});
dynamicText.add({
    id          : 'getMed3',
    evaluate    : () => {
        return LF.Utilities.medDynamicText('3');
    },
    screenshots : {
        values : ['Medication3']
    }
});
dynamicText.add({
    id          : 'getMed4',
    evaluate    : () => {
        return LF.Utilities.medDynamicText('4');
    },
    screenshots : {
        values : ['Medication4']
    }
});
dynamicText.add({
    id          : 'getMed5',
    evaluate    : () => {
        return LF.Utilities.medDynamicText('5');
    },
    screenshots : {
        values : ['Medication5']
    }
});
dynamicText.add({
    id          : 'getMed6',
    evaluate    : () => {
        return LF.Utilities.medDynamicText('6');
    },
    screenshots : {
        values : ['Medication6']
    }
});
dynamicText.add({
    id          : 'getMed7',
    evaluate    : () => {
        return LF.Utilities.medDynamicText('7');
    },
    screenshots : {
        values : ['Medication7']
    }
});
dynamicText.add({
    id          : 'getMed8',
    evaluate    : () => {
        return LF.Utilities.medDynamicText('8');
    },
    screenshots : {
        values : ['Medication8']
    }
});
dynamicText.add({
    id          : 'getMed9',
    evaluate    : () => {
        return LF.Utilities.medDynamicText('9');
    },
    screenshots : {
        values : ['Medication9']
    }
});
dynamicText.add({
    id          : 'getOtherMed',
    evaluate    : () => {
        return LF.getStrings('OtherMed');
    },
    screenshots : {
        getValues : () => {
            return LF.getStrings('OtherMed')
                .then(string => string);
        }
        //values : ['Other Medication']
    }
});
dynamicText.add({
    id          : 'getMedUnits',
    evaluate    : () => {
        let ig = LF.Data.Questionnaire.screens[LF.router.view().screen].get('id').indexOf('DD404') !== -1 ? 'HeadacheMed' : 'AURA';
        return LF.getStrings(LF.Utilities.getSelectedMedicationInfo(ig).unitsSingle);
    },
    screenshots : {
        getValues : () => {
            return LF.getStrings('MLS_INJ')
                .then(string => string);
        }
    }
});
dynamicText.add({
    id          : 'getOngoingHeadacheDate',
    evaluate    : () => {
        return LF.Utilities.getOngoingHeadaches()
            .then((ongoingHeadaches) => {
                let ongoingHeadache = _.last(ongoingHeadaches);
                if(ongoingHeadache){
                    return LF.DynamicText.formatDateTime(new Date(ongoingHeadache.get('startDate')));
                }else{
                    return ('-');
                }
            });
    },
    screenshots : {
        getValues : () => {
            return LF.DynamicText.formatDateTime(new Date());
        }
    }
});
dynamicText.add({
    id          : 'getHeadacheHourDuration',
    evaluate    : () => {
        return LF.Utilities.getOngoingHeadaches()
            .then((ongoingHeadaches) => {
                let ongoingHeadache = _.last(ongoingHeadaches),
                    now             = new Date();
                if(ongoingHeadache){
                    return Math.floor((now.getTime() - new Date(ongoingHeadache.get('startDate')).getTime()) / (1000 * 60 * 60));
                }else{
                    return ('-');
                }
            });
    },
    screenshots : {
        getValues : () => {
            return 999;
        }
    }
});
dynamicText.add({
    id          : 'getHeadacheMinDuration',
    evaluate    : () => {
        return LF.Utilities.getOngoingHeadaches()
            .then((ongoingHeadaches) => {
                let ongoingHeadache = _.last(ongoingHeadaches),
                    now             = new Date(),
                    hours;
                if(ongoingHeadache){
                    hours = Math.floor((now.getTime() - new Date(ongoingHeadache.get('startDate')).getTime()) / (1000 * 60 * 60));
                    return Math.floor(Math.floor((now.getTime() - new Date(ongoingHeadache.get('startDate')).getTime()) / (1000 * 60)) - 60 * hours);
                }else{
                    return ('-');
                }
            });
    },
    screenshots : {
        getValues : () => {
            return 59;
        }
    }
});
dynamicText.add({
    id          : 'HeadacheDurationHours',
    evaluate    : () => {
        return LF.Utilities.getHeadacheDuration()
            .then(headacheDuration => {
                return headacheDuration.time[0].toString();
            });
    },
    screenshots : {
        getValues : () => {
            return 72;
        }
    }
});
dynamicText.add({
    id          : 'HeadacheDurationMinutes',
    evaluate    : () => {
        return LF.Utilities.getHeadacheDuration()
            .then(headacheDuration => {
                return headacheDuration.time[1].toString();
            });
    },
    screenshots : {
        getValues : () => {
            return 59;
        }
    }
});
dynamicText.add({
    id          : 'HeadacheStartTime',
    evaluate    : () => {
        return LF.Utilities.getOngoingHeadaches()
            .then((ongoingHeadaches) => {
                let ig              = 'Headache',
                    igr             = LF.Data.Questionnaire.getCurrentIGR(ig),
                    ongoingHeadache = _.last(ongoingHeadaches),
                    startTime       = _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('HEDBEG1S', igr));
                if(ongoingHeadache){
                    return LF.DynamicText.formatDateTime(new Date(ongoingHeadache.get('startDate')));
                }else{
                    if(startTime){
                        return LF.DynamicText.formatDateTime(new Date(startTime.get('response')));
                    }else{
                        return ('-');
                    }
                }
            });
    },
    screenshots : {
        getValues : () => {
            return LF.DynamicText.formatDateTime(new Date());
        }
    }
});
dynamicText.add({
    id          : 'HeadacheEndTime',
    evaluate    : () => {
        let ig      = 'Headache',
            igr     = LF.Data.Questionnaire.getCurrentIGR(ig),
            endDate = _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('ENDHED1S', igr)) || _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('FELSLP1S', igr));
        if(endDate){
            return LF.DynamicText.formatDateTime(new Date(endDate.get('response')));
        }else{
            return ('-');
        }
    },
    screenshots : {
        getValues : () => {
            return LF.DynamicText.formatDateTime(new Date());
        }
    }
});
dynamicText.add({
    id          : 'headacheOnlyMinutes',
    evaluate    : () => {
        return LF.Utilities.getOngoingHeadaches()
            .then((ongoingHeadaches) => {
                let ig              = 'Headache',
                    igr             = LF.Data.Questionnaire.getCurrentIGR(ig),
                    ongoingHeadache = _.last(ongoingHeadaches),
                    startDate       = _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('HEDBEG1S', igr)),
                    endDate         = _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('ENDHED1S', igr)) || _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('FELSLP1S', igr));
                if(startDate){
                    startDate = new Date(startDate.get('response'));
                }else if(ongoingHeadache){
                    startDate = new Date(ongoingHeadache.get('startDate'));
                }else{
                    startDate = new Date('11 Sep 2011 00:00:00');
                }
                endDate = endDate ? new Date(endDate.get('response')) : startDate;
                return Math.floor((endDate.getTime() - startDate.getTime()) / (1000 * 60));
            });
    },
    screenshots : {
        getValues : () => {
            return 999;
        }
    }
});
dynamicText.add({
    id          : 'headacheNewStart',
    evaluate    : () => {
        return LF.Utilities.getOngoingHeadaches()
            .then((ongoingHeadaches) => {
                let ig              = 'Headache',
                    igr             = LF.Data.Questionnaire.getCurrentIGR(ig),
                    ongoingHeadache = _.last(ongoingHeadaches),
                    startDate       = _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('HEDBEG1S', igr));
                if(startDate){
                    startDate = new Date(startDate.get('response'));
                }else if(ongoingHeadache){
                    startDate = new Date(ongoingHeadache.get('startDate'));
                }else{
                    startDate = new Date('11 Sep 2011 00:00:00');
                }
                return LF.DynamicText.formatDateTime(startDate);
            });
    },
    screenshots : {
        getValues : () => {
            return LF.DynamicText.formatDateTime(new Date());
        }
    }
});
dynamicText.add({
    id          : 'headacheNewEnd',
    evaluate    : () => {
        let ig      = 'Headache',
            igr     = LF.Data.Questionnaire.getCurrentIGR(ig),
            endDate = _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('ENDHED1S', igr)) || _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('FELSLP1S', igr));
        if(endDate){
            endDate = new Date(endDate.get('response'));
        }else{
            endDate = new Date('11 Sep 2011 00:00:00');
        }
        return LF.DynamicText.formatDateTime(endDate);
    },
    screenshots : {
        getValues : () => {
            return LF.DynamicText.formatDateTime(new Date());
        }
    }
});
dynamicText.add({
    id          : 'getConflictingRanges',
    evaluate    : () => {
        return LF.Utilities.getOverlappingHeadaches()
            .then((overlappingHeadache) => {
                if(overlappingHeadache){
                    return `${LF.DynamicText.formatDateTime(new Date(overlappingHeadache.startDate))} - ${LF.DynamicText.formatDateTime(new Date(overlappingHeadache.endDate))}`;
                }else{
                    return ('-');
                }
            });
    },
    screenshots : {
        getValues : () => {
            return `${LF.DynamicText.formatDateTime(new Date())} - ${LF.DynamicText.formatDateTime(new Date())}`;
        }
    }
});
dynamicText.add({
    id          : 'getSleepDuration',
    evaluate    : () => {
        let durations      = LF.Utilities.getSleepDuration(),
            stringsToFetch = {
                minutes    : 'MINUTES_FORMAT',
                hourFormat : 'HOUR_FORMAT'
            };
        return LF.getStrings(stringsToFetch)
            .then((strings) => {
                if(durations.minutes < 15){
                    return strings.minutes.format(durations.minutes);
                }else{
                    return strings.hourFormat.format(durations.time[0], durations.time[1]);
                }
            });
    },
    screenshots : {
        getValues : () => {
            return LF.getStrings(['MINUTES_FORMAT', 'HOUR_FORMAT'])
                .then(string => string);
        }
    }
});
dynamicText.add({
    id          : 'getTimeFellAsleep',
    evaluate    : () => {
        let ig         = 'Headache',
            igr        = LF.Data.Questionnaire.getCurrentIGR(ig),
            fellAsleep = _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('FELSLP1S', igr));
        if(fellAsleep){
            return LF.DynamicText.formatDateTime(new Date(fellAsleep.get('response')));
        }else{
            return ('-');
        }
    },
    screenshots : {
        getValues : () => {
            return LF.DynamicText.formatDateTime(new Date());
        }
    }
});
dynamicText.add({
    id          : 'getTimeWokeUp',
    evaluate    : () => {
        let ig     = 'Headache',
            igr    = LF.Data.Questionnaire.getCurrentIGR(ig),
            wokeUp = _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('TIMAWK1S', igr));
        if(wokeUp){
            return LF.DynamicText.formatDateTime(new Date(wokeUp.get('response')));
        }else{
            return ('-');
        }
    },
    screenshots : {
        getValues : () => {
            return LF.DynamicText.formatDateTime(new Date());
        }
    }
});
dynamicText.add({
    id          : 'getOverlappingMedTime',
    evaluate    : () => {
        let ig = LF.Data.Questionnaire.screens[LF.router.view().screen].get('id').indexOf('DD403') !== -1 ? 'HeadacheMed' : 'AURA';
        return LF.Utilities.getOverlappingMedication(ig)
            .then((overlappingMedication) => {
                if(overlappingMedication){
                    return LF.DynamicText.formatDateTime(new Date(overlappingMedication.dateTaken));
                }else{
                    return ('-');
                }
            });
    },
    screenshots : {
        getValues : () => {
            return LF.DynamicText.formatDateTime(new Date());
        }
    }
});
