import Data from 'core/Data';
import Answers from 'core/collections/Answers';
import dynamicText from 'core/dynamicText';
/**
 * Create the rows for the table using the given answers.
 * @param {array} answers - Array of answers.
 * @returns {string} The string value of the created rows.
 */
const createRows = (answers) => {
    const answerLen = answers.length;
    let retString = '';
    for(let i = 0; i < answerLen; i += 1){
        retString += `<tr><td>${answers.at(i).get('question_id')}</td><td>${answers.at(i).get('response')}</td>`;
    }
    return retString;
};
/**
 * Create the table from the given answers.
 * @param {array} answers - Array of answers.
 * @returns {string} The string value of the created table.
 */
const createTable = (answers) => {
    return `
        <table border="1" cellpadding="0,3">
            <tr> <th>IT</th> <th>Answers</th> </tr>
            ${createRows(answers)}
        </table>
    `;
};
/**
 * Generates the review screen text for the radiobutton matrix test diary.
 */
dynamicText.add({
    id          : 'getMatrixRadioButtonAnswers',
    evaluate    : () => createTable(Data.Questionnaire.answers),
    screenshots : {
        // Answers collection fails if constructed before database is set up.
        //  This cannot use the static "values".
        getValues () {
            return [
                createTable(new Answers([
                    {
                        question_id : 'TEST_QUESTION_1',
                        response    : 1
                    }, {
                        question_id : 'TEST_QUESTION_2',
                        response    : 5
                    }
                ]))
            ];
        }
    }
});
