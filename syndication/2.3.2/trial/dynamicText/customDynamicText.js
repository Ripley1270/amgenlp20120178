/**
 * Created by jonathan.carr on 31-Jul-17.
 */
import dynamicText from 'core/dynamicText';
dynamicText.add({
    id          : 'getSubjectNumber',
    evaluate    : () => {
        return LF.Data.Questionnaire.subject.get('subject_number');
    },
    screenshots : {
        // Answers collection fails if constructed before database is set up.
        //  This cannot use the static 'values'.
        values : ['999999']
    }
});
dynamicText.add({
    id          : 'getSelectedVisit',
    evaluate    : () => {
        let selectedVisit = _.last(LF.Data.Questionnaire.queryAnswersByIT('VST1L')),
            stringObj     = {
                1 : 'WEEK_188',
                2 : 'WEEK_192',
                3 : 'WEEK_212',
                4 : 'WEEK_216',
                5 : 'WEEK_236',
                6 : 'WEEK_240',
                7 : 'WEEK_260',
                8 : 'WEEK_264',
                9 : 'WEEK_268'
            };
        selectedVisit = selectedVisit ? selectedVisit.get('response') : 1020;
        return LF.getStrings(stringObj[selectedVisit]);
    },
    screenshots : {
        // Answers collection fails if constructed before database is set up.
        //  This cannot use the static 'values'.
        values : [
            'WEEK_188',
            'WEEK_192',
            'WEEK_212',
            'WEEK_216',
            'WEEK_236',
            'WEEK_240',
            'WEEK_260',
            'WEEK_264',
            'WEEK_268'
        ]
    }
});
dynamicText.add({
    id          : 'getLastVisit',
    evaluate    : () => {
        return LF.Utilities.getCurrentVisitPromise()
            .then((lastVisit) => {
                let stringObj = {
                    0 : 'noVistSelected',
                    1 : 'WEEK_188',
                    2 : 'WEEK_192',
                    3 : 'WEEK_212',
                    4 : 'WEEK_216',
                    5 : 'WEEK_236',
                    6 : 'WEEK_240',
                    7 : 'WEEK_260',
                    8 : 'WEEK_264',
                    9 : 'WEEK_268'
                };
                return LF.getStrings(stringObj[lastVisit ? lastVisit.get('visit') : 0]);
            });
    },
    screenshots : {
        // Answers collection fails if constructed before database is set up.
        //  This cannot use the static 'values'.
        values : [
            'noVistSelected',
            'WEEK_188',
            'WEEK_192',
            'WEEK_212',
            'WEEK_216',
            'WEEK_236',
            'WEEK_240',
            'WEEK_260',
            'WEEK_264',
            'WEEK_268'
        ]
    }
});
dynamicText.add({
    id          : 'checkCountry',
    evaluate    : () => {
        let thisUser = LF.security.activeUser,
            strings  = ['PGIAL_REMINDER_STANDARD', 'PGIAL_REMINDER_KOREAN'],
            country;
        if(thisUser){
            country = thisUser.get('language').split('-')[1];
            //TODO: Add Argentina and Mexico...and Russia(?)
            return LF.getStrings((country == 'KR' || country == 'AU') ? strings[1] : strings[0]);
        }else{
            return LF.getStrings(strings[0]);
        }
    },
    screenshots : {
        values : [
            'PGIAL_REMINDER_STANDARD',
            'PGIAL_REMINDER_KOREAN'
        ]
    }
});
dynamicText.add({
    id          : 'AuraMedRange',
    evaluate    : () => {
        if(LF.Utilities.dateDiffInDays(new Date(), new Date(LF.Data.Questionnaire.subject.get('activationDate'))) === 0){
            return LF.DynamicText.formatDateTime(new Date(LF.Data.Questionnaire.subject.get('activationDate')));
        }else{
            return LF.Utilities.getCollection('LastDiaries')
                .then((lastDiaries) => {
                    let now        = new Date(),
                        dailyDiary = _.find(lastDiaries, (diary) => {
                            return diary.get('questionnaire_id') === 'DailyDiary'
                                && LF.Utilities.dateDiffInDays(now, new Date(diary.get('lastStartedDate'))) <= 1;
                        });
                    return dailyDiary ? LF.DynamicText.formatDateTime(new Date(dailyDiary.get('lastStartedDate'))) : LF.getStrings('MIDNIGHT');
                });
        }
    },
    screenshots : {
        getValues : () => {
            return LF.getStrings('MIDNIGHT')
                .then(string => {
                    return [string, '14 Nov 2017 12:00 AM'];
                });
        }
    }
});
dynamicText.add({
    id          : 'DD180_HAAfter',
    evaluate    : () => {
        if(LF.Utilities.dateDiffInDays(new Date(), new Date(LF.Data.Questionnaire.subject.get('activationDate'))) === 0){
            return LF.DynamicText.formatDateTime(new Date(LF.Data.Questionnaire.subject.get('activationDate')));
        }else{
            return LF.Utilities.getCollection('LastDiaries')
                .then((lastDiaries) => {
                    let now        = new Date(),
                        dailyDiary = _.find(lastDiaries, (diary) => {
                            return diary.get('questionnaire_id') === 'DailyDiary'
                                && LF.Utilities.dateDiffInDays(now, new Date(diary.get('lastStartedDate'))) <= 1;
                        });
                    return dailyDiary ? LF.DynamicText.formatDateTime(new Date(dailyDiary.get('lastStartedDate'))) : LF.getStrings('MIDNIGHT_TODAY');
                });
        }
    },
    screenshots : {
        getValues : () => {
            return LF.getStrings('MIDNIGHT')
                .then(string => {
                    return [string, '14 Nov 2017 12:00 AM']
                });
        }
    }
});
