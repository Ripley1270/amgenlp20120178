import * as Helpers from 'core/Helpers';
import dynamicText from 'core/dynamicText';
/**
 * Calculate the Pain average for VAS Diary.
 */
dynamicText.add({
    id          : 'painAverage',
    evaluate    : () => {
        let answer1   = Helpers.getResponses('VAS_DIARY_Q_1')[0],
            answer2   = Helpers.getResponses('VAS_DIARY_Q_2')[0],
            response1 = answer1 ? parseInt(answer1.response, 10) : 0,
            response2 = answer2 ? parseInt(answer2.response, 10) : 0;
        return (response1 + response2) / 2;
    },
    screenshots : {
        values : ['1', '100']
    }
});
