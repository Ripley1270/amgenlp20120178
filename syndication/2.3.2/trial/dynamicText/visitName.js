import dynamicText from 'core/dynamicText';
dynamicText.add({
    id          : 'getVisitName',
    evaluate    : () => {
        return LF.DynamicText.visitName;
    },
    screenshots : {
        values : ['Visit 1']
    }
});
