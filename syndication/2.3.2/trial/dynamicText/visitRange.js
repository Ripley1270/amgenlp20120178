import dynamicText from 'core/dynamicText';
dynamicText.add({
    id          : 'startDate',
    evaluate    : () => {
        return LF.DynamicText.startDate;
    },
    screenshots : {
        values : ['Jun 19, 2017']
    }
});
dynamicText.add({
    id          : 'endDate',
    evaluate    : () => {
        return LF.DynamicText.endDate;
    },
    screenshots : {
        values : ['Jun 22, 2017']
    }
});
