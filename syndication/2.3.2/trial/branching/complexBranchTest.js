LF.Branching.branchFunctions.checkVisitSelection = function (branchObj) {
    return new Q.Promise((resolve) => {
        LF.Utilities.getNextVisit()
            .then((nextVisit) => {
                LF.Utilities.getOngoingHeadaches()
                    .then(ongoingHeadaches => {
                        let selectedVisit = LF.Data.Questionnaire.queryAnswersByIT('VST1L')[0],
                            visitList     = LF.StudyDesign.visitList,
                            dormantVisits = [
                                visitList.week192,
                                visitList.week216,
                                visitList.week240,
                                visitList.week268
                            ];
                        if(ongoingHeadaches.length > 0 && selectedVisit && _.contains(dormantVisits, selectedVisit.get('response'))){
                            //doThings
                            console.log('going to Dormant Phase');
                            branchObj.branchTo = 'VisitConfirmation_10113';
                        }else{
                            branchObj.branchTo = 'AFFIDAVIT';
                        }
                        resolve(selectedVisit && Number(selectedVisit.get('response')) === nextVisit.value);
                    });
            });
    });
};
LF.Branching.branchFunctions.checkDormantStuff = function (branchObj) {
    return new Q.Promise((resolve) => {
        LF.Utilities.getOngoingHeadaches()
            .then(ongoingHeadaches => {
                let selectedVisit = LF.Data.Questionnaire.queryAnswersByIT('VST1L')[0],
                    visitList     = LF.StudyDesign.visitList,
                    dormantVisits = [
                        visitList.week192,
                        visitList.week216,
                        visitList.week240,
                        visitList.week268
                    ];
                if(ongoingHeadaches.length > 0 && selectedVisit && _.contains(dormantVisits, selectedVisit.get('response'))){
                    //doThings
                    console.log('going to Dormant Phase');
                    branchObj.branchTo = 'VisitConfirmation_10113';
                }else{
                    branchObj.branchTo = 'AFFIDAVIT';
                }
                resolve(true);
            });
    });
};
LF.Branching.branchFunctions.checkHeadache = function (branchObj) {
    return new Q.Promise((resolve) => {
        let igr          = LF.Data.Questionnaire.getCurrentIGR(branchObj.branchParams.IG),
            headacheType = _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('HATYPE1L', igr));
        resolve(headacheType && headacheType.get('response') === branchObj.branchParams.headache);
    });
};
LF.Branching.branchFunctions.checkMedicationCount = function (branchObj) {
    return new Q.Promise((resolve) => {
        resolve(LF.Data.Questionnaire.getCurrentIGR(branchObj.branchParams.IG) > 1);
    });
};
LF.Branching.branchFunctions.checkLoops = function (branchObj) {
    return new Q.Promise((resolve) => {
        let ig       = branchObj.branchParams.IG,
            igr      = LF.Data.Questionnaire.getCurrentIGR(ig),
            maxLoops = branchObj.branchParams.maxLoops;
        if(ig === 'HeadacheMed'){
            maxLoops += (LF.Data.Questionnaire.baseIGR || 0);
        }
        if(igr >= maxLoops){
            branchObj.branchTo = branchObj.branchParams.endOfLoop;
        }else{
            branchObj.branchTo = branchObj.branchParams.startOfLoop;
            LF.Data.Questionnaire.incrementIGR(ig);
        }
        resolve(true);
    });
};
LF.Branching.branchFunctions.equalHeadache = function (branchObj, view) {
    return new Q.Promise((resolve) => {
        resolve(LF.Branching.branchFunctions.equal(branchObj, view) && LF.Branching.branchFunctions.checkHeadache(branchObj));
    });
};
LF.Branching.branchFunctions.equalMedCount = function (branchObj, view) {
    return new Q.Promise((resolve) => {
        resolve(LF.Branching.branchFunctions.equal(branchObj, view) && LF.Branching.branchFunctions.checkMedicationCount(branchObj));
    });
};
LF.Branching.branchFunctions.checkMedicationProximity = (branchObj) => {
    return new Q.Promise((resolve) => {
        let info     = LF.Utilities.getSelectedMedicationInfo(branchObj.branchParams.IG || 'AURA'),
            branchTo = branchObj.branchFrom !== 'DD402' ? 'DailyDiary_9119_0' : 'DD404_0',
            units    = info.unitsSingle || info.units || 'nothing';
        if(units.indexOf('MLS') !== -1){
            branchTo = branchObj.branchFrom !== 'DD402' ? 'DailyDiary_9119_1' : 'DD404_1';
        }
        branchObj.branchTo = branchTo;
        resolve(LF.Branching.branchFunctions.checkDuplicateMedications(branchObj));
    });
};
LF.Branching.branchFunctions.isMilliliters = (branchObj) => {
    return new Q.Promise((resolve) => {
        let info     = LF.Utilities.getSelectedMedicationInfo(branchObj.branchParams.IG || 'AURA'),
            branchTo = branchObj.branchFrom !== 'DD403' ? 'DailyDiary_9119_0' : 'DD404_0',
            units    = info.unitsSingle || info.units || 'nothing';
        if(units.indexOf('MLS') !== -1){
            branchTo = branchObj.branchFrom !== 'DD403' ? 'DailyDiary_9119_1' : 'DD404_1';
        }
        branchObj.branchTo = branchTo;
        resolve(true);
    });
};
LF.Branching.branchFunctions.checkOngoingHeadache = function () {
    return new Q.Promise((resolve) => {
        resolve(LF.Utilities.getOngoingHeadaches()
            .then((ongoingHeadaches) => {
                return ongoingHeadaches.length > 0;
            }));
    });
};
LF.Branching.branchFunctions.checkTimeSinceLastHeadache = function () {
    return new Q.Promise((resolve) => {
        resolve(LF.Utilities.getOngoingHeadaches()
            .then((ongoingHeadaches) => {
                let ongoingHeadache = _.last(ongoingHeadaches),
                    now             = new Date().getTime();
                return ongoingHeadache && (now - new Date(ongoingHeadache.get('startDate')).getTime()) > (72 * 60 * 60 * 1000);
            }));
    });
};
LF.Branching.branchFunctions.checkTimeOverlap = function () {
    return new Q.Promise((resolve) => {
        resolve(LF.Utilities.getOverlappingHeadaches());
    });
};
LF.Branching.branchFunctions.setStartingMedLoop = (branchObj, view) => {
    return new Q.Promise((resolve) => {
        return LF.Branching.branchFunctions.equal(branchObj, view)
            .then((result) => {
                let ig  = 'HeadacheMed',
                    igr = LF.Data.Questionnaire.getCurrentIGR(ig);
                if(result){
                    if(LF.Data.Questionnaire.queryAnswersByIGAndIGR(ig, igr).length > 0){
                        LF.Data.Questionnaire.incrementIGR(ig);
                        igr = LF.Data.Questionnaire.getCurrentIGR(ig);
                    }
                    LF.Data.Questionnaire.baseIGR = (igr - 1);
                    LF.Data.Questionnaire.medStartIndex.push(igr);
                }else{
                    let index = LF.Data.Questionnaire.medStartIndex.indexOf(igr);
                    if(index > -1){
                        LF.Data.Questionnaire.medStartIndex.splice(index, 1);
                    }
                }
                resolve(result);
            });
    });
};
LF.Branching.branchFunctions.checkboxEqual = (branchObj) => {
    return new Q.Promise((resolve) => {
        let answers = branchObj.branchParams.answers,
            ig      = branchObj.branchParams.IG,
            igr     = LF.Data.Questionnaire.getCurrentIGR(ig),
            thisResponse;
        resolve(_.every(answers, (answer) => {
            thisResponse = _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR(answers.IT, igr));
            return (thisResponse && thisResponse.get('response') === answer.value);
        }));
    });
};
LF.Branching.branchFunctions.checkMinutesElapsed = (branchObj) => {
    return new Q.Promise((resolve) => {
        let durationType = branchObj.branchParams.durationType;
        resolve(LF.Utilities[`get${durationType}Duration`]()
            .then(headacheDuration => {
                return headacheDuration.minutes < 15;
            }));
        //resolve(LF.Utilities[`get${durationType}Duration`]().minutes < 15);
    });
};
LF.Branching.branchFunctions.checkHoursElapsed = (branchObj) => {
    return new Q.Promise((resolve) => {
        let durationType = branchObj.branchParams.durationType;
        resolve(LF.Utilities[`get${durationType}Duration`]()
            .then(headacheDuration => {
                return headacheDuration.time[0] >= 72;
            }));
    });
};
LF.Branching.branchFunctions.checkSleepDuration = () => {
    return new Q.Promise((resolve) => {
        let durations = LF.Utilities.getSleepDuration();
        resolve(durations.time[0] < 12 && durations.minutes >= 15);
    });
};
LF.Branching.branchFunctions.checkDuplicateMedications = (branchObj) => {
    return new Q.Promise((resolve) => {
        resolve(LF.Utilities.getMedicationArray(true, branchObj.branchParams.IG)
            .then((medications) => {
                let ig           = branchObj.branchParams.IG,
                    igr          = LF.Data.Questionnaire.getCurrentIGR(ig),
                    medicationID = _.last(LF.Data.Questionnaire.queryAnswersByIGITAndIGR(ig, 'MEDNAM1L', igr)),
                    dateTaken    = _.last(LF.Data.Questionnaire.queryAnswersByIGITAndIGR(ig, 'TIMTAK1S', igr)),
                    minuteDiff;
                if(medicationID && dateTaken){
                    medicationID = LF.Utilities.getMedicationID(medicationID.get('response'));
                    dateTaken = new Date(dateTaken.get('response')).getTime();
                    medications = _.filter(medications, (medication) => {
                        minuteDiff = Math.abs((new Date(medication.get('dateTaken')).getTime() - dateTaken) / (1000 * 60));
                        return medication.get('medicationID') === medicationID && minuteDiff <= 15;
                    });
                    return medications.length === 0;
                }else{
                    return false;
                }
            }));
    });
};
LF.Branching.branchFunctions.DD250Branching = (branchObj, view) => {
    return new Q.Promise((resolve) => {
        resolve(LF.Utilities.getOngoingHeadaches()
            .then((ongoingHeadaches) => {
                let ig               = 'Headache',
                    igr              = LF.Data.Questionnaire.getCurrentIGR(ig),
                    ongoingHeadache  = _.last(ongoingHeadaches),
                    now              = new Date().getTime(),
                    currentSelection = _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('M78IT', igr)),
                    headacheSleep    = _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('HEDSLP1L', igr));
                if(currentSelection){
                    currentSelection = currentSelection.get('response');
                    if(currentSelection === '1'){
                        if(ongoingHeadache){
                            branchObj.branchParams.simulateBackButton = true;
                            return LF.Branching.branchFunctions.alwaysBranchBack(branchObj, view);
                        }else{
                            branchObj.branchTo = 'DailyDiary_91119';
                            branchObj.branchParams.simulateBackButton = false;
                            return LF.Branching.branchFunctions.alwaysBranchBack(branchObj, view);
                        }
                    }else if(currentSelection === '2'){
                        if(headacheSleep && headacheSleep.get('response') === '1'){
                            branchObj.branchTo = 'DailyDiary_91128';
                        }else{
                            branchObj.branchTo = 'DailyDiary_91130';
                        }
                    }else{
                        if(ongoingHeadache){
                            branchObj.branchTo = 'DailyDiary_91116';
                            branchObj.branchParams.simulateBackButton = false;
                            return LF.Branching.branchFunctions.alwaysBranchBack(branchObj, view);
                        }else{
                            branchObj.branchTo = 'DailyDiary_91143';
                            LF.Widget.ReviewScreen.deleteLoop(ig, igr);
                        }
                    }
                    return true;
                }else{
                    return false;
                }
            }));
    });
};
LF.Branching.branchFunctions.DD260Branching = (branchObj, view) => {
    return new Q.Promise((resolve) => {
        let ig               = 'Headache',
            igr              = LF.Data.Questionnaire.getCurrentIGR(ig),
            currentSelection = _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('M81IT', igr)),
            headacheSleep    = _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('HEDSLP1L', igr));
        if(currentSelection){
            currentSelection = currentSelection.get('response');
            if(currentSelection === '1'){
                branchObj.branchParams.simulateBackButton = true;
                resolve(LF.Branching.branchFunctions.alwaysBranchBack(branchObj, view));
            }else if(currentSelection === '2'){
                if(headacheSleep && headacheSleep.get('response') === '1'){
                    branchObj.branchTo = 'DailyDiary_91128';
                }else{
                    branchObj.branchTo = 'DailyDiary_91130';
                }
                resolve(true);
            }else{
                resolve(false);
            }
        }else{
            resolve(false);
        }
    });
};
LF.Branching.branchFunctions.clearLoopThenEqual = (branchObj, view) => {
    return new Q.Promise((resolve) => {
        resolve(LF.Branching.branchFunctions.equal(branchObj, view)
            .then(result => {
                if(result){
                    LF.Widget.ReviewScreen.deleteLoop(branchObj.branchParams.IG, LF.Data.Questionnaire.getCurrentIGR(branchObj.branchParams.IG));
                    LF.Data.Questionnaire.screenStack.pop();
                }
                return result;
            }));
    });
};
