LF.Branching.branchFunctions.toReviewScreen = function (branchObj, view) {
    return new Q.Promise((resolve) => {
        let branchFunction = LF.Branching.branchFunctions.always;
        //Check for conditional branching params
        if(branchObj.branchParams.questionId){
            branchFunction = LF.Branching.branchFunctions.equal;
        }
        //Execute branching function and save loop if it returns true
        resolve(new Q.Promise((resolve) => {
            branchFunction(branchObj)
                .then((result) => {
                    if(result){
                        //Clear remaining screens in loop
                        while(_.last(view.screenStack) !== branchObj.branchTo){
                            view.screenStack.pop();
                        }
                        view.screenStack.pop();
                        let IG = branchObj.branchParams.IG;
                        LF.Widget.ReviewScreen.saveLoop(IG);
                    }
                    resolve(result);
                });
        }));
    });
};
LF.Branching.branchFunctions.deleteLoop = function (branchObj, view) {
    return new Q.Promise((resolve) => {
        LF.Branching.branchFunctions.equal(branchObj, view)
            .then((result) => {
                //Clear loop if equal rule returns true
                if(result){
                    let IG  = branchObj.branchParams.IG,
                        // getting id from the selected item and using it as IGR. This works because
                        // when creating review screen item, IGRs were used as item ids which makes
                        // it easier and doesn't require any mapping.
                        IGR = LF.Data.Questionnaire.editing;
                    LF.Widget.ReviewScreen.deleteLoop(IG, IGR);
                }
                //Always branch back
                resolve(
                    LF.Branching.branchFunctions.alwaysBranchBack(branchObj, view)
                        .then((result) => {
                            return new Q.Promise((resolve) => {
                                resolve(result);
                            });
                        })
                );
            });
    });
};
