import StudyDesign from 'core/classes/StudyDesign';
import Subjects from 'core/collections/Subjects';
import Users from 'core/collections/Users';
import Sites from 'core/collections/Sites';
import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';
import Language from 'core/models/Language';
import * as lStorage from 'core/lStorage';
import Data from 'core/Data';
import * as Utils from 'core/utilities';
import config from './config.json';
import Logger from 'core/Logger';
const logger = new Logger('studyBuilder.loadContext');
/**
 * Does the AJAX RESTful call to the Server.
 * @param {Object} ajaxConfig The data required to make AJAX call.
 * @param {Object} data The data sent with AJAX call.
 * @returns {Q.Promise} resolved when transmit is complete
 * @example this.transmit({
 *      type    : 'GET',
 *      uri     : '/subjects/12345678'
 *  }, { }, onSuccess, onError);
 */
const transmit = (ajaxConfig, data) => {
    return Q.Promise((resolve, reject) => {
        let timeout = ( /*design.transmissionTimeout || */ LF.CoreSettings.transmissionTimeout) * 1000;
        if(ajaxConfig.type == null){
            throw new Error('Missing Argument: type is null or undefined.');
        }else if(ajaxConfig.uri == null){
            throw new Error('Missing Argument: uri is null or undefined.');
        }
        logger.operational(`Attempt to transmit ${ajaxConfig.type} ${ajaxConfig.uri} started.`, {
            method : ajaxConfig.type,
            uri    : ajaxConfig.uri
        }, 'CommStart');
        $.ajax({
            type        : ajaxConfig.type,
            url         : ajaxConfig.uri,
            data        : ajaxConfig.type === 'GET' ? '' : JSON.stringify(data),
            dataType    : ajaxConfig.dataType || 'json',
            contentType : 'application/json',
            timeout     : timeout,
            cache       : false,
            // jQuery is setting the context of this... Fat arrows don't work properly.
            success     : (res) => {
                let path = ajaxConfig.uri.replace(/https?:\/\/[^\/]+\//, '');
                // Only log up to 100 chars of response at OPERATIONAL level;
                // keep in mind all this data gets POST'ed back to server
                logger.operational(`Successful request ${ajaxConfig.type} ${path}`, {
                    uri      : ajaxConfig.uri,
                    method   : ajaxConfig.type,
                    response : (typeof res === 'string' ? res : JSON.stringify(res)).slice(0, 100)
                }, 'CommEnd');
                // But log the whole thing at TRACE level (goes to console only)
                logger.trace(`Successful request ${ajaxConfig.type} ${path}`, {
                    response : typeof res === 'string' ? res : JSON.stringify(res)
                });
                resolve(res);
            },
            error       : (jqXHR) => {
                let isSubjectActive = jqXHR.getResponseHeader('X-Device-Status') == null ? 1 :
                        +jqXHR.getResponseHeader('X-Device-Status'),
                    errorCode       = jqXHR.getResponseHeader('X-Error-Code'),
                    httpCode        = jqXHR.status,
                    swisError       = errorCode ? ` and SWIS error: ${errorCode}` : '';
                logger.operational(`Unsuccessful transmission ${ajaxConfig.type} ${ajaxConfig.uri} with HTTP code: ${httpCode} ${swisError}`, {}, 'CommFail');
                reject({
                    errorCode,
                    httpCode,
                    isSubjectActive
                });
            }
        })
            .done();
    });
};
/**
 * alters the design for StudyDesign mode.  One can tell that you should be in study design embedded mode
 * if mandatoryOff = true
 * @param {Object} design - the design to modify
 */
function setStudyDesignEmbeddedMode (design) {
    const screens = design.screens;
    _.each(screens, (screen) => {
        _.each(screen.questions, (question) => {
            question.mandatory = false;
        });
    });
}
let _syndicatorUrlBase,
    _mandatoryOff = false,
    _deviceType   = 'handheld';
export default class StudyDesignerData {
    static set deviceType (val) {
        _deviceType = val;
    }

    static get deviceType () {
        return _deviceType;
    }

    static set mandatoryOff (val) {
        _mandatoryOff = val;
    }

    static get mandatoryOff () {
        return _mandatoryOff;
    }

    static set syndicatorUrlBase (val) {
        _syndicatorUrlBase = val;
    }

    static get syndicatorUrlBase () {
        return _syndicatorUrlBase;
    }

    static set currentStudyId (val) {
        localStorage.setItem('studyDesignStudyId', val);
    }

    static get currentStudyId () {
        return localStorage.getItem('studyDesignStudyId');
    }

    static set horizonBase (val) {
        localStorage.setItem('horizonBase', val);
    }

    static get horizonBase () {
        return localStorage.getItem('horizonBase');
    }

    static set messageURL (val) {
        localStorage.setItem('horizonMessageURL', val);
    }

    static get messageURL () {
        return localStorage.getItem('horizonMessageURL');
    }

    /**
     * sets the compass server and looks up any relevent services before resolving
     * @param {string} url - the URL of the compass server
     * @returns {Q.promise<void>}
     */
    static setCompassServiceURL (url) {
        StudyDesignerData.horizonBase = url || StudyDesignerData.horizonBase || config.compassUrl;
        logger.trace(`Setting compass server to: ${StudyDesignerData.horizonBase}`);
        const promises = [];
        promises.push(transmit({
            type   : 'GET',
            uri    : `${StudyDesignerData.horizonBase}/api/v1/services/syndicator`,
            //            auth: auth,
            syncID : localStorage.getItem('PHT_syncID')
        })
            .then((data) => {
                StudyDesignerData.syndicatorUrlBase = `${data.protocol}://${data.domain}:${data.ports.h_port}/api/v1`;
            }));
        promises.push(transmit({
            type   : 'GET',
            uri    : `${StudyDesignerData.horizonBase}/api/v1/services/hmq_adapter`,
            //            auth: auth,
            syncID : localStorage.getItem('PHT_syncID')
        })
            .then((data) => {
                StudyDesignerData.messageURL = `${data.domain}:${data.ports.webSocketPort}`;
            }));
        return Q.all(promises);
    }

    /**
     * @param {string} questionId - The ID of the question
     */
    /**
     * loads up the dummy environment by querying server for dummy environment and then installing
     * appropriate date
     * @param {string} studyId - the id of the study
     * @param {string} platform - the platform to get teh environemt for.
     * @returns {Q.Promise<void>}
     */
    static setupEnv () {
        let subjects,
            sites,
            users;
        const envName = StudyDesignerData.deviceType || LF.appName;
        const ajaxConfig = {
            type   : 'GET',
            uri    : `${StudyDesignerData.syndicatorUrlBase}/designs/${StudyDesignerData.currentStudyId ? StudyDesignerData.currentStudyId : '$tudydesigner'}/syndicationEnvironment/${envName}`,
            //            auth: auth,
            syncID : localStorage.getItem('PHT_syncID')
        };
        return transmit(ajaxConfig, {})
            .then((data) => {
                if(data.LF){
                    _.forEach(data.LF, (value, key) => {
                        LF[key] = _.extend(LF[key], value);
                    });
                }
                _.forEach(data.localStorage, (value, key) => {
                    localStorage.setItem(key, value);
                });
                _.forEach(data.lStorage, (value, key) => {
                    lStorage.setItem(key, value);
                });
                Utils.setServiceBase(data.serviceBase);
                subjects = new Subjects(data.subjects);
                sites = new Sites(data.sites);
                users = new Users(data.users);
                return Q.all([subjects.save(), sites.save(), users.save()]);
            })
            .catch((e) => {
                logger.error(`Unexpected error retrieving the syndication environment from syndicator: 
                    ${(e instanceof Object ? JSON.stringify(e) : e)}`);
                throw (e);
            })
            .then(() => {
                return Q.Promise((resolve, reject) => {
                    const checkComplete = () => {
                        Q.all([Subjects.fetchCollection(), Users.fetchCollection(), Sites.fetchCollection()])
                            .spread((fetchedSubjects, fetchedUsers, fetchedSites) => {
                                if(fetchedSubjects.length >= subjects.length &&
                                    fetchedUsers.length >= users.length &&
                                    fetchedSites.length >= sites.length
                                ){
                                    resolve();
                                }else{
                                    setTimeout(checkComplete);
                                }
                            })
                            .catch((e) => {
                                reject(e);
                            });
                    };
                    checkComplete();
                });
            })
            .then(() => {
                Data.subject = subjects.at(0);
                Data.site = sites.at(0);
                // dummy out setContentLanguage
                LF.content.setContentLanguage = (lang) => {
                    let language = lang.split('-'),
                        langloc  = {
                            language : language[0],
                            locale   : language[1]
                        };
                    LF.content.language = langloc.language;
                    LF.content.locale = langloc.locale;
                    LF.Preferred = langloc;
                    Utils.setLanguage(lang, LF.strings.getLanguageDirection(langloc));
                };
            });
    }

    /**
     * currently just getting the data needed by the study list, but actual will probably need to return more
     * and this method would have to be updated to take that return and make what the study list requires
     * @returns {object} the design list for use in the SD picklist
     */
    static FetchSDList () {
        const ajaxConfig = {
            type   : 'GET',
            uri    : `${StudyDesignerData.syndicatorUrlBase}/designlist`,
            //            auth: auth,
            syncID : localStorage.getItem('PHT_syncID')
        };
        return transmit(ajaxConfig, {});
    }

    static resetQuestionnaire (view) {
        // are we in StudyDesigner embedded mode?
        if(StudyDesignerData.mandatoryOff){
            //noinspection JSUnresolvedVariable
            view.model = LF.StudyDesign.questionnaires.get(view.id);
            view.evaluateBranching = view.model.evaluateBranching || view.evaluateBranching;
            //noinspection JSUnresolvedVariable
            view.prepScreens();
            //noinspection JSUnresolvedVariable
            view.prepQuestions();
        }
        view.configureAffidavit();
        return view.render(view.currentScreenId);
    }

    static getStudy (studyId) {
        const ajaxConfig = {
            type   : 'GET',
            uri    : `${StudyDesignerData.syndicatorUrlBase}/designs`,
            //            auth: auth,
            syncID : localStorage.getItem('PHT_syncID')
        };
        if(studyId){
            ajaxConfig.uri += `/${studyId}/${StudyDesignerData.deviceType}`;
        }else{
            ajaxConfig.uri += `/${StudyDesignerData.deviceType}`;
        }
        return transmit(ajaxConfig, {})
            .then((design) => {
                if(StudyDesignerData.mandatoryOff){
                    setStudyDesignEmbeddedMode(design);
                }
                const languageResources = design.languageResources,
                      configEvaluate    = design.evaluate;
                delete design.languageResources;
                delete design.evaluate;
                const newStudyDesign = new StudyDesign(design);
                const oldSchedules = LF.StudyDesign.schedules;
                // keep the original schedule listeners
                oldSchedules.reset(newStudyDesign.schedules.models);
                newStudyDesign.schedules = oldSchedules;
                LF.StudyDesign = newStudyDesign;
                if(newStudyDesign.sessionTimeout){
                    LF.security.sessionTimeout = newStudyDesign.sessionTimeout * 60 * 1000;
                }else{
                    newStudyDesign.sessionTimeout = studyDesignerData.sessionTimeout;
                }
                configEvaluate.forEach((fnObj) => {
                    try{
                        //eslint-disable-next-line no-eval
                        eval(fnObj);
                    }catch(e){
                        logger.error(`getStudy(): Exception while interpreting function ${fnObj.dest}`);
                    }
                });
                _.each(languageResources, (item) => {
                    let existingResources = LF.strings.match({
                        namespace : item.namespace,
                        language  : item.language,
                        locale    : item.locale
                    });
                    if(existingResources){
                        _.each(existingResources, (item) => {
                            LF.strings.remove(item);
                        });
                    }
                    LF.strings.add(new Language(item));
                });
                _.forEach(design.templates, (value, key) => {
                    const curScript = $(`script#${key}`);
                    if(curScript.length !== 0){
                        curScript[0].innerHTML = value;
                    }else{
                        const curScript = document.createElement('script');
                        curScript.type = 'text/template';
                        curScript.id = 'key';
                        curScript.innerHTML = value;
                        document.body.appendChild(curScript);
                    }
                });
                const currentView = LF.router.controller.view;
                if(currentView instanceof BaseQuestionnaireView){
                    return StudyDesignerData.resetQuestionnaire(currentView);
                }
                return Q();
            })
            .then(() => {
                if(LF.content && LF.content.loadContextSchedules){
                    // Logpad is messeed up.   the schedules are loaded when you pick a subject in the login view and
                    // cached (note not when you actually log in, but when you pick the subject it sets a global)
                    // refresh cached schedules (no idea why cached... just have to do this to make it work.
                    // dpeterson)
                    return LF.content.loadContextSchedules();
                }else{
                    return Q();
                }
            });
    }
}
