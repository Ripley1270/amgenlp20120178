import Logger from 'core/Logger';
let logger = new Logger('studyDesigner.DataAccess', {consoleOnly : true});
let eCoaDB = {};
let LogDB = {};
export default function dummyDataAccess (method, object, options = {}) {
    logger.debug(`Initiating ${method} for ${object}`); // assume objects have reasonable .toString()
    logger.trace(`Object data: ${JSON.stringify(object)}`);
    let db      = object.storage === 'Log' ? LogDB : eCoaDB,
        storage = db[object.storage] || (db[object.storage] = {});
    return Q()
        .then(() => {
            switch(method){
                case 'read':

                    // The object is a collection.
                    if(object.length >= 0){
                        return Q()
                            .then(() => {
                                let output = [];
                                _.forEach(storage, (item) => {
                                    let model = item.clone();
                                    output.push(model.toJSON());
                                });
                                // Query the decrypted data.
                                if(options.search){
                                    output = query(output, options.search);
                                }
                                // Reset the collection with the query results.
                                object.reset(output);
                                return (object.models);
                            });
                    }else{
                        // The object is a model.
                        // If there is a query...
                        if(options.search && options.search.where){
                            return Q(storage)
                                .then((result) => {
                                    let output = [],
                                        search = options.search || {};
                                    // Loop thru each record and decrypt it.
                                    _.forEach(result, (item) => {
                                        let model = object.clone();
                                        model.set(item);
                                        output.push(model.toJSON());
                                    });
                                    // Query the decrypted data.
                                    output = query(output, search);
                                    // Set the first data set to the model or an empty object literal.
                                    object.set(output[0] || {});
                                    return (object.attributes);
                                });
                            // Otherwise, get by id...
                        }else{
                            return Q(storage[object.id])
                                .then((res) => {
                                    // Instead of encrypting directly on the model in question, we clone it and do so
                                    // there.
                                    let model = object.clone();
                                    model.set(res);
                                    object.set(model);
                                    return (model);
                                });
                        }
                    }
                case 'create': // Fallthrough...
                case 'update':
                    let clone = object.clone();
                    if(clone.isValid()){
                        let id  = Date.now(),
                            ndx = 0;
                        while(!clone.id){
                            if(!storage[id + ndx]){
                                clone.id = id + ndx;
                                break;
                            }
                            ndx++;
                        }
                        storage[clone.id] = clone;
                        object.set({
                            id : clone.id
                        });
                        return Q(clone.id);
                    }else{
                        return Q.reject(clone.validationError);
                    }
                case 'delete':
                    delete storage[clone.id];
                    return Q();
                case 'clear' :
                    db[object.storage] = {};
                    return Q();
                case 'count' :
                    return Q(storage.keys().length);

                // This method is deprecated.  Just resolves the promise to support legacy code.
                case 'createStorage':
                    db[object.storage] = {};
                    return Q();
                case 'removeStorage':
                    db[object.storage] = {};
                    return Q();

                // This method is deprecated.  Just resolves the promise to support legacy code.
                case 'checkInstall':
                    return Q();
            }
        })
        .then((res) => {
            (options.onSuccess || $.noop)(res);
            return res;
        })
        .catch((err) => {
            (options.onError || $.noop)(err);
            throw err;
        });
}

