/**
 * Subscribe to a remote hmq by using a WebSocket. <b>For use in browser only</b>; not NodeJS.
 *
 * @param {Object}          opts            (required) connection/subscription options, some of which are actually
 *     required
 * @param {string}          opts.address    (required) host:port where hmq_websocket_server is running
 * @param {array<string>}          opts.topics      (required) topics to subscribe to
 * @param {Logger=}         opts.logger     (optional) ...should use standard Syndication class
 * @param {string}          opts.queue      (optional) give your queue a specific name (advanced use)
 * @param {MessageHandler}  opts.onmessage  (required) called when new data is available
 * @param {ErrorHandler=}   opts.onerror    (optional) called if some error occurs
 * @param {CloseHandler=}   opts.onclose    (optional) called when the connection is somehow closed
 */
function hmq_subscribe (opts) {
    var connection;
    var defaultLogger = {
        trace : function (str) {
            console.log(str);
        },
        error : function (str) {
            console.log(str);
        }
    };
    opts.logger = opts.logger || defaultLogger;
    opts.logger.trace('hmq_subscribe: to address:' + opts.address);
    connection = new WebSocket("ws://" + opts.address);
    connection.onopen = function () {
        opts.logger.trace("hmq_subscribe: Connection opened, sending details");
        var request = {
            action : 'subscribe',
            topics : opts.topics,
            queue  : opts.queue
        };
        opts.logger.trace('Sending:' + JSON.stringify(request));
        connection.send(JSON.stringify(request));
    };
    connection.onclose = function () {
        opts.logger.trace('hmq_subscribe: Connection closed');
        if(opts.onclose) opts.onclose();
    };
    connection.onerror = function (err) {
        opts.logger.trace('hmq_subscribe: Connection error:' + err);
        if(opts.onerror) opts.onerror(err);
    };
    connection.onmessage = function (event) {
        opts.logger.trace('hmq_subscribe: websocket received:' + event);
        let msg = JSON.parse(event.data);
        if(msg.cmd == 'NewData'){
            opts.onmessage(msg.msg, msg.hdr);
        }else{
            connection.onerror(msg.msg);
        }
    };
}
export default {
    subscribe : hmq_subscribe
};
/**
 * A Logger, such as Syndication's Logger class
 * @typedef {Object} Logger
 * @property {Function} trace
 * @property {Function} error
 */

/**
 * Your handler in case of errors
 * @callback ErrorHandler
 * @param {*} error the error code?
 */

/**
 * Your handler in case of disconnect
 * @callback CloseHandler
 */

/**
 * Your message handler
 * @callback MessageHandler
 * @param {*} message The decoded message
 * @param {{string:string}} Message headers
 */
