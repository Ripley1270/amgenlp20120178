import StudyDesignerData from './loadContext';
import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';
import * as lStorage from 'core/lStorage';
import Data from 'core/Data';
import {Banner} from 'core/Notify';
import Logger from 'core/Logger';
import ELF from 'core/ELF';
import hmq from './hmq_websocket_client.js';
import CurrentContext from 'core/CurrentContext';
import dummyDataAccess from './dataAccess';
let logger = new Logger('trial/studyDesigner/index.js', {consoleOnly : true});
let sessionId;
let curSdPromise                 = Q(),
    unexpectedSdPromiseRejection = (err) => {
        logger.error('unexpected promise in SDPromise chain');
        logger.error(err);
    },
    /**
     * used to hold the last navigation command. If a new command has been passed by the time the system has
     * performed any preprocessing, then the system will not attempt to navigate so that you don't get
     * screen flashes or delays caused by the same screen redrawing multiple times.
     * @type {object}
     */
    lastNavigation               = null,
    // holds onto the last navigation successfully performed.  Used to not re-send to StudyDesigner a screen change
    // if redrawing (the design reloaded for some reason)
    lastCompletedNavigation      = null,
    // parent of iframe's hostname
    parentHostName               = null;
class StudyDesignerNavMode {
    static removeFromElf () {
        ELF.remove('StudyDesignerNaveMode_Backout');
        ELF.remove('StudyDesignerNaveMode_Navigate');
        StudyDesignerData.mandatoryOff = false;
    }

    static addToElf () {
        StudyDesignerData.mandatoryOff = true;
        ELF.rules.add({
            id       : 'StudyDesignerNavMode_skipQuestions',
            salience : Number.MAX_SAFE_INTEGER,
            trigger  : ['QUESTIONNAIRE:SkipQuestions'],
            evaluate () {
                return (true);
            },
            resolve  : [
                {
                    action : () => {
                        return {
                            preventDefault : false,
                            stopRules      : true
                        };
                    }
                }
            ]
        });
        ELF.rules.add({
            id       : 'StudyDesignerNaveMode_Backout',
            salience : Number.MAX_SAFE_INTEGER,
            trigger  : ['QUESTIONNAIRE:BackOut'],
            evaluate () {
                return (true);
            },
            resolve  : [
                {
                    action : () => {
                        Banner.show({
                            text : 'This is the first screen',
                            type : 'error'
                        });
                        return {
                            preventDefault : true,
                            stopRules      : true
                        };
                    }
                }
            ]
        });
        ELF.rules.add({
            id       : 'StudyDesignerNaveMode_Navigate',
            salience : Number.MAX_SAFE_INTEGER,
            trigger  : ['QUESTIONNAIRE:Navigate'],
            evaluate () {
                return (true);
            },
            resolve  : [{action : StudyDesignerNavMode.processNavigate}]
        });
        // Add screenshot router and route to screenshot view.
        ELF.rules.add({
            id      : 'StudyDesignerNavMode_screenRefresh',
            trigger : [
                'QUESTIONNAIRE:Before'
            ],
            evaluate () {
                return true;
            },
            resolve : [
                {
                    action () {
                        // if we were sent to this screen by SD, don't repost
                        if(lastCompletedNavigation !== null &&
                            this.model.get('horizonId') === lastCompletedNavigation.questionnaireId &&
                            this.data.screens[this.screen].get('horizonId') === lastCompletedNavigation.screenId){
                            return;
                        }
                        //const src = this.iframe.src.split('/').slice(0,3).join('/');
                        if(parentHostName && window.parent.postMessage){
                            window.parent.postMessage({
                                currentScreen : this.data.screens[this.screen].get('horizonId')
                                || this.data.screens[this.screen].get('id')
                            }, parentHostName);
                        }
                        return;
                    }
                }
            ]
        });
    }

    /**
     * performs the actual navigation handling of moving forward or back one screen and
     * then prevents the standard navigation (so no branching, etc)
     * @param {object} context - contains a direction attribute with 'next' or 'previous'
     * @returns {Q.Promise<void>}
     */
    static processNavigate (context) {
        if(context.direction === 'next'){
            if(this.screen + 1 >= this.data.screens.length){
                Banner.show({
                    text : 'This is the last screen',
                    type : 'error'
                });
                return Q({
                    preventDefault : true,
                    stopRules      : true
                });
            }else{
                ++this.screen;
                const screen = this.data.screens[this.screen];
                this.screenStack.push(screen.id);
                return this.displayScreen(screen.id)
                    .then(() => {
                        return {
                            preventDefault : true,
                            stopRules      : true
                        };
                    });
            }
        }else{
            // assumes backout handler won't let it this get called on the first screen
            ++this.screen;
            this.screenStack.pop();
            const screenId = this.screenStack[this.screenStack.length - 1];
            return this.displayScreen(screenId)
                .then(() => {
                    return {
                        preventDefault : true,
                        stopRules      : true
                    };
                });
        }
    }
}
const optionalElfRules = {'studyDesignerNavMode' : StudyDesignerNavMode};
/**
 * if the studyID isn't the navigate studyID, go fetch it
 * @param {object} curNavigate - the current navigation command
 * @returns {Promise} - resolved when complete
 */
function getStudyIfDifferent (curNavigate) {
    //noinspection JSUnresolvedVariable
    if(StudyDesignerData.currentStudyId !== curNavigate.studyId){
        //noinspection JSUnresolvedVariable
        StudyDesignerData.currentStudyId = curNavigate.studyId;
        // TODO: need to detach from any prior subscription
        // and set this to only happen in one place.  currently happening
        // in at least 2.   StudyDesignerData doesn't have access to msgProcessor
        // and I need to do this in a few hours.
        hmq.subscribe({
            address   : StudyDesignerData.messageURL,
            topics    : [`StudyDesignChange.${StudyDesignerData.currentStudyId}`],
            //eslint-disable-next-line no-use-before-define
            onmessage : msgProcessor
        });
        //noinspection JSUnresolvedVariable
        return StudyDesignerData.getStudy(curNavigate.studyId);
    }else{
        return Q();
    }
}
/**
 * if the studyID isn't the navigate studyID, go fetch it
 * @param {object} data - the message data
 * @param {object} curNavigate - the current navigation being performed
 * @returns {Promise} - resolved when complete
 */
function getQuestionnaireIfDifferent (data, curNavigate) {
    // if this is still the last navigate, go to the questionnaire
    if(lastNavigation === data.navigate && !(LF.router.controller.view instanceof BaseQuestionnaireView) ||
        LF.router.controller.view.model.get('horizonId') !== curNavigate.questionnaireId){
        // hack for some odd check in questionnaire dispatch.
        Data.Questionnaire = true;
        //noinspection JSUnresolvedVariable
        if(!Backbone.history.fragment){
            Backbone.history.fragment = 'questionnaire';
        }
        const questionnaire = LF.StudyDesign.questionnaires.find({horizonId : curNavigate.questionnaireId});
        if(questionnaire){
            return LF.router.controller.questionnaire(questionnaire.id);
        }else{
            return Q();
        }
    }else{
        return Q();
    }
}
/**
 * if the studyID isn't the navigate studyID, go fetch it
 * @param {object} data - the message data
 * @param {object} curNavigate - the current navigation being performed
 * @returns {Promise} - resolved when complete
 */
function getScreensIfDifferent (data, curNavigate) {
    const currentView = LF.router.controller.view;
    // if this is still the last navigate, go to the screen
    if(lastNavigation === data.navigate && currentView instanceof BaseQuestionnaireView){
        currentView.screenStack = [];
        const screens = currentView.data.screens;
        let screen = null,
            i;
        // no sceens yet, build a fake screenstack of all screens up to and including this
        for(i = 0; i < screens.length; ++i){
            screen = screens[i];
            currentView.screenStack.push(screen.id);
            if(( screen.get('horizonId') || screen.get('id')) === curNavigate.screenId){
                break;
            }
        }
        if(i < screens.length){
            lastCompletedNavigation = curNavigate;
            return currentView.displayScreen(screen.id);
        }
    }
    return Q();
}
const msgProcessor = (data) => {
    if(data.parentHostName){
        parentHostName = data.parentHostName;
    }
    if(data.addRules){
        _.each(data.addRules, (rule) => {
            if(optionalElfRules[rule] && optionalElfRules[rule].addToElf){
                optionalElfRules[rule].addToElf();
            }
        });
    }
    //noinspection JSUnresolvedVariable
    if(data.changes){
        //noinspection JSUnresolvedVariable
        const changes = data.changes;
        if(changes.screen){
            curSdPromise = curSdPromise
                .then(() => {
                    return StudyDesignerData.getStudy(StudyDesignerData.currentStudyId);
                })
                .catch((e) => {
                    logger.error('unexpected exception while processing StudyDesign change:', e);
                });
        }
        if(changes.questionnaire){
            curSdPromise = curSdPromise
                .then(() => {
                    return StudyDesignerData.getStudy(StudyDesignerData.currentStudyId);
                })
                .catch((e) => {
                    logger.error('unexpected exception while processing StudyDesign change:', e);
                });
        }
    }
    // only navigate if we haven't loaded a config (currentStudyId == ''
    // or we are embedded (manditoryOff would be true)
    if((!StudyDesignerData.currentStudyId || StudyDesignerData.mandatoryOff) && data.navigate && data.sessionId === sessionId){
        lastNavigation = data.navigate;
        curSdPromise = curSdPromise
            .catch(unexpectedSdPromiseRejection)
            .then(() => {
                // only navigate if you are the last nevigation
                if(lastNavigation === data.navigate){
                    const curNavigate = lastNavigation;
                    return Q()
                        .then(() => {
                            return getStudyIfDifferent(curNavigate);
                        })
                        .then(() => {
                            return getQuestionnaireIfDifferent(data, curNavigate);
                        })
                        .then(() => {
                            return getScreensIfDifferent(data, curNavigate);
                        })
                        .catch((e) => {
                            logger.error('unexpected exception while processing StudyDesign navigation:', e);
                        });
                }
                return Q();
            });
    }
};
const sdChangedListener = (event) => {
    msgProcessor(event.data);
};
/**
 * queues up the processes necesasry to setup designer mode
 * @returns {promise<void>}
 */
let setupStudyDesignerMode = () => {
    // TODO: if setEnv fails put something onto the screen...
    return StudyDesignerData.setupEnv()
        .then(() => {
            if(addEventListener){
                addEventListener('message', sdChangedListener, false);
            }
            LF.Widget.ParamFunctions.FetchSDList = StudyDesignerData.FetchSDList;
            // Add screenshot router and route to screenshot view.
            ELF.rules.add({
                id      : 'StudyDesigner_dashboard_reload',
                trigger : [
                    'DASHBOARD:Open'
                ],
                evaluate () {
                    return true;
                },
                resolve : [
                    {
                        action () {
                            return StudyDesignerData.getStudy(StudyDesignerData.currentStudyId)
                                .then(() => {
                                    this.resetModels();
                                });
                        }
                    }
                ]
            });
            // Add screenshot router and route to screenshot view.
            ELF.rules.add({
                id      : 'StudyDesigner_StudyPick',
                trigger : [
                    'QUESTIONNAIRE:Completed/_SD_Config_Picker'
                ],
                evaluate () {
                    return (true);
                },
                resolve : [
                    {
                        action () {
                            //noinspection JSUnresolvedVariable
                            StudyDesignerData.currentStudyId = this.data.answers.match({question_id : 'StudyID'})[0]
                                .get('response');
                            // TODO: need to detach from any prior subscription
                            // and set this to only happen in one place.  currently happening
                            // in at least 3.   StudyDesignerData doesn't have access to msgProcessor
                            // and I need to do this in a few hours.
                            hmq.subscribe({
                                address   : StudyDesignerData.messageURL,
                                topics    : `StudyDesignChange.${StudyDesignerData.currentStudyId}`,
                                onmessage : msgProcessor
                            });
                            //noinspection JSUnresolvedVariable
                            this.navigate('dashboard', true);
                            return {preventDefault : true};
                        }
                    }
                ]
            });
        });
};
/**
 * Run to add studydesigner rule listeners.
 */
function addRules () {
    let startupQueue       = Q.defer(),
        lastStartupPromise = startupQueue.promise;
    lastStartupPromise = lastStartupPromise
        .then(() => {
            return StudyDesignerData.setCompassServiceURL();
        })
        .then(() => {
            return setupStudyDesignerMode.call(this);
        });
    // Add screenshot router and route to screenshot view.
    ELF.rules.add({
        id      : 'StudyDesigner_connector',
        trigger : [
            'CODEENTRY:Submit',
            'MODESELECT:Submit'
        ],
        evaluate (context) {
            if(context.value.substring(0, 1) === '$'){
                if(context.value.substring(1) === 'tudydesigner'){
                    return StudyDesignerData.setCompassServiceURL()
                        .then(() => {
                            StudyDesignerData.currentStudyId = '';
                            return true;
                        });
                }else if(context.value.substring(1, 8).toLowerCase() === 'http://' ||
                    context.value.substring(1, 8).toLowerCase() === 'https://'){
                    return StudyDesignerData.setCompassServiceURL(context.value.substring(1))
                        .then(() => {
                            StudyDesignerData.currentStudyId = '';
                            return true;
                        });
                }
            }
            return Q(false);
        },
        resolve : [
            {
                action () {
                    return setupStudyDesignerMode.call(this)
                        .then(() => {
                            this.navigate('login', true);
                            return {preventDefault : true};
                        })
                        .catch(() => {
                            return {preventDefault : true};
                        });
                }
            }
        ]
    });
    // Add screenshot router and route to screenshot view.
    ELF.rules.add({
        id      : 'StudyDesigner_reloader',
        trigger : [
            'APPLICATION:Loaded'
        ],
        evaluate () {
            return true;
        },
        resolve : [
            {
                action () {
                    if(StudyDesignerData.currentStudyId !== null){
                        hmq.subscribe({
                            address   : StudyDesignerData.messageURL,
                            topics    : [`StudyDesignChange.${StudyDesignerData.currentStudyId}`],
                            onmessage : msgProcessor
                        });
                        let ret = Q.Promise((resolve, reject) => {
                            lastStartupPromise = lastStartupPromise
                                .then(() => {
                                    if(!Backbone.history.fragment){
                                        // haven't set a route yet.... so  goto default
                                        LF.router.navigate('', true);
                                    }
                                    resolve({preventDefault : true});
                                })
                                .catch((err) => {
                                    reject(err);
                                    throw(err);
                                });
                        });
                        startupQueue.resolve();
                        return ret;
                    }else{
                        startupQueue = undefined;        // free up the memeory
                        return Q();
                    }
                }
            }
        ]
    });
    const sdListener = (event) => {
        const data = event.data;
        if(!data.horizonUrl){
            logger.warn('invalid command: no horizonURL Specificed.');
            return;
        }
        sessionId = data.sessionId;
        const localStorage = {'krpt' : 'krpt.39ed120a0981231'};
        window.localStorage.setItem = (key, value) => {
            localStorage[key] = value;
            return value;
        };
        window.localStorage.getItem = (key) => {
            return localStorage[key];
        };
        window.localStorage.removeItem = (key) => {
            delete localStorage[key];
        };
        Backbone.sync = dummyDataAccess;
        StudyDesignerData.horizonBase = data.horizonUrl;
        StudyDesignerData.deviceType = data.deviceType;
        // first navigate will load config
        StudyDesignerData.currentStudyId = '';
        lastStartupPromise = lastStartupPromise
            .catch(unexpectedSdPromiseRejection)
            .then(() => {
                // TODO: this is a nasty hack....   can't really use LF.router.navigate( "application#questionnaire')
                // since that is async and not promise based.  In reality, you can't actually count on it being the
                // controller named 'application'
                if(!LF.router.controllers.application){
                    throw(new Error('LF.router.controllers does not contain a controllers.application'));
                }
                // check to make sure the application controller has a questionnaire()
                if(!LF.router.controllers.application.questionnaire &&
                    typeof LF.router.controllers.application.questionnaire === 'function'){
                    // jscs:disable maximumLineLength
                    throw(new Error('LF.router.controllers does not contain a controllers.application.questionnaire'));
                    // jscs:enable maximumLineLength
                }
                LF.router.controller = LF.router.controllers.application;
                // session object holds a cache of users rather than fetching during lookup, so refresh cache
                // and to make matters worse, session.login doesn't even use it's own cached users, it uses users
                // cached in CurrentContext() so refresh that too (dtp).
                return LF.security.fetchUsers();
            })
            .then(() => {
                return CurrentContext().fetchUsers();
            })
            .then((users) => {
                //noinspection JSCheckFunctionSignatures
                return LF.security.login(users.at(0).get('id'), '1111');
            })
            .then((user) => {
                // normally done in a loginView not in login().   Why??? don't know... dpeterson
                lStorage.setItem('User_Login', user.id);
            })
            .then(() => {
                return Q.Promise((resolve) => {
                    // enqueue the navigate - adds any navigation to curSdPromise;
                    sdChangedListener(event);
                    // wait for it to be processed.  Note we don't want to return curSdPromise because more
                    // might be added before we are done.  so just add a node in curSdPromise that notifies us
                    // that it is done til there.
                    curSdPromise = curSdPromise
                        .finally(() => {
                            resolve();
                        });
                });
            });
        startupQueue.resolve();
    };
    //noinspection JSUnresolvedVariable
    if(window.addEventListener){
        addEventListener('message', sdListener, false);
    }
}
addRules();
