import Base from '../../app/core/models/StorageBase';
export default class SubjectMed extends Base {
    /**
     * @property {string} name - The model's name
     * @readonly
     * @default 'LastDiary'
     */
    get name () {
        return 'SubjectMed';
    }

    /**
     * @property {Object} schema - The model's schema used for validation and storage construction.
     * @static
     * @readonly
     */
    static get schema () {
        return {
            MEDCOD1C : {
                type     : String,
                required : false
            },
            MEDALS1C : {
                type     : String,
                required : false
            }
        };
    }
}
window.LF.Model.SubjectMed = SubjectMed;
