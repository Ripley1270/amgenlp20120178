import Base from '../../app/core/models/StorageBase';
export default class DailyDiary extends Base {
    /**
     * @property {string} name - The model's name
     * @readonly
     * @default 'LastDiary'
     */
    get name () {
        return 'DailyDiary';
    }

    /**
     * @property {Object} schema - The model's schema used for validation and storage construction.
     * @static
     * @readonly
     */
    static get schema () {
        return {
            date : {
                type     : String,
                required : false
            }
        };
    }
}
window.LF.Model.DailyDiary = DailyDiary;
