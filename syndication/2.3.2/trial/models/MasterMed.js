import Base from '../../app/core/models/StorageBase';
export default class MasterMed extends Base {
    /**
     * @property {string} name - The model's name
     * @readonly
     * @default 'LastDiary'
     */
    get name () {
        return 'MasterMed';
    }

    /**
     * @property {Object} schema - The model's schema used for validation and storage construction.
     * @static
     * @readonly
     */
    static get schema () {
        return {
            MEDCOD1C : {
                type     : String,
                required : false
            },
            MEDNAM1C : {
                type     : String,
                required : false
            },
            MEDDOS1C : {
                type     : String,
                required : false
            },
            MEDRTE1L : {
                type     : String,
                required : false
            },
            MEDFRM1L : {
                type     : String,
                required : false
            },
            MEDFRM2L : {
                type     : String,
                required : false
            },
            MEDFLG1B : {
                type     : String,
                required : false
            },
            MEDFLG2B : {
                type     : String,
                required : false
            },
            MEDFLG3B : {
                type     : String,
                required : false
            },
            MEDFLG4B : {
                type     : String,
                required : false
            },
            MEDFLG5B : {
                type     : String,
                required : false
            },
            MEDFLG6B : {
                type     : String,
                required : false
            },
            MEDFLG7B : {
                type     : String,
                required : false
            },
            MEDFLG8B : {
                type     : String,
                required : false
            },
            MEDFLG9B : {
                type     : String,
                required : false
            },
            MEDVAL1N : {
                type     : String,
                required : false
            },
            MEDVAL2N : {
                type     : String,
                required : false
            },
            MEDVAL3N : {
                type     : String,
                required : false
            },
            MEDVAL4N : {
                type     : String,
                required : false
            },
            MEDVAL5N : {
                type     : String,
                required : false
            },
            MEDVAL6N : {
                type     : String,
                required : false
            },
            MEDVAL7N : {
                type     : String,
                required : false
            },
            MEDVAL8N : {
                type     : String,
                required : false
            },
            MEDVAL9N : {
                type     : String,
                required : false
            },
            MEDLBL1C : {
                type     : String,
                required : false
            },
            MEDLBL2C : {
                type     : String,
                required : false
            },
            MEDLBL3C : {
                type     : String,
                required : false
            },
            MEDLBL4C : {
                type     : String,
                required : false
            },
            MEDLBL5C : {
                type     : String,
                required : false
            },
            MEDLBL6C : {
                type     : String,
                required : false
            },
            MEDLBL7C : {
                type     : String,
                required : false
            },
            MEDLBL8C : {
                type     : String,
                required : false
            },
            MEDLBL9C : {
                type     : String,
                required : false
            },
            MEDGRP1L : {
                type     : String,
                required : false
            }
        };
    }
}
window.LF.Model.MasterMed = MasterMed;
