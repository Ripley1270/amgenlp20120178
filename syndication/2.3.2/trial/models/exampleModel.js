import Base from '../../app/core/models/StorageBase';
export default class ExampleModel extends Base {
    /**
     * @property {string} name - The model's name
     * @readonly
     * @default 'LastDiary'
     */
    get name () {
        return 'ExampleModel';
    }

    /**
     * @property {Object} schema - The model's schema used for validation and storage construction.
     * @static
     * @readonly
     */
    static get schema () {
        return {
            dateTime  : {
                type     : String,
                required : false
            },
            response1 : {
                type     : String,
                required : false
            },
            response2 : {
                type     : String,
                required : false
            },
            response3 : {
                type     : String,
                required : false
            },
            response4 : {
                type     : String,
                required : false
            },
            response5 : {
                type     : String,
                required : false
            }
        };
    }
}
window.LF.Model.ExampleModel = ExampleModel;
