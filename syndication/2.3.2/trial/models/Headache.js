import Base from '../../app/core/models/StorageBase';
export default class Headache extends Base {
    /**
     * @property {string} name - The model's name
     * @readonly
     * @default 'LastDiary'
     */
    get name () {
        return 'Headache';
    }

    /**
     * @property {Object} schema - The model's schema used for validation and storage construction.
     * @static
     * @readonly
     */
    static get schema () {
        return {
            headacheID : {
                type     : String,
                required : false
            },
            startDate  : {
                type     : String,
                required : false
            },
            endDate    : {
                type     : String,
                required : false
            }
        };
    }
}
window.LF.Model.Headache = Headache;
