/**
 * Created by jonathan.carr on 25-Jul-17.
 */
import Base from '../../app/core/models/StorageBase';
export default class VisitModel extends Base {
    /**
     * @property {string} name - The model's name
     * @readonly
     * @default 'LastDiary'
     */
    get name () {
        return 'VisitModel';
    }

    /**
     * @property {Object} schema - The model's schema used for validation and storage construction.
     * @static
     * @readonly
     */
    static get schema () {
        return {
            visitDate : {
                type     : String,
                required : false
            },
            visit     : {
                type     : String,
                required : false
            }
        };
    }
}
window.LF.Model.VisitModel = VisitModel;
