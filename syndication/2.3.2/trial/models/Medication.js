import Base from '../../app/core/models/StorageBase';
export default class Medication extends Base {
    /**
     * @property {string} name - The model's name
     * @readonly
     * @default 'LastDiary'
     */
    get name () {
        return 'Medication';
    }

    /**
     * @property {Object} schema - The model's schema used for validation and storage construction.
     * @static
     * @readonly
     */
    static get schema () {
        return {
            medicationID : {
                type     : String,
                required : false
            },
            dateTaken    : {
                type     : String,
                required : false
            }
        };
    }
}
window.LF.Model.Medication = Medication;
