/* // CORE FILES
 import 'trial/alarm';
 import 'trial/schedule';
 // END CORE FILES */
import '../PDECommon';
import 'trial/alarm';
import assets from 'trial/assets';
import 'trial/branching';
import 'trial/collections';
import 'trial/dynamicText';
import 'trial/widgets';
import 'trial/models';
import 'trial/schedule';
import 'trial/visits';
import 'trial/expressions';
import 'trial/resources';
//import 'trial/resources/core-logpad-rules'; // TODO: KM REMOVE IF UNUSED
export default {assets};


