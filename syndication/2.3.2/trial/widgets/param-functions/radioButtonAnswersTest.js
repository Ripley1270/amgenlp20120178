import * as Helpers from 'core/Helpers';
/**
 * @file Example of a function that returns widget answers dynamically
 * @author <a href="mailto:tim.kelly@ert.com">Tim Kelly</a>
 */

/*eslint-disable no-unused-vars*/
/**
 * A function that dynamically returns answers for a radiobutton question
 * @this {Object} the widget that calls this function
 * @returns {Q.Promise|Array} array of answers or a promise containing them in the resolve method
 */
LF.Widget.ParamFunctions.radioButtonAnswersTest = function () {
    return new Q.Promise((resolve) => {
        let ret = [
            {text : 'DYNAMIC_ONE', value : '0'},
            {text : 'DYNAMIC_TWO', value : '1'},
            {text : 'DYNAMIC_THREE', value : '2'}
        ];
        // You can even do asynchronous stuff here...
        //  then call:
        resolve(ret);
    });
};
LF.Widget.ParamFunctions.checkboxAnswersTest = function () {
    return new Q.Promise((resolve) => {
        let ret = [
            {text : 'DYNAMIC_ONE', value : '0', IT : 'CB_TEST_S1_Q7_0'},
            {text : 'DYNAMIC_TWO', value : '1', IT : 'CB_TEST_S1_Q7_1'},
            {text : 'DYNAMIC_THREE', value : '2', IT : 'CB_TEST_S1_Q7_2'}
        ];
        // You can even do asynchronous stuff here...
        //  then call:
        resolve(ret);
    });
};
/**
 *  Function to return answer array based on previous question.
 */
LF.Widget.ParamFunctions.nrsAnswerFunc = function () {
    return new Q.Promise((resolve) => {
        let prev         = Helpers.getResponses('NRS_DIARY_Q11')[0].response,
            responses,
            numResponses = 0,
            answers      = [];
        if(prev === '0'){
            numResponses = 3;
        }else{
            numResponses = 4;
        }
        for(let i = 0; i < numResponses; i += 1){
            answers.push({
                text  : `NRS_DYN_${i}`,
                value : i.toString()
            });
        }
        resolve(answers);
    });
};
LF.Widget.ParamFunctions.checkboxClearAnswers = function (questionId, scheduleId, response) {
    return new Q.Promise((resolve) => {
        let i              = 0,
            prevAnswers    = Helpers.getResponses('CHECKBOX_TEST_S7_Q1'),
            q1Answer,
            q1Response,
            answers        = [],
            widget         = Helpers.getWidget('CHECKBOX_TEST_S7_Q1'),
            oldAnswers     = widget.model.get('answers'),
            currentWidget  = Helpers.getWidget('CHECKBOX_TEST_S8_Q1'),
            currentAnswers = currentWidget.model.get('answers');
        if(!prevAnswers){
            q1Response = -1;
        }else{
            let a = [];
            for(let i = 0, l = prevAnswers.length; i < l; i += 1){
                a.push(prevAnswers[i].response);
            }
            q1Response = a.toString();
        }
        if(q1Response === '1,0,0'){
            for(; i <= 1; i += 1){
                answers.push({
                    text  : `DAILY_${i}`,
                    value : i.toString(),
                    IT    : `CB_TEST_S1_S8_${i}`
                });
            }
        }else if(q1Response === '0,1,0'){
            for(; i <= 2; i += 1){
                answers.push({
                    text  : `DAILY_${i}`,
                    value : i.toString(),
                    IT    : `CB_TEST_S1_S8_${i}`
                });
            }
        }else if(q1Response === '0,0,1'){
            for(; i <= 3; i += 1){
                answers.push({
                    text  : `DAILY_${i}`,
                    value : i.toString(),
                    IT    : `CB_TEST_S1_S8_${i}`
                });
            }
        }
        // If we haven't populated the answer array yet, just use what was pulled from the model
        if(!_.isEqual(answers, oldAnswers) && answers.length === 0){
            answers = currentAnswers;
        }
        resolve(answers);
    });
};
LF.Widget.ParamFunctions.radioButtonClearAnswers = function (questionId, scheduleId, response) {
    return new Q.Promise((resolve) => {
        let i              = 0,
            q1Answer       = Helpers.getWidget('RADIOBUTTON_TEST_S7_Q1').answer,
            q1Response     = (q1Answer !== undefined ? q1Answer.get('response') : -1),
            answers        = [],
            widget         = Helpers.getWidget('RADIOBUTTON_TEST_S7_Q1'),
            oldAnswers     = widget.model.get('answers'),
            currentWidget  = Helpers.getWidget('RADIOBUTTON_TEST_S8_Q1'),
            currentAnswers = currentWidget.model.get('answers');
        if(q1Response === '0'){
            for(; i <= 2; i += 1){
                answers.push({
                    text  : `DAILY_${i}`,
                    value : i.toString()
                });
            }
        }else if(q1Response === '1'){
            for(; i <= 3; i += 1){
                answers.push({
                    text  : `DAILY_${i}`,
                    value : i.toString()
                });
            }
        }else if(q1Response === '2'){
            for(; i <= 4; i += 1){
                answers.push({
                    text  : `DAILY_${i}`,
                    value : i.toString()
                });
            }
        }
        if(!_.isEqual(answers, oldAnswers) && answers.length > 0){
            // widget.removeAnswer(response);
            widget.answer = undefined;
        }else{
            answers = currentAnswers;
        }
        resolve(answers);
    });
};
LF.Widget.ParamFunctions.radioButtonAnswers2 = function () {
    let i          = 0,
        q1Answer   = LF.Helpers.getWidget('RADIOBUTTON_TEST_S6_Q1').answer.get('response'),
        answers    = [],
        widget     = this,
        oldAnswers = widget.model.get('answers'),
        response   = (LF.Helpers.getResponses(this.model.id) && LF.Helpers.getResponses(this.model.id).length > 0)
            ? LF.Helpers.getResponses(this.model.id)[0].response
            : undefined;
    for(; i <= q1Answer; i++){
        answers.push({
            text  : 'RADIO_' + i,
            value : i.toString()
        });
    }
    if(!_.isEqual(answers, oldAnswers)){
        widget.removeAnswer(response);
        widget.answer = undefined;
    }
    return answers;
};
LF.Widget.ParamFunctions.customCheckBoxAnswers = function (questionId, scheduleId, response) {
    let i         = 0,
        answers   = [],
        widget    = LF.Helpers.getWidget(questionId),
        q1Answer  = LF.Helpers.getResponses('TOSS_Q_0'),
        k         = q1Answer.length,
        lastValue = q1Answer[k - 1];
    k = (lastValue.response === '1') ? 4 : k;
    for(; i <= k; i++){
        answers.push({
            text  : 'TOSS_' + i,
            value : i.toString(),
            IT    : 'TOSS_Q_1_' + i
        });
    }
    return answers;
};
LF.Widget.ParamFunctions.checkboxAnswers3 = function (questionId, scheduleId, response) {
    let i          = 0,
        answers    = [],
        q0widget   = LF.Helpers.getWidget('TOSS_Q_0'),
        widget     = LF.Helpers.getWidget('TOSS_Q_1'),
        q0Answers  = LF.Helpers.getResponses('TOSS_Q_0'),
        k          = q0Answers.length,
        oldAnswers = widget.model.get('answers');
    k = q0widget.skipped ? 3 : k;
    for(; i <= k; i++){
        answers.push({
            text  : 'TOSS_' + i,
            value : i.toString(),
            IT    : 'TOSS_Q_1_' + i
        });
    }
    if(!_.isEqual(answers, oldAnswers)){
        widget.removeAllAnswers();
        widget.lastResponses = undefined;
    }
    return answers;
};
LF.Widget.ParamFunctions.customTossAnswers3 = function (questionId, scheduleId, response) {
    var i         = 0,
        answers   = [],
        q1Answer  = LF.Helpers.getResponses('TOSS_Q_2'),
        k         = q1Answer.length,
        lastValue = q1Answer[k - 1];
    k = (lastValue.response === '1') ? 3 : k;
    for(; i < k; i++){
        answers.push({
            text  : `TOSS_${i}`,
            value : i.toString(),
            IT    : `TOSS_Q_3_${i}`
        });
    }
    return answers;
};
