/**
 * @file Example of a function that returns widget parameters dynamically
 * @author <a href="mailto:lsiegert@phtcorp.com">Lauren Siegert</a>
 * @version 1.7
 */

LF.Widget.ParamFunctions.dropDownListItems = function () {
    return new Q.Promise((resolve) => {
        resolve([
            {
                value : 1,
                text  : 'Custom_1'
            },
            {
                value : 2,
                text  : 'Custom_2'
            },
            {
                value : 3,
                text  : 'Custom_3'
            },
            {
                value : 4,
                text  : 'Custom_4'
            },
            {
                value : 5,
                text  : 'Custom_5'
            }
        ]);
    });
};
LF.Widget.ParamFunctions.dropDownListDefaultVal = function () {
    return new Q.Promise((resolve) => {
        resolve(4);
    });
}
/**
 * @param {string} questionId - The ID of the question
 */
LF.Widget.ParamFunctions.dropDownListTest = function () {
    return new Q.Promise((resolve) => {
        let params = {
            items      : [
                {
                    value : 1,
                    text  : 'Custom_1'
                },
                {
                    value : 2,
                    text  : 'Custom_2'
                },
                {
                    value : 3,
                    text  : 'Custom_3'
                },
                {
                    value : 4,
                    text  : 'Custom_4'
                },
                {
                    value : 5,
                    text  : 'Custom_5'
                }
            ],
            defaultVal : 4
        };
        resolve(params);
    });
};
