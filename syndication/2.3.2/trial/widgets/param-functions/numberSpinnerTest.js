/**
 * @file Example of a function that returns widget parameters dynamically
 * @author <a href="mailto:lsiegert@phtcorp.com">Lauren Siegert</a>
 * @version 1.7
 */
/**
 * @param {string} questionId - The ID of the question
 */
LF.Widget.ParamFunctions.FunctionName = function (questionId) {
    return new Promise((resolve) => {
        let params = {
            spinnerInputOptions : [
                {
                    min  : 0,
                    max  : 400,
                    step : 5
                }
            ],
            defaultVal          : 5
        };
        resolve(params);
    });
};
