////////////////////////////////////////////////////////
//Define Loop Information
//////////////////////////////////////////////////////
LF.Widget.ReviewScreen.ExampleLoopInfo = {
    IG           : 'ExampleLoopIG',//Looping IG
    ReviewScreen : 'EXLOOP010',
    StartOfLoop  : 'EXLOOP020', //first screen as part of the loop after Review Screen
    EndOfLoop    : 'EXLOOP060',//This is the first screen no longer part of the loop
    DeleteScreen : 'EXLOOP050',//Delete Conf Screen
    Max_Loops    : 20,// Max Loops Allowed
    HistoricData : false, //Set to true if using historic data
    Collection   : 'ExampleCollection',//Collection to use for Historic Data. Keep as ExampleCollection if not using
                                       // historic data
    Model        : 'ExampleModel',//Model to use for Historic Data. Keep as ExampleModel if not using historic data
    Questions    : ['EXLOOP020', 'EXLOOP030', 'EXLOOP040']//List of Questions to Display
};
//////////////////////////////////////////////////////
//Start Example Loop
//////////////////////////////////////////////////////
LF.Widget.ReviewScreen.ExampleLoop = (widget) => {
    return LF.Widget.ReviewScreen.loadLoopScreen(
        LF.Widget.ReviewScreen.ExampleLoopInfo.IG,
        LF.Widget.ReviewScreen.ExampleLoopInfo.ReviewScreen,
        LF.Widget.ReviewScreen.ExampleLoopItems,
        widget);
};
LF.Widget.ReviewScreen.getExampleLoopReviewData = (IG, addIGR) => {
    let index             = LF.Data.Questionnaire.getIGR(IG),
        Reading,
        //change to true when using historic data
        useHistoricData   = false,
        collectionPromise = useHistoricData ? LF.Utilities.getCollection(LF.Widget.ReviewScreen.ExampleLoopInfo.Collection) : Q(new LF.Collection[LF.Widget.ReviewScreen.ExampleLoopInfo.Collection]);
    return collectionPromise
        .then((Readings) => {
            //grab array of models from collection
            Readings = Readings.models;
            let questions = LF.Widget.ReviewScreen.ExampleLoopInfo.Questions;
            for(let i = index; i > 0; i--){
                let value0 = LF.Data.Questionnaire.queryAnswersByQuestionIDAndIGR(questions[0], i)[0],
                    value1 = LF.Data.Questionnaire.queryAnswersByQuestionIDAndIGR(questions[1], i)[0],
                    value2 = LF.Data.Questionnaire.queryAnswersByQuestionIDAndIGR(questions[2], i)[0];
                if(value0 && value1 && value2){
                    Reading = new LF.Model[LF.Widget.ReviewScreen.ExampleLoopInfo.Model]({
                        //match model schema when using historic data, otherwise use whatever naming makes sense.
                        dateTime  : value0.get('response'),
                        response1 : value1.get('response'),
                        response2 : value2.get('response'),
                        //extra piece for loop identification
                        igr       : (addIGR) ? i : null
                    });
                    Readings.models.push(Reading);
                }
            }
            //Filter and sort array here
            let readingsArray = Readings.sort();
            return readingsArray;
        });
};
LF.Widget.ReviewScreen.ExampleLoopItems = (IG) => {
    let result         = [],
        stringsToFetch = {
            //add Strings to fetch for various display
            NO_MEDS     : 'NO_MEDS',
            ITEM_FORMAT : 'reviewScreenItemFormat',
            TIME_FORMAT : 'reviewScreenTimeFormat',
            OtherMed    : 'OtherMed'
        };
    //add {namespace : 'NameSpace'} to getStrings if applicable
    return LF.getStrings(stringsToFetch)
        .then((resources) => {
            return LF.Widget.ReviewScreen.getExampleLoopReviewData(IG, true)
                .then((readingsArray) => {
                    let size = readingsArray.length;
                    if(size === 0){
                        //If there are no readings, display a message
                        result.push({
                            'classes' : 'not-selectable',
                            'id'      : '-1',
                            'text'    : [resources.NO_MEDS, '', '', '']
                        });
                    }else{
                        /*
                         process each item into a display format
                         Each item will be an object containing:
                         classes : string with classnames for CSS purposes
                         id : igr value
                         text: array strings, the number of elements in the array should be the number of columns in the review screen
                         */
                        for(let i = size - 1; i >= 0; i--){
                            let Reading = readingsArray[i],
                                value0  = Reading.get('dateTime'),
                                value1  = Reading.get('response1'),
                                value2  = Reading.get('response2'),
                                igr     = Reading.get('igr'),
                                item    = {classes : ''};
                            //If no igr, push to negative number so that the item cannot be picked in the review screen
                            if(!igr){
                                igr = -1;
                                item.classes += ' not-selectable';
                            }
                            /*Perform whatever transformations on the values are necessary for display*/
                            value0 = value0.toString();
                            value1 = value1.toString();
                            value2 = value2.toString();
                            /*add id to item*/
                            item.id = igr;
                            //item.text -> each index is a column
                            //item.text = [new Date(value1).format('timeOutput'), 'value2', 'value3'];
                            item.text = [value0, value1, value2];
                            result.push(item);
                        }
                    }
                    return result;
                });
        });
};
LF.Widget.ReviewScreen.addNewExampleLoop = () => {
    let answers = LF.Data.Questionnaire.queryAnswersByIGAndIGR(LF.Widget.ReviewScreen.ExampleLoopInfo.IG, LF.Widget.ReviewScreen.ExampleLoopInfo.Max_Loops);
    if(answers.length > 0){
        LF.Utilities.navigateToScreen(LF.Widget.ReviewScreen.ExampleLoopInfo.EndOfLoop);
    }else{
        LF.Utilities.navigateToScreen(LF.Widget.ReviewScreen.ExampleLoopInfo.StartOfLoop);
    }
};
LF.Widget.ReviewScreen.editDailyDiary = (params) => {
    LF.Widget.ReviewScreen.beginEditLoop(LF.Widget.ReviewScreen.ExampleLoopInfo.IG, LF.Widget.ReviewScreen.ExampleLoopInfo.StartOfLoop, params);
};
LF.Widget.ReviewScreen.deleteDailyDiary = (params) => {


    /*//This section is to delete the entry instantly; no confirmation screen necessary
     LF.Widget.ReviewScreen.deleteLoop(LF.Widget.ReviewScreen.DailyDiaryLoopInfo.IG, LF.Utilities.getReviewScreenItemID(params.item));
     params.question.widget.render();*/
    LF.Widget.ReviewScreen.beginEditLoop(LF.Widget.ReviewScreen.ExampleLoopInfo.IG, LF.Widget.ReviewScreen.ExampleLoopInfo.DeleteScreen, params);
};
