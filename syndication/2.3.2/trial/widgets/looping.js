LF.Widget.ValidationFunctions.requireOneLoopInPractice = function (answer, params, callback) {
    let questionnaire = LF.Data.Questionnaire.Dashboard.get('questionnaire_id');
    if(questionnaire === 'TrainingModule' && !localStorage.getItem('ScreenshotMode')){
        let answersByCurrentIGR = LF.Utilities.queryAnswersByIGAndIGR(params.IG, 1);
        if(answersByCurrentIGR.length){
            callback(true);
        }else{
            LF.getStrings('REQUIRED_ANSWER', function (string) {
                LF.Notify.Banner.show({
                    text : string,
                    type : 'error'
                });
            });
            callback(false);
        }
    }else{
        callback(true);
    }
};
LF.Widget.ValidationFunctions.always = function (answer, params, callback) {
    callback(true);
};
LF.Widget.ReviewScreen.editNotAvailable = function (widget) {
    //Return the opposite of checkIfRowIsEditable
    return Q().then((resolve) => {
        if(widget.activeItem){
            let IGR = LF.Utilities.getReviewScreenItemID(widget.activeItem);
            return IGR <= 0;
        }else{
            return true;
        }
    });
};
LF.Widget.ReviewScreen.checkIfRowIsEditable = function (widget) {
    return Q().then(() => {
            if(widget.activeItem){
                let IGR = LF.Utilities.getReviewScreenItemID(widget.activeItem);
                return IGR > 0;
            }else{
                return false;
            }
        }
    );
};
LF.Widget.ReviewScreen.checkIfRowIsSelected = function (widget) {
    return Q().then(() => {
            if(widget.activeItem){
                let IGR = LF.Utilities.getReviewScreenItemID(widget.activeItem);
                if(IGR > 0){
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }
    );
};
LF.Widget.ReviewScreen.loadLoopScreen = function (IG, screenID, itemsFunction, params, widget) {
    let currentIGR,
        answersByIGR;
    // if backing out from edit mode, discard edited version as user backed out
    if(LF.Data.Questionnaire.editing){
        LF.Data.Questionnaire.removeLoopEvent(IG, -1);
    }
    LF.Data.Questionnaire.editing = null;
    LF.Data.Questionnaire.updateIGR(IG);
    // This determines if the an event was started and then abandoned by backing out.
    // In this case the partly answered event is removed.
    currentIGR = LF.Data.Questionnaire.getCurrentIGR(IG);
    answersByIGR = LF.Data.Questionnaire.queryAnswersByIGAndIGR(IG, currentIGR);
    if(answersByIGR.length > 1){
        // Removes partly answered event
        LF.Data.Questionnaire.clearLoopScreenFromStack(IG);
        LF.Data.Questionnaire.removeLoopEvent(IG, currentIGR);
        LF.Utilities.navigateToScreen(screenID);
        return;
    }
    return Q.Promise((resolve) => {
        itemsFunction(IG).then((items) => {
            resolve(items);
        });
    });
};
LF.Widget.ReviewScreen.saveLoop = function (IG) {
    let currentIGR,
        answersByIGR;
    // If the editing was happening, then this is the place where edited answers are
    // commited and saved into Answers collection
    if(LF.Data.Questionnaire.editing){
        // Remove the original version
        LF.Data.Questionnaire.removeLoopEvent(IG, LF.Data.Questionnaire.editing);
        // change the edited version to original version
        LF.Data.Questionnaire.changeIGR(IG, -1, LF.Data.Questionnaire.editing);
        // sets the flag to null which indicates the editing has been completed
        LF.Data.Questionnaire.editing = null;
    }
    // Update the IGR incase navigating to review screen from edit mode.
    LF.Data.Questionnaire.updateIGR(IG);
    //clears the stack before review screen is put on the stack.
    LF.Utilities.clearLoopScreenFromStack(IG);
    // get the current IGR
    currentIGR = LF.Data.Questionnaire.getCurrentIGR(IG);
    // gets all answers for the current IGR
    answersByIGR = LF.Data.Questionnaire.queryAnswersByIGAndIGR(IG, currentIGR);
    if(answersByIGR.length){
        // increments if the current IGR is already completed, so next available IGR becomes current
        LF.Data.Questionnaire.incrementIGR(IG);
    }
};
LF.Widget.ReviewScreen.beginEditLoop = function (IG, startScreen, params) {
    // getting id from the selected item and using it as IGR. This works because
    // when creating review screen item, IGRs were used as item ids which makes
    // it easier and doesn't require any mapping.
    let IGR = LF.Utilities.getReviewScreenItemID(params.item);
    // Preserve the IGR which is being edited
    LF.Data.Questionnaire.editing = IGR;
    // Copy all the Answers and Question Views from IGR which is being
    // edited to placeholder (-1)
    LF.Data.Questionnaire.copyIGR(IG, IGR, -1);
    // set the IGR to current and navigate to screen 3.
    // setting IGR will fetch the correct answer previously recorded.
    LF.Data.Questionnaire.setCurrentIGR(IG, -1);
    LF.Utilities.navigateToScreen(startScreen);
};
LF.Widget.ReviewScreen.deleteAllLoops = function (IG) {
    let IGR = LF.Data.Questionnaire.getIGR(IG);
    while(IGR > 0){
        LF.Widget.ReviewScreen.deleteLoop(IG, IGR);
        IGR--;
    }
};
LF.Widget.ReviewScreen.deleteLoop = function (IG, IGR) {
    // Removing the event, this utility function removes answers from
    // LF.Data.Questionnaire.Answers and also removes Question views instances
    // from LF.Data.Questionnaire.QuestionViews for IGR to be deleted.
    LF.Data.Questionnaire.removeLoopEvent(IG, IGR);
    // Ordering IGR must be done so all IGR are in sequence. this utility function
    // order igr on all Answer records and also changes the igr property on
    // all question view instances
    LF.Data.Questionnaire.orderIGR(IG, IGR);
    // decrement the igr to reflect the change in the count
    LF.Data.Questionnaire.decrementIGR(IG);
};
LF.Widget.ReviewScreen.sortAndFilterCollection = function (collection, sortKeys, filterKey, filterNumDays) {
    for(let i = 0; i < sortKeys.length; i++){
        if(i == sortKeys.length - 1){
            //TODO: we are assuming last key is a date
            collection.comparator = function (model) {
                if(sortKeys[i] == 'time'){
                    return new Date('01 Jan 1900 ' + model.get(sortKeys[i])).getTime();
                }else{
                    return new Date(model.get(sortKeys[i])).getTime();
                }
            };
        }else{
            collection.comparator = function (model) {
                return Number(model.get(sortKeys[i]));
            };
        }
        collection.sort();
        //Ascending
        collection.models.reverse();
    }
    //Min data boundary is current Datetime - filterNumDays
    let minDataBoundary = new Date(LF.Data.Questionnaire.Dashboard.get('started'));
    minDataBoundary.setDate(minDataBoundary.getDate() - filterNumDays);
    //Filter readings
    collection = _.filter(collection.models, function (model) {
        return (LF.Utilities.dateDiffInDays(new Date(model.get(filterKey)), minDataBoundary, false) >= 0);
    });
    return collection;
};
