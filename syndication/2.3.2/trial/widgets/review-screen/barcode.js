LF.Widget.ReviewScreen.barCodeList = function (widget, callback) {
    return new Q.Promise((resolve) => {
        let text1 = LF.Helpers.getResponses('BARCODE_Q_1')[0].response,
            text2 = LF.Helpers.getResponses('BARCODE_Q_2')[0] ? LF.Helpers.getResponses('BARCODE_Q_2')[0].response : '',
            text3 = LF.Helpers.getResponses('BARCODE_Q_3')[0] ? LF.Helpers.getResponses('BARCODE_Q_3')[0].response : '',
            items = [];
        items.push({
            id   : '1',
            text : [text1, text2, text3]
        });
        resolve(items);
    });
};
