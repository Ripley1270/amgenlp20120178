LF.Widget.ReviewScreen.branchingReview = (widget, callback) => {
    return new Q.Promise((resolve) => {
        let responseObj     = [],
            questions       = ['WELCOME_Q_1', 'WELCOME_Q_2', 'WELCOME_Q_3'],
            branchQuestions = ['WELCOME_Q_1_A', 'WELCOME_Q_2_A', 'WELCOME_Q_3_A'],
            formatStrings   = ['Smoke', 'Alcoholic Beverages', 'Drugs'],
            responseStrings = ['All of the time', 'Most of the time', 'Some of the time', 'A little of the time'],
            count           = 1,
            index,
            responseText,
            response;
        questions.forEach((question) => {
            index = questions.indexOf(question);
            response = LF.Helpers.getResponses(question)[0].response;
            if(response === '0'){
                responseText = `${formatStrings[index]} - ${responseStrings[parseInt(LF.Helpers.getResponses(branchQuestions[index])[0].response)]}`;
                if(question === 'WELCOME_Q_3'){
                    responseObj.push({id : String(count++), exclude : ['2'], text : [responseText]});
                }else{
                    responseObj.push({id : String(count++), text : [responseText]});
                }
            }else{
                responseText = `${formatStrings[index]} - No`;
                if(question === 'WELCOME_Q_3'){
                    responseObj.push({id : String(count++), exclude : ['2'], text : [responseText]});
                }else{
                    responseObj.push({id : String(count++), text : [responseText]});
                }
            }
        });
        resolve(responseObj);
    });
};
LF.Widget.ReviewScreen.editAction_Welcome = (params, callback = $.noop) => {
    return new Q.Promise((resolve) => {
        let screens   = ['WELCOME_S_1', 'WELCOME_S_2', 'WELCOME_S_3'],
            index     = params.item.attr('data-id'),
            screenNdx = 0,
            i;
        LF.router.view.screenStack = [];
        for(i = 0; i < parseInt(index); i++){
            LF.router.view().screenStack.push(screens[i]);
        }
        // Find screen by ID
        _.each(LF.router.view().data.screens, (item, idx) => {
            if(item.id === screens[parseInt(index) - 1]){
                screenNdx = idx;
                return;
            }
        });
        LF.router.view().screen = screenNdx;
        LF.router.view().displayScreen(screens[parseInt(index) - 1]);
        resolve();
    });
};
LF.Widget.ReviewScreen.alertAction_Welcome = (params, callback = $.noop) => {
    return new Q.Promise((resolve) => {
        LF.Actions.notify({message : {key : 'ALERT_MESSAGE', namespace : 'P_Welcome'}}, () => resolve());
    });
};
