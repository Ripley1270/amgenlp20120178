import dynamicText from 'core/dynamicText';
import * as Helpers from 'core/Helpers';
LF.Widget.ReviewScreen.defaultSummary = (widget, callback) => {
    return new Q.Promise((resolve) => {
        let questions = ['NRS_DIARY_Q1', 'NRS_DIARY_Q2', 'NRS_DIARY_Q3'],
            count     = 1,
            items     = [];
        questions.forEach((question) => {
            let responses = LF.Helpers.getResponses(question),
                responseText;
            responseText = (responses && responses.length > 0) ? responses[0].response : null;
            if(question === 'NRS_DIARY_Q3'){
                items.push({
                    id      : String(count++),
                    exclude : ['averageBtnId'],
                    text    : [responseText]
                });
            }else{
                items.push({
                    id   : String(count++),
                    text : [responseText]
                });
            }
        });
        resolve(items);
    });
};
LF.Widget.ReviewScreen.alertAction_NRS = (params, callback = $.noop) => {
    return new Q.Promise((resolve) => {
        LF.Actions.notify({message : {key : 'ALERT_MESSAGE_1', namespace : 'P_NRS_Diary'}}, () => resolve());
    });
};
LF.Widget.ReviewScreen.averageAction = (params, callback = $.noop) => {
    return new Q.Promise((resolve) => {
        LF.Actions.notify({message : {key : 'AVERAGE_DATA', namespace : 'P_NRS_Diary'}}, () => resolve());
    });
};
dynamicText.add({
    id          : 'averageData',
    evaluate    : () => {
        let answer1   = LF.Helpers.getResponses('NRS_DIARY_Q1')[0],
            answer2   = LF.Helpers.getResponses('NRS_DIARY_Q2')[0],
            answer3   = LF.Helpers.getResponses('NRS_DIARY_Q3')[0],
            response1 = parseInt(answer1.response, 10),
            response2 = parseInt(answer2.response, 10),
            response3 = parseInt(answer3.response, 10);
        return (response1 + response2 + response3) / 3;
    },
    screenshots : {
        values : ['20']
    }
});
LF.DynamicText.averageData_old = (callback) => {
    let answer1   = LF.Helpers.getResponses('NRS_DIARY_Q1')[0],
        answer2   = LF.Helpers.getResponses('NRS_DIARY_Q2')[0],
        answer3   = LF.Helpers.getResponses('NRS_DIARY_Q3')[0],
        response1 = parseInt(answer1.response),
        response2 = parseInt(answer2.response),
        response3 = parseInt(answer3.response);
    return Q((response1 + response2 + response3) / 3);
};
/**
 * Fetch the first three question's answers and return it with callback.
 * @param {Object} widget Review screen widget option.
 * @param {Function} callback The function to be invoked with array of Review screen
 * item.
 */
LF.Widget.ReviewScreen.painNRS = (widget, callback = $.noop) => {
    return new Q.Promise((resolve) => {
        let text1 = LF.Helpers.getWidget('NRS_DIARY_Q1').answer.get('response'),
            text2 = LF.Helpers.getWidget('NRS_DIARY_Q2').answer.get('response'),
            text3 = LF.Helpers.getWidget('NRS_DIARY_Q3').answer.get('response'),
            items = [];
        items.push({
            id   : '1',
            text : [parseInt(text1), parseInt(text2), parseInt(text3)]
        });
        resolve(items);
    });
};
LF.Widget.ReviewScreen.nrsAlertAction = (params, callback = $.noop) => {
    return new Q.Promise((resolve) => {
        return LF.Actions.notify({
            message : {key : 'ALERT_MESSAGE', namespace : 'P_NRS_Diary'}
        }, () => resolve());
    });
};
LF.Widget.ParamFunctions.nrsAnswers = function () {
    return new Q.Promise((resolve) => {
        let ret = [
            {text : 'NRS_0_CHAR', value : '0'},
            {text : 'NRS_1_CHAR', value : '1'},
            {text : 'NRS_2_CHAR', value : '2'},
            {text : 'NRS_3_CHAR', value : '3'},
            {text : 'NRS_4_CHAR', value : '4'},
            {text : 'NRS_5_CHAR', value : '5'}
        ];
        resolve(ret);
    });
};
LF.Widget.ParamFunctions.nrsAnswers2 = function () {
    let i          = 0,
        answers    = [],
        q1Answer   = [],
        widget     = this,
        oldAnswers = widget.model.get('answers'),
        response   = LF.Helpers.getResponses(this.model.id)
            ? LF.Helpers.getResponses(this.model.id)[0].response
            : undefined;
    if(LF.Helpers.getWidget('NRS_DIARY_Q11').answer){
        q1Answer = LF.Helpers.getWidget('NRS_DIARY_Q11').answer.get('response');
    }
    for(; i <= q1Answer; i++){
        answers.push({
            text  : 'NRS_' + i,
            value : i.toString()
        });
    }
    if(!_.isEqual(answers, oldAnswers)){
        widget.removeAnswer(response);
        widget.answer = undefined;
    }
    return answers;
};
