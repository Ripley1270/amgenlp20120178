/**
 * @file Example of Looping Diaries.
 * @author <a href="mailto:mkamthewala@phtcorp.com">Mansoor Kamthewala</a>
 * @version 1.7.0
 */
/**
 * This function will allow the insertion of a new IGR based on the selected item in the review Screen.
 * It gets the IGR for the selected item and then creates space for it to be used..
 * @param {Object} params Review screen widget option.
 * item.
 */
LF.Widget.ReviewScreen.insertMedLoop1 = (params) => {
    let view   = LF.router.view(),
        screen = 'LOOP1_S_3',
        IG     = 'Loop1B',
        IGR    = LF.Utilities.getReviewScreenItemID(params.item);
    LF.Data.Questionnaire.setCurrentIGR(IG, -1);
    LF.Data.Questionnaire.inserting = true;
    LF.Data.Questionnaire.targetInsertionIndex = IGR;
    view.screenStack.push(screen);
    view.displayScreen(screen);
};
/**
 * This function determines the items to be displayed on the Review Screen. It fetches the
 * response for the medication and then the response is used to fetch the correct
 * medication string. Also the time is displayed which is chosen when this medication
 * was consumed.
 * @returns {Q.Promise<Array>}
 */
LF.Widget.ReviewScreen.medsLoop1 = function () {
    return new Q.Promise((resolve) => {
        let currentIGR,
            answersByIGR,
            IG            = 'Loop1B',
            questionnaire = LF.Data.Questionnaire;
        let medsList       = questionnaire.data.questions.at(2).get('widget').answers,
            result         = [],
            stringsToFetch = [],
            index          = 1;
        // if backing out from edit mode, discard edited version as user backed out
        if(questionnaire.editing){
            questionnaire.removeLoopEvent(IG, -1);
            questionnaire.editing = null;
        }
        if(questionnaire.inserting){
            questionnaire.removeLoopEvent(IG, -1);
            questionnaire.inserting = false;
        }
        questionnaire.updateIGR(IG);
        // This determines if the an event was started and then abandoned by backing out.
        // In this case the partly answered event is removed.
        // currentIGR = questionnaire.getCurrentIGR(IG);
        currentIGR = questionnaire.getCurrentIGR(IG);
        answersByIGR = questionnaire.queryAnswersByIGR(currentIGR);
        if(answersByIGR.length > 0){
            // Removes partly answered event
            questionnaire.clearLoopScreenFromStack(IG);
            questionnaire.removeLoopEvent(IG, currentIGR);
            questionnaire.displayScreen('LOOP1_S_2');
            resolve(result);
        }
        // Rest of the code is only for determining items to be displayed on Review screen.
        // This evaluate all the events and create an object literal with item ID (using IGR as ID)
        // and array of text which will be displayed. The evaluation starts with IGR 1 and iterate
        // until all events are evaluated.
        while(index){
            let answers = questionnaire.queryAnswersByIGR(index),
                item    = {};
            if(answers.length){
                let rb, tp;
                for(let i = 0; i < answers.length; i++){
                    if(answers[i].get('question_id') === 'LOOP1_Q3'){
                        rb = answers[i];
                    }else{
                        tp = answers[i];
                    }
                }
                item.id = index;
                item.text = ['', tp.get('response')];
                if(LF.Data.Questionnaire.screen === 6){
                    stringsToFetch.push(`${medsList[rb.get('response')].text} - SW_Alias - ${rb.get('SW_Alias')}`);
                }else{
                    stringsToFetch.push(medsList[rb.get('response')].text);
                }
                result.push(item);
                index++;
            }else{
                index = false;
            }
        }
        // added to assist test automation team for looping automation tests
        LF.Data.Questionnaire.$('#LOOP1_W2').append(`<p id="igr_display">Loop1B - ${currentIGR}</p>`);
        LF.getStrings(stringsToFetch, (strings) => {
            let i;
            // replace with localized strings
            for(i = 0; i < strings.length; i++){
                result[i].text[0] = strings[i];
            }
            // callback with results to be displayed on the review screen.
            // callback(result);
            resolve(result);
        }, {namespace : 'P_Loop1'});
    });
};
/**
 * This function executes and decide if the limit has reached for invoke the call back function
 * with true or false for either to put the "Add Medication" button or not.
 * @returns {Q.Promise<void>}
 */
LF.Widget.ReviewScreen.limitReached = function () {
    return new Q.Promise((resolve) => {
        LF.Data.Questionnaire.getIGR('Loop1B') > 15 ? resolve(false) : resolve(true);
    });
};
/**
 * This function executes and navigate the user to the Screen 3 which is the first screen
 * of the loop event.
 */
LF.Widget.ReviewScreen.addNewMedsLoop1 = function () {
    // LF.Data.Questionnaire.displayScreen('LOOP1_S_3', callback);
    let view   = LF.router.view(),
        screen = 'LOOP1_S_3';
    view.screenStack.push(screen);
    view.displayScreen(screen);
};
/**
 * This function executes and navigate the user to the Screen 3 which is the first screen
 * of the loop event, but after setting the IGR based on the item selected on the Review
 * screen. This way user enter into edit mode for selected IGR.
 * @param {Object} params An Object which contains button configuration for the button which was
 * clicked from the Review Screen, item which is an item selected on the review screen and it is
 * null if no item is selected. The object also contains question on which the review screen is configured.
 * @returns {Q.Promise<boolean>}
 */
LF.Widget.ReviewScreen.editMedLoop1 = function (params) {
    return new Q.Promise((resolve) => {
        let IG  = 'Loop1B',
            // getting id from the selected item and using it as IGR. This works because
            // when creating review screen item, IGRs were used as item ids which makes
            // it easier and doesn't require any mapping.
            IGR = LF.Utilities.getReviewScreenItemID(params.item);
        // Preserve the IGR which is being edited
        LF.Data.Questionnaire.editing = IGR;
        // Copy all the Answers and Question Views from IGR which is being
        // edited to placeholder (-1)
        LF.Data.Questionnaire.copyIGR(IG, IGR, -1);
        // set the IGR to current and navigate to screen 3.
        // setting IGR will fetch the correct answer previously reccorded.
        LF.Data.Questionnaire.setCurrentIGR(IG, -1);
        LF.Data.Questionnaire.screenStack.push('LOOP1_S_3');
        LF.Data.Questionnaire.displayScreen('LOOP1_S_3');
        resolve(true);
    });
};
/**
 * This function executes and delete the event based on the item selected on the review screen.
 * This function removes the loop event, then order the IGRs for answers recorded after the
 * deleted event. Then the IGR from the IG List for this IG is decremented as one item has been
 * removed and then before displaying the review screen again (to reflect the changes), stack is
 * cleared for this IG to avoid duplicate Review Screen ID on the stack.
 * @param {Object} params An Object which contains button configuration for the button which was
 * clicked from the Review Screen, item which is an item selected on the review screen and it is
 * null if no item is selected. The object also contains question on which the review screen is configured.
 * @param {Function} callback The function needs to be called to finish the Rule which triggered this action.
 */
LF.Widget.ReviewScreen.deleteMedLoop1 = function (params, callback) {
    let IG  = 'Loop1B',
        // getting id from the selected item and using it as IGR. This works because
        // when creating review screen item, IGRs were used as item ids which makes
        // it easier and doesn't require any mapping.
        IGR = LF.Utilities.getReviewScreenItemID(params.item);
    // Removing the event, this utility function removes answers from
    // LF.Data.Questionnaire.Answers and also removes Question views instances
    // from LF.Data.Questionnaire.QuestionViews for IGR to be deleted.
    LF.Data.Questionnaire.removeLoopEvent(IG, IGR);
    // Ordering IGR must be done so all IGR are in sequence. this utliity function
    // order igr on all Answer records and also changes the igr property on
    // all question view instances
    LF.Data.Questionnaire.orderIGR(IG, IGR);
    // decrement the igr to reflect the change in the count
    LF.Data.Questionnaire.decrementIGR(IG);
    // clear the stack and navigate to review screen again to reflect the changes
    LF.Utilities.clearLoopScreenFromStack(IG);
    // Renders review screen again to reflect changes
    LF.Data.Questionnaire.displayScreen('LOOP1_S_2', callback);
};
/**
 * This function determines the items to be displayed on the Review Screen. It fetches the
 * response for the food and then the response is used to fetch the correct
 * food string.
 * @returns {Q.Promise<Array>} promise resolved when finished.
 */
LF.Widget.ReviewScreen.foodLoop2 = function () {
    let IG             = 'Loop2A',
        // gets the list of food from the model which is helpful when
        // fetching correct strings for food
        foodList       = LF.Data.Questionnaire.data.questions.at(0).get('widget').answers,
        result         = [],
        stringsToFetch = [],
        index          = 1;
    LF.Data.Questionnaire.updateIGR(IG);
    // clear everything else from this loop from the screen stack.  Add self back in.
    let curScreenName = _.last(LF.Data.Questionnaire.screenStack);
    LF.Data.Questionnaire.clearLoopScreenFromStack(IG);
    LF.Data.Questionnaire.screenStack.push(curScreenName);
    // Rest of the code is only for determining items to be displayed on Review screen.
    // This evaluate all the events and create an object literal with item ID (using IGR as ID)
    // and array of text which will be displayed. The evaluation starts with IGR 1 and iterate
    // until all events are evaluated.
    while(index){
        let answers = LF.Data.Questionnaire.queryAnswersByIGAndIGR(IG, index),
            item    = {};
        if(answers.length){
            item.id = index;
            item.text = [''];
            stringsToFetch.push(foodList[parseInt(answers[0].get('response'), 10)].text);
            result.push(item);
            index++;
        }else{
            index = false;
        }
    }
    return LF.getStrings(stringsToFetch, () => {
    }, {namespace : 'P_Loop2'}).then((strings) => {
        let i;
        // replace with localized strings
        for(i = 0; i < strings.length; i++){
            result[i].text[0] = strings[i];
        }
        return result;
    });
};
/**
 * This function determines the items to be displayed on the Review Screen. It fetches the
 * response for the medication and then the response is used to fetch the correct
 * medication string.
 * @returns {Q.Promise<Array>} promise resolved when finished
 */
LF.Widget.ReviewScreen.medsLoop2 = function () {
    let currentIGR,
        answers,
        IG             = 'Loop2B',
        medsList       = LF.Data.Questionnaire.data.questions.at(3).get('widget').answers,
        result         = [],
        stringsToFetch = [],
        index          = 1;
    // clear everything else from this loop from the screen stack.  Add self back in.
    let curScreenName = _.last(LF.Data.Questionnaire.screenStack);
    LF.Data.Questionnaire.clearLoopScreenFromStack(IG);
    LF.Data.Questionnaire.screenStack.push(curScreenName);
    LF.Data.Questionnaire.updateIGR(IG);
    currentIGR = LF.Data.Questionnaire.getCurrentIGR(IG);
    answers = LF.Data.Questionnaire.queryAnswersByIGAndIGR(IG, currentIGR);
    // if on backing out, the question was answered then incrememt the IGR
    if(answers.length === 1){
        LF.Data.Questionnaire.incrementIGR(IG);
    }
    // Rest of the code is only for determining items to be displayed on Review screen.
    // This evaluate all the events and create an object literal with item ID (using IGR as ID)
    // and array of text which will be displayed. The evaluation starts with IGR 1 and iterate
    // until all events are evaluated.
    while(index){
        let answers = LF.Data.Questionnaire.queryAnswersByIGAndIGR(IG, index),
            item    = {};
        if(answers.length){
            item.id = index;
            item.text = [''];
            stringsToFetch.push(medsList[parseInt(answers[0].get('response'), 10)].text);
            result.push(item);
            index++;
        }else{
            index = false;
        }
    }
    return LF.getStrings(stringsToFetch, () => {
    }, {namespace : 'P_Loop2'}).then((strings) => {
        let i;
        // replace with localized strings
        for(i = 0; i < strings.length; i++){
            result[i].text[0] = strings[i];
        }
        return result;
    });
};
/**
 * This function executes and navigate the user to the Screen 1 which is the first screen
 * of the food loop event.
 */
LF.Widget.ReviewScreen.addNewFoodLoop2 = function () {
    let IG                  = 'Loop2A',
        // get the current IGR
        currentIGR          = LF.Data.Questionnaire.getCurrentIGR(IG),
        // get the answers to see if the current IGR has answers
        answersByCurrentIGR = LF.Data.Questionnaire.queryAnswersByIGAndIGR(IG, currentIGR);
    // Increment IGR only if current IGR has been answered
    if(answersByCurrentIGR.length){
        // increments the IGR becase of new event.
        LF.Data.Questionnaire.incrementIGR(IG);
    }
    LF.Data.Questionnaire.displayScreen('LOOP2_S_1');
};
/**
 * This function executes and navigate the user to the Screen 4 which is the first screen
 * of the medication loop event.
 */
LF.Widget.ReviewScreen.addNewMedsLoop2 = function () {
    LF.Data.Questionnaire.displayScreen('LOOP2_S_4');
};
/**
 * Validate food Review Screen to make sure at least one food is added else show error banner.
 * @param {string} answer - Not useful for Review screen. Responses are fetched in the function
 * @param {object} params - An object containing configuration parameters for the validation function
 * @param {Function<boolean>} callback true - At least one response is added
 *                      false - No response available
 */
LF.Widget.ValidationFunctions.checkForFood = function (answer, params, callback) {
    let answersByCurrentIGR = LF.Data.Questionnaire.queryAnswersByIGAndIGR('Loop2A', 1);
    if(answersByCurrentIGR.length){
        callback(true);
    }else{
        LF.getStrings('REQUIRED_ANSWER', function (string) {
            LF.Notify.Banner.show({
                text : string,
                type : 'error'
            });
        });
        callback(false);
    }
};
/**
 * This function executes and delete the event based on the item selected on the review screen.
 * This function removes the loop event, then order the IGRs for answers recorded after the
 * deleted event. Then the IGR from the IG List for this IG is decremented as one item has been
 * removed and then before displaying the review screen again it clears the stack.
 * @param {Object} params An Object which contains button configuration for the button which was
 * clicked from the Review Screen, item which is an item selected on the review screen and it is
 * null if no item is selected. The object also contains question on which the review screen is configured.
 */
LF.Widget.ReviewScreen.deleteFoodLoop2 = function (params) {
    let IG  = 'Loop2A',
        // getting id from the selected item and using it as IGR. This works because
        // when creating review screen item, IGRs were used as item ids which makes
        // it easier and doesn't require any mapping.
        IGR = LF.Utilities.getReviewScreenItemID(params.item);
    // Removes the loop event and also order the IGRs
    LF.Data.Questionnaire.deleteIGRAndOrder(IG, IGR);
    // decrement the igr to reflect the change in the count
    LF.Data.Questionnaire.decrementIGR(IG);
    //clears the stack before review screen is put back on the stack.
    LF.Utilities.clearLoopScreenFromStack(IG);
    // Renders review screen again to reflect changes
    LF.Data.Questionnaire.displayScreen('LOOP2_S_2');
};
LF.Widget.ReviewScreen.editFoodLoop2 = function (params, callback) {
    let IG  = 'Loop2A',
        // getting id from the selected item and using it as IGR. This works because
        // when creating review screen item, IGRs were used as item ids which makes
        // it easier and doesn't require any mapping.
        IGR = LF.Utilities.getReviewScreenItemID(params.item);
    // set the IGR to current and navigate to screen 5.
    // setting IGR will fetch the correct answer previously reccorded.
    LF.Data.Questionnaire.setCurrentIGR(IG, IGR);
    LF.Data.Questionnaire.displayScreen('LOOP2_S_1', callback);
};
LF.Widget.ReviewScreen.editMedsLoop2 = function (params, callback) {
    let IG  = 'Loop2B',
        // getting id from the selected item and using it as IGR. This works because
        // when creating review screen item, IGRs were used as item ids which makes
        // it easier and doesn't require any mapping.
        IGR = LF.Utilities.getReviewScreenItemID(params.item);
    // set the IGR to current and navigate to screen 5.
    // setting IGR will fetch the correct answer previously reccorded.
    LF.Data.Questionnaire.setCurrentIGR(IG, IGR);
    LF.Data.Questionnaire.displayScreen('LOOP2_S_4', callback);
};
/**
 * This function executes and delete the event based on the item selected on the review screen.
 * This function removes the loop event, then order the IGRs for answers recorded after the
 * deleted event. Then the IGR from the IG List for this IG is decremented as one item has been
 * removed and then before displaying the review screen again it clears the stack.
 * @param {Object} params An Object which contains button configuration for the button which was
 * clicked from the Review Screen, item which is an item selected on the review screen and it is
 * null if no item is selected. The object also contains question on which the review screen is configured.
 */
LF.Widget.ReviewScreen.deleteMedsLoop2 = function (params) {
    let IG  = 'Loop2B',
        // getting id from the selected item and using it as IGR. This works because
        // when creating review screen item, IGRs were used as item ids which makes
        // it easier and doesn't require any mapping.
        IGR = LF.Utilities.getReviewScreenItemID(params.item);
    // Removes the loop event and also order the IGRs
    LF.Data.Questionnaire.deleteIGRAndOrder(IG, IGR);
    // decrement the igr to reflect the change in the count
    LF.Data.Questionnaire.decrementIGR(IG);
    // clear the stack and navigate to review screen again to reflect the changes
    LF.Utilities.clearLoopScreenFromStack(IG);
    // Renders review screen again to reflect changes
    LF.Data.Questionnaire.displayScreen('LOOP2_S_3');
};
/**
 * This function determines the items to be displayed on the Review Screen for medication for Loop2 diary.
 * It fetches the response for the medication and then the response is used to fetch the correct
 * medication string. Also number of medication taken is displayed.
 * @returns {Q.Promise<Array>}
 */
LF.Widget.ReviewScreen.medsLoop3 = function () {
    let i,
        index,
        currentIGR,
        answersByCurrentIGR,
        IG               = 'Loop3B',
        parentIG         = 'Loop3A',
        questionnaire    = LF.Data.Questionnaire,
        // gets the current IGR for outer loop event.
        currentParentIGR = LF.Data.Questionnaire.getCurrentIGR(parentIG),
        // fetch answers with IGR -1 which indicates that the add for
        // inner event was done on edit.
        addedOnEdit      = LF.Data.Questionnaire.queryAnswersByIGAndIGR(IG, -1),
        // gets the list of medication from the model which is helpful when
        // fetching correct strings for medication
        medsList         = questionnaire.data.questions.at(4).get('widget').answers,
        result           = [],
        stringsToFetch   = [],
        startingIGR      = 0,
        insertAt         = 1;
    // Establish nestedIGRs array.
    // LF.Data.Questionnaire.nestedIGRs || (LF.Data.Questionnaire.nestedIGRs = [0]);
    // clear everything else from this loop from the screen stack.  Add self back in.
    let curScreenName = _.last(LF.Data.Questionnaire.screenStack);
    LF.Data.Questionnaire.clearLoopScreenFromStack(IG);
    LF.Data.Questionnaire.screenStack.push(curScreenName);
    // If there was an addtion to inner loop then the IGR for that event is
    // set to -1. Now if all required question are answered then the IGR needs to be
    // inserted in between. If all question are not answered, that means that the adding
    // of event was abandoned and we need to removed event for IGR -1.
    if(addedOnEdit.length === 2){
        // All required questions were answered. The following loop determines the correct
        // spot where this needs to ne inserted. nestedIGRs is an array at LF.Data.Questionnaire
        // is used to track the outer loop and each value in the array represents number
        // of inner event added for particular outer IGR.
        for(let i = 0; i < currentParentIGR; i++){
            insertAt += LF.Data.Questionnaire.nestedIGRs[i];
        }
        // After deciding where to insert the new event, that IGR must become
        // available. Followting Utility function does the trick.
        LF.Data.Questionnaire.makeIGRAvailable(IG, insertAt);
        // increment the igr to reflect the change in the count
        LF.Data.Questionnaire.incrementIGR(IG);
        // Change IGR from -1 to now available IGR.
        LF.Data.Questionnaire.changeIGR(IG, -1, insertAt);
        // increment the value in nestedIGRs array to reflect change in count.
        LF.Data.Questionnaire.nestedIGRs[currentParentIGR - 1]++;
    }else if(addedOnEdit.length === 1){
        // Removes partly answered event
        LF.Utilities.clearLoopScreenFromStack(IG);
        LF.Data.Questionnaire.removeLoopEvent(IG, -1);
        LF.Data.Questionnaire.displayScreen('LOOP3_S_4');
        return Q([]);
    }
    // get the current IGR
    currentIGR = LF.Data.Questionnaire.getCurrentIGR(IG);
    // determines the starting IGR based on the outer Loop IGR.
    // This is very helpful when creating Review Screen item and only those
    // item are evaluated which belongs to the current outer Loop IGR.
    for(i = 0; i < currentParentIGR - 1; i++){
        startingIGR += LF.Data.Questionnaire.nestedIGRs[i];
    }
    index = startingIGR + 1;
    // Get all answers for the current inner Loop IGR
    answersByCurrentIGR = LF.Data.Questionnaire.queryAnswersByIGAndIGR(IG, currentIGR);
    // New loop event was started and user backed out. This is different than the add on edit.
    // Removing all answers and question views for partly completed event.
    if(answersByCurrentIGR.length > 0){
        LF.Utilities.clearLoopScreenFromStack(IG);
        LF.Data.Questionnaire.removeLoopEvent(IG, currentIGR);
        LF.Data.Questionnaire.displayScreen('LOOP3_S_4');
        return Q([]);
    }
    // Rest of the code is only for determining items to be displayed on Review screen.
    // This evaluate all the events and create an object literal with item ID (using IGR as ID)
    // and array of text which will be displayed. The evaluation starts with IGR evaluated. It only iterates
    // for number of item to be displayed based on the current outer loop event.
    while(index <= startingIGR + LF.Data.Questionnaire.nestedIGRs[currentParentIGR - 1]){
        let answers = LF.Data.Questionnaire.queryAnswersByIGAndIGR(IG, index),
            item    = {};
        // Look each answer and store data based on question id.
        _.each(answers, (answer) => {
            switch(answer.get('question_id')){
                case 'LOOP3_Q5':
                    // ColdMed
                    stringsToFetch.push(medsList[parseInt(answer.get('response'), 10)].text);
                    result.push(item);
                    break;
                case 'LOOP3_Q6':
                    // HowMuch
                    item.id = index;
                    item.text = ['', answer.get('response')];
                    break;
            }
        });
        index++;
    }
    return LF.getStrings(stringsToFetch, () => {
    }, {namespace : 'P_Loop3'}).then((strings) => {
        let i;
        // replace with localized strings
        for(i = 0; i < strings.length; i++){
            result[i].text[0] = strings[i];
        }
        // callback with results to be displayed on the review screen.
        return result;
    });
};
/**
 * This function determines the items to be displayed on the Review Screen for Loop3 diary.
 * It fetches the response for the symptoms and then the response is used to fetch the correct
 * symptom string. Also severity of the symptom is displayed.
 * @returns {Q.Promise<Array>} promise resolved when finished
 */
LF.Widget.ReviewScreen.sympLoop3 = function () {
    let currentIGR,
        answersByCurrentIGR,
        IG             = 'Loop3A',
        childIG        = 'Loop3B',
        // gets the list of symptoms from the model which is helpful when
        // fetching correct strings for symptoms
        sympList       = LF.Data.Questionnaire.data.questions.at(0).get('widget').answers,
        // gets the list of severity from the model which is helpful when
        // fetching correct strings for severity
        sevList        = LF.Data.Questionnaire.data.questions.at(1).get('widget').answers,
        result         = [],
        stringsToFetch = [],
        index          = 1;
    // Update the IGR incase if coming back from editing. For editing the IGR is set
    // to the item selected on the review screen, so update is required.
    LF.Data.Questionnaire.updateIGR(IG);
    // get the current IGR after update. Updating is very important
    currentIGR = LF.Data.Questionnaire.getCurrentIGR(IG);
    // Get all answers for the current outer Loop IGR
    answersByCurrentIGR = LF.Data.Questionnaire.queryAnswersByIGAndIGR(IG, currentIGR);
    // clear everything else from this loop from the screen stack.  Add self back in.
    let curScreenName = _.last(LF.Data.Questionnaire.screenStack);
    LF.Data.Questionnaire.clearLoopScreenFromStack(IG);
    LF.Data.Questionnaire.clearLoopScreenFromStack(childIG);
    LF.Data.Questionnaire.screenStack.push(curScreenName);
    // New loop event was started and user backed out.
    // Removing all answers and question views for partly completed event.
    if(answersByCurrentIGR.length > 0 && answersByCurrentIGR.length < 3){
        // decrementing IGR as with this Review sreen the IGR is incremented from
        // LF.Widget.ReviewScreen.addNewSympLoop3 function and on abandoning the event
        // decrementing IGR is required.
        if(currentIGR > 1){
            LF.Data.Questionnaire.decrementIGR(IG);
        }
        // Also as new item was pushed to track inner event (from LF.Widget.ReviewScreen.addNewSympLoop3
        // function), on abandoning the event, nestedIGRs array also needs to remove last pushed vlue.
        if(LF.Data.Questionnaire.nestedIGRs.length > 1){
            LF.Data.Questionnaire.nestedIGRs.pop();
        }
        // Removing all answers and question views for partly completed event.
        LF.Data.Questionnaire.removeLoopEvent(IG, currentIGR);
    }
    // Rest of the code is only for determining items to be displayed on Review screen.
    // This evaluate all the events and create an object literal with item ID (using IGR as ID)
    // and array of text which will be displayed. The evaluation starts with IGR 1 and iterate
    // until all events are evaluated.
    while(index){
        let answers = LF.Data.Questionnaire.queryAnswersByIGAndIGR(IG, index),
            item    = {},
            sympText,
            sevText;
        _.each(answers, (answer) => {
            switch(answer.get('question_id')){
                case 'LOOP3_Q1':
                    sympText = sympList[parseInt(answer.get('response'), 10)].text;
                    break;
                case 'LOOP3_Q2':
                    sevText = sevList[parseInt(answer.get('response'), 10)].text;
                    break;
            }
        });
        if(answers.length){
            stringsToFetch.push(sympText, sevText);
            item.id = index;
            item.text = ['', ''];
            result.push(item);
            index++;
        }else{
            index = false;
        }
    }
    return LF.getStrings(stringsToFetch, () => {
    }, {namespace : 'P_Loop3'}).then((strings) => {
        let i, j;
        // replace with localized strings
        for(i = 0, j = 0; j < strings.length; i++, j += 2){
            result[i].text[0] = strings[j];
            result[i].text[1] = strings[j + 1];
        }
        return result;
    });
};
/**
 * This function executes and navigate the user to the Screen 5 which is the first screen
 * of the inner loop event.
 * @param {Object} params An Object which contains button configuration for the button which was
 * clicked from the Review Screen, item which is an item selected on the review screen and it is
 * null if no item is selected. The object also contains question on which the review screen is configured.
 * @param {Function} callback The function needs to be called to finish the Rule which triggered this action.
 */
LF.Widget.ReviewScreen.addNewRemLoop3 = function (params, callback) {
    let IG               = 'Loop3B',
        // gets the current outer loop IGR
        currentParentIGR = LF.Data.Questionnaire.getCurrentIGR('Loop3A');
    // This determines if the add is happening for the outer event which
    // is being edited. If so then the IGR is set to -1 and will be inserted
    // in between when the review screen is evaluated.
    if(_.isNumber(LF.Data.Questionnaire.nestedIGRs[currentParentIGR])){
        LF.Data.Questionnaire.setCurrentIGR(IG, -1);
    }
    LF.Data.Questionnaire.screenStack.push('LOOP3_S_5');
    LF.Data.Questionnaire.displayScreen('LOOP3_S_5', callback);
};
/**
 * This function executes and navigate the user to the Screen 1 which is the first screen
 * of the outer loop event.
 * @param {Object} params An Object which contains button configuration for the button which was
 * clicked from the Review Screen, item which is an item selected on the review screen and it is
 * null if no item is selected. The object also contains question on which the review screen is configured.
 * @param {Function} callback The function needs to be called to finish the Rule which triggered this action.
 */
LF.Widget.ReviewScreen.addNewSympLoop3 = function (params, callback) {
    let IG                  = 'Loop3A',
        // get the current IGR
        currentIGR          = LF.Data.Questionnaire.getCurrentIGR(IG),
        // get the answers to see if the current IGR has answers
        answersByCurrentIGR = LF.Data.Questionnaire.queryAnswersByIGAndIGR(IG, currentIGR);
    // Increment IGR only if current IGR has been answered
    if(answersByCurrentIGR.length){
        // increments the IGR becase of new event for outer loop has started.
        LF.Data.Questionnaire.incrementIGR(IG);
        // push 0 in nestedIGRs array to track inner events for new outer event
        LF.Data.Questionnaire.nestedIGRs.push(0);
    }
    LF.Data.Questionnaire.displayScreen('LOOP3_S_1', callback);
};
/**
 * This function executes and delete the event based on the item selected on the review screen.
 * This function removes the loop event, then order the IGRs for answers recorded after the
 * deleted event. Then the IGR from the IG List for this IG is decremented as one item has been
 * removed and then before displaying the review screen again (to reflect the changes), nestedIGRs
 * array is updated (decremented) for the current outer loop event.
 * @param {Object} params An Object which contains button configuration for the button which was
 * clicked from the Review Screen, item which is an item selected on the review screen and it is
 * null if no item is selected. The object also contains question on which the review screen is configured.
 * @param {Function} callback The function needs to be called to finish the Rule which triggered this action.
 */
LF.Widget.ReviewScreen.deleteRemLoop3 = function (params, callback) {
    let IG               = 'Loop3B',
        // getting id from the selected item and using it as IGR. This works because
        // when creating review screen item, IGRs were used as item ids which makes
        // it easier and doesn't require any mapping.
        IGR              = LF.Utilities.getReviewScreenItemID(params.item),
        // gets the current outer loop IGR
        currentParentIGR = LF.Data.Questionnaire.getCurrentIGR('Loop3A');
    // Removing the event, this utility function removes answers from
    // LF.Data.Questionnaire.Answers and also removes Question views instances
    // from LF.Data.Questionnaire.QuestionViews for IGR to be deleted.
    LF.Data.Questionnaire.removeLoopEvent(IG, IGR);
    // Ordering IGR must be done so all IGR are in sequence. this utliity function
    // order igr on all Answer records and also changes the igr property on
    // all question view instances
    LF.Data.Questionnaire.orderIGR(IG, IGR);
    // decrement the igr to reflect the change in the count
    LF.Data.Questionnaire.decrementIGR(IG);
    // decrement value from nestedIGRs array to reflect the deletion
    LF.Data.Questionnaire.nestedIGRs[currentParentIGR - 1]--;
    //clears the stack before review screen is put back on the stack.
    LF.Utilities.clearLoopScreenFromStack(IG);
    // Renders review screen again to reflect changes
    LF.Data.Questionnaire.displayScreen('LOOP3_S_4', callback);
};
/**
 * This function executes and delete the event based on the item selected on the review screen.
 * This function removes the loop event, then order the IGRs for answers recorded after the
 * deleted event. Then the IGR from the IG List for this IG is decremented as one item has been
 * removed and then before displaying the review screen again (to reflect the changes), all the
 * event added with inner loop for deleted outer event are also removed.
 * @param {Object} params An Object which contains button configuration for the button which was
 * clicked from the Review Screen, item which is an item selected on the review screen and it is
 * null if no item is selected. The object also contains question on which the review screen is configured.
 * @param {Function} callback The function needs to be called to finish the Rule which triggered this action.
 */
LF.Widget.ReviewScreen.deleteSympLoop3 = function (params, callback) {
    let i,
        IG           = 'Loop3A',
        innerIG      = 'Loop3B',
        // getting id from the selected item and using it as IGR. This works because
        // when creating review screen item, IGRs were used as item ids which makes
        // it easier and doesn't require any mapping.
        IGR          = LF.Utilities.getReviewScreenItemID(params.item),
        startingIGR  = 0,
        numberofMeds = [];
    // Removing the event, this utility function removes answers from
    // LF.Data.Questionnaire.Answers and also removes Question views instances
    // from LF.Data.Questionnaire.QuestionViews for IGR to be deleted.
    LF.Data.Questionnaire.removeLoopEvent(IG, IGR);
    // Ordering IGR must be done so all IGR are in sequence. this utliity function
    // order igr on all Answer records and also changes the igr property on
    // all question view instances
    LF.Data.Questionnaire.orderIGR(IG, IGR);
    // decrement the igr to reflect the change in the count only if the
    // item to be deleted is not only one left
    if(LF.Data.Questionnaire.getCurrentIGR(IG) !== 1){
        LF.Data.Questionnaire.decrementIGR(IG);
    }
    // Following code removes all the event added for inner loop for the outer
    // event just removed.
    for(i = 0; i < IGR - 1; i++){
        // determine the starting IGR for the inner loop.
        startingIGR += LF.Data.Questionnaire.nestedIGRs[i];
    }
    // using array splice to actually remove value permanantly from
    // nestedIGRs and the value is used to remove that many inner events
    // But if the item to be deleted is the only in the Symptom list then
    // make the value to 0
    if(LF.Data.Questionnaire.nestedIGRs.length === 1){
        numberofMeds[0] = LF.Data.Questionnaire.nestedIGRs[0];
        LF.Data.Questionnaire.nestedIGRs[0] = 0;
    }else{
        numberofMeds = LF.Data.Questionnaire.nestedIGRs.splice(IGR - 1, 1);
    }
    // iterate and remove all exixtings inner loop event
    for(i = startingIGR + 1; i <= startingIGR + numberofMeds[0]; i++){
        // following the same patter of removing a loop event
        LF.Data.Questionnaire.removeLoopEvent(innerIG, startingIGR + 1);
        LF.Data.Questionnaire.orderIGR(innerIG, startingIGR + 1);
        LF.Data.Questionnaire.decrementIGR(innerIG);
    }
    //clears the stack before review screen is put back on the stack.
    LF.Utilities.clearLoopScreenFromStack(IG);
    // Renders review screen again to reflect changes
    LF.Data.Questionnaire.displayScreen('LOOP3_S_7', callback);
};
/**
 * This function executes and navigate the user to the Screen 5 which is the first screen
 * of the inner loop event, but after setting the IGR based on the item selected on the Review
 * screen. This way user enter into edit mode for selected IGR.
 * @param {Object} params An Object which contains button configuration for the button which was
 * clicked from the Review Screen, item which is an item selected on the review screen and it is
 * null if no item is selected. The object also contains question on which the review screen is configured.
 * @param {Function} callback The function needs to be called to finish the Rule which triggered this action.
 */
LF.Widget.ReviewScreen.editRemLoop3 = function (params, callback) {
    let IG  = 'Loop3B',
        // getting id from the selected item and using it as IGR. This works because
        // when creating review screen item, IGRs were used as item ids which makes
        // it easier and doesn't require any mapping.
        IGR = LF.Utilities.getReviewScreenItemID(params.item);
    // set the IGR to current and navigate to screen 5.
    // setting IGR will fetch the correct answer previously reccorded.
    LF.Data.Questionnaire.setCurrentIGR(IG, IGR);
    LF.Data.Questionnaire.displayScreen('LOOP3_S_5', callback);
};
/**
 * This function executes and navigate the user to the Screen 1 which is the first screen
 * of the outer loop event, but after setting the IGR based on the item selected on the Review
 * screen. This way user enter into edit mode for selected IGR.
 * @param {Object} params An Object which contains button configuration for the button which was
 * clicked from the Review Screen, item which is an item selected on the review screen and it is
 * null if no item is selected. The object also contains question on which the review screen is configured.
 * @param {Function} callback The function needs to be called to finish the Rule which triggered this action.
 */
LF.Widget.ReviewScreen.editSympLoop3 = function (params, callback) {
    let IG  = 'Loop3A',
        // getting id from the selected item and using it as IGR. This works because
        // when creating review screen item, IGRs were used as item ids which makes
        // it easier and doesn't require any mapping.
        IGR = LF.Utilities.getReviewScreenItemID(params.item);
    // set the IGR to current and navigate to screen 5.
    // setting IGR will fetch the correct answer previously reccorded.
    LF.Data.Questionnaire.setCurrentIGR(IG, IGR);
    LF.Data.Questionnaire.displayScreen('LOOP3_S_1', callback);
};
/**
 * Validate symptom Review Screen to make sure at least one symptom is added else show error banner.
 * @param {string} answer - Not useful for Review screen. Responses are fetched in the function
 * @param {object} params - An object containing configuration parameters for the validation function
 * @param {Function<boolean>} callback - true - At least one response is added
 *                      false - No response available
 */
LF.Widget.ValidationFunctions.checkForSymptom = function (answer, params, callback) {
    let answersByCurrentIGR = LF.Data.Questionnaire.queryAnswersByIGAndIGR('Loop3A', 1);
    if(answersByCurrentIGR.length){
        callback(true);
    }else{
        LF.getStrings('REQUIRED_ANSWER', function (string) {
            LF.Notify.Banner.show({
                text : string,
                type : 'error'
            });
        });
        callback(false);
    }
};
