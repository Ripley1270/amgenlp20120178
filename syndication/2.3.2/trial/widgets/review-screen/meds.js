LF.Widget.ReviewScreen.medsList = (widget, callback = $.noop) => {
    return new Q.Promise((resolve) => {
        let text1 = LF.Helpers.getWidget('MEDS_Q_1').answer.get('response'),
            text2,
            items = [];
        if(text1 === '0'){
            text2 = 'Half a pill';
        }else{
            text2 = text1;
        }
        items.push({id : '1', text : [text1]});
        resolve(items);
    });
};
LF.Widget.ReviewScreen.alertAction = (params, callback = $.noop) => {
    return new Q.Promise((resolve) => {
        LF.Actions.notify({
                message : {key : 'ALERT_MESSAGE', namespace : 'P_Meds'}
            },
            () => resolve()
        );
    });
};
LF.Widget.ReviewScreen.addAction = (params, callback = $.noop) => {
    return new Q.Promise((resolve) => {
        LF.Actions.notify({message : {key : 'ADD_MESSAGE', namespace : 'P_Meds'}}, () => resolve());
    });
};
LF.Widget.ReviewScreen.editAction = (params, callback = $.noop) => {
    return new Q.Promise((resolve) => {
        LF.Actions.notify({message : {key : 'EDIT_MESSAGE', namespace : 'P_Meds'}}, () => resolve());
    });
};
LF.Widget.ReviewScreen.deleteAction = (params, callback = $.noop) => {
    return new Q.Promise((resolve) => {
        LF.Actions.notify({message : {key : 'DELETE_MESSAGE', namespace : 'P_Meds'}}, () => resolve());
    });
};
LF.Widget.ReviewScreen.medsList_Q4 = (widget, callback = $.noop) => {
    return new Q.Promise((resolve) => {
        let text1 = LF.Helpers.getWidget('MEDS_Q_1').answer.get('response'),
            items = [];
        items.push(
            {
                id   : '1',
                text : [text1]
            }
        );
        resolve(items);
    });
};
LF.Widget.ReviewScreen.editAction_Q4 = (params, callback = $.noop) => {
    return new Q.Promise((resolve) => {
        LF.router.view().screenStack = [];
        LF.router.view().screenStack.push('MEDS_S_1');
        LF.router.view().screen = 0; // First screen
        LF.router.view().displayScreen('MEDS_S_1');
        resolve();
    });
};
