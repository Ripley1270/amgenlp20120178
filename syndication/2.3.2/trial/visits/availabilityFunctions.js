/**
 * Demo function.
 * @param {Object} params - visit context params.
 * @param {Object} params.availability config
 * @param {Object} params.visit from study design
 * @param {Object} params.user visit model object
 * @param {Object} params.subject - subject
 * @returns {Q.Promise<boolean>}
 */
export function notAvailable (params) {
    return Q(false);
};
LF.Visits.availabilityFunctions.notAvailable = notAvailable;
