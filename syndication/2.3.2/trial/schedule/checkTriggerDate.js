/**
 * Created by jonathan.carr on 01-Aug-17.
 */
LF.Schedule.schedulingFunctions.checkTriggerDate = function (schedule, completedQuestionnaires, value, context, callback) {
    LF.Utilities.getCollection('EveningDiaryCollection')
        .then((collection) => {
            let now              = new Date(),
                triggered        = _.find(collection, function (model) {
                    return !!model.get('CATDAT1D');
                }),
                runSchedule      = schedule.get('scheduleParams').runSchedule || 'checkRepeatByDateAvailability',
                otherCatSchedule = _.find(LF.Helpers.getScheduleModels(), (scheduleModel) => {
                    return scheduleModel.get('target').id === 'CAT'
                        && scheduleModel.get('scheduleFunction') === 'visitBasedAvailability';
                });
            if(triggered && LF.Utilities.dateDiffInDays(now, new Date(triggered.get('CATDAT1D'))) === 0){
                LF.Schedule.schedulingFunctions[runSchedule](schedule, completedQuestionnaires, value, context, (available) => {
                    LF.Schedule.schedulingFunctions.visitBasedAvailability(otherCatSchedule, completedQuestionnaires, value, context, (visitAvailiable) => {
                        callback(available && !visitAvailiable);
                    });
                });
            }else{
                callback(false);
            }
        });
};
export default LF.Schedule.schedulingFunctions.checkTriggerDate;
