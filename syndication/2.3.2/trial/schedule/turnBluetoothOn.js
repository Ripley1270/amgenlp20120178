/**
 * Created by jonathan.carr on 21-Aug-17.
 */
LF.Schedule.schedulingFunctions.turnBluetoothOn = function (schedule, completedQuestionnaires, value, context, callback) {
    let currentTime = new Date();
    schedule.get('scheduleParams').startAvailability = ((currentTime.getHours() + (3 - currentTime.getHours() % 3)) % 24).toString() + ':00';
    if(LF.Wrapper.isWrapped){
        LF.Wrapper.Utils.eSense.enable(
            function () {
            },
            function () {
            }
        );
    }
    callback(false);
};
