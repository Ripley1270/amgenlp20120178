/**
 * Created by jonathan.carr on 01-Aug-17.
 */
LF.Schedule.schedulingFunctions.visitSelectionSchedule = function (schedule, completedQuestionnaires, value, context, callback) {
    LF.Utilities.getCurrentVisit((lastVisit) => {
        callback(!lastVisit || lastVisit.get('visit') !== LF.StudyDesign.visitList.week268);
    });
};
export default LF.Schedule.schedulingFunctions.visitSelectionSchedule;
