/**
 * Created by jonathan.carr on 01-Aug-17.
 */
LF.Schedule.schedulingFunctions.completeOnce = function (schedule, completedQuestionnaires, value, context, callback) {
    callback(!_.first(completedQuestionnaires.where({'questionnaire_id' : schedule.get('target').id})));
};
export default LF.Schedule.schedulingFunctions.completeOnce;
