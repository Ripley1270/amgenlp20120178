/**
 * Created by jonathan.carr on 01-Aug-17.
 */
LF.Schedule.schedulingFunctions.visitBasedAvailability = function (schedule, completedQuestionnaires, value, context, callback) {
    let now         = new Date(),
        reportID    = schedule.get('target').id,
        diary       = _.first(completedQuestionnaires.where({'questionnaire_id' : reportID})),
        lastTS,
        phase       = LF.Data.Questionnaire.subject.get('phase'),
        visits      = schedule.get('scheduleParams').visits,
        repeat      = schedule.get('scheduleParams').repeat,
        runSchedule = schedule.get('scheduleParams').runSchedule ? schedule.get('scheduleParams').runSchedule : 'checkAlwaysAvailability';
    if(diary){
        lastTS = new Date(diary.get('lastStartedDate'));
    }
    LF.Utilities.getCurrentVisit(function (currentVisit) {
        let isAvailable = false,
            lastVisitID,
            lastVisitTS;
        if(currentVisit){
            lastVisitID = currentVisit.get('visit');
            lastVisitTS = new Date(currentVisit.get('visitDate'));
        }
        if(lastVisitID == null){
            //Disable diary as no visits confirmed yet
            callback(false);
        }else if(repeat != null && repeat == 'none' && lastTS && (lastTS.getTime() >= lastVisitTS.getTime())){
            //Disable diary if it is done since the last visit
            callback(false);
        }else{
            //Iterate through each visit configuration and evaluate if that visit is active
            _.every(visits, function (visit) {
                let visitID       = LF.StudyDesign.visitList[visit.visitID],
                    predecessorID = visit.predecessorID,
                    durationDays  = visit.durationDays,
                    langList      = visit.langList || ['All'],
                    langLocale = `${LF.Preferred.language}_${LF.Preferred.locale}`;
                //Check if we are within the visit and in correct Protocol
                if(visitID == lastVisitID && (langList.indexOf('All') !== -1 || langList.indexOf(`${langLocale}`) !== -1)){
                    //Check for predecessor
                    if(predecessorID){
                        //if the predecessor is Eligibility Report and they are not eligible, don't show SF36 or
                        // EQ5D5L
                        //Check if the predecessor was done today
                        let predDiary = _.first(completedQuestionnaires.where({'questionnaire_id' : predecessorID}));
                        if(predDiary){
                            let predDiaryLastTS = new Date(predDiary.get('lastStartedDate'));
                            if(predDiaryLastTS && (predDiaryLastTS.getTime() >= lastVisitTS.getTime())){
                                //Predecessor done today. Check for duration
                                isAvailable = !(durationDays != null && durationDays > 0 && LF.Utilities.dateDiffInDays(now, lastVisitTS) >= durationDays);
                            }
                        }
                    }else{
                        //No predecessor, check for duration
                        isAvailable = !(durationDays != null && durationDays > 0 && LF.Utilities.dateDiffInDays(now, lastVisitTS) >= durationDays);
                    }
                    runSchedule = visit.runSchedule ? visit.runSchedule : 'checkAlwaysAvailability';
                }
                isAvailable = isAvailable && (!lastTS || lastTS < lastVisitTS);
                //break on success
                return !isAvailable;
            });
            if(isAvailable){
                LF.Schedule.schedulingFunctions[runSchedule](schedule, completedQuestionnaires, value, context, callback);
            }else{
                callback(false);
            }
        }
    });
};
export default LF.Schedule.schedulingFunctions.visitBasedAvailability;
