/**
 * Created by jonathan.carr on 01-Aug-17.
 */
LF.Schedule.schedulingFunctions.checkPrereq = function (schedule, completedQuestionnaires, value, context, callback) {
    let now                = new Date(),
        preReqID           = schedule.get('scheduleParams').prereq,
        preReqdiary        = _.first(completedQuestionnaires.where({'questionnaire_id' : preReqID})),
        duration           = Number(schedule.get('scheduleParams').duration || 1),
        delay              = Number(schedule.get('scheduleParams').delay || 0),
        preReqCompleteDate,
        daysSinceCompleted = -1,
        runSchedule        = schedule.get('scheduleParams').runSchedule || 'checkRepeatByDateAvailability';
    if(preReqdiary){
        preReqCompleteDate = new Date(preReqdiary.get('lastStartedDate'));
        daysSinceCompleted = LF.Utilities.dateDiffInDays(now, preReqCompleteDate);
        if(daysSinceCompleted >= delay && (duration < 0 || daysSinceCompleted < (duration + delay))){
            LF.Schedule.schedulingFunctions[runSchedule](schedule, completedQuestionnaires, value, context, callback);
        }else{
            callback(false);
        }
    }else{
        callback(false);
    }
};
export default LF.Schedule.schedulingFunctions.checkPrereq;
