import Logger from 'core/Logger';
import AlarmManager from 'core/AlarmManager';
let logger = new Logger('Alarm Trigger');
LF.Schedule.alarmFunctions.standardAlarm = function (schedule, alarmParams, completedQuestionnaires, parentView, callback) {
    let baseAlarmTime    = alarmParams.time,
        today            = new Date(),
        questionnaireID  = schedule.get('target').id,
        preReqID         = schedule.get('scheduleParams') ? schedule.get('scheduleParams').prereq : null,
        preReqdiary      = preReqID ? _.first(completedQuestionnaires.where({'questionnaire_id' : preReqID})) : true,
        diary            = _.first(completedQuestionnaires.where({questionnaire_id : questionnaireID})),
        delay            = schedule.get('scheduleParams') ? schedule.get('scheduleParams').delay : 0,
        baseID           = alarmParams.id,
        reminders        = alarmParams.reminders,
        reminderInterval = alarmParams.reminderInterval,
        alarmIDs         = LF.Wrapper.Utils.generateIds(baseID, reminders + 1),
        alarmID,
        timeArray,
        alarmTime,
        startDate        = new Date(),
        lastDiaryDate,
        alarmStrings,
        volumeConfig     = LF.StudyDesign.alarmVolumeConfig,
        setupAlarms      = () => {
            //Let's setup them alarms
            if(diary){
                lastDiaryDate = new Date(diary.get('lastStartedDate'));
            }
            if(lastDiaryDate){
                // diary completed today?
                if(LF.Utilities.dateDiffInDays(today, lastDiaryDate) === 0){
                    //you did it today already! good work, fam!
                    logger.info('Diary done today, pushing to tomorrow');
                    startDate.setDate(startDate.getDate() + 1);
                }
            }
            timeArray = baseAlarmTime.split(':');
            startDate.setHours(timeArray[0], timeArray[1], 0, 0);
            //insert delay for alarm
            startDate = new Date(startDate + (500));
            for(let i = 0; i <= reminders; i++){
                alarmTime = new Date(startDate);
                alarmTime.setMinutes(startDate.getMinutes() + i * reminderInterval);
                if(today > alarmTime){
                    //that time past already
                    //we gonna set it for tomorrow for ya tho
                    alarmTime = new Date(new Date(alarmTime.setDate(alarmTime.getDate() + 1)));
                }
                if(today < alarmTime){
                    alarmID = alarmIDs.shift();
                    if(alarmID){
                        logger.info(`Setting ${questionnaireID} alarm at: ${alarmTime}`);
                        //Adding that alarm tho
                        AlarmManager.addAlarm(alarmID, alarmTime, alarmStrings.title, alarmStrings.message, alarmParams.repeat, schedule.get('id'), volumeConfig);
                    }
                }
            }
        };
    setTimeout(() => {
        _.each(alarmIDs, function (id) {
            AlarmManager.cancelAlarm(id);
        });
        LF.getStrings({
            title   : alarmParams.title || 'APPLICATION_HEADER',
            message : alarmParams.message || 'NEXT_ACTION'
        }, (strings) => {
            //getting them strings tho
            alarmStrings = strings;
        }).then(() =>
            LF.Utilities.getCollection('SubjectAlarms')
                .then((subjectAlarms) => {
                    //did y'all change something?
                    if(subjectAlarms.length !== 0){
                        let subjectAlarm = _.find(subjectAlarms, (alarm) => {
                            return Number(alarm.get('id')) === alarmParams.id;
                        });
                        if(subjectAlarm){
                            //y'all did change something!
                            //no worries, I got you
                            baseAlarmTime = subjectAlarm.get('time');
                        }
                    }
                    if(preReqdiary){
                        //if(LF.Utilities.dateDiffInDays(today, new Date(preReqdiary.get('lastStartedDate'))) >= delay){
                        //making sure y'all doing things in order
                        setupAlarms();
                        //}
                    }
                }));
    }, 500);
};
export default LF.Schedule.alarmFunctions.standardAlarm;
