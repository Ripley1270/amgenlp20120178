/**
 * Created by jonathan.carr on 25-Jul-17.
 */
import BaseCollection from '../../app/core/collections/StorageBase';
import Medication from '../models/Medication';
export default class Medications extends BaseCollection {
    /**
     * @property {Visit} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link Visit}'
     */
    get model () {
        return Medication;
    }
}
window.LF.Collection.Medications = Medications;
