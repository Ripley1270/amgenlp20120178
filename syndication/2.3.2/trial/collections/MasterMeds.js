/**
 * Created by jonathan.carr on 25-Jul-17.
 */
import BaseCollection from '../../app/core/collections/StorageBase';
import MasterMed from '../models/MasterMed';
/**
 * A collection of patient visits.
 * @class Visits
 * @extends BaseCollection
 */
export default class MasterMeds extends BaseCollection {
    /**
     * @property {Visit} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link Visit}'
     */
    get model () {
        return MasterMed;
    }
}
window.LF.Collection.MasterMeds = MasterMeds;
