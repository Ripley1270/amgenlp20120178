/**
 * Created by jonathan.carr on 25-Jul-17.
 */
import BaseCollection from '../../app/core/collections/StorageBase';
import Headache from '../models/Headache';
export default class Headaches extends BaseCollection {
    /**
     * @property {Visit} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link Visit}'
     */
    get model () {
        return Headache;
    }
}
window.LF.Collection.Headaches = Headaches;
