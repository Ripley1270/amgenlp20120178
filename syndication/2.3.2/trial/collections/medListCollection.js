/**
 * Created by jonathan.carr on 25-Jul-17.
 */
import BaseCollection from '../../app/core/collections/StorageBase';
import medListModel from '../models/medListModel';
/**
 * A collection of patient visits.
 * @class Visits
 * @extends BaseCollection
 */
export default class medListCollection extends BaseCollection {
    /**
     * @property {Visit} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link Visit}'
     */
    get model () {
        return medListModel;
    }
}
window.LF.Collection.medListCollection = medListCollection;
