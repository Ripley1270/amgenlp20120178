/**
 * Created by jonathan.carr on 25-Jul-17.
 */
import BaseCollection from '../../app/core/collections/StorageBase';
import DailyDiary from '../models/DailyDiary';
/**
 * A collection of patient visits.
 * @class Visits
 * @extends BaseCollection
 */
export default class DailyDiaries extends BaseCollection {
    /**
     * @property {Visit} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link Visit}'
     */
    get model () {
        return DailyDiary;
    }
}
window.LF.Collection.DailyDiaries = DailyDiaries;
