/**
 * Created by jonathan.carr on 25-Jul-17.
 */
import BaseCollection from '../../app/core/collections/StorageBase';
import exampleModel from '../models/exampleModel';
/**
 * A collection of patient visits.
 * @class Visits
 * @extends BaseCollection
 */
export default class ExampleCollection extends BaseCollection {
    /**
     * @property {Visit} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link Visit}'
     */
    get model () {
        return exampleModel;
    }
}
window.LF.Collection.ExampleCollection = ExampleCollection;
