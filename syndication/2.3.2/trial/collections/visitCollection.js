/**
 * Created by jonathan.carr on 25-Jul-17.
 */
import BaseCollection from '../../app/core/collections/StorageBase';
import VisitModel from '../models/visitModel';
export default class VisitCollection extends BaseCollection {
    get model () {
        return VisitModel;
    }
}
window.LF.Collection.VisitCollection = VisitCollection;
