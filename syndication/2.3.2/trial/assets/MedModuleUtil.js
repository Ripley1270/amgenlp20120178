import Logger from 'core/Logger';
const logger = new Logger('MedModuleUtils');
LF.MedModuleUtil = LF.MedModuleUtil || {};
LF.MedModuleUtil.updateSubjectMasterMeds = function (syncResult, callback) {
    let xmlDoc   = $.parseXML(syncResult),
        parentEl = $(xmlDoc).find('sync');
    LF.MedModuleUtil.updateMasterMedsByXml(parentEl, function () {
        LF.MedModuleUtil.updateSubjectMedsByXml(parentEl, callback);
    });
};
LF.MedModuleUtil.updateMasterMedsByXml = function (parentElement, done) {
    if(parentElement.attr('updateMasterMeds') === '1'){
        let masterMedsEl = parentElement.find('mastermeds'),
            modelXML,
            modelJSON,
            model,
            collection   = new LF.Collection.MasterMeds(),
            stepModel    = function (counter) {
                try{
                    logger.info(`Parsing Model ${counter} for MasterMeds`);
                    modelXML = masterMedsEl.children()[counter];
                    if(modelXML){
                        logger.info('Start parsing model: MasterMed');
                        model = new LF.Model.MasterMed();
                        modelJSON = {};
                        $.each(modelXML.attributes, function (i, attr) {
                            let name  = attr.name,
                                value = attr.value;
                            modelJSON[name] = value;
                        });
                        model.save(modelJSON, {
                            onSuccess : function () {
                                collection.add(model);
                                logger.info('Successfully saved model, move to the next model');
                                stepModel(counter + 1);
                            },
                            onError   : function () {
                                logger.error('Failed to save model, move to the next model');
                                stepModel(counter + 1);
                            }
                        });
                    }else{
                        logger.info(`Finished with collection: ${collection}`);
                        //stepCollection(collectionCounter + 1);
                        done();
                    }
                }catch(e){
                    logger.error(`Exception processing custom sync: ${e}`);
                    done();
                }
            };
        if(masterMedsEl){
            stepModel(0);
        }
    }else{
        done();
    }
};
LF.MedModuleUtil.updateSubjectMedsByXml = function (parentElement, done) {
    if(parentElement.attr('updateSubjectMeds') === '1'){
        let masterMedsEl = parentElement.find('subjectmeds'),
            modelXML,
            modelJSON,
            model,
            collection   = new LF.Collection.SubjectMeds(),
            stepModel    = function (counter) {
                try{
                    logger.info(`Parsing Model ${counter} for SubjectMeds`);
                    modelXML = masterMedsEl.children()[counter];
                    if(modelXML){
                        logger.info('Start parsing model: SubjectMed');
                        model = new LF.Model.SubjectMed();
                        modelJSON = {};
                        $.each(modelXML.attributes, function (i, attr) {
                            let name  = attr.name,
                                value = attr.value;
                            modelJSON[name] = value;
                        });
                        model.save(modelJSON, {
                            onSuccess : function () {
                                collection.add(model);
                                logger.info('Successfully saved model, move to the next model');
                                stepModel(counter + 1);
                            },
                            onError   : function () {
                                logger.error('Failed to save model, move to the next model');
                                stepModel(counter + 1);
                            }
                        });
                    }else{
                        logger.info(`Finished with collection: ${collection}`);
                        //stepCollection(collectionCounter + 1);
                        done();
                    }
                }catch(e){
                    logger.error(`Exception processing custom sync: ${e}`);
                    done();
                }
            };
        if(masterMedsEl){
            stepModel(0);
        }
    }else{
        done();
    }
};
LF.MedModuleUtil.getMasterMedURLEncodeProperties = function () {
    return [
        'MEDALS1C',
        'MEDNAM1C',
        'MEDDOS1C',
        'MEDUNT1L',
        'MEDLBL1C',
        'MEDLBL2C',
        'MEDLBL3C',
        'MEDLBL4C',
        'MEDLBL5C',
        'MEDLBL6C',
        'MEDLBL7C',
        'MEDLBL8C',
        'MEDLBL9C'
    ];
};
//Save Master Meds using an object as source
LF.MedModuleUtil.saveMasterMed = function (masterMedList, callback) {
    //Parse xml to retrieve master meds and save it to collections
    LF.Utilities.getCollections(['MasterMeds', 'MasterMedSync'], function (collections) {
        let masterMedCollection     = collections['MasterMeds'],
            masterMedSyncCollection = collections['MasterMedSync'],
            medArray                = [],
            cleanUpdate,
            newEffectiveDate,
            masterMedSyncRecord,
            stepModel               = function (meds, modelCounter, callback) {
                let medModel = new LF.Model.MasterMed();
                try{
                    if(modelCounter < meds.length){
                        medModel.save(meds[modelCounter], {
                            onSuccess : function () {
                                stepModel(meds, modelCounter + 1, callback);
                            },
                            onError   : function () {
                                stepModel(meds, modelCounter + 1, callback);
                            }
                        });
                    }
                    else{
                        callback();
                    }
                }catch(e){
                    callback();
                }
            };
        masterMedSyncRecord = _.first(masterMedSyncCollection.models);
        if(!masterMedSyncRecord){
            masterMedSyncRecord = new LF.Model.MasterMedSyncRecord();
        }
        if(masterMedList && masterMedList.master){
            if(masterMedList.master.med && masterMedList.master.med.length > 0){
                medArray = masterMedList.master.med;
                cleanUpdate = masterMedList.master.cleanUpdate;
                newEffectiveDate = masterMedList.master.effectiveDate;
            }
        }
        masterMedSyncRecord.set('EffectiveDate', newEffectiveDate);
        if(cleanUpdate === 'true'){
            masterMedCollection.clear({
                onSuccess : function () {
                    stepModel(medArray, 0, function () {
                        masterMedSyncRecord.save({},
                            {
                                onSuccess : function () {
                                    callback();
                                },
                                onError   : function () {
                                    callback();
                                }
                            });
                    });
                },
                onError   : function (error) {
                    callback();
                }
            });
        }
        else{
            stepModel(medArray, 0, function () {
                masterMedSyncRecord.save({},
                    {
                        onSuccess : function () {
                            callback();
                        },
                        onError   : function () {
                            callback();
                        }
                    });
            });
        }
    });
};
LF.MedModuleUtil.getMedFormText = function (value) {
    let formStringID,
        newText = '';
    switch(value){
        case '1':
            formStringID = 'MEDFORM1';
            break;
        case '2':
            formStringID = 'MEDFORM2';
            break;
        case '3':
            formStringID = 'MEDFORM3';
            break;
        case '4':
            formStringID = 'MEDFORM4';
            break;
        case '5':
            formStringID = 'MEDFORM5';
            break;
        case '6':
            formStringID = 'MEDFORM6';
            break;
        case '7':
            formStringID = 'MEDFORM7';
            break;
        case '8':
            formStringID = 'MEDFORM8';
            break;
        case '9':
            formStringID = 'MEDFORM9';
            break;
        case '10':
            formStringID = 'MEDFORM10';
            break;
        case '11':
            formStringID = 'MEDFORM11';
            break;
        case '12':
            formStringID = 'MEDFORM12';
            break;
    }
    if(formStringID){
        newText = LF.Utilities.getString(formStringID, 'AnalgesicDiary');
    }
    return newText;
};
LF.MedModuleUtil.getMedDisplayText = function (masterMedsModels, medCode, SBJALS1C) {
    let orgText = LF.Utilities.getString('MED_FORMAT', 'AnalgesicDiary'),
        masterMed,
        MEDDOS1C,
        MEDFRM1L,
        newText;
    masterMed = _.first(_.filter(masterMedsModels, function (model) {
        return (model.get('MEDCOD1C') == medCode);
    }));
    if(masterMed){
        MEDDOS1C = masterMed.get('MEDDOS1C');
        MEDFRM1L = masterMed.get('MEDFRM1L');
        newText = orgText.replace('{0}', SBJALS1C);
        newText = newText.replace('{1}', MEDDOS1C);
        newText = newText.replace('{2}', LF.MedModuleUtil.getMedFormText(MEDFRM1L));
    }
    return newText;
};
LF.MedModuleUtil.getScreenshotMasterMeds = function () {
    let defaultMasterMeds = LF.DEFAULT_MASTER_MED_LIST.master.med,
        masterMeds        = [],
        medModel;
    $.each(defaultMasterMeds, function (index, med) {
        medModel = new LF.Model.MasterMed(med);
        masterMeds.push(medModel);
    });
    return masterMeds;
};
LF.MedModuleUtil.getMedAttribute = function (masterMedsModels, medCode, key) {
    let masterMed,
        value;
    if(masterMedsModels){
        masterMed = _.first(_.filter(masterMedsModels, function (model) {
            return (model.get('MEDCOD1C') == medCode);
        }));
    }
    if(masterMed){
        value = masterMed.get(key);
    }
    return value;
};
LF.MedModuleUtil.getMedDisplayTextNoForm = function (masterMedsModels, medCode, MEDALS1C) {
    let orgText = LF.Utilities.getString('MED_FORMAT_NO_FORM', 'AnalgesicDiary'),
        masterMed,
        MEDDOS1C,
        MEDFRM1L,
        newText;
    masterMed = _.first(_.filter(masterMedsModels, function (model) {
        return (model.get('MEDCOD1C') == medCode);
    }));
    if(masterMed){
        MEDDOS1C = masterMed.get('MEDDOS1C');
        MEDFRM1L = masterMed.get('MEDFRM1L');
        newText = orgText.replace('{0}', MEDALS1C);
        newText = newText.replace('{1}', MEDDOS1C);
    }
    return newText;
};
LF.MedModuleUtil.loadDefaultSubjectMedList = function (callback) {
    let subjectMeds = LF.StudyDesign.medConfig.TRAINER_SUBJECT_MED,
        step        = function (index) {
            let subjectMedModel = new LF.Model.SubjectMed();
            subjectMedModel.set('MEDCOD1C', subjectMeds[index].MEDCOD1C);
            subjectMedModel.set('MEDALS1C', subjectMeds[index].MEDALS1C);
            subjectMedModel.save({},
                {
                    onSuccess : function () {
                        index++;
                        if(index < subjectMeds.length){
                            step(index);
                        }else{
                            callback();
                        }
                    },
                    onError   : function () {
                        index++;
                        if(index < subjectMeds.length){
                            step(index);
                        }else{
                            callback();
                        }
                    }
                });
        };
    localStorage.setItem('SUBJECT_MED_LIST_DEFAULT_LOADED', '1');
    step(0);
};
LF.MedModuleUtil.createSubjectList = function () {
    return LF.Utilities.getCollection('SubjectMeds')
        .then((subjectMeds) => {
            if(subjectMeds.length > 0){
                _.each(subjectMeds, function (med, index) {
                    LF.StudyDesign.medicationInfo[`medication${index + 1}`].code = med.get('MEDCOD1C');
                    LF.StudyDesign.medicationInfo[`medication${index + 1}`].name = med.get('MEDALS1C');
                });
            }
            return LF.MedModuleUtil.addSubjectMedAttributes();
        });
};
LF.MedModuleUtil.addSubjectMedAttributes = function () {
    return LF.Utilities.getCollection('MasterMeds')
        .then((masterMeds) => {
            let subjectMeds = LF.StudyDesign.medicationInfo,
                thisMasterMed;
            if(masterMeds.length > 0){
                _.each(subjectMeds, function (med, index) {
                    if(index !== 'Other'){
                        thisMasterMed = _.find(masterMeds, function (masterMed) {
                            return (masterMed.get('MEDCOD1C') === med.code
                            && masterMed.get('MEDFRM1L') !== null);
                        });
                        if(thisMasterMed){
                            LF.StudyDesign.medicationInfo[index].units = LF.MedModuleUtil.getUnitsString(thisMasterMed.get('MEDFRM2L'));
                            LF.StudyDesign.medicationInfo[index].unitsSingle = LF.MedModuleUtil.getSingleFormString(thisMasterMed.get('MEDFRM1L'));
                            LF.StudyDesign.medicationInfo[index].strength = thisMasterMed.get('MEDDOS1C');
                            LF.StudyDesign.medicationInfo[index].methodRoute = thisMasterMed.get('MEDRTE1L');
                        }
                    }
                });
            }
        });
};
LF.MedModuleUtil.getSingleFormString = function (medfrm1l) {
    let formArray = [
        'GRANULATES',
        'MLS_INJ',
        'MLS_ORAL',
        'PATCHES',
        'PILLS',
        'POWDER',
        'SPRAYS',
        'SUPPOSITORIES',
        'INHALATIONS',
        'patch',
        'suppository',
        'injection (mLs)'
    ];
    return formArray[Number(medfrm1l) - 1];
};
LF.MedModuleUtil.getUnitsString = function (medfrm2l) {
    let formArray = [
        'pill(s)',
        'packet(s)',
        'mLs (oral)',
        'teaspoon(s)',
        'tablespoon(s)',
        'drop(s)',
        'oral film(s)',
        'spray(s)',
        'ug',
        'mg',
        'g',
        'fingertip(s)',
        'patch(es)',
        'suppository(ies)',
        'mLs (injectable)'
    ];
    return formArray[Number(medfrm2l) - 1];
};

