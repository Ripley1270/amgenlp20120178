/**
 * @fileOverview This is an example environments study configuration file.
 * @author <a href='mailto:bill.calderwood@ert.com'>William A. Calderwood</a>
 * @version 2.0
 */


export default {
    // when constructing a service name from study name, use this template
    serviceUrlFormat : 'https://{{study}}.phtnetpro.com',
    environments     : [
        {
            id          : 'individual',
            label       : 'Local Server',
            modes       : ['provision'],
            url         : 'http://localhost:3000',
            studyDbName : 'Syndication',
            timeTravel  : true
        },
        {
            id          : 'development',
            label       : 'Development',
            modes       : ['provision'],
            url         : 'https://Amgen20120178P2Dev.phtnetpro.com',
            studyDbName : 'Amgen20120178P2_1',
            timeTravel  : true
        },
        {
            id          : 'staging',
            label       : 'Staging',
            modes       : ['provision'],
            url         : 'https://Amgen20120178P2Staging.phtnetpro.com',
            studyDbName : 'Amgen20120178P2Staging',
            timeTravel  : true
        },
        {
            id          : 'ust',
            label       : 'UST',
            modes       : ['provision'],
            url         : 'https://Amgen20120178P2UST.phtnetpro.com',
            studyDbName : 'Amgen20120178P2_UST',
            timeTravel  : true
        },
        {
            id          : 'production',
            label       : 'Production',
            modes       : ['provision', 'depot'],
            url         : 'https://Amgen20120178P2.phtnetpro.com',
            studyDbName : 'Amgen20120178P2'
        }
    ]
};
