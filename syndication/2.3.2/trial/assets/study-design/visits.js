/**
 * @fileOverview This is an example visits study configuration file.
 * @author <a href='mailto:bill.calderwood@ert.com'>William A. Calderwood</a>
 * @version 2.1
 */
export default {
    visits      : [
        //Tablet only
        /*{
         id    : 'visit1',
         studyEventId: '10',
         displayName  : 'VISIT_1',
         visitType : 'ordered',
         visitOrder : 1,
         forms : ['P_Welcome_SitePad'],
         delay: '0'
         }*/
    ],
    // Server value map. value maps to LF.VisitStates (see application.js)
    visitStatus : {
        1 : 'SKIPPED',
        2 : 'INCOMPLETE',
        3 : 'COMPLETED'
    }
};
