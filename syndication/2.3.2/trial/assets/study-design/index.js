/* // CORE FILES
 import affidavits from './affidavits';
 import supportOptions from './support-options';
 import roles from './roles';
 import timeZoneConfig from './time-zone-config';
 import timeZoneWindowsConfig from './time-zone-windows-config';
 import timeZoneIosConfig from './time-zone-ios-config';
 import toolBoxConfig from './toolbox-config';
 import securityQuestions from './security-questions-config';
 // END CORE FILES */
import studyDesign from './study-design';
import diaries from './diaries';
import environments from './environments';
import roles from './roles';
import schedules from './schedules';
import visits from './visits';
import {mergeObjects} from 'core/utilities/languageExtensions';
export default mergeObjects({},
    /* // CORE FILES
     affidavits, supportOptions, roles, timeZoneConfig, timeZoneWindowsConfig, timeZoneIosConfig, toolBoxConfig,
     securityQuestions,
     // END CORE FILES */
    schedules, environments, roles, studyDesign, visits, diaries
);
