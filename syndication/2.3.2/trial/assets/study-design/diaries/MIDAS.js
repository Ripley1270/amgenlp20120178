export default {
    schedules      : [
        {
            id               : 'MIDAS_Schedule0',
            target           : {
                objectType : 'questionnaire',
                id         : 'MIDAS'
            },
            salience         : 4,
            scheduleRoles    : ['subject'],
            // scheduleFunction : 'checkAlwaysAvailability',
            scheduleFunction : 'visitBasedAvailability',
            scheduleParams   : {
                startAvailability : '00:00',
                endAvailability   : '00:00',
                repeat            : 'always',
                visits            : [
                    {
                        visitID       : 'week192',
                        delayDays     : '0',
                        durationDays  : '1',
                        predecessorID : 'MSQ'
                    },
                    {
                        visitID       : 'week216',
                        delayDays     : '0',
                        durationDays  : '1',
                        predecessorID : 'MSQ'
                    },
                    {
                        visitID       : 'week240',
                        delayDays     : '0',
                        durationDays  : '1',
                        predecessorID : 'MSQ'
                    },
                    {
                        visitID       : 'week264',
                        delayDays     : '0',
                        durationDays  : '1',
                        predecessorID : 'MSQ'
                    },
                    {
                        visitID       : 'week268',
                        delayDays     : '0',
                        durationDays  : '1',
                        predecessorID : 'MSQ'
                    }
                ]
            },
            phase : [
                'TREATMENT',
                'DORMANT'
            ]
        }
    ],
    questionnaires : [
        {
            id             : 'MIDAS',
            SU             : 'MIDAS',
            displayName    : 'DISPLAY_NAME',
            className      : 'questionnaire',
            previousScreen : false,
            affidavit      : 'DEFAULT',
            screens        : [
                'MIDAS_9311',
                'MIDAS_9312',
                'MIDAS_9313',
                'MIDAS_9314',
                'MIDAS_9314_1',
                'MIDAS_9315',
                'MIDAS_9316',
                'MIDAS_9317'
            ],
            branches       : [],
            initialScreen  : []
        }
    ],
    screens        : [
        {
            id        : 'MIDAS_9311',
            className : 'MIDAS_9311',
            questions : [
                {
                    id        : 'MIDAS_9311_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'MIDAS_9312',
            className : 'MIDAS_9312',
            questions : [
                {
                    id        : 'MIDAS_9312_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'MIDAS_9313',
            className : 'MIDAS_9313',
            questions : [
                {
                    id        : 'MIDAS_9313_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'MIDAS_9314',
            className : 'MIDAS_9314',
            questions : [
                {
                    id        : 'MIDAS_9314_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'MIDAS_9314_1',
            className : 'MIDAS_9314_1',
            questions : [
                {
                    id        : 'MIDAS_9314_1_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'MIDAS_9315',
            className : 'MIDAS_9315',
            questions : [
                {
                    id        : 'MIDAS_9315_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'MIDAS_9316',
            className : 'MIDAS_9316',
            questions : [
                {
                    id        : 'MIDAS_9316_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'MIDAS_9317',
            className : 'MIDAS_9317',
            questions : [
                {
                    id        : 'MIDAS_9317_Q0',
                    mandatory : true
                }
            ]
        }
    ],
    questions      : [
        {
            id        : 'MIDAS_9311_Q0',
            text      : 'MIDAS_9311_Q0_TEXT',
            title     : '',
            className : 'MIDAS_9311_Q0'
        }, {
            id        : 'MIDAS_9312_Q0',
            IG        : 'MIDAS',
            IT        : 'MISWRK1N',
            text      : 'MIDAS_9312_Q0_TEXT',
            title     : '',
            className : 'MIDAS_9312_Q0',
            widget    : {
                id                  : 'MIDAS_9312_Q0_W0',
                className           : 'MIDAS_9312_Q0_W0',
                label               : 'MIDAS_9312_Q0_W0_CLICK_TO_MAKE_SELECTION',
                type                : 'NumberSpinner',
                modalTitle          : 'MIDAS_9312_Q0_W0_modalTitle',
                labels              : {
                    labelOne : 'MIDAS_9312_Q0_W0_labels_label1'
                },
                okButtonText        : 'MIDAS_9312_Q0_W0_okButtonText',
                spinnerInputOptions : [
                    {
                        min       : 0,
                        max       : 99,
                        step      : '1',
                        precision : 0
                    }
                ]
            }
        }, {
            id        : 'MIDAS_9313_Q0',
            IG        : 'MIDAS',
            IT        : 'WRKRED1N',
            text      : 'MIDAS_9313_Q0_TEXT',
            title     : '',
            className : 'MIDAS_9313_Q0',
            widget    : {
                id                  : 'MIDAS_9313_Q0_W0',
                className           : 'MIDAS_9313_Q0_W0',
                label               : 'MIDAS_9313_Q0_W0_CLICK_TO_MAKE_SELECTION',
                type                : 'NumberSpinner',
                modalTitle          : 'MIDAS_9313_Q0_W0_modalTitle',
                labels              : {
                    labelOne : 'MIDAS_9313_Q0_W0_labels_label1'
                },
                okButtonText        : 'MIDAS_9313_Q0_W0_okButtonText',
                spinnerInputOptions : [
                    {
                        min       : 0,
                        max       : 99,
                        step      : '1',
                        precision : 0
                    }
                ]
            }
        }, {
            id        : 'MIDAS_9314_Q0',
            IG        : 'MIDAS',
            IT        : 'NOTHSH1L',
            text      : 'MIDAS_9314_Q0_TEXT',
            title     : '',
            className : 'MIDAS_9314_Q0',
            widget    : {
                id                  : 'MIDAS_9314_Q0_W0',
                className           : 'MIDAS_9314_Q0_W0',
                label               : 'MIDAS_9314_Q0_W0_CLICK_TO_MAKE_SELECTION',
                type                : 'NumberSpinner',
                modalTitle          : 'MIDAS_9314_Q0_W0_modalTitle',
                labels              : {
                    labelOne : 'MIDAS_9314_Q0_W0_labels_label1'
                },
                okButtonText        : 'MIDAS_9314_Q0_W0_okButtonText',
                spinnerInputOptions : [
                    {
                        min       : 0,
                        max       : 99,
                        step      : '1',
                        precision : 0
                    }
                ]
            }
        }, {
            id        : 'MIDAS_9314_1_Q0',
            IG        : 'MIDAS',
            IT        : 'HHWHLF1L',
            text      : 'MIDAS_9314_1_Q0_TEXT',
            title     : '',
            className : 'MIDAS_9314_1_Q0',
            widget    : {
                id                  : 'MIDAS_9314_1_Q0_W0',
                className           : 'MIDAS_9314_1_Q0_W0',
                label               : 'MIDAS_9314_1_Q0_W0_CLICK_TO_MAKE_SELECTION',
                type                : 'NumberSpinner',
                modalTitle          : 'MIDAS_9314_1_Q0_W0_modalTitle',
                labels              : {
                    labelOne : 'MIDAS_9314_1_Q0_W0_labels_label1'
                },
                okButtonText        : 'MIDAS_9314_1_Q0_W0_okButtonText',
                spinnerInputOptions : [
                    {
                        min       : 0,
                        max       : 99,
                        step      : '1',
                        precision : 0
                    }
                ]
            }
        }, {
            id        : 'MIDAS_9315_Q0',
            IG        : 'MIDAS',
            IT        : 'PRDWRK1L',
            text      : 'MIDAS_9315_Q0_TEXT',
            title     : '',
            className : 'MIDAS_9315_Q0',
            widget    : {
                id                  : 'MIDAS_9315_Q0_W0',
                className           : 'MIDAS_9315_Q0_W0',
                label               : 'MIDAS_9315_Q0_W0_CLICK_TO_MAKE_SELECTION',
                type                : 'NumberSpinner',
                modalTitle          : 'MIDAS_9315_Q0_W0_modalTitle',
                labels              : {
                    labelOne : 'MIDAS_9315_Q0_W0_labels_label1'
                },
                okButtonText        : 'MIDAS_9315_Q0_W0_okButtonText',
                spinnerInputOptions : [
                    {
                        min       : 0,
                        max       : 99,
                        step      : '1',
                        precision : 0
                    }
                ]
            }
        }, {
            id        : 'MIDAS_9316_Q0',
            IG        : 'MIDAS',
            IT        : 'SOCLSR1N',
            text      : 'MIDAS_9316_Q0_TEXT',
            title     : '',
            className : 'MIDAS_9316_Q0',
            widget    : {
                id                  : 'MIDAS_9316_Q0_W0',
                className           : 'MIDAS_9316_Q0_W0',
                label               : 'MIDAS_9316_Q0_W0_CLICK_TO_MAKE_SELECTION',
                type                : 'NumberSpinner',
                modalTitle          : 'MIDAS_9316_Q0_W0_modalTitle',
                labels              : {
                    labelOne : 'MIDAS_9316_Q0_W0_labels_label1'
                },
                okButtonText        : 'MIDAS_9316_Q0_W0_okButtonText',
                spinnerInputOptions : [
                    {
                        min       : 0,
                        max       : 99,
                        step      : '1',
                        precision : 0
                    }
                ]
            }
        }, {
            id        : 'MIDAS_9317_Q0',
            IG        : 'MIDAS',
            IT        : 'HAVHED1L',
            text      : 'MIDAS_9317_Q0_TEXT',
            title     : '',
            className : 'MIDAS_9317_Q0',
            widget    : {
                id           : 'MIDAS_9317_Q0_W0',
                type         : 'NumericRatingScale',
                width        : '100',
                displayText  : 'MIDAS_9317_Q0_W0_DisplayText',
                reverseOnRtl : true,
                text         : 'MIDAS_9317_Q0_W0_Text',
                answers      : [
                    {
                        value : '0',
                        text  : 'MIDAS_9317_Q0_W0_A0'
                    },
                    {
                        value : '1',
                        text  : 'MIDAS_9317_Q0_W0_A1'
                    },
                    {
                        value : '2',
                        text  : 'MIDAS_9317_Q0_W0_A2'
                    },
                    {
                        value : '3',
                        text  : 'MIDAS_9317_Q0_W0_A3'
                    },
                    {
                        value : '4',
                        text  : 'MIDAS_9317_Q0_W0_A4'
                    },
                    {
                        value : '5',
                        text  : 'MIDAS_9317_Q0_W0_A5'
                    },
                    {
                        value : '6',
                        text  : 'MIDAS_9317_Q0_W0_A6'
                    },
                    {
                        value : '7',
                        text  : 'MIDAS_9317_Q0_W0_A7'
                    },
                    {
                        value : '8',
                        text  : 'MIDAS_9317_Q0_W0_A8'
                    },
                    {
                        value : '9',
                        text  : 'MIDAS_9317_Q0_W0_A9'
                    },
                    {
                        value : '10',
                        text  : 'MIDAS_9317_Q0_W0_A10'
                    }
                ],
                markers      : {
                    left          : 'MIDAS_9317_Q0_W0_LeftMarker',
                    right         : 'MIDAS_9317_Q0_W0_RightMarker',
                    justification : 'inside',
                    position      : 'above',
                    arrow         : true,
                    spaceAllowed  : 2
                }
            }
        }
    ]
};
