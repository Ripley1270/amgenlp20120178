export default {
    schedules      : [
        {
            id               : 'MSQ_Schedule0',
            target           : {
                objectType : 'questionnaire',
                id         : 'MSQ'
            },
            salience         : 3,
            scheduleRoles    : ['subject'],
            // scheduleFunction : 'checkAlwaysAvailability',
            scheduleFunction : 'visitBasedAvailability',
            scheduleParams   : {
                startAvailability : '00:00',
                endAvailability   : '00:00',
                repeat            : 'always',
                visits            : [
                    {
                        visitID      : 'week192',
                        delayDays    : '0',
                        durationDays : '1'
                    },
                    {
                        visitID      : 'week216',
                        delayDays    : '0',
                        durationDays : '1'
                    },
                    {
                        visitID      : 'week240',
                        delayDays    : '0',
                        durationDays : '1'
                    },
                    {
                        visitID      : 'week264',
                        delayDays    : '0',
                        durationDays : '1'
                    },
                    {
                        visitID      : 'week268',
                        delayDays    : '0',
                        durationDays : '1'
                    }
                ]
            },
            phase : [
                'TREATMENT',
                'DORMANT'
            ]
        }
    ],
    questionnaires : [
        {
            id             : 'MSQ',
            SU             : 'MSQ',
            displayName    : 'DISPLAY_NAME',
            className      : 'questionnaire',
            previousScreen : false,
            affidavit      : 'DEFAULT',
            screens        : [
                'MSQ_9211',
                'MSQ_9212',
                'MSQ_9213',
                'MSQ_9214',
                'MSQ_9215',
                'MSQ_9216',
                'MSQ_9217',
                'MSQ_9218',
                'MSQ_9219',
                'MSQ_92110',
                'MSQ_92111',
                'MSQ_92112',
                'MSQ_92113',
                'MSQ_92114',
                'MSQ_92115',
                'MSQ_92116'
            ],
            branches       : [],
            initialScreen  : []
        }
    ],
    screens        : [
        {
            id        : 'MSQ_9211',
            className : 'MSQ_9211',
            questions : [
                {
                    id        : 'MSQ_9211_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'MSQ_9212',
            className : 'MSQ_9212',
            questions : [
                {
                    id        : 'MSQ_9212_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'MSQ_9213',
            className : 'MSQ_9213',
            questions : [
                {
                    id        : 'MSQ_9213_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'MSQ_9214',
            className : 'MSQ_9214',
            questions : [
                {
                    id        : 'MSQ_9214_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'MSQ_9215',
            className : 'MSQ_9215',
            questions : [
                {
                    id        : 'MSQ_9215_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'MSQ_9216',
            className : 'MSQ_9216',
            questions : [
                {
                    id        : 'MSQ_9216_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'MSQ_9217',
            className : 'MSQ_9217',
            questions : [
                {
                    id        : 'MSQ_9217_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'MSQ_9218',
            className : 'MSQ_9218',
            questions : [
                {
                    id        : 'MSQ_9218_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'MSQ_9219',
            className : 'MSQ_9219',
            questions : [
                {
                    id        : 'MSQ_9219_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'MSQ_92110',
            className : 'MSQ_92110',
            questions : [
                {
                    id        : 'MSQ_92110_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'MSQ_92111',
            className : 'MSQ_92111',
            questions : [
                {
                    id        : 'MSQ_92111_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'MSQ_92112',
            className : 'MSQ_92112',
            questions : [
                {
                    id        : 'MSQ_92112_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'MSQ_92113',
            className : 'MSQ_92113',
            questions : [
                {
                    id        : 'MSQ_92113_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'MSQ_92114',
            className : 'MSQ_92114',
            questions : [
                {
                    id        : 'MSQ_92114_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'MSQ_92115',
            className : 'MSQ_92115',
            questions : [
                {
                    id        : 'MSQ_92115_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'MSQ_92116',
            className : 'MSQ_92116',
            questions : [
                {
                    id        : 'MSQ_92116_Q0',
                    mandatory : true
                }
            ]
        }
    ],
    questions      : [
        {
            id        : 'MSQ_9211_Q0',
            text      : 'MSQ_9211_Q0_TEXT',
            title     : '',
            className : 'MSQ_9211_Q0'
        }, {
            id        : 'MSQ_9212_Q0',
            text      : 'MSQ_9212_Q0_TEXT',
            title     : '',
            className : 'MSQ_9212_Q0'
        }, {
            id        : 'MSQ_9213_Q0',
            IG        : 'MSQ',
            IT        : 'INTFAM1L',
            text      : 'MSQ_9213_Q0_TEXT',
            title     : '',
            className : 'MSQ_9213_Q0',
            widget    : {
                id      : 'MSQ_9213_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'MSQ_9213_Q0_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'MSQ_9213_Q0_W0_A1'
                    },
                    {
                        value : '3',
                        text  : 'MSQ_9213_Q0_W0_A2'
                    },
                    {
                        value : '4',
                        text  : 'MSQ_9213_Q0_W0_A3'
                    },
                    {
                        value : '5',
                        text  : 'MSQ_9213_Q0_W0_A4'
                    },
                    {
                        value : '6',
                        text  : 'MSQ_9213_Q0_W0_A5'
                    }
                ]
            }
        }, {
            id        : 'MSQ_9214_Q0',
            IG        : 'MSQ',
            IT        : 'ITREXE1L',
            text      : 'MSQ_9214_Q0_TEXT',
            title     : '',
            className : 'MSQ_9214_Q0',
            widget    : {
                id      : 'MSQ_9214_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'MSQ_9214_Q0_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'MSQ_9214_Q0_W0_A1'
                    },
                    {
                        value : '3',
                        text  : 'MSQ_9214_Q0_W0_A2'
                    },
                    {
                        value : '4',
                        text  : 'MSQ_9214_Q0_W0_A3'
                    },
                    {
                        value : '5',
                        text  : 'MSQ_9214_Q0_W0_A4'
                    },
                    {
                        value : '6',
                        text  : 'MSQ_9214_Q0_W0_A5'
                    }
                ]
            }
        }, {
            id        : 'MSQ_9215_Q0',
            IG        : 'MSQ',
            IT        : 'DIFPER1L',
            text      : 'MSQ_9215_Q0_TEXT',
            title     : '',
            className : 'MSQ_9215_Q0',
            widget    : {
                id      : 'MSQ_9215_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'MSQ_9215_Q0_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'MSQ_9215_Q0_W0_A1'
                    },
                    {
                        value : '3',
                        text  : 'MSQ_9215_Q0_W0_A2'
                    },
                    {
                        value : '4',
                        text  : 'MSQ_9215_Q0_W0_A3'
                    },
                    {
                        value : '5',
                        text  : 'MSQ_9215_Q0_W0_A4'
                    },
                    {
                        value : '6',
                        text  : 'MSQ_9215_Q0_W0_A5'
                    }
                ]
            }
        }, {
            id        : 'MSQ_9216_Q0',
            IG        : 'MSQ',
            IT        : 'WRKDON1L',
            text      : 'MSQ_9216_Q0_TEXT',
            title     : '',
            className : 'MSQ_9216_Q0',
            widget    : {
                id      : 'MSQ_9216_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'MSQ_9216_Q0_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'MSQ_9216_Q0_W0_A1'
                    },
                    {
                        value : '3',
                        text  : 'MSQ_9216_Q0_W0_A2'
                    },
                    {
                        value : '4',
                        text  : 'MSQ_9216_Q0_W0_A3'
                    },
                    {
                        value : '5',
                        text  : 'MSQ_9216_Q0_W0_A4'
                    },
                    {
                        value : '6',
                        text  : 'MSQ_9216_Q0_W0_A5'
                    }
                ]
            }
        }, {
            id        : 'MSQ_9217_Q0',
            IG        : 'MSQ',
            IT        : 'LIMACT1L',
            text      : 'MSQ_9217_Q0_TEXT',
            title     : '',
            className : 'MSQ_9217_Q0',
            widget    : {
                id      : 'MSQ_9217_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'MSQ_9217_Q0_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'MSQ_9217_Q0_W0_A1'
                    },
                    {
                        value : '3',
                        text  : 'MSQ_9217_Q0_W0_A2'
                    },
                    {
                        value : '4',
                        text  : 'MSQ_9217_Q0_W0_A3'
                    },
                    {
                        value : '5',
                        text  : 'MSQ_9217_Q0_W0_A4'
                    },
                    {
                        value : '6',
                        text  : 'MSQ_9217_Q0_W0_A5'
                    }
                ]
            }
        }, {
            id        : 'MSQ_9218_Q0',
            IG        : 'MSQ',
            IT        : 'TOOTRD1L',
            text      : 'MSQ_9218_Q0_TEXT',
            title     : '',
            className : 'MSQ_9218_Q0',
            widget    : {
                id      : 'MSQ_9218_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'MSQ_9218_Q0_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'MSQ_9218_Q0_W0_A1'
                    },
                    {
                        value : '3',
                        text  : 'MSQ_9218_Q0_W0_A2'
                    },
                    {
                        value : '4',
                        text  : 'MSQ_9218_Q0_W0_A3'
                    },
                    {
                        value : '5',
                        text  : 'MSQ_9218_Q0_W0_A4'
                    },
                    {
                        value : '6',
                        text  : 'MSQ_9218_Q0_W0_A5'
                    }
                ]
            }
        }, {
            id        : 'MSQ_9219_Q0',
            IG        : 'MSQ',
            IT        : 'LIMDAY1L',
            text      : 'MSQ_9219_Q0_TEXT',
            title     : '',
            className : 'MSQ_9219_Q0',
            widget    : {
                id      : 'MSQ_9219_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'MSQ_9219_Q0_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'MSQ_9219_Q0_W0_A1'
                    },
                    {
                        value : '3',
                        text  : 'MSQ_9219_Q0_W0_A2'
                    },
                    {
                        value : '4',
                        text  : 'MSQ_9219_Q0_W0_A3'
                    },
                    {
                        value : '5',
                        text  : 'MSQ_9219_Q0_W0_A4'
                    },
                    {
                        value : '6',
                        text  : 'MSQ_9219_Q0_W0_A5'
                    }
                ]
            }
        }, {
            id        : 'MSQ_92110_Q0',
            IG        : 'MSQ',
            IT        : 'CNCWRK1L',
            text      : 'MSQ_92110_Q0_TEXT',
            title     : '',
            className : 'MSQ_92110_Q0',
            widget    : {
                id      : 'MSQ_92110_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'MSQ_92110_Q0_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'MSQ_92110_Q0_W0_A1'
                    },
                    {
                        value : '3',
                        text  : 'MSQ_92110_Q0_W0_A2'
                    },
                    {
                        value : '4',
                        text  : 'MSQ_92110_Q0_W0_A3'
                    },
                    {
                        value : '5',
                        text  : 'MSQ_92110_Q0_W0_A4'
                    },
                    {
                        value : '6',
                        text  : 'MSQ_92110_Q0_W0_A5'
                    }
                ]
            }
        }, {
            id        : 'MSQ_92111_Q0',
            IG        : 'MSQ',
            IT        : 'NEDHLP1L',
            text      : 'MSQ_92111_Q0_TEXT',
            title     : '',
            className : 'MSQ_92111_Q0',
            widget    : {
                id      : 'MSQ_92111_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'MSQ_92111_Q0_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'MSQ_92111_Q0_W0_A1'
                    },
                    {
                        value : '3',
                        text  : 'MSQ_92111_Q0_W0_A2'
                    },
                    {
                        value : '4',
                        text  : 'MSQ_92111_Q0_W0_A3'
                    },
                    {
                        value : '5',
                        text  : 'MSQ_92111_Q0_W0_A4'
                    },
                    {
                        value : '6',
                        text  : 'MSQ_92111_Q0_W0_A5'
                    }
                ]
            }
        }, {
            id        : 'MSQ_92112_Q0',
            IG        : 'MSQ',
            IT        : 'STPWRK1L',
            text      : 'MSQ_92112_Q0_TEXT',
            title     : '',
            className : 'MSQ_92112_Q0',
            widget    : {
                id      : 'MSQ_92112_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'MSQ_92112_Q0_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'MSQ_92112_Q0_W0_A1'
                    },
                    {
                        value : '3',
                        text  : 'MSQ_92112_Q0_W0_A2'
                    },
                    {
                        value : '4',
                        text  : 'MSQ_92112_Q0_W0_A3'
                    },
                    {
                        value : '5',
                        text  : 'MSQ_92112_Q0_W0_A4'
                    },
                    {
                        value : '6',
                        text  : 'MSQ_92112_Q0_W0_A5'
                    }
                ]
            }
        }, {
            id        : 'MSQ_92113_Q0',
            IG        : 'MSQ',
            IT        : 'ABLTGO1L',
            text      : 'MSQ_92113_Q0_TEXT',
            title     : '',
            className : 'MSQ_92113_Q0',
            widget    : {
                id      : 'MSQ_92113_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'MSQ_92113_Q0_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'MSQ_92113_Q0_W0_A1'
                    },
                    {
                        value : '3',
                        text  : 'MSQ_92113_Q0_W0_A2'
                    },
                    {
                        value : '4',
                        text  : 'MSQ_92113_Q0_W0_A3'
                    },
                    {
                        value : '5',
                        text  : 'MSQ_92113_Q0_W0_A4'
                    },
                    {
                        value : '6',
                        text  : 'MSQ_92113_Q0_W0_A5'
                    }
                ]
            }
        }, {
            id        : 'MSQ_92114_Q0',
            IG        : 'MSQ',
            IT        : 'FLTFED1L',
            text      : 'MSQ_92114_Q0_TEXT',
            title     : '',
            className : 'MSQ_92114_Q0',
            widget    : {
                id      : 'MSQ_92114_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'MSQ_92114_Q0_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'MSQ_92114_Q0_W0_A1'
                    },
                    {
                        value : '3',
                        text  : 'MSQ_92114_Q0_W0_A2'
                    },
                    {
                        value : '4',
                        text  : 'MSQ_92114_Q0_W0_A3'
                    },
                    {
                        value : '5',
                        text  : 'MSQ_92114_Q0_W0_A4'
                    },
                    {
                        value : '6',
                        text  : 'MSQ_92114_Q0_W0_A5'
                    }
                ]
            }
        }, {
            id        : 'MSQ_92115_Q0',
            IG        : 'MSQ',
            IT        : 'FLTBRD1L',
            text      : 'MSQ_92115_Q0_TEXT',
            title     : '',
            className : 'MSQ_92115_Q0',
            widget    : {
                id      : 'MSQ_92115_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'MSQ_92115_Q0_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'MSQ_92115_Q0_W0_A1'
                    },
                    {
                        value : '3',
                        text  : 'MSQ_92115_Q0_W0_A2'
                    },
                    {
                        value : '4',
                        text  : 'MSQ_92115_Q0_W0_A3'
                    },
                    {
                        value : '5',
                        text  : 'MSQ_92115_Q0_W0_A4'
                    },
                    {
                        value : '6',
                        text  : 'MSQ_92115_Q0_W0_A5'
                    }
                ]
            }
        }, {
            id        : 'MSQ_92116_Q0',
            IG        : 'MSQ',
            IT        : 'AFRDWN1L',
            text      : 'MSQ_92116_Q0_TEXT',
            title     : '',
            className : 'MSQ_92116_Q0',
            widget    : {
                id      : 'MSQ_92116_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'MSQ_92116_Q0_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'MSQ_92116_Q0_W0_A1'
                    },
                    {
                        value : '3',
                        text  : 'MSQ_92116_Q0_W0_A2'
                    },
                    {
                        value : '4',
                        text  : 'MSQ_92116_Q0_W0_A3'
                    },
                    {
                        value : '5',
                        text  : 'MSQ_92116_Q0_W0_A4'
                    },
                    {
                        value : '6',
                        text  : 'MSQ_92116_Q0_W0_A5'
                    }
                ]
            }
        }
    ]
};
