export default {
    schedules      : [
        {
            id               : 'PROMIS_Schedule0',
            target           : {
                objectType : 'questionnaire',
                id         : 'PROMIS'
            },
            salience         : 3,
            scheduleRoles    : ['subject'],
            scheduleFunction : 'visitBasedAvailability',
            scheduleParams   : {
                startAvailability : '00:00',
                endAvailability   : '00:00',
                repeat            : 'always',
                visits            : [
                    {
                        visitID       : 'week268',
                        delayDays     : '0',
                        durationDays  : '1',
                        predecessorID : 'MIDAS',
                        langList      : ['en_US', 'es_US', 'da_DK', 'fi_FI', 'sv_FI', 'de_DE', 'sv_SE']
                    }
                ]
            },
            phase            : [
                'TREATMENT',
                'DORMANT'
            ]
        }
    ],
    questionnaires : [
        {
            id             : 'PROMIS',
            SU             : 'PROMIS',
            displayName    : 'DISPLAY_NAME',
            className      : 'questionnaire',
            previousScreen : false,
            screens        : [
                'PROMIS_94010_1',
                'PROMIS_94011_3',
                'PROMIS_94012_4',
                'PROMIS_94020_2',
                'PROMIS_94030_5',
                'PROMIS_94040_6'
            ],
            branches       : [],
            initialScreen  : [],
            accessRoles    : [
                'subject'
            ]
        }
    ],
    screens        : [
        {
            id        : 'PROMIS_94010_1',
            className : 'PROMIS_94010_1',
            questions : [
                {
                    id        : 'PROMIS_94010_1_Q1',
                    mandatory : true
                }
            ]
        }, {
            id        : 'PROMIS_94011_3',
            className : 'PROMIS_94011_3',
            questions : [
                {
                    id        : 'PROMIS_94011_3_Q1',
                    mandatory : true
                }
            ]
        }, {
            id        : 'PROMIS_94012_4',
            className : 'PROMIS_94012_4',
            questions : [
                {
                    id        : 'PROMIS_94012_3_Q1',
                    mandatory : true
                }
            ]
        }, {
            id        : 'PROMIS_94020_2',
            className : 'PROMIS_94020_2',
            questions : [
                {
                    id        : 'PROMIS_94020_2_Q1',
                    mandatory : true
                }
            ]
        }, {
            id        : 'PROMIS_94030_5',
            className : 'PROMIS_94030_5',
            questions : [
                {
                    id        : 'PROMIS_94030_5_Q1',
                    mandatory : true
                }
            ]
        }, {
            id        : 'PROMIS_94040_6',
            className : 'PROMIS_94040_6',
            questions : [
                {
                    id        : 'PROMIS_94040_6_Q1',
                    mandatory : true
                }
            ]
        }
    ],
    questions      : [
        {
            id        : 'PROMIS_94010_1_Q1',
            IG        : 'PROMIS',
            IT        : 'ENJLIF1L',
            text      : 'PROMIS_94010_1_Q1_TEXT',
            title     : '',
            className : 'PROMIS_94010_1_Q1',
            widget    : {
                id      : 'PROMIS_94010_1_Q1_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'PROMIS_94010_1_Q1_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'PROMIS_94010_1_Q1_W0_A1'
                    },
                    {
                        value : '3',
                        text  : 'PROMIS_94010_1_Q1_W0_A2'
                    },
                    {
                        value : '4',
                        text  : 'PROMIS_94010_1_Q1_W0_A3'
                    },
                    {
                        value : '5',
                        text  : 'PROMIS_94010_1_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id        : 'PROMIS_94011_3_Q1',
            IG        : 'PROMIS',
            IT        : 'ABLCTR1L',
            text      : 'PROMIS_94011_3_Q1_TEXT',
            title     : '',
            className : 'PROMIS_94011_3_Q1',
            widget    : {
                id      : 'PROMIS_94011_3_Q1_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'PROMIS_94011_3_Q1_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'PROMIS_94011_3_Q1_W0_A1'
                    },
                    {
                        value : '3',
                        text  : 'PROMIS_94011_3_Q1_W0_A2'
                    },
                    {
                        value : '4',
                        text  : 'PROMIS_94011_3_Q1_W0_A3'
                    },
                    {
                        value : '5',
                        text  : 'PROMIS_94011_3_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id        : 'PROMIS_94012_3_Q1',
            IG        : 'PROMIS',
            IT        : 'DTDACT1L',
            text      : 'PROMIS_94012_3_Q1_TEXT',
            title     : '',
            className : 'PROMIS_94012_3_Q1',
            widget    : {
                id      : 'PROMIS_94012_3_Q1_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'PROMIS_94012_3_Q1_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'PROMIS_94012_3_Q1_W0_A1'
                    },
                    {
                        value : '3',
                        text  : 'PROMIS_94012_3_Q1_W0_A2'
                    },
                    {
                        value : '4',
                        text  : 'PROMIS_94012_3_Q1_W0_A3'
                    },
                    {
                        value : '5',
                        text  : 'PROMIS_94012_3_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id        : 'PROMIS_94020_2_Q1',
            IG        : 'PROMIS',
            IT        : 'RECACT1L',
            text      : 'PROMIS_94020_2_Q1_TEXT',
            title     : '',
            className : 'PROMIS_94020_2_Q1',
            widget    : {
                id      : 'PROMIS_94020_2_Q1_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'PROMIS_94020_2_Q1_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'PROMIS_94020_2_Q1_W0_A1'
                    },
                    {
                        value : '3',
                        text  : 'PROMIS_94020_2_Q1_W0_A2'
                    },
                    {
                        value : '4',
                        text  : 'PROMIS_94020_2_Q1_W0_A3'
                    },
                    {
                        value : '5',
                        text  : 'PROMIS_94020_2_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id        : 'PROMIS_94030_5_Q1',
            IG        : 'PROMIS',
            IT        : 'TSKAFH1L',
            text      : 'PROMIS_94030_5_Q1_TEXT',
            title     : '',
            className : 'PROMIS_94030_5_Q1',
            widget    : {
                id      : 'PROMIS_94030_5_Q1_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'PROMIS_94030_5_Q1_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'PROMIS_94030_5_Q1_W0_A1'
                    },
                    {
                        value : '3',
                        text  : 'PROMIS_94030_5_Q1_W0_A2'
                    },
                    {
                        value : '4',
                        text  : 'PROMIS_94030_5_Q1_W0_A3'
                    },
                    {
                        value : '5',
                        text  : 'PROMIS_94030_5_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id        : 'PROMIS_94040_6_Q1',
            IG        : 'PROMIS',
            IT        : 'SOCOTH1L',
            text      : 'PROMIS_94040_6_Q1_TEXT',
            title     : '',
            className : 'PROMIS_94040_6_Q1',
            widget    : {
                id      : 'PROMIS_94040_6_Q1_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'PROMIS_94040_6_Q1_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'PROMIS_94040_6_Q1_W0_A1'
                    },
                    {
                        value : '3',
                        text  : 'PROMIS_94040_6_Q1_W0_A2'
                    },
                    {
                        value : '4',
                        text  : 'PROMIS_94040_6_Q1_W0_A3'
                    },
                    {
                        value : '5',
                        text  : 'PROMIS_94040_6_Q1_W0_A4'
                    }
                ]
            }
        }
    ]
};
