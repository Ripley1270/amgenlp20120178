import DailyDiary from './DailyDiary';
import MSQ from './MSQ';
import MIDAS from './MIDAS';
import PROMIS from './PROMIS';
import VisitConfirmation from './VisitConfirmation';
import {mergeObjects} from 'core/utilities/languageExtensions';
export default mergeObjects({}, DailyDiary, MSQ, MIDAS, PROMIS, VisitConfirmation);

