export default {
    schedules      : [
        {
            id               : 'VisitConfirmation_Schedule0',
            target           : {
                objectType : 'questionnaire',
                id         : 'VisitConfirmation'
            },
            salience         : 1,
            scheduleRoles    : ['site'],
            scheduleFunction : 'visitSelectionSchedule',
            phase            : [
                'TREATMENT',
                'DORMANT'
            ]
        }
    ],
    questionnaires : [
        {
            id             : 'VisitConfirmation',
            SU             : 'VisitConfirmation',
            displayName    : 'DISPLAY_NAME',
            className      : 'questionnaire',
            previousScreen : false,
            affidavit      : 'SignatureAffidavit',
            screens        : [
                'VisitConfirmation_10111',
                'VisitConfirmation_10112',
                'VisitConfirmation_10113',
                'VisitConfirmation_10114',
                'VisitConfirmation_10115'
            ],
            branches       : [
                {
                    branchFrom     : 'VisitConfirmation_10111',
                    branchTo       : 'AFFIDAVIT',
                    branchFunction : 'checkVisitSelection',
                    branchParams   : {}
                },
                {
                    branchFrom     : 'VisitConfirmation_10112',
                    branchTo       : 'VisitConfirmation_10111',
                    branchFunction : 'equalBranchBack',
                    branchParams   : {
                        IG    : 'VisitConfirmation',
                        IT    : 'CON1L',
                        value : '0'
                    }
                },
                {
                    branchFrom     : 'VisitConfirmation_10112',
                    branchTo       : 'VisitConfirmation_10111',
                    branchFunction : 'checkDormantStuff',
                    branchParams   : {
                        IG    : 'VisitConfirmation',
                        IT    : 'CON1L',
                        value : '0'
                    }
                },
                {
                    branchFrom     : 'VisitConfirmation_10113',
                    branchTo       : 'VisitConfirmation_10115',
                    branchFunction : 'equal',
                    branchParams   : {
                        IG    : 'Headache',
                        IT    : 'ENDHED1L',
                        value : '2'
                    }
                },
                {
                    branchFrom     : 'VisitConfirmation_10114',
                    branchTo       : 'AFFIDAVIT',
                    branchFunction : 'always'
                }
            ],
            initialScreen  : []
        }
    ],
    screens        : [
        {
            id        : 'VisitConfirmation_10111',
            className : 'VisitConfirmation_10111',
            questions : [
                {
                    id        : 'VisitConfirmation_10111_Q0',
                    mandatory : true
                }
            ]
        },
        {
            id        : 'VisitConfirmation_10112',
            className : 'VisitConfirmation_10112',
            questions : [
                {
                    id        : 'VisitConfirmation_10112_Q0',
                    mandatory : true
                }
            ]
        },
        {
            id        : 'VisitConfirmation_10113',
            className : 'VisitConfirmation_10113',
            questions : [
                {
                    id        : 'VisitConfirmation_10113_Q0',
                    mandatory : true
                }
            ]
        },
        {
            id        : 'VisitConfirmation_10114',
            className : 'VisitConfirmation_10114',
            questions : [
                {
                    id        : 'VisitConfirmation_10114_Q0',
                    mandatory : true
                }
            ]
        },
        {
            id        : 'VisitConfirmation_10115',
            className : 'VisitConfirmation_10115',
            questions : [
                {
                    id        : 'VisitConfirmation_10115_Q0',
                    mandatory : true
                }
            ]
        }
    ],
    questions      : [
        {
            id        : 'VisitConfirmation_10111_Q0',
            IG        : 'VisitConfirmation',
            IT        : 'VST1L',
            text      : 'VisitConfirmation_10111_Q0_TEXT',
            title     : '',
            className : 'VisitConfirmation_10111_Q0',
            widget    : {
                id    : 'VisitConfirmation_10111_Q0_W0',
                type  : 'DropDownList',
                items : [
                    {
                        value : '1',
                        text  : 'VisitConfirmation_10111_Q0_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'VisitConfirmation_10111_Q0_W0_A1'
                    },
                    {
                        value : '3',
                        text  : 'VisitConfirmation_10111_Q0_W0_A2'
                    },
                    {
                        value : '4',
                        text  : 'VisitConfirmation_10111_Q0_W0_A3'
                    },
                    {
                        value : '5',
                        text  : 'VisitConfirmation_10111_Q0_W0_A4'
                    },
                    {
                        value : '6',
                        text  : 'VisitConfirmation_10111_Q0_W0_A5'
                    },
                    {
                        value : '7',
                        text  : 'VisitConfirmation_10111_Q0_W0_A6'
                    },
                    {
                        value : '8',
                        text  : 'VisitConfirmation_10111_Q0_W0_A7'
                    },
                    {
                        value : '9',
                        text  : 'VisitConfirmation_10111_Q0_W0_A8'
                    }
                ]
            }
        },
        {
            id        : 'VisitConfirmation_10112_Q0',
            IG        : 'VisitConfirmation',
            IT        : 'CON1L',
            text      : 'VisitConfirmation_10112_Q0_TEXT',
            title     : '',
            className : 'VisitConfirmation_10112_Q0',
            widget    : {
                id      : 'VisitConfirmation_10112_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'VisitConfirmation_10112_Q0_W0_A0'
                    },
                    {
                        value : '0',
                        text  : 'VisitConfirmation_10112_Q0_W0_A1'
                    }
                ]
            }
        },
        {
            id        : 'VisitConfirmation_10113_Q0',
            IG        : 'Headache',
            repeating : true,
            IT        : 'ENDHED1L',
            text      : 'VisitConfirmation_10113_Q0_TEXT',
            title     : '',
            className : 'VisitConfirmation_10113_Q0',
            widget    : {
                id      : 'VisitConfirmation_10113_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'VisitConfirmation_10113_Q0_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'VisitConfirmation_10113_Q0_W0_A1'
                    }
                ]
            }
        },
        {
            id        : 'VisitConfirmation_10114_Q0',
            IG        : 'Headache',
            IT        : 'ENDHED1S',
            text      : 'VisitConfirmation_10114_Q0_TEXT',
            title     : '',
            className : 'VisitConfirmation_10114_Q0',
            widget    : {
                id            : 'VisitConfirmation_10114_Q0_W0',
                className     : 'VisitConfirmation_10114_Q0_W0',
                type          : 'DateTimeSpinner',
                modalTitle    : 'VisitConfirmation_10114_Q0_W0_modalTitle',
                okButtonText  : 'VisitConfirmation_10114_Q0_W0_okButtonText',
                label         : 'VisitConfirmation_10114_Q0_W0_label',
                dateFormat    : 'DD-MMM-YYYY',
                timeFormat    : 'HH:mm',
                displayFormat : 'DD-MMM-YYYY HH:mm',
                minDate       : () => {
                    return LF.Utilities.getOngoingHeadaches()
                        .then(headaches => {
                            let ongoingHeadache = _.last(headaches),
                                ongoingHeadacheStart;
                            if(ongoingHeadache){
                                ongoingHeadacheStart = new Date(ongoingHeadache.get('startDate'));
                                return LF.Utilities.timeStamp(new Date(ongoingHeadacheStart));
                            }else{
                                return LF.Utilities.timeStamp(new Date());
                            }
                        });
                },
                maxDate       : () => {
                    return LF.Utilities.timeStamp(new Date());
                },
                initialDate   : () => {
                    return LF.Utilities.timeStamp(new Date());
                },
            }
        },
        {
            id        : 'VisitConfirmation_10115_Q0',
            text      : 'VisitConfirmation_10115_Q0_TEXT',
            title     : '',
            className : 'VisitConfirmation_10115_Q0'
        }
    ],
    rules          : [
        {
            id       : 'getOngoingHeadacheForVisitConf',
            trigger  : [
                'QUESTIONNAIRE:Open/VisitConfirmation'
            ],
            evaluate : function (filter, resume) {
                resume(!localStorage.getItem('ScreenshotMode'));
            },
            resolve  : [
                {
                    action (input, done) {
                        LF.Utilities.getOngoingHeadaches()
                            .then(ongoingheadaches => {
                                let currentPhase  = LF.Data.Questionnaire.subject.get('phase'),
                                    headacheEnded = _.last(LF.Data.Questionnaire.queryAnswersByIT('ENDHED1S')),
                                    visitSelected = _.last(LF.Data.Questionnaire.queryAnswersByIT('VST1L')),
                                    visitList     = LF.StudyDesign.visitList;
                                ongoingheadaches = _.last(ongoingheadaches);
                                if(ongoingheadaches){
                                    LF.Utilities.addIT({
                                        question_id      : 'HEDNUM1N',
                                        questionnaire_id : 'VisitConfirmation',
                                        response         : ongoingheadaches.get('headacheID'),
                                        IG               : 'Headache',
                                        IGR              : 1,
                                        IT               : 'HEDNUM1N'
                                    });
                                }
                                done();
                            });
                    }
                }
            ]
        },
        {
            id       : 'saveVisitSelection',
            trigger  : [
                'QUESTIONNAIRE:Completed/VisitConfirmation'
            ],
            evaluate : function (filter, resume) {
                resume(!localStorage.getItem("ScreenshotMode"));
            },
            resolve  : [
                {
                    action (input, done) {
                        let visit           = _.last(LF.Data.Questionnaire.queryAnswersByIT('VST1L')),
                            headacheEnded   = _.last(LF.Data.Questionnaire.queryAnswersByIT('ENDHED1S')),
                            ongoingHeadache = _.last(LF.Data.Questionnaire.queryAnswersByIT('HEDNUM1N')),
                            visitList       = LF.StudyDesign.visitList,
                            phaseList       = LF.StudyDesign.studyPhase,
                            currentPhase    = LF.Data.Questionnaire.subject.get('phase');
                        LF.Utilities.saveNewModel('VisitModel', {
                            visitDate : LF.Utilities.timeStamp(new Date()),
                            visit     : visit ? visit.get('response') : ''
                        });
                        visit = visit ? visit.get('response') : '0';
                        //Week 212, Week 236, or Week 260 visit
                        if(ongoingHeadache && currentPhase === phaseList.DORMANT && !headacheEnded && (visit === visitList.week212 || visit === visitList.week236 || visit === visitList.week260 || visit === visitList.week264)){
                            LF.Utilities.addIT({
                                question_id      : 'UNKEND1B',
                                questionnaire_id : 'VisitConfirmation',
                                response         : '1',
                                IG               : 'Headache',
                                IGR              : 1,
                                IT               : 'UNKEND1B'
                            });
                        }
                        done();
                    }
                },
                {
                    action : 'changePhase',
                    data   : function (context, done) {
                        let visit        = _.last(LF.Data.Questionnaire.queryAnswersByIT('VST1L')),
                            //subjectContinue = _.last(LF.Data.Questionnaire.queryAnswersByIT('SUBCON1L')),
                            subject      = LF.Data.Questionnaire.subject,
                            phaseList    = LF.StudyDesign.studyPhase,
                            visitList    = LF.StudyDesign.visitList,
                            currentPhase = LF.Data.Questionnaire.subject.get('phase');
                        if(visit){
                            visit = visit.get('response');
                            if(currentPhase === phaseList.TREATMENT && (visit === visitList.week192 || visit === visitList.week216 || visit === visitList.week240 || visit === visitList.week268 )){
                                console.log('Going into Dormant Phase...');
                                done({
                                    change                 : true,
                                    phase                  : phaseList.DORMANT,
                                    phaseStartDateTZOffset : LF.Utilities.timeStamp(new Date())
                                });
                            }else if(currentPhase === phaseList.DORMANT && (visit === visitList.week212 || visit === visitList.week236 || visit === visitList.week260 || visit === visitList.week264)){
                                console.log('Going into Treatment Phase...');
                                done({
                                    change                 : true,
                                    phase                  : phaseList.TREATMENT,
                                    phaseStartDateTZOffset : LF.Utilities.timeStamp(new Date())
                                });
                            }else{
                                //Visit selected does not trigger phase change
                                //don't change Phase
                                console.log('No phase change triggered');
                                done({
                                    change                 : false,
                                    phase                  : subject.get('phase'),
                                    phaseStartDateTZOffset : subject.get('phaseStartDateTZOffset')
                                });
                            }
                        }else{
                            //no visit selected somehow
                            //don't change Phase
                            done({
                                change                 : false,
                                phase                  : subject.get('phase'),
                                phaseStartDateTZOffset : subject.get('phaseStartDateTZOffset')
                            });
                        }
                    }
                }
            ]
        }
    ]
};
