export default {
    schedules      : [
        {
            id               : 'DailyDiary_Schedule0',
            target           : {
                objectType : 'questionnaire',
                id         : 'DailyDiary'
            },
            salience         : 2,
            scheduleRoles    : ['subject'],
            scheduleFunction : 'checkRepeatByDateAvailability',
            // scheduleFunction : 'DailyRetroDiaryAvailability',
            scheduleParams   : {
                startAvailability : '17:00',
                endAvailability   : '00:00'
            },
            phase : [
                'TREATMENT'
            ],
            alarmFunction    : 'standardAlarm',
            alarmParams      : {
                id               : 1,
                onAlarm          : 'alarmTriggerFunction',
                time             : '20:00',
                repeat           : 'daily',
                reminders        : 2,
                reminderInterval : 30,
                onAlarmParams    : {
                    testData : ''
                },
                subjectConfig    : {
                    minAlarmTime       : '17:00',
                    maxAlarmTime       : '22:30',
                    alarmRangeInterval : 30,
                    alarmOffSubject    : false
                }
            }
        }
    ],
    questionnaires : [
        {
            id             : 'DailyDiary',
            SU             : 'DailyDiary',
            displayName    : 'DISPLAY_NAME',
            className      : 'questionnaire',
            previousScreen : false,
            affidavit      : 'DEFAULT',
            screens        : [
                'DailyDiary_9111',
                'DailyDiary_9112',
                'DailyDiary_9113',
                'DailyDiary_9114',
                'DailyDiary_9115',
                'DailyDiary_9116',
                'DailyDiary_9117',
                'DailyDiary_9118',
                'DailyDiary_9119_0',
                'DailyDiary_9119_1',
                'DailyDiary_91110',
                'DailyDiary_91111',
                'DailyDiary_91112',
                'DailyDiary_91113',
                'DailyDiary_91114',
                'DailyDiary_91115',
                'DailyDiary_91116',
                'DailyDiary_91117',
                'DailyDiary_91118',
                'DailyDiary_91119',
                'DailyDiary_91120',
                'DailyDiary_91121',
                'DailyDiary_91122',
                'DailyDiary_91123',
                'DailyDiary_91124',
                'DailyDiary_91125',
                'DailyDiary_91126',
                'DailyDiary_91127',
                'DailyDiary_91128',
                'DailyDiary_91129',
                'DailyDiary_91130',
                'DailyDiary_91131',
                'DailyDiary_91132',
                'DailyDiary_91133',
                'DailyDiary_91134',
                'DailyDiary_91135',
                'DailyDiary_91136',
                'DailyDiary_91137',
                'DailyDiary_91138',
                'DailyDiary_91139',
                'DailyDiary_91140',
                'DD401',
                'DD402',
                'DD403',
                'DD404_0',
                'DD404_1',
                'DD407',
                'DD405',
                'DD406',
                'DailyDiary_91141',
                'DailyDiary_91142',
                'DailyDiary_91143'
            ],
            branches       : [
                {
                    branchFrom     : 'DailyDiary_9115',
                    branchTo       : 'DailyDiary_91115',
                    branchFunction : 'equal',
                    branchParams   : {
                        IG    : 'DailyDiary',
                        IT    : 'MEDAUR1L',
                        value : '0'
                    }
                },
                {
                    branchFrom     : 'DailyDiary_9117',
                    branchTo       : 'DailyDiary_91110',
                    branchFunction : 'equal',
                    branchParams   : {
                        IG    : 'AURA',
                        IT    : 'MEDNAM1L',
                        value : '10'
                    }
                },
                {
                    branchFrom     : 'DailyDiary_9117',
                    branchTo       : 'DailyDiary_9119_0',
                    branchFunction : 'checkMedicationProximity',
                    branchParams   : {
                        IG : 'AURA'
                    }
                },
                {
                    branchFrom     : 'DailyDiary_9118',
                    branchTo       : 'DailyDiary_9116',
                    branchFunction : 'equalBranchBack',
                    branchParams   : {
                        IG    : 'AURA',
                        IT    : 'M27IT',
                        value : '1'
                    }
                },
                {
                    branchFrom             : 'DailyDiary_9118',
                    branchTo               : 'DailyDiary_91111',
                    clearBranchedResponses : false,
                    branchFunction         : 'clearLoopThenEqual',
                    branchParams           : {
                        IG       : 'AURA',
                        IT       : 'M27IT',
                        value    : '2',
                        headache : '1'
                    }
                },
                {
                    branchFrom     : 'DailyDiary_9118',
                    branchTo       : 'DailyDiary_9119_0',
                    branchFunction : 'isMilliliters',
                    branchParams   : {
                        IG : 'AURA'
                    }
                },
                /*{
                 branchFrom             : 'DailyDiary_9119_0',
                 branchTo               : 'DailyDiary_91111',
                 clearBranchedResponses : false,
                 branchFunction         : 'checkHeadache',
                 branchParams           : {
                 IG       : 'AURA',
                 headache : '1'
                 }
                 },*/
                {
                    branchFrom             : 'DailyDiary_9119_0',
                    branchTo               : 'DailyDiary_91111',
                    clearBranchedResponses : false,
                    branchFunction         : 'always',
                    branchParams           : {}
                },
                /*{
                 branchFrom     : 'DailyDiary_9119_1',
                 branchTo       : 'DailyDiary_91111',
                 branchFunction : 'checkHeadache',
                 branchParams   : {
                 IG       : 'AURA',
                 headache : '1'
                 }
                 },*/
                {
                    branchFrom     : 'DailyDiary_9119_1',
                    branchTo       : 'DailyDiary_91111',
                    branchFunction : 'always',
                    branchParams   : {}
                },
                /*{
                 branchFrom     : 'DailyDiary_91110',
                 branchTo       : 'DailyDiary_91111',
                 branchFunction : 'checkHeadache',
                 branchParams   : {
                 IG       : 'AURA',
                 headache : '1'
                 }
                 },*/
                {
                    branchFrom     : 'DailyDiary_91111',
                    branchTo       : 'DailyDiary_91115',
                    branchFunction : 'equal',
                    branchParams   : {
                        IG    : 'AURA',
                        IT    : 'M36IT',
                        value : '0'
                    }
                },
                {
                    branchFrom     : 'DailyDiary_91111',
                    branchTo       : 'DailyDiary_91112',
                    branchFunction : 'checkLoops',
                    branchParams   : {
                        IG          : 'AURA',
                        maxLoops    : 5,
                        startOfLoop : 'DailyDiary_9116',
                        endOfLoop   : 'DailyDiary_91114'
                    }
                },
                {
                    branchFrom     : 'DailyDiary_91112',
                    branchTo       : 'DailyDiary_91141',
                    branchFunction : 'equal',
                    branchParams   : {
                        IG    : 'AURA',
                        IT    : 'M39IT',
                        value : '0'
                    }
                },
                {
                    branchFrom     : 'DailyDiary_91112',
                    branchTo       : 'DailyDiary_91112',
                    branchFunction : 'checkLoops',
                    branchParams   : {
                        IG          : 'AURA',
                        maxLoops    : 5,
                        startOfLoop : 'DailyDiary_9116',
                        endOfLoop   : 'DailyDiary_91113'
                    }
                },
                {
                    branchFrom     : 'DailyDiary_91113',
                    branchTo       : 'DailyDiary_91141',
                    branchFunction : 'always',
                    branchParams   : {}
                },
                {
                    branchFrom     : 'DailyDiary_91115',
                    branchTo       : 'DailyDiary_91116',
                    branchFunction : 'checkOngoingHeadache',
                    branchParams   : {}
                },
                {
                    branchFrom     : 'DailyDiary_91115',
                    branchTo       : 'DailyDiary_91118',
                    branchFunction : 'always',
                    branchParams   : {}
                },
                {
                    branchFrom     : 'DailyDiary_91116',
                    branchTo       : 'DailyDiary_91121',
                    branchFunction : 'equal',
                    branchParams   : {
                        IG    : 'Headache',
                        IT    : 'ENDHED1L',
                        value : '1'
                    }
                },
                {
                    branchFrom     : 'DailyDiary_91116',
                    branchTo       : 'DailyDiary_91117',
                    branchFunction : 'checkTimeSinceLastHeadache',
                    branchParams   : {}
                },
                {
                    //Answered No and time is less than 72 hours
                    branchFrom     : 'DailyDiary_91116',
                    branchTo       : 'AFFIDAVIT',
                    branchFunction : 'always',
                    branchParams   : {}
                },
                {
                    branchFrom     : 'DailyDiary_91117',
                    branchTo       : 'DailyDiary_91121',
                    branchFunction : 'equal',
                    branchParams   : {
                        IG    : 'Headache',
                        IT    : 'M54IT',
                        value : '1'
                    }
                },
                {
                    //Answered "Continue; this headache is ongoing"
                    branchFrom     : 'DailyDiary_91117',
                    branchTo       : 'AFFIDAVIT',
                    branchFunction : 'always',
                    branchParams   : {}
                },
                {
                    branchFrom     : 'DailyDiary_91118',
                    branchTo       : 'DailyDiary_91143',
                    branchFunction : 'equal',
                    branchParams   : {
                        IG    : 'Headache',
                        IT    : 'M57IT',
                        value : '0'
                    }
                },
                {
                    //Time Overlaps
                    branchFrom     : 'DailyDiary_91119',
                    branchTo       : 'DailyDiary_91120',
                    branchFunction : 'checkTimeOverlap',
                    branchParams   : {
                        IG : 'Headache',
                        IT : 'HEDBEG1S'
                    }
                },
                {
                    //Time does not Overlap
                    branchFrom             : 'DailyDiary_91119',
                    branchTo               : 'AFFIDAVIT',
                    clearBranchedResponses : false,
                    branchFunction         : 'equal',
                    branchParams           : {
                        IG    : 'DailyDiary',
                        IT    : 'HEDNOW1L',
                        value : '1'
                    }
                },
                {
                    //Time does not Overlap
                    branchFrom             : 'DailyDiary_91119',
                    branchTo               : 'AFFIDAVIT',
                    clearBranchedResponses : false,
                    branchFunction         : 'equal',
                    branchParams           : {
                        IG    : 'DailyDiary',
                        IT    : 'HEDNOW1L',
                        value : '0'
                    }
                },
                {
                    //Time does not Overlap
                    branchFrom     : 'DailyDiary_91119',
                    branchTo       : 'DailyDiary_91121',
                    branchFunction : 'always',
                    branchParams   : {}
                },
                {
                    branchFrom     : 'DailyDiary_91120',
                    branchTo       : 'DailyDiary_91119',
                    branchFunction : 'equalBranchBack',
                    branchParams   : {
                        IG    : 'Headache',
                        IT    : 'M63IT',
                        value : '1'
                    }
                },
                {
                    branchFrom             : 'DailyDiary_91120',
                    branchTo               : 'AFFIDAVIT',
                    clearBranchedResponses : false,
                    branchFunction         : 'equal',
                    branchParams           : {
                        IG    : 'Headache',
                        IT    : 'HEDNOW1L',
                        value : '1'
                    }
                },
                {
                    branchFrom             : 'DailyDiary_91120',
                    branchTo               : 'AFFIDAVIT',
                    clearBranchedResponses : false,
                    branchFunction         : 'equal',
                    branchParams           : {
                        IG    : 'Headache',
                        IT    : 'HEDNOW1L',
                        value : '0'
                    }
                },
                {
                    branchFrom             : 'DailyDiary_91120',
                    branchTo               : 'DailyDiary_91143',
                    clearBranchedResponses : false,
                    branchFunction         : 'clearLoopThenEqual',
                    branchParams           : {
                        IG    : 'Headache',
                        IT    : 'M63IT',
                        value : '2'
                    }
                },
                {
                    branchFrom     : 'DailyDiary_91121',
                    branchTo       : 'DailyDiary_91127',
                    branchFunction : 'equal',
                    branchParams   : {
                        IG    : 'Headache',
                        IT    : 'HEDSLP1L',
                        value : '1'
                    }
                },
                {
                    //Time Overlaps
                    branchFrom     : 'DailyDiary_91122',
                    branchTo       : 'DailyDiary_91124',
                    branchFunction : 'checkTimeOverlap',
                    branchParams   : {
                        IG : 'Headache',
                        IT : 'ENDHED1S'
                    }
                },
                {
                    //Time does not Overlap
                    branchFrom     : 'DailyDiary_91122',
                    branchTo       : 'DailyDiary_91125',
                    branchFunction : 'checkMinutesElapsed',
                    branchParams   : {
                        durationType : 'Headache'
                    }
                },
                {
                    //Time does not Overlap
                    branchFrom     : 'DailyDiary_91122',
                    branchTo       : 'DailyDiary_91126',
                    branchFunction : 'checkHoursElapsed',
                    branchParams   : {
                        durationType : 'Headache'
                    }
                },
                {
                    //Time does not Overlap
                    branchFrom     : 'DailyDiary_91122',
                    branchTo       : 'DailyDiary_91130',
                    branchFunction : 'always',
                    branchParams   : {}
                },
                {
                    branchFrom     : 'DailyDiary_91124',
                    branchTo       : 'DailyDiary_91127',
                    branchFunction : 'equalBranchBack',
                    branchParams   : {
                        simulateBackButton : true,
                        IG                 : 'Headache',
                        IT                 : 'M75IT',
                        value              : '1'
                    }
                },
                {
                    branchFrom             : 'DailyDiary_91124',
                    branchTo               : 'DailyDiary_91143',
                    clearBranchedResponses : false,
                    branchFunction         : 'clearLoopThenEqual',
                    branchParams           : {
                        IG    : 'Headache',
                        IT    : 'M75IT',
                        value : '2'
                    }
                },
                {
                    branchFrom             : 'DailyDiary_91125',
                    branchTo               : 'DailyDiary_91128',
                    clearBranchedResponses : false,
                    branchFunction         : 'DD250Branching',
                    branchParams           : {
                        IG    : 'Headache',
                        IT    : 'HEDSLP1L',
                        value : '1'
                    }
                },
                /*{
                 branchFrom     : 'DailyDiary_91125',
                 branchTo       : 'DailyDiary_91130',
                 branchFunction : 'always',
                 branchParams   : {}
                 },*/
                {
                    branchFrom             : 'DailyDiary_91126',
                    branchTo               : 'DailyDiary_91127',
                    clearBranchedResponses : false,
                    branchFunction         : 'DD260Branching',
                    branchParams           : {
                        IG    : 'Headache',
                        IT    : 'M81IT',
                        value : '1'
                    }
                },
                /*{
                 branchFrom     : 'DailyDiary_91126',
                 branchTo       : 'DailyDiary_91128',
                 branchFunction : 'equal',
                 branchParams   : {
                 IG    : 'Headache',
                 IT    : 'HEDSLP1L',
                 value : '1'
                 }
                 },
                 {
                 branchFrom     : 'DailyDiary_91126',
                 branchTo       : 'DailyDiary_91130',
                 branchFunction : 'always',
                 branchParams   : {}
                 },*/
                {
                    //Time Overlaps
                    branchFrom     : 'DailyDiary_91127',
                    branchTo       : 'DailyDiary_91124',
                    branchFunction : 'checkTimeOverlap',
                    branchParams   : {
                        IG : 'Headache',
                        IT : 'FELSLP1S'
                    }
                },
                {
                    //Time does not Overlap
                    branchFrom     : 'DailyDiary_91127',
                    branchTo       : 'DailyDiary_91125',
                    branchFunction : 'checkMinutesElapsed',
                    branchParams   : {
                        durationType : 'Headache'
                    }
                },
                {
                    //Time does not Overlap
                    branchFrom     : 'DailyDiary_91127',
                    branchTo       : 'DailyDiary_91126',
                    branchFunction : 'checkHoursElapsed',
                    branchParams   : {
                        durationType : 'Headache'
                    }
                },
                {
                    //Time does not Overlap
                    branchFrom     : 'DailyDiary_91127',
                    branchTo       : 'DailyDiary_91128',
                    branchFunction : 'always',
                    branchParams   : {}
                },
                {
                    branchFrom     : 'DailyDiary_91128',
                    branchTo       : 'DailyDiary_91130',
                    branchFunction : 'checkSleepDuration',
                    branchParams   : {}
                },
                //NEED TO ADD THINGS STILL
                {
                    branchFrom     : 'DailyDiary_91129',
                    branchTo       : 'DailyDiary_91127',
                    branchFunction : 'equalBranchBack',
                    branchParams   : {
                        IG    : 'Headache',
                        IT    : 'M90IT',
                        value : '1'
                    }
                },
                {
                    branchFrom     : 'DailyDiary_91133',
                    branchTo       : 'DailyDiary_91134',
                    branchFunction : 'equal',
                    branchParams   : {
                        IG    : 'Headache',
                        IT    : 'HEDSYM5B',
                        value : '1'
                    }
                },
                {
                    branchFrom     : 'DailyDiary_91133',
                    branchTo       : 'DailyDiary_91135',
                    branchFunction : 'equal',
                    branchParams   : {
                        IG    : 'Headache',
                        IT    : 'HEDSYM6B',
                        value : '1'
                    }
                },
                {
                    branchFrom     : 'DailyDiary_91133',
                    branchTo       : 'DailyDiary_91136',
                    branchFunction : 'equal',
                    branchParams   : {
                        IG    : 'Headache',
                        IT    : 'HEDSYM7B',
                        value : '1'
                    }
                },
                {
                    branchFrom     : 'DailyDiary_91133',
                    branchTo       : 'DailyDiary_91137',
                    branchFunction : 'equal',
                    branchParams   : {
                        IG    : 'Headache',
                        IT    : 'HEDSYM8B',
                        value : '1'
                    }
                },
                {
                    branchFrom     : 'DailyDiary_91133',
                    branchTo       : 'DailyDiary_91138',
                    branchFunction : 'equal',
                    branchParams   : {
                        IG    : 'Headache',
                        IT    : 'HEDSYM9B',
                        value : '1'
                    }
                },
                {
                    branchFrom     : 'DailyDiary_91134',
                    branchTo       : 'DailyDiary_91135',
                    branchFunction : 'equal',
                    branchParams   : {
                        IG    : 'Headache',
                        IT    : 'HEDSYM6B',
                        value : '1'
                    }
                },
                {
                    branchFrom     : 'DailyDiary_91134',
                    branchTo       : 'DailyDiary_91136',
                    branchFunction : 'equal',
                    branchParams   : {
                        IG    : 'Headache',
                        IT    : 'HEDSYM7B',
                        value : '1'
                    }
                },
                {
                    branchFrom     : 'DailyDiary_91134',
                    branchTo       : 'DailyDiary_91137',
                    branchFunction : 'equal',
                    branchParams   : {
                        IG    : 'Headache',
                        IT    : 'HEDSYM8B',
                        value : '1'
                    }
                },
                {
                    branchFrom     : 'DailyDiary_91134',
                    branchTo       : 'DailyDiary_91138',
                    branchFunction : 'always',
                    branchParams   : {}
                },
                {
                    branchFrom     : 'DailyDiary_91135',
                    branchTo       : 'DailyDiary_91136',
                    branchFunction : 'equal',
                    branchParams   : {
                        IG    : 'Headache',
                        IT    : 'HEDSYM7B',
                        value : '1'
                    }
                },
                {
                    branchFrom     : 'DailyDiary_91135',
                    branchTo       : 'DailyDiary_91137',
                    branchFunction : 'equal',
                    branchParams   : {
                        IG    : 'Headache',
                        IT    : 'HEDSYM8B',
                        value : '1'
                    }
                },
                {
                    branchFrom     : 'DailyDiary_91135',
                    branchTo       : 'DailyDiary_91138',
                    branchFunction : 'always',
                    branchParams   : {}
                },
                {
                    branchFrom     : 'DailyDiary_91136',
                    branchTo       : 'DailyDiary_91137',
                    branchFunction : 'equal',
                    branchParams   : {
                        IG    : 'Headache',
                        IT    : 'HEDSYM8B',
                        value : '1'
                    }
                },
                {
                    branchFrom     : 'DailyDiary_91136',
                    branchTo       : 'DailyDiary_91138',
                    branchFunction : 'always',
                    branchParams   : {}
                },
                {
                    branchFrom     : 'DailyDiary_91138',
                    branchTo       : 'DailyDiary_91140',
                    branchFunction : 'equal',
                    branchParams   : {
                        IG    : 'Headache',
                        IT    : 'EXPAUR1L',
                        value : '0'
                    }
                },
                {
                    branchFrom             : 'DailyDiary_91140',
                    branchTo               : 'DailyDiary_91141',
                    clearBranchedResponses : false,
                    branchFunction         : 'equal',
                    branchParams           : {
                        IG    : 'Headache',
                        IT    : 'MEDHED1L',
                        value : '0'
                    }
                },
                {
                    branchFrom     : 'DailyDiary_91140',
                    branchTo       : 'DD401',
                    branchFunction : 'setStartingMedLoop',
                    branchParams   : {
                        IG    : 'Headache',
                        IT    : 'MEDHED1L',
                        value : '1'
                    }
                },
                {
                    branchFrom     : 'DD402',
                    branchTo       : 'DD407',
                    branchFunction : 'equal',
                    branchParams   : {
                        IG    : 'HeadacheMed',
                        IT    : 'MEDNAM1L',
                        value : '10'
                    }
                },
                {
                    branchFrom     : 'DD402',
                    branchTo       : 'DD404_0',
                    branchFunction : 'checkMedicationProximity',
                    branchParams   : {
                        IG : 'HeadacheMed'
                    }
                },
                {
                    branchFrom     : 'DD403',
                    branchTo       : 'DD401',
                    branchFunction : 'equalBranchBack',
                    branchParams   : {
                        IG    : 'HeadacheMed',
                        IT    : 'M27IT',
                        value : '1'
                    }
                },
                {
                    branchFrom             : 'DD403',
                    branchTo               : 'DailyDiary_91141',
                    clearBranchedResponses : false,
                    branchFunction         : 'clearLoopThenEqual',
                    branchParams           : {
                        IG    : 'HeadacheMed',
                        IT    : 'M27IT',
                        value : '2'
                    }
                },
                {
                    branchFrom     : 'DD403',
                    branchTo       : 'DD404_0',
                    branchFunction : 'isMilliliters',
                    branchParams   : {
                        IG : 'HeadacheMed'
                    }
                },
                {
                    branchFrom             : 'DD404_0',
                    branchTo               : 'DD405',
                    clearBranchedResponses : false,
                    branchFunction         : 'always',
                    branchParams           : {}
                },
                {
                    branchFrom             : 'DD404_1',
                    branchTo               : 'DD405',
                    clearBranchedResponses : false,
                    branchFunction         : 'always',
                    branchParams           : {}
                },
                {
                    branchFrom             : 'DD405',
                    branchTo               : 'DailyDiary_91141',
                    clearBranchedResponses : false,
                    branchFunction         : 'equal',
                    branchParams           : {
                        IG    : 'HeadacheMed',
                        IT    : 'M39IT',
                        value : '0'
                    }
                },
                {
                    branchFrom             : 'DD405',
                    clearBranchedResponses : false,
                    branchTo               : 'DD406',
                    branchFunction         : 'checkLoops',
                    branchParams           : {
                        IG          : 'HeadacheMed',
                        maxLoops    : 5,
                        startOfLoop : 'DD401',
                        endOfLoop   : 'DD406'
                    }
                },
                {
                    branchFrom     : 'DailyDiary_91141',
                    branchTo       : 'DailyDiary_91143',
                    branchFunction : 'equal',
                    branchParams   : {
                        IG    : 'Headache',
                        IT    : 'M126IT',
                        value : '0'
                    }
                },
                {
                    branchFrom             : 'DailyDiary_91141',
                    branchTo               : 'DailyDiary_91119',
                    clearBranchedResponses : false,
                    branchFunction         : 'checkLoops',
                    branchParams           : {
                        IG          : 'Headache',
                        maxLoops    : 4,
                        startOfLoop : 'DailyDiary_91119',
                        endOfLoop   : 'DailyDiary_91142'
                    }
                },
                {
                    branchFrom     : 'DailyDiary_91143',
                    branchTo       : 'AFFIDAVIT',
                    branchFunction : 'equal',
                    branchParams   : {
                        IG    : 'DailyDiary',
                        IT    : 'HEDNOW1L',
                        value : '0'
                    }
                },
                {
                    branchFrom             : 'DailyDiary_91143',
                    branchTo               : 'DailyDiary_91119',
                    clearBranchedResponses : false,
                    branchFunction         : 'checkLoops',
                    branchParams           : {
                        IG          : 'Headache',
                        maxLoops    : 6,
                        startOfLoop : 'DailyDiary_91119',
                        endOfLoop   : 'AFFIDAVIT'
                    }
                }
            ],
            initialScreen  : []
        }
    ],
    screens        : [
        {
            id          : 'DailyDiary_9111',
            className   : 'DailyDiary_9111',
            orientation : 'landscape',
            questions   : [
                {
                    id        : 'DailyDiary_9111_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_9112',
            className : 'DailyDiary_9112',
            questions : [
                {
                    id        : 'DailyDiary_9112_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_9113',
            className : 'DailyDiary_9113',
            questions : [
                {
                    id        : 'DailyDiary_9113_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_9114',
            className : 'DailyDiary_9114',
            questions : [
                {
                    id        : 'DailyDiary_9114_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_9115',
            className : 'DailyDiary_9115',
            questions : [
                {
                    id        : 'DailyDiary_9115_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_9116',
            className : 'DailyDiary_9116',
            questions : [
                {
                    id        : 'DailyDiary_9116_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_9117',
            className : 'DailyDiary_9117',
            questions : [
                {
                    id        : 'DailyDiary_9117_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_9118',
            className : 'DailyDiary_9118',
            questions : [
                {
                    id        : 'DailyDiary_9118_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_9119_0',
            className : 'DailyDiary_9119',
            questions : [
                {
                    id        : 'DailyDiary_9119_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_9119_1',
            className : 'DailyDiary_9119',
            questions : [
                {
                    id        : 'DailyDiary_9119_Q1',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_91110',
            className : 'DailyDiary_91110',
            questions : [
                {
                    id        : 'DailyDiary_91110_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_91111',
            className : 'DailyDiary_91111',
            questions : [
                {
                    id        : 'DailyDiary_91111_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_91112',
            className : 'DailyDiary_91112',
            questions : [
                {
                    id        : 'DailyDiary_91112_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_91113',
            className : 'DailyDiary_91113',
            questions : [
                {
                    id        : 'DailyDiary_91113_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_91114',
            className : 'DailyDiary_91114',
            questions : [
                {
                    id        : 'DailyDiary_91114_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_91115',
            className : 'DailyDiary_91115',
            questions : [
                {
                    id        : 'DailyDiary_91115_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_91116',
            className : 'DailyDiary_91116',
            questions : [
                {
                    id        : 'DailyDiary_91116_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_91117',
            className : 'DailyDiary_91117',
            questions : [
                {
                    id        : 'DailyDiary_91117_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_91118',
            className : 'DailyDiary_91118',
            questions : [
                {
                    id        : 'DailyDiary_91118_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_91119',
            className : 'DailyDiary_91119',
            questions : [
                {
                    id        : 'DailyDiary_91119_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_91120',
            className : 'DailyDiary_91120',
            questions : [
                {
                    id        : 'DailyDiary_91120_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_91121',
            className : 'DailyDiary_91121',
            questions : [
                {
                    id        : 'DailyDiary_91121_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_91122',
            className : 'DailyDiary_91122',
            questions : [
                {
                    id        : 'DailyDiary_91122_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_91123',
            className : 'DailyDiary_91123',
            questions : [
                {
                    id        : 'DailyDiary_91123_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_91124',
            className : 'DailyDiary_91124',
            questions : [
                {
                    id        : 'DailyDiary_91124_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_91125',
            className : 'DailyDiary_91125',
            questions : [
                {
                    id        : 'DailyDiary_91125_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_91126',
            className : 'DailyDiary_91126',
            questions : [
                {
                    id        : 'DailyDiary_91126_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_91127',
            className : 'DailyDiary_91127',
            questions : [
                {
                    id        : 'DailyDiary_91127_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_91128',
            className : 'DailyDiary_91128',
            questions : [
                {
                    id        : 'DailyDiary_91128_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_91129',
            className : 'DailyDiary_91129',
            questions : [
                {
                    id        : 'DailyDiary_91129_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_91130',
            className : 'DailyDiary_91130',
            questions : [
                {
                    id        : 'DailyDiary_91130_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_91131',
            className : 'DailyDiary_91131',
            questions : [
                {
                    id        : 'DailyDiary_91131_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_91132',
            className : 'DailyDiary_91132',
            questions : [
                {
                    id        : 'DailyDiary_91132_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_91133',
            className : 'DailyDiary_91133',
            questions : [
                {
                    id        : 'DailyDiary_91133_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_91134',
            className : 'DailyDiary_91134',
            questions : [
                {
                    id        : 'DailyDiary_91134_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_91135',
            className : 'DailyDiary_91135',
            questions : [
                {
                    id        : 'DailyDiary_91135_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_91136',
            className : 'DailyDiary_91136',
            questions : [
                {
                    id        : 'DailyDiary_91136_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_91137',
            className : 'DailyDiary_91137',
            questions : [
                {
                    id        : 'DailyDiary_91137_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_91138',
            className : 'DailyDiary_91138',
            questions : [
                {
                    id        : 'DailyDiary_91138_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_91139',
            className : 'DailyDiary_91139',
            questions : [
                {
                    id        : 'DailyDiary_91139_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_91140',
            className : 'DailyDiary_91140',
            questions : [
                {
                    id        : 'DailyDiary_91140_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DD401',
            className : 'DD401',
            questions : [
                {
                    id        : 'DD401_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DD402',
            className : 'DD402',
            questions : [
                {
                    id        : 'DD402_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DD403',
            className : 'DD403',
            questions : [
                {
                    id        : 'DD403_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DD404_0',
            className : 'DD404_0',
            questions : [
                {
                    id        : 'DD404_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DD404_1',
            className : 'DD404_1',
            questions : [
                {
                    id        : 'DD404_Q1',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DD405',
            className : 'DD405',
            questions : [
                {
                    id        : 'DD405_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DD406',
            className : 'DD406',
            questions : [
                {
                    id        : 'DD406_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DD407',
            className : 'DD407',
            questions : [
                {
                    id        : 'DD407_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_91141',
            className : 'DailyDiary_91141',
            questions : [
                {
                    id        : 'DailyDiary_91141_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_91142',
            className : 'DailyDiary_91142',
            questions : [
                {
                    id        : 'DailyDiary_91142_Q0',
                    mandatory : true
                }
            ]
        }, {
            id        : 'DailyDiary_91143',
            className : 'DailyDiary_91143',
            questions : [
                {
                    id        : 'DailyDiary_91143_Q0',
                    mandatory : true
                }
            ]
        }
    ],
    questions      : [
        {
            id        : 'DailyDiary_9111_Q0',
            IG        : 'DailyDiary',
            IT        : 'MIGINT1L',
            text      : 'DailyDiary_9111_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_9111_Q0',
            widget    : {
                id           : 'DailyDiary_9111_Q0_W0',
                type         : 'AmgenNRS',
                width        : '100',
                displayText  : 'DailyDiary_9111_Q0_W0_DisplayText',
                reverseOnRtl : true,
                text         : 'DailyDiary_9111_Q0_W0_Text',
                answers      : [
                    {
                        value : '0',
                        text  : 'DailyDiary_9111_Q0_W0_A'
                    },
                    {
                        value : '10',
                        text  : 'DailyDiary_9111_Q0_W0_A0'
                    },
                    {
                        value : '20',
                        text  : 'DailyDiary_9111_Q0_W0_A1'
                    },
                    {
                        value : '30',
                        text  : 'DailyDiary_9111_Q0_W0_A2'
                    },
                    {
                        value : '40',
                        text  : 'DailyDiary_9111_Q0_W0_A3'
                    },
                    {
                        value : '50',
                        text  : 'DailyDiary_9111_Q0_W0_A4'
                    },
                    {
                        value : '60',
                        text  : 'DailyDiary_9111_Q0_W0_A5'
                    },
                    {
                        value : '70',
                        text  : 'DailyDiary_9111_Q0_W0_A6'
                    },
                    {
                        value : '80',
                        text  : 'DailyDiary_9111_Q0_W0_A7'
                    },
                    {
                        value : '90',
                        text  : 'DailyDiary_9111_Q0_W0_A8'
                    },
                    {
                        value : '100',
                        text  : 'DailyDiary_9111_Q0_W0_A9'
                    }
                ],
                templates    : {
                    markers : 'AMGEN:NRSMarkers'
                },
                markers      : {
                    text          : {
                        left            : 'DailyDiary_9111_Q0_W0_LeftMarker',
                        leftMiddle      : 'DailyDiary_9111_Q0_W0_LeftMiddleMarker',
                        middle          : 'DailyDiary_9111_Q0_W0_MiddleMarker',
                        rightMiddle     : 'DailyDiary_9111_Q0_W0_RightMiddleMarker',
                        right           : 'DailyDiary_9111_Q0_W0_RightMarker',
                        nrsMarkersImage : 'nrsMarkersImage'
                    },
                    justification : 'inside',
                    position      : 'above',
                    arrow         : true,
                    spaceAllowed  : 2
                }
            }
        }, {
            id        : 'DailyDiary_9112_Q0',
            IG        : 'DailyDiary',
            IT        : 'DAYBED1L',
            text      : 'DailyDiary_9112_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_9112_Q0',
            widget    : {
                id      : 'DailyDiary_9112_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'DailyDiary_9112_Q0_W0_A0'
                    },
                    {
                        value : '0',
                        text  : 'DailyDiary_9112_Q0_W0_A1'
                    }
                ]
            }
        }, {
            id        : 'DailyDiary_9113_Q0',
            IG        : 'DailyDiary',
            IT        : 'MISWRK1L',
            text      : 'DailyDiary_9113_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_9113_Q0',
            widget    : {
                id      : 'DailyDiary_9113_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'DailyDiary_9113_Q0_W0_A0'
                    },
                    {
                        value : '0',
                        text  : 'DailyDiary_9113_Q0_W0_A1'
                    },
                    {
                        value : '2',
                        text  : 'DailyDiary_9113_Q0_W0_A2'
                    }
                ]
            }
        }, {
            id        : 'DailyDiary_9114_Q0',
            text      : 'DailyDiary_9114_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_9114_Q0'
        }, {
            id        : 'DailyDiary_9115_Q0',
            IG        : 'DailyDiary',
            IT        : 'MEDAUR1L',
            text      : 'DailyDiary_9115_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_9115_Q0',
            widget    : {
                id      : 'DailyDiary_9115_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'DailyDiary_9115_Q0_W0_A0'
                    },
                    {
                        value : '0',
                        text  : 'DailyDiary_9115_Q0_W0_A1'
                    }
                ]
            }
        }, {
            id        : 'DailyDiary_9116_Q0',
            repeating : true,
            IG        : 'AURA',
            IT        : 'TIMTAK1S',
            text      : 'DailyDiary_9116_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_9116_Q0',
            widget    : {
                id            : 'DailyDiary_9116_Q0_W0',
                className     : 'DailyDiary_9116_Q0_W0',
                type          : 'DateTimeSpinner',
                // type          : 'DateTimePicker',
                modalTitle    : 'DailyDiary_9116_Q0_W0_modalTitle',
                okButtonText  : 'DailyDiary_9116_Q0_W0_okButtonText',
                label         : 'DailyDiary_9116_Q0_W0_label',
                minDate       : () => {
                    if(LF.Utilities.dateDiffInDays(new Date(), new Date(LF.Data.Questionnaire.subject.get('activationDate'))) === 0){
                        return LF.Utilities.timeStamp(new Date(LF.Data.Questionnaire.subject.get('activationDate')));
                    }else{
                        return LF.Utilities.getCollection('LastDiaries')
                            .then((lastDiaries) => {
                                let now        = new Date(),
                                    midnight   = new Date(new Date().setHours(0, 0, 0, 0)),
                                    dailyDiary = _.find(lastDiaries, (diary) => {
                                        return diary.get('questionnaire_id') === 'DailyDiary'
                                            && LF.Utilities.dateDiffInDays(now, new Date(diary.get('lastStartedDate'))) <= 1;
                                    });
                                return dailyDiary ? LF.Utilities.timeStamp(new Date(dailyDiary.get('lastStartedDate'))) : LF.Utilities.timeStamp(midnight);
                            });
                    }
                },
                maxDate       : () => {
                    return LF.Utilities.timeStamp(new Date());
                },
                dateFormat    : 'DD-MMM-YYYY',
                timeFormat    : 'HH:mm',
                displayFormat : 'DD-MMM-YYYY HH:mm'
            }
        }, {
            id        : 'DailyDiary_9117_Q0',
            IG        : 'AURA',
            IT        : 'MEDNAM1L',
            text      : 'DailyDiary_9117_Q0_TEXT',
            title     : '',
            className : 'ScrollingListBox',
            widget    : {
                id        : 'DailyDiary_9117_Q0_W0',
                type      : 'CustomRadioButton',
                className : 'CustomRadioButton',
                answers   : [
                    {
                        text  : 'DailyDiary_9117_Q0_W0_A0',
                        value : '1'
                    }, {
                        text  : 'DailyDiary_9117_Q0_W0_A1',
                        value : '2'
                    }, {
                        text  : 'DailyDiary_9117_Q0_W0_A2',
                        value : '3'
                    }, {
                        text  : 'DailyDiary_9117_Q0_W0_A3',
                        value : '4'
                    }, {
                        text  : 'DailyDiary_9117_Q0_W0_A4',
                        value : '5'
                    }, {
                        text  : 'DailyDiary_9117_Q0_W0_A5',
                        value : '6'
                    }, {
                        text  : 'DailyDiary_9117_Q0_W0_A6',
                        value : '7'
                    }, {
                        text  : 'DailyDiary_9117_Q0_W0_A7',
                        value : '8'
                    }, {
                        text  : 'DailyDiary_9117_Q0_W0_A8',
                        value : '9'
                    }, {
                        text  : 'DailyDiary_9117_Q0_W0_A9',
                        value : '10'
                    }
                ]
            }
        }, {
            id        : 'DailyDiary_9118_Q0',
            IG        : 'AURA',
            IT        : 'M27IT',
            text      : 'DailyDiary_9118_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_9118_Q0',
            widget    : {
                id      : 'DailyDiary_9118_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'DailyDiary_9118_Q0_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'DailyDiary_9118_Q0_W0_A1'
                    },
                    {
                        value : '3',
                        text  : 'DailyDiary_9118_Q0_W0_A2'
                    }
                ]
            }
        }, {
            id        : 'DailyDiary_9119_Q0',
            IG        : 'AURA',
            IT        : 'MEDAMT1L',
            text      : 'DailyDiary_9119_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_9119_Q0',
            widget    : {
                id                  : 'DailyDiary_9119_Q0_W0',
                className           : 'DailyDiary_9119_Q0_W0',
                label               : 'DailyDiary_9119_Q0_W0_CLICK_TO_MAKE_SELECTION',
                type                : 'NumberSpinner',
                modalTitle          : 'DailyDiary_9119_Q0_W0_modalTitle',
                labels              : {
                    labelOne : 'DailyDiary_9119_Q0_W0_labels_label1'
                },
                okButtonText        : 'DailyDiary_9119_Q0_W0_okButtonText',
                spinnerInputOptions : [
                    {
                        min       : 1,
                        max       : 9,
                        step      : 1,
                        precision : 0
                    }
                ]
            }
        },
        {
            id        : 'DailyDiary_9119_Q1',
            IG        : 'AURA',
            IT        : 'MEDAMT1L',
            text      : 'DailyDiary_9119_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_9119_Q1',
            widget    : {
                id                  : 'DailyDiary_9119_Q1_W0',
                className           : 'DailyDiary_9119_Q1_W0',
                label               : 'DailyDiary_9119_Q0_W0_CLICK_TO_MAKE_SELECTION',
                type                : 'NumberSpinner',
                modalTitle          : 'DailyDiary_9119_Q0_W0_modalTitle',
                labels              : {
                    labelOne : 'DailyDiary_9119_Q0_W0_labels_label1'
                },
                okButtonText        : 'DailyDiary_9119_Q0_W0_okButtonText',
                spinnerInputOptions : [
                    {
                        min       : 1,
                        max       : 99,
                        step      : 1,
                        precision : 0
                    }
                ]
            }
        },
        {
            id        : 'DailyDiary_91110_Q0',
            text      : 'DailyDiary_91110_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_91110_Q0'
        }, {
            id        : 'DailyDiary_91111_Q0',
            IG        : 'AURA',
            IT        : 'M36IT',
            text      : 'DailyDiary_91111_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_91111_Q0',
            widget    : {
                id      : 'DailyDiary_91111_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'DailyDiary_91111_Q0_W0_A0'
                    },
                    {
                        value : '0',
                        text  : 'DailyDiary_91111_Q0_W0_A1'
                    }
                ]
            }
        }, {
            id        : 'DailyDiary_91112_Q0',
            IG        : 'AURA',
            IT        : 'M39IT',
            text      : 'DailyDiary_91112_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_91112_Q0',
            widget    : {
                id      : 'DailyDiary_91112_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'DailyDiary_91112_Q0_W0_A0'
                    },
                    {
                        value : '0',
                        text  : 'DailyDiary_91112_Q0_W0_A1'
                    }
                ]
            }
        }, {
            id        : 'DailyDiary_91113_Q0',
            text      : 'DailyDiary_91113_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_91113_Q0'
        }, {
            id        : 'DailyDiary_91114_Q0',
            text      : 'DailyDiary_91114_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_91114_Q0'
        }, {
            id        : 'DailyDiary_91115_Q0',
            text      : 'DailyDiary_91115_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_91115_Q0'
        }, {
            id        : 'DailyDiary_91116_Q0',
            repeating : true,
            IG        : 'Headache',
            IT        : 'ENDHED1L',
            text      : 'DailyDiary_91116_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_91116_Q0',
            widget    : {
                id      : 'DailyDiary_91116_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'DailyDiary_91116_Q0_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'DailyDiary_91116_Q0_W0_A1'
                    }
                ]
            }
        }, {
            id        : 'DailyDiary_91117_Q0',
            IG        : 'Headache',
            IT        : 'M54IT',
            text      : 'DailyDiary_91117_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_91117_Q0',
            widget    : {
                id      : 'DailyDiary_91117_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'DailyDiary_91117_Q0_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'DailyDiary_91117_Q0_W0_A1'
                    }
                ]
            }
        }, {
            id        : 'DailyDiary_91118_Q0',
            IG        : 'Headache',
            IT        : 'M57IT',
            text      : 'DailyDiary_91118_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_91118_Q0',
            widget    : {
                id      : 'DailyDiary_91118_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'DailyDiary_91118_Q0_W0_A0'
                    },
                    {
                        value : '0',
                        text  : 'DailyDiary_91118_Q0_W0_A1'
                    }
                ]
            }
        }, {
            id        : 'DailyDiary_91119_Q0',
            IG        : 'Headache',
            IT        : 'HEDBEG1S',
            text      : 'DailyDiary_91119_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_91119_Q0',
            widget    : {
                id            : 'DailyDiary_91119_Q0_W0',
                className     : 'DailyDiary_91119_Q0_W0',
                type          : 'DateTimeSpinner',
                modalTitle    : 'DailyDiary_91119_Q0_W0_modalTitle',
                okButtonText  : 'DailyDiary_91119_Q0_W0_okButtonText',
                label         : 'DailyDiary_91119_Q0_W0_label',
                minDate       : () => {
                    if(LF.Utilities.dateDiffInDays(new Date(), new Date(LF.Data.Questionnaire.subject.get('activationDate'))) === 0){
                        return LF.Utilities.timeStamp(new Date(LF.Data.Questionnaire.subject.get('activationDate')));
                    }else{
                        return LF.Utilities.getCollection('LastDiaries')
                            .then((lastDiaries) => {
                                let now        = new Date(),
                                    midnight   = new Date(new Date().setHours(0, 0, 0, 0)),
                                    dailyDiary = _.find(lastDiaries, (diary) => {
                                        return diary.get('questionnaire_id') === 'DailyDiary'
                                            && LF.Utilities.dateDiffInDays(now, new Date(diary.get('lastStartedDate'))) <= 1;
                                    });
                                return dailyDiary ? LF.Utilities.timeStamp(new Date(dailyDiary.get('lastStartedDate'))) : LF.Utilities.timeStamp(new Date(midnight));
                            });
                    }
                },
                maxDate       : () => {
                    return LF.Utilities.timeStamp(new Date());
                },
                dateFormat    : 'DD-MMM-YYYY',
                timeFormat    : 'HH:mm',
                displayFormat : 'DD-MMM-YYYY HH:mm'
            }
        }, {
            id        : 'DailyDiary_91120_Q0',
            IG        : 'Headache',
            IT        : 'M63IT',
            text      : 'DailyDiary_91120_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_91120_Q0',
            widget    : {
                id      : 'DailyDiary_91120_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'DailyDiary_91120_Q0_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'DailyDiary_91120_Q0_W0_A1'
                    }
                ]
            }
        }, {
            id        : 'DailyDiary_91121_Q0',
            IG        : 'Headache',
            IT        : 'HEDSLP1L',
            text      : 'DailyDiary_91121_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_91121_Q0',
            widget    : {
                id      : 'DailyDiary_91121_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'DailyDiary_91121_Q0_W0_A0'
                    },
                    {
                        value : '0',
                        text  : 'DailyDiary_91121_Q0_W0_A1'
                    }
                ]
            }
        }, {
            id        : 'DailyDiary_91122_Q0',
            IG        : 'Headache',
            IT        : 'ENDHED1S',
            text      : 'DailyDiary_91122_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_91122_Q0',
            widget    : {
                id            : 'DailyDiary_91122_Q0_W0',
                className     : 'DailyDiary_91122_Q0_W0',
                type          : 'DateTimeSpinner',
                modalTitle    : 'DailyDiary_91122_Q0_W0_modalTitle',
                okButtonText  : 'DailyDiary_91122_Q0_W0_okButtonText',
                label         : 'DailyDiary_91122_Q0_W0_label',
                dateFormat    : 'DD-MMM-YYYY',
                timeFormat    : 'HH:mm',
                displayFormat : 'DD-MMM-YYYY HH:mm',
                minDate       : () => {
                    return LF.Utilities.getOngoingHeadaches()
                        .then(headaches => {
                            let ig              = 'Headache',
                                igr             = LF.Data.Questionnaire.getCurrentIGR(ig),
                                headacheBegin   = _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('HEDBEG1S', igr)),
                                ongoingHeadache = _.last(headaches),
                                ongoingHeadacheStart,
                                lastDiary;
                            if(headacheBegin){
                                return LF.Utilities.timeStamp(new Date(headacheBegin.get('response')));
                            }else if(ongoingHeadache){
                                return LF.Utilities.getCollection('LastDiaries')
                                    .then(lastDiaries => {
                                        lastDiary = _.find(lastDiaries, (diary) => {
                                            return (diary.get('questionnaire_id') === 'DailyDiary');
                                        });
                                        ongoingHeadacheStart = new Date(ongoingHeadache.get('startDate'));
                                        return lastDiary ? LF.Utilities.timeStamp(new Date(Math.max(new Date(lastDiary.get('lastStartedDate')).getTime(), ongoingHeadacheStart.getTime()))) : LF.Utilities.timeStamp(new Date());
                                    });
                            }else{
                                return LF.Utilities.timeStamp(new Date());
                            }
                        });
                },
                maxDate       : () => {
                    return LF.Utilities.timeStamp(new Date());
                },
                initialDate   : () => {
                    return LF.Utilities.getOngoingHeadaches()
                        .then(headaches => {
                            let ig              = 'Headache',
                                igr             = LF.Data.Questionnaire.getCurrentIGR(ig),
                                headacheBegin   = _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('HEDBEG1S', igr)),
                                ongoingHeadache = _.last(headaches),
                                lastDiary;
                            if(headacheBegin){
                                return LF.Utilities.timeStamp(new Date(headacheBegin.get('response')));
                            }else if(ongoingHeadache){
                                return LF.Utilities.getCollection('LastDiaries')
                                    .then(lastDiaries => {
                                        lastDiary = _.find(lastDiaries, (diary) => {
                                            return (diary.get('questionnaire_id') === 'DailyDiary');
                                        });
                                        return lastDiary ? LF.Utilities.timeStamp(new Date(lastDiary.get('lastStartedDate'))) : LF.Utilities.timeStamp(new Date());
                                    });
                            }else{
                                return LF.Utilities.timeStamp(new Date());
                            }
                        });
                }
            }
        }, {
            id        : 'DailyDiary_91123_Q0',
            IG        : 'Headache',
            IT        : 'M72IT',
            text      : 'DailyDiary_91123_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_91123_Q0',
            widget    : {
                id      : 'DailyDiary_91123_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'DailyDiary_91123_Q0_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'DailyDiary_91123_Q0_W0_A1'
                    }
                ]
            }
        }, {
            id        : 'DailyDiary_91124_Q0',
            IG        : 'Headache',
            IT        : 'M75IT',
            text      : 'DailyDiary_91124_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_91124_Q0',
            widget    : {
                id      : 'DailyDiary_91124_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'DailyDiary_91124_Q0_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'DailyDiary_91124_Q0_W0_A1'
                    }
                ]
            }
        }, {
            id        : 'DailyDiary_91125_Q0',
            IG        : 'Headache',
            IT        : 'M78IT',
            text      : 'DailyDiary_91125_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_91125_Q0',
            widget    : {
                id      : 'DailyDiary_91125_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'DailyDiary_91125_Q0_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'DailyDiary_91125_Q0_W0_A1'
                    },
                    {
                        value : '3',
                        text  : 'DailyDiary_91125_Q0_W0_A2'
                    }
                ]
            }
        }, {
            id        : 'DailyDiary_91126_Q0',
            IG        : 'Headache',
            IT        : 'M81IT',
            text      : 'DailyDiary_91126_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_91126_Q0',
            widget    : {
                id      : 'DailyDiary_91126_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'DailyDiary_91126_Q0_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'DailyDiary_91126_Q0_W0_A1'
                    }
                ]
            }
        }, {
            id        : 'DailyDiary_91127_Q0',
            IG        : 'Headache',
            IT        : 'FELSLP1S',
            text      : 'DailyDiary_91127_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_91127_Q0',
            widget    : {
                id            : 'DailyDiary_91127_Q0_W0',
                className     : 'DailyDiary_91127_Q0_W0',
                type          : 'DateTimeSpinner',
                modalTitle    : 'DailyDiary_91127_Q0_W0_modalTitle',
                okButtonText  : 'DailyDiary_91127_Q0_W0_okButtonText',
                label         : 'DailyDiary_91127_Q0_W0_label',
                dateFormat    : 'DD-MMM-YYYY',
                timeFormat    : 'HH:mm',
                displayFormat : 'DD-MMM-YYYY HH:mm',
                minDate       : () => {
                    return LF.Utilities.getOngoingHeadaches()
                        .then(headaches => {
                            let ig              = 'Headache',
                                igr             = LF.Data.Questionnaire.getCurrentIGR(ig),
                                headacheBegin   = _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('HEDBEG1S', igr)),
                                ongoingHeadache = _.last(headaches);
                            if(headacheBegin){
                                return LF.Utilities.timeStamp(new Date(headacheBegin.get('response')));
                            }else{
                                return ongoingHeadache ? LF.Utilities.timeStamp(new Date(ongoingHeadache.get('startDate'))) : LF.Utilities.timeStamp(new Date());
                            }
                        });
                },
                maxDate       : () => {
                    return LF.Utilities.timeStamp(new Date());
                },
                initialDate   : () => {
                    return LF.Utilities.timeStamp(new Date());
                }
            }
        }, {
            id        : 'DailyDiary_91128_Q0',
            IG        : 'Headache',
            IT        : 'TIMAWK1S',
            text      : 'DailyDiary_91128_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_91128_Q0',
            widget    : {
                id            : 'DailyDiary_91128_Q0_W0',
                className     : 'DailyDiary_91128_Q0_W0',
                type          : 'DateTimeSpinner',
                modalTitle    : 'DailyDiary_91128_Q0_W0_modalTitle',
                okButtonText  : 'DailyDiary_91128_Q0_W0_okButtonText',
                label         : 'DailyDiary_91128_Q0_W0_label',
                dateFormat    : 'DD-MMM-YYYY',
                timeFormat    : 'HH:mm',
                displayFormat : 'DD-MMM-YYYY HH:mm',
                minDate       : () => {
                    let ig         = 'Headache',
                        igr        = LF.Data.Questionnaire.getCurrentIGR(ig),
                        feelAsleep = _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('FELSLP1S', igr));
                    return feelAsleep ? LF.Utilities.timeStamp(new Date(feelAsleep.get('response'))) : LF.Utilities.timeStamp(new Date());
                },
                maxDate       : () => {
                    return LF.Utilities.timeStamp(new Date());
                },
                initialDate   : () => {
                    return LF.Utilities.timeStamp(new Date());
                }
            }
        }, {
            id        : 'DailyDiary_91129_Q0',
            IG        : 'Headache',
            IT        : 'M90IT',
            text      : 'DailyDiary_91129_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_91129_Q0',
            widget    : {
                id      : 'DailyDiary_91129_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'DailyDiary_91129_Q0_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'DailyDiary_91129_Q0_W0_A1'
                    }
                ]
            }
        }, {
            id        : 'DailyDiary_91130_Q0',
            IG        : 'Headache',
            IT        : 'HEDTYP1L',
            text      : 'DailyDiary_91130_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_91130_Q0',
            widget    : {
                id      : 'DailyDiary_91130_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'DailyDiary_91130_Q0_W0_A0'
                    },
                    {
                        value : '0',
                        text  : 'DailyDiary_91130_Q0_W0_A1'
                    }
                ]
            }
        }, {
            id        : 'DailyDiary_91131_Q0',
            IG        : 'Headache',
            IT        : 'PANINT1L',
            text      : 'DailyDiary_91131_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_91131_Q0',
            widget    : {
                id      : 'DailyDiary_91131_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'DailyDiary_91131_Q0_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'DailyDiary_91131_Q0_W0_A1'
                    },
                    {
                        value : '3',
                        text  : 'DailyDiary_91131_Q0_W0_A2'
                    }
                ]
            }
        }, {
            id        : 'DailyDiary_91132_Q0',
            IG        : 'Headache',
            IT        : 'HEDSYM1B',
            text      : 'DailyDiary_91132_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_91132_Q0',
            widget    : {
                id      : 'DailyDiary_91132_Q0_W0',
                type    : 'CheckBox',
                answers : [
                    {
                        IT    : 'HEDSYM1B',
                        text  : 'DailyDiary_91132_Q0_W0_A0',
                        value : '1'
                    },
                    {
                        IT    : 'HEDSYM2B',
                        text  : 'DailyDiary_91132_Q0_W0_A1',
                        value : '2'
                    },
                    {
                        IT    : 'HEDSYM3B',
                        text  : 'DailyDiary_91132_Q0_W0_A2',
                        value : '3'
                    },
                    {
                        IT      : 'HEDSYM4B',
                        text    : 'DailyDiary_91132_Q0_W0_A3',
                        value   : '4',
                        exclude : ['All']
                    }
                ]
            }
        }, {
            id        : 'DailyDiary_91133_Q0',
            IG        : 'Headache',
            IT        : 'HEDSYM5B',
            text      : 'DailyDiary_91133_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_91133_Q0',
            widget    : {
                id      : 'DailyDiary_91133_Q0_W0',
                type    : 'CheckBox',
                answers : [
                    {
                        IT    : 'HEDSYM5B',
                        text  : 'DailyDiary_91133_Q0_W0_A0',
                        value : '5'
                    },
                    {
                        IT    : 'HEDSYM6B',
                        text  : 'DailyDiary_91133_Q0_W0_A1',
                        value : '6'
                    },
                    {
                        IT    : 'HEDSYM7B',
                        text  : 'DailyDiary_91133_Q0_W0_A2',
                        value : '7'
                    },
                    {
                        IT    : 'HEDSYM8B',
                        text  : 'DailyDiary_91133_Q0_W0_A3',
                        value : '8'
                    },
                    {
                        IT      : 'HEDSYM9B',
                        text    : 'DailyDiary_91133_Q0_W0_A4',
                        value   : '9',
                        exclude : ['All']
                    }
                ]
            }
        }, {
            id        : 'DailyDiary_91134_Q0',
            IG        : 'Headache',
            IT        : 'SEVNAU1L',
            text      : 'DailyDiary_91134_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_91134_Q0',
            widget    : {
                id      : 'DailyDiary_91134_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'DailyDiary_91134_Q0_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'DailyDiary_91134_Q0_W0_A1'
                    },
                    {
                        value : '3',
                        text  : 'DailyDiary_91134_Q0_W0_A2'
                    }
                ]
            }
        }, {
            id        : 'DailyDiary_91135_Q0',
            IG        : 'Headache',
            IT        : 'SEVVOM1L',
            text      : 'DailyDiary_91135_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_91135_Q0',
            widget    : {
                id      : 'DailyDiary_91135_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'DailyDiary_91135_Q0_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'DailyDiary_91135_Q0_W0_A1'
                    },
                    {
                        value : '3',
                        text  : 'DailyDiary_91135_Q0_W0_A2'
                    }
                ]
            }
        }, {
            id        : 'DailyDiary_91136_Q0',
            IG        : 'Headache',
            IT        : 'SEVLGT1L',
            text      : 'DailyDiary_91136_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_91136_Q0',
            widget    : {
                id      : 'DailyDiary_91136_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'DailyDiary_91136_Q0_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'DailyDiary_91136_Q0_W0_A1'
                    },
                    {
                        value : '3',
                        text  : 'DailyDiary_91136_Q0_W0_A2'
                    }
                ]
            }
        }, {
            id        : 'DailyDiary_91137_Q0',
            IG        : 'Headache',
            IT        : 'SEVSND1L',
            text      : 'DailyDiary_91137_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_91137_Q0',
            widget    : {
                id      : 'DailyDiary_91137_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'DailyDiary_91137_Q0_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'DailyDiary_91137_Q0_W0_A1'
                    },
                    {
                        value : '3',
                        text  : 'DailyDiary_91137_Q0_W0_A2'
                    }
                ]
            }
        }, {
            id        : 'DailyDiary_91138_Q0',
            IG        : 'Headache',
            IT        : 'EXPAUR1L',
            text      : 'DailyDiary_91138_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_91138_Q0',
            widget    : {
                id      : 'DailyDiary_91138_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'DailyDiary_91138_Q0_W0_A0'
                    },
                    {
                        value : '0',
                        text  : 'DailyDiary_91138_Q0_W0_A1'
                    }
                ]
            }
        }, {
            id        : 'DailyDiary_91139_Q0',
            IG        : 'Headache',
            IT        : 'SEVAUR1L',
            text      : 'DailyDiary_91139_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_91139_Q0',
            widget    : {
                id      : 'DailyDiary_91139_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'DailyDiary_91139_Q0_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'DailyDiary_91139_Q0_W0_A1'
                    },
                    {
                        value : '3',
                        text  : 'DailyDiary_91139_Q0_W0_A2'
                    }
                ]
            }
        }, {
            id        : 'DailyDiary_91140_Q0',
            IG        : 'Headache',
            IT        : 'MEDHED1L',
            text      : 'DailyDiary_91140_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_91140_Q0',
            widget    : {
                id      : 'DailyDiary_91140_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'DailyDiary_91140_Q0_W0_A0'
                    },
                    {
                        value : '0',
                        text  : 'DailyDiary_91140_Q0_W0_A1'
                    }
                ]
            }
        }, {
            id        : 'DD401_Q0',
            repeating : true,
            IG        : 'HeadacheMed',
            IT        : 'TIMTAK1S',
            text      : 'DailyDiary_9116_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_9116_Q0',
            widget    : {
                id            : 'DailyDiary_9116_Q0_W0',
                className     : 'DailyDiary_9116_Q0_W0',
                type          : 'DateTimeSpinner',
                modalTitle    : 'DailyDiary_9116_Q0_W0_modalTitle',
                okButtonText  : 'DailyDiary_9116_Q0_W0_okButtonText',
                label         : 'DailyDiary_9116_Q0_W0_label',
                dateFormat    : 'DD-MMM-YYYY',
                timeFormat    : 'HH:mm',
                displayFormat : 'DD-MMM-YYYY HH:mm',
                minDate       : () => {
                    return LF.Utilities.getOngoingHeadaches()
                        .then(headaches => {
                            let ig              = 'Headache',
                                igr             = LF.Data.Questionnaire.getCurrentIGR(ig),
                                headacheBegin   = _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('HEDBEG1S', igr)),
                                ongoingHeadache = _.last(headaches);
                            if(headacheBegin){
                                return LF.Utilities.timeStamp(new Date(headacheBegin.get('response')));
                            }else{
                                return ongoingHeadache ? LF.Utilities.timeStamp(new Date(ongoingHeadache.get('startDate'))) : LF.Utilities.timeStamp(new Date());
                            }
                        });
                },
                maxDate       : () => {
                    let ig      = 'Headache',
                        igr     = LF.Data.Questionnaire.getCurrentIGR(ig),
                        endDate = _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('ENDHED1S', igr)) || _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('FELSLP1S', igr));
                    return endDate ? LF.Utilities.timeStamp(new Date(endDate.get('response'))) : LF.Utilities.timeStamp(new Date());
                },
                initialDate   : () => {
                    let ig      = 'Headache',
                        igr     = LF.Data.Questionnaire.getCurrentIGR(ig),
                        endDate = _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('ENDHED1S', igr)) || _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('FELSLP1S', igr));
                    return endDate ? LF.Utilities.timeStamp(new Date(endDate.get('response'))) : LF.Utilities.timeStamp(new Date());
                }
            }
        }, {
            id        : 'DD402_Q0',
            IG        : 'HeadacheMed',
            IT        : 'MEDNAM1L',
            text      : 'DailyDiary_9117_Q0_TEXT',
            title     : '',
            className : 'ScrollingListBox',
            widget    : {
                id        : 'DailyDiary_9117_Q0_W0',
                type      : 'CustomRadioButton',
                className : 'CustomRadioButton',
                answers   : [
                    {
                        text  : 'DailyDiary_9117_Q0_W0_A0',
                        value : '1'
                    }, {
                        text  : 'DailyDiary_9117_Q0_W0_A1',
                        value : '2'
                    }, {
                        text  : 'DailyDiary_9117_Q0_W0_A2',
                        value : '3'
                    }, {
                        text  : 'DailyDiary_9117_Q0_W0_A3',
                        value : '4'
                    }, {
                        text  : 'DailyDiary_9117_Q0_W0_A4',
                        value : '5'
                    }, {
                        text  : 'DailyDiary_9117_Q0_W0_A5',
                        value : '6'
                    }, {
                        text  : 'DailyDiary_9117_Q0_W0_A6',
                        value : '7'
                    }, {
                        text  : 'DailyDiary_9117_Q0_W0_A7',
                        value : '8'
                    }, {
                        text  : 'DailyDiary_9117_Q0_W0_A8',
                        value : '9'
                    }, {
                        text  : 'DailyDiary_9117_Q0_W0_A9',
                        value : '10'
                    }
                ]
            }
        }, {
            id        : 'DD403_Q0',
            IG        : 'HeadacheMed',
            IT        : 'M27IT',
            text      : 'DailyDiary_9118_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_9118_Q0',
            widget    : {
                id      : 'DailyDiary_9118_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'DailyDiary_9118_Q0_W0_A0'
                    },
                    {
                        value : '2',
                        text  : 'DailyDiary_9118_Q0_W0_A1'
                    },
                    {
                        value : '3',
                        text  : 'DailyDiary_9118_Q0_W0_A2'
                    }
                ]
            }
        }, {
            id        : 'DD404_Q0',
            IG        : 'HeadacheMed',
            IT        : 'MEDAMT1L',
            text      : 'DailyDiary_9119_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_9119_Q0',
            widget    : {
                id                  : 'DailyDiary_9119_Q0_W0',
                className           : 'DailyDiary_9119_Q0_W0',
                label               : 'DailyDiary_9119_Q0_W0_CLICK_TO_MAKE_SELECTION',
                type                : 'NumberSpinner',
                modalTitle          : 'DailyDiary_9119_Q0_W0_modalTitle',
                labels              : {
                    labelOne : 'DailyDiary_9119_Q0_W0_labels_label1'
                },
                okButtonText        : 'DailyDiary_9119_Q0_W0_okButtonText',
                spinnerInputOptions : [
                    {
                        min       : 1,
                        max       : 9,
                        step      : 1,
                        precision : 0
                    }
                ]
            }
        },
        {
            id        : 'DD404_Q1',
            IG        : 'HeadacheMed',
            IT        : 'MEDAMT1L',
            text      : 'DailyDiary_9119_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_9119_Q1',
            widget    : {
                id                  : 'DailyDiary_9119_Q1_W0',
                className           : 'DailyDiary_9119_Q1_W0',
                label               : 'DailyDiary_9119_Q0_W0_CLICK_TO_MAKE_SELECTION',
                type                : 'NumberSpinner',
                modalTitle          : 'DailyDiary_9119_Q0_W0_modalTitle',
                labels              : {
                    labelOne : 'DailyDiary_9119_Q0_W0_labels_label1'
                },
                okButtonText        : 'DailyDiary_9119_Q0_W0_okButtonText',
                spinnerInputOptions : [
                    {
                        min       : 1,
                        max       : 99,
                        step      : 1,
                        precision : 0
                    }
                ]
            }
        }, {
            id        : 'DD407_Q0',
            text      : 'DailyDiary_91110_Q0_TEXT',
            title     : '',
            className : 'DD407_Q0'
        }, {
            id        : 'DD405_Q0',
            IG        : 'HeadacheMed',
            IT        : 'M39IT',
            text      : 'DailyDiary_91112_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_91112_Q0',
            widget    : {
                id      : 'DailyDiary_91112_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'DailyDiary_91112_Q0_W0_A0'
                    },
                    {
                        value : '0',
                        text  : 'DailyDiary_91112_Q0_W0_A1'
                    }
                ]
            }
        }, {
            id        : 'DD406_Q0',
            text      : 'DailyDiary_91113_Q0_TEXT',
            title     : '',
            className : 'DD406_Q0'
        }, {
            id        : 'DailyDiary_91141_Q0',
            IG        : 'Headache',
            IT        : 'M126IT',
            text      : 'DailyDiary_91141_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_91141_Q0',
            widget    : {
                id      : 'DailyDiary_91141_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'DailyDiary_91141_Q0_W0_A0'
                    },
                    {
                        value : '0',
                        text  : 'DailyDiary_91141_Q0_W0_A1'
                    }
                ]
            }
        }, {
            id        : 'DailyDiary_91142_Q0',
            text      : 'DailyDiary_91142_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_91142_Q0'
        }, {
            id        : 'DailyDiary_91143_Q0',
            IG        : 'DailyDiary',
            IT        : 'HEDNOW1L',
            text      : 'DailyDiary_91143_Q0_TEXT',
            title     : '',
            className : 'DailyDiary_91143_Q0',
            widget    : {
                id      : 'DailyDiary_91143_Q0_W0',
                type    : 'RadioButton',
                answers : [
                    {
                        value : '1',
                        text  : 'DailyDiary_91143_Q0_W0_A0'
                    },
                    {
                        value : '0',
                        text  : 'DailyDiary_91143_Q0_W0_A1'
                    }
                ]
            }
        }
    ],
    rules          : [
        {
            id       : 'redoSubjectMeds',
            trigger  : [
                'QUESTIONNAIRE:Open/DailyDiary'
            ],
            evaluate : function (filter, resume) {
                resume(!localStorage.getItem('ScreenshotMode'));
            },
            resolve  : [
                {
                    action : 'displayMessage',
                    data   : 'PLEASE_WAIT'
                },
                {
                    action(input, done){
                        LF.MedModuleUtil.createSubjectList()
                            .then(() => {
                                done();
                            });
                    }
                },
                {
                    action(input, done){
                        LF.Data.Questionnaire.medStartIndex = [];
                        LF.Utilities.setDynamicText()
                            .then(() => {
                                done();
                            });
                    }
                },
                {
                    action : 'removeMessage'
                }
            ]
        },
        {
            id       : 'adjustListSelectionList',
            trigger  : [
                'QUESTIONNAIRE:Displayed/DailyDiary/DailyDiary_9117',
                'QUESTIONNAIRE:Displayed/DailyDiary/DD402'
            ],
            salience : 1,
            evaluate : function (filter, resume) {
                resume(!localStorage.getItem('ScreenshotMode'));
            },
            resolve  : [
                {
                    action : 'displayMessage',
                    data   : 'PLEASE_WAIT'
                },
                {
                    action (input, done){
                        //let screenId = LF.Data.Questionnaire.screens[LF.router.view().screen].get('id');
                        if(LF.Utilities.isTrainer()){
                            let medCount = 3;
                            for(let i = medCount; i < 9; i++){
                                $(`.btn.btn-default.btn-block.btn-DailyDiary_9117_Q0_W0_radio_${i}`).hide();
                            }
                            done();
                        }else{
                            LF.Utilities.getCollection('SubjectMeds').then(function (collection) {
                                let medCount = collection.length;
                                for(let i = medCount; i < 9; i++){
                                    $(`.btn.btn-default.btn-block.btn-DailyDiary_9117_Q0_W0_radio_${i}`).hide();
                                }
                                done();
                            });
                        }
                    }
                },
                {
                    action (input, done){
                        let maxHeight = 29;
                        _.each($('label'), function (element) {
                            maxHeight = Math.max($(element).height(), maxHeight);
                        });
                        _.each($('div.btn.btn-default.btn-block'), function (element) {
                            $(element).height(maxHeight);
                        });
                        done();
                    }
                },
                {
                    action : 'removeMessage'
                }
            ]
        },
        {
            id       : 'decrementLoop',
            trigger  : [
                'QUESTIONNAIRE:Navigate/DailyDiary/DailyDiary_9116',
                'QUESTIONNAIRE:Navigate/DailyDiary/DailyDiary_91119',
                'QUESTIONNAIRE:Navigate/DailyDiary/DD401'
            ],
            evaluate : function (filter, resume) {
                resume(!localStorage.getItem('ScreenshotMode'));
            },
            resolve  : [
                {
                    action (input, done){
                        let screenID  = input.screenId,
                            direction = input.direction,
                            ig, igr;
                        switch(screenID){
                            case 'DailyDiary_9116' :{
                                ig = 'AURA';
                                break;
                            }
                            case 'DailyDiary_91119' :{
                                ig = 'Headache';
                                break;
                            }
                            case 'DD401' :{
                                ig = 'HeadacheMed';
                                break;
                            }
                            default :{
                                ig = 'AURA';
                            }
                        }
                        igr = LF.Data.Questionnaire.getCurrentIGR(ig);
                        if(direction === 'previous' && igr > 1){
                            LF.Data.Questionnaire.decrementIGR(ig);
                        }
                        done();
                    }
                }
            ]
        },
        {
            id       : 'removeElementFromMedArray',
            trigger  : [
                'QUESTIONNAIRE:Navigate/DailyDiary/DailyDiary_91140'
            ],
            evaluate : function (filter, resume) {
                resume(!localStorage.getItem('ScreenshotMode'));
            },
            resolve  : [
                {
                    action (input, done){
                        let ig     = 'Headache',
                            igMed  = 'HeadacheMed',
                            igr    = LF.Data.Questionnaire.getCurrentIGR(ig),
                            igrMed = LF.Data.Questionnaire.getCurrentIGR(igMed),
                            answer,
                            index;
                        if(input.direction === 'previous'){
                            LF.Data.Questionnaire.medStartIndex.pop();
                        }else{
                            answer = _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('MEDHED1L', igr));
                            if(answer && answer.get('response') === '0'){
                                if(LF.Data.Questionnaire.queryAnswersByIGAndIGR(igMed, igrMed).length > 0){
                                    igrMed++;
                                }
                                index = LF.Data.Questionnaire.medStartIndex.indexOf(igrMed);
                                if(index > -1){
                                    LF.Data.Questionnaire.medStartIndex.splice(index, 1);
                                }
                            }
                        }
                        done();
                    }
                }
            ]
        },
        {
            id       : 'removeHeadacheNowQuestion',
            trigger  : [
                'QUESTIONNAIRE:Navigate/DailyDiary/DailyDiary_91143'
            ],
            evaluate : function (filter, resume) {
                resume(!localStorage.getItem('ScreenshotMode'));
            },
            resolve  : [
                {
                    action (input, done){
                        let ig           = 'Headache',
                            igr          = LF.Data.Questionnaire.getCurrentIGR(ig),
                            thisResponse = _.last(LF.Data.Questionnaire.queryAnswersByIT('HEDNOW1L'));
                        if(input.direction === 'previous'){
                            LF.Data.Questionnaire.data.answers.remove(thisResponse);
                        }else{
                            if(thisResponse && thisResponse.get('response') === '0'){
                                if(LF.Data.Questionnaire.queryAnswersByIGAndIGR(ig, igr + 1).length > 0){
                                    LF.Widget.ReviewScreen.deleteLoop('Headache', igr + 1);
                                }
                            }
                        }
                        done();
                    }
                }
            ]
        },
        {
            id       : 'saveHeadaches',
            trigger  : [
                'QUESTIONNAIRE:Completed/DailyDiary'
            ],
            evaluate : function (filter, resume) {
                resume(!localStorage.getItem('ScreenshotMode'));
            },
            resolve  : [
                {
                    action (input, done){
                        LF.Utilities.getOngoingHeadaches()
                            .then(ongoingHeadaches => {
                                LF.Utilities.getLastHeadacheNumber()
                                    .then(lastHeadacheNumber => {
                                        let ig                 = 'Headache',
                                            igr                = LF.Data.Questionnaire.getCurrentIGR(ig),
                                            startTime,
                                            endTime,
                                            ongoingHeadache    = _.last(ongoingHeadaches),
                                            durationObject,
                                            medicationTaken,
                                            headacheMedIGR     = LF.Data.Questionnaire.getCurrentIGR('HeadacheMed'),
                                            endedHeadacheCount = 0,
                                            AURA_MAXLOOPS      = 5,
                                            //HEADACHEMED_MAXLOOPS = 20,
                                            HEADACHE_MAXLOOPS  = 4;
                                        LF.Data.Questionnaire.medStartIndex = LF.Utilities.createMedIndexArray();
                                        for(let i = 1; i <= igr; i++){
                                            startTime = _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('HEDBEG1S', i));
                                            medicationTaken = _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('MEDHED1L', i));
                                            endTime = _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('ENDHED1S', i)) || _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('FELSLP1S', i));
                                            if(startTime){
                                                lastHeadacheNumber++;
                                                LF.Utilities.saveNewModel('Headache', {
                                                    headacheID : lastHeadacheNumber.toString(),
                                                    startDate  : LF.Utilities.timeStamp(new Date(startTime.get('response'))),
                                                    endDate    : endTime ? LF.Utilities.timeStamp(new Date(endTime.get('response'))) : ''
                                                });
                                                if(endTime){
                                                    endedHeadacheCount++;
                                                    durationObject = LF.Utilities.calcTimeDuration(new Date(startTime.get('response')), new Date(endTime.get('response')));
                                                    LF.Utilities.addIT({
                                                        question_id      : 'HADMIN1N',
                                                        questionnaire_id : 'DailyDiary',
                                                        response         : durationObject.minutes.toString(),
                                                        IG               : 'Headache',
                                                        IGR              : i,
                                                        IT               : 'HADMIN1N'
                                                    });
                                                    LF.Utilities.addIT({
                                                        question_id      : 'HADRHR1N',
                                                        questionnaire_id : 'DailyDiary',
                                                        response         : durationObject.time[0].toString(),
                                                        IG               : 'Headache',
                                                        IGR              : i,
                                                        IT               : 'HADRHR1N'
                                                    });
                                                    LF.Utilities.addIT({
                                                        question_id      : 'HADRMN1N',
                                                        questionnaire_id : 'DailyDiary',
                                                        response         : durationObject.time[1].toString(),
                                                        IG               : 'Headache',
                                                        IGR              : i,
                                                        IT               : 'HADRMN1N'
                                                    });
                                                }
                                                LF.Utilities.addIT({
                                                    question_id      : 'HEDNUM1N',
                                                    questionnaire_id : 'DailyDiary',
                                                    response         : lastHeadacheNumber.toString(),
                                                    IG               : 'Headache',
                                                    IGR              : i,
                                                    IT               : 'HEDNUM1N'
                                                });
                                                if(medicationTaken && medicationTaken.get('response') === '1'){
                                                    LF.Utilities.saveHeacheNumberToMedLoop(lastHeadacheNumber);
                                                }
                                            }else{
                                                if(ongoingHeadache){
                                                    ongoingHeadache.set('endDate', endTime ? LF.Utilities.timeStamp(new Date(endTime.get('response'))) : '');
                                                    ongoingHeadache.save();
                                                    if(endTime){
                                                        endedHeadacheCount++;
                                                        durationObject = LF.Utilities.calcTimeDuration(new Date(ongoingHeadache.get('startDate')), new Date(endTime.get('response')));
                                                        LF.Utilities.addIT({
                                                            question_id      : 'HADMIN1N',
                                                            questionnaire_id : 'DailyDiary',
                                                            response         : durationObject.minutes.toString(),
                                                            IG               : 'Headache',
                                                            IGR              : i,
                                                            IT               : 'HADMIN1N'
                                                        });
                                                        LF.Utilities.addIT({
                                                            question_id      : 'HADRHR1N',
                                                            questionnaire_id : 'DailyDiary',
                                                            response         : durationObject.time[0].toString(),
                                                            IG               : 'Headache',
                                                            IGR              : i,
                                                            IT               : 'HADRHR1N'
                                                        });
                                                        LF.Utilities.addIT({
                                                            question_id      : 'HADRMN1N',
                                                            questionnaire_id : 'DailyDiary',
                                                            response         : durationObject.time[1].toString(),
                                                            IG               : 'Headache',
                                                            IGR              : i,
                                                            IT               : 'HADRMN1N'
                                                        });
                                                        LF.Utilities.addIT({
                                                            question_id      : 'HEDNUM1N',
                                                            questionnaire_id : 'DailyDiary',
                                                            response         : ongoingHeadache.get('headacheID'),
                                                            IG               : 'Headache',
                                                            IGR              : i,
                                                            IT               : 'HEDNUM1N'
                                                        });
                                                        if(medicationTaken && medicationTaken.get('response') === '1'){
                                                            LF.Utilities.saveHeacheNumberToMedLoop(lastHeadacheNumber);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        LF.Utilities.saveSleepDurations();
                                        if(LF.Data.Questionnaire.getCurrentIGR('AURA') >= AURA_MAXLOOPS){
                                            LF.Utilities.addIT({
                                                question_id      : 'AMDMAX1B',
                                                questionnaire_id : 'DailyDiary',
                                                response         : '1',
                                                IG               : 'AURA',
                                                IGR              : AURA_MAXLOOPS,
                                                IT               : 'AMDMAX1B'
                                            });
                                        }
                                        /*if(LF.Data.Questionnaire.getCurrentIGR('HeadacheMed') >= HEADACHEMED_MAXLOOPS){
                                         LF.Utilities.addIT({
                                         question_id      : 'HMDMAX1B',
                                         questionnaire_id : 'DailyDiary',
                                         response         : '1',
                                         IG               : 'HeadacheMed',
                                         IGR              : 1,
                                         IT               : 'HMDMAX1B'
                                         });
                                         }*/
                                        if(endedHeadacheCount >= HEADACHE_MAXLOOPS){
                                            LF.Utilities.addIT({
                                                question_id      : 'HEDMAX1B',
                                                questionnaire_id : 'DailyDiary',
                                                response         : '1',
                                                IG               : 'Headache',
                                                IGR              : HEADACHE_MAXLOOPS,
                                                IT               : 'HEDMAX1B'
                                            });
                                        }
                                        LF.Utilities.removeFakeITs();
                                        if(LF.Data.Questionnaire.queryAnswersByIGAndIGR('Headache', 1).length === 0){
                                            LF.Widget.ReviewScreen.deleteLoop('Headache', 1);
                                        }
                                        while(LF.Data.Questionnaire.queryAnswersByIGAndIGR('HeadacheMed', headacheMedIGR + 1).length > 0){
                                            LF.Widget.ReviewScreen.deleteLoop('HeadacheMed', headacheMedIGR + 1);
                                        }
                                        LF.Utilities.updateMedNames();
                                        done();
                                    });
                            });
                    }
                }
            ]
        }
    ]
};
