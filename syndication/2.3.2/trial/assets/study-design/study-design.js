import ArrayRef from 'core/classes/ArrayRef';
import ObjectRef from 'core/classes/ObjectRef';
export default {
    // The current study version
    studyVersion                   : '05.01',
    // The current study database version
    studyDbVersion                 : 0,
    // The default language of the study.
    defaultLanguage                : 'en',
    // The default locale of the study
    defaultLocale                  : 'US',
    //Set this flag to true if your study requires partial sync
    enablePartialSync              : false,
    //Set to false to use LPA 1.9 access codes, should always be set to true
    useAcccessCodesFor1dot10       : true,
    // The default option to ask secret question.
    askSecurityQuestion            : true,
    answerFormat                   : '^[a-zA-Z0-9]{4,32}$',
    // If set to true, will show the password rules on password creation views.
    showPasswordRules              : false,
    // The maximum number of consecutive unsuccessful password attempts at login and reactivation.
    maxLoginAttempts               : 3,
    // The maximum number of consecutive unsuccessful secret question attempts.
    maxSecurityQuestionAttempts    : 5,
    // Number of minutes after which the account is automatically re-enabled
    // (where 0 means the account doesn't get re-enabled until a daily reset/override code is used).
    lockoutTime                    : 5,
    // The amount of time in minutes that a user can be inactive before being logged out.
    sessionTimeout                 : 60,
    // The amount of time in minutes that a user is allowed to complete a diary within.
    questionnaireTimeout           : 30,
    // The number of minutes to subtract from the updated timestamp when syncing users to close window of
    // client->server time difference.
    userSyncTimeStampThreshold     : 20,
    // Configuration for the unlock code feature
    unlockCodeConfig               : {
        // Seed code used in calculation for unlock code combined with current date.
        seedCode   : 4820,
        // Sets the length of unlock code
        codeLength : 5
    },
    // Default volume settings for alarms
    alarmVolumeConfig              : {
        // Volume Percentage at which to sound the alarm (0-100)
        alarmVolume : 100,
        // Enable or Disable vibration for notifications
        vibrate     : true,
        // Delay after alarm sounds to revert back to the users configured phone settings
        resetDelay  : 2
    },
    // A string array (of diary SUs) to manually specify the diaries that are unscheduled
    lastDiarySyncUnscheduledSUList : null,
    // Configuration for the eSense feature
    eSenseConfig                   : {
        // Whether or not Bluetooth should be disabled automatically after an operation is complete
        disableBluetooth   : true,
        // Timeout value in milliseconds for eSense all API calls (except findDevices and get records).
        apiCallTimeout     : 10000,
        // Timeout value in milliseconds for eSense findDevices API calls (in milliseconds).
        findDevicesTimeout : 60000,
        // Timeout value in milliseconds for eSense get records API calls (in milliseconds).
        getRecordsTimeout  : 30000,
        //AM1+ Device specific configurations
        AM1Plus            : {
            //Timeout Window configuration for AM1+
            timeWindow : {
                windowNumber : 1,
                startHour    : 0,
                startMinute  : 0,
                endHour      : 23,
                endMinute    : 59,
                maxNumber    : 15
            }
        }
    },
    // Configuration for participant/patient
    participantSettings            : {
        participantID            : {
            seed  : 100,
            steps : 1,
            max   : 600
        },
        participantIDFormat      : '0000-0000',
        participantNumberPortion : new ArrayRef([5, 9]),
        siteNumberPortion        : new ArrayRef([0, 4]),
        languageList             : new ArrayRef([
            {
                language  : 'en',
                locale    : 'US',
                localized : 'EN-US'
            }, {
                language  : 'es',
                locale    : 'ES',
                localized : 'ES-ES'
            },
            {
                language  : 'he',
                locale    : 'IL',
                localized : 'HE-IL'
            }
        ])
    },
    // The study Protocol, mapping for protocol codes to display text
    studyProtocol                  : {
        defaultProtocol : 1,
        protocolList    : new ObjectRef({
            1 : '20120178'
        })
    },
    // protocol string used for filename and build path.
    protocolBuild                  : '20120178',
    // The client name
    clientName                     : 'Amgen',
    // A Regular Expression or a String of valid characters for defining which characters are allowed for input fields.
    validInputCharacters           : 'a-zA-Z0-9\\s\\.-',
    // A list of supported devices for the study.
    /*
     supportedDevices : [{
     name: 'iPhone',
     match: [['Safari']]
     }, {
     name: 'Nexus S',
     match: [['Safari']]
     }, {
     name: 'Galaxy Nexus',
     match: [['Safari']]
     }],
     */

    // The phases used by the study
    studyPhase              : {
        SCREENING   : 10,
        TREATMENT   : 20,
        DORMANT     : 30,
        FOLLOWUP    : 40,
        TERMINATION : 999
    },
    visitList               : {
        week188 : '1',
        week192 : '2',
        week212 : '3',
        week216 : '4',
        week236 : '5',
        week240 : '6',
        week260 : '7',
        week264 : '8',
        week268 : '9'
    },
    //CacheMedications
    medicationInfo          : {
        medication1 : {
            code        : '12651510',
            name        : 'Med 1',
            units       : 'pill(s)',
            unitsSingle : 'PILL',
            methodRoute : 'PILL',
            strength    : '325 mg'
        },
        medication2 : {
            code        : '12651511',
            name        : 'Med 2',
            units       : 'MLS',
            unitsSingle : 'MLS',
            methodRoute : '',
            strength    : '50 ug'
        },
        medication3 : {
            code        : '12651512',
            name        : 'Med 3',
            units       : 'pill(s)',
            unitsSingle : 'Pill',
            methodRoute : '',
            strength    : '150 mg'
        },
        medication4 : {
            code        : '12651513',
            name        : 'Med 4',
            units       : 'pill(s)',
            unitsSingle : 'Pill',
            methodRoute : '',
            strength    : '304 mg'
        },
        medication5 : {
            code        : '12651514',
            name        : 'Med 5',
            units       : 'pill(s)',
            unitsSingle : 'Pill',
            methodRoute : '',
            strength    : '305 mg'
        },
        medication6 : {
            code        : '12651515',
            name        : 'Med 6',
            units       : 'pill(s)',
            unitsSingle : 'Pill',
            strength    : '306 mg'
        },
        medication7 : {
            code        : '12651516',
            name        : 'Med 7',
            units       : 'pill(s)',
            unitsSingle : 'Pill',
            methodRoute : '',
            strength    : '307 mg'
        },
        medication8 : {
            code        : '12651517',
            name        : 'Med 8',
            units       : 'pill(s)',
            unitsSingle : 'Pill',
            methodRoute : '',
            strength    : '308 mg'
        },
        medication9 : {
            code        : '12651518',
            name        : 'Med 9',
            units       : 'mls',
            unitsSingle : 'mls',
            methodRoute : '',
            strength    : '309 mls'
        },
        Other       : {
            code        : '99999999',
            name        : 'OtherMed',
            units       : 'pill(s)',
            unitsSingle : 'Pill',
            methodRoute : '',
            strength    : '310 mg'
        }
    },
    DEFAULT_MASTER_MED_LIST : {
        master : {
            med           : [
                {
                    MEDCOD1C : '105032',
                    MEDNAM1C : 'Rizatriptan',
                    MEDDOS1C : '5 mg',
                    MEDRTE1L : '2',
                    MEDFRM1L : '2'
                },
                {
                    MEDCOD1C : '105042',
                    MEDNAM1C : 'Rizatriptan',
                    MEDDOS1C : '10 mg',
                    MEDRTE1L : '2',
                    MEDFRM1L : '2'
                },
                {
                    MEDCOD1C : '106011',
                    MEDNAM1C : 'Sumatriptan',
                    MEDDOS1C : '4 mg',
                    MEDRTE1L : '1',
                    MEDFRM1L : '1'
                },
                {
                    MEDCOD1C : '106021',
                    MEDNAM1C : 'Sumatriptan',
                    MEDDOS1C : '6 mg',
                    MEDRTE1L : '1',
                    MEDFRM1L : '1'
                },
                {
                    MEDCOD1C : '106035',
                    MEDNAM1C : 'Sumatriptan',
                    MEDDOS1C : '25 mg',
                    MEDRTE1L : '5',
                    MEDFRM1L : '5'
                },
                {
                    MEDCOD1C : '106042',
                    MEDNAM1C : 'Sumatriptan',
                    MEDDOS1C : '50 mg',
                    MEDRTE1L : '2',
                    MEDFRM1L : '2'
                },
                {
                    MEDCOD1C : '106052',
                    MEDNAM1C : 'Sumatriptan',
                    MEDDOS1C : '100 mg',
                    MEDRTE1L : '2',
                    MEDFRM1L : '2'
                },
                {
                    MEDCOD1C : '106062',
                    MEDNAM1C : 'Sumatriptan',
                    MEDDOS1C : '12 mg',
                    MEDRTE1L : '2',
                    MEDFRM1L : '3'
                }
            ],
            effectiveDate : '11 SEP 2001',
            cleanUpdate   : 'true'
        }
    },
    // The phase from studyPhase to be used as the phase after Subject Assignment
    subjectAssignmentPhase  : 10,
    // The phase from studyPhase to be used as the termination phase
    terminationPhase        : 999,
    allowEnvironmentSelect  : true,
    // Whether subject QR codes should be enabled  and displayed on the Home Screen.
    displaySubjectQRCodes   : true,
    // A collection of indicator configurations
    indicators              : [
        /*{
         'id'        : 'PatientConnectIcon',
         'className' : 'PatientConnectIcon',
         'image'     : 'PATIENT_CONNECT_IMG',
         'label'     : 'PATIENT_CONNECT_LABEL'
         }*/
    ]
};
