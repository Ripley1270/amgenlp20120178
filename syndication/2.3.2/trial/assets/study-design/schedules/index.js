import StudySchedules from './schedules';
import {mergeObjects} from 'core/utilities/languageExtensions';
export default mergeObjects({}, StudySchedules);
