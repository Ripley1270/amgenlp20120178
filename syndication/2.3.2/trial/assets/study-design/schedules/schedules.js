export default {
    schedules : [
        {
            id               : 'AutoDial',
            target           : {
                objectType : 'autodial'
            },
            scheduleFunction : 'checkAlwaysAvailability',
            alarmFunction    : 'addAlwaysAlarm',
            alarmParams      : {
                id     : 9,
                time   : {
                    'startTimeRange' : '02:00',
                    'endTimeRange'   : '03:00'
                },
                repeat : 'daily'
            }
        },
        {
            id               : 'newUser_Schedule',
            target           : {
                objectType : 'questionnaire',
                id         : 'New_User'
            },
            scheduleFunction : 'checkAlwaysAvailability',
            scheduleRoles    : ['admin']
        }
    ]
};
