import Logger from 'core/Logger';
import * as Utilities from 'core/utilities';
import LoginView from 'core/views/LoginView';
LF.Utilities.getCollection = function (collectionName) {
    return new Q.Promise((resolve) => {
        let collection = new LF.Collection[collectionName]();
        collection.fetch()
            .then((fetchedCollection) => {
                if(_.isArray(fetchedCollection)){
                    resolve(fetchedCollection);
                }else{
                    resolve(fetchedCollection.models);
                }
            });
    });
};
LF.Utilities.getProtocol = function () {
    return Q.promise((resolve) => {
        if(LF.Utilities.isTrainer()){
            resolve('1');
        }else{
            /*
             LF.Utilities.getCollection("LocalDBCollection").then((localDBCollection) => {
             var localDBModel = _.first(localDBCollection.models);

             if (localDBModel) {
             resolve(localDBModel.get("protocol"));
             } else {
             resolve(null);
             }
             });
             */
            resolve('1');
        }
    });
};
LF.Utilities.getProtocolDisplay = function () {
    return Q.promise((resolve) => {
        LF.Utilities.getProtocol().then((protocol) => {
            let protDisplay = LF.StudyDesign.studyProtocol;
            //TODO: implement switch to add new protocol displays
            switch(protocol){
                //case "1": protDisplay = "KF7019-01"; break;
                default:
                    protDisplay = protDisplay;
                    break;
            }
            resolve(protDisplay);
        });
    });
};
LF.Utilities.getCachedCollection = function (collectionName) {
    return LF.Data.Questionnaire.PDECollections[collectionName];
};
LF.Utilities.checkTrainerOnlyBehavior = function () {
    let trainerOnlyBehaviorOn = false,
        trainerSetupFlag      = localStorage.getItem('Trainer_Data_pdeTrainerMode');
    //check if trainer mode
    if(LF.Utilities.isTrainer()){
        //check if trainer setup flag exists, if it does check if it is set to no
        //If null, default trainer settings used and trainer only behavior should be turned on
        if(trainerSetupFlag == null || trainerSetupFlag == "2"){
            trainerOnlyBehaviorOn = true;
        }
    }
    return trainerOnlyBehaviorOn;
};
LF.Utilities.calculateVisit3Eligibility = function () {
    let morningDiaryCollection = LF.Utilities.getCachedCollection('MorningDiaryCollection'),
        eveningDiaryCollection = LF.Utilities.getCachedCollection('EveningDiaryCollection'),
        reportStartDate        = new Date(LF.Data.Questionnaire.data.dashboard.get('started')),
        twentyOneDaysAgo       = new Date(reportStartDate),
        sevenDaysAgo           = new Date(reportStartDate),
        filteredMorningModels  = [],
        filteredEveningModels  = [],
        sevenDayHighestPEF     = [],
        sevenDayASSDLY1N       = [],
        sevenDayBSLRSC1N       = [],
        sevenDayBSLNBL1N       = [],
        sevenDayBSLNAW1N       = [],
        dailyDate, //temp variable used in looping - calendar day the loop is looking at
        tmpMorningModel,
        tmpEveningModel,
        ELGCRT1N               = 0,
        ELGCRT2N               = 0,
        ELGCRT3N               = 0,
        ELGCRT4N               = 0,
        ELGCRT5N               = 0,
        ELGCRT6N               = 0,
        ELGCRT7N               = 0,
        ELGCRT1L               = 0,
        totalHighestPEF        = 0,
        totalASSDLY1N          = 0,
        totalBSLRSC1N          = 0,
        totalBSLNBL1N          = 0,
        totalBSLNAW1N          = 0,
        BSLMPF1N               = null,
        BSLASS1N               = null,
        BSLRSC1N               = null,
        BSLNBL1N               = null,
        BSLNAW1N               = null;
    twentyOneDaysAgo.setDate(twentyOneDaysAgo.getDate() - 21);
    twentyOneDaysAgo.setHours(0, 0, 0, 0);
    sevenDaysAgo.setDate(sevenDaysAgo.getDate() - 7);
    sevenDaysAgo.setHours(0, 0, 0, 0);
    if(morningDiaryCollection){
        filteredMorningModels = _.filter(morningDiaryCollection, function (model) {
            return new Date(model.get('reportStartDate')).getTime() >= twentyOneDaysAgo.getTime();
        });
    }
    if(eveningDiaryCollection){
        filteredEveningModels = _.filter(eveningDiaryCollection, function (model) {
            return new Date(model.get('reportStartDate')).getTime() >= twentyOneDaysAgo.getTime();
        });
    }
    //21 day window calculations
    for(var i = 0; i < 21; i++){
        dailyDate = new Date(reportStartDate);
        dailyDate.setDate(dailyDate.getDate() - i);
        tmpMorningModel = _.find(filteredMorningModels, function (model) {
            return LF.Utilities.dateDiffInDays(new Date(model.get('reportStartDate')), dailyDate, true) == 0;
        });
        //Evening Diary is from the previous day as the morning diary
        dailyDate.setDate(dailyDate.getDate() - 1);
        tmpEveningModel = _.find(filteredEveningModels, function (model) {
            return LF.Utilities.dateDiffInDays(new Date(model.get('reportStartDate')), dailyDate, true) == 0;
        });
        if(tmpMorningModel &&
            tmpEveningModel){
            ELGCRT1N = ELGCRT1N + 1;
        }
        //subset 7 day calculations
        if(i < 7){
            if(tmpMorningModel &&
                tmpEveningModel){
                ELGCRT2N = ELGCRT2N + 1;
            }
            if(tmpMorningModel){
                //ELGCRT3N
                if(tmpMorningModel.get('AMHPEF1N')){
                    ELGCRT3N = ELGCRT3N + 1;
                    sevenDayHighestPEF.push(tmpMorningModel.get('AMHPEF1N'));
                }
                //ELGCRT5N
                if(tmpMorningModel.get('ASSDLY1N')){
                    if(Number(tmpMorningModel.get('ASSDLY1N')) > 0){
                        ELGCRT5N = ELGCRT5N + 1;
                    }
                    sevenDayASSDLY1N.push(tmpMorningModel.get('ASSDLY1N'));
                }
                //ELGCRT6N
                if((tmpMorningModel.get('RSCTIM1N') && Number(tmpMorningModel.get('RSCTIM1N')) > 0) ||
                    (tmpMorningModel.get('NBLTIM1N') && Number(tmpMorningModel.get('NBLTIM1N')) > 0)){
                    ELGCRT6N = ELGCRT6N + 1;
                }
                //ELGCRT7N
                if((tmpMorningModel.get('ASTUWK1L') && Number(tmpMorningModel.get('ASTUWK1L')) > 0)){
                    ELGCRT7N = ELGCRT7N + 1;
                }
                //push RSCDLY1N values to array to calculate average later on
                if((tmpMorningModel.get('RSCDLY1N'))){
                    sevenDayBSLRSC1N.push(tmpMorningModel.get('RSCDLY1N'));
                }
                //push NBLDLY1N values to array to calculate average later on
                if((tmpMorningModel.get('NBLDLY1N'))){
                    sevenDayBSLNBL1N.push(tmpMorningModel.get('NBLDLY1N'));
                }
                //push ASTUWK1L values to array to calculate average later on
                if((tmpMorningModel.get('ASTUWK1L'))){
                    sevenDayBSLNAW1N.push(tmpMorningModel.get('ASTUWK1L'));
                }
            }
        }
        //21 day window values
        if(tmpMorningModel){

            //MEDTKN1L = 'Yes, I took all doses as scheduled'
            if(tmpMorningModel.get('MEDTKN1L') == '2'){
                ELGCRT4N = ELGCRT4N + 1;
            }
        }
    }
    LF.Utilities.addIT({
        question_id      : 'ELGCRT1N',
        questionnaire_id : 'VisitConfirmation',
        response         : String(ELGCRT1N),
        IG               : 'Eligibility',
        IGR              : 0,
        IT               : 'ELGCRT1N'
    });
    LF.Utilities.addIT({
        question_id      : 'ELGCRT2N',
        questionnaire_id : 'VisitConfirmation',
        response         : String(ELGCRT2N),
        IG               : 'Eligibility',
        IGR              : 0,
        IT               : 'ELGCRT2N'
    });
    LF.Utilities.addIT({
        question_id      : 'ELGCRT3N',
        questionnaire_id : 'VisitConfirmation',
        response         : String(ELGCRT3N),
        IG               : 'Eligibility',
        IGR              : 0,
        IT               : 'ELGCRT3N'
    });
    LF.Utilities.addIT({
        question_id      : 'ELGCRT4N',
        questionnaire_id : 'VisitConfirmation',
        response         : String(ELGCRT4N),
        IG               : 'Eligibility',
        IGR              : 0,
        IT               : 'ELGCRT4N'
    });
    LF.Utilities.addIT({
        question_id      : 'ELGCRT5N',
        questionnaire_id : 'VisitConfirmation',
        response         : String(ELGCRT5N),
        IG               : 'Eligibility',
        IGR              : 0,
        IT               : 'ELGCRT5N'
    });
    LF.Utilities.addIT({
        question_id      : 'ELGCRT6N',
        questionnaire_id : 'VisitConfirmation',
        response         : String(ELGCRT6N),
        IG               : 'Eligibility',
        IGR              : 0,
        IT               : 'ELGCRT6N'
    });
    LF.Utilities.addIT({
        question_id      : 'ELGCRT7N',
        questionnaire_id : 'VisitConfirmation',
        response         : String(ELGCRT7N),
        IG               : 'Eligibility',
        IGR              : 0,
        IT               : 'ELGCRT7N'
    });
    if((ELGCRT1N >= 18 && ELGCRT2N >= 4 && ELGCRT3N >= 4 && ELGCRT4N >= 18 && ELGCRT5N >= 2) ||
        (ELGCRT1N >= 18 && ELGCRT2N >= 4 && ELGCRT3N >= 4 && ELGCRT4N >= 18 && ELGCRT6N >= 2) ||
        (ELGCRT1N >= 18 && ELGCRT2N >= 4 && ELGCRT3N >= 4 && ELGCRT4N >= 18 && ELGCRT7N >= 1) ||
        LF.PDEBypassElig == '1'){ //Dev flag - must be set in console to use
        ELGCRT1L = 1;
    }
    LF.Utilities.addIT({
        question_id      : 'ELGCRT1L',
        questionnaire_id : 'VisitConfirmation',
        response         : String(ELGCRT1L),
        IG               : 'Eligibility',
        IGR              : 0,
        IT               : 'ELGCRT1L'
    });
    //calculate baseline values used for alerts
    //Sum up all recorded highest PEF values for baseline highest PEF
    if(sevenDayHighestPEF.length >= 4){
        _.each(sevenDayHighestPEF, function (val) {
            totalHighestPEF = totalHighestPEF + Number(val);
        });
        BSLMPF1N = totalHighestPEF / sevenDayHighestPEF.length;
        BSLMPF1N = BSLMPF1N.toFixed(2);
        LF.Utilities.addIT({
            question_id      : 'BSLMPF1N',
            questionnaire_id : 'VisitConfirmation',
            response         : String(BSLMPF1N),
            IG               : 'Baseline',
            IGR              : 0,
            IT               : 'BSLMPF1N'
        });
    }
    //Sum up all ASSDLY1N values for baseline asthma symptom score
    if(sevenDayASSDLY1N.length >= 4){
        _.each(sevenDayASSDLY1N, function (val) {
            totalASSDLY1N = totalASSDLY1N + Number(val);
        });
        BSLASS1N = totalASSDLY1N / sevenDayASSDLY1N.length;
        BSLASS1N = BSLASS1N.toFixed(0);
        LF.Utilities.addIT({
            question_id      : 'BSLASS1N',
            questionnaire_id : 'VisitConfirmation',
            response         : String(BSLASS1N),
            IG               : 'Baseline',
            IGR              : 0,
            IT               : 'BSLASS1N'
        });
    }
    //Sum up all RSCDLY1N values for baseline inhaler use puffs
    if(sevenDayBSLRSC1N.length >= 4){
        _.each(sevenDayBSLRSC1N, function (val) {
            totalBSLRSC1N = totalBSLRSC1N + Number(val);
        });
        BSLRSC1N = totalBSLRSC1N / sevenDayBSLRSC1N.length;
        BSLRSC1N = BSLRSC1N.toFixed(0);
        LF.Utilities.addIT({
            question_id      : 'BSLRSC1N',
            questionnaire_id : 'VisitConfirmation',
            response         : String(BSLRSC1N),
            IG               : 'Baseline',
            IGR              : 0,
            IT               : 'BSLRSC1N'
        });
    }
    //Sum up all NBLDLY1N values for baseline nebulizer use
    if(sevenDayBSLNBL1N.length >= 4){
        _.each(sevenDayBSLNBL1N, function (val) {
            totalBSLNBL1N = totalBSLNBL1N + Number(val);
        });
        BSLNBL1N = totalBSLNBL1N / sevenDayBSLNBL1N.length;
        BSLNBL1N = BSLNBL1N.toFixed(0);
        LF.Utilities.addIT({
            question_id      : 'BSLNBL1N',
            questionnaire_id : 'VisitConfirmation',
            response         : String(BSLNBL1N),
            IG               : 'Baseline',
            IGR              : 0,
            IT               : 'BSLNBL1N'
        });
    }
    //Sum up all ASTUWK1L values for baseline nocturnal awakenings score
    if(sevenDayBSLNAW1N.length >= 4){
        _.each(sevenDayBSLNAW1N, function (val) {
            totalBSLNAW1N = totalBSLNAW1N + Number(val);
        });
        BSLNAW1N = totalBSLNAW1N / sevenDayBSLNAW1N.length;
        BSLNAW1N = BSLNAW1N.toFixed(2);
        LF.Utilities.addIT({
            question_id      : 'BSLNAW1N',
            questionnaire_id : 'VisitConfirmation',
            response         : String(BSLNAW1N),
            IG               : 'Baseline',
            IGR              : 0,
            IT               : 'BSLNAW1N'
        });
    }
};
LF.Utilities.calculateTmpEligibilityMorningDiary = function () {
    let morningDiaryCollection = LF.Utilities.getCachedCollection('MorningDiaryCollection'),
        eveningDiaryCollection = LF.Utilities.getCachedCollection('EveningDiaryCollection'),
        reportStartDate        = new Date(LF.Data.Questionnaire.data.dashboard.get('started')),
        twentyOneDaysAgo       = new Date(reportStartDate),
        sevenDaysAgo           = new Date(reportStartDate),
        filteredMorningModels  = [],
        filteredEveningModels  = [],
        dailyDate, //temp variable used in looping - calendar day the loop is looking at
        tmpMorningModel,
        tmpEveningModel,
        ELGCRT1N               = 0,
        ELGCRT2N               = 0,
        ELGCRT3N               = 0,
        ELGCRT4N               = 0,
        ELGCRT5N               = 0,
        ELGCRT6N               = 0,
        ELGCRT7N               = 0,
        ELGCRT1L               = 0;
    twentyOneDaysAgo.setDate(twentyOneDaysAgo.getDate() - 21);
    twentyOneDaysAgo.setHours(0, 0, 0, 0);
    sevenDaysAgo.setDate(sevenDaysAgo.getDate() - 7);
    sevenDaysAgo.setHours(0, 0, 0, 0);
    if(morningDiaryCollection){
        filteredMorningModels = _.filter(morningDiaryCollection, function (model) {
            return new Date(model.get('reportStartDate')).getTime() >= twentyOneDaysAgo.getTime();
        });
    }
    if(eveningDiaryCollection){
        filteredEveningModels = _.filter(eveningDiaryCollection, function (model) {
            return new Date(model.get('reportStartDate')).getTime() >= twentyOneDaysAgo.getTime();
        });
    }
    //21 day window calculations
    for(var i = 0; i < 21; i++){
        dailyDate = new Date(reportStartDate);
        dailyDate.setDate(dailyDate.getDate() - i);
        tmpMorningModel = _.find(filteredMorningModels, function (model) {
            return LF.Utilities.dateDiffInDays(new Date(model.get('reportStartDate')), dailyDate, true) == 0;
        });
        //Evening Diary is from the previous day as the morning diary
        dailyDate.setDate(dailyDate.getDate() - 1);
        tmpEveningModel = _.find(filteredEveningModels, function (model) {
            return LF.Utilities.dateDiffInDays(new Date(model.get('reportStartDate')), dailyDate, true) == 0;
        });
        if(tmpMorningModel &&
            tmpEveningModel){
            ELGCRT1N = ELGCRT1N + 1;
        }
        //subset 7 day calculations
        if(i < 7){
            if(tmpMorningModel &&
                tmpEveningModel){
                ELGCRT2N = ELGCRT2N + 1;
            }
            if(tmpMorningModel){
                //ELGCRT3N
                if(tmpMorningModel.get('AMHPEF1N')){
                    ELGCRT3N = ELGCRT3N + 1;
                }
                //ELGCRT5N
                if(tmpMorningModel.get('ASSDLY1N')){
                    if(Number(tmpMorningModel.get('ASSDLY1N')) > 0){
                        ELGCRT5N = ELGCRT5N + 1;
                    }
                }
                //ELGCRT6N
                if((tmpMorningModel.get('RSCTIM1N') && Number(tmpMorningModel.get('RSCTIM1N')) > 0) ||
                    (tmpMorningModel.get('NBLTIM1N') && Number(tmpMorningModel.get('NBLTIM1N')) > 0)){
                    ELGCRT6N = ELGCRT6N + 1;
                }
                //ELGCRT7N
                if((tmpMorningModel.get('ASTUWK1L') && Number(tmpMorningModel.get('ASTUWK1L')) > 0)){
                    ELGCRT7N = ELGCRT7N + 1;
                }
            }
        }
        //21 day window values
        if(tmpMorningModel){

            //MEDTKN1L = 'Yes, I took all doses as scheduled'
            if(tmpMorningModel.get('MEDTKN1L') == '2'){
                ELGCRT4N = ELGCRT4N + 1;
            }
        }
    }
    LF.Utilities.addIT({
        question_id      : 'ELGCRT1N',
        questionnaire_id : 'MorningDiary',
        response         : String(ELGCRT1N),
        IG               : 'TmpEligibility',
        IGR              : 0,
        IT               : 'ELGCRT1N'
    });
    LF.Utilities.addIT({
        question_id      : 'ELGCRT2N',
        questionnaire_id : 'MorningDiary',
        response         : String(ELGCRT2N),
        IG               : 'TmpEligibility',
        IGR              : 0,
        IT               : 'ELGCRT2N'
    });
    LF.Utilities.addIT({
        question_id      : 'ELGCRT3N',
        questionnaire_id : 'MorningDiary',
        response         : String(ELGCRT3N),
        IG               : 'TmpEligibility',
        IGR              : 0,
        IT               : 'ELGCRT3N'
    });
    LF.Utilities.addIT({
        question_id      : 'ELGCRT4N',
        questionnaire_id : 'MorningDiary',
        response         : String(ELGCRT4N),
        IG               : 'TmpEligibility',
        IGR              : 0,
        IT               : 'ELGCRT4N'
    });
    LF.Utilities.addIT({
        question_id      : 'ELGCRT5N',
        questionnaire_id : 'MorningDiary',
        response         : String(ELGCRT5N),
        IG               : 'TmpEligibility',
        IGR              : 0,
        IT               : 'ELGCRT5N'
    });
    LF.Utilities.addIT({
        question_id      : 'ELGCRT6N',
        questionnaire_id : 'MorningDiary',
        response         : String(ELGCRT6N),
        IG               : 'TmpEligibility',
        IGR              : 0,
        IT               : 'ELGCRT6N'
    });
    LF.Utilities.addIT({
        question_id      : 'ELGCRT7N',
        questionnaire_id : 'MorningDiary',
        response         : String(ELGCRT7N),
        IG               : 'TmpEligibility',
        IGR              : 0,
        IT               : 'ELGCRT7N'
    });
    if((ELGCRT1N >= 18 && ELGCRT2N >= 4 && ELGCRT3N >= 4 && ELGCRT4N >= 18 && ELGCRT5N >= 2) ||
        (ELGCRT1N >= 18 && ELGCRT2N >= 4 && ELGCRT3N >= 4 && ELGCRT4N >= 18 && ELGCRT6N >= 2) ||
        (ELGCRT1N >= 18 && ELGCRT2N >= 4 && ELGCRT3N >= 4 && ELGCRT4N >= 18 && ELGCRT7N >= 1)){
        ELGCRT1L = 1;
    }
    LF.Utilities.addIT({
        question_id      : 'ELGCRT1L',
        questionnaire_id : 'MorningDiary',
        response         : String(ELGCRT1L),
        IG               : 'TmpEligibility',
        IGR              : 0,
        IT               : 'ELGCRT1L'
    });
};
LF.Utilities.calculateMorningDiaryWorseningAlerts = function () {
    let logger             = new Logger('Utilities:calculateMorningDiaryWorseningAlerts'),
        morningDiaryModel  = new LF.Model.MorningDiaryModel(),
        eveningDiaryModels = LF.Utilities.getCachedCollection('EveningDiaryCollection'),
        morningDiaryModels = LF.Utilities.getCachedCollection('MorningDiaryCollection'),
        localDBModel       = _.first(LF.Utilities.getCachedCollection('LocalDBCollection')),
        yesterdayEveningModel,
        twoDaysAgoEveningModel,
        yesterdayMorningModel,
        filterSevenDayMorningModels,
        questionnaire      = LF.Data.Questionnaire.data.dashboard.get('questionnaire_id'),
        phase              = LF.Data.Questionnaire.data.dashboard.get('phase'),
        phaseStartTS       = new Date(LF.Data.Questionnaire.data.dashboard.get('phaseStartDateTZOffset')),
        now                = new Date(),
        dateDiffInDays     = 0,
        reportStartDate    = LF.Data.Questionnaire.data.dashboard.get('started'),
        yesterday          = new Date(reportStartDate),
        twoDaysAgo         = new Date(reportStartDate),
        sevenDayWindow     = new Date(reportStartDate),
        AMHPEF1N           = LF.Data.Questionnaire.queryAnswersByIT('AMHPEF1N')[0],
        SMYNGH1L           = LF.Data.Questionnaire.queryAnswersByIT('SMYNGH1L')[0],
        WRSALR1B,
        MPFCHN1N,
        MPFCHN1B,
        MPFCHN2B,
        ASSDLY1N,
        ASSCHN1C,
        ASSCHN1B,
        ASSCHN2B,
        ASSHSC1B,
        ASSHSC2B,
        RSCCHN1C,
        RSCCHN1B,
        RSCCHN2B,
        NBLCHN1C,
        NBLCHN1B,
        NBLCHN2B,
        NAWCHN1C,
        NAWCHN1B,
        NAWCHN2B,
        NAWHSC1B,
        NAWHSC2B,
        RSCMAX1B,
        yesterdayWRSALR1B,
        yesterdayASSCHN1B,
        yesterdayASSHSC1B,
        yesterdayRSCCHN1B,
        yesterdayNBLCHN1B,
        yesterdayNAWCHN1B,
        yesterdayNAWHSC1B,
        yesterdayMPFCHN1B,
        RSCMED1L           = LF.Data.Questionnaire.queryAnswersByIT('RSCMED1L')[0],
        RSCTIM1N           = LF.Data.Questionnaire.queryAnswersByIT('RSCTIM1N')[0],
        NBLTIM1N           = LF.Data.Questionnaire.queryAnswersByIT('NBLTIM1N')[0],
        ASTUWK1L           = LF.Data.Questionnaire.queryAnswersByIT('ASTUWK1L')[0],
        MEDTKN1L           = LF.Data.Questionnaire.queryAnswersByIT('MEDTKN1L')[0],
        RSCDLY1N,
        NBLDLY1N,
        NAWSCR1N,
        eveningSMYDYT1L,
        eveningRSCTIM1N,
        eveningNBLTIM1N;
    yesterday.setDate(yesterday.getDate() - 1);
    sevenDayWindow.setDate(sevenDayWindow.getDate() - 6);
    twoDaysAgo.setDate(twoDaysAgo.getDate() - 2);
    if(ASTUWK1L){
        ASTUWK1L = ASTUWK1L.get('response');
        morningDiaryModel.set('ASTUWK1L', String(ASTUWK1L));
    }
    if(MEDTKN1L){
        MEDTKN1L = MEDTKN1L.get('response');
        morningDiaryModel.set('MEDTKN1L', String(MEDTKN1L));
    }
    if(AMHPEF1N){
        AMHPEF1N = AMHPEF1N.get('response');
        morningDiaryModel.set('AMHPEF1N', String(AMHPEF1N));
    }
    LF.Utilities.addIT({
        question_id      : 'DIARSTM',
        questionnaire_id : questionnaire,
        response         : LF.Utilities.buildStudyWorksFormat(new Date(reportStartDate)),
        IG               : LF.StudyDesign.studyIG.IG_MORNING_DIARY,
        IGR              : 0,
        IT               : 'DIARSTM'
    });
    LF.Utilities.addIT({
        question_id      : 'DIARETM',
        questionnaire_id : questionnaire,
        response         : LF.Utilities.buildStudyWorksFormat(now),
        IG               : LF.StudyDesign.studyIG.IG_MORNING_DIARY,
        IGR              : 0,
        IT               : 'DIARETM'
    });
    //Find the evening diary model from yesterday
    if(eveningDiaryModels){
        yesterdayEveningModel = _.find(eveningDiaryModels, function (model) {
            return LF.Utilities.dateDiffInDays(yesterday, new Date(model.get('reportStartDate')), true) == 0;
        });
    }
    //Find the morning diary model from yesterday, needed for worsening alert flags
    if(morningDiaryModels){
        yesterdayMorningModel = _.find(morningDiaryModels, function (model) {
            return LF.Utilities.dateDiffInDays(yesterday, new Date(model.get('reportStartDate')), true) == 0;
        });
    }
    if(eveningDiaryModels){
        twoDaysAgoEveningModel = _.find(eveningDiaryModels, function (model) {
            return LF.Utilities.dateDiffInDays(twoDaysAgo, new Date(model.get('reportStartDate')), true) == 0;
        });
    }
    //Fetch yesterday's morning diary alert flags if they exist
    if(yesterdayMorningModel){
        yesterdayWRSALR1B = yesterdayMorningModel.get('WRSALR1B');
        yesterdayASSCHN1B = yesterdayMorningModel.get('ASSCHN1B');
        yesterdayRSCCHN1B = yesterdayMorningModel.get('RSCCHN1B');
        yesterdayNBLCHN1B = yesterdayMorningModel.get('NBLCHN1B');
        yesterdayNAWCHN1B = yesterdayMorningModel.get('NAWCHN1B');
        yesterdayNAWHSC1B = yesterdayMorningModel.get('NAWHSC1B');
        yesterdayASSHSC1B = yesterdayMorningModel.get('ASSHSC1B');
        yesterdayMPFCHN1B = yesterdayMorningModel.get('MPFCHN1B');
    }
    //set RSCTIM1N
    if(RSCMED1L){
        RSCMED1L = RSCMED1L.get('response');
        if(RSCMED1L == 0 || RSCMED1L == 2){
            RSCTIM1N = '0';
            LF.Utilities.addIT({
                question_id      : 'RSCTIM1N',
                questionnaire_id : questionnaire,
                response         : RSCTIM1N,
                IG               : LF.StudyDesign.studyIG.IG_MORNING_DIARY,
                IGR              : 0,
                IT               : 'RSCTIM1N'
            });
        }
        else if(RSCTIM1N){
            RSCTIM1N = RSCTIM1N.get('response');
        }
        else{
            logger.error('Error - unable to retrieve RSCTIM1N value');
            RSCTIM1N = '0';
        }
        morningDiaryModel.set('RSCTIM1N', RSCTIM1N);
        //set NBLTIM1N
        if(RSCMED1L == 0 || RSCMED1L == 1){
            NBLTIM1N = '0';
            LF.Utilities.addIT({
                question_id      : 'NBLTIM1N',
                questionnaire_id : questionnaire,
                response         : NBLTIM1N,
                IG               : LF.StudyDesign.studyIG.IG_MORNING_DIARY,
                IGR              : 0,
                IT               : 'NBLTIM1N'
            });
        }
        else if(NBLTIM1N){
            NBLTIM1N = NBLTIM1N.get('response');
        }
        else{
            logger.error('Error - unable to retrieve NBLTIM1N value');
            NBLTIM1N = '0';
        }
        morningDiaryModel.set('NBLTIM1N', NBLTIM1N);
    }
    //Calc MorningDiary custom variables that rely on data from yesterday's evening diary
    //ASSDLY1N
    //RSCDLY1N
    if(yesterdayEveningModel){
        eveningSMYDYT1L = yesterdayEveningModel.get('SMYDYT1L');
        eveningRSCTIM1N = yesterdayEveningModel.get('RSCTIM1N');
        eveningNBLTIM1N = yesterdayEveningModel.get('NBLTIM1N');
        //calc ASSDLY1N
        if(eveningSMYDYT1L && SMYNGH1L){
            ASSDLY1N = Number(eveningSMYDYT1L) + Number(SMYNGH1L.get('response'));
            morningDiaryModel.set('ASSDLY1N', String(ASSDLY1N));
        }
        else{
            logger.error('Error getting SMYDYT1L and SMYNGH1L to calculate ASSDLY1N');
        }
        // calc RSCDLY1N
        if(eveningRSCTIM1N && RSCTIM1N){
            RSCDLY1N = Number(eveningRSCTIM1N) + Number(RSCTIM1N);
        }
        else{
            logger.error('Error getting eveningRSCTIM1N and RSCTIM1N to calculate RSCDLY1N');
        }
        //calc NBLDLY1N
        if(eveningNBLTIM1N && NBLTIM1N){
            NBLDLY1N = Number(eveningNBLTIM1N) + Number(NBLTIM1N);
        }
        else{
            logger.error('Error getting eveningNBLTIM1N and NBLTIM1N to calculate NBLDLY1N');
        }
    }
    morningDiaryModel.set('reportStartDate', LF.Utilities.timeStamp(new Date(reportStartDate)));
    if(ASSDLY1N){
        morningDiaryModel.set('ASSDLY1N', String(ASSDLY1N));
        LF.Utilities.addIT({
            question_id      : 'ASSDLY1N',
            questionnaire_id : questionnaire,
            response         : String(ASSDLY1N),
            IG               : LF.StudyDesign.studyIG.IG_MORNING_DIARY,
            IGR              : 0,
            IT               : 'ASSDLY1N'
        });
    }
    if(NBLDLY1N){
        morningDiaryModel.set('NBLDLY1N', String(NBLDLY1N));
        LF.Utilities.addIT({
            question_id      : 'NBLDLY1N',
            questionnaire_id : questionnaire,
            response         : String(NBLDLY1N),
            IG               : LF.StudyDesign.studyIG.IG_MORNING_DIARY,
            IGR              : 0,
            IT               : 'NBLDLY1N'
        });
    }
    if(RSCDLY1N){
        morningDiaryModel.set('RSCDLY1N', String(RSCDLY1N));
        LF.Utilities.addIT({
            question_id      : 'RSCDLY1N',
            questionnaire_id : questionnaire,
            response         : String(RSCDLY1N),
            IG               : LF.StudyDesign.studyIG.IG_MORNING_DIARY,
            IGR              : 0,
            IT               : 'RSCDLY1N'
        });
    }
    //Worsening alert flags - treatment and IPD_treatment only
    if(phase == LF.StudyDesign.studyPhase.TREATMENT || phase == LF.StudyDesign.studyPhase.IPD_TREATMENT){
        let BSLMPF1N,
            BSLASS1N,
            BSLRSC1N,
            BSLNBL1N,
            BSLNAW1N;
        if(localDBModel){
            BSLMPF1N = localDBModel.get('BSLMPF1N');
            BSLASS1N = localDBModel.get('BSLASS1N');
            BSLRSC1N = localDBModel.get('BSLRSC1N');
            BSLNBL1N = localDBModel.get('BSLNBL1N');
            BSLNAW1N = localDBModel.get('BSLNAW1N');
        }
        if(BSLMPF1N && AMHPEF1N){
            if(BSLMPF1N != '0'){
                MPFCHN1N = (Number(AMHPEF1N) / Number(BSLMPF1N)) * 100;
                MPFCHN1N = MPFCHN1N.toFixed(2);
                LF.Utilities.addIT({
                    question_id      : 'MPFCHN1N',
                    questionnaire_id : questionnaire,
                    response         : String(MPFCHN1N),
                    IG               : LF.StudyDesign.studyIG.IG_WORSENINGALERT,
                    IGR              : 0,
                    IT               : 'MPFCHN1N'
                });
                if(Number(MPFCHN1N) <= 80){
                    MPFCHN1B = '1';
                    LF.Utilities.addIT({
                        question_id      : 'MPFCHN1B',
                        questionnaire_id : questionnaire,
                        response         : String(MPFCHN1B),
                        IG               : LF.StudyDesign.studyIG.IG_WORSENINGALERT,
                        IGR              : 0,
                        IT               : 'MPFCHN1B'
                    });
                }
                //ASSCHN2B set if WRSALR1B was not set yesterday and ASSCHIN1B is true yesterday and today
                if(yesterdayWRSALR1B != '1' && yesterdayMPFCHN1B == '1' && MPFCHN1B == '1'){
                    MPFCHN2B = '1';
                    LF.Utilities.addIT({
                        question_id      : 'MPFCHN2B',
                        questionnaire_id : questionnaire,
                        response         : String(MPFCHN2B),
                        IG               : LF.StudyDesign.studyIG.IG_WORSENINGALERT,
                        IGR              : 0,
                        IT               : 'MPFCHN2B'
                    });
                }
            }
        }
        //ASSCHN1C, ASSCHN1B, ASSCHN2B
        if(BSLASS1N && ASSDLY1N){
            ASSCHN1C = Number(ASSDLY1N) - Number(BSLASS1N);
            LF.Utilities.addIT({
                question_id      : 'ASSCHN1C',
                questionnaire_id : questionnaire,
                response         : String(ASSCHN1C),
                IG               : LF.StudyDesign.studyIG.IG_WORSENINGALERT,
                IGR              : 0,
                IT               : 'ASSCHN1C'
            });
            if(ASSCHN1C >= 2){
                ASSCHN1B = '1';
                morningDiaryModel.set('ASSCHN1B', String(ASSCHN1B));
                LF.Utilities.addIT({
                    question_id      : 'ASSCHN1B',
                    questionnaire_id : questionnaire,
                    response         : String(ASSCHN1B),
                    IG               : LF.StudyDesign.studyIG.IG_WORSENINGALERT,
                    IGR              : 0,
                    IT               : 'ASSCHN1B'
                });
            }
            //ASSCHN2B set if WRSALR1B was not set yesterday and ASSCHIN1B is true yesterday and today
            if(yesterdayWRSALR1B != '1' && yesterdayASSCHN1B == '1' && ASSCHN1B == '1'){
                ASSCHN2B = '1';
                LF.Utilities.addIT({
                    question_id      : 'ASSCHN2B',
                    questionnaire_id : questionnaire,
                    response         : String(ASSCHN2B),
                    IG               : LF.StudyDesign.studyIG.IG_WORSENINGALERT,
                    IGR              : 0,
                    IT               : 'ASSCHN2B'
                });
            }
            //ASSHSC1B
            if(ASSDLY1N == '6'){
                ASSHSC1B = '1';
                morningDiaryModel.set('ASSHSC1B', String(ASSHSC1B));
                LF.Utilities.addIT({
                    question_id      : 'ASSHSC1B',
                    questionnaire_id : questionnaire,
                    response         : String(ASSHSC1B),
                    IG               : LF.StudyDesign.studyIG.IG_WORSENINGALERT,
                    IGR              : 0,
                    IT               : 'ASSHSC1B'
                });
            }
            //ASSHSC2B set if WRSALR1B was not set yesterday and ASSHSC1B is true yesterday and today
            if(yesterdayWRSALR1B != '1' && yesterdayASSHSC1B == '1' && ASSHSC1B == '1'){
                ASSHSC2B = '1';
                LF.Utilities.addIT({
                    question_id      : 'ASSHSC2B',
                    questionnaire_id : questionnaire,
                    response         : String(ASSHSC2B),
                    IG               : LF.StudyDesign.studyIG.IG_WORSENINGALERT,
                    IGR              : 0,
                    IT               : 'ASSHSC2B'
                });
            }
        }
        //RSCCHN1C, RSCCHN1B, RSCCHN2B
        if(BSLRSC1N && RSCTIM1N){
            RSCCHN1C = Number(RSCTIM1N) - Number(BSLRSC1N);
            LF.Utilities.addIT({
                question_id      : 'RSCCHN1C',
                questionnaire_id : questionnaire,
                response         : String(RSCCHN1C),
                IG               : LF.StudyDesign.studyIG.IG_WORSENINGALERT,
                IGR              : 0,
                IT               : 'RSCCHN1C'
            });
            if(RSCCHN1C >= 4){
                RSCCHN1B = '1';
                morningDiaryModel.set('RSCCHN1B', String(RSCCHN1B));
                LF.Utilities.addIT({
                    question_id      : 'RSCCHN1B',
                    questionnaire_id : questionnaire,
                    response         : String(RSCCHN1B),
                    IG               : LF.StudyDesign.studyIG.IG_WORSENINGALERT,
                    IGR              : 0,
                    IT               : 'RSCCHN1B'
                });
            }
            //RSCCHN2B set if WRSALR1B was not set yesterday and RSCCHN1B is true yesterday and today
            if(yesterdayWRSALR1B != '1' && yesterdayRSCCHN1B == '1' && RSCCHN1B == '1'){
                RSCCHN2B = '1';
                LF.Utilities.addIT({
                    question_id      : 'RSCCHN2B',
                    questionnaire_id : questionnaire,
                    response         : String(RSCCHN2B),
                    IG               : LF.StudyDesign.studyIG.IG_WORSENINGALERT,
                    IGR              : 0,
                    IT               : 'RSCCHN2B'
                });
            }
        }
        //NBLCHN1C, NBLCHN1B, NBLCHN2B,
        if(BSLNBL1N && NBLTIM1N){
            NBLCHN1C = Number(NBLTIM1N) - Number(BSLNBL1N);
            LF.Utilities.addIT({
                question_id      : 'NBLCHN1C',
                questionnaire_id : questionnaire,
                response         : String(NBLCHN1C),
                IG               : LF.StudyDesign.studyIG.IG_WORSENINGALERT,
                IGR              : 0,
                IT               : 'NBLCHN1C'
            });
            if(NBLCHN1C >= 1){
                NBLCHN1B = '1';
                morningDiaryModel.set('NBLCHN1B', String(NBLCHN1B));
                LF.Utilities.addIT({
                    question_id      : 'NBLCHN1B',
                    questionnaire_id : questionnaire,
                    response         : String(NBLCHN1B),
                    IG               : LF.StudyDesign.studyIG.IG_WORSENINGALERT,
                    IGR              : 0,
                    IT               : 'NBLCHN1B'
                });
            }
            //NBLCHN2B set if WRSALR1B was not set yesterday and NBLCHN1B is true yesterday and today
            if(yesterdayWRSALR1B != '1' && yesterdayNBLCHN1B == '1' && NBLCHN1B == '1'){
                NBLCHN2B = '1';
                LF.Utilities.addIT({
                    question_id      : 'NBLCHN2B',
                    questionnaire_id : questionnaire,
                    response         : String(NBLCHN2B),
                    IG               : LF.StudyDesign.studyIG.IG_WORSENINGALERT,
                    IGR              : 0,
                    IT               : 'NBLCHN2B'
                });
            }
        }
        //NAWSCR1N, NAWCHN1C, NAWCHN1B, NAWCHN2B
        dateDiffInDays = LF.Utilities.dateDiffInDays(phaseStartTS, new Date(reportStartDate), true);
        if(dateDiffInDays >= 7){
            filterSevenDayMorningModels = _.find(morningDiaryModels, function (model) {
                return LF.Utilities.dateDiffInDays(sevenDayWindow, new Date(model.get('reportStartDate')), true) <= 7;
            });
            //NAWSCR1N - requires 4 morning diaries done in the last 7days (check at least 3 historical diaries plus
            // today's data is 4)
            if(filterSevenDayMorningModels && filterSevenDayMorningModels.length >= 3){
                NAWSCR1N = 0;
                //Check if today's ASTUWK1L = Yes
                if(ASTUWK1L == '1'){
                    NAWSCR1N = NAWSCR1N + 1;
                }
                _.each(filterSevenDayMorningModels, function (model) {
                    if(model.get('ASTUWK1L') == '1'){
                        NAWSCR1N = NAWSCR1N + 1;
                    }
                });
                NAWSCR1N = NAWSCR1N / filterSevenDayMorningModels.length;
                NAWSCR1N = NAWSCR1N.toFixed(2);
                LF.Utilities.addIT({
                    question_id      : 'NAWSCR1N',
                    questionnaire_id : questionnaire,
                    response         : String(NAWSCR1N),
                    IG               : LF.StudyDesign.studyIG.IG_MORNING_DIARY,
                    IGR              : 0,
                    IT               : 'NAWSCR1N'
                });
                //NAWCHN1C
                if(BSLNAW1N){
                    NAWCHN1C = Number(NAWSCR1N) - Number(BSLNAW1N);
                    LF.Utilities.addIT({
                        question_id      : 'NAWCHN1C',
                        questionnaire_id : questionnaire,
                        response         : String(NAWCHN1C),
                        IG               : LF.StudyDesign.studyIG.IG_WORSENINGALERT,
                        IGR              : 0,
                        IT               : 'NAWCHN1C'
                    });
                    if(NAWCHN1C >= 2){
                        NAWCHN1B = '1';
                        morningDiaryModel.set('NAWCHN1B', String(NAWCHN1B));
                        LF.Utilities.addIT({
                            question_id      : 'NAWCHN1B',
                            questionnaire_id : questionnaire,
                            response         : String(NAWCHN1B),
                            IG               : LF.StudyDesign.studyIG.IG_WORSENINGALERT,
                            IGR              : 0,
                            IT               : 'NAWCHN1B'
                        });
                    }
                    //NAWCHN2B set if WRSALR1B was not set yesterday and NAWCHN1B is true yesterday and today
                    if(yesterdayWRSALR1B != '1' && yesterdayNAWCHN1B == '1' && NAWCHN1B == '1'){
                        NAWCHN2B = '1';
                        LF.Utilities.addIT({
                            question_id      : 'NAWCHN2B',
                            questionnaire_id : questionnaire,
                            response         : String(NAWCHN2B),
                            IG               : LF.StudyDesign.studyIG.IG_WORSENINGALERT,
                            IGR              : 0,
                            IT               : 'NAWCHN2B'
                        });
                    }
                }
                //NAWHSC1B
                if(NAWSCR1N >= 6){
                    NAWHSC1B = '1';
                    morningDiaryModel.set('NAWHSC1B', String(NAWHSC1B));
                    LF.Utilities.addIT({
                        question_id      : 'NAWHSC1B',
                        questionnaire_id : questionnaire,
                        response         : String(NAWHSC1B),
                        IG               : LF.StudyDesign.studyIG.IG_WORSENINGALERT,
                        IGR              : 0,
                        IT               : 'NAWHSC1B'
                    });
                }
                //NAWHSC2B set if WRSALR1B was not set yesterday and NAWHSC1B is true yesterday and today
                if(yesterdayWRSALR1B != '1' && yesterdayNAWHSC1B == '1' && NAWHSC1B == '1'){
                    NAWHSC2B = '1';
                    LF.Utilities.addIT({
                        question_id      : 'NAWHSC2B',
                        questionnaire_id : questionnaire,
                        response         : String(NAWHSC2B),
                        IG               : LF.StudyDesign.studyIG.IG_WORSENINGALERT,
                        IGR              : 0,
                        IT               : 'NAWHSC2B'
                    });
                }
            }
        }
        //RSCMAX1B
        let twoDaysAgoEveningRSCTIM1N;
        if(twoDaysAgoEveningModel){
            twoDaysAgoEveningRSCTIM1N = twoDaysAgoEveningModel.get('RSCTIM1N');
        }
        if((RSCDLY1N && Number(RSCDLY1N) >= 12) ||
            (RSCTIM1N && Number(RSCTIM1N) >= 12) ||
            (twoDaysAgoEveningRSCTIM1N && Number(twoDaysAgoEveningRSCTIM1N) >= 12)){
            RSCMAX1B = '1';
            LF.Utilities.addIT({
                question_id      : 'RSCMAX1B',
                questionnaire_id : questionnaire,
                response         : String(RSCMAX1B),
                IG               : LF.StudyDesign.studyIG.IG_WORSENINGALERT,
                IGR              : 0,
                IT               : 'RSCMAX1B'
            });
        }
    }
    if(MPFCHN2B == '1' || ASSCHN2B == '1' || ASSHSC2B == '1' ||
        RSCCHN2B == '1' || NBLCHN2B == '1' || NAWHSC2B == '1' ||
        NAWCHN2B == '1' || RSCMAX1B == '1'){
        WRSALR1B = '1';
        morningDiaryModel.set('WRSALR1B', String(WRSALR1B));
        LF.Utilities.addIT({
            question_id      : 'WRSALR1B',
            questionnaire_id : questionnaire,
            response         : String(WRSALR1B),
            IG               : LF.StudyDesign.studyIG.IG_WORSENINGALERT,
            IGR              : 0,
            IT               : 'WRSALR1B'
        });
    }
    //Cache morning diary model, will be committed to disk in diary completion event
    LF.Data.Questionnaire.data.PDEMorningDiaryModel = morningDiaryModel;
    return WRSALR1B;
};
LF.Utilities.determineAutolaunch = function (schedulesToCheck) {
    return Q()
        .then(() => {
            return LF.Utilities.getCollection('LastDiaries');
        })
        .then((lastDiaries) => {
            let length                = schedulesToCheck.length,
                completedQ            = lastDiaries,
                phase                 = LF.Data.Questionnaire.subject.get('phase'),
                tempSchedule,
                tempSchedulePhases,
                isActiveSchedulePhase = false,
                step                  = function (count) {
                    tempSchedule = _.find(LF.StudyDesign.schedules.models, function (sched) {
                        return sched.get('id') == schedulesToCheck[count];
                    });
                    tempSchedulePhases = tempSchedule.get('phase');
                    _.each(tempSchedulePhases, function (tempSchedulePhase) {
                        if(LF.StudyDesign.studyPhase[tempSchedulePhase] == phase){
                            isActiveSchedulePhase = true;
                            //break out of loop
                            return false;
                        }
                    });
                    if(isActiveSchedulePhase){
                        LF.Schedule.schedulingFunctions[tempSchedule.get('scheduleFunction')](tempSchedule, completedQ, null, null, function (diaryAvail) {
                            if(diaryAvail){
                                localStorage.setItem('AUTOLAUNCH_ID', tempSchedule.get('target').id);
                                return;
                            }
                            else if(count + 1 < length){
                                step(count + 1);
                            }
                            else{
                                return;
                            }
                        });
                    }
                    else{
                        if(count + 1 < length){
                            step(count + 1);
                        }
                        else{
                            return;
                        }
                    }
                };
            step(0);
        });
};
LF.Utilities.determineQSTWND1N = function (visit3Date, now) {
    let dateDiffInDays = LF.Utilities.dateDiffInDays(new Date(visit3Date), new Date(now), true),
        QSTWND1N;
    if(dateDiffInDays >= 11 && dateDiffInDays <= 17){
        QSTWND1N = '14';
    }
    else if(dateDiffInDays >= 23 && dateDiffInDays <= 33){
        QSTWND1N = '28';
    }
    else if(dateDiffInDays >= 37 && dateDiffInDays <= 47){
        QSTWND1N = '42';
    }
    else if(dateDiffInDays >= 51 && dateDiffInDays <= 61){
        QSTWND1N = '56';
    }
    else if(dateDiffInDays >= 65 && dateDiffInDays <= 75){
        QSTWND1N = '70';
    }
    else if(dateDiffInDays >= 79 && dateDiffInDays <= 89){
        QSTWND1N = '84';
    }
    else if(dateDiffInDays >= 93 && dateDiffInDays <= 103){
        QSTWND1N = '98';
    }
    else if(dateDiffInDays >= 107 && dateDiffInDays <= 117){
        QSTWND1N = '112';
    }
    else if(dateDiffInDays >= 121 && dateDiffInDays <= 131){
        QSTWND1N = '126';
    }
    else if(dateDiffInDays >= 135 && dateDiffInDays <= 145){
        QSTWND1N = '140';
    }
    else if(dateDiffInDays >= 149 && dateDiffInDays <= 159){
        QSTWND1N = '154';
    }
    else if(dateDiffInDays >= 163 && dateDiffInDays <= 173){
        QSTWND1N = '168';
    }
    else if(dateDiffInDays >= 177 && dateDiffInDays <= 187){
        QSTWND1N = '182';
    }
    else if(dateDiffInDays >= 191 && dateDiffInDays <= 201){
        QSTWND1N = '196';
    }
    else if(dateDiffInDays >= 205 && dateDiffInDays <= 215){
        QSTWND1N = '210';
    }
    else if(dateDiffInDays >= 219 && dateDiffInDays <= 229){
        QSTWND1N = '224';
    }
    else if(dateDiffInDays >= 233 && dateDiffInDays <= 243){
        QSTWND1N = '238';
    }
    else if(dateDiffInDays >= 247 && dateDiffInDays <= 257){
        QSTWND1N = '252';
    }
    else if(dateDiffInDays >= 261 && dateDiffInDays <= 271){
        QSTWND1N = '266';
    }
    else if(dateDiffInDays >= 275 && dateDiffInDays <= 285){
        QSTWND1N = '280';
    }
    else if(dateDiffInDays >= 289 && dateDiffInDays <= 299){
        QSTWND1N = '294';
    }
    else if(dateDiffInDays >= 303 && dateDiffInDays <= 313){
        QSTWND1N = '308';
    }
    else if(dateDiffInDays >= 317 && dateDiffInDays <= 327){
        QSTWND1N = '322';
    }
    else if(dateDiffInDays >= 331 && dateDiffInDays <= 341){
        QSTWND1N = '336';
    }
    else if(dateDiffInDays >= 345 && dateDiffInDays <= 355){
        QSTWND1N = '350';
    }
    return QSTWND1N;
};
LF.Utilities.isQuestionnaireExpectedAtVisit = function (questionnaire, visitNum) {
    let visitSchedule,
        questionnaireExpectedAtVisit = false,
        visitAvailList               = [],
        tmpVisitNum;
    //Pull the visit based availability schedule for a questionnaire
    visitSchedule = _.find(LF.StudyDesign.schedules.models, function (schedule) {
        return schedule.get('target').id == questionnaire && schedule.get('scheduleFunction') == 'visitBasedAvailability';
    });
    //Pull the schedules visit list
    if(visitSchedule){
        visitAvailList = visitSchedule.get('scheduleParams').visits;
    }
    //Check if passed in visit is in this questionnaire's visit list
    _.each(visitAvailList, function (visit) {
        tmpVisitNum = LF.StudyDesign.studyVisitList[visit.visitID];
        if(tmpVisitNum == visitNum){
            questionnaireExpectedAtVisit = true;
        }
    });
    return questionnaireExpectedAtVisit;
};
LF.Utilities.purgeOldCollectionData = function (collectionName, filterKey, numDays) {
    return Q.Promise((resolve) => {
        LF.Utilities.getCollection(collectionName)
            .then((collection) => {
                //Min data boundary is midnight x days ago
                let minDataBoundary = new Date(LF.Data.Questionnaire.data.dashboard.get('started'));
                minDataBoundary.setDate(minDataBoundary.getDate() - numDays);
                minDataBoundary.setHours(0, 0, 0, 0);
                let logger = new Logger('Utilities:purgeOldCollectionData'),
                    modelsToDestroy,
                    length,
                    model,
                    step   = function (count) {
                        model = modelsToDestroy[count];
                        if(model){
                            try{
                                model.destroy({
                                    onSuccess : function () {
                                        logger.info('Successfully destroyed old historical item');
                                        if(count + 1 < length){
                                            step(count + 1);
                                        }else{
                                            resolve();
                                        }
                                    },
                                    onError   : function () {
                                        logger.error('Error destroying old historical item');
                                        if(count + 1 < length){
                                            step(count + 1);
                                        }else{
                                            resolve();
                                        }
                                    }
                                });
                            }catch(ex){
                                logger.error('Exception removing old historical item: ' + ex);
                                resolve();
                            }
                        }else{
                            if(count + 1 < length){
                                step(count + 1);
                            }else{
                                resolve();
                            }
                        }
                    };
                modelsToDestroy = _.filter(collection.models, function (model) {
                    return (new Date(model.get(filterKey)).getTime() < minDataBoundary.getTime());
                });
                length = modelsToDestroy.length;
                if(length > 0){
                    step(0);
                }else{
                    resolve();
                }
            });
    });
};
LF.Utilities.clearCollection = function (collectionName) {
    return Q.Promise((resolve) => {
        let collection = new LF.Collection[collectionName]();
        try{
            //logger.info(`Clearing ${collectionName}`);
            collection.clear({
                onSuccess : function () {
                    //logger.info(`Successfully cleared ${collectionName}`);
                    resolve();
                },
                onError   : function () {
                    //logger.error(`Failed to clear ${collectionName}`);
                    resolve();
                }
            });
        }catch(e){
            //logger.error(`Exception clearing ${collectionName}: ${e}`);
            resolve();
        }
    });
};
LF.Utilities.sortSchedules = () => {
    let schedules = LF.StudyDesign.schedules.models;
    schedules.sort((schedule1, schedule2) => {
        return schedule1.get('salience') - schedule2.get('salience');
    });
    LF.StudyDesign.schedules.models = schedules;
};
/*Visit Functions*/
LF.Utilities.getCurrentVisit = function (callback) {
    LF.Utilities.getCollection('VisitCollection').then((visits) => {
        let lastVisit = _.last(visits);
        if(lastVisit){
            callback(lastVisit);
        }else{
            callback(null);
        }
    });
};
LF.Utilities.getCurrentVisitPromise = function () {
    return LF.Utilities.getCollection('VisitCollection').then((visits) => {
        let lastVisit = _.last(visits);
        if(lastVisit){
            return lastVisit;
        }else{
            return null;
        }
    });
};
LF.Utilities.getCurrentVisitDropDown = function () {
    return LF.Utilities.getCollection('VisitCollection').then((visits) => {
        let lastVisit = _.last(_.filter(visits, function (visit) {
            return visit.get('visit') !== LF.StudyDesign.visitList.visitIPWithdrawal;
        }));
        if(lastVisit){
            return lastVisit;
        }else{
            return null;
        }
    });
};
LF.Widget.DropDownList.prototype.renderOptions = function (divToAppendTo) {
    return Q()
        .then(() => {
            let itemTemplate   = this.model.get('itemTemplate'),
                items          = this.model.get('items'),
                filterFunction = this.model.get('filterFunction') || 'dropDownFilter',
                $targetDiv     = this.$(divToAppendTo),
                addItemPromiseFactories,
                itemTemplateFactory;
            itemTemplateFactory = LF.templates.getTemplateFromKey(itemTemplate);
            $targetDiv.empty();
            return LF.Utilities[filterFunction](items).then((itemsList) => {
                addItemPromiseFactories = itemsList.map((item) => {
                    return () => {
                        return LF.strings.display.call(LF.strings, item.text, () => null, {namespace : this.getQuestion().getQuestionnaire().id}).then((string) => {
                            item.localized = string;
                            let el = $(itemTemplateFactory(item));
                            $targetDiv.append(el);
                        });
                    };
                });
                return addItemPromiseFactories.reduce(Q.when, Q());
            });
        });
};
LF.Utilities.dropDownFilter = function (items) {
    return LF.Utilities.getCurrentVisitDropDown()
        .then((lastVisit) => {
                let visitList = LF.StudyDesign.visitList;
                if(!lastVisit){
                    /*return _.filter(items, function (item) {
                     return Number(item.value) === Number(visitList.week188);
                     });*/
                    return items;
                }else{
                    lastVisit = Number(lastVisit.get('visit'));
                    return _.filter(items, function (item) {
                        return (Number(item.value) > lastVisit);
                    });
                }
            }
        );
};
LF.Utilities.getNextVisit = function () {
    let items = [
        {
            value : 1,
            text  : 'WEEK_188'
        },
        {
            value : 2,
            text  : 'WEEK_192'
        },
        {
            value : 3,
            text  : 'WEEK_212'
        },
        {
            value : 4,
            text  : 'WEEK_216'
        },
        {
            value : 5,
            text  : 'WEEK_236'
        },
        {
            value : 6,
            text  : 'WEEK_240'
        },
        {
            value : 7,
            text  : 'WEEK_260'
        },
        {
            value : 8,
            text  : 'WEEK_264'
        },
        {
            value : 9,
            text  : 'WEEK_268'
        }
    ];
    return LF.Utilities.dropDownFilter(items)
        .then((lastVisit) => {
            return _.first(lastVisit);
        });
};
LF.Utilities.getOngoingHeadaches = () => {
    return LF.Utilities.getCollection('Headaches')
        .then((headaches) => {
            return _.filter(headaches, (headache) => {
                return !headache.get('endDate');
            });
        });
};
LF.Utilities.getHeadacheDuration = () => {
    return LF.Utilities.getOngoingHeadaches()
        .then(headaches => {
            let ig              = 'Headache',
                igr             = LF.Data.Questionnaire.getCurrentIGR(ig),
                startTime       = _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('HEDBEG1S', igr)),
                endTime         = _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('ENDHED1S', igr)),
                fellAsleep      = _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('FELSLP1S', igr)),
                ongoingHeadache = _.last(headaches);
            if(startTime){
                startTime = new Date(startTime.get('response')).getTime()
            }else{
                if(ongoingHeadache){
                    startTime = new Date(ongoingHeadache.get('startDate')).getTime();
                }else{
                    startTime = 0;
                }
            }
            endTime = (endTime || fellAsleep);
            endTime = endTime ? new Date(endTime.get('response')).getTime() : startTime;
            return {
                minutes : (endTime - startTime) / (1000 * 60),
                time    : [
                    Math.floor((endTime - startTime) / (1000 * 60 * 60)),
                    Math.floor((endTime - startTime) / (1000 * 60) - 60 * Math.floor((endTime - startTime) / (1000 * 60 * 60)))
                ]
            };
        });
};
LF.Utilities.calcTimeDuration = (startTime, endTime) => {
    return {
        minutes : (endTime - startTime) / (1000 * 60),
        time    : [
            Math.floor((endTime - startTime) / (1000 * 60 * 60)),
            Math.floor((endTime - startTime) / (1000 * 60) - 60 * Math.floor((endTime - startTime) / (1000 * 60 * 60)))
        ]
    };
};
LF.Utilities.getSleepDuration = () => {
    let ig        = 'Headache',
        igr       = LF.Data.Questionnaire.getCurrentIGR(ig),
        startTime = _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('FELSLP1S', igr)),
        endTime   = _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('TIMAWK1S', igr));
    startTime = startTime ? new Date(startTime.get('response')).getTime() : 0;
    endTime = endTime ? new Date(endTime.get('response')).getTime() : startTime;
    return {
        minutes : (endTime - startTime) / (1000 * 60),
        time    : [
            Math.floor((endTime - startTime) / (1000 * 60 * 60)),
            Math.floor((endTime - startTime) / (1000 * 60) - 60 * Math.floor((endTime - startTime) / (1000 * 60 * 60)))
        ]
    };
};
LF.Utilities.saveSleepDurations = () => {
    let ig  = 'Headache',
        igr = LF.Data.Questionnaire.getCurrentIGR(ig),
        startTime,
        endTime,
        durations;
    while(igr > 0){
        startTime = _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('FELSLP1S', igr));
        endTime = _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('TIMAWK1S', igr));
        startTime = startTime ? new Date(startTime.get('response')).getTime() : 0;
        endTime = endTime ? new Date(endTime.get('response')).getTime() : startTime;
        if(startTime && endTime){
            durations = LF.Utilities.calcTimeDuration(startTime, endTime);
            LF.Utilities.addIT({
                question_id      : 'SLPMIN1N',
                questionnaire_id : 'DailyDiary',
                response         : durations.minutes.toString(),
                IG               : 'Headache',
                IGR              : igr,
                IT               : 'SLPMIN1N'
            });
            LF.Utilities.addIT({
                question_id      : 'SLPRHR1N',
                questionnaire_id : 'DailyDiary',
                response         : durations.time[0].toString(),
                IG               : 'Headache',
                IGR              : igr,
                IT               : 'SLPRHR1N'
            });
            LF.Utilities.addIT({
                question_id      : 'SLPRMN1N',
                questionnaire_id : 'DailyDiary',
                response         : durations.time[1].toString(),
                IG               : 'Headache',
                IGR              : igr,
                IT               : 'SLPRMN1N'
            });
        }
        igr--;
    }
};
LF.Utilities.getSelectedMedicationInfo = (ig) => {
    let igr         = LF.Data.Questionnaire.getCurrentIGR(ig),
        selectedMed = _.last(LF.Data.Questionnaire.queryAnswersByIGITAndIGR(ig, 'MEDNAM1L', igr));
    if(selectedMed){
        selectedMed = selectedMed.get('response');
        if(selectedMed !== '10'){
            return LF.StudyDesign.medicationInfo[`medication${selectedMed}`];
        }else{
            return LF.StudyDesign.medicationInfo.Other;
        }
    }else{
        return {};
    }
};
LF.Utilities.getOverlappingHeadaches = () => {
    return LF.Utilities.getCollection('Headaches')
        .then((headaches) => {
            let ig                  = 'Headache',
                igr                 = LF.Data.Questionnaire.getCurrentIGR(ig),
                startDate           = _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('HEDBEG1S', igr)),
                endDate             = _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('ENDHED1S', igr)) || _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('FELSLP1S', igr)),
                ongoingHeadacheLoop = _.last(LF.Data.Questionnaire.queryAnswersByIT('HEDNOW1L')),
                currentStartDate,
                currentEndDate,
                ongoingHeadache     = _.find(headaches, headache => {
                    return !headache.get('endDate');
                }),
                overlapping         = false,
                tempModel;
            startDate = startDate ? new Date(startDate.get('response')).getTime() : ongoingHeadache ? new Date(ongoingHeadache.get('startDate')).getTime() : 0;
            endDate = endDate ? new Date(endDate.get('response')).getTime() : startDate;
            ongoingHeadacheLoop = ongoingHeadacheLoop ? ongoingHeadacheLoop.get('response') === '1' : false;
            while(igr > 1){
                igr--;
                currentStartDate = _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('HEDBEG1S', igr));
                currentEndDate = _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('ENDHED1S', igr)) || _.last(LF.Data.Questionnaire.queryAnswersByITAndIGR('FELSLP1S', igr));
                currentEndDate = currentEndDate ? currentEndDate.get('response') : '11 Sep 2001';
                if(!currentStartDate){
                    currentStartDate = _.find(headaches, (headache) => {
                        return !headache.get('endDate');
                    });
                    if(currentStartDate){
                        currentStartDate = currentStartDate.get('startDate');
                    }else{
                        currentStartDate = '';
                    }
                }else{
                    currentStartDate = currentStartDate ? currentStartDate.get('response') : '11 Sep 2001';
                }
                tempModel = new LF.Model.Headache({
                    startDate : currentStartDate,
                    endDate   : currentEndDate
                });
                headaches.push(tempModel);
            }
            headaches = _.filter(headaches, (headache) => {
                return !!headache.get('endDate') && headache.get('endDate') !== '1';
            });
            _.every(headaches, (headache) => {
                let thisStart = new Date(headache.get('startDate')).getTime(),
                    thisEnd   = new Date(headache.get('endDate')).getTime();
                if(ongoingHeadacheLoop && startDate < thisEnd){
                    overlapping = headache.attributes;
                    return false;
                }else if(!((startDate >= thisStart && startDate <= thisEnd)
                    || (endDate >= thisStart && endDate <= thisEnd)
                    || (startDate < thisStart && endDate > thisEnd))){
                    return true;
                }else{
                    overlapping = headache.attributes;
                    return false;
                }
            });
            return overlapping;
        });
};
LF.Utilities.getMedicationArray = (excludeCurrent, igToExclude) => {
    return LF.Utilities.getCollection('Medications')
        .then((medications) => {
            let aura           = 'AURA',
                headacheMed    = 'HeadacheMed',
                igrMed         = LF.Data.Questionnaire.getCurrentIGR(aura),
                igrHeadacheMed = LF.Data.Questionnaire.getCurrentIGR(headacheMed),
                tempModel,
                tempAnswer,
                tempTime;
            if(excludeCurrent){
                if(igToExclude === 'AURA'){
                    igrMed--;
                }else{
                    igrHeadacheMed--;
                }
            }
            while(igrMed > 0){
                tempAnswer = _.last(LF.Data.Questionnaire.queryAnswersByIGITAndIGR(aura, 'MEDNAM1L', igrMed));
                tempTime = _.last(LF.Data.Questionnaire.queryAnswersByIGITAndIGR(aura, 'TIMTAK1S', igrMed));
                if(tempAnswer && tempAnswer.get('response') !== '10' && tempTime){
                    tempModel = new LF.Model.Medication({
                        medicationID : LF.Utilities.getMedicationID(tempAnswer.get('response')),
                        dateTaken    : LF.Utilities.timeStamp(new Date(tempTime.get('response')))
                    });
                    medications.push(tempModel);
                }
                igrMed--;
            }
            while(igrHeadacheMed > 0){
                tempAnswer = _.last(LF.Data.Questionnaire.queryAnswersByIGITAndIGR(headacheMed, 'MEDNAM1L', igrHeadacheMed));
                tempTime = _.last(LF.Data.Questionnaire.queryAnswersByIGITAndIGR(headacheMed, 'TIMTAK1S', igrHeadacheMed));
                if(tempAnswer && tempAnswer.get('response') !== '10' && tempTime){
                    tempModel = new LF.Model.Medication({
                        medicationID : LF.Utilities.getMedicationID(tempAnswer.get('response')),
                        dateTaken    : LF.Utilities.timeStamp(new Date(tempTime.get('response')))
                    });
                    medications.push(tempModel);
                }
                igrHeadacheMed--;
            }
            return medications;
        });
};
LF.Utilities.getMedicationID = (index) => {
    if(index !== '10'){
        return LF.StudyDesign.medicationInfo[`medication${index}`].code;
    }else{
        return LF.StudyDesign.medicationInfo.Other.code;
    }
};
LF.Utilities.getOverlappingMedication = (ig) => {
    return LF.Utilities.getMedicationArray(true, ig)
        .then((medications) => {
            let igr          = LF.Data.Questionnaire.getCurrentIGR(ig),
                medicationID = _.last(LF.Data.Questionnaire.queryAnswersByIGITAndIGR(ig, 'MEDNAM1L', igr)),
                dateTaken    = _.last(LF.Data.Questionnaire.queryAnswersByIGITAndIGR(ig, 'TIMTAK1S', igr)),
                minuteDiff;
            if(medicationID && dateTaken){
                medicationID = LF.Utilities.getMedicationID(medicationID.get('response'));
                dateTaken = new Date(dateTaken.get('response')).getTime();
                medications = _.find(medications, (medication) => {
                    minuteDiff = Math.abs((new Date(medication.get('dateTaken')).getTime() - dateTaken) / (1000 * 60));
                    return medication.get('medicationID') === medicationID && minuteDiff <= 15;
                });
                return medications.attributes;
            }else{
                return null;
            }
        });
};
LF.Utilities.getString = function (stringID, namespace) {

    //if namespace is not defined
    if(typeof namespace === 'undefined'){
        namespace = 'STUDY';
    }
    let params  = {
            namespace : namespace
        },
        results = LF.strings.find(params),
        result  = !!results && results.get('resources')[stringID];
    if(result){
        return result;
    }else{
        return stringID;
    }
};
Date.prototype.YESTERDAY = (time) => {
    let timeArray;
    if(time){
        timeArray = time.split(':');
        return new Date(new Date(new Date().setDate(new Date().getDate() - 1)).setHours(timeArray[0] || 0, timeArray[1] || 0, timeArray[2] || 0, timeArray[3] || 0));
    }else{
        return new Date(new Date().setDate(new Date().getDate() - 1));
    }
};
Date.prototype.TOMORROW = () => {
    let timeArray;
    if(time){
        timeArray = time.split(':');
        return new Date(new Date(new Date().setDate(new Date().getDate() + 1)).setHours(timeArray[0] || 0, timeArray[1] || 0, timeArray[2] || 0, timeArray[3] || 0));
    }else{
        return new Date(new Date().setDate(new Date().getDate() + 1));
    }
};
LF.Utilities.checkFakeITs = () => {
    //returns true when all fake ITs are gone
    let answers        = LF.Data.Questionnaire.data.answers.models,
        regex          = new RegExp('.M[0-9]{2,3}IT$'),
        stillRemaining = true;
    return _.every(answers, answer => {
        if(answer && answer.get('SW_Alias').match(regex)){
            stillRemaining = false;
        }
        return stillRemaining;
    });
};
LF.Utilities.removeFakeITs = () => {
    let answers     = LF.Data.Questionnaire.data.answers.models,
        regex       = new RegExp('.M[0-9]{2,3}IT$'),
        deleteArray = [];
    _.each(answers, answer => {
        if(answer && answer.get('SW_Alias').match(regex)){
            deleteArray.push(answer);
        }
    });
    _.each(deleteArray, answer => {
        LF.Data.Questionnaire.data.answers.remove(answer);
    });
};
LF.Utilities.updateMedNames = () => {
    let initialID        = (new Date()).getTime(),
        auraMedCount     = LF.Utilities.addMedNameITs('DailyDiary_9117_Q0', 'AURA'),
        headacheMedCount = LF.Utilities.addMedNameITs('DD402_Q0', 'HeadacheMed');
    for(let i = 0; i < auraMedCount; i++){
        console.log(`Adding Med ID - ${(initialID + i).toString()}`);
        LF.Utilities.addIT({
            question_id      : 'MED070',
            questionnaire_id : 'DailyDiary',
            IG               : 'AURA',
            IGR              : i + 1,
            IT               : 'MED_ID1N',
            response         : (initialID + i).toString()
        });
    }
    for(let i = 0; i < headacheMedCount; i++){
        console.log(`Adding Med ID - ${(initialID + i).toString()}`);
        LF.Utilities.addIT({
            question_id      : 'MED070',
            questionnaire_id : 'DailyDiary',
            IG               : 'HeadacheMed',
            IGR              : i + 1,
            IT               : 'MED_ID1N',
            response         : (initialID + auraMedCount + i).toString()
        });
    }
};
LF.Utilities.addMedNameITs = (questionID, IG) => {
    let MEDCOD1C,
        currentIGR = LF.Data.Questionnaire.getCurrentIGR(IG),
        totalCount = 0;
    for(let i = 1; i <= currentIGR; i++){
        MEDCOD1C = LF.Data.Questionnaire.queryAnswersByIGITAndIGR(IG, 'MEDNAM1L', i);
        if(_.last(MEDCOD1C)){
            MEDCOD1C = Number(_.last(MEDCOD1C).get('response'));
            if(MEDCOD1C === 10){
                LF.Utilities.addIT({
                    question_id      : 'OTHTRT1B',
                    questionnaire_id : 'DailyDiary',
                    IG               : IG,
                    IGR              : i,
                    IT               : 'OTHTRT1B',
                    response         : true
                });
                LF.Utilities.addIT({
                    question_id      : questionID,
                    questionnaire_id : 'DailyDiary',
                    IG               : IG,
                    IGR              : i,
                    IT               : 'MEDNAM1L',
                    response         : 'OTH99999'
                });
            }else{
                LF.Utilities.addIT({
                    question_id      : questionID,
                    questionnaire_id : 'DailyDiary',
                    IG               : IG,
                    IGR              : i,
                    IT               : 'MEDNAM1L',
                    response         : LF.StudyDesign.medicationInfo[`medication${MEDCOD1C}`].code
                });
            }
            totalCount++;
        }
    }
    return totalCount;
};
LF.Utilities.medDynamicText = (medNumber) => {
    return LF.Data.Questionnaire.medDynamicText[`medication${medNumber}`];
};
LF.Utilities.setDynamicText = () => {
    let stringsToFetch = {
        //addStrings to Fetch
        ITEM_FORMAT   : 'dynamicTextFormat',
        OtherMed      : 'OtherMed',
        methodRoute   : [
            '',
            'INJECTABLE',
            'NASAL_SPARY',
            'ORAL',
            'ORAL_INHALED',
            'ORAL_LIQUID',
            'SUPPOSITORY',
            'TRANSDERMAL'
        ],
        GRANULATES    : 'GRANULATES',
        MLS_INJ       : 'MLS_INJ',
        MLS_ORAL      : 'MLS_ORAL',
        PATCHES       : 'PATCHES',
        PILLS         : 'PILLS',
        POWDER        : 'POWDER',
        SPRAYS        : 'SPRAYS',
        SUPPOSITORIES : 'SUPPOSITORIES',
        INHALATIONS   : 'INHALATIONS'
    };
    LF.Data.Questionnaire.medDynamicText = {};
    return LF.getStrings(stringsToFetch)
        .then((resources) => {
            for(let i = 1; i < 10; i++){
                LF.Data.Questionnaire.medDynamicText[`medication${i}`] = (resources.ITEM_FORMAT).format(
                    LF.StudyDesign.medicationInfo[`medication${i}`].code,
                    LF.StudyDesign.medicationInfo[`medication${i}`].name,
                    LF.StudyDesign.medicationInfo[`medication${i}`].strength,
                    resources.methodRoute[LF.StudyDesign.medicationInfo[`medication${i}`].methodRoute],
                    resources[LF.StudyDesign.medicationInfo[`medication${i}`].unitsSingle]
                );
            }
        });
};
LF.Utilities.getLastHeadacheNumber = () => {
    return LF.Utilities.getCollection('Headaches')
        .then((headaches) => {
            let lastHeadache = headaches.length > 0 ? _.max(headaches, (headache) => {
                    return Number(headache.get('headacheID'));
                }) : null;
            return lastHeadache ? lastHeadache.get('headacheID') : '-1';
        });
};
LF.Utilities.saveHeacheNumberToMedLoop = (lastHeadacheNumber) => {
    let medStartIGR = LF.Data.Questionnaire.medStartIndex[0] + 1,
        medEndIGR   = LF.Data.Questionnaire.medStartIndex[1] || LF.Data.Questionnaire.getCurrentIGR('HeadacheMed'),
        igrCount    = medStartIGR;
    //LF.Data.Questionnaire.medStartIndex;
    while(igrCount <= medEndIGR){
        LF.Utilities.addIT({
            question_id      : 'HEDNUM1N',
            questionnaire_id : 'DailyDiary',
            response         : lastHeadacheNumber.toString(),
            IG               : 'HeadacheMed',
            IGR              : igrCount,
            IT               : 'HEDNUM1N'
        });
        igrCount++;
    }
    if((medEndIGR - medStartIGR + 1) >= 5){
        LF.Utilities.addIT({
            question_id      : 'HMDMAX1B',
            questionnaire_id : 'DailyDiary',
            response         : '1',
            IG               : 'HeadacheMed',
            IGR              : medEndIGR,
            IT               : 'HMDMAX1B'
        });
    }
    LF.Data.Questionnaire.medStartIndex.splice(0, 1);
};
LF.Utilities.queryAnswersByITandIG = (ig, it) => {
    let swAlias,
        answersQuery      = [],
        answersCollection = LF.Data.Questionnaire.answers;
    answersCollection.each((model) => {
        swAlias = model.get('SW_Alias').split('.');
        if(swAlias[0] === ig && swAlias[2] === it){
            answersQuery.push(model);
        }
    });
    return answersQuery;
};
LF.Utilities.createMedIndexArray = () => {
    let noMoreMeds = LF.Utilities.queryAnswersByITandIG('HeadacheMed', 'M39IT'),
        medArray   = [0],
        loopCount  = 0,
        currentIndex;
    _.each(noMoreMeds, (answer) => {
        loopCount++;
        if(answer && answer.get('response') === '0' || loopCount >= 5){
            currentIndex = Number(answer.get('SW_Alias').split('.')[1]);
            if(medArray.indexOf(currentIndex) === -1 || currentIndex !== 1){
                medArray.push(currentIndex);
                loopCount = 0;
            }
        }
    });
    return medArray;
};
LF.Utilities.resizeScreen = (done) => {
    console.time('ResizeScreen');
    let logger               = new Logger('resizeScreen'),
        totalHeight          = $(window).height(),
        headerHeight         = Math.max($('header').height(), $('.navbar').height()),
        footerHeight         = $('footer').height(),
        usableHeight         = totalHeight - headerHeight - footerHeight,
        questionnaireSection = $('#questionnaire section'),
        labelText            = $('label'),
        questionnaireHeight  = questionnaireSection.height() + 30,// for stupid fucking padding
        questionText         = questionnaireSection.find('.text'),
        fontDefaultSize      = questionText[0] ? Number(questionText[0].style.fontSize.substr(0, questionText[0].style.fontSize.indexOf('px')) || 16) : 0,
        checkBoxDefaultSize  = labelText[0] ? Number(labelText[0].style.fontSize.substr(0, labelText[0].style.fontSize.indexOf('px')) || 14) : 0,
        nrsTextContainer     = $('.marker.marker-right .marker-text'),
        nrsText              = nrsTextContainer.children(),
        containerWidth       = nrsTextContainer.width(),
        textWidth            = nrsText.length > 0 ? nrsText.width() : nrsTextContainer.length > 0 ? nrsTextContainer[0].scrollWidth : 0,
        widthDiff            = containerWidth - textWidth,
        maxButtonHeight      = 0;
    if(widthDiff < 0){
        //fixes right label on nrs screens
        nrsTextContainer.css('margin-left', `${widthDiff}px`);
    }
    //Review Screen adjustment should be handled in Review Screen Widget render function
    if(fontDefaultSize > 12 && questionnaireHeight > usableHeight && questionnaireHeight < (usableHeight * 1.5)){
        checkBoxDefaultSize -= 0.25;
        fontDefaultSize -= 0.25;
        questionText[0].style.fontSize = Math.max((fontDefaultSize), 12) + 'px';
        _.each(labelText, checkbox => {
            checkbox.style.fontSize = Math.max((checkBoxDefaultSize), 10) + 'px';
        });
        _.each($('label'), function (element) {
            maxButtonHeight = Math.max($(element).height(), maxButtonHeight);
        });
        _.each($('div.btn.btn-default.btn-block'), function (element) {
            $(element).height(maxButtonHeight + 20);
        });
        logger.info('rerunningChanges');
        LF.Utilities.resizeScreen(done);
    }else{
        logger.info('ScreenFits');
        console.timeEnd('ResizeScreen');
        (done || $.noop)();
    }
};
LF.Widget.LanguageSelectWidget.prototype.renderOptions = function (divToAppendTo) {
    //@todo change languages to be only those configured for a role. Part of US6150
    let languages      = LF.strings.getLanguages(),
        answer         = (this.answer) ? JSON.parse(this.answer.get('response'))[this.model.get('field')] : false,
        template       = _.template('<option id="{{ language }}-{{ locale }}" value="{{ language }}-{{ locale }}">{{ localized }}</option>'),
        currentRole    = LF.security.activeUser,
        roleAttributes = _.find(LF.StudyDesign.roles.models, (role) => {
            return role.get('id') === currentRole.get('role');
        }),
        allowedLanguages,
        selectedRole,
        selectedRoleAttributes,
        defaultLang;
    defaultLang = roleAttributes ? roleAttributes.get('defaultLang') || 'en-US' : 'en-US';
    $(divToAppendTo).empty();
    this.needToRespond = {value : `${languages[0].language}-${languages[0].locale}`};
    let addItemPromiseFactories = languages.map((resource) => {
        return () => {
            resource.localized = (`${resource.language}-${resource.locale}`).toLocaleUpperCase();
            return LF.strings.display.call(LF.strings, resource.localized)
                .then((string) => {
                    selectedRole = $('#ADD_USER_W_2').val();
                    selectedRoleAttributes = _.find(LF.StudyDesign.roles.models, (role) => {
                        return (role.get('id') === selectedRole);
                    });
                    allowedLanguages = selectedRoleAttributes ? selectedRoleAttributes.get('supportedLanguages') : null;
                    if(!allowedLanguages || allowedLanguages.indexOf('ALL') !== -1 || allowedLanguages.indexOf(`${resource.language}-${resource.locale}`) !== -1){
                        resource.localized = string;
                        let el = template(resource);
                        if(answer && (allowedLanguages.indexOf(answer) !== -1 || allowedLanguages.indexOf('ALL') !== -1)){
                            if(answer === (`${resource.language}-${resource.locale}`)){
                                el = $(el).attr('selected', 'selected');
                            }
                        }else if(defaultLang && defaultLang === (`${resource.language}-${resource.locale}`)){
                            this.respond({value : `${resource.language}-${resource.locale}`});
                            el = $(el).attr('selected', 'selected');
                        }
                        this.$(divToAppendTo).append(el);
                    }
                });
        };
    });
    return addItemPromiseFactories.reduce(Q.when, Q());
};
LoginView.prototype.createLanguageSelectionData = function () {
    let selectedRoleSupportedLanguages,
        localized = LF.strings.match({namespace : 'CORE', localized : {}}),
        langs     = [],
        roleAttributes;
    if(this.user){
        roleAttributes = _.find(LF.StudyDesign.roles.models, (role) => {
            return role.get('id') === this.user.get('role');
        });
        selectedRoleSupportedLanguages = roleAttributes.get('supportedLanguages') || 'ALL';
    }
    // get the localized names from the resource string files in order of localized language name
    localized.sort((firstComparatorObject, secondComparatorObject) => {
        let firstComparator  = firstComparatorObject.get('localized'),
            secondComparator = secondComparatorObject.get('localized');
        return (firstComparator > secondComparator) ? 1 : (firstComparator < secondComparator) ? -1 : 0;
    }).forEach((language) => {
        let langCode = `${language.get('language')}-${language.get('locale')}`,
            lang     = {
                id         : langCode,
                localized  : language.get('localized'),
                fontFamily : Utilities.getFontFamily(langCode),
                dir        : language.get('direction'),
                cssClass   : language.get('direction') === 'rtl' ? 'right-direction text-right-absolute' : 'left-direction text-left-absolute',
                selected   : langCode === this.user.get('language')
            };
        if(selectedRoleSupportedLanguages.toString() === 'ALL'){
            langs.push(lang);
        }else if(_.contains(selectedRoleSupportedLanguages, langCode)){
            langs.push(lang);
            selectedRoleSupportedLanguages = _.without(selectedRoleSupportedLanguages, langCode);
        }
    });
    return langs;
};
