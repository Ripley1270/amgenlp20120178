export default [
    {
        name      : 'ReviewScreen',
        namespace : 'TwoColumns',
        template  : '<li data-role="list-divider" role="heading" class="ui-li ui-li-divider reveiwScreen_Headers"><div class="ui-grid-a" style="display:flex; flex-direction: row;">' +
        '<div class="ui-block-a" style="width: 50%;">{{header0}}</div>' +
        '<div class="ui-block-b" style="width: 50%;">{{header1}}</div>' +
        '</div></li>' +
        '<ul id="{{ id }}_Items" class="reviewScreen_Items" data-role="listview">' +
        '</ul>' +
        '<hr />' +
        '<ul id="{{ id }}_Buttons" class="reviewScreen_Buttons" data-role="listview"></ul>' +
        '<p class="edit-instructions">{{editInstructions}}</p>'
    },
    {
        name      : 'ScreenItems',
        namespace : 'TwoColumns',
        template  : '<li><div class="ui-grid-a">' +
        '<div class="ui-block-a">{{ text0 }}</div>' +
        '<div class="ui-block-b">{{ text1 }}</div>' +
        '</div></li>'
    },
    {
        name      : 'ReviewScreen',
        namespace : 'ThreeColumns',
        template  : '<li data-role="list-divider" role="heading" class="ui-li ui-li-divider reveiwScreen_Headers"><div class="ui-grid-a" style="display:flex; flex-direction: row;">' +
        '<div class="ui-block-a" style="width: 33%;">{{header0}}</div>' +
        '<div class="ui-block-b" style="width: 33%;">{{header1}}</div>' +
        '<div class="ui-block-c" style="width: 33%;">{{header1}}</div>' +
        '</div></li>' +
        '<ul id="{{ id }}_Items" class="reviewScreen_Items" data-role="listview">' +
        '</ul>' +
        '<hr />' +
        '<ul id="{{ id }}_Buttons" class="reviewScreen_Buttons" data-role="listview"></ul>' +
        '<p class="edit-instructions">{{editInstructions}}</p>'
    },
    {
        name      : 'ScreenItems',
        namespace : 'ThreeColumns',
        template  : '<li><div class="ui-grid-a" style="display:flex; flex-direction: row;">' +
        '<div class="ui-block-a" style="width: 33%;">{{ text0 }}</div>' +
        '<div class="ui-block-b" style="width: 33%;">{{ text1 }}</div>' +
        '<div class="ui-block-c" style="width: 33%;">{{ text2 }}</div>' +
        '</div></li>'
    },
    {
        name      : 'NRSMarkers',
        namespace : 'AMGEN',
        template  : '<div class="markers row">' +
        '<div class="ui-block-a" style="width: 10%;"><p>{{ left }}</p></div>' +
        '<div class="ui-block-a" style="width: 25%;"><p>{{ leftMiddle }}</p></div>' +
        '<div class="ui-block-a" style="width: 30%;"><p>{{ middle }}</p></div>' +
        '<div class="ui-block-a" style="width: 25%;"><p>{{ rightMiddle }}</p></div>' +
        '<div class="ui-block-a" style="width: 10%;"><p>{{ right }}</p></div>' +
        '</div>' +
        '<img style="width: 97%" src=\"{{nrsMarkersImage}}\"/>'
    }
];
