/* // CORE FILES
 import dbUpgrade from './db-upgrade';
 import StudyController from './study-controller';
 import studyRoutes from './study-routes';
 import studyWebService from './study-web-service';
 // END CORE FILES */
import './PDETools';
import studyRules from './study-rules';
import dbUpgrade from './db-upgrade';
import studyDesign from './study-design';
import loggingConfig from './logging-config';
import studyTemplates from './study-templates';
import studyMessages from './study-messages';
import './MedModuleUtil';
import {mergeArrays} from 'core/utilities/languageExtensions';
studyTemplates = mergeArrays(studyTemplates);
export default {
    /* // CORE FILES
     StudyController, studyRoutes, studyWebService,
     // END CORE FILES */

    dbUpgrade, studyRules, studyDesign, loggingConfig, studyTemplates, studyMessages
};
