export default (rules) => {
    rules.add({
        id       : 'AdjustTextSize',
        trigger  : [
            'QUESTIONNAIRE:Displayed'
        ],
        salience : 999,
        evaluate (filter, resume) {
            resume(!localStorage.getItem('ScreenshotMode'));
        },
        resolve  : [
            {
                action : 'displayMessage',
                data   : 'PLEASE_WAIT'
            }, {
                action (input, done) {
                    // ONEECOA#77038 -> Updated Screen Logic to adjust label to fit on screen
                    LF.Utilities.resizeScreen(done);
                }
            }, {
                action : 'removeMessage'
            }
        ]
    });
    /*rules.add({
     id       : 'fixNRSLabels',
     trigger  : [
     'QUESTIONNAIRE:Displayed/MIDAS/MIDAS_9317'
     ],
     evaluate : true,
     resolve  : [
     {
     action () {

     let nrsTextContainer = $('.marker.marker-right .marker-text'),
     nrsText          = nrsTextContainer.children(),
     containerWidth   = nrsTextContainer.width(),
     widthDiff        = containerWidth - nrsText.width();
     if(widthDiff < 0){
     nrsTextContainer.css('margin-left', `${widthDiff}px`);
     }
     }
     }
     ]
     });*/
    //Begin MedModule Rules
    rules.add({
        id         : 'SubjectMasterMedSyncDataReceived',
        trigger    : [
            'CUSTOMDATASYNC:Received/getSubjMasterMedListSyncData'
        ],
        expression : true,
        actionData : [
            {
                trueAction    : function (filter, callback) {
                    LF.Utilities.clearCollection('SubjectMeds')
                        .then(() => LF.Utilities.clearCollection('MasterMeds')
                            .then(() => LF.MedModuleUtil.updateSubjectMasterMeds(filter.res, callback)));
                },
                trueArguments : function (filter, callback) {
                    callback(filter);
                }
            }
        ]
    });
    rules.add({
        id         : 'LoadDefaultMasterMedList',
        trigger    : [
            'APPLICATION:Loaded'
        ],
        expression : function (filter, resume) {
            resume((LF.Utilities.isTrainer() || localStorage.getItem('ScreenshotMode')) &&
                localStorage.getItem('MASTER_MED_LIST_DEFAULT_LOADED') !== '1');
        },
        actionData : [
            {
                trueAction    : 'displayMessage',
                trueArguments : 'PLEASE_WAIT'
            },
            {
                trueAction : function (filter, callback) {
                    LF.MedModuleUtil.saveMasterMed(LF.DEFAULT_MASTER_MED_LIST, function () {
                        localStorage.setItem('MASTER_MED_LIST_DEFAULT_LOADED', '1');
                        callback();
                    });
                }
            },
            {
                trueAction : 'removeMessage'
            }
        ]
    });
    rules.add({
        id         : 'LoadDefaultSubjectMedList',
        trigger    : [
            'APPLICATION:Loaded'
        ],
        expression : function (filter, resume) {
            resume((LF.Utilities.isTrainer() || localStorage.getItem('ScreenshotMode')) &&
                localStorage.getItem('SUBJECT_MED_LIST_DEFAULT_LOADED') !== '1');
        },
        actionData : [
            {
                trueAction : function (filter, callback) {
                    LF.MedModuleUtil.loadDefaultSubjectMedList(callback);
                }
            }
        ]
    });
    //End MedModule Rules
};
