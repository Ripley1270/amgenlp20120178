import ELF from 'core/ELF';
import Logger from 'core/Logger';
import StudyDesign from 'trial/assets/study-design/';
// Add rules, actions, and expressions here...
(function (rules) {

    // ONEECOA-144014: Override the PDE Common ManualFullSync because
    // it was bad.
    ELF.rules.remove('ManualFullSync');
    rules.add({
        id       : 'ManualFullSync',
        trigger  : [
            'TOOLBOX:Transmit'
        ],
        evaluate : [
            'AND',
            'isOnline',
            'isStudyOnline',
            { expression: 'setNeedToSyncIfPending', input: 'getFullSyncData' }],
        resolve  : [
            {
                action : 'displayMessage',
                data   : 'PLEASE_WAIT'
            }, {
                action : 'customSyncDataRequest',
                data   : {
                    webServiceFunction : 'getFullSyncData',
                    activation         : false
                }
            },
            {
                action : 'customSyncDataRequest',
                data   : {
                    webServiceFunction : 'getSubjMasterMedListSyncData',
                    activation         : false
                }
            }, {
                action : 'removeMessage'
            }
        ]
    });

    // ONEECOA-144014: Remove ForcedPartialSync because it was bad.
    // and we are not using Partial Sync.
    ELF.rules.remove('ForcedPartialSync');
    // ONEECOA-144014: Re-write PDE Common ForcedFullSync to use the
    // checkNeedToSync in conjunction with setNeedToSync
    ELF.rules.remove('ForcedFullSync');
    rules.add({
        id         : 'ForcedFullSync',
        trigger    : [
            'DASHBOARD:Open'
        ],
        evaluate   : [
            'AND',
            'isOnline',
            'isStudyOnline',
            { expression: 'checkNeedToSync', input: 'getFullSyncData' }],
        actionData : [
            {
                trueAction    : 'displayMessage',
                trueArguments : 'PLEASE_WAIT'
            }, {
                trueAction    : 'customSyncDataRequest',
                trueArguments : {
                    webServiceFunction : 'getFullSyncData',
                    activation         : false
                }
            },
            {
                trueAction    : 'customSyncDataRequest',
                trueArguments : {
                    webServiceFunction : 'getSubjMasterMedListSyncData',
                    activation         : false
                }
            },
            {
                trueAction : 'removeMessage'
            }
        ]
    });
}(ELF.rules));
