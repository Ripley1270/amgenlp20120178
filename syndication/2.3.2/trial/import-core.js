var fs = require('fs-extra');
var p = require('path');
var debugNoChange = false;
var coreDir = 'app/core/';
var coreModulePattern = 'core/';
var relCoreDir = '../app/core/';
var trialDir = 'trial/';
// Make it so this works even if the current directory isn't trial when run.
process.chdir(__dirname);
function fixRelativePath (filePath, rootPath, indexPath) {
    return filePath.replace('./', indexPath + '/').replace('./', rootPath);
}
function resolveFilePath (filePath, sourceDir, actualDir, indexPath) {
    let retFilePath = fixRelativePath(filePath, sourceDir, indexPath).replace(sourceDir, actualDir);
    console.log('retfilePath = ' + retFilePath);
    return require.resolve(retFilePath).replace(/\\/g, '/');
}
function importCoreFile (filePath, indexPath) {
    // If include has core hardcoded and is not relative, then leave the include where it is and don't import.
    if(filePath.includes(coreModulePattern)){
        return;
    }
    let coreFile  = resolveFilePath(filePath, trialDir, relCoreDir, indexPath),
        trialFile = coreFile.replace(coreDir, trialDir);
    console.log('importCoreFile:  copy "' + coreFile + '" to "' + trialFile + '"');
    if(!debugNoChange){
        fs.ensureFileSync(trialFile);
        fs.copySync(coreFile, trialFile);
    }
}
function processTrialFile (filePath) {
    let content                   = fs.readFileSync(filePath, 'utf-8'),
        importBlockRegex          = /\/\/ CORE FILES([\s\S]*?)\/\/ END CORE FILES/mg,
        commentedImportBlockRegex = /\/\* \/\/ CORE FILES([\s\S]*?)\/\/ END CORE FILES \*\//mg,
        commentedCodeBlockRegex   = /\/\* \/\/ CORE CODE([\s\S]*?)\/\/ END CORE CODE \*\//mg,
        importRegex               = /import[^']*'([^']+)';/mg,
        importBlockMatches,
        importMatches,
        contentChanged            = false,
        imports                   = [];
    let fnNewImportBlock = function (a, b) {
        contentChanged = true;
        console.log('Found and replaced import block contents for "' + filePath + '"');
        return '// CORE FILES' + b + '// END CORE FILES';
    };
    let fnNewCodeBlock = function (a, b) {
        contentChanged = true;
        console.log('Found and replaced code block contents for "' + filePath + '"');
        return '// CORE CODE' + b + '// END CORE CODE';
    };
    let newContent = content.replace(commentedImportBlockRegex, fnNewImportBlock);
    newContent = newContent.replace(commentedCodeBlockRegex, fnNewCodeBlock);
    while(importBlockMatches = importBlockRegex.exec(newContent)){
        while(importMatches = importRegex.exec(importBlockMatches[0])){
            imports.push(importMatches[1]);
        }
    }
    if(contentChanged && !debugNoChange){
        fs.writeFileSync(filePath, newContent, 'utf-8');
    }
    for(let i = 0; i < imports.length; ++i){
        console.log('Attempting to import ' + imports[i]);
        importCoreFile(imports[i], p.dirname(filePath));
    }
}
processTrialFile('./index.js');
processTrialFile('./schedule/index.js');
processTrialFile('./alarm/index.js');
processTrialFile('./assets/index.js');
processTrialFile('./assets/study-design/index.js');
processTrialFile('./assets/study-design/diaries/index.js');
processTrialFile('./branching/index.js');
processTrialFile('./dynamicText/index.js');
processTrialFile('./schedule/index.js');
processTrialFile('./widgets/index.js');
processTrialFile('./widgets/param-functions/index.js');
processTrialFile('./widgets/review-screen/index.js');
processTrialFile('./widgets/validation-functions/index.js');
processTrialFile('./assets/study-rules.js');
