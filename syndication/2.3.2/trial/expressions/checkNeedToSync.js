import ELF from 'core/ELF';
import Logger from 'core/Logger';
import { isTrainer } from '../../PDECommon/utils/commonUtils';
import { Utilities } from 'core/utilities/coreUtilities';

/**
 * @memberOf ELF.expressions
 * @method  checkNeedToSync
 * @description
 * Checks local storage to see if the "Need To Sync" flag is set for the given webservice function
 * @param {string} webServiceFunction the name of the webservice function we are checking
 * @returns {PromiseLike} Resolves True if the given webservice function has a need to sync flag set and is able to perform a sync
 * @example
 * evaluate : {
 *   expression : 'checkNeedToSync',
 *   input  : 'getFullSyncData'
 * }
 */
export default function checkNeedToSync (webServiceFunction) {
    let logger = new Logger(`checkNeedToSync:${webServiceFunction}`);

    if (localStorage.getItem(`NeedToSync_${webServiceFunction}`)) {
        return Q().then(() => {
            // first, check if in trainer mode
            if (isTrainer()) {
                return Q.reject({ logtype: 'info', message: 'Trainer mode, no syncing necessary.' });
            }

            // next, check that we are actually online
            return Utilities.isOnline();
        }).then((isOnline) => {
            if (!isOnline) {
                return Q.reject({ logtype: 'warn', message: 'Device is not online, unable to sync.' });
            }

            // if a full sync, check for pending transmissions
            if (webServiceFunction === 'getFullSyncData') {
                return LF.dataAccess.count('Transmission').then((count) => {
                    // resume true action if no pending reports
                    if (count === 0) {
                        return true;
                    }
                    return Q.reject({ logtype: 'warn', message: 'Sync delayed because of stored reports' });
                });
            }
            return true;
        }).catch((rejectObj) => {
            if (rejectObj && rejectObj.logtype && rejectObj.message) {
                logger[rejectObj.logtype](rejectObj.message);
                return false;
            }
            logger.error('Unhandled rejection occured, resolving false.', rejectObj);
            return false;
        });
    }

    // no flag set, no forced sync necessary
    return Q(false);
}

ELF.expression('checkNeedToSync', checkNeedToSync);
