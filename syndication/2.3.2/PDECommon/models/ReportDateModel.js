import Base from '../../app/core/models/StorageBase';
export default class ReportDate extends Base {
    /**
     * @property {string} name - The model's reference name
     * @static
     * @readonly
     * @return {string}
     */
    get name () {
        return 'ReportDate';
    }

    /**
     * @property {Object} schema - The model's schema used for validation and storage construction.
     * @property {Object} schema.* - object containg details on item being stored, key named after item being stored
     * @property {Object} schema.*.type - the type for the item being stored, should virtually always be a String
     * @property {boolean} schema.*.required - flags whether a model will save without the item being assigned a value.
     *     defaults to false
     * @property {boolean} schema.*.encrypt - flags whether the item's value will be encrypted when saved. defaults to
     *     false
     * @static
     * @readonly
     */
    get schema () {
        return {
            reportID        : {
                type     : String,
                required : false
            },
            reportDate      : {
                type     : String,
                required : false,
                encrypt  : true
            },
            reportStartDate : {
                type     : String,
                required : false
            }
        };
    }
}
window.LF.Model.ReportDate = ReportDate;
