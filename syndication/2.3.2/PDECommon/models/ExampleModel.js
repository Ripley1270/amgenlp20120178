import Base from '../../app/core/models/StorageBase';
export default class ExampleModel extends Base {
    /**
     * @property {string} name - The model's reference name
     * @static
     * @readonly
     * @return {string}
     */
    static get name () {
        return 'ExampleModel';
    }

    /**
     * @property {Object} schema - The model's schema used for validation and storage construction.
     * @property {Object} schema.* - object containg details on item being stored, key named after item being stored
     * @property {Object} schema.*.type - the type for the item being stored, should virtually always be a String
     * @property {boolean} schema.*.required - flags whether a model will save without the item being assigned a value.
     *     defaults to false
     * @property {boolean} schema.*.encrypt - flags whether the item's value will be encrypted when saved. defaults to
     *     false
     * @static
     * @readonly
     */
    static get schema () {
        return {
            dateTime  : {
                type     : String,
                required : false
            },
            response1 : {
                type     : String,
                required : false,
                encrypt  : true
            },
            response2 : {
                type     : String,
                required : false
            },
            response3 : {
                type     : String,
                required : false
            },
            response4 : {
                type     : String,
                required : false
            },
            response5 : {
                type     : String,
                required : false
            }
        };
    }
}
window.LF.Model.ExampleModel = ExampleModel;
