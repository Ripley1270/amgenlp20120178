import ELF from 'core/ELF';
import Logger from 'core/Logger';
import StudyDesign from 'trial/assets/study-design/';
// Add rules, actions, and expressions here...
(function (rules) {
    rules.add({
        id       : 'SendProtocol',
        trigger  : [
            'QUESTIONNAIRE:Completed'
        ],
        evaluate : function (filter, resume) {
            resume(!localStorage.getItem('screenshot'));
        },
        resolve  : [
            {
                action (input, done) {
                    // LF.Utilities.getProtocol()
                    //     .then((protocol) => {
                    let questionnaire = LF.Data.Questionnaire.data.dashboard.get('questionnaire_id');
                    // Send protocol
                    LF.Utilities.addIT({
                        question_id      : 'Protocol',
                        questionnaire_id : questionnaire,
                        response         : '1',//protocol,
                        IG               : 'Protocol',
                        IGR              : 0,
                        IT               : 'Protocol'
                    });
                    done();
                    // });
                }
            }
        ]
    });
    rules.add({
        id         : 'AddTrainerSiteUser',
        trigger    : [
            'INSTALL:ModelsInstalled'
        ],
        expression : function (filter, resume) {
            resume(LF.Utilities.isTrainer());
        },
        actionData : [
            {
                trueAction : function (filter, callback) {
                    let logger            = new Logger('StudyRules:AddTrainerSiteUser'),
                        user              = new LF.Model.User(),
                        getPasswordParams = function (newPassword) {
                            let i,
                                // Due to SW string length limitations, we have to lower the char length of the salt to
                                // a max of 36. Ideally it should be larger. (Uint8Array(16)). If possible in the
                                // future, change this.
                                max      = 36,
                                salt     = '',
                                fallback = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',
                                rand     = null;
                            if(window.crypto && typeof window.crypto.getRandomValues === 'function'){
                                rand = window.crypto.getRandomValues(new Uint8Array(12));
                            }
                            if(rand){
                                for(i = 0; i < rand.length; i++){
                                    salt += rand[i].toString();
                                }
                            }else{
                                for(i = 0; i < max; i++){
                                    salt += fallback.charAt(Math.floor(Math.random() * fallback.length));
                                }
                            }
                            return {
                                password : hex_sha512(newPassword + salt),
                                salt     : salt
                            };
                        },
                        passwordParams    = getPasswordParams('1234'),
                        siteData          = JSON.parse(localStorage.getItem('PHT_site'));
                    user.save(
                        {
                            username       : 'Trainer Site User',
                            language       : LF.StudyDesign.defaultLanguage + '-' + LF.StudyDesign.defaultLocale,
                            password       : passwordParams.password,
                            salt           : passwordParams.salt,
                            secretQuestion : '0',
                            secretAnswer   : hex_md5('1234'),
                            role           : 'site',
                            active         : 1,
                            userType       : '',
                            syncValue      : siteData.krdom
                        },
                        {
                            onSuccess : function () {
                                logger.info('Successfully saved trainer site user');
                                callback();
                            },
                            onError   : function () {
                                logger.error('Failed to save trainer site user');
                                callback();
                            }
                        }
                    );
                }
            }
        ]
    });
    rules.add({
        id       : 'PostActivationFullSync',
        trigger  : [
            'INSTALL:ModelsInstalled'
        ],
        evaluate : ['AND', 'isOnline', 'isStudyOnline'],
        resolve  : [
            {
                action : 'customSyncDataRequest',
                data   : {
                    webServiceFunction : 'getFullSyncData',
                    activation         : true
                }
            },
            {
                action : 'customSyncDataRequest',
                data   : {
                    webServiceFunction : 'getSubjMasterMedListSyncData',
                    activation         : false
                }
            }
        ]
    });
    rules.add({
        id       : 'ManualFullSync',
        trigger  : [
            'TOOLBOX:Transmit'
        ],
        evaluate : ['AND', 'isOnline', 'isStudyOnline'],
        resolve  : [
            {
                action : 'displayMessage',
                data   : 'PLEASE_WAIT'
            }, {
                action : 'customSyncDataRequest',
                data   : {
                    webServiceFunction : 'getFullSyncData',
                    activation         : false
                }
            },
            {
                action : 'customSyncDataRequest',
                data   : {
                    webServiceFunction : 'getSubjMasterMedListSyncData',
                    activation         : false
                }
            }, {
                action : 'removeMessage'
            }
        ]
    });
    rules.add({
        id         : 'ManualPartialSync',
        trigger    : [
            'LOGIN:Transmit',
            'DASHBOARD:Transmit'
        ],
        evaluate   : ['AND', 'isOnline', 'isStudyOnline'],
        actionData : [
            {
                trueAction    : 'displayMessage',
                trueArguments : 'PLEASE_WAIT'
            }, {
                trueAction    : 'customSyncDataRequest',
                trueArguments : {
                    webServiceFunction : 'getPartialSyncData'
                }
            }, {
                trueAction : 'removeMessage'
            }
        ]
    });
    rules.add({
        id         : 'LoginSync',
        trigger    : [
            'LOGIN:Success'
        ],
        evaluate   : ['AND', 'isOnline', 'isStudyOnline'],
        actionData : [
            {
                trueAction    : 'displayMessage',
                trueArguments : 'PLEASE_WAIT'
            }, {
                trueAction    : 'customSyncDataRequest',
                trueArguments : {
                    webServiceFunction : 'getPartialSyncData'
                }
            }, {
                trueAction : 'subjectSync'
            }, {
                trueAction : 'removeMessage'
            }
        ]
    });
    /*rules.add({
        id         : 'ForcedFullSync',
        trigger    : [
            'DASHBOARD:Open'
        ],
        evaluate   : ['AND', 'isOnline', 'isStudyOnline'],
        actionData : [
            {
                trueAction    : 'displayMessage',
                trueArguments : 'PLEASE_WAIT'
            }, {
                trueAction    : 'customSyncDataRequest',
                trueArguments : {
                    webServiceFunction : 'getFullSyncData',
                    activation         : false
                }
            },
            {
                trueAction    : 'customSyncDataRequest',
                trueArguments : {
                    webServiceFunction : 'getSubjMasterMedListSyncData',
                    activation         : false
                }
            },
            {
                trueAction : 'removeMessage'
            }
        ]
    });*/
    /*rules.add({
        id         : 'ForcedPartialSync',
        trigger    : [
            'DASHBOARD:Open'
        ],
        evaluate   : ['AND', 'isOnline', 'isStudyOnline'],
        actionData : [
            {
                trueAction    : 'displayMessage',
                trueArguments : 'PLEASE_WAIT'
            }, {
                trueAction    : 'customSyncDataRequest',
                trueArguments : {
                    webServiceFunction : 'getFullSyncData',
                    activation         : false
                }
            }, {
                trueAction : 'removeMessage'
            }
        ]
    });*/
    rules.add({
        id       : 'SyncDataReceived',
        trigger  : [
            'CUSTOMDATASYNC:Received/getFullSyncData',
            'CUSTOMDATASYNC:Received/getPartialSyncData'
        ],
        evaluate : true,
        resolve  : [
            {
                action (input, done) {
                    //Clear sync flag
                    localStorage.removeItem('NeedToSync_' + input.webServiceFunction);
                    let logger         = new Logger('StudyRules:SyncDataReceived'),
                        xmlDoc,
                        customSyncXML,
                        collectionXML,
                        collection,
                        modelXML,
                        modelJSON,
                        model,
                        stepCollection = function (collectionCounter) {
                            try{
                                logger.info('Enter: stepCollection, counter: ' + collectionCounter);
                                collectionXML = customSyncXML.children()[collectionCounter];
                                if(collectionXML){
                                    logger.info('Start parsing collection: ' + collectionXML.nodeName);
                                    collection = new LF.Collection[collectionXML.nodeName];
                                    //Flush model before processing
                                    //TODO: we should build up our sync before clearing model
                                    collection.clear({
                                        onSuccess : function () {
                                            logger.info('Successfully cleared collection, begin parsing models');
                                            stepModel(collectionCounter, 0);
                                        },
                                        onError   : function (error) {
                                            logger.error('Failed to clear collection, moving to next collection: ' + error);
                                            stepCollection(collectionCounter + 1);
                                        }
                                    });
                                }else{
                                    logger.info('Finished parsing sync');
                                    done();
                                }
                            }catch(e){
                                logger.error('Exception processing custom sync: ' + e);
                                done();
                            }
                        },
                        stepModel      = function (collectionCounter, modelCounter) {
                            try{
                                logger.info('Enter: stepModel, counter: ' + collectionCounter + '.' + modelCounter);
                                modelXML = $(collectionXML).children()[modelCounter];
                                if(modelXML){
                                    logger.info('Start parsing model: ' + modelXML.nodeName);
                                    model = new LF.Model[modelXML.nodeName];
                                    modelJSON = {};
                                    $.each(modelXML.attributes, function (i, attr) {
                                        let name  = attr.name,
                                            value = attr.value;
                                        modelJSON[name] = value;
                                    });
                                    model.save(modelJSON, {
                                        onSuccess : function () {
                                            logger.info('Successfully saved model, move to the next model');
                                            stepModel(collectionCounter, modelCounter + 1);
                                        },
                                        onError   : function () {
                                            logger.error('Failed to save model, move to the next model');
                                            stepModel(collectionCounter, modelCounter + 1);
                                        }
                                    });
                                }else{
                                    logger.info('Finished with collection, moving to next one');
                                    stepCollection(collectionCounter + 1);
                                }
                            }catch(e){
                                logger.error('Exception processing custom sync: ' + e);
                                done();
                            }
                        };
                    try{
                        logger.info('Start parsing sync');
                        //Parse xml
                        xmlDoc = $.parseXML(input.res);
                        customSyncXML = $(xmlDoc).children();
                        stepCollection(0);
                    }catch(e){
                        logger.error('Exception processing custom sync: ' + e);
                        done();
                    }
                },
                data : function (input, done) {
                    done(input);
                }
            }
        ]
    });
    rules.add({
        id       : 'orderSchedules',
        trigger  : [
            'APPLICATION:Loaded'
        ],
        evaluate : true,
        resolve  : [
            {
                action : function (input, done) {
                    let schedules = LF.StudyDesign.schedules.models;
                    schedules.sort((schedule1, schedule2) => {
                        return schedule1.get('salience') - schedule2.get('salience');
                    });
                    LF.StudyDesign.schedules.models = schedules;
                    done();
                }
            }
        ]
    });
}(ELF.rules));
