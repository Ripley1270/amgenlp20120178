/**
 * Created by jonathan.carr on 25-Jul-17.
 */
import BaseCollection from '../../app/core/collections/StorageBase';
import ReportDate from '../models/ReportDateModel';
export default class ReportDates extends BaseCollection {
    /**
     * @property {Visit} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link Visit}'
     */
    get model () {
        return ReportDate;
    }
}
window.LF.Collection.ReportDates = ReportDates;
