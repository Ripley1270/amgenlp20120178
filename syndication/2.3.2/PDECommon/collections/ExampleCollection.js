import BaseCollection from '../../app/core/collections/StorageBase';
import ExampleModel from '../models/ExampleModel';
/**
 * An example collection.
 * @class ExampleCollection
 * @extends BaseCollection
 */
export default class ExampleCollection extends BaseCollection {
    static get model () {
        return ExampleModel;
    }
}
window.LF.Collection.ExampleCollection = ExampleCollection;
