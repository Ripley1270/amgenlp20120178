import * as lStorage from 'core/lStorage';
import Logger from 'core/Logger';
import CurrentContext from 'core/CurrentContext';
import ELF from 'core/ELF';
import Session from 'core/classes/Session';
import CurrentSubject from 'core/classes/CurrentSubject';
const logger = new Logger('Session');
(function (Session) {
    let oldSession = Session;
    /*
     * @method validate
     * Validate a user login.
     */
    Session.prototype.login = function (id, password) {
        return this.getUser(id, true)
        // eslint-disable-next-line consistent-return
            .then((user) => {
                if(user){
                    let now          = new Date(),
                        userPassword = user.get('password'),
                        isAdminUser  = user.get('userType') === 'Admin',
                        hash,
                        changePassword;
                    if(isAdminUser){
                        hash = (userPassword.length > 32) ? hex_sha512(user.get('salt') + password) : hex_md5(user.get('salt') + password);
                    }else{
                        hash = (userPassword.length > 32) ? hex_sha512(password + user.get('salt')) : hex_md5(password + user.get('salt'));
                    }
                    if(hash === userPassword){
                        logger.operational('User {{ username }} with role {{ role }} successfully logged in.', {
                            username : user.get('username'),
                            role     : user.get('role')
                        });
                        (user.get('salt') === 'temp') ? lStorage.setItem('changePassword', true) : lStorage.removeItem('changePassword');
                        this.activeUser = user;
                        lStorage.setItem('isAuthorized', true);
                        if(this.timerOn){
                            this.setLastActive();
                            this.setTimeoutLastCheck();
                        }
                        // Record the time the user logged in
                        if(userPassword.length <= 32 && !isAdminUser){
                            changePassword = hex_sha512(password + user.get('salt'));
                            user.changeCredentials({password : changePassword, salt : user.get('salt')});
                        }
                        user.set('lastLogin', now.toString()).save();
                        this.resetFailureCount(user);
                        // Resolve the promise, passing in the current user.
                        //PDE Customization - add in login success Trigger
                        return Session.prototype.triggerLoginSuccessRule()
                            .then(() => {
                                return user;
                            });
                    }
                }
            });
    };
    Session.prototype.triggerLoginSuccessRule = () => {
        return CurrentSubject.getSubject()
            .then(subject => ELF.trigger(`LOGIN:Success/${CurrentContext().role}`, {subject}, this));
    };
})(Session);
