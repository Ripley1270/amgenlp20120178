/**
 * This index merges all utilities files into one utilities object for use as an import where necessary.
 * One can write the following statement to access all PDE_Common utilities:
 *      import PDE_Utils from 'path_to_PDE_Common/utils'
 * and then use any function from the object, for example
 *      PDE_Utils.getCollection();
 * The utilities functions are also mapped into the LF.Utilities namespace for compatibility.
 */
import * as commonUtils from './commonUtils';
import * as reportUtils from './reportUtils';
import * as tabletUtils from './tabletUtils';
import {mergeObjects} from 'core/utilities/languageExtensions';
export default mergeObjects({}, commonUtils, reportUtils, tabletUtils);
