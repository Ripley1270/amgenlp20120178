import Logger from 'core/Logger';
LF.Utilities.getActiveDiaryData = function (key) {
    let val = null;
    if(LF.Data.Questionnaire && LF.Data.Questionnaire.data.dashboard){
        val = LF.Data.Questionnaire.data.dashboard.get(key);
    }
    return val;
};
LF.Utilities.getActiveDiaryReportDate = function (key) {
    return LF.Utilities.convertToUtc(new Date(LF.Utilities.getActiveDiaryData('report_date')));
};
LF.Utilities.getActiveDiaryReportStartDate = function (key) {
    return new Date(LF.Utilities.getActiveDiaryData('started'));
};
LF.Utilities.getActiveDiaryID = function (key) {
    return LF.Utilities.getActiveDiaryData('questionnaire_id');
};
LF.Utilities.getActiveDiaryScheduleID = function () {
    let val = null;
    if(LF.router.view() instanceof LF.View.QuestionnaireView){
        val = LF.router.view().schedule_id;
    }
    return val;
};
