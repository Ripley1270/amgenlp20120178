import AboutView from 'logpad/views/AboutView';
import LoginView from 'logpad/views/LoginView';
import Answer from 'core/models/Answer';
import Logger from 'core/Logger';
import * as lstorage from 'core/lStorage';
/**
 * commonUtils is written such that it can be imported into another file via 'import * as PDE_Utils,
 * or the traditional global namespace LF.Utilities
 */
/**
 * addSysVar returns a promise that resolves true if a Sysvar is successfully added, or rejects if it fails.
 * @param {Object} params object containing parameters for sysvar to be added
 * @return {Promise<T>} resolves true if successful, else rejects
 */
export function addSysVar (params) {
    let model,
        logger = new Logger('PDE_Utilities.addSysvar');
    return Q.Promise((resolve) => {
        if(params && params.view && params.questionnaire_id && params.response && params.IG && params.sysvar){
            model = new Answer({
                response         : params.response,
                SW_Alias         : `${params.IG}.${params.sysvar}`,
                questionnaire_id : params.questionnaire_id,
                question_id      : 'SYSVAR'
            });
            if(params.view.data.dashboard){
                model.set('instance_ordinal', params.view.data.dashboard.get('instance_ordinal'));
            }
            params.view.data.answers.add(model);
            resolve(true);
        }else{
            logger.error('Attempt to insert a sysVar failed: params are incomplete.');
            throw new Error('Attempt to insert a sysVar failed: params are incomplete.');
        }
    });
}
LF.Utilities.addSysvar = addSysVar;
if(!String.format){
    /**
     * adding String.format function - replaces {i} with text at index provided
     * where i is an integer
     * To Call -> String.format(StringToFormat, 'text1', 'text2', 'text3',...);
     * @param format - String to format with text to replace with the format of {number}
     * @returns {*|string} - returns formatted String
     */
    String.format = function (format) {
        let args = Array.prototype.slice.call(arguments, 1);
        return format.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match
                ;
        });
    };
}
/**
 * String.format attached to string data type - replaces {i} with text at index provided
 * where i is an integer
 * To Call -> StringToFormat.format('text1', 'text2', 'text3',...);
 * @param format - String to format with text to replace with the format of {number}
 * @returns {*|string} - returns formatted String
 */
String.prototype.format = function () {
    let args = Array.prototype.slice.call(arguments, 0);
    return this.replace(/{(\d+)}/g, function (match, number) {
        return typeof args[number] != 'undefined'
            ? args[number]
            : match
            ;
    });
};
/**
 * dateDiffInDays returns the number of days (24 hour periods) between date 1 and date 2, rounded down to the nearest
 * day, taking into account time zone offsets. If useAbsolute evaluates as false, then the return value is date1 -
 * date2. If useAbsolute evaluates as true, the return value is |date1 - date2| (the absolute value of the difference)
 * @param {Date} date1 first date to compare
 * @param {Date} date2 second date to compare
 * @param {boolean} [useAbsolute] if true, then returns the absolute value of calculation
 * @return {number} dateDiff
 */
export function dateDiffInDays (date1, date2, useAbsolute) {
    let date1Copy   = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate(), 0, 0, 0, 0),
        date2Copy   = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate(), 0, 0, 0, 0),
        date1Offset = (date1Copy.getTimezoneOffset() * 60 * 1000),
        date2Offset = (date2Copy.getTimezoneOffset() * 60 * 1000),
        msDiff;
    if(!useAbsolute){
        msDiff = date1Copy - date2Copy + (date2Offset - date1Offset);
    }else{
        msDiff = Math.abs(date1Copy - date2Copy + (date2Offset - date1Offset));
    }
    return Math.floor(msDiff / 1000 / 60 / 60 / 24);
}
LF.Utilities.dateDiffInDays = dateDiffInDays;
/**
 * isTrainer checks local storage for a trainer flag
 * @return {boolean} returns true if in trainer mode
 */
export function isTrainer () {
    return Boolean(localStorage.getItem('trainer'));
}
LF.Utilities.isTrainer = isTrainer;
/**
 * isScreenshotMode checks local storage for a screenshot flag
 * @return {boolean} returns true if in screenshot mode
 */
export function isScreenshotMode () {
    return Boolean(localStorage.getItem('screenshot'));
}
LF.Utilities.isScreenshotMode = isScreenshotMode;
/**
 * getCollections returns a promise that resolves with an object,
 * where the keys are names of collections that contains values of the fetched collections.
 * @param {string[]} collectionNames array of collection names to fetch
 * @return {Promise.<Object<collections>>} object containing keys names for each fetched collection, with values of the
 *     collections
 */
export function getCollections (collectionNames) {
    let promiseFetches = [];
    _.forEach(collectionNames, (colName) => {
        promiseFetches.push(LF.Utilities.getCollection(colName));
    });
    return Q.all(promiseFetches)
        .then((fetchedColArray) => {
            return fetchedColArray.reduce((colObject, collection, index) => {
                colObject[collectionNames[index]] = collection;
            }, {});
        });
}
LF.Utilities.getCollections = getCollections;
/**
 * saveModel returns a promise which executes the save function of a supplied model, with optional data object
 * @param {LF.Model.<T>} model the model to be saved
 * @param {Object} [data] data to be saved into the model
 * @return {Promise.<T>} resolves upon successful save, rejects if model DNE or cannot be saved
 */
export function saveModel (model, data = {}) {
    let logger = new Logger('PDE_Utilities.saveModel');
    return Q.Promise((resolve) => {
        model.save(data, {
            onSuccess : () => {
                logger.info(`Successfully saved model ${model.name}`);
                resolve();
            },
            onError   : (err) => {
                logger.error(`Failed to save model ${model.name}`, err);
                resolve(err);
            }
        });
    }).then((err) => {
        // this catches the onError error and transforms it into a reject without accidentally logging both
        // the Failed to save model and Attempted to save non-model object messages
        if(err){
            throw err;
        }else{
            return true;
        }
    }, (err) => {
        logger.error(`Attempted to save non-model object`, err);
        throw err;
    });
}
LF.Utilities.saveModel = saveModel;
/**
 * saveNewModel returns a promise that resolves after a model of type modelName
 * is successfully created and saved with optional data
 * @param {string} modelName name of model type to be saved
 * @param {Object} [data] data to be saved
 * @return {Promise.<TResult>} resolves upon successful save of model, rejects if modelName isn't a real model type or
 *     if the model fails to save.
 */
export function saveNewModel (modelName, data = {}) {
    let logger = new Logger('PDE_Utilities.saveNewModel');
    return Q.Promise((resolve) => {
        resolve(new LF.Model[modelName]());
    }).then((model) => {
        return saveModel(model, data);
    }, (err) => {
        logger.error(`Failed to create new model: ${modelName} is not a model.`, err);
        throw err;
    });
}
LF.Utilities.saveNewModel = saveNewModel;
/**
 * getLastStartedDate returns a promise that resolves with a date object of the last started date of diaryID,
 * or rejects with string message if diaryID cannot be found.
 * If stripTimeZone is true, lastStartedDate will be returned as UTC time
 * (ie, if the lastStartedDate is 12:00 at tz +6:00, and the device is in tz -4:00,
 * then getLastStartedDate will resolve with a date at 12:00 at tz -4:00)
 * @param {string} diaryID id of the diary
 * @param {boolean} [stripTimeZone] optional flag
 * @return {Promise.<Date>} resolves with Date object, or rejects with error message
 */
export function getLastStartedDate (diaryID, stripTimeZone) {
    let logger = new Logger('PDE_Utilities.getLastStartedDate');
    return LF.Utilities.getCollection('LastDiaries')
        .then((lastDiariesCol) => {
            let diary = _.last(lastDiariesCol.where({questionnaire_id : diaryID}));
            if(diary){
                let date;
                if(stripTimeZone){
                    date = new Date(LF.Utilities.shiftToNewLocal(diary.get('lastStartedDate')));
                }else{
                    date = new Date(diary.get('lastStartedDate'));
                }
                return date;
            }else{
                let err = `No diary with id ${diaryID} has been started`;
                logger.error(err);
                throw new Error(err);
            }
        });
}
LF.Utilities.getLastStartedDate = getLastStartedDate;
/*

 LF.Utilities.getProtocol = function() {
 return Q.promise((resolve) => {
 if (LF.Utilities.isTrainer()) {
 resolve("1");
 } else {
 /!*
 LF.Utilities.getCollection("LocalDBCollection").then((localDBCollection) => {
 var localDBModel = _.first(localDBCollection.models);

 if (localDBModel) {
 resolve(localDBModel.get("protocol"));
 } else {
 resolve(null);
 }
 });
 *!/
 resolve("1");
 }
 });
 };

 LF.Utilities.getProtocolDisplay = function() {
 return Q.promise((resolve) => {
 LF.Utilities.getProtocol().then((protocol) => {
 let protDisplay = LF.StudyDesign.studyProtocol;

 //TODO: implement switch to add new protocol displays
 switch(protocol) {
 //case "1": protDisplay = "KF7019-01"; break;
 default: protDisplay = protDisplay; break;
 }

 resolve(protDisplay);
 });
 });
 };


 //Override protocol display on login screen
 (function (LoginView) {
 let oldInit = LoginView.prototype.initialize;

 LoginView.prototype.initialize = function () {
 let _this = this,
 initArgs = arguments;

 LF.Utilities.getProtocolDisplay()
 .then((protocolDisp) => {
 LF.StudyDesign.studyProtocol = protocolDisp;

 oldInit.apply(_this, initArgs);
 });
 };
 })(LoginView);

 //Override protocol display on about screen
 (function (AboutView) {
 let oldInit = AboutView.prototype.initialize;

 AboutView.prototype.initialize = function () {
 let _this = this,
 initArgs = arguments;

 LF.Utilities.getProtocolDisplay()
 .then((protocolDisp) => {
 LF.StudyDesign.studyProtocol = protocolDisp;

 oldInit.apply(_this, initArgs);
 });
 };
 })(AboutView);
 */
/**
 * Modified function from:
 * http://stackoverflow.com/questions/4912788/truncate-not-round-off-decimal-numbers-in-javascript
 * Takes a number/float, removes decimals after specified precision without applying rounding rules.
 * EX: truncate SBMTOT1N to 3 decimal places
 * SBMTOT1N = LF.Utilities.TruncateNumber(SBMTOT1N, 3);
 * @param {Number} number the value to truncate
 * @param {Number} precision the number of decimal places to truncate 'number' to
 * @return {string} the truncated value
 */
export function TruncateNumber (number, precision) {
    let re = new RegExp("(\\d+\\.\\d{" + precision + "})(\\d)"),
        m  = number.toString().match(re);
    return m ? parseFloat(m[1]).toFixed(precision) : number.valueOf().toFixed(precision);
}
LF.Utilities.TruncateNumber = TruncateNumber;
//TODO: find out the use case for this function
/**
 *
 * @param num
 * @param numZeros
 * @return {string}
 */
export function pdeZeroPad (num, numZeros) {
    let n = Math.abs(num);
    let zeros = Math.max(0, numZeros - Math.floor(n).toString().length);
    let zeroString = Math.pow(10, zeros).toString().substr(1);
    if(num < 0){
        zeroString = '-' + zeroString;
    }
    return zeroString + n;
}
LF.Utilities.pdeZeroPad = pdeZeroPad;
//[schan] Remove IT by IG
/**
 *
 * @param {string} IG
 */
export function removeITByIG (IG) {
    let answers,
        view = LF.Data.Questionnaire;
    if(view instanceof LF.View.QuestionnaireView){
        if(typeof IG !== 'undefined'){
            answers = view.queryAnswersByIG(IG);
            view.data.answers.remove(answers);
        }
    }
}
LF.Utilities.removeITByIG = removeITByIG;
/**
 *
 * @param IG
 * @param IT
 * @param IGR
 */
export function removeIT (IG, IT, IGR) {
    let view = LF.Data.Questionnaire;
    if(view instanceof LF.View.QuestionnaireView){
        view.removeIT(IG, IT, IGR);
    }
}
LF.Utilities.removeIT = removeIT;
