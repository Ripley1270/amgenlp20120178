import * as commonUtils from './commonUtils';
import Logger from 'core/Logger';
import UserReports from 'sitepad/collections/UserReports';
import Schedules from 'core/collections/Schedules';
/**
 * getSite returns a promise that resolves with the site
 * @return {Promise.<T>}
 */
export function getSite () {
    return commonUtils.getCollection('Sites')
        .then((sitesCollection) => {
            return sitesCollection.at(0);
        });
}
LF.Utilities.getSite = getSite;
/**
 * Return whether the specified visit has been touched.
 * A touched visit is one that has a UserVisit object associated with it.
 * That would include one that was skipped, expired, complete, or in-progress.
 * @param {UserVisit} userVisit the user visit
 * @return {boolean} true if visit has been touched, otherwise false
 */
export function isVisitTouched (userVisit) {
    return (userVisit !== null) && (userVisit !== undefined);
}
LF.Utilities.isVisitTouched = isVisitTouched;
/**
 * Return whether the specified visit is completed.
 * @param {UserVisit} userVisit the user visit
 * @return {boolean} true if visit is completed, otherwise false
 */
export function isVisitCompleted (userVisit) {
    return isVisitTouched(userVisit) && (userVisit.get('state') === LF.VisitStates.COMPLETED);
}
LF.Utilities.isVisitCompleted = isVisitCompleted;
/**
 * Return whether the specified visit is in progress.
 * @param {UserVisit} userVisit the user visit
 * @return {boolean} true if visit is in progress, otherwise false
 */
export function isVisitInProgress (userVisit) {
    return isVisitTouched(userVisit) && (userVisit.get('state') === LF.VisitStates.IN_PROGRESS);
}
LF.Utilities.isVisitInProgress = isVisitInProgress;
/**
 * Return whether the specified visit is finished.
 * A visit is considered finished if a visit has dateEnded for the UserVisit object. This is
 * equivalent to a visit with incomplete, aborted, expired or completed visit status.
 * @param {UserVisit} userVisit the user visit
 * @return {boolean} true if visit has ended, otherwise false
 */
export function isVisitFinished (userVisit) {
    return isVisitTouched(userVisit) && (userVisit.get('dateEnded') !== null);
}
LF.Utilities.isVisitFinished = isVisitFinished;
/**
 * Return whether the specified visit has been skipped.
 * @param {UserVisit} userVisit the user visit
 * @return {boolean} true if visit has been skipped, otherwise false
 */
export function isVisitSkipped (userVisit) {
    return isVisitTouched(userVisit) && (userVisit.get('state') === LF.VisitStates.SKIPPED);
}
LF.Utilities.isVisitSkipped = isVisitSkipped;
/**
 * Return whether the specified visit is available.
 * @param {UserVisit} userVisit the user visit
 * @return {boolean} true if visit is available, otherwise false
 */
export function isVisitAvailable (userVisit) {
    return isVisitTouched(userVisit) && (userVisit.get('state') === LF.VisitStates.AVAILABLE);
}
LF.Utilities.isVisitAvailable = isVisitAvailable;
/**
 * Return whether the specified visit has expired.
 * @param {UserVisit} userVisit the user visit
 * @return {boolean} true if visit has expired, otherwise false
 */
export function isVisitExpired (userVisit) {
    return isVisitTouched(userVisit) && (userVisit.get('state') === LF.VisitStates.INCOMPLETE)
        && (userVisit.get('expired') === true);
}
LF.Utilities.isVisitExpired = isVisitExpired;
/**
 * Return whether the specified visit was aborted.
 * A visit is considered aborted if it was started and then prematurely closed
 * but has not expired.
 * @param {UserVisit} userVisit the user visit
 * @return {boolean} true if visit was aborted, otherwise false
 */
export function isVisitAborted (userVisit) {
    return isVisitTouched(userVisit) && (userVisit.get('state') === LF.VisitStates.INCOMPLETE) && !isVisitExpired(userVisit);
}
LF.Utilities.isVisitAborted = isVisitAborted;
/**
 * Return the list of available report schedules remaining
 * @param {Subject} subject the subject
 * @param {UserVisit} visit the visit
 * @returns {Schedule[]}array of schedules
 */
export function getAvailableSchedules (subject, visit) {
    let logger = new Logger('getAvailableSchedules');
    // Because LF.schedule.determineSchedules has not yet been changed to use promises, the "available" callback
    // function is called multiple times.  The workaround is to wrap it to return a promise inside our own function,
    // then call our own function, below.
    let determineSchedules = (schedules, options) => {
        return Q.Promise(resolve => {
            LF.schedule.determineSchedules(schedules, options, resolve);
        });
    };
    let forms                = visit.get('forms'),
        userReports          = new UserReports(),
        visitId              = visit.id,
        subjectKrpt          = subject.get('krpt'),
        formSchedules        = new Schedules(),
        myAvailableSchedules = new Schedules();
    if(!forms){
        // We didn't get to userReports.fetch, so only now do we need a new promise. Q() basically creates a promise
        // that resolves immediately. "return Q.resolve()" is the same thing if that is more clear to you.
        return Q.resolve(myAvailableSchedules);
    }
    // userReports.fetch() already returns a new promise so no need to create our own.
    return userReports.fetch({
        search : {
            where : {user_visit_id : visitId, subject_krpt : subjectKrpt}
        }
    })
        .then((reports) => {

            //Determine how many reports are available for this visit in this phase
            forms.forEach((visit) => {
                formSchedules.add(LF.StudyDesign.schedules.filter((schedule) => schedule.get('target').id === visit), {merge : true});
            });
            // Call our new determineSchedules function
            return determineSchedules(formSchedules, {
                subject, // Same as subject: subject
                visit // same as visit: visit
            });
        })
        .then(available => {
            myAvailableSchedules.set(available);
            return myAvailableSchedules;
        })
        .catch(err => {
            // log the error.
            logger.error('Cannot get available schedules', err);
        });
}
LF.Utilities.getAvailableSchedules = getAvailableSchedules;
