import ELF from 'core/ELF';
import Logger from 'core/Logger';
const logger = new Logger('RetroRules');
// Add rules, actions, and expressions here...
(function (rules) {
    // rules.add({
    //     id         : 'AfterOpenRetroDiary',
    //     trigger    : [
    //         'QUESTIONNAIRE:Completed/DailyDiary'
    //     ],
    //     expression : function (filter, resume) {
    //         resume(!LF.Utilities.isScreenshotMode());
    //     },
    //     actionData : [
    //         {
    //             trueAction    : function (filter, done) {
    //                 //Change report date if diary is a retro diary
    //                 LF.Utilities.getReportDateForRetro(filter.questionnaire)
    //                     .then(reportDate => {
    //                         LF.Data.Questionnaire.data.dashboard.set('report_date',
    // LF.Utilities.convertToDate(reportDate)); done(); }); }, trueArguments : function (filter, callback) {
    // callback(filter); } } ] });
    rules.add({
        id         : 'AfterSaveRetroDiary',
        trigger    : [
            'QUESTIONNAIRE:Completed/DailyDiary'
        ],
        expression : false,
        actionData : [
            {
                trueAction    : function (filter, done) {
                    //Change report date if diary is a retro diary
                    LF.Utilities.getReportDateForRetro(filter.questionnaire)
                        .then(reportDate => {
                            LF.Data.Questionnaire.data.dashboard.set('report_date', LF.Utilities.convertToDate(reportDate));
                            done();
                        });
                },
                trueArguments : function (filter, callback) {
                    callback(filter);
                }
            },
            {
                trueAction    : function (filter, callback) {
                    let reportID        = filter.questionnaire,
                        reportDate      = LF.Data.Questionnaire.data.dashboard.get('report_date'),
                        reportStartDate = LF.Data.Questionnaire.data.dashboard.get('started'),
                        ReportDateModel;
                    //Save report date
                    LF.Utilities.getCollection('ReportDates')
                        .then(reportDates => {
                            ReportDateModel = _.first(_.filter(reportDates, function (model) {
                                return model.get('reportID') === reportID;
                            }));
                            if(ReportDateModel){
                                //Update
                                ReportDateModel.set('reportID', reportID);
                                ReportDateModel.set('reportDate', reportDate);
                                ReportDateModel.set('reportStartDate', LF.Utilities.timeStamp(new Date(reportStartDate)));
                            }else{
                                //New
                                ReportDateModel = new LF.Model.ReportDate({
                                    'reportID'        : reportID,
                                    'reportDate'      : reportDate,
                                    'reportStartDate' : LF.Utilities.timeStamp(new Date(reportStartDate))
                                });
                            }
                            ReportDateModel.save({},
                                {
                                    onSuccess : function () {
                                        logger.info('Successfully saved ReportDateModel');
                                        callback();
                                    },
                                    onError   : function () {
                                        logger.error('Failed to save ReportDateModel');
                                        callback();
                                    }
                                }
                            );
                        });
                },
                trueArguments : function (filter, callback) {
                    callback(filter);
                }
            }
        ]
    });
}(ELF.rules));
