LF.Utilities.getReportDateForRetro = function (diaryID) {
    return LF.Utilities.getBaseTimeForDiary(diaryID)
        .then(baseTime => {
            return LF.Utilities.getLastReportDateForRetro(diaryID)
                .then(lastReportDate => {
                    let reportDate = new Date();
                    //If after base time and diary not done yesterday then retro
                    if(LF.Utilities.dateDiffInDays(reportDate, baseTime, false) > 0 &&
                        (!lastReportDate || LF.Utilities.dateDiffInDays(lastReportDate, reportDate, false) < -1)){
                        reportDate.setDate(reportDate.getDate() - 1);
                    }
                    return (reportDate);
                });
        });
};
LF.Utilities.getLastReportDateForRetro = function (diaryID) {
    return LF.Utilities.getCollection('ReportDates')
        .then(reportDates => {
            let lastReport = _.find(reportDates, function (model) {
                return model.get('reportID') === diaryID;
            });
            if(lastReport){
                return (LF.Utilities.convertToUtc(new Date(lastReport.get('reportDate'))));
            }else{
                return (null);
            }
        });
};
LF.Utilities.getBaseTimeForDiary = function (diaryID) {
    return LF.Utilities.getCollection('LastDiaries')
        .then(lastDiaries => {
            let activation = new Date(LF.Data.Questionnaire.subject.get('activationDate'));
            return (activation);
            /*LF.Utilities.getCurrentAvailableDiary(function(availableDiary){
             if(dosingReassignment && LF.Utilities.dateDiffInDays(today, new Date(dosingReassignment.get('lastStartedDate'))) == 0 && diaryID != availableDiary){
             callback(new Date(Math.max(phaseStart.getTime(), new Date(model.get('resumeDate')).getTime())));
             }else if(model && model.get('DISCON1L') == 2 && LF.Utilities.dateDiffInDays(new Date(), new Date(model.get('resumeDate'))) >= 0){
             callback(new Date(Math.max(phaseStart.getTime(), new Date(model.get('resumeDate')).getTime(), dosingReassignment ? new Date(dosingReassignment.get('lastStartedDate')).getTime() : 0)));
             }else if(LF.Data.Subjects.at(0).get('phase') == LF.StudyDesign.studyPhase.RESCUEARM){
             //Base is start of phase
             callback(new Date(Math.max(phaseStart.getTime(), dosingReassignment ? new Date(dosingReassignment.get('lastStartedDate')).getTime() : 0)));
             }else{
             //Base is activation
             callback(new Date(Math.max(activation.getTime(), phaseStart.getTime(), dosingReassignment ? new Date(dosingReassignment.get('lastStartedDate')).getTime() : 0)));
             }
             });*/
        });
};
LF.Utilities.isRetro = function () {
    let questionnaireId = LF.Data.Questionnaire.id,
        activationDate  = new Date(LF.Data.Questionnaire.subject.get('activationDate'));
    return LF.Utilities.getCollection('ReportDates')
        .then(collection => {
            let reportDates = _.first(_.filter(collection.models, function (models) {
                    return (models.get('reportID') == questionnaireId);
                })),
                today       = new Date();
            if(reportDates){
                let reportDate = LF.Utilities.convertToUtc(new Date(reportDates.get('reportDate')));
                return LF.Utilities.dateDiffInDays(today, reportDate) > 1;
            }else{
                return LF.Utilities.dateDiffInDays(today, activationDate) !== 0;
            }
        });
};
