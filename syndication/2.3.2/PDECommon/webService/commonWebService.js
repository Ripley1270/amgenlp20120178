import COOL from 'core/COOL';
import Data from 'core/Data';
import Trainer from 'trainer/logpad/Trainer';
import WebService from 'core/classes/WebService';
class CommonWebService extends COOL.getClass('WebService', WebService) {
    getSyncData (procedureName, params, onSuccess, onError) {
        let ajaxConfig = {
                type : 'POST',
                uri  : this.buildUri('/v1/SWAPI/GetDataClin'),
                auth : params.auth || null
            },
            krpt       = params.krpt || localStorage.getItem('krpt'),
            payload    = {
                'StoredProc' : procedureName,
                'Params'     : [
                    krpt,
                    Data.deviceID,
                    new Date().ISOStamp(),
                    new Date().getTimezoneOffset() * 60 * 1000
                ]
            };
        if(procedureName === 'PDE_LPASubjMasterMedListSyncSQLV1'){
            payload.Params = payload.Params.concat(['11 SEP 2001', '11 SEP 2001']);
        }
        return this.transmit(ajaxConfig, payload, onSuccess, onError);
    }

    getFullSyncData (params, onSuccess, onError) {
        return this.getSyncData('PDE_LPAFullSyncSQLV1', params, onSuccess, onError);
    }

    getPartialSyncData (params, onSuccess, onError) {
        return this.getSyncData('PDE_LPAFullSyncSQLV1', params, onSuccess, onError);
    }

    getSubjMasterMedListSyncData (params, onSuccess, onError) {
        let additionalParams = [
            '11 SEP 2001',
            '11 SEP 2001'
        ];
        return this.getSyncData('PDE_LPASubjMasterMedListSyncSQLV1', params, onSuccess, onError, additionalParams);
    }
}
class commonTrainer_WebService extends CommonWebService {
    getFullSyncData (params, onSuccess = $.noop, onError = $.noop) {
        if(!Trainer.trainerMode()){
            return super.getFullSyncData(params, onSuccess, onError);
        }
        let result = '';
        onSuccess(result);
        return Q.promise((resolve, reject) => {
            resolve(result);
        });
    }

    getPartialSyncData (params, onSuccess = $.noop, onError = $.noop) {
        if(!Trainer.trainerMode()){
            return super.getPartialSyncData(params, onSuccess, onError);
        }
        let result = '';
        onSuccess(result);
        return Q.promise((resolve, reject) => {
            resolve(result);
        });
    }
}
COOL.add('WebService', commonTrainer_WebService);
