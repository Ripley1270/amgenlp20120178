import Logger from 'core/Logger';
import ELF from 'core/ELF';
import COOL from 'core/COOL';
import {isLogPad, isSitePad} from 'core/utilities';
/**
 * Handles custom historical data sync transmission to the web service
 * @param {Object} transmissionItem The item in the transmission queue to send.
 */
export function customSyncData (transmissionItem, callback = $.noop) {
    let logger = new Logger('Transmit:customSyncData');
    let params                = JSON.parse(transmissionItem.get('params')),
        customSyncDataSuccess = (res) => {
            // Don't update data unless we get a response
            if(!_.isEmpty(res)){
                // Platform-specific trigger - prepend "SP_" or "LP_"
                let platform = (isSitePad() ? "SP_" : "");
                ELF.trigger(`${platform}CUSTOMDATASYNC:Received/${params.webServiceFunction}`, {res})
                    .then(() => {
                        this.destroy(transmissionItem.get('id'), callback);
                    })
                    .catch(err => {
                        err && logger.error(`Error destroying transmission`, err);
                    })
                    .done();
            }else{
                this.destroy(transmissionItem.get('id'), callback);
            }
        },
        customSyncDataError   = () => {
            // Remove from queue
            this.destroy(transmissionItem.get('id'), callback);
        };
    LF.studyWebService[params.webServiceFunction](params, customSyncDataSuccess, customSyncDataError);
}
let CustomTransmit = {
    customSyncData
};
// For 2.3.1, this also requires a companion change to app/sitepad/transmit/index.js
// and app/logpad/transmit/index.js. (DE20597).  See Astellas/8951-CL-0103-0301
LF.Transmit = _.extend({}, LF.Transmit, CustomTransmit);
COOL.service('Transmit', LF.Transmit);
