LF.DynamicText.formatTime = function (date) {
    return LF.DynamicText.format("01 Jan 1900 " + date, 'timeOutput');
};
LF.DynamicText.formatDate = function (date) {
    return LF.DynamicText.format(date, 'dateFormat');
};
LF.DynamicText.formatDateTime = function (date) {
    return LF.DynamicText.format(date, 'dateTimeFormat');
};
LF.DynamicText.format = function (date, format) {
    const dateOptions = _.extend({}, LF.strings.dates({dates : {}}), LF.strings.dates({dateConfigs : {}})),
          dateBox     = $('<input data-role=\"datebox\" />'),
          dateLang    = {
              lang : {
                  default : dateOptions,
                  isRTL   : (LF.strings.getLanguageDirection() === 'rtl')
              },
              mode : 'datebox'
          };
    dateBox.datebox(dateLang);
    if(date){
        if(typeof date == "string"){
            date = new Date(date);
        }
        return dateBox.datebox('callFormat', dateOptions[format], date);
    }else{
        return dateBox.datebox('callFormat', dateOptions[format], dateBox.datebox('getTheDate'));
    }
};
