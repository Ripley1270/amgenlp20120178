import DateTimePicker from 'core/widgets/DateTimePicker';
import Logger from 'core/Logger';
export default class DateTimePickerCustom extends DateTimePicker {
    constructor (options) {
        super(options);
    }

    render () {
        if(!LF.Utilities.isScreenshotMode()){
            let configuration = this.model.get('configuration'),
                minFunction   = (configuration.minFunction) ? window.LF.DateTimeFunctions[configuration.minFunction] : window.LF.DateTimeFunctions.NoopFunction,
                minParams     = _.extend({}, configuration.minParams),
                maxFunction   = (configuration.maxFunction) ? window.LF.DateTimeFunctions[configuration.maxFunction] : window.LF.DateTimeFunctions.NoopFunction,
                maxParams     = _.extend({}, configuration.maxParams),
                defFunction   = (configuration.defFunction) ? window.LF.DateTimeFunctions[configuration.defFunction] : window.LF.DateTimeFunctions.NoopFunction,
                defParams     = _.extend({}, configuration.defParams);
            Q.all([minFunction(minParams), maxFunction(maxParams), defFunction(defParams)])
                .spread((minVal, maxVal, defVal) => {
                    if(minVal){
                        configuration.min = minVal.ISOLocalTZStamp();
                    }
                    if(maxVal){
                        configuration.max = maxVal.ISOLocalTZStamp();
                    }
                    if(defVal){
                        configuration.defaultValue = defVal.ISOLocalTZStamp();
                    }
                    //Validate that current value is still within boundaries
                    if(this.answers.models[0] && this.answers.models[0].get('response')){
                        var currentVal = new Date(this.answers.models[0].get('response'));
                        if((minVal != null && currentVal.getTime() < minVal.getTime()) ||
                            (maxVal != null && currentVal.getTime() > maxVal.getTime())){
                            this.removeAllAnswers();
                            this.answer = null;
                            this.datetimeVal = null;
                            this.localizedDate = null;
                            this.localizedTime = null;
                        }else{
                            //Set default to current value
                            configuration.defaultValue = currentVal.ISOLocalTZStamp();
                        }
                    }
                    return super.render();
                });
        }else{
            this.doRender();
        }
    }
};
window.LF.Widget.DateTimePickerCustom = DateTimePickerCustom;
//Start of DateTime helper functions, define new functions within LF.DateTimeFunctions
let DateTimeFunctions = {};
//Default function, returns null
DateTimeFunctions.NoopFunction = function (params) {
    return Q.Promise((resolve) => {
        resolve(null);
    });
};
DateTimeFunctions.getMidnightTwodaysAgo = function (params) {
    return Q.Promise((resolve) => {
        let clipToActivation = (params.clipToActivation != null) ? params.clipToActivation : true,
            val              = LF.Utilities.convertToUtc(new Date(LF.Data.Questionnaire.data.dashboard.get('report_date')));
        val.setDate(val.getDate() - 2);
        //Clip to activation
        LF.DateTimeFunctions.getActivationDate(params)
            .then((activationDT) => {
                if(clipToActivation && val.getTime() < activationDT.getTime()){
                    resolve(activationDT);
                }else{
                    resolve(val);
                }
            });
    });
};
DateTimeFunctions.getActivationDate = function (params) {
    return Q.Promise((resolve) => {
        LF.Collection.Subjects.getSubjectBy()
            .then((subject) => {
                let val = new Date(subject.get('activationDate'));
                val.setSeconds(0, 0);
                resolve(val);
            });
    });
};
DateTimeFunctions.getCurrentTime = function (params) {
    return Q.Promise((resolve) => {
        let val = new Date();
        val.setSeconds(0, 0);
        resolve(val);
    });
};
DateTimeFunctions.getNumHoursAgo = function (params) {
    return Q.Promise((resolve) => {
        let clipToActivation = (params.clipToActivation != null) ? params.clipToActivation : true,
            val              = new Date(),
            numHoursAgo      = params.hours || 0;
        val.setHours(val.getHours() - numHoursAgo);
        val.setSeconds(0, 0);
        //Clip to activation
        LF.DateTimeFunctions.getActivationDate(params)
            .then((activationDT) => {
                if(clipToActivation && val.getTime() < activationDT.getTime()){
                    resolve(activationDT);
                }else{
                    resolve(val);
                }
            });
    });
};
DateTimeFunctions.getScreenvalue = function (params) {
    return Q.Promise((resolve) => {
        let logger     = new Logger('DateTimeFunctions:getScreenvalue'),
            responseDT = null,
            response;
        try{
            if(params.loopingIG){
                let igr = LF.router.view().getCurrentIGR(params.loopingIG);
                response = LF.router.view().queryAnswersByQuestionIDAndIGR(params.questionID, igr)[0];
                if(response){
                    responseDT = new Date(response.get('response'));
                }
            }else{
                response = LF.Helpers.getResponses(params.questionID)[0];
                if(response){
                    responseDT = new Date(response.response);
                }
            }
        }catch(ex){
            logger.error('Exception getting screen value: ' + ex);
        }
        resolve(responseDT);
    });
};
window.LF.DateTimeFunctions = DateTimeFunctions;
