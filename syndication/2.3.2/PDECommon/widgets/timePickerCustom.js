import TimePicker from 'core/widgets/TimePicker';
import Logger from 'core/Logger';
export default class TimePickerCustom extends TimePicker {
    constructor (options) {
        super(options);
    }

    render () {
        if(!LF.Utilities.isScreenshotMode()){
            let configuration = this.model.get('configuration'),
                minFunction   = (configuration.minFunction) ? window.LF.TimeFunctions[configuration.minFunction] : window.LF.TimeFunctions.NoopFunction,
                minParams     = _.extend({}, configuration.minParams),
                maxFunction   = (configuration.maxFunction) ? window.LF.TimeFunctions[configuration.maxFunction] : window.LF.TimeFunctions.NoopFunction,
                maxParams     = _.extend({}, configuration.maxParams),
                defFunction   = (configuration.defFunction) ? window.LF.TimeFunctions[configuration.defFunction] : window.LF.TimeFunctions.NoopFunction,
                defParams     = _.extend({}, configuration.defParams);
            Q.all([minFunction(minParams), maxFunction(maxParams), defFunction(defParams)])
                .spread((minVal, maxVal, defVal) => {
                    if(minVal){
                        configuration.minTime = '' + minVal.getHours() + ':' + minVal.getMinutes();
                    }
                    if(maxVal){
                        configuration.maxTime = '' + maxVal.getHours() + ':' + maxVal.getMinutes();
                    }
                    if(defVal){
                        configuration.defaultValue = '' + defVal.getHours() + ':' + defVal.getMinutes();
                    }
                    //TODO: validate time picker boundary
                    //Validate that current value is still within boundaries
                    //                    if (this.answers.models[0] && this.answers.models[0].get('response')) {
                    //                        var currentVal = new Date(this.answers.models[0].get('response'));
                    //
                    //                        if ((minVal != null && currentVal.getTime() < minVal.getTime()) ||
                    //                            (maxVal != null && currentVal.getTime() > maxVal.getTime())) {
                    //
                    //                            this.removeAllAnswers();
                    //                            this.answer = null;
                    //                            this.datetimeVal = null;
                    //                            this.localizedDate = null;
                    //                        } else {
                    //                            //Set default to current value
                    //                            configuration.defaultValue = '' + currentVal.getHours() + ':' +
                    // currentVal.getMinutes(); } }
                    return super.render();
                });
        }else{
            this.doRender();
        }
    }
};
window.LF.Widget.TimePickerCustom = TimePickerCustom;
//Start of Time helper functions, define new functions within LF.TimeFunctions
let TimeFunctions = {};
//Default function, returns null
TimeFunctions.NoopFunction = function (params) {
    return Q.Promise((resolve) => {
        resolve(null);
    });
};
TimeFunctions.getCurrentTime = function (params) {
    return Q.Promise((resolve) => {
        let now = new Date();
        resolve(now);
    });
};
TimeFunctions.getMidnight = function (params) {
    return Q.Promise((resolve) => {
        let now = new Date();
        now.setHours(0, 0, 0, 0);
        resolve(now);
    });
};
window.LF.TimeFunctions = TimeFunctions;
