import DatePicker from 'core/widgets/DatePicker';
import Logger from 'core/Logger';
export default class DatePickerCustom extends DatePicker {
    constructor (options) {
        super(options);
    }

    render () {
        if(!LF.Utilities.isScreenshotMode()){
            let configuration = this.model.get('configuration'),
                minFunction   = (configuration.minFunction) ? window.LF.DateFunctions[configuration.minFunction] : window.LF.DateFunctions.NoopFunction,
                minParams     = _.extend({}, configuration.minParams),
                maxFunction   = (configuration.maxFunction) ? window.LF.DateFunctions[configuration.maxFunction] : window.LF.DateFunctions.NoopFunction,
                maxParams     = _.extend({}, configuration.maxParams),
                defFunction   = (configuration.defFunction) ? window.LF.DateFunctions[configuration.defFunction] : window.LF.DateFunctions.NoopFunction,
                defParams     = _.extend({}, configuration.defParams);
            Q.all([minFunction(minParams), maxFunction(maxParams), defFunction(defParams)])
                .spread((minVal, maxVal, defVal) => {
                    if(minVal){
                        configuration.min = minVal.ISOLocalTZStamp();
                    }
                    if(maxVal){
                        configuration.max = maxVal.ISOLocalTZStamp();
                    }
                    if(defVal){
                        configuration.defaultValue = defVal.ISOLocalTZStamp();
                    }
                    //Validate that current value is still within boundaries
                    if(this.answers.models[0] && this.answers.models[0].get('response')){
                        var currentVal = new Date(this.answers.models[0].get('response'));
                        if((minVal != null && currentVal.getTime() < minVal.getTime()) ||
                            (maxVal != null && currentVal.getTime() > maxVal.getTime())){
                            this.removeAllAnswers();
                            this.answer = null;
                            this.datetimeVal = null;
                            this.localizedDate = null;
                        }else{
                            //Set default to current value
                            configuration.defaultValue = currentVal.ISOLocalTZStamp();
                        }
                    }
                    return super.render();
                });
        }else{
            this.doRender();
        }
    }
};
window.LF.Widget.DatePickerCustom = DatePickerCustom;
//Start of Date helper functions, define new functions within LF.DateFunctions
let DateFunctions = {};
//Default function, returns null
DateFunctions.NoopFunction = function (params) {
    return Q.Promise((resolve) => {
        resolve(null);
    });
};
DateFunctions.getActivationDate = function (params) {
    return Q.Promise((resolve) => {
        LF.Collection.Subjects.getSubjectBy()
            .then((subject) => {
                let val = new Date(subject.get('activationDate'));
                val.setHours(0, 0, 0, 0);
                resolve(val);
            });
    });
};
DateFunctions.getCurrentDate = function (params) {
    return Q.Promise((resolve) => {
        let reportDate = LF.Utilities.convertToUtc(new Date(LF.Data.Questionnaire.data.dashboard.get('report_date')));
        resolve(reportDate);
    });
};
DateFunctions.getNumDaysAgo = function (params) {
    return Q.Promise((resolve) => {
        Q.all([DateFunctions.getCurrentDate(params), DateFunctions.getActivationDate(params)])
            .spread(function (reportDate, activationDate) {
                let numDaysAgo = params.numDaysAgo || 0;
                reportDate.setDate(reportDate.getDate() - numDaysAgo);
                if(reportDate.getTime() < activationDate.getTime()){
                    //Clip to activation
                    resolve(activationDate);
                }else{
                    resolve(reportDate);
                }
            });
    });
};
window.LF.DateFunctions = DateFunctions;
