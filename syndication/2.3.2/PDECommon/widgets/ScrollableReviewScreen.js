/** #depends ReviewScreen.js
 * @file Scrollable Review screen widget
 * @author <a href='mailto:reldridge@phtcorp.com'>Ryan Eldridge</a>
 * @version 1.8
 */
import ReviewScreen from '../../app/core/widgets/ReviewScreen';
export default class ScrollableReviewScreen extends ReviewScreen {
    //TODO: this is a copy of the ReviewScreen.js render function, with the ability to add resources to the template's
    // context
    doRender (headerStrings) {
        let that = this;
        return Q()
            .then(() => {
                let _this          = that,
                    widgetView     = LF.templates.display(this.container, headerStrings),
                    screenFunction = that.model.get('screenFunction'),
                    modelId        = that.model.get('id'),
                    selectableEntries;
                that.$el.empty();
                that.$el.append(widgetView);
                if(screenFunction && _.isFunction(LF.Widget.ReviewScreen[screenFunction])){
                    // calls the Screen function to get the Item list to display
                    LF.Widget.ReviewScreen[screenFunction](that)
                        .then((items) => {
                            _this.reviewScreenList = items;
                            _(_this.reviewScreenList).each(function (item) {
                                // create and append items separately
                                _this.$(`#${modelId}_Items`).append(_this.addItem(item));
                            });
                            selectableEntries = !!(_.find(items, function (item) {
                                return (item.id !== '-1');
                            }));
                            if(selectableEntries){
                                that.$el.find('.edit-instructions').show();
                            }else{
                                that.$el.find('.edit-instructions').hide();
                            }
                            // passing empty array as nothing to exclude since no item is selected at this point.
                            that.addDefaultButtons([]);
                            that.$el.appendTo(that.parent.$el);
                            that.delegateEvents();
                            that.$el.trigger('create');
                        });
                }else{
                    that.logger.log(Log4js.Level.ERROR, 'Screen Function not found in the Widget Configuration or its not defined.');
                    throw new Error('Screen Function not found in the Widget Configuration or its not defined.');
                }
            });
    }

    /**
     * Responsible for displaying the widget.
     * @returns '{@link LF.Widget.ScrollableReviewScreen}'
     */
    render () {
        let that = this;
        return Q()
            .then(() => {
                let config           = that.model.get('config'),
                    visibleRows      = config ? config.visibleRows : 4,
                    itemHeight,
                    itemList,
                    buttonList,
                    currentRows,
                    headers          = that.model.get('headers'),
                    editInstructions = that.model.get('editInstructions'),
                    headerResources  = [],
                    headerStrings    = {id : that.model.get('id')};
                that.activeItem = null;
                //Look for header strings to add to context
                if(_.isArray(headers)){
                    headerResources = headers;
                }
                //If edit instructions, add to end of header strings
                if(editInstructions){
                    headerResources.push(editInstructions);
                }else{
                    headerStrings['editInstructions'] = '';
                }
                LF.getStrings(headerResources, () => {
                }, {namespace : that.parent.parent.id})
                    .then((strings) => {
                        _(strings).each(function (str, index) {
                            if(!editInstructions || index < (strings.length - 1)){
                                headerStrings[`header${index}`] = str;
                            }else if(editInstructions && index === (strings.length - 1)){
                                //Add instructions
                                headerStrings['editInstructions'] = str;
                            }
                        });
                        //LF.Widget.ReviewScreen.prototype.render.call(this);
                        that.doRender(headerStrings)
                            .then(() => {
                                itemList = that.$('.reviewScreen_Items');
                                itemList.css('overflow-y', 'auto');
                            });
                    });
            });
    }

    addItem (item) {
        //kgonsalves: allow custom classes
        if(!item.classes){
            item.classes = '';
        }
        let strings = {};
        if(_.isArray(item.text)){
            _(item.text).each(function (str, index) {
                strings['text' + index] = str;
            });
        }else{
            this.logger.log(Log4js.Level.ERROR, 'Text for the Review Screen Item is not configured in an Array.');
            throw new Error('Text for the Review Screen Item is not configured in an Array.');
        }
        return $(LF.templates.display(this.reviewScreenItems, strings)).addClass('item').addClass(item.classes).attr('data-id', item.id);
    }

    itemSelected (e) {
        //kgonsalves: do not highlight unselectable data
        let targetItem = this.$(e.currentTarget),
            id         = targetItem.data('id');
        if(id > 0){
            LF.Widget.ReviewScreen.prototype.itemSelected.call(this, e);
            if(this.activeItem){
                this.$el.find('.edit-instructions').hide();
            }else{
                this.$el.find('.edit-instructions').show();
            }
        }
        return Q().then(() => {
            return this.$el.trigger('create');
        });
    }
}
export default ScrollableReviewScreen;
window.LF.Widget.ScrollableReviewScreen = ScrollableReviewScreen;
