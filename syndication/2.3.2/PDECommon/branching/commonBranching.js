/**
 * always returns a promise that will always resolve true and execute the branch
 * @param {Object} branchObj contains the branching configuration
 * @param {QuestionnaireView} view current Questionnaire view
 * @return {Promise<T>} returns a promise that resolves as true or false
 */
LF.Branching.branchFunctions.always = function (branchObj, view) {
    return Q(true);
};
//Added for backwards compatibility
/**
 * equal will compare a supplied value to a supplied question's response, and resolve true if the two match
 * @param {Object} branchObj contains the branching configuration
 * @param {QuestionnaireView} view current Questionnaire view
 * @return {Promise<T>} returns a promise that resolves as true or false
 */
LF.Branching.branchFunctions.equal = function (branchObj, view) {
    branchObj.branchParams.operand = '=';
    return LF.Branching.branchFunctions.compare(branchObj, view);
};
/**
 * compare will compare a supplied value to a supplied question's response,
 * utilizing one of the following operands provided in branchParams.operand:
 * = , != , >= , <= , > , <
 * and resolve true if the two pass the comparison.
 * @param {Object} branchObj contains the branching configuration
 * @param {QuestionnaireView} view current Questionnaire view
 * @return {Promise<T>} returns a promise that resolves as true or false
 */
LF.Branching.branchFunctions.compare = function (branchObj, view) {
    return Q.Promise((resolve) => {
        let response,
            questionId = branchObj.branchParams.questionId,
            operand    = branchObj.branchParams.operand,
            questionIT = branchObj.branchParams.IT,
            result     = false;
        if(branchObj.branchParams.IG){
            //Get looping data
            let igr = view.getCurrentIGR(branchObj.branchParams.IG);
            if(questionId){
                response = view.queryAnswersByQuestionIDAndIGR(questionId, igr)[0];
            }else if(questionIT){
                response = view.queryAnswersByIGITAndIGR(branchObj.branchParams.IG, questionIT, igr)[0];
            }
        }else{
            if(questionId){
                response = view.queryAnswersByID(questionId)[0];
            }else if(questionIT){
                response = view.queryAnswersByIT(questionIT)[0];
            }
        }
        if(response){
            response = response.get('response');
        }
        switch(operand){
            case '=':{
                result = (response == branchObj.branchParams.value);
            }
                break;
            case '!=':{
                result = (response != branchObj.branchParams.value);
            }
                break;
            case '>=':{
                result = (response != null && Number(response) >= Number(branchObj.branchParams.value));
            }
                break;
            case '<=':{
                result = (response != null && Number(response) <= Number(branchObj.branchParams.value));
            }
                break;
            case '>':{
                result = (response != null && Number(response) > Number(branchObj.branchParams.value));
            }
                break;
            case '<':{
                result = (response != null && Number(response) < Number(branchObj.branchParams.value));
            }
                break;
            default:{
                //No operand or unknown operand
                result = false;
            }
                break;
        }
        resolve(result);
    });
};
/**
 * alwaysBranchBack will pop screens off the stack until it reaches the supplied branchTo
 * If branchObj.clearBranchedResponses is explicitly false, then the branchBack will maintain responses,
 * otherwise responses are cleared.
 * If branchObject.branchParams.simulatebackButton is true, the function will branch back only one screen in the stack,
 * as though the back button was pressed.
 * @param {Object} branchObj contains the branching configuration
 * @param {QuestionnaireView} view current Questionnaire view
 * @return {Promise<T>} returns a promise that resolves as true or false
 */
LF.Branching.branchFunctions.alwaysBranchBack = function (branchObj, view) {
    return Q.Promise((resolve) => {
        if(branchObj.clearBranchedResponses !== false){
            let screen    = _.filter(view.data.screens, function (screen) {
                    return screen.id === branchObj.branchFrom;
                })[0],
                questions = _(screen.get('questions')).pluck('id');
            // QuestionnaireView does not clear the responses of the
            // 'branchFrom' screen's questions
            view.resetAnswersSelected(questions);
        }
        if(branchObj.branchParams && branchObj.branchParams.simulateBackButton){
            view.screenStack.pop();
            //Just pop to the previous screen
            branchObj.branchTo = view.screenStack.pop();
        }else{
            // Clear the screens stacked above the 'branchTo' target
            let aBranch = view.screenStack.pop();
            while(aBranch !== branchObj.branchTo){
                aBranch = view.screenStack.pop();
            }
        }
        resolve(true);
    });
};
//Added for backwards compatibility
/**
 * equalBranchBack calls compareBranchBack with an operand of '='
 * @param {Object} branchObj contains the branching configuration
 * @param {QuestionnaireView} view current Questionnaire view
 * @return {Promise<T>} returns a promise that resolves as true or false
 */
LF.Branching.branchFunctions.equalBranchBack = function (branchObj, view) {
    branchObj.branchParams.operand = '=';
    return LF.Branching.branchFunctions.compareBranchBack(branchObj, view);
};
/**
 * compareBranchBack calls compare, then if compare resolves true, it calls alwaysBranchBack.
 * If compare resolves false, then compareBranchBack resolves false.
 * @param {Object} branchObj contains the branching configuration
 * @param {QuestionnaireView} view current Questionnaire view
 * @return {Promise<T>} returns a promise that resolves as true or false
 */
LF.Branching.branchFunctions.compareBranchBack = function (branchObj, view) {
    return LF.Branching.branchFunctions.compare(branchObj, view)
        .then((result) => {
            if(result){
                return LF.Branching.branchFunctions.alwaysBranchBack(branchObj, view);
            }else{
                return false;
            }
        });
};
/**
 * exitDiary will execute always, or equal if the branchObj contains a branchParams.questionId
 * if the always or equal resolves as true, then it will exit the diary
 * If branchParams.promptBeforeExit == true, the backoutHandler will be called as though the back button was hit on the
 * first screen Else, diary will exit to dashboard without prompt.
 * @param {Object} branchObj contains the branching configuration
 * @param {QuestionnaireView} view current Questionnaire view
 * @return {Promise<T>} returns a promise that resolves as true or false
 */
LF.Branching.branchFunctions.exitDiary = function (branchObj, view) {
    let branchFunction = LF.Branching.branchFunctions.always;
    if(branchObj.branchParams && branchObj.branchParams.questionId){
        branchFunction = LF.Branching.branchFunctions.equal;
    }
    //Execute branching function and exit diary if it returns true
    return branchFunction(branchObj, view)
        .then((result) => {
            if(result){
                if(branchObj.branchParams && Boolean(branchObj.branchParams.promptBeforeExit) === true){
                    return view.backoutHandler();
                }else{
                    LF.security.stopQuestionnaireTimeOut();
                    localStorage.setItem('questionnaireToDashboard', true);
                    LF.router.navigate('dashboard', {trigger : true, replace : true});
                    return true;
                }
            }else{
                return false;
            }
        });
};
/**
 * isCurrentPhase will compare current subject phase to phase supplied in branchParams.value
 * if they match, resolves true, else resolves false
 * @param {Object} branchObj contains the branching configuration
 * @param {QuestionnaireView} view current Questionnaire view
 * @return {Promise<T>} returns a promise that resolves as true or false
 */
LF.Branching.branchFunctions.isCurrentPhase = function (branchObj, view) {
    let configPhase = Number(LF.StudyDesign.studyPhase[branchObj.branchParams.value]),
        phase       = Number(view.subject.get('phase'));
    return Q(configPhase === phase);
};
/////////////////////////////////////
//Helper Functions
/////////////////////////////////////
LF.Branching.branchFunctions.clearToScreen = function (toScreenID, fromScreenID) {
    //Taken from QuestionnaireView.navigationHandler
    //Get the list of skipped questions
    let view                 = LF.router.view(),
        screenCollection     = view.data.screens,
        screenObj            = screenCollection.at(view.screen),
        isSkippedByBranching = false,
        questionsToClear     = [];
    if(!fromScreenID){
        //Assume current screen
        fromScreenID = screenObj.get('id');
    }
    $(screenCollection.models).each(function (index, screen) {
        /* screenId - screen we are branching from
         * branchObj.branchParams.clearTo - screen we are branching to
         */
        if(fromScreenID === screen.get('id') || toScreenID === screen.get('id')){
            if(!isSkippedByBranching){
                //set flag to true and jump to next screen
                isSkippedByBranching = true;
                return true;
            }else{
                // Breaks the 'each' loop.
                return false;
            }
        }
        if(isSkippedByBranching){
            questionsToClear = questionsToClear.concat(_(screen.get('questions')).pluck('id'));
        }
    });
    view.resetAnswersSelected(questionsToClear);
};
