PDECommon Version info for Syndication app core release 2.3.1:
LABEL   DATE            CHANGES
======  ===========     ============
00.01   18OCT2017       [mblaschak]
                            -cleanup of utils directory and contents
                            -updated grunt/json_wrapper.js to correctly include library nls folder structure
                            -included PDECommon/schedules

                            TODO: clean up webService contents
