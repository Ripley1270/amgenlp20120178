/**
 * Syncs custom historical data with the Web Service using transmission queue.
 * @param {Object} params Includes web service method names.
 */
import Transmission from 'core/models/Transmission';
import Transmissions from 'core/collections/Transmissions';
import ELF from 'core/ELF';
import Data from 'core/Data';
import Logger from 'core/Logger';
let logger = new Logger('customSyncDataRequest');
/**
 * Syncs historical data with the Web Service using transmission queue.
 * @param {Object} params Includes web service method names.
 * @param {boolean} [rejectOnError=false] If true rejects the promise returned
 * @return {Q.Promise<void>}
 */
export function customSyncDataRequest (params, rejectOnError = false) {
    let method = 'customSyncData',
        model  = new Transmission();
    return model.save({
        method,
        params  : JSON.stringify(params),
        created : new Date().getTime()
    })
    // TODO: should we change LF.Collection.Transmissions to transmissions?
    // May be it is there to prevent a circular dependency issue.
        .then(() => LF.Collection.Transmissions.fetchCollection())
        .then(transmissions => {
            let syncTransmission = transmissions.findWhere({method});
            return Q.Promise((resolve, reject) => {
                transmissions.execute(transmissions.indexOf(transmissions.get(syncTransmission)), (response) => {
                    if(response && response.error && rejectOnError){
                        reject();
                    }else{
                        resolve();
                    }
                });
            });
        });
}
ELF.action('customSyncDataRequest', customSyncDataRequest);
