import Logger from 'core/Logger';
LF.Widget.DatePicker.prototype.buildStudyWorksString = function (date) {
    if(!(date instanceof Date)){
        throw new Error('Expected a Date, got ' + date);
    }
    let swDate   = (date.getDate() < 10) ? '0' + date.getDate() : date.getDate(),
        swMonths = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        swMonth  = swMonths[date.getMonth()],
        swYear   = date.getFullYear();
    return (swDate + ' ' + swMonth + ' ' + swYear);
};
LF.Utilities.navigateToScreen = function (screeenID) {
    LF.Data.Questionnaire.screenStack.push(screeenID);
    return LF.Data.Questionnaire.displayScreen(screeenID);
};
/**
 * addIT was removed from LF.Utilities and just plain doesn't work right using the one in BaseReportHandler
 * Added: Syndication 2.1
 */
LF.Utilities.addIT = function (params) {
    let model,
        answer,
        logger = new Logger('Utilities.addIT'),
        view   = LF.Data.Questionnaire;
    if(view instanceof LF.View.QuestionnaireView &&
        params.question_id != null &&
        params.questionnaire_id != null &&
        params.response != null &&
        params.IG != null &&
        params.IGR != null &&
        params.IT != null){
        answer = view.queryAnswersByIGITAndIGR(params.IG, params.IT, params.IGR);
        // if answer already in collection, just modify the response
        if(answer.length){
            answer[0].set('response', String(params.response));
        }else{
            model = new view.Answer({
                question_id      : params.question_id,
                questionnaire_id : params.questionnaire_id,
                response         : String(params.response),
                SW_Alias         : `${params.IG}.${params.IGR}.${params.IT}`
            });
            view.answers.add(model);
        }
    }else{
        logger.error('Attempt failed to insert an IT.');
        throw new Error('Attempt failed to insert an IT.');
    }
};
