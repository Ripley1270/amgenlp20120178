/**
 * checks if the diary has been completed before, callback true if never completed
 * @param {LF.Model.Schedules} schedule object containing schedule configuration.
 * @param {LF.Collection.Dashboards} completedQuestionnaires list of all completed diaries.
 * @param {Object} parentView The view to append this scheduled item to.
 * @param {Object} context an object that contains subject model and visit model (sitepad only)
 * @param {Function} callback A callback function invoked upon.
 * @param {Boolean} isAlarm States if this scheduling is for an alarm
 */
LF.Schedule.schedulingFunctions.checkCompletedOnceAvailability = function (schedule, completedQuestionnaires, parentView, context, callback, isAlarm) {
    callback(!Boolean(
        _.first(
            completedQuestionnaires.where(
                {
                    questionnaire_id : schedule.get('target').id
                }
            )
        )
    ));
};
export default LF.Schedule.schedulingFunctions.checkCompletedOnceAvailability;
