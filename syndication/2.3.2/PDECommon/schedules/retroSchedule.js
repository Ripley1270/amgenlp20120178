LF.Schedule.schedulingFunctions.DailyRetroDiaryAvailability = function (schedule, completedQuestionnaires, parentView, context, callback) {
    //Get last confirmed visit
    LF.Utilities.getBaseTimeForDiary(schedule.get('target').id)
        .then(baseTime => {
            let daysSinceBaseTime,
                requirePracticeCompletion = schedule.get('scheduleParams').requirePracticeCompletion,
                practiceDiary             = _.first(completedQuestionnaires.where({'questionnaire_id' : 'TrainingModule'})),
                now                       = new Date(),
                nowUtc                    = LF.Utilities.convertToUtc(new Date()),
                midnight                  = LF.Utilities.convertToUtc(new Date(new Date().setHours(0, 0, 0, 0))).getTime(),
                startTimeUtc              = LF.Utilities.parseTime(schedule.get('scheduleParams').startAvailability),
                endTimeUtc                = LF.Utilities.parseTime(schedule.get('scheduleParams').endAvailability);
            //Determine days since base time
            if(baseTime){
                daysSinceBaseTime = LF.Utilities.dateDiffInDays(now, baseTime, false);
            }
            if(startTimeUtc >= endTimeUtc){
                if(nowUtc <= startTimeUtc && baseTime && daysSinceBaseTime !== 0){
                    startTimeUtc = startTimeUtc - (24 * 60 * 60 * 1000);
                }else{
                    endTimeUtc = endTimeUtc + (24 * 60 * 60 * 1000);
                }
            }
            //Check if base time (plus any delay) has been passed
            if(baseTime && daysSinceBaseTime >= 0){
                //Check for practice diary completion
                if(!requirePracticeCompletion || practiceDiary){
                    //Get last report date for diary
                    let scheduleId = schedule.get('target').id;
                    LF.Utilities.getLastReportDateForRetro(scheduleId)
                        .then(lastReportDate => {
                            if(lastReportDate && LF.Utilities.dateDiffInDays(baseTime, lastReportDate) > 0){
                                lastReportDate = null;
                            }
                            if(lastReportDate && LF.Utilities.dateDiffInDays(lastReportDate, now, false) === 0){
                                //Today's diary was done; diary is not available
                                callback(false);
                            }else if(lastReportDate && LF.Utilities.dateDiffInDays(lastReportDate, now, false) === -1){
                                //Yesterday's diary was done; only available when start time is reached
                                callback(nowUtc.getTime() >= startTimeUtc && nowUtc.getTime() <= endTimeUtc);
                            }else{
                                //We might be in retro mode
                                if(midnight === endTimeUtc){
                                    endTimeUtc = endTimeUtc + (24 * 60 * 60 * 1000);
                                }
                                //Diary would have been available yesterday; available ||
                                //Diary has never been done; available when start time is reached
                                //else today is first day
                                callback((daysSinceBaseTime > 0 && nowUtc.getTime() >= midnight && nowUtc.getTime() <= endTimeUtc) ||
                                    (!lastReportDate && nowUtc.getTime() >= startTimeUtc && nowUtc.getTime() <= endTimeUtc));
                            }
                        });
                }else{
                    //Still need to do practice diary
                    callback(false);
                }
            }else{
                //Number of days since activation not yet reached
                callback(false);
            }
        });
};
