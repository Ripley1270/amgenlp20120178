IF exists(SELECT *
          FROM dbo.sysobjects
          WHERE id = object_id(N'[dbo].[PDE_Split]') AND OBJECTPROPERTY(id, N'IsTableFunction') = 1)
  DROP FUNCTION [dbo].[PDE_Split]
GO
--http://ole.michelsen.dk/blog/split-string-to-table-using-transact-sql.html
CREATE FUNCTION [dbo].[PDE_Split]
  (
    @String    NVARCHAR(4000),
    @Delimiter NCHAR(1)
  )
  RETURNS TABLE
  AS
  RETURN
  (
  WITH Split(stpos, endpos)
  AS (
    SELECT
      0                              AS stpos,
      CHARINDEX(@Delimiter, @String) AS endpos
    UNION ALL
    SELECT
      endpos + 1,
      CHARINDEX(@Delimiter, @String, endpos + 1)
    FROM Split
    WHERE endpos > 0
  )
  SELECT
      'Id' = ROW_NUMBER()
    OVER (
      ORDER BY (SELECT 1) ),
      'SplitData' = SUBSTRING(@String, stpos, COALESCE(NULLIF(endpos, 0), LEN(@String) + 1) - stpos)
  FROM Split
  )
GO


IF exists(SELECT *
          FROM dbo.sysobjects
          WHERE id = object_id(N'[dbo].[PDE_NetPro_InsertNewWorkMed]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[PDE_NetPro_InsertNewWorkMed]
GO

SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

CREATE PROCEDURE dbo.PDE_NetPro_InsertNewWorkMed
  (@medName               [VARCHAR](36),
   @medNamePart2          [VARCHAR](36),
   @ingredients           [VARCHAR](36),
   @ingredientsPart2      [VARCHAR](36),
   @ingredientsPart3      [VARCHAR](36),
   @doseStrength          [VARCHAR](36),
   @doseStrengthPart2     [VARCHAR](36),
   @delimitedString       [VARCHAR](36), -- doseUnit(MEDUNT1L)|medRoute(MEDRTE1L)|adminForm(MEDFRM1L)
   @siteUser              [VARCHAR](36),
   @delimitedString2      [VARCHAR](36), --@entryDate [varchar] (25), -- entryDaye|subjLang	
   @logTimeClient         [VARCHAR](25),
   @logTimeClientTZOffset [VARCHAR](36))
AS

  SET NOCOUNT ON
  SET ANSI_WARNINGS OFF

  /*****************************************************************************************************************
  AstraZeneca D081DC00007 PDE_NetPro_InsertNewWorkMed: Inserts a new work medication into the Study wide table
  
  Version	    Date		Change
  1.00	    20Jan2017   [jpresto] 
                          - Initial Release
  *****************************************************************************************************************/
  DECLARE @syncdata VARCHAR(MAX)

  DECLARE @ProcedureName VARCHAR(256)
  SELECT @ProcedureName = OBJECT_NAME(@@PROCID)

  DECLARE @Delimiter VARCHAR(1)
  SELECT @Delimiter = '|'

  DECLARE @pendingStatus VARCHAR(1)
  SELECT @pendingStatus = '3'        -- Codelist status for pending


  DECLARE @doseUnit VARCHAR(2)
  DECLARE @medRoute VARCHAR(2)
  DECLARE @adminForm VARCHAR(2)


  DECLARE @splitTable TABLE(id INT, splitValue VARCHAR(36))

  INSERT INTO @splitTable(id, splitValue)
    SELECT
      Id,
      SplitData
    FROM PDE_Split(@delimitedString, '|')

  -- Delimited list order: doseUnit|medRoute|adminForm
  -- pull out values from delimited list
  SELECT TOP 1 @doseUnit = splitValue
  FROM @splitTable
  WHERE id = 1
  SELECT TOP 1 @medRoute = splitValue
  FROM @splitTable
  WHERE id = 2
  SELECT TOP 1 @adminForm = splitValue
  FROM @splitTable
  WHERE id = 3

  DECLARE @entryDate VARCHAR(26)
  DECLARE @subjLang VARCHAR(5)

  DECLARE @splitTable2 TABLE(id INT, splitValue VARCHAR(36))

  INSERT INTO @splitTable2(id, splitValue)
    SELECT
      Id,
      SplitData
    FROM PDE_Split(@delimitedString2, '|')

  SELECT TOP 1 @entryDate = splitValue
  FROM @splitTable2
  WHERE id = 1
  SELECT TOP 1 @subjLang = splitValue
  FROM @splitTable2
  WHERE id = 2

  -- insert query
  INSERT INTO DD_WorkMedication (WRKNAM2C,
                                 WRKNAM1C,
                                 WRKDOS1C,
                                 MEDUNT1L,
                                 MEDRTE1L,
                                 MEDFRM1L,
                                 WRKSIT1C,
                                 WRKENT1S,
                                 RGNLNG1C,
                                 WRKSTT1L)
  VALUES (@medName + @medNamePart2,
          @ingredients + @ingredientsPart2 + @ingredientsPart3,
          @doseStrength + @doseStrengthPart2,
          @doseUnit,
          @medRoute,
          @adminForm,
          @siteUser,
          @entryDate,
          @subjLang,
          @pendingStatus);

  -- Retrieve unique key generated on insert
  DECLARE @WorkRowID VARCHAR(6)
  SELECT @WorkRowID = SCOPE_IDENTITY()

  --Prepend 'WRK' and left pad with zeros
  DECLARE @WorkID VARCHAR(10)
  SET @WorkID = @WorkRowID
  SELECT @WorkID = 'WRK' + REPLACE(STR(@WorkID, 5), SPACE(1), '0')

  -- Insert Temp WRK Code into the table
  UPDATE DD_WorkMedication
  SET WRKCOD1C = @WorkID
  WHERE WorkRowID = @WorkRowID
  -- Return WorkID 
  SELECT @syncdata = isnull(@WorkID, ' ') + @Delimiter + '$$$'

  SELECT @syncdata

  -- Get Version
  DECLARE @Version VARCHAR(10)
  SELECT @Version = ver.procVersion
  FROM PDE_SQLVersion VER WITH ( NOLOCK )
  WHERE VER.procName = @ProcedureName

  -- Add sync data to log
  EXEC [dbo].[insertStudySyncLogs] @ProcedureName, @Version, '', 'NetPro', @logTimeClient, @logTimeClientTZOffset,
                                   @syncdata


GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

IF EXISTS(SELECT 1
          FROM SYSOBJECTS
          WHERE NAME = 'PDE_NetPro_InsertNewWorkMed' AND TYPE = 'P')
  BEGIN
    DECLARE @name AS VARCHAR(50)
    DECLARE @version AS NUMERIC(18, 2)

    SELECT
      @name = 'PDE_NetPro_InsertNewWorkMed',
      @version = 1.00
    EXEC [dbo].[PDE_UpdateSQLVersion] @name, @version

  END

GO

IF NOT exists(SELECT *
              FROM allowed_clin_procs WITH ( NOLOCK )
              WHERE proc_name = 'PDE_NetPro_InsertNewWorkMed')
  INSERT INTO allowed_clin_procs (proc_name) VALUES ('PDE_NetPro_InsertNewWorkMed')

GRANT EXECUTE ON dbo.PDE_NetPro_InsertNewWorkMed TO pht_server_read_only