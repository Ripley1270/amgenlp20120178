/*****************************************************************************************************************
AstraZeneca D081DC00007 PDE_fn_NVarCharToHTMLEncode Function


Version	    Date		  Change

1.00	    20Jan2017     [jpresto] 
                           - Initial Release
						   
*****************************************************************************************************************/

IF exists(SELECT *
          FROM dbo.sysobjects
          WHERE id = object_id(N'[dbo].[PDE_fn_NVarCharToHTMLEncode]') AND OBJECTPROPERTY(id, N'IsScalarFunction') = 1)
  DROP FUNCTION [dbo].[PDE_fn_NVarCharToHTMLEncode]
GO

CREATE FUNCTION DBO.PDE_fn_NVarCharToHTMLEncode(@nstring NVARCHAR(1000))
  RETURNS VARCHAR(8000) AS
  BEGIN
    DECLARE @position INT,
    @Output VARCHAR(8000)
    SET @position = 1
    SELECT @Output = ''
    WHILE @position <= LEN(@nstring)
      BEGIN
        SELECT
          @Output = @Output + '&#' + cast(UNICODE(SUBSTRING(@nstring, @position, 1)) AS VARCHAR(15)) + ';',
          @position = @position + 1
      END
    RETURN @Output
  END
GO

IF EXISTS(SELECT 1
          FROM SYSOBJECTS
          WHERE NAME = 'PDE_fn_NVarCharToHTMLEncode' AND TYPE = 'FN')
  BEGIN
    DECLARE @name AS VARCHAR(50)
    DECLARE @version AS NUMERIC(18, 2)

    SELECT
      @name = 'PDE_fn_NVarCharToHTMLEncode',
      @version = 1.00
    EXEC [dbo].[PDE_UpdateSQLVersion] @name, @version

  END

GO