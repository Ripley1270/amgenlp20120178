IF exists(SELECT *
          FROM dbo.sysobjects
          WHERE id = object_id(N'[dbo].[PDE_NetPro_GetMasterList]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[PDE_NetPro_GetMasterList]
GO

SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

CREATE PROCEDURE dbo.PDE_NetPro_GetMasterList
  (@modifyDate   DATETIME,
   @subjLanguage [VARCHAR](10))
AS

  SET NOCOUNT ON
  SET ANSI_WARNINGS OFF

  /*****************************************************************************************************************
  AstraZeneca D081DC00007 PDE_NetPro_GetMasterList
  
  
  Version	    Date		Change
  1.00        20Jan2017     [jpresto]
                            - Initial Release
  
  1.01        01Feb2017     [jpresto]
                            - Replaced MEDDOS1C to MEDDOS2C				  
  *****************************************************************************************************************/
  DECLARE @syncdata VARCHAR(MAX)

  DECLARE @ProcedureName VARCHAR(256)
  SELECT @ProcedureName = OBJECT_NAME(@@PROCID)

  DECLARE @Delimiter VARCHAR(1)
  SELECT @Delimiter = '|'


  -- Determine the most recent modification date in MasterMed Table
  DECLARE @masterModDate DATETIME

  SELECT TOP 1 @masterModDate = m.MEDEFF1S
  FROM DD_MasterMedication m WITH ( NOLOCK )
  ORDER BY m.MEDEFF1S DESC, m.MEDCOD1C DESC


  -- Determine the most recent modification date in the regional master med table for subject's language
  /*
  Declare @regionModDate DateTime
  
  Select top 1 @regionModDate = m.RGNEFF1S
  from DD_MasterMedicationRegion m with (nolock)
  where m.RGNLNG1L = @subjLanguage
  order by m.RGNEFF1S desc, m.MedRowID desc
  */


  /***************************************************************
   *   If the master medication table or the region table have been 
   *   modified since NetPro last generated master javascript file, 
   *   bring master list to regenerate file
   ***************************************************************/
  IF ((@masterModDate >= '11 SEP 2001')) --or
  --(@regionModDate >= @modifyDate))
    BEGIN
      SELECT @syncdata = (
        SELECT
          @ProcedureName AS '@Version',
          @masterModDate AS '@eff1s',
          '1'            AS '@numMeds',
          (SELECT
             isNull(cast(m.MEDCOD1C AS VARCHAR(10)), ' ')                                                AS '@medCode',
             isNull(dbo.PDE_fn_NVarCharToHTMLEncode(cast(m.MEDNAM1C AS VARCHAR(255))), ' ')              AS '@medName',
             dbo.PDE_fn_NVarCharToHTMLEncode(('*' + isNull(cast(m.MEDNAM1C AS VARCHAR(255)), ' ')))      AS '@rgnName',
             isNull(dbo.PDE_fn_NVarCharToHTMLEncode(
                        cast(RTRIM(Replace(m.MEDDOS2C, CHAR(13) + CHAR(10), '')) AS VARCHAR(255))), ' ') AS '@medDose',
             isNull(cast(m.MEDRTE1L AS VARCHAR), ' ')                                                    AS '@medRoute',
             isNull(cast(m.MEDFRM1L AS VARCHAR), ' ')                                                    AS '@medForm',
             isNull(cast(m.MEDNOT1B AS VARCHAR), '0')                                                    AS '@medNOT1B',
             isNull(cast(m.MEDSTS1L AS VARCHAR), '0')                                                    AS '@MEDSTS1L',
             isNull(cast(m.MEDFLG2B AS VARCHAR), '1')                                                    AS '@MEDFLG2B',
             isNull(cast(m.MEDFRM2L AS VARCHAR), ' ')                                                    AS '@MEDFRM1L'
           FROM DD_MasterMedication m WITH ( NOLOCK )
           WHERE m.MEDCOD1C IS NOT NULL
                 AND ISNUMERIC(m.MEDCOD1C) = 1
           ORDER BY m.MEDCOD1C ASC
           FOR XML PATH ('Med'), TYPE
          )
        FOR XML PATH ('Sync')
      )
    END
  ELSE
    BEGIN
      SELECT @syncdata = (
        SELECT
          @ProcedureName AS '@Version',
          '0'            AS '@numMeds'
        FOR XML PATH ('Sync')
      )
    END

  SELECT @syncdata AS '@syncdata'

  SELECT isnull(@syncdata, '<sync />')

--Select @syncdata = CAST(@@ROWCOUNT as Varchar(5)) + isnull(@DataStream, '') + @Delimiter + '$$$'

--Select @syncdata


GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

IF EXISTS(SELECT 1
          FROM SYSOBJECTS
          WHERE NAME = 'PDE_NetPro_GetMasterList' AND TYPE = 'P')
  BEGIN
    DECLARE @name AS VARCHAR(50)
    DECLARE @version AS NUMERIC(18, 2)

    SELECT
      @name = 'PDE_NetPro_GetMasterList',
      @version = 1.01
    EXEC [dbo].[PDE_UpdateSQLVersion] @name, @version

  END

GO

IF NOT exists(SELECT *
              FROM allowed_clin_procs WITH ( NOLOCK )
              WHERE proc_name = 'PDE_NetPro_GetMasterList')
  INSERT INTO allowed_clin_procs (proc_name) VALUES ('PDE_NetPro_GetMasterList')

GRANT EXECUTE ON dbo.PDE_NetPro_GetMasterList TO pht_server_read_only