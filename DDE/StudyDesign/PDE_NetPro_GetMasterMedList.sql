-- START OF FILE
IF exists(SELECT *
          FROM dbo.sysobjects
          WHERE id = object_id(N'[dbo].[PDE_NetPro_GetMasterMedList]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[PDE_NetPro_GetMasterMedList]
GO

SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

IF OBJECT_ID('tempdb..#tempVersionTable') IS NOT NULL
  DROP TABLE #tempVersionTable

SELECT procVersion = '1.01' -- TODO: UPDATE VERSION
INTO #tempVersionTable

GO

CREATE PROCEDURE dbo.PDE_NetPro_GetMasterMedList
  (@modifyDate DATETIME)
AS

  SET NOCOUNT ON
  SET ANSI_WARNINGS OFF

  /* INSTRUCTIONS: 
  * 1)	Make sure the first line of this file contain "START OF FILE".
  *		Also check if the last line of this file contain "END OF FILE"
  * 2)	Change procVersion ABOVE with each update
  * 3)	Change history BELOW with each update
  **/

  /*****************************************************************************************************************
  AMGEN 20150308 PDE_NetPro_GetMasterMedList
  Version	    Date		Change
  1.00        17APR2017   [schan] Initial Release
  
  1.01        25May2017   [jpresto] ONEECOA-27168: NP - Runtime Error occurs when Subject Medication Selection List is started
  *****************************************************************************************************************/
  DECLARE @syncdata VARCHAR(MAX)

  DECLARE @ProcedureName VARCHAR(256)
  SELECT @ProcedureName = OBJECT_NAME(@@PROCID)

  -- Determine the most recent modification date in MasterMed Table
  DECLARE @masterModDate DATETIME

  SELECT TOP 1 @masterModDate = m.MEDEFF1S
  FROM DD_MasterMedication m WITH ( NOLOCK )
  ORDER BY m.MEDEFF1S DESC

  /***************************************************************
   *   If the master medication table or the region table have been 
   *   modified since NetPro last generated master javascript file, 
   *   bring master list to regenerate file
   ***************************************************************/
  IF (@masterModDate >= @modifyDate)
    BEGIN
      SELECT @syncdata = (
        SELECT
          '1'            AS '@updateList',
          @masterModDate AS '@EFF1S',
          (SELECT
             m.MEDCOD1C                                                   AS '@MEDCOD1C',
             m.MEDGRP1L                                                   AS '@MEDGRP1L',
             dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDNAM1C, CHAR(13) + CHAR(10),
                                                           '')))          AS '@MEDNAM1C',
             dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDDOS2C, CHAR(13) + CHAR(10),
                                                           '')))          AS '@MEDDOS2C',
             dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDUNT1L, CHAR(13) + CHAR(10),
                                                           '')))          AS '@MEDUNT1L',
             m.MEDRTE1L                                                   AS '@MEDRTE1L',
             m.MEDFRM1L                                                   AS '@MEDFRM1L',
             m.MEDSTS1L                                                   AS '@MEDSTS1L',
             m.MEDNOT1B                                                   AS '@MEDNOT1B',
             m.MEDEFF1S                                                   AS '@MEDEFF1S',
             m.MEDFLG1B                                                   AS '@MEDFLG1B',
             m.MEDFLG2B                                                   AS '@MEDFLG2B',
             m.MEDFLG3B                                                   AS '@MEDFLG3B',
             m.MEDFLG4B                                                   AS '@MEDFLG4B',
             m.MEDFLG5B                                                   AS '@MEDFLG5B',
             m.MEDFLG6B                                                   AS '@MEDFLG6B',
             m.MEDFLG7B                                                   AS '@MEDFLG7B',
             m.MEDFLG8B                                                   AS '@MEDFLG8B',
             m.MEDFLG9B                                                   AS '@MEDFLG9B',
             m.MEDVAL1N                                                   AS '@MEDVAL1N',
             m.MEDVAL2N                                                   AS '@MEDVAL2N',
             m.MEDVAL3N                                                   AS '@MEDVAL3N',
             m.MEDVAL4N                                                   AS '@MEDVAL4N',
             m.MEDVAL5N                                                   AS '@MEDVAL5N',
             m.MEDVAL6N                                                   AS '@MEDVAL6N',
             m.MEDVAL7N                                                   AS '@MEDVAL7N',
             m.MEDVAL8N                                                   AS '@MEDVAL8N',
             m.MEDVAL9N                                                   AS '@MEDVAL9N',
             CASE WHEN m.MEDLBL1C IS NULL
               THEN NULL
             ELSE dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDLBL1C, CHAR(13) + CHAR(10),
                                                                ''))) END AS '@MEDLBL1C',
             CASE WHEN m.MEDLBL2C IS NULL
               THEN NULL
             ELSE dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDLBL2C, CHAR(13) + CHAR(10),
                                                                ''))) END AS '@MEDLBL2C',
             CASE WHEN m.MEDLBL3C IS NULL
               THEN NULL
             ELSE dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDLBL3C, CHAR(13) + CHAR(10),
                                                                ''))) END AS '@MEDLBL3C',
             CASE WHEN m.MEDLBL4C IS NULL
               THEN NULL
             ELSE dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDLBL4C, CHAR(13) + CHAR(10),
                                                                ''))) END AS '@MEDLBL4C',
             CASE WHEN m.MEDLBL5C IS NULL
               THEN NULL
             ELSE dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDLBL5C, CHAR(13) + CHAR(10),
                                                                ''))) END AS '@MEDLBL5C',
             CASE WHEN m.MEDLBL6C IS NULL
               THEN NULL
             ELSE dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDLBL6C, CHAR(13) + CHAR(10),
                                                                ''))) END AS '@MEDLBL6C',
             CASE WHEN m.MEDLBL7C IS NULL
               THEN NULL
             ELSE dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDLBL7C, CHAR(13) + CHAR(10),
                                                                ''))) END AS '@MEDLBL7C',
             CASE WHEN m.MEDLBL8C IS NULL
               THEN NULL
             ELSE dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDLBL8C, CHAR(13) + CHAR(10),
                                                                ''))) END AS '@MEDLBL8C',
             CASE WHEN m.MEDLBL9C IS NULL
               THEN NULL
             ELSE dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDLBL9C, CHAR(13) + CHAR(10),
                                                                ''))) END AS '@MEDLBL9C'
           FROM DD_MasterMedication m WITH ( NOLOCK )
           WHERE m.MEDCOD1C IS NOT NULL
                 --and m.MEDSTS1L = 1
                 AND ISNUMERIC(m.MEDCOD1C) = 1
           --ORDER BY CAST(m.MEDCOD1C AS NUMERIC) ASC
           FOR XML PATH ('med'), TYPE)
        FOR XML PATH ('mastermeds')
      )
    END
  ELSE
    BEGIN
      SELECT @syncdata = (
        SELECT '0' AS '@updateList'
        FOR XML PATH ('mastermeds')
      )
    END

  SELECT @syncdata AS '@syncdata'

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

IF EXISTS(SELECT 1
          FROM SYSOBJECTS
          WHERE NAME = 'PDE_NetPro_GetMasterMedList' AND TYPE = 'P')
  BEGIN

    DECLARE @name AS VARCHAR(50)
    SELECT @name = 'PDE_NetPro_GetMasterMedList'

    IF OBJECT_ID('tempdb..#tempVersionTable') IS NULL
      BEGIN
        RAISERROR (N'WARNING: SQL version is missing. Please verify version is properly defined in [%s] stored procedure.',
        11, 1, @name);
      END
    ELSE
      BEGIN
        DECLARE @version AS NUMERIC(18, 2)
        SELECT @version = procVersion
        FROM #tempVersionTable
        EXEC [dbo].[PDE_UpdateSQLVersion] @name, @version
        DROP TABLE #tempVersionTable
      END
  END

GO

-- Set Permissions
IF NOT exists(SELECT *
              FROM allowed_clin_procs WITH ( NOLOCK )
              WHERE proc_name = 'PDE_NetPro_GetMasterMedList')
  INSERT INTO allowed_clin_procs (proc_name) VALUES ('PDE_NetPro_GetMasterMedList')

GRANT EXECUTE ON dbo.PDE_NetPro_GetMasterMedList TO pht_server_read_only
-- END OF FILE