IF exists(SELECT *
          FROM dbo.sysobjects
          WHERE id = object_id(N'[dbo].[PDE_LPAFullSyncSQLV1]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[PDE_LPAFullSyncSQLV1]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.PDE_LPAFullSyncSQLV1
  (@krPT                  VARCHAR(255),
   @deviceID              VARCHAR(36),
   @logTimeClient         VARCHAR(64),
   @logTimeClientTZOffset VARCHAR(36))
AS

/*****************************************************************************************************************
AstraZeneca D081DC00007 PDE_LPAFullSyncSQLV1 - Full custom sync SQL
Version     Date        Change

1.01        12Feb2018   [jcarr]
              -Updates
1.00        13Sep2017   [jcarr]
							- initial release
*****************************************************************************************************************/

  DECLARE @ProcedureName VARCHAR(256)
  SELECT @ProcedureName = OBJECT_NAME(@@PROCID)

  DECLARE @customSync VARCHAR(MAX)
  DECLARE @customSyncXML XML

  /*******************************************************
  * Insert Custom SQL Here
  *******************************************************/
  --Get Evening Collection data
  DECLARE @DailyDiaries TABLE(
    diaryDate VARCHAR(36)
  )

  INSERT INTO @DailyDiaries
    SELECT su.ReportStartDate AS diaryDate
    FROM ig_DailyDiary ig
      JOIN lookup_su su WITH ( NOLOCK )
        ON ig.SIGORIG = su.SIGORIG
    WHERE ig.KRPT = @krpt
          AND su.deleted = '0'
    ORDER BY su.ReportStartDate DESC

  --Get Evening Collection data
  DECLARE @visitTable TABLE(
    visitDate VARCHAR(36),
    visitID   VARCHAR(36)
  )

  INSERT INTO @visitTable
    SELECT
      su.ReportStartDate AS visitDate,
      vc.VST1L           AS visitID
    FROM ig_VisitConfirmation vc
      JOIN lookup_su su WITH ( NOLOCK )
        ON vc.SIGORIG = su.SIGORIG
    WHERE vc.KRPT = @krpt
          AND su.deleted = '0'
    ORDER BY su.ReportStartDate DESC

  --Get Headache Collection data
  DECLARE @headaches TABLE(
    headacheID VARCHAR(36),
    startDate  VARCHAR(36),
    endDate    VARCHAR(36)
  )

  DECLARE @ongoingHeadaches TABLE(
    headacheID VARCHAR(36),
    startDate  VARCHAR(36),
    endDate    VARCHAR(36)
  )

  DECLARE @endedHeadaches TABLE(
    headacheID VARCHAR(36),
    startDate  VARCHAR(36),
    endDate    VARCHAR(36)
  )

  INSERT INTO @endedHeadaches
    SELECT
      ha.HEDNUM1N                                           AS headacheID,
      ha.HEDBEG1S                                           AS startDate,
      ISNULL(ha.ENDHED1S, ISNULL(ha.FELSLP1S, ha.UNKEND1B)) AS endDate
    FROM ig_Headache ha
      JOIN lookup_su su WITH ( NOLOCK )
        ON ha.SIGORIG = su.SIGORIG
    WHERE ha.KRPT = @krpt
          AND su.deleted = '0'
          AND ha.HEDNUM1N IS NOT NULL
          --AND ha.HEDBEG1S IS NULL
          AND (ha.ENDHED1S IS NOT NULL OR ha.FELSLP1S IS NOT NULL OR ha.UNKEND1B IS NOT NULL)
    ORDER BY su.ReportStartDate DESC

  INSERT INTO @ongoingHeadaches
    SELECT
      ha.HEDNUM1N                                           AS headacheID,
      ha.HEDBEG1S                                           AS startDate,
      ISNULL(ha.ENDHED1S, ISNULL(ha.FELSLP1S, ha.UNKEND1B)) AS endDate
    FROM ig_Headache ha
      JOIN lookup_su su WITH ( NOLOCK )
        ON ha.SIGORIG = su.SIGORIG
    WHERE ha.KRPT = @krpt
          AND su.deleted = '0'
          AND ha.HEDNUM1N IS NOT NULL
          AND (ha.ENDHED1S IS NULL AND ha.FELSLP1S IS NULL AND ha.UNKEND1B IS NULL)
    ORDER BY su.ReportStartDate DESC

  INSERT INTO @headaches
    SELECT
      ha.HEDNUM1N                                           AS headacheID,
      ha.HEDBEG1S                                           AS startDate,
      ISNULL(ha.ENDHED1S, ISNULL(ha.FELSLP1S, ha.UNKEND1B)) AS endDate
    FROM ig_Headache ha
      JOIN lookup_su su WITH ( NOLOCK )
        ON ha.SIGORIG = su.SIGORIG
    WHERE ha.KRPT = @krpt
          AND su.deleted = '0'
          AND ha.HEDNUM1N IS NOT NULL
          AND (ha.ENDHED1S IS NOT NULL OR ha.FELSLP1S IS NOT NULL OR ha.UNKEND1B IS NOT NULL)
          AND ha.HEDBEG1S IS NOT NULL
    ORDER BY su.ReportStartDate DESC

  INSERT INTO @headaches
    SELECT
      igo.headacheID                         AS headacheID,
      COALESCE(igo.startDate, ige.startDate) AS startDate,
      COALESCE(ige.endDate, igo.endDate, '') AS endDate
    FROM @ongoingHeadaches igo
      LEFT JOIN @endedHeadaches ige
        ON ige.headacheID = igo.headacheID

  SELECT @customSyncXML = (SELECT
                             (SELECT (SELECT dd.diaryDate AS '@date'
                                      FROM @DailyDiaries dd
                                      ORDER BY CAST(dd.diaryDate AS DATETIME) ASC
                                      FOR XML PATH ('DailyDiary'), TYPE)
                              FOR XML PATH ('DailyDiaries'), TYPE),
                             (SELECT (SELECT
                                        vc.visitDate AS '@visitDate',
                                        vc.visitID   AS '@visit'
                                      FROM @visitTable vc
                                      ORDER BY CAST(vc.visitDate AS DATETIME) ASC
                                      FOR XML PATH ('VisitModel'), TYPE)
                              FOR XML PATH ('VisitCollection'), TYPE),
                             (SELECT (SELECT
                                        ha.headacheID AS '@headacheID',
                                        ha.startDate  AS '@startDate',
                                        ha.endDate    AS '@endDate'
                                      FROM @headaches ha
                                      WHERE ha.startDate IS NOT NULL
                                            AND ha.headacheID IS NOT NULL
                                      ORDER BY ha.headacheID ASC
                                      FOR XML PATH ('Headache'), TYPE)
                              FOR XML PATH ('Headaches'), TYPE)
                           FOR XML PATH ('CustomSync')
  )

  SELECT @customSyncXML
  SELECT @customSync = cast(@customSyncXML AS VARCHAR(MAX))

  -- Get Version
  DECLARE @Version VARCHAR(10)
  SELECT @Version = ver.procVersion
  FROM PDE_SQLVersion VER WITH ( NOLOCK )
  WHERE VER.procName = @ProcedureName

  -- Add sync data to log
  EXEC [dbo].[insertStudySyncLogs] @ProcedureName, @Version, @krpt, @deviceID, @logTimeClient, @logTimeClientTZOffset,
                                   @customSync

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

IF EXISTS(SELECT 1
          FROM SYSOBJECTS
          WHERE NAME = 'PDE_LPAFullSyncSQLV1' AND TYPE = 'P')
  BEGIN
    DECLARE @name AS VARCHAR(50)
    DECLARE @version AS NUMERIC(18, 2)

    SELECT
      @name = 'PDE_LPAFullSyncSQLV1',
      @version = 1.04 -- UPDATE VERSION NUMBER HERE
    EXEC [dbo].[PDE_UpdateSQLVersion] @name, @version
  END

GO

-- Set Permissions
IF NOT exists(SELECT *
              FROM allowed_clin_procs WITH ( NOLOCK )
              WHERE proc_name = 'PDE_LPAFullSyncSQLV1')
  INSERT INTO allowed_clin_procs (proc_name) VALUES ('PDE_LPAFullSyncSQLV1')

GRANT EXECUTE ON dbo.PDE_LPAFullSyncSQLV1 TO pht_server_read_only