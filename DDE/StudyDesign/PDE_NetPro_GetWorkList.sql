IF exists(SELECT *
          FROM dbo.sysobjects
          WHERE id = object_id(N'[dbo].[PDE_NetPro_GetWorkList]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[PDE_NetPro_GetWorkList]
GO

SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

CREATE PROCEDURE dbo.PDE_NetPro_GetWorkList
  (@modifyDate DATETIME)
AS

  SET NOCOUNT ON
  SET ANSI_WARNINGS OFF

  /*****************************************************************************************************************
  AstraZeneca D081DC00007 PDE_NetPro_GetWorkList
  
  
  Version	    Date		  Change
  1.00	    20Jan2017     [jpresto] 
                             - Initial Release
                 
  *****************************************************************************************************************/
  DECLARE @syncdata VARCHAR(MAX)

  DECLARE @ProcedureName VARCHAR(256)
  SELECT @ProcedureName = OBJECT_NAME(@@PROCID)

  DECLARE @Delimiter VARCHAR(1)
  SELECT @Delimiter = '|'


  -- Determine the most recent entry date in the DD_WorkMedication Table
  DECLARE @workModDT DATETIME

  SELECT TOP 1 @workModDT = wm.WRKENT1S
  FROM DD_WorkMedication wm WITH ( NOLOCK )
  ORDER BY cast(wm.WRKENT1S AS DATETIME) DESC


  DECLARE @EMPTY VARCHAR(2)
  SELECT @EMPTY = ' '

  DECLARE @TmpWorkTable TABLE
  (
    WRKCOD1C VARCHAR(50),
    WRKNAM1C VARCHAR(80),
    WRKNAM2C VARCHAR(60),
    WRKDOS1C VARCHAR(50),
    MEDRTE1L INT,
    MEDFRM1L INT,
    MEDCOD1C INT
  )

  ----IF (@workModDT >= @modifyDate)
  --IF (@workModDT >= '11 SEP 2001')
  --  BEGIN
  INSERT INTO @TmpWorkTable
    SELECT
      wm.WRKCOD1C,
      wm.WRKNAM1C,
      replace(wm.WRKNAM2C, '"', ''),
      replace(wm.WRKDOS1C, '"', ''),
      wm.MEDRTE1L,
      wm.MEDFRM1L,
      wm.MEDCOD1C
    FROM DD_WorkMedication wm WITH ( NOLOCK )
    ORDER BY wm.WorkRowID ASC
  --END


  -- Get number of work meds
  DECLARE @numMeds VARCHAR(4)

  SELECT @numMeds = count(*)
  FROM @TmpWorkTable

  --Build up xml
  SELECT @syncdata = (
    SELECT
      @ProcedureName AS '@Version',
      @workModDT     AS '@workModDT',
      @numMeds       AS '@numMeds',
      (SELECT
         wm.WRKCOD1C                                  AS '@medCode',
         dbo.PDE_fn_NVarCharToHTMLEncode(wm.WRKNAM1C) AS '@genName',
         dbo.PDE_fn_NVarCharToHTMLEncode(wm.WRKNAM2C) AS '@medName',
         dbo.PDE_fn_NVarCharToHTMLEncode(wm.WRKDOS1C) AS '@medDose',
         wm.MEDRTE1L                                  AS '@medRoute',
         wm.MEDFRM1L                                  AS '@medForm',
         wm.MEDCOD1C                                  AS '@medID'
       FROM @TmpWorkTable wm
       FOR XML PATH ('Med'), TYPE
      )
    FOR XML PATH ('Sync')
  )

  SELECT @syncdata AS '@syncdata'
  SELECT isnull(@syncdata, '<sync />')


GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

IF EXISTS(SELECT 1
          FROM SYSOBJECTS
          WHERE NAME = 'PDE_NetPro_GetWorkList' AND TYPE = 'P')
  BEGIN
    DECLARE @name AS VARCHAR(50)
    DECLARE @version AS NUMERIC(18, 2)

    SELECT
      @name = 'PDE_NetPro_GetWorkList',
      @version = 1.00
    EXEC [dbo].[PDE_UpdateSQLVersion] @name, @version

  END

GO

IF NOT exists(SELECT *
              FROM allowed_clin_procs WITH ( NOLOCK )
              WHERE proc_name = 'PDE_NetPro_GetWorkList')
  INSERT INTO allowed_clin_procs (proc_name) VALUES ('PDE_NetPro_GetWorkList')

GRANT EXECUTE ON dbo.PDE_NetPro_GetWorkList TO pht_server_read_only