IF exists(SELECT *
          FROM dbo.sysobjects
          WHERE id = object_id(N'[dbo].[PDE_LPASubjMasterMedListSyncSQLV1]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[PDE_LPASubjMasterMedListSyncSQLV1]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

IF OBJECT_ID('tempdb..#tempVersionTable') IS NOT NULL
  DROP TABLE #tempVersionTable

SELECT procVersion = '1.00' -- TODO: UPDATE VERSION
INTO #tempVersionTable

GO

CREATE PROCEDURE dbo.PDE_LPASubjMasterMedListSyncSQLV1
  (@krpt                        [VARCHAR](255) = '',
   @deviceID                    [VARCHAR](36),
   @logTimeClient               [VARCHAR](25),
   @logTimeClientTZOffset       [VARCHAR](36),
   @MasterMedListEffectiveDate  DATETIME = NULL,
   @SubjectMedListEffectiveDate DATETIME = NULL)
AS

  SET NOCOUNT ON
  SET ANSI_WARNINGS ON

  /*****************************************************************************************************************
  AstraZeneca D081DC00007 PDE_LPASubjMasterMedListSyncSQLV1
  Version	Date		Change
  1.00	20JAN2017	[jpresto] Initial Release

  1.01	01Feb2017	[jpresto]	Replaced MEDDOS1C to MEDDOS2C
  2.00	09MAR2017	[schan] Fixed INSERT INTO with ORDER BY
  *****************************************************************************************************************/
  DECLARE @ProcedureName VARCHAR(256)
  SELECT @ProcedureName = OBJECT_NAME(@@PROCID)

  DECLARE @syncdata VARCHAR(MAX)
  /*******************************************************
  * Insert Custom SQL Here
  *******************************************************/

  DECLARE @syncXML XML
  DECLARE @subjectMedsXML XML
  DECLARE @masterMedsXML XML
  DECLARE @updateSubjectMeds VARCHAR(1)
  DECLARE @updateMasterMeds VARCHAR(1)

  /*****************************
  * Subject Meds
  *****************************/
  DECLARE @SBJEFF1S VARCHAR(36)
  DECLARE @SubjectMedTable TABLE(id INT IDENTITY, MEDCOD1C VARCHAR(10), MEDALS1C NVARCHAR(255))
  DECLARE @latestSigorig VARCHAR(25)

  -- Get the SigOrig from the latest SubjectMedList report
  SELECT TOP 1
    @latestSigorig = su.SIGORIG,
    @SBJEFF1S = ig.SBJEFF1S
  FROM ig_SubjectMedListSingle ig WITH ( NOLOCK )
    JOIN lookup_su su WITH ( NOLOCK )
      ON ig.SIGORIG = su.sigorig
  WHERE su.krpt = @krpt
        AND su.deleted = 0
  ORDER BY CAST(su.ReportStartDate_mirror AS DATETIME) DESC

  -- If last Medication Selection is newer than the subject med list on the device
  IF (@SubjectMedListEffectiveDate IS NULL OR
      @SubjectMedListEffectiveDate < cast(@SBJEFF1S AS DATETIME))
    BEGIN
      SELECT @updateSubjectMeds = '1'

      -- Get Subject Meds
      INSERT INTO @SubjectMedTable(MEDCOD1C, MEDALS1C)
        SELECT
          ig.MEDCOD1C,
          ig.MEDALS1C
        FROM ig_SubjectMed ig WITH ( NOLOCK )
        WHERE ig.MEDCOD1C IS NOT NULL
              AND ig.MEDALS1C IS NOT NULL
              AND (ig.Deleted IS NULL OR ig.Deleted = 0)
              AND (ig.Deleted IS NULL OR ig.Deleted = 0)
              AND ig.SIGORIG = @latestSigorig
        ORDER BY ig.KRIGR
    END
  ELSE
    BEGIN
      SELECT @updateSubjectMeds = '0'
    END

  SELECT @subjectMedsXML = (SELECT
                              @SBJEFF1S AS '@EffectiveDate',
                              (SELECT
                                 MEDCOD1C                                            AS '@MEDCOD1C',
                                 dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(MEDALS1C, CHAR(13) + CHAR(10),
                                                                               ''))) AS '@MEDALS1C'
                               FROM @SubjectMedTable
                               ORDER BY id ASC
                               FOR XML PATH ('subjectmed'), TYPE)
                            FOR XML PATH ('subjectmeds'), TYPE)

  /*****************************
  * Master Meds
  *****************************/
  DECLARE @numNewMeds INT
  DECLARE @EffectiveDate VARCHAR(255)

  -- Get the number of new meds since the last update
  SELECT
    @numNewMeds = COUNT(*),
    @EffectiveDate = MAX(convert(VARCHAR, m.MEDEFF1S, 126))
  FROM DD_MasterMedication m WITH ( NOLOCK )
  WHERE m.MEDSTS1L = 1 AND
        (@MasterMedListEffectiveDate IS NULL OR
         m.MEDEFF1S > @MasterMedListEffectiveDate)

  IF (@numNewMeds > 0)
    BEGIN
      SELECT @updateMasterMeds = '1'

      SELECT @masterMedsXML = (
        SELECT
          @EffectiveDate AS '@EffectiveDate',
          (SELECT
             m.MEDCOD1C                                                   AS '@MEDCOD1C',
             m.MEDGRP1L                                                   AS '@MEDGRP1L',
             dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDNAM1C, CHAR(13) + CHAR(10),
                                                           '')))          AS '@MEDNAM1C',
             dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDDOS2C, CHAR(13) + CHAR(10),
                                                           '')))          AS '@MEDDOS1C',
             --dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDUNT1L, CHAR(13) + CHAR(10), ''))) as '@MEDUNT1L',
             m.MEDRTE1L                                                   AS '@MEDRTE1L',
             m.MEDFRM1L                                                   AS '@MEDFRM1L',
             m.MEDFRM2L                                                   AS '@MEDFRM2L',
             m.MEDFLG1B                                                   AS '@MEDFLG1B',
             m.MEDFLG2B                                                   AS '@MEDFLG2B',
             m.MEDFLG3B                                                   AS '@MEDFLG3B',
             m.MEDFLG4B                                                   AS '@MEDFLG4B',
             m.MEDFLG5B                                                   AS '@MEDFLG5B',
             m.MEDFLG6B                                                   AS '@MEDFLG6B',
             m.MEDFLG7B                                                   AS '@MEDFLG7B',
             m.MEDFLG8B                                                   AS '@MEDFLG8B',
             m.MEDFLG9B                                                   AS '@MEDFLG9B',
             m.MEDVAL1N                                                   AS '@MEDVAL1N',
             m.MEDVAL2N                                                   AS '@MEDVAL2N',
             m.MEDVAL3N                                                   AS '@MEDVAL3N',
             m.MEDVAL4N                                                   AS '@MEDVAL4N',
             m.MEDVAL5N                                                   AS '@MEDVAL5N',
             m.MEDVAL6N                                                   AS '@MEDVAL6N',
             m.MEDVAL7N                                                   AS '@MEDVAL7N',
             m.MEDVAL8N                                                   AS '@MEDVAL8N',
             m.MEDVAL9N                                                   AS '@MEDVAL9N',
             CASE WHEN m.MEDLBL1C IS NULL
               THEN NULL
             ELSE dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDLBL1C, CHAR(13) + CHAR(10),
                                                                ''))) END AS '@MEDLBL1C',
             CASE WHEN m.MEDLBL2C IS NULL
               THEN NULL
             ELSE dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDLBL2C, CHAR(13) + CHAR(10),
                                                                ''))) END AS '@MEDLBL2C',
             CASE WHEN m.MEDLBL3C IS NULL
               THEN NULL
             ELSE dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDLBL3C, CHAR(13) + CHAR(10),
                                                                ''))) END AS '@MEDLBL3C',
             CASE WHEN m.MEDLBL4C IS NULL
               THEN NULL
             ELSE dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDLBL4C, CHAR(13) + CHAR(10),
                                                                ''))) END AS '@MEDLBL4C',
             CASE WHEN m.MEDLBL5C IS NULL
               THEN NULL
             ELSE dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDLBL5C, CHAR(13) + CHAR(10),
                                                                ''))) END AS '@MEDLBL5C',
             CASE WHEN m.MEDLBL6C IS NULL
               THEN NULL
             ELSE dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDLBL6C, CHAR(13) + CHAR(10),
                                                                ''))) END AS '@MEDLBL6C',
             CASE WHEN m.MEDLBL7C IS NULL
               THEN NULL
             ELSE dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDLBL7C, CHAR(13) + CHAR(10),
                                                                ''))) END AS '@MEDLBL7C',
             CASE WHEN m.MEDLBL8C IS NULL
               THEN NULL
             ELSE dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDLBL8C, CHAR(13) + CHAR(10),
                                                                ''))) END AS '@MEDLBL8C',
             CASE WHEN m.MEDLBL9C IS NULL
               THEN NULL
             ELSE dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDLBL9C, CHAR(13) + CHAR(10),
                                                                ''))) END AS '@MEDLBL9C'
           FROM DD_MasterMedication m WITH ( NOLOCK )
           WHERE m.MEDSTS1L = 1 AND
                 (@MasterMedListEffectiveDate IS NULL OR
                  m.MEDEFF1S > @MasterMedListEffectiveDate)
                 AND ISNUMERIC(m.MEDCOD1C) = 1
           --order by cast(m.MEDCOD1C as numeric) asc
           FOR XML PATH ('med'), TYPE)
        FOR XML PATH ('mastermeds')
      )
    END
  ELSE
    BEGIN
      SELECT @updateMasterMeds = '0'
    END

  -- Results

  SELECT @syncXML =
         (SELECT
            @updateSubjectMeds AS '@updateSubjectMeds',
            @updateMasterMeds  AS '@updateMasterMeds'
          FOR XML PATH ('sync'))

  IF (@updateSubjectMeds = '1')
    BEGIN
      -- Append subject meds
      SET @syncXML.modify('insert sql:variable("@subjectMedsXML")
					into (/sync)[1]')
    END

  IF (@updateMasterMeds = '1')
    BEGIN
      -- Append master meds
      SET @syncXML.modify('insert sql:variable("@masterMedsXML")
					into (/sync)[1]')
    END

  -- Set syncData
  SELECT @syncData = cast(@syncXML AS VARCHAR(MAX))

  -- Return syncData
  SELECT @syncData

  -- Get Version
  DECLARE @Version VARCHAR(10)
  SELECT @Version = ver.procVersion
  FROM PDE_SQLVersion VER WITH ( NOLOCK )
  WHERE VER.procName = @ProcedureName

  -- Add sync data to log
  EXEC [dbo].[insertStudySyncLogs] @ProcedureName, @Version, @krpt, @deviceID, @logTimeClient, @logTimeClientTZOffset,
                                   @syncdata

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

IF EXISTS(SELECT 1
          FROM SYSOBJECTS
          WHERE NAME = 'PDE_LPASubjMasterMedListSyncSQLV1' AND TYPE = 'P')
  BEGIN

    DECLARE @name AS VARCHAR(50)
    SELECT @name = 'PDE_LPASubjMasterMedListSyncSQLV1'

    IF OBJECT_ID('tempdb..#tempVersionTable') IS NULL
      BEGIN
        RAISERROR (N'WARNING: SQL version is missing. Please verify version is properly defined in [%s] stored procedure.',
        11, 1, @name);
      END
    ELSE
      BEGIN
        DECLARE @version AS NUMERIC(18, 2)
        SELECT @version = procVersion
        FROM #tempVersionTable
        EXEC [dbo].[PDE_UpdateSQLVersion] @name, @version
        DROP TABLE #tempVersionTable
      END
  END

GO

IF NOT exists(SELECT *
              FROM allowed_clin_procs WITH ( NOLOCK )
              WHERE proc_name = 'PDE_LPASubjMasterMedListSyncSQLV1')
  INSERT INTO allowed_clin_procs (proc_name) VALUES ('PDE_LPASubjMasterMedListSyncSQLV1')

GRANT EXECUTE ON dbo.PDE_LPASubjMasterMedListSyncSQLV1 TO pht_server_read_only