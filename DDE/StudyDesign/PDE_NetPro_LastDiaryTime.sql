IF exists(SELECT *
          FROM dbo.sysobjects
          WHERE id = object_id(N'[dbo].[PDE_NetPro_LastDiaryTime]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[PDE_NetPro_LastDiaryTime]
GO

SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

CREATE PROCEDURE dbo.PDE_NetPro_LastDiaryTime
  (@krpt [VARCHAR](255) = '')
AS

  SET NOCOUNT ON
  SET ANSI_WARNINGS OFF

  /*****************************************************************************************************************
  AstraZeneca D081DC00007 Returns the last Report Time for diaries PDE_NetPro_LastDiaryTime
  
  
  Version	    Date		Change
  1.00	20Jan2017     [jpresto] Initial Release
  
  
  *****************************************************************************************************************/
  DECLARE @syncdata VARCHAR(MAX)

  DECLARE @ProcedureName VARCHAR(256)
  SELECT @ProcedureName = OBJECT_NAME(@@PROCID)

  DECLARE @EMPTY VARCHAR(2)
  SELECT @EMPTY = ' '


  DECLARE @tmpDiaryInfo TABLE(
    KRSU            VARCHAR(36),
    KRSUR           VARCHAR(36),
    ReportStartDate VARCHAR(20)
  )

  INSERT INTO @tmpDiaryInfo (KRSU, KRSUR, ReportStartDate)
    SELECT
      isNull(KRSU, ''),
      isNull(Cast(Max(Cast(KRSUR AS INT)) AS VARCHAR(36)), '0'),
      isNull(Convert(VARCHAR(20), MAX(Cast(ReportStartDate AS DATETIME)), 113), '')
    FROM lookup_SU SU WITH ( NOLOCK )
    WHERE SU.Krpt = @krpt AND
          su.Deleted = 0 AND
          krse != 'SystemVariables' AND
          ReportStartDate IS NOT NULL
    GROUP BY KRSU

  -- Subject's current phase and phase start time
  DECLARE @currentPhase VARCHAR(36)
  DECLARE @phaseDate VARCHAR(20)

  SELECT TOP 1
    @currentPhase = pt.krphase,
    @phaseDate = Convert(VARCHAR(20), Cast(pt.phasedate AS DATETIME), 113)
  FROM lookup_pt pt WITH ( NOLOCK )
  WHERE pt.krpt = @krpt
        AND pt.deleted = 0
  ORDER BY cast(pt.enrolldate AS DATETIME) DESC



  --Build up xml
  SELECT @syncdata = (
    SELECT
      @ProcedureName AS '@Version',
      @currentPhase  AS '@krphase',
      @phaseDate     AS '@phaseStartDate',
      (SELECT
         KRSU            AS '@KRSU',
         KRSUR           AS '@KRSUR',
         ReportStartDate AS '@lastTS'
       FROM @tmpDiaryInfo
       FOR XML PATH ('Diary'), TYPE
      )
    FOR XML PATH ('Sync')
  )


  SELECT @syncdata AS '@syncdata'
  SELECT isnull(@syncdata, '<sync />')

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

IF EXISTS(SELECT 1
          FROM SYSOBJECTS
          WHERE NAME = 'PDE_NetPro_LastDiaryTime' AND TYPE = 'P')
  BEGIN
    DECLARE @name AS VARCHAR(50)
    DECLARE @version AS NUMERIC(18, 2)

    SELECT
      @name = 'PDE_NetPro_LastDiaryTime',
      @version = 1.00
    EXEC [dbo].[PDE_UpdateSQLVersion] @name, @version

  END

GO

IF NOT exists(SELECT *
              FROM allowed_clin_procs WITH ( NOLOCK )
              WHERE proc_name = 'PDE_NetPro_LastDiaryTime')
  INSERT INTO allowed_clin_procs (proc_name) VALUES ('PDE_NetPro_LastDiaryTime')

GRANT EXECUTE ON dbo.PDE_NetPro_LastDiaryTime TO pht_server_read_only