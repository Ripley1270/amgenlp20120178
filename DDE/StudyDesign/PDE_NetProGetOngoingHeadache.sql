IF exists(SELECT *
          FROM dbo.sysobjects
          WHERE id = object_id(N'[dbo].[PDE_NetProGetOngoingHeadache]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[PDE_NetProGetOngoingHeadache]
GO

SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

CREATE PROCEDURE dbo.PDE_NetProGetOngoingHeadache
  (@krpt                  VARCHAR(255),
   @deviceID              [VARCHAR](10),
   @logTimeClient         [VARCHAR](25),
   @logTimeClientTZOffset [VARCHAR](36))
AS

  SET NOCOUNT ON
  SET ANSI_WARNINGS OFF

  /*****************************************************************************************************************
  AstraZeneca D081DC00007 PDE_NetProGetOngoingHeadache - Full custom sync SQL
  Version     Date        Change

  1.01        12Feb2018   [jcarr]
                -Updates
  1.00        13Sep2017   [jcarr]
                - initial release
  *****************************************************************************************************************/

  DECLARE @ProcedureName VARCHAR(256)
  SELECT @ProcedureName = OBJECT_NAME(@@PROCID)

  DECLARE @customSync VARCHAR(MAX)
  DECLARE @customSyncXML XML

  /*******************************************************
  * Insert Custom SQL Here
  *******************************************************/
  --Get Headache Collection data
  DECLARE @headaches TABLE(
    headacheID VARCHAR(36),
    startDate  VARCHAR(36),
    endDate    VARCHAR(36)
  )

  DECLARE @ongoingHeadaches TABLE(
    headacheID VARCHAR(36),
    startDate  VARCHAR(36),
    endDate    VARCHAR(36)
  )

  DECLARE @endedHeadaches TABLE(
    headacheID VARCHAR(36),
    startDate  VARCHAR(36),
    endDate    VARCHAR(36)
  )

  INSERT INTO @endedHeadaches
    SELECT
      ha.HEDNUM1N                                           AS headacheID,
      ha.HEDBEG1S                                           AS startDate,
      ISNULL(ha.ENDHED1S, ISNULL(ha.FELSLP1S, ha.UNKEND1B)) AS endDate
    FROM ig_Headache ha
      JOIN lookup_su su WITH ( NOLOCK )
        ON ha.SIGORIG = su.SIGORIG
    WHERE ha.KRPT = @krpt
          AND su.deleted = '0'
          AND ha.HEDNUM1N IS NOT NULL
          --AND ha.HEDBEG1S IS NULL
          AND (ha.ENDHED1S IS NOT NULL OR ha.FELSLP1S IS NOT NULL OR ha.UNKEND1B IS NOT NULL)
    ORDER BY su.ReportStartDate DESC

  INSERT INTO @ongoingHeadaches
    SELECT
      ha.HEDNUM1N                                           AS headacheID,
      ha.HEDBEG1S                                           AS startDate,
      ISNULL(ha.ENDHED1S, ISNULL(ha.FELSLP1S, ha.UNKEND1B)) AS endDate
    FROM ig_Headache ha
      JOIN lookup_su su WITH ( NOLOCK )
        ON ha.SIGORIG = su.SIGORIG
    WHERE ha.KRPT = @krpt
          AND su.deleted = '0'
          AND ha.HEDNUM1N IS NOT NULL
          AND (ha.ENDHED1S IS NULL AND ha.FELSLP1S IS NULL OR ha.UNKEND1B IS NOT NULL)
    ORDER BY su.ReportStartDate DESC

  INSERT INTO @headaches
    SELECT
      ha.HEDNUM1N                                           AS headacheID,
      ha.HEDBEG1S                                           AS startDate,
      ISNULL(ha.ENDHED1S, ISNULL(ha.FELSLP1S, ha.UNKEND1B)) AS endDate
    FROM ig_Headache ha
      JOIN lookup_su su WITH ( NOLOCK )
        ON ha.SIGORIG = su.SIGORIG
    WHERE ha.KRPT = @krpt
          AND su.deleted = '0'
          AND ha.HEDNUM1N IS NOT NULL
          AND (ha.ENDHED1S IS NOT NULL OR ha.FELSLP1S IS NOT NULL OR ha.UNKEND1B IS NOT NULL)
          AND ha.HEDBEG1S IS NOT NULL
    ORDER BY su.ReportStartDate DESC

  INSERT INTO @headaches
    SELECT
      igo.headacheID                         AS headacheID,
      COALESCE(igo.startDate, ige.startDate) AS startDate,
      COALESCE(ige.endDate, igo.endDate, '') AS endDate
    FROM @ongoingHeadaches igo
      LEFT JOIN @endedHeadaches ige
        ON ige.headacheID = igo.headacheID

  SELECT @customSyncXML = (SELECT TOP 1
                             ha.headacheID AS '@headacheID',
                             ha.startDate  AS '@startDate',
                             ha.endDate    AS '@endDate'
                           FROM @headaches ha
                           WHERE ha.endDate LIKE ''
                                 AND ha.startDate IS NOT NULL
                           ORDER BY ha.headacheID ASC
                           FOR XML PATH ('Headache'), TYPE
  )

  --   SELECT isNull(@customSyncXML, '<Headache></Headache>')
  SELECT @customSyncXML = isNull(@customSyncXML, '<Headache></Headache>')
  SELECT @customSyncXML
  SELECT @customSync = cast(@customSyncXML AS VARCHAR(MAX))

  -- Get Version
  DECLARE @Version VARCHAR(10)
  SELECT @Version = ver.procVersion
  FROM PDE_SQLVersion VER WITH ( NOLOCK )
  WHERE VER.procName = @ProcedureName

  -- Add sync data to log
  EXEC [dbo].[insertStudySyncLogs] @ProcedureName, @Version, @krpt, @deviceID, @logTimeClient, @logTimeClientTZOffset,
                                   @customSync

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

IF EXISTS(SELECT 1
          FROM SYSOBJECTS
          WHERE NAME = 'PDE_NetProGetOngoingHeadache' AND TYPE = 'P')
  BEGIN
    DECLARE @name AS VARCHAR(50)
    DECLARE @version AS NUMERIC(18, 2)

    SELECT
      @name = 'PDE_NetProGetOngoingHeadache',
      @version = 1.07
    EXEC [dbo].[PDE_UpdateSQLVersion] @name, @version
  END

GO

-- Set Permissions
IF NOT exists(SELECT *
              FROM allowed_clin_procs WITH ( NOLOCK )
              WHERE proc_name = 'PDE_NetProGetOngoingHeadache')
  INSERT INTO allowed_clin_procs (proc_name) VALUES ('PDE_NetProGetOngoingHeadache')

GRANT EXECUTE ON dbo.PDE_NetProGetOngoingHeadache TO pht_server_read_only