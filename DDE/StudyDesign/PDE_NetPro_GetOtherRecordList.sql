IF exists(SELECT *
          FROM dbo.sysobjects
          WHERE id = object_id(N'[dbo].[PDE_NetPro_GetOtherRecordList]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[PDE_NetPro_GetOtherRecordList]
GO

SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

CREATE PROCEDURE dbo.PDE_NetPro_GetOtherRecordList
  (@krpt                  VARCHAR(255),
   @deviceID              [VARCHAR](10),
   @logTimeClient         [VARCHAR](25),
   @logTimeClientTZOffset [VARCHAR](36))
AS

  SET NOCOUNT ON
  SET ANSI_WARNINGS OFF

  /*****************************************************************************************************************
  AstraZeneca D081DC00007 PDE_NetPro_GetOtherRecordList
  Version	    Date		Change
  1.00	20Jan2017    [jpresto]
                       - Initial Release

  *****************************************************************************************************************/
  DECLARE @syncdata VARCHAR(MAX)

  DECLARE @ProcedureName VARCHAR(256)
  SELECT @ProcedureName = OBJECT_NAME(@@PROCID)

  DECLARE @Delimiter VARCHAR(1)
  SELECT @Delimiter = '|'

  DECLARE @numberOfRecords VARCHAR(5)


  --Get Other Record Dates
  DECLARE @otherRecordsData VARCHAR(MAX)

  SELECT @otherRecordsData = isNull(@otherRecordsData, '')
                             + convert(VARCHAR(20), Cast(ig.TIMTAK1S AS DATETIME), 113) + @Delimiter
                             --+ convert(varchar(20), isnull(ig.MEDNUM1N, '0') , 113) + @Delimiter
                             + convert(VARCHAR(20), isnull(ig.MED_ID1N, '0'), 113) + @Delimiter
  --+ '' + @Delimiter
  --+ cast(isnull(ig.DOSCNT1N, '0') as varchar(4)) + @Delimiter
  FROM ig_AURA ig WITH ( NOLOCK )
    JOIN lookup_su su WITH ( NOLOCK )
      ON su.sigorig = ig.sigorig
  WHERE @krpt = su.krpt
        AND su.deleted = 0
        --AND ig.OTHTRT1B = 1
        AND ig.MEDNAM1L = 'OTH99999'
        AND NOT exists(SELECT *
                       FROM ig_OtherMedResolve rec WITH ( NOLOCK )
                         JOIN lookup_su su2 WITH ( NOLOCK )
                           ON su2.sigorig = rec.sigorig
                       WHERE @krpt = su2.krpt
--                              AND convert(VARCHAR(20), Cast(ig.TIMTAK1S AS DATETIME), 113) =
--                                  convert(VARCHAR(20), Cast(rec.MEDTKN1S AS DATETIME), 113)
                             AND rec.MED_ID1N = ig.MED_ID1N
                             AND su2.deleted = 0)
  ORDER BY Cast(su.ReportStartDate AS DATETIME) ASC, Cast(ig.TIMTAK1S AS DATETIME) ASC

  SELECT @numberOfRecords = isNull(@numberOfRecords, 0) + cast(@@rowcount AS NUMERIC)

  SELECT @otherRecordsData = isNull(@otherRecordsData, '')
                             + convert(VARCHAR(20), Cast(ig.TIMTAK1S AS DATETIME), 113) + @Delimiter
                             --+ convert(varchar(20), isnull(ig.MEDNUM1N, '0') , 113) + @Delimiter
                             + convert(VARCHAR(20), isnull(ig.MED_ID1N, '0'), 113) + @Delimiter
  --+ '' + @Delimiter
  --+ cast(isnull(ig.DOSCNT1N, '0') as varchar(4)) + @Delimiter
  FROM ig_HeadacheMed ig WITH ( NOLOCK )
    JOIN lookup_su su WITH ( NOLOCK )
      ON su.sigorig = ig.sigorig
  WHERE @krpt = su.krpt
        AND su.deleted = 0
        --AND ig.OTHTRT1B = 1
        AND ig.MEDNAM1L = 'OTH99999'
        AND NOT exists(SELECT *
                       FROM ig_OtherMedResolve rec WITH ( NOLOCK )
                         JOIN lookup_su su2 WITH ( NOLOCK )
                           ON su2.sigorig = rec.sigorig
                       WHERE @krpt = su2.krpt
--                              AND convert(VARCHAR(20), Cast(ig.TIMTAK1S AS DATETIME), 113) =
--                                  convert(VARCHAR(20), Cast(rec.MEDTKN1S AS DATETIME), 113)
                             AND rec.MED_ID1N = ig.MED_ID1N
                             AND su2.deleted = 0)
  ORDER BY Cast(su.ReportStartDate AS DATETIME) ASC, Cast(ig.TIMTAK1S AS DATETIME) ASC

  SELECT @numberOfRecords = isNull(@numberOfRecords, 0) + cast(@@rowcount AS NUMERIC)


  DECLARE @lpAssign VARCHAR(4)
  SELECT @lpAssign = cast(count(*) AS VARCHAR)
  FROM lookup_pt pt WITH ( NOLOCK )
  WHERE @krpt = pt.krpt
        AND pt.deleted = 0
        AND pt.LPStartDate IS NOT NULL


  SELECT @syncdata = @numberOfRecords + @Delimiter + ISNULL(@otherRecordsData, '') + '$$$'
  --Select @syncdata =  '1' + @Delimiter + ISNULL('09 Mar 2015|12313141|', '') + @Delimiter + '$$$'

  SELECT @syncdata

  -- Get Version
  DECLARE @Version VARCHAR(10)
  SELECT @Version = ver.procVersion
  FROM PDE_SQLVersion VER WITH ( NOLOCK )
  WHERE VER.procName = @ProcedureName

  -- Add sync data to log
  EXEC [dbo].[insertStudySyncLogs] @ProcedureName, @Version, @krpt, @deviceID, @logTimeClient, @logTimeClientTZOffset,
                                   @syncdata

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

IF EXISTS(SELECT 1
          FROM SYSOBJECTS
          WHERE NAME = 'PDE_NetPro_GetOtherRecordList' AND TYPE = 'P')
  BEGIN
    DECLARE @name AS VARCHAR(50)
    DECLARE @version AS NUMERIC(18, 2)

    SELECT
      @name = 'PDE_NetPro_GetOtherRecordList',
      @version = 1.00
    EXEC [dbo].[PDE_UpdateSQLVersion] @name, @version

  END

GO

IF NOT exists(SELECT *
              FROM allowed_clin_procs WITH ( NOLOCK )
              WHERE proc_name = 'PDE_NetPro_GetOtherRecordList')
  INSERT INTO allowed_clin_procs (proc_name) VALUES ('PDE_NetPro_GetOtherRecordList')

GRANT EXECUTE ON dbo.PDE_NetPro_GetOtherRecordList TO pht_server_read_only