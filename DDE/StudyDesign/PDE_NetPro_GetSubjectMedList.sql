IF exists(SELECT *
          FROM dbo.sysobjects
          WHERE id = object_id(N'[dbo].[PDE_NetPro_GetSubjectMedList]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[PDE_NetPro_GetSubjectMedList]
GO

SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

CREATE PROCEDURE dbo.PDE_NetPro_GetSubjectMedList
  (@krpt                  [VARCHAR](255) = '',
   @deviceID              [VARCHAR](10),
   @logTimeClient         [VARCHAR](25),
   @logTimeClientTZOffset [VARCHAR](36))
AS

  SET NOCOUNT ON
  SET ANSI_WARNINGS OFF

  /*****************************************************************************************************************
  AstraZeneca D081DC00007 PDE_NetPro_GetSubjectMedList
  Version	    Date		Change
  1.00		20Jan2017    [jpresto] Initial Release

  1.01		30Jan2017    [jpresto]
                            - ONEECOA-9068

  1.02        01Feb2017    [jpresto]
                           - ONEECOA-9376
  *****************************************************************************************************************/
  DECLARE @syncdata VARCHAR(MAX)

  DECLARE @ProcedureName VARCHAR(256)
  SELECT @ProcedureName = OBJECT_NAME(@@PROCID)

  DECLARE @Delimiter VARCHAR(1)
  SELECT @Delimiter = '|'

  DECLARE @recordDT VARCHAR(25)

  DECLARE @subjectMedList VARCHAR(MAX)
  DECLARE @latestSigorig VARCHAR(25)

  SELECT TOP 1 @latestSigorig = su2.SIGORIG
  FROM lookup_su su2 WITH ( NOLOCK )
  WHERE su2.krpt = @krpt
        AND (su2.krsu = 'SubjectMedicationSelectionList' OR su2.krsu = 'OtherMedResolve')
  ORDER BY CAST(su2.reportStartDate AS DATETIME) DESC

  SELECT @subjectMedList = ISNULL(@subjectMedList, '') +
                           ISNULL(cast(ig.MEDCOD1C AS VARCHAR), '') + '|' +
                           ISNULL(dbo.PDE_fn_NVarCharToHTMLEncode(cast(ig.MEDALS1C AS NVARCHAR)), ' ') + '|'
  FROM lookup_su su WITH ( NOLOCK )
    JOIN ig_SubjectMed ig WITH ( NOLOCK )
      ON su.sigorig = ig.sigorig
  WHERE ig.KRPT = @krpt
        AND (ig.MEDCOD1C IS NOT NULL OR ig.MEDCOD1C != '')
        AND su.SIGORIG = @latestSigorig


  DECLARE @numMeds VARCHAR(4)
  SELECT @numMeds = CAST(@@ROWCOUNT AS VARCHAR(4))


  SELECT TOP 1 @recordDT = su.ReportStartDate
  FROM lookup_su su WITH ( NOLOCK )
    JOIN ig_SubjectMed ig WITH ( NOLOCK )
      ON su.sigorig = ig.sigorig
  WHERE ig.KRPT = @krpt
        AND su.SIGORIG = @latestSigorig
  ORDER BY cast(su.ReportStartDate AS DATETIME) DESC


  DECLARE @subjLang VARCHAR(10)
  SELECT TOP 1 @subjLang = ig.Language
  FROM ig_Assignment ig WITH ( NOLOCK )
    JOIN lookup_su su WITH ( NOLOCK )
      ON su.sigorig = ig.sigorig
    JOIN lookup_pt pt WITH ( NOLOCK )
      ON su.krpt = pt.krpt
  WHERE @krpt = su.krpt
        AND su.deleted = 0
  ORDER BY CAST(su.ReportStartDate AS DATETIME) DESC

  SELECT @syncdata = IsNull(cast(@recordDT AS VARCHAR(25)), ' ') + @Delimiter
                     + ISNULL(@numMeds, '0') + @Delimiter
                     + ISNULL(cast(@subjectMedList AS VARCHAR(MAX)), ' ') + @Delimiter
                     + ISNULL(@subjLang, 'en_US') + @Delimiter

  SELECT @syncdata


  -- Get Version
  DECLARE @Version VARCHAR(10)
  SELECT @Version = ver.procVersion
  FROM PDE_SQLVersion VER WITH ( NOLOCK )
  WHERE VER.procName = @ProcedureName

  -- Add sync data to log
  EXEC [dbo].[insertStudySyncLogs] @ProcedureName, @Version, @krpt, @deviceID, @logTimeClient, @logTimeClientTZOffset,
                                   @syncdata

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

IF EXISTS(SELECT 1
          FROM SYSOBJECTS
          WHERE NAME = 'PDE_NetPro_GetSubjectMedList' AND TYPE = 'P')
  BEGIN
    DECLARE @name AS VARCHAR(50)
    DECLARE @version AS NUMERIC(18, 2)

    SELECT
      @name = 'PDE_NetPro_GetSubjectMedList',
      @version = 1.02
    EXEC [dbo].[PDE_UpdateSQLVersion] @name, @version

  END

GO

IF NOT exists(SELECT *
              FROM allowed_clin_procs WITH ( NOLOCK )
              WHERE proc_name = 'PDE_NetPro_GetSubjectMedList')
  INSERT INTO allowed_clin_procs (proc_name) VALUES ('PDE_NetPro_GetSubjectMedList')

GRANT EXECUTE ON dbo.PDE_NetPro_GetSubjectMedList TO pht_server_read_only