/*
 * Name: ClinField.java
 * Date: 5/17/2010
 */
package com.phtcorp.clinical;

/**
 * This class prepresents a structure in StudyWorks for submitting clinical data.
 * @author Jsalesi
 */
public class ClinField {

    /**
     * Don't know if this is needed yet.
     */
    private transient static final long serialVersionUID = -4236694533968134348L;

    /**
     * StudyWorks fields.
     */
    private String sysvar;
    private String krIT;
    private String comment;
    private String value;

    public ClinField() {
        //Do nothing.
    }

    /**
     * Gets the sysvar.
     * @return String containing sysvar.
     */
    public String getSysvar() {
        return sysvar;
    }

    /**
     * Sets the sysvar.
     * @param sysvar is a string containing the sysvar.
     */
    public void setSysvar(String sysvar) {
        this.sysvar = sysvar;
    }

    /**
     * Gets the krIT.
     * @return String containing the krIT.
     */
    public String getKrIT() {
        return krIT;
    }

    /**
     * Sets the krIT.
     * @param krIT is a String containing the krIT.
     */
    public void setKrIT(String krIT) {
        this.krIT = krIT;
    }

    /**
     * Gets the comment.
     * @return String containing the comment.
     */
    public String getComment() {
        return comment;
    }

    /**
     * Sets the comment.
     * @param comment is a String containing the comment.
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * Gets the IT value.
     * @return String containing the IT value.
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the IT value.
     * @param value is a String containing the IT value.
     */
    public void setValue(String value) {
        this.value = value;
    }
}
