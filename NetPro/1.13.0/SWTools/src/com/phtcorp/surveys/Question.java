/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.phtcorp.surveys;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Jsalesi
 */
public class Question {

    private String m_questionLabel;
    private ArrayList m_answerList;

    public Question(String questionLabel, Answer answer) {
        this(questionLabel);
        addAnswer(answer);
    }

    public Question(String questionLabel) {
        m_questionLabel = questionLabel;
        m_answerList = new ArrayList();
    }

    public void setQuestionText(String questionText) {
        m_questionLabel = questionText;
    }

    public String getQuestionText() {
        return m_questionLabel;
    }

    public void addAnswer(String itemName) {
        Answer answer = new Answer(itemName);
        m_answerList.add(answer);
    }

    public void addAnswer(Answer answer) {
        m_answerList.add(answer);
    }

    public Iterator getAnswers() {
        return m_answerList.iterator();
    }

    public int getNumberOfAnswers() {
        return m_answerList.size();
    }

    public void clearAnswers() {
        m_answerList.clear();
    }
}
