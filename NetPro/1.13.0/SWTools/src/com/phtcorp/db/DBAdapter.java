/*
 * Class: DBAdapter
 * Created: 5/25/2010
 */

package com.phtcorp.db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 *
 * @author Jsalesi
 */
public abstract class DBAdapter implements Runnable {

    /**
     * Retry count for connecting to database.
     */
    private int NUM_RETRIES = 3;
    /**
     * The sleep time between database scans.
     * Defaults to 5 seconds.
     */
    private int SLEEP_TIME = 5000;
    /**
     * Various flags.
     */
    private boolean IS_RUNNING;
    /**
     * Log file support.
     */
    private static Logger m_logger;
    private final int LOG_FILE_SIZE = 2048;
    /**
     * Adapter specific.
     */
    private String adapterName;
    /**
     * Database specific.
     */
    private Connection m_dbConnection;
    /**
     * For now, we're using MS SQL Server JDBC Driver.
     */
    private final String JDBC_DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    /**
     * Database specific (use hard-coded values for now).
     */
    private String DATABASE_CONNECTION_INFO = "jdbc:sqlserver://webdiary-m58p;" +
                "databaseName=limesurvey;user=admin;password=admin;";

    /**
     * Ctor
     * @param databaseURL is the database info with login password.
     * @param className is the name of the adapter class.
     */
    public DBAdapter(Class className) {

        //Setup the log file.
        String logFileName;
        if(className != null && className instanceof Class) {
            adapterName = className.getName();
        } else {
            adapterName = DBAdapter.class.getName();
        }

        logFileName = className.getSimpleName() + ".log";
        m_logger = Logger.getLogger(adapterName);

        //Setup log file.
        try {
            FileHandler fn = new FileHandler(logFileName, LOG_FILE_SIZE, 1, true);
            m_logger.addHandler(fn);
            fn.setFormatter(new SimpleFormatter());

            //If we can't even create the logfile, somethings is seriously wrong,
            //so we exit.
        } catch(IOException e) {
            log(Level.SEVERE, "Unable to create log file.");
            throw new RuntimeException(e.getMessage());
        } catch(SecurityException e) {
            log(Level.SEVERE, "Unable to create log file.");
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * This starts the monitoring service.
     */
    @Override
    public void run() {

        if(dbConnect()) {
            IS_RUNNING = true;
            m_logger.log(Level.CONFIG, "The databse adapter service is running.");

            //Start monitoring the database.
            monitor();
        } else {
            String msg = "Unable to connect to the database: "
                    + DATABASE_CONNECTION_INFO;
            log(Level.SEVERE, msg);
        }
    }

    /**
     * This checks if the service is running.
     * @return true if the service is running, false otherwise.
     */
    public boolean isRunning() {
        return IS_RUNNING;
    }

    /**
     * This tries to maintain a connection to the database.
     * @return true if connected, false if not.
     */
    private boolean dbConnect() {
        boolean connected = false;

        //Try to connect with retries.
        for (int i = 0; i < NUM_RETRIES && !connected; i++) {
            try {
                //Check to see if JDBC driver exists.
                Class.forName(JDBC_DRIVER);

                //Try to make a connection to the database.
                m_dbConnection = DriverManager.getConnection(DATABASE_CONNECTION_INFO);

                connected = true;
            } catch (ClassNotFoundException e) {
                String msg = "Class Not Found Exception: "+ e.getMessage();
                log(Level.SEVERE, msg);
            } catch (SQLException e) {
                String msg = "SQL Exception: "+ e.getMessage();
                log(Level.WARNING, msg);
            }
        }

        return connected;
    }

    /**
     * Sets the number for database connection retries.
     * @param numRetries
     */
    public void setNumConnectionRetries(int numRetries) {
       if(numRetries > -1) {
            NUM_RETRIES = numRetries;
       }
    }

    /**
     * Sets the monitor sleep timer in millis.
     * @param milliseconds
     */
    public void setSleepTimer(int milliseconds) {
        if(milliseconds >= 0) {
            SLEEP_TIME = milliseconds;
        }
    }

    /**
     * This is the main service loop.
     */
    public void monitor() {

         while(isRunning()) {
            try {
                //Call worker method.
                doWork();
                //Sleep a little bit.
                Thread.sleep(SLEEP_TIME);
            } catch (InterruptedException ex) {
                String msg = "Interrupt error exception: " + ex.getMessage();
                log(Level.SEVERE, msg);
            }
        }
    }

    /**
     * Stops the service from running.
     */
    public void stop() {
        IS_RUNNING = false;
    }

    /**
     * We call this because the base class maintains the logger.
     * @param logLevel is the level for the logger.
     * @param msg is the message to print out to the log file.
     */
    public void log(Level logLevel, String msg) {
        m_logger.log(logLevel, msg);
    }

    /**
     * Abstract method that gets called in the main loop.
     */
    public abstract void doWork();

}
