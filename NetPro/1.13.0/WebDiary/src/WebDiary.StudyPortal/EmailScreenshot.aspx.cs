﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Text;
using System.Globalization;
using WebDiary.Core.Entities;
using WebDiary.Core.Repositories;
using WebDiary.WDAPI;

using WebDiary.Membership.Provider;
using WebDiary.Membership.User;
using WebDiary.StudyPortal.Helpers;

namespace WebDiary.StudyPortal
{
    public partial class EmailScreenshot : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string language = Request.QueryString["language"] as string ?? string.Empty;
            string emailType = Request.QueryString["emailType"] as string ?? string.Empty;
            string surveyId = Request.QueryString["surveyId"] as string ?? string.Empty;
 
            //Validate query strings
            if (string.IsNullOrEmpty(language))
            {
                Response.Write("language is empty");
                Response.End();
           }
            if (string.IsNullOrEmpty(emailType))
            {
                Response.Write("emailType is empty");
                Response.End();
            }
            else if (emailType.ToLower(CultureInfo.InvariantCulture) == "scheduledemailreminder" && string.IsNullOrEmpty(surveyId))
            {
                Response.Write("surveyId is empty");
                Response.End();
            }

            using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
            {
                SubjectRepository subjectRep = new SubjectRepository(db);
                Subject subject = subjectRep.FindById(new Guid(Context.User.Identity.Name));
                StudyRepository studyRep = new StudyRepository(db);
                Study study = studyRep.FindById(subject.StudyId);

                SurveyRepository surveyRep = new SurveyRepository(db);
                StudySurveys survey = null;

                if (!string.IsNullOrEmpty(surveyId))
                {
                  survey = surveyRep.FindById(Convert.ToInt32(surveyId));
                }

                CultureInfo culture = new CultureInfo(language);
                System.Threading.Thread.CurrentThread.CurrentCulture = culture;
                System.Threading.Thread.CurrentThread.CurrentUICulture = culture;

                //Set language direction for UI 
                if (culture.TextInfo.IsRightToLeft)
                {
                    emailContent.Attributes.Add("dir", "rtl");                        
                }
                else
                {
                    emailContent.Attributes.Add("dir", "ltr");
                }

                string emailBody = string.Empty;
                if (emailType.ToLower(CultureInfo.InvariantCulture) == "activation") 
                    emailBody = EmailHelper.GetActivationEmail(study, subject);
                else if (emailType.ToLower(CultureInfo.InvariantCulture) == "confirmation") 
                    emailBody = EmailHelper.GetConfirmationEmail(study, subject);
                else if (emailType.ToLower(CultureInfo.InvariantCulture) == "forgotpassword")
                {
                    WebDiaryProvider provider = System.Web.Security.Membership.Provider as WebDiaryProvider;
                    WebDiaryUser user = (WebDiaryUser)provider.GetUser(subject.Email.ToString(CultureInfo.InvariantCulture), false);
                    Guid token = provider.CreateForgotPasswordRequest((Guid)user.ProviderUserKey);
                    string resetUrl = String.Format(CultureInfo.InvariantCulture, "{0}/reset-password.aspx?token={1}", study.WebDiaryStudyBaseUrl.TrimEnd('/'), token);

                    emailBody = EmailHelper.GetForgotPasswordEmail(resetUrl, subject.Email.ToString(CultureInfo.InvariantCulture));
                }
                else if (emailType.ToLower(CultureInfo.InvariantCulture) == "resetpassword")
                    emailBody = EmailHelper.GetResetPasswordEmail(study, subject);
                else if (emailType.ToLower(CultureInfo.InvariantCulture) == "scheduledemailreminder")
                    emailBody = EmailHelper.GetScheduledEmail(study, survey);
                else if (emailType.ToLower(CultureInfo.InvariantCulture) == "emailupdate")
                    emailBody = EmailHelper.GetChangeEmail(study, subject, Core.Constants.WDConstants.EmailType.EmailUpdate);
                else if (emailType.ToLower(CultureInfo.InvariantCulture) == "emaillanguageupdate")
                    emailBody = EmailHelper.GetChangeEmail(study, subject, Core.Constants.WDConstants.EmailType.EmailLanguageUpdate);
                else if (emailType.ToLower(CultureInfo.InvariantCulture) == "languageupdate")
                    emailBody = EmailHelper.GetChangeEmail(study, subject, Core.Constants.WDConstants.EmailType.LanguageUpdate);

                emailContent.InnerHtml = emailBody;
           }
        }
    }
}