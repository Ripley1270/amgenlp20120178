﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

using WebDiary.Controls;
using WebDiary.Core.Enums;
using WebDiary.Membership.Provider;
using WebDiary.Membership.User;

using log4net;
using System.Text;
using System.Globalization;
using WebDiary.Core.Entities;
using System.Configuration;
using WebDiary.Core.Repositories;
using WebDiary.Core.Constants;

namespace WebDiary.StudyPortal
{
    public partial class ChangePassword : BasePage
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(ChangePassword));

        private Guid _token = Guid.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _token = new Guid(Request.QueryString["token"]);
            }
            catch
            {
                Page.Validators.Add(new ValidationError(Resources.Resource.invalidToken));
                pnlForm.Visible = false;
                return;
            }

            // Check token on page load
            WebDiaryProvider provider = System.Web.Security.Membership.Provider as WebDiaryProvider;

            if (!IsPostBack)
            {
                PasswordLengthValidator.ErrorMessage = String.Format(CultureInfo.CurrentCulture, Resources.Resource.PasswordMinCharError, provider.MinRequiredPasswordLength);
                PasswordLengthValidator.ValidationExpression = String.Format(CultureInfo.InvariantCulture, "^.{{{0},}}$", provider.MinRequiredPasswordLength);
            }

            if (provider.IsValidForgotPasswordToken(_token))
            {
                pnlForm.Visible = true;
                if (Session[WDConstants.ResetPasswordText] != null)
                    lblResetPswrdMsgBody.Text = Session[WDConstants.ResetPasswordText].ToString();
            }
            else
            {
                Page.Validators.Add(new ValidationError(Resources.Resource.invalidToken));
                pnlForm.Visible = false;
                return;
            }


            btnSubmit.Click += new EventHandler(btnSubmit_Click);
        }

        void btnSubmit_Click(object sender, EventArgs e)
        {
            Page.Validate();

            if (Page.IsValid)
            {
                vsErrors.Visible = true;
                if (String.CompareOrdinal(txtPassword.Text, txtConfirmPassword.Text) != 0)
                {
                    Page.Validators.Add(new ValidationError(Resources.Resource.PwdsDontMatch));
                    return;
                }

                WebDiaryProvider provider = System.Web.Security.Membership.Provider as WebDiaryProvider;

                Guid providerUserKey = (Guid)provider.GetProviderUserKeyByForgotPasswordToken(_token);

                WebDiaryUser user = (WebDiaryUser)provider.GetUser(providerUserKey, false);

                if (user == null)
                {
                    Page.Validators.Add(new ValidationError(Resources.Resource.invalidToken));
                    return;
                }

                try
                {
                    using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                    {
                        SubjectRepository subjectRep = new SubjectRepository(db);
                        Subject subject = subjectRep.FindById((Guid)user.ProviderUserKey);

                        ResetPasswordStatus status = provider.ResetPassword(user, subject.Email, _token, txtPassword.Text);
                        if (status == ResetPasswordStatus.Success)
                        {
                            SurveyScheduler.WriteEmailSchedule((int)WDConstants.EmailType.ResetPassword, subject, DateTime.UtcNow, false, true);
                            if (Session["login"] == null)
                            {
                                String loginlink = String.Format(CultureInfo.CurrentCulture, "<a href=\"login.aspx\">{0}</a>", Resources.Resource.LoginPage);
                                String confirm = String.Format(CultureInfo.CurrentCulture, Resources.Resource.ResetPasswdConfirm, loginlink);
                                litConfirmScreen.Text = String.Format(CultureInfo.CurrentCulture, "<p>{0}</p>", confirm);
                                litConfirmScreen.Text += "<script type=\"text/javascript\">setTimeout(\"window.location.href = 'logout.aspx';\", 5000);</script>";
                            }
                            else
                            {
                                String loginlink = String.Format(CultureInfo.CurrentCulture, "<a href=\"settings.aspx\">{0}</a>", Resources.Resource.SettingPage);
                                String confirm = String.Format(CultureInfo.CurrentCulture, Resources.Resource.ResetPasswdConfirm, loginlink);
                                litConfirmScreen.Text = String.Format(CultureInfo.CurrentCulture, "<p>{0}</p>", confirm);
                                litConfirmScreen.Text += "<script type=\"text/javascript\">setTimeout(\"window.location.href = 'settings.aspx';\", 5000);</script>";
                            }
                            vsErrors.Visible = false;
                            pnlForm.Visible = false;
                            pnlSuccess.Visible = true;
                            return;
                        }

                        switch (status)
                        {
                            case ResetPasswordStatus.InvalidEmail:
                                Page.Validators.Add(new ValidationError(Resources.Resource.EmailDoesntMatchsubmittedOne));
                                break;
                            case ResetPasswordStatus.ProviderError:
                                Page.Validators.Add(new ValidationError(Resources.Resource.ErrorUpdatingUrPswd));
                                break;
                        }
                    }
                }
                catch (MembershipPasswordException e_mpe)
                {
                    switch (e_mpe.Message)
                    {
                        case WebDiaryProvider.PasswordFormatError:
                            Page.Validators.Add(new ValidationError(Resources.Resource.PasswordFormatError));
                            break;
                        case WebDiaryProvider.PasswordMinCharError:
                            Page.Validators.Add(new ValidationError(String.Format(CultureInfo.CurrentCulture, Resources.Resource.PasswordMinCharError, provider.MinRequiredPasswordLength)));
                            break;
                        case WebDiaryProvider.PasswordMinNonAlphaCharError:
                            Page.Validators.Add(new ValidationError(String.Format(CultureInfo.CurrentCulture, Resources.Resource.PasswordMinNonAlphaNumError, provider.MinRequiredNonAlphanumericCharacters)));
                            break;
                        case WebDiaryProvider.PasswordValidationError:
                            Page.Validators.Add(new ValidationError(Resources.Resource.PasswordValidationError));
                            break;
                        case WebDiaryProvider.PasswordEqualError:
                            Page.Validators.Add(new ValidationError(Resources.Resource.PasswordEqualError));
                            break;
                        default:
                            Page.Validators.Add(new ValidationError(e_mpe.Message));
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Page.Validators.Add(new ValidationError(Resources.Resource.ErrorUpdatingUrPswd));
                    logger.Error(ex.Message, ex);
                }
            }
            else
                vsErrors.Visible = false; //Validation on the page is already visible so disabling the validation summary
        }

        protected void ValidatePassword(object source, ServerValidateEventArgs args)
        {
            //Set to false by default
            args.IsValid = false;

            try
            {
                string oldPassword = args.Value;

                if (!string.IsNullOrEmpty(oldPassword))
                {
                    WebDiaryProvider provider = System.Web.Security.Membership.Provider as WebDiaryProvider;
                    Guid providerUserKey = (Guid)provider.GetProviderUserKeyByForgotPasswordToken(_token);
                    WebDiaryUser user = (WebDiaryUser)provider.GetUser(providerUserKey, false);

                    if (user == null)
                    {
                        Page.Validators.Add(new ValidationError(Resources.Resource.invalidToken));
                        return;
                    }

                    using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                    {
                        SubjectRepository subjectRep = new SubjectRepository(db);
                        Subject subject = subjectRep.FindById((Guid)user.ProviderUserKey);

                          if (provider.ValidatePassword(subject, oldPassword))
                                args.IsValid = true;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }
        }
    }
}
