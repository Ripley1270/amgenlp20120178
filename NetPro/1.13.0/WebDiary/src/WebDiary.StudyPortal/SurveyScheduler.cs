﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Collections.Specialized;
using System.Globalization;
using WebDiary.Core.Helpers;
using WebDiary.Core.Entities;
using WebDiary.Core.Repositories;
using WebDiary.SWAPI;
using WebDiary.SurveyToolDBAdapters;
using WebDiary.SurveyToolDBAdapters.Checkbox_Entities;
using WebDiary.SWAPI.SWData;
using WebDiary.WDAPI;
using log4net;

namespace WebDiary.StudyPortal
{
    public sealed class SurveyScheduler
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(SurveyScheduler));
        private const string ALL_SUBJECTS = "$ALL_SUBJECTS";
        public enum ScheduleState
        {
            None,
            Activation,
            Submit,
            EmailSent,
            ScheduleNextEmail,
            TimeTravel
        }

        private SurveyScheduler() { }

        public static void ScheduleSurveyEmail(StudySurveys survey, Subject subject, Study study, ScheduleState state)
        {
            using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
            {
                DateTime alarmTime = DateTime.MaxValue;
                DateTime nextAlarmTime = DateTime.MaxValue;
                
                ScheduleConf config = GetSchedule(survey, subject, study);
                // schedule Email Reminder Survey alarm at 11am which repeats on a daily basis
                if (survey.DisplayName == "Email Reminder Survey")
                {
                    alarmTime = config.Available.Date.AddHours(11);
                    nextAlarmTime = alarmTime.AddDays(1);
                }

                // schedule Daily Diary 2reminders alarm at 10am and 3PM which repeats on a daily basis
                if (survey.DisplayName == "Daily Diary 2reminders")
                {
                    alarmTime = config.Available.Date.AddHours(10);
                    nextAlarmTime = alarmTime.AddDays(1);
                    DateTime localTime = GetSubjectLocalTime(subject);

                    //Schedule 3PM email if the 10AM email has been sent before 3PM or there was a time travel into 10am-3PM period
                    //Since there is a chance that the db server and NetPRO server are not perfectly time synced, after the 10AM email has been sent if the time
                    //on the NetPRO server is 9:59, the same email will be scheduled again for 10AM. This is why for the ScheduleState.EmailSent option
                    //there is a 5 minutes tolerance so we check if the localtime is greater than 9:55 and not 10
                    if (state == ScheduleState.TimeTravel && localTime > alarmTime && localTime < config.Available.Date.AddHours(15) ||
                        state == ScheduleState.EmailSent && localTime > alarmTime.AddMinutes(-5) && localTime < config.Available.Date.AddHours(15) ||
                        state == ScheduleState.ScheduleNextEmail && localTime > alarmTime.AddMinutes(-5) && localTime < config.Available.Date.AddHours(15))
                        alarmTime = config.Available.Date.AddHours(15);
                }

                if (alarmTime != DateTime.MaxValue)
                {
                    if (state == ScheduleState.Submit || state == ScheduleState.Activation)
                        WriteEmailSchedule(survey.Id, subject, alarmTime);
                    else if (state == ScheduleState.TimeTravel)
                    {
                        //If today's alarm has passed, schedule it for next one
                        if (alarmTime < GetSubjectLocalTime(subject))
                            WriteEmailSchedule(survey.Id, subject, nextAlarmTime);
                        else
                            WriteEmailSchedule(survey.Id, subject, alarmTime);
                    }
                    else if (state == ScheduleState.EmailSent)
                    {
                        //If today's alarm has passed, schedule it for next one(add 5 minutes tolerance for db server and NetPRO server not time synced)
                        if (alarmTime.AddMinutes(-5) < GetSubjectLocalTime(subject))
                            WriteEmailSchedule(survey.Id, subject, nextAlarmTime);
                        else
                            WriteEmailSchedule(survey.Id, subject, alarmTime);
                    }
                    else if (state == ScheduleState.ScheduleNextEmail)
                    {
                        //If today's alarm has passed, schedule it for next one(add 5 minutes tolerance for db server and NetPRO server not time synced)
                        if (alarmTime.AddMinutes(-5) < GetSubjectLocalTime(subject))
                        {
                            WriteEmailSchedule(survey.Id, subject, nextAlarmTime);
                        }
                        else
                        {
                            WriteEmailSchedule(survey.Id, subject, alarmTime);
                        }
                    }
                }
            }
        }

        public static void InsertNewSchedule(int surveyId, Guid subjectId, DateTime utcScheduleDT, WebDiaryContext db)
        {
            try
            {
                SurveyEmailScheduleRepository scheduleRep = new SurveyEmailScheduleRepository(db);
                SurveyEmailSchedule schedule = scheduleRep.FindBySurveyIdAndSubjectId(surveyId, subjectId);
                if (schedule != null)
                {
                    schedule.ScheduleDT = utcScheduleDT;
                }
                else
                {
                    schedule = new SurveyEmailSchedule
                    {
                        SurveyId = surveyId,
                        SubjectId = subjectId,
                        ScheduleDT = utcScheduleDT,
                    };
                    db.SurveyEmailSchedule.InsertOnSubmit(schedule);
                    logger.Info(String.Format(CultureInfo.InvariantCulture, "SUCCESS: subject GUID: {0}, surveyId: {1}, scheduled date time in UTC: {2}, logging time stamp in UTC: {3}. Entered scheduled email to SURVEY_EMAIL_SCHEDULE table successfully.", schedule.SubjectId.ToString(), schedule.SurveyId.ToString(), schedule.ScheduleDT, DateTime.UtcNow));
                }
            }
            catch (Exception ex)
            {
                logger.Error(String.Format(CultureInfo.InvariantCulture, "FAIL: subject GUID: {0}, surveyId: {1}, logging time stamp in UTC: {2}, error message: {3}. Failed in entering scheduled email to SURVEY_EMAIL_SCHEDULE table.", subjectId.ToString(), surveyId.ToString(), DateTime.UtcNow, ex.Message), ex);
                throw;
            }
        }

        public static void WriteEmailSchedule(int surveyId, Subject subject, DateTime scheduleDT, bool allSubjects = false, bool isUtc = false)
        {
            using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
            {
                DateTime utcScheduleTime;
                    
                if (allSubjects)
                {
                    SubjectRepository subjectRep = new SubjectRepository(db);
                    var subjects = subjectRep.FindByStudy(subject.StudyId);
                    foreach (Subject sub in subjects)
                    {
                        if (isUtc)
                            utcScheduleTime = scheduleDT;
                        else
                            utcScheduleTime = DateTimeHelper.GetUtcTime(scheduleDT, sub.StudyId, sub.TimeZone).AddMinutes(-sub.TimeTravelOffset);
                        InsertNewSchedule(surveyId, sub.Id, utcScheduleTime, db);
                    }
                }
                else
                {
                    if (isUtc)
                        utcScheduleTime = scheduleDT;
                    else
                        utcScheduleTime = DateTimeHelper.GetUtcTime(scheduleDT, subject.StudyId, subject.TimeZone).AddMinutes(-subject.TimeTravelOffset);
                    InsertNewSchedule(surveyId, subject.Id, utcScheduleTime, db);
                }
                db.SubmitChanges();
            }
        }

        public static void ScheduleEmailAllSurveys(Subject subject, Study study, ScheduleState state)
        {
            using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
            {
                SurveyRepository surveyRep = new SurveyRepository(db);
                foreach(var survey in surveyRep.FindByStudyId(subject.StudyId))
                    ScheduleSurveyEmail(survey, subject, study, state);
            }
        }

        public static DateTime GetLastSubmit(StudySurveys survey, Subject subject, Study study)
        {
            DateTime lastSubmitDT = DateTime.MinValue;
            using (CheckboxContext cbDb = new CheckboxContext(ConfigurationManager.ConnectionStrings["checkboxdb"].ConnectionString))
            {
                using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                {
                    WDAPIObject wdapi = new WDAPIObject(study.StudyWorksUsername, study.StudyWorksPassword,
                        study.Name, study.StudyWorksBaseUrl);
                    ArrayList returnItems = new ArrayList(1);
                    returnItems.Add("SU.ReportStartDate");
                    ClinData cData = wdapi.GetClinDataByNumberRange(subject.KrPT, survey.KRSU, -1, returnItems);
                    if (cData.datatable.Count != 0 && cData.datatable[0].Count != 0)
                    {
                        bool parseOK = DateTime.TryParseExact(cData.datatable[0][0], SWAPIConstants.SwDateTimeFormat, CultureInfo.InvariantCulture,
                                DateTimeStyles.None, out lastSubmitDT);

                        if (!parseOK) throw new FormatException("Incorrect date/time format");
                    }
                }
            }
            return lastSubmitDT;
        }

        public static DateTime GetSubjectLocalTime(Subject subject)
        {
            return DateTimeHelper.GetLocalTime(DateTime.UtcNow.AddMinutes(subject.TimeTravelOffset), subject.StudyId, subject.TimeZone);
        }

        private static ScheduleConf getDailyConfig(int startTime, int endTime, DateTime localTime, StudySurveys survey, Subject subject, Study study)
        {
            ScheduleConf config = new ScheduleConf(localTime);
            DateTime lastSubmit = GetLastSubmit(survey, subject, study);
            if (lastSubmit.Date < localTime.Date)
            {
                if (localTime.TimeOfDay.Hours < startTime)
                {
                    config.SetData(localTime.Date.AddHours(startTime), localTime.Date.AddHours(endTime), false);
                }
                else if (localTime.TimeOfDay.Hours < endTime)
                {
                    config.SetData(localTime, localTime.Date.AddHours(endTime), true);
                    config.StringAvailable = Resources.Resource.Now;
                }
                else
                {
                    config.SetData(localTime.Date.AddDays(1).AddHours(startTime), localTime.Date.AddDays(1).AddHours(endTime), false);
                }
            }
            else
            {
                config.SetData(lastSubmit.Date.AddDays(1).AddHours(startTime), lastSubmit.Date.AddDays(1).AddHours(endTime), false);
            }
            return config;
        }

        private static ScheduleConf getWeeklyConfig(DayOfWeek startDay, DayOfWeek endDay, DateTime localTime, StudySurveys survey, Subject subject, Study study)
        {
            ScheduleConf config = new ScheduleConf(localTime);
            DateTime lastSubmit = GetLastSubmit(survey, subject, study);

            DateTime dtAvailable;
            if (lastSubmit.AddDays(endDay - startDay + 1).Date < localTime.Date)
            {
                if (localTime.DayOfWeek >= startDay && localTime.DayOfWeek <= endDay)
                {
                    config.SetData(localTime, localTime.Date.AddDays(endDay + 1 - localTime.DayOfWeek), true);
                    config.StringAvailable = Resources.Resource.Now;
                    return config;
                }
                dtAvailable = localTime.Date;
            }
            else
            {
                dtAvailable = lastSubmit.Date.AddDays(1);
            }
            while (dtAvailable.DayOfWeek != startDay)
                dtAvailable = dtAvailable.AddDays(1);
            config.SetData(dtAvailable, dtAvailable.AddDays(endDay + 1 - startDay), false);
            return config;
        }

        private static ScheduleConf getMonthlyConfig(int startDay, int endDay, DateTime localTime, StudySurveys survey, Subject subject, Study study)
        {
            ScheduleConf config = new ScheduleConf(localTime);
            DateTime lastSubmit = GetLastSubmit(survey, subject, study);
            DateTime dtAvailable;
            if (lastSubmit.AddDays(2) < localTime.Date)
            {
                if (localTime.Day >= startDay && localTime.Day <= endDay)
                {
                    config.SetData(localTime, localTime.Date.AddDays(endDay + 1 - localTime.Day), true);
                    config.StringAvailable = Resources.Resource.Now;
                    return config;
                }
                dtAvailable = localTime.Date;
            }
            else
            {
                dtAvailable = lastSubmit.Date.AddDays(1);
            }
            while (dtAvailable.Day != 26)
                dtAvailable = dtAvailable.AddDays(1);

            config.SetData(dtAvailable, dtAvailable.AddDays(endDay + 1 - startDay), false);
            return config;
        }

        public static ScheduleConf GetSchedule(StudySurveys survey, Subject subject, Study study)
        {
            using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
            {
                DateTime localTime = GetSubjectLocalTime(subject);
                ScheduleConf config = new ScheduleConf(localTime);
                switch (survey.DisplayName)
                {
                    //Example for custom scheduling DAILY 10am - 12pm for survey named Email Reminder Survey
                    case "Email Reminder Survey":
                        config = getDailyConfig(10, 12, localTime, survey, subject, study);
                        break;
                    //Example for custom scheduling DAILY 10am - 4pm for survey named Daily Diary 2reminders
                    case "Daily Diary 2reminders":
                        config = getDailyConfig(10, 16, localTime, survey, subject, study);
                        break;
                    //Example for custom scheduling DAILY 10am - 2pm for survey named SF-36
                    case "SF-36":
                        config = getDailyConfig(10, 14, localTime, survey, subject, study);
                        break;
                    //Example for custom scheduling WEEKLY Tuesday and Wednesday for survey named SF-36
                    case "SF-36.v2":
                        config = getWeeklyConfig(DayOfWeek.Tuesday, DayOfWeek.Wednesday, localTime, survey, subject, study);
                        break;
                    //Example for custom scheduling MONTHLY 26th and 27th for survey named SF-36
                    case "SF-36.v3":
                        config = getMonthlyConfig(26, 27, localTime, survey, subject, study);
                        break;
                    default:
                        config.SetData(localTime, DateTime.MaxValue, true);
                        config.StringAvailable = Resources.Resource.Now;
                        config.StringDue = "";
                        break;
                }
                return config;
            }
        }

        /// <summary>
        /// Check if Notification has passed due time
        /// Compare system time of web server with notification scheduled time in SURVEY_EMAIL_SCHEDULE table 
        /// </summary>
        /// <param name="survey">Survey</param>
        /// <param name="subject">Subject</param>
        /// <param name="systemTime">Webserver System Time</param>
        /// <returns>true/false</returns>
        public static bool IsNotificationPassedDue(StudySurveys survey, Subject subject, DateTime systemTime)
        {
            using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
            {
                PendingNotificationScheduleRepository sr = new PendingNotificationScheduleRepository(db);
                PendingNotificationSchedule schedule = sr.FindBySurveyIdAndSubjectId(survey.Id, subject.Id);

                DateTime scheduleTime = schedule.ScheduleDT;

                if (DateTime.Compare(systemTime, scheduleTime) > 0)
                    return true;
                else
                    return false;
            }
        }

        public static void DeletePendingEmail(string krpt, int studyId)
        {
            using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
            {
                SubjectRepository sr = new SubjectRepository(db);
                Subject sub = sr.FindByKrptAndStudy(krpt, studyId);

                if (sub != null)
                {
                    //Delete pending emails
                    var pendingEmails = db.SurveyEmailSchedule.Where(s => s.SubjectId == sub.Id);
                    db.SurveyEmailSchedule.DeleteAllOnSubmit(pendingEmails);
                    db.SubmitChanges();
                }
            }
        }
   }

    public class ScheduleConf
    {
        private string _sAvailable;
        private DateTime _available;
        private string _sDue;
        private DateTime _due;
        private bool _enabled;
        private DateTime localTime;
        const string DATE_FORMAT = "dddd, dd MMMM yyyy";
        const string TIME_FORMAT = "hh:mm tt";

        public ScheduleConf(DateTime localTime)
        {
            this.localTime = localTime;
        }

        public string StringAvailable
        {
            get { return _sAvailable; }
            set { _sAvailable = value; }
        }

        public DateTime Available
        {
            get { return _available; }
            set { _available = value; }
        }

        public string StringDue
        {
            get { return _sDue; }
            set { _sDue = value; }
        }

        public DateTime Due
        {
            get { return _due; }
            set { _due = value; }
        }

        public bool Enabled
        {
            get { return _enabled; }
            set { _enabled = value; }
        }

        private string SetDate(DateTime dt)
        {
            if (localTime.Date == dt.Date)
                return Resources.Resource.ScheduleToday;
            else if (localTime.AddDays(1).Date == dt.Date)
                return Resources.Resource.ScheduleTomorrow;
            else
                return dt.ToString(DATE_FORMAT, CultureInfo.CurrentCulture);
        }

        public void SetData(DateTime available, DateTime due, bool enabled)
        {
            Available = available;
            Due = due;
            StringAvailable = SetDate(available);
            StringDue = SetDate(due);
            if (!string.IsNullOrEmpty(TIME_FORMAT))
            {
                StringAvailable += ", ";
                StringDue += ", ";
                if (localTime.AddDays(1).Date == available)
                    StringAvailable = Resources.Resource.ScheduleMidnight;
                else
                    StringAvailable += available.ToString(TIME_FORMAT, CultureInfo.CurrentCulture);
                if (localTime.AddDays(1).Date == due)
                    StringDue = Resources.Resource.ScheduleMidnight;
                else
                    StringDue += due.ToString(TIME_FORMAT, CultureInfo.CurrentCulture);
            }
            Enabled = enabled;
        }
    }
}
