﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebDiary.Core.Helpers;
using WebDiary.Core.Entities;
using WebDiary.SWAPI.SWData;
using WebDiary.WDAPI;
using System.Collections;
using System.Globalization;
using WebDiary.SWAPI;
namespace WebDiary.StudyPortal{
    public class SubmitMedications{
        public static ClinRecord processMedicationUpdate(SurveyResponseData surveyResponseData, SurveyData surveyData, ClinRecord clinRec, Boolean setSBJEFF1S = true){
            ClinRecord clinRecord = clinRec;

            //Populate new med list variables and deleted variables
            if(surveyResponseData.Answers.ContainsKey(Medication.IG_SubjectMedList + ".1.DelimitedMedList")){
                //Find MedicationUpdate.0.DelimitedMedList
                ClinField DelimitedMedList = clinRecord.iglist[Medication.IG_SubjectMedList]["1"].Find(delegate(ClinField field){return field.krit.Equals("DelimitedMedList");});

                //Find MedicationUpdate.0.DelimitedAliasList
                ClinField DelimitedAliasList = clinRecord.iglist[Medication.IG_SubjectMedList]["1"].Find(delegate(ClinField field){return field.krit.Equals("DelimitedAliasList");});

                //Find MedicationUpdate.0.DelimitedIndicationList
                //ClinField DelimitedIndicationList = clinRecord.iglist[Medication.IG_SubjectMedList]["1"].Find(delegate(ClinField field) { return field.krit.Equals("DelimitedIndicationList"); });

                //Pull value
                String DelimitedMedListVal = "";
                if(DelimitedMedList != null){
                    DelimitedMedListVal = HttpContext.Current.Server.UrlDecode(DelimitedMedList.value);
                }
                String DelimitedAliasListVal = "";
                if(DelimitedAliasList != null){
                    DelimitedAliasListVal = HttpContext.Current.Server.UrlDecode(DelimitedAliasList.value);
                }
                /*String DelimitedIndicationListVal = "";
                if (DelimitedIndicationListVal != null)
                {
                    DelimitedIndicationListVal = HttpContext.Current.Server.UrlDecode(DelimitedIndicationList.value);
                }*/

                //Drop field, we will be rebuilding the current Med list and deleted med list
                clinRecord.iglist[Medication.IG_SubjectMedList]["1"].Remove(DelimitedMedList);
                clinRecord.iglist[Medication.IG_SubjectMedList]["1"].Remove(DelimitedAliasList);
                //clinRecord.iglist[Medication.IG_SubjectMedList]["1"].Remove(DelimitedIndicationList);
                String[] splitMeds = DelimitedMedListVal.Split('|');
                String[] newMedCodeList = parseSplitMeds(splitMeds);
                String[] splitAlias = DelimitedAliasListVal.Split('|');
                //String[] splitIndication = DelimitedIndicationListVal.Split('|');

                //Get the subject's previous med list
                String oldDelimitedMedList = surveyResponseData.HiddenItems["delimitedMedList"];
                String[] oldMedCodeList = parseOriginalSplitMeds(oldDelimitedMedList.Split('|'));
                int newMedClinFieldCount = 0;
                int deleteMedClinFieldCount = 0;

                //Create clinfields for the current subject medications
                for(int j = 0; j < newMedCodeList.Length; j++){
                    if(newMedCodeList[j] != null){
                        Dictionary<string, List<ClinField>> ig = new Dictionary<string, List<ClinField>>();
                        List<ClinField> SubjMedIG = new List<ClinField>(1);
                        //ig.Add((j + 1).ToString(), SubjMedIG);
                        //clinRecord.iglist.Add(Medication.IG_SubjectMedList, ig);
                        if(!clinRecord.iglist[Medication.IG_SubjectMedList].ContainsKey((j + 1).ToString())){
                            clinRecord.iglist[Medication.IG_SubjectMedList].Add((j + 1).ToString(), SubjMedIG);
                        }
                        //clinRecord.iglist[Medication.IG_SubjectMedList]["0"].Add(createMedRecord(newMedCodeList[j], newMedClinFieldCount));
                        clinRecord.iglist[Medication.IG_SubjectMedList][(j + 1).ToString()].Add(createMedRecordV2(newMedCodeList[j], newMedClinFieldCount));
                        if(j < splitAlias.Length &&
                           !splitAlias[j].Equals("") &&
                           !splitAlias[j].Equals(" ")){
                            //clinRecord.iglist[Medication.IG_SubjectMedList][(j + 1).ToString()].Add(createMedAliasRecord(HttpUtility.UrlEncode(splitAlias[j]), newMedClinFieldCount));
                            //clinRecord.iglist[Medication.IG_SubjectMedList][(j + 1).ToString()].Add(createMedAliasRecord(PDETools.encodeHTMLUnicode(splitAlias[j]), newMedClinFieldCount));
                            clinRecord.iglist[Medication.IG_SubjectMedList][(j + 1).ToString()].Add(createMedAliasRecord(splitAlias[j], newMedClinFieldCount));
                            clinRecord.iglist[Medication.IG_SubjectMedList][(j + 1).ToString()].Add(createMedAliasRecordSBJ(splitAlias[j], newMedClinFieldCount));
                            //clinRecord.iglist[Medication.IG_SubjectMedList][(j + 1).ToString()].Add(createMedAliasFlagRecord());
                        }
                        /*if (j < splitIndication.Length &&
                            !splitIndication[j].Equals("") &&
                            !splitIndication[j].Equals(" "))
                        {
                            clinRecord.iglist[Medication.IG_SubjectMedList][(j + 1).ToString()].Add(createMedIndicationRecord(splitIndication[j], newMedClinFieldCount));
                        }*/
                        newMedClinFieldCount++;
                    }
                }

                //Determine which medications were deleted and create clinfields for them
                //Old list is of the form: <effective date>|med1|med2|med3|etc...
                //Deleted meds Depricated - no longer part of medication module
                for(int i = 0; i < oldMedCodeList.Length; i++){
                    if(!isCurrentMed(oldMedCodeList[i], newMedCodeList)){
                        //clinRecord.iglist[Medication.IG_SubjectMedList]["0"].Add(createDeleteMedRecord(oldMedCodeList[i], deleteMedClinFieldCount));
                        if(!clinRecord.iglist.ContainsKey(Medication.IG_SubjectMedListRepeating)){
                            Dictionary<string, List<ClinField>> igTmp = new Dictionary<string, List<ClinField>>();
                            List<ClinField> SubjMedDelIG = new List<ClinField>(1);
                            igTmp.Add("1", SubjMedDelIG);
                            clinRecord.iglist.Add(Medication.IG_SubjectMedListRepeating, igTmp);
                        }
                        List<ClinField> SubjMedIG = new List<ClinField>(1);
                        if(!clinRecord.iglist[Medication.IG_SubjectMedListRepeating].ContainsKey((deleteMedClinFieldCount + 1).ToString())){
                            clinRecord.iglist[Medication.IG_SubjectMedListRepeating].Add((deleteMedClinFieldCount + 1).ToString(), SubjMedIG);
                        }
                        clinRecord.iglist[Medication.IG_SubjectMedListRepeating][(deleteMedClinFieldCount + 1).ToString()].Add(createDeleteMedRecordV2(oldMedCodeList[i]));
                        deleteMedClinFieldCount++;
                    }
                }
            } else{
                //Get the subject's previous med list
                String oldDelimitedMedList = surveyResponseData.HiddenItems["delimitedMedList"];
                String[] oldMedCodeList = parseOriginalSplitMeds(oldDelimitedMedList.Split('|'));
                String[] oldAliasList = parseOriginalSplitAlias(oldDelimitedMedList.Split('|'));
                //String[] oldIndicationList = parseOriginalSplitIndication(oldDelimitedMedList.Split('|'));
                int newMedClinFieldCount = 0;
                for(int j = 0; j < oldMedCodeList.Length; j++){
                    if(oldMedCodeList[j] != null){
                        Dictionary<string, List<ClinField>> ig = new Dictionary<string, List<ClinField>>();
                        List<ClinField> SubjMedIG = new List<ClinField>(1);
                        if(!clinRecord.iglist[Medication.IG_SubjectMedList].ContainsKey((j + 1).ToString())){
                            clinRecord.iglist[Medication.IG_SubjectMedList].Add((j + 1).ToString(), SubjMedIG);
                        }
                        clinRecord.iglist[Medication.IG_SubjectMedList][(j + 1).ToString()].Add(createMedRecordV2(oldMedCodeList[j], newMedClinFieldCount));
                        clinRecord.iglist[Medication.IG_SubjectMedList][(j + 1).ToString()].Add(createMedAliasRecord(oldAliasList[j], newMedClinFieldCount));
                        clinRecord.iglist[Medication.IG_SubjectMedList][(j + 1).ToString()].Add(createMedAliasRecordSBJ(oldAliasList[j], newMedClinFieldCount));
                        //clinRecord.iglist[Medication.IG_SubjectMedList][(j + 1).ToString()].Add(createMedIndicationRecord(oldIndicationList[j], newMedClinFieldCount));
                        newMedClinFieldCount++;
                    }
                }
            }

            //Set Subject Medication List effective Date if this is Medication Update Diary
            if(setSBJEFF1S){
                for(int i = 0; i < clinRecord.sysvars.Count; i++){
                    if(clinRecord.sysvars[i].sysvar.Equals("SU.ReportStartDate")){
                        Dictionary<string, List<ClinField>> ig = new Dictionary<string, List<ClinField>>();
                        List<ClinField> SubjMedIG = new List<ClinField>(1);
                        ig.Add("0", SubjMedIG);
                        if(!clinRecord.iglist[Medication.IG_SubjectMedSingle].ContainsKey("0")){
                            clinRecord.iglist[Medication.IG_SubjectMedSingle].Add("0", SubjMedIG);
                        }
                        ClinField reportStartDate = clinRecord.sysvars[i];
                        ClinField SBJEFF1S = createClinfield("SBJEFF1S", reportStartDate.value);
                        clinRecord.iglist[Medication.IG_SubjectMedSingle]["0"].Add(SBJEFF1S);
                    }
                }
            }
            return clinRecord;
        }
        public static ClinRecord processMedicationSelection(SurveyResponseData surveyResponseData, SurveyData surveyData, ClinRecord clinRec, WDAPIObject wd, Subject subject){
            ClinRecord clinRecord = clinRec;

            //Remove Work Medication related fields from Subject specific form.
            //Then submit work related info into Global study table
            if(surveyResponseData.Answers.ContainsKey(Medication.IG_WorkMed + ".0.WRKNAM2C")){
                ClinField WRKNAM2C = clinRecord.iglist[Medication.IG_WorkMed]["0"].Find(delegate(ClinField field){return field.krit.Equals("WRKNAM2C");});
                String WRKNAM2CVal = "";
                String WRKNAM2CValPart2 = "";
                if(WRKNAM2C != null){
                    WRKNAM2CVal = WRKNAM2C.value;
                    if(WRKNAM2CVal.Length > 36){
                        WRKNAM2CValPart2 = WRKNAM2CVal.Substring(36);
                        WRKNAM2CVal = WRKNAM2CVal.Substring(0, 36);
                    }
                }
//                clinRecord.iglist[Medication.IG_WorkMed]["0"].Remove(WRKNAM2C);
                String WRKNAM1CVal = "";
                String WRKNAM1CValPart2 = "";
                String WRKNAM1CValPart3 = "";
                ClinField WRKNAM1C = clinRecord.iglist[Medication.IG_WorkMed]["0"].Find(delegate(ClinField field){return field.krit.Equals("WRKNAM1C");});
                if(WRKNAM1C != null){
                    WRKNAM1CVal = WRKNAM1C.value;
                    if(WRKNAM1CVal.Length > 72){
                        WRKNAM1CValPart3 = WRKNAM1CVal.Substring(72);
                        WRKNAM1CValPart2 = WRKNAM1CVal.Substring(36, 36);
                        WRKNAM1CVal = WRKNAM1CVal.Substring(0, 36);
                    } else if(WRKNAM1CVal.Length > 36){
                        WRKNAM1CValPart2 = WRKNAM1CVal.Substring(36);
                        WRKNAM1CVal = WRKNAM1CVal.Substring(0, 36);
                    }
                }
//                clinRecord.iglist[Medication.IG_WorkMed]["0"].Remove(WRKNAM1C);
                String WRKDOS1CVal = "";
                String WRKDOS1CValPart2 = "";
                ClinField WRKDOS1C = clinRecord.iglist[Medication.IG_WorkMed]["0"].Find(delegate(ClinField field){return field.krit.Equals("WRKDOS1C");});
                if(WRKDOS1C != null){
                    WRKDOS1CVal = WRKDOS1C.value;
                    if(WRKDOS1CVal.Length > 36){
                        WRKDOS1CValPart2 = WRKDOS1CVal.Substring(36);
                        WRKDOS1CVal = WRKDOS1CVal.Substring(0, 36);
                    }
                }
//                clinRecord.iglist[Medication.IG_WorkMed]["0"].Remove(WRKDOS1C);
                /*
                ClinField MEDUNT1L = clinRecord.iglist[Medication.IG_WorkMed]["0"].Find(delegate(ClinField field) { return field.krit.Equals("MEDUNT1L"); });
                String MEDUNT1LVal = MEDUNT1L.value;
                clinRecord.iglist[Medication.IG_WorkMed]["0"].Remove(MEDUNT1L);
                */
                ClinField MEDRTE1L = clinRecord.iglist[Medication.IG_WorkMed]["0"].Find(delegate(ClinField field){return field.krit.Equals("MEDRTE1L");});
                String MEDRTE1LVal = MEDRTE1L.value;
//                clinRecord.iglist[Medication.IG_WorkMed]["0"].Remove(MEDRTE1L);
                ClinField MEDFRM1L = clinRecord.iglist[Medication.IG_WorkMed]["0"].Find(delegate(ClinField field){return field.krit.Equals("MEDFRM1L");});
                String MEDFRM1LVal = MEDFRM1L.value;
//                clinRecord.iglist[Medication.IG_WorkMed]["0"].Remove(MEDFRM1L);
                /*ClinField MEDFLG1L = clinRecord.iglist[Medication.IG_WorkMed]["0"].Find(delegate(ClinField field) { return field.krit.Equals("MEDFLG1L"); });
                String MEDFLG1LVal = "";
                if (MEDFLG1L != null)
                {
                    MEDFLG1LVal = MEDFLG1L.value;
                                       
                }
                clinRecord.iglist[Medication.IG_WorkMed]["0"].Remove(MEDFLG1L);


                ClinField MEDVAL1N = clinRecord.iglist[Medication.IG_WorkMed]["0"].Find(delegate(ClinField field) { return field.krit.Equals("MEDVAL1N"); });
                String MEDVAL1NVal = "";
                if (MEDVAL1NVal != null)
                {
                    MEDVAL1NVal = MEDVAL1N.value;
                                       
                }
                clinRecord.iglist[Medication.IG_WorkMed]["0"].Remove(MEDVAL1N);


                ClinField MEDFLG2L = clinRecord.iglist[Medication.IG_WorkMed]["0"].Find(delegate(ClinField field) { return field.krit.Equals("MEDFLG2L"); });
                String MEDFLG2LVal = "";
                if (MEDFLG2L != null)
                {
                    MEDFLG2LVal = MEDFLG2L.value;
                                       
                }
                clinRecord.iglist[Medication.IG_WorkMed]["0"].Remove(MEDFLG2L);

                ClinField MEDVAL2N = clinRecord.iglist[Medication.IG_WorkMed]["0"].Find(delegate(ClinField field) { return field.krit.Equals("MEDVAL2N"); });
                String MEDVAL2NVal = "";
                if (MEDVAL2N != null)
                {
                    MEDVAL2NVal = MEDVAL2N.value;
                                       
                }
                clinRecord.iglist[Medication.IG_WorkMed]["0"].Remove(MEDVAL2N);


                ClinField MEDFLG3L = clinRecord.iglist[Medication.IG_WorkMed]["0"].Find(delegate(ClinField field) { return field.krit.Equals("MEDFLG3L"); });
                String MEDFLG3LVal = "";
                if (MEDFLG3L != null)
                {
                    MEDFLG3LVal = MEDFLG3L.value;
                                       
                }
                clinRecord.iglist[Medication.IG_WorkMed]["0"].Remove(MEDFLG3L);


                ClinField MEDFLG4L = clinRecord.iglist[Medication.IG_WorkMed]["0"].Find(delegate(ClinField field) { return field.krit.Equals("MEDFLG4L"); });
                String MEDFLG4LVal = "";
                if (MEDFLG4L != null)
                {
                    MEDFLG4LVal = MEDFLG4L.value;
                                       
                }
                clinRecord.iglist[Medication.IG_WorkMed]["0"].Remove(MEDFLG4L);*/

                //ReportDateDate
                String WRKENT1S = "";
                for(int i = 0; i < clinRecord.sysvars.Count; i++){
                    if(clinRecord.sysvars[i].sysvar.Equals("SU.ReportStartDate")){
                        ClinField reportStartDate = clinRecord.sysvars[i];
                        WRKENT1S = DateTimeHelper.GetUtcTime(DateTime.Parse(reportStartDate.value)).ToString();
                    }
                }
                ArrayList PDEArgs = new ArrayList();
                PDEArgs.Add(WRKNAM2CVal);
                PDEArgs.Add(WRKNAM2CValPart2);
                PDEArgs.Add(WRKNAM1CVal);
                PDEArgs.Add(WRKNAM1CValPart2);
                PDEArgs.Add(WRKNAM1CValPart3);
                PDEArgs.Add(WRKDOS1CVal);
                PDEArgs.Add(WRKDOS1CValPart2);
                PDEArgs.Add(" " + "|" + MEDRTE1LVal + "|" + MEDFRM1LVal);
                //PDEArgs.Add(MEDRTE1LVal);
                //PDEArgs.Add(MEDFRM1LVal);
                PDEArgs.Add(surveyResponseData.HiddenItems["siteName"]);
                PDEArgs.Add(WRKENT1S + "|" + subject.Language);
                //PDEArgs.Add(subject.Language);
                //Add device ID as NetPRO
                //PDEArgs.Add("NetPRO");
                //Add current local date time
                DateTime now = DateTimeHelper.GetLocalTime();
                PDEArgs.Add(now.ToString(SWAPIConstants.SwDateTimeFormat, CultureInfo.InvariantCulture));
                PDEArgs.Add(subject.TimeZone);
                String newWorkMedResult = wd.GetData("PDE_NetPro_InsertNewWorkMed", PDEArgs).response;
                String[] splitNewWorkMedResult = newWorkMedResult.Split('|');

                //Number of medications currently in subject specific med list
                ClinField OTHCOD1C = createClinfield("OTHCOD1C", splitNewWorkMedResult[0]);
                clinRecord.iglist[Medication.IG_OtherResolve]["0"].Add(OTHCOD1C);
                ClinField OTHRES1L = createClinfield("OTHRES1L", Medication.MedSource_NewWorkMed);
                clinRecord.iglist[Medication.IG_OtherResolve]["0"].Add(OTHRES1L);
            }

            //Other Medication DateTime - MEDTKN1S
            /*
            String dtVal = surveyResponseData.HiddenItems["otherDateTime"];
            DateTime dtConvert = Convert.ToDateTime(dtVal, CultureInfo.InvariantCulture);
            dtVal = dtConvert.ToString(SWAPIConstants.SwDateTimeFormat, CultureInfo.InvariantCulture);
            ClinField MEDTKN1S = createClinfield("MEDTKN1S", dtVal);
            clinRecord.iglist[Medication.IG_OtherResolve]["0"].Add(MEDTKN1S);
            */
            //Other Medication Date - DOSDAT1D
            /*String dtVal = surveyResponseData.HiddenItems["otherDateTime"];
            DateTime dtConvert = Convert.ToDateTime(dtVal, CultureInfo.InvariantCulture);
            dtVal = dtConvert.ToString(SWAPIConstants.SwDateFormat, CultureInfo.InvariantCulture);
            ClinField DOSDAT1D = createClinfield("DOSDAT1D", dtVal);
            clinRecord.iglist[Medication.IG_OtherResolve]["0"].Add(DOSDAT1D);*/
            /*
            if (surveyResponseData.HiddenItems.ContainsKey("DOSDAY1C"))
            {
                String DOSDAY1CVal = surveyResponseData.HiddenItems["DOSDAY1C"];
                ClinField DOSDAY1L = createClinfield("MEDTKN1S", DOSDAY1CVal);
                clinRecord.iglist[Medication.IG_OtherResolve]["0"].Add(DOSDAY1L);
            }
            */

            //Custom for studY
            if(surveyResponseData.Answers.ContainsKey(Medication.IG_OtherResolve + ".0.SRCDTA1L")){
                ClinField source = clinRecord.iglist[Medication.IG_OtherResolve]["0"].Find(delegate(ClinField field){return field.krit.Equals("SRCDTA1L");});
                String sourceVal = source.value;
                if(sourceVal.Equals(Medication.Mistake_Med)){
                    clinRecord.iglist[Medication.IG_OtherResolve]["0"].Remove(source);
                    ClinField mistakeClinField = createClinfield("OTHCOD1C", sourceVal);
                    clinRecord.iglist[Medication.IG_OtherResolve]["0"].Add(mistakeClinField);
                    ClinField OTHRES1L = createClinfield("OTHRES1L", Medication.MedSource_MistakeEntry);
                    clinRecord.iglist[Medication.IG_OtherResolve]["0"].Add(OTHRES1L);
                }
            }

            //Determine source of OTHCOD1C
            if(surveyResponseData.Answers.ContainsKey(Medication.IG_OtherResolve + ".0.OnSubjectList")){
                ClinField SubjectList = clinRecord.iglist[Medication.IG_OtherResolve]["0"].Find(delegate(ClinField field){return field.krit.Equals("OnSubjectList");});
                String SubjectListVal = SubjectList.value;
                clinRecord.iglist[Medication.IG_OtherResolve]["0"].Remove(SubjectList);
                if(SubjectListVal.Equals(PDETools.STD_YES)){
                    ClinField OTHRES1L = createClinfield("OTHRES1L", Medication.MedSource_SubjList);
                    clinRecord.iglist[Medication.IG_OtherResolve]["0"].Add(OTHRES1L);
                }
            }
            if(surveyResponseData.Answers.ContainsKey(Medication.IG_OtherResolve + ".0.OnMasterList")){
                ClinField MasterList = clinRecord.iglist[Medication.IG_OtherResolve]["0"].Find(delegate(ClinField field){return field.krit.Equals("OnMasterList");});
                String MasterListVal = MasterList.value;
                clinRecord.iglist[Medication.IG_OtherResolve]["0"].Remove(MasterList);
                if(MasterListVal.Equals(PDETools.STD_YES)){
                    ClinField OTHRES1L = createClinfield("OTHRES1L", Medication.MedSource_MasterList);
                    clinRecord.iglist[Medication.IG_OtherResolve]["0"].Add(OTHRES1L);
                }
            }
            if(surveyResponseData.Answers.ContainsKey(Medication.IG_OtherResolve + ".0.OnWorkList")){
                ClinField WorkList = clinRecord.iglist[Medication.IG_OtherResolve]["0"].Find(delegate(ClinField field){return field.krit.Equals("OnWorkList");});
                String WorkListVal = WorkList.value;
                clinRecord.iglist[Medication.IG_OtherResolve]["0"].Remove(WorkList);
                if(WorkListVal.Equals(PDETools.STD_YES)){
                    ClinField OTHRES1L = createClinfield("OTHRES1L", Medication.MedSource_WorkList);
                    clinRecord.iglist[Medication.IG_OtherResolve]["0"].Add(OTHRES1L);
                }
            }

            //Generate SubjectMedication IG values
            processMedicationUpdate(surveyResponseData, surveyData, clinRecord, false);

            //Set Effective date of med list
            if(surveyResponseData.Answers.ContainsKey(Medication.IG_OtherResolve + ".0.MEDADD1L")){
                ClinField MEDADD1L = clinRecord.iglist[Medication.IG_OtherResolve]["0"].Find(delegate(ClinField field){return field.krit.Equals("MEDADD1L");});
                String MEDADD1LVal = MEDADD1L.value;
                clinRecord.iglist[Medication.IG_OtherResolve]["0"].Remove(MEDADD1L);
                if(MEDADD1LVal.Equals(PDETools.STD_YES)){
                    for(int i = 0; i < clinRecord.sysvars.Count; i++){
                        if(clinRecord.sysvars[i].sysvar.Equals("SU.ReportStartDate")){
                            Dictionary<string, List<ClinField>> ig = new Dictionary<string, List<ClinField>>();
                            List<ClinField> SubjMedIG = new List<ClinField>(1);
                            if(!clinRecord.iglist[Medication.IG_SubjectMedSingle].ContainsKey("0")){
                                clinRecord.iglist[Medication.IG_SubjectMedSingle].Add("0", SubjMedIG);
                            }
                            ClinField reportStartDate = clinRecord.sysvars[i];
                            ClinField SBJEFF1S = createClinfield("SBJEFF1S", reportStartDate.value);
                            clinRecord.iglist[Medication.IG_SubjectMedSingle]["0"].Add(SBJEFF1S);
                        }
                    }
                } else{
                    Dictionary<string, List<ClinField>> ig = new Dictionary<string, List<ClinField>>();
                    List<ClinField> SubjMedIG = new List<ClinField>(1);
                    if(!clinRecord.iglist[Medication.IG_SubjectMedSingle].ContainsKey("0")){
                        clinRecord.iglist[Medication.IG_SubjectMedSingle].Add("0", SubjMedIG);
                    }
                    ClinField SBJEFF1S = createClinfield("SBJEFF1S", surveyResponseData.HiddenItems["SBJEFF1S"]);
                    clinRecord.iglist[Medication.IG_SubjectMedSingle]["0"].Add(SBJEFF1S);
                }
            } else{
                Dictionary<string, List<ClinField>> ig = new Dictionary<string, List<ClinField>>();
                List<ClinField> SubjMedIG = new List<ClinField>(1);
                if(!clinRecord.iglist[Medication.IG_SubjectMedSingle].ContainsKey("0")){
                    clinRecord.iglist[Medication.IG_SubjectMedSingle].Add("0", SubjMedIG);
                }
                ClinField SBJEFF1S = createClinfield("SBJEFF1S", surveyResponseData.HiddenItems["SBJEFF1S"]);
                clinRecord.iglist[Medication.IG_SubjectMedSingle]["0"].Add(SBJEFF1S);
            }

            //Set EventID
            ClinField eventID = createClinfield("MED_ID1N", surveyResponseData.HiddenItems["medID"]);
            clinRecord.iglist[Medication.IG_OtherResolve]["0"].Add(eventID);
            return clinRecord;
        }

        //Pulls out the code value of each medication. The entire medication name comes back from checkbox.
        private static String[] parseSplitMeds(String[] splitMeds){
            String[] newMedCodeList = new String[splitMeds.Length - 1];
            String tmpString = "";
            for(int i = 0; i < splitMeds.Length - 1; i++){
                if(splitMeds[i] != null &&
                   !splitMeds[i].Equals("")){
                    tmpString = splitMeds[i].Substring(0, Medication.MED_CODE_LENGTH);
                    tmpString = tmpString.TrimEnd();
                    newMedCodeList[i] = tmpString;
                }
            }
            return newMedCodeList;
        }

        //Pulls out the code value of each medication. The subject's original medication list when launching a diary:
        //Form: <effective date>|med1|med2|med3|med4|etc...
        private static String[] parseOriginalSplitMeds(String[] splitMeds){
            String[] newMedCodeList = new String[(splitMeds.Length - 1) / 2];
            String tmpString = "";
            int medCount = 0;
            for(int i = 1; i <= splitMeds.Length - 1; i += 2){
                if(splitMeds[i] != null &&
                   !splitMeds[i].Equals("") &&
                   !splitMeds[i].Equals(" ")){
                    if(splitMeds[i].Length >= Medication.MED_CODE_LENGTH){
                        tmpString = splitMeds[i].Substring(0, Medication.MED_CODE_LENGTH);
                    } else//DS5565 - support old test data 5 med length
                    {
                        tmpString = splitMeds[i].Substring(0, Medication.MED_CODE_LENGTH - 1);
                    }
                    newMedCodeList[medCount] = tmpString;
                    medCount++;
                }
            }
            return newMedCodeList;
        }
        private static String[] parseOriginalSplitAlias(String[] splitMeds){
            String[] newAliasCodeList = new String[(splitMeds.Length - 1) / 2];
            String tmpString = "";
            int medCount = 0;
            for(int i = 2; i <= splitMeds.Length - 1; i += 2){
                if(splitMeds[i] != null &&
                   !splitMeds[i].Equals("") &&
                   !splitMeds[i].Equals(" ")){
                    tmpString = splitMeds[i];
                    newAliasCodeList[medCount] = tmpString;
                    medCount++;
                }
            }
            return newAliasCodeList;
        }
        private static String[] parseOriginalSplitIndication(String[] splitMeds){
            String[] newIndicationCodeList = new String[(splitMeds.Length - 1) / 3];
            String tmpString = "";
            int medCount = 0;
            for(int i = 3; i <= splitMeds.Length - 1; i += 3){
                if(splitMeds[i] != null &&
                   !splitMeds[i].Equals("") &&
                   !splitMeds[i].Equals(" ")){
                    tmpString = splitMeds[i];
                    newIndicationCodeList[medCount] = tmpString;
                    medCount++;
                }
            }
            return newIndicationCodeList;
        }

        //Determines if the medication is still part of the subject's new medication list
        private static Boolean isCurrentMed(String origMed, String[] newMedList){
            Boolean retVal = false;
            for(int i = 0; i < newMedList.Length; i++){
                if(newMedList[i] != null){
                    if(newMedList[i].Equals(origMed)){
                        retVal = true;
                        break;
                    }
                }
            }
            return retVal;
        }

        //Create a clinfield for a deleted med
        //Form of SBJDEL<#>N
        private static ClinField createDeleteMedRecord(String origCode, int currentCount){
            ClinField retVal = new ClinField();
            retVal.krit = "SBJDEL" + (currentCount + 1) + "C";
            retVal.value = origCode;
            retVal.edit = false;
            return retVal;
        }

        //Create a clinfield for a deleted med
        //Form of SBJDEL<#>N
        private static ClinField createDeleteMedRecordV2(String origCode){
            ClinField retVal = new ClinField();
            retVal.krit = "SBJDEL1C";
            retVal.value = origCode;
            retVal.edit = false;
            return retVal;
        }

        //Create clinfield for medication in subject specific list.
        //Form of SBJCOD<#>C
        private static ClinField createMedRecord(String currentCode, int currentCount){
            ClinField retVal = new ClinField();
            retVal.krit = "SBJCOD" + (currentCount + 1) + "C";
            retVal.value = currentCode;
            retVal.edit = false;
            return retVal;
        }

        //Create clinfield for medication in subject specific list.
        //Form of SBJCOD1C
        private static ClinField createMedRecordV2(String currentCode, int currentCount){
            ClinField retVal = new ClinField();
            //retVal.krit = "SBJCOD1C";
            retVal.krit = "MEDCOD1C";
            retVal.value = currentCode;
            retVal.edit = false;
            return retVal;
        }

        //Create clinfield for medication alias in subject specific list.
        //Form of SBJCOD1C
        private static ClinField createMedAliasRecord(String currentCode, int currentCount){
            ClinField retVal = new ClinField();
            //            retVal.krit = "SBJALS1C";
            retVal.krit = "MEDALS1C";
            retVal.value = currentCode;
            retVal.edit = false;
            return retVal;
        }
        private static ClinField createMedAliasRecordSBJ(String currentCode, int currentCount){
            ClinField retVal = new ClinField();
            retVal.krit = "SBJALS1C";
            //            retVal.krit = "MEDALS1C";
            retVal.value = currentCode;
            retVal.edit = false;
            return retVal;
        }

        //Create clinfield for medication indication in subject specific list.
        /*private static ClinField createMedIndicationRecord(String currentCode, int currentCount)
        {
            ClinField retVal = new ClinField();
            retVal.krit = "MEDRSN1L";
            retVal.value = currentCode;
            retVal.edit = false;

            return retVal;
        }*/

        //Create clinfield for medication alias flag in subject specific list.
        //Form of ALSFLG1B
        private static ClinField createMedAliasFlagRecord(){
            ClinField retVal = new ClinField();
            retVal.krit = "ALSFLG1B";
            retVal.value = "1";
            retVal.edit = false;
            return retVal;
        }

        //Generic function to create clinfields
        private static ClinField createClinfield(String krit, String value){
            ClinField clinfield = new ClinField();
            clinfield.krit = krit;
            clinfield.value = value;
            clinfield.edit = false;
            return clinfield;
        }
        /*
        public static ClinRecord processCommonFields(SurveyResponseData surveyResponseData, SurveyData surveyData, ClinRecord clinRecord, String KRSU)
        {
            //Process Common IG/IT fields
            ClinRecord retVal = clinRecord;

           // ClinField SITEUSER = createClinfield("SITEUSER", surveyResponseData.HiddenItems["siteName"]);
            //clinRecord.iglist[Medication.IG_SubjectMedList].Add(SITEUSER);

            //Determine which Medication diary this is, then process diary specific fields
            if (KRSU.Equals(PDETools.MedSelectionKRSU))
            {
                retVal = processMedicationSelection(surveyResponseData, surveyData, retVal);
            }
            else if(KRSU.Equals(PDETools.MedUpdateKRSU))
            {
                retVal = processMedicationUpdate(surveyResponseData, surveyData, retVal);
            }

            return retVal;
        } */
    }
}