﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Net;
using System.Xml.Linq;
namespace WebDiary.StudyPortal{
    public class Medication{
        public const int MED_CODE_LENGTH = 6;
        public const int MAX_NUM_SUBJECT_MEDS = 9;
        public const String EMPTY_STRING = " ";
        public const String IG_SubjectMedList = "SubjectMed";
        public const String IG_SubjectMedListRepeating = "SubjectMed";
        public const String IG_OtherResolve = "OtherMedResolve";
        public const String IG_WorkMed = "OtherMedWorksheet";
        public const String IG_SubjectMedDelList = "SubjectMedDelList";
        public const String IG_SubjectMedSingle = "SubjectMedListSingle";
        public const String MedSource_SubjList = "SC";
        public const String MedSource_MasterList = "MC";
        public const String MedSource_WorkList = "WC";
        public const String MedSource_NewWorkMed = "WE";
        public const String MedSource_MistakeEntry = "ME";
        public const String Mistake_Med = "99999998";
        private String m_code = "";
        private String m_genericName = "";
        private String m_friendlyName = "";
        private String m_dosage = "";
        private String m_route = "";
        private String m_unit = "";
        public void setFriendlyName(String name){
            m_friendlyName = name;
        }
        public String getFriendlyName(){
            return m_friendlyName;
        }
        public void setGenericName(String name){
            m_genericName = name;
        }
        public String getGenericName(){
            return m_genericName;
        }
        public void setUnit(String unit){
            m_unit = unit;
        }
        public String getUnit(){
            return m_unit;
        }
        public void setCode(String code){
            m_code = code;
        }
        public String getCode(){
            return m_code;
        }
        public void setDosage(String dosage){
            m_dosage = dosage;
        }
        public String getDosage(){
            return m_dosage;
        }
        public void setRoute(String route){
            m_route = route;
        }
        public String getRoute(){
            return m_route;
        }
        public override String ToString(){
            return getCode() + " " + getGenericName() + " " + getFriendlyName() + " " + getDosage() + " " + getRoute() + " " + getUnit();
        }
        /**
         * Return a medication record for a specified code value.
         * Useful when the number of medications is large and could create device
         * memory issues if all records are loaded.
         * @param code the code value to look up.
         * @return the matching medication record, or null if no match found.
         */
        public static Medication getMedicationByCode(String value, String baseurl, String lang){
            if(value == null ||
               value.Equals("")){
                return null;
            }
            Medication retVal = new Medication();
            String tmpString = "";
            Medication tmpMedRecord;
            //default english
            string filePath = baseurl + "/medicationCodeList.txt";//System.Web.HttpContext.Current.Request.PhysicalApplicationPath;
            /*if (lang.Equals(PDETools.FRENCH))
            {
                filePath = baseurl + "/medicationCodeList.fr_CA.txt";
            }
            else if (lang.Equals(PDETools.GERMAN))
            {
                filePath = baseurl + "/medicationCodeList.de_DE.txt";
            }*/
            WebClient client = new WebClient();
            Stream stream = client.OpenRead(filePath);
            using(StreamReader sr = new StreamReader(stream)){
                while(sr.Peek() >= 0){
                    tmpString = sr.ReadLine();
                    tmpMedRecord = parseMedString(tmpString);
                    if(tmpMedRecord != null &&
                       tmpMedRecord.getCode().Equals(value)){
                        retVal = tmpMedRecord;
                        break;
                    }
                }
                sr.Close();
            }
            return retVal;
        }
        /**
         * Parse a line of medication attributes and return the values in an object.
         * @param line a single medication line read from the master file
         * @return a record of the parsed values, or null if the line is a comment.
         */
        private static Medication parseMedString(String line){
            // Treat any line starting with '#' as a comment.
            if(line.StartsWith("#")){
                return null;
            }

            // Format is code|generic name|user-friendly name|dosage|route|unit
            Medication record = new Medication();
            string[] st = line.Split('|');
            String code = st[0];
            String genericName = st[1];
            String friendlyName = st[2];
            String dosage = st[3];
            String route = st[4];
            String unit = st[5];
            record.setCode(code);
            record.setGenericName(genericName);
            record.setFriendlyName(friendlyName);
            record.setDosage(dosage);
            record.setRoute(route);
            record.setUnit(unit);
            return record;
        }
        public static void createMasterList(String serverPath, String[] splitMasterList){
            String tmpMedString;
            using(StreamWriter file = new StreamWriter(serverPath + "\\medicationCodeList.js", false)){
                file.WriteLine("var masterArray = [");
                for(int i = 1; i < splitMasterList.Length - 1; i += 6){
                    //file.WriteLine(splitMasterList[i] + "|" + splitMasterList[i + 1] + "|" + splitMasterList[i + 2] + "|" + splitMasterList[i + 3] + "|" + splitMasterList[i + 4] + "|" + splitMasterList[i + 5]);
                    tmpMedString = "[\"" + splitMasterList[i] + "\",\"" + splitMasterList[i + 1] + "\",\"" + splitMasterList[i + 2] + "\",\"" + splitMasterList[i + 3] + "\",\"" + splitMasterList[i + 4] + "\",\"" + splitMasterList[i + 5] + "\"]";
                    if(i + 6 < splitMasterList.Length - 1){
                        tmpMedString += ",";
                    }
                    file.WriteLine(tmpMedString);
                }
                file.WriteLine("];");
            }
        }
        public static void createMasterListV2(String serverPath, String[] splitMasterList, String language){
            int numMeds = int.Parse(splitMasterList[0]);
            String tmpMedString = "";
            using(StreamWriter file = new StreamWriter(serverPath + "\\masterMedicationList." + language + ".js", false)){
                file.WriteLine("var masterArray = {");
                for(int i = 1; i < splitMasterList.Length - 1; i += 8){
                    //file.WriteLine(splitMasterList[i] + "|" + splitMasterList[i + 1] + "|" + splitMasterList[i + 2] + "|" + splitMasterList[i + 3] + "|" + splitMasterList[i + 4] + "|" + splitMasterList[i + 5]);
                    tmpMedString = splitMasterList[i] + ": [\"" + splitMasterList[i] + "\",\"" + splitMasterList[i + 1] + "\",\"" + splitMasterList[i + 2] + "\",\"" + splitMasterList[i + 3] + "\",\"" + splitMasterList[i + 4] + "\",\"" + splitMasterList[i + 5] + "\",\"" + splitMasterList[i + 6] + "\",\"" + splitMasterList[i + 7] + "\"]";
                    if(i + 8 < splitMasterList.Length - 1){
                        tmpMedString += ",";
                    }
                    file.WriteLine(tmpMedString);
                }
                file.WriteLine("};");
            }
        }
        public static void createMasterListV3(String serverPath, String medList, String language){
            XElement xmlDoc = XElement.Parse(medList);
            IEnumerable<XElement> elements = from el in xmlDoc.Elements("Med") select el;
            int numMeds = elements.Count();
            int loopCount = 0;
            using(StreamWriter file = new StreamWriter(serverPath + "\\masterMedicationList." + language + ".js", false)){
                file.WriteLine("var masterArray = {");
                file.WriteLine("//" + numMeds);
                foreach(XElement el in elements){
                    loopCount++;
                    string medCode = el.Attribute("medCode").Value;
                    string medName = WebUtility.HtmlDecode(el.Attribute("medName").Value);
                    string rgnName = WebUtility.HtmlDecode(el.Attribute("rgnName").Value);
                    string medDose = WebUtility.HtmlDecode(el.Attribute("medDose").Value);
                    string medRoute = el.Attribute("medRoute").Value;
                    string medForm = el.Attribute("medForm").Value;
                    string medMEDNOT1B = el.Attribute("medNOT1B").Value;
                    string medOTHSTS1L = el.Attribute("MEDSTS1L").Value;
                    //string medOTHSTS1L = "";
                    string medMEDFLG2B = el.Attribute("MEDFLG2B").Value;
                    string MEDFRM2L = el.Attribute("MEDFRM1L").Value;
                    if(medName.Contains('"')){
                        medName = medName.Replace("\"", "\\\"");
                    }
                    if(rgnName.Contains('"')){
                        rgnName = rgnName.Replace("\"", "\\\"");
                    }
                    if(medDose.Contains('"')){
                        medDose = medDose.Replace("\"", "\\\"");
                    }
                    string tmpMedString = "\"" + medCode + "\": [\"" + medCode + "\",\"" + medName + "\",\"" + rgnName + "\",\"" + medDose + "\",\"" + medRoute + "\",\"" + medForm + "\",\"" + medMEDNOT1B + "\",\"" + medOTHSTS1L + "\",\"" + medMEDFLG2B + "\",\"" + MEDFRM2L + "\"]";
                    if(loopCount < numMeds){
                        tmpMedString += ",";
                    }
                    file.WriteLine(tmpMedString);
                }
                file.WriteLine("};");
            }
        }
        public static void createWorkList(String serverPath, String[] splitWorkList){
            //int numMeds = int.Parse(splitWorkList[0]);
            //String tmpMedString;
            using(StreamWriter file = new StreamWriter(serverPath + "\\masterWorkList.en_US.js", false)){
                file.WriteLine("var workArray = {");
                for(int i = 1; i < splitWorkList.Length - 1; i += 6){
                    //file.WriteLine(splitMasterList[i] + "|" + splitMasterList[i + 1] + "|" + splitMasterList[i + 2] + "|" + splitMasterList[i + 3] + "|" + splitMasterList[i + 4] + "|" + splitMasterList[i + 5]);
                    String tmpMedString = splitWorkList[i] + ": [\"" + splitWorkList[i] + "\",\"" + splitWorkList[i + 1] + "\",\"" + splitWorkList[i + 2] + "\",\"" + splitWorkList[i + 3] + "\",\"" + splitWorkList[i + 4] + "\",\"" + splitWorkList[i + 5] + "\"]";
                    if(i + 6 < splitWorkList.Length - 1){
                        tmpMedString += ",";
                    }
                    file.WriteLine(tmpMedString);
                }
                file.WriteLine("};");
            }
        }
        public static void createWorkListV2(String serverPath, String medList){
            XElement xmlDoc = XElement.Parse(medList);
            IEnumerable<XElement> elements = from el in xmlDoc.Elements("Med") select el;
            int numMeds = elements.Count();
            int loopCount = 0;
            using(StreamWriter file = new StreamWriter(serverPath + "\\masterWorkList.en_US.js", false)){
                file.WriteLine("var workArray = {");
                if(numMeds != 0){
                    foreach(XElement el in elements){
                        loopCount++;
                        string medCode = el.Attribute("medCode").Value;
                        string medName = WebUtility.HtmlDecode(el.Attribute("medName").Value).Replace("\"", "").Replace("\\", "");
                        string genName = WebUtility.HtmlDecode(el.Attribute("genName").Value).Replace("\"", "").Replace("\\", "");
                        string medDose = WebUtility.HtmlDecode(el.Attribute("medDose").Value).Replace("\"", "").Replace("\\", "");
                        string medRoute = el.Attribute("medRoute").Value;
                        string medForm = el.Attribute("medForm").Value;
                        string medID = "";
                        if(el.Attribute("medID") != null){
                            medID = el.Attribute("medID").Value;
                        }
                        string tmpMedString = medCode + ": [\"" + medCode + "\",\"" + medName + "\",\"" + genName + "\",\"" + medDose + "\",\"" + medRoute + "\",\"" + medForm + "\",\"" + medID + "\"]";
//                        string tmpMedString = medCode + ": [\"" + medCode + "\",\"" + medName + "\",\"" + genName + "\",\"" + medDose + "\",\"" + medRoute + "\",\"" + medForm + "\",\"\"]";
                        if(loopCount < numMeds){
                            tmpMedString += ",";
                        }
                        file.WriteLine(tmpMedString);
                    }
                }
                file.WriteLine("};");
            }
        }
        public static String getNumMeds(String dataXML){
            XElement xmlDoc = XElement.Parse(dataXML);
            String numMeds = "0";
            try{
                numMeds = (string)xmlDoc.Attribute("numMeds");
            } catch(Exception e){
                //numMeds attribute missing
            }
            return numMeds;
        }
        public static String getMasterEFFTime(String dataXML){
            XElement xmlDoc = XElement.Parse(dataXML);
            String eff1S = "";
            try{
                eff1S = (string)xmlDoc.Attribute("eff1s");
            } catch(Exception e){
                //numMeds attribute missing
            }
            return eff1S;
        }
    }
}