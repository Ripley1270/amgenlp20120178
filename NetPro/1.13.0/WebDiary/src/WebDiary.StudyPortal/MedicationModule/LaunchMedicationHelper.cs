﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Specialized;
using WebDiary.WDAPI;
using WebDiary.Core.Constants;
using System.Collections;
using System.Text;
using WebDiary.Core.Helpers;
using WebDiary.SWAPI;
using System.Globalization;
using System.Net;

namespace WebDiary.StudyPortal.MedicationModule
{
    public class LaunchMedicationHelper
    {
        //Class helps setup query string and hidden variables used in checkbox surveys for Medication Module
        public static NameValueCollection setupMedicationModuleQueryStrings(NameValueCollection qStrings, String KRSU, WDAPIObject wd, String timezone = "", String otherRecordDT = "", String eventID = "", String medID = "")
        {
            //Setup fields common to both Medication Update and Medication Selection
            NameValueCollection queryStrings = qStrings;
            /*
            //Site ID:
            String siteID = System.Web.HttpContext.Current.Session[WDConstants.VarSiteCode].ToString();
            queryStrings.Add("siteID", siteID);

            //Site Username:
            String userID = System.Web.HttpContext.Current.Session[WDConstants.VarUserId].ToString();
            queryStrings.Add("siteName", userID);

            //Subject ID:
            String subjectID = System.Web.HttpContext.Current.Session[WDConstants.VarPtPatientId].ToString();
            queryStrings.Add("subjectID", subjectID);
            */
            //Max number of medications on subject specific med list
            queryStrings.Add("maxNumMeds", Medication.MAX_NUM_SUBJECT_MEDS.ToString());

            //Expected length of individual medication code
            queryStrings.Add("medCodeLength", Medication.MED_CODE_LENGTH.ToString());

            //Get Subject's current Med list
            String krpt = HttpContext.Current.Session[WDConstants.VarKrpt].ToString();
            ArrayList pdeArgs = new ArrayList();
            pdeArgs.Add(HttpContext.Current.Server.UrlEncode(krpt));
            //Add device ID as NetPRO
            pdeArgs.Add("NetPRO");
            //Add current local date time
            DateTime now = DateTimeHelper.GetLocalTime();
            pdeArgs.Add(now.ToString(SWAPIConstants.SwDateTimeFormat, CultureInfo.InvariantCulture));
            //Add local timezone offset from UTC in milliseconds 
            pdeArgs.Add(timezone);

            String medListResult = wd.GetData("PDE_NetPro_GetSubjectMedList", pdeArgs).response;

            String[] splitMedListResult = medListResult.Split('|');
            //Number of medications currently in subject specific med list
            queryStrings.Add("SBJEFF1S", splitMedListResult[0]);

            //Find the current number of meds in subject list
            int numCurrentMeds = 0;
            List<String> sortedMedList = new List<String>();
            int numMedsSQL = int.Parse(splitMedListResult[1]);
            //for (int i = 1; i <= Medication.MAX_NUM_SUBJECT_MEDS; i++)
            for (int i = 2; i < 2 * numMedsSQL + 2; i+= 2)
            {
                if(!splitMedListResult[i].Equals(Medication.EMPTY_STRING)) {
                    numCurrentMeds++;
                    sortedMedList.Add(splitMedListResult[i] + '|' + WebUtility.HtmlDecode(splitMedListResult[i + 1]));
                }
            }

            //sort meds
            sortedMedList.Sort();
            StringBuilder sb = new StringBuilder();
            sb.Append(splitMedListResult[0]);

            //Rebuild delimited string to pass in as query string
            sortedMedList.ForEach(delegate (String medCode) {
                sb.Append("|");
                sb.Append(medCode);
            });

            //Number of medications currently in subject specific med list
            queryStrings.Add("numMedList", numCurrentMeds.ToString());

            //Pipe delimited string of current med list. Takes the following form:
            //<Effective Date>|numMeds|<med1 number>|<med 1 alias>|<med 2 number>|<med 2 alias>|<med 3 number>|<med 3 alias>|...|<med 9 number>|<med 9 alias>
            //queryStrings.Add("delimitedMedList", medListResult);
            queryStrings.Add("delimitedMedList", sb.ToString());


            //Add additional fields needed for Medication Selection if needed
            if (KRSU.Equals(PDETools.MedSelectionKRSU))
            {
                DateTime dt = DateTime.Parse(otherRecordDT);

                queryStrings.Add("otherDateTime", dt.ToString("dddd, dd MMM yyyy hh:mm tt"));

                //queryStrings.Add("DOSDAY1C", dt.ToString("dd MMM yyyy HH:mm:ss"));
                queryStrings.Add("eventID", eventID);
                queryStrings.Add("medID", medID);
            }

            return queryStrings;
        }

    }
}