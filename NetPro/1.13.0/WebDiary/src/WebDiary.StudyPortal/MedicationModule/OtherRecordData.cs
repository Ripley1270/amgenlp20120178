﻿using System;
using System.Collections.Generic;
namespace WebDiary.StudyPortal{
    public class OtherRecordData{
        private int _ID;
        private string _ReportDateTime;
        private string _ReportDateTimeString;
        private Int64 _MedicationID;
        public override string ToString(){
            return _ReportDateTimeString;
        }
        public static List<OtherRecordData> generateTestList(){
            List<OtherRecordData> myDiaryList = new List<OtherRecordData>();
            OtherRecordData d1 = new OtherRecordData();
            d1.ID = 1;
            d1.ReportDate = "08 Jul 2012 09:15";
            d1._MedicationID = 1231237;
            OtherRecordData d2 = new OtherRecordData();
            d2.ID = 2;
            d2.ReportDate = "08 Jul 2012 14:23";
            d2._MedicationID = 1111231237;
            OtherRecordData d3 = new OtherRecordData();
            d3.ID = 3;
            d3.ReportDate = "09 Jul 2012 12:30";
            d3._MedicationID = 1231237;
            myDiaryList.Add(d1);
            myDiaryList.Add(d2);
            myDiaryList.Add(d3);
            return myDiaryList;
        }

        //Process the other record data returned by the stored procedure
        // 
        // Data is returned in the following form:
        // |<num Record|<record 1 datetime>|<record 2 datetime>|<record 3 datetime>|...|...|$$$
        //
        // Example:
        // |3|24 Jan 2012 12:33|24 Jan 2012 22:32|27 Jan 2012 07:31|$$$
        public static List<OtherRecordData> ProcessDoseDiaryData(String data){
            List<OtherRecordData> myRecordList = new List<OtherRecordData>();
            String[] splitData = data.Split('|');
            Int16 numRecords = Convert.ToInt16(splitData[0]);
            for(int i = 1; i <= numRecords * 2; i = i + 2){
                OtherRecordData diaryData = new OtherRecordData();
                String currDate = splitData[i];
                Int64 medicationID = Int64.Parse(splitData[i + 1]);
                DateTime dt = DateTime.Parse(currDate);
                diaryData._ID = i;
                diaryData._ReportDateTime = dt.ToString("dd MMM yyyy hh:mm tt");//currDate;
                diaryData._ReportDateTimeString = (i-1)/2+1 + " - " + dt.ToString("dd MMM yyyy hh:mm tt");//currDate;
                diaryData._MedicationID = medicationID;
                myRecordList.Add(diaryData);
            }
            return myRecordList;
        }

        //Properties
        public int ID{
            get{return _ID;}
            set{_ID = value;}
        }
        public string ReportDate{
            get{return _ReportDateTime;}
            set{_ReportDateTime = value;}
        }
        public Int64 MedicationID{
            get{return _MedicationID;}
            set{_MedicationID = value;}
        }
    }
}