﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using WebDiary.Core.Email;
using WebDiary.Core.Helpers;
using WebDiary.Core.Repositories;
using WebDiary.Core.Entities;
using WebDiary.Core.Constants;
using System.Configuration;
using log4net;
using WebDiary.SWAPI;
using WebDiary.WDAPI;
using System.Globalization;
using WebDiary.Membership.Provider;
using System.Linq;
using System.IO;
using System.Xml.Linq;

namespace WebDiary.StudyPortal.Helpers
{
    public class AuthorizedBrowser
    {
        public static bool CheckAuthorizedBrowser(string studyName, string browserName, int majorVersion)
        {
            using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
            {
                StudyRepository sr = new StudyRepository(db);
                Study study = sr.FindByName(studyName);

                bool isAuthorizedBrowser = false;

                if (!string.IsNullOrEmpty(study.AuthorizedBrowser))
                {
                    TextReader tr = new StringReader(study.AuthorizedBrowser);
                    XElement xmlDoc = XElement.Load(tr);

                    var browserMajorVersion = (from s in xmlDoc.Descendants("browser")
                                               where s.Attribute("name").Value.ToLower(CultureInfo.InvariantCulture) == browserName.ToLower(CultureInfo.InvariantCulture)
                                               select s.Attribute("major_version")).FirstOrDefault();

                    if (browserMajorVersion != null && !string.IsNullOrEmpty(browserMajorVersion.Value))
                    {
                        if (browserMajorVersion.Value.Contains("+"))
                        {
                            int supportedMV = Convert.ToInt32(WDExtensions.TrimLastCharacter(browserMajorVersion.Value));

                            if (supportedMV <= majorVersion)
                                isAuthorizedBrowser = true;
                        }
                        else
                        {
                            if (string.Equals(browserMajorVersion.Value, majorVersion.ToString(CultureInfo.InvariantCulture), StringComparison.OrdinalIgnoreCase))
                                isAuthorizedBrowser = true;
                        }
                    }
                }

                return isAuthorizedBrowser;
            }
        }

        public static string GetAuthorizedBrowser(string studyName)
        {
            using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
            {
                StudyRepository sr = new StudyRepository(db);
                Study study = sr.FindByName(studyName);

                string authorizedBrowser = string.Empty;

                if (!string.IsNullOrEmpty(study.AuthorizedBrowser))
                {
                    TextReader tr = new StringReader(study.AuthorizedBrowser);
                    XElement xmlDoc = XElement.Load(tr);

                    var listAuthorizedBrowser = (from s in xmlDoc.Descendants("browser")
                                                 select s.Attribute("description")).ToList();

                    foreach (var browser in listAuthorizedBrowser)
                    {
                        if (string.IsNullOrEmpty(authorizedBrowser))
                            authorizedBrowser = browser.Value;
                        else
                            authorizedBrowser = authorizedBrowser + ", " + browser.Value;
                    }

                    return authorizedBrowser;
                }

                return authorizedBrowser;
            }
        }

        public static List<string> GetAuthorizedBrowser2(string studyName)
        {
            using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
            {
                StudyRepository sr = new StudyRepository(db);
                Study study = sr.FindByName(studyName);

                if (!string.IsNullOrEmpty(study.AuthorizedBrowser))
                {
                    TextReader tr = new StringReader(study.AuthorizedBrowser);
                    XElement xmlDoc = XElement.Load(tr);

                    var listAuthorizedBrowser = (from s in xmlDoc.Descendants("browser")
                                                 select s.Attribute("description").Value).ToList();

                    return listAuthorizedBrowser;
                }
                else
                    return new List<string>();
            }
        }

        public static string GenerateBrowserList(List<string> browserList)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<ul id = \"authBrowserList\" class=\"authBrowser\">");
            foreach (var browser in browserList)
            {
                sb.Append("<li>");
                sb.Append(browser);
                sb.Append("</li>");
            }
            sb.Append("</ul>");

            return sb.ToString();
        }
    }
}