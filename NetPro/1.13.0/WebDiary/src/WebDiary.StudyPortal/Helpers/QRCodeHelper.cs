﻿using System.Web;
using System.IO;
using System.Drawing.Imaging;
using System.Drawing;
using System.Globalization;
using Newtonsoft.Json.Linq;
using WebDiary.Core.Errors;
using WebDiary.Core.Constants;
using System.Text;
using ZXing;
using ZXing.Common;

namespace WebDiary.StudyPortal.Helpers
{
    public class QRCodeHelper
    {
        public static string EncodeQRCode(string studyName, long setupCode, string language)
        {
            int height = 250;
            int width = 250;
            int margin = 0;

            string qrValue = CreateJson(studyName, setupCode, language);
            string fileName = CleanForFileName(studyName) + "_" + setupCode + "_" + language + ".png";
            string imageFile = HttpContext.Current.Server.MapPath(WDConstants.QRCodeImagePath + "/" + fileName); 

            IBarcodeWriter barcodeWriter = new BarcodeWriter
            {
                Format = BarcodeFormat.QR_CODE,
                Options = new EncodingOptions
                {
                    Height = height,
                    Width = width,
                    Margin = margin
                }
            };

            using (var result = barcodeWriter.Write(qrValue))
            {
                Bitmap bitmap = new Bitmap(result);
                bitmap.Save(imageFile, ImageFormat.Png);
            }

           return WDConstants.QRCodeImagePath + "/" + fileName;
        }

        public static string CreateJson(string studyName, long setupCode, string language)
        {
            JObject jobject = new JObject(
                new JProperty("study", studyName),
                new JProperty("setupcode", setupCode),
                new JProperty("language", language)
            );

            return jobject.ToString();
        }

        public static string GetQRCode(string studyName, long setupCode, string language)
        {
            if (string.IsNullOrEmpty(studyName))
                throw new NetProException("A main part of study base url can not be empty");

            if (string.IsNullOrEmpty(language))
                throw new NetProException("subject language can not be empty");  

            string fileName = CleanForFileName(studyName) + "_" + setupCode + "_" + language + ".png";
            string imageFile = HttpContext.Current.Server.MapPath(WDConstants.QRCodeImagePath + "/" + fileName); 

            if (File.Exists(imageFile))
                return WDConstants.QRCodeImagePath + "/" + fileName;
            else
            {
                imageFile = EncodeQRCode(studyName, setupCode, language);
            }

            return imageFile.ToString(CultureInfo.InvariantCulture);
        }

        public static string CleanForFileName(string str)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if (c >= '0' && c <= '9' || c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z' || c == '.' || c == '_' || c == '-')
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }
    }
}