﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

using System.Runtime.Serialization;
using System.Net;
using System.Net.Configuration;
using System.IO;
using System.Text;
using System.Configuration;
using System.Net.Mail;

using WebDiary.Membership.Provider;
using WebDiary.Membership.User;
using WebDiary.Controls;
using WebDiary.Core.Enums;
using WebDiary.Core.Entities;
using WebDiary.Core.Helpers;
using WebDiary.Core.Constants;
using WebDiary.Core.Email;
using WebDiary.Core.Repositories;
using WebDiary.WDAPI;
using WebDiary.StudyPortal.Helpers;

using log4net;
using System.Globalization;

namespace WebDiary.StudyPortal
{
    public partial class Activate : BasePage
    {
        private Guid _token = Guid.Empty;
        private static readonly ILog logger = LogManager.GetLogger(typeof(Activate));
        WebDiaryUser user;

        protected override void InitializeCulture()
        {
            try
            {
                _token = new Guid(Request.QueryString["token"]);
                WebDiaryProvider provider = System.Web.Security.Membership.Provider as WebDiaryProvider;
                user = (WebDiaryUser)provider.GetUserByActivationToken(_token);
                if (user != null)
                {
                    string language = WDAPIObject.JavaLocaleToDotNetCulture(user.Language);
                    UICulture = Culture = language;
                    Session[WDConstants.Language] = Response.Cookies[WDConstants.Language].Value = language;
                }
            }
            catch (Exception ex) 
            {
                logger.Error(ex.Message, ex);
            }
            //Call base method for all cases - user idenfied with token, usre not identified, error occured
            //so that messages are diaplayed in user's language 
            base.InitializeCulture();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                HttpBrowserCapabilities browser = Request.Browser;
                bool isAuthorizedBrowser = AuthorizedBrowser.CheckAuthorizedBrowser(StudyData.study.Name, browser.Browser, browser.MajorVersion);
                if (!isAuthorizedBrowser)
                {
                    lblUnauthorizedMessage.Visible = true;
                    string browserNameVersion = browser.Browser;

                    if (string.Equals(browserNameVersion, "Edge", StringComparison.OrdinalIgnoreCase))
                        browserNameVersion = browserNameVersion + "HTML" + " " + browser.MajorVersion;
                    else
                        browserNameVersion = browserNameVersion + " " + browser.MajorVersion;
                    lblUnauthorizedMessage.Text = String.Format(CultureInfo.CurrentCulture, Resources.Resource.UnauthorizedBrowserMsg.ToString(CultureInfo.CurrentCulture), browserNameVersion);

                    List<string> authorizedBrowser = AuthorizedBrowser.GetAuthorizedBrowser2(StudyData.study.Name);
                    string browserList = AuthorizedBrowser.GenerateBrowserList(authorizedBrowser);
                    authBrowserArea.InnerHtml = browserList;

                    txtEmail.Enabled = false;
                    txtPassword.Enabled = false;
                    txtConfirmPassword.Enabled = false;
                    chkboxAgreement.Enabled = false;
                    btnLogOn.Enabled = false;

                    logger.Info(String.Format(CultureInfo.InvariantCulture, "Use of {0} is not supported. Browser Name: {1}, Major version:{2}, userAgent: {3}, token in request: {4}", browserNameVersion, browser.Browser, browser.MajorVersion, Request.UserAgent, Request.QueryString["token"]));
                    return;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }

            try
            {
                _token = new Guid(Request.QueryString["token"]);
            }
            catch 
            {
                Page.Validators.Add(new ValidationError(Resources.Resource.invalidToken));
                pnlForm.Visible = false;
            }

            // Check token on page load
            
                WebDiaryProvider provider = System.Web.Security.Membership.Provider as WebDiaryProvider;

                if (!IsPostBack)
                {
                    PasswordLengthValidator.ErrorMessage = String.Format(CultureInfo.CurrentCulture, Resources.Resource.PasswordMinCharError, provider.MinRequiredPasswordLength);
                    PasswordLengthValidator.ValidationExpression = String.Format(CultureInfo.InvariantCulture, "^.{{{0},}}$", provider.MinRequiredPasswordLength);
                }

                user = (WebDiaryUser)provider.GetUserByActivationToken(_token);

                if (user == null)
                {
                    Page.Validators.Add(new ValidationError(Resources.Resource.invalidToken));
                    pnlForm.Visible = false;
                }
                else
                {
                    using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                    {
                        StudyRepository sr = new StudyRepository(db);
                        Study study = sr.FindById(user.StudyId);
                        WDAPIObject wd = new WDAPIObject(study.StudyWorksUsername, study.StudyWorksPassword, study.Name, study.StudyWorksBaseUrl);
                        SubjectStudyEnabled result = wd.IsSubjectAndStudyEnabled(user.KrPT, study);
                        if (result == SubjectStudyEnabled.SubjectDisabled)
                        {
                            Page.Validators.Add(new ValidationError(Resources.Resource.AcctCannotBeActivated));
                            pnlForm.Visible = false;
                            return;
                        }
                        else if (result == SubjectStudyEnabled.StudyDisabled)
                        {
                            Page.Validators.Add(new ValidationError(Resources.Resource.ErrorStudyDisabled));
                            pnlForm.Visible = false;
                            return;
                        }
                        else if (result == SubjectStudyEnabled.Error)
                        {
                            Page.Validators.Add(new ValidationError(String.Format(CultureInfo.InvariantCulture, Resources.Resource.AppErrHasOccurred, "<a href=\"https://mystudy.phtstudy.com/ssa/pages/contact_us/\">", "</a>")));
                            pnlForm.Visible = false;
                            return;
                        }
                        else if (user.IsApproved)
                        {
                            String loginlink = String.Format(CultureInfo.CurrentCulture, "<a href=\"{0}\">{1}</a>", StudyData.study.WebDiaryStudyBaseUrl.TrimEnd('/'), Resources.Resource.PleaseLogin);
                            Page.Validators.Add(new ValidationError(String.Format(CultureInfo.CurrentCulture, Resources.Resource.AcctAlreadyActivated, loginlink)));
                            pnlForm.Visible = false;
                        }
                        else if (DateTime.UtcNow.CompareTo(user.ActivationTokenExpiration) > 0)
                        {
                            Page.Validators.Add(new ValidationError(Resources.Resource.ActivationTokenHasExpired));
                            pnlForm.Visible = false;
                        }
                        else
                        {
                            pnlForm.Visible = true;
                        }
                    }
                }
            

            btnLogOn.Click += new EventHandler(btnLogin_Click);

    
        }

        void btnLogin_Click(object sender, EventArgs e)
        {

            Page.Validate();

            if (!Page.IsValid) return;

            //This is already handled by CompareValidator control
            // Compare passwords
            //if (String.Compare(txtPassword.Text, txtPasswordConfirm.Text) != 0)
            //{
            //    Page.Validators.Add(new ValidationError(Resources.Resource.PwdsDontMatch));
            //    return;
            //}

            WebDiaryProvider provider = System.Web.Security.Membership.Provider as WebDiaryProvider;

            try
            {
                MembershipActivateStatus status = provider.Activate(txtEmail.Text.Trim(), _token, txtPassword.Text);
                if (status == MembershipActivateStatus.Success)
                {
                    try
                    {
                        using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                        {
                            SubjectRepository subjectRep = new SubjectRepository(db);
                            Subject subject = subjectRep.FindById((Guid)user.ProviderUserKey);
                            StudyRepository studyRep = new StudyRepository(db);
                            Study study = studyRep.FindById(subject.StudyId);
                            SurveyScheduler.WriteEmailSchedule((int)WDConstants.EmailType.Confirmation, subject, DateTime.UtcNow, false, true);
                            SurveyScheduler.ScheduleEmailAllSurveys(subject, study, SurveyScheduler.ScheduleState.Activation);
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex.Message, ex);
                        throw;
                    }

                    litConfirmText.Text = Resources.Resource.EmailAcctActivationConf;
                    litConfirmTextP1.Text = String.Format(CultureInfo.CurrentCulture, Resources.Resource.EmailAcctActivationConfP1, "<a href=\"login.aspx\">","</a>");
                        
                    pnlForm.Visible = false;
                    pnlSuccess.Visible = true;
                    vsErrors.Visible = false;

                    return;
                }

                string error = String.Empty;

                switch (status)
                {
                    case MembershipActivateStatus.InvalidToken:
                        error = Resources.Resource.UProvidedAnIinvalidToken;
                        break;
                    case MembershipActivateStatus.InvalidUser:
                        error = Resources.Resource.rfvEmailNotValid;
                        break;
                    case MembershipActivateStatus.TokenExpired:
                        error = Resources.Resource.UrTokenHasExpired;
                        break;
                    case MembershipActivateStatus.ProviderError:
                        error = String.Format(CultureInfo.InvariantCulture, Resources.Resource.AppErrHasOccurred, "<a href=\"https://mystudy.phtstudy.com/ssa/pages/contact_us/\">", "</a>");
                        break;
                }

                if (!String.IsNullOrEmpty(error)) 
                {
                    Page.Validators.Add(new ValidationError(error));
                }
            }
            catch (MembershipPasswordException e_mpe)
            {
                switch (e_mpe.Message)
                {
                    case WebDiaryProvider.PasswordFormatError:
                        Page.Validators.Add(new ValidationError(Resources.Resource.PasswordFormatError));
                        break;
                    case WebDiaryProvider.PasswordMinCharError:
                        Page.Validators.Add(new ValidationError(String.Format(CultureInfo.CurrentCulture, Resources.Resource.PasswordMinCharError, provider.MinRequiredPasswordLength)));
                        break;
                    case WebDiaryProvider.PasswordMinNonAlphaCharError:
                        Page.Validators.Add(new ValidationError(String.Format(CultureInfo.CurrentCulture, Resources.Resource.PasswordMinNonAlphaNumError, provider.MinRequiredNonAlphanumericCharacters)));
                        break;
                    case WebDiaryProvider.PasswordValidationError:
                        Page.Validators.Add(new ValidationError(Resources.Resource.PasswordValidationError));
                        break;
                    case WebDiaryProvider.PasswordEqualError:
                        Page.Validators.Add(new ValidationError(Resources.Resource.PasswordEqualError));
                        break;
                    default:
                        Page.Validators.Add(new ValidationError(e_mpe.Message));
                        break;
                }
                
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                Page.Validators.Add(new ValidationError(String.Format(CultureInfo.InvariantCulture, Resources.Resource.AppErrHasOccurred, "<a href=\"https://mystudy.phtstudy.com/ssa/pages/contact_us/\">", "</a>")));
            }
        }
    }
}
