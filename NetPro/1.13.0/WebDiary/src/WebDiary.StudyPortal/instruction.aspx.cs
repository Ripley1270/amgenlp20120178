﻿using System;
using System.Web;
using WebDiary.Core.Helpers;
using WebDiary.SWAPI;
using WebDiary.Controls;
using WebDiary.Core.Constants;
using log4net;

namespace WebDiary.StudyPortal
{
    public partial class Instruction : BasePage
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(Instruction));
        protected const string ReturnUrl = "submit_return_url";

        protected void Page_Load(object sender, EventArgs e)
        {
            string showInstruction = Request.QueryString["showInstruction"] as string ?? string.Empty;
            if (!string.IsNullOrEmpty(showInstruction))
            {
            if (showInstruction.Equals("false", StringComparison.OrdinalIgnoreCase))
                WriteNoInstructionCookie();
            else if (showInstruction.Equals("true", StringComparison.OrdinalIgnoreCase))
                DeleteNoInstructionCookie();
            }

            ShowWhatsNextInstruction();
        }

        protected void DontShow_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (dontShow.Checked)
                    Response.Redirect("instruction.aspx?showInstruction=false", false);
                else
                    Response.Redirect("instruction.aspx?showInstruction=true", false);
            }
            catch (Exception ex)
            {
                litResult.Text = Resources.Resource.GeneralErrorMsg;
                litResult.Visible = true;
                instructionPanel.Visible = false;
                logger.Error(ex.Message, ex);
            }
        }

        protected void GoStudyWork_Click(object sender, EventArgs e)
        {
            try
            {
                //Check to see if session is still active.
                if (!SessionHelper.CheckSwSession(Session[WDConstants.VarRequestUrl] as String))
                {
                    Response.Redirect(Session[WDConstants.VarSSABASEURL] + SWAPIConstants.SessionTimeoutPage, false);
                    return;
                }
                Response.Redirect(Session[ReturnUrl].ToString(), false);
            }
            catch (System.Net.WebException webex)
            {
                litResult.Text = Resources.Resource.ServerDown;
                litResult.Visible = true;
                instructionPanel.Visible = false;
                logger.Error(webex.Message, webex);
            }
            catch (Exception ex)
            {
                litResult.Text = Resources.Resource.GeneralErrorMsg;
                litResult.Visible = true;
                instructionPanel.Visible = false;
                logger.Error(ex.Message, ex);
            }
        }

        protected void ShowWhatsNextInstruction()
        {
            string studyType = string.IsNullOrEmpty(StudyData.version.LFHash) ? "np" : "lf";

            litResult.Visible = false;
            instructionPanel.Visible = true;

            //Separate instructions and steps to handle right to left language
            if (studyType == "np")
            {
                npInstruction.Visible = true;
                npStep1.Text = Resources.Resource.NPInstructionStep1;
                npStep2.Text = Resources.Resource.NPInstructionStep2;
            }
            else
            {
                lpaInstruction.Visible = true;
                //Removed all steps
            }

            if(!IsPostBack)
            {
                string showInstruction = Request.QueryString["showInstruction"] as string ?? string.Empty;
                if (!string.IsNullOrEmpty(showInstruction) && showInstruction.Equals("false", StringComparison.OrdinalIgnoreCase))
                {
                    dontShow.Checked = true;

                }
                else
                {
                    dontShow.Checked = false;
                }
            }
        }

        protected void WriteNoInstructionCookie()
        {
            //Session SiteUser should have always value
            //Cookie name is generated dynamically based on hashed siteUserId to handle multiple users on a public computer
            string siteUserId = Session[WDConstants.VarUserId] as string ?? string.Empty;
            string hashedSiteUserId = CryptoHelper.EncodeUserName(siteUserId);
            Response.Cookies[hashedSiteUserId].Value = "true";
            Response.Cookies[hashedSiteUserId].Expires = DateTime.Now.AddDays(30);
        }

        protected void DeleteNoInstructionCookie()
        {
            string siteUserId = Session[WDConstants.VarUserId] as string ?? string.Empty;
            string hashedSiteUserId = CryptoHelper.EncodeUserName(siteUserId);
            if (Request.Cookies[hashedSiteUserId] != null)
            {
                HttpCookie hashedSiteUserIdCookie = new HttpCookie(hashedSiteUserId);
                hashedSiteUserIdCookie.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(hashedSiteUserIdCookie);
            }
        }
    }
}
