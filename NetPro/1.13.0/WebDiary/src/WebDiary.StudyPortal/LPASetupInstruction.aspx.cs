﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using WebDiary.Core.Entities;
using WebDiary.Controls;
using WebDiary.Core.Helpers;
using WebDiary.Core.Repositories;
using WebDiary.Core.Errors;
using WebDiary.Core.Constants;
using WebDiary.WDAPI;
using System.IO;
using log4net;

namespace WebDiary.StudyPortal
{
    public partial class LPASetupInstruction : Page
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(LPASetupInstruction));
        private const string  subjectInstructionImagPath = "~/img/lf/subjectinstruction/";

        protected string SessionTimeout
        {
            get
            {
                return (Session.Timeout * 60000).ToString(CultureInfo.InvariantCulture);
            }
        }

        protected string ReturnUrl
        {
            get
            {
                return StudyData.study.SsaBaseUrl.TrimEnd('/') + SWAPI.SWAPIConstants.SessionTimeoutPage;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                litScript.Text = "<script type=\"text/javascript\">var sTimeout = " + Session.Timeout * 60000 + "; var returnUrl = '" + StudyData.study.SsaBaseUrl.TrimEnd('/') + SWAPI.SWAPIConstants.SessionTimeoutPage + "'; setTimeout(function () { location.replace(returnUrl); }, sTimeout);</script>";

                string instruction = Request.QueryString["instruction"] as string ?? string.Empty;   
                string language = WDConstants.DefaultLanguageCode;

                using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                {
                    string protocol = Session[WDConstants.VarProtocol] as string ?? string.Empty;
                    if (string.IsNullOrEmpty(protocol))
                        throw new NetProException("Protocol can not be empty");

                    string krPT = Session[WDConstants.VarKrpt] as string ?? string.Empty;
                    if (string.IsNullOrEmpty(krPT))
                        throw new NetProException("krPT can not be empty");

                    Study study = StudyData.study;
                    SubjectRepository subjectRep = new SubjectRepository(db);
                    Subject subject = subjectRep.FindByKrptAndStudy(krPT, study.Id);

                    if (string.Equals(instruction, "studycoordinator", StringComparison.OrdinalIgnoreCase))
                    {
                        string site_language = SessionHelper.GetSiteLanguageKey();
                        if (!string.IsNullOrEmpty(Session[site_language] as string))
                            language = Session[site_language].ToString();
                    }
                    else
                    {
                        //Set culture to subject culture
                        language = subject.Language.ToString(CultureInfo.InvariantCulture);
                        if (!string.IsNullOrEmpty(language))
                            language = WDAPIObject.JavaLocaleToDotNetCulture(language);
                    }

                    CultureInfo culture = new CultureInfo(language);
                    System.Threading.Thread.CurrentThread.CurrentCulture = culture;
                    System.Threading.Thread.CurrentThread.CurrentUICulture = culture;

                    //Set language direction for UI 
                    if (culture.TextInfo.IsRightToLeft)
                    {
                        header.Attributes.Add("dir", "rtl");
                        content.Attributes.Add("dir", "rtl");
                        printBtnContainer.Attributes.Add("dir", "rtl");
                    }
                    else
                    {
                        header.Attributes.Add("dir", "ltr");
                        content.Attributes.Add("dir", "ltr");
                        printBtnContainer.Attributes.Add("dir", "ltr");
                    }
                     
                    //Start of Set text
                    instructionTitle.Text = string.Format(CultureInfo.CurrentCulture, Resources.Resource.LPASetupInsTitle.ToString(CultureInfo.CurrentCulture), protocol);

                    printOutBtn.Value = string.Format(CultureInfo.CurrentCulture, "{0}", Resources.Resource.Print.ToString(CultureInfo.CurrentCulture));

                    insMsg.Text = string.Format(CultureInfo.CurrentCulture, "{0}", Resources.Resource.LPASetupInsStartMsg.ToString(CultureInfo.CurrentCulture));
                   
                    step1.Text = string.Format(CultureInfo.CurrentCulture, "{0}", Resources.Resource.LPASetupInsStep1.ToString(CultureInfo.CurrentCulture));

                    //string lpaIconLocation = @"<img src=""img/lf/common/LPDI.png"" class=""valign""/>"; //Icon for LogPad App
                    string lpaIconLocation = @"<img src=""img/lf/common/small_logo.png"" class=""valign2""/>";
                    step2.Text = string.Format(CultureInfo.CurrentCulture, Resources.Resource.LPASetupInsStep2.ToString(CultureInfo.CurrentCulture), lpaIconLocation.ToString(CultureInfo.InvariantCulture));

                    string lfStudyUrl = study.WebDiaryStudyBaseUrl.ToString(CultureInfo.InvariantCulture);
                    int startPosition = lfStudyUrl.IndexOf("//") + "//".Length;
                    int endPosition = lfStudyUrl.IndexOf(".");
                    string lfStudyName = lfStudyUrl.Substring(startPosition, endPosition - startPosition);

                    string pinImgLocation = @"<img src=""img/lf/common/pin.png"" class=""valign""/>";
                    if (subject.Pin.HasValue)
                    {
                        step3.Text = string.Format(CultureInfo.CurrentCulture, Resources.Resource.LPASetupInsStep3.ToString(CultureInfo.CurrentCulture), lfStudyName, pinImgLocation.ToString(CultureInfo.InvariantCulture));
                    }

                    if (subject.Pin.HasValue)
                    {
                        step4.Text = string.Format(CultureInfo.CurrentCulture, Resources.Resource.LPASetupInsStep4.ToString(CultureInfo.CurrentCulture), subject.Pin.ToString());
                    }
                    else
                        throw new NetProException("Pin for the subject krPT " + subject.KrPT + " can not be empty");

                    setupcodeImg.Src = GetImageLocation(language, "setupcode.bmp");

                    step5.Text = string.Format(CultureInfo.CurrentCulture, "{0}", Resources.Resource.LPASetupInsStep5.ToString(CultureInfo.CurrentCulture));
                    passwordImg.Src = GetImageLocation(language, "password.bmp");

                    step5Note.Text = string.Format(CultureInfo.CurrentCulture, "{0}", Resources.Resource.LPASetupInsStep5Note.ToString(CultureInfo.CurrentCulture));

                    step6.Text = string.Format(CultureInfo.CurrentCulture, "{0}", Resources.Resource.LPASetupInsStep6.ToString(CultureInfo.CurrentCulture));
                    securityquestionImg.Src = GetImageLocation(language, "securityquestion.bmp");

                    insEndMsg.Text = string.Format(CultureInfo.CurrentCulture, "{0}", Resources.Resource.LPASetupInsEndMsg.ToString(CultureInfo.CurrentCulture));

                    phtCo.Text = string.Format(CultureInfo.CurrentCulture, "{0}", Resources.Resource.SMPHT.ToString(CultureInfo.CurrentCulture));
                    //End of Set text
                }
            }
            catch (NetProException npex)
            {
                logger.Error(npex.Message, npex);
                litResult.Text = Resources.Resource.GeneralErrorMsg;
                litResult.Visible = true;
                container.Visible = false;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                litResult.Text = Resources.Resource.GeneralErrorMsg;
                litResult.Visible = true;
                container.Visible = false;
            }
        }

        protected string GetImageLocation(string languageCode, string fileName)
        {
            //Check file exist or not;
            string imgFile = HttpContext.Current.Server.MapPath(subjectInstructionImagPath + languageCode + "/" + fileName);
            if(File.Exists(imgFile))
                return subjectInstructionImagPath + languageCode + "/" + fileName;
            else
            {
                languageCode = WDConstants.DefaultLanguageCode;
                return subjectInstructionImagPath + languageCode + "/" + fileName;
            }
        }
    }
}