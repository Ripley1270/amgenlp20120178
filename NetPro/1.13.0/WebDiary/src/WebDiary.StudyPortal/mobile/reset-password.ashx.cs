﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Web.Security;

using WebDiary.Controls;
using WebDiary.Core.Enums;
using WebDiary.Membership.Provider;
using WebDiary.Membership.User;

using log4net;
using System.Text;
using System.Globalization;
using WebDiary.Core.Entities;
using System.Configuration;
using WebDiary.Core.Repositories;
using WebDiary.Core.Constants;
using WebDiary.Core.Helpers;

namespace WebDiary.StudyPortal.Mobile.Services
{
    /// <summary>
    /// Summary description for reset_password1
    /// </summary>
    public class ResetPassword : IHttpHandler, IRequiresSessionState
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(ResetPassword));
        private Guid _token = Guid.Empty;

        public void ProcessRequest(HttpContext context)
        {
            // Set culture
            SubjectHelper.SetCulture(context);

            context.Response.ContentType = "text/plain";
            try
            {
                _token = new Guid(context.Request.Form["rpfToken"]);
            }
            catch
            {
                context.Response.Write(Resources.Resource.invalidToken);
                return;
            }

            WebDiaryProvider provider = System.Web.Security.Membership.Provider as WebDiaryProvider;
            Guid providerUserKey = (Guid)provider.GetProviderUserKeyByForgotPasswordToken(_token);
            WebDiaryUser user = (WebDiaryUser)provider.GetUser(providerUserKey, false);

            if (user == null)
            {
                context.Response.Write(Resources.Resource.invalidToken);
                return;
            }

            try
            {
                using (WebDiaryContext db = new WebDiaryContext(ConfigurationManager.ConnectionStrings["default"].ConnectionString))
                {
                    SubjectRepository subjectRep = new SubjectRepository(db);
                    Subject subject = subjectRep.FindById((Guid)user.ProviderUserKey);
                    ResetPasswordStatus status = provider.ResetPassword(user, context.Request.Form["rpfEmail"], _token, context.Request.Form["rpfPassword"]);
                    if (status == ResetPasswordStatus.Success)
                    {
                        SurveyScheduler.WriteEmailSchedule((int)WDConstants.EmailType.ResetPassword, subject, DateTime.UtcNow, false, true);                        
                        context.Response.Write("passed");
                        return;
                    }

                    switch (status)
                    {
                        case ResetPasswordStatus.InvalidEmail:
                            context.Response.Write(Resources.Resource.EmailDoesntMatchsubmittedOne);
                            break;
                        case ResetPasswordStatus.ProviderError:
                            context.Response.Write(Resources.Resource.ErrorUpdatingUrPswd);
                            break;
                    }
                }
            }
            catch (MembershipPasswordException e_mpe)
            {
                switch (e_mpe.Message)
                {
                    case WebDiaryProvider.PasswordFormatError:
                        context.Response.Write(Resources.Resource.PasswordFormatError);
                        break;
                    case WebDiaryProvider.PasswordMinCharError:
                        context.Response.Write(String.Format(CultureInfo.CurrentCulture, Resources.Resource.PasswordMinCharError, provider.MinRequiredPasswordLength));
                        break;
                    case WebDiaryProvider.PasswordMinNonAlphaCharError:
                        context.Response.Write(String.Format(CultureInfo.CurrentCulture, Resources.Resource.PasswordMinNonAlphaNumError, provider.MinRequiredNonAlphanumericCharacters));
                        break;
                    case WebDiaryProvider.PasswordValidationError:
                        context.Response.Write(Resources.Resource.PasswordValidationError);
                        break;
                    case WebDiaryProvider.PasswordEqualError:
                        context.Response.Write(Resources.Resource.PasswordEqualError);
                        break;
                    default:
                        context.Response.Write(e_mpe.Message);
                        break;
                }
            }
            catch (Exception ex)
            {
                context.Response.Write(Resources.Resource.ErrorUpdatingUrPswd);
                logger.Error(ex.Message, ex);
            }
        }

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }
    }
}