﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mobile/mobile.Master" AutoEventWireup="true" CodeBehind="home.aspx.cs" Inherits="WebDiary.StudyPortal.Mobile.Home" %>
        
        <asp:Content ID="LeftToolbarContent" ContentPlaceHolderID="LeftToolbarPlaceHolder" runat="server">
            <a href="#" onclick="logout();" data-icon="arrow-l" data-theme="c"><%= Resources.Resource.SMLogout %></a>
        </asp:Content>
		<asp:Content ID="RightToolbarContent" ContentPlaceHolderID="RightToolbarPlaceHolder" runat="server">
            <a href="settings.aspx" data-icon="gear" class="ui-btn-right" data-theme="c" data-transition="slide"><%= Resources.Resource.SettingsPageLnk %></a>
	    </asp:Content>

         <asp:Content ID="mainContent" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
            <div class="center-wrapper">
                <asp:Literal ID="litHomeErrors" runat="server"></asp:Literal>
                <div><asp:Literal ID="litSurveyInstr" runat="server"></asp:Literal></div>

                <!-- Required questionnaire list -->
                <div id="questionaireWrapper"></div>

                <!-- History list -->
                <ul id="historyList" data-role="listview" data-inset="true"><li data-role="list-divider"><%= Resources.Resource.RecentQuestionnaires %></li></ul>
                <a id="loadMoreBtn" href="#" data-role="button" data-inline="true" onclick="populateHistoryList()"><%= Resources.Resource.LoadMore %></a>
                <asp:Literal ID="notificationScriptID" runat="server"></asp:Literal>

                <!-- Time Travel Utility -->
                <asp:Panel ID="DTPanel" CssClass="pht_np_sub_pgHeader" runat="server">                    
                    <label for="txtTimeTravel"><%= Resources.Resource.TTravel%></label>
                    <asp:Literal ID="litTimeTravel" runat="server"></asp:Literal>
                    <input type="button" id="btnApply" onclick="applyTimeTravel($('#txtTimeTravel').val())" data-inline="true" data-role="none" value="<%= Resources.Resource.ApplyTTravel%>" />                    
                </asp:Panel>
            </div>
         </asp:Content>
