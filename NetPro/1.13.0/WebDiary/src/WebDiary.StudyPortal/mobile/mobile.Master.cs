﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Text;

using WebDiary.Core.Constants;
using WebDiary.Core.Entities;
using WebDiary.Core.Helpers;

using log4net;
using WebDiary.Core.Repositories;
using WebDiary.StudyPortal.Includes.Usercontrols;
using System.Globalization;


namespace WebDiary.StudyPortal.Mobile
{
    public partial class Mobile : Controls.BaseMasterPage
    {
        private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static string client_page_id = "";

        protected string BasePath
        {
            get
            {
                return String.Concat(HttpHelper.GetBaseUrlWithScheme(), BaseUrl);
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (ConfigurationManager.AppSettings["SUBJECT_SESSION_TIMEOUT"] != null)
                Session.Timeout = int.Parse(ConfigurationManager.AppSettings["SUBJECT_SESSION_TIMEOUT"]);

            string returnUrl = "";
            if (SessionHelper.IsSiteSession())
            {
                using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                {
                    returnUrl = StudyData.study.SsaBaseUrl + SWAPI.SWAPIConstants.SessionTimeoutPage;
                }
            }
            else
            {
                returnUrl = StudyData.study.WebDiaryStudyBaseUrl.TrimEnd('/') + "/mobile/logout.aspx";
            }
            if (!IsPostBack)
            {
                //Initialize session timeout script
                litScript.Text = "<script type=\"text/javascript\">var sTimeout = " + Session.Timeout * 60000 + "; var returnUrl = '" + returnUrl + "';</script>";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            switch (Page.GetType().Name)
            {
                case "mobile_index_aspx":
                    client_page_id = "login";
                    break;
                case "mobile_home_aspx":
                    client_page_id = "home";
                    break;
                case "mobile_settings_aspx":
                    client_page_id = "settings";
                    break;
                case "mobile_change_password_aspx":
                    client_page_id = "password";
                    break;
                case "mobile_change_timezone_aspx":
                    client_page_id = "timezone";
                    break;
                case "mobile_forgot_password_aspx":
                    client_page_id = "forgotPassword";
                    break;
                default:
                    client_page_id = "unknown";
                    break;
            }
            Page.DataBind();
            if (Context.User != null)
            {
                if (StudyData.version.ProductEnvironment == Core.Helpers.Version.EnvironmentStaging
                        || StudyData.version.ProductEnvironment == Core.Helpers.Version.EnvironmentDeveloper)
                {
                    lblNetProVer.InnerText = Resources.Resource.SMNetPROVer + StudyData.version.ProductVersionFullBuild;
                    lblStudyVer.InnerText = Resources.Resource.SMStudyVer + StudyData.version.StudyVersionFullBuild;
                }
                else
                {
                    lblNetProVer.InnerText = Resources.Resource.SMNetPROVer + StudyData.version.ProductVersionShort;
                    liStudyVer.Visible = false;
                }
                lnkContactUs.InnerText = Resources.Resource.FooterContactUs;
            }
        }
    }
}
