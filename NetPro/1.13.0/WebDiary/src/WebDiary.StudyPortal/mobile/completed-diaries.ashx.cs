﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Globalization;
using System.Text;

using WebDiary.Core.Entities;
using WebDiary.Controls;
using WebDiary.Core.Helpers;
using WebDiary.Core.Repositories;
using WebDiary.Core.Constants;
using WebDiary.SWAPI;
using WebDiary.SWAPI.SWData;
using WebDiary.WDAPI;
using WebDiary.SurveyToolDBAdapters;
using WebDiary.SurveyToolDBAdapters.Checkbox_Entities;

using log4net;
using System.Collections;
using WebDiary.Core.Errors;
using WebDiary.Membership.User;
using WebDiary.Membership.Provider;

namespace WebDiary.StudyPortal.mobile
{
    /// <summary>
    /// Summary description for completed_diaries
    /// </summary>
    public class completed_diaries : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            // Set culture
            SubjectHelper.SetCulture(context);

            context.Response.ContentType = "application/json";
            if (context.User.Identity.IsAuthenticated)
            {
                using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                {
                    SubjectRepository subjectRep = new SubjectRepository(db);
                    Subject subject = subjectRep.FindById(new Guid(context.User.Identity.Name));
                    int studyId = subject.StudyId;
                    WDAPIObject wd = new WDAPIObject(StudyData.study.StudyWorksUsername, StudyData.study.StudyWorksPassword, StudyData.study.Name, StudyData.study.StudyWorksBaseUrl);

                    var surveyList = db.StudySurveys.Where(s => s.StudyId == studyId && s.FormType == SWAPIConstants.FormTypeSubjectSubmit);                    
                    List<List<string>> completedDiaries = new List<List<string>>();
                    ArrayList returnItems = new ArrayList(1);
                    returnItems.Add("SU.ReportStartDate");

                    foreach (var survey in surveyList)
                    {
                        //pull the list of last completed diaries
                        ClinData clinData = wd.GetClinDataByDateRange(subject.KrPT, survey.KRSU, "startts", DateTime.UtcNow.AddDays(-14), DateTime.UtcNow.AddHours(1), returnItems);
                        foreach (List<string> item in clinData.datatable)
                            item.Add(survey.DisplayName);
                        completedDiaries.AddRange(clinData.datatable);
                    }

                    var compDiaryList = from c in completedDiaries
                                        select new
                                        {
                                            DateReport = FormatDT(c[0]),
                                            SU = c[1],
                                            Date = DateTime.Parse(c[0])
                                        };

                    int skip = int.Parse(context.Request["skip"]);
                    int take = int.Parse(context.Request["take"]);
                    int count = compDiaryList.Count();
                    StringBuilder sb = new StringBuilder();
                    sb.AppendFormat("{{\"count\":{0}, \"diaries\":[", count);
                    if (count > 0)
                    {
                        foreach (var diary in compDiaryList.OrderByDescending(d => d.Date).Skip(skip).Take(take))
                        {
                            sb.AppendFormat("{{\"dateReport\":\"{0}\",\"su\":\"{1}\"}},", diary.DateReport, diary.SU);
                        }
                    }
                    else
                    {
                        sb.AppendFormat("{{\"dateReport\":\"{0}\",\"su\":\"\"}},", Resources.Resource.NoQuestionnairesEntered);
                    }
                    context.Response.Write(sb.ToString().TrimEnd(',') + "]}");
                }
            }
            else
            {
                context.Response.StatusCode = 403;
            }
        }

        private string FormatDT(string dtStr)
        {
            DateTime dt = DateTime.Parse(dtStr, CultureInfo.InvariantCulture);
            return dt.ToString(SWAPIConstants.NetProShortDateTime, CultureInfo.CurrentCulture);
        }

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }
    }
}