﻿using System;
using System.Web;
using System.Web.SessionState;
using System.Globalization;
using System.Web.Security;
using System.Configuration;
using WebDiary.Membership.Provider;
using WebDiary.Membership.User;
using WebDiary.Core.Entities;
using WebDiary.Core.Helpers;
using WebDiary.Core.Constants;
using WebDiary.WDAPI;
using log4net;
using WebDiary.Core.Repositories;
namespace WebDiary.StudyPortal.mobile{
    /// <summary>
    /// Summary description for login
    /// </summary>
    public class Login : IHttpHandler, IRequiresSessionState{
        private static readonly ILog Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public void ProcessRequest(HttpContext context){
            // Set culture
            SubjectHelper.SetCulture(context);
            context.Response.ContentType = "text/html";
            if(context.Session["isDBAccessible"] != null &&
               context.Session["isDBAccessible"].ToString().Equals("db_NotAccessible")){
                context.Response.Write(Resources.Resource.DBNotAccessibleError);
            } else{
                context.Response.Write(ValidateUser(context));
            }
        }
        public bool IsReusable{
            get{return false;}
        }
        string ValidateUser(HttpContext context){
            WebDiaryProvider provider = System.Web.Security.Membership.Provider as WebDiaryProvider;
            string returnValue = string.Empty;
            if(provider != null){
                using(WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString)){
                    SubjectRepository subRep = new SubjectRepository(db);
                    Subject subject = subRep.FindByEmailAndStudyId(context.Request.Form["txtEmail"].Trim(), StudyData.study.Id);
                    WebDiaryUser user = null;
                    if(subject != null)
                        user = (WebDiaryUser)provider.GetUser(subject, db, false);
                    if(user == null){
                        ClearSession(context);
                        Logger.Info("Failed login attempt with invalid e-mail address.");
                        return Resources.Resource.eMailPswdInvalid;
                    }

                    //PDE block mobile access by default - should only be enabled if contracted for mobile support
                    ClearSession(context);
                    Logger.Info("Support for mobile device is not enabled yet, please use desktop browser.");
                    //return Resources.PDEResource.disableMobile;
                    //PDE block mobile access until mobile support is supposed to be turned on
                    context.Session[WDConstants.Language] = context.Response.Cookies[WDConstants.Language].Value = WDAPIObject.JavaLocaleToDotNetCulture(user.Language);
                    WDAPIObject wd = null;
                    if(StudyData.study != null){
                        wd = new WDAPIObject(StudyData.study.StudyWorksUsername, StudyData.study.StudyWorksPassword, StudyData.study.Name, StudyData.study.StudyWorksBaseUrl);
                    }
                    if(provider.ValidateUser(subject, db, context.Request.Form["txtPassword"])){
                        if(ConfigurationManager.AppSettings["SUBJECT_SESSION_TIMEOUT"] != null)
                            context.Session.Timeout = int.Parse(ConfigurationManager.AppSettings["SUBJECT_SESSION_TIMEOUT"]);
                        context.Session["userName"] = user.Email;
                        context.Session[WDConstants.VarKrpt] = user.KrPT;

                        //Set a cookie for when the user logs in so that we can handle timeout message on the login page.
                        context.Response.Cookies.Add(new HttpCookie(WDConstants.UserStatusCookie, WDConstants.UserStatusLoggedIn));

                        // Check status against StudyWorks
                        SubjectStudyEnabled result = wd.IsSubjectAndStudyEnabled(user.KrPT, StudyData.study);
                        switch(result){
                            case SubjectStudyEnabled.SubjectDisabled:
                                return Resources.Resource.ErrorSubjectDisabled;
                            case SubjectStudyEnabled.StudyDisabled:
                                return Resources.Resource.ErrorStudyDisabled;
                            case SubjectStudyEnabled.Error:
                                return String.Format(CultureInfo.InvariantCulture, Resources.Resource.AppErrHasOccurred, "<a href=\"https://mystudy.phtstudy.com/ssa/pages/contact_us/\">", "</a>");
                            case SubjectStudyEnabled.Enabled:
                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }

                        // check if the current password has expired
                        if(StudyData.study.PasswordExpiryDays > 0 &&
                           (DateTimeHelper.GetUtcTime() - (DateTime)subject.LastPasswordChangedDate).TotalDays >= StudyData.study.PasswordExpiryDays){
                            Guid token = provider.CreateForgotPasswordRequest((Guid)user.ProviderUserKey);
                            context.Session[WDConstants.ResetPasswordText] = Resources.Resource.PasswordExpired;
                            return "reset-password.aspx?token=" + token;
                        }

                        //set login flag for validating session on checkbox
                        context.Session["login"] = true;
                        wd.BeginSendSecurityEvent(HttpHelper.GetIP4Address(), SWAPI.SWAPIAbstraction.SecurityEventValidEntry, DateTimeHelper.GetUtcTime(), user.KrPT, DateTimeHelper.GetTimeZoneOffsetMilliseconds(user.TimeZone.ToString(CultureInfo.InvariantCulture), user.StudyId), "Valid login", new AsyncCallback(SendSecurityEventCallback));
                        FormsAuthentication.SetAuthCookie(user.ProviderUserKey.ToString(), false);
                        Logger.Info("The subject successfully logged in. Subject krpt: " + user.KrPT);
                        if(!string.IsNullOrEmpty(context.Request.Form["surveyref"])){
                            return "home.aspx?surveyref=" + context.Request.Form["surveyref"];
                        }
                        return "passed";
                    }
                    ClearSession(context);

                    // Users state may have changed since calling ValidateUser, repopulate
                    user = (WebDiaryUser)provider.GetUser(subject, db, false);
                    if(user.IsLockedOut){
                        returnValue = Resources.Resource.AcctLockedExcessiveFailedLoginAttempts;
                    } else{
                        returnValue = Resources.Resource.eMailPswdInvalid;
                        wd.BeginSendSecurityEvent(HttpHelper.GetIP4Address(), SWAPI.SWAPIAbstraction.SecurityEventInvalidEntry, DateTimeHelper.GetUtcTime(), user.KrPT, DateTimeHelper.GetTimeZoneOffsetMilliseconds(user.TimeZone.ToString(CultureInfo.InvariantCulture), user.StudyId), "Invalid login", new AsyncCallback(SendSecurityEventCallback));
                    }
                }
            }
            return returnValue;
        }
        public void SendSecurityEventCallback(IAsyncResult ia){
            try{
                var ar = (System.Runtime.Remoting.Messaging.AsyncResult)ia;
                var del = (SendSecurityEventDelegate)ar.AsyncDelegate;
                del.EndInvoke(ia);
            } catch(Exception ex){
                Logger.Error(ex.Message, ex);
            }
        }
        private void ClearSession(HttpContext context){
            FormsAuthentication.SignOut();
            context.Session.Clear();
            context.Session.Abandon();
        }
    }
}