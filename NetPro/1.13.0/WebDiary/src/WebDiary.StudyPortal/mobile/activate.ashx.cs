﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Web.Security;

using WebDiary.Membership.Provider;
using WebDiary.Membership.User;
using WebDiary.Controls;
using WebDiary.Core.Enums;
using WebDiary.Core.Entities;
using WebDiary.Core.Helpers;
using WebDiary.Core.Constants;
using WebDiary.Core.Email;
using WebDiary.Core.Repositories;
using WebDiary.WDAPI;

using log4net;
using System.Globalization;

namespace WebDiary.StudyPortal.Mobile.Services
{
    /// <summary>
    /// Summary description for activate1
    /// </summary>
    public class Activate : IHttpHandler, IRequiresSessionState
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(Activate));

        public void ProcessRequest(HttpContext context)
        {
            // Set culture
            SubjectHelper.SetCulture(context);

            context.Response.ContentType = "text/plain";

            WebDiaryProvider provider = System.Web.Security.Membership.Provider as WebDiaryProvider;
            
            try
            {
                Guid _token = new Guid(context.Request.Form["afToken"]);
                WebDiaryUser user = (WebDiaryUser)provider.GetUserByActivationToken(_token);
                MembershipActivateStatus status = provider.Activate(context.Request.Form["afEmail"].Trim(), _token, context.Request.Form["afPassword"]);
                if (status == MembershipActivateStatus.Success)
                {
                    try
                    {
                        using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                        {
                            SubjectRepository subjectRep = new SubjectRepository(db);
                            Subject subject = subjectRep.FindById((Guid)user.ProviderUserKey);
                            SurveyScheduler.WriteEmailSchedule((int)WDConstants.EmailType.Confirmation, subject, DateTime.UtcNow, false, true);
                            SurveyScheduler.ScheduleEmailAllSurveys(subject, StudyData.study, SurveyScheduler.ScheduleState.Activation);
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex.Message, ex);
                    }

                    context.Response.Write("passed");
                }
                else
                {
                    switch (status)
                    {
                        case MembershipActivateStatus.InvalidToken:
                            context.Response.Write(Resources.Resource.UProvidedAnIinvalidToken);
                            break;
                        case MembershipActivateStatus.InvalidUser:
                            context.Response.Write(Resources.Resource.rfvEmailNotValid);
                            break;
                        case MembershipActivateStatus.TokenExpired:
                            context.Response.Write(Resources.Resource.UrTokenHasExpired);
                            break;
                        case MembershipActivateStatus.ProviderError:
                            context.Response.Write(String.Format(CultureInfo.InvariantCulture, Resources.Resource.AppErrHasOccurred, "<a href=\"https://mystudy.phtstudy.com/ssa/pages/contact_us/\">", "</a>"));
                            break;
                    }
                }
            }
            catch (MembershipPasswordException e_mpe)
            {
                switch (e_mpe.Message)
                {
                    case WebDiaryProvider.PasswordFormatError:
                        context.Response.Write(Resources.Resource.PasswordFormatError);
                        break;
                    case WebDiaryProvider.PasswordMinCharError:
                        context.Response.Write(String.Format(CultureInfo.CurrentCulture, Resources.Resource.PasswordMinCharError, provider.MinRequiredPasswordLength));
                        break;
                    case WebDiaryProvider.PasswordMinNonAlphaCharError:
                        context.Response.Write(String.Format(CultureInfo.CurrentCulture, Resources.Resource.PasswordMinNonAlphaNumError, provider.MinRequiredNonAlphanumericCharacters));
                        break;
                    case WebDiaryProvider.PasswordValidationError:
                        context.Response.Write(Resources.Resource.PasswordValidationError);
                        break;
                    case WebDiaryProvider.PasswordEqualError:
                        context.Response.Write(Resources.Resource.PasswordEqualError);
                        break;
                    default:
                        context.Response.Write(e_mpe.Message);
                        break;
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                context.Response.Write(String.Format(CultureInfo.InvariantCulture, Resources.Resource.AppErrHasOccurred, "<a href=\"https://mystudy.phtstudy.com/ssa/pages/contact_us/\">", "</a>"));
            }
        }

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }
    }
}