﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebDiary.StudyPortal.mobile
{
    public partial class timeout_redirect : Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Session.Clear();
            Session.Abandon();
            HttpCookie timeoutCookie = new HttpCookie("timeout");
            timeoutCookie.Value = "1";
            timeoutCookie.Expires = DateTime.Now.AddDays(1);
            Response.SetCookie(timeoutCookie);
            Response.Redirect("~/mobile/");
        }
    }
}