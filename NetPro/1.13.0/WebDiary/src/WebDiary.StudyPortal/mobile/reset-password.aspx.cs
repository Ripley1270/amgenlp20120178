﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using WebDiary.Controls;
using WebDiary.Core.Enums;
using WebDiary.Membership.Provider;
using WebDiary.Membership.User;
using WebDiary.WDAPI;

using log4net;
using System.Text;
using System.Globalization;
using WebDiary.Core.Entities;
using System.Configuration;
using WebDiary.Core.Repositories;
using WebDiary.Core.Constants;
using WebDiary.StudyPortal.Helpers;
using WebDiary.Core.Helpers;

namespace WebDiary.StudyPortal.Mobile
{
    public partial class ResetPassword : BasePage
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(ResetPassword));
        private Guid _token = Guid.Empty;

        protected override void InitializeCulture()
        {
            try
            {
                using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                {
                    _token = new Guid(Request.QueryString["token"]);
                    ForgotPasswordRequestRepository repo = new ForgotPasswordRequestRepository(db);
                    ForgotPasswordRequest request = repo.FindById(_token);
                    
                    if (request != null)
                    {
                        string language = WDAPIObject.JavaLocaleToDotNetCulture(request.Subject.Language);
                        UICulture = Culture = language;
                        Session[WDConstants.Language] = Response.Cookies[WDConstants.Language].Value = language;
                        base.InitializeCulture();
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                HttpBrowserCapabilities browser = Request.Browser;
                bool isAuthorizedBrowser = AuthorizedBrowser.CheckAuthorizedBrowser(StudyData.study.Name, browser.Browser, browser.MajorVersion);
                if (!isAuthorizedBrowser)
                {
                    authBrowser.Value = "false";
                    lblUnauthorizedMessage.Visible = true;
                    string browserNameVersion = browser.Browser;

                    if (string.Equals(browserNameVersion, "Edge", StringComparison.OrdinalIgnoreCase))
                        browserNameVersion = browserNameVersion + "HTML" + " " + browser.MajorVersion;
                    else
                        browserNameVersion = browserNameVersion + " " + browser.MajorVersion;
                    lblUnauthorizedMessage.Text = String.Format(CultureInfo.CurrentCulture, Resources.Resource.UnauthorizedBrowserMsg.ToString(CultureInfo.CurrentCulture), browserNameVersion);

                    List<string> authorizedBrowser = AuthorizedBrowser.GetAuthorizedBrowser2(StudyData.study.Name);

                    string browserList = AuthorizedBrowser.GenerateBrowserList(authorizedBrowser);

                    authBrowserArea.InnerHtml = browserList;

                    logger.Info(String.Format(CultureInfo.InvariantCulture, "Use of {0} is not supported. Browser Name: {1}, Major version:{2}, userAgent: {3}, token in request: {4}", browserNameVersion, browser.Browser, browser.MajorVersion, Request.UserAgent, Request.QueryString["token"]));
                    return;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }

            try
            {
                _token = new Guid(Request.QueryString["token"]);
                litToken.Text = "<input type=\"hidden\" name=\"rpfToken\" value=\"" + Request.QueryString["token"] + "\" />";
            }
            catch
            {
                litServerError.Text = Resources.Resource.invalidToken;
                pnlForm.Visible = false;
            }
            

            // Check token on page load
            WebDiaryProvider provider = System.Web.Security.Membership.Provider as WebDiaryProvider;

            if (provider.IsValidForgotPasswordToken(_token))
            {
                pnlForm.Visible = true;
                if (Session[WDConstants.ResetPasswordText] != null)
                    lblResetPswrdMsgBody.Text = Session[WDConstants.ResetPasswordText].ToString();
            }
            else
            {
                litServerError.Text = Resources.Resource.invalidToken;
                pnlForm.Visible = false;
            }

            String loginlink = String.Format(CultureInfo.CurrentCulture, "<a href=\"" + StudyData.study.WebDiaryStudyBaseUrl.TrimEnd('/') + "/mobile/\">{0}</a>", Resources.Resource.LoginPage);
            String confirm = String.Format(CultureInfo.CurrentCulture, Resources.Resource.ResetPasswdConfirm, loginlink);
            litConfirmMsg.Text = String.Format(CultureInfo.CurrentCulture, "<p>{0}</p>", confirm);
        }
    }
}