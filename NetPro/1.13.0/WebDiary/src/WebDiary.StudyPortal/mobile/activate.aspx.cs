﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using WebDiary.Membership.Provider;
using WebDiary.Membership.User;
using WebDiary.Controls;
using WebDiary.Core.Enums;
using WebDiary.Core.Entities;
using WebDiary.Core.Helpers;
using WebDiary.Core.Constants;
using WebDiary.Core.Email;
using WebDiary.Core.Repositories;
using WebDiary.WDAPI;
using WebDiary.StudyPortal.Helpers;

using log4net;
using System.Globalization;


namespace WebDiary.StudyPortal.Mobile
{
    public partial class Activate : BasePage
    {
        private Guid _token = Guid.Empty;
        private static readonly ILog logger = LogManager.GetLogger(typeof(Activate));
        WebDiaryUser user;

        protected override void InitializeCulture()
        {
            try
            {
                _token = new Guid(Request.QueryString["token"]);
                WebDiaryProvider provider = System.Web.Security.Membership.Provider as WebDiaryProvider;
                user = (WebDiaryUser)provider.GetUserByActivationToken(_token);
                if (user != null)
                {
                    string language = WDAPIObject.JavaLocaleToDotNetCulture(user.Language);
                    UICulture = Culture = language;
                    Session[WDConstants.Language] = Response.Cookies[WDConstants.Language].Value = language;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }

            base.InitializeCulture();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                HttpBrowserCapabilities browser = Request.Browser;
                bool isAuthorizedBrowser = AuthorizedBrowser.CheckAuthorizedBrowser(StudyData.study.Name, browser.Browser, browser.MajorVersion);
                if (!isAuthorizedBrowser)
                {
                    authBrowser.Value = "false";
                    lblUnauthorizedMessage.Visible = true;
                    string browserNameVersion = browser.Browser;

                    if (string.Equals(browserNameVersion, "Edge", StringComparison.OrdinalIgnoreCase))
                        browserNameVersion = browserNameVersion + "HTML" + " " + browser.MajorVersion;
                    else
                        browserNameVersion = browserNameVersion + " " + browser.MajorVersion;
                    lblUnauthorizedMessage.Text = String.Format(CultureInfo.CurrentCulture, Resources.Resource.UnauthorizedBrowserMsg.ToString(CultureInfo.CurrentCulture), browserNameVersion);

                    List<string> authorizedBrowser = AuthorizedBrowser.GetAuthorizedBrowser2(StudyData.study.Name);

                    string browserList = AuthorizedBrowser.GenerateBrowserList(authorizedBrowser);
                    authBrowserArea.InnerHtml = browserList;

                    logger.Info(String.Format(CultureInfo.InvariantCulture, "Use of {0} is not supported. Browser Name: {1}, Major version:{2}, userAgent: {3}, token in request: {4}", browserNameVersion, browser.Browser, browser.MajorVersion, Request.UserAgent, Request.QueryString["token"]));
                    return;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }

            try
            {
                _token = new Guid(Request.QueryString["token"]);
            }
            catch
            {
                litServerError.Text = Resources.Resource.invalidToken;
                pnlForm.Visible = false;
            }
            
            // Check token on page load
            WebDiaryProvider provider = System.Web.Security.Membership.Provider as WebDiaryProvider;
            user = (WebDiaryUser)provider.GetUserByActivationToken(_token);

            if (user == null)
            {
                litServerError.Text = Resources.Resource.invalidToken;
                pnlForm.Visible = false;
            }
            else
            {
                using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                {
                    WDAPIObject wd = new WDAPIObject(StudyData.study.StudyWorksUsername, StudyData.study.StudyWorksPassword, StudyData.study.Name, StudyData.study.StudyWorksBaseUrl);
                    SubjectStudyEnabled result = wd.IsSubjectAndStudyEnabled(user.KrPT, StudyData.study);
                    if (result == SubjectStudyEnabled.SubjectDisabled)
                    {
                        litServerError.Text = Resources.Resource.AcctCannotBeActivated;
                        pnlForm.Visible = false;
                    }
                    else if (result == SubjectStudyEnabled.StudyDisabled)
                    {
                        litServerError.Text = Resources.Resource.ErrorStudyDisabled;
                        pnlForm.Visible = false;
                    }
                    else if (result == SubjectStudyEnabled.Error)
                    {
                        litServerError.Text = String.Format(CultureInfo.InvariantCulture, Resources.Resource.AppErrHasOccurred, "<a href=\"https://mystudy.phtstudy.com/ssa/pages/contact_us/\" data-ajax=\"false\">", "</a>");
                        pnlForm.Visible = false;
                    }
                    else if (user.IsApproved)
                    {
                        String loginlink = String.Format(CultureInfo.CurrentCulture, "<a href=\"" + StudyData.study.WebDiaryStudyBaseUrl.TrimEnd('/') + "/mobile/\">{0}</a>", Resources.Resource.PleaseLogin);
                        litServerError.Text = String.Format(CultureInfo.CurrentCulture, Resources.Resource.AcctAlreadyActivated, loginlink);
                        pnlForm.Visible = false;
                    }
                    else if (DateTime.UtcNow.CompareTo(user.ActivationTokenExpiration) > 0)
                    {
                        litServerError.Text = Resources.Resource.ActivationTokenHasExpired;
                        pnlForm.Visible = false;
                    }
                    else
                    {
                        pnlForm.Visible = true;
                        litToken.Text = "<input type=\"hidden\" name=\"afToken\" value=\"" + Request.QueryString["token"] + "\" />";
                    }
                }
            }
        }
    }
}