﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Web.Security;
using System.Threading.Tasks;

using WebDiary.Core.Helpers;
using WebDiary.Membership.Provider;
using WebDiary.Membership.User;
using log4net;
using System.Text;
using System.Globalization;
using WebDiary.Core.Entities;
using System.Configuration;
using WebDiary.Core.Repositories;
using WebDiary.Core.Constants;
using WebDiary.Core.Enums;

namespace WebDiary.StudyPortal.Mobile.Services
{
    /// <summary>
    /// Summary description for change_password
    /// </summary>
    public class ChangePassword : IHttpHandler, IRequiresSessionState
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(ChangePassword));

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/html";
            if (!context.User.Identity.IsAuthenticated)
            {
                context.Response.StatusCode = 403;
                return;
            }

            // Set culture
            SubjectHelper.SetCulture(context);

            // generate a new token
            WebDiaryProvider provider = System.Web.Security.Membership.Provider as WebDiaryProvider;
            WebDiaryUser user = (WebDiaryUser)provider.GetUser(context.Session["userName"].ToString(), true);
            Guid token  = provider.CreateForgotPasswordRequest((Guid)user.ProviderUserKey);

            try
            {
                string oldPassword = context.Request.Form["txtOldPassword"];
                using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                {
                    SubjectRepository subjectRep = new SubjectRepository(db);
                    Subject subject = subjectRep.FindByEmailAndStudyId(context.Session["userName"].ToString(), StudyData.study.Id);

                    if (!provider.ValidatePassword(subject, oldPassword))
                    {
                        context.Response.Write(Resources.Resource.IncorrectOldPassword);
                    }
                    else
                    {
                        ResetPasswordStatus status = provider.ResetPassword(user, subject.Email, token, context.Request.Form["txtNewPassword"]);
                        if (status == ResetPasswordStatus.Success)
                        {
                            var task = Task.Factory.StartNew(() => SurveyScheduler.WriteEmailSchedule((int)WDConstants.EmailType.ResetPassword, subject, DateTime.UtcNow, false, true));
                            context.Response.Write("passed");
                        }
                        else
                        {
                            switch (status)
                            {
                                case ResetPasswordStatus.InvalidEmail:
                                    context.Response.Write(Resources.Resource.EmailDoesntMatchsubmittedOne);
                                    break;
                                case ResetPasswordStatus.ProviderError:
                                    context.Response.Write(Resources.Resource.ErrorUpdatingUrPswd);
                                    break;
                            }
                        }
                    }
                }
            }
            catch (MembershipPasswordException e_mpe)
            {
                switch (e_mpe.Message)
                {
                    case WebDiaryProvider.PasswordFormatError:
                        context.Response.Write(Resources.Resource.PasswordFormatError);
                        break;
                    case WebDiaryProvider.PasswordMinCharError:
                        context.Response.Write(String.Format(CultureInfo.CurrentCulture, Resources.Resource.PasswordMinCharError, provider.MinRequiredPasswordLength));
                        break;
                    case WebDiaryProvider.PasswordMinNonAlphaCharError:
                        context.Response.Write(String.Format(CultureInfo.CurrentCulture, Resources.Resource.PasswordMinNonAlphaNumError, provider.MinRequiredNonAlphanumericCharacters));
                        break;
                    case WebDiaryProvider.PasswordValidationError:
                        context.Response.Write(Resources.Resource.PasswordValidationError);
                        break;
                    case WebDiaryProvider.PasswordEqualError:
                        context.Response.Write(Resources.Resource.PasswordEqualError);
                        break;
                    default:
                        context.Response.Write(e_mpe.Message);
                        break;
                }
            }
            catch (Exception ex)
            {
                context.Response.Write(Resources.Resource.ErrorUpdatingUrPswd);
                logger.Error(ex.Message, ex);
            }
        }

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }
    }
}