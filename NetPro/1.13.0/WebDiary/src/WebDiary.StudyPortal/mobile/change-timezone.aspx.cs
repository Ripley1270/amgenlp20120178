﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using WebDiary.StudyPortal.Includes.Usercontrols;
using WebDiary.Core.Entities;
using WebDiary.Core.Repositories;
using WebDiary.Membership.Provider;
using WebDiary.Membership.User;
using WebDiary.Controls;
using log4net;
using WebDiary.Core.Helpers;
using WebDiary.WDAPI;
using System.Globalization;

namespace WebDiary.StudyPortal.Mobile
{
    public partial class ChangeTimeZone : BasePage
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(ChangeTimeZone));
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    //Get subject's timezone
                    using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                    {
                        SubjectRepository subjectRep = new SubjectRepository(db);
                        Subject subject = subjectRep.FindByEmailAndStudyId(Session["userName"].ToString(), StudyData.study.Id);

                        WDAPIObject wd = new WDAPIObject(StudyData.study.StudyWorksUsername, StudyData.study.StudyWorksPassword, StudyData.study.Name, StudyData.study.StudyWorksBaseUrl);

                        TimeZoneRepository tzresp = new TimeZoneRepository(db);
                        TimeZones tz = tzresp.FindByStudyIdAndTimeZoneId(StudyData.study.Id, subject.TimeZone);

                        litCurrentTimezone.Text = tz.TZName;

                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        foreach (var item in wd.GetTZList().list)
                        {
                            sb.AppendFormat(CultureInfo.InvariantCulture, "<input type=\"radio\" name=\"timezone-list\" id=\"timezone-choice-{1}\" value=\"{1}\"{2}  onclick=\"startAutoLock()\"> <label for=\"timezone-choice-{1}\">{0}</label>", item.decode, item.code, Int32.Parse(item.code, CultureInfo.InvariantCulture) == tz.TZId? " checked=\"checked\"" : "");
                        }

                        litTimezoneList.Text = sb.ToString();
                    }
                }
                catch (System.Net.WebException webex)
                {
                    //Session[WDError.ErrorMessageKey] = Resources.Resource.ServerDown;
                    //Response.Redirect(WDError.Path);
                    logger.Error("unable to pull timezone list from StudyWorks", webex);
                }
            }
        }
    }
}