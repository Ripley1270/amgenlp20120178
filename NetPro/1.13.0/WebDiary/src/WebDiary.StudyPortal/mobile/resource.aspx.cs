﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebDiary.Core.Constants;

namespace WebDiary.StudyPortal.mobile
{
    public partial class Resource : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string langCode = "en-US";

            if (!string.IsNullOrEmpty(Session[WDConstants.Language] as string))
            {
                UICulture = Culture = langCode = Session[WDConstants.Language].ToString();
            }
            else if (Request.Cookies[WDConstants.Language] != null && !string.IsNullOrEmpty(Request.Cookies[WDConstants.Language].Value))
            {
                UICulture = Culture = langCode = Request.Cookies[WDConstants.Language].Value;
            }

            Response.Cookies[WDConstants.Language].Value = langCode;
            Response.Cookies[WDConstants.Language].Expires = DateTime.Now.AddYears(1);

            //Use double quote for creating Javascript. Single quote in the Javascript causes error in French, Hebrew etc.
            //Add escape character if double quote exists in resource string. Some Hebrew resource strings contain double quote
            string script = @"var resources = {
                                        invalidEmail: """ + Resources.Resource.rfvEmailNotValid.Replace(@"""", @"\""") + @""",
                                        requiredEmail: """ + Resources.Resource.LoginrfvEmail.Replace(@"""", @"\""") + @""",
                                        requiredPassword: """ + Resources.Resource.LoginrfvPassword.Replace(@"""", @"\""") + @""",
                                        requiredOldPassword: """ + Resources.Resource.rfvOldPassword.Replace(@"""", @"\""") + @""",
                                        requiredNewPassword: """ + Resources.Resource.rfvNewPassword.Replace(@"""", @"\""") + @""",
                                        requiredConfirmPassword: """ + Resources.Resource.rfvPasswordConfirm.Replace(@"""", @"\""") + @""",
                                        passwordMismatch: """ + Resources.Resource.PwdsDontMatch.Replace(@"""", @"\""") + @""",
                                        requiredAffidavitCheck: """ + Resources.Resource.rfvCheckbox.Replace(@"""", @"\""") + @""",
                                        changesSavedMsg: """ + Resources.Resource.ChangesSavedMsg.Replace(@"""", @"\""") + @""",
                                        noConnection: """ + Resources.Resource.NotOnlineMessage.Replace(@"""", @"\""") + @"""
                                    };";

            Response.Write(script);
            Response.End();
        }
    }
}