﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Web.Security;

namespace WebDiary.StudyPortal.mobile
{
    /// <summary>
    /// Summary description for logout1
    /// </summary>
    public class logout1 : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            FormsAuthentication.SignOut();
            context.Session.Clear();
            context.Session.Abandon();
            context.Response.Write("loggedout");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}