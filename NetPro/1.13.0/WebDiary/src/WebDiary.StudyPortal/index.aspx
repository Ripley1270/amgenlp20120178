﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="WebDiary.StudyPortal.Index" MasterPageFile="~/includes/masters/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" type="text/css" href="includes/css/anytime.css" />
    <link rel="stylesheet" type="text/css" href="includes/css/dataTable.css" />    
    <script type="text/javascript" src="includes/scripts/anytime.js"></script>
    <script type="text/javascript" src="//ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
    <asp:Literal ID="notificationScriptID" EnableViewState="false" runat="server"></asp:Literal>
    <script type="text/javascript">
        
        $(document).ready(function () {
            AnyTime.picker("txtTimeTravel",
            { format: "%W, %M %d, %z %h:%i:%s %p", firstDOW: 1 });

            //initialize DataTable for history list
            $('#HistoryList').dataTable({
                "bProcessing": true,
                "bAutoWidth": false,
                "bSort": true,
                "bPaginate": true,
                "iDisplayLength": 10,
                "bLengthChange": false,
                "bFilter": false,
                "oLanguage": { "oPaginate": {"sNext":"", "sPrevious":""}, "sInfoEmpty": "<%= Resources.Resource.NoQuestionnairesEntered %>", "sEmptyTable": "<%= Resources.Resource.NoActivity %>", "sInfo": "<%= String.Format(System.Globalization.CultureInfo.CurrentCulture, Resources.Resource.PagingInfo, "_START_", "_END_", "_TOTAL_") %>" },
                "aoColumnDefs": [
                        { bSortable: false, "aTargets": [0] },
                        { bSortable: false, "aTargets": [1] },
                        { bSortable: false, "aTargets": [3] },
						{ "sType": "date", bSortable: true, "bSearchable": false, "bVisible": false, "aTargets": [ 2 ] }
					],
                "aaSorting": [[2, "desc"]]
            });
            
            window.setInterval(function(){                
                var now = new Date();
                if(now.getSeconds() == 0){ //sets the timer’s interval to 600000 milliseconds (or 60 seconds)
                    $('#<%= ButtonUpdate.ClientID %>').trigger('click');
                }
            }, 1000);
        });
    </script>
    
    <script type="text/javascript">
    //This is for screenshot of email messages in developer mode
        $(document).ready(function () {
           if ($('#SurveyList').length > 0) {
                if ($('#ScreenShotEmailList').find(":selected").val().toLowerCase() == "scheduledemailreminder") {
                    $('#SurveyList').show();
                }
                else {
                    $('#SurveyList').hide();
                }
            }
             });

        function checkSelectedEmailType() {
            var selectedEmailType = $('#ScreenShotEmailList').find(":selected").val();
            if (selectedEmailType.toLowerCase() == "scheduledemailreminder") {
                $('#SurveyList').show();
            }
            else
                $('#SurveyList').hide();
        }

        function showEmail() {
            //hide messages for sending email
            $('#lblEmailSuccess').hide();
            $('#lblEmailError').hide();

            var selectedLanguage = $('#ScreenShotLanguageList').find(":selected").val();
            var selectedEmailType = $('#ScreenShotEmailList').find(":selected").val();
            var selectedSurveyList = "";

            if (selectedEmailType.toLowerCase() == "scheduledemailreminder")
                selectedSurveyList = $('#SurveyList').find(":selected").val();

            var baseUrl = "https://" + window.location.hostname;
            var pathName = window.location.pathname.substring(0, window.location.pathname.lastIndexOf("/"));
            window.open(baseUrl + pathName + "/emailscreenshot.aspx?language=" + selectedLanguage + "&emailType=" + selectedEmailType + "&surveyId=" + selectedSurveyList, null, "width=700,height=500")
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <asp:ValidationSummary ID="vsErrors" CssClass="errors" runat="server" 
        meta:resourcekey="vsErrorsResource1" />
    <asp:ScriptManager runat="server" ID="IndexScriptManager" />

    <asp:Panel ID="PatientPnl" CssClass="pht_np_sub_pgHeader" runat="server">
        <asp:Label ID="patientInfoLbl" runat="server" Text="<%$ Resources:Resource, saPatientInfoLabel %>" CssClass="field"></asp:Label>
        <asp:Label ID="patientIDLbl" runat="server" CssClass="fieldText"></asp:Label>
    </asp:Panel>
    <br />

    <asp:Panel ID="Panel1" CssClass="pht_np_sub_pgHeader" runat="server">
        <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, QuestionnaireSchedule%>"></asp:Label>
    </asp:Panel>

    <p>
        <asp:Label runat="server" ID="SurveyInstructionLabel"></asp:Label>
    </p><br>

    <asp:Panel ID="pnlTimezoneWarning" runat="server" Visible="false" CssClass="phtUserNoticeWarning">
        <%= Resources.Resource.newLocationAlert%> &nbsp;&nbsp;<a href="settings.aspx"><%= Resources.Resource.changeTZ %></a></asp:Panel>
    <%--invisible button for updating questionnaire list --%>
    <asp:Button ID="ButtonUpdate" runat="server" OnClick="ButtonUpdate_Click" style="display:none" />
    <asp:UpdatePanel runat="server" ID="QuestionnaireUpdatePanel" UpdateMode="Conditional">
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="buttonUpdate" EventName="Click" />
    </Triggers>
    <ContentTemplate>
    <!-- Required questionnaire list -->
    <asp:Repeater ID="requiredQuestionnaireList" OnItemCommand="RequiredQuestionnaireList_ItemCommand" OnItemDataBound="RequiredQuestionnaireList_ItemDataBound" runat="server">
        <HeaderTemplate>
            <table class="pht_np_pageSection" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="pht_np_calloutHead" colspan="3"><asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, ToDo%>" /></td>
                </tr>
                <tr>
                    <td class="pht_np_calloutSubhead"><%= Resources.Resource.QName %></td>
                    <td class="pht_np_calloutSubhead"><%= Resources.Resource.QAvailable %></td>
                    <td class="pht_np_calloutSubhead"><%= Resources.Resource.QDue %></td>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr class="odd">
                <td class="pht_np_calloutToDo"><asp:LinkButton ID="LinkButton1" runat="server" 
                                    CommandName="launch" CommandArgument='<%# DataBinder.Eval(Container,"DataItem.SurveyID") %>' 
                                    Text='<%# DataBinder.Eval(Container,"DataItem.DisplayName") %>' Enabled='<%# DataBinder.Eval(Container,"DataItem.URLEnabled") %>'/></td>
                <td class="pht_np_calloutToDo"><asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Availability") %>' /></td>
                <td class="pht_np_calloutToDo"><asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Due") %>' /></td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>

    <!-- Event driven questionniare list -->
    <asp:Repeater ID="eventDrivenQuestionnaires" runat="server">
        <HeaderTemplate>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td class="pht_np_calloutHead" colspan="3"><asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, CurrentlyAvailableRecentSurveys%>" /></td>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td class="pht_np_calloutToDo"><asp:LinkButton ID="LinkButton1" runat="server" 
                                    CommandName="launch" CommandArgument='<%# DataBinder.Eval(Container,"DataItem.SurveyID") %>' 
                                    Text='<%# DataBinder.Eval(Container,"DataItem.DisplayName") %>' /></td>
                <td class="pht_np_calloutToDo"><asp:Label runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Availability") %>' /></td>
                <td class="pht_np_calloutToDo"><asp:Label runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Due") %>' /></td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    </ContentTemplate>        
    </asp:UpdatePanel>
        
    <!-- Questionnaire history list -->
    <asp:Repeater ID="rptHistoryList" runat="server">
        <HeaderTemplate>
            <table cellpadding="0" cellspacing="0" border="0" class="display" id="HistoryList">
	        <thead>
		        <tr>
			        <th class="pht_np_calloutHead align" colspan="4"><%= Resources.Resource.RecentQuestionnaires %></th>
		        </tr>
                <tr>
                    <th class="pht_np_calloutSubhead" style="width:20px"></th>
                    <th class="pht_np_calloutSubhead align" style="width:150px"><%= Resources.Resource.QDate%></th>
                    <th style="width:0px;">Hidden</th>
                    <th class="pht_np_calloutSubhead align"><%= Resources.Resource.QName%></th>                    
                </tr>
	        </thead>
	        <tbody>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td class="pht_np_calloutToDo"><img src="img/interface/check-mark-th.png" width="10" alt="Done" /></td>
                <td class="pht_np_calloutToDo"><asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.DateReport") %>' /></td>
                <td><asp:Literal ID="litHiddenCol" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.HiddenCol") %>' /></td>
                <td class="pht_np_calloutToDo"><asp:Literal ID="litSurvey" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.SU") %>' /></td>                
            </tr>
        </ItemTemplate>
        <FooterTemplate>
        </tbody>
</table>
        </FooterTemplate>
    </asp:Repeater>



    <asp:Panel ID="DTPanel" CssClass="pht_np_sub_pgHeader" runat="server">
        <asp:Label ID="Label5" runat="server" Text="<%$ Resources:Resource, TTravel%>"></asp:Label>
        <asp:TextBox ID="txtTimeTravel" runat="server" Width="280px"></asp:TextBox>
        <asp:Button ID="DTApply" runat="server" Text="<%$ Resources:Resource, ApplyTTravel%>" onclick="DTApply_Click"/>
    </asp:Panel>

    <asp:Panel ID="pnlScreenShot" CssClass="pht_np_sub_pgHeader" runat="server">
         <asp:Label ID="lblScreenShot" runat="server" Text="Screenshot"></asp:Label>
        <asp:CheckBox ID="chkScreenShot" runat="server" 
             oncheckedchanged="chkScreenShot_CheckedChanged" AutoPostBack="true"></asp:CheckBox>
    </asp:Panel>

      <asp:Panel ID="pnlScreenShotLanguage" CssClass="pht_np_sub_pgHeader" runat="server" Visible="false">
        <asp:Label ID="lblScreenShotLanguage" runat="server" Text="Select Language"></asp:Label>
         <asp:DropDownList ID="ScreenShotLanguageList" runat="server" OnSelectedIndexChanged="ScreenShotLanguageList_IndexChanged" AutoPostBack="true"></asp:DropDownList>
    </asp:Panel>

       <asp:Panel ID="pnlScreenShotEmail" CssClass="pht_np_sub_pgHeader" runat="server" Visible="false">
        <asp:Label ID="lblScreenShotEmail" runat="server" Text="Select Email Type"></asp:Label>
        <asp:DropDownList ID="ScreenShotEmailList" runat="server" onchange="checkSelectedEmailType()"></asp:DropDownList>
        <asp:DropDownList ID="SurveyList" runat="server"></asp:DropDownList>
        <input type="button" id="btnShowEmail" value="Show Email" onclick="showEmail()"/>
        <asp:Button ID="btnSendEmail" Text="Send Email" runat="server" OnClick="SendEmail_Click"/>
        <asp:Label ID="lblEmailSuccess" runat="server" Text="Sent email successfully." Visible="false"></asp:Label>
        <asp:Label ID="lblEmailError" runat="server" Text="Sending email falied. Please try again." Visible="false" CssClass="errors"></asp:Label>               
    </asp:Panel>
</asp:Content>
