﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Description;
using System.Text;
using System.Net.Mail;
using WebDiary.Core.Email;
using WebDiary.Core.Helpers;
using WebDiary.Core.Repositories;
using WebDiary.Core.Entities;
using WebDiary.Core.Constants;
using System.Configuration;
using log4net;
using WebDiary.SWAPI;
using WebDiary.WDAPI;
using System.Globalization;
using WebDiary.StudyPortal.Helpers;

namespace WebDiary.StudyPortal
{
    public class EmailScheduler : IEmailScheduler
    {
        private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static void SendChangeEmail(Study study, Subject sub, WDConstants.EmailType type)
        {
            Message changeMessage = new Message();
            changeMessage.AddTo(sub.Email);
            changeMessage.SetFrom(ConfigurationManager.AppSettings["FROM_ADDRESS"]);
            changeMessage.SetSubject(Resources.Resource.EmailAcctChangeSubject);

            string sb = string.Empty;
            sb = EmailHelper.GetChangeEmail(study, sub, type);
            if (type == WDConstants.EmailType.EmailUpdate || type == WDConstants.EmailType.EmailLanguageUpdate)
            {
                if (!string.IsNullOrEmpty(sub.PreviousEmail))
                    changeMessage.AddTo(sub.PreviousEmail);
            }
            changeMessage.SetBody(sb);
            try
            {
                changeMessage.Send();
                logger.Info(String.Format(CultureInfo.InvariantCulture, "SUCCESS: subject GUID: {0}, subject krpt: {1}, StudyWorks timeZone: {2}, study name: {3}, study ID in NPDB: {4}, email info: {5}, logging time stamp in UTC: {6}. Email was sent to the email server successfully.", sub.Id.ToString(), sub.KrPT, sub.TimeZone, study.Name, study.Id, "changes to subject account", DateTime.UtcNow));
            }
            catch (SmtpException ex)
            {
                logger.Error(String.Format(CultureInfo.InvariantCulture, "FAIL: subject GUID: {0}, subject krpt: {1}, StudyWorks timeZone: {2}, study name: {3}, study ID in NPDB: {4}, email info: {5}, logging time stamp in UTC: {6}, error message: {7}. Sending email failed in StudyPortal:SendChangeEmail.", sub.Id.ToString(), sub.KrPT, sub.TimeZone, study.Name, study.Id, "changes to subject account", DateTime.UtcNow, ex.Message), ex);
                throw;
            }
        }

        public static void SendActivationEmail(Study study, Subject sub)
        {
            Message activationMessage = new Message();
            activationMessage.AddTo(sub.Email);
            activationMessage.SetFrom(ConfigurationManager.AppSettings["FROM_ADDRESS"]);
            activationMessage.SetSubject(Resources.Resource.EmailAcctActivationSubject);

            string sb = string.Empty;
            sb = EmailHelper.GetActivationEmail(study, sub);

            activationMessage.SetBody(sb);
            try
            {
                activationMessage.Send();
                logger.Info(String.Format(CultureInfo.InvariantCulture, "SUCCESS: subject GUID: {0}, subject krpt: {1}, StudyWorks timeZone: {2}, study name: {3}, study ID in NPDB: {4}, email info: {5}, logging time stamp in UTC: {6}. Email was sent to the email server successfully.", sub.Id.ToString(), sub.KrPT, sub.TimeZone, study.Name, study.Id, "activation", DateTime.UtcNow));
            }
            catch (SmtpException ex)
            {
                logger.Error(String.Format(CultureInfo.InvariantCulture, "FAIL: subject GUID: {0}, subject krpt: {1}, StudyWorks timeZone: {2}, study name: {3}, study ID in NPDB: {4}, email info: {5}, logging time stamp in UTC: {6}, error message: {7}. Sending email failed in StudyPortal:SendActivationEmail.", sub.Id.ToString(), sub.KrPT, sub.TimeZone, study.Name, study.Id, "activation", DateTime.UtcNow, ex.Message), ex);
                throw;
            }
        }

        public static void SendConfirmationEmail(Study study, Subject sub)
        {
            string body = EmailHelper.GetConfirmationEmail(study, sub);

            Message message = new Message(ConfigurationManager.AppSettings["FROM_ADDRESS"], sub.Email, Resources.Resource.EmailAcctActivConfirmationSubject, body);
            try
            {
                message.Send();
                logger.Info(String.Format(CultureInfo.InvariantCulture, "SUCCESS: subject GUID: {0}, subject krpt: {1}, StudyWorks timeZone: {2}, study name: {3}, study ID in NPDB: {4}, email info: {5}, logging time stamp in UTC: {6}. Email was sent to the email server successfully.", sub.Id.ToString(), sub.KrPT, sub.TimeZone, study.Name, study.Id, "confirmation of activated account", DateTime.UtcNow));
            }
            catch (SmtpException ex)
            {
                logger.Error(String.Format(CultureInfo.InvariantCulture, "FAIL: subject GUID: {0}, subject krpt: {1}, StudyWorks timeZone: {2}, study name: {3}, study ID in NPDB: {4}, email info: {5}, logging time stamp in UTC: {6}, error message: {7}. Sending email failed in StudyPortal:SendConfirmationEmail.", sub.Id.ToString(), sub.KrPT, sub.TimeZone, study.Name, study.Id, "confirmation of activated account", DateTime.UtcNow, ex.Message), ex);
                throw;
            }
        }

        public static void SendResetPasswordEmail(Study study, Subject sub)
        {
            string sb = EmailHelper.GetResetPasswordEmail(study, sub);

            string emailSubject = Resources.Resource.EmailAcctForgotResetSubject;
            Message message = new Message();
            message.AddTo(sub.Email);
            message.SetFrom(ConfigurationManager.AppSettings["FROM_ADDRESS"]);
            message.SetSubject(emailSubject);
            message.SetBody(sb);
            try
            {
                message.Send();
                logger.Info(String.Format(CultureInfo.InvariantCulture, "SUCCESS: subject GUID: {0}, subject krpt: {1}, StudyWorks timeZone: {2}, study name: {3}, study ID in NPDB: {4}, email info: {5}, logging time stamp in UTC: {6}. Email was sent to the email server successfully.", sub.Id.ToString(), sub.KrPT, sub.TimeZone, study.Name, study.Id, "confirmation of successful password reset", DateTime.UtcNow));
            }
            catch (SmtpException ex)
            {
                logger.Error(String.Format(CultureInfo.InvariantCulture, "FAIL: subject GUID: {0}, subject krpt: {1}, StudyWorks timeZone: {2}, study name: {3}, study ID in NPDB: {4}, email info: {5}, logging time stamp in UTC: {6}, error message: {7}. Sending email failed in StudyPortal:SendResetPasswordEmail.", sub.Id.ToString(), sub.KrPT, sub.TimeZone, study.Name, study.Id, "confirmation of successful password reset", DateTime.UtcNow, ex.Message), ex);
                throw;
            }
        }

        public void SendEmail(int surveyId, Guid subjectId)
        {
            try
            {
                using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                {
                    if (logger.IsDebugEnabled)
                    {
                        //Get client ip and port.
                        ServiceClientInfo clientInfo = ServiceDebuggingTools.GetServiceClientInfo(OperationContext.Current);
                        logger.Info(clientInfo.ToString());
                    }

                    SubjectRepository subjectRep = new SubjectRepository(db);
                    Subject sub = subjectRep.FindById(subjectId);

                    //Set culture
                    string language = WDAPIObject.JavaLocaleToDotNetCulture(sub.Language.Trim());//if language in subject table contains padding, trim it.
                    CultureInfo culture = new CultureInfo(language);
                    System.Threading.Thread.CurrentThread.CurrentCulture = culture;
                    System.Threading.Thread.CurrentThread.CurrentUICulture = culture;

                    StudyRepository studyRep = new StudyRepository(db);
                    Study study = studyRep.FindById(sub.StudyId);
                    SurveyEmailScheduleRepository scheduleRep = new SurveyEmailScheduleRepository(db);
                    SurveyEmailSchedule schedule = scheduleRep.FindBySurveyIdAndSubjectId(surveyId, subjectId);

                    if (surveyId < 0)
                    {
                        WDConstants.EmailType type = (WDConstants.EmailType)surveyId;
                        switch (type)
                        {
                            case WDConstants.EmailType.LanguageUpdate:
                            case WDConstants.EmailType.EmailUpdate:
                            case WDConstants.EmailType.EmailLanguageUpdate:
                                SendChangeEmail(study, sub, type);
                                if (!sub.IsApproved)
                                    SendActivationEmail(study, sub);
                                break;
                            case WDConstants.EmailType.Activation:
                                SendActivationEmail(study, sub);
                                break;
                            case WDConstants.EmailType.Confirmation:
                                SendConfirmationEmail(study, sub);
                                break;
                            case WDConstants.EmailType.ResetPassword:
                                SendResetPasswordEmail(study, sub);
                                break;
                        }
                        db.SurveyEmailSchedule.DeleteOnSubmit(schedule);
                        db.SubmitChanges();
                    }
                    else
                    {
                        //Get webserver system time in UTC
                        DateTime webServerSystemTime = DateTime.UtcNow;

                        SurveyRepository surveyRep = new SurveyRepository(db);
                        StudySurveys survey = surveyRep.FindById(surveyId);
                        ScheduleConf config = SurveyScheduler.GetSchedule(survey, sub, study);
                        if (config.Enabled)
                        {
                            string eSubject = string.Format(CultureInfo.CurrentCulture, Resources.Resource.EmailScheduleSubject, study.Name);
                            string sb = EmailHelper.GetScheduledEmail(study, survey);

                            Message message = new Message(ConfigurationManager.AppSettings["FROM_ADDRESS"], sub.Email, eSubject, sb);
                            string emailInfo = "email for " + survey.DisplayName;

                            try
                            {
                                message.Send();
                                logger.Info(String.Format(CultureInfo.InvariantCulture, "SUCCESS: subject GUID: {0}, surveyId: {1}, subject krpt: {2}, StudyWorks timeZone: {3}, study name: {4}, study ID in NPDB: {5}, email info: {6}, scheduled date time in UTC: {7}, logging time stamp in UTC: {8}. Email was sent to the email server successfully.", sub.Id.ToString(), surveyId.ToString(), sub.KrPT, sub.TimeZone, study.Name, study.Id, emailInfo, schedule.ScheduleDT, DateTime.UtcNow));
                            }
                            catch (SmtpException ex)
                            {
                                logger.Error(String.Format(CultureInfo.InvariantCulture, "FAIL: subject GUID: {0}, surveyId: {1}, subject krpt: {2}, StudyWorks timeZone: {3}, study name: {4}, study ID in NPDB: {5}, email info: {6}, scheduled date time in UTC: {7}, logging time stamp in UTC: {8}, error message: {9}. Sending email failed in StudyPortal:SendEmail.", sub.Id.ToString(), surveyId.ToString(), sub.KrPT, sub.TimeZone, study.Name, study.Id, emailInfo, schedule.ScheduleDT, DateTime.UtcNow, ex.Message), ex);
                                throw;
                            }
                            db.SurveyEmailSchedule.DeleteOnSubmit(schedule);
                            db.SubmitChanges();
                            SurveyScheduler.ScheduleSurveyEmail(survey, sub, study, SurveyScheduler.ScheduleState.EmailSent);
                        }
                        else if (SurveyScheduler.IsNotificationPassedDue(survey, sub, webServerSystemTime))
                        {
                            db.SurveyEmailSchedule.DeleteOnSubmit(schedule);
                            db.SubmitChanges();
                            SurveyScheduler.ScheduleSurveyEmail(survey, sub, study, SurveyScheduler.ScheduleState.ScheduleNextEmail);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(String.Format(CultureInfo.InvariantCulture, "ERROR: subject GUID: {0}, surveyId: {1}, logging time stamp in UTC: {2}, error message: {3}. Error occured in StudyPortal:SendEmail.", subjectId.ToString(), surveyId.ToString(), DateTime.UtcNow, ex.Message), ex);
                throw; //must rethrow so that error is notified to WebDiary.Scheduler service
            }
        }
    }

    public class EmailSchedulerFactory : ServiceHostFactoryBase
    {
        private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public override ServiceHostBase CreateServiceHost(string constructorString, Uri[] baseAddresses)
        {
            ServiceHost serviceHost = new ServiceHost(typeof(EmailScheduler), baseAddresses);

            if (logger.IsDebugEnabled)
            {
                //Enable debugging
                logger.Info("Enabling email service debugger.");
                ServiceDebuggingTools.EnableServiceDebugging(serviceHost);
            }

            try
            {
                // create the noSSL endpoint         
                serviceHost.AddServiceEndpoint(typeof(IEmailScheduler), new BasicHttpBinding(), "");
            }
            catch
            {
                //There will be an exception if there is only https port and no http port but this is acceptable
            }
            try
            {
                // create SSL endpoint         
                serviceHost.AddServiceEndpoint(typeof(IEmailScheduler), new BasicHttpBinding(BasicHttpSecurityMode.Transport), "");
            }
            catch (Exception e)
            {
                logger.Error("HTTPS is not enabled on the website", e);
            }
            // Enable HttpGet for the service         
            ServiceMetadataBehavior metadata = new ServiceMetadataBehavior();
            metadata.HttpGetEnabled = true;
            serviceHost.Description.Behaviors.Add(metadata);
            return serviceHost;
        }
    }
}
