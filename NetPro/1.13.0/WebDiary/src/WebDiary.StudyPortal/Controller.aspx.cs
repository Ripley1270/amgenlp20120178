﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebDiary.Core.Errors;
using WebDiary.Core.Entities;
using WebDiary.Core.Helpers;
using WebDiary.Core.Repositories;
using WebDiary.SWIS;
using WebDiary.SWAPI;
using Newtonsoft.Json.Linq;
using System.Data.SqlClient;

public partial class Controller : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DateTimeOffset clientRequestTime;
        DateTime serverReceiptTime;
        JSONRPCParser rpc = null;
        try
        {
            // Parse method, params, and id out of the input stream
            rpc = new JSONRPCParser(Request.InputStream);

            // Load the database
            WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString);
            QueueManager queueManager = new QueueManager(db);            

            if (log4net.GlobalContext.Properties["study_id"] == null)
            {
                log4net.GlobalContext.Properties["study_id"] = StudyData.study.Id.ToString(CultureInfo.InvariantCulture);
            }

            // Initialize the response queue
            List<ResponseQueueItem> responseQueue = new List<ResponseQueueItem>();

            // Record transmission times
            serverReceiptTime = DateTime.UtcNow;
            try
            {
                clientRequestTime = DateTimeOffset.ParseExact(Request.Headers["Clientdate"], SWAPIConstants.LogPadAppLocalDateTimeFormat, CultureInfo.InvariantCulture);                                    
            }
            catch (FormatException)
            {
                throw new NetProException("Improperly formatted client date header");
            }
            

            // Attempt to authenticate subject
            Subject subject = null;
            string authorizationHeader = Request.Headers["Authorization"];
            long? deviceId = null;
            if (!string.IsNullOrWhiteSpace(authorizationHeader))
            {
                string[] authSplit = authorizationHeader.Split(':');
                if (authSplit.Length > 2)
                {
                    subject = queueManager.FindSubjectByUserId(authSplit[0]);
                    if (subject != null)
                    {
                        // Validate subject password and status - null the subject if this fails as if it was never found
                        if (subject.Password != authSplit[1].Trim() || subject.IsLockedOut)
                            subject = null;
                        else
                        {
                            SubjectDeviceRepository deviceRepo = new SubjectDeviceRepository(db);
                            SubjectDevice currentDevice = deviceRepo.FindLatestBySubjectId(subject.Id);                            
                            // Check to see if the device is current device or not.
                            // If it is not current device, it is deactivated device.
                            if (currentDevice == null || currentDevice.DeviceCode.ToString() != authSplit[2].Trim()) // not active device
                            {
                                Response.Headers.Add("X-Device-Status", SWISDeviceStatus.NotActive);

                                // Check if the method(action) is allowed from deactivated device
                                if (AllowedMethod(rpc.Method))
                                {
                                    // Check deviceCode in Authorization header
                                    string deviceCode = authSplit[2].Trim();
                                    if (string.IsNullOrEmpty(deviceCode))
                                        throw new NetProException("Device_Id is not found in HTTP Authorization header");
                                    else
                                    {
                                        // Check to see if the device is in the list
                                        SubjectDevice replacedDevice = deviceRepo.FindBySubjectIdDeviceCode(subject.Id, new Guid(deviceCode));
                                        if (replacedDevice == null)
                                            throw new NetProException(SWISStatusCodes.UserNotFound);
                                        else
                                            deviceId = replacedDevice.SubjectDeviceId;
                                    }
                                }
                                else
                                    throw new NetProException(SWISStatusCodes.ActionNotAllowed);
                            }
                            else
                            {
                                deviceId = currentDevice.SubjectDeviceId;
                                Response.Headers.Add("X-Device-Status", SWISDeviceStatus.Active);
                            }
                        }
                    }
                }
            }            

            // Add the newly found method to the ActionQueue
            List<ActionQueueItem> newActions = new List<ActionQueueItem>();
            var actionRepo = new ActionQueueRepository(db);

            if (rpc.Method.ToUpperInvariant() == "BATCH")
            {
                if (subject == null)
                    throw new NetProException("Unauthenticated user - batch disallowed");
                var batchCalls = rpc.ParseBatch();
                foreach (var batchCall in batchCalls)
                {
                    ActionQueueItem existingAction = actionRepo.FindByRpcIdAndDeviceId(subject.Id, Convert.ToInt32(batchCall.ID, CultureInfo.InvariantCulture), deviceId);
                    if (existingAction == null)
                        newActions.Add(queueManager.AddActionToQueue(batchCall, clientRequestTime.UtcDateTime, serverReceiptTime, subject, deviceId));
                    else if (existingAction.ActionState > ActionQueueItem.ActionStateNew)
                    {
                        ResponseQueueRepository responseRepo = new ResponseQueueRepository(db);
                        ResponseQueueItem existingResponse = responseRepo.FindByRpcIdAndDeviceId(subject.Id, Convert.ToInt32(batchCall.ID, CultureInfo.InvariantCulture), deviceId);
                        if (existingResponse != null)
                            responseQueue.Add(existingResponse);
                        else
                            newActions.Add(existingAction);
                    }
                }
            }
            else
            {
                bool skipAction = false;
                if (subject != null)
                {
                    ActionQueueItem existingAction = actionRepo.FindByRpcIdAndDeviceId(subject.Id, Convert.ToInt32(rpc.ID, CultureInfo.InvariantCulture), deviceId);
                    if (existingAction != null)
                    {
                        skipAction = true;
                        if (existingAction.ActionState > ActionQueueItem.ActionStateNew)
                        {
                            ResponseQueueRepository responseRepo = new ResponseQueueRepository(db);
                            ResponseQueueItem existingResponse = responseRepo.FindByRpcIdAndDeviceId(subject.Id, Convert.ToInt32(rpc.ID, CultureInfo.InvariantCulture), deviceId);
                            if (existingResponse != null)
                                responseQueue.Add(existingResponse);
                            else
                                newActions.Add(existingAction);
                        }
                    }
                }
                if (!skipAction)
                    newActions.Add(queueManager.AddActionToQueue(rpc, clientRequestTime.UtcDateTime, serverReceiptTime, subject, deviceId));
            }

            // If we know the subject, load the subject's ActionQueue, otherwise, use the queue based on the actions we know of
            IEnumerable<ActionQueueItem> actionQueue = null;
            if (subject == null)
                actionQueue = newActions.ToList();
            else
            {
                actionQueue = actionRepo.FindBySubjectAndStateInOrder(subject.Id, ActionQueueItem.ActionStateNew).ToList();
            }

            // Load the types out of the assembly containing the method class wrapper interface
            Type[] assemblyTypes = typeof(IRPCMethodWrapper).Assembly.GetTypes();

            int queueIndex = 0;
            string isBatchMode = actionQueue.Count() > 1 ? "BatchMode" : "";
            while (queueIndex < actionQueue.Count())
            {
                ActionQueueItem actionItem = actionQueue.ElementAt(queueIndex);
                string result = "";
                bool typeFound = false;
                bool queueRefreshed = false;
                bool removeOnError = true;

                try
                {
                    // Iterate over the types looking for types that implement the interface
                    foreach (Type type in assemblyTypes)
                    {
                        if (type.GetInterfaces().Contains(typeof(IRPCMethodWrapper)))
                        {
                            // Check the attributes on the type to see if it contains the RPCMethodAttribute
                            object[] atts = type.GetCustomAttributes(false);
                            foreach (var att in atts)
                            {
                                RPCMethodAttribute rpcMethod = att as RPCMethodAttribute;
                                if (rpcMethod != null)
                                {
                                    // Compare the method on the attribute to the method from the input stream
                                    if (rpcMethod.Method.ToUpperInvariant() == actionItem.Method.ToUpperInvariant())
                                    {
                                        // Mark RemoveOnError behavior externally
                                        removeOnError = rpcMethod.RemoveOnError;

                                        // Throw an Exception if the method invocation requires authentication, but authentication has failed
                                        if (rpcMethod.RequireAuthentication && subject == null)
                                        {
                                            throw new NetProException("Procedure call requires authentication");
                                        }

                                        // Construct the object from the matching type and invoke the method
                                        IRPCMethodWrapper wrapper = (IRPCMethodWrapper)type.GetConstructors()[0].Invoke(new object[] { });
                                        result = wrapper.RPCMethodCall(actionItem.Data, subject != null ? subject.Id.ToString() : null, Request.Headers["Clientdate"], actionItem.ServerReceiptTime.ToString(), isBatchMode, deviceId.ToString());

                                        // If the authentication information was included in the request, parse it out and update the ActionQueueItem accordingly
                                        if (subject == null && rpcMethod.ContainsAuthentication)
                                        {
                                            BaseUserDataContract userDataContract = DataContractUtil.CreateDataContract<BaseUserDataContract>(actionItem.Data);
                                            subject = queueManager.FindSubjectByUserId(userDataContract.UserId);
                                            actionItem.SubjectId = subject.Id;
                                        }
                                        else if (subject != null)
                                        {
                                            // If the subject is previously authenticated, its data could have been changed by the latest method. Refresh the local object.
                                            db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, subject);
                                        }

                                        // Update the ActionState appropriately
                                        if (rpcMethod.NoHandshake)
                                            actionItem.ActionState = ActionQueueItem.ActionStateConfirmed;
                                        else
                                            actionItem.ActionState = ActionQueueItem.ActionStateCompleted;

                                        // If the method calls for it, reload the ActionQueue
                                        if (rpcMethod.RefreshQueue)
                                        {
                                            actionQueue = null;
                                            actionQueue = actionRepo.FindBySubjectAndStateInOrder(subject.Id, ActionQueueItem.ActionStateNew);
                                            queueIndex = 0;
                                            queueRefreshed = true;
                                        }

                                        // Break out of the outer and inner loop once a match is found
                                        typeFound = true;
                                        break;
                                    }
                                }
                            }
                            if (typeFound)
                                break;
                        }
                    }

                    // Populate a response queue object
                    JObject rpcResponse = new JObject(
                        new JProperty("Error", ""),
                        new JProperty("ID", actionItem.RpcId),
                        new JProperty("Result", JSONRPCParser.ParseResult(result))
                    );
                    ResponseQueueItem responseItem = new ResponseQueueItem();
                    responseItem.Message = rpcResponse.ToString();
                    responseItem.QueueTimestamp = DateTime.Now;
                    responseItem.RpcId = actionItem.RpcId;
                    responseItem.DeviceId = deviceId;

                    // Save ResponseQueue items unless the subject is null or the ActionQueue item state is confirmed
                    if (subject != null && actionItem.ActionState != ActionQueueItem.ActionStateConfirmed)
                    {
                        responseItem.SubjectId = subject.Id;
                        db.ResponseQueue.InsertOnSubmit(responseItem);
                    }
                    responseQueue.Add(responseItem);

                    // If the subject was never identified at the completion of processing this action, then it would never be retrievable and should be deleted from the database
                    if (subject == null)
                        db.ActionQueue.DeleteOnSubmit(actionItem);
                }
                catch (Exception ex)
                {
                    // Clear out any pending database changes that could be in error
                    var dbChanges = db.GetChangeSet();
                    foreach (var insertChange in dbChanges.Inserts)
                    {
                        db.GetTable(insertChange.GetType()).DeleteOnSubmit(insertChange);
                    }
                    foreach (var deleteChange in dbChanges.Deletes)
                    {
                        db.GetTable(deleteChange.GetType()).InsertOnSubmit(deleteChange);
                    }
                    IList<System.Data.Linq.ITable> updatedTables = new List<System.Data.Linq.ITable>();
                    foreach (var updateChange in dbChanges.Updates)
                    {
                        var changedTable = db.GetTable(updateChange.GetType());
                        if (updatedTables.Contains(changedTable))
                            continue;
                        else
                            updatedTables.Add(changedTable);
                        db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, updateChange);
                    }

                    // Populate a result object for the error
                    RPCResponseDataContract rpcResponse = new RPCResponseDataContract();
                    rpcResponse.Id = actionItem.RpcId.ToString(CultureInfo.InvariantCulture);
                    rpcResponse.Result = "";
                    // Include the exception message as the error
                    if (ex is System.Runtime.Serialization.SerializationException)
                        rpcResponse.Error = SWISStatusCodes.JSONParseError;
                    else if (ex is SqlException || ex is System.Net.WebException)
                        rpcResponse.Error = SWISStatusCodes.ServerError;
                    else
                        rpcResponse.Error = ex.Message;
                    ResponseQueueItem responseItem = new ResponseQueueItem();
                    responseItem.Message = DataContractUtil.SerializeDataContract<RPCResponseDataContract>(rpcResponse);
                    responseItem.QueueTimestamp = DateTime.Now;
                    responseItem.RpcId = actionItem.RpcId;
                    responseItem.DeviceId = deviceId;
                    responseQueue.Add(responseItem);

                    // If the subject was never identified at the completion of processing this action, then it would never be retrievable and should be deleted from the database
                    // If the method is flagged for RemoveOnError, always delete it from the database for an error
                    if (subject == null || removeOnError)
                        db.ActionQueue.DeleteOnSubmit(actionItem);
                }

                // Submit changes to action queue and response queue records to the database
                db.SubmitChanges();
                if (!queueRefreshed)
                    queueIndex++;
            }

            // Delete any confirmed actions from the action queue
            if (subject != null)
            {
                var confirmedActions = (from a in db.ActionQueue where a.ActionState == ActionQueueItem.ActionStateConfirmed && a.SubjectId == subject.Id select a).AsEnumerable();
                db.ActionQueue.DeleteAllOnSubmit(confirmedActions);
                db.SubmitChanges();

                // Load server initiated actions that haven't been confirmed and add them to the ResponseQueue
                var serverActions = from r in db.ResponseQueue
                                    join a in db.ActionQueue on r.RpcId equals a.RpcId
                                    where r.SubjectId == subject.Id && a.ActionState == ActionQueueItem.ActionStateCompleted
                                          && a.RpcId > 1000000
                                    orderby r.RpcId
                                    select r;
                foreach (ResponseQueueItem rQueue in serverActions)
                {
                    if ((from r in responseQueue where r.ResponseQueueId == rQueue.ResponseQueueId select r).Count() == 0)
                        responseQueue.Add(rQueue);
                }
            }

            // Construct the response
            // If there's only one response item in the queue, construct a single item response
            if (responseQueue.Count() == 1)
            {
                Response.Write(responseQueue[0].Message);
            }
            // If there's more than one response item in the queue, construct a batch response
            else if (responseQueue.Count() > 1)
            {
                JObject batchResponse = new JObject(new JProperty("ID", 0), new JProperty("Error", ""));
                IList<JRaw> responseResultList = new List<JRaw>();
                foreach (ResponseQueueItem responseItem in responseQueue)
                {
                    responseResultList.Add(new JRaw(responseItem.Message));
                }
                batchResponse.Add(new JProperty("Result", responseResultList));
                Response.Write(batchResponse.ToString());
            }
            // If the queue was empty, send an error response
            else
                throw new NetProException("No Actions");
        }
        catch (SqlException sqlEx)
        {
            // Populate a result object for the error
            RPCResponseDataContract rpcResponse = new RPCResponseDataContract();
            // Include the id if we have it, if not, leave blank
            if (rpc != null)
                rpcResponse.Id = rpc.ID;
            else
            {
                rpc = new JSONRPCParser(Request.InputStream);
                rpcResponse.Id = rpc.ID;
            }
            rpcResponse.Result = "";
            // Error code: 5 (DB is down)
            rpcResponse.Error = SWISStatusCodes.ServerError;

            // Serialize as a response
            var serial = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(RPCResponseDataContract));
            serial.WriteObject(Response.OutputStream, rpcResponse);
        }
        catch (Exception ex)
        {
            // Populate a result object for the error
            RPCResponseDataContract rpcResponse = new RPCResponseDataContract();
            // Include the id if we have it, if not, leave blank
            if (rpc != null)
                rpcResponse.Id = rpc.ID;
            else
            {
                rpc = new JSONRPCParser(Request.InputStream);
                rpcResponse.Id = rpc.ID;
            }
            rpcResponse.Result = "";
            // Include the exception message as the error
            rpcResponse.Error = ex.Message;

            // Serialize as a response
            var serial = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(RPCResponseDataContract));
            serial.WriteObject(Response.OutputStream, rpcResponse);
        }
    }

    /// <summary>
    /// Check if the method(action) is allowed from deactivated device.
    /// diary, batch is allowed. batch method contains a collection of diary data. 
    /// </summary>
    /// <param name="method">Service request method(action) from client application such as diary, batch, confirm, passwordReset</param>
    /// <returns>True/False</returns>
    private bool AllowedMethod(string method)
    {
        string[] allowedMethod = new string[] { "Diary", "Confirm", "SubjectSyncData", "LogEntry", "Batch" };
        return Array.Exists(allowedMethod, element => element == method);
    }
}
