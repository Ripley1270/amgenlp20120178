﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebDiary.Controls;
using WebDiary.Core.Constants;
using WebDiary.Core.Entities;
using WebDiary.Core.Repositories;
using WebDiary.SWAPI;
using WebDiary.Core.Helpers;

namespace WebDiary.StudyPortal
{
    public partial class Survey : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //prevent back button cache
            Response.AddHeader("Cache-Control", "no-cache, max-age=0, must-revalidate, no-store");
            string returnUrl = null, appUrl = null;
            string loginPage = Session["isMobile"] != null ? "/mobile/timeout-redirect.aspx" : "/login.aspx";

            returnUrl = StudyData.study.WebDiaryStudyBaseUrl.TrimEnd('/') + loginPage;
            appUrl = StudyData.study.WebDiaryStudyBaseUrl.TrimEnd('/'); 

            if (string.IsNullOrEmpty(Request.QueryString["psd_id"]))
            {
                script.Text = @"<script type='text/javascript'>location.href = '" + returnUrl + "'</script>";
            }
            else
            {
                using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                {
                    SurveyDataRepository sd_rep = new SurveyDataRepository(db);
                    SurveyData surveyData = sd_rep.FindById(new Guid(Request.QueryString["psd_id"]));

                    if (surveyData == null)
                    {
                        if (SessionHelper.IsSiteSession())
                        {
                            if (Session[WDConstants.VarReturnUrl] != null)
                                script.Text = @"<script type='text/javascript'>location.href = '" + Session[WDConstants.VarReturnUrl] + "'</script>";
                            else
                                script.Text = @"<script type='text/javascript'>location.href = '" + StudyData.study.SsaBaseUrl.TrimEnd('/') + SWAPIConstants.SessionTimeoutPage + "'</script>";
                        }
                        else
                        {
                            script.Text = @"<script type='text/javascript'>location.href = '" + appUrl + "'</script>";
                        }
                        return;
                    }

                    SurveyRepository svr = new SurveyRepository(db);
                    StudySurveys survey = svr.FindById(surveyData.SurveyId);

                    //determine if the request is for patient access or site access
                    if (survey.FormType.Equals("Subject Submit")) //Patient gateway
                    {
                        if (Session["login"] == null)
                        {
                            script.Text = @"<script type='text/javascript'>location.href = '" + returnUrl + "'</script>";
                        }
                    }
                    else //site gateway
                    {
                        StudyRepository sr = new StudyRepository(db);
                        Study study = sr.FindById(surveyData.StudyId);
                        returnUrl = study.SsaBaseUrl.TrimEnd('/') + SWAPIConstants.SessionTimeoutPage;

                        if (!string.IsNullOrEmpty(Session[WDConstants.VarRequestUrl] as string))
                        {
                            if (!SessionHelper.CheckSwSession(Session[WDConstants.VarRequestUrl] as string))
                            {
                                script.Text = @"<script type='text/javascript'>location.href = '" + returnUrl + "'</script>";
                            }
                        }
                        else
                        {
                            script.Text = @"<script type='text/javascript'>location.href = '" + returnUrl + "'</script>";
                        }
                    }

                    // set session checked flag so that we don't need to check session in SurveySupport.aspx again
                    Session[surveyData.Id.ToString()] = true;
                }
            }            

            if (Session[WDConstants.IsExternal] != null)
            {
                //Initialize session timeout script
                script.Text += @"<script type='text/javascript'>
                                    var returnUrl = '" + returnUrl + @"';
                                    var sTimeout = " + Session.Timeout * 60000 + @";
                                    var timeOutHandle;
                                    var lastCheck = new Date().getTime();

                                    function sleepCheck () { var now = new Date().getTime(); var diff = now - lastCheck; if (diff > sTimeout) {top.location.href = returnUrl; } };

                                    function frameUsage() {
                                        clearInterval(timeOutHandle);
                                        timeOutSet();
                                    }

                                    function timeOutSet() {
                                        timeOutHandle = setInterval(function(){sleepCheck();}, 2000);
                                    }        
                                </script>";
                frame.Attributes.Add("onload", "frameUsage()");
            }
        }
    }
}
