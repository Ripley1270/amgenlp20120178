﻿<%@ Page Title="" Language="C#" MasterPageFile="~/includes/masters/Site.Master" AutoEventWireup="true" CodeBehind="site-access.aspx.cs" Inherits="WebDiary.StudyPortal.SiteAccess" %>
<asp:Content ContentPlaceHolderID="head" runat="server">
    <asp:Literal ID="notificationScriptID" EnableViewState="false" runat="server"></asp:Literal>
    <link rel="stylesheet" type="text/css" href="includes/css/anytime.css"/>
    <script type="text/javascript" src="includes/scripts/anytime.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            AnyTime.picker("txtTimeTravel",
                { format: "%W, %M %d, %z %h:%i:%s %p", firstDOW: 1 });

            $("#SendActivationEmailLabelFail").hide();
            $("#SendActivationEmailLabelSuccess").hide();
            $('#SendActivationEmailButton').click(function() {
                $.ajax({
                    type: "POST",
                    url: "site-access.aspx/SendActivationEmail",
                    data: "{}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(msg) {
                        if (msg.d) {
                            $("#SendActivationEmailLabelFail").hide();
                            $("#SendActivationEmailLabelSuccess").show();
                        } else {
                            $("#SendActivationEmailLabelFail").show();
                            $("#SendActivationEmailLabelSuccess").hide();
                        }
                        $('#ActivationEmailTextDiv').show();
                        $('#ActivationEmailTextDiv').delay('2000').fadeOut('slow', function() {});
                    }
                })
            })
        });
    </script>
    <script language="javascript" type="text/javascript">
        function
            jsPhtSetConditionalDisplayButton(whichButton) { /* Supports any number of buttons and governing checkboxes */
                switch (whichButton) {
                case 'patientInfo':
                    bPatientInfoButton = document.getElementById('editSubjectBtn');
                    if ($('input[id="emailChk"]:checked').length > 0 || $('input[id="languageChk"]:checked').length > 0)
                        bPatientInfoButton.className = 'phtConditionalDisplayButton phtButtonOn';
                    else
                        bPatientInfoButton.className = 'phtConditionalDisplayButton phtButtonOff';
                    break;

                case 'terminateStudy':

                    cTerminate = document.getElementById('endStudyParticipationChk');

                    bTerminateButton = document.getElementById('endStudyParticipationBtn');

                    if (cTerminate.checked) bTerminateButton.className = 'phtConditionalDisplayButton phtButtonOn'
                    else bTerminateButton.className = 'phtConditionalDisplayButton phtButtonOff';

                    break;
                }
            }
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#togChk').click(function() {
                $('#qrcodeContainer').toggle();
            });
        });
    </script>
</asp:Content>
<asp:Content ContentPlaceHolderID="mainContent" runat="server">
<div id="page-container">
<div id="sidebar">
    <ul class="menu">
        <li>
            <asp:LinkButton ID="dataSummariesLnk" OnClick="DataSummariesLnk_Click" runat="server" Text="<%$ Resources:Resource, saDataSummariesLnk %>"></asp:LinkButton>
        </li>
        <li>
            <asp:LinkButton ID="chooseSubjectLnk" OnClick="ChooseSubjectLnk_Click" runat="server" Text="<%$ Resources:Resource, saChooseSubjectLnk %>"></asp:LinkButton>
        </li>
    </ul>
</div>
<div id="page-content">
<div id="unauthBrowser">
    <input type="hidden" id="authBrowser" value="" runat="server"/>
    <div id="unauthMsgArea">
        <asp:Label ID="lblUnauthorizedMessage" CssClass="unAuthorizedError" Visible="false" runat="server">
        </asp:Label>
    </div>
    <div id="authBrowserArea" runat="server">
    </div>
</div>

<!-- Change Language block -->
<%--<div class="phtFormEnclosure" id="changeLanguageBlock" runat="server">
                <asp:Label runat="server" ID="selectLanguageLbl" Text="<%$ Resources:Resource, SelectLanguage %>" /> 
                <asp:DropDownList runat="server" ID="selectLanguageDdl" />
            </div>--%>
<!-- Patient Info block -->
<div class="phtFormEnclosure" id="editSubjectBlock" runat="server">
    <div class="phtFormEnclosureHeader">
        <asp:Label ID="patientInfoLbl" runat="server" Text="<%$ Resources:Resource, saPatientInfoLabel %>"></asp:Label>
        <asp:Label ID="patientIDLbl" runat="server"></asp:Label>
    </div>
    <table id="patientInfoBlock" runat="server">
        <thead>
        <tr class="phtFormEnclosureBody">
            <th class="phtFormEnclosureBody"></th>
            <th class="phtFormEnclosureBody"></th>
            <th class="phtFormEnclosureBody phtFormControls">
                <asp:Label ID="modifyLabel" runat="server" Text="<%$ Resources:Resource, saModifyLabel %>"></asp:Label>
            </th>
        </tr>
        </thead>
        <tbody>
        <tr class="phtFormEnclosureBody" id="emailAddrRow" runat="server">
            <td class="phtFormEnclosureBody">
                <asp:Label ID="emailAddrLbl" runat="server" Text="<%$ Resources:Resource, saEmailAddrLabel %>"></asp:Label>
            </td>
            <td class="phtFormEnclosureBody">
                <asp:Label ID="emailAddrValueLbl" runat="server"></asp:Label>
            </td>
            <td class="phtFormEnclosureBody phtFormControls">
                <asp:CheckBox ID="emailChk" cssclass="phtCheckboxVerticalFix" clientidmode="Static" runat="server" onclick="jsPhtSetConditionalDisplayButton('patientInfo');"/>
            </td>
        </tr>
        <tr class="phtFormEnclosureBody">
            <td class="phtFormEnclosureBody">
                <asp:Label ID="preferredLangLbl" runat="server" Text="<%$ Resources:Resource, saPreferredLanguageLbl %>"></asp:Label>
            </td>
            <td class="phtFormEnclosureBody">
                <asp:Label ID="preferredLangValueLbl" runat="server"></asp:Label>
            </td>
            <td class="phtFormEnclosureBody phtFormControls">
                <asp:CheckBox id="languageChk" cssclass="phtCheckboxVerticalFix" clientidmode="Static" runat="server" onclick="jsPhtSetConditionalDisplayButton('patientInfo');"/>
            </td>
        </tr>
        </tbody>
    </table>
    <div class="phtFormEnclosureFooter">
        <asp:Button ID="editSubjectBtn" text="<%$ Resources:Resource, saEditSubjectBtn %>" onclick="EditSubjectBtn_Click" cssclass="phtConditionalDisplayButton phtButtonOff" clientidmode="Static" runat="server"/>
    </div>
    <table id="ActivationEmailTable" runat="server">
        <tr class="phtFormEnclosureBody">
            <td class="phtFormEnclosureBody">
                <asp:Button ID="SendActivationEmailButton" Text="<%$ Resources:Resource, saActivationEmailButton %>" runat="server" OnClientClick="return false;"/>
            </td>
            <td id="ActivationEmailTextDiv" class="phtFormEnclosureBody">
                <asp:Label ID="SendActivationEmailLabelSuccess" Text="<%$ Resources:Resource, saActivationEmailSuccess %>" runat="server"></asp:Label>
                <asp:Label ID="SendActivationEmailLabelFail" Text="<%$ Resources:Resource, saActivationEmailFail %>" CssClass="nb-errors" runat="server"></asp:Label>
            </td>
        </tr>
    </table>


</div>

<!-- LogPad App Info block -->
<asp:Panel ID="PanelLF" runat="server">
    <div class="phtFormEnclosureMargined" id="lfInfoHeader">
        <div class="phtFormEnclosureHeader">
            <asp:Label ID="leapFrogInfoHeaderLabel" runat="server" Text="<%$ Resources:Resource, saLFInfoLabel %>"></asp:Label>
        </div>
        <table id="lfInfoBlock" runat="server">
            <tr class="phtFormEnclosureBody">
                <td class="phtFormEnclosureBody">
                    <div class="studyPin">
                        <img src="img/lf/common/pin.png" alt=""/>
                    </div>
                </td>
                <td class="phtFormEnclosureBody">
                    <asp:Label ID="LFStudyName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr class="phtFormEnclosureBody">
                <td class="phtFormEnclosureBody">
                    <asp:Label ID="LFStudyUrlLabel" runat="server" Text="<%$ Resources:Resource, saLFUrlLabel %>"></asp:Label>
                </td>
                <td class="phtFormEnclosureBody">
                    <asp:Label ID="LFStudyUrlValueLabel" runat="server"></asp:Label>
                </td>
            </tr>
            <tr class="phtFormEnclosureBody">
                <td class="phtFormEnclosureBody">
                    <asp:Label ID="LFPinLabel" runat="server" Text="<%$ Resources:Resource, saLFPinLabel %>"></asp:Label>
                </td>
                <td class="phtFormEnclosureBody">
                    <asp:Label ID="LFPinValueLabel" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        <div id="setupInsContainerCoor" class="phtFormEnclosureBody">
            <asp:HyperLink ID="setupInsStudyCoordinator" runat="server" NavigateUrl="LPASetUpInstruction.aspx?instruction=studycoordinator" CssClass="linkDeco" Target="_blank" Text="<%$ Resources:Resource, LPASetupInsForStudyCoor%>"></asp:HyperLink>
        </div>
        <div id="setupInsContainerSub" class="phtFormEnclosureBody">
            <asp:HyperLink ID="setupInsSubject" runat="server" NavigateUrl="LPASetUpInstruction.aspx" CssClass="linkDeco" Target="_blank" Text="<%$ Resources:Resource, LPASetupInsForSubject%>"></asp:HyperLink>
        </div>
        <div id="togChkContainer" class="phtFormEnclosureBody">
            <table>
                <tr class="phtFormEnclosureBody">
                    <td class="phtFormEnclosureBody">
                        <asp:label ID="qrcodeLabel" runat="server" Text="<%$ Resources:Resource, qrCode %>"></asp:label>
                    </td>
                    <td class="phtFormEnclosureBody">
                        <input id="togChk" name="togChk" type="checkbox"/>
                    </td>
                </tr>
            </table>
        </div>
        <div id="qrcodeContainer" class="phtFormEnclosureBody hide">
            <div id="qrcode" runat="server"></div>
        </div>
    </div>
</asp:Panel>

<!-- Patient status block -->
<div class="phtFormEnclosureMargined">
    <div class="phtFormEnclosureHeader">
        <asp:Label ID="patientStatusLbl" runat="server" Text="<%$ Resources:Resource, saPatientStatusLbl %>"></asp:Label>
    </div>
    <asp:panel id="endStudyPnl" class="phtFormEnclosureBody" runat="server">
        <table>
            <tr class="phtFormEnclosureBody">
                <td class="phtFormEnclosureBody">
                    <asp:Label ID="endStudyParticipationLbl" runat="server" Text="<%$ Resources:Resource, saEndStudyParticipationLbl %>"></asp:Label>
                </td>
                <td class="phtFormEnclosureBody">
                    <asp:CheckBox ID="endStudyParticipationChk" cssclass="phtCheckboxVerticalFix" clientidmode="Static" runat="server" onclick="jsPhtSetConditionalDisplayButton('terminateStudy');"/>
                </td>
            </tr>
        </table>
    </asp:panel>
    <asp:Label runat="server" ID="ParticipationEndedLabel" Text="<%$ Resources:Resource, saParticipationEnded %>" Visible="false"></asp:Label>
    <div class="phtFormEnclosureFooter">
        <asp:Button ID="endStudyParticipationBtn" text="<%$ Resources:Resource, saEndStudyParticipationBtn %>" onclick="EndStudyParticipationBtn_Click" cssclass="phtConditionalDisplayButton phtButtonOff" clientidmode="Static" runat="server"/>
    </div>
</div>
<!-- Patient questionnaires block -->
<div class="phtFormEnclosureMargined">
    <div class="phtFormEnclosureHeader">
        <asp:Label ID="siteQuestionnairesLbl" runat="server" Text="<%$ Resources:Resource, saSiteQuestionnairesLbl %>"></asp:Label>
    </div>
    <asp:Label runat="server" ID="NoQuestionnairesLabel" Text="<%$ Resources:Resource, NoQuestionnaires %>" Visible="false"></asp:Label>
    <asp:DropDownList runat="server" name="DropDownList1" id="OtherRecordList" OnSelectedIndexChanged="Diary_IndexChanged" AutoPostBack="true"/>
    <asp:DropDownList runat="server" name="DropDownList3" id="MedIDList" AutoPostBack="true" Visible="false"/>
    <asp:GridView ID="questionnaireGridView" AutoGenerateColumns="False" CssClass="gwGrid" runat="server" OnRowCommand="QuestionnaireGridView_RowCommand" ShowHeader="false">
        <Columns>
            <asp:TemplateField HeaderText="<%$ Resources:Resource, saQuestionnairesHeaderText %>">
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="launch" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.ActivationURL") %>'
                                    Text='<%# DataBinder.Eval(Container, "DataItem.DisplayName") %>'/>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="<%$ Resources:Resource, saStatusHeaderText %>">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, saStatusLabel %>"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</div>
<asp:Label ID="selectionError" Text="<%$ Resources:PDEResource, SelectionError %>" Visible="false" runat="server" CssClass="nb-errors"/>
<!-- Site Language Selection -->
<!-- PDE Template - removed language dropdown
<div class="phtFormEnclosureMargined">
    <div class="phtFormEnclosureHeader">
        <asp:Label ID="siteLanguagesLabel" runat="server" Text="<%$ Resources:Resource, saSiteLanguagesLabel %>"></asp:Label>
    </div>
    <asp:DropDownList ID="SiteLanguagesList" runat="server" OnSelectedIndexChanged="SiteLanguages_IndexChanged" AutoPostBack="true">
    </asp:DropDownList>
</div>
-->

<asp:Panel ID="DTPanel" CssClass="pht_np_sub_pgHeader" runat="server">
    <div class="phtFormEnclosureMargined">
        <div class="phtFormEnclosureHeader">
            <asp:Label ID="Label5" runat="server" Text="<%$ Resources:Resource, TTravel%>"></asp:Label>
        </div>
        <div class="phtFormEnclosureBody">
            <asp:TextBox ID="txtTimeTravel" runat="server" Width="280px"></asp:TextBox>
            <asp:Button ID="DTApply" runat="server" Text="<%$ Resources:Resource, ApplyTTravel%>" onclick="DTApply_Click"/>
        </div>
    </div>
</asp:Panel>

<asp:Panel ID="pnlScreenShotContainer" runat="server">
    <div class="phtFormEnclosureMargined">
        <div class="phtFormEnclosureHeader">
            <asp:Label ID="lblScreenShotHeader" runat="server" Text="Screen Shot"></asp:Label>
        </div>
        <asp:panel id="pnlScreenShot" class="phtFormEnclosureBody" runat="server">
            <table>
                <tr class="phtFormEnclosureBody">
                    <td class="phtFormEnclosureBody">
                        <asp:Label ID="lblScreenShot" runat="server" Text="Screen Shot"></asp:Label>
                    </td>
                    <td class="phtFormEnclosureBody">
                        <asp:CheckBox ID="chkScreenShot" cssclass="phtCheckboxVerticalFix" clientidmode="Static" runat="server" oncheckedchanged="chkScreenShot_CheckedChanged" AutoPostBack="true"/>
                    </td>
                </tr>
            </table>
        </asp:panel>
    </div>
</asp:Panel>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        var authBrowser = $('#authBrowser').val();
        if (authBrowser == "false") {
            $("#page-content input").prop("disabled", true);
            $("#SiteLanguagesList").prop("disabled", true);
            $('#setupInsStudyCoordinator, #setupInsSubject').click(function(e) {
                e.preventDefault();
            });
        }
    });
</script>
</div>
</asp:Content>