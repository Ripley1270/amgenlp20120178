﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebDiary.Core.Constants;
using WebDiary.Core.Helpers;
using System.Globalization;

namespace WebDiary.StudyPortal
{
    public partial class survey_internal : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.Session[WDConstants.FrameLeaveUrl] != null)
            {
                redirectScript.Text = "<script type=\"text/javascript\"> top.location = \"" + HttpContext.Current.Session[WDConstants.FrameLeaveUrl] + "\"; </script>";
                HttpContext.Current.Session[WDConstants.FrameLeaveUrl] = null;
                HttpContext.Current.Session[WDConstants.FramePostUrl] = null;
                HttpContext.Current.Session[WDConstants.FrameRedirectUrl] = null;
            }
            else if (HttpContext.Current.Session[WDConstants.FramePostUrl] != null)
            {
                Response.Clear();
                Response.Write(HttpContext.Current.Session[WDConstants.FramePostUrl].ToString());
                Response.End();
            }
            else if (HttpContext.Current.Session[WDConstants.FrameRedirectUrl] != null)
            {
                string url = HttpContext.Current.Session[WDConstants.FrameRedirectUrl].ToString();

                // Check ScreenShotMode
                string screenShot = Session[WDConstants.VarScreenShot] as string ?? string.Empty;
                if (!string.IsNullOrEmpty(screenShot) && screenShot == "true")
                    url += "&ScreenShot=true";

                url += "&product=" + (string.IsNullOrEmpty(StudyData.version.LFHash)? "np" : "lf");
                
                if (StudyData.study.SubjectInitials.HasValue)
                    url += "&initial=" + StudyData.study.SubjectInitials.Value.ToString(CultureInfo.InvariantCulture);

                Response.Redirect(url, true);
            }
            else
            {
                redirectScript.Text = "<script type=\"text/javascript\">top.location.href = top.location.href.replace('survey.aspx', '');</script>";
            }
            
        }
    }
}