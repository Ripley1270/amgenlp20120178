﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Configuration;

using WebDiary.Core.Constants;
using WebDiary.Core.Entities;
using WebDiary.Core.Helpers;
using System.Text;

using log4net;
using WebDiary.Core.Repositories;
using WebDiary.StudyPortal.Includes.Usercontrols;
using System.Globalization;
using WebDiary.StudyPortal.Helpers;


namespace WebDiary.StudyPortal.Includes.Masters
{
    public partial class Site : Controls.BaseMasterPage
    {
        private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        protected string BasePath
        {
            get
            {
                return String.Concat(HttpHelper.GetBaseUrlWithScheme(), BaseUrl);
            }
        }        

        protected void Page_Load(object sender, EventArgs e)
        {            
            // detect mobile browser
            if (Page.GetType().Name != "submit_aspx" && Page.GetType().Name != "site_access_aspx" && Page.GetType().Name != "wd_error_aspx" && Page.GetType().Name != "instruction_aspx")
            {
                string strUserAgent = Request.UserAgent.ToLower();
                if (strUserAgent != null)
                {
                    if (Request.Browser.IsMobileDevice == true ||
                        strUserAgent.Contains("android") ||
                        strUserAgent.Contains("iphone") ||
                        strUserAgent.Contains("ipad") ||
                        strUserAgent.Contains("ipod") ||
                        strUserAgent.Contains("blackberry") ||
                        strUserAgent.Contains("mobile") ||
                        strUserAgent.Contains("windows ce") ||
                        strUserAgent.Contains("opera mini") ||
                        strUserAgent.Contains("webos") ||
                        strUserAgent.Contains("palm"))
                    {
                        if (string.IsNullOrEmpty(Request.QueryString["token"]))
                        {
                            if (!string.IsNullOrEmpty(Request.QueryString["surveyref"]))
                            {
                                Response.Redirect("~/mobile/?surveyref=" + Request.QueryString["surveyref"]);
                            }
                            else
                            {                                
                                Response.Redirect("~/mobile/");
                            }
                        }
                        else
                        {
                            if (Page.GetType().Name == "activate_aspx")
                                Response.Redirect("~/mobile/activate.aspx?token=" + Request.QueryString["token"]);
                            if (Page.GetType().Name == "reset_password_aspx")
                                Response.Redirect("~/mobile/reset-password.aspx?token=" + Request.QueryString["token"]);
                        }
                    }
                }
            }

            string returnUrl = "";
            if (SessionHelper.IsSiteSession())
            {
                using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                {
                    returnUrl = StudyData.study.SsaBaseUrl + SWAPI.SWAPIConstants.SessionTimeoutPage;
                }
            }
            else
            {
                    returnUrl = StudyData.study.WebDiaryStudyBaseUrl.TrimEnd('/') + "/login.aspx";
            }
            if (!IsPostBack)
            {
                //Initialize session timeout script
                litScript.Text = "<script type=\"text/javascript\">var sTimeout = " + Session.Timeout * 60000 + "; var autoLock = true; var returnUrl = '" + returnUrl + "';</script>";
            }
            
            if (Context.User != null)
            {
                WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString);
                
                lblUserID.Text = Resources.Resource.PageHeaderSignedInAs + (Session["userName"] != null ? Session["userName"].ToString() : "");
                if (StudyData.version.ProductEnvironment == Core.Helpers.Version.EnvironmentStaging
                    || StudyData.version.ProductEnvironment == Core.Helpers.Version.EnvironmentDeveloper)
                {
                    string coreVersion = OriginalCoreVersionHelper.OriginalCoreVersionFullBuild(StudyData.study.Id);
                    lblNetProVer.InnerText = Resources.Resource.SMNetPROVer + StudyData.version.ProductVersionFullBuild;
                    if (!string.IsNullOrEmpty(coreVersion))
                        lblCoreVer.InnerText = Resources.Resource.SMCoreVer + coreVersion.ToString(CultureInfo.InvariantCulture);
                    lblStudyVer.InnerText = Resources.Resource.SMStudyVer + StudyData.version.StudyVersionFullBuild;
                }
                else
                {
                    string coreVersion = OriginalCoreVersionHelper.OriginalCoreVersionShort(StudyData.study.Id);
                    lblNetProVer.InnerText = Resources.Resource.SMNetPROVer + StudyData.version.ProductVersionShort;
                    if (!string.IsNullOrEmpty(coreVersion))
                        lblCoreVer.InnerText = Resources.Resource.SMCoreVer + coreVersion.ToString(CultureInfo.InvariantCulture);
                    liStudyVer.Visible = false;
                }
                lnkContactUs.InnerText = Resources.Resource.FooterContactUs;

                string authorizedBrowser = AuthorizedBrowser.GetAuthorizedBrowser(StudyData.study.Name);
                lblIESupport.InnerHtml = String.Format(CultureInfo.CurrentCulture, Resources.Resource.FooterSupporterBrowser.ToString(CultureInfo.CurrentCulture), authorizedBrowser);
                pnlLogOff.Visible = Session["login"] != null;
            }
        }
    }
}
