﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Globalization;
using System.Configuration;

using WebDiary.Membership.Provider;
using WebDiary.Membership.User;
using WebDiary.Controls;
using WebDiary.Core.Entities;
using WebDiary.Core.Helpers;
using WebDiary.Core.Constants;
using WebDiary.WDAPI;
using WebDiary.StudyPortal.Helpers;

using log4net;
using WebDiary.Core.Repositories;

namespace WebDiary.StudyPortal.Includes.Usercontrols
{
    public partial class LogOnControl : BaseUserControl
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(LogOnControl));

        public void EnableSessionTimeoutMsg(bool enable)
        {
            SessionTimeoutLabel.Visible = enable;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                HttpBrowserCapabilities browser = Request.Browser;
                bool isAuthorizedBrowser = AuthorizedBrowser.CheckAuthorizedBrowser(StudyData.study.Name, browser.Browser, browser.MajorVersion);
                if (!isAuthorizedBrowser)
                {
                    lblUnauthorizedMessage.Visible = true;

                    string browserNameVersion = browser.Browser;

                    if (string.Equals(browserNameVersion, "Edge", StringComparison.OrdinalIgnoreCase))
                            browserNameVersion = browserNameVersion + "HTML" + " " + browser.MajorVersion;
                    else
                        browserNameVersion = browserNameVersion + " " + browser.MajorVersion;
                    lblUnauthorizedMessage.Text = String.Format(CultureInfo.CurrentCulture, Resources.Resource.UnauthorizedBrowserMsg.ToString(CultureInfo.CurrentCulture), browserNameVersion);

                    List<string> authorizedBrowser = AuthorizedBrowser.GetAuthorizedBrowser2(StudyData.study.Name);
                    string browserList = AuthorizedBrowser.GenerateBrowserList(authorizedBrowser);
                    authBrowserArea.InnerHtml = browserList;

                    txtEmail.Enabled = false;
                    txtPassword.Enabled = false;
                    btnLogOn.Enabled = false;

                    logger.Info(String.Format(CultureInfo.InvariantCulture, "Use of {0} is not supported. Browser Name: {1}, Major version:{2}, userAgent: {3}", browserNameVersion, browser.Browser, browser.MajorVersion, Request.UserAgent));
                    return;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }

            btnLogOn.Attributes.Add("onclick", "validateHtmlForm()");

            btnLogOn.Click += new EventHandler(btnLogin_Click);
        }

        void btnLogin_Click(object sender, EventArgs e)
        {
            Page.Validate();

            if (!Page.IsValid)
            {
                //Validation on the page is already visible so disabling the validation summary
                vsErrors.Visible = false;
                return;
            }
            vsErrors.Visible = true;

            WebDiaryProvider provider = System.Web.Security.Membership.Provider as WebDiaryProvider;
            if (provider != null)
            {
                using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                {
                    SubjectRepository subRep = new SubjectRepository(db);
                    Subject subject = subRep.FindByEmailAndStudyId(txtEmail.Text.Trim(), StudyData.study.Id);
                    WebDiaryUser user = null;
                    if(subject != null)
                        user = (WebDiaryUser)provider.GetUser(subject, db, false);
                    if (user == null)
                    {
                        ClearSession();
                        Page.Validators.Add(new ValidationError(Resources.Resource.eMailPswdInvalid));
                        logger.Info("Failed login attempt with invalid e-mail address.");
                        return;
                    }
                    Session[WDConstants.Language] = Response.Cookies[WDConstants.Language].Value = WDAPIObject.JavaLocaleToDotNetCulture(user.Language);

                    WDAPIObject wd = null;

                    if (StudyData.study != null)
                    {
                        wd = new WDAPIObject(StudyData.study.StudyWorksUsername, StudyData.study.StudyWorksPassword, StudyData.study.Name, StudyData.study.StudyWorksBaseUrl);
                    }

                    if (provider.ValidateUser(subject, db, txtPassword.Text))
                    {
                        if(ConfigurationManager.AppSettings["SUBJECT_SESSION_TIMEOUT"] != null)
                            Session.Timeout = int.Parse(ConfigurationManager.AppSettings["SUBJECT_SESSION_TIMEOUT"]);
                        Session["userName"] = user.Email;
                        Session[WDConstants.VarKrpt] = user.KrPT;

                        //Set a cookie for when the user logs in so that we can handle timeout message on the login page.
                        Response.Cookies.Add(new HttpCookie(WDConstants.UserStatusCookie, WDConstants.UserStatusLoggedIn));


                        // Check status against StudyWorks
                        SubjectStudyEnabled result = wd.IsSubjectAndStudyEnabled(user.KrPT, StudyData.study);
                        if (result == SubjectStudyEnabled.SubjectDisabled)
                        {
                            Page.Validators.Add(new ValidationError(Resources.Resource.ErrorSubjectDisabled));
                            return;
                        }
                        else if (result == SubjectStudyEnabled.StudyDisabled)
                        {
                            Page.Validators.Add(new ValidationError(Resources.Resource.ErrorStudyDisabled));
                            return;
                        }
                        else if (result == SubjectStudyEnabled.Error)
                        {
                            Page.Validators.Add(new ValidationError(String.Format(CultureInfo.InvariantCulture, Resources.Resource.AppErrHasOccurred, "<a href=\"https://mystudy.phtstudy.com/ssa/pages/contact_us/\">", "</a>")));
                            return;
                        }

                        // check if the current password has expired
                        if (StudyData.study.PasswordExpiryDays > 0 && (DateTimeHelper.GetUtcTime() - (DateTime)subject.LastPasswordChangedDate).TotalDays >= StudyData.study.PasswordExpiryDays)
                        {
                            Guid token = provider.CreateForgotPasswordRequest((Guid)user.ProviderUserKey);
                            Session[WDConstants.ResetPasswordText] = Resources.Resource.PasswordExpired;
                            Response.Redirect("reset-password.aspx?token=" + token);
                            return;
                        }

                        //set login flag for validating session on checkbox
                        Session["login"] = true;

                        wd.BeginSendSecurityEvent(HttpHelper.GetIP4Address(), SWAPI.SWAPIAbstraction.SecurityEventValidEntry, DateTimeHelper.GetUtcTime(), user.KrPT,
                            DateTimeHelper.GetTimeZoneOffsetMilliseconds(user.TimeZone.ToString(CultureInfo.InvariantCulture), user.StudyId), "Valid login", new AsyncCallback(SendSecurityEventCallback));

                        if (Request.QueryString["ReturnUrl"] != null)
                        {
                            FormsAuthentication.RedirectFromLoginPage(user.ProviderUserKey.ToString(), false);
                            return;
                        }
                        else
                        {
                            FormsAuthentication.SetAuthCookie(user.ProviderUserKey.ToString(), false);
                            logger.Info("The subject successfully logged in. Subject krpt: " + user.KrPT);
                            Response.Redirect(HttpContext.Current.Request.ApplicationPath);
                        }
                    }
                    else
                    {
                        ClearSession();
                    }

                    // Users state may have changed since calling ValidateUser, repopulate

                    user = (WebDiaryUser)provider.GetUser(subject, db, false);
                    if (user.IsLockedOut)
                    {
                        Page.Validators.Add(new ValidationError(Resources.Resource.AcctLockedExcessiveFailedLoginAttempts));
                    }
                    else
                    {
                        Page.Validators.Add(new ValidationError(Resources.Resource.eMailPswdInvalid));

                        wd.BeginSendSecurityEvent(HttpHelper.GetIP4Address(), SWAPI.SWAPIAbstraction.SecurityEventInvalidEntry, DateTimeHelper.GetUtcTime(), user.KrPT, 
                            DateTimeHelper.GetTimeZoneOffsetMilliseconds(user.TimeZone.ToString(CultureInfo.InvariantCulture), user.StudyId), "Invalid login", new AsyncCallback(SendSecurityEventCallback));
                    }
                }
            }
        }

        public void SendSecurityEventCallback(IAsyncResult ia)
        {
            try
            {
                var ar = (System.Runtime.Remoting.Messaging.AsyncResult)ia;
                var del = (SendSecurityEventDelegate)ar.AsyncDelegate;
                del.EndInvoke(ia);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }
        }

        private void ClearSession()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            Session.Clear();

            Page.Master.FindControl("pnlLogOff").Visible = false;
        }
    }
}
