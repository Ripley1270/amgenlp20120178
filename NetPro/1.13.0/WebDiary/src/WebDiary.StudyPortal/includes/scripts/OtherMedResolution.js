﻿//Generate SubjectMedication Review List (See page 2)
function generateReviewMedList() {

    $('div[id^="custom-control-"]').first().append('<div id="customContainer" class="customContainer">');

    var subjectMasterMedList = getSubjectMasterMedList(),
        container = $('#customContainer');

    numMedList.value = subjectMasterMedList.length;

    //Add input for each subject med
    $.each(subjectMasterMedList, function (index, subjectMasterMed) {
        container.append(MedModuleUtils.getSubjectMasterMedDisplayName(subjectMasterMed) + '<br/>');
    });

    $('#customContainer').append('</div>');

    PDECommonUtils.AddNextButtionValidation(function () {
        var result = true,
		radioSelected = $('input:radio:checked'),
		radioID,
		radioIDOffset;

        if (radioSelected.length > 0) {

            radioID = radioSelected[0].id;
            radioIDOffset = radioID[radioID.length - 1];

            //If Yes selected and there are no medications in the subject's list
            if (radioIDOffset == 0 && numMedList.value == 0) {
                result = false;
            }
        }

        if (result == false) {

            if ($("#myError").length <= 0) {
                $('input[name="ResponseView$LayoutTemplate$_nextZone$_nextBtn"]').after('<div id="myError"><span class="Error" style="text-decoration:none;">' + myCustomText.errorText + '</span></div>');
            }
        }

        return result;
    });
}

function getSubjectMasterMedList() {
    var orgSubjectMedList = MedModuleUtils.getSubjectMedList(),
        subjectMasterMedList = MedModuleUtils.getSubjectMasterMedList(orgSubjectMedList);

    return subjectMasterMedList;
}

//Generate SubjectMedication MultipleChoice List (see page 3)
function generateMultiChoiceMedList() {
    $('div[id^="custom-control-"]').first().append('<div id="customContainer" class="customContainer">');

    var subjectMasterMedList = getSubjectMasterMedList(),
        medSelectListName = MedSelectList.name,
        medSelectListID = MedSelectList.id,
        medSelectListValue = MedSelectList.value,
        container = $('#customContainer');

    $(MedSelectList).remove();

    $.each(subjectMasterMedList, function (index, subjectMasterMed) {
        container.append('<input id="Q' + index + '" type="radio" name="' + medSelectListName + '" value="' + subjectMasterMed.MEDCOD1C + '" class="medClass">' + MedModuleUtils.getSubjectMasterMedDisplayName(subjectMasterMed) + '<br/>');
    });

    $('#customContainer').append('</div>');

    if (medSelectListValue != '') {
        $('input:radio[value="' + medSelectListValue + '"]').prop("checked", true);
    }

    PDECommonUtils.AddNextButtionValidation(function () {
        var result = false,
            numSelected = $('input:radio:checked').length;

        if (numSelected > 0) {
            result = true;
        }

        if (result == false) {

            if ($("#myError").length <= 0) {
                $('input[name="ResponseView$LayoutTemplate$_nextZone$_nextBtn"]').after('<div id="myError"><span class="Error" style="text-decoration:none;">' + ErrorText.text1 + '</span></div>');
            }
        }

        return result;
    });
}

//Generate Add new medication control (Page 5)
function generateNewMedicationControl() {
  var addMedName = AddMed.name,
        addMedID = AddMed.id,
        addMedValue = AddMed.value;

    $(AddMed).remove();

    $('div[id^="custom-control-"]').first().append('<div id="customContainer" class="leftCol"><input id="addBtn" type="button" value="Verify"><input id="newMedTxtbox" name="' + addMedName + '" type="textbox"/><div class="Error" id="errorMessage">Error Message Placeholder</div><div class="result" id="resultMsg"></div>');

    //Try Load old value
    try {
        if (addMedValue != '') {
            var medCodeLength = getParam('medCodeLength'),
                displayVal = $.trim(addMedValue.substring(0, medCodeLength));

            $('#newMedTxtbox').val(addMedValue);
        }
    }
    catch (err) {

    }

    //Hide error message on page load
    $("#errorMessage").hide();

    $("#addBtn").click(function () {
        var medCode = $('#newMedTxtbox').val();

        validateNewMasterMed(medCode);
    });

    PDECommonUtils.AddNextButtionValidation(function () {
        var medCode = $('#newMedTxtbox').val();

        return validateNewMasterMed(medCode);
    });
}

function validateNewMasterMed(medCode) {
    var errorMessageDiv = $("#errorMessage"),
        resultMsgDiv = $('#resultMsg'),
        result = false;

    if (MedModuleUtils.isCorrectMedCodeLength(medCode)) {
        var masterMed = MedModuleUtils.getMasterMedByMedCode(medCode),
            subjectMed = MedModuleUtils.getSubjectMedByMedCode(medCode); 
        
        if (typeof masterMed !== 'undefined') {
            //If med is already in the subject med list
            if (typeof subjectMed !== 'undefined') {
                resultMsgDiv.text('');
                errorMessageDiv.show();
                errorMessageDiv.text(StudyText.text4);
            }
            //Else if med is not in used
            else if (masterMed.MEDNOT1B == '1') {
                resultMsgDiv.text('');
                errorMessageDiv.show();
                errorMessageDiv.text(StudyText.text5);
            }
            //Else this is a valid med
            else {
                errorMessageDiv.hide();
                //Display Med
                resultMsgDiv.html(StudyText.text1 + MedModuleUtils.getMasterMedDisplayName(masterMed) + StudyText.text2);
                result = true;           
            }
        }
        //Else unknown med code
        else {            
            resultMsgDiv.text('');
            errorMessageDiv.show();
            errorMessageDiv.text(StudyText.text3);
        }

    }
    // Else med code length is incorrect
    else {
        resultMsgDiv.text('');
        errorMessageDiv.show();
        errorMessageDiv.text(StudyText.text3);
    }

    return result;
}

//Generate work medication lookup control (Page 7)
function generateNewWorkControl() {
    var addWorkName = AddWork.name,
        addWorkID = AddWork.id,
        addWorkValue = AddWork.value;

    $(AddWork).remove();

    $('div[id^="custom-control-"]').first().append('<div id="customContainer" class="leftCol"><input id="addBtn" type="button" value="Verify"><input id="newMedTxtbox" name="' + addWorkName + '" type="textbox"/><div class="Error" id="errorMessage">Error Message Placeholder</div><div class="result" id="resultMsg"></div>');

    if (addWorkValue != '') {
        var medCodeLength = getParam('medCodeLength'),
            displayVal = $.trim(addWorkValue.substring(0, medCodeLength));

        $('#newMedTxtbox').val(addWorkValue);
    }

    //Hide error message on page load
    $("#errorMessage").hide();

    $("#addBtn").click(function () {
        var workMedCode = $('#newMedTxtbox').val();

        validateNewWorkMed(workMedCode);
    });

    PDECommonUtils.AddNextButtionValidation(function () {
        var workMedCode = $('#newMedTxtbox').val();

        return validateNewWorkMed(workMedCode);
    });
}

function validateNewWorkMed(workMedCode) {
    var errorMessageDiv = $("#errorMessage"),
        resultMsgDiv = $('#resultMsg'),
        workMed = MedModuleUtils.getWorkMedByWorkCode(workMedCode),
        result = false;

    if (workMed) {
        errorMessageDiv.hide();
        resultMsgDiv.html(StudyText.text1 + MedModuleUtils.getWorkMedDisplayName(workMed) + StudyText.text2);
        result = true;
    }
    else {
        resultMsgDiv.text('');
        errorMessageDiv.show();
        errorMessageDiv.html(StudyText.text3);
    }

    return result;
}

//Generate med amount control work medication lookup control (Page 9)
function generateMedAmountControl() {
    var medAmountName = MedAmount.name,
        medAmountID = MedAmount.id,
        medAmountValue = MedAmount.value,
        MEDFRM1L = getMedForm();

    $(MedAmount).remove();

    $('div[id^="custom-control-"]').first().append('<div id="customContainer"><input id="newMedAmountbox" name="' + medAmountName + '" type="textbox"/><div class="Error" id="errorMessage">Error Message Placeholder</div></div>');

    if (medAmountValue != '') {
        $('#newMedAmountbox').val(medAmountValue);
    }

    //Hide error message on page load
    $("#errorMessage").hide();

    $('span#MEDFRM1L').text(MedModuleUtils.decodeMasterMedForm(MEDFRM1L));

    PDECommonUtils.AddNextButtionValidation(function () {
        var workMedCode = $('#newMedAmountbox').val(),
            medForm = getMedForm(),
            errorMessageDiv = $("#errorMessage"),
            response = $('#newMedAmountbox').val(),
            min,
            max,
            errorMSG,
            isValid = false;

        //If med form = mls (inj)(2) or mls (oral)(3)
        if (medForm == '2' || medForm == '3') {
            min = 1;
            max = 99;
            errorMSG = StudyText.text2;
        }
        else {
            min = 1;
            max = 9;
            errorMSG = StudyText.text1;
        }

        //if (PDECommonUtils.isNormalInteger(response)) {
        if (Number(response) >= min && Number(response) <= max) {
            isValid = true;
        }
        //}
        else if (response == '' || response == null) {
            errorMSG = StudyText.text3;
        }

        if (isValid) {
            errorMessageDiv.hide();
        }
        else {
            errorMessageDiv.show();
            errorMessageDiv.text(errorMSG);
        }

        return isValid;
    });
}

//Get MEDFRM1L base on the entry from Page 3, 5, 7 and 8
function getMedForm() {
    var MEDFRM1L,
        masterMed,
        workMed,
        OTHCOD1C;

    OTHCOD1C = $('#OTHCOD1CP3').val();

    //Page 3
    if (OTHCOD1C != '') {
        masterMed = MedModuleUtils.getMasterMedByMedCode(OTHCOD1C);

        if (masterMed) {
            MEDFRM1L = masterMed['MEDFRM1L'];
        }
    }

    //Page 5
    if (typeof MEDFRM1L == 'undefined') {
        OTHCOD1C = $('#OTHCOD1CP5').val();

        if (OTHCOD1C != '') {
            masterMed = MedModuleUtils.getMasterMedByMedCode(OTHCOD1C);

            if (masterMed) {
                MEDFRM1L = masterMed['MEDFRM1L'];
            }
        }
    }

    //Page 7
    if (typeof MEDFRM1L == 'undefined') {
        OTHCOD1C = $('#OTHCOD1CP7').val();

        if (OTHCOD1C != '') {
            workMed = MedModuleUtils.getWorkMedByWorkCode(OTHCOD1C);

            if (workMed) {
                MEDFRM1L = workMed['MEDFRM1L'];
            }
        }
    }

    //Page 9
    if (typeof MEDFRM1L == 'undefined') {
        MEDFRM1L = MedModuleUtils.encodeMasterMedForm($('#MEDFRM1LP8').val());
    }

    return MEDFRM1L;
}