function activateDateTimePicker() {
   //pull today's date from hidden item
   var todaysDate = new Date($('#maxDate').val());
   var assignDate = new Date($('#minDate').val());

   //Set Med ID
   bleedID.value = todaysDate.getTime().toString().slice(2, 11);

   if (todaysDate.getMinutes() < 30) {
       todaysDate.setMinutes(0);
   }
   else {
       todaysDate.setMinutes(30);
   }


   if (assignDate.getMinutes() == 0) {
       assignDate.setMinutes(0);
   }
   else if (assignDate.getMinutes() <= 30) {
       assignDate.setMinutes(30);
   }
   else {
       assignDate.setHours(assignDate.getHours() + 1);
       assignDate.setMinutes(0);
   }

   //Get information 
   var visitDateName = medDate.name;
   var visitDateID = medDate.id;
   var tempVal = medDate.value;

   //Move textArea outside of the matrix
   $(medDate).remove();
   $('#dateContainer').append('<br/><input class="datepicker" name= "' + visitDateName + '" type="text" id= "' + visitDateID + '" />');
   $('#' + visitDateID).datetimepicker({ controlType: 'select', minDate: assignDate, maxDate: todaysDate, defaultDate: todaysDate, dateFormat: 'dd M yy', stepMinute: 30 });

   $('#' + visitDateID).attr('readOnly', 'true');
   $('#' + visitDateID).datepicker("setDate", todaysDate);

   //copy current value into new date picker
   $('#' + visitDateID).val(tempVal);


   //set default time 
   $('#' + visitDateID).unbind('click');
   $('#' + visitDateID).click(function () {
       if (tempVal == "" && $('#' + visitDateID).val() == "") {
           $('#' + visitDateID).datetimepicker("setDate", todaysDate);
       }
   });
}