﻿//loadjscssfile("myscript.js");
//loadjscssfile("mystyle.css");

//load default css for checkbox survey
loadjscssfile("survey.css");


loadjscssfile("handlebars-v4.0.2.js");
loadjscssfile("lodash.min.js");
loadjscssfile("masterWorkList.en_US.js?" + new Date().getTime()); //Append time to make sure file is not cached and latest list is loaded
loadjscssfile("StudyCustom.js");
loadjscssfile("MedicationModule.js");
loadjscssfile("jquery-ui-timepicker-addon.js");
loadjscssfile("jquery-ui-timepicker-addon.css");
