﻿
var MedModuleUtils = MedModuleUtils || {},
    //This needs to be updated for each study. The MUST match the responses in OtherResolveMed survey in Page 8
    MED_FORM = [
                    {
                        VALUE: "1",
                        TEXT: "granulates"
                    },
                    {
                        VALUE: "2",
                        TEXT: "mls (Inj.)"
                    },
                    {
                        VALUE: "3",
                        TEXT: "mls (oral)"
                    },
                    {
                        VALUE: "4",
                        TEXT: "patches"
                    },
                    {
                        VALUE: "5",
                        TEXT: "pills"
                    },
                    {
                        VALUE: "6",
                        TEXT: "powder"
                    },
                    {
                        VALUE: "7",
                        TEXT: "sprays"
                    },
                    {
                        VALUE: "8",
                        TEXT: "suppositories"
                    },
                    {
                        VALUE: "9",
                        TEXT: "Inhalations"
                    }
               ],
    //This needs to be updated for each study. The MUST match the responses in OtherResolveMed survey in Page 8
    MED_ROUTE = [
                    {
                        VALUE: "1",
                        TEXT: "Injectable"
                    },
                    {
                        VALUE: "2",
                        TEXT: "Nasal spray"
                    },
                    {
                        VALUE: "3",
                        TEXT: "Oral"
                    },
                    {
                        VALUE: "4",
                        TEXT: "Orally Inhaled"
                    },
                    {
                        VALUE: "5",
                        TEXT: "Suppository"
                    },
                    {
                        VALUE: "6",
                        TEXT: "Transdermal"
                    }
               ];



//Get Subject Med list that contain Med Code and Alias for each Subject Med
MedModuleUtils.getSubjectMedList = function () {
    var subjectMeds = $.extend(true, [], SUBJECT_MEDS); //Clone subject meds

    return subjectMeds;
};

//Get Master Meds
MedModuleUtils.getMasterMeds = function () {
    var masterMeds = $.extend(true, {}, MASTER_MEDS); //Clone master meds

    return masterMeds;
};

//Get Work Meds
MedModuleUtils.getWorkMeds = function () {
    var workMeds = $.extend(true, {}, WORK_MEDS); //Clone master meds

    return workMeds;
};

MedModuleUtils.getSubjectMedByMedCode = function (medCode) {
    var subjectMeds = MedModuleUtils.getSubjectMedList(),
        tempSubjectMed,
        subjectMed;

    if (subjectMeds) {
        for (var i = 0; i < subjectMeds.length; i++) {
            tempSubjectMed = subjectMeds[i];

            if (tempSubjectMed.MEDCOD1C == medCode) {
                subjectMed = tempSubjectMed;
                break;
            }
        }
    }

    return subjectMed;
};

MedModuleUtils.getMasterMedByMedCode = function (medCode) {
    var masterMeds = MedModuleUtils.getMasterMeds(),
        masterMed;

    if (masterMeds) {
        masterMed = masterMeds[medCode];
    }

    return masterMed;
};

MedModuleUtils.getWorkMedByWorkCode = function (workCode) {
    var workMeds = MedModuleUtils.getWorkMeds(),
        workMed;

    if (workMeds) {
        workMed = workMeds[workCode];
    }

    return workMed;
};

//Get Subject Master Med list that contain the combination of master med and subject med for each subject med
MedModuleUtils.getSubjectMasterMedList = function (subjectMeds) {
    var subjectMasterMedList = [];

    if (typeof subjectMeds === 'undefined') {
        subjectMeds = MedModuleUtils.getSubjectMedList();
    }

    $.each(subjectMeds, function (index, subjectMed) {
        var masterMed = MedModuleUtils.getMasterMedByMedCode(subjectMed.MEDCOD1C),
        //Create Subject master med by merging subject med and master med
            subjectMasterMed = $.extend(true, subjectMed, masterMed);

        subjectMasterMedList.push(subjectMasterMed);
    });

    return subjectMasterMedList;
};

MedModuleUtils.getMasterMedDisplayName = function (med) {
    var result = '';

    if (med) {
        result += med['MEDCOD1C'];
        result += ' - ';
        result += med['MEDNAM1C'];
        result += '; ';
        result += med['MEDDOS2C'];
        result += '; ';
        result += MedModuleUtils.decodeMasterMedRoute(med['MEDRTE1L']);
        result += '; ';
        result += MedModuleUtils.decodeMasterMedForm(med['MEDFRM1L']);
    }

    return result;
};

MedModuleUtils.getSubjectMasterMedDisplayName = function (med) {
    var result = '';

    if (med) {
        result += med['MEDCOD1C'];
        result += ' - ';
        result += med['MEDNAM1C'];
        result += '; ';
        result += med['SBJALS1C'];
        result += '; ';
        result += med['MEDDOS2C'];
        result += '; ';
        result += MedModuleUtils.decodeMasterMedRoute(med['MEDRTE1L']);
        result += '; ';
        result += MedModuleUtils.decodeMasterMedForm(med['MEDFRM1L']);
    }

    return result;
};

MedModuleUtils.getWorkMedDisplayName = function (med) {
    var result = '';

    if (med) {
        result += med['WRKCOD1C'];
        result += ' - ';
        result += med['WRKNAM2C'];
        result += '; ';
        result += med['WRKNAM1C'];
        result += '; ';
        result += med['WRKDOS1C'];
        result += '; ';
        result += MedModuleUtils.decodeMasterMedRoute(med['MEDRTE1L']);
        result += '; ';
        result += MedModuleUtils.decodeMasterMedForm(med['MEDFRM1L']);
    }

    return result;
};

MedModuleUtils.getMedRouteList = function () {
    return $.extend(true, [], MED_ROUTE);
};

//Med Route(MEDRTE1L)
MedModuleUtils.decodeMasterMedRoute = function (code) {
    var retVal = "",
        medRouteList = MedModuleUtils.getMedRouteList(),
        medRoute;

    for (var i = 0; i < medRouteList.length; i++) {
        medRoute = medRouteList[i];

        if (medRoute.VALUE == code) {
            retVal = medRoute.TEXT;
            break;
        }
    }

    return retVal;
};

MedModuleUtils.getMedFormList = function () {
    return $.extend(true, [], MED_FORM);
};

//Med Form(MEDFRM1L)
MedModuleUtils.decodeMasterMedForm = function (code) {
    var retVal = "",
        medFormList = MedModuleUtils.getMedFormList(),
        medForm;

    for (var i = 0; i < medFormList.length; i++) {
        medForm = medFormList[i];

        if (medForm.VALUE == code) {
            retVal = medForm.TEXT;
            break;
        }
    }

    return retVal;
};

MedModuleUtils.encodeMasterMedForm = function (label) {
    var retVal = "",
        medFormList = MedModuleUtils.getMedFormList(),
        medForm;

    for (var i = 0; i < medFormList.length; i++) {
        medForm = medFormList[i];

        if (medForm.TEXT == label) {
            retVal = medForm.VALUE;
            break;
        }
    }

    return retVal;
};

MedModuleUtils.getMedCodeLength = function () {
    return getParam('medCodeLength');
};

MedModuleUtils.isCorrectMedCodeLength = function (medCode) {
    var expectedMedCodeLength = MedModuleUtils.getMedCodeLength(),
        actualMedCodeLength = medCode.length;

    return (expectedMedCodeLength == actualMedCodeLength);
};

MedModuleUtils.decodeHtml = function (html) {
    return $('<div>').html(html).text();
};