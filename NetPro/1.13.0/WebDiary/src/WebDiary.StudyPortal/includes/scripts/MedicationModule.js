function parseSubjectMedList() {
    var delimitedList = $('#delimitedMedList').val();
    var currentNumMeds = $('#numMeds').val();
    var splitList = delimitedList.split('|');
    //var numMeds = splitList[0];

    var subjMedList = [];

    //Build Medication meta arrays
    for (var i = 1; i <= currentNumMeds * 2; i += 2) {
        var med = lookUpMedCodeObjectAlias(splitList[i], splitList[i + 1]);
        subjMedList.push(med);
    }

    return subjMedList;
}

function parseSubjectAliasList() {
    var delimitedList = $('#delimitedMedList').val(),
        currentNumMeds = $('#numMeds').val(),
        splitList = delimitedList.split('|'),
        subjAliasList = [];

    //Build Alias meta arrays
    for (var i = 2; i <= currentNumMeds * 2; i += 2) {
        //subjAliasList.push(decodeURIComponent(splitList[i]));
        subjAliasList.push(splitList[i]);
    }

    return subjAliasList;
}


/*function parseSubjectIndicationList() {
 var delimitedList = $('#delimitedMedList').val();
 var currentNumMeds = $('#numMeds').val();
 var splitList = delimitedList.split('|');
 var splitList = delimitedList.split('|');

 var subjIndicationList = [];

 //Build Alias meta arrays
 for (var i = 3; i <= currentNumMeds * 3; i += 3) {
 subjIndicationList.push(splitList[i]);
 }

 return subjIndicationList;
 }  */

//Find Medication in master medication javascript array
function lookUpMedCode(code) {
    var retVal = "";
    for (var i = 0; i < masterArray.length; i++) {
        if (masterArray[i][0] === code) {
            retVal = masterArray[i][0] + " " + masterArray[i][1] + " " + masterArray[i][2] + " " + masterArray[i][3] + " " + masterArray[i][4] + " " + masterArray[i][5];
            break;
        }
    }

    return retVal;
}

//Find Medication in master medication javascript array
//Depricated
function lookUpMedCodeV2(code) {
    var retVal = "";

    if (code in masterArray) {
        retVal = masterArray[i][0] + " " + masterArray[i][1] + " " + masterArray[i][2] + " " + masterArray[i][3] + " " + masterArray[i][4] + " " + masterArray[i][5];
    }

    return retVal;
}

//Find Medication in master medication javascript array
function lookUpMedCodeObject(code) {
    var retVal = "";

    if (code in masterArray) {
        retVal = {
            medNum: masterArray[code][0],
            medName: masterArray[code][1],
            medSubjName: masterArray[code][2],
            medDose: masterArray[code][3],
            medAdmin: masterArray[code][4],
            medRoute: masterArray[code][5],
            medDisabled: masterArray[code][6],
            medExcluded: masterArray[code][7],
            medMEDFLG2B: masterArray[code][8],
            medMEDFRM2L: masterArray[code][9],
            medDisplayName: masterArray[code][0] + " - " + masterArray[code][1] + "; " + masterArray[code][3] + "; " + decodeMasterMedForm(masterArray[code][4]) + "; " + decodeMasterMedUnit(masterArray[code][5]),
            medAlisDisplayName: masterArray[code][0] + " - " + masterArray[code][1] + "; " + masterArray[code][2] + "; " + masterArray[code][3] + "; " + decodeMasterMedForm(masterArray[code][4]) + "; " + decodeMasterMedUnit(masterArray[code][5])
        };
    }

    return retVal;
}

//Find Medication in master medication javascript array
function lookUpMedCodeObjectAlias(code, alias) {
    var retVal = "";

    if (code in masterArray) {
        retVal = {
            medNum: masterArray[code][0],
            medName: masterArray[code][1],
            medSubjName: masterArray[code][2],
            medDose: masterArray[code][3],
            medAdmin: masterArray[code][4],
            medRoute: masterArray[code][5],
            medDisabled: masterArray[code][6],
            medExcluded: masterArray[code][7],
            medMEDFLG2B: masterArray[code][8],
            medMEDFRM2L: masterArray[code][9],
            medAlias: alias,
            //medIndication: indication,
            medDisplayName: masterArray[code][0] + " - " + masterArray[code][1] + "; " + masterArray[code][3] + "; " + decodeMasterMedForm(masterArray[code][4]) + "; " + decodeMasterMedUnit(masterArray[code][5]),
            medAlisDisplayName: masterArray[code][0] + " - " + masterArray[code][1] + "; " + alias + "; " + masterArray[code][3] + "; " + decodeMasterMedForm(masterArray[code][4]) + "; " + decodeMasterMedUnit(masterArray[code][5]),
            medIndicationDisplayName: masterArray[code][0] + " - " + masterArray[code][1] + "; " + masterArray[code][3] + "; " + decodeMasterMedForm(masterArray[code][4]) + "; " + decodeMasterMedUnit(masterArray[code][5])
        };
    }

    return retVal;
}

//route
function decodeMasterMedForm(code) {
    var retVal = "";

    if (code === "1") {
        retVal = 'Injectable'; // "Oral";
    } else if (code === "2") {
        retVal = 'Nasal spray'; // "Oral (liquid)";
    } else if (code === "3") {
        retVal = 'Oral';  //"Oral (inhaled)";
    } else if (code === "4") {
        retVal = 'Oral - inhaled'; //"Nasal";
    } else if (code === "5") {
        retVal = 'Oral - liquid'; //"Optic";
    } else if (code === "6") {
        retVal = 'Suppository'; //"Aural";
    } else if (code === "7") {
        retVal = 'Transdermal'; //"Transdermal";
    } else if (code === "8") {
        retVal = StudyText.form8; //"Rectal)";
    } else if (code === "9") {
        retVal = StudyText.form9; //"Injectable (IM) ";
    } else if (code === "10") {
        retVal = StudyText.form10; //"Injectable (IV)";
    } else if (code === "11") {
        retVal = StudyText.form11; //"Injectable (SC)";
    } else if (code === "12") {
        retVal = StudyText.form12; //"Injectable";
    }

    return retVal;
}

//form
function decodeMasterMedUnit(code) {
    var retVal = "";

    if (code === "1") {
        retVal = 'granulates'; //"pill";
    } else if (code === "2") {
        retVal = "mls (Inj.)"; //"powder packet";
    } else if (code === "3") {
        retVal = "mls (oral)"; //"mls liquid";
    } else if (code === "4") {
        retVal = "patches"; //"tsp. liquid";
    } else if (code === "5") {
        retVal = "pills"; //"tbsp. liquid";
    } else if (code === "6") {
        retVal = 'powder'; //"drop";
    } else if (code === "7") {
        retVal = 'sprays'; //"film";
    } else if (code === "8") {
        retVal = 'suppositories'; //"spray";
    } else if (code === "9") {
        retVal = 'Inhalations'; //"ointment";
    } else if (code === "10") {
        retVal = StudyText.unit10; //"patch";
    } else if (code === "11") {
        retVal = StudyText.unit11; //"suppository";
    } else if (code === "12") {
        retVal = StudyText.unit12; //"mls injection";
    }

    return retVal;
}

//form2
function decodeMasterMedUnitFormZwei(code) {
    var retVal = "";

    if (code === "1") {
        retVal = "granulates";
    } else if (code === "2") {
        retVal = "mls (Inj.)";
    } else if (code === "3") {
        retVal = "mLs (oral)";
    } else if (code === "4") {
        retVal = "patches";
    } else if (code === "5") {
        retVal = "pills";
    } else if (code === "6") {
        retVal = "powder";
    } else if (code === "7") {
        retVal = "sprays";
    } else if (code === "8") {
        retVal = "suppositories";
    } else if (code === "9") {
        retVal = "Inhalations";
    } else if (code === "10") {
        retVal = "mg";
    } else if (code === "11") {
        retVal = "g";
    } else if (code === "12") {
        retVal = "fingertip(s)";
    } else if (code === "13") {
        retVal = "patch(es)";
    } else if (code === "14") {
        retVal = "suppository(ies)";
    } else if (code === "15") {
        retVal = "mLs (injectable)";
    }

    return retVal;
}


//indication decode
/*function decodeIndication(code) {
 var retVal = "";

 if (code == "1") {
 retVal = "OA Pain";
 } else if (code == "2") {
 retVal = "OA Worsening";
 } else if (code == "3") {
 retVal = "Back Pain";
 } else if (code == "4") {
 retVal = "Neck Pain";
 } else if (code == "5") {
 retVal = "Headache";
 } else if (code == "6") {
 retVal = "Headache / Migraine";
 } else if (code == "7") {
 retVal = "Toothache";
 } else if (code == "8") {
 retVal = "Joint surgery pain";
 } else if (code == "9") {
 retVal = "Other surgery pain";
 } else if (code == "10") {
 retVal = "Other Reason";
 }

 return retVal;
 }*/


function decodeWorkMedForm(code) {

    return decodeMasterMedForm(code);
}

function decodeWorkMedUnit(code) {

    return decodeMasterMedUnit(code);
}

//Find Medication in work medication javascript array
function lookUpWorkCode(code) {
    var retVal = "";
    for (var i = 0; i < workArray.length; i++) {
        if (workArray[i][0] == code) {
            retVal = workArray[i][0] + " " + workArray[i][1] + " " + workArray[i][2] + " " + workArray[i][3] + " " + workArray[i][4] + " " + workArray[i][5];
            break;
        }
    }

    return retVal;
}

//Find Work medication in work medication javascript array
function lookUpWorkCodeObject(code) {
    var retVal = "";

    if (code in workArray) {
        retVal = {
            medNum: workArray[code][0],
            medName: workArray[code][1],
            medSubjName: workArray[code][2],
            medDose: workArray[code][3],
            medAdmin: workArray[code][4],
            medRoute: workArray[code][5],
            medMEDFRM2L: lookUpMedCodeObject(workArray[code][6]).medMEDFRM2L,
            medDisplayName: workArray[code][0] + " - " + workArray[code][1] + "; " + workArray[code][2] + "; " + workArray[code][3] + "; " + decodeWorkMedForm(workArray[code][4]) + "; " + decodeWorkMedUnit(workArray[code][5])
        };
    }

    return retVal;
}

//Generate SubjectMedication MultipleChoice List
function generateMultiChoiceMedList() {
    var subjMedList = parseSubjectMedList();
    var numMeds = subjMedList.length;

    //Pull Checkbox javascript custom control values and remove from page
    var nameQ1 = MedSelectList.name;
    var nameID = MedSelectList.id;
    var tempVal = MedSelectList.value;
    $(MedSelectList).remove();

    $('div[id^="custom-control-"]').first().append('<div id="customContainer" class="customContainer">');

    //Build custom control
    for (var i = 0; i < numMeds; i++) {
        $('#customContainer').append('<input id="Q' + i + '" type="radio" name="' + nameQ1 + '" value="' + subjMedList[i].medNum + '" class="medClass">' + subjMedList[i].medAlisDisplayName + '<br/>');
    }

    $('#customContainer').append('</div>');

    //Set default value
    try {
        if (tempVal != '') {
            for (var j = 0; j < numMeds; j++) {
                if (tempVal == subjMedList[j].medNum) {
                    $('input:radio[name="' + nameQ1 + '"]')[j].checked = true;
                    break;
                }
            }
        }
    }
    catch (err) {

    }

    //Store med Type in "cookie" to be used on future screen
    $('input[name="' + nameQ1 + '"]:radio').change(function () {
        var radioVal = $('input[type=radio]:checked').val();

        for (var k = 0; k < numMeds; k++) {
            if (radioVal == subjMedList[k].medNum) {
                selectedRouteCookie.value = subjMedList[k].medRoute;
//                selectedRouteCookie.value = subjMedList[k].medMEDFRM2L;
                selectedRouteDisplay.value = decodeMasterMedUnit(subjMedList[k].medRoute);
//                selectedRouteDisplay.value = decodeMasterMedUnitFormZwei(subjMedList[k].medMEDFRM2L);

                break;
            }
        }

    });
}

//Generate SubjectMedication Review List
function generateReviewMedList() {
    var subjMedList = parseSubjectMedList();
    var numMeds = subjMedList.length;

    $('div[id^="custom-control-"]').first().append('<div id="customContainer" class="customContainer">');
    //document.write('<div id="customContainer" class="customContainer">');

    for (var i = 0; i < subjMedList.length; i++) {
        $('#customContainer').append(subjMedList[i].medAlisDisplayName + '<br/>');
    }

    $('#customContainer').append('</div>');
}


//Generate Add new medication control
function generateNewMedicationControl() {
    var nameQ1 = AddMed.name;
    var nameID = AddMed.id;
    var tempVal = AddMed.value;
    $(AddMed).remove();

    $('div[id^="custom-control-"]').first().append('<div id="customContainer" class="leftCol"><input id="addBtn" type="button" value="Verify"><input id="newMedTxtbox" name="' + nameQ1 + '" type="textbox"/><input id="isValidVal" name="isValidValue" type="hidden"/><div class="Error" id="errorMessage">Error Message Placeholder</div><div class="result" id="resultMsg"></div>');

    //Try Load old value
    try {
        if (tempVal != '') {
            var medCodeLength = $('#medCodeLength').val();
            displayVal = $.trim(tempVal.substring(0, medCodeLength));
            $('#newMedTxtbox').val(tempVal);

            var textVal = newMedCookie.value;
            if (textVal != -1 && textVal.length > medCodeLength) { //Check that cookie at least has the Medication Code
                $('#resultMsg').text('' + StudyText.text1 + textVal + StudyText.text2 + '');
                $('#isValidVal').val('true');
            }
        }
    }
    catch (err) {

    }

    //Hide error message on page load
    $("#errorMessage").hide();

    $("#addBtn").click(function () {
        return verifyNewMed();
    });
}

function verifyNewMed() {
    var validCode = validateMedCodeLength();
    if (validCode) {
        var medCode = $('#newMedTxtbox').val();
        var retVal = lookUpMedCodeObject(medCode);
        var currentIndication = $('#currentIndication').val();

        if (retVal.medNum == medCode) {
            //check if med is already in the list
            if (isCurrentMed()) {
                $('#resultMsg').text('');
                $("#errorMessage").show();
                $('#errorMessage').text(StudyText.text4);
                $('#isValidVal').val('false');
                newMedCookie.value = '-1';
            }
            else {
                if (retVal.medDisabled == "1" || retVal.medExcluded == "0") {
                    $('#resultMsg').text('');
                    $("#errorMessage").show();
                    $('#errorMessage').text(StudyText.text5);
                    $('#isValidVal').val('false');
                    newMedCookie.value = '-1';
                }
                else {
                    $("#errorMessage").hide();
                    newMedCookie.value = retVal.medDisplayName;
                    selectedRouteCookie.value = retVal.medRoute;
//                    selectedRouteCookie.value = retVal.medMEDFRM2L;
                    selectedRouteDisplay.value = decodeMasterMedUnitFormZwei(retVal.medRoute);
                    excludedCookie.value = retVal.medExcluded;
                    $('#resultMsg').text('' + StudyText.text1 + retVal.medDisplayName + StudyText.text2 + '');
                    tempVal = retVal.medDisplayName;
                    $('#isValidVal').val('true');

                }
            }
        }
        else { //Med code is not valid
            $('#resultMsg').text('');
            $('#isValidVal').val('false');
            newMedCookie.value = '-1';
            $("#errorMessage").show();
            $('#errorMessage').text('The code you have entered is not valid. Please enter a valid code and select Verify.');
        }
    } else {
        $('#resultMsg').text('');
        $('#isValidVal').val('false');
        newMedCookie.value = '-1';
        $("#errorMessage").show();
        $('#errorMessage').text('The code you have entered is not valid. Please enter a valid code and select Verify.');
    }
}

//Generate work medication lookup control
function generateNewWorkControl() {
    var nameQ1 = AddWork.name;
    var nameID = AddWork.id;
    var tempVal = AddWork.value;
    $(AddWork).remove();

    $('div[id^="custom-control-"]').first().append('<div id="customContainer" class="leftCol"><input id="addBtn" type="button" value="Verify"><input id="newMedTxtbox" name="' + nameQ1 + '" type="textbox"/><input id="isValidVal" name="isValidValue" type="hidden"/><div class="Error" id="errorMessage">Error Message Placeholder</div><div class="result" id="resultMsg"></div>');

    //Try Load old value
    try {
        if (tempVal != '') {
            var medCodeLength = $('#medCodeLength').val();
            $('#newMedTxtbox').val(tempVal);

            var textVal = newWorkCookie.value;
            if (textVal != -1 && textVal.length > medCodeLength) { //Check that cookie at least has the Medication Code
                $('#resultMsg').text('' + StudyText.text1 + textVal + StudyText.text2 + '');
                $('#isValidVal').val('true');
            }
        }
    }
    catch (err) {

    }

    //Hide error message on page load
    $("#errorMessage").hide();

    $("#addBtn").click(function () {
        var medCode = $('#newMedTxtbox').val();
        var retVal = lookUpWorkCodeObject(medCode);

        if (retVal.medNum == medCode) {
            $("#errorMessage").hide();
            newWorkCookie.value = retVal.medDisplayName;
            selectedRouteCookie.value = retVal.medRoute;
//            selectedRouteCookie.value = retVal.medMEDFRM2L;
            //selectedRouteDisplay.value = decodeWorkMedUnit(retVal.medRoute);
            selectedRouteDisplay.value = decodeMasterMedUnitFormZwei(retVal.medRoute);
            $('#resultMsg').text('' + StudyText.text1 + retVal.medDisplayName + StudyText.text2 + '');
            tempVal = retVal.medDisplayName;
            $('#isValidVal').val('true');
        }
        else { //Med code is not valid
            $('#resultMsg').text('');
            $('#isValidVal').val('false');
            newWorkCookie.value = '-1';
            $("#errorMessage").show();
            $('#errorMessage').text(StudyText.text3);
        }
    });
}


function WaitAndLoadOldVal() {
    try {
        if (tempVal != '') {
            var medCodeLength = $('#medCodeLength').val();
            displayVal = $.trim(tempVal.substring(0, medCodeLength));
            $('#newMedTxtbox').val(displayVal);

            var textVal = newMedCookie.value;
            if (textVal != -1 && textVal.length > medCodeLength) { //Check that cookie at least has the Medication Code
                $('#resultMsg').text('' + StudyText.text1 + textVal + StudyText.text2 + '');
                $('#isValidVal').val('true');
            }
        }
    }
    catch (err) {

    }

}


//Checks if the medication code is valid length
function validateMedCodeLength() {
    var retVal = false;
    var medCode = $('#newMedTxtbox').val();
    var medCodeLength = $('#medCodeLength').val();

    if (medCode.length == medCodeLength) {
        retVal = true;
    }

    return retVal;
}


//Checks if the medication code is in the subject's current list
function isCurrentMed() {
    var retVal = false;
    var newMedCode = $('#newMedTxtbox').val();
    //var numMeds = $('#numMeds').val();

    var subjMedList = parseSubjectMedList();

    for (var med in subjMedList) {
        if (newMedCode == subjMedList[med].medNum) {
            retVal = true;
        }
    }

    return retVal;
}

//Checks if the medication code is in the subject's current list
/*function isCurrentMedAndIndication(indication) {
 var retVal = false;
 var newMedCode = $('#newMedTxtbox').val();
 //var numMeds = $('#numMeds').val();

 var subjMedList = parseSubjectMedList();

 for (var med in subjMedList) {
 if (newMedCode == subjMedList[med].medNum && indication == decodeIndication(subjMedList[med].medIndication)) {
 retVal = true;
 }
 }

 return retVal;
 }*/

function generateUpdateMedListControl() {
    if (typeof masterArray === 'undefined') {
        setTimeout(function () {
            generateUpdateMedListControl();
        }, 100);
    } else {
        var subjMedList = parseSubjectMedList();
        var subjAliasList = parseSubjectAliasList();
        //var subjIndicationList = parseSubjectIndicationList();
        var numMeds = subjMedList.length;
        var medCodeLength = $('#medCodeLength').val();
        var phase = $('#phase').val();

        var nameQ1 = updateMedList.name;
        var nameID = updateMedList.id;
        var tempVal = updateMedList.value;
        $(updateMedList).remove();

        var aliasQ2 = updateAliasList.name;
        var aliasID = updateAliasList.id;
        var aliasTempVal = updateAliasList.value;
        $(updateAliasList).remove();

        /*var indicationQ3 = updateIndicationList.name;
         var indicationID = updateIndicationList.id;
         var indicationTempVal = updateIndicationList.value;
         $(updateIndicationList).remove();*/

        $('div[id^="custom-control-"]').first().append('<div id="customContainer" class="customContainer"><div id="medListContainer">');

        if (tempVal == '') {
            for (var i = 0; i < numMeds; i++) {
                $('#medListContainer').append('<div id="s' + subjMedList[i].medNum + subjMedList[i].indication + '"><input class="subjAlias" maxlength="25" type="text" value="' + subjAliasList[i] + '"/><input class="medClass" id="' + subjMedList[i].medNum + subjMedList[i].indication + '" type="radio" name="medlist" value="' + subjMedList[i].medIndicationDisplayName + '">' + subjMedList[i].medIndicationDisplayName + '<br/></div>');
                //$('#medListContainer').append('<div id="s' + subjMedList[i].medNum + subjMedList[i].indication + '"><input class="subjAlias" maxlength="25" type="text" value="' + subjAliasList[i] + '"/><input class="medClass" id="' + subjMedList[i].medNum + subjMedList[i].indication +'" type="radio" name="medlist" value="' + subjMedList[i].medIndicationDisplayName + '">' + subjMedList[i].medIndicationDisplayName + '<input class="subjIndication" type="hidden" value="' + subjIndicationList[i] + '"><br/></div>');
            }
        } else {
            var medSplit = tempVal.split("|");
            var aliasSplit = aliasTempVal.split("|");
            //var indicationSplit = indicationTempVal.split("|");
            for (var j = 0; j < medSplit.length - 1; j++) {
                var medCodeStr = $.trim(medSplit[j].substring(0, medCodeLength));
                $('#medListContainer').append('<div id="s' + medCodeStr + '"><input class="subjAlias" maxlength="25" type="text" value="' + aliasSplit[j] + '"/><input class="medClass" id="' + medCodeStr + '" type="radio" name="medlist" value="' + medSplit[j] + '">' + medSplit[j] + '<br/></div>');
            }
        }

        $('#medListContainer').append('</div></div>');
        //$('#customContainer').append('<br/><br/><table><tr><td/><td>&nbsp;&nbsp;<b>Indication</b></td></tr><tr><td><input id="deleteBtn" type="button" value="Delete"><input style="margin-left: 10px;" id="addBtn" type="button" value="Add Item"><input id="newMedTxtbox" type="textbox"/></td><td>&nbsp;&nbsp;<select id="indicationDrop" /></td></tr></table><input id="valueHolder" name="' + nameQ1 + '" type="hidden"/><input id="aliasHolder" name="' + aliasQ2 + '" type="hidden"/><input id="indicationHolder" name="' + indicationQ3 + '" type="hidden" /><div class="Error" id="errorMessage"></div>');
        $('#customContainer').append('<br/><br/><table><tr><td><input id="deleteBtn" type="button" value="Delete"><input style="margin-left: 10px;" id="addBtn" type="button" value="Add Item"><input id="newMedTxtbox" type="textbox"/></td></tr></table><input id="valueHolder" name="' + nameQ1 + '" type="hidden"/><input id="aliasHolder" name="' + aliasQ2 + '" type="hidden"/><div class="Error" id="errorMessage"></div>');

        //populate indication dropdown
        /*$('#indicationDrop').append($('<option></option>').val(-1).html('-'));
         for (var k = 1; k <= 10; k++) {
         $('#indicationDrop').append($('<option></option>').val(k).html(decodeIndication(k)));
         }*/

        var allMedString = "";
        var allAliasString = "";
        //var allIndicationString = "";
        $(".medClass").each(function () {
            var $this = $(this);
            //Build up a string with all med values
            allMedString = allMedString + $this.val() + "|";
        });

        $(".subjAlias").each(function () {
            var $this = $(this);
            //Build up a string with all med values
            allAliasString = allAliasString + $this.val() + "|";
        });

        /*$(".subjIndication").each(function () {
         var $this = $(this);
         //Build up a string with all med values
         allIndicationString = allIndicationString + $this.val() + "|";
         });*/

        tempVal = allMedString;
        aliasTempVal = allAliasString;
        //indicationTempVal = allIndicationString;
        $('#valueHolder').val(tempVal);
        $('#aliasHolder').val(aliasTempVal);
        //$('#indicationHolder').val(indicationTempVal);


        //Bind delete button handler
        $("#deleteBtn").click(function () {
            var allMedString = "";
            var allAliasString = "";
            //var allIndicationString = "";

            $(".medClass").each(function () {
                var $this = $(this);
                if ($this.is(":checked")) {
                    $('#s' + $this.attr("id")).remove();
                } else {
                    //Build up a string with all values
                    allMedString = allMedString + $this.val() + "|";
                }
            });

            $(".subjAlias").each(function () {
                var $this = $(this);
                //Build up a string with all med values
                allAliasString = allAliasString + $this.val() + "|";
            });

            /*$(".subjIndication").each(function () {
             var $this = $(this);
             //Build up a string with all med values
             allIndicationString = allIndicationString + $this.val() + "|";
             });*/

            tempVal = allMedString;
            aliasTempVal = allAliasString;
            //indicationTempVal = allIndicationString;
            $('#valueHolder').val(tempVal);
            $('#aliasHolder').val(aliasTempVal);
            //$('#indicationHolder').val(indicationTempVal);
        });


        //Bind Add button handler
        $("#addBtn").click(function () {
            var validCode = validateMedCode();
            if (validCode) {
                var medCode = $('#newMedTxtbox').val();
                var indicationCode = $('#indicationDrop').val();
                //var retVal = lookUpMedCodeObjectAlias(medCode, "", indicationCode);
                var retVal = lookUpMedCodeObjectAlias(medCode, "");

                //if (retVal.medNum == medCode && indicationCode > 0) {
                if (retVal.medNum == medCode) {
                    //check if med is already in the list

                    if (isCurrentMed(medCode)) {
                        $('#errorMessage').text(StudyText.text3);
                    }
                    else {
                        if (retVal.medDisabled == "1") {
                            $('#errorMessage').text(StudyText.text5);
                        }
                        else if (retVal.medExcluded == "0") {
                            $('#errorMessage').text(StudyText.text5);
                        }
                        else {
                            var newMed = $('<span id="s' + medCode + indicationCode + '"><input class="subjAlias" type="text" maxlength="25" /><input class="medClass" id="' + medCode + indicationCode + '" type="radio"    name="medlist" value="' + retVal.medIndicationDisplayName + '"/>' + retVal.medIndicationDisplayName + '<input class="subjIndication" type="hidden" value="' + indicationCode + '"><br></span>');

                            $('#medListContainer').append(newMed);
                            $('#errorMessage').text(""); //clear error text
                            $('#newMedTxtbox').val(''); //clear text entry
                        }
                    }
                }
                else {
                    $('#errorMessage').text(StudyText.text2);
                }
                var allMedString = "";
                var allAliasString = "";
                var allIndicationString = "";

                $(".medClass").each(function () {
                    var $this = $(this);
                    //Build up a string with all values
                    allMedString = allMedString + $this.val() + "|";
                });

                $(".subjAlias").each(function () {
                    var $this = $(this);
                    //Build up a string with all med values
                    allAliasString = allAliasString + $this.val() + "|";
                });

                /*$(".subjIndication").each(function () {
                 var $this = $(this);
                 //Build up a string with all med values
                 allIndicationString = allIndicationString + $this.val() + "|";
                 });*/


                tempVal = allMedString;
                aliasTempVal = allAliasString;
                //indicationTempVal = allIndicationString;
                $('#valueHolder').val(tempVal);
                $('#aliasHolder').val(aliasTempVal);
                //$('#indicationHolder').val(indicationTempVal);
            }
        });

        //Checks if the medication code is valid
        function validateMedCode() {
            var retVal = false;
            var medCode = $('#newMedTxtbox').val();
            var medCodeLength = $('#medCodeLength').val();
            var maxNumMeds = $('#maxNumMeds').val();
            var numMeds = $('input:radio').length;

            if (numMeds < maxNumMeds) {
                if (medCode.length == medCodeLength) {
                    retVal = true;
                }
                else {
                    $('#errorMessage').text(StudyText.text2);
                }
            }
            else {
                $('#errorMessage').text(StudyText.text1);
            }

            return retVal;
        }


        //Determines if medication code is already in the list
        //function isCurrentMed(newMedCode, newIndication) {
        function isCurrentMed(newMedCode) {
            var retVal = false,
                currentMedList = $('#valueHolder').val(),
                medCodeLength = $('#medCodeLength').val(),
                medSplit = tempVal.split("|"),
                //indicationSplit = indicationTempVal.split("|"),
                tmpMed,
                tmpIndication;

            for (var j = 0; j < medSplit.length - 1; j++) {
                tmpMed = $.trim(medSplit[j].substring(0, medCodeLength));
                //tmpIndication = $.trim(indicationSplit[j]);

                //Considered same med if medcode and indication are both the same
                if (tmpMed == newMedCode) {
                    retVal = true;
                }
            }

            return retVal;
        }
    }
}

function displayMedRoute() {
    var medFormSpan = $('#medFormSpan'),
        medRoutePageList = [3, 5, 7],
        medRouteInput,
        tempVal,
        medRouteValue;

    for (var i = 0; i < medRoutePageList.length; i++) {
        medRouteInput = $('#medRouteP' + medRoutePageList[i]);

        if (medRouteInput.length > 0) {
            tempVal = medRouteInput.val().trim();

            if (tempVal && tempVal.length > 0) {
                medRouteValue = tempVal;
                break;
            }
        }
    }

    if (medRouteValue) {
        medFormSpan.text(medRouteValue);
    }
}


function updateMedfinalValidation() {
    var result = true,
        allAliasString = "",
        tmpString = "",
        aliasNames = {},
        isDuplicateAlias = false,
        errorText,
        aliasTempVal,
        emptyAlias = false;

    $(".subjAlias").each(function () {
        var $this = $(this);

        tmpString = $this.val();
        //Remove duplicate spaces
        tmpString = tmpString.replace(/ +/g, " ");
        //Remove leading and trailing spaces
        //tmpString = tmpString.trim();
        tmpString = $.trim(tmpString);
        //remove pipe
        tmpString = tmpString.replace(/(\|)/g, "");

        if (tmpString.length <= 0) {
            emptyAlias = true;
            result = false;
        }

        //Build up a string with all med values
        allAliasString = allAliasString + tmpString + "|";

        //check if existing alias name already exist
        if (aliasNames[tmpString]) {
            result = false;
            isDuplicateAlias = true;
            $this.addClass("inputError");
            aliasNames[tmpString].addClass("inputError");
        }
        else {
            $this.removeClass("inputError");
            aliasNames[tmpString] = $this;
        }
    });

    aliasTempVal = allAliasString;
    $('#aliasHolder').val(aliasTempVal);


    // Set focus on first invalid field and hilight it as in error.
    if (result == false) {
        if (emptyAlias) {
            errorText = StudyText.text7;
        }
        else {
            errorText = StudyText.text8;
        }

        if ($("#myError").length <= 0) {
            $('input[name="ResponseView$LayoutTemplate$_nextZone$_nextBtn"]').after('<div id="myError"><span class="Error" style="text-decoration:none;">' + errorText + '</span></div>');
        }
        else {
            $('div#myError span.Error').text(errorText)
        }
    }

    return result;
}