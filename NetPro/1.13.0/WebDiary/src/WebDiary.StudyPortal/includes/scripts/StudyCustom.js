﻿if (!String.format) {
    String.format = function (format) {
        var args = Array.prototype.slice.call(arguments, 1);
        return format.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match
                ;
        });
    };
}

/*
 *   Handlerbar Templates
 */

Templates = {};

Templates.question = [
    "<span id={{questionID}} class='Question'>{{{questionText}}}</span><br/>"
].join("\n");

Templates.radioAnswer = [
    "<input id={{className}}a{{answerValue}} class='{{className}}' type='radio' style='display:inline;' name={{answerName}} value={{answerValue}}>",
    "<label for={{className}}a{{answerValue}}>{{answerText}}</label>",
    "</br>"
].join("\n");

Templates.checkboxAnswer = [
    "<input id={{className}}a{{answerValue}} class='{{className}}' type='checkbox' style='display:inline;' name={{answerName}} value={{answerValue}}>",
    "<label for={{className}}a{{answerValue}}>{{answerText}}</label>",
    "</br>"
].join("\n");

Templates.textBox = [
    "<input id={{id}} class='{{className}}' type='text' name={{answerName}} maxlength={{maxLength}} ><br/>"
].join("\n");

Templates.dropDown = [
    "<br/>",
    "<select id={{id}} class={{className}} name={{answerName}}>",
    "<option value='-1'> </option>",
    "</select>"
].join("\n");


function generateRandomizationDatePicker() {
    var maxDate = new Date($('#maxDate').val()),
        minDate = new Date($('#minDate').val()),
        StartDateName = randDate.name,
        StartDateID = randDate.id,
        tempVal = randDate.value;

    //Move textArea outside of the matrix
    $(randDate).remove();
    $('#dateContainer').closest('table').find('div[id^="custom-control-"]').append('<input name= "' + StartDateName + '" type="text" id= "' + StartDateID + '" class="randDate" />');
    $('#' + StartDateID).datepicker({
        minDate: minDate,
        maxDate: maxDate,
        defaultDate: maxDate,
        dateFormat: 'dd M yy',
        showOtherMonths: true
    });

    $('#' + StartDateID).attr('readOnly', 'true');

    //copy current value into new date picker
    $('#' + StartDateID).val(tempVal);
}


function generateVisitDropDownQuestion() {
    var className = "A1",
        nameQ1 = visitList.name,
        nameID = visitList.id,
        tempVal = visitList.value || $('#' + className).val(),

        dropdownTemplate = Handlebars.compile(Templates.dropDown),

        answers = [
            {key: 'rescreening', value: 1},
            {key: 'startTreatment', value: 2},
            {key: 'treatmentDisc', value: 3},
            {key: 'progAndTreatmentDisc', value: 4},
            {key: 'progression', value: 5},
            {key: 'studyDisc', value: 6}
        ],
        answerHTML = "",
        currentPhase = $('#phase').val(),
        studyArm = $('#studyArm').val();

    $(visitList).remove();

    //screening
    if (currentPhase == '100') {
        answers = _.filter(answers, function (answer) {
            return answer.value == 1 || answer.value == 2 || answer.value == 6;
        });
    }
    else if (currentPhase == '200' &&
        studyArm == '1') {
        answers = _.filter(answers, function (answer) {
            return answer.value == 3 || answer.value == 4 || answer.value == 6;
        });
    }
    else if (currentPhase == '200' &&
        studyArm != '1') {
        answers = _.filter(answers, function (answer) {
            return answer.value == 4 || answer.value == 6;
        });
    }
    else if (currentPhase == '300') {
        answers = _.filter(answers, function (answer) {
            return answer.value == 5 || answer.value == 6;
        });
    }
    else if (currentPhase == '301') {
        answers = _.filter(answers, function (answer) {
            return answer.value == 6;
        });
    }

    answerHTML = dropdownTemplate({id: className, className: className, answerName: nameQ1});
    $('#visitContainer').html(answerHTML);

    for (var i = 0; i < answers.length; i++) {
        $('#' + className).append($('<option/>', {
            value: answers[i].value,
            text: myCustomText[answers[i].key]
        }));
    }

    //Set default value
    try {
        if (tempVal != '' && tempVal != -1) {
            $('#' + className).val(tempVal);
        }
    }
    catch (err) {

    }

}


function generateOtherSourceQuestion() {
    var className = "Q1",
        answers = [
            {key: 'Q1A1', value: 0},
            {key: 'Q1A2', value: 1},
            {key: 'Q1A3', value: 2}
        ],
        questionKey = "Q1Question",
        checkBoxVar = sourceSelect,
        containerID = "#Q1Container";


    generateMultipleChoiceWithCheckbox(className, questionKey, containerID, answers, checkBoxVar, false);

    //single page approach - need to manually update checkbox side variable as there is no post-back
    $('input[type="radio"][name="' + checkBoxVar.name + '"]').unbind('change');
    $('input[type="radio"][name="' + checkBoxVar.name + '"]').change(function () {
        $("#Q1a99999998").prop("checked", false);
    });


    $('input[type="checkbox"][name="' + checkBoxVar.name + '"]').unbind('change');
    $('input[type="checkbox"][name="' + checkBoxVar.name + '"]').change(function () {
        $("#Q1a0").prop("checked", false);
        $("#Q1a1").prop("checked", false);
        $("#Q1a2").prop("checked", false);
    });

}


/* 
 *  Multiple choice control
 *  - className
 *  - questionKey: key to customText javascript object to pull question text
 *  - containerID: html id that control will get injected into
 *  - answers: array of answer objects. Each answer object must have a key attribute and a value attribute. Key is used to fetch text.
 *  - checkboxVariable: name of Matrix Row id added to the pht javascript control in checkbox
 *  - boldAnswers: true or false flag to determine if answer text appears bolded
 */
function generateMultipleChoiceWithCheckbox(className, questionKey, containerID, answers, checkboxVariable, boldAnswers) {
    var nameQ1 = checkboxVariable.name,
        /*nameID = checkboxVariable.id,
         tempVal = checkboxVariable.value,*/
        questionTemplate = Handlebars.compile(Templates.question),
        answerTemplate,
        /*checkboxAnswerTemplate,*/
        answerHTML = "";

    if (boldAnswers) {
        answerTemplate = Handlebars.compile(Templates.radioAnswerBold);
    }
    else {
        answerTemplate = Handlebars.compile(Templates.radioAnswer);
    }

    /*checkboxAnswerTemplate = Handlebars.compile(Templates.checkboxAnswer);*/

    $(checkboxVariable).remove();

    answerHTML += questionTemplate({questionText: myCustomText[questionKey], questionID: className + "Q1"});

    for (var i = 0; i < answers.length; i++) {
        answerHTML += answerTemplate({
            answerText: myCustomText[answers[i].key],
            answerName: nameQ1,
            answerValue: answers[i].value,
            className: className
        });
    }
    /*answerHTML += '<br/><br/>';
     answerHTML += checkboxAnswerTemplate({ answerText: myCustomText['Q2A1'], answerName: nameQ1, answerValue: 99999998, className: className });*/

    $(containerID).html(answerHTML);

    /*try {
     if (tempVal != '') {
     $('#' + className + 'a' + tempVal).prop('checked', true);
     }
     }
     catch (err) {

     }*/
}

/* 
 *  Multiple select control
 *  - className
 *  - questionKey: key to customText javascript object to pull question text
 *  - containerID: html id that control will get injected into
 *  - answers: array of answer objects. Each answer object must have a key attribute and a value attribute. Key is used to fetch text.
 *  - checkboxVariable: name of Matrix Row id added to the pht javascript control in checkbox
 */
function generateMultipleSelect(className, questionKey, containerID, answers, checkboxVariable) {
    var nameQ1 = checkboxVariable.name,
        nameID = checkboxVariable.id,
        tempVal = checkboxVariable.value,
        questionTemplate = Handlebars.compile(Templates.question),
        answerTemplate = Handlebars.compile(Templates.checkboxAnswer),
        answerHTML = "";

    $(checkboxVariable).remove();

    answerHTML += questionTemplate({questionText: myCustomText[questionKey], questionID: className + "Q1"});

    for (var i = 0; i < answers.length; i++) {
        answerHTML += answerTemplate({
            answerText: myCustomText[answers[i].key],
            answerName: nameQ1,
            answerValue: answers[i].value,
            className: className
        });
    }

    $(containerID).html(answerHTML);

    try {
        if (tempVal != '') {
            $('#' + className + 'a' + tempVal).prop('checked', true);
        }
    }
    catch (err) {

    }

}

function activateNoBackHandler() {
    // Override the Next Button
    $('#ResponseView_LayoutTemplate__nextZone__nextBtn').off('click');
    $('#ResponseView_LayoutTemplate__nextZone__nextBtn').on('click', function () {
        var result = true,
            selectedInput = $('input[type="radio"]:checked'),
            selectedID,
            selectedIDSuffix;

        for (var i = 0; i < selectedInput.length; i++) {
            selectedID = $(selectedInput[i]).attr('id');
            selectedIDSuffix = selectedID.substr(selectedID.length - 1);

            if (selectedIDSuffix == '1') { //Index 1, not value 1
                //Press back button
                $('#ResponseView_LayoutTemplate__previousZone__prevBtn').trigger('click');
                result = false;
                break;
            }
        }

        return result;
    });
}