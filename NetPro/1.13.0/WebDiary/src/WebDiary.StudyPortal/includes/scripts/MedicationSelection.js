﻿function AddMedSelectionControl() {
    var subjectMasterMedList = getCurrentSubjectMasterMedList(),
        subjectMedListID = SubjectMedListResponse.id,
        subjectMedListName = SubjectMedListResponse.name;

    $(SubjectMedListResponse).remove();

    $('div[id^="custom-control-"]').first().append('<div id="customContainer" class="customContainer"><div id="medListContainer">');

    var medListContainer = $('#medListContainer');

    //Add input for each subject med
    $.each(subjectMasterMedList, function (index, subjectMasterMed) {
        addSubjectMedInput(medListContainer, subjectMasterMed);
    });

    medListContainer.append('</div></div>');

    $('#customContainer').append('<br/><br/><table><tr><td><input id="deleteBtn" type="button" value="Delete"><input style="margin-left: 10px;" id="addBtn" type="button" value="Add Item"><input id="newMedTxtbox" type="textbox"/></td></tr></table><input id="' + subjectMedListID + '" name="' +
                                subjectMedListName + '" type="hidden"/><div class="Error" id="errorMessage"></div>');

    saveSubjectMedsValue();

    PDECommonUtils.AddNextButtionValidation(updateMedfinalValidation);

    //Bind delete button handler
    $("#deleteBtn").click(function () {

        $(".medClass").each(function () {
            var $this = $(this);
            if ($this.is(":checked")) {
                $('#s' + $this.attr("id")).remove();
            } 
        });

        saveSubjectMedsValue();
    });

    //Bind Add button handler
    $("#addBtn").click(function () {
        var validCode = validateMedCode();

        if (validCode) {
            var medCode = $('#newMedTxtbox').val(),
                masterMed = MedModuleUtils.getMasterMedByMedCode(medCode),
                medListContainer = $('#medListContainer');

            if (masterMed && masterMed.MEDCOD1C == medCode) {

                //check if med is already in the list
                if (isCurrentMed(medCode)) {
                    $('#errorMessage').text(StudyText.text3);
                }
                else {
                    if (masterMed.MEDNOT1B == "1") {
                        $('#errorMessage').text(StudyText.text5);
                    }
                    else if (masterMed.MEDSTS1L == "0") {
                        $('#errorMessage').text(StudyText.text6);
                    }
                    //Else successful
                    else {
                        addSubjectMedInput(medListContainer, masterMed);

                        saveSubjectMedsValue();

                        $('#newMedTxtbox').val("");

                        $('#errorMessage').text(""); //clear error text
                    }
                }
            }
            else {
                $('#errorMessage').text(StudyText.text2);
            }
        }
    });

    function addSubjectMedInput(container, subjectMasterMed) {

        if (subjectMasterMed && typeof subjectMasterMed.SBJALS1C == 'undefined') {
            subjectMasterMed.SBJALS1C = "";
        }

        medListContainer.append('<div id="s' + subjectMasterMed.MEDCOD1C + '" class="subjMed"><input class="subjAlias" maxlength="40" type="text" value="' +
                                subjectMasterMed.SBJALS1C + '"/><input class="medClass" id="' + subjectMasterMed.MEDCOD1C +
                                '" type="radio" name="medlist" value="' + subjectMasterMed.MEDNAM1C + '">' + MedModuleUtils.getMasterMedDisplayName(subjectMasterMed) + '<br/></div>');
    }

    //Checks if the medication code is valid
    function validateMedCode() {
        var retVal = false,
            medCode = $('#newMedTxtbox').val(),
            maxNumMeds = getParam('maxNumMeds'),
            numMeds = $('input:radio').length;

        if (numMeds < maxNumMeds) {
            if (MedModuleUtils.isCorrectMedCodeLength(medCode)) {
                retVal = true;
            }
            else {
                $('#errorMessage').text(StudyText.text2);
            }
        }
        else {
            $('#errorMessage').text(StudyText.text1);
        }

        return retVal;
    }

    //Determines if medication code is already in the list
    function isCurrentMed(newMedCode) {
        var retVal = false,
            subjectMedsJSON = $('#' + SubjectMedListResponse.id).val(),
            subjectMeds = (subjectMedsJSON)? JSON.parse(subjectMedsJSON): [],
            subjectMed;

        for(var i = 0; i < subjectMeds.length; i++){
            subjectMed = subjectMeds[i];

            if (subjectMed.MEDCOD1C == newMedCode) {
                retVal = true;
                break;
            }
        }

        return retVal;
    }

    function getCurrentSubjectMasterMedList() {
        var orgSubjectMedList = MedModuleUtils.getSubjectMedList();
            currentSubjectMedList = getSubjectMedListValue(true),
            subjectMasterMedList;

        if (currentSubjectMedList.length == 0) {
            currentSubjectMedList = orgSubjectMedList;
        }

        subjectMasterMedList = MedModuleUtils.getSubjectMasterMedList(currentSubjectMedList);

        return subjectMasterMedList;
    }
}

//Save Subject Med value
function saveSubjectMedsValue() {
    var updatedSubjectMeds = [],
        updatedSubjectMedsJSON,
        updatedSortedSubjectMeds = [],
        updatedSortedSubjectMedsJSON,
        SBJEFF1S = getParam('currentDT'),
        orgSubjectMeds = MedModuleUtils.getSubjectMedList(),
        sortByMedCode = function (subjectMed1, subjectMed2) {
            var medCode1 = Number(subjectMed1.MEDCOD1C),
                medCode2 = Number(subjectMed2.MEDCOD1C);

            return ((medCode1 < medCode2) ? -1 : ((medCode1 > medCode2) ? 1 : 0));
        };

    //For each row, add a subject med to array
    $('.subjMed').each(function () {
        var $this = $(this),
            alias = $this.find('.subjAlias').val(),
            medCode = $this.find('.medClass').attr('id');

        //Clean alias
        alias = alias.replace(/ +/g, " ");
        alias = $.trim(alias);

        //TODO encode alias?
        if (typeof medCode != 'undefined' && medCode != '') {
            updatedSubjectMeds.push({
                'MEDCOD1C': medCode,
                'SBJALS1C': alias
            });
        }
    });

    //Sort subject meds
    updatedSortedSubjectMeds = $.extend(true, [], updatedSubjectMeds);
    updatedSortedSubjectMeds.sort(sortByMedCode);

    //Add org subject meds that've been deleted
    $.each(orgSubjectMeds, function (index, orgSubjectMed) {
        //Check if the original subject med still being used in the Updated Subject Med list
        var filterMeds = $.grep(updatedSubjectMeds, function (updatedSubjectMed, index) {
            var result = false;

            if (orgSubjectMed.MEDCOD1C == updatedSubjectMed.MEDCOD1C) {
                result = true;
            }

            return result;
        });

        //If filter meds is empty, it means the med is removed
        if (filterMeds.length == 0) {
            //Add med as deleted
            orgSubjectMed['SBJDEL1B'] = '1';
            //html decode alias
            orgSubjectMed['SBJALS1C'] = MedModuleUtils.decodeHtml(orgSubjectMed['SBJALS1C']);

            orgSubjectMed['SBJEFF1S'] = PDECommonUtils.urlDecode(SBJEFF1S);

            updatedSubjectMeds.push(orgSubjectMed);
            updatedSortedSubjectMeds.push(orgSubjectMed);
        }
    });

    //Convert subject meds array to json
    updatedSubjectMedsJSON = JSON.stringify(updatedSubjectMeds);
    updatedSortedSubjectMedsJSON = JSON.stringify(updatedSortedSubjectMeds);

    //Save to input
    $('#' + SubjectMedListResponse.id).val(updatedSubjectMedsJSON);
    $('#' + SortedSubjectMedListResponse.id).val(updatedSortedSubjectMedsJSON);
}

function getSubjectMedListValue(filterDeleted) {
    var subjectMedsJSON = $('#' + SubjectMedListResponse.id).val(),
        updatedSubjectMeds = (subjectMedsJSON) ? JSON.parse(subjectMedsJSON) : [],
        result = [];

    if (filterDeleted) {

        $.each(updatedSubjectMeds, function (index, updatedSubjectMed){
            if(updatedSubjectMed['SBJDEL1B'] != '1'){
                result.push(updatedSubjectMed);
            }
        });
    }
    //Return unfiltered list
    else{
        result = updatedSubjectMeds;
    }

    return result;
}

function updateMedfinalValidation() {
    var result = true,
        isDuplicateAlias = false,
        aliasNames = {},
        errorText,
        currentSubjectMedList;        

    //Save subject meds
    saveSubjectMedsValue();

    currentSubjectMedList = getSubjectMedListValue(true);

    $.each(currentSubjectMedList, function (index, subjectMed) {
        var SBJALS1C = subjectMed['SBJALS1C'],
            MEDCOD1C = subjectMed['MEDCOD1C'],
            subjMedDiv = $('.subjMed#s' + MEDCOD1C);

        //Chec if alias is set
        if (typeof SBJALS1C == 'undefined' || SBJALS1C == null || SBJALS1C == '') {
            result = false;
        }
        //check if existing alias name already exist
        else if (aliasNames[SBJALS1C]) {
            result = false;
            isDuplicateAlias = true;
            subjMedDiv.addClass("inputError");
            aliasNames[SBJALS1C].addClass("inputError");
        }
        else {
            subjMedDiv.removeClass("inputError");
            aliasNames[SBJALS1C] = subjMedDiv;
        }

    });

    // Set focus on first invalid field and hilight it as in error.
    if (result == false) {

        if (isDuplicateAlias) {
            errorText = StudyText.text8;
        }
        else {
            errorText = StudyText.text7;
        }

        if ($("#myError").length <= 0) {
            $('input[name="ResponseView$LayoutTemplate$_nextZone$_nextBtn"]').after('<div id="myError"><span id="medError" class="Error" style="text-decoration:none;">' + errorText + '</span></div>');
        }
        else {
            $("#medError").text(errorText);
        }
    }

    return result;
}