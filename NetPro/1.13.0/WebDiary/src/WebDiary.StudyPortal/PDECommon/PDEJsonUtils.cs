﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;
using System.Xml.Linq;

namespace WebDiary.StudyPortal.PDECommon
{
    public class PDEJsonUtils
    {

        /*
         * Save JArray to the ClinRecord.
         * Note: This is only used for Repeating Rows
         * 
         * clinRecord = PDE ClinRecord / a container for all data within a form
         * jArray = JArray contain a list of JObject that are sent to StudyWorks.
         * IG = form section / IG value / database table        
         * 
         */
        public static void saveJArrayToClinRecord(PDEClinRecord clinRecord, JArray jArray, String IG)
        {
            int IGR = 1;
            foreach (JObject jObj in jArray)
            {
                saveJObjectToClinRecord(clinRecord, jObj, IG, IGR.ToString());

                IGR++;
            }
        }

        /*
         * Save JObject to the ClinRecord. 
         * Each property and its value of a JObject (JSON object) is added as an IT and value to the PDE ClinRecord.
         * 
         * clinRecord = PDE ClinRecord / a container for all data within a form
         * jObj = JObject / JSON that contain a set of name and value pairs that needs to be sent to StudyWorks
         * IG = form section / IG value / database table
         * IGR = optional IGR or repeat IG field, if not provided defaults to 0
         * 
         */
        public static void saveJObjectToClinRecord(PDEClinRecord clinRecord, JObject jObj, String IG, String IGR = "0")
        {
            foreach (JProperty jPro in jObj.Children())
            {
                clinRecord.addIT(jPro.Name, IG, (String)jPro.Value, IGR);
            }
        }

        /*
        * Converts a XElement to a JObject
        * This only convert a XElement attributes (excludes child elements) to a JObject properties and values
        * 
        * el = XElement to be converted to JObject
        * 
        */
        public static JObject XmlELtoJObject(XElement el)
        {
            JObject jObj = new JObject();

            foreach (XAttribute attr in el.Attributes())
            {
                jObj.Add(attr.Name.ToString(), new JValue(attr.Value));
            }

            return jObj;
        }
    }
}