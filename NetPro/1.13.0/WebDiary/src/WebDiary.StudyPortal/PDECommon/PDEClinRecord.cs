﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebDiary.SWAPI.SWData;
using WebDiary.Core.Helpers;

namespace WebDiary.StudyPortal
{
    public class PDEClinRecord
    {
        public ClinRecord clinRecord;

        public PDEClinRecord(ClinRecord baseClinRecord)
        {
            clinRecord = baseClinRecord;    
        }


        /*
         * Add a new IT field to a clinRecord
         * 
         * it = IT value / database column
         * ig = form section / IG value / database table
         * val = value to be added
         * igr = optional IGR or repeat IG field, if not provided defaults to 0
         * 
         */
        public void addIT(String it, String ig, String val, String igr = "0")
        {
            //If IG does not exist in the clinrecord, create a IG dictionary and add it to the clinRecord
            if (!clinRecord.iglist.ContainsKey(ig) || !clinRecord.iglist[ig].ContainsKey(igr))
            {
                Dictionary<string, List<ClinField>> newIG = new Dictionary<string, List<ClinField>>();
                List<ClinField> newIGList = new List<ClinField>(1);

                if (!clinRecord.iglist.ContainsKey(ig))
                {
                    newIG.Add(igr, newIGList);
                    clinRecord.iglist.Add(ig, newIG);
                }
                else
                {
                    clinRecord.iglist[ig].Add(igr, newIGList);
                }

            }

            //Create the clinField
            ClinField newClinField = PDECommonTools.generateClinField(it, val);

            //Add the clinField to the IG dictionary within the clinrecord
            clinRecord.iglist[ig][igr].Add(newClinField);
        }


        /*
         * Add a new systems variable field to a clinRecord
         * 
         * sysvar = system variable field that is to be set
         * val = value to be set
         * it = optional field if system variable value also maps to an IT. Will be an empty string if not provided
         * 
         */
        public void addSysVar(String sysvar, String val, String it = "")
        {
            ClinField newSysVar = new ClinField();

            newSysVar.value = val;
            newSysVar.sysvar = sysvar;
            newSysVar.krit = it;
            newSysVar.edit = false;

            clinRecord.sysvars.Add(newSysVar);
        }



        /*
         * Pull a hidden item from the checkbox survey responses and add it to the clinField
         * 
         * hiddenKey = hidden value to be pulled from the survey response data
         * ig = form section / table hidden value is to be added to.
         * surveyResponseData = survey data from checkbox
         * igr = optional field to specifcy IG repeat, will default to 0 if not provided
         * alternateIT = optional field to specify a different IT for the hidden value to be added under. If not specified, IT will equal the hiddenKey
         * 
         */
        public void addHiddenItem(String hiddenKey, String IG, SurveyResponseData surveyResponseData, String IGR = "0", String alternateIT = "")
        {
            if (surveyResponseData.HiddenItems.ContainsKey(hiddenKey))
            {
                String val = surveyResponseData.HiddenItems[hiddenKey];
                String IT;
                
                if(!alternateIT.Equals("")) {
                    IT = alternateIT;
                }
                else {
                    IT = hiddenKey;
                }

                addIT(IT, IG, val, IGR);
            }
        }


        /*
         * Removes an IT value from the clinrecord
         * 
         * ig = form section / table hidden value is to be removed from.
         * it = IT to be removed
         * igr = optional field to specifcy IG repeat, will default to 0 if not provided
         * 
         */
        public void removeIT(String ig, String it, String igr = "0")
        {
            if (clinRecord.iglist.ContainsKey(ig) && clinRecord.iglist[ig].ContainsKey(igr))
            {
                ClinField clinField = clinRecord.iglist[ig][igr].Find(delegate(ClinField field) { return field.krit.Equals(it); });

                if (clinField != null)
                {
                    clinRecord.iglist[ig][igr].Remove(clinField);

                    //If there is no more field in the current IG repeating row
                    if (clinRecord.iglist[ig][igr].Count == 0)
                    {   
                        //Remove repeating row
                        clinRecord.iglist[ig].Remove(igr);

                        //If there is no more row in the current IG
                        if (clinRecord.iglist[ig].Count == 0)
                        {
                            //Remove IG
                            clinRecord.iglist.Remove(ig);
                        }
                    }

                }
            }
        }

    }
}