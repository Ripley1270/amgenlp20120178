﻿<%@ Page Language="C#" AutoEventWireup="True"  validateRequest="false"  CodeBehind="change-password.aspx.cs" Inherits="WebDiary.StudyPortal.ChangePassword" MasterPageFile="~/includes/masters/Site.Master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="head">
    <script type="text/javascript">
        $(document).ready(function () {
            $('form').submit(function () {
                $('#vsErrors').hide();
            });
        });

        function comparePassword(oSrc, args) {
            if (Page_Validators[2].isvalid && Page_Validators[3].isvalid && Page_Validators[4].isvalid && args.Value != $('#txtPassword').val()) {
                args.IsValid = false;
                $('#txtPassword').val('');
                $('#txtConfirmPassword').val('');
                $('#txtPassword').focus();
            } else {
                args.IsValid = true;
            }
        }
                    
    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="mainContent" runat="server">
    <asp:ValidationSummary ID="vsErrors" CssClass="errors" runat="server" 
    DisplayMode="BulletList" EnableClientScript="False" />

    <pht:DivPanel ID="pnlForm" runat="server">

        <p>
            <asp:Label id="lblResetPswrdMsgBody" runat="server" 
                Text="<%$ Resources:Resource, ConfirmOldPswdAndSetPswd%>">
            </asp:Label>
        </p>
        
        <div class="form-row">
           <table>
            <tr>
                <td class="label">
                    <asp:Label id="lblOldPassword" runat="server" 
                        Text="<%$ Resources:Resource, OldPassword%>">
                    </asp:Label>
                </td>
                <td class="field">
                    <!--Prevent autocomplete for latest browser such as IE 11-->
                    <input id="prevent_autocomplete" name="prevent_autocomplete" type="password" class="hide"/>
                    <asp:TextBox ID="txtOldPassword" oncopy="return false;" onpaste="return false;" oncut="return false;"
                        runat="server" MaxLength="64" TextMode="Password" />
                </td>
            </tr>
            <tr class="form-empty-row">
                <td />
                <td>
                    <asp:RequiredFieldValidator ID="rfvOldPassword" runat="server" CssClass="nb-errors"
                        ControlToValidate="txtOldPassword" 
                        ErrorMessage="<%$ Resources:Resource, rfvOldPassword%>" 
                        Display="Dynamic" />
                    <asp:CustomValidator ID="validatePasswordValidator" runat="server" ErrorMessage="<%$ Resources:Resource, IncorrectOldPassword%>" ControlToValidate="txtOldPassword"
                    Display="Dynamic" CssClass="nb-errors" OnServerValidate="ValidatePassword"></asp:CustomValidator>
                                     

                </td>
            </tr>
            <tr>
                <td class="label">
                    <asp:Label ID="lblPassword" runat="server" 
                        Text="<%$ Resources:Resource, ResetNewPswrdLbl%>">
                    </asp:Label>
                </td>
                <td class="field">
                    <!--Prevent autocomplete for latest browser such as IE 11-->
                    <input id="prevent_autocomplete2" name="prevent_autocomplete2" type="password" class="hide"/>
                    <asp:TextBox ID="txtPassword" oncopy="return false;" onpaste="return false;" oncut="return false;"
                        runat="server" MaxLength="64" TextMode="Password" />
                </td>
            </tr>
            <tr class="form-empty-row">
                <td />
                <td>
                    <asp:RequiredFieldValidator ID="rfvPassword" runat="server" CssClass="nb-errors"
                        ControlToValidate="txtPassword" 
                        ErrorMessage="<%$ Resources:Resource, rfvNewPassword%>" 
                        Display="Dynamic" />                   
                    <asp:RegularExpressionValidator ID="PasswordLengthValidator" runat="server" 
                        ControlToValidate="txtPassword" CssClass="nb-errors" Display="Dynamic"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <asp:Label id="lblConfirmPassword" runat="server" 
                        Text="<%$ Resources:Resource, ConfirmPasswordLbl%>">
                    </asp:Label>
                </td>
                <td class="field">
                <!--Prevent autocomplete for latest browser such as IE 11-->
                <input id="prevent_autocomplete3" name="prevent_autocomplete3" type="password" class="hide"/>
                <asp:TextBox ID="txtConfirmPassword" oncopy="return false;" onpaste="return false;" oncut="return false;"
                    runat="server" MaxLength="64" TextMode="Password" />
                </td>
            </tr>
            <tr class="form-empty-row">
                <td />
                <td>
                    <asp:RequiredFieldValidator ID="rfvConfirmPassword" runat="server" CssClass="nb-errors"
                        ControlToValidate="txtConfirmPassword" 
                        ErrorMessage="<%$ Resources:Resource, rfvPasswordConfirm%>" 
                        Display="Dynamic"/>
                    <asp:CustomValidator ID="ComparePasswordsValidator" runat="server" ErrorMessage="<%$ Resources:Resource, PwdsDontMatch%>" ControlToValidate="txtConfirmPassword"
                    Display="Dynamic" CssClass="nb-errors" ClientValidationFunction="comparePassword"></asp:CustomValidator>
                </td>
            </tr>
           </table>
                  
        </div>
        <asp:Button ID="btnSubmit" Text="<%$ Resources:Resource, Submit%>" runat="server" OnClientClick="if (!Page_Validators[5].isvalid) return false;"/>

    </pht:DivPanel>

    <pht:DivPanel ID="pnlSuccess" Visible="False" runat="server">
        <asp:Literal ID="litConfirmScreen" runat="server"></asp:Literal>
    </pht:DivPanel>

</asp:Content>

