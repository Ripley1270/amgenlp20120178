﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.IO;
using System.Collections;
using System.Collections.Specialized;
using WebDiary.Core.Entities;
using WebDiary.Controls;
using WebDiary.Core.Helpers;
using WebDiary.Core.Repositories;
using WebDiary.SWAPI;
using WebDiary.SWAPI.SWData;
using WebDiary.WDAPI;
using log4net;
using System.Globalization;
using WebDiary.Core.Errors;
using WebDiary.Core.Constants;
using System.Web.Services;
using WebDiary.StudyPortal.Helpers;
using WebDiary.StudyPortal.MedicationModule;
namespace WebDiary.StudyPortal{
    public partial class SiteAccess : BasePage{
        private static readonly ILog logger = LogManager.GetLogger(typeof(SiteAccess));
        //private const string QUESTIONNAIRE_LIST = "questionnaire_list";
        //private const string EDIT_SUBJECT = "edit_subject";
        //private const string OFF_STUDY = "off_study";
        private const string SSA_BASE_URL = "ssa_base_url";
        private string site_language;
        private const string SUBJECTLIST_PARAM = "requestType=subject";
        private List<OtherRecordData> myRecordList;
        public SiteAccess(){
            try{
                using(WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString)){
                    db.ExecuteCommand("EXECUTE dbo.DeleteExpiredSessions");
                }
            } catch(System.Data.SqlClient.SqlException ex){
                string err = String.Format(CultureInfo.InvariantCulture, Resources.Resource.ErrorDB, ex.Message);
                logger.Error(err, ex);
            }
        }
        protected void Page_Load(object sender, EventArgs e){
            bool isAuthorizedBrowser = false;
            try{
                HttpBrowserCapabilities browser = Request.Browser;
                isAuthorizedBrowser = AuthorizedBrowser.CheckAuthorizedBrowser(StudyData.study.Name, browser.Browser, browser.MajorVersion);
                if(!isAuthorizedBrowser){
                    authBrowser.Value = "false";
                    lblUnauthorizedMessage.Visible = true;
                    string browserNameVersion = browser.Browser;
                    if(string.Equals(browserNameVersion, "Edge", StringComparison.OrdinalIgnoreCase))
                        browserNameVersion = browserNameVersion + "HTML" + " " + browser.MajorVersion;
                    else
                        browserNameVersion = browserNameVersion + " " + browser.MajorVersion;
                    lblUnauthorizedMessage.Text = String.Format(CultureInfo.CurrentCulture, Resources.Resource.UnauthorizedBrowserMsg.ToString(CultureInfo.CurrentCulture), browserNameVersion);
                    List<string> authorizedBrowser = AuthorizedBrowser.GetAuthorizedBrowser2(StudyData.study.Name);
                    string browserList = AuthorizedBrowser.GenerateBrowserList(authorizedBrowser);
                    authBrowserArea.InnerHtml = browserList;
                    logger.Info(String.Format(CultureInfo.InvariantCulture, "Use of {0} is not supported. Browser Name: {1}, Major version:{2}, userAgent: {3}", browserNameVersion, browser.Browser, browser.MajorVersion, Request.UserAgent));
                }
            } catch(Exception ex){
                logger.Error(ex.Message, ex);
            }
            if(Session["SubmitResult"] != null){
                if(!string.IsNullOrEmpty(Session["SubmitResult"].ToString())){
                    notificationScriptID.Text = "<script type=\"text/javascript\">notification.text = '" + Session["SubmitResult"] + "';</script>";
                    Session["SubmitResult"] = null;
                }
            }
            site_language = SessionHelper.GetSiteLanguageKey();
            if(!IsPostBack){
                //In case we back out of survey frame
                Session[WDConstants.FramePostUrl] = null;
                Session[WDConstants.FrameRedirectUrl] = null;
                Session[WDConstants.FrameLeaveUrl] = null;

                //Check is session is still active.
                //Check NetPRO session
                if(Session[WDConstants.VarRequestUrl] != null){
                    //Check StudyWork Session
                    if(!SessionHelper.CheckSwSession(Session[WDConstants.VarRequestUrl] as String)){
                        Response.Redirect(Session[SSA_BASE_URL] + SWAPIConstants.SessionTimeoutPage, false);
                        return;
                    }
                } else{
                    // Construct SW Return URL
                    try{
                        using(WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString)){
                            string returnUrl = StudyData.study.SsaBaseUrl + SWAPIConstants.SessionTimeoutPage;
                            Response.Redirect(returnUrl, false);
                        }
                    } catch(Exception ex){
                        string err = String.Format(CultureInfo.InvariantCulture, Resources.Resource.ErrorDB, ex.Message);
                        logger.Error(err, ex);
                        Session[WDError.ErrorMessageKey] = err;
                        Response.Redirect(WDError.Path, true);
                    }
                    return;
                }
                try{
                    //Load languages
                    //Stream studyStream = null;
                    //try
                    //{
                    //    studyStream = File.Open(Server.MapPath("studyconfig/study.xml"), FileMode.Open);
                    //    XDocument xdoc = XDocument.Load(studyStream);
                    //    XElement sls = xdoc.Element("study").Element("SiteLanguages");
                    //    var slsList = sls.Elements("SiteLanguage");
                    //    if (slsList.Count() > 1)
                    //    {
                    //        foreach (var slsItem in slsList)
                    //        {
                    //            selectLanguageDdl.Items.Add(new ListItem(slsItem.Element("LanguageDisplayName").Value, slsItem.Element("LanguageCode").Value));
                    //        }
                    //    }
                    //    else
                    //        changeLanguageBlock.Visible = false;
                    //}
                    //catch (IOException ex)
                    //{
                    //    string err = String.Format(CultureInfo.InvariantCulture, Resources.Resource.ErrorProcessingUrRequest, ex.Message);
                    //    logger.Error(err);
                    //    Session[WDError.ErrorMessageKey] = err;
                    //    Response.Redirect(WDError.Path, true);
                    //}
                    //finally
                    //{
                    //    studyStream.Close();
                    //}

                    //Get Subject information and Site Gateway questionnaires.
                    using(WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString)){
                        //PDE Disable timetravel in production
                        VersionsRepository vr = new VersionsRepository(db);
                        Versions ver = vr.GetSingleOrDefault();
                        DTPanel.Visible = ver.Environment != 0;
                        Study study = StudyData.study;
                        bool lfStudy = !string.IsNullOrEmpty(study.StudyVersions.Lf_Hash);
                        PanelLF.Visible = !string.IsNullOrEmpty(StudyData.version.LFHash);
                        Session[SSA_BASE_URL] = study.SsaBaseUrl;
                        string krPT = Session[WDConstants.VarKrpt].ToString();
                        WDAPIObject wd = new WDAPIObject(study.StudyWorksUsername, study.StudyWorksPassword, study.Name, study.StudyWorksBaseUrl);
                        SubjectData sdata = wd.GetSubjectData(Session[WDConstants.VarKrpt].ToString(), new ArrayList {
                            SWAPIConstants.ColumnPatientId,
                            SWAPIConstants.ColumnPatientKrPhase,
                            SWAPIConstants.ColumnPatientPhaseDate
                        });
                        if(sdata != null){
                            patientIDLbl.Text = sdata.data.Patientid;
                            Session[WDConstants.VarPtPatientId] = sdata.data.Patientid;

                            //Get current phase and phase date.
                            Session[WDConstants.VarSuPhaseAtEntry] = sdata.data.Krphase;

                            //Fix krphasedate if necessary.
                            if(sdata.data.Phasedate.Contains('.')){
                                string[] str = sdata.data.Phasedate.Split('.');
                                string strnew = DateTimeHelper.ChangeFormat(str[0], SWAPIConstants.SwWeirdDateTimeFormat, SWAPIConstants.SwDateTimeFormat);
                                Session[WDConstants.VarSuPhaseStartDate] = strnew;
                            } else{
                                Session[WDConstants.VarSuPhaseStartDate] = sdata.data.Phasedate;
                            }
                        } else{
                            string err = Resources.Resource.ErrorSWDB;
                            logger.Error(err);
                            Session[WDError.ErrorMessageKey] = err;
                            Response.Redirect(WDError.Path, true);
                            return;
                        }
                        SubjectRepository subjectRep = new SubjectRepository(db);
                        Subject subject = subjectRep.FindByKrptAndStudy(krPT, study.Id);
                        ClinData clinData;
                        if(subject.Email.Contains(WDConstants.NoEmailToken) &&
                           !lfStudy)// LogPad 5.x combo study
                        {
                            //Get date from the Subject's Assignment report.
                            clinData = wd.GetClinDataByNumberRange(krPT, SWAPIConstants.DefaultAssignmentSu, -1, new ArrayList {
                                SWAPIConstants.TableColumnAssignmentSuSigOrig,
                                SWAPIConstants.TableColumnAssignmentSuSigIg
                            });
                            patientInfoBlock.Visible = false;
                        } else{
                            clinData = wd.GetClinDataByNumberRange(krPT, SWAPIConstants.DefaultAssignmentSu, -1, new ArrayList {
                                SWAPIConstants.TableColumnAssignmentSuSigOrig,
                                SWAPIConstants.TableColumnAssignmentSuSigIg,
                                SWAPIConstants.TableColumnSubjectEmailId,
                                SWAPIConstants.TableColumnSubjectLanguage
                            });
                            Session[WDConstants.VarSubjectEmailId] = clinData.datatable[0][2];
                            preferredLangValueLbl.Text = wd.GetLanguageNameFromCode(clinData.datatable[0][3]);
                            Session[WDConstants.VarLanguage] = preferredLangValueLbl.Text;
                        }
                        Session[WDConstants.VarSuSigOrig] = clinData.datatable[0][0];
                        string sigprev = clinData.datatable[0][1];
                        Session[WDConstants.VarSuSigPrev] = sigprev ?? (string)Session[WDConstants.VarSuSigOrig];
                        string lfStudyUrl = study.WebDiaryStudyBaseUrl.Trim().ToString(CultureInfo.InvariantCulture);
                        int startPosition = lfStudyUrl.IndexOf("//", StringComparison.Ordinal) + "//".Length;
                        int endPosition = lfStudyUrl.IndexOf(".", StringComparison.Ordinal);
                        string lfMainStudyUrl = lfStudyUrl.Substring(startPosition, endPosition - startPosition);
                        LFStudyName.Text = lfMainStudyUrl.ToString(CultureInfo.InvariantCulture);

                        //Set Subject LogPad LF Info
                        LFStudyUrlValueLabel.Text = (study.WebDiaryStudyBaseUrl.EndsWith("/", StringComparison.OrdinalIgnoreCase) ? study.WebDiaryStudyBaseUrl : study.WebDiaryStudyBaseUrl + "/") + "LF/";
                        if(subject.Pin.HasValue)
                            LFPinValueLabel.Text = subject.Pin.ToString();

                        //QR code
                        if(lfStudy){
                            string subjectLanguage = WDAPIObject.JavaLocaleToDotNetCulture(clinData.datatable[0][3]);
                            string imgFile = QRCodeHelper.GetQRCode(lfMainStudyUrl, subject.Pin.Value, subjectLanguage);
                            Image qrcodeImg = new Image {
                                ImageUrl = imgFile
                            };
                            qrcode.Controls.Add(qrcodeImg);
                        }

                        //Get Subject's time zone id.
                        if(subject != null){
                            Session[WDConstants.VarSuTimeZoneValue] = subject.TimeZone.ToString(CultureInfo.InvariantCulture);
                            Session[DateTimeHelper.TimeZoneId] = subject.TimeZone;
                            int pos = subject.Email.IndexOf('@');
                            string hiddenEmail = new string('*', pos);
                            hiddenEmail += subject.Email.Substring(pos);
                            emailAddrValueLbl.Text = hiddenEmail;
                            //PDE - set time to timetravel widget
                            txtTimeTravel.Text = DateTimeHelper.GetLocalTime().ToString(SWAPIConstants.DateTimeTravelFormat, CultureInfo.InvariantCulture);
                            //SurveyScheduler.GetSubjectLocalTime(subject);
                        }
                        string currentPhase = sdata.data.Krphase;
                        //PDE - list of surveys to hide
                        List<String> hiddenSurveyList = new List<String>();
                        //Flush the hidden survey list and re-calculate
                        hiddenSurveyList.Clear();
                        /*
                         *  PDE - Add custom schedule logic to hide diaries
                         *  
                         *  Add KRSU values to the hidden survey list that you do not want to appear on the gateway
                         *  
                         *  ex: hiddenSurveyList.Add(PDETools.KRSU_BRFreq);
                         */
                        Boolean hideDropDown = false;
                        ArrayList PDEArgs = new ArrayList();
                        String krpt = subject.KrPT;
                        PDEArgs.Add(HttpContext.Current.Server.UrlEncode(krpt));
                        //Add device ID as NetPRO
                        PDEArgs.Add("NetPRO");
                        //Add current local date time
                        DateTime now = DateTimeHelper.GetLocalTime();
                        PDEArgs.Add(now.ToString(SWAPIConstants.SwDateTimeFormat, CultureInfo.CurrentCulture));
                        //Add local timezone offset from UTC in milliseconds 
                        PDEArgs.Add(Session[WDConstants.VarSuTimeZoneValue]);
                        String otherRecordRawList = wd.GetData("PDE_NetPro_GetOtherRecordList", PDEArgs).response;
                        //TODO : this gave issues before but seems to be okay now
                        // if this breaks again, I think it has something to do with the PDEArgs but not sure what
                        String ongoingHeadacheInfo = wd.GetData("PDE_NetProGetOngoingHeadache", PDEArgs).response;
                        String headacheID = PDECommonTools.getStringValue("headacheID", ongoingHeadacheInfo);
//                        String headacheID = "";
                        String[] splitData = otherRecordRawList.Split('|');
                        Int16 numRecords = Convert.ToInt16(splitData[0]);
                        //String lpStartExist = splitData[splitData.Length - 2];
//                        PDEArgs = new ArrayList {
//                            HttpContext.Current.Server.UrlEncode(krpt)
//                        };
                        //String lastDiaryInfo = wd.GetData("PDE_NetPro_LastDiaryTime", PDEArgs).response;

                        // disable Medication Selection update if deactivated
                        if(currentPhase.Equals(PDETools.PHASE_TERMINATION) ||
                           currentPhase.Equals(PDETools.PHASE_PRETERM)){
                            hiddenSurveyList.Add(PDETools.MedUpdateKRSU);
                            if(numRecords == 0){
                                hiddenSurveyList.Add(PDETools.MedSelectionKRSU);
                                hideDropDown = true;
                            }
                        }
                        if(!currentPhase.Equals(PDETools.PHASE_DORMANT) || headacheID == null ||headacheID.Equals("")){
                            hiddenSurveyList.Add(PDETools.OngoingHeadacheResolutionKRSU);
                        }

                        //Allow other med resolve to be completed after ELPU
                        if(wd.IsSubjectEnabled(krPT) || true){
                            //Get questionnaires.
                            var siteQuestionnaireList = from s in db.StudySurveys
                                                        where s.StudyId == study.Id && s.FormType == SWAPIConstants.FormTypeSiteSubmit && hiddenSurveyList.Contains(s.KRSU) == false
                                                        orderby s.SortOrder
                                                        select new {
                                                            DisplayName = s.DisplayName,//(String)GetGlobalResourceObject("PDEResource", s.KRSU),//s.DisplayName,
                                                            ActivationURL = s.ActivationURL,
                                                            TriggeredPhase = s.TriggeredPhase
                                                        };

                            //Bind questionnaires to data grid.
                            if(!siteQuestionnaireList.Any())
                                NoQuestionnairesLabel.Visible = true;
                            questionnaireGridView.DataSource = siteQuestionnaireList;
                            questionnaireGridView.DataBind();
                            if(!isAuthorizedBrowser)
                                questionnaireGridView.Enabled = false;
                            ActivationEmailTable.Visible = !subject.IsApproved;

                            //Set drop down list
                            if(!hideDropDown){
                                OtherRecordList.Visible = true;
                                myRecordList = OtherRecordData.ProcessDoseDiaryData(otherRecordRawList);//DoseDiaryData.generateTestList();
                                List<String> medicationIDList = new List<String>();
                                for(int i = 0; i < myRecordList.Count; i++){
                                    medicationIDList.Add(myRecordList[i].MedicationID.ToString());
                                }
                                OtherRecordList.DataSource = myRecordList;
                                MedIDList.DataSource = medicationIDList;
                                OtherRecordList.DataBind();
                                MedIDList.DataBind();
                                //default value
                                OtherRecordList.Items.Insert(0, myRecordList.Count == 0 ? new ListItem(Resources.PDEResource.DropDownNoRecords, "") : new ListItem(Resources.PDEResource.DropDownChooseRecord, ""));
                            } else{
                                OtherRecordList.Visible = false;
                            }
                        }
                        /*else
                        {
                            endStudyPnl.Visible = false;
                            ActivationEmailTable.Visible = false;
                            NoQuestionnairesLabel.Visible = true;
                            ParticipationEndedLabel.Visible = true;

                            if (hideDropDown)
                            {
                                OtherRecordList.Visible = false;
                            }
                        }*/
                        if(!wd.IsSubjectEnabled(krPT)){
                            endStudyPnl.Visible = false;
                            ActivationEmailTable.Visible = false;
                            //NoQuestionnairesLabel.Visible = true;
                            ParticipationEndedLabel.Visible = true;
                        }

                        //if Lf_Hash != null, activation is on Logpad App, which doesn't use email
                        if(lfStudy){
                            ActivationEmailTable.Visible = false;
                            emailAddrRow.Visible = false;
                        }
                        CodeList languages = wd.GetLanguages();
                        if(languages != null){
                            int counter = 0;
                            int selectedIndex = 0;
                            string defaultLanguage = "en-US";
                            if(!string.IsNullOrEmpty(Session[site_language] as string)){
                                defaultLanguage = Session[site_language].ToString();
                            } else if(Request.Cookies[site_language] != null &&
                                      !string.IsNullOrEmpty(Request.Cookies[site_language].Value)){
                                defaultLanguage = Request.Cookies[site_language].Value;
                            }
                            foreach(CodeList.CodeListItem item in languages.list){
                                SiteLanguagesList.Items.Add(new ListItem(item.decode, item.code));
                                if(WDAPIObject.JavaLocaleToDotNetCulture(item.code).Contains(defaultLanguage))
                                    selectedIndex = counter;
                                counter++;
                            }
                            SiteLanguagesList.SelectedIndex = selectedIndex;
                        }

                        //Only if Developer mode show screen shot checkbox
                        if(StudyData.version.ProductEnvironment == Core.Helpers.Version.EnvironmentDeveloper){
                            pnlScreenShotContainer.Visible = true;
                            string screenShot = Session[WDConstants.VarScreenShot] as string ?? string.Empty;
                            if(!string.IsNullOrEmpty(screenShot) &&
                               screenShot == "true")
                                chkScreenShot.Checked = true;
                        } else
                            pnlScreenShotContainer.Visible = false;

                        //Try to send setup code to StudyWorks again
                        if(!lfStudy ||
                           !subject.Pin.HasValue){
                            return;
                        }
                        try{
                            SimpleResponse setupCodeResponse = wd.GetData("LPA_getSetupCode", new ArrayList {
                                krPT
                            });
                            try{
                                if(string.IsNullOrEmpty(setupCodeResponse.response)){
                                    //SimpleResponse swapiResponse = wd.GetData("LPA_submitSetupCode", new ArrayList() {
                                    //    krPT,
                                    //    subject.Pin.Value
                                    //});
                                }
                            } catch(Exception ex){
                                logger.Info(String.Format(CultureInfo.InvariantCulture, "Error occured while sending setup code to StudyWorks. But the error will be supressed to allow normal process of NetPRO. {0}", ex.Message), ex);
                            }
                        } catch(Exception ex){
                            logger.Info(String.Format(CultureInfo.InvariantCulture, "Error occured while checking setup code in StudyWorks. But the error will be supressed to allow normal process of NetPRO. {0}", ex.Message), ex);
                        }
                    }
                } catch(NetProException npex){
                    string err = String.Format(CultureInfo.InvariantCulture, Resources.Resource.ErrorQRCode, npex.Message);
                    logger.Error(err, npex);
                    Session[WDError.ErrorMessageKey] = err;
                    Response.Redirect(WDError.Path, true);
                } catch(Exception ex){
                    string err = String.Format(CultureInfo.InvariantCulture, Resources.Resource.ErrorDB, ex.Message);
                    logger.Error(err, ex);
                    Session[WDError.ErrorMessageKey] = err;
                    Response.Redirect(WDError.Path, true);
                }
            }
        }
        protected void QuestionnaireGridView_RowCommand(Object sender, CommandEventArgs e){
            try{
                if(e.CommandName != "launch"){
                    return;
                }
                WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString);
                SurveyRepository surveyRep = new SurveyRepository(db);
                string url = e.CommandArgument.ToString().Trim();
                StudySurveys survey = surveyRep.FindByActivationUrl(url);
                SurveyData surveyData = SurveyDataHelper.CreateSurveyData(survey.Id, survey.TriggeredPhase);
                surveyData.Sigid = "NP." + WDAPIObject.GenerateSignatureId();
                NameValueCollection queryStrings = SurveyDataHelper.SaveSurveyData(surveyData, db);
                if(!survey.IsExternal){
                    /*
                         *  PDE - pass in hidden items to a survey here
                         */
                    String subjectID = HttpContext.Current.Session[WDConstants.VarPtPatientId].ToString();
                    queryStrings.Add("subjectID", subjectID);
                    String siteID = HttpContext.Current.Session[WDConstants.VarSiteCode].ToString();
                    queryStrings.Add("siteID", siteID);
                    if(survey.FormType.Equals(SWAPIConstants.FormTypeSiteSubmit)){
                        String siteUser = HttpContext.Current.Session[WDConstants.VarUserId].ToString();
                        queryStrings.Add("siteName", siteUser);
                    }
                    /* PDE - SWAPI call example
                         * //Get SWAPI hook
                         * StudyRepository sr = new StudyRepository(db);
                         * Study study = sr.FindByName(Session[WDConstants.VarProtocol].ToString());
                         * SubjectRepository subjRep = new SubjectRepository(db);
                         * Subject subject = subjRep.FindByKrptAndStudy(surveyData.Krpt, study.Id);
                         * WDAPIObject wd = new WDAPIObject(study.StudyWorksUsername, study.StudyWorksPassword, study.Name, study.StudyWorksBaseUrl);
                         * 
                         * 
                         * String krpt = subject.KrPT.ToString();
                         * //Setup arguments for stored procedure
                         * ArrayList PDEArgs = PDETools.setupSwapiArgs(krpt, Session[WDConstants.VarSuTimeZoneValue].ToString());
                         * //Run stored procedure and get results
                         * String launchSurveyData = wd.GetData("PDE_NetProBPInfoSQLV1", PDEArgs).response;
                         */
                    StudyRepository sr = new StudyRepository(db);
                    Study study = sr.FindByName(Session[WDConstants.VarProtocol].ToString());
                    SubjectRepository subjRep = new SubjectRepository(db);
                    Subject subject = subjRep.FindByKrptAndStudy(surveyData.Krpt, study.Id);
                    WDAPIObject wd = new WDAPIObject(study.StudyWorksUsername, study.StudyWorksPassword, study.Name, study.StudyWorksBaseUrl);
                    String krpt = subject.KrPT;
                    //Setup arguments for stored procedure
                    ArrayList PDEArgs = new ArrayList();                    
                    if(OtherRecordList.SelectedIndex == 0 &&
                       survey.KRSU.Equals(PDETools.MedSelectionKRSU)){
                        selectionError.Visible = true;
                    } else{
                        SubjectData sdata = wd.GetSubjectData(Session[WDConstants.VarKrpt].ToString(), new ArrayList {
                            SWAPIConstants.ColumnPatientKrPhase
                        });
                        if(survey.KRSU.Equals(PDETools.KRSU_VISIT_CONF)){
                            String phase = sdata.data.Krphase;
                            queryStrings.Add("phase", phase);
                            PDEArgs = PDECommonTools.setupSwapiArgs(krpt, Session[WDConstants.VarSuTimeZoneValue].ToString());
                            String launchSurveyData = wd.GetData("PDE_NetPro_LaunchSurveyDataSQLV1", PDEArgs).response;
                            String studyArm = PDETools.GetStudyArm(launchSurveyData);
                            queryStrings.Add("studyArm", studyArm);
                            DateTime? lastBPISF = PDETools.getLastBPIDT(launchSurveyData);
                            DateTime? randomizationDT = PDETools.getRandomizationDT(launchSurveyData);
                            String rescreeningCount = PDETools.GetRescreeningCount(launchSurveyData);
                            queryStrings.Add("rescreeningCount", rescreeningCount);
                            DateTime currentDT = SurveyScheduler.GetSubjectLocalTime(subject);
                            queryStrings.Add("maxDate", currentDT.ToString());
                            DateTime sevenDaysAgo = SurveyScheduler.GetSubjectLocalTime(subject).AddDays(-7);
                            //Randomization Date picker - min boundary is either current date - 7 or last BPISF date, whichever is more recent
                            if(lastBPISF.HasValue){
                                queryStrings.Add("minDate", lastBPISF.Value.CompareTo(sevenDaysAgo) > 0 ? lastBPISF.ToString() : sevenDaysAgo.ToString());
                            } else{
                                queryStrings.Add("minDate", sevenDaysAgo.ToString());
                            }
                            if(randomizationDT.HasValue){
                                int studyDay = (currentDT.Date - randomizationDT.Value).Days + 1;
                                queryStrings.Add("studyDay", studyDay.ToString());
                            }
                        } else if(survey.KRSU.Equals(PDETools.MedSelectionKRSU) ||
                                  survey.KRSU.Equals(PDETools.MedUpdateKRSU)){
                            String serverPath = Server.MapPath("~/includes/scripts");

                            //Check to see if master medication list and work medication list need to be generated
                            DateTime masterMedModTime;
                            Boolean createMasterMedFile = false;
                            if(File.Exists(serverPath + "\\masterMedicationList." + "en_US" + ".js")){
                                masterMedModTime = File.GetLastWriteTimeUtc(serverPath + "\\masterMedicationList." + "en_US" + ".js");//subject.Language + ".js");
                            } else{
                                masterMedModTime = new DateTime(1900, 01, 01, 01, 01, 0, 0);//file doesn't exist, send date in past so medlist is returned
                                createMasterMedFile = true;
                            }
                            PDEArgs.Add(masterMedModTime.ToString());
                            PDEArgs.Add("en_US");
                            //PDEArgs.Add(subject.Language);
                            String masterListResult = wd.GetData("PDE_NetPro_GetMasterList", PDEArgs).response;
                            String numMasterMeds = Medication.getNumMeds(masterListResult);

                            //String[] splitMasterList = masterListResult.Split('|');

                            //If file doesn't exist or master list is out of date, regenerate the file
                            if(createMasterMedFile || !numMasterMeds.Equals(PDETools.STD_NO)){
                                Medication.createMasterListV3(serverPath, masterListResult, "en_US");
                            } else{
                                Medication.createMasterListV3(serverPath, masterListResult, "en_US");
                            }
                            //Boolean createWorkFile = false;
                            DateTime workMedModTime = File.Exists(serverPath + "\\masterWorkList.en_US.js") ? File.GetLastWriteTimeUtc(serverPath + "\\masterWorkList.en_US.js") : new DateTime(1900, 01, 01, 01, 01, 0, 0);
                            PDEArgs = new ArrayList {
                                workMedModTime.ToString()
                            };
                            String workListResult = wd.GetData("PDE_NetPro_GetWorkList", PDEArgs).response;
                            //String numMeds = Medication.getNumMeds(workListResult);

                            //String[] workListSplit = workListResult.Split('|');
                            //If file doesnt't exist or if file is out of date, create new file
                            //if(createWorkFile || !numMeds.Equals(PDETools.STD_NO))//!workListSplit[0].Equals(PDETools.STD_NO))
                            //{
                            //    Medication.createWorkListV2(serverPath, workListResult);
                            //}
                            //Always create the WorkFile
                            Medication.createWorkListV2(serverPath, workListResult);
                            queryStrings.Add("protocol", "1");
                            String phase = sdata.data.Krphase;
                            queryStrings.Add("phase", phase);
                            if(survey.KRSU.Equals(PDETools.MedUpdateKRSU)){
                                String timezone = Session[WDConstants.VarSuTimeZoneValue].ToString();
                                queryStrings = LaunchMedicationHelper.setupMedicationModuleQueryStrings(queryStrings, survey.KRSU, wd, timezone);
                            } else if(survey.KRSU.Equals(PDETools.MedSelectionKRSU)){
                                String orderRecordSelect = OtherRecordList.SelectedValue.Split('-')[1];

                                //Other Record Date Time
                                String recordDT = orderRecordSelect.Trim();//selectedDiary.ReportDate;
                                //                                    String recordDT = "12 Oct 2017 11:17 PM";//selectedDiary.ReportDate;

                                // Added a hidden list of headache IDs as the recordlist is memory may have an incorrect cached version
                                String medicationID = MedIDList.Items[OtherRecordList.SelectedIndex - 1].Text;
                                String timezone = Session[WDConstants.VarSuTimeZoneValue].ToString();
                                queryStrings = LaunchMedicationHelper.setupMedicationModuleQueryStrings(queryStrings, survey.KRSU, wd, timezone, recordDT, OtherRecordList.SelectedIndex + "", medicationID);
                            }
                            queryStrings.Add("language", subject.Language);
                        } else if(survey.KRSU.Equals(PDETools.OngoingHeadacheResolutionKRSU)){
                            PDEArgs = PDECommonTools.setupSwapiArgs(krpt, Session[WDConstants.VarSuTimeZoneValue].ToString());
                            String ongoingHeadacheInfo = wd.GetData("PDE_NetProGetOngoingHeadache", PDEArgs).response;
                            String headacheID = PDECommonTools.getStringValue("headacheID", ongoingHeadacheInfo);
                            String headacheStartDate = PDECommonTools.getStringValue("startDate", ongoingHeadacheInfo);
                            queryStrings.Add("HeadacheID", headacheID);
                            queryStrings.Add("headacheBeginDate", headacheStartDate);
                        }
                        url += "&" + queryStrings;
                        HttpContext.Current.Session[WDConstants.FrameRedirectUrl] = url;
                        Response.Redirect("survey.aspx?psd_id=" + surveyData.Id, true);
                    }
                } else
                    ExternalLauncher.LaunchExternalSurvey(Request, Response, survey, surveyData);
            } catch(System.Data.SqlClient.SqlException ex){
                string err = String.Format(CultureInfo.InvariantCulture, Resources.Resource.ErrorDB, ex.Message);
                logger.Error(err, ex);
                Session[WDError.ErrorMessageKey] = err;
                Response.Redirect(WDError.Path, true);
            }
        }
        protected void EditSubjectBtn_Click(Object sender, EventArgs e){
            try{
                WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString);
                SurveyRepository surveyRep = new SurveyRepository(db);
                IQueryable<StudySurveys> editSurvey = surveyRep.FindByStudyIdAndFormType(StudyData.study.Id, SWAPIConstants.FormTypeEditSubject);
                StudySurveys editSubject = editSurvey.FirstOrDefault();
                if(editSubject == null){
                    logger.Fatal("Edit survey is not found in the database. NetPRO is not configured correctly.");
                    Session[WDError.ErrorMessageKey] = String.Format(CultureInfo.InvariantCulture, Resources.Resource.AppErrHasOccurred, "<a href=\"https://mystudy.phtstudy.com/ssa/pages/contact_us/\">", "</a>");
                    Response.Redirect(WDError.Path, false);
                    return;
                }
                SurveyData surveyData = SurveyDataHelper.CreateSurveyData(editSubject.Id, editSubject.TriggeredPhase);
                surveyData.Sigorig = Session[WDConstants.VarSuSigOrig].ToString();
                surveyData.SigPrev = Session[WDConstants.VarSuSigPrev].ToString();
                NameValueCollection queryStrings = SurveyDataHelper.SaveSurveyData(surveyData, db);
                if(emailChk.Checked){
                    queryStrings.Add(WDConstants.VarSubjectEmailId, (string)Session[WDConstants.VarSubjectEmailId]);
                }
                if(languageChk.Checked){
                    queryStrings.Add(WDConstants.VarLanguage, (string)Session[WDConstants.VarLanguage]);
                }
                string url = editSubject.ActivationURL;
                url += "&" + queryStrings;
                HttpContext.Current.Session[WDConstants.FrameRedirectUrl] = url;
                Response.Redirect("survey.aspx?psd_id=" + surveyData.Id, true);
            } catch(System.Data.SqlClient.SqlException ex){
                string err = String.Format(CultureInfo.InvariantCulture, Resources.Resource.ErrorDB, ex.Message);
                logger.Error(err, ex);
                Session[WDError.ErrorMessageKey] = err;
                Response.Redirect(WDError.Path, true);
            }
        }
        protected void EndStudyParticipationBtn_Click(Object sender, EventArgs e){
            try{
                WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString);
                SurveyRepository surveyRep = new SurveyRepository(db);
                var offStudySurvey = surveyRep.FindByStudyIdAndFormType(StudyData.study.Id, SWAPIConstants.FormTypeOffStudy);
                StudySurveys offStudy = offStudySurvey.FirstOrDefault();
                if(offStudy == null){
                    logger.Fatal("End NetPRO use survey is not found in the database. NetPRO is not configured correctly.");
                    Session[WDError.ErrorMessageKey] = String.Format(CultureInfo.InvariantCulture, Resources.Resource.AppErrHasOccurred, "<a href=\"https://mystudy.phtstudy.com/ssa/pages/contact_us/\">", "</a>");
                    Response.Redirect(WDError.Path, false);
                    return;
                }
                SurveyData surveyData = SurveyDataHelper.CreateSurveyData(offStudy.Id, offStudy.TriggeredPhase);
                NameValueCollection queryStrings = SurveyDataHelper.SaveSurveyData(surveyData, db);
                string url = offStudy.ActivationURL;
                url += "&" + queryStrings;

                //Add last_report_date for End Study Participation Survey
                string krPT = Session[WDConstants.VarKrpt] as string ?? string.Empty;
                if(!string.IsNullOrEmpty(krPT)){
                    string daysFromToday = GetLastReportDate(krPT, StudyData.study.Id);
                    if(!string.IsNullOrEmpty(daysFromToday))
                        url += "&last_report_date=" + daysFromToday;
                }
                HttpContext.Current.Session[WDConstants.FrameRedirectUrl] = url;
                Response.Redirect("survey.aspx?psd_id=" + surveyData.Id, true);
            } catch(System.Data.SqlClient.SqlException ex){
                string err = String.Format(CultureInfo.InvariantCulture, Resources.Resource.ErrorDB, ex.Message);
                logger.Error(err, ex);
                Session[WDError.ErrorMessageKey] = err;
                Response.Redirect(WDError.Path, true);
            }
        }
        protected void DataSummariesLnk_Click(object sender, EventArgs e){
            //Check is session is still active.
            if(!SessionHelper.CheckSwSession(Session[WDConstants.VarRequestUrl] as String)){
                Response.Redirect(Session[SSA_BASE_URL] + "/ssa", false);
                return;
            }
            string test1 = Session[WDConstants.VarSwReturnUrl] as string;
            logger.Debug(test1);
            Response.Redirect(test1, true);
        }
        protected void ChooseSubjectLnk_Click(object sender, EventArgs e){
            //Check if session is still active.
            if(!SessionHelper.CheckSwSession(Session[WDConstants.VarRequestUrl] as String)){
                Response.Redirect(Session[SSA_BASE_URL] + "/ssa", false);
                return;
            }

            //Redirect to choose another subject
            Response.Redirect(Session[WDConstants.VarSwReturnUrl] + "&" + SUBJECTLIST_PARAM, true);
        }
        [WebMethod]
        public static bool SendActivationEmail(){
            try{
                using(WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString)){
                    SurveyEmailScheduleRepository scheduleRep = new SurveyEmailScheduleRepository(db);
                    SubjectRepository subjectRep = new SubjectRepository(db);
                    Study study = StudyData.study;
                    Subject subject = subjectRep.FindByKrptAndStudy(HttpContext.Current.Session[WDConstants.VarKrpt].ToString(), study.Id);
                    var schedule = scheduleRep.FindBySurveyIdAndSubjectId((int)WDConstants.EmailType.Activation, subject.Id);
                    WDAPIObject wd = new WDAPIObject(study.StudyWorksUsername, study.StudyWorksPassword, study.Name, study.StudyWorksBaseUrl);
                    int surveyId = (int)WDConstants.EmailType.Activation;
                    if(schedule != null){
                        logger.Error(String.Format(CultureInfo.InvariantCulture, "FAIL: subject GUID: {0}, surveyId: {1}, subject krpt: {2}, study name: {3}, study ID in NPDB: {4}, email info: {5}, scheduled date time in UTC: {6}, logging time stamp in UTC: {7}. Sending activation email failed, a record for activation email already exists in SURVEY_EMAIL_SCHEDULE table, scheduling service might be down.", subject.Id.ToString(), surveyId, subject.KrPT, StudyData.study.Name, StudyData.study.Id, "activation", schedule.ScheduleDT, DateTime.UtcNow));
                    } else if(!wd.IsSubjectEnabled(subject.KrPT))
                        logger.Error(String.Format(CultureInfo.InvariantCulture, "FAIL: subject GUID: {0}, subject krpt: {1}, study name: {2}, study ID in NPDB: {3}, email info: {4}, logging time stamp in UTC: {5}. Sending activation email failed, subject is not enabled.", subject.Id.ToString(), subject.KrPT, StudyData.study.Name, StudyData.study.Id, "activation", DateTime.UtcNow));
                    else{
                        schedule = new SurveyEmailSchedule {
                            SurveyId = surveyId,
                            SubjectId = subject.Id,
                            ScheduleDT = DateTime.UtcNow,
                        };
                        db.SurveyEmailSchedule.InsertOnSubmit(schedule);
                        //New token and expiration
                        subject.ActivationToken = Guid.NewGuid();
                        subject.ActivationTokenExpirationDate = DateTime.UtcNow.AddDays(study.TokenExpiration);
                        db.SubmitChanges();
                        logger.Info(String.Format(CultureInfo.InvariantCulture, "SUCCESS: subject GUID: {0}, surveyId: {1}, subject krpt: {2}, StudyWorks timeZone: {3}, study name: {4}, study ID in NPDB: {5}, email info: {6}, scheduled date time in UTC: {7}, logging time stamp in UTC: {8}. Entered a record for activation email to SURVEY_EMAIL_SCHEDULE table successfully.", subject.Id.ToString(), surveyId, subject.KrPT, subject.TimeZone, StudyData.study.Name, StudyData.study.Id, "activation", schedule.ScheduleDT, DateTime.UtcNow));
                        return true;
                    }
                }
            } catch(System.Data.SqlClient.SqlException ex){
                string krpt = HttpContext.Current.Session[WDConstants.VarKrpt].ToString();
                int surveyId = (int)WDConstants.EmailType.Activation;
                logger.Error(String.Format(CultureInfo.InvariantCulture, "FAIL: subject krpt: {0}, surveyId: {1}, study name: {2}, study ID in NPDB: {3}, email info: {4}, logging time stamp in UTC: {5}, error message: {6}. Sending activation email failed, failed in entering a record for activation email to SURVEY_EMAIL_SCHEDULE table.", krpt, surveyId, StudyData.study.Name, StudyData.study.Id, "activation", DateTime.UtcNow, ex.Message), ex);
            } catch(Exception ex){
                string krpt = HttpContext.Current.Session[WDConstants.VarKrpt].ToString();
                int surveyId = (int)WDConstants.EmailType.Activation;
                logger.Error(String.Format(CultureInfo.InvariantCulture, "FAIL: subject krpt: {0}, surveyId: {1}, study name: {2}, study ID in NPDB: {3}, email info: {4}, logging time stamp in UTC: {5}, error message: {6}. Sending activation email failed, failed in entering a record for activation email to SURVEY_EMAIL_SCHEDULE table.", krpt, surveyId, StudyData.study.Name, StudyData.study.Id, "activation", DateTime.UtcNow, ex.Message), ex);
            }
            return false;
        }
        protected void SiteLanguages_IndexChanged(object sender, EventArgs e){
            Session[site_language] = Response.Cookies[site_language].Value = WDAPIObject.JavaLocaleToDotNetCulture(SiteLanguagesList.SelectedItem.Value);
            Response.Redirect(Request.Url.ToString(), true);
        }
        protected void DTApply_Click(object sender, EventArgs e){
            using(WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString)){
                DateTime DTtravel = DateTime.Parse(txtTimeTravel.Text, CultureInfo.InvariantCulture);
                SubjectRepository subRep = new SubjectRepository(db);
                Subject subject = subRep.FindByKrptAndStudy(HttpContext.Current.Session[WDConstants.VarKrpt].ToString(), StudyData.study.Id);
                subject.TimeTravelOffset = (int)TimeSpan.FromTicks(DateTimeHelper.GetUtcTime(DTtravel, subject.StudyId, subject.TimeZone).Ticks - DateTime.UtcNow.Ticks).TotalMinutes;
                db.SubmitChanges();
                SurveyScheduler.ScheduleEmailAllSurveys(subject, StudyData.study, SurveyScheduler.ScheduleState.TimeTravel);
                Session[DateTimeHelper.TimeTravelOffset] = subject.TimeTravelOffset;
            }
            Response.Redirect(Request.Url.ToString(), true);
        }
        protected string GetLastReportDate(string krPT, int studyId){
            HashSet<DateTime> lastReportStartDateSet = new HashSet<DateTime>();
            try{
                using(WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString)){
                    Study study = StudyData.study;
                    WDAPIObject wd = new WDAPIObject(study.StudyWorksUsername, study.StudyWorksPassword, study.Name, study.StudyWorksBaseUrl);
                    var surveyList = db.StudySurveys.Where(s => s.StudyId == studyId && (s.FormType == SWAPIConstants.FormTypeSubjectSubmit || s.FormType == SWAPIConstants.FormTypeSubjectAssignment || s.FormType == SWAPIConstants.FormTypeSiteSubmit));
                    foreach(var survey in surveyList){
                        string krSU = survey.KRSU;
                        ClinData clinData = wd.GetClinDataByNumberRange(krPT, krSU, -1, new ArrayList {
                            SWAPIConstants.SuReportStartDate
                        });
                        if(!clinData.datatable.Any()){
                            continue;
                        }
                        string lastReportStartDate = clinData.datatable[0][0];
                        lastReportStartDateSet.Add(Convert.ToDateTime(lastReportStartDate, CultureInfo.InvariantCulture));
                    }

                    //If no report start date is retrieved, get enrolldate of subject  
                    if(!lastReportStartDateSet.Any()){
                        SubjectData sdata = wd.GetSubjectData(krPT, new ArrayList {
                            SWAPIConstants.ColumnPatientEnrollDate
                        });
                        if(sdata != null)
                            lastReportStartDateSet.Add(Convert.ToDateTime(sdata.data.enrolldate, CultureInfo.InvariantCulture));
                    }
                }
                TimeSpan daysFromToday = lastReportStartDateSet.Max().Date.Subtract(DateTime.Now.Date);
                return daysFromToday.TotalDays.ToString(CultureInfo.InvariantCulture);
            } catch(Exception ex){
                string err = "A problem occurred while trying to get last report start date from Study Works: " + ex.Message;
                logger.Error(err, ex);
            }
            return string.Empty;
        }
        protected void chkScreenShot_CheckedChanged(object sender, EventArgs e){
            if(chkScreenShot.Checked)
                Session[WDConstants.VarScreenShot] = "true";
            else
                Session[WDConstants.VarScreenShot] = null;
            Response.Redirect(Request.Url.ToString(), true);
        }
        protected void Diary_IndexChanged(object sender, EventArgs e){
            if(OtherRecordList.SelectedIndex != 0){
                selectionError.Visible = false;
            }
        }
    }
}