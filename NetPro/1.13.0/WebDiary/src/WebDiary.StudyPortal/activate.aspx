﻿<%@ Page Language="C#" AutoEventWireup="true" validateRequest="false" CodeBehind="activate.aspx.cs" Inherits="WebDiary.StudyPortal.Activate" MasterPageFile="~/includes/masters/Site.Master" %>
<asp:Content ContentPlaceHolderID="head" runat="server">
    <script language="javascript" type="text/javascript">
   <!--
        function ClientValidate(oSrc, args) {
            if ($('#chkboxAgreement').attr('checked') == 'checked')
                args.IsValid = true;
            else
                args.IsValid = false;
        }

        function comparePassword(oSrc, args) {
            if (Page_Validators[2].isvalid && Page_Validators[3].isvalid && Page_Validators[4].isvalid && args.Value != $('#txtPassword').val()) {
                args.IsValid = false;
                $('input:password').val('');
                $('#txtPassword').focus();
            } else {
                args.IsValid = true;
            }
        }

        autoLock = false;
        $(document).ready(function () {
            $('input[id="txtEmail"]').change(function () {
                $('#vsErrors').hide();
            });

            $('form').submit(function () {
                $('#vsErrors').hide();
            });
        });
   // -->
</script>
</asp:Content>
<asp:Content ContentPlaceHolderID="mainContent" runat="server">

    <asp:ValidationSummary ID="vsErrors" CssClass="errors" runat="server" 
        DisplayMode="BulletList" EnableClientScript="False"/>
    <div id="unauthBrowser">
        <div id="unauthMsgArea">
            <asp:Label ID="lblUnauthorizedMessage" CssClass="unAuthorizedError" Visible="false" runat="server">
            </asp:Label>
        </div>
        <div id="authBrowserArea" runat="server">
        </div>      
  </div>
    
    <pht:DivPanel ID="pnlForm" runat="server">
        <p>
            <asp:Label ID="lblEmailMsgBody" runat="server" 
                Text="<%$ Resources:Resource, confirmUrEmailprovidePswrd %>">
            </asp:Label>        
        </p>
        <div class="form-row">
            <table>
            <tr>
                <td class="label">
                    <asp:Label id="lblEmail" runat="server"
                        Text="<%$ Resources:Resource, emailLbl %>">
                    </asp:Label>
                </td>
                <td class="field">
                    <asp:TextBox ID="txtEmail" runat="server" MaxLength="256" />
                </td>
            </tr>
            <tr class="form-empty-row">
                <td />
                <td>
                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" CssClass="nb-errors"
                    ControlToValidate="txtEmail" 
                    ErrorMessage="<%$ Resources:Resource, LoginrfvEmail %>" Display="Dynamic"/>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                        ControlToValidate="txtEmail" Display="Dynamic" ErrorMessage="<%$ Resources:Resource, rfvEmailNotValid %>" 
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                        CssClass="nb-errors"></asp:RegularExpressionValidator>
                </td>
            </tr>

            <tr>
                <td class="label">
                    <asp:Label id="lblPassword" runat="server"
                        Text="<%$ Resources:Resource, PswrdLabel%>">
                    </asp:Label>
                </td>
                <td class="field">
                    <!--Prevent autocomplete for latest browsers such as IE 11-->
                    <input id="prevent_autocomplete" name="prevent_autocomplete" type="password" class="hide"/>
                    <asp:TextBox ID="txtPassword" oncopy="return false;" onpaste="return false;" oncut="return false;"
                        runat="server" MaxLength="64" TextMode="Password" />
                </td>
            </tr>
            <tr class="form-empty-row">
                <td />
                <td>
                    <asp:RequiredFieldValidator ID="rfvPassword" runat="server" CssClass="nb-errors"
                        ControlToValidate="txtPassword" 
                        ErrorMessage="<%$ Resources:Resource, rfvPassword%>" Display="Dynamic"/>
                    <asp:RegularExpressionValidator ID="PasswordLengthValidator" runat="server" 
                        ControlToValidate="txtPassword" CssClass="nb-errors" Display="Dynamic"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <asp:Label id="lblPasswordConfirm" runat="server"
                        Text="<%$ Resources:Resource, ConfirmPasswordLbl%>">
                    </asp:Label>
                </td>
                <td class="field">
                    <!--Prevent autocomplete for latest browsers such as IE 11-->
                    <input id="prevent_autocomplete2" name="prevent_autocomplete2" type="password" class="hide"/>
                    <asp:TextBox ID="txtConfirmPassword" oncopy="return false;" onpaste="return false;" oncut="return false;"
                        runat="server" MaxLength="64" TextMode="Password"/>              
                </td>
            </tr>
            <tr class="form-empty-row">
                <td />
                <td>
                    <asp:RequiredFieldValidator ID="rfvConfirmPassword" runat="server" CssClass="nb-errors"
                        ControlToValidate="txtConfirmPassword" 
                        ErrorMessage="<%$ Resources:Resource, rfvPasswordConfirm%>" 
                        Display="Dynamic" /> 
                    <asp:CustomValidator ID="ComparePasswordsValidator" runat="server" ErrorMessage="<%$ Resources:Resource, PwdsDontMatch%>" ControlToValidate="txtConfirmPassword"
                    Display="Dynamic" CssClass="nb-errors" ClientValidationFunction="comparePassword"></asp:CustomValidator>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <p><asp:CheckBox ID="chkboxAgreement" runat="server" Text="<%$ Resources:Resource, EmailAffidavitCheckbox%>" /> &nbsp;&nbsp;<a href="#" onclick="$('#EmailAffidavitContent').slideDown(); $(this).hide();"><%= Resources.Resource.detailLink %></a><asp:CustomValidator 
            ID="CustomValidator1" runat="server" ClientValidationFunction="ClientValidate" 
            CssClass="nb-errors" Display="Dynamic" 
            ErrorMessage="<%$ Resources:Resource, rfvCheckbox%>"></asp:CustomValidator>
        </p>
    <div style="display:none" id="EmailAffidavitContent">
        <p><%= Resources.Resource.EmailAffidavit %></p>
        <ul>
            <li><%= Resources.Resource.EmailAffidavitLI1 %></li>
            <li><%= Resources.Resource.EmailAffidavitLI2 %></li>
            <li><%= Resources.Resource.EmailAffidavitLI3 %></li>
            <li><%= Resources.Resource.EmailAffidavitLI4 %></li>
            <li><%= Resources.Resource.EmailAffidavitLI5 %></li>
            <li><%= Resources.Resource.EmailAffidavitLI6 %></li>
        </ul>
     </div>
       <asp:Button ID="btnLogOn" Text="<%$ Resources:Resource, EmailAffidavitSubmitBtn%>"  runat="server" OnClientClick="if (!Page_Validators[5].isvalid) return false;"/>
     <br />
    </pht:DivPanel>
         

    <pht:DivPanel ID="pnlSuccess" runat="server" Visible="False">
      <br />
        <p>
            <asp:Literal ID="litConfirmText" runat="server"></asp:Literal>
        </p>
        <p/>
        <p>
            <asp:Literal ID="litConfirmTextP1" runat="server"></asp:Literal>
        </p>
    </pht:DivPanel>

</asp:Content>
