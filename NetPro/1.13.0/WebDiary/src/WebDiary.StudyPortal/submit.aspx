﻿<%@ Page Title="" Language="C#" MasterPageFile="~/includes/masters/Site.Master" AutoEventWireup="true" CodeBehind="submit.aspx.cs" Inherits="WebDiary.StudyPortal.Submit" %>
<%@ Register assembly="WebDiary.Controls" namespace="WebDiary.Controls" tagprefix="cc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
<asp:Literal ID="redirectScript" runat="server"></asp:Literal>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">

<asp:Literal ID="litResult" runat="server" />

    <cc1:DivPanel ID="pnlSuccess" runat="server" Visible="False">
    </cc1:DivPanel>
    
    <cc1:DivPanel ID="DivPanel2" runat="server" Height="28px" Width="962px">
        <asp:Button ID="Retry" runat="server" 
            Text="<%$ Resources:Resource, SubmitTryAgain%>" />
    </cc1:DivPanel>
    <br />
    <cc1:DivPanel ID="DivPanel1" runat="server" Height="28px" Width="962px">
        <asp:Button ID="Cancel" runat="server" 
            Text="<%$ Resources:Resource, SubmitDiscard%>" />
    </cc1:DivPanel>
    
</asp:Content>
