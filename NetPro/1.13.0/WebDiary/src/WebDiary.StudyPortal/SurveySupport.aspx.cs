﻿using System;
using System.Web;
using WebDiary.Core.Helpers;
using WebDiary.SWAPI;
using WebDiary.Core.Entities;
using WebDiary.Core.Repositories;
using WebDiary.Core.Constants;
using WebDiary.Controls;
using WebDiary.StudyPortal.PDECustom;
namespace WebDiary.StudyPortal{
    public partial class SurveySupport : BasePage{
        protected void Page_Init(object sender, EventArgs e){
            char[] traillers = {'/'};
            int survey_id = 0;
            string returnUrl;
            string appUrl;
            string loginPage = Session["isMobile"] != null ? "/mobile/timeout-redirect.aspx" : "/login.aspx";
            if(Request.IsSecureConnection){
                returnUrl = "https://" + Request.Url.Host + Request.ApplicationPath.TrimEnd(traillers) + loginPage;
                appUrl = "https://" + Request.Url.Host + Request.ApplicationPath.TrimEnd(traillers);
            } else{
                returnUrl = "http://" + Request.Url.Host + Request.ApplicationPath.TrimEnd(traillers) + loginPage;
                appUrl = "http://" + Request.Url.Host + Request.ApplicationPath.TrimEnd(traillers);
            }
            if(string.IsNullOrEmpty(Request.QueryString["survey_id"]) ||
               !int.TryParse(Request.QueryString["survey_id"], out survey_id)){
                Response.Write("top.location.href = '" + returnUrl + "';");
            }
            if(Session[Request.QueryString[WDConstants.VarPsdId]] == null){
                using(WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString)){
                    SurveyRepository svr = new SurveyRepository(db);
                    StudySurveys survey = svr.FindById(survey_id);

                    //determine if the request is for patient access or site access
                    if(survey.FormType.Equals("Subject Submit"))//Patient gateway
                    {
                        if(Session["login"] == null){
                            Response.Write("top.location.href = '" + returnUrl + "';");
                        }
                    } else//site gateway
                    {
                        returnUrl = Request.QueryString["ssa_base_url"] + SWAPIConstants.SessionTimeoutPage;
                        if(!string.IsNullOrEmpty(Session[WDConstants.VarRequestUrl] as string)){
                            if(!SessionHelper.CheckSwSession(Session[WDConstants.VarRequestUrl] as string)){
                                Response.Write("top.location.href = '" + returnUrl + "';");
                            }
                        } else{
                            Response.Write("top.location.href = '" + returnUrl + "';");
                        }
                    }
                }
            }
            if(SessionHelper.IsSiteSession())
                returnUrl = appUrl + "/logout.aspx?ssa_url=" + Server.UrlEncode(Request.QueryString["ssa_base_url"] + SWAPIConstants.SessionTimeoutPage);
            Response.Write(" secCheck = true; window.onbeforeunload = function () { if (isReloaded) return '" + Resources.Resource.RefreshMessage + "'; };");
            //[pdecustom] - add logic to account for ?
            Response.Write("function loadjscssfile(filename){ var filetype = filename.substr(filename.lastIndexOf('.') + 1, 2).toLowerCase(); if(filetype==\"js\"){var fileref=document.createElement('script'); fileref.setAttribute(\"type\",\"text/javascript\"); fileref.setAttribute(\"src\", '" + appUrl + "/includes/scripts/' + filename);} else if (filetype==\"cs\"){var fileref=document.createElement(\"link\"); fileref.setAttribute(\"rel\",\"stylesheet\"); fileref.setAttribute(\"type\",\"text/css\"); fileref.setAttribute(\"href\",'" + appUrl + "/includes/css/' + filename);} if(typeof fileref!=\"undefined\")document.getElementsByTagName(\"head\")[0].appendChild(fileref);}");
            //Response.Write("function loadjscssfile(filename){ var questionMarkIndex = filename.lastIndexOf('?'), filetype = (questionMarkIndex == -1)? filename.substr(filename.lastIndexOf('.') + 1).toLowerCase(): filename.substring(filename.lastIndexOf('.') + 1, filename.lastIndexOf('?')).toLowerCase(); if(filetype==\"js\"){var fileref=document.createElement('script'); fileref.setAttribute(\"type\",\"text/javascript\"); fileref.setAttribute(\"src\", '" + appUrl + "/includes/scripts/' + filename);} else if (filetype==\"css\"){var fileref=document.createElement(\"link\"); fileref.setAttribute(\"rel\",\"stylesheet\"); fileref.setAttribute(\"type\",\"text/css\"); fileref.setAttribute(\"href\",'" + appUrl + "/includes/css/' + filename);} if(typeof fileref!=\"undefined\")document.getElementsByTagName(\"head\")[0].appendChild(fileref);}");
            //Response.Write("function loadjscssfile(filename){ var filetype = filename.substr(filename.lastIndexOf('.') + 1).toLowerCase(); if(filetype==\"js\"){var fileref=document.createElement('script'); fileref.setAttribute(\"type\",\"text/javascript\"); fileref.setAttribute(\"src\", '" + appUrl + "/includes/scripts/' + filename);} else if (filetype==\"css\"){var fileref=document.createElement(\"link\"); fileref.setAttribute(\"rel\",\"stylesheet\"); fileref.setAttribute(\"type\",\"text/css\"); fileref.setAttribute(\"href\",'" + appUrl + "/includes/css/' + filename);} if(typeof fileref!=\"undefined\")document.getElementsByTagName(\"head\")[0].appendChild(fileref);}");
            Response.Write("loadjscssfile(\"app.js\", \"js\"); var sTimeout = " + Session.Timeout * 60000 + "; ");

            //[pdecustom] Load Master Med List
            Response.Write("loadjscssfile(\"masterMedicationList.en_US.js?" + DateTime.Now.Ticks + "\", \"js\");");
            Response.Write("var lastCheck = new Date().getTime(); function sleepCheck () { var now = new Date().getTime(); var diff = now - lastCheck; if (diff > sTimeout) {isReloaded = false; top.location.href = '" + returnUrl + "'; } }; setInterval(function(){sleepCheck();}, 2000);");
            Response.Write("$(function() { $('body').attr('dir','" + TextDirection + "'); $('div[align=\"left\"]').attr('align',''); });");
            Response.Write(" notOnlineMessage = '" + Resources.Resource.NotOnlineMessage + "';");

            //[schan] Add custom javascript
            String customJavascript = StudyUtils.getCustomJavascript(Session);
            if(customJavascript != null){
                Response.Write(customJavascript);
            }
            Response.End();
        }
        [System.Web.Services.WebMethod]
        public static string EQBackupSwap(){
            if(HttpContext.Current.Session[WDConstants.EQBackup] != null &&
               HttpContext.Current.Session[WDConstants.EQBackup].ToString() == "0"){
                HttpContext.Current.Session[WDConstants.EQBackup] = "1";
                return "0";
            }
            HttpContext.Current.Session[WDConstants.EQBackup] = "0";
            return "1";
        }
    }
}