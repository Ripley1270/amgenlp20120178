﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Web.UI;
using WebDiary.Core.Entities;
using WebDiary.Core.Helpers;
using WebDiary.Core.Repositories;

namespace WebDiary.StudyPortal
{
    public partial class AppCache : Page
    {
        private const string LF_BOM = "manifest.appcache";
        public string selectedLanguageHash;
        protected void Page_Load(object sender, EventArgs e)
        {               
            using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
            {
                Subject subject = SubjectHelper.AuthenticateLFSubject(Context);
                if (subject != null)
                {
                    // Get selected language hash
                    LanguageHash selectedLanguage = new LanguageHashRespository(db).Find(StudyData.study.Id, subject.Language.Replace('_', '-'));
                    if(selectedLanguage != null)
                        selectedLanguageHash = selectedLanguage.Hash;
                }

                //Generate filelist from bom.                    
                string appcacheFile = HttpContext.Current.Server.MapPath(@"~/LF/" + LF_BOM);
                string fileContent = Cache["appcachefile"] as string;
                if (string.IsNullOrEmpty(fileContent))
                {
                    using (StreamReader sr = File.OpenText(appcacheFile))
                    {
                        fileContent = sr.ReadToEnd().Replace("CACHE MANIFEST", "");
                        Cache.Insert("appcachefile", fileContent, new CacheDependency(appcacheFile));
                    }
                }

                litBomFile.Text = fileContent;
            }
            Page.DataBind();
        }
    }
}