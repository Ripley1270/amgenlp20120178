﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using WebDiary.StudyPortal.Api.Models;

namespace WebDiary.StudyPortal.Api
{
    public class JSONM
    {
        /// <summary>
        /// This checks to see if incoming JSON is a single diary or a collection of diaries
        /// </summary>
        /// <param name="inputJson">A diary or a collection diaries</param>
        /// <returns>A single or a collection of diaries with decompressed answer</returns>
        static public string TransformJSONM(string inputJson)
        {
            JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings {
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                DateParseHandling = DateParseHandling.None
            };

            object obj = JsonConvert.DeserializeObject(inputJson, jsonSerializerSettings);

            if (obj.GetType().Name == "JArray")//inputJson is array of objects
            {
                JArray inputItem = (JArray)obj;
                JArray outputItem = new JArray();
                foreach (JObject item in inputItem)
                {
                    JObject transformedItem = TransformItem(item);
                    outputItem.Add(transformedItem);
                }
                return outputItem.ToString();
            }
            else if (obj.GetType().Name == "JObject")//inputJson is object
            {
                JObject inputItem = (JObject)obj;
                JObject outputItem = TransformItem(inputItem);
                return outputItem.ToString();
            }
            else
            {
                throw new FormatException("Compressed diary does not have valid format");
            }
        }

        /// <summary>
        /// This checks to see if it has key"A" and decompresses data using algorithm 
        /// </summary>
        /// <param name="item">Diary data</param>
        /// <returns>Diary data with decompressed answer</returns>
        static public JObject TransformItem(JObject item)
        {
            JArray answer = new JArray();
            List<Dictionary<string, string>> items = new List<Dictionary<string, string>>();

            if (item["A"].Type == JTokenType.Array)
            {
                answer = (JArray)item["A"];
                items = decompress(answer);
            }
            else
            {
                //Diary Answer is not JSON Array
                throw new FormatException("Answer in compressed diary is not JSON array");
            }

            JArray decompressedAnswer = JArray.FromObject(items);
            item.Property("A").Replace(new JProperty("A", decompressedAnswer));

            return item;
        }

        /// <summary>
        /// This is algorithm to decompress JSONH compressed data
        /// </summary>
        /// <param name="compressedList">JSONH compressed data</param>
        /// <returns>Decompressed data</returns>
        static public List<Dictionary<string, string>> decompress(JArray compressedList)
        {
            int length = compressedList.Count();
            //get number of keys
            int keyLength = compressedList[0].Value<int>();

            if ((length - 1) % keyLength != 0)
            {
                throw new FormatException("Number of values in compressed anwer is not compatible with number of keys");
            }

            Dictionary<string, string> item = new Dictionary<string, string>();
            List<Dictionary<string, string>> items = new List<Dictionary<string, string>>();

            for (int i = 1 + keyLength; i < length; )
            {
                for (int j = 0; j < keyLength; )
                {
                    item.Add((string)compressedList[++j], (string)compressedList[i++]);

                    if (j == keyLength)
                    {
                        items.Add(new Dictionary<string, string>(item));
                        item.Clear();
                    }
                }
            }

            return items;
        }
    }
}