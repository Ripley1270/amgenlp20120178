﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Globalization;
using Newtonsoft.Json;

namespace WebDiary.StudyPortal.Api.Models
{
    [DataContract]
    public class CredentialModel
    {
        [DataMember(IsRequired = true, Name = "setupCode")]
        public long SetupCode { get; set; }

        [DataMember(IsRequired = true, Name = "clientPassword")]
        public string ClientPassword { get; set; }

        [DataMember(Name = "challengeQuestions")]
        public List<ChallengeQuestionModel> ChallengeQuestions { get; set; }

        [DataMember(IsRequired = true, Name = "source")]
        public string Source { get; set; }

        [DataMember(Name = "regDeviceId")]
        public int? RegDeviceId { get; set; }

        [DataMember(Name = "clientId")]
        public string ClientID { get; set; }
    }

    [DataContract]
    public class UpdateCredentialModel
    {
        [DataMember(IsRequired = true, Name = "clientPassword")]
        public string ClientPassword { get; set; }

        [DataMember(Name = "challengeQuestions")]
        public List<ChallengeQuestionModel> ChallengeQuestions { get; set; }
    }

    [DataContract]
    public class ChallengeQuestionModel
    {
        [DataMember(IsRequired = true, Name = "question")]
        public int? Question { get; set; }

        [DataMember(IsRequired = true, Name = "answer")]
        public string Answer { get; set; }
    }

    [DataContract]
    public class ChallengeQuestionsModel
    {
        [DataMember(IsRequired= true, Name = "challengeQuestions")]
        public List<ChallengeQuestionModel> ChallengeQuestions { get; set; }
    }
    
    [DataContract]
    public class CredentialResult
    {
        [DataMember(Name = "clientPassword")]
        public string ClientPassword { get; set; }

        [DataMember(Name = "password")]
        public string Password { get; set; }

        [DataMember(Name = "challengeQuestions")]
        public List<ChallengeQuestionResult> ChallengeQuestions { get; set; }
    }

    [DataContract]
    public class ChallengeQuestionsResult
    {
        [DataMember(Name = "challengeQuestions")]
        public List<ChallengeQuestionResult> ChallengeQuestions { get; set; }
    }
    
    [DataContract]
    public class ChallengeQuestionResult
    {
        [DataMember(Name = "question")]
        public int? Question { get; set; }

        [DataMember(Name = "answer")]
        public string Answer { get; set; }
    }
}
