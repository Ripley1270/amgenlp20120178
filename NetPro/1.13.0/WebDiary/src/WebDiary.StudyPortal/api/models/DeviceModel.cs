﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace WebDiary.StudyPortal.Api.Models
{
    [DataContract]
    public class NewDeviceModel
    {
        [DataMember(IsRequired = true, Name = "U")]
        public long UserID { get; set; }

        [DataMember(IsRequired = true, Name = "W")]
        public string Password { get; set; }

        [DataMember(Name = "Q")]
        public int? Question { get; set; }

        [DataMember(Name = "A")]
        public string Answer { get; set; }

        [DataMember(Name = "S")]
        public string SerialNumber { get; set; }

        [DataMember(Name = "M")]
        public string Model { get; set; }

        [DataMember(Name = "O")]
        public string Source { get; set; }
    }

    [DataContract]
    public class DeviceModel
    {
        [DataMember(Name = "D")]
        public Guid? DeviceID { get; set; }
        [DataMember(Name = "W")]
        public string Password { get; set; }
        [DataMember(Name = "Q")]
        public int? Question { get; set; }
        [DataMember(Name = "A")]
        public string Answer { get; set; }
        [DataMember(Name = "E")]
        public bool? EndLP { get; set; }
    }
}
