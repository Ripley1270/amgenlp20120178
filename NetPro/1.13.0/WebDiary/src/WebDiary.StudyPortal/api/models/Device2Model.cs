﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace WebDiary.StudyPortal.Api.Models
{
    [DataContract]
    public class Device2Model
    {
        [DataMember(IsRequired = true, Name = "studyName")]
        public string StudyName { get; set; }

        [DataMember(IsRequired = true, Name = "krdom")]
        public string KrDom {get; set; }

       [DataMember(IsRequired = true, Name = "startupCode")]
        public string StartupCode { get; set; }

        [DataMember(IsRequired = true, Name = "deviceUuid")]
        public string DeviceUuid { get; set; }

        [DataMember(Name = "assetTag")]
        public string AssetTag { get; set; }

        [DataMember(Name = "model")]
        public string Model { get; set; }

        [DataMember(Name = "imei")]
        public string IMEI { get; set; }
    }

    [DataContract]
    public class RegistrationResult 
    {
        [DataMember(Name = "apiToken")]
        public string APIToken { get; set; }

        [DataMember(Name = "regDeviceId")]
        public int? RegDeviceId { get; set; }

        [DataMember(Name = "clientId")]
        public Guid ClientId { get; set; }
    }
}