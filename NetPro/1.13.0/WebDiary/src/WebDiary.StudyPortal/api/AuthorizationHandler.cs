﻿using System;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Security.Principal;
using WebDiary.Core.Entities;
using WebDiary.Core.Repositories;
using WebDiary.Core.Helpers;
using WebDiary.Core.Errors;
using WebDiary.SWAPI;
using log4net;

namespace WebDiary.StudyPortal.Api
{
    public class AuthorizationHandler : DelegatingHandler
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(AuthorizationHandler));

        protected override Task<HttpResponseMessage> SendAsync(
            HttpRequestMessage request, CancellationToken cancellationToken)
        {
            // Process request
            
            Subject subject = null;
            IEnumerable<string> headers;
            NameValueHeaderValue deviceStatusHeader = null;
            WebApiPrincipal principal = new WebApiPrincipal();

            // Record transmission times
            principal.ServerReceiptTime = DateTime.UtcNow;
            try
            {
                if (!request.Headers.Contains("Clientdate"))
                    throw new FormatException();
                string clientDate = request.Headers.GetValues("Clientdate").FirstOrDefault<string>();
                request.Headers.Date = principal.ClientRequestTime = DateTimeOffset.ParseExact(clientDate, SWAPIConstants.LogPadAppLocalDateTimeFormat, CultureInfo.InvariantCulture);
            }
            catch (FormatException fex)
            {
                logger.Error("Improperly formatted client date header", fex);
                // skip the inner handler and directly create the response.
                var directResponse = new HttpResponseMessage(HttpStatusCode.BadRequest);
                // Note: TaskCompletionSource creates a task that does not contain a delegate.
                var tsc = new TaskCompletionSource<HttpResponseMessage>();
                tsc.SetResult(directResponse);   // Also sets the task state to "RanToCompletion"
                return tsc.Task;
            }

            // Attempt to authenticate subject or a device for SitePad App 
            try
            {
                if (request.Headers.TryGetValues("Authorization", out headers)) // Authenticate a subject
                {
                    string authorizationHeader = headers.FirstOrDefault<string>();
                    if (!string.IsNullOrWhiteSpace(authorizationHeader))
                    {
                        string[] authSplit = authorizationHeader.Split(':');
                        if (authSplit.Length > 2)
                        {
                            string authPassword = authSplit[1].Trim();

                            // validate authorization header
                            bool isValidAuth = false;
                            long authPin = 0;                            
                            Guid authDeviceCode;
                            isValidAuth = Guid.TryParse(authSplit[2], out authDeviceCode) && long.TryParse(authSplit[0], out authPin);

                            using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                            {
                                SubjectRepository subjectRep = new SubjectRepository(db);
                                subject = subjectRep.FindByPin(authPin);
                                if (!isValidAuth || subject == null)
                                {
                                    throw new NetProException(SWISStatusCodes.AuthorizationFailure);
                                }
                                else
                                {
                                    SubjectDeviceRepository deviceRepo = new SubjectDeviceRepository(db);

                                    // Validate device password and subject status - null the subject if this fails as if it was never found
                                    SubjectDevice device = deviceRepo.FindBySubjectIdDeviceCode(subject.Id, authDeviceCode);
                                    if (device == null || device.Password != authPassword || subject.IsLockedOut)
                                        throw new NetProException(SWISStatusCodes.AuthorizationFailure);
                                    else
                                    {
                                        principal.SubjectID = subject.Id;
                                        principal.UserID = subject.Pin.Value;                                        
                                        SubjectDevice currentDevice = deviceRepo.FindLatestBySubjectId(subject.Id);
                                        if (!currentDevice.DeviceCode.Equals(authDeviceCode)) // not active device
                                        {
                                            deviceStatusHeader = new NameValueHeaderValue("X-Device-Status", SWISDeviceStatus.NotActive);

                                            // Check if the action is allowed from deactivated device
                                            string controller = request.GetRouteData().Values["controller"].ToString().ToLower(CultureInfo.InvariantCulture);
                                            if (ActionIsAllowed(controller, request.Method.Method))
                                                principal.DeviceID = device.DeviceCode;
                                            else
                                                throw new NetProException(SWISStatusCodes.ActionNotAllowed);
                                        }
                                        else
                                        {
                                            deviceStatusHeader = new NameValueHeaderValue("X-Device-Status", SWISDeviceStatus.Active);
                                            principal.DeviceID = currentDevice.DeviceCode;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else if (request.Headers.TryGetValues("X-Sync-API-Token", out headers)) // Authenticate a device for SitePad App
                {
                    string tokenHeader = headers.FirstOrDefault<string>();
                    if (!string.IsNullOrWhiteSpace(tokenHeader))
                    {
                        using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                        {
                            DeviceRepository deviceRep = new DeviceRepository(db);
                            Device activeDevice = deviceRep.FindByApiTokenAndStatus(tokenHeader, true);

                            if (activeDevice != null)
                            {
                                principal.DeviceID = activeDevice.DeviceCode;
                                principal.KrDom = activeDevice.KrDom;
                                principal.ApiTokenAuth = true;
                                principal.StudyId = activeDevice.StudyId;
                                if (activeDevice.SWDeviceId.HasValue)
                                    principal.SWDeviceId = activeDevice.SWDeviceId.Value;
                            }
                            else
                                throw new NetProException(SWISStatusCodes.AuthorizationFailure);
                        }
                    }
                }
            }
            catch (NetProException npex)
            {
                logger.Error(npex.Message, npex);
                // skip the inner handler and directly create the response.
                var directResponse = new HttpResponseMessage();
                if (npex.Message == SWISStatusCodes.AuthorizationFailure)
                    directResponse.StatusCode = HttpStatusCode.Unauthorized;
                else if (npex.Message == SWISStatusCodes.ActionNotAllowed)
                    directResponse.StatusCode = HttpStatusCode.Forbidden;

                if (deviceStatusHeader != null)
                    directResponse.Headers.Add(deviceStatusHeader.Name, deviceStatusHeader.Value);

                // Note: TaskCompletionSource creates a task that does not contain a delegate.
                var tsc = new TaskCompletionSource<HttpResponseMessage>();
                tsc.SetResult(directResponse);   // Also sets the task state to "RanToCompletion"
                return tsc.Task;
            }
            catch (FormatException fex)
            {
                logger.Error(fex.Message, fex);
                var directResponse = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                // Note: TaskCompletionSource creates a task that does not contain a delegate.
                var tsc = new TaskCompletionSource<HttpResponseMessage>();
                tsc.SetResult(directResponse);   // Also sets the task state to "RanToCompletion"
                return tsc.Task;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                // skip the inner handler and directly create the response.
                var directResponse = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                directResponse.Headers.Add("X-Error-Code", SWISStatusCodes.ServerError);
                // Note: TaskCompletionSource creates a task that does not contain a delegate.
                var tsc = new TaskCompletionSource<HttpResponseMessage>();
                tsc.SetResult(directResponse);   // Also sets the task state to "RanToCompletion"
                return tsc.Task;
            }            

            // if it gets this far, set a custom principal
            if (principal.DeviceID != Guid.Empty)
            {
                Thread.CurrentPrincipal = principal;
                if (HttpContext.Current != null)
                {
                    HttpContext.Current.User = principal;
                }                
            }           
            
            // Process response
            // Call the inner handler.
            var response = base.SendAsync(request, cancellationToken);
            if (deviceStatusHeader != null)
                response.Result.Headers.Add(deviceStatusHeader.Name, deviceStatusHeader.Value);
            
            return response;
        }

        /// <summary>
        /// Check whether or not action is allowed on deactivated device 
        /// </summary>
        /// <param name="controller">controllerName</param>
        /// <param name="method">verb</param>
        /// <returns>true/false</returns>
        private bool ActionIsAllowed(string controller, string method)
        {
            //Not allowed actions
            Dictionary<string, List<string>> badActions = new Dictionary<string, List<string>>();
            badActions.Add("subjects", new List<string> { "PUT" });
            badActions.Add("devices", new List<string> { "POST"}); //Removed "PUT" from the bad action list to allow End Device Use for deactivated device

            if (badActions.ContainsKey(controller))
            {
                List<string> value = badActions[controller];
                foreach (string verb in value)
                {
                    if (method == verb)
                        return false;
                } 
            }
            return true;
        }
    }
}
