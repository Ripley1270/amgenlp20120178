﻿using System.Linq;
using System.IO;
using System.Xml.Linq;

namespace WebDiary.StudyPortal.Api.Helpers
{
    public class TriggeredPhase
    {
        public static string GetTriggeredPhase(string protocol, string protocolTriggeredPhase)
        {
            TextReader tr = new StringReader(protocolTriggeredPhase);
            XElement xmlDoc = XElement.Load(tr);
            
            var triggeredPhase = (from s in xmlDoc.Descendants("protocol")
                                  where s.Attribute("code").Value == protocol
                                  select s.Attribute("triggered_phase")).FirstOrDefault();

            if (triggeredPhase != null && !string.IsNullOrEmpty(triggeredPhase.Value))
                return triggeredPhase.Value;
            else
                return string.Empty;
        }
    }
}