﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.IO;
using System.IO.Compression;
using WebDiary.Core.Errors;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using log4net;

namespace WebDiary.StudyPortal.Api
{
    public class JSONMHandler : DelegatingHandler
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(JSONMHandler));

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {

            //check if JSONM content
            if (!IsJSONMContent(request))
            {
                return base.SendAsync(request, cancellationToken);
            }

            return request.Content.ReadAsStreamAsync().ContinueWith(readTsk =>
            {
                Stream outputStream = new MemoryStream();
                string inputJson = string.Empty;
                try
                {
                    using (Stream inputStream = readTsk.Result)
                    {
                        using (var streamReader = new StreamReader(inputStream, Encoding.UTF8))
                        {
                            inputJson = streamReader.ReadToEnd();
                        }
                    }

                    string outputJson = JSONM.TransformJSONM(inputJson);
                    HttpContent originalContent = request.Content;

                    //write it to memory stream
                    byte[] bytes = Encoding.UTF8.GetBytes(outputJson);
                    outputStream.Write(bytes, 0, bytes.Length);
                    outputStream.Seek(0, SeekOrigin.Begin);

                    //replace request content with the decompressed stream
                    request.Content = new StreamContent(outputStream);

                    //copy headers from original content to new content
                    foreach (var header in originalContent.Headers)
                    {
                        if (header.Key != "Content-Length")
                            request.Content.Headers.Add(header.Key, header.Value);
                    }

                    return base.SendAsync(request, cancellationToken).ContinueWith<HttpResponseMessage>(tsk =>
                    {
                        HttpResponseMessage response = tsk.Result;
                        return response;
                    });
                }
                catch (JsonReaderException ex)
                {
                    logger.Error(ex.Message, ex);
                    var directResponse = new HttpResponseMessage(HttpStatusCode.BadRequest);
                    var tsc = new TaskCompletionSource<HttpResponseMessage>();
                    tsc.SetResult(directResponse);
                    return tsc.Task;
                }
                catch (FormatException ex)
                {
                    logger.Error(ex.Message, ex);
                    var directResponse = new HttpResponseMessage(HttpStatusCode.BadRequest);
                    var tsc = new TaskCompletionSource<HttpResponseMessage>();
                    tsc.SetResult(directResponse);
                    return tsc.Task;
                }
                catch (DivideByZeroException ex)
                {
                    logger.Error(String.Format(CultureInfo.InvariantCulture, "Key length in answer of diary was 0 - incoming data: {0}, error message: {1}.", inputJson, ex.Message), ex);
                    var directResponse = new HttpResponseMessage(HttpStatusCode.BadRequest);
                    var tsc = new TaskCompletionSource<HttpResponseMessage>();
                    tsc.SetResult(directResponse);
                    return tsc.Task; 
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message, ex);
                    var directResponse = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                    directResponse.Headers.Add("X-Error-Code", SWISStatusCodes.ServerError);
                    var tsc = new TaskCompletionSource<HttpResponseMessage>();
                    tsc.SetResult(directResponse);
                    return tsc.Task;
                }
                finally
                {
                    outputStream.Dispose();
                }

            }).Unwrap();

        }

        protected bool IsJSONMContent(HttpRequestMessage request)
        {
            bool isJsonm = false;

            if (request.Content.Headers.ContentType != null && request.Content.Headers.ContentType.MediaType == "application/json" && request.Headers.Contains("Diary-Encoding"))
            {
                string diaryEncoding = request.Headers.GetValues("Diary-Encoding").FirstOrDefault<string>();
                if (!string.IsNullOrEmpty(diaryEncoding) && diaryEncoding.ToLowerInvariant() == "jsonm")
                    isJsonm = true;
            }
            return isJsonm;
        }
    }


}