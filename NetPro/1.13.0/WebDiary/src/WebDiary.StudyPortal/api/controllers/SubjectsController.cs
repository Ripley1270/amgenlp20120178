﻿using System;
using System.Threading;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.SqlClient;
using System.Xml.Linq;
using WebDiary.StudyPortal.Api.Models;
using WebDiary.Core.Helpers;
using WebDiary.Core.Entities;
using WebDiary.Core.Repositories;
using WebDiary.Core.Errors;
using WebDiary.Core.Constants;
using WebDiary.WDAPI;
using WebDiary.SWAPI;
using WebDiary.SWAPI.SWData;
using WebDiary.SWIS;
using log4net;
using System.Text.RegularExpressions;

namespace WebDiary.StudyPortal.Api.Controllers
{
    public class SubjectsController : ApiController
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(SubjectsController));
        private WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString);

        //GET api/v1/subjects, api/v1/subjects?since=<ts>
        [Authorize]
        public IEnumerable<SubjectResult2Model> GetSubjects([FromUri] string since = "")
        {
            WebApiPrincipal principal = Thread.CurrentPrincipal as WebApiPrincipal;
            if (!principal.ApiTokenAuth)
            {
                HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                throw responseException;
            }

            DateTime timeStamp = DateTime.MinValue;
            //Parse time stamp
            if (!string.IsNullOrEmpty(since))
            {
                try
                {
                    double millisecond;
                    if (Double.TryParse(since, NumberStyles.None, CultureInfo.InvariantCulture, out millisecond))
                    {
                        timeStamp = DateTimeHelper.FromUnixTime(millisecond);
                    }
                }
                catch (Exception ex) //suppress error
                {
                }

                try
                {
                    if (timeStamp == DateTime.MinValue)
                        timeStamp = DateTime.ParseExact(since, SWAPIConstants.UTCDateTimeFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
                }
                catch (Exception ex) //suppress error
                {
                }

                try
                {
                    if (timeStamp == DateTime.MinValue)
                        timeStamp = DateTimeOffset.ParseExact(since, SWAPIConstants.DateTimeFormatWithTimeZone, CultureInfo.InvariantCulture).UtcDateTime;
                }
                catch (Exception ex)
                {
                    logger.Error(String.Format(CultureInfo.InvariantCulture, "Time stamp passed in incoming url is not valid format. Time stamp in incoming url: {0}, krdom: {1}.", since, principal.KrDom), ex);
                    HttpResponseException responseException = new HttpResponseException(HttpStatusCode.BadRequest);
                    responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.InvalidDateTimeFormat);
                    throw responseException;
                }
            }

            string krDom = principal.KrDom;
            ArrayList parameter = new ArrayList();
            parameter.Add(krDom);

            WebDiaryContext db = null;

            try
            {
                // Perform a SWAPI call
                WDAPIObject wdapi = new WDAPIObject(StudyData.study.StudyWorksUsername, StudyData.study.StudyWorksPassword, StudyData.study.Name, StudyData.study.StudyWorksBaseUrl);
                string swResponse = wdapi.GetData(SWAPIConstants.SPSyndGetSubjectsByKrDom, parameter).response;

                if (string.IsNullOrEmpty(swResponse))
                {
                    List<SubjectResult2Model> emptyList = new List<SubjectResult2Model>();
                    return emptyList;
                }

                List<SubjectResult2Model> subjects = XDocument.Parse(swResponse)
                                                    .Root
                                                    .Elements("subject")
                                                    .Select(s => new SubjectResult2Model
                                                    {
                                                        SubjectId = s.Attribute("subjectId").Value,
                                                        Krpt = s.Attribute("krpt").Value,
                                                        Initials = s.Attribute("initials").Value,
                                                        Language = s.Attribute("language").Value,
                                                        Phase = s.Attribute("phase").Value,
                                                        PhaseStartDate = DateTimeHelper.FormatDateTime(s.Attribute("phaseDate").Value, s.Attribute("phaseTZOffset").Value),
                                                        Deleted = Convert.ToBoolean(Convert.ToByte(s.Attribute("deleted").Value)),
                                                        DOB = s.Attribute("dob").Value,
                                                        Custom1 = s.Attribute("custom1").Value,
                                                        Custom2 = s.Attribute("custom2").Value,
                                                        Custom3 = s.Attribute("custom3").Value,
                                                        Custom4 = s.Attribute("custom4").Value,
                                                        Custom5 = s.Attribute("custom5").Value,
                                                        Custom6 = s.Attribute("custom6").Value,
                                                        Custom7 = s.Attribute("custom7").Value,
                                                        Custom8 = s.Attribute("custom8").Value,
                                                        Custom9 = s.Attribute("custom9").Value,
                                                        Custom10 = s.Attribute("custom10").Value,
                                                        LPStartDate = DateTimeHelper.FormatDateTime(s.Attribute("LPStartDate").Value, s.Attribute("origKrtz").Value),
                                                        LPEndDate = DateTimeHelper.FormatDateTime(s.Attribute("LPEndDate").Value, s.Attribute("origKrtz").Value),
                                                        SPStartDate = DateTimeHelper.FormatDateTime(s.Attribute("SPStartDate").Value, s.Attribute("origKrtz").Value),
                                                        SPEndDate = DateTimeHelper.FormatDateTime(s.Attribute("SPEndDate").Value, s.Attribute("origKrtz").Value),
                                                        EnrollmentDate = DateTimeHelper.FormatDateTime(s.Attribute("enrollDate").Value, s.Attribute("origKrtz").Value),
                                                        Updated = Convert.ToDateTime(s.Attribute("updated").Value, CultureInfo.InvariantCulture).ToString(SWAPIConstants.UTCDateTimeFormat, CultureInfo.InvariantCulture),
                                                        UpdatedForComparison = DateTime.SpecifyKind(Convert.ToDateTime(s.Attribute("updated").Value, CultureInfo.InvariantCulture), DateTimeKind.Utc)
                                                    })
                                                    .ToList();


                //Get subject data from NetPRO database
                db = new WebDiaryContext(WebDiaryContext.ConnectionString);
                var npSubjects = (from s in db.Subjects
                                  where s.StudyId == principal.StudyId
                                  orderby s.KrPT
                                  select new
                                  {
                                      krpt = s.KrPT,
                                      clientPassword = s.ClientPassword,
                                      password = s.Password,
                                      setupCode = s.Pin,
                                      securityQuestion = s.SecurityQuestion,
                                      securityAnswer = s.SecurityAnswer,
                                      lastPasswordChanged = s.LastPasswordChangedDate.HasValue? DateTime.SpecifyKind(s.LastPasswordChangedDate.Value, DateTimeKind.Utc) : DateTime.MinValue,
                                      lastSecurityQAChanged = s.LastSecurityQuestionAnswerChanged.HasValue? DateTime.SpecifyKind(s.LastSecurityQuestionAnswerChanged.Value, DateTimeKind.Utc) : DateTime.MinValue                           
                                  }).ToList();

                if (npSubjects.Count() > 0)
                {
                    foreach (SubjectResult2Model subject in subjects)
                    {
                        var npSubject = npSubjects.Find(a => a.krpt == subject.Krpt);
                        if (npSubject != null)
                        {
                            if (string.IsNullOrEmpty(npSubject.clientPassword))
                                subject.ClientPassword = "";
                            else
                                subject.ClientPassword = npSubject.clientPassword;

                            if (string.IsNullOrEmpty(npSubject.password))
                                subject.Password = "";
                            else
                                subject.Password = npSubject.password;

                            if (npSubject.setupCode.HasValue)
                                subject.SetupCode = npSubject.setupCode.Value;
                            else
                                subject.SetupCode = null;

                            if (npSubject.securityQuestion.HasValue)
                                subject.SecurityQuestion = npSubject.securityQuestion.Value;
                            else
                                subject.SecurityQuestion = null;

                            if (string.IsNullOrEmpty(npSubject.securityAnswer))
                                subject.SecurityAnswer = "";
                            else
                                subject.SecurityAnswer = npSubject.securityAnswer;

                            if (npSubject.lastPasswordChanged.CompareTo(npSubject.lastSecurityQAChanged) > 0)
                            {
                                if (npSubject.lastPasswordChanged.CompareTo(subject.UpdatedForComparison) > 0)
                                    subject.Updated = npSubject.lastPasswordChanged.ToString(SWAPIConstants.UTCDateTimeFormat, CultureInfo.InvariantCulture);
                            }
                            else
                            {
                                if (npSubject.lastSecurityQAChanged.CompareTo(subject.UpdatedForComparison) > 0)
                                    subject.Updated = npSubject.lastSecurityQAChanged.ToString(SWAPIConstants.UTCDateTimeFormat, CultureInfo.InvariantCulture);
                            }
                       }
                    }
                }

                db.Dispose();

                if (timeStamp != DateTime.MinValue)
                {
                    //Filter returned subjects 
                    //because time stamps in StudyWorks database uses DateTime data type whose precision can not handle exact milisecond resolution
                    List<SubjectResult2Model> filteredSubjects = new List<SubjectResult2Model>();
                    filteredSubjects = subjects.FindAll(u => DateTime.ParseExact(u.Updated, SWAPIConstants.UTCDateTimeFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal) >= timeStamp);
                    return filteredSubjects;
                }
                else
                    return subjects;
            }
            catch (Exception ex)
            {
                if (db != null)
                    db.Dispose();

                logger.Error(ex.Message, ex);
                HttpResponseException responseException = new HttpResponseException(HttpStatusCode.InternalServerError);
                if (ex is SqlException || ex is WebException)
                {
                    responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.ServerError);
                }
                throw responseException;
            }
        }

        // GET api/<controller>/5        
        public HttpResponseMessage Get(string id, [FromUri]string fields="")
        {
            Regex regex = new Regex(@"^\d+$");
            Match match = regex.Match(id);
            bool isDigit = false;

            if (match.Success)
            {
                isDigit = true;
            }

            if (!isDigit)
            {
                SubjectRepository subjectRep = new SubjectRepository(db);
                Subject subject = subjectRep.FindByKrptAndStudy(id, StudyData.study.Id);

                if (subject == null)
                {
                    logger.Error(String.Format(CultureInfo.InvariantCulture, "Subject does not exist in the study. Krpt in incoming url: {0}, study name: {1}.", id, StudyData.study.Name));
                    HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                    responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.InvalidKrPT);
                    throw responseException;
                }                

                try
                {
                    if (User.Identity.IsAuthenticated)
                    {
                        WebApiPrincipal principal = Thread.CurrentPrincipal as WebApiPrincipal;
                        if (!principal.ApiTokenAuth)
                        {
                            Subject subject2 = subjectRep.FindById(principal.SubjectID);
                            if (!string.Equals(id, subject2.KrPT, StringComparison.OrdinalIgnoreCase))
                            {
                                logger.Error(String.Format(CultureInfo.InvariantCulture, "Krpt in incoming url does not match krpt of the authorized subject. Krpt in incoming url: {0}, krpt of the authorized subject: {1}, study name: {2}.", id, subject2.KrPT, StudyData.study.Name));
                                throw new HttpResponseException(HttpStatusCode.Unauthorized);
                            }
                        }

                        // Perform a SWAPI call
                        WDAPIObject wdapi = new WDAPIObject(StudyData.study.StudyWorksUsername, StudyData.study.StudyWorksPassword, StudyData.study.Name, StudyData.study.StudyWorksBaseUrl);

                        ArrayList parameter = new ArrayList();
                        parameter.Add(id);
                        string swResponse = wdapi.GetData(SWAPIConstants.SPSyndGetSubjectByKrPT, parameter).response;

                        if (string.IsNullOrEmpty(swResponse))
                        {
                            logger.Error(String.Format(CultureInfo.InvariantCulture, "StudyWorks returned empty data. Subject does not exist in the study. Krpt in incoming url: {0}, study name: {1}.", id, StudyData.study.Name));
                            HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                            responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.InvalidKrPT);
                            throw responseException;                           
                        }

                        ExtendedSubjectResultModel subjectResult = XDocument.Parse(swResponse)
                                                            .Descendants("subject")
                                                            .Select(s => new ExtendedSubjectResultModel
                                                            {
                                                                SubjectId = s.Attribute("subjectId").Value,
                                                                Initials = s.Attribute("initials").Value,
                                                                Language = s.Attribute("language").Value,
                                                                Krdom = s.Attribute("krdom").Value,
                                                                SiteCode = s.Attribute("siteCode").Value,
                                                                Phase = s.Attribute("phase").Value,
                                                                PhaseStartDate = DateTimeHelper.FormatDateTime(s.Attribute("phaseDate").Value, s.Attribute("phaseTZOffset").Value),
                                                                Deleted = Convert.ToBoolean(Convert.ToByte(s.Attribute("deleted").Value)),
                                                                DOB = s.Attribute("dob").Value,
                                                                Gender = s.Attribute("gender").Value,
                                                                Custom1 = s.Attribute("custom1").Value,
                                                                Custom2 = s.Attribute("custom2").Value,
                                                                Custom3 = s.Attribute("custom3").Value,
                                                                Custom4 = s.Attribute("custom4").Value,
                                                                Custom5 = s.Attribute("custom5").Value,
                                                                Custom6 = s.Attribute("custom6").Value,
                                                                Custom7 = s.Attribute("custom7").Value,
                                                                Custom8 = s.Attribute("custom8").Value,
                                                                Custom9 = s.Attribute("custom9").Value,
                                                                Custom10 = s.Attribute("custom10").Value,
                                                                LPStartDate = DateTimeHelper.FormatDateTime(s.Attribute("LPStartDate").Value, s.Attribute("origKrtz").Value),
                                                                LPEndDate = DateTimeHelper.FormatDateTime(s.Attribute("LPEndDate").Value, s.Attribute("origKrtz").Value),
                                                                SPStartDate = DateTimeHelper.FormatDateTime(s.Attribute("SPStartDate").Value, s.Attribute("origKrtz").Value),
                                                                SPEndDate = DateTimeHelper.FormatDateTime(s.Attribute("SPEndDate").Value, s.Attribute("origKrtz").Value),
                                                                EnrollmentDate = DateTimeHelper.FormatDateTime(s.Attribute("enrollDate").Value, s.Attribute("origKrtz").Value),
                                                                ActivationDate = DateTimeHelper.FormatDateTime(s.Attribute("activationDate").Value, s.Attribute("suOrigKrtz").Value),
                                                                Updated = Convert.ToDateTime(s.Attribute("updated").Value, CultureInfo.InvariantCulture).ToString(SWAPIConstants.UTCDateTimeFormat, CultureInfo.InvariantCulture),
                                                                UpdatedForComparison = DateTime.SpecifyKind(Convert.ToDateTime(s.Attribute("updated").Value, CultureInfo.InvariantCulture), DateTimeKind.Utc)
                                                            }).SingleOrDefault();

                            if (string.IsNullOrEmpty(subject.ClientPassword))
                                subjectResult.ClientPassword = "";
                            else
                                subjectResult.ClientPassword = subject.ClientPassword;

                            if (string.IsNullOrEmpty(subject.Password))
                                subjectResult.Password = "";
                            else
                                subjectResult.Password = subject.Password;

                            if (subject.Pin.HasValue)
                                subjectResult.SetupCode = subject.Pin.Value;
                            else
                                subjectResult.SetupCode = null;

                            if (subject.SecurityQuestion.HasValue)
                                subjectResult.SecurityQuestion = subject.SecurityQuestion.Value;
                            else
                                subjectResult.SecurityQuestion = null;

                            if (string.IsNullOrEmpty(subject.SecurityAnswer))
                                subjectResult.SecurityAnswer = "";
                            else
                                subjectResult.SecurityAnswer = subject.SecurityAnswer;

                            if (subject.SubjectMiddleTier == null)
                                subjectResult.LogLevel = "";
                            else
                                subjectResult.LogLevel = subject.SubjectMiddleTier.NetproLogLevel.LogLevelName;

                            subjectResult.IsActive = !string.IsNullOrEmpty(subject.Password);

                            DateTime lastPasswordChanged = subject.LastPasswordChangedDate.HasValue ? DateTime.SpecifyKind(subject.LastPasswordChangedDate.Value, DateTimeKind.Utc) : DateTime.MinValue;
                            DateTime lastSecurityQAChanged = subject.LastSecurityQuestionAnswerChanged.HasValue ? DateTime.SpecifyKind(subject.LastSecurityQuestionAnswerChanged.Value, DateTimeKind.Utc) : DateTime.MinValue;

                            if (lastPasswordChanged.CompareTo(lastSecurityQAChanged) > 0)
                            {
                                if (lastPasswordChanged.CompareTo(subjectResult.UpdatedForComparison) > 0)
                                    subjectResult.Updated = lastPasswordChanged.ToString(SWAPIConstants.UTCDateTimeFormat, CultureInfo.InvariantCulture);
                            }
                            else
                            {
                                if (lastSecurityQAChanged.CompareTo(subjectResult.UpdatedForComparison) > 0)
                                    subjectResult.Updated = lastSecurityQAChanged.ToString(SWAPIConstants.UTCDateTimeFormat, CultureInfo.InvariantCulture);
                            }

                            HttpResponseMessage responseMsg = Request.CreateResponse<ExtendedSubjectResultModel>(HttpStatusCode.OK, subjectResult);
                            return responseMsg;
                    }
                    else
                    {
                        WDAPIObject wdapi = new WDAPIObject(StudyData.study.StudyWorksUsername, StudyData.study.StudyWorksPassword, StudyData.study.Name, StudyData.study.StudyWorksBaseUrl);
                        ExtendedSubjectResultModel subjectResult = new ExtendedSubjectResultModel();
                        subjectResult.IsActive = !string.IsNullOrEmpty(subject.Password);
                        subjectResult.Language = WDAPIObject.JavaLocaleToDotNetCulture(GetSubjectLanguage(wdapi, subject.KrPT));

                        HttpResponseMessage responseMsg = Request.CreateResponse<ExtendedSubjectResultModel>(HttpStatusCode.OK, subjectResult);
                        return responseMsg;
                    }
                }
                catch (HttpResponseException resEx)
                {
                    throw resEx;
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message, ex);
                    HttpResponseException responseException = new HttpResponseException(HttpStatusCode.InternalServerError);
                    if (ex is SqlException || ex is WebException)
                    {
                        responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.ServerError);
                    }
                    throw responseException;
                }
            }
            else
            {

                SubjectResultModel subjectResult = new SubjectResultModel();
                int clientSyncId = -1, serverSyncId = 0;

                try
                {
                    SubjectRepository subjectRep = new SubjectRepository(db);
                    Subject subject = subjectRep.FindByPinAndStudy(Convert.ToInt64(id), StudyData.study.Id);
                    WDAPIObject wdapi = new WDAPIObject(StudyData.study.StudyWorksUsername, StudyData.study.StudyWorksPassword, StudyData.study.Name, StudyData.study.StudyWorksBaseUrl);
                    bool hasDCF = false;

                    if (subject == null)
                    {
                        return Request.CreateResponse<SubjectResultModel>(HttpStatusCode.OK, new SubjectResultModel());
                    }

                    //Return StatusCode 403 and x-error-code 6
                    //If subject is not enabled
                    //If any data is populated in LPEndDate column of lookup_pt table in StudyWorks table
                    if (!wdapi.IsSubjectEnabled(subject.KrPT) || !string.IsNullOrEmpty(GetLPEndDate(wdapi, subject.KrPT)))
                    {
                        HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                        responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.UserNotFound);
                        throw responseException;
                    }

                    if (User.Identity.IsAuthenticated) // from activated device
                    {
                        // Retrieve the principal
                        WebApiPrincipal principal = Thread.CurrentPrincipal as WebApiPrincipal;

                        if (!principal.ApiTokenAuth && Convert.ToInt64(id) != principal.UserID)
                            throw new HttpResponseException(HttpStatusCode.Unauthorized);

                        // get client sync id
                        IEnumerable<string> headers;
                        if (Request.Headers.TryGetValues("X-Sync-ID", out headers))
                        {
                            if (!int.TryParse(headers.FirstOrDefault<string>(), out clientSyncId))
                                throw new HttpResponseException(HttpStatusCode.BadRequest);
                        }

                        // get server sync id
                        if (subject.SubjectMiddleTier != null)
                            serverSyncId = subject.SubjectMiddleTier.SyncID;

                        // Get subject data from StudyWorks
                        SubjectData subjectData = wdapi.GetSubjectData(subject.KrPT,
                                                                new ArrayList {SWAPIConstants.ColumnPatientId,
                                                        SWAPIConstants.ColumnPatientInitials,
                                                        SWAPIConstants.ColumnPatientKrPhase,
                                                        SWAPIConstants.ColumnPatientKrDom,
                                                        SWAPIConstants.ColumnPatientPhaseDate,
                                                        SWAPIConstants.ColumnPatientPhaseTZOffset,
                                                        SWAPIConstants.ColumnPatientEnrollDate,
                                                        SWAPIConstants.ColumnPatientOrigKrTz});

                        SimpleResponse siteCodeResponse = wdapi.GetData("customGetSiteCodeFromkrDOM", new ArrayList { subjectData.data.krdom });

                        if (string.IsNullOrEmpty(fields))
                        {
                            subjectResult.IsActive = !string.IsNullOrEmpty(subject.Password);
                            subjectResult.KrPT = subject.KrPT;

                            if (subject.SWPatientInitials != subjectData.data.Initials)
                            {
                                subjectResult.Initials = subject.SWPatientInitials = subjectData.data.Initials;
                                hasDCF = true;
                            }

                            if (subject.SWPatientId != subjectData.data.Patientid)
                            {
                                subjectResult.SubjectNumber = subject.SWPatientId = subjectData.data.Patientid;
                                hasDCF = true;
                            }

                            if (!subject.Phase.HasValue || subject.Phase.Value.ToString(CultureInfo.InvariantCulture) != subjectData.data.Krphase)
                            {
                                subjectResult.Phase = subject.Phase = Convert.ToInt32(subjectData.data.Krphase, CultureInfo.InvariantCulture);
                                hasDCF = true;
                            }

                            if (subject.SiteCode != siteCodeResponse.response)
                            {
                                subjectResult.SiteCode = subject.SiteCode = siteCodeResponse.response;
                                hasDCF = true;
                            }

                            if (subject.SubjectMiddleTier.LogLevel != subject.SubjectMiddleTier.ClientLogLevel)
                            {
                                subject.SubjectMiddleTier.ClientLogLevel = subject.SubjectMiddleTier.LogLevel;
                                subjectResult.LogLevel = subject.SubjectMiddleTier.NetproLogLevel.LogLevelName;
                                hasDCF = true;
                            }

                            DateTimeOffset swPhaseStartDate;
                            if (NeedSyncPhaseStartDate(subjectData, subject.SubjectMiddleTier.PhaseStartDate, out swPhaseStartDate))
                            {
                                subjectResult.PhaseStartDate = subject.SubjectMiddleTier.PhaseStartDate = swPhaseStartDate;
                                hasDCF = true;
                            }

                            DateTimeOffset swEnrollDate;
                            if (NeedSyncEnrollmentDate(subjectData, subject.SubjectMiddleTier.EnrollmentDate, out swEnrollDate))
                            {
                                subjectResult.EnrollmentDate = subject.SubjectMiddleTier.EnrollmentDate = swEnrollDate;
                                hasDCF = true;
                            }

                            DateTime startDateTime = DateTime.Parse(subjectData.data.enrolldate, CultureInfo.InvariantCulture).AddDays(-2);
                            DateTimeOffset activationDate = GetActivationDate(subject.KrPT, startDateTime);
                            if (!principal.ApiTokenAuth && activationDate.CompareTo(DateTimeOffset.MinValue) == 0)
                            {
                                //throw error if no activation report in Study Works
                                throw new NetProException(SWISStatusCodes.UserIsNotActive);
                            }
                            else if (activationDate.CompareTo(DateTimeOffset.MinValue) == 0)
                            {
                                //skip if no activation form
                            }
                            else
                            {
                                if (!subject.SubjectMiddleTier.ActivationDate.HasValue || NeedSyncActivationDate(subject.SubjectMiddleTier.ActivationDate, activationDate))
                                {
                                    subjectResult.ActivationDate = subject.SubjectMiddleTier.ActivationDate = activationDate;
                                    hasDCF = true;
                                }
                            }

                            string subjectLanguage = GetSubjectLanguage(wdapi, subject.KrPT);
                            subjectResult.Language = WDAPIObject.JavaLocaleToDotNetCulture(subjectLanguage);

                            if (subject.Language != subjectLanguage)
                            {
                                subject.Language = subjectLanguage;
                                hasDCF = true;
                            }

                            if (string.IsNullOrEmpty(subject.Password))
                                subjectResult.Password = "";
                            else
                                subjectResult.Password = subject.Password;

                            if (string.IsNullOrEmpty(subject.ClientPassword))
                                subjectResult.ClientPassword = "";
                            else
                                subjectResult.ClientPassword = subject.ClientPassword;

                            if (clientSyncId < serverSyncId)
                            {
                                subjectResult.Initials = subject.SWPatientInitials;
                                subjectResult.SubjectNumber = subject.SWPatientId;
                                subjectResult.Phase = subject.Phase;
                                subjectResult.SiteCode = subject.SiteCode;
                                subjectResult.LogLevel = subject.SubjectMiddleTier.NetproLogLevel.LogLevelName;
                                subjectResult.PhaseStartDate = subject.SubjectMiddleTier.PhaseStartDate;
                                subjectResult.EnrollmentDate = subject.SubjectMiddleTier.EnrollmentDate;
                                if (activationDate.CompareTo(DateTimeOffset.MinValue) != 0)
                                    subjectResult.ActivationDate = subject.SubjectMiddleTier.ActivationDate;
                            }
                        }
                        else
                        {
                            var fieldList = fields.Split(',');

                            if (fieldList.Contains("IsActive"))
                                subjectResult.IsActive = !string.IsNullOrEmpty(subject.Password);

                            if (fieldList.Contains("KrPT"))
                                subjectResult.KrPT = subject.KrPT;

                            if (fieldList.Contains("Initials") && subject.SWPatientInitials != subjectData.data.Initials)
                            {
                                subjectResult.Initials = subject.SWPatientInitials = subjectData.data.Initials;
                                hasDCF = true;
                            }

                            if (fieldList.Contains("SubjectNumber") && subject.SWPatientId != subjectData.data.Patientid)
                            {
                                subjectResult.SubjectNumber = subject.SWPatientId = subjectData.data.Patientid;
                                hasDCF = true;
                            }

                            if (fieldList.Contains("Phase") && (!subject.Phase.HasValue || subject.Phase.Value.ToString(CultureInfo.InvariantCulture) != subjectData.data.Krphase))
                            {
                                subjectResult.Phase = subject.Phase = Convert.ToInt32(subjectData.data.Krphase, CultureInfo.InvariantCulture);
                                hasDCF = true;
                            }

                            if (fieldList.Contains("SiteCode") && subject.SiteCode != siteCodeResponse.response)
                            {
                                subjectResult.SiteCode = subject.SiteCode = siteCodeResponse.response;
                                hasDCF = true;
                            }

                            if (fieldList.Contains("LogLevel") && subject.SubjectMiddleTier.LogLevel != subject.SubjectMiddleTier.ClientLogLevel)
                            {
                                subject.SubjectMiddleTier.ClientLogLevel = subject.SubjectMiddleTier.LogLevel;
                                subjectResult.LogLevel = subject.SubjectMiddleTier.NetproLogLevel.LogLevelName;
                                hasDCF = true;
                            }

                            DateTimeOffset swPhaseStartDate;
                            if (fieldList.Contains("PhaseStartDate") && NeedSyncPhaseStartDate(subjectData, subject.SubjectMiddleTier.PhaseStartDate, out swPhaseStartDate))
                            {
                                subjectResult.PhaseStartDate = subject.SubjectMiddleTier.PhaseStartDate = swPhaseStartDate;
                                hasDCF = true;
                            }

                            DateTimeOffset swEnrollDate;
                            if (fieldList.Contains("EnrollmentDate") && NeedSyncEnrollmentDate(subjectData, subject.SubjectMiddleTier.EnrollmentDate, out swEnrollDate))
                            {
                                subjectResult.EnrollmentDate = subject.SubjectMiddleTier.EnrollmentDate = swEnrollDate;
                                hasDCF = true;
                            }

                            DateTime startDateTime = DateTime.Parse(subjectData.data.enrolldate, CultureInfo.InvariantCulture).AddDays(-2);
                            DateTimeOffset activationDate = GetActivationDate(subject.KrPT, startDateTime);
                            if (!principal.ApiTokenAuth && activationDate.CompareTo(DateTimeOffset.MinValue) == 0)
                            {
                                //throw error if no activation report in Study Works
                                throw new NetProException(SWISStatusCodes.UserIsNotActive);
                            }
                            else if (activationDate.CompareTo(DateTimeOffset.MinValue) == 0)
                            {
                                //skip if no activation form
                            }
                            else
                            {
                                if (fieldList.Contains("ActivationDate") && (!subject.SubjectMiddleTier.ActivationDate.HasValue || NeedSyncActivationDate(subject.SubjectMiddleTier.ActivationDate, activationDate)))
                                {
                                    subjectResult.ActivationDate = subject.SubjectMiddleTier.ActivationDate = activationDate;
                                    hasDCF = true;
                                }
                            }

                            string subjectLanguage = GetSubjectLanguage(wdapi, subject.KrPT);
                            if (fieldList.Contains("Language"))
                            {
                                subjectResult.Language = WDAPIObject.JavaLocaleToDotNetCulture(subjectLanguage);
                                if (subject.Language != subjectLanguage)
                                {
                                    subject.Language = subjectLanguage;
                                    hasDCF = true;
                                }
                            }

                            if (fieldList.Contains("Password"))
                            {
                                if (string.IsNullOrEmpty(subject.Password))
                                    subjectResult.Password = "";
                                else
                                    subjectResult.Password = subject.Password;
                            }

                            if (fieldList.Contains("ClientPassword"))
                            {
                                if (string.IsNullOrEmpty(subject.ClientPassword))
                                    subjectResult.ClientPassword = "";
                                else
                                    subjectResult.ClientPassword = subject.ClientPassword;
                            }


                            if (clientSyncId < serverSyncId)
                            {
                                if (fieldList.Contains("Initials"))
                                    subjectResult.Initials = subject.SWPatientInitials;
                                if (fieldList.Contains("SubjectNumber"))
                                    subjectResult.SubjectNumber = subject.SWPatientId;
                                if (fieldList.Contains("Phase"))
                                    subjectResult.Phase = subject.Phase;
                                if (fieldList.Contains("SiteCode"))
                                    subjectResult.SiteCode = subject.SiteCode;
                                if (fieldList.Contains("LogLevel"))
                                    subjectResult.LogLevel = subject.SubjectMiddleTier.NetproLogLevel.LogLevelName;
                                if (fieldList.Contains("PhaseStartDate"))
                                    subjectResult.PhaseStartDate = subject.SubjectMiddleTier.PhaseStartDate;
                                if (fieldList.Contains("EnrollmentDate"))
                                    subjectResult.EnrollmentDate = subject.SubjectMiddleTier.EnrollmentDate;
                                if (fieldList.Contains("ActivationDate") && activationDate.CompareTo(DateTimeOffset.MinValue) != 0)
                                    subjectResult.ActivationDate = subject.SubjectMiddleTier.ActivationDate;
                            }
                        }
                    }
                    else // anonymous request
                    {
                        if (!string.IsNullOrEmpty(fields))
                        {
                            var fieldList = fields.Split(',');
                            if (fieldList.Contains("Initials")
                                || fieldList.Contains("SubjectNumber")
                                || fieldList.Contains("Phase")
                                || fieldList.Contains("SiteCode")
                                || fieldList.Contains("LogLevel")
                                || fieldList.Contains("PhaseStartDate")
                                || fieldList.Contains("EnrollmentDate")
                                || fieldList.Contains("ActivationDate")
                                || fieldList.Contains("Password")
                                || fieldList.Contains("ClientPassword"))
                            {
                                throw new HttpResponseException(HttpStatusCode.Unauthorized);
                            }

                            if (fieldList.Contains("IsActive"))
                                subjectResult.IsActive = !string.IsNullOrEmpty(subject.Password);

                            if (fieldList.Contains("Language"))
                                subjectResult.Language = WDAPIObject.JavaLocaleToDotNetCulture(GetSubjectLanguage(wdapi, subject.KrPT));

                            if (fieldList.Contains("KrPT"))
                                subjectResult.KrPT = subject.KrPT;
                        }
                        else
                        {
                            subjectResult.IsActive = !string.IsNullOrEmpty(subject.Password);
                            subjectResult.Language = WDAPIObject.JavaLocaleToDotNetCulture(GetSubjectLanguage(wdapi, subject.KrPT));
                            subjectResult.KrPT = subject.KrPT;
                        }
                    }

                    if (hasDCF)
                    {
                        serverSyncId++;
                        subject.SubjectMiddleTier.SyncID = serverSyncId;
                        db.SubmitChanges();
                    }
                }
                catch (HttpResponseException resEx)
                {
                    throw resEx;
                }
                catch (NetProException npEx)
                {
                    logger.Error(npEx.Message, npEx);
                    HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                    if (npEx.Message == SWISStatusCodes.UserIsNotActive)
                    {
                        responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.UserIsNotActive);
                    }
                    throw responseException;
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message, ex);
                    HttpResponseException responseException = new HttpResponseException(HttpStatusCode.InternalServerError);
                    if (ex is SqlException || ex is WebException)
                    {
                        responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.ServerError);
                    }
                    throw responseException;
                }

                HttpResponseMessage responseMsg = Request.CreateResponse<SubjectResultModel>(HttpStatusCode.OK, subjectResult);

                responseMsg.Headers.Add("X-Sync-ID", serverSyncId.ToString());
                return responseMsg;
            }
        }

        public HttpResponseMessage Post([FromBody] AssignmentFormModel form)
        {
            WebApiPrincipal principal = Thread.CurrentPrincipal as WebApiPrincipal;           
            WebDiaryContext db = null;

            if (!principal.ApiTokenAuth)
            {
                HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                throw responseException;
            }

            ReportData assignment = new ReportData();

            if (!string.IsNullOrEmpty(form.SigID))
            {
                if (!form.SigID.StartsWith(SWAPIConstants.SigidPrefixForSitePadApp, false, CultureInfo.InvariantCulture))
                {
                    logger.Error(String.Format(CultureInfo.InvariantCulture, "Sigid from SitePad App must start with \"" + SWAPIConstants.SigidPrefixForSitePadApp + "\".  Sigid: {0} for krdom: {1}, krsu: {2}", form.SigID, principal.KrDom, form.SU));
                    HttpResponseException responseException = new HttpResponseException(HttpStatusCode.BadRequest);
                    throw responseException;
                }
                else
                {
                    assignment.Sigid = form.SigID;
                    assignment.Sigorig = form.SigID;
                    assignment.Sigprev = string.Empty;
                }
            }
            else
            {
                string sigid = SWAPIConstants.NetPROGeneratedSigidPrefix + WDAPIObject.GenerateSignatureId();
                assignment.Sigid = sigid;
                assignment.Sigorig = sigid;
                assignment.Sigprev = string.Empty;
            }

            try
            {
               db = new WebDiaryContext(WebDiaryContext.ConnectionString);

                StudyRepository studyRepo = new StudyRepository(db);
                Study study = studyRepo.FindById(principal.StudyId);

                DateTime assignmentStartUtc = DateTime.SpecifyKind(form.Started, DateTimeKind.Utc);
                DateTime assignmentCompleteUtc = DateTime.SpecifyKind(form.Completed, DateTimeKind.Utc);
                DateTime assignmentReportDate = DateTime.SpecifyKind(form.ReportDate, DateTimeKind.Local);
                assignment.Krdom = principal.KrDom;
                assignment.Krpt = form.KrPT;
                assignment.Krsu = form.SU;
                assignment.StudyId = study.Id;
                assignment.PrintedName = form.ResponsibleParty;
                assignment.SuReportDate = assignmentReportDate;
                assignment.SuReportStartDate = assignmentStartUtc;
                assignment.ReportCompleteDate = assignmentCompleteUtc;
                assignment.SuPhaseAtEntry = form.Phase;
                assignment.Krphase = form.Phase;
                assignment.SuPhaseStartDate = DateTime.SpecifyKind(form.PhaseStartDate.UtcDateTime, DateTimeKind.Utc);
                assignment.Source = SWAPIConstants.SourceSitePadApp;
                assignment.Kruser = study.StudyWorksUsername;

                // Construct answer records
                string affidavit = string.Empty;
                Dictionary<string, string> answers = new Dictionary<string, string>();

                foreach (var answerContract in form.Answers)
                {
                    if (answerContract.QuestionID == DiaryAnswerDataContract.AffidavitQuestionId)
                    {
                        affidavit = answerContract.SWAlias;
                    }
                    else
                        answers.Add(answerContract.SWAlias, answerContract.Response);
                }

                assignment.Affidavit = affidavit;

                //Battery Level
                if (!string.IsNullOrEmpty(form.BatteryLevel))
                {
                    answers.Add(SWAPIConstants.SuBatteryLevel, form.BatteryLevel);
                }

                //SWDeviceId
                if (principal.SWDeviceId.HasValue)
                {
                    answers.Add(SWAPIConstants.SuLogPadDeviceId, principal.SWDeviceId.Value.ToString(CultureInfo.InvariantCulture));
                }

                //SuLPVersion
                if (!string.IsNullOrEmpty(form.StudyVersion))
                {
                    answers.Add(SWAPIConstants.SuLPVersion, form.StudyVersion);
                }

                Ink ink = null;
                if (form.Ink != null)
                {
                    ink = new Ink();
                    ink.xsize = form.Ink.Xsize;
                    ink.ysize = form.Ink.Ysize;
                    ink.value = form.Ink.Vector.ToString(CultureInfo.InvariantCulture);
                }

                string clientBrowserPlatForm = ((System.Web.HttpContextWrapper)Request.Properties["MS_HttpContext"]).Request.Browser.Platform;
                string clientBrowserType = ((System.Web.HttpContextWrapper)Request.Properties["MS_HttpContext"]).Request.Browser.Type;
                string clientBrowserVersion = ((System.Web.HttpContextWrapper)Request.Properties["MS_HttpContext"]).Request.Browser.Version;

                //NPVersion
                // Set NetProInformation IG - Note: Needs to be customized here and in StudyWorks to distinguish LF from NetPRO
                VersionsRepository verResp = new VersionsRepository(db);
                Versions npVer = verResp.FindById(1);
                StudyVersions studyVer = study.StudyVersions;

                string npVersion = npVer.MicroVersion == 0 ? npVer.MajorVersion + "." + npVer.MinorVersion : npVer.MajorVersion + "." + npVer.MinorVersion + "." + npVer.MicroVersion;
                answers.Add(SWAPIConstants.IGNPI + ".0." + SWAPIConstants.ITNPVersion, npVersion);
                answers.Add(SWAPIConstants.IGNPI + ".0." + SWAPIConstants.ITNPStudyVersion, studyVer.MajorVersion + "." + studyVer.MinorVersion);
                answers.Add(SWAPIConstants.IGNPI + ".0." + SWAPIConstants.ITOSVersion, clientBrowserPlatForm);
                answers.Add(SWAPIConstants.IGNPI + ".0." + SWAPIConstants.ITBrowserName, clientBrowserType);
                answers.Add(SWAPIConstants.IGNPI + ".0." + SWAPIConstants.ITBrowserVersion, clientBrowserVersion);
                answers.Add(SWAPIConstants.IGNPI + ".0." + SWAPIConstants.ITSitePadAppCoreVersion, form.CoreVersion);
                answers.Add(SWAPIConstants.IGNPI + ".0." + SWAPIConstants.ITSitePadAppStudyVersion, form.StudyVersion);
                answers.Add(SWAPIConstants.IGNPI + ".0." + SWAPIConstants.ITClientID, principal.DeviceID.ToString());
                answers.Add(SWAPIConstants.IGNPI + ".0." + SWAPIConstants.ITDateTimeSubmitted, principal.ClientRequestTime.UtcDateTime.ToString(SWAPIConstants.SwDateTimeFormat, CultureInfo.InvariantCulture));
                answers.Add(SWAPIConstants.IGNPI + ".0." + SWAPIConstants.ITDateTimeReceived, principal.ServerReceiptTime.ToString(SWAPIConstants.SwDateTimeFormat, CultureInfo.InvariantCulture));

                ClinRecord clinRecord = WDAPIObject.GenerateClinicalRecordForSubjectAssignment(assignment, answers, ink, true);

                WDAPIObject wdapi = new WDAPIObject(study.StudyWorksUsername, study.StudyWorksPassword, study.Name, study.StudyWorksBaseUrl);
                wdapi.SubmitClinReportRRInk(clinRecord);

                db.Dispose();
            }
            catch (NetProException ex)
            {
                logger.Error(ex.Message, ex);
                db.Dispose();
                HttpResponseException responseException = new HttpResponseException(HttpStatusCode.BadRequest);
                throw responseException;
            }
            catch (SWAPIException ex)
            {
                logger.Error(ex.Message, ex);
                db.Dispose();

                //10028 - isPrimaryKeyError, krpt is primary key in lookup_su table in StudyWorks
                //10030 - DuplicatePatientIDError in StudyWorks
                //10391 - DuplicateSigIDError in StudyWorks
                if (String.Compare(ex.Error.error_message, "10028", StringComparison.InvariantCulture) == 0)
                {
                    HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                    responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.DuplicateKrpt);
                    throw responseException;
                }
                else if (String.Compare(ex.Error.error_message, "10030", StringComparison.InvariantCulture) == 0)
                {
                    HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                    responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.DuplicateSubjectId);
                    throw responseException;
                }
                else if (String.Compare(ex.Error.error_message, "10391", StringComparison.InvariantCulture) == 0)
                {
                    HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                    responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.DuplicateSigId);
                    throw responseException;
                }
                else
                {
                    HttpResponseException responseException = new HttpResponseException(HttpStatusCode.BadRequest);
                    throw responseException;
                }
            }
            catch (ArgumentException ex)
            {
                logger.Error(ex.Message, ex);
                db.Dispose();
                HttpResponseException responseException = new HttpResponseException(HttpStatusCode.BadRequest);
                throw responseException;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                db.Dispose();

                HttpResponseException responseException = new HttpResponseException(HttpStatusCode.InternalServerError);
                throw responseException;
            }

            AssignmentFormResult result = new AssignmentFormResult();
            result.KrPT = form.KrPT;

            return Request.CreateResponse<AssignmentFormResult>(HttpStatusCode.Created, result);
        }

        /// <summary>
        /// Subject Edit
        /// </summary>
        /// <param name="form">assignment form</param>
        /// <returns>HttpStatusCode with krpt</returns>
        [Authorize]
        public HttpResponseMessage Put(string id, [FromBody] AssignmentFormModel form)
        {
            WebApiPrincipal principal = Thread.CurrentPrincipal as WebApiPrincipal;
            WebDiaryContext db = null;

            if (!principal.ApiTokenAuth)
            {
                HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                throw responseException;
            }

            if (!string.Equals(id, form.KrPT, StringComparison.Ordinal))
            {
                logger.Error(String.Format(CultureInfo.InvariantCulture, "Krpt in request body does not match krpt in incoming url for subject edit. krpt in request body: {0}, krpt in incoming url: {1}, krdom: {2}, krsu: {3}", form.KrPT, id, principal.KrDom, form.SU));
                HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.KrptNotMatch);
                throw responseException;
            }

            ReportData assignment = new ReportData();

            if (!string.IsNullOrEmpty(form.SigID))
            {
                if (!form.SigID.StartsWith(SWAPIConstants.SigidPrefixForSitePadApp, false, CultureInfo.InvariantCulture))
                {
                    logger.Error(String.Format(CultureInfo.InvariantCulture, "Sigid from SitePad App must start with \"" + SWAPIConstants.SigidPrefixForSitePadApp + "\".  Sigid: {0} for krdom: {1}, krsu: {2}", form.SigID, principal.KrDom, form.SU));
                    HttpResponseException responseException = new HttpResponseException(HttpStatusCode.BadRequest);
                    throw responseException;
                }
                else
                {
                    assignment.Sigid = form.SigID;
                }
            }
            else
            {
                string sigid = SWAPIConstants.NetPROGeneratedSigidPrefix + WDAPIObject.GenerateSignatureId();
                assignment.Sigid = sigid;
            }

            try
            {
                db = new WebDiaryContext(WebDiaryContext.ConnectionString);

                StudyRepository studyRepo = new StudyRepository(db);
                Study study = studyRepo.FindById(principal.StudyId);

                DateTime assignmentStartUtc = DateTime.SpecifyKind(form.Started, DateTimeKind.Utc);
                DateTime assignmentCompleteUtc = DateTime.SpecifyKind(form.Completed, DateTimeKind.Utc);
                DateTime assignmentReportDate = DateTime.SpecifyKind(form.ReportDate, DateTimeKind.Local);
                assignment.Krpt = form.KrPT;
                assignment.Krsu = form.SU;
                assignment.StudyId = study.Id;
                assignment.PrintedName = form.ResponsibleParty;
                assignment.SuReportDate = assignmentReportDate;
                assignment.SuReportStartDate = assignmentStartUtc;
                assignment.ReportCompleteDate = assignmentCompleteUtc;
                assignment.SuPhaseAtEntry = form.Phase;
                assignment.Krphase = form.Phase;
                assignment.SuPhaseStartDate = DateTime.SpecifyKind(form.PhaseStartDate.UtcDateTime, DateTimeKind.Utc);
                assignment.Source = SWAPIConstants.SourceSitePadApp;
                assignment.Kruser = study.StudyWorksUsername;

                // Construct answer records
                string affidavit = string.Empty;
                Dictionary<string, string> answers = new Dictionary<string, string>();

                foreach (var answerContract in form.Answers)
                {
                    if (answerContract.QuestionID == DiaryAnswerDataContract.AffidavitQuestionId)
                    {
                        affidavit = answerContract.SWAlias;
                    }
                    else
                        answers.Add(answerContract.SWAlias, answerContract.Response);
                }

                assignment.Affidavit = affidavit;

                //Battery Level
                if (!string.IsNullOrEmpty(form.BatteryLevel))
                {
                    answers.Add(SWAPIConstants.SuBatteryLevel, form.BatteryLevel);
                }

                //SWDeviceId
                if (principal.SWDeviceId.HasValue)
                {
                    answers.Add(SWAPIConstants.SuLogPadDeviceId, principal.SWDeviceId.Value.ToString(CultureInfo.InvariantCulture));
                }

                //SuLPVersion
                if (!string.IsNullOrEmpty(form.StudyVersion))
                {
                    answers.Add(SWAPIConstants.SuLPVersion, form.StudyVersion);
                }

                Ink ink = null;
                if (form.Ink != null)
                {
                    ink = new Ink();
                    ink.xsize = form.Ink.Xsize;
                    ink.ysize = form.Ink.Ysize;
                    ink.value = form.Ink.Vector.ToString(CultureInfo.InvariantCulture);
                }

                string clientBrowserPlatForm = ((System.Web.HttpContextWrapper)Request.Properties["MS_HttpContext"]).Request.Browser.Platform;
                string clientBrowserType = ((System.Web.HttpContextWrapper)Request.Properties["MS_HttpContext"]).Request.Browser.Type;
                string clientBrowserVersion = ((System.Web.HttpContextWrapper)Request.Properties["MS_HttpContext"]).Request.Browser.Version;

                //NPVersion
                // Set NetProInformation IG - Note: Needs to be customized here and in StudyWorks to distinguish LF from NetPRO
                VersionsRepository verResp = new VersionsRepository(db);
                Versions npVer = verResp.FindById(1);
                StudyVersions studyVer = study.StudyVersions;

                string npVersion = npVer.MicroVersion == 0 ? npVer.MajorVersion + "." + npVer.MinorVersion : npVer.MajorVersion + "." + npVer.MinorVersion + "." + npVer.MicroVersion;
                answers.Add(SWAPIConstants.IGNPI + ".0." + SWAPIConstants.ITNPVersion, npVersion);
                answers.Add(SWAPIConstants.IGNPI + ".0." + SWAPIConstants.ITNPStudyVersion, studyVer.MajorVersion + "." + studyVer.MinorVersion);
                answers.Add(SWAPIConstants.IGNPI + ".0." + SWAPIConstants.ITOSVersion, clientBrowserPlatForm);
                answers.Add(SWAPIConstants.IGNPI + ".0." + SWAPIConstants.ITBrowserName, clientBrowserType);
                answers.Add(SWAPIConstants.IGNPI + ".0." + SWAPIConstants.ITBrowserVersion, clientBrowserVersion);
                answers.Add(SWAPIConstants.IGNPI + ".0." + SWAPIConstants.ITSitePadAppCoreVersion, form.CoreVersion);
                answers.Add(SWAPIConstants.IGNPI + ".0." + SWAPIConstants.ITSitePadAppStudyVersion, form.StudyVersion);
                answers.Add(SWAPIConstants.IGNPI + ".0." + SWAPIConstants.ITClientID, principal.DeviceID.ToString());
                answers.Add(SWAPIConstants.IGNPI + ".0." + SWAPIConstants.ITDateTimeSubmitted, principal.ClientRequestTime.UtcDateTime.ToString(SWAPIConstants.SwDateTimeFormat, CultureInfo.InvariantCulture));
                answers.Add(SWAPIConstants.IGNPI + ".0." + SWAPIConstants.ITDateTimeReceived, principal.ServerReceiptTime.ToString(SWAPIConstants.SwDateTimeFormat, CultureInfo.InvariantCulture));

                WDAPIObject wdapi = new WDAPIObject(study.StudyWorksUsername, study.StudyWorksPassword, study.Name, study.StudyWorksBaseUrl);

                ClinData clinData = wdapi.GetClinDataByNumberRange(form.KrPT, SWAPIConstants.DefaultAssignmentSu, -1,
                           new ArrayList { SWAPIConstants.TableColumnAssignmentSuSigOrig, SWAPIConstants.TableColumnAssignmentSuSigIg, SWAPIConstants.SuTimeZoneValue });

                if (clinData.datatable.Count > 0)
                {
                    assignment.Sigorig = clinData.datatable[0][0];
                    assignment.Sigprev = clinData.datatable[0][1];
                    answers.Add(SWAPIConstants.SuTimeZoneValue, clinData.datatable[0][2]);
                }
                else
                    throw new NetProException("Assignment data from StudyWorks is empty for subject edit - krsu: " + form.SU + ", krpt: " + form.KrPT + ", krdom: " + principal.KrDom);

                ClinRecord clinRecord = WDAPIObject.GenerateClinicalRecordForSubjectAssignment(assignment, answers, ink, false);

                // Validate signatures in the clin record
                if (string.IsNullOrEmpty(clinRecord.ptsigorig))
                {
                    foreach (var field in clinRecord.sysvars)
                    {
                        if (field.sysvar.StartsWith("PT.", StringComparison.OrdinalIgnoreCase))
                        {
                            SubjectData sdata = wdapi.GetSubjectData(clinRecord.krpt, new ArrayList { SWAPIConstants.ColumnPatientSigOrig, SWAPIConstants.ColumnPatientSigId, SWAPIConstants.ColumnPatientKrDom });
                            if (sdata != null)
                            {
                                clinRecord.ptsigorig = sdata.data.Sigorig;
                                clinRecord.ptsigprev = sdata.data.Sigid;
                                clinRecord.krdom = sdata.data.krdom;
                            }
                            break;
                        }
                    }
                }

                if (string.IsNullOrEmpty(clinRecord.krdom))
                {
                    SubjectData sdata = wdapi.GetSubjectData(clinRecord.krpt, new ArrayList {SWAPIConstants.ColumnPatientKrDom});
                    clinRecord.krdom = sdata.data.krdom;
                }

                wdapi.SubmitClinReportRRInk(clinRecord);

                db.Dispose();
            }
            catch (NetProException ex)
            {
                logger.Error(ex.Message, ex);
                db.Dispose();
                HttpResponseException responseException = new HttpResponseException(HttpStatusCode.BadRequest);
                throw responseException;
            }
            catch (SWAPIException ex)
            {
                logger.Error(ex.Message, ex);
                db.Dispose();

                //10030 - DuplicatePatientIDError in StudyWorks
                if (String.Compare(ex.Error.error_message, "10030", StringComparison.InvariantCulture) == 0)
                {
                    HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                    responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.DuplicateSubjectId);
                    throw responseException;
                }
                //StudyWorks returns 10029 for subject edit if sigid is the same as the sigid used in subject assignment  
                else if (String.Compare(ex.Error.error_message, "10029", StringComparison.InvariantCulture) == 0)
                {
                    HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                    responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.DuplicateSigId);
                    throw responseException;
                }
               //10391 - DuplicateSigIDError in StudyWorks
                else if (String.Compare(ex.Error.error_message, "10391", StringComparison.InvariantCulture) == 0)
                {
                    HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                    responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.DuplicateSigId);
                    throw responseException;
                }
                else
                {
                    HttpResponseException responseException = new HttpResponseException(HttpStatusCode.BadRequest);
                    throw responseException;
                }
            }
            catch (ArgumentException ex)
            {
                logger.Error(ex.Message, ex);
                db.Dispose();
                HttpResponseException responseException = new HttpResponseException(HttpStatusCode.BadRequest);
                throw responseException;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                db.Dispose();

                HttpResponseException responseException = new HttpResponseException(HttpStatusCode.InternalServerError);
                throw responseException;
            }

            AssignmentFormResult result = new AssignmentFormResult();
            result.KrPT = form.KrPT;

            return Request.CreateResponse<AssignmentFormResult>(HttpStatusCode.OK, result);
        }

        protected override void Dispose(bool disposing)
        {
            if(disposing)
                db.Dispose();
            base.Dispose(disposing);
        }

        protected bool NeedSyncPhaseStartDate(SubjectData subjectData, DateTimeOffset? npPhaseStartDate, out DateTimeOffset swPhaseStartDate)
        {
            //PhaseStartDate in StudyWorks
            DateTime SUPhaseStartDate = DateTime.MinValue;
            string phaseDateString = "";
            if (subjectData.data.Phasedate.Contains('.'))
            {
                string[] str = subjectData.data.Phasedate.Split('.');
                phaseDateString = DateTimeHelper.ChangeFormat(str[0], SWAPIConstants.SwWeirdDateTimeFormat, SWAPIConstants.SwDateTimeFormat);
            }
            else
            {
                phaseDateString = subjectData.data.Phasedate;
            }

            if (!DateTime.TryParseExact(phaseDateString, SWAPIConstants.SwDateTimeFormat, CultureInfo.InvariantCulture,
                                    DateTimeStyles.None, out SUPhaseStartDate))
            {
                throw new WDAPIException(WDErrors.CannotFormatDateTime,
                    "The datetime format for \"" + WDConstants.VarSuPhaseStartDate + "\" is incorrect. Should be \"" + SWAPIConstants.SwDateTimeFormat + "\"");
            }

            //PhaseTZOffset in StudyWorks
            if (string.IsNullOrEmpty(subjectData.data.phasetzoffset))
            {
                throw new WDAPIException(WDErrors.InvalidTZOffSet, "The PhaseTZOffset in StudyWorks for the patientId: \"" + subjectData.data.Patientid + "\" and the patient initials: \"" + subjectData.data.Initials + "\" has empty or null value. It should have a valid value.\"");
            }

            if (npPhaseStartDate.HasValue)
            {
                TimeSpan swTZ = TimeSpan.FromMilliseconds(Convert.ToDouble(subjectData.data.phasetzoffset));
                TimeSpan npTZ = npPhaseStartDate.Value.Offset;
                if (npTZ.CompareTo(swTZ) == 0)
                {
                    DateTime npPhaseStartDateUtc = npPhaseStartDate.Value.DateTime.ToUniversalTime();
                    DateTime swPhaseStartDateUtc = SUPhaseStartDate.ToUniversalTime();

                    if (npPhaseStartDateUtc.CompareTo(swPhaseStartDateUtc) == 0)
                    {
                        swPhaseStartDate = DateTimeOffset.MinValue;
                        return false;
                    }
                    else
                    {
                        swPhaseStartDate = new DateTimeOffset(DateTime.SpecifyKind(SUPhaseStartDate, DateTimeKind.Unspecified), swTZ);
                        return true;
                    }
                }
                else
                {
                    swPhaseStartDate = new DateTimeOffset(DateTime.SpecifyKind(SUPhaseStartDate, DateTimeKind.Unspecified), swTZ);
                    return true;
                }
            }
            else
            {
                TimeSpan swTZ = TimeSpan.FromMilliseconds(Convert.ToDouble(subjectData.data.phasetzoffset));
                swPhaseStartDate = new DateTimeOffset(DateTime.SpecifyKind(SUPhaseStartDate, DateTimeKind.Unspecified), swTZ);
                return true;
            }
        }

        protected bool NeedSyncEnrollmentDate(SubjectData subjectData, DateTimeOffset? npEnrollDate, out DateTimeOffset swEnrollDate)
        {
            DateTime enrollDate = DateTime.Parse(subjectData.data.enrolldate, CultureInfo.InvariantCulture);
            
            //orig_krtz in StudyWorks
            if (string.IsNullOrEmpty(subjectData.data.orig_krtz))
                {
                throw new WDAPIException(WDErrors.InvalidTZOffSet, "The orig_krtz of lookup_pt table in StudyWorks for the patientId: \"" + subjectData.data.Patientid + "\" and the patient initials: \"" + subjectData.data.Initials + "\" has empty or null value. It should have a valid value.\"");
            }
            
            if (npEnrollDate.HasValue)
            {
                TimeSpan swTZ = TimeSpan.FromMilliseconds(Convert.ToDouble(subjectData.data.orig_krtz));
                TimeSpan npTZ = npEnrollDate.Value.Offset;
                if (npTZ.CompareTo(swTZ) == 0)
                {
                    DateTime npEnrollDateUtc = npEnrollDate.Value.DateTime.ToUniversalTime();
                    DateTime swEnrollDateUtc = enrollDate.ToUniversalTime();

                    if (npEnrollDateUtc.CompareTo(swEnrollDateUtc) == 0)
                    {
                        swEnrollDate = DateTimeOffset.MinValue;
                        return false;
                    }
                    else
                    {
                        swEnrollDate = new DateTimeOffset(DateTime.SpecifyKind(enrollDate, DateTimeKind.Unspecified), swTZ);
                        return true;
                    }
                }
                else
                {
                    swEnrollDate = new DateTimeOffset(DateTime.SpecifyKind(enrollDate, DateTimeKind.Unspecified), swTZ);
                    return true;
                }
            }
            else
            {
                TimeSpan swTZ = TimeSpan.FromMilliseconds(Convert.ToDouble(subjectData.data.orig_krtz));
                swEnrollDate = new DateTimeOffset(DateTime.SpecifyKind(enrollDate, DateTimeKind.Unspecified), swTZ);
                return true;
            }
        }

        protected DateTimeOffset GetActivationDate(string krPT, DateTime startDateTime)
        {
            Study study = StudyData.study;
            WDAPIObject wdapi = new WDAPIObject(study.StudyWorksUsername, study.StudyWorksPassword, study.Name, study.StudyWorksBaseUrl);
            ClinData clinData = wdapi.GetClinDataByDateRange(krPT, SWAPIConstants.KrSUTypeActivation, "orig_startts", startDateTime, DateTime.UtcNow.AddDays(2), new ArrayList { SWAPIConstants.SuReportStartDate, SWAPIConstants.SuOrigKrTz, SWAPIConstants.SuStatus });

            DateTimeOffset activationDate = DateTimeOffset.MinValue;
            foreach (List<string> list in clinData.datatable)
            {
                if(!list[2].Equals(SWAPIConstants.StatusDeleted, StringComparison.OrdinalIgnoreCase))
                {
                    DateTime SUReportStartDate = DateTime.Parse(list[0], CultureInfo.InvariantCulture);
                    TimeSpan swTZ = TimeSpan.FromMilliseconds(Convert.ToDouble(list[1]));
                    activationDate = new DateTimeOffset(DateTime.SpecifyKind(SUReportStartDate, DateTimeKind.Unspecified), swTZ);
                    break;
                }
            }
            return activationDate;
        }

        protected bool NeedSyncActivationDate(DateTimeOffset? npActivationDate, DateTimeOffset swActivationDate)
        {
            if (npActivationDate.HasValue)
            {
                TimeSpan npTZ = npActivationDate.Value.Offset;
                TimeSpan swTZ = swActivationDate.Offset;
                if (npTZ.CompareTo(swTZ) == 0)
                {
                    DateTime npActivationDateUtc = npActivationDate.Value.DateTime.ToUniversalTime();
                    DateTime swActivationDateUtc = swActivationDate.DateTime.ToUniversalTime();
                    if (npActivationDateUtc.CompareTo(swActivationDateUtc) == 0)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }

        protected string GetLPEndDate(WDAPIObject wdapi, string krpt)
        {
            SubjectData subjectData = wdapi.GetSubjectData(krpt, new ArrayList { SWAPIConstants.ColumnPatientLPEndDate });

            if (subjectData != null && subjectData.data != null)
            {
                return subjectData.data.lpenddate;
            }
            return null;
        }

        protected string GetSubjectLanguage(WDAPIObject wdapi, string krPT)
        {
           ClinData clinData = wdapi.GetClinDataByNumberRange(krPT, SWAPIConstants.DefaultAssignmentSu, -1,
                                new ArrayList {SWAPIConstants.TableColumnSubjectLanguage});

           if (clinData.datatable[0].Count() > 0)
           {
               if (string.IsNullOrEmpty(clinData.datatable[0][0]))
                   return string.Empty;
               else
                   return clinData.datatable[0][0];
           }
           else
               return string.Empty;
        }
    }
}
