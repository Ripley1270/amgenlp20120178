﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Data.SqlClient;

using log4net;
using WebDiary.StudyPortal.Api.Models;
using WebDiary.Core.Entities;
using WebDiary.Core.Repositories;
using WebDiary.Core.Helpers;
using WebDiary.Core.Errors;
using WebDiary.SWIS;
using WebDiary.SWAPI;

namespace WebDiary.StudyPortal.Api.Controllers
{
    public class LogsController : ApiController
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(LogsController));
        private WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString);

        // POST api/<controller>
        [Authorize]
        public HttpResponseMessage Post([FromBody]LogModel log)
        {
            // Retrieve the principal
            WebApiPrincipal principal = Thread.CurrentPrincipal as WebApiPrincipal;

            try
            {
                // Add to BATCH_LOG_QUEUE table
                LogQueueItem logQueueItem = new LogQueueItem();
                if (!principal.ApiTokenAuth)
                    logQueueItem.SubjectId = principal.SubjectID;

                logQueueItem.ClientSentDate = principal.ClientRequestTime.UtcDateTime;
                logQueueItem.QueueDate = DateTime.Now;
                logQueueItem.QueueState = QueueState.New;
                logQueueItem.Json = log.Json.ToString();
                logQueueItem.StudyId = StudyData.study.Id;
                db.LogQueue.InsertOnSubmit(logQueueItem);
                db.SubmitChanges();

                if (logQueueItem.QueueState == QueueState.New)
                {
                    // Asynchronously submit the log entry to sw ng
                    LoggingService service = new LoggingService();
                    var task = Task.Factory.StartNew(() => service.LoggingApiSubmit(logQueueItem.Id), TaskCreationOptions.LongRunning);
                }
            }
            catch (SqlException sqlEx)
            {
                logger.Error(sqlEx.Message, sqlEx);
                HttpResponseException responseException = new HttpResponseException(HttpStatusCode.InternalServerError);
                responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.ServerError);
                throw responseException;
            }            

            return new HttpResponseMessage(HttpStatusCode.Created);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                db.Dispose();
            base.Dispose(disposing);
        }
    }
}