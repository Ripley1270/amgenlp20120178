﻿using System;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.SqlClient;
using WebDiary.StudyPortal.Api.Models;
using WebDiary.Core.Errors;
using WebDiary.Core.Helpers;
using WebDiary.Core.Entities;
using WebDiary.Core.Repositories;
using WebDiary.Core.Constants;
using System.Threading;
using log4net;
using WebDiary.SWAPI.SWData;
using WebDiary.SWAPI;
using WebDiary.WDAPI;
using WebDiary.StudyPortal.Api.Helpers;
using Newtonsoft.Json.Linq;

namespace WebDiary.StudyPortal.Api.Controllers
{
    public class DevicesController : ApiController
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(DevicesController));
        private WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString);

        // GET api/<controller>
        public IEnumerable<DeviceModel> Get([FromUri] long UserID = 0, [FromUri] string fields = "")
        {
            SubjectRepository subjectRep = new SubjectRepository(db);
            Subject subject = subjectRep.FindByPinAndStudy(UserID, StudyData.study.Id);
            if (subject == null)
                yield break;

            var query = (from sd in db.SubjectDevices
                        where sd.SubjectId == subject.Id
                        orderby sd.ActivationDate descending
                        select sd).Take<SubjectDevice>(1);

            // Retrieve the principal
            WebApiPrincipal principal = Thread.CurrentPrincipal as WebApiPrincipal;
            if (principal == null)
                principal = HttpContext.Current.User as WebApiPrincipal;

            foreach(SubjectDevice device in query)
            {
                //construct Device Model
                DeviceModel deviceResultModel = new DeviceModel();
                if (string.IsNullOrEmpty(fields))
                {
                    if (principal != null)
                    {
                        deviceResultModel.DeviceID = device.DeviceCode;
                        deviceResultModel.Question = subject.SecurityQuestion;
                        deviceResultModel.Answer = subject.SecurityAnswer;
                        deviceResultModel.Password = device.Password;
                    }
                    else //anonymous request
                    {
                        deviceResultModel.Question = subject.SecurityQuestion;
                    }
                }
                else
                {
                    var fieldList = fields.Split(',');

                    if (principal != null)
                    {
                        if(fieldList.Contains("DeviceID"))
                            deviceResultModel.DeviceID = device.DeviceCode;
                        if (fieldList.Contains("Question"))
                            deviceResultModel.Question = subject.SecurityQuestion;
                        if (fieldList.Contains("Answer"))
                            deviceResultModel.Answer = subject.SecurityAnswer;
                        if (fieldList.Contains("Password"))
                            deviceResultModel.Password = device.Password;
                    }
                    else //anonymous request
                    {
                        if (fieldList.Contains("Answer") || fieldList.Contains("DeviceID") || fieldList.Contains("Password"))
                        {
                            throw new HttpResponseException(HttpStatusCode.Unauthorized);
                        }

                        if (fieldList.Contains("Question"))
                        deviceResultModel.Question = subject.SecurityQuestion;
                    }
                }
                yield return deviceResultModel;
            }
        }

        // GET api/<controller>/5
        [Authorize]
        public HttpResponseMessage GetById(Guid id, [FromUri]string fields = "")
        {
            DeviceModel result = new DeviceModel();

            try
            {
                // Retrieve the principal
                WebApiPrincipal principal = Thread.CurrentPrincipal as WebApiPrincipal;
                if (principal == null)
                    principal = HttpContext.Current.User as WebApiPrincipal;
                SubjectRepository subjectRep = new SubjectRepository(db);
                Subject subject = subjectRep.FindById(principal.SubjectID);

                SubjectDeviceRepository subjectDeviceRep = new SubjectDeviceRepository(db);
                SubjectDevice device = subjectDeviceRep.FindByDeviceCode(id);

                if (device != null)
                {
                    if (string.IsNullOrEmpty(fields))
                    {
                        result.Question = subject.SecurityQuestion;
                        result.Answer = subject.SecurityAnswer;
                        result.Password = device.Password;
                    }
                    else
                    {
                        var fieldList = fields.Split(',');

                        if(fieldList.Contains("Question"))
                            result.Question = subject.SecurityQuestion;
                        if (fieldList.Contains("Answer"))
                            result.Answer = subject.SecurityAnswer;
                        if (fieldList.Contains("Password"))
                            result.Password = device.Password;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                HttpResponseException responseException = new HttpResponseException(HttpStatusCode.InternalServerError);
                if (ex is SqlException || ex is WebException)
                {
                    responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.ServerError);
                }
                throw responseException;
            }

            return Request.CreateResponse(HttpStatusCode.OK, result); 
        }

        // POST api/<controller>
        public HttpResponseMessage Post([FromBody] NewDeviceModel device)
        {            
            DeviceModel result = new DeviceModel();            

            try
            {
                SubjectRepository subjectRep = new SubjectRepository(db);
                Subject subject = subjectRep.FindByPin(device.UserID);
                StudyMiddleTier studyMiddleTier = db.StudyMiddleTiers.Where(s => s.StudyId == StudyData.study.Id).SingleOrDefault();

                if (subject == null || subject.IsLockedOut)
                {
                    throw new HttpResponseException(HttpStatusCode.Unauthorized);
                }

                bool activationWithoutQA = string.IsNullOrEmpty(subject.Password) && (!device.Question.HasValue || string.IsNullOrEmpty(device.Answer));
                bool activationWithQA = string.IsNullOrEmpty(subject.Password) && (device.Question.HasValue || !string.IsNullOrEmpty(device.Answer));

                if (studyMiddleTier.AuthenticationLevel == AuthenticationLevel.SecurityQuestion && activationWithoutQA
                        || studyMiddleTier.AuthenticationLevel == AuthenticationLevel.Basic && activationWithQA)
                    throw new HttpResponseException(HttpStatusCode.Forbidden);

                // Log to studyworks
                WDAPIObject wdapi = new WDAPIObject(StudyData.study.StudyWorksUsername, StudyData.study.StudyWorksPassword, StudyData.study.Name, StudyData.study.StudyWorksBaseUrl);

                if (!wdapi.IsSubjectEnabled(subject.KrPT))
                {
                    HttpResponseException userNotFoundException = new HttpResponseException(HttpStatusCode.Forbidden);
                    userNotFoundException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.UserNotFound);
                    throw userNotFoundException;
                }
				
                // transmission time
                DateTime serverReceiptTimeUtc = DateTime.UtcNow;
                DateTimeOffset dateOffset = Request.Headers.Date.Value;

                SubjectData subjectData = wdapi.GetSubjectData(subject.KrPT,
                                                                new ArrayList {SWAPIConstants.ColumnPatientId,
                                                                    SWAPIConstants.ColumnPatientInitials,
                                                                    SWAPIConstants.ColumnPatientKrDom,
                                                                    SWAPIConstants.ColumnPatientKrPhase,
                                                                    SWAPIConstants.ColumnPatientPhaseDate,
                                                                    SWAPIConstants.ColumnPatientSigId,
                                                                    SWAPIConstants.ColumnPatientSigOrig,
                                                                    SWAPIConstants.ColumnPatientCustom10,
                                                                    SWAPIConstants.ColumnPatientLPStartDate});

                string submittedPassword = result.Password = SubjectHelper.EncodePassword(device.Password, subject.KrPT);
                Boolean initialActivation = false;

                if (string.IsNullOrEmpty(subject.Password)) // Activation
                {
                    initialActivation = true;

                    // Update the subject record in the NetPRO database
                    subject.IsApproved = true;
                    subject.LastLoginDate = DateTime.UtcNow;
                    subject.LastActivityDate = DateTime.UtcNow;
                    subject.LastPasswordChangedDate = DateTime.UtcNow;
                    subject.SecurityQuestion = device.Question;
                    subject.SecurityAnswer = device.Answer;
                    subject.LastSecurityQuestionAnswerChanged = DateTime.UtcNow;
                }
                else // Reactivation
                {
                    // invalid unlock code
                    bool badCode = Request.Headers.Contains("Unlock-Code") && !IsValidCode(studyMiddleTier);
                    // invalid security question or answer
                    bool badQA = !Request.Headers.Contains("Unlock-Code") && device.Question.HasValue && !string.IsNullOrEmpty(device.Answer) && (subject.SecurityQuestion.Value != device.Question.Value || subject.SecurityAnswer != device.Answer);
                    // invalid password
                    bool badPassword = !Request.Headers.Contains("Unlock-Code") && !device.Question.HasValue && string.IsNullOrEmpty(device.Answer) && subject.Password != submittedPassword;

                    if (studyMiddleTier.AuthenticationLevel == AuthenticationLevel.SecurityQuestion)
                    {
                        if (badCode || badQA || badPassword)
                            throw new HttpResponseException(HttpStatusCode.Unauthorized);

                        if (device.Question.HasValue)
                        {
                            if (subject.SecurityQuestion.Value != device.Question.Value)
                                subject.LastSecurityQuestionAnswerChanged = DateTime.UtcNow;

                            subject.SecurityQuestion = result.Question = device.Question.Value;
                        }
                        else
                            result.Question = subject.SecurityQuestion;

                        if (device.Answer != null)
                        {
                            if (subject.SecurityAnswer != device.Answer)
                                subject.LastSecurityQuestionAnswerChanged = DateTime.UtcNow;
                            
                            subject.SecurityAnswer = result.Answer = device.Answer;
                        }
                        else
                            result.Answer = subject.SecurityAnswer;
                    }
                    else // AuthenticationLevel.Basic or No secret question
                    {
                        badPassword = !Request.Headers.Contains("Unlock-Code") && subject.Password != submittedPassword;
                        if (badCode || badPassword)
                            throw new HttpResponseException(HttpStatusCode.Unauthorized);
                    }

                    if (subject.Password != submittedPassword)
                    {
                        subject.LastPasswordChangedDate = DateTime.UtcNow;
                    }
                }

                SubjectDevice subjectDevice = new SubjectDevice();
                subjectDevice.DeviceCode = Guid.NewGuid();

				// register device in sw if serial number is specified
				int? sw_device_id = null;
				if(!string.IsNullOrEmpty(device.SerialNumber))
				{
					if (string.IsNullOrEmpty(device.Model))
						device.Model = "Unknown";
					
					sw_device_id = RegisterDevice(new DeviceData
						{
                            deviceUsername = subjectDevice.DeviceCode.ToString(),
							devicePassword = "123456",
							deviceType = "LogPadApp",
							deviceTypeId = "LogPad.DeviceTypeDef.506",
							deviceSerialNumber = device.SerialNumber,
							model = device.Model
						}
					);

					AssignSubject(sw_device_id.Value, subject.KrPT);
				}

                subject.Password = submittedPassword;
                subject.ClientPassword = device.Password;

                
                subjectDevice.SubjectId = subject.Id;
                subjectDevice.ActivationDate = DateTime.UtcNow;
                
                subjectDevice.Password = submittedPassword;
                subjectDevice.ClientPassword = device.Password;
                subjectDevice.SWDeviceId = sw_device_id;
                result.DeviceID = subjectDevice.DeviceCode;
                db.SubjectDevices.InsertOnSubmit(subjectDevice);

                string triggeredPhase = string.Empty;
                if (initialActivation)
                {
                    if (!string.IsNullOrEmpty(subjectData.data.custom10) && subjectData.data.custom10.Trim().Length > 0)
                    {
                        string protocolTriggeredPhase = StudyData.study.triggeredPhaseAtInitialActivation;
                        if (!string.IsNullOrEmpty(protocolTriggeredPhase))
                            triggeredPhase = TriggeredPhase.GetTriggeredPhase(subjectData.data.custom10.Trim(), protocolTriggeredPhase);
                    }
                }

                // submit activation report                        
                SubmitActivationReport(subject, subjectData, subjectDevice, dateOffset, serverReceiptTimeUtc, wdapi, device.Source, triggeredPhase);

                wdapi.BeginSendSecurityEvent(HttpHelper.GetIP4Address(), SWAPIAbstraction.SecurityEventSet, DateTime.UtcNow, subject.KrPT,
                        DateTimeHelper.GetTimeZoneOffsetMilliseconds(subject.TimeZone.ToString(CultureInfo.InvariantCulture), StudyData.study.Id), "Account activated", new AsyncCallback(SendSecurityEventCallback));

                // Save Database
                db.SubmitChanges();
            }
            catch (HttpResponseException httpEx)
            {
                throw httpEx;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                HttpResponseException responseException = new HttpResponseException(HttpStatusCode.InternalServerError);
                if (ex is SqlException || ex is WebException)
                {
                    responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.ServerError);
                }
                throw responseException;
            }

            return Request.CreateResponse(HttpStatusCode.Created, result);
        }

        private bool IsValidCode(StudyMiddleTier studyMiddleTier)
        {
            short seed = studyMiddleTier.SeedCode;
            byte length = studyMiddleTier.CodeLength;
            DateTime date = Request.Headers.Date.Value.DateTime;
            string clientUnlockCode = Request.Headers.GetValues("Unlock-Code").FirstOrDefault();
            string unlockcode = SubjectHelper.GetUnlockCode(seed, length, date); // current date code
            if (unlockcode != clientUnlockCode)
            {
                unlockcode = SubjectHelper.GetUnlockCode(seed, length, date.AddDays(-1)); // previous date code
                if (unlockcode != clientUnlockCode)
                {
                    unlockcode = SubjectHelper.GetUnlockCode(seed, length, date.AddDays(1)); // next date code
                    if (unlockcode != clientUnlockCode)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        // PUT api/<controller>/5
        [Authorize]
        public HttpResponseMessage Put(Guid id, [FromBody]DeviceModel device)
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            // Retrieve the principal
            WebApiPrincipal principal = Thread.CurrentPrincipal as WebApiPrincipal;
            try
            {
                if (principal.DeviceID != id)
                    throw new HttpResponseException(HttpStatusCode.Unauthorized);

                SubjectDeviceRepository subjectDeviceRep = new SubjectDeviceRepository(db);
                var subjectDevice = subjectDeviceRep.FindByDeviceCode(id);
                SubjectRepository subjectRep = new SubjectRepository(db);
                Subject subject = subjectRep.FindById(principal.SubjectID);
                StudyMiddleTier studyMiddleTier = db.StudyMiddleTiers.Where(s => s.StudyId == StudyData.study.Id).SingleOrDefault();

                if (studyMiddleTier.AuthenticationLevel == AuthenticationLevel.Basic && (device.Question.HasValue || !string.IsNullOrEmpty(device.Answer)))
                        throw new HttpResponseException(HttpStatusCode.Forbidden);

                //Prevent password reset from deactivated device
                if (IsDeactivatedDevice(principal.SubjectID, id) && (device.Question.HasValue || !string.IsNullOrEmpty(device.Answer) || !string.IsNullOrEmpty(device.Password)))
                    throw new HttpResponseException(HttpStatusCode.Forbidden);   

                // Password reset
                if (!string.IsNullOrEmpty(device.Password))
                {
                    if (studyMiddleTier.AuthenticationLevel == AuthenticationLevel.SecurityQuestion)
                    {
                        //verify security question and answer
                        if (!subject.SecurityQuestion.HasValue || !device.Question.HasValue || string.IsNullOrEmpty(device.Answer))
                        {
                            throw new HttpResponseException(HttpStatusCode.Forbidden);
                        }
                        
                        if (subject.SecurityQuestion.Value != device.Question.Value || subject.SecurityAnswer != device.Answer)
                        {
                            HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                            responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.SecurityAnswerDoesNotMatch);
                            throw responseException;
                        }
                    }

                    DeviceModel deviceResultModel = new DeviceModel();
                    subject.ClientPassword = subjectDevice.ClientPassword = device.Password;
                    subject.Password = deviceResultModel.Password = subjectDevice.Password = SubjectHelper.EncodePassword(device.Password, subject.KrPT);
                    subject.LastPasswordChangedDate = DateTime.UtcNow;
                    result = Request.CreateResponse<DeviceModel>(HttpStatusCode.OK, deviceResultModel);
                }

                if (string.IsNullOrEmpty(device.Password) && device.Question.HasValue)
                {
                    if (subject.SecurityQuestion.Value != device.Question.Value)
                        subject.LastSecurityQuestionAnswerChanged = DateTime.UtcNow;

                    subject.SecurityQuestion = device.Question;
                }

                if (string.IsNullOrEmpty(device.Password) && !string.IsNullOrEmpty(device.Answer))
                {
                    if (subject.SecurityAnswer != device.Answer)
                        subject.LastSecurityQuestionAnswerChanged = DateTime.UtcNow; 
                    subject.SecurityAnswer = device.Answer;
                }

                // check end logpad use flag
                if (device.EndLP.HasValue && device.EndLP.Value == true && subjectDevice.SWDeviceId.HasValue)
                    EndLogPadUse(subjectDevice.SWDeviceId.Value, subject.KrPT);

                db.SubmitChanges();
            }
            catch (HttpResponseException httpEx)
            {
                throw httpEx;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                HttpResponseException responseException = new HttpResponseException(HttpStatusCode.InternalServerError);
                if (ex is SqlException || ex is WebException)
                {
                    responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.ServerError);
                }
                throw responseException;
            }
            return result;
        }

        private void SubmitActivationReport(Subject subject, SubjectData subjectData, SubjectDevice subjectDevice, DateTimeOffset dateOffset, DateTime serverReceiptTimeUtc, WDAPIObject wdapi, string source, string triggeredPhase)
        {
            Study study = subject.Study;

            // construct survey data
            SurveyData surveyData = new SurveyData();

            // form level data
            DateTime diaryReportDate = dateOffset.UtcDateTime;
            DateTime diaryStartUtc = dateOffset.UtcDateTime;

            // phase
            surveyData.SuPhaseAtEntry = subjectData.data.Krphase;
            DateTime SUPhaseStartDate = DateTime.MinValue;
            string phaseDateString = "";
            if (subjectData.data.Phasedate.Contains('.'))
            {
                string[] str = subjectData.data.Phasedate.Split('.');
                phaseDateString = DateTimeHelper.ChangeFormat(str[0], SWAPIConstants.SwWeirdDateTimeFormat, SWAPIConstants.SwDateTimeFormat);
            }
            else
            {
                phaseDateString = subjectData.data.Phasedate;
            }
            if (!DateTime.TryParseExact(phaseDateString, SWAPIConstants.SwDateTimeFormat, CultureInfo.InvariantCulture,
                                    DateTimeStyles.None, out SUPhaseStartDate))
            {
                throw new WDAPIException(WDErrors.CannotFormatDateTime,
                    "The datetime format for \"" + WDConstants.VarSuPhaseStartDate + "\" is incorrect. Should be \"" + SWAPIConstants.SwDateTimeFormat + "\"");
            }
            surveyData.SuPhaseStartDate = DateTime.SpecifyKind(SUPhaseStartDate, DateTimeKind.Local);


            surveyData.Id = Guid.NewGuid();
            surveyData.ExpiryDate = DateTime.UtcNow.AddDays(1);
            surveyData.Krpt = subject.KrPT;
            surveyData.Krdom = subjectData.data.krdom;
            surveyData.StudyId = study.Id;
            surveyData.PrintedName = subjectData.data.Initials;
            surveyData.SuReportDate = diaryReportDate;
            surveyData.PatientId = subjectData.data.Patientid;
            surveyData.SurveyStartDate = diaryStartUtc;
            surveyData.SurveyId = 0;

            // empty survey records
            SurveyResponseData surveyResponses = new SurveyResponseData();

            string krSU = "Activation";
            string formType = SWAPIConstants.FormTypeSubjectSubmit;
            double clientTZOffset = dateOffset.Offset.TotalSeconds;

            // Construct the clin record
            ClinRecord clinRecord = WDAPIObject.GenerateClinicalRecord(surveyResponses, surveyData, triggeredPhase, formType, krSU, string.Empty, DateTime.UtcNow, clientTZOffset, null);

            //Set PT.LPStartDate if all of the followings are met
            //1. LPStartDate is null or empty
            //2. source is empty or equals "LogPadApp"
            string logPadStartDate = string.Empty;
            if (!string.IsNullOrEmpty(subjectData.data.lpstartdate))
                logPadStartDate = subjectData.data.lpstartdate.Trim();

            string deviceSource = string.Empty;
            if (!string.IsNullOrEmpty(source))
                deviceSource = source.Trim();

            if (string.IsNullOrEmpty(logPadStartDate))
            {
                if (string.IsNullOrEmpty(deviceSource) || string.Equals(deviceSource, SWAPIConstants.SourceLogPadApp, StringComparison.OrdinalIgnoreCase))
                {
                    //Get SU.TZValue
                    ClinData clinData = wdapi.GetClinDataByNumberRange(subject.KrPT, SWAPIConstants.DefaultAssignmentSu, -1,
                               new ArrayList { SWAPIConstants.SuTimeZoneValue });

                    string tzID = String.Empty;
                    if (clinData.datatable.Count > 0)
                    {
                        tzID = clinData.datatable[0][0];
                    }

                    if (string.IsNullOrEmpty(tzID))
                    {
                        throw new WDAPIException(WDErrors.InvalidTZValue, String.Format(CultureInfo.InvariantCulture, "TZValue in lookup_su table of StudyWorks for the patientId: {0}, subject krpt: {1} has empty or null value.", subjectData.data.Patientid, subject.KrPT));
                    }

                    ClinField lpStartDate = new ClinField();
                    lpStartDate.sysvar = SWAPIConstants.PTLPStartDate;
                    lpStartDate.krit = "";
                    lpStartDate.comment = SWAPIConstants.SystemComment;
                    lpStartDate.value = DateTimeHelper.GetLocalTime(dateOffset.UtcDateTime, study.Id, int.Parse(tzID, CultureInfo.InvariantCulture)).ToString(SWAPIConstants.SwDateTimeFormat, CultureInfo.InvariantCulture);
                    lpStartDate.edit = false;
                    clinRecord.sysvars.Add(lpStartDate);
                }
            }
            
            // SW Device ID
            if (subjectDevice.SWDeviceId.HasValue)
            {
                ClinField lpaDeviceId = new ClinField();
                lpaDeviceId.sysvar = SWAPIConstants.SuLogPadDeviceId;
                lpaDeviceId.krit = "";
                lpaDeviceId.comment = SWAPIConstants.SystemComment;
                lpaDeviceId.value = subjectDevice.SWDeviceId.Value.ToString(CultureInfo.InvariantCulture);
                lpaDeviceId.edit = false;
                clinRecord.sysvars.Add(lpaDeviceId);
            }

            // Validate signatures in the clin record
            if (string.IsNullOrEmpty(clinRecord.ptsigorig))
            {
                foreach (var field in clinRecord.sysvars)
                {
                    if (field.sysvar.StartsWith("pt.", StringComparison.OrdinalIgnoreCase))
                    {
                        if (subjectData != null)
                        {
                            clinRecord.ptsigorig = subjectData.data.Sigorig;
                            clinRecord.ptsigprev = subjectData.data.Sigid;
                        }
                        break;
                    }
                }
            }

            // Set NetProInformation IG - Note: Needs to be customized here and in StudyWorks to distinguish LF from NetPRO            
            List<ClinField> NetPROInfo = new List<ClinField>(5);

            ClinField NPVersion = new ClinField();
            NPVersion.krit = "NPVersion";
            if (string.IsNullOrEmpty(StudyData.version.ProductMicroVersion) || StudyData.version.ProductMicroVersion == "0")
               NPVersion.value = StudyData.version.ProductMajorVersion + "." + StudyData.version.ProductMinorVersion;
            else
                NPVersion.value = StudyData.version.ProductMajorVersion + "." + StudyData.version.ProductMinorVersion + "." + StudyData.version.ProductMicroVersion;
             NPVersion.edit = false;

            ClinField NPStudyVersion = new ClinField();
            NPStudyVersion.krit = "NPStudyVersion";
            NPStudyVersion.value = StudyData.version.StudyMajorVersion + "." + StudyData.version.StudyMinorVersion;
            NPStudyVersion.edit = false;

            ClinField OSVersion = new ClinField();
            OSVersion.krit = "OSVersion";
            OSVersion.value = HttpContext.Current.Request.Browser.Platform;
            OSVersion.edit = false;

            ClinField browserType = new ClinField();
            browserType.krit = "BrowserName";
            browserType.value = HttpContext.Current.Request.Browser.Type;
            browserType.edit = false;

            ClinField browserVersion = new ClinField();
            browserVersion.krit = "BrowserVersion";
            browserVersion.value = HttpContext.Current.Request.Browser.Version;
            browserVersion.edit = false;

            ClinField clientCoreVersion = new ClinField();
            clientCoreVersion.krit = "LogPadAppCoreVersion";
            clientCoreVersion.value = string.Empty;
            clientCoreVersion.edit = false;

            ClinField clientStudyVersion = new ClinField();
            clientStudyVersion.krit = "LogPadAppStudyVersion";
            clientStudyVersion.value = string.Empty;
            clientStudyVersion.edit = false;

            ClinField deviceId = new ClinField();
            deviceId.krit = "ClientID";
            deviceId.value = subjectDevice.DeviceCode.ToString();
            deviceId.edit = false;

            ClinField dateTimeSubmitted = new ClinField();
            dateTimeSubmitted.krit = "DateTimeSubmitted";
            dateTimeSubmitted.value = dateOffset.UtcDateTime.ToString(SWAPIConstants.SwDateTimeFormat, CultureInfo.InvariantCulture);
            dateTimeSubmitted.edit = false;

            ClinField dateTimeReceived = new ClinField();
            dateTimeReceived.krit = "DateTimeReceived";
            dateTimeReceived.value = serverReceiptTimeUtc.ToString(SWAPIConstants.SwDateTimeFormat, CultureInfo.InvariantCulture);
            dateTimeReceived.edit = false;

            NetPROInfo.Add(NPVersion);
            NetPROInfo.Add(NPStudyVersion);
            NetPROInfo.Add(OSVersion);
            NetPROInfo.Add(browserType);
            NetPROInfo.Add(browserVersion);
            NetPROInfo.Add(clientCoreVersion);
            NetPROInfo.Add(clientStudyVersion);
            NetPROInfo.Add(deviceId);
            NetPROInfo.Add(dateTimeSubmitted);
            NetPROInfo.Add(dateTimeReceived);

            Dictionary<string, List<ClinField>> ig = new Dictionary<string, List<ClinField>>();
            ig.Add("0", NetPROInfo);
            clinRecord.iglist.Add("NetProInformation", ig);

            //Set Protocol
            if (!string.IsNullOrEmpty(subjectData.data.custom10) && subjectData.data.custom10.Trim().Length > 0)
            {
                List<ClinField> protocolInfo = new List<ClinField>(1);
                ClinField protocol = new ClinField();
                protocol.krit = SWAPIConstants.ITProtocol;
                protocol.value = subjectData.data.custom10;
                protocol.edit = false;
                protocolInfo.Add(protocol);

                Dictionary<string, List<ClinField>> IgProtocol = new Dictionary<string, List<ClinField>>();
                IgProtocol.Add("0", protocolInfo);
                clinRecord.iglist.Add(SWAPIConstants.IGProtocol, IgProtocol);
            }

            //Set krUSER.
            clinRecord.kruser = study.StudyWorksUsername;

            //Set source
            if (!string.IsNullOrEmpty(source))
                clinRecord.source = source.Trim();

            // Submit
            wdapi.SubmitClinReportRRInk(clinRecord);

        }

        private void SendSecurityEventCallback(IAsyncResult ia)
        {
            try
            {
                var ar = (System.Runtime.Remoting.Messaging.AsyncResult)ia;
                var del = (SendSecurityEventDelegate)ar.AsyncDelegate;
                del.EndInvoke(ia);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if(disposing)
                db.Dispose();
            base.Dispose(disposing);
        }

        /// <summary>
        /// register one single device to the study.
        /// </summary>
        /// <param name="deviceData">an object that contain all necessary data for registering a device</param>
        /// <returns>int</returns>
        /// <exception cref="SWAPIException"></exception>
        /// <exception cref="WebException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        private int RegisterDevice(DeviceData deviceData)
        {
            Uri uri = new Uri(StudyData.study.StudyWorksBaseUrl.TrimEnd('/') + "/Voltron/DeviceJsonRpcService");

            JsonRpcRequest request = new JsonRpcRequest(uri, "registerDevice", new JArray(
                    StudyData.study.StudyWorksUsername,
                    StudyData.study.StudyWorksPassword,
                    StudyData.study.Name,
                    new JObject(
                        new JProperty("deviceUsername", deviceData.deviceUsername),
                        new JProperty("devicePassword", deviceData.devicePassword),
                        new JProperty("deviceSerialNumber", deviceData.deviceSerialNumber),
                        new JProperty("deviceType", "LogPadApp"),
                        new JProperty("deviceTypeID", "LogPad.DeviceTypeDef.506"),
                        new JProperty("model", deviceData.model)
                    )
                ));

            JObject response = request.GetResponse();

            return (int)response["result"];
        }

        /// <summary>
        /// assign a subject to a device.
        /// </summary>
        /// <param name="sw_device_id">StudyWorks generated device id</param>
        /// <param name="krPT">Subject's krPT</param>
        /// <exception cref="SWAPIException"></exception>
        /// <exception cref="WebException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        private void AssignSubject(int sw_device_id, string krPT)
        {
            Uri uri = new Uri(StudyData.study.StudyWorksBaseUrl.TrimEnd('/') + "/Voltron/DeviceJsonRpcService");

            JsonRpcRequest request = new JsonRpcRequest(uri, "assignSubject", new JArray(
                    sw_device_id,
                    krPT,
                    StudyData.study.Name
                ));

            JObject response = request.GetResponse();
        }

        /// <summary>
        /// end Logpad use on a device.
        /// </summary>
        /// <param name="deviceID">registered StudyWorks device id</param>
        /// <param name="krPT">subject's krPT</param>
        /// <exception cref="SWAPIException"></exception>
        /// <exception cref="WebException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        private void EndLogPadUse(int deviceID, string krPT)
        {
            Uri uri = new Uri(StudyData.study.StudyWorksBaseUrl.TrimEnd('/') + "/Voltron/DeviceJsonRpcService");

            JsonRpcRequest request = new JsonRpcRequest(uri, "endDeviceUse", new JArray(
                    deviceID,
                    krPT,
                    StudyData.study.Name
                ));

            JObject response = request.GetResponse();
        }

        private bool IsDeactivatedDevice(Guid subjectId, Guid deviceId)
        {
            SubjectDeviceRepository deviceRepo = new SubjectDeviceRepository(db);
            SubjectDevice currentDevice = deviceRepo.FindLatestBySubjectId(subjectId);
            if (!currentDevice.DeviceCode.Equals(deviceId)) // not active device
            {
                return true;
            }

            return false;
        }
    }
}
