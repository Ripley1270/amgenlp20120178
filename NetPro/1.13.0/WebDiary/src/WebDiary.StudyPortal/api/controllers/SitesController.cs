﻿using System;
using System.Collections;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WebDiary.Core.Errors;
using WebDiary.Core.Helpers;
using WebDiary.SWAPI;
using WebDiary.WDAPI;

namespace WebDiary.StudyPortal.Api.Controllers
{
    public class SitesController : ApiController
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(SitesController));

        // Get api/v1/sites/studyName
        public HttpResponseMessage GetSites(string id)
        {
            if (!string.IsNullOrEmpty(id) && String.Equals(id, StudyData.study.Name, StringComparison.OrdinalIgnoreCase))
            {
                try
                {
                    ArrayList parameter = new ArrayList();

                    // Perform a SWAPI call
                    WDAPIObject wdapi = new WDAPIObject(StudyData.study.StudyWorksUsername, StudyData.study.StudyWorksPassword, StudyData.study.Name, StudyData.study.StudyWorksBaseUrl);
                    string swResponse = string.Empty;

                    //Pass empty parameter because this stored procedure does not require a parameter
                    swResponse = wdapi.GetData(SWAPIConstants.SPSyndGetValidSiteCode, parameter).response;

                    if (string.IsNullOrEmpty(swResponse))
                    {
                        JArray jsonResponse = JArray.Parse("[]");
                        return Request.CreateResponse<JArray>(HttpStatusCode.OK, jsonResponse);
                    }
                    else
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(swResponse);

                        string jsonString = JsonConvert.SerializeXmlNode(doc, Newtonsoft.Json.Formatting.None);

                        JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings {
                            DateFormatHandling = DateFormatHandling.IsoDateFormat,
                            DateParseHandling = DateParseHandling.None
                        };

                        object obj = JsonConvert.DeserializeObject(jsonString, jsonSerializerSettings);
                        JObject jObject = (JObject)obj;

                        JArray jsonResponse = new JArray();

                        if (jObject["sites"]["site"].Type == JTokenType.Array)
                        {
                            jsonResponse = (JArray)jObject["sites"]["site"];
                        }
                        else
                        {
                            jsonResponse.Add(jObject["sites"]["site"]);
                        }

                        return Request.CreateResponse<JArray>(HttpStatusCode.OK, jsonResponse);
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message, ex);
                    HttpResponseException responseException = new HttpResponseException(HttpStatusCode.InternalServerError);
                    if (ex is SqlException || ex is WebException)
                    {
                        responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.ServerError);
                    }
                    throw responseException;
                }
            }
            else
            {
                HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.InvalidStudyName);
                throw responseException; 
            }
        }
    }
}