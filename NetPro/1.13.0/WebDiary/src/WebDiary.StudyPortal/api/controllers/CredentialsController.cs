﻿using System;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.SqlClient;
using WebDiary.StudyPortal.Api.Models;
using WebDiary.Core.Errors;
using WebDiary.Core.Helpers;
using WebDiary.Core.Entities;
using WebDiary.Core.Repositories;
using WebDiary.Core.Constants;
using System.Threading;
using log4net;
using WebDiary.SWAPI.SWData;
using WebDiary.SWAPI;
using WebDiary.WDAPI;
using WebDiary.StudyPortal.Api.Helpers;
using Newtonsoft.Json.Linq;

namespace WebDiary.StudyPortal.Api.Controllers
{
    public class CredentialsController : ApiController
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(CredentialsController));
        private WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString);

        [Authorize]
        public HttpResponseMessage Get(string id)
        {
            CredentialResult result = new CredentialResult();
            try
            {
                //Check krpt
                SubjectRepository subjectRep = new SubjectRepository(db);
                Subject subject = subjectRep.FindByKrptAndStudy(id, StudyData.study.Id);

                if (subject == null)
                {
                    logger.Error(String.Format(CultureInfo.InvariantCulture, "Subject for krpt in incoming url does not exist. Krpt in incoming url: {0}", id));
                    HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                    responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.InvalidKrPT);
                    throw responseException;
                }

                result.ClientPassword = subject.ClientPassword;
                result.Password = subject.Password;

                List<ChallengeQuestionResult> challengeQuestions = new List<ChallengeQuestionResult>();

                if (subject.SecurityQuestion.HasValue || !string.IsNullOrEmpty(subject.SecurityAnswer))
                {
                    challengeQuestions.Add(new ChallengeQuestionResult { Question = subject.SecurityQuestion, Answer = subject.SecurityAnswer });
                    result.ChallengeQuestions = challengeQuestions;
                }
            }
            catch (HttpResponseException httpEx)
            {
                throw httpEx;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                HttpResponseException responseException = new HttpResponseException(HttpStatusCode.InternalServerError);
                if (ex is SqlException || ex is WebException)
                {
                    responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.ServerError);
                }
                throw responseException;
            }

            return Request.CreateResponse<CredentialResult>(HttpStatusCode.OK, result);
        }

        public HttpResponseMessage POST(string id, [FromBody] CredentialModel credential)
        {
            CredentialResult result = new CredentialResult();

            try
            {
                SubjectRepository subjectRep = new SubjectRepository(db);
                Subject subject = subjectRep.FindByPin(credential.SetupCode);
                StudyMiddleTier studyMiddleTier = db.StudyMiddleTiers.Where(s => s.StudyId == StudyData.study.Id).SingleOrDefault();
                bool activeLPADevice = false;
                int? swDeviceId = null;

                if (subject == null)
                {
                    logger.Error(String.Format(CultureInfo.InvariantCulture, "Subject for setupCode provided in request body does not exist."));
                    HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                    responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.InvalidSetupCode);
                    throw responseException;
                }

                if (!string.Equals(id, subject.KrPT, StringComparison.OrdinalIgnoreCase))
                {
                    logger.Error(String.Format(CultureInfo.InvariantCulture, "Krpt in incoming url does not match krpt for the subject. Krpt in incoming url: {0}, krpt for the subject: {1}", id, subject.KrPT));
                    HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                    responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.KrptNotMatch);
                    throw responseException;
                }

                if (subject.IsLockedOut)
                {
                    logger.Error(String.Format(CultureInfo.InvariantCulture, "The subject is locked out. Krpt: {0}", subject.KrPT));
                    HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                    responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.UserLockedOut);
                    throw responseException;
                }

                //Prevent updating challenge questions from deactivated LPA device
                if (!string.IsNullOrEmpty(credential.Source) && string.Equals(credential.Source.Trim(), SWAPIConstants.SourceLogPadApp, StringComparison.OrdinalIgnoreCase))
                {
                    if (string.IsNullOrEmpty(credential.ClientID) || credential.ClientID.Trim().Length == 0)
                    {
                        logger.Error(String.Format(CultureInfo.InvariantCulture, "ClientID - DeviceID is not provided for a LogPad App device for a password set(reset). Krpt: {0}", subject.KrPT));
                        HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                        responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.NoClientIDProvided);
                        throw responseException;
                    }

                    SubjectDeviceRepository deviceRepo = new SubjectDeviceRepository(db);
                    SubjectDevice currentDevice = deviceRepo.FindLatestBySubjectId(subject.Id);
                    if (currentDevice == null || !string.Equals(currentDevice.DeviceCode.ToString(), credential.ClientID, StringComparison.OrdinalIgnoreCase)) // not active device
                    {
                        logger.Error(String.Format(CultureInfo.InvariantCulture, "LogPad App Device for ClientID - DeviceID is not active device. Krpt: {0}, ClientID - DeviceID: {1}", subject.KrPT, credential.ClientID));
                        HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                        responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.NotActiveClientID);
                        throw responseException;
                    }
                    activeLPADevice = true;
                }

                bool firstActivationWithoutQA = string.IsNullOrEmpty(subject.Password) && credential.ChallengeQuestions == null;
                bool firstActivationWithQA = string.IsNullOrEmpty(subject.Password) && credential.ChallengeQuestions != null;

                if (studyMiddleTier.AuthenticationLevel == AuthenticationLevel.SecurityQuestion && firstActivationWithoutQA
                        || studyMiddleTier.AuthenticationLevel == AuthenticationLevel.Basic && firstActivationWithQA)
                    throw new HttpResponseException(HttpStatusCode.Forbidden);

                // Log to studyworks
                WDAPIObject wdapi = new WDAPIObject(StudyData.study.StudyWorksUsername, StudyData.study.StudyWorksPassword, StudyData.study.Name, StudyData.study.StudyWorksBaseUrl);

                if (!wdapi.IsSubjectEnabled(subject.KrPT))
                {
                    logger.Error(String.Format(CultureInfo.InvariantCulture, "The subject is not enabled. Krpt: {0}", subject.KrPT));
                    HttpResponseException userNotFoundException = new HttpResponseException(HttpStatusCode.Forbidden);
                    userNotFoundException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.UserNotFound);
                    throw userNotFoundException;
                }

                // Transmission time
                DateTime serverReceiptTimeUtc = DateTime.UtcNow;
                DateTimeOffset dateOffset = Request.Headers.Date.Value;

                SubjectData subjectData = wdapi.GetSubjectData(subject.KrPT,
                                                                new ArrayList {SWAPIConstants.ColumnPatientId,
                                                                    SWAPIConstants.ColumnPatientInitials,
                                                                    SWAPIConstants.ColumnPatientKrDom,
                                                                    SWAPIConstants.ColumnPatientKrPhase,
                                                                    SWAPIConstants.ColumnPatientPhaseDate,
                                                                    SWAPIConstants.ColumnPatientSigId,
                                                                    SWAPIConstants.ColumnPatientSigOrig,
                                                                    SWAPIConstants.ColumnPatientCustom10 });

                string serviceEncryptedPassword = result.Password = SubjectHelper.EncodePassword(credential.ClientPassword, subject.KrPT);
                Boolean initialActivation = false;
                DateTime utcNow = DateTime.UtcNow;

                if (string.IsNullOrEmpty(subject.Password)) //First activation
                {
                    initialActivation = true;

                    // Update the subject record in the NetPRO database
                    subject.IsApproved = true;
                    subject.LastLoginDate = utcNow;
                    subject.LastActivityDate = utcNow;
                    subject.LastPasswordChangedDate = utcNow;
                    if (firstActivationWithQA)
                    {
                        subject.SecurityQuestion = credential.ChallengeQuestions.SingleOrDefault().Question.Value;
                        subject.SecurityAnswer = credential.ChallengeQuestions.SingleOrDefault().Answer;
                        subject.LastSecurityQuestionAnswerChanged = utcNow;
                    }
                }
                else // Forgot password or unlock code 
                {
                    // invalid unlock code
                    bool badCode = Request.Headers.Contains("Unlock-Code") && !CheckUnlockCode.IsValidCode(studyMiddleTier, Request.Headers.Date.Value.DateTime, Request.Headers.GetValues("Unlock-Code").FirstOrDefault());

                    if (badCode)
                        throw new HttpResponseException(HttpStatusCode.Unauthorized);

                    if (studyMiddleTier.AuthenticationLevel == AuthenticationLevel.SecurityQuestion)
                    {
                        if (credential.ChallengeQuestions == null)
                            throw new HttpResponseException(HttpStatusCode.Forbidden);

                        ChallengeQuestionModel cq = credential.ChallengeQuestions.SingleOrDefault();

                        //verify security question and answer further
                        if (!cq.Question.HasValue || string.IsNullOrEmpty(cq.Answer) || cq.Answer.Trim().Length == 0)
                        {
                            throw new HttpResponseException(HttpStatusCode.Forbidden);
                        }

                        if (!Request.Headers.Contains("Unlock-Code") && (subject.SecurityQuestion.Value != cq.Question.Value || subject.SecurityAnswer != cq.Answer))
                        {
                            logger.Error(String.Format(CultureInfo.InvariantCulture, "Security question and answer for the subject does not match existing ones. Krpt: {0}", subject.KrPT));
                            HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                            responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.SecurityAnswerDoesNotMatch);
                            throw responseException;
                        }

                        if (cq.Question.HasValue && subject.SecurityQuestion.Value != cq.Question.Value)
                        {
                            subject.LastSecurityQuestionAnswerChanged = utcNow;
                            subject.SecurityQuestion = cq.Question.Value;
                        }

                        if (!string.IsNullOrEmpty(cq.Answer) && subject.SecurityAnswer != cq.Answer)
                        {
                            subject.LastSecurityQuestionAnswerChanged = utcNow;
                            subject.SecurityAnswer = cq.Answer;
                        }
                    }
                    else //AuthenticationLevel.Basic which is no securityQuestion
                    {
                        bool notAllowedAction = !Request.Headers.Contains("Unlock-Code") && !string.IsNullOrEmpty(subject.Password);
                        if (notAllowedAction)
                        {
                            logger.Error(String.Format(CultureInfo.InvariantCulture, "Password reset is not allowed for the call. Krpt: {0}", subject.KrPT));
                            HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                            responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.PasswordResetNotAllowed);
                            throw responseException;
                        }

                        if (credential.ChallengeQuestions != null)
                            throw new HttpResponseException(HttpStatusCode.Forbidden);
                    }

                    if (subject.Password != serviceEncryptedPassword)
                    {
                        subject.LastPasswordChangedDate = utcNow;
                    }
                }

                if (credential.RegDeviceId.HasValue)
                    swDeviceId = credential.RegDeviceId;

                if (activeLPADevice)
                {
                    Guid deviceCode = new Guid(credential.ClientID);
                    SubjectDeviceRepository subjectDeviceRep = new SubjectDeviceRepository(db);
                    SubjectDevice subjectDevice = subjectDeviceRep.FindBySubjectIdDeviceCode(subject.Id, deviceCode);
                    subjectDevice.ClientPassword = credential.ClientPassword;
                    subjectDevice.Password = serviceEncryptedPassword;
                    if (!swDeviceId.HasValue)
                        swDeviceId = subjectDevice.SWDeviceId;
                }

                subject.Password = serviceEncryptedPassword;
                subject.ClientPassword = credential.ClientPassword;

                string triggeredPhase = string.Empty;
                if (initialActivation)
                {
                    if (!string.IsNullOrEmpty(subjectData.data.custom10) && subjectData.data.custom10.Trim().Length > 0)
                    {
                        string protocolTriggeredPhase = StudyData.study.triggeredPhaseAtInitialActivation;
                        if (!string.IsNullOrEmpty(protocolTriggeredPhase))
                            triggeredPhase = TriggeredPhase.GetTriggeredPhase(subjectData.data.custom10.Trim(), protocolTriggeredPhase);
                    }
                }

                // submit activation report                        
                SubmitActivationReport(subject, subjectData, dateOffset, serverReceiptTimeUtc, wdapi, swDeviceId, credential.ClientID, credential.Source, triggeredPhase);

                wdapi.BeginSendSecurityEvent(HttpHelper.GetIP4Address(), SWAPIAbstraction.SecurityEventSet, DateTime.UtcNow, subject.KrPT,
                        DateTimeHelper.GetTimeZoneOffsetMilliseconds(subject.TimeZone.ToString(CultureInfo.InvariantCulture), StudyData.study.Id), "Account activated", new AsyncCallback(SendSecurityEventCallback));

                // Save Database
                db.SubmitChanges();          
            }
            catch (HttpResponseException httpEx)
            {
                throw httpEx;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                HttpResponseException responseException = new HttpResponseException(HttpStatusCode.InternalServerError);
                if (ex is SqlException || ex is WebException)
                {
                    responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.ServerError);
                }
                throw responseException;
            }

            return Request.CreateResponse(HttpStatusCode.Created, result);
        }

        [Authorize]
        public HttpResponseMessage PUT(string id, [FromBody] UpdateCredentialModel credential)
        {
            CredentialResult credentialResult = new CredentialResult();
            WebApiPrincipal principal = Thread.CurrentPrincipal as WebApiPrincipal;

            try
            {
                SubjectRepository subjectRep = new SubjectRepository(db);
                Subject subject = subjectRep.FindByKrptAndStudy(id, StudyData.study.Id);

                if (subject == null)
                {
                    logger.Error(String.Format(CultureInfo.InvariantCulture, "Subject for krpt in incoming url does not exist. Krpt in incoming url: {0}", id));
                    HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                    responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.InvalidKrPT);
                    throw responseException;
                }

                if (!principal.ApiTokenAuth && principal.UserID != subject.Pin)
                {
                    HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Unauthorized);
                    throw responseException;
                }

                //Can not update subject password if subject account is not activated yet
                if (string.IsNullOrEmpty(subject.Password))
                {
                    logger.Error(String.Format(CultureInfo.InvariantCulture, "Subject account is not activated yet. Krpt: {0}", subject.KrPT));
                    HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                    responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.UserIsNotActive);
                    throw responseException;
                }

                //Prevent updating subject password from deactivated LPA device
                if (!principal.ApiTokenAuth)
                {
                    SubjectDeviceRepository deviceRep = new SubjectDeviceRepository(db);
                    SubjectDevice currentDevice = deviceRep.FindLatestBySubjectId(principal.SubjectID);
                    if (!string.Equals(currentDevice.DeviceCode.ToString(), principal.DeviceID.ToString(), StringComparison.OrdinalIgnoreCase)) // not active device
                    {
                        logger.Error(String.Format(CultureInfo.InvariantCulture, "LogPad App Device for ClientID - DeviceID is not active device. Krpt: {0}, ClientID - DeviceID: {1}", subject.KrPT, principal.DeviceID.ToString()));
                        HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                        responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.NotActiveClientID);
                        throw responseException;      
                    }
                }

                StudyMiddleTier studyMiddleTier = db.StudyMiddleTiers.Where(s => s.StudyId == StudyData.study.Id).SingleOrDefault();

                if (studyMiddleTier.AuthenticationLevel == AuthenticationLevel.SecurityQuestion && credential.ChallengeQuestions == null
                        || studyMiddleTier.AuthenticationLevel == AuthenticationLevel.Basic && credential.ChallengeQuestions != null)
                    throw new HttpResponseException(HttpStatusCode.Forbidden);

                //Check clientPassword
                if (string.IsNullOrEmpty(credential.ClientPassword) || credential.ClientPassword.Trim().Length == 0)
                {
                    HttpResponseException responseException = new HttpResponseException(HttpStatusCode.BadRequest);
                    throw responseException;
                }

                if (studyMiddleTier.AuthenticationLevel == AuthenticationLevel.SecurityQuestion)
                {
                    ChallengeQuestionModel cq = credential.ChallengeQuestions.SingleOrDefault();

                    //verify security question and answer
                    if (!subject.SecurityQuestion.HasValue || !cq.Question.HasValue || string.IsNullOrEmpty(cq.Answer) || cq.Answer.Trim().Length == 0)
                    {
                        throw new HttpResponseException(HttpStatusCode.Forbidden);
                    }

                    if (subject.SecurityQuestion.Value != cq.Question.Value || subject.SecurityAnswer != cq.Answer)
                    {
                        logger.Error(String.Format(CultureInfo.InvariantCulture, "Security question and answer for the subject does not match existing ones. Krpt: {0}", subject.KrPT));
                        HttpResponseException responseException = new HttpResponseException(HttpStatusCode.Forbidden);
                        responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.SecurityAnswerDoesNotMatch);
                        throw responseException;
                    }
                }

                string serviceEncryptedPassword = SubjectHelper.EncodePassword(credential.ClientPassword, subject.KrPT);

                if (!principal.ApiTokenAuth)
                {
                    SubjectDeviceRepository subjectDeviceRep = new SubjectDeviceRepository(db);
                    SubjectDevice subjectDevice = subjectDeviceRep.FindBySubjectIdDeviceCode(subject.Id, principal.DeviceID);
                    subjectDevice.ClientPassword = credential.ClientPassword;
                    subjectDevice.Password = serviceEncryptedPassword;
                }

                subject.ClientPassword = credential.ClientPassword;
                subject.Password = credentialResult.Password = serviceEncryptedPassword;
                subject.LastPasswordChangedDate = DateTime.UtcNow;
                db.SubmitChanges();
            }
            catch (HttpResponseException httpEx)
            {
                throw httpEx;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                HttpResponseException responseException = new HttpResponseException(HttpStatusCode.InternalServerError);
                if (ex is SqlException || ex is WebException)
                {
                    responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.ServerError);
                }
                throw responseException;
            }
            return Request.CreateResponse<CredentialResult>(HttpStatusCode.OK, credentialResult);
        }

        private void SubmitActivationReport(Subject subject, SubjectData subjectData, DateTimeOffset dateOffset, DateTime serverReceiptTimeUtc, WDAPIObject wdapi, int? regDeviceId, string clientID, string source, string triggeredPhase)
        {
            Study study = subject.Study;

            // construct survey data
            SurveyData surveyData = new SurveyData();

            // form level data
            DateTime diaryReportDate = dateOffset.UtcDateTime;
            DateTime diaryStartUtc = dateOffset.UtcDateTime;

            // phase
            surveyData.SuPhaseAtEntry = subjectData.data.Krphase;
            DateTime SUPhaseStartDate = DateTime.MinValue;
            string phaseDateString = "";
            if (subjectData.data.Phasedate.Contains('.'))
            {
                string[] str = subjectData.data.Phasedate.Split('.');
                phaseDateString = DateTimeHelper.ChangeFormat(str[0], SWAPIConstants.SwWeirdDateTimeFormat, SWAPIConstants.SwDateTimeFormat);
            }
            else
            {
                phaseDateString = subjectData.data.Phasedate;
            }
            if (!DateTime.TryParseExact(phaseDateString, SWAPIConstants.SwDateTimeFormat, CultureInfo.InvariantCulture,
                                    DateTimeStyles.None, out SUPhaseStartDate))
            {
                throw new WDAPIException(WDErrors.CannotFormatDateTime,
                    "The datetime format for \"" + WDConstants.VarSuPhaseStartDate + "\" is incorrect. Should be \"" + SWAPIConstants.SwDateTimeFormat + "\"");
            }
            surveyData.SuPhaseStartDate = DateTime.SpecifyKind(SUPhaseStartDate, DateTimeKind.Local);


            surveyData.Id = Guid.NewGuid();
            surveyData.ExpiryDate = DateTime.UtcNow.AddDays(1);
            surveyData.Krpt = subject.KrPT;
            surveyData.Krdom = subjectData.data.krdom;
            surveyData.StudyId = study.Id;
            surveyData.PrintedName = subjectData.data.Initials;
            surveyData.SuReportDate = diaryReportDate;
            surveyData.PatientId = subjectData.data.Patientid;
            surveyData.SurveyStartDate = diaryStartUtc;
            surveyData.SurveyId = 0;

            // empty survey records
            SurveyResponseData surveyResponses = new SurveyResponseData();

            string krSU = "Activation";
            string formType = SWAPIConstants.FormTypeSubjectSubmit;
            double clientTZOffset = dateOffset.Offset.TotalSeconds;

            // Construct the clin record
            ClinRecord clinRecord = WDAPIObject.GenerateClinicalRecord(surveyResponses, surveyData, triggeredPhase, formType, krSU, string.Empty, DateTime.UtcNow, clientTZOffset, null);

            // SW Device ID
            if (regDeviceId.HasValue)
            {
                ClinField lpaDeviceId = new ClinField();
                lpaDeviceId.sysvar = SWAPIConstants.SuLogPadDeviceId;
                lpaDeviceId.krit = "";
                lpaDeviceId.comment = SWAPIConstants.SystemComment;
                lpaDeviceId.value = regDeviceId.Value.ToString(CultureInfo.InvariantCulture);
                lpaDeviceId.edit = false;
                clinRecord.sysvars.Add(lpaDeviceId);
            }

            // Validate signatures in the clin record
            if (string.IsNullOrEmpty(clinRecord.ptsigorig))
            {
                foreach (var field in clinRecord.sysvars)
                {
                    if (field.sysvar.StartsWith("pt.", StringComparison.OrdinalIgnoreCase))
                    {
                        if (subjectData != null)
                        {
                            clinRecord.ptsigorig = subjectData.data.Sigorig;
                            clinRecord.ptsigprev = subjectData.data.Sigid;
                        }
                        break;
                    }
                }
            }

            // Set NetProInformation IG - Note: Needs to be customized here and in StudyWorks to distinguish LF from NetPRO            
            List<ClinField> NetPROInfo = new List<ClinField>(5);

            ClinField NPVersion = new ClinField();
            NPVersion.krit = "NPVersion";
            if (string.IsNullOrEmpty(StudyData.version.ProductMicroVersion) || StudyData.version.ProductMicroVersion == "0")
                NPVersion.value = StudyData.version.ProductMajorVersion + "." + StudyData.version.ProductMinorVersion;
            else
                NPVersion.value = StudyData.version.ProductMajorVersion + "." + StudyData.version.ProductMinorVersion + "." + StudyData.version.ProductMicroVersion;
            NPVersion.edit = false;

            ClinField NPStudyVersion = new ClinField();
            NPStudyVersion.krit = "NPStudyVersion";
            NPStudyVersion.value = StudyData.version.StudyMajorVersion + "." + StudyData.version.StudyMinorVersion;
            NPStudyVersion.edit = false;

            ClinField OSVersion = new ClinField();
            OSVersion.krit = "OSVersion";
            OSVersion.value = HttpContext.Current.Request.Browser.Platform;
            OSVersion.edit = false;

            ClinField browserType = new ClinField();
            browserType.krit = "BrowserName";
            browserType.value = HttpContext.Current.Request.Browser.Type;
            browserType.edit = false;

            ClinField browserVersion = new ClinField();
            browserVersion.krit = "BrowserVersion";
            browserVersion.value = HttpContext.Current.Request.Browser.Version;
            browserVersion.edit = false;

            ClinField clientCoreVersion = new ClinField();

            ClinField clientStudyVersion = new ClinField();

            if (!string.IsNullOrEmpty(source) && string.Equals(source.Trim(), SWAPIConstants.SourceSitePadApp, StringComparison.OrdinalIgnoreCase))
            {
                clientCoreVersion.krit = "SitePadAppCoreVersion";
                clientCoreVersion.value = string.Empty;
                clientCoreVersion.edit = false;

                clientStudyVersion.krit = "SitePadAppStudyVersion";
                clientStudyVersion.value = string.Empty;
                clientStudyVersion.edit = false;
            }
            else
            {
                clientCoreVersion.krit = "LogPadAppCoreVersion";
                clientCoreVersion.value = string.Empty;
                clientCoreVersion.edit = false;

                clientStudyVersion.krit = "LogPadAppStudyVersion";
                clientStudyVersion.value = string.Empty;
                clientStudyVersion.edit = false;
            }

            ClinField deviceId = new ClinField();
            deviceId.krit = "ClientID";
            if (string.IsNullOrEmpty(clientID))
                deviceId.value = string.Empty;
            else
                deviceId.value = clientID;
            deviceId.edit = false;

            ClinField dateTimeSubmitted = new ClinField();
            dateTimeSubmitted.krit = "DateTimeSubmitted";
            dateTimeSubmitted.value = dateOffset.UtcDateTime.ToString(SWAPIConstants.SwDateTimeFormat, CultureInfo.InvariantCulture);
            dateTimeSubmitted.edit = false;

            ClinField dateTimeReceived = new ClinField();
            dateTimeReceived.krit = "DateTimeReceived";
            dateTimeReceived.value = serverReceiptTimeUtc.ToString(SWAPIConstants.SwDateTimeFormat, CultureInfo.InvariantCulture);
            dateTimeReceived.edit = false;

            NetPROInfo.Add(NPVersion);
            NetPROInfo.Add(NPStudyVersion);
            NetPROInfo.Add(OSVersion);
            NetPROInfo.Add(browserType);
            NetPROInfo.Add(browserVersion);
            NetPROInfo.Add(clientCoreVersion);
            NetPROInfo.Add(clientStudyVersion);
            NetPROInfo.Add(deviceId);
            NetPROInfo.Add(dateTimeSubmitted);
            NetPROInfo.Add(dateTimeReceived);

            Dictionary<string, List<ClinField>> ig = new Dictionary<string, List<ClinField>>();
            ig.Add("0", NetPROInfo);
            clinRecord.iglist.Add("NetProInformation", ig);

            //Set Protocol
            if (!string.IsNullOrEmpty(subjectData.data.custom10) && subjectData.data.custom10.Trim().Length > 0)
            {
                List<ClinField> protocolInfo = new List<ClinField>(1);
                ClinField protocol = new ClinField();
                protocol.krit = SWAPIConstants.ITProtocol;
                protocol.value = subjectData.data.custom10;
                protocol.edit = false;
                protocolInfo.Add(protocol);

                Dictionary<string, List<ClinField>> IgProtocol = new Dictionary<string, List<ClinField>>();
                IgProtocol.Add("0", protocolInfo);
                clinRecord.iglist.Add(SWAPIConstants.IGProtocol, IgProtocol);
            }

            //Set krUSER.
            clinRecord.kruser = study.StudyWorksUsername;

            //Set source
            if (!string.IsNullOrEmpty(source))
                clinRecord.source = source.Trim();

            // Submit
            wdapi.SubmitClinReportRRInk(clinRecord);

        }

        private void SendSecurityEventCallback(IAsyncResult ia)
        {
            try
            {
                var ar = (System.Runtime.Remoting.Messaging.AsyncResult)ia;
                var del = (SendSecurityEventDelegate)ar.AsyncDelegate;
                del.EndInvoke(ia);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                db.Dispose();
            base.Dispose(disposing);
        }
    }
}