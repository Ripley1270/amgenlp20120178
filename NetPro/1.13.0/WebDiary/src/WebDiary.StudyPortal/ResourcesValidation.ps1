﻿#script to validate resoursces before installation
#author Adalberto DePina
Add-Type -AssemblyName System.Windows.Forms
$scriptPath = split-path -parent $MyInvocation.MyCommand.Definition
set-location $scriptPath

function prompt { "$pwd\" }

$strMsg = $null
$resourcevalidationfilename = $scriptPath + "\resourcevalidation.txt"
$errorfound = 0

$coreEngFile = $null
$pdeEngFile =  $null

#check for error. If errors exists apend to log and display a dialog informing user
function checkErrors($res)
{
     if ($errorfound -eq 1) {
         $strMsg | Out-File  $resourcevalidationfilename
         write-host $strMsg      
         [System.Windows.Forms.MessageBox]::Show($strMsg , "Resource Validation Failed")  
         
         if($res.CompareTo("core") -eq 0 ) 
         { 
              
         }
         
         if($res.CompareTo("pde") -eq 0 ) 
         {                                 
             exit 2
         }
     }
     else
     {
         $strMsg = $res + " Resources Validation Finished successfully!" 
         write-host $strMsg   
     }
}


#validate core resources
function validateCoreResources {
     
    $location = prompt    
    
    write-host "Location" $location  
        
    $coreEngFile = "$location\App_GlobalResources\Resource.resx";
    If (Test-Path $coreEngFile){
        # // File exists    
        #read the English Core Resource file    
        $core_xml = [xml] (get-content $coreEngFile)     
    
        #Get the number of core nodes to compare
        $core_eng = $core_xml.SelectNodes("//data")
        $core_engCount = $core_eng.Count
        #Write-Host ("the number of child node count is " + $core_engCount)    
         
        # read languages  Resource files to find number of files
        $files = Get-ChildItem "$location\App_GlobalResources\"    
            
        #Write-Host ("File Count in the directory " + $files.Count);
        
        #Get the files
        For ($i=0; $i -le $files.Count; $i++)   
        {      
           $file = $files[$i].Name;
           if($file -ne $null)
           {
               if(($file.StartsWith("Resource")) -and (!$file.ToLower().Contains("designer")) -and ($file -ne ("Resource.resx"))) 
               {
                   #Write-Host ("File in the directory " + $file);                       
                   $languages_xml = [xml] (get-content $location\App_GlobalResources\$file) 
                   #Get the number of nodes to compare
                   $core_language = $languages_xml.SelectNodes("//data")
                   $core_languageCount  = $core_language.Count
                   #Write-Host ("the number of core language node is " + $core_languageCount)    
               
                   if($core_engCount -ne $core_languageCount)
                   {                   
                       $strMsg += "`r" + "`n" + "Core Resources Validation Failed! " + "Number of Resource in Core English file does not match the number of Resources in " + $file + " Core Language file.";
                       $errorfound  = 1;                  
                   }
                   else #number of nodes match. validate nodes
                   {
                        $languages_xml = Get-Content -Path "$location\App_GlobalResources\$file" | Out-String 
         
                        #Write-Host ("languages_xml " + $languages_xml )                    
                   
                       foreach ($data in $core_eng) {
                       $core_data_name = $data.name
                       #echo "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx " + $core_data_name
                       $strToCompare= "name=" + '"'  + $core_data_name  + '"' + ""          
                       
                                                     
                       if($languages_xml -NotMatch $strToCompare)
                       {
                           $strMsg += "`r" + "`n" + "Core Resources Validation Failed! " + "Core Resource " + $core_data_name + " not found in " + $file + ".";
                           $errorfound  = 1;                            
                       }                                    
                   }
               }  
           }
       }
    }
    checkErrors("core")
    }Else{
      $strMsg += "`r" + "`n" + "English Core Resource file not found.";
      $errorfound  = 1;
      checkErrors("core")
  }
    
}

#validate PDE resources
function validatePDEResources {
     
    $location = prompt    
    
    $pdeEngFile = "$location\App_GlobalResources\PDEResource.resx";
    
    If (Test-Path $pdeEngFile){
     # // File exists        
        #read the English Core Resource file    
        $pde_xml = [xml] (get-content $location\App_GlobalResources\PDEResource.resx)    
   
   
        #Get the number of core nodes to compare
        $pde_eng = $pde_xml.SelectNodes("//data")
        $pde_engCount = $pde_eng.Count
        #Write-Host ("the number of pde child node count is " + $pde_engCount)    
    
     
       # read languages  Resource files to find number of files
        $files = Get-ChildItem "$location\App_GlobalResources\"    
           
        #Write-Host ("File Count in the directory " + $files.Count);
      
        #Get the files
        For ($i=0; $i -le $files.Count; $i++)   
        {      
           $file = $files[$i].Name;
           if($file -ne $null)
           {
               if(($file.StartsWith("PDEResource")) -and (!$file.ToLower().Contains("designer")) -and ($file -ne ("PDEResource.resx"))) 
               {
                   #Write-Host ("File in the directory " + $file);                       
                   $languages_xml = [xml] (get-content $location\App_GlobalResources\$file) 
                   #Get the number of nodes to compare
                   $pde_language = $languages_xml.SelectNodes("//data")
                   $pde_languageCount  = $pde_language.Count
                   #Write-Host ("the number of PDE language node is " + $pde_languageCount)    
               
                   if($pde_engCount -ne $pde_languageCount)
                   {                   
                       $strMsg += "`r" + "`n" + "PDE Resources Validation Failed! " + "Number of Resource in PDE English file does not match the number of Resources in " + $file + " PDE Language file.";
                       $errorfound  = 1;                  
                   }
                   else #number of nodes match. validate nodes
                   {
                        $languages_xml = Get-Content -Path "$location\App_GlobalResources\$file" | Out-String 
         
                        #Write-Host ("languages_xml " + $languages_xml )
                 
                        foreach ($data in $pde_eng) {
                       $pde_data_name = $data.name
                       #echo "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx " + $pde_data_name
                       
                       $strToCompare= "name=" + '"'  + $pde_data_name  + '"' + ""                       
                       
                       if($languages_xml -NotMatch $strToCompare)
                       {
                           $strMsg += "`r" + "`n" + "PDE Resources Validation Failed! " + "PDE Resource " + $pde_data_name + " not found in " + $file + ".";
                           $errorfound  = 1;                               
                       }                  
                   }
               }  
           }
       }
    }
        checkErrors("pde")
    }Else{
      #$strMsg += "`r" + "`n" + "English PDE Resource file not found.";
      #$errorfound  = 1;
      #checkErrors("pde")
  }
}

$resvalfile = $scriptPath + "\resourcevalidation.txt"
If (Test-Path $resvalfile){
    Remove-Item $resourcevalidationfilename
}
validateCoreResources
validatePDEResources

