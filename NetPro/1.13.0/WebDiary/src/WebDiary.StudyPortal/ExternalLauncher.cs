﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;

using WebDiary.Core.Entities;
using WebDiary.Controls;
using WebDiary.Core.Helpers;
using WebDiary.Core.Repositories;
using WebDiary.Core.Constants;
using WebDiary.SWAPI;
using WebDiary.SWAPI.SWData;
using WebDiary.WDAPI;
using WebDiary.SurveyToolDBAdapters;
using WebDiary.SurveyToolDBAdapters.Checkbox_Entities;

using log4net;
using System.Collections;
using WebDiary.Core.Errors;
using WebDiary.Membership.User;
using WebDiary.Membership.Provider;

namespace WebDiary.StudyPortal
{
    /// <summary>
    /// Use this class' static method to launch an EuroQOL survey.  Launching is accomplished by constructing a completed POST form and sending it to a client, with scripting to make it POST immediately upon load.
    /// </summary>
    public sealed class ExternalLauncher
    {
        private ExternalLauncher() { }

        /// <summary>
        /// Launches a EuroQOL survey.
        /// </summary>
        /// <param name="request">The current HTTP Request object</param>
        /// <param name="response">The current HTTP Response object</param>
        /// <param name="survey">The external survey</param>
        /// <param name="surveyData">The data specific to this external survey</param>
        public static void LaunchExternalSurvey(HttpRequest request, HttpResponse response, StudySurveys survey, SurveyData surveyData)
        {
            string[] postElements = survey.ActivationURL.Split('&');

            var postBuilder = new System.Text.StringBuilder();
            var bodyBuilder = new System.Text.StringBuilder();

            string eqTimeZone = GetEQTimeZoneCode(survey.StudyId, surveyData.SuTzValue);
            string eqLanguage = GetEQLanguageCode();

            // Reset the EQ backup session variable
            HttpContext.Current.Session[WDConstants.EQBackup] = "0";

            // Begin constructing the POST form
            postBuilder.Append("<html>");
            postBuilder.Append("<head><script type=\"text/javascript\" src=\"includes/scripts/jquery-latest.min.js\"></script></head>");
            postBuilder.Append("<body onload='EQBackupCheck()'>");
            postBuilder.Append("<script type=\"text/javascript\" language=\"javascript\">");
            postBuilder.Append("function EQBackupCheck() { ");
            postBuilder.Append("var jqxhr = $.ajax({ ");
            postBuilder.Append("type: \"POST\", ");
            postBuilder.Append("url: \"");
            postBuilder.Append("SurveySupport.aspx/EQBackupSwap\", ");
            postBuilder.Append("contentType: \"application/json; charset=utf-8\", ");
            postBuilder.Append("cache: false");
            postBuilder.Append("}); ");
            postBuilder.Append("jqxhr.done(function (data, textStatus, obj) { ");
            postBuilder.Append("var result = data.d; ");
            postBuilder.Append("if (result == '0') { document.getElementById('eq5dform').submit(); } else { ");
            postBuilder.Append("history.back(); }");
            postBuilder.Append("}); ");
            postBuilder.Append("}");
            postBuilder.Append("</script>");
            
            // Parse the POST data extracted from the custom survey XML.  Extract the url to post to from the data and include it in the form header.
            foreach (string field in postElements)
            {
                string[] fieldSplit = field.Split('=');
                // PBU is the Post Back Url, allowing the survey to send data back to NetPRO when it finishes
                if (fieldSplit[0] == "PBU")
                {
                    string pbBaseUrl = fieldSplit.Length > 0 ? fieldSplit[1] : StudyData.study.WebDiaryStudyBaseUrl.TrimEnd('/');
                    if (!pbBaseUrl.EndsWith("/", StringComparison.Ordinal))
                        pbBaseUrl += "/";
                    bodyBuilder.AppendFormat("<input type='hidden' name='PBU' value='{0}submit.aspx?psd_id={1}&provider=EuroQOL' />",
                        pbBaseUrl, surveyData.Id);
                }
                // EuroQOLUrl is where to post this form to
                else if (fieldSplit[0] == "EuroQOLUrl")
                    postBuilder.AppendFormat("<form action='{0}' method='post' id=\"eq5dform\">", fieldSplit.Length > 0 ? fieldSplit[1] : "");
                else if (fieldSplit[0] == "LID")
                    postBuilder.AppendFormat("<input type='hidden' name='LID' value='{0}' />", !string.IsNullOrEmpty(eqLanguage) ? eqLanguage : (fieldSplit.Length > 0 ? fieldSplit[1] : ""));
                else if (fieldSplit[0] == "DTZ")
                    postBuilder.AppendFormat("<input type='hidden' name='DTZ' value='{0}' />", !string.IsNullOrEmpty(eqTimeZone) ? eqTimeZone : (fieldSplit.Length > 0 ? fieldSplit[1] : ""));
                else
                    bodyBuilder.AppendFormat("<input type='hidden' name='{0}' value='{1}' />", fieldSplit[0], fieldSplit.Length > 0 ? fieldSplit[1] : "");
            }
            string[] nameSplit = surveyData.PrintedName.Split(' ');
            bodyBuilder.AppendFormat("<input type='hidden' name='FNM' value='{0}' />", nameSplit[0]);
            if (nameSplit.Length > 1)
                bodyBuilder.AppendFormat("<input type='hidden' name='LNM' value='{0}' />", nameSplit[1]);
            bodyBuilder.AppendFormat("<input type='hidden' name='RID' value='{0}' />", surveyData.Id);

            bodyBuilder.Append("</form>");

            bodyBuilder.Append("</body></html>");
            postBuilder.Append(bodyBuilder);
            HttpContext.Current.Session[WDConstants.FramePostUrl] = postBuilder.ToString();
            response.Redirect("~/survey.aspx?psd_id=" + surveyData.Id, true);  
        }

        public static string GetEQTimeZoneCode(int studyId, int? swTimeZoneId)
        {
            if (swTimeZoneId.HasValue)
            {
                using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                {
                    TimeZoneRepository timeZoneRepos = new TimeZoneRepository(db);
                    TimeZones zone = timeZoneRepos.FindByStudyIdAndTimeZoneId(studyId, swTimeZoneId.Value);
                    TimeZoneRuleRepository ruleRepos = new TimeZoneRuleRepository(db);
                    TimeZoneRules rule = ruleRepos.FindByTimeZoneName(zone.TZName);
                    EQTimeZoneRepository eqTimeZoneRepos = new EQTimeZoneRepository(db);
                    EQTimeZones eqzone = eqTimeZoneRepos.FindByTZNameAndDaylight(rule.Name, rule.currently_dst == 1, studyId);
                    if (eqzone != null)
                        return eqzone.EQTZId.ToString(CultureInfo.InvariantCulture);
                }
                
            }
            return null;
        }

        public static string GetEQLanguageCode()
        {
            var session = HttpContext.Current.Session;
            if (session[WDConstants.Language] != null && !string.IsNullOrEmpty(session[WDConstants.Language].ToString()))
            {
                using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                {
                    EQLanguageRepository langRepos = new EQLanguageRepository(db);
                    EQLanguages lang = langRepos.FindByCulture(session[WDConstants.Language].ToString(), StudyData.study.Id);
                    if (lang != null)
                        return lang.EQLanguageId.ToString(CultureInfo.InvariantCulture);
                }
            }
            return null;
        }
    }
}
