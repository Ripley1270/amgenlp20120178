﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebDiary.Core.Constants;
using WebDiary.Core.Helpers;
using WebDiary.Core.Errors;

namespace WebDiary.StudyPortal
{
    public sealed class EQData
    {
        private EQData() { }

        private const string AnxietyDepression = "AnxietyDepression";
        private const string AnxietyDepressionKey = "EQ5D.0.EQ5D5";

        private const string Mobility = "Mobility";
        private const string MobilityKey = "EQ5D.0.EQ5D1";

        private const string Paindiscomfort = "Paindiscomfort";
        private const string PaindiscomfortKey = "EQ5D.0.EQ5D4";

        private const string Selfcare = "Selfcare";
        private const string SelfcareKey = "EQ5D.0.EQ5D2";

        private const string UsualActivites = "UsualActivites";
        private const string UsualActivitesKey = "EQ5D.0.EQ5D3";

        private const string Vas = "VAS";
        private const string VasKey = "EQ5D.0.EQ5D6";

        public static SurveyResponseData ResponseData()
        {
            SurveyResponseData sData = new SurveyResponseData();
            if (HttpContext.Current.Request.Params.AllKeys.Contains(AnxietyDepression))
                sData.Answers.Add(AnxietyDepressionKey, HttpContext.Current.Request.Params[AnxietyDepression]);
            else
                throw new NetProException("EQ5 AnxietyDepression data is missing");

            if (HttpContext.Current.Request.Params.AllKeys.Contains(Mobility))
                sData.Answers.Add(MobilityKey, HttpContext.Current.Request.Params[Mobility]);
            else
                throw new NetProException("EQ5 Mobility data is missing");

            if (HttpContext.Current.Request.Params.AllKeys.Contains(Paindiscomfort))
                sData.Answers.Add(PaindiscomfortKey, HttpContext.Current.Request.Params[Paindiscomfort]);
            else
                throw new NetProException("EQ5 Paindiscomfort data is missing");

            if (HttpContext.Current.Request.Params.AllKeys.Contains(Selfcare))
                sData.Answers.Add(SelfcareKey, HttpContext.Current.Request.Params[Selfcare]);
            else
                throw new NetProException("EQ5 Selfcare data is missing");

            if (HttpContext.Current.Request.Params.AllKeys.Contains(UsualActivites))
                sData.Answers.Add(UsualActivitesKey, HttpContext.Current.Request.Params[UsualActivites]);
            else
                throw new NetProException("EQ5 UsualActivites data is missing");

            if (HttpContext.Current.Request.Params.AllKeys.Contains(Vas))
                sData.Answers.Add(VasKey, HttpContext.Current.Request.Params[Vas]);
            else
                throw new NetProException("EQ5 Vas data is missing");

            return sData;
        }
    }
}