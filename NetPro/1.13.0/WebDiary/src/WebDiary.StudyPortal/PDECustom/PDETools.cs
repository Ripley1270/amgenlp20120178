﻿using System;
using System.Xml.Linq;
namespace WebDiary.StudyPortal{
    public class PDETools : PDECommonTools{
        //Diary KRSU's
        public const String KRSU_ASSIGN = "Assignment";
        public const String KRSU_Deactivate = "EndWebProUse";
        public const String KRSU_VISIT_CONF = "VisitConfirmation";
        public const String MedUpdateKRSU = "SubjectMedicationSelectionList";
        public const String MedSelectionKRSU = "OtherMedResolve";
        public const String OngoingHeadacheResolutionKRSU = "OngoingHeadacheResolution";

        //Constants
        public const String EMPTY_STRING = " ";
        public const String STD_YES = "1";
        public const String STD_NO = "0";
        public const String STD_NO_VALID_DATA = "-1";
        public const String DEFAULT_PROTOCOL = "1";

        //Phases
        public const String PHASE_SCREENING = "100";
        public const String PHASE_TREATMENT = "200";
        public const String PHASE_FOLLOWUP1 = "300";
        public const String PHASE_FOLLOWUP2 = "301";
        public const String PHASE_PRETERM = "998";
        public const String PHASE_TERMINATION = "999";
        public const String PHASE_DORMANT = "30";

        //Visits
        public const String VISIT_RESCREENING = "1";
        public const String VISIT_TREATMENT = "2";
        public const String VISIT_TREATMENT_DISC = "3";
        public const String VISIT_TREATMENT_DISC_AND_PROG = "4";
        public const String VISIT_PROG = "5";
        public const String VISIT_DISC = "6";

        //Languages
        public const String LANG_ENGLISH = "en_US";
        public const String SW_DATE_FORMAT = "dd MMM yyyy";

        //If study uses PDE_NetPro_LastDiaryTime.sql, pulls last diary time out of result
        public static DateTime? getLastBPIDT(String lastDiaryDataXML){
            XElement xmlDoc = XElement.Parse(lastDiaryDataXML);
            DateTime? retVal = null;
            try{
                String attr = (String)xmlDoc.Attribute("lastBPIReportDT");
                if(attr != null){
                    retVal = DateTime.Parse(attr);
                }
            } catch(Exception e){
                // ignored
            }
            return retVal;
        }
        public static String GetStudyArm(String dataXML){
            XElement xmlDoc = XElement.Parse(dataXML);
            String studyArm = "";
            try{
                studyArm = (String)xmlDoc.Attribute("studyArm");
            } catch(Exception e){
                // ignored
            }
            return studyArm;
        }
        public static String GetRescreeningCount(String dataXML){
            XElement xmlDoc = XElement.Parse(dataXML);
            String studyArm;
            try{
                studyArm = (String)xmlDoc.Attribute("rescreeningCount");
            } catch(Exception e){
                studyArm = "0";
            }
            return studyArm;
        }
        public static DateTime? getRandomizationDT(String lastDiaryDataXML){
            XElement xmlDoc = XElement.Parse(lastDiaryDataXML);
            DateTime? retVal = null;
            try{
                String attr = (String)xmlDoc.Attribute("randomizationDT");
                if(attr != null){
                    retVal = DateTime.Parse(attr);
                }
            } catch(Exception e){
                // ignored
            }
            return retVal;
        }
    }
}