﻿using System;
using System.Web.SessionState;
using Newtonsoft.Json.Linq;
using WebDiary.SWAPI.SWData;
namespace WebDiary.StudyPortal.PDECustom{
    public class StudyUtils{
        public static void addCustomJavascript(HttpSessionState session, String js){
            session["customJS"] = js;
        }
        public static String getCustomJavascript(HttpSessionState session){
            String js = (String)session["customJS"];
            String result = null;
            if(js != null){
                result = "var jsonScript = document.createElement('script'); jsonScript.setAttribute('type','text/javascript'); jsonScript.text = '" + js + ";';document.getElementsByTagName(\"head\")[0].appendChild(jsonScript);";
            }
            return result;
        }
        public static void saveJSONToClinRecord(PDEClinRecord clinRecord, JArray jArray, String IG){
            int IGR = 1;
            foreach(JObject jObj in jArray){
                foreach(JProperty jPro in jObj.Children()){
                    clinRecord.addIT(jPro.Name, IG, (String)jPro.Value, IGR.ToString());
                }
                IGR++;
            }
        }
        public static string getReportStartDate(PDEClinRecord clinRecord){
            ClinField clinReportstartDate = clinRecord.clinRecord.sysvars.Find(item => item.sysvar == "SU.ReportStartDate");
            String reportStartDate = clinReportstartDate.value;
            return reportStartDate;
        }
    }
}