﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;

using WebDiary.Core.Helpers;
using WebDiary.SWAPI;
using WebDiary.Core.Entities;
using WebDiary.Core.Repositories;

using log4net;
using System.Globalization;
using WebDiary.Core.Constants;

namespace WebDiary.StudyPortal
{
    public partial class SWAccess : Page
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(SWAccess));
        private const string SITE_GATEWAY = "SITE_GATEWAY";

        //Constants.
        private const string IS_GATEWAY = "1";
        private const string SW_SESSION_CHECK_FORMAT = "{0}/ssa/remote/CheckKey?key={1}";
        private const string SW_RETURN_URL_FORMAT = "{0}{1}{2}key={3}";
        
        protected void Page_Load(object sender, EventArgs e)
        {
            Session[WDConstants.VarSessionKey] = HttpHelper.GetPostParam(WDConstants.VarSessionKey, String.Empty);
            Session[WDConstants.VarKrpt] = HttpHelper.GetPostParam(WDConstants.VarKrpt, String.Empty);
            Session[WDConstants.VarKrDom] = HttpHelper.GetPostParam(WDConstants.VarKrDom, String.Empty);
            Session[WDConstants.VarSiteCode] = HttpHelper.GetPostParam(WDConstants.VarSiteCode, String.Empty);
            Session[WDConstants.VarProtocol] = HttpHelper.GetPostParam(WDConstants.VarProtocol, String.Empty);
            Session[WDConstants.VarTimeZone] = HttpHelper.GetPostParam(WDConstants.VarTimeZone, String.Empty);
            Session[WDConstants.VarSwUrl] = HttpHelper.GetPostParam(WDConstants.VarSwUrl, String.Empty);
            Session[WDConstants.VarSiteGateway] = HttpHelper.GetPostParam(WDConstants.VarSiteGateway, String.Empty);
            Session[WDConstants.VarUserId] = HttpHelper.GetPostParam(WDConstants.VarUserId, String.Empty);
            Session[WDConstants.VarPrintedName] = HttpHelper.GetPostParam(WDConstants.VarFullName, String.Empty);

            // Set default site language if it's not set
            string defaultLanguage = "en-US";
            string site_language = SessionHelper.GetSiteLanguageKey();
            if (string.IsNullOrEmpty(Session[site_language] as string))
            {
                if (Request.Cookies[site_language] != null && !string.IsNullOrEmpty(Request.Cookies[site_language].Value))
                {
                    Session[site_language] = Request.Cookies[site_language].Value;
                }
                else
                {
                    Session[site_language] = defaultLanguage;
                }
            }

            string redirectUrl = null;

            if (HttpContext.Current.Request.HttpMethod.Equals("HEAD"))
            {
                return;
            }

            try
            {
                using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                {
                    Study study = StudyData.study;

                    if (study != null)
                    {
                        int studyId = study.Id;                        

                        //Setup session check url.
                        Session[WDConstants.VarRequestUrl] = String.Format(CultureInfo.InvariantCulture, SW_SESSION_CHECK_FORMAT, study.SsaBaseUrl.Trim(),
                            Session[WDConstants.VarSessionKey].ToString());

                        //Setup StudyWorks return url.
                        string urlConnect = "?";
                        if (Session[WDConstants.VarSwUrl].ToString().Contains("?"))
                            urlConnect = "&";
                        Session[WDConstants.VarSwReturnUrl] = String.Format(CultureInfo.InvariantCulture, SW_RETURN_URL_FORMAT, study.StudyWorksBaseUrl,
                            Session[WDConstants.VarSwUrl].ToString(),
                            urlConnect, 
                            Session[WDConstants.VarSessionKey].ToString());

                        //Get study and WebDiary versions.
                        Core.Helpers.Version ver = new Core.Helpers.Version(db, studyId);
                        Session[WDConstants.VarSuWebDiaryVersion] = ver.ProductVersionFull;
                        Session[WDConstants.VarSuStudyVersion] = ver.StudyVersionFull;


                        if (!Session[WDConstants.VarSiteGateway].ToString().Equals(IS_GATEWAY))
                        {
                            //Copy over sw return url.
                            Session[WDConstants.VarReturnUrl] = Session[WDConstants.VarSwReturnUrl];

                            SurveyRepository surveyRep = new SurveyRepository(db);

                            var addSubject = surveyRep.FindByStudyIdAndFormType(studyId, SWAPIConstants.FormTypeSubjectAssignment);
                            if (addSubject != null && addSubject.Count() == 1)
                            {
                                StudySurveys studySurvey = addSubject.First();

                                redirectUrl = studySurvey.ActivationURL.Trim();

                                //Build Assignment url.
                                SurveyData surveyData = SurveyDataHelper.CreateSurveyData(studySurvey.Id, studySurvey.TriggeredPhase);
                                surveyData.SiteCode = Session[WDConstants.VarSiteCode].ToString();
                                surveyData.PtEnrollDate = DateTime.UtcNow;
                                NameValueCollection queryStrings = SurveyDataHelper.SaveSurveyData(surveyData, db);

                                redirectUrl += "&" + queryStrings;
                                HttpContext.Current.Session[WDConstants.FramePostUrl] = null;
                                HttpContext.Current.Session[WDConstants.FrameLeaveUrl] = null;
                                HttpContext.Current.Session[WDConstants.FrameRedirectUrl] = redirectUrl;
                                redirectUrl = "survey.aspx?psd_id=" + surveyData.Id;
                            }
                            else
                            {
                                logger.Error("Assignment questionnaire not found for this study: " + Session[WDConstants.VarProtocol]);
                                //Redirect to error page. Error page must take sw return page.
                                return;
                            }
                        }
                        else
                        {
                            //Copy over the current url.
                            string returnUrl = StudyData.study.WebDiaryStudyBaseUrl.TrimEnd('/') + "/sw-access.aspx";
                            if (Request.QueryString.Count > 0)
                                returnUrl += "?" + Request.QueryString;

                            Session[WDConstants.VarReturnUrl] = returnUrl;
 
                            if (String.IsNullOrEmpty(HttpContext.Current.Request.Form.ToString()) == false)
                            {
                                Session[WDConstants.VarReturnUrl] += "?";
                                Session[WDConstants.VarReturnUrl] += HttpContext.Current.Request.Form.ToString();
                            }
                            Session[WDConstants.VarReturnUrl] = Session[WDConstants.VarReturnUrl];

                            string siteGateway = System.Configuration.ConfigurationManager.AppSettings[SITE_GATEWAY];
                            if (!siteGateway.StartsWith("/", StringComparison.Ordinal)) siteGateway = "~/" + siteGateway;

                            //redirectUrl = siteGateway + "?" + SWAPIConstants.VAR_SW_RETURN_URL + 
                            //    "=" + Server.UrlEncode(Session[SWAPIConstants.VAR_SW_RETURN_URL] as string);

                            //***** TEMPORARY FIX UNTIL CHANGES ARE MADE IN StudyWorks. *****
                            redirectUrl = siteGateway + "?" + WDConstants.VarSwReturnUrl +
                                "=" + Server.UrlEncode(study.SsaBaseUrl + SWAPIConstants.SessionTimeoutPage);
                        }
                    }
                    else
                    {
                        logger.Error("Study not found: " + Session[WDConstants.VarProtocol]);
                        //redirect to error page. Error page must take sw return page.
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                string err = String.Format(CultureInfo.InvariantCulture, Resources.Resource.ErrUnexpectedErr + ":{0}", ex.Message);
                logger.Error(err, ex);
                Session[WDError.ErrorMessageKey] = err;
                Response.Redirect(WDError.Path, true);
            }

            //Redirect to appropriate page.
            Response.Redirect(redirectUrl);
        }
    }
}
