﻿<%@ Page Language="C#" AutoEventWireup="true"  validateRequest="false"  CodeBehind="login.aspx.cs" Inherits="WebDiary.StudyPortal.LogOn" MasterPageFile="~/includes/masters/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        if (top.location.href.indexOf('survey.aspx') > 0)
            top.location.href = top.location.href.replace('survey.aspx', '');

        autoLock = false;
        $(document).ready(function () {
            $('body > form').submit(function () {
                $('#vsErrors').hide();
            });
        });
    </script>
</asp:Content>
<asp:Content ContentPlaceHolderID="mainContent" runat="server">

<pht:loginControl runat="server" ID="LogOnControl" />

</asp:Content>
