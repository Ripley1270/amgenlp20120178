param([string]$dbName, [string]$templateFolder, [string]$outputFolder)


#'testStudyX'
#'C:\accurevWork\netpro_pde_template\NetPro\1.11.0\WebDiary\data\survey_templates'
#'C:\accurevWork\output\NetPro\1.11.0\WebDiary\src\WebDiary.StudyPortal\studyconfig'

## new survey name - checkbox removes underscores on export, following same naming convention for file
$surveyStudyName = $dbName
$surveyStudyName = $surveyStudyName.replace('_','')

## Template surveys
$assignmentTemplate = $templateFolder + '\SubjectAssignment_V2.xml'
$editSubjectTemplate = $templateFolder + '\EditSubject.xml'
$offstudyTemplate = $templateFolder + '\OffStudy.xml'

## Output survey names
$assignmentOutput = $outputFolder + '\' + $surveyStudyName + 'SubjectAssignmentV1.0.0.xml'
$editSubjectOutput = $outputFolder + '\' + $surveyStudyName + 'EditSubjectV1.0.0.xml'
$offstudyOutput = $outputFolder + '\' + $surveyStudyName + 'OffStudyV1.0.0.xml'

## Survey Filenames for study.xml 
$assignmentOutputFileName = (Split-Path -Path $assignmentOutput -Leaf)
$editSubjectOutputFileName = (Split-Path -Path $editSubjectOutput -Leaf)
$offstudyOutputFileName = (Split-Path -Path $offstudyOutput -Leaf)

## Checkbox Name
$assignmentCheckboxDisplayName = $dbName + '_SubjectAssignment_V1.0.0'
$editSubjectCheckboxDisplayName = $dbName + '_EditSubject_V1.0.0'
$offstudyCheckboxDisplayName = $dbName + '_OffStudy_V1.0.0'

## Study xml location
$studyXML = $outputFolder + '\study.xml'

## Update survey xmls ##
$ScriptPath = Split-Path $MyInvocation.MyCommand.Path

## Assignment ##
$cmd = "$ScriptPath\updateSurveyXml.ps1"
$args = @()
$args += ("-baseFilePath", "$assignmentTemplate")
$args += ("-newFilePath", "$assignmentOutput")
$args += ("-newCheckboxName", "$assignmentCheckboxDisplayName")

echo 'generating assignment form'
Invoke-Expression "& '$cmd' $args"

## Edit Subject ##
$cmd = "$ScriptPath\updateSurveyXml.ps1"
$args = @()
$args += ("-baseFilePath", "$editSubjectTemplate")
$args += ("-newFilePath", "$editSubjectOutput")
$args += ("-newCheckboxName", "$editSubjectCheckboxDisplayName")

echo 'generating edit subject form'
Invoke-Expression "& '$cmd' $args"

## OffStudy ##
$cmd = "$ScriptPath\updateSurveyXml.ps1"
$args = @()
$args += ("-baseFilePath", "$offstudyTemplate")
$args += ("-newFilePath", "$offstudyOutput")
$args += ("-newCheckboxName", "$offstudyCheckboxDisplayName")

echo 'generating offstudy form'
Invoke-Expression "& '$cmd' $args"


## Update Study.xml ##
$cmd = "$ScriptPath\updateStudyXml.ps1"
$args = @()
$args += ("-baseFilePath", "$studyXML")
$args += ("-databaseName", "$dbName")
$args += ("-assignmentFileName", "$assignmentOutputFileName")
$args += ("-editSubjectFileName", "$editSubjectOutputFileName")
$args += ("-offStudyFileName", "$offstudyOutputFileName")

echo 'updating study.xml'
Invoke-Expression "& '$cmd' $args"