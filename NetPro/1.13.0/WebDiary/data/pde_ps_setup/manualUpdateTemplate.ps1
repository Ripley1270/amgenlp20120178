param([string]$dbName)

[void][Reflection.Assembly]::LoadWithPartialName('Microsoft.VisualBasic')

$title = 'Database'
$msg   = 'Enter your Staging Database name:'

$dbNameInput = [Microsoft.VisualBasic.Interaction]::InputBox($msg, $title)
echo $dbNameInput
#'testStudyX'
#'C:\accurevWork\netpro_pde_template\NetPro\1.11.0\WebDiary\data\survey_templates'
#'C:\accurevWork\output\NetPro\1.11.0\WebDiary\src\WebDiary.StudyPortal\studyconfig'

## Update survey xmls ##
$ScriptPath = Split-Path $MyInvocation.MyCommand.Path

$ScriptPathTrimed = $ScriptPath.TrimEnd("data\pde_ps_setup") 

$templateSurveyPath = $ScriptPathTrimed + "\data\pde_survey_templates"
echo $templateSurveyPath
$outputPath = $ScriptPathTrimed + "\src\WebDiary.StudyPortal\studyconfig"
echo $outputPath

## Assignment ##
$cmd = "$ScriptPath\updateTemplate.ps1"
$args = @()
$args += ("-dbName", "$dbNameInput")
$args += ("-templateFolder", "$templateSurveyPath")
$args += ("-outputFolder", "$outputPath")

echo 'calling updateTemplate.ps1'
Invoke-Expression "& '$cmd' $args"

echo 'done'


#Z:\pde49\Base_NP_Template_1_11_0\WebDiary\data\pde_ps_setup
#Z:\pde49\Base_NP_Template_1_11_0\WebDiary\src\WebDiary.StudyPortal\studyconfig