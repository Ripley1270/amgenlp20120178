param([string]$baseFilePath, [string]$newFilePath, [string]$newCheckboxName)

##Args:
##baseFilePath - file path for the template survey   ex: 'C:\Users\jpresto\SubjectAssignmentcore1.10.0Copy3t.xml'
##pathToSave - file path to save new file and with what name ex: 'C:\Users\jpresto\SubjectAssignmentcore1.10.0Copy3tHelloWorld.xml'
##name - new name for survey display in checkbox     ex: clientProtocol_AssignmentSurveyV1.0.0
$xml = [xml](Get-Content $baseFilePath -Encoding UTF8)

$node = $xml.ResponseTemplate.ConfigurationData.TemplateData

$node.TemplateName = $newCheckboxName

$xml.save($newFilePath)