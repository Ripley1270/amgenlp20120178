/*****************************************************************************
Stored procedure: synd_GetSubjectsByKrDom 
Description: Get subject related data by KrDom 
Project: Syndication
History:
03Sep2015 Initial creation
22Oct2015 Optimized sql and retrieve additional fields
*****************************************************************************/

IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'synd_GetSubjectsByKrDom' AND TYPE = 'P')
   DROP PROCEDURE [dbo].[synd_GetSubjectsByKrDom]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[synd_GetSubjectsByKrDom]

@KrDom VARCHAR(255) 

AS

BEGIN

	SET NOCOUNT ON;

	DECLARE @Xml XML;
	DECLARE @XmlRow INT;
	DECLARE @String NVARCHAR(MAX);

	SET @String = ''; 

	CREATE TABLE #ServerTimeStamp
	(
		krpt VARCHAR(36) NOT NULL,
		receiptts DATETIME NULL 
	)

	--Insert server timestamp from lookup_pt into temporary table
	INSERT INTO #ServerTimeStamp(krpt, receiptts)
	SELECT krpt, receiptts
	FROM lookup_pt WITH (NOLOCK)
	WHERE krdom =  @Krdom;

	--Get latest server time stamp from opslog table whose su is assignment and activation  
	--Insert the server timestamp into temporary table
	--When some data such as language is updated, the time stamp in lookup_pt table is not updated and time stamp in opslog table is updated.
	--Thus get latest time stamp from opslog table to cover these cases	
	INSERT INTO #ServerTimeStamp(krpt, receiptts)
	SELECT pt.krpt, MAX(su.receiptts) AS receiptts
	FROM lookup_su su WITH (NOLOCK) 
	INNER JOIN lookup_pt pt WITH (NOLOCK) ON su.krpt = pt.krpt
	WHERE pt.krdom = @KrDom
	AND su.krsu IN ('assignment', 'activation')
	GROUP BY pt.krpt;	

	SET @Xml = (SELECT ISNULL(pt.patientid, '') AS "@subjectId",
						pt.krpt AS "@krpt",
						ISNULL(pt.initials, '') AS "@initials",
						REPLACE(ISNULL(am.Language, ''), '_', '-') AS "@language", 
						ISNULL(pt.krphase, '') AS "@phase",
						ISNULL(pt.phasedate, '') AS "@phaseDate",
						ISNULL(pt.deleted, '') AS "@deleted",
						ISNULL(pt.dob, '') AS "@dob",
						ISNULL(pt.custom1, '') AS "@custom1",
						ISNULL(pt.custom2, '') AS "@custom2",
						ISNULL(pt.custom3, '') AS "@custom3",
						ISNULL(pt.custom4, '') AS "@custom4",
						ISNULL(pt.custom5, '') AS "@custom5",
						ISNULL(pt.custom6, '') AS "@custom6",
						ISNULL(pt.custom7, '') AS "@custom7",
						ISNULL(pt.custom8, '') AS "@custom8",
						ISNULL(pt.custom9, '') AS "@custom9",
						ISNULL(pt.custom10, '') AS "@custom10",
						ISNULL(pt.LPStartDate, '') AS "@LPStartDate",
						ISNULL(pt.LPEndDate, '') AS "@LPEndDate",
						ISNULL(pt.SPStartDate, '') AS "@SPStartDate",
						ISNULL(pt.SPEndDate, '') AS "@SPEndDate",
						ISNULL(pt.enrolldate, '') AS "@enrollDate",
						ISNULL(pt.PhaseTZOffset, '') AS "@phaseTZOffset",
						ISNULL(pt.orig_krtz, '') AS "@origKrtz",						
						ISNULL(lst.updated, '') AS "@updated" 
				FROM lookup_pt pt WITH (NOLOCK) LEFT JOIN ig_Assignment am WITH (NOLOCK)
				ON pt.krpt = am.KRPT INNER JOIN (SELECT krpt, MAX(receiptts) AS updated
				FROM #ServerTimeStamp 
				GROUP BY krpt) lst
				ON pt.krpt = lst.krpt  
				WHERE pt.krdom = @KrDom
				ORDER BY pt.patientid FOR xml PATH('subject'), ROOT('subjects'));

	SELECT @XmlRow = COUNT(*) FROM @XML.nodes('/subjects/*') x(a);

	--SWAPI GetDataClin expects string data type 
	--Convert xml data type to string data type explicitly. Implicit conversion is not allowed
	IF @XmlRow > 0
		SET @String = CONVERT(NVARCHAR(MAX),@Xml);

	SELECT @String;
	
	DECLARE @ProcedureName varchar(50);
	SET @ProcedureName = OBJECT_NAME(@@PROCID);
	
	--Get version
	DECLARE @Version VARCHAR(10);
	SELECT @Version = procVersion
	FROM PDE_SQLVersion WITH (NOLOCK)
	WHERE procname = @ProcedureName;
	
	--Log data 
	EXEC [dbo].[insertStudySyncLogs] @ProcedureName, @Version, @KrDom, NULL, NULL, NULL, @String;
	
	DROP TABLE #ServerTimeStamp;
END
GO


SET ANSI_NULLS ON 
GO
SET QUOTED_IDENTIFIER OFF 
GO


IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'synd_GetSubjectsByKrDom' AND TYPE = 'P')
BEGIN
	DECLARE @name as varchar(50)
	DECLARE @version as numeric(18,2)

	SELECT @name = 'synd_GetSubjectsByKrDom', @version = 1.00
	EXEC [dbo].[PDE_UpdateSQLVersion] @name, @version
END
GO


GRANT EXECUTE ON [dbo].[synd_GetSubjectsByKrDom] TO pht_server_read_only 
GO


IF NOT EXISTS (SELECT * FROM allowed_clin_procs WHERE proc_name='synd_GetSubjectsByKrDom')
	INSERT INTO allowed_clin_procs (proc_name) values ('synd_GetSubjectsByKrDom');
GO
