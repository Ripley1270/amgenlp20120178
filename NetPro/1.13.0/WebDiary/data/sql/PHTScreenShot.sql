USE [Checkbox]
GO

--******************************************
--Create PHTScreenShotRTGetResponseTemplate
--******************************************
IF (EXISTS (SELECT name FROM sysobjects WHERE (name = N'PHTScreenShotRTGetResponseTemplate') AND (type = 'P')))
    DROP PROCEDURE [dbo].PHTScreenShotRTGetResponseTemplate
GO

CREATE PROCEDURE [dbo].[PHTScreenShotRTGetResponseTemplate]
(
	@ResponseTemplateID int
)
AS
	--Template Data
	SELECT
		t.TemplateID,
		t.ModifiedDate,
		t.Deleted,
		t.DefaultPolicy,
		t.AclID,
		t.CreatedDate,
		rt.*
	FROM
		ckbx_ResponseTemplate rt
		INNER JOIN ckbx_Template t on t.TemplateID = rt.ResponseTemplateID
	WHERE
		rt.ResponseTemplateID = @ResponseTemplateID
	
	--Item Data
	SELECT
		ckbx_Item.ItemID,
		ckbx_Item.ItemTypeID,
		ckbx_Item.Alias,
		ckbx_Item.CreatedDate,
		ckbx_Item.ModifiedDate,
		ckbx_Item.Deleted,
		ckbx_Item.IsActive,
		ckbx_Template_Items.TemplateID,
		ckbx_ItemType.ItemDataClassName,
		ckbx_ItemType.ItemDataAssemblyName,
		ckbx_ItemType.DefaultAppearanceCode,
		ckbx_ItemType.ItemName,
		ckbx_ItemType.MobileCompatible
	FROM
		ckbx_Item
		INNER JOIN ckbx_Template_Items ON ckbx_Item.ItemID = ckbx_Template_Items.ItemID
		INNER JOIN ckbx_ItemType on ckbx_ItemType.ItemTypeID = ckbx_Item.ItemTypeID
	WHERE
		ckbx_Template_Items.TemplateID = @ResponseTemplateID
		AND (ckbx_Item.Deleted IS NULL OR ckbx_Item.Deleted = 0)

	--Pages
	SELECT TemplateID, PageID, PagePosition, LayoutTemplateID FROM ckbx_Template_Pages WHERE TemplateID = @ResponseTemplateID ORDER BY PagePosition
	
	--Item Options
	SELECT
		opt.OptionID, opt.ItemID, opt.TextID, opt.Alias, opt.IsDefault, opt.[Position], opt.IsOther, opt.Points, opt.Deleted, opt.ListID
	FROM
		ckbx_ItemOptions opt
		INNER JOIN ckbx_Template_Items ti ON ti.ItemID = opt.ItemID
	WHERE
		ti.TemplateID = @ResponseTemplateID
		AND (opt.Deleted IS NULL OR (opt.Deleted IS NOT NULL AND opt.Deleted = 0))
	
	--Item ordering
	SELECT
		tp.PageID, tpi.ItemID, tpi.[Position]
	FROM
		ckbx_Template_Pages tp
		INNER JOIN ckbx_TemplatePage_Items tpi ON tpi.PageID = tp.PageID
		INNER JOIN ckbx_Template_Items ti ON ti.ItemID = tpi.ItemID and ti.TemplateID = tp.TemplateID
		INNER JOIN ckbx_Item i ON i.ItemID = ti.ItemID
		INNER JOIN ckbx_ItemType it ON it.ItemTypeID = i.ItemTypeID
	WHERE
		tp.TemplateID = @ResponseTemplateID
	ORDER BY
		tp.PagePosition, tpi.Position

	--New Item/Rule mapping for screen shot
	DECLARE @FakeResponseTemplateID int
	SET @FakeResponseTemplateID = 0  		
	SELECT
		ir.ItemID, ir.RuleID
	FROM
		ckbx_ItemRules ir
		LEFT OUTER JOIN ckbx_Template_Items ti ON ti.ItemID = ir.ItemID
		LEFT OUTER JOIN ckbx_ItemData_MatrixItems mi ON mi.ItemID = ir.ItemID
		LEFT OUTER JOIN ckbx_Template_Items ti2 ON ti2.ItemID = mi.MatrixID
	WHERE
		(ti.TemplateID = @FakeResponseTemplateID OR ti2.TemplateID = @FakeResponseTemplateID)
		AND ((ti.ItemID IS NOT NULL) OR (mi.ItemID IS NOT NULL AND ti2.ItemID IS NOT NULL))

	--New Page/rule mapping
	--For PHT ScreenShot, this statement must not return any row
	-- Set @ResponseTemplateID = 1
	-- In ckbx_Template table TemplateId is greater that 1000 by design
	--DECLARE @FakeResponseTemplateID int
	--SET @FakeResponseTemplateID = 1 
	SELECT
		tpr.PageID, tpr.RuleID, tpr.EventTrigger
	FROM
		ckbx_TemplatePage_Rules tpr
		INNER JOIN ckbx_Template_Pages tp ON tp.PageID = tpr.PageID
	WHERE
		tp.TemplateID = @FakeResponseTemplateID
	ORDER BY
		tpr.PageId ASC,
		tpr.RuleId ASC
		
	
	--Logic Temp Tables
	IF object_id('tempdb..#Rules') IS NOT NULL
		BEGIN
			DROP Table #Rules
		END
	
	IF object_id('tempdb..#Expressions') IS NOT NULL
		BEGIN
			DROP Table #Expressions
		END
	
	IF object_id('tempdb..#Operands') IS NOT NULL
		BEGIN
			DROP Table #Operands
		END
	
	--New Rule for screen shot	
	SELECT 
		RuleID, ExpressionID
	INTO 
		#Rules
	FROM 
		ckbx_Rule 
	WHERE 
		RuleID IN
			(SELECT RuleID FROM ckbx_ItemRules WHERE ItemID IN
			(SELECT ItemID FROM ckbx_Template_Items WHERE TemplateID = @FakeResponseTemplateID)
		OR ItemID IN
			(SELECT ItemID FROM ckbx_ItemData_MatrixItems WHERE MatrixID IN
			(SELECT ItemID FROM ckbx_Template_Items WHERE TemplateID = @FakeResponseTemplateID)))
		OR RuleID IN 
			(SELECT RuleID FROM ckbx_TemplatePage_Rules WHERE PageID IN (
			 SELECT PageID FROM ckbx_Template_Pages WHERE TemplateID = @FakeResponseTemplateID))
	
	--Expressions
	SELECT 
		*
	INTO 
		#Expressions
	FROM 
		ckbx_Expression 
	WHERE 
		ExpressionID IN 
			(SELECT ExpressionID FROM #Rules)
		OR ExpressionID IN 
			(SELECT ExpressionID FROM ckbx_Expression WHERE Root IN (
			SELECT ExpressionID FROM #Rules))
			
	--Clear up expressions with invalid parents, which occasionally occurs on import/export
	DELETE FROM #Expressions WHERE Parent IS NOT NULL AND Parent NOT IN(SELECT ExpressionID FROM #Expressions)

	--Operands
	SELECT 
		* 
	INTO 
		#Operands 
	FROM 
		ckbx_Operand 
	WHERE 
		OperandID IN 
			(SELECT LeftOperand FROM #Expressions WHERE LeftOperand IS NOT NULL)
		OR OperandID IN 
			(SELECT RightOperand FROM #Expressions WHERE RightOperand IS NOT NULL )

	--Rule/Expression mapping
	SELECT RuleID, ExpressionID FROM #Rules
	
	--Expresion relations
	SELECT ExpressionID, Operator, LeftOperand, RightOperand, Parent, Depth, Lineage, Root, ChildRelation FROM #Expressions
	
	--Operands
	SELECT OperandID, TypeName, TypeAssembly FROM #Operands
	
	--Item operands
	SELECT
		op.OperandID, op.ItemID, op.ParentItemID
	FROM
		#Operands o
		INNER JOIN ckbx_ItemOperand op ON op.OperandID = o.OperandID

	--Value operands
	SELECT
		vo.OperandID, vo.ItemID, vo.OptionID, vo.AnswerValue
	FROM
		#Operands o
		INNER JOIN ckbx_ValueOperand vo ON vo.OperandID = o.OperandID

	--Profile operands
	SELECT
		po.OperandID, po.ProfileKey
	FROM
		#Operands o
		INNER JOIN ckbx_ProfileOperand po ON po.OperandID = o.OperandID

	--Rule/action mapping
	SELECT
		ra.RuleID, ra.ActionID
	FROM
		#Rules r
		INNER JOIN ckbx_RuleActions ra ON ra.RuleID = r.RuleID
		
	--Action data
	SELECT
		a.ActionID, a.ActionTypeName, a.ActionAssembly
	FROM
		#Rules r
		INNER JOIN ckbx_RuleActions ra ON ra.RuleID = r.RuleID
		INNER JOIN ckbx_Action a ON a.ActionID = ra.ActionID
	
	--Response pipes
	SELECT ResponseTemplateID, PipeName, ItemID FROM ckbx_ResponsePipe WHERE ResponseTemplateID = @ResponseTemplateID
	
	--Response operands
	SELECT
		ro.OperandID, ro.ResponseKey
	FROM
		#Operands o
		INNER JOIN ckbx_ResponseOperand ro ON ro.OperandID = o.OperandID

	--Item/appearance mappings
	SELECT
		ia.ItemID, ia.AppearanceID
	FROM
		ckbx_ItemAppearances ia
		INNER JOIN ckbx_Template_Items ti ON ti.ItemID = ia.ItemID
		INNER JOIN ckbx_Item i ON i.ItemID = ti.ItemID
		INNER JOIN ckbx_ItemType it ON it.ItemTypeID = i.ItemTypeID
	WHERE
		ti.TemplateID = @ResponseTemplateID
		AND (i.Deleted IS NULL OR i.Deleted = 0)
	
	--Appearance data
	SELECT
		a.*
	FROM
		ckbx_ItemAppearance a
		INNER JOIN ckbx_ItemAppearances ia ON ia.AppearanceID = a.AppearanceID
		INNER JOIN ckbx_Template_Items ti ON ti.ItemID = ia.ItemID
		INNER JOIN ckbx_Item i ON i.ItemID = ti.ItemID
		INNER JOIN ckbx_ItemType it ON it.ItemTypeID = i.ItemTypeID
	WHERE
		ti.TemplateID = @ResponseTemplateID
		AND (i.Deleted IS NULL OR i.Deleted = 0)

	--Page actions
	SELECT
		pba.ActionID,
		pba.GoToPageID
	FROM
		#Rules r
		INNER JOIN ckbx_RuleActions ra ON ra.RuleID = r.RuleID
		INNER JOIN ckbx_PageBranchAction pba ON pba.ActionID = ra.ActionID
	
	--Drop temp tables
	DROP TABLE #Rules
	DROP TABLE #Expressions
	DROP TABLE #Operands
	
	--Item lists
	SELECT
		il.ListID, il.ItemID
	FROM
		ckbx_ItemLists il
		INNER JOIN ckbx_Template_Items ti ON ti.ItemID = il.ItemID
	WHERE
		ti.TemplateID = @ResponseTemplateID

	--Profile updater properties
	SELECT ItemID, SourceItemID, ProviderName, PropertyName FROM ckbx_ItemData_PUProps WHERE ItemID IN (Select ti.ItemID FROM ckbx_Template_Items ti WHERE ti.TemplateID = @ResponseTemplateID)

GO


--******************************************
--Create PHTScreenShotItemDataGetMLText
--******************************************
IF (EXISTS (SELECT name FROM sysobjects WHERE (name = N'PHTScreenShotItemDataGetMLText') AND (type = 'P')))
    DROP PROCEDURE [dbo].PHTScreenShotItemDataGetMLText
GO

CREATE PROCEDURE [dbo].[PHTScreenShotItemDataGetMLText]
(
  @ItemID int
)
AS
  SELECT
    ItemID,
    TextID,
    SubTextID,
    0 as IsRequired, --IsRequired,
    DefaultTextID
  FROM
    ckbx_ItemData_MLText
  WHERE
    ItemID = @ItemID

GO

--******************************************
--Create PHTScreenShotItemDataGetRS
--******************************************
IF (EXISTS (SELECT name FROM sysobjects WHERE (name = N'PHTScreenShotItemDataGetRS') AND (type = 'P')))
    DROP PROCEDURE [dbo].PHTScreenShotItemDataGetRS
GO

CREATE PROCEDURE [dbo].[PHTScreenShotItemDataGetRS]
(
  @ItemID int
)
AS
  --Get Item Data
  SELECT 
    ItemID,
    StartValue,
    EndValue,
    TextID,
    SubTextID,
    0 as IsRequired, --IsRequired,
    DisplayNotApplicableText,
    StartTextID,
    MidTextID,
	NotApplicableTextID
FROM 
    ckbx_ItemData_RS 
WHERE 
    ItemID = @ItemID

--Get Item Options Lists
  SELECT
    ItemID,
    ListID
  FROM
    ckbx_ItemLists
  WHERE
    ItemID = @ItemID

  --Get item options data
  SELECT
    OptionID,
    @ItemID as ItemID,
    TextID,
    Alias,
    IsDefault,
    Position,
    IsOther,
    Points,
    Deleted,
--    [io].ListID
    ListID
  FROM
    ckbx_ItemOptions [io]
    --INNER join ckbx_ItemLists il ON il.ListID = [io].ListID
  WHERE
    --il.ItemID = @ItemID
    [io].ItemID = @ItemID
    AND ([io].Deleted IS NULL OR [io].Deleted = 0)

GO

--******************************************
--Create PHTScreenShotItemDataGetSelect1
--******************************************
IF (EXISTS (SELECT name FROM sysobjects WHERE (name = N'PHTScreenShotItemDataGetSelect1') AND (type = 'P')))
    DROP PROCEDURE [dbo].PHTScreenShotItemDataGetSelect1
GO

CREATE PROCEDURE [dbo].[PHTScreenShotItemDataGetSelect1]
(
  @ItemID int
)
AS
  --Get Base Select1 Data
  SELECT
    ItemID,
    TextID,
    SubTextID,
    0 as IsRequired, --IsRequired
    AllowOther,
    OtherTextID,
    Randomize
  FROM
    ckbx_ItemData_Select1
  WHERE
    ItemID = @ItemID

  --Get Item Options Lists
  SELECT
    ItemID,
    ListID
  FROM
    ckbx_ItemLists
  WHERE
    ItemID = @ItemID

  --Get item options data
  SELECT
    OptionID,
    @ItemID as ItemID,
    TextID,
    Alias,
    IsDefault,
    Position,
    IsOther,
    Points,
    Deleted,
--    [io].ListID
    ListID
  FROM
    ckbx_ItemOptions [io]
    --INNER join ckbx_ItemLists il ON il.ListID = [io].ListID
  WHERE
    --il.ItemID = @ItemID
    [io].ItemID = @ItemID
    AND ([io].Deleted IS NULL OR [io].Deleted = 0)

GO    


--******************************************
--Create PHTScreenShotItemDataGetSelectMany
--******************************************
IF (EXISTS (SELECT name FROM sysobjects WHERE (name = N'PHTScreenShotItemDataGetSelectMany') AND (type = 'P')))
    DROP PROCEDURE [dbo].PHTScreenShotItemDataGetSelectMany
GO

CREATE PROCEDURE [dbo].[PHTScreenShotItemDataGetSelectMany]
(
  @ItemID int
)
AS
  --Get Base SelectMany Data
  SELECT
    ItemID,
    TextID,
    SubTextID,
    0 as IsRequired, --IsRequired,
    AllowOther,
    OtherTextID,
    Randomize,
    0 as MinToSelect, --MinToSelect,
    MaxToSelect
  FROM
    ckbx_ItemData_SelectMany
  WHERE
    ItemID = @ItemID

   --Get Item Options Lists
  SELECT
    ItemID,
    ListID
  FROM
    ckbx_ItemLists
  WHERE
    ItemID = @ItemID

  --Get item options data
  SELECT
    OptionID,
    @ItemID as ItemID,
    TextID,
    Alias,
    IsDefault,
    Position,
    IsOther,
    Points,
    Deleted,
--    il.ListID
     ListID
  FROM
    ckbx_ItemOptions [io]
--    INNER join ckbx_ItemLists il ON il.ListID = io.ListID
  WHERE
--    il.ItemID = @ItemID
    ItemID = @ItemID
    AND ([io].Deleted IS NULL OR [io].Deleted = 0)

GO


--***************************************
--Create PHTScreenShotItemDataGetSLText
--***************************************
IF (EXISTS (SELECT name FROM sysobjects WHERE (name = N'PHTScreenShotItemDataGetSLText') AND (type = 'P')))
    DROP PROCEDURE [dbo].PHTScreenShotItemDataGetSLText
GO

CREATE PROCEDURE [dbo].[PHTScreenShotItemDataGetSLText]
(
  @ItemID int
)
AS
  SELECT
    ItemID,
    TextID,
    SubTextID,
    0 as IsRequired, --IsRequired,
    DefaultTextID,
    TextFormat,
    MaxLength,
    MaxValue,
    MinValue,
	CustomFormatId 
  FROM
    ckbx_ItemData_SLText
  WHERE
    ItemID = @ItemID

GO    
    
    
    
    

    
    
    
    
    
    



    
    



    
    



    
    
    
    
    	

