/*********************************************************************
  InstallSqlState.SQL                                                
                                                                    
  Installs the tables, and stored procedures necessary for           
  supporting ASP.NET session state.                                  

  Copyright Microsoft, Inc.
  All Rights Reserved.

 *********************************************************************/

USE [master]
GO

/* Drop all tables, startup procedures, stored procedures and types. */

/* Drop the DeleteExpiredSessions_Job */

DECLARE @jobname nvarchar(200)
SET @jobname = N'ASPState' + '_Job_DeleteExpiredSessions' 

-- Delete the [local] job 
DECLARE @jobId binary(16)
SELECT @jobId = job_id FROM msdb.dbo.sysjobs WHERE (name = @jobname)
IF (@jobId IS NOT NULL)
BEGIN
    EXEC msdb.dbo.sp_delete_job @jobId
END
GO

DECLARE @sstype nvarchar(128)
SET @sstype = N'sstype_temp'

IF UPPER(@sstype) = 'SSTYPE_TEMP' AND OBJECT_ID(N'dbo.ASPState_Startup', 'P') IS NOT NULL BEGIN
    DROP PROCEDURE dbo.ASPState_Startup
END    

USE [WebDiary]
GO

IF OBJECT_ID(N'dbo.ASPStateTempSessions','U') IS NOT NULL BEGIN
    DROP TABLE dbo.ASPStateTempSessions
END

IF OBJECT_ID(N'dbo.ASPStateTempApplications','U') IS NOT NULL BEGIN
    DROP TABLE dbo.ASPStateTempApplications
END

IF (EXISTS (SELECT name FROM sysobjects WHERE (name = N'GetMajorVersion') AND (type = 'P')))
    DROP PROCEDURE [dbo].GetMajorVersion
GO

IF (EXISTS (SELECT name FROM sysobjects WHERE (name = N'CreateTempTables') AND (type = 'P')))
    DROP PROCEDURE [dbo].CreateTempTables
GO

IF (EXISTS (SELECT name FROM sysobjects WHERE (name = N'TempGetVersion') AND (type = 'P')))
    DROP PROCEDURE [dbo].TempGetVersion
GO

IF (EXISTS (SELECT name FROM sysobjects WHERE (name = N'GetHashCode') AND (type = 'P')))
    DROP PROCEDURE [dbo].GetHashCode
GO

IF (EXISTS (SELECT name FROM sysobjects WHERE (name = N'TempGetAppID') AND (type = 'P')))
    DROP PROCEDURE [dbo].TempGetAppID
GO

IF (EXISTS (SELECT name FROM sysobjects WHERE (name = N'TempGetStateItem') AND (type = 'P')))
    DROP PROCEDURE [dbo].TempGetStateItem
GO

IF (EXISTS (SELECT name FROM sysobjects WHERE (name = N'TempGetStateItem2') AND (type = 'P')))
    DROP PROCEDURE [dbo].TempGetStateItem2
GO

IF (EXISTS (SELECT name FROM sysobjects WHERE (name = N'TempGetStateItem3') AND (type = 'P')))
    DROP PROCEDURE [dbo].TempGetStateItem3
GO

IF (EXISTS (SELECT name FROM sysobjects WHERE (name = N'TempGetStateItemExclusive') AND (type = 'P')))
    DROP PROCEDURE [dbo].TempGetStateItemExclusive
GO

IF (EXISTS (SELECT name FROM sysobjects WHERE (name = N'TempGetStateItemExclusive2') AND (type = 'P')))
    DROP PROCEDURE [dbo].TempGetStateItemExclusive2
GO

IF (EXISTS (SELECT name FROM sysobjects WHERE (name = N'TempGetStateItemExclusive3') AND (type = 'P')))
    DROP PROCEDURE [dbo].TempGetStateItemExclusive3
GO

IF (EXISTS (SELECT name FROM sysobjects WHERE (name = N'TempReleaseStateItemExclusive') AND (type = 'P')))
    DROP PROCEDURE [dbo].TempReleaseStateItemExclusive
GO

IF (EXISTS (SELECT name FROM sysobjects WHERE (name = N'TempInsertUninitializedItem') AND (type = 'P')))
    DROP PROCEDURE [dbo].TempInsertUninitializedItem
GO

IF (EXISTS (SELECT name FROM sysobjects WHERE (name = N'TempInsertStateItemShort') AND (type = 'P')))
    DROP PROCEDURE [dbo].TempInsertStateItemShort
GO

IF (EXISTS (SELECT name FROM sysobjects WHERE (name = N'TempInsertStateItemLong') AND (type = 'P')))
    DROP PROCEDURE [dbo].TempInsertStateItemLong
GO

IF (EXISTS (SELECT name FROM sysobjects WHERE (name = N'TempUpdateStateItemShort') AND (type = 'P')))
    DROP PROCEDURE [dbo].TempUpdateStateItemShort
GO

IF (EXISTS (SELECT name FROM sysobjects WHERE (name = N'TempUpdateStateItemShortNullLong') AND (type = 'P')))
    DROP PROCEDURE [dbo].TempUpdateStateItemShortNullLong
GO

IF (EXISTS (SELECT name FROM sysobjects WHERE (name = N'TempUpdateStateItemLong') AND (type = 'P')))
    DROP PROCEDURE [dbo].TempUpdateStateItemLong
GO

IF (EXISTS (SELECT name FROM sysobjects WHERE (name = N'TempUpdateStateItemLongNullShort') AND (type = 'P')))
    DROP PROCEDURE [dbo].TempUpdateStateItemLongNullShort
GO

IF (EXISTS (SELECT name FROM sysobjects WHERE (name = N'TempRemoveStateItem') AND (type = 'P')))
    DROP PROCEDURE [dbo].TempRemoveStateItem
GO

IF (EXISTS (SELECT name FROM sysobjects WHERE (name = N'TempResetTimeout') AND (type = 'P')))
    DROP PROCEDURE [dbo].TempResetTimeout
GO

IF (EXISTS (SELECT name FROM sysobjects WHERE (name = N'DeleteExpiredSessions') AND (type = 'P')))
    DROP PROCEDURE [dbo].DeleteExpiredSessions
GO

IF EXISTS(SELECT name FROM systypes WHERE name ='tSessionId')
    EXECUTE sp_droptype tSessionId
GO

IF EXISTS(SELECT name FROM systypes WHERE name ='tAppName')
    EXECUTE sp_droptype tAppName
GO

IF EXISTS(SELECT name FROM systypes WHERE name ='tSessionItemShort')
    EXECUTE sp_droptype tSessionItemShort
GO

IF EXISTS(SELECT name FROM systypes WHERE name ='tSessionItemLong')
    EXECUTE sp_droptype tSessionItemLong
GO

IF EXISTS(SELECT name FROM systypes WHERE name ='tTextPtr')
    EXECUTE sp_droptype tTextPtr
GO

/*****************************************************************************/

CREATE PROCEDURE dbo.GetMajorVersion
    @@ver int OUTPUT
AS
BEGIN
	DECLARE @version        nchar(100)
	DECLARE @dot            int
	DECLARE @hyphen         int
	DECLARE @SqlToExec      nchar(4000)

	SELECT @@ver = 7
	SELECT @version = @@Version
	SELECT @hyphen  = CHARINDEX(N' - ', @version)
	IF (NOT(@hyphen IS NULL) AND @hyphen > 0)
	BEGIN
		SELECT @hyphen = @hyphen + 3
		SELECT @dot    = CHARINDEX(N'.', @version, @hyphen)
		IF (NOT(@dot IS NULL) AND @dot > @hyphen)
		BEGIN
			SELECT @version = SUBSTRING(@version, @hyphen, @dot - @hyphen)
			SELECT @@ver     = CONVERT(int, @version)
		END
	END
END
GO   

/*****************************************************************************/

USE [WebDiary]

/* Find out the version */
DECLARE @ver int
EXEC dbo.GetMajorVersion @@ver=@ver OUTPUT

DECLARE @cmd nchar(4000)

IF (@ver >= 8)
    SET @cmd = N'
        CREATE PROCEDURE dbo.CreateTempTables
        AS
            CREATE TABLE [WebDiary].dbo.ASPStateTempSessions (
                SessionId           nvarchar(88)    NOT NULL PRIMARY KEY,
                Created             datetime        NOT NULL DEFAULT GETUTCDATE(),
                Expires             datetime        NOT NULL,
                LockDate            datetime        NOT NULL,
                LockDateLocal       datetime        NOT NULL,
                LockCookie          int             NOT NULL,
                Timeout             int             NOT NULL,
                Locked              bit             NOT NULL,
                SessionItemShort    VARBINARY(7000) NULL,
                SessionItemLong     image           NULL,
                Flags               int             NOT NULL DEFAULT 0,
            ) 

            CREATE NONCLUSTERED INDEX Index_Expires ON [WebDiary].dbo.ASPStateTempSessions(Expires)

            CREATE TABLE [WebDiary].dbo.ASPStateTempApplications (
                AppId               int             NOT NULL PRIMARY KEY,
                AppName             char(280)       NOT NULL,
            ) 

            CREATE NONCLUSTERED INDEX Index_AppName ON [WebDiary].dbo.ASPStateTempApplications(AppName)

            RETURN 0'
ELSE
    SET @cmd = N'
        CREATE PROCEDURE dbo.CreateTempTables
        AS
            CREATE TABLE [WebDiary].dbo.ASPStateTempSessions (
                SessionId           nvarchar(88)    NOT NULL PRIMARY KEY,
                Created             datetime        NOT NULL DEFAULT GETDATE(),
                Expires             datetime        NOT NULL,
                LockDate            datetime        NOT NULL,
                LockCookie          int             NOT NULL,
                Timeout             int             NOT NULL,
                Locked              bit             NOT NULL,
                SessionItemShort    VARBINARY(7000) NULL,
                SessionItemLong     image           NULL,
                Flags               int             NOT NULL DEFAULT 0,
            ) 

            CREATE NONCLUSTERED INDEX Index_Expires ON [WebDiary].dbo.ASPStateTempSessions(Expires)

            CREATE TABLE [WebDiary].dbo.ASPStateTempApplications (
                AppId               int             NOT NULL PRIMARY KEY,
                AppName             char(280)       NOT NULL,
            ) 

            CREATE NONCLUSTERED INDEX Index_AppName ON [WebDiary].dbo.ASPStateTempApplications(AppName)

            RETURN 0'

EXEC (@cmd)
GO   

/*****************************************************************************/

EXECUTE sp_addtype tSessionId, 'nvarchar(88)',  'NOT NULL'
GO

EXECUTE sp_addtype tAppName, 'varchar(280)', 'NOT NULL'
GO

EXECUTE sp_addtype tSessionItemShort, 'varbinary(7000)'
GO

EXECUTE sp_addtype tSessionItemLong, 'image'
GO

EXECUTE sp_addtype tTextPtr, 'varbinary(16)'
GO

/*****************************************************************************/

CREATE PROCEDURE dbo.TempGetVersion
    @ver      char(10) OUTPUT
AS
    SELECT @ver = '2'
    RETURN 0
GO

/*****************************************************************************/

CREATE PROCEDURE dbo.GetHashCode
    @input tAppName,
    @hash int OUTPUT
AS
    /* 
       This sproc is based on this C# hash function:

        int GetHashCode(string s)
        {
            int     hash = 5381;
            int     len = s.Length;

            for (int i = 0; i < len; i++) {
                int     c = Convert.ToInt32(s[i]);
                hash = ((hash << 5) + hash) ^ c;
            }

            return hash;
        }

        However, SQL 7 doesn't provide a 32-bit integer
        type that allows rollover of bits, we have to
        divide our 32bit integer into the upper and lower
        16 bits to do our calculation.
    */
       
    DECLARE @hi_16bit   int
    DECLARE @lo_16bit   int
    DECLARE @hi_t       int
    DECLARE @lo_t       int
    DECLARE @len        int
    DECLARE @i          int
    DECLARE @c          int
    DECLARE @carry      int

    SET @hi_16bit = 0
    SET @lo_16bit = 5381
    
    SET @len = DATALENGTH(@input)
    SET @i = 1
    
    WHILE (@i <= @len)
    BEGIN
        SET @c = ASCII(SUBSTRING(@input, @i, 1))

        /* Formula:                        
           hash = ((hash << 5) + hash) ^ c */

        /* hash << 5 */
        SET @hi_t = @hi_16bit * 32 /* high 16bits << 5 */
        SET @hi_t = @hi_t & 0xFFFF /* zero out overflow */
        
        SET @lo_t = @lo_16bit * 32 /* low 16bits << 5 */
        
        SET @carry = @lo_16bit & 0x1F0000 /* move low 16bits carryover to hi 16bits */
        SET @carry = @carry / 0x10000 /* >> 16 */
        SET @hi_t = @hi_t + @carry
        SET @hi_t = @hi_t & 0xFFFF /* zero out overflow */

        /* + hash */
        SET @lo_16bit = @lo_16bit + @lo_t
        SET @hi_16bit = @hi_16bit + @hi_t + (@lo_16bit / 0x10000)
        /* delay clearing the overflow */

        /* ^c */
        SET @lo_16bit = @lo_16bit ^ @c

        /* Now clear the overflow bits */	
        SET @hi_16bit = @hi_16bit & 0xFFFF
        SET @lo_16bit = @lo_16bit & 0xFFFF

        SET @i = @i + 1
    END

    /* Do a sign extension of the hi-16bit if needed */
    IF (@hi_16bit & 0x8000 <> 0)
        SET @hi_16bit = 0xFFFF0000 | @hi_16bit

    /* Merge hi and lo 16bit back together */
    SET @hi_16bit = @hi_16bit * 0x10000 /* << 16 */
    SET @hash = @hi_16bit | @lo_16bit

    RETURN 0
GO

/*****************************************************************************/

DECLARE @cmd nchar(4000)

SET @cmd = N'
    CREATE PROCEDURE dbo.TempGetAppID
    @appName    tAppName,
    @appId      int OUTPUT
    AS
    SET @appName = LOWER(@appName)
    SET @appId = NULL

    SELECT @appId = AppId
    FROM [WebDiary].dbo.ASPStateTempApplications
    WHERE AppName = @appName

    IF @appId IS NULL BEGIN
        BEGIN TRAN        

        SELECT @appId = AppId
        FROM [WebDiary].dbo.ASPStateTempApplications WITH (TABLOCKX)
        WHERE AppName = @appName
        
        IF @appId IS NULL
        BEGIN
            EXEC GetHashCode @appName, @appId OUTPUT
            
            INSERT [WebDiary].dbo.ASPStateTempApplications
            VALUES
            (@appId, @appName)
            
            IF @@ERROR = 2627 
            BEGIN
                DECLARE @dupApp tAppName
            
                SELECT @dupApp = RTRIM(AppName)
                FROM [WebDiary].dbo.ASPStateTempApplications 
                WHERE AppId = @appId
                
                RAISERROR(''SQL session state fatal error: hash-code collision between applications ''''%s'''' and ''''%s''''. Please rename the 1st application to resolve the problem.'', 
                            18, 1, @appName, @dupApp)
            END
        END

        COMMIT
    END

    RETURN 0'
EXEC(@cmd)    
GO

/*****************************************************************************/

/* Find out the version */

DECLARE @ver int
EXEC dbo.GetMajorVersion @@ver=@ver OUTPUT
DECLARE @cmd nchar(4000)
IF (@ver >= 8)
    SET @cmd = N'
        CREATE PROCEDURE dbo.TempGetStateItem
            @id         tSessionId,
            @itemShort  tSessionItemShort OUTPUT,
            @locked     bit OUTPUT,
            @lockDate   datetime OUTPUT,
            @lockCookie int OUTPUT
        AS
            DECLARE @textptr AS tTextPtr
            DECLARE @length AS int
            DECLARE @now AS datetime
            SET @now = GETUTCDATE()

            UPDATE [WebDiary].dbo.ASPStateTempSessions
            SET Expires = DATEADD(n, Timeout, @now), 
                @locked = Locked,
                @lockDate = LockDateLocal,
                @lockCookie = LockCookie,
                @itemShort = CASE @locked
                    WHEN 0 THEN SessionItemShort
                    ELSE NULL
                    END,
                @textptr = CASE @locked
                    WHEN 0 THEN TEXTPTR(SessionItemLong)
                    ELSE NULL
                    END,
                @length = CASE @locked
                    WHEN 0 THEN DATALENGTH(SessionItemLong)
                    ELSE NULL
                    END
            WHERE SessionId = @id
            IF @length IS NOT NULL BEGIN
                READTEXT [WebDiary].dbo.ASPStateTempSessions.SessionItemLong @textptr 0 @length
            END

            RETURN 0'
ELSE
    SET @cmd = N'
        CREATE PROCEDURE dbo.TempGetStateItem
            @id         tSessionId,
            @itemShort  tSessionItemShort OUTPUT,
            @locked     bit OUTPUT,
            @lockDate   datetime OUTPUT,
            @lockCookie int OUTPUT
        AS
            DECLARE @textptr AS tTextPtr
            DECLARE @length AS int
            DECLARE @now AS datetime
            SET @now = GETDATE()

            UPDATE [WebDiary].dbo.ASPStateTempSessions
            SET Expires = DATEADD(n, Timeout, @now), 
                @locked = Locked,
                @lockDate = LockDate,
                @lockCookie = LockCookie,
                @itemShort = CASE @locked
                    WHEN 0 THEN SessionItemShort
                    ELSE NULL
                    END,
                @textptr = CASE @locked
                    WHEN 0 THEN TEXTPTR(SessionItemLong)
                    ELSE NULL
                    END,
                @length = CASE @locked
                    WHEN 0 THEN DATALENGTH(SessionItemLong)
                    ELSE NULL
                    END
            WHERE SessionId = @id
            IF @length IS NOT NULL BEGIN
                READTEXT [WebDiary].dbo.ASPStateTempSessions.SessionItemLong @textptr 0 @length
            END

            RETURN 0'
    
EXEC (@cmd)    
GO

/*****************************************************************************/

DECLARE @ver int
EXEC dbo.GetMajorVersion @@ver=@ver OUTPUT
DECLARE @cmd nchar(4000)
IF (@ver >= 8)
    SET @cmd = N'
        CREATE PROCEDURE dbo.TempGetStateItem2
            @id         tSessionId,
            @itemShort  tSessionItemShort OUTPUT,
            @locked     bit OUTPUT,
            @lockAge    int OUTPUT,
            @lockCookie int OUTPUT
        AS
            DECLARE @textptr AS tTextPtr
            DECLARE @length AS int
            DECLARE @now AS datetime
            SET @now = GETUTCDATE()

            UPDATE [WebDiary].dbo.ASPStateTempSessions
            SET Expires = DATEADD(n, Timeout, @now), 
                @locked = Locked,
                @lockAge = DATEDIFF(second, LockDate, @now),
                @lockCookie = LockCookie,
                @itemShort = CASE @locked
                    WHEN 0 THEN SessionItemShort
                    ELSE NULL
                    END,
                @textptr = CASE @locked
                    WHEN 0 THEN TEXTPTR(SessionItemLong)
                    ELSE NULL
                    END,
                @length = CASE @locked
                    WHEN 0 THEN DATALENGTH(SessionItemLong)
                    ELSE NULL
                    END
            WHERE SessionId = @id
            IF @length IS NOT NULL BEGIN
                READTEXT [WebDiary].dbo.ASPStateTempSessions.SessionItemLong @textptr 0 @length
            END

            RETURN 0'

EXEC (@cmd)    
GO
            

/*****************************************************************************/

/* Find out the version */

DECLARE @ver int
EXEC dbo.GetMajorVersion @@ver=@ver OUTPUT
DECLARE @cmd nchar(4000)
IF (@ver >= 8)
    SET @cmd = N'
        CREATE PROCEDURE dbo.TempGetStateItem3
            @id         tSessionId,
            @itemShort  tSessionItemShort OUTPUT,
            @locked     bit OUTPUT,
            @lockAge    int OUTPUT,
            @lockCookie int OUTPUT,
            @actionFlags int OUTPUT
        AS
            DECLARE @textptr AS tTextPtr
            DECLARE @length AS int
            DECLARE @now AS datetime
            SET @now = GETUTCDATE()

            UPDATE [WebDiary].dbo.ASPStateTempSessions
            SET Expires = DATEADD(n, Timeout, @now), 
                @locked = Locked,
                @lockAge = DATEDIFF(second, LockDate, @now),
                @lockCookie = LockCookie,
                @itemShort = CASE @locked
                    WHEN 0 THEN SessionItemShort
                    ELSE NULL
                    END,
                @textptr = CASE @locked
                    WHEN 0 THEN TEXTPTR(SessionItemLong)
                    ELSE NULL
                    END,
                @length = CASE @locked
                    WHEN 0 THEN DATALENGTH(SessionItemLong)
                    ELSE NULL
                    END,

                /* If the Uninitialized flag (0x1) if it is set,
                   remove it and return InitializeItem (0x1) in actionFlags */
                Flags = CASE
                    WHEN (Flags & 1) <> 0 THEN (Flags & ~1)
                    ELSE Flags
                    END,
                @actionFlags = CASE
                    WHEN (Flags & 1) <> 0 THEN 1
                    ELSE 0
                    END
            WHERE SessionId = @id
            IF @length IS NOT NULL BEGIN
                READTEXT [WebDiary].dbo.ASPStateTempSessions.SessionItemLong @textptr 0 @length
            END

            RETURN 0'
ELSE
    SET @cmd = N'
        CREATE PROCEDURE dbo.TempGetStateItem3
            @id         tSessionId,
            @itemShort  tSessionItemShort OUTPUT,
            @locked     bit OUTPUT,
            @lockDate   datetime OUTPUT,
            @lockCookie int OUTPUT,
            @actionFlags int OUTPUT
        AS
            DECLARE @textptr AS tTextPtr
            DECLARE @length AS int
            DECLARE @now AS datetime
            SET @now = GETDATE()

            UPDATE [WebDiary].dbo.ASPStateTempSessions
            SET Expires = DATEADD(n, Timeout, @now), 
                @locked = Locked,
                @lockDate = LockDate,
                @lockCookie = LockCookie,
                @itemShort = CASE @locked
                    WHEN 0 THEN SessionItemShort
                    ELSE NULL
                    END,
                @textptr = CASE @locked
                    WHEN 0 THEN TEXTPTR(SessionItemLong)
                    ELSE NULL
                    END,
                @length = CASE @locked
                    WHEN 0 THEN DATALENGTH(SessionItemLong)
                    ELSE NULL
                    END,

                /* If the Uninitialized flag (0x1) if it is set,
                   remove it and return InitializeItem (0x1) in actionFlags */
                Flags = CASE
                    WHEN (Flags & 1) <> 0 THEN (Flags & ~1)
                    ELSE Flags
                    END,
                @actionFlags = CASE
                    WHEN (Flags & 1) <> 0 THEN 1
                    ELSE 0
                    END
            WHERE SessionId = @id
            IF @length IS NOT NULL BEGIN
                READTEXT [WebDiary].dbo.ASPStateTempSessions.SessionItemLong @textptr 0 @length
            END

            RETURN 0'
    
EXEC (@cmd)    
GO

/*****************************************************************************/

DECLARE @ver int
EXEC dbo.GetMajorVersion @@ver=@ver OUTPUT
DECLARE @cmd nchar(4000)
IF (@ver >= 8)
    SET @cmd = N'
        CREATE PROCEDURE dbo.TempGetStateItemExclusive
            @id         tSessionId,
            @itemShort  tSessionItemShort OUTPUT,
            @locked     bit OUTPUT,
            @lockDate   datetime OUTPUT,
            @lockCookie int OUTPUT
        AS
            DECLARE @textptr AS tTextPtr
            DECLARE @length AS int
            DECLARE @now AS datetime
            DECLARE @nowLocal AS datetime

            SET @now = GETUTCDATE()
            SET @nowLocal = GETDATE()
            
            UPDATE [WebDiary].dbo.ASPStateTempSessions
            SET Expires = DATEADD(n, Timeout, @now), 
                LockDate = CASE Locked
                    WHEN 0 THEN @now
                    ELSE LockDate
                    END,
                @lockDate = LockDateLocal = CASE Locked
                    WHEN 0 THEN @nowLocal
                    ELSE LockDateLocal
                    END,
                @lockCookie = LockCookie = CASE Locked
                    WHEN 0 THEN LockCookie + 1
                    ELSE LockCookie
                    END,
                @itemShort = CASE Locked
                    WHEN 0 THEN SessionItemShort
                    ELSE NULL
                    END,
                @textptr = CASE Locked
                    WHEN 0 THEN TEXTPTR(SessionItemLong)
                    ELSE NULL
                    END,
                @length = CASE Locked
                    WHEN 0 THEN DATALENGTH(SessionItemLong)
                    ELSE NULL
                    END,
                @locked = Locked,
                Locked = 1
            WHERE SessionId = @id
            IF @length IS NOT NULL BEGIN
                READTEXT [WebDiary].dbo.ASPStateTempSessions.SessionItemLong @textptr 0 @length
            END

            RETURN 0'
ELSE
    SET @cmd = N'
        CREATE PROCEDURE dbo.TempGetStateItemExclusive
            @id         tSessionId,
            @itemShort  tSessionItemShort OUTPUT,
            @locked     bit OUTPUT,
            @lockDate   datetime OUTPUT,
            @lockCookie int OUTPUT
        AS
            DECLARE @textptr AS tTextPtr
            DECLARE @length AS int
            DECLARE @now AS datetime

            SET @now = GETDATE()
            UPDATE [WebDiary].dbo.ASPStateTempSessions
            SET Expires = DATEADD(n, Timeout, @now), 
                @lockDate = LockDate = CASE Locked
                    WHEN 0 THEN @now
                    ELSE LockDate
                    END,
                @lockCookie = LockCookie = CASE Locked
                    WHEN 0 THEN LockCookie + 1
                    ELSE LockCookie
                    END,
                @itemShort = CASE Locked
                    WHEN 0 THEN SessionItemShort
                    ELSE NULL
                    END,
                @textptr = CASE Locked
                    WHEN 0 THEN TEXTPTR(SessionItemLong)
                    ELSE NULL
                    END,
                @length = CASE Locked
                    WHEN 0 THEN DATALENGTH(SessionItemLong)
                    ELSE NULL
                    END,
                @locked = Locked,
                Locked = 1
            WHERE SessionId = @id
            IF @length IS NOT NULL BEGIN
                READTEXT [WebDiary].dbo.ASPStateTempSessions.SessionItemLong @textptr 0 @length
            END

            RETURN 0'    

EXEC (@cmd)    
GO


/*****************************************************************************/

DECLARE @ver int
EXEC dbo.GetMajorVersion @@ver=@ver OUTPUT
DECLARE @cmd nchar(4000)
IF (@ver >= 8)
    SET @cmd = N'
        CREATE PROCEDURE dbo.TempGetStateItemExclusive2
            @id         tSessionId,
            @itemShort  tSessionItemShort OUTPUT,
            @locked     bit OUTPUT,
            @lockAge    int OUTPUT,
            @lockCookie int OUTPUT
        AS
            DECLARE @textptr AS tTextPtr
            DECLARE @length AS int
            DECLARE @now AS datetime
            DECLARE @nowLocal AS datetime

            SET @now = GETUTCDATE()
            SET @nowLocal = GETDATE()
            
            UPDATE [WebDiary].dbo.ASPStateTempSessions
            SET Expires = DATEADD(n, Timeout, @now), 
                LockDate = CASE Locked
                    WHEN 0 THEN @now
                    ELSE LockDate
                    END,
                LockDateLocal = CASE Locked
                    WHEN 0 THEN @nowLocal
                    ELSE LockDateLocal
                    END,
                @lockAge = CASE Locked
                    WHEN 0 THEN 0
                    ELSE DATEDIFF(second, LockDate, @now)
                    END,
                @lockCookie = LockCookie = CASE Locked
                    WHEN 0 THEN LockCookie + 1
                    ELSE LockCookie
                    END,
                @itemShort = CASE Locked
                    WHEN 0 THEN SessionItemShort
                    ELSE NULL
                    END,
                @textptr = CASE Locked
                    WHEN 0 THEN TEXTPTR(SessionItemLong)
                    ELSE NULL
                    END,
                @length = CASE Locked
                    WHEN 0 THEN DATALENGTH(SessionItemLong)
                    ELSE NULL
                    END,
                @locked = Locked,
                Locked = 1
            WHERE SessionId = @id
            IF @length IS NOT NULL BEGIN
                READTEXT [WebDiary].dbo.ASPStateTempSessions.SessionItemLong @textptr 0 @length
            END

            RETURN 0'

EXEC (@cmd)    
GO


/*****************************************************************************/

DECLARE @ver int
EXEC dbo.GetMajorVersion @@ver=@ver OUTPUT
DECLARE @cmd nchar(4000)
IF (@ver >= 8)
    SET @cmd = N'
        CREATE PROCEDURE dbo.TempGetStateItemExclusive3
            @id         tSessionId,
            @itemShort  tSessionItemShort OUTPUT,
            @locked     bit OUTPUT,
            @lockAge    int OUTPUT,
            @lockCookie int OUTPUT,
            @actionFlags int OUTPUT
        AS
            DECLARE @textptr AS tTextPtr
            DECLARE @length AS int
            DECLARE @now AS datetime
            DECLARE @nowLocal AS datetime

            SET @now = GETUTCDATE()
            SET @nowLocal = GETDATE()
            
            UPDATE [WebDiary].dbo.ASPStateTempSessions
            SET Expires = DATEADD(n, Timeout, @now), 
                LockDate = CASE Locked
                    WHEN 0 THEN @now
                    ELSE LockDate
                    END,
                LockDateLocal = CASE Locked
                    WHEN 0 THEN @nowLocal
                    ELSE LockDateLocal
                    END,
                @lockAge = CASE Locked
                    WHEN 0 THEN 0
                    ELSE DATEDIFF(second, LockDate, @now)
                    END,
                @lockCookie = LockCookie = CASE Locked
                    WHEN 0 THEN LockCookie + 1
                    ELSE LockCookie
                    END,
                @itemShort = CASE Locked
                    WHEN 0 THEN SessionItemShort
                    ELSE NULL
                    END,
                @textptr = CASE Locked
                    WHEN 0 THEN TEXTPTR(SessionItemLong)
                    ELSE NULL
                    END,
                @length = CASE Locked
                    WHEN 0 THEN DATALENGTH(SessionItemLong)
                    ELSE NULL
                    END,
                @locked = Locked,
                Locked = 1,

                /* If the Uninitialized flag (0x1) if it is set,
                   remove it and return InitializeItem (0x1) in actionFlags */
                Flags = CASE
                    WHEN (Flags & 1) <> 0 THEN (Flags & ~1)
                    ELSE Flags
                    END,
                @actionFlags = CASE
                    WHEN (Flags & 1) <> 0 THEN 1
                    ELSE 0
                    END
            WHERE SessionId = @id
            IF @length IS NOT NULL BEGIN
                READTEXT [WebDiary].dbo.ASPStateTempSessions.SessionItemLong @textptr 0 @length
            END

            RETURN 0'
ELSE
    SET @cmd = N'
        CREATE PROCEDURE dbo.TempGetStateItemExclusive3
            @id         tSessionId,
            @itemShort  tSessionItemShort OUTPUT,
            @locked     bit OUTPUT,
            @lockDate   datetime OUTPUT,
            @lockCookie int OUTPUT,
            @actionFlags int OUTPUT
        AS
            DECLARE @textptr AS tTextPtr
            DECLARE @length AS int
            DECLARE @now AS datetime

            SET @now = GETDATE()
            UPDATE [WebDiary].dbo.ASPStateTempSessions
            SET Expires = DATEADD(n, Timeout, @now), 
                @lockDate = LockDate = CASE Locked
                    WHEN 0 THEN @now
                    ELSE LockDate
                    END,
                @lockCookie = LockCookie = CASE Locked
                    WHEN 0 THEN LockCookie + 1
                    ELSE LockCookie
                    END,
                @itemShort = CASE Locked
                    WHEN 0 THEN SessionItemShort
                    ELSE NULL
                    END,
                @textptr = CASE Locked
                    WHEN 0 THEN TEXTPTR(SessionItemLong)
                    ELSE NULL
                    END,
                @length = CASE Locked
                    WHEN 0 THEN DATALENGTH(SessionItemLong)
                    ELSE NULL
                    END,
                @locked = Locked,
                Locked = 1,

                /* If the Uninitialized flag (0x1) if it is set,
                   remove it and return InitializeItem (0x1) in actionFlags */
                Flags = CASE
                    WHEN (Flags & 1) <> 0 THEN (Flags & ~1)
                    ELSE Flags
                    END,
                @actionFlags = CASE
                    WHEN (Flags & 1) <> 0 THEN 1
                    ELSE 0
                    END
            WHERE SessionId = @id
            IF @length IS NOT NULL BEGIN
                READTEXT [WebDiary].dbo.ASPStateTempSessions.SessionItemLong @textptr 0 @length
            END

            RETURN 0'    

EXEC (@cmd)    
GO


/*****************************************************************************/

DECLARE @ver int
EXEC dbo.GetMajorVersion @@ver=@ver OUTPUT
DECLARE @cmd nchar(4000)
IF (@ver >= 8)
    SET @cmd = N'
        CREATE PROCEDURE dbo.TempReleaseStateItemExclusive
            @id         tSessionId,
            @lockCookie int
        AS
            UPDATE [WebDiary].dbo.ASPStateTempSessions
            SET Expires = DATEADD(n, Timeout, GETUTCDATE()), 
                Locked = 0
            WHERE SessionId = @id AND LockCookie = @lockCookie

            RETURN 0'
ELSE
    SET @cmd = N'
        CREATE PROCEDURE dbo.TempReleaseStateItemExclusive
            @id         tSessionId,
            @lockCookie int
        AS
            UPDATE [WebDiary].dbo.ASPStateTempSessions
            SET Expires = DATEADD(n, Timeout, GETDATE()), 
                Locked = 0
            WHERE SessionId = @id AND LockCookie = @lockCookie

            RETURN 0'

EXEC (@cmd)    
GO


/*****************************************************************************/

DECLARE @ver int
EXEC dbo.GetMajorVersion @@ver=@ver OUTPUT
DECLARE @cmd nchar(4000)
IF (@ver >= 8)
    SET @cmd = N'
        CREATE PROCEDURE dbo.TempInsertUninitializedItem
            @id         tSessionId,
            @itemShort  tSessionItemShort,
            @timeout    int
        AS    

            DECLARE @now AS datetime
            DECLARE @nowLocal AS datetime
            
            SET @now = GETUTCDATE()
            SET @nowLocal = GETDATE()

            INSERT [WebDiary].dbo.ASPStateTempSessions 
                (SessionId, 
                 SessionItemShort, 
                 Timeout, 
                 Expires, 
                 Locked, 
                 LockDate,
                 LockDateLocal,
                 LockCookie,
                 Flags) 
            VALUES 
                (@id, 
                 @itemShort, 
                 @timeout, 
                 DATEADD(n, @timeout, @now), 
                 0, 
                 @now,
                 @nowLocal,
                 1,
                 1)

            RETURN 0'
ELSE
    SET @cmd = N'
        CREATE PROCEDURE dbo.TempInsertUninitializedItem
            @id         tSessionId,
            @itemShort  tSessionItemShort,
            @timeout    int
        AS    

            DECLARE @now AS datetime
            SET @now = GETDATE()

            INSERT [WebDiary].dbo.ASPStateTempSessions 
                (SessionId, 
                 SessionItemShort, 
                 Timeout, 
                 Expires, 
                 Locked, 
                 LockDate,
                 LockCookie,
                 Flags) 
            VALUES 
                (@id, 
                 @itemShort, 
                 @timeout, 
                 DATEADD(n, @timeout, @now), 
                 0, 
                 @now,
                 1,
                 1)

            RETURN 0'

EXEC (@cmd)    
GO


/*****************************************************************************/

DECLARE @ver int
EXEC dbo.GetMajorVersion @@ver=@ver OUTPUT
DECLARE @cmd nchar(4000)
IF (@ver >= 8)
    SET @cmd = N'
        CREATE PROCEDURE dbo.TempInsertStateItemShort
            @id         tSessionId,
            @itemShort  tSessionItemShort,
            @timeout    int
        AS    

            DECLARE @now AS datetime
            DECLARE @nowLocal AS datetime
            
            SET @now = GETUTCDATE()
            SET @nowLocal = GETDATE()

            INSERT [WebDiary].dbo.ASPStateTempSessions 
                (SessionId, 
                 SessionItemShort, 
                 Timeout, 
                 Expires, 
                 Locked, 
                 LockDate,
                 LockDateLocal,
                 LockCookie) 
            VALUES 
                (@id, 
                 @itemShort, 
                 @timeout, 
                 DATEADD(n, @timeout, @now), 
                 0, 
                 @now,
                 @nowLocal,
                 1)

            RETURN 0'
ELSE
    SET @cmd = N'
        CREATE PROCEDURE dbo.TempInsertStateItemShort
            @id         tSessionId,
            @itemShort  tSessionItemShort,
            @timeout    int
        AS    

            DECLARE @now AS datetime
            SET @now = GETDATE()

            INSERT [WebDiary].dbo.ASPStateTempSessions 
                (SessionId, 
                 SessionItemShort, 
                 Timeout, 
                 Expires, 
                 Locked, 
                 LockDate,
                 LockCookie) 
            VALUES 
                (@id, 
                 @itemShort, 
                 @timeout, 
                 DATEADD(n, @timeout, @now), 
                 0, 
                 @now,
                 1)

            RETURN 0'

EXEC (@cmd)    
GO


/*****************************************************************************/

DECLARE @ver int
EXEC dbo.GetMajorVersion @@ver=@ver OUTPUT
DECLARE @cmd nchar(4000)
IF (@ver >= 8)
    SET @cmd = N'
        CREATE PROCEDURE dbo.TempInsertStateItemLong
            @id         tSessionId,
            @itemLong   tSessionItemLong,
            @timeout    int
        AS    
            DECLARE @now AS datetime
            DECLARE @nowLocal AS datetime
            
            SET @now = GETUTCDATE()
            SET @nowLocal = GETDATE()

            INSERT [WebDiary].dbo.ASPStateTempSessions 
                (SessionId, 
                 SessionItemLong, 
                 Timeout, 
                 Expires, 
                 Locked, 
                 LockDate,
                 LockDateLocal,
                 LockCookie) 
            VALUES 
                (@id, 
                 @itemLong, 
                 @timeout, 
                 DATEADD(n, @timeout, @now), 
                 0, 
                 @now,
                 @nowLocal,
                 1)

            RETURN 0'
ELSE
    SET @cmd = N'
        CREATE PROCEDURE dbo.TempInsertStateItemLong
            @id         tSessionId,
            @itemLong   tSessionItemLong,
            @timeout    int
        AS    
            DECLARE @now AS datetime
            SET @now = GETDATE()

            INSERT [WebDiary].dbo.ASPStateTempSessions 
                (SessionId, 
                 SessionItemLong, 
                 Timeout, 
                 Expires, 
                 Locked, 
                 LockDate,
                 LockCookie) 
            VALUES 
                (@id, 
                 @itemLong, 
                 @timeout, 
                 DATEADD(n, @timeout, @now), 
                 0, 
                 @now,
                 1)

            RETURN 0'

EXEC (@cmd)    
GO


/*****************************************************************************/

DECLARE @ver int
EXEC dbo.GetMajorVersion @@ver=@ver OUTPUT
DECLARE @cmd nchar(4000)
IF (@ver >= 8)
    SET @cmd = N'
        CREATE PROCEDURE dbo.TempUpdateStateItemShort
            @id         tSessionId,
            @itemShort  tSessionItemShort,
            @timeout    int,
            @lockCookie int
        AS    
            UPDATE [WebDiary].dbo.ASPStateTempSessions
            SET Expires = DATEADD(n, @timeout, GETUTCDATE()), 
                SessionItemShort = @itemShort, 
                Timeout = @timeout,
                Locked = 0
            WHERE SessionId = @id AND LockCookie = @lockCookie

            RETURN 0'
ELSE
    SET @cmd = N'
        CREATE PROCEDURE dbo.TempUpdateStateItemShort
            @id         tSessionId,
            @itemShort  tSessionItemShort,
            @timeout    int,
            @lockCookie int
        AS    
            UPDATE [WebDiary].dbo.ASPStateTempSessions
            SET Expires = DATEADD(n, @timeout, GETDATE()), 
                SessionItemShort = @itemShort, 
                Timeout = @timeout,
                Locked = 0
            WHERE SessionId = @id AND LockCookie = @lockCookie

            RETURN 0'

EXEC (@cmd)    
GO


/*****************************************************************************/

DECLARE @ver int
EXEC dbo.GetMajorVersion @@ver=@ver OUTPUT
DECLARE @cmd nchar(4000)
IF (@ver >= 8)
    SET @cmd = N'
        CREATE PROCEDURE dbo.TempUpdateStateItemShortNullLong
            @id         tSessionId,
            @itemShort  tSessionItemShort,
            @timeout    int,
            @lockCookie int
        AS    
            UPDATE [WebDiary].dbo.ASPStateTempSessions
            SET Expires = DATEADD(n, @timeout, GETUTCDATE()), 
                SessionItemShort = @itemShort, 
                SessionItemLong = NULL, 
                Timeout = @timeout,
                Locked = 0
            WHERE SessionId = @id AND LockCookie = @lockCookie

            RETURN 0'
ELSE
    SET @cmd = N'
        CREATE PROCEDURE dbo.TempUpdateStateItemShortNullLong
            @id         tSessionId,
            @itemShort  tSessionItemShort,
            @timeout    int,
            @lockCookie int
        AS    
            UPDATE [WebDiary].dbo.ASPStateTempSessions
            SET Expires = DATEADD(n, @timeout, GETDATE()), 
                SessionItemShort = @itemShort, 
                SessionItemLong = NULL, 
                Timeout = @timeout,
                Locked = 0
            WHERE SessionId = @id AND LockCookie = @lockCookie

            RETURN 0'

EXEC (@cmd)    
GO


/*****************************************************************************/

DECLARE @ver int
EXEC dbo.GetMajorVersion @@ver=@ver OUTPUT
DECLARE @cmd nchar(4000)
IF (@ver >= 8)
    SET @cmd = N'
        CREATE PROCEDURE dbo.TempUpdateStateItemLong
            @id         tSessionId,
            @itemLong   tSessionItemLong,
            @timeout    int,
            @lockCookie int
        AS    
            UPDATE [WebDiary].dbo.ASPStateTempSessions
            SET Expires = DATEADD(n, @timeout, GETUTCDATE()), 
                SessionItemLong = @itemLong,
                Timeout = @timeout,
                Locked = 0
            WHERE SessionId = @id AND LockCookie = @lockCookie

            RETURN 0'
ELSE
    SET @cmd = N'
        CREATE PROCEDURE dbo.TempUpdateStateItemLong
            @id         tSessionId,
            @itemLong   tSessionItemLong,
            @timeout    int,
            @lockCookie int
        AS    
            UPDATE [WebDiary].dbo.ASPStateTempSessions
            SET Expires = DATEADD(n, @timeout, GETDATE()), 
                SessionItemLong = @itemLong,
                Timeout = @timeout,
                Locked = 0
            WHERE SessionId = @id AND LockCookie = @lockCookie

            RETURN 0'

EXEC (@cmd)            
GO


/*****************************************************************************/

DECLARE @ver int
EXEC dbo.GetMajorVersion @@ver=@ver OUTPUT
DECLARE @cmd nchar(4000)
IF (@ver >= 8)
    SET @cmd = N'
        CREATE PROCEDURE dbo.TempUpdateStateItemLongNullShort
            @id         tSessionId,
            @itemLong   tSessionItemLong,
            @timeout    int,
            @lockCookie int
        AS    
            UPDATE [WebDiary].dbo.ASPStateTempSessions
            SET Expires = DATEADD(n, @timeout, GETUTCDATE()), 
                SessionItemLong = @itemLong, 
                SessionItemShort = NULL,
                Timeout = @timeout,
                Locked = 0
            WHERE SessionId = @id AND LockCookie = @lockCookie

            RETURN 0'
ELSE
    SET @cmd = N'
    CREATE PROCEDURE dbo.TempUpdateStateItemLongNullShort
        @id         tSessionId,
        @itemLong   tSessionItemLong,
        @timeout    int,
        @lockCookie int
    AS    
        UPDATE [WebDiary].dbo.ASPStateTempSessions
        SET Expires = DATEADD(n, @timeout, GETDATE()), 
            SessionItemLong = @itemLong, 
            SessionItemShort = NULL,
            Timeout = @timeout,
            Locked = 0
        WHERE SessionId = @id AND LockCookie = @lockCookie

        RETURN 0'

EXEC (@cmd)            
GO

/*****************************************************************************/

DECLARE @cmd nchar(4000)
SET @cmd = N'
    CREATE PROCEDURE dbo.TempRemoveStateItem
        @id     tSessionId,
        @lockCookie int
    AS
        DELETE [WebDiary].dbo.ASPStateTempSessions
        WHERE SessionId = @id AND LockCookie = @lockCookie
        RETURN 0'
EXEC(@cmd)    
GO
            
/*****************************************************************************/

DECLARE @ver int
EXEC dbo.GetMajorVersion @@ver=@ver OUTPUT
DECLARE @cmd nchar(4000)
IF (@ver >= 8)
    SET @cmd = N'
        CREATE PROCEDURE dbo.TempResetTimeout
            @id     tSessionId
        AS
            UPDATE [WebDiary].dbo.ASPStateTempSessions
            SET Expires = DATEADD(n, Timeout, GETUTCDATE())
            WHERE SessionId = @id
            RETURN 0'
ELSE
    SET @cmd = N'
        CREATE PROCEDURE dbo.TempResetTimeout
            @id     tSessionId
        AS
            UPDATE [WebDiary].dbo.ASPStateTempSessions
            SET Expires = DATEADD(n, Timeout, GETDATE())
            WHERE SessionId = @id
            RETURN 0'

EXEC (@cmd)            
GO

            
/*****************************************************************************/

DECLARE @ver int
EXEC dbo.GetMajorVersion @@ver=@ver OUTPUT
DECLARE @cmd nchar(4000)
IF (@ver >= 8)
    SET @cmd = N'
        CREATE PROCEDURE dbo.DeleteExpiredSessions
        AS
            SET NOCOUNT ON
            SET DEADLOCK_PRIORITY LOW 

            DECLARE @now datetime
            SET @now = GETUTCDATE() 

            CREATE TABLE #tblExpiredSessions 
            ( 
                SessionID nvarchar(88) NOT NULL PRIMARY KEY
            )

            INSERT #tblExpiredSessions (SessionID)
                SELECT SessionID
                FROM [WebDiary].dbo.ASPStateTempSessions WITH (READUNCOMMITTED)
                WHERE Expires < @now

            IF @@ROWCOUNT <> 0 
            BEGIN 
                DECLARE ExpiredSessionCursor CURSOR LOCAL FORWARD_ONLY READ_ONLY
                FOR SELECT SessionID FROM #tblExpiredSessions 

                DECLARE @SessionID nvarchar(88)

                OPEN ExpiredSessionCursor

                FETCH NEXT FROM ExpiredSessionCursor INTO @SessionID

                WHILE @@FETCH_STATUS = 0 
                    BEGIN
                        DELETE FROM [WebDiary].dbo.ASPStateTempSessions WHERE SessionID = @SessionID AND Expires < @now
                        FETCH NEXT FROM ExpiredSessionCursor INTO @SessionID
                    END

                CLOSE ExpiredSessionCursor

                DEALLOCATE ExpiredSessionCursor

            END 

            DROP TABLE #tblExpiredSessions

        RETURN 0'
ELSE
    SET @cmd = N'
        CREATE PROCEDURE dbo.DeleteExpiredSessions
        AS
            SET NOCOUNT ON
            SET DEADLOCK_PRIORITY LOW 

            DECLARE @now datetime
            SET @now = GETDATE() 

            CREATE TABLE #tblExpiredSessions 
            ( 
                SessionID nvarchar(88) NOT NULL PRIMARY KEY
            )

            INSERT #tblExpiredSessions (SessionID)
                SELECT SessionID
                FROM [WebDiary].dbo.ASPStateTempSessions WITH (READUNCOMMITTED)
                WHERE Expires < @now

            IF @@ROWCOUNT <> 0 
            BEGIN 
                DECLARE ExpiredSessionCursor CURSOR LOCAL FORWARD_ONLY READ_ONLY
                FOR SELECT SessionID FROM #tblExpiredSessions 

                DECLARE @SessionID nvarchar(88)

                OPEN ExpiredSessionCursor

                FETCH NEXT FROM ExpiredSessionCursor INTO @SessionID

                WHILE @@FETCH_STATUS = 0 
                    BEGIN
                        DELETE FROM [WebDiary].dbo.ASPStateTempSessions WHERE SessionID = @SessionID AND Expires < @now
                        FETCH NEXT FROM ExpiredSessionCursor INTO @SessionID
                    END

                CLOSE ExpiredSessionCursor

                DEALLOCATE ExpiredSessionCursor

            END 

            DROP TABLE #tblExpiredSessions

        RETURN 0'
EXEC (@cmd)            
GO
            
/*****************************************************************************/

EXECUTE dbo.CreateTempTables
GO

USE master
GO

DECLARE @sstype nvarchar(128)
SET @sstype = N'sstype_temp'

IF UPPER(@sstype) = 'SSTYPE_TEMP' BEGIN
    DECLARE @cmd nchar(4000)

    SET @cmd = N'
        /* Create the startup procedure */
        CREATE PROCEDURE dbo.ASPState_Startup 
        AS
            EXECUTE WebDiary.dbo.CreateTempTables

            RETURN 0'
    EXEC(@cmd)
    EXECUTE sp_procoption @ProcName='dbo.ASPState_Startup', @OptionName='startup', @OptionValue='true'
END    

/*****************************************************************************/

/* Create the job to delete expired sessions */

-- Add job category
-- We don't expect an error if the category already exists. So we need to check if it already exists
DECLARE @catId int
SELECT @catId = category_id FROM msdb.dbo.syscategories WHERE (name = N'[Uncategorized (Local)]')
IF (@catId IS NULL)
BEGIN
    EXEC msdb.dbo.sp_add_category @name = N'[Uncategorized (Local)]'
END
GO

BEGIN TRANSACTION            
    DECLARE @JobID BINARY(16)  
    DECLARE @ReturnCode int    
    DECLARE @nameT nchar(200)
    SELECT @ReturnCode = 0     

    -- Add the job
    SET @nameT = N'ASPState' + '_Job_DeleteExpiredSessions'
    EXECUTE @ReturnCode = msdb.dbo.sp_add_job 
            @job_id = @JobID OUTPUT, 
            @job_name = @nameT, 
            @owner_login_name = NULL, 
            @description = N'Deletes expired sessions from the session state database.', 
            @category_name = N'[Uncategorized (Local)]', 
            @enabled = 1, 
            @notify_level_email = 0, 
            @notify_level_page = 0, 
            @notify_level_netsend = 0, 
            @notify_level_eventlog = 0, 
            @delete_level= 0

    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 
    
    -- Add the job steps
    SET @nameT = N'ASPState' + '_JobStep_DeleteExpiredSessions'
    EXECUTE @ReturnCode = msdb.dbo.sp_add_jobstep 
            @job_id = @JobID,
            @step_id = 1, 
            @step_name = @nameT, 
            @command = N'EXECUTE DeleteExpiredSessions', 
            @database_name = N'WebDiary', 
            @server = N'', 
            @subsystem = N'TSQL', 
            @cmdexec_success_code = 0, 
            @flags = 0, 
            @retry_attempts = 0, 
            @retry_interval = 1, 
            @output_file_name = N'', 
            @on_success_step_id = 0, 
            @on_success_action = 1, 
            @on_fail_step_id = 0, 
            @on_fail_action = 2

    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 

    EXECUTE @ReturnCode = msdb.dbo.sp_update_job @job_id = @JobID, @start_step_id = 1 
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 
    
    -- Add the job schedules
    SET @nameT = N'ASPState' + '_JobSchedule_DeleteExpiredSessions'
    EXECUTE @ReturnCode = msdb.dbo.sp_add_jobschedule 
            @job_id = @JobID, 
            @name = @nameT, 
            @enabled = 1, 
            @freq_type = 4,     
            @active_start_date = 20001016, 
            @active_start_time = 0, 
            @freq_interval = 1, 
            @freq_subday_type = 4, 
            @freq_subday_interval = 1, 
            @freq_relative_interval = 0, 
            @freq_recurrence_factor = 0, 
            @active_end_date = 99991231, 
            @active_end_time = 235959

    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 
    
    -- Add the Target Servers
    EXECUTE @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @JobID, @server_name = N'(local)' 
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 
    
    COMMIT TRANSACTION          
    GOTO   EndSave              
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION 
EndSave: 
GO

/***************************************************************************************/
/********************* Completed execution of InstallSqlState.SQL **********************/
/***************************************************************************************/


/****** Object:  Table [dbo].[ENVIRONMENTS]    Script Date: 03/15/2011 04:13:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

use [WebDiary]

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ENVIRONMENTS')
BEGIN
	CREATE TABLE [dbo].[ENVIRONMENTS](
		[environment_id] [int] IDENTITY(0,1) NOT NULL,
		[description] [varchar](255) NULL,
	 CONSTRAINT [PK_ENVIRONMENTS] PRIMARY KEY CLUSTERED 
	(
		[environment_id] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
END
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[VERSIONS]    Script Date: 11/04/2010 16:14:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'VERSIONS')
BEGIN
	CREATE TABLE [dbo].[VERSIONS](
		[version_id] [int] IDENTITY(1,1) NOT NULL,
		[major_version] [int] NOT NULL,
		[minor_version] [int] NOT NULL,
		[revision] [int] NOT NULL,
		[build_number] [int] NOT NULL,
		[creation_date] [datetime] NOT NULL,
		[modification_date] [datetime] NULL,
		[survey_tool_database] [varchar](255) NULL,
		[environment] [int] NOT NULL,
	 CONSTRAINT [PK_VERSIONS] PRIMARY KEY CLUSTERED 
	(
		[version_id] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY];

	ALTER TABLE [dbo].[VERSIONS]  WITH CHECK ADD  CONSTRAINT [FK_ENVIRONMENTS] FOREIGN KEY([environment])
REFERENCES [dbo].[ENVIRONMENTS] ([environment_id]);

	ALTER TABLE [dbo].[VERSIONS] CHECK CONSTRAINT [FK_ENVIRONMENTS];
END

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[STUDY_VERSIONS]    Script Date: 11/04/2010 16:13:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'STUDY_VERSIONS')
BEGIN
	CREATE TABLE [dbo].[STUDY_VERSIONS](
		[study_version_id] [int] IDENTITY(1,1) NOT NULL,
		[study_name] [varchar](40) NOT NULL,
		[major_version] [int] NOT NULL,
		[minor_version] [int] NOT NULL,
		[revision] [int] NOT NULL,
		[build_number] [int] NOT NULL,
		[creation_date] [datetime] NOT NULL,
		[modification_date] [datetime]NULL,
	 CONSTRAINT [PK_STUDY_VERSIONS] PRIMARY KEY CLUSTERED 
	(
		[study_version_id] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
END

/****** Object:  Table [dbo].[STUDIES]    Script Date: 09/14/2010 11:48:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'STUDIES')
BEGIN
	CREATE TABLE [dbo].[STUDIES](
		[study_id] [int] IDENTITY(1,1) NOT NULL,
		[study_name] [varchar](40) NOT NULL,
		[token_expiration_days] [int] NOT NULL,
		[ssa_base_url] [varchar](255) NULL,
		[sw_base_url] [varchar](255) NULL,
		[sw_username] [varchar](50) NULL,
		[sw_password] [varchar](50) NULL,
		[wd_enabled] [bit] NULL,
		[wd_study_base_url] [varchar](255) NOT NULL,
		[study_version_id] [int] NOT NULL,
		[version_id] [int] NOT NULL,
		[lang_code_list_id] [varchar](255) NOT NULL,
		[time_zone_code_list_id] [varchar](255) NOT NULL,
	 CONSTRAINT [PK_STUDIES] PRIMARY KEY CLUSTERED 
	(
		[study_id] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY];

	ALTER TABLE [dbo].[STUDIES]  WITH CHECK ADD  CONSTRAINT [FK_STUDIES_STUDY_VERSIONS] FOREIGN KEY([study_version_id])
REFERENCES [dbo].[STUDY_VERSIONS] ([study_version_id]);

	ALTER TABLE [dbo].[STUDIES] CHECK CONSTRAINT [FK_STUDIES_STUDY_VERSIONS];

	ALTER TABLE [dbo].[STUDIES]  WITH CHECK ADD  CONSTRAINT [FK_STUDIES_VERSIONS] FOREIGN KEY([version_id])
REFERENCES [dbo].[VERSIONS] ([version_id]);

	ALTER TABLE [dbo].[STUDIES] CHECK CONSTRAINT [FK_STUDIES_VERSIONS];

	ALTER TABLE [dbo].[STUDIES] ADD CONSTRAINT UNQ_STUDIES_STUDY_NAME UNIQUE ([study_name]);
END

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[STUDY_SURVEYS]    Script Date: 11/18/2010 10:18:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'STUDY_SURVEYS')
BEGIN
	CREATE TABLE [dbo].[STUDY_SURVEYS](
		[id] [int] IDENTITY(1,1) NOT NULL,
		[study_id] [int] NOT NULL,
		[display_name] [varchar](50) NULL,
		[form_type] [varchar](50) NULL,
		[activation_url] [varchar](255) NULL,
		[triggered_phase] [varchar](16) NULL,
	 CONSTRAINT [PK_STUDY_SURVEYS] PRIMARY KEY CLUSTERED 
	(
		[id] ASC
	)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
	) ON [PRIMARY];

	ALTER TABLE [dbo].[STUDY_SURVEYS]  WITH CHECK ADD  CONSTRAINT [FK_STUDY_SURVEYS_STUDIES] FOREIGN KEY([study_id])
	REFERENCES [dbo].[STUDIES] ([study_id]);
	
	ALTER TABLE [dbo].[STUDY_SURVEYS] CHECK CONSTRAINT [FK_STUDY_SURVEYS_STUDIES];
END
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[SUBJECTS]    Script Date: 09/13/2010 17:40:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SUBJECTS')
BEGIN
	CREATE TABLE [dbo].[SUBJECTS](
		[Id] [uniqueidentifier] NOT NULL,
		[email] [nvarchar](256) NOT NULL,
		[previous_email] [nvarchar](256) NULL,
		[password] [varchar](128) NULL,
		[is_approved] [bit] NOT NULL,
		[last_activity_date] [datetime] NULL,
		[last_login_date] [datetime] NULL,
		[last_password_changed_date] [datetime] NULL,
		[creation_date] [datetime] NOT NULL,
		[is_locked_out] [bit] NOT NULL,
		[last_lockout_date] [datetime] NULL,
		[failed_password_attempt_count] [int] NOT NULL,
		[failed_password_attempt_window_start] [datetime] NULL,
		[activation_token] [uniqueidentifier] NOT NULL,
		[activation_token_expiration_date] [datetime] NOT NULL,
		[krpt] [varchar](36) NOT NULL,
		[language] [char](5) NOT NULL,
		[timezone] [int] NOT NULL,
		[study_id] [int] NOT NULL,
		[time_travel_offset] [int] NOT NULL,
	 CONSTRAINT [PK_SUBJECTS] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
	 CONSTRAINT [DF_SUBJECTS_activation_token] UNIQUE NONCLUSTERED 
	(
		[activation_token] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
	 CONSTRAINT [DF_SUBJECTS_krpt_study_id] UNIQUE NONCLUSTERED 
	(
		[krpt] ASC,
		[study_id] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
	 CONSTRAINT [IX_SUBJECTS] UNIQUE NONCLUSTERED 
	(
		[email] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY];

	ALTER TABLE [dbo].[SUBJECTS]  WITH CHECK ADD  CONSTRAINT [FK_SUBJECTS_STUDIES] FOREIGN KEY([study_id])
REFERENCES [dbo].[STUDIES] ([study_id]);

	ALTER TABLE [dbo].[SUBJECTS] CHECK CONSTRAINT [FK_SUBJECTS_STUDIES];

	ALTER TABLE [dbo].[SUBJECTS] ADD  CONSTRAINT [DF_SUBJECTS_subject_id]  DEFAULT (newsequentialid()) FOR [Id];

	ALTER TABLE [dbo].[SUBJECTS] ADD  CONSTRAINT [DF_SUBJECTS_is_approved]  DEFAULT ((0)) FOR [is_approved];

	ALTER TABLE [dbo].[SUBJECTS] ADD  CONSTRAINT [DF_SUBJECTS_is_locked_out]  DEFAULT ((0)) FOR [is_locked_out];

	ALTER TABLE [dbo].[SUBJECTS] ADD  CONSTRAINT [DF_SUBJECTS_failed_password_attempt_count]  DEFAULT ((0)) FOR [failed_password_attempt_count];
END
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[FORGOT_PASSWORD_REQUESTS]    Script Date: 08/02/2010 12:27:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'FORGOT_PASSWORD_REQUESTS')
BEGIN
	CREATE TABLE [dbo].[FORGOT_PASSWORD_REQUESTS](
		[Id] [uniqueidentifier] NOT NULL,
		[subject_id] [uniqueidentifier] NOT NULL,
		[request_date] [datetime] NOT NULL,
		[is_active] [bit] NOT NULL,
	 CONSTRAINT [PK_FORGOT_PASSWORD_REQUESTS] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY];

	ALTER TABLE [dbo].[FORGOT_PASSWORD_REQUESTS]  WITH CHECK ADD  CONSTRAINT [FK_FORGOT_PASSWORD_REQUESTS_SUBJECTS] FOREIGN KEY([subject_id])
REFERENCES [dbo].[SUBJECTS] ([Id]);

	ALTER TABLE [dbo].[FORGOT_PASSWORD_REQUESTS] CHECK CONSTRAINT [FK_FORGOT_PASSWORD_REQUESTS_SUBJECTS];

	ALTER TABLE [dbo].[FORGOT_PASSWORD_REQUESTS] ADD  CONSTRAINT [DF_FORGOT_PASSWORD_REQUESTS_forgot_password_request_id]  DEFAULT (newsequentialid()) FOR [Id];
END
GO


/****** Object:  Table [dbo].[ERRORS]    Script Date: 07/28/2010 15:06:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ERRORS')
BEGIN
	CREATE TABLE [dbo].[ERRORS](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[Date] [datetime] NOT NULL,
		[Thread] [varchar](255) NOT NULL,
		[Level] [varchar](50) NOT NULL,
		[Logger] [varchar](255) NOT NULL,
		[Message] [varchar](4000) NOT NULL,
		[Exception] [varchar](4000) NULL,
		[StudyID] [int] NULL,
	CONSTRAINT [PK_ERROR] PRIMARY KEY CLUSTERED
	(
		[Date], [Id] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
	) ON [PRIMARY];

	CREATE INDEX idx_study_id ON ERRORS(StudyID);	
END
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[SURVEY_COMPLETE_EVENT]    Script Date: 08/06/2010 16:30:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SURVEY_COMPLETE_EVENT')
BEGIN
	CREATE TABLE [dbo].[SURVEY_COMPLETE_EVENT](
		[ResponseEventID] [bigint] IDENTITY(1,1) NOT NULL,
		[ResponseID] [bigint] NOT NULL,
		[krPT] [varchar](50) NOT NULL,
		[InProcess] [bit] NOT NULL,
		[StatusDate] [datetime] NULL,
		[OpsID] [varchar](50) NULL,
	 CONSTRAINT [PK_RESPONSE_COMPLETE_EVENT] PRIMARY KEY CLUSTERED 
	(
		[ResponseEventID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY];
END
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[SURVEY_EMAIL_SCHEDULE]    Script Date: 04/13/2011 13:23:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SURVEY_EMAIL_SCHEDULE')
BEGIN
	CREATE TABLE [dbo].[SURVEY_EMAIL_SCHEDULE](
		[survey_id] [int] NOT NULL,
		[subject_id] [uniqueidentifier] NOT NULL,
		[schedule_dt_utc] [datetime] NOT NULL,
	CONSTRAINT [PK_SCHEDULE] PRIMARY KEY NONCLUSTERED
	(
		[survey_id], [subject_id] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
	) ON [PRIMARY];

	CREATE UNIQUE CLUSTERED INDEX idx_schedule_dt ON SURVEY_EMAIL_SCHEDULE(schedule_dt_utc, survey_id, subject_id);
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TIME_ZONE_RULE')
BEGIN
	CREATE TABLE [dbo].[TIME_ZONE_RULE](
		[name] [varchar](255) NOT NULL,
		[locationid] [varchar](16) NOT NULL,
		[baseoffset] [int] NOT NULL,
		[dstadjust] [int] NOT NULL,
		[dstendday] [int] NULL,
		[dstenddow] [int] NULL,
		[dstendhour] [int] NULL,
		[dstendmonth] [int] NULL,
		[dststartday] [int] NULL,
		[dststartdow] [int] NULL,
		[dststarthr] [int] NULL,
		[dststartmonth] [int] NULL,
		[currently_dst] [int] NULL,
	CONSTRAINT [PK_NAME] PRIMARY KEY
	(
		[name] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
	) ON [PRIMARY];

	CREATE INDEX idx_offset_tz ON TIME_ZONE_RULE(baseoffset);
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TIME_ZONES')
BEGIN
	CREATE TABLE [dbo].[TIME_ZONES](
		[tz_id] [int] NOT NULL,
		[study_id] [int] NOT NULL,
		[tz_name] [varchar](255) NOT NULL,
	CONSTRAINT [PK_TZID] PRIMARY KEY
	(
		[study_id],[tz_id] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
	) ON [PRIMARY];

	ALTER TABLE [dbo].[TIME_ZONES]  WITH CHECK ADD  CONSTRAINT [FK_TZ_STUDIES] FOREIGN KEY([study_id])
	REFERENCES [dbo].[STUDIES] ([study_id]);

	ALTER TABLE [dbo].[TIME_ZONES]  WITH CHECK ADD  CONSTRAINT [FK_TZ_NAME] FOREIGN KEY([tz_name])
	REFERENCES [dbo].[TIME_ZONE_RULE] ([name]);
END
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'PRODUCT_SURVEY_DATA')
BEGIN
	CREATE TABLE [dbo].[PRODUCT_SURVEY_DATA](
		[id] [uniqueidentifier] NOT NULL,
		[study_id] [int] NOT NULL,
		[krpt] [varchar](36) NOT NULL,
		[krdom] [varchar](256) NOT NULL,
		[site_code] [varchar](256) NULL,
		[krphase] [varchar](16) NULL,
		[pt_enroll_date] [datetime] NULL,
		[su_report_date] [datetime] NOT NULL,
		[su_phase_start_date] [datetime] NOT NULL,
		[su_tz_value] [int] NULL,
		[su_phase_at_entry] [varchar](16) NOT NULL,
		[sigorig] [varchar](256) NULL,
		[sigprev] [varchar](256) NULL,
		[printed_name] [varchar](256) NOT NULL,
		[patient_id] [varchar](256) NULL,
		[survey_start_date] [datetime] NOT NULL,
		[survey_id] [int] NOT NULL,
		[return_url] [varchar](2083) NULL,
		[uid] [varchar](256) NULL,
		[creation_date] [datetime] NOT NULL,
	 CONSTRAINT [PK_SURVEY_DATA] PRIMARY KEY CLUSTERED 
	(
		[id] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY];

	ALTER TABLE [dbo].[PRODUCT_SURVEY_DATA]  WITH CHECK ADD  CONSTRAINT [FK_PSD_STUDIES] FOREIGN KEY([study_id])
	REFERENCES [dbo].[STUDIES] ([study_id]);

	ALTER TABLE [dbo].[PRODUCT_SURVEY_DATA]  WITH CHECK ADD  CONSTRAINT [FK_PSD_SURVEYS] FOREIGN KEY([survey_id])
	REFERENCES [dbo].[STUDY_SURVEYS] ([id]) ON DELETE CASCADE;
END
GO

IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'pin' AND OBJECT_ID = OBJECT_ID(N'SUBJECTS'))
BEGIN
	ALTER TABLE [dbo].[SUBJECTS]
	ADD [pin] bigint NULL
END
GO

DECLARE @revision int;

SET @revision = (SELECT (CASE COUNT(*) WHEN 0 THEN 0 ELSE
	(SELECT TOP 1 ISNULL(revision, 0)
	 FROM [dbo].[VERSIONS]) END) FROM [dbo].[VERSIONS]);

/** Changes for revision 1 **/
IF @revision < 1
BEGIN
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'krsu' AND OBJECT_ID = OBJECT_ID(N'STUDY_SURVEYS'))
		ALTER TABLE [dbo].[STUDY_SURVEYS] ADD [krsu] VARCHAR(36) NULL;
	
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'affidavit' AND OBJECT_ID = OBJECT_ID(N'STUDY_SURVEYS'))
		ALTER TABLE [dbo].[STUDY_SURVEYS] ADD [affidavit] [VARCHAR](36) NULL;

	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'password_expiry_days' AND OBJECT_ID = OBJECT_ID(N'STUDIES'))
		ALTER TABLE [dbo].[STUDIES]	ADD [password_expiry_days] [int] NOT NULL DEFAULT 0;

END

/**Changes for revision 2**/ 
IF @revision < 2
BEGIN
	
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'is_external' AND OBJECT_ID = OBJECT_ID(N'STUDY_SURVEYS'))
	BEGIN
		ALTER TABLE [dbo].[STUDY_SURVEYS]
		ADD [is_external] [bit] NOT NULL Default(0);
	END
	
END

/**Changes for revision 3**/ 
IF @revision < 3
BEGIN
	IF EXISTS (SELECT * FROM SYS.columns WHERE name = N'creation_date' AND OBJECT_ID = OBJECT_ID(N'PRODUCT_SURVEY_DATA'))
	BEGIN
		ALTER TABLE [dbo].[PRODUCT_SURVEY_DATA]
		ALTER COLUMN creation_date [datetime] NULL;
	END

	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'expiry_date' AND OBJECT_ID = OBJECT_ID(N'PRODUCT_SURVEY_DATA'))
	BEGIN
		ALTER TABLE [dbo].[PRODUCT_SURVEY_DATA]
		ADD [expiry_date] [datetime] NULL;
	END
	ELSE
	BEGIN
		ALTER TABLE [dbo].[PRODUCT_SURVEY_DATA]
		ALTER COLUMN [expiry_date] [datetime] NULL;
	END
	
	IF EXISTS (SELECT * FROM SYS.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.FK_PSD_SURVEYS')
		AND parent_object_id = OBJECT_ID(N'dbo.PRODUCT_SURVEY_DATA'))
	BEGIN
		ALTER TABLE [dbo].[PRODUCT_SURVEY_DATA]
			DROP CONSTRAINT FK_PSD_SURVEYS
	END
	ALTER TABLE [dbo].[PRODUCT_SURVEY_DATA]  WITH CHECK ADD  CONSTRAINT [FK_PSD_SURVEYS] FOREIGN KEY([survey_id])
		REFERENCES [dbo].[STUDY_SURVEYS] ([id]) ON DELETE CASCADE;	

	IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'EQ_TIME_ZONES')		
	BEGIN
		CREATE TABLE [dbo].[EQ_TIME_ZONES](
			[eq_time_zone_id] [int] NOT NULL,
			[sw_tz_name] [varchar](255) NULL,
			[daylight_savings] [bit] NOT NULL DEFAULT(0),
		 CONSTRAINT [PK_EQ_TIME_ZONES] PRIMARY KEY CLUSTERED ([eq_time_zone_id] ASC))
	END

	IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'EQ_LANGUAGES')
	BEGIN
		CREATE TABLE [dbo].[EQ_LANGUAGES](
			[eq_language_id] [int] NOT NULL,
			[culture] [varchar](50) NULL,
		 CONSTRAINT [PK_EQ_LANGUAGES] PRIMARY KEY CLUSTERED ([eq_language_id] ASC))
	END
	
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'portal_url' AND OBJECT_ID = OBJECT_ID(N'VERSIONS'))
	BEGIN
		ALTER TABLE [dbo].[VERSIONS]
		ADD [portal_url] [varchar] (255) NULL		
	END
	
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'service_url' AND OBJECT_ID = OBJECT_ID(N'VERSIONS'))
	BEGIN
		ALTER TABLE [dbo].[VERSIONS]
		ADD [service_url] [varchar] (255) NULL
	END
END

/**Changes for Revision 4**/
IF @revision < 4
BEGIN
	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetUniquePin]') AND type in (N'P', N'PC'))
	BEGIN
	EXEC dbo.sp_executesql @statement = N'-- Verify that the pin is unique in the database, and if not, change it randomly until it is
		CREATE PROCEDURE [dbo].[GetUniquePin]
			@Pin bigint
		AS
		WHILE (EXISTS (SELECT Pin FROM Subjects WHERE Pin = @Pin))
			SET @Pin = (SELECT @Pin + ROUND((100 * RAND()), 0))
		SELECT @Pin' 
	END


END

/**Changes for Revision 5**/
IF @revision < 5
BEGIN
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'security_question' AND OBJECT_ID = OBJECT_ID(N'SUBJECTS'))
	BEGIN
		ALTER TABLE [dbo].[SUBJECTS]
		ADD [security_question] int NULL
	END
	
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'security_answer' AND OBJECT_ID = OBJECT_ID(N'SUBJECTS'))
	BEGIN
		ALTER TABLE [dbo].[SUBJECTS]
		ADD [security_answer] nvarchar(512) NULL
	END
END

/**Changes for Revision 6**/
IF @revision < 6
BEGIN
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'micro_version' AND OBJECT_ID = OBJECT_ID(N'VERSIONS'))
	BEGIN
		ALTER TABLE [dbo].[VERSIONS]
		ADD [micro_version] int NOT NULL Default(0);
	END
END

/**Changes for Revision 7**/
IF @revision < 7
BEGIN
	IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ACTION_QUEUE')
	BEGIN
	CREATE TABLE [dbo].[ACTION_QUEUE](
		[action_queue_id] [bigint] IDENTITY(1,1) NOT NULL,
		[method] [varchar](50) NULL,
		[data] [nvarchar](max) NULL,
		[rpc_id] [int] NOT NULL,
		[subject_id] [uniqueidentifier] NULL,
		[queue_timestamp] [datetime] NULL,
		[action_state] [int] NOT NULL DEFAULT(1),
		[client_request_time] [datetime] NULL,
		[server_receipt_time] [datetime] NULL,
	 CONSTRAINT [PK_ACTION_QUEUE] PRIMARY KEY CLUSTERED 
	(
		[action_queue_id] ASC
	))
	END
	
	IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'RESPONSE_QUEUE')
	BEGIN
	CREATE TABLE [dbo].[RESPONSE_QUEUE](
		[response_queue_id] [bigint] IDENTITY(1,1) NOT NULL,
		[message] [nvarchar](max) NULL,
		[subject_id] [uniqueidentifier] NULL,
		[queue_timestamp] [datetime] NULL,
		[rpc_id] [int] NOT NULL,
	 CONSTRAINT [PK_RESPONSE_QUEUE] PRIMARY KEY CLUSTERED 
	(
		[response_queue_id] ASC	
	))
	END
	
	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ACTION_QUEUE_SUBJECTS]') AND parent_object_id = OBJECT_ID(N'[dbo].[ACTION_QUEUE]'))
	ALTER TABLE [dbo].[ACTION_QUEUE]  WITH CHECK ADD  CONSTRAINT [FK_ACTION_QUEUE_SUBJECTS] FOREIGN KEY([subject_id])
	REFERENCES [dbo].[SUBJECTS] ([Id])
	
	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RESPONSE_QUEUE_SUBJECTS]') AND parent_object_id = OBJECT_ID(N'[dbo].[RESPONSE_QUEUE]'))
	ALTER TABLE [dbo].[RESPONSE_QUEUE]  WITH CHECK ADD  CONSTRAINT [FK_RESPONSE_QUEUE_SUBJECTS] FOREIGN KEY([subject_id])
	REFERENCES [dbo].[SUBJECTS] ([Id])
END

/**Changes for Revision 8**/
IF @revision < 8
BEGIN
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'survey_id' AND OBJECT_ID = OBJECT_ID(N'STUDY_SURVEYS'))
	BEGIN
		ALTER TABLE [dbo].[STUDY_SURVEYS]
		ADD [survey_id] int NULL
	END
END

/**Changes for Revision 9**/
IF @revision < 9
BEGIN

	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'sw_patient_id' AND OBJECT_ID = OBJECT_ID(N'SUBJECTS'))
	BEGIN
		ALTER TABLE [dbo].[SUBJECTS]
		ADD [sw_patient_id] nvarchar(50) NULL;
	END
	
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'sw_patient_initials' AND OBJECT_ID = OBJECT_ID(N'SUBJECTS'))
	BEGIN
		ALTER TABLE [dbo].[SUBJECTS]
		ADD [sw_patient_initials] nvarchar(50) NULL;
	END
	
	IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'LANGUAGE_HASH')
	BEGIN
		CREATE TABLE [dbo].[LANGUAGE_HASH](
		[study_id] int REFERENCES [dbo].[STUDIES] ([study_id])  NOT NULL,
		[language] [char](5) NOT NULL,
		[hash] nvarchar(32) NOT NULL,
		CONSTRAINT PK_LANGUAGE_HASH PRIMARY KEY (study_id, [language])
		);
	END
	
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'lf_hash' AND OBJECT_ID = OBJECT_ID(N'STUDY_VERSIONS'))
	BEGIN
		ALTER TABLE [dbo].[STUDY_VERSIONS]
		ADD [lf_hash] nvarchar(32) NULL
	END
END

/**Changes for Revision 10**/
IF @revision < 10
BEGIN
	-- Create the DIARY_QUEUE
	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DIARY_QUEUE]') AND type in (N'U'))
	BEGIN
	CREATE TABLE [dbo].[DIARY_QUEUE](
		[diary_queue_id] [bigint] IDENTITY(1,1) NOT NULL,
		[diary_state] [int] NOT NULL,
		[queue_date] [datetime] NOT NULL,
		[activity_date] [datetime] NOT NULL,
		[subject_id] [uniqueidentifier] NOT NULL,
		[browser_platform] [nvarchar](50) NULL,
		[browser_type] [nvarchar](50) NULL,
		[browser_version] [nvarchar](50) NULL,
		[su] [nvarchar](50) NULL,
		[started] [datetime] NOT NULL,
		[completed] [datetime] NOT NULL,
		[completed_tz_offset] [decimal](18, 2) NOT NULL,
		[completed_epoch_t] [bigint] NOT NULL,
		[start_phase] [nvarchar](10) NULL,
		[end_phase] [nvarchar](10) NULL,
	 CONSTRAINT [PK_DIARY_QUEUE] PRIMARY KEY CLUSTERED 
	(
		[diary_queue_id] ASC
	))
	END

	-- Create the DIARY_QUEUE_ANSWERS
	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DIARY_QUEUE_ANSWERS]') AND type in (N'U'))
	BEGIN
	CREATE TABLE [dbo].[DIARY_QUEUE_ANSWERS](
		[diary_queue_answer_id] [bigint] IDENTITY(1,1) NOT NULL,
		[diary_queue_id] [bigint] NOT NULL,
		[data_changed] [bit] NOT NULL DEFAULT(0),
		[response] [nvarchar](300) NULL,
		[sw_alias] [nvarchar](100) NULL,
		[instance_ordinal] [int] NULL,
		[question_id] [nvarchar](100) NULL,
	 CONSTRAINT [PK_DIARY_QUEUE_ANSWERS] PRIMARY KEY CLUSTERED 
	(
		[diary_queue_answer_id] ASC
	)) 
	END

	-- Create the Foreign Key Contraints for DIARY_QUEUE and DIARY_QUEUE_ANSWERS
	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DIARY_QUEUE_SUBJECTS]') AND parent_object_id = OBJECT_ID(N'[dbo].[DIARY_QUEUE]'))
	ALTER TABLE [dbo].[DIARY_QUEUE]  WITH CHECK ADD  CONSTRAINT [FK_DIARY_QUEUE_SUBJECTS] FOREIGN KEY([subject_id])
	REFERENCES [dbo].[SUBJECTS] ([Id])

	IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DIARY_QUEUE_SUBJECTS]') AND parent_object_id = OBJECT_ID(N'[dbo].[DIARY_QUEUE]'))
	ALTER TABLE [dbo].[DIARY_QUEUE] CHECK CONSTRAINT [FK_DIARY_QUEUE_SUBJECTS]

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DIARY_QUEUE_ANSWERS_DIARY_QUEUE]') AND parent_object_id = OBJECT_ID(N'[dbo].[DIARY_QUEUE_ANSWERS]'))
	ALTER TABLE [dbo].[DIARY_QUEUE_ANSWERS]  WITH CHECK ADD  CONSTRAINT [FK_DIARY_QUEUE_ANSWERS_DIARY_QUEUE] FOREIGN KEY([diary_queue_id])
	REFERENCES [dbo].[DIARY_QUEUE] ([diary_queue_id])

	IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DIARY_QUEUE_ANSWERS_DIARY_QUEUE]') AND parent_object_id = OBJECT_ID(N'[dbo].[DIARY_QUEUE_ANSWERS]'))
	ALTER TABLE [dbo].[DIARY_QUEUE_ANSWERS] CHECK CONSTRAINT [FK_DIARY_QUEUE_ANSWERS_DIARY_QUEUE]
END

-- Changes for revision 11
IF @revision < 11
BEGIN
	IF EXISTS (SELECT * FROM SYS.columns WHERE name = N'start_phase' AND OBJECT_ID = OBJECT_ID(N'DIARY_QUEUE'))
	ALTER TABLE [DIARY_QUEUE] DROP COLUMN [start_phase]
	
	IF EXISTS (SELECT * FROM SYS.columns WHERE name = N'end_phase' AND OBJECT_ID = OBJECT_ID(N'DIARY_QUEUE'))
	EXEC SP_RENAME 'DIARY_QUEUE.end_phase', 'phase', 'COLUMN' 

	IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = N'phase' AND OBJECT_ID = OBJECT_ID(N'SUBJECTS'))
	ALTER TABLE [SUBJECTS] ADD [phase] INT NULL 
	
	IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = N'site_code' AND OBJECT_ID = OBJECT_ID(N'SUBJECTS'))
	ALTER TABLE [SUBJECTS] ADD [site_code] NVARCHAR(50) NULL
END

-- Change for revision 12
IF @revision < 12
BEGIN
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'change_phase' AND OBJECT_ID = OBJECT_ID(N'DIARY_QUEUE'))
	ALTER TABLE [DIARY_QUEUE] ADD [change_phase] BIT NOT NULL DEFAULT(0)
END

-- Change for revision 13
IF @revision < 13
BEGIN
	-- Device Id support
	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SUBJECT_DEVICES]') AND type in (N'U'))
	BEGIN
	CREATE TABLE [dbo].[SUBJECT_DEVICES](
	[subject_device_id] [bigint] IDENTITY(1,1) NOT NULL,
	[subject_id] [uniqueidentifier] NOT NULL,
	[device_code] [uniqueidentifier] NOT NULL,
	[activation_date] [datetime] NOT NULL,
	CONSTRAINT [PK_SUBJECT_DEVICES] PRIMARY KEY CLUSTERED 
	(
		[subject_device_id] ASC
	))

	ALTER TABLE [dbo].[SUBJECT_DEVICES]  WITH CHECK ADD  CONSTRAINT [FK_SUBJECT_DEVICES_SUBJECTS] FOREIGN KEY([subject_id])
	REFERENCES [dbo].[SUBJECTS] ([Id])

	ALTER TABLE [dbo].[SUBJECT_DEVICES] CHECK CONSTRAINT [FK_SUBJECT_DEVICES_SUBJECTS]
	
	CREATE NONCLUSTERED INDEX [IX_SUBJECT_DEVICES_device_code] ON [dbo].[SUBJECT_DEVICES] ( [device_code] ASC )
	
	END
	
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'datetime_submitted' AND OBJECT_ID = OBJECT_ID(N'DIARY_QUEUE'))
	ALTER TABLE [DIARY_QUEUE] ADD [datetime_submitted] DATETIME NULL
	
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'datetime_received' AND OBJECT_ID = OBJECT_ID(N'DIARY_QUEUE'))
	ALTER TABLE [DIARY_QUEUE] ADD [datetime_received] DATETIME NULL
	
	-- Logging Service support
	IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'LOG_QUEUE')
	BEGIN
		CREATE TABLE [dbo].[LOG_QUEUE](
			[Id] [int] IDENTITY(1,1) NOT NULL,
			[subject_id] [uniqueidentifier] NOT NULL,
			[sw_id] [int] NULL,
			[client_sent_date] [datetime] NULL,
			[queue_date] [datetime] NOT NULL,
			[sent] [bit] NOT NULL,
			[message] [varchar](4000) NULL,
			[level] [varchar](50) NULL,
			[client_time] [datetime] NOT NULL,
			[source] [varchar](255) NOT NULL,
			CONSTRAINT [PK_LOG_QUEUE] PRIMARY KEY CLUSTERED 
			(
				[Id] ASC
			)
		)

		ALTER TABLE [dbo].[LOG_QUEUE]  WITH CHECK ADD  CONSTRAINT [FK_LOG_QUEUE_SUBJECTS] FOREIGN KEY([subject_id])
		REFERENCES [dbo].[SUBJECTS] ([Id])
	END
	
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'sw_ng_base_url' AND OBJECT_ID = OBJECT_ID(N'STUDIES'))
	BEGIN
		ALTER TABLE [dbo].[STUDIES]
		ADD [sw_ng_base_url] [varchar](255) NULL
	END
END

-- Change for revision 14
IF @revision < 14
BEGIN
	IF EXISTS (SELECT * FROM SYS.columns WHERE name = N'client_time' AND OBJECT_ID = OBJECT_ID(N'LOG_QUEUE'))
	BEGIN
		ALTER TABLE [dbo].[LOG_QUEUE]
		ALTER COLUMN client_time [varchar](50) NULL;
	END
END

-- Change for revision 15
IF @revision < 15
BEGIN
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'non_pii' AND OBJECT_ID = OBJECT_ID(N'STUDIES'))
	BEGIN
		ALTER TABLE [dbo].[STUDIES]
		ADD non_pii bit NOT NULL DEFAULT(0);
	END
END

-- Change for revision 16
IF @revision < 16
BEGIN
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'client_core_version' AND OBJECT_ID = OBJECT_ID(N'DIARY_QUEUE'))
		ALTER TABLE [dbo].[DIARY_QUEUE]	ADD client_core_version [varchar] (36) NOT NULL Default('');
	
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'client_study_version' AND OBJECT_ID = OBJECT_ID(N'DIARY_QUEUE'))
		ALTER TABLE [dbo].[DIARY_QUEUE]	ADD client_study_version [varchar] (36) NOT NULL Default('');
	
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'device_id' AND OBJECT_ID = OBJECT_ID(N'DIARY_QUEUE'))
		ALTER TABLE [dbo].[DIARY_QUEUE] ADD device_id [varchar] (40) NOT NULL Default('');
END

-- Change for revision 17
IF @revision < 17
BEGIN	
	IF(NOT EXISTS(SELECT * FROM sys.triggers where name = 'SubjectPinTrigger'))
	BEGIN
		EXEC dbo.sp_executesql @statement = N'
		CREATE TRIGGER SubjectPinTrigger 
		   ON  SUBJECTS 
		   AFTER INSERT
		AS 
		DECLARE @Pin INT;
		DECLARE @Upper INT;
		DECLARE @Lower INT

		---- This will create a random number between 10000000 and 99999999
		SET @Lower = 10000000; ---- The lowest random number
		SET @Upper = 99999999; ---- The highest random number
		SET @Pin = ROUND(((@Upper - @Lower -1) * RAND() + @Lower), 0);

		---- Check if generated number already exists
		WHILE (EXISTS (SELECT * FROM SUBJECTS WHERE pin = @Pin))
			SET @Pin = ROUND(((@Upper - @Lower -1) * RAND() + @Lower), 0);
			
		DECLARE @InsertedId uniqueidentifier;
		SELECT @InsertedId = Id FROM inserted;
		UPDATE SUBJECTS SET pin = @Pin WHERE Id = @InsertedId;'
	END
	
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'activation_url' AND OBJECT_ID = OBJECT_ID(N'SURVEY_EMAIL_SCHEDULE'))
	BEGIN
		ALTER TABLE [dbo].[SURVEY_EMAIL_SCHEDULE]
		ADD [activation_url] varchar(255) NULL
	END
END

-- Change for revision 18
IF @revision < 18
BEGIN
	IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'BATCH_LOG_QUEUE')
	BEGIN
		CREATE TABLE [dbo].[BATCH_LOG_QUEUE](
			[batch_log_id] [int] IDENTITY(1,1) NOT NULL,
			[subject_id] [uniqueidentifier] NOT NULL,
			[client_sent_date] [datetime] NOT NULL,
			[queue_date] [datetime] NOT NULL,
			[queue_state] [int] NOT NULL,
			[json] varchar(MAX) NOT NULL
			CONSTRAINT [PK_BATCH_LOG_QUEUE] PRIMARY KEY CLUSTERED 
			(
				[batch_log_id] ASC
			)
		)

		ALTER TABLE [dbo].[BATCH_LOG_QUEUE]  WITH CHECK ADD  CONSTRAINT [FK_BATCH_LOG_QUEUE_SUBJECTS] FOREIGN KEY([subject_id])
		REFERENCES [dbo].[SUBJECTS] ([Id])
	END
	
	IF(EXISTS(SELECT * FROM sys.objects where name = 'Pending_Diaries' and [type] = 'V'))
		drop view Pending_Diaries
	
		EXEC dbo.sp_executesql @statement = N'
		CREATE VIEW [dbo].[Pending_Diaries]
AS
SELECT     s.study_id, dq.diary_queue_id
FROM       dbo.DIARY_QUEUE AS dq INNER JOIN
                      dbo.SUBJECTS AS s ON dq.subject_id = s.Id
WHERE     (dq.diary_state = 3) OR (dq.diary_state = 4)';
END

-- Change for revision 19
IF @revision < 19
BEGIN
	--Create table NOTIFICATION_HISTORY 
	IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'NOTIFICATION_HISTORY')
	BEGIN
		CREATE TABLE [dbo].[NOTIFICATION_HISTORY](
			[history_id] [bigint] IDENTITY(1,1) NOT NULL,
			[survey_id] [int] NOT NULL,
			[subject_id] [uniqueidentifier] NOT NULL,
			[message_type] [varchar](50) NULL,
			[notification_created_date_utc] [datetime] NOT NULL,
			[notification_scheduled_date_utc] [datetime] NOT NULL,
			[operation] [char](1) NOT NULL,
			[history_created_date_utc] [datetime] NOT NULL,
		 CONSTRAINT [PK_NOTIFICATION_HISTORY] PRIMARY KEY CLUSTERED 
		(
			[history_id] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]

		ALTER TABLE [dbo].[NOTIFICATION_HISTORY] ADD  CONSTRAINT [DF_NOTIFICATION_HISTORY_history_created_time_utc]  DEFAULT (getutcdate()) FOR [history_created_date_utc]
	END
	
	--Add columns to STUDIES table
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'notification_expiry_days' AND OBJECT_ID = OBJECT_ID(N'STUDIES'))
	BEGIN
		ALTER TABLE [dbo].[STUDIES] ADD [notification_expiry_days] [int] NOT NULL DEFAULT ((0))
	END

	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'wd_last_disabled_date_utc' AND OBJECT_ID = OBJECT_ID(N'STUDIES'))
	BEGIN
		ALTER TABLE [dbo].[STUDIES] ADD [wd_last_disabled_date_utc] [datetime] NULL
	END
	
	--Add a column to SURVEY_EMAIL_SCHEDULE 
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'created_date_utc' AND OBJECT_ID = OBJECT_ID(N'SURVEY_EMAIL_SCHEDULE'))
	BEGIN
		ALTER TABLE [dbo].[SURVEY_EMAIL_SCHEDULE] ADD [created_date_utc] [datetime] NOT NULL DEFAULT (getutcdate())
	END
	
    --Create a trigger SetWDLastDisabledDate for STUDIES table
    IF EXISTS(SELECT * FROM sys.triggers where name = 'SetWDLastDisabledDate')
	BEGIN
		DROP TRIGGER [dbo].[SetWDLastDisabledDate]
	END
	EXEC dbo.sp_executesql @statement = N'CREATE TRIGGER [dbo].[SetWDLastDisabledDate]
	ON [dbo].[STUDIES]
	AFTER UPDATE
	AS
	IF (UPDATE(wd_enabled))
	BEGIN
		DECLARE @StudyId int
		DECLARE @NewWDEnabled bit
		DECLARE @DisabledDate datetime
		
		SELECT @DisabledDate = GETUTCDATE()
		SELECT @StudyId = study_id, @NewWDEnabled = wd_enabled FROM inserted
		
		IF (@NewWDEnabled = 0)
		BEGIN
			UPDATE STUDIES 
			SET wd_last_disabled_date_utc = @DisabledDate 
			WHERE study_id = @StudyId
		END
	END'
	
	--Create a procedure DeleteExpiredNotifications
	IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteExpiredNotifications]') AND type in (N'P', N'PC'))
	BEGIN
		DROP PROCEDURE [dbo].[DeleteExpiredNotifications]
	END
	EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[DeleteExpiredNotifications]
	AS
   
            SET NOCOUNT ON
            SET DEADLOCK_PRIORITY LOW 

            CREATE TABLE #tblExpiredNotifications
            (
				survey_id int NOT NULL, 
				subject_id uniqueidentifier NOT NULL,
				scheduled_date_utc datetime NOT NULL,
				created_date_utc datetime NOT NULL,
				message_type varchar(50) NULL				  
            )
            
            DECLARE @Now datetime
			SELECT @Now = GETUTCDATE() 

            INSERT #tblExpiredNotifications(survey_id, subject_id, scheduled_date_utc, created_date_utc, message_type)
			SELECT toDelete.survey_id, toDelete.subject_id, toDelete.schedule_dt_utc, toDelete.created_date_utc, 
			CASE toDelete.survey_id
			WHEN -1 THEN ''Activation''
			WHEN -2 THEN ''Email Update''
			WHEN -3 THEN ''Language Update''
			WHEN -4 THEN ''Email Language Update''			
			WHEN -5 THEN ''Confirmation''
			WHEN -6 THEN ''Reset Password''
			ELSE ss.display_name
			END
			AS message_type						 
			FROM (SELECT ses.survey_id, ses.subject_id, ses.schedule_dt_utc, ses.created_date_utc 
			FROM dbo.SURVEY_EMAIL_SCHEDULE ses INNER JOIN dbo.SUBJECTS sub ON ses.subject_id = sub.Id
			INNER JOIN dbo.STUDIES s ON s.study_id = sub.study_id
			WHERE s.wd_enabled = 0
			AND s.notification_expiry_days <> 0
			AND (DATEADD(day,s.notification_expiry_days,s.wd_last_disabled_date_utc) < @Now)
			AND s.wd_last_disabled_date_utc IS NOT NULL) AS toDelete
			LEFT JOIN dbo.STUDY_SURVEYS AS ss
			ON toDelete.survey_id = ss.id

            IF @@ROWCOUNT <> 0 
            BEGIN
                DECLARE ExpiredNotificationCursor CURSOR LOCAL FORWARD_ONLY READ_ONLY
                FOR SELECT survey_id, subject_id, scheduled_date_utc, created_date_utc, message_type FROM #tblExpiredNotifications

                DECLARE @SurveyId int, @SubjectId uniqueidentifier
                DECLARE @ScheduledDateUtc datetime, @CreatedDateUtc datetime, @MessageType varchar(50)

                OPEN ExpiredNotificationCursor

                FETCH NEXT FROM ExpiredNotificationCursor INTO @SurveyId, @SubjectId, @ScheduledDateUtc, @CreatedDateUtc, @MessageType  

                WHILE @@FETCH_STATUS = 0 
                    BEGIN
					    --Delete a notification
					    DELETE FROM dbo.SURVEY_EMAIL_SCHEDULE WHERE survey_id = @SurveyId AND subject_id = @SubjectId
					    --Insert into NOTIFICATION_HISTORY table
					    --Operation d is for delete operation 
					    INSERT INTO dbo.NOTIFICATION_HISTORY (survey_id,subject_id,message_type,notification_created_date_utc,notification_scheduled_date_utc,operation)
					    VALUES (@SurveyId, @SubjectId, @MessageType, @CreatedDateUtc, @ScheduledDateUtc, ''d'')
                       
                        FETCH NEXT FROM ExpiredNotificationCursor INTO @SurveyId, @SubjectId, @ScheduledDateUtc, @CreatedDateUtc, @MessageType  
                    END

                CLOSE ExpiredNotificationCursor

                DEALLOCATE ExpiredNotificationCursor

            END
            
            DROP TABLE #tblExpiredNotifications
            
            RETURN 0'
            
END

--Create Job_DeleteExpiredNotifications 
IF @revision < 19
BEGIN
	/* Drop Job_DeleteExpiredNotifications */
	DECLARE @jobname nvarchar(200)
	SET @jobname = N'Job_DeleteExpiredNotifications'
	-- Delete the [local] job 
	DECLARE @LocalJobId binary(16)
	SELECT @LocalJobId = job_id FROM msdb.dbo.sysjobs WHERE (name = @jobname)
	IF (@LocalJobId IS NOT NULL)
	BEGIN
		EXEC msdb.dbo.sp_delete_job @LocalJobId
	END
	
	/* Add job */
	BEGIN TRANSACTION
	DECLARE @ReturnCode INT
	SELECT @ReturnCode = 0

	IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
	BEGIN
	EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

	END

	DECLARE @jobId BINARY(16)
	EXEC @ReturnCode =  msdb.dbo.sp_add_job 
			@job_name=N'Job_DeleteExpiredNotifications', 
			@enabled=1, 
			@notify_level_eventlog=0, 
			@notify_level_email=0, 
			@notify_level_netsend=0, 
			@notify_level_page=0, 
			@delete_level=0, 
			@description=N'Delete expired notifications from SURVEY_EMAIL_SCHEDULE table and log deleted notifications in NOTIFICATION_HISTORY table', 
			@category_name=N'[Uncategorized (Local)]', 
			@owner_login_name=NULL, 
			@job_id = @jobId OUTPUT
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

	--Add job step
	EXEC @ReturnCode = msdb.dbo.sp_add_jobstep
			@job_id=@jobId,
			@step_name=N'JobStep_DeleteExpiredNotifications', 
			@step_id=1, 
			@cmdexec_success_code=0, 
			@on_success_action=1, 
			@on_success_step_id=0, 
			@on_fail_action=2, 
			@on_fail_step_id=0, 
			@retry_attempts=0, 
			@retry_interval=0, 
			@os_run_priority=0, @subsystem=N'TSQL', 
			@command=N'EXECUTE DeleteExpiredNotifications ', 
			@database_name=N'WebDiary', 
			@flags=0
			
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

	EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

	--Add job schedule
	EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule
			@job_id=@jobId,
			@name=N'JobSchedule_DeleteExpiredNotifications', 
			@enabled=1, 
			@freq_type=4, 
			@freq_interval=1, 
			@freq_subday_type=1, 
			@freq_subday_interval=0, 
			@freq_relative_interval=0, 
			@freq_recurrence_factor=0, 
			@active_start_date=20121204, 
			@active_end_date=99991231, 
			@active_start_time=0, 
			@active_end_time=235959 
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

	EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

	COMMIT TRANSACTION
	GOTO EndSave
	QuitWithRollback:
		IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
	EndSave:
END

-- Change for revision 20
IF @revision < 20
BEGIN
	IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'LOG_LEVEL')
	BEGIN
		CREATE TABLE [dbo].[LOG_LEVEL](
		[level_id] [tinyint] NOT NULL,
		[log_level] [varchar](5) NOT NULL,
		[description] [varchar](30) NULL,
		 CONSTRAINT [PK_LOG_LEVEL] PRIMARY KEY CLUSTERED 
		(
			[level_id] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]
	END
	
	IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'STUDY_MIDDLE_TIER')
	BEGIN
		CREATE TABLE [dbo].[STUDY_MIDDLE_TIER](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[study_id] [int] NOT NULL,
		[default_log_level] [tinyint] NOT NULL,
		 CONSTRAINT [PK_STUDY_MIDDLE_TIER] PRIMARY KEY CLUSTERED 
		(
			[Id] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
		 CONSTRAINT [UNQ_STUDY_MIDDLE_TIER] UNIQUE NONCLUSTERED 
		(
			[study_id] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]

		ALTER TABLE [dbo].[STUDY_MIDDLE_TIER]  WITH CHECK ADD  CONSTRAINT [FK_STUDY_MIDDLE_TIER_LOG_LEVEL] FOREIGN KEY([default_log_level])
		REFERENCES [dbo].[LOG_LEVEL] ([level_id])

		ALTER TABLE [dbo].[STUDY_MIDDLE_TIER] CHECK CONSTRAINT [FK_STUDY_MIDDLE_TIER_LOG_LEVEL]

		ALTER TABLE [dbo].[STUDY_MIDDLE_TIER]  WITH NOCHECK ADD  CONSTRAINT [FK_STUDY_MIDDLE_TIER_STUDIES] FOREIGN KEY([study_id])
		REFERENCES [dbo].[STUDIES] ([study_id])

		ALTER TABLE [dbo].[STUDY_MIDDLE_TIER] CHECK CONSTRAINT [FK_STUDY_MIDDLE_TIER_STUDIES]
	END
	
	IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SUBJECT_MIDDLE_TIER')
	BEGIN
	
		CREATE TABLE [dbo].[SUBJECT_MIDDLE_TIER](
		[Id] [bigint] IDENTITY(1,1) NOT NULL,
		[subject_id] [uniqueidentifier] NOT NULL,
		[log_level] [tinyint] NOT NULL,
		[client_log_level] [tinyint] NULL,
		 CONSTRAINT [PK_SUBJECT_MIDDLE_TIER] PRIMARY KEY CLUSTERED 
		(
			[Id] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
		 CONSTRAINT [UNQ_SUBJECT_MIDDLE_TIER] UNIQUE NONCLUSTERED 
		(
			[subject_id] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]

		ALTER TABLE [dbo].[SUBJECT_MIDDLE_TIER]  WITH NOCHECK ADD  CONSTRAINT [FK_SUBJECT_MIDDLE_TIER_LOG_LEVEL] FOREIGN KEY([log_level])
		REFERENCES [dbo].[LOG_LEVEL] ([level_id])

		ALTER TABLE [dbo].[SUBJECT_MIDDLE_TIER] CHECK CONSTRAINT [FK_SUBJECT_MIDDLE_TIER_LOG_LEVEL]

		ALTER TABLE [dbo].[SUBJECT_MIDDLE_TIER]  WITH CHECK ADD  CONSTRAINT [FK_SUBJECT_MIDDLE_TIER_LOG_LEVEL_CLIENT] FOREIGN KEY([client_log_level])
		REFERENCES [dbo].[LOG_LEVEL] ([level_id])

		ALTER TABLE [dbo].[SUBJECT_MIDDLE_TIER] CHECK CONSTRAINT [FK_SUBJECT_MIDDLE_TIER_LOG_LEVEL_CLIENT]

		ALTER TABLE [dbo].[SUBJECT_MIDDLE_TIER]  WITH NOCHECK ADD  CONSTRAINT [FK_SUBJECT_MIDDLE_TIER_SUBJECTS] FOREIGN KEY([subject_id])
		REFERENCES [dbo].[SUBJECTS] ([Id])

		ALTER TABLE [dbo].[SUBJECT_MIDDLE_TIER] CHECK CONSTRAINT [FK_SUBJECT_MIDDLE_TIER_SUBJECTS]
	END
	
	-- Prepopulate LOG_LEVEL table with log_levels 
	IF NOT EXISTS (SELECT * FROM [dbo].[LOG_LEVEL] WHERE [level_id] = 1 AND [log_level] = 'DEBUG')
	BEGIN
		INSERT INTO [dbo].[LOG_LEVEL] ([level_id], [log_level], [description])
		VALUES(1, 'DEBUG', 'Debug')
	END

	IF NOT EXISTS (SELECT * FROM [dbo].[LOG_LEVEL] WHERE [level_id] = 2 AND [log_level] = 'INFO')
	BEGIN
		INSERT INTO [dbo].[LOG_LEVEL] ([level_id], [log_level], [description])
		VALUES(2, 'INFO', 'Information')
	END

	IF NOT EXISTS (SELECT * FROM [dbo].[LOG_LEVEL] WHERE [level_id] = 3 AND [log_level] = 'WARN')
	BEGIN
		INSERT INTO [dbo].[LOG_LEVEL] ([level_id], [log_level], [description])
		VALUES(3, 'WARN', 'Warning')
	END   

	IF NOT EXISTS (SELECT * FROM [dbo].[LOG_LEVEL] WHERE [level_id] = 4 AND [log_level] = 'ERROR')
	BEGIN
		INSERT INTO [dbo].[LOG_LEVEL] ([level_id], [log_level], [description])
		VALUES(4, 'ERROR', 'Error')
	END	   

	IF NOT EXISTS (SELECT * FROM [dbo].[LOG_LEVEL] WHERE [level_id] = 5 AND [log_level] = 'FATAL')
	BEGIN
		INSERT INTO [dbo].[LOG_LEVEL] ([level_id], [log_level], [description])
		VALUES(5, 'FATAL', 'Fatal')
	END 
	
	--Create a trigger SetSubjectLogLevel for SUBJECTS table
    IF EXISTS(SELECT * FROM sys.triggers where name = 'SetSubjectLogLevel')
	BEGIN
		DROP TRIGGER [dbo].[SetSubjectLogLevel]
	END
	EXEC dbo.sp_executesql @statement = N'CREATE TRIGGER [dbo].[SetSubjectLogLevel]
      ON [dbo].[SUBJECTS]
      AFTER INSERT
      AS
      BEGIN
            
            SET NOCOUNT ON
            
            DECLARE @StudyId int;
            DECLARE @SubjectId uniqueidentifier;
            DECLARE @DefaultStudyLogLevel tinyint;
            
            SELECT @StudyId = study_id, @SubjectId = Id FROM inserted;  
            
            IF EXISTS (SELECT * FROM [dbo].[STUDY_MIDDLE_TIER] WHERE study_id = @StudyId)
            BEGIN
				SELECT @DefaultStudyLogLevel = default_log_level FROM [dbo].[STUDY_MIDDLE_TIER] WHERE study_id = @StudyId;
				INSERT INTO [dbo].[SUBJECT_MIDDLE_TIER] (subject_id, log_level, client_log_level)
				VALUES (@SubjectId, @DefaultStudyLogLevel, NULL)
            END
      END'

	--create user-defined table type SubjectLogLevelTableType
    IF NOT EXISTS(SELECT * FROM sys.table_types where name = 'SubjectLogLevelTableType')
	BEGIN
		EXEC dbo.sp_executesql @statement = N'CREATE TYPE [dbo].[SubjectLogLevelTableType] AS TABLE(
			[Id] [bigint] NOT NULL,
			[log_level] [tinyint] NOT NULL,
			PRIMARY KEY CLUSTERED 
			(
				[Id] ASC
			)WITH (IGNORE_DUP_KEY = OFF)
			)'
	END
	
	--Create a procedure UpdateSubjectLogLevel 
	IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateSubjectLogLevel]') AND type in (N'P', N'PC'))
	BEGIN
		DROP PROCEDURE [dbo].[UpdateSubjectLogLevel]
	END
	EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UpdateSubjectLogLevel]

	@tvpUpdatedSubLogLevel [dbo].[SubjectLogLevelTableType] READONLY

	AS

	BEGIN
		UPDATE [dbo].[SUBJECT_MIDDLE_TIER]
		SET [dbo].[SUBJECT_MIDDLE_TIER].log_level = usl.log_level
		FROM [dbo].[SUBJECT_MIDDLE_TIER] INNER JOIN @tvpUpdatedSubLogLevel AS usl
		ON [dbo].[SUBJECT_MIDDLE_TIER].Id = usl.Id;
	END'
	
	--Create a procedure GetLogLevel 
	IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetLogLevel]') AND type in (N'P', N'PC'))
	BEGIN
		DROP PROCEDURE [dbo].[GetLogLevel]
	END
	EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GetLogLevel]
	AS
	BEGIN
		SET NOCOUNT ON
		SELECT level_id, log_level FROM [dbo].[LOG_LEVEL] ORDER BY log_level
	END'
	
	--Create a procedure GetStudyListWithLogLevel 
	IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetStudyListWithLogLevel]') AND type in (N'P', N'PC'))
	BEGIN
		DROP PROCEDURE [dbo].[GetStudyListWithLogLevel]
	END
	EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GetStudyListWithLogLevel]
	AS
	BEGIN
		SET NOCOUNT ON	
		SELECT smt.study_id, s.study_name FROM [dbo].[STUDY_MIDDLE_TIER] smt INNER JOIN dbo.STUDIES s ON smt.study_id = s.study_id ORDER BY s.study_name
	END'
	
	--Create a procedure GetSubjectLogLevelByStudyId 
	IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetSubjectLogLevelByStudyId]') AND type in (N'P', N'PC'))
	BEGIN
		DROP PROCEDURE [dbo].[GetSubjectLogLevelByStudyId]
	END
	EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GetSubjectLogLevelByStudyId]

	@StudyId int

	AS
	BEGIN

		SET NOCOUNT ON

		SELECT smt.Id, selectedSubject.sw_patient_id, l.log_level
		FROM 
		(SELECT sub.Id, sub.sw_patient_id
		FROM [dbo].[SUBJECTS] sub INNER JOIN [dbo].[STUDIES] s ON sub.study_id = s.study_id
		WHERE sub.study_id = @StudyId AND sub.password IS NOT NULL) selectedSubject INNER JOIN [dbo].[SUBJECT_MIDDLE_TIER] smt ON selectedSubject.Id = smt.subject_id INNER JOIN [dbo].[LOG_LEVEL] l ON smt.log_level = l.level_id
		ORDER BY selectedSubject.sw_patient_id 
	  
	END'

END

-- Change for revision 21
IF @revision < 21
BEGIN
	IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'EQ5D_LANGUAGES')
	BEGIN
		CREATE TABLE [dbo].[EQ5D_LANGUAGES](
		[study_id] [int] references [dbo].[STUDIES](study_id) NOT NULL,
		[eq_language_id] [int] NOT NULL,
		[culture] [varchar](50) NULL,
		PRIMARY KEY([study_id],[eq_language_id])
		);
	END
	
	IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'EQ5D_TIMEZONES')
	BEGIN
		CREATE TABLE [dbo].[EQ5D_TIMEZONES](
		[study_id] [int] references [dbo].[STUDIES](study_id) NOT NULL,
		[eq_time_zone_id] [int] NOT NULL,
		[sw_tz_name] [varchar](255) NOT NULL,
		[daylight_savings] [bit] NOT NULL,
		PRIMARY KEY([study_id],[eq_time_zone_id], [sw_tz_name])
		);
	END
END

IF @revision < 22
BEGIN
	--Create view PENDING_SURVEY_NOTIFICATION_SCHEDULE
	IF(EXISTS(SELECT * FROM sys.objects where name = 'PENDING_SURVEY_NOTIFICATION_SCHEDULE' and [type] = 'V'))
	BEGIN
		DROP VIEW PENDING_SURVEY_NOTIFICATION_SCHEDULE
	END
	EXEC dbo.sp_executesql @statement = N'
	--Must meet these conditions
	--1. wd_enabled = 1
	CREATE VIEW [dbo].[PENDING_SURVEY_NOTIFICATION_SCHEDULE] AS
	SELECT ses.survey_id, ses.subject_id, ses.schedule_dt_utc
	FROM dbo.SURVEY_EMAIL_SCHEDULE ses INNER JOIN dbo.SUBJECTS sub ON ses.subject_id = sub.Id
	INNER JOIN dbo.STUDIES s ON s.study_id = sub.study_id
	WHERE s.wd_enabled = 1'
END

/**Changes for Revision 23**/
IF @revision < 23
BEGIN
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE OBJECT_ID = OBJECT_ID(N'STUDY_MIDDLE_TIER') AND name = N'default_language')
		ALTER TABLE [dbo].[STUDY_MIDDLE_TIER] ADD [default_language] char(5) NULL;
END

IF @revision < 24
BEGIN

	--Create a procedure GetNotificationInformation 
	IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetNotificationInformation]') AND type in (N'P', N'PC'))
	BEGIN
		DROP PROCEDURE [dbo].[GetNotificationInformation]
	END
	
	SET ANSI_NULLS ON
	SET QUOTED_IDENTIFIER ON

	EXEC dbo.sp_executesql @statement = N'
	--Used to set sql depedency in WebDiary.SchedulerService to send notification
    CREATE PROCEDURE [dbo].[GetNotificationInformation]
	AS
	BEGIN
		SELECT ses.schedule_dt_utc, st.wd_enabled FROM dbo.SURVEY_EMAIL_SCHEDULE ses INNER JOIN dbo.SUBJECTS sub on ses.subject_id = sub.id INNER JOIN dbo.STUDIES st on sub.study_id = st.study_id
	END'
END

/** Revision 25 **/
IF @revision < 25
BEGIN  
    IF NOT EXISTS (SELECT * FROM SYS.columns WHERE OBJECT_ID = OBJECT_ID(N'DIARY_QUEUE') AND name = N'report_date')
		ALTER TABLE [dbo].[DIARY_QUEUE] ADD report_date date NULL;
END

IF @revision < 26
BEGIN
    --Create a trigger InsertSubjectLogLevelForUpgrade 
    IF EXISTS(SELECT * FROM sys.triggers where name = 'InsertSubjectLogLevelForUpgrade')
	BEGIN
		DROP TRIGGER [dbo].[InsertSubjectLogLevelForUpgrade]
	END
	EXEC dbo.sp_executesql @statement = N'CREATE TRIGGER [dbo].[InsertSubjectLogLevelForUpgrade]
	ON [dbo].[STUDY_MIDDLE_TIER]
	AFTER INSERT
	AS 
	BEGIN
		--This trigger is used to insert subject_log_level
		--when LogPad App installer populates STUDY_MIDDLE_TIER table
		--It inserts subject_log_level for the subjects which exists in SUBJECTS but not in SUBJECT_MIDDLE_TIER  
		SET NOCOUNT ON;

		DECLARE @StudyId int;
		DECLARE @DefaultStudyLogLevel tinyint;
	    
		SELECT @StudyId = study_id, @DefaultStudyLogLevel = default_log_level FROM inserted;
   		INSERT INTO [dbo].[SUBJECT_MIDDLE_TIER] (subject_id, log_level)
		SELECT sub.Id, @DefaultStudyLogLevel
		FROM [dbo].[SUBJECTS] sub LEFT OUTER JOIN [dbo].[SUBJECT_MIDDLE_TIER] smt 
		ON sub.Id = smt.subject_id
		WHERE sub.study_id = @StudyId
		AND smt.subject_id IS NULL  
	END'
END

IF @revision < 27
BEGIN
	IF EXISTS(SELECT * FROM sys.triggers where name = 'DeleteBatchLog')
	BEGIN
		DROP TRIGGER [dbo].[DeleteBatchLog]
	END
	EXEC dbo.sp_executesql @statement = N'CREATE TRIGGER [dbo].[DeleteBatchLog]
      ON [dbo].[BATCH_LOG_QUEUE]
      AFTER UPDATE
      AS
      --Delete a row when queue_state is updated to state 2
      BEGIN            
            SET NOCOUNT ON;
            IF UPDATE(queue_state)
            BEGIN            
				DECLARE @BatchLogId bigint;
				DECLARE @QueueState int;
				
				SELECT @BatchLogId = batch_log_id, @QueueState = queue_state FROM inserted;
				
				IF (@QueueState = 2)
				BEGIN
					DELETE FROM [dbo].[BATCH_LOG_QUEUE] WHERE batch_log_id = @BatchLogId;
				END          
            END
      END'
END

IF @revision < 28
BEGIN
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE OBJECT_ID = OBJECT_ID(N'ACTION_QUEUE') AND name = N'device_id')
		ALTER TABLE [dbo].[ACTION_QUEUE] ADD [device_id] bigint NULL;
	
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE OBJECT_ID = OBJECT_ID(N'RESPONSE_QUEUE') AND name = N'device_id')
		ALTER TABLE [dbo].[RESPONSE_QUEUE] ADD [device_id] bigint NULL;
END

IF @revision < 29
BEGIN	
	IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SUBMITTED_DIARIES')
	BEGIN
		CREATE TABLE [dbo].[SUBMITTED_DIARIES](
			[subject_id] uniqueidentifier NOT NULL,
			[completed_epoch] bigint NOT NULL,
			[created] date NOT NULL DEFAULT(GETDATE()),
			CONSTRAINT PK_SUBMITTED_DIARIES PRIMARY KEY NONCLUSTERED([subject_id],[completed_epoch])
		)
	END
	
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE OBJECT_ID = OBJECT_ID(N'SUBJECT_MIDDLE_TIER') AND name = N'sync_id')
	BEGIN
		ALTER TABLE [dbo].[SUBJECT_MIDDLE_TIER]
		ADD [sync_id] int NOT NULL DEFAULT(0);
	END
	
	IF EXISTS(SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID('SUBJECTS') AND name = 'IX_SUBJECT_PIN')
	DROP INDEX [IX_SUBJECT_PIN] ON [dbo].[SUBJECTS]
	
	-- fix null pin before creating unique index on it
	SET NOCOUNT ON;
	DECLARE @subjectid uniqueidentifier;
	DECLARE Subject_Cursor CURSOR FOR
	SELECT Id 
	FROM [dbo].[SUBJECTS]
	WHERE pin IS NULL;

	OPEN Subject_Cursor;
	FETCH NEXT FROM Subject_Cursor
	INTO @subjectid;
	WHILE @@FETCH_STATUS = 0
	   BEGIN
				DECLARE @Pin INT;
				DECLARE @Upper INT;
				DECLARE @Lower INT

				---- This will create a random number between 10000000 and 99999999
				SET @Lower = 10000000; ---- The lowest random number
				SET @Upper = 99999999; ---- The highest random number
				SET @Pin = ROUND(((@Upper - @Lower -1) * RAND() + @Lower), 0);

				---- Check if generated number already exists
				WHILE (EXISTS (SELECT * FROM SUBJECTS WHERE pin = @Pin))
					  SET @Pin = ROUND(((@Upper - @Lower -1) * RAND() + @Lower), 0);
	                  
				UPDATE SUBJECTS SET pin = @Pin WHERE Id = @subjectid;
	            
				FETCH NEXT FROM Subject_Cursor
				INTO @subjectid;        
	   END
	CLOSE Subject_Cursor;
	DEALLOCATE Subject_Cursor;
	-- end fix null pin
		 
	CREATE UNIQUE NONCLUSTERED INDEX [IX_SUBJECT_PIN] ON [dbo].[SUBJECTS] ([pin] ASC);
	
	IF EXISTS(SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID('SUBJECT_DEVICES') AND name = 'IX_SUBJECT_ID')
	DROP INDEX [IX_SUBJECT_ID] ON [dbo].[SUBJECT_DEVICES]
		
	CREATE NONCLUSTERED INDEX [IX_SUBJECT_ID] ON [dbo].[SUBJECT_DEVICES] ([activation_date] DESC,[subject_id] ASC);
	
	IF EXISTS(SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID('SUBJECT_MIDDLE_TIER') AND name = 'IX_SUBJECT_ID')
	DROP INDEX [IX_SUBJECT_ID] ON [dbo].[SUBJECT_MIDDLE_TIER]
		
	CREATE NONCLUSTERED INDEX [IX_SUBJECT_ID] ON [dbo].[SUBJECT_MIDDLE_TIER] ([subject_id] ASC);
END

IF @revision < 30
BEGIN
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'subject_id_format' AND OBJECT_ID = OBJECT_ID(N'STUDIES'))
	BEGIN
		ALTER TABLE [dbo].[STUDIES]
		ADD [subject_id_format] varchar(50) NULL;
	END
END

IF @revision < 31
BEGIN
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'phase_start_date' AND OBJECT_ID = OBJECT_ID(N'DIARY_QUEUE'))
	BEGIN
		ALTER TABLE [dbo].[DIARY_QUEUE]
		ADD [phase_start_date] [datetimeoffset](7) NOT NULL;
	END 
	
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'phase_start_date' AND OBJECT_ID = OBJECT_ID(N'SUBJECT_MIDDLE_TIER'))
	BEGIN
		ALTER TABLE [dbo].[SUBJECT_MIDDLE_TIER]
		ADD [phase_start_date] [datetimeoffset](7) NULL; 
    END 
END

IF @revision < 32
BEGIN
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'seed_code' AND OBJECT_ID = OBJECT_ID(N'STUDY_MIDDLE_TIER'))
		ALTER TABLE [dbo].[STUDY_MIDDLE_TIER] ADD [seed_code] smallint NOT NULL DEFAULT(1111);
	
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'code_length' AND OBJECT_ID = OBJECT_ID(N'STUDY_MIDDLE_TIER'))
		ALTER TABLE [dbo].[STUDY_MIDDLE_TIER] ADD [code_length] tinyint NOT NULL DEFAULT(6);
		
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'authentication_level' AND OBJECT_ID = OBJECT_ID(N'STUDY_MIDDLE_TIER'))
		ALTER TABLE [dbo].[STUDY_MIDDLE_TIER] ADD [authentication_level] tinyint NOT NULL DEFAULT(1);
END

IF @revision < 33
BEGIN
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'enrollment_date' AND OBJECT_ID = OBJECT_ID(N'SUBJECT_MIDDLE_TIER'))
	BEGIN
		ALTER TABLE [dbo].[SUBJECT_MIDDLE_TIER]
		ADD [enrollment_date] [datetimeoffset](7) NULL; 
    END
    
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'activation_date' AND OBJECT_ID = OBJECT_ID(N'SUBJECT_MIDDLE_TIER'))
	BEGIN
		ALTER TABLE [dbo].[SUBJECT_MIDDLE_TIER]
		ADD [activation_date] [datetimeoffset](7) NULL; 
    END 
END

IF @revision < 34
BEGIN
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'password' AND OBJECT_ID = OBJECT_ID(N'SUBJECT_DEVICES'))
	BEGIN
		ALTER TABLE [dbo].[SUBJECT_DEVICES]
		ADD [password] varchar(128) NULL; 
    END
END

IF @revision < 35
BEGIN
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'sw_error_code' AND OBJECT_ID = OBJECT_ID(N'DIARY_QUEUE'))
	BEGIN
		ALTER TABLE [dbo].[DIARY_QUEUE]
		ADD [sw_error_code] [varchar](20) NULL; 
    END
    
    --Create UserDefinedTableType [DiaryQueueTableType]
	IF NOT EXISTS(SELECT * FROM sys.table_types where name = 'DiaryQueueTableType')
	BEGIN
		EXEC dbo.sp_executesql @statement = N'CREATE TYPE [dbo].[DiaryQueueTableType] AS TABLE(
			[diary_queue_id] [bigint] NOT NULL,
			[diary_state] [int] NOT NULL,
			PRIMARY KEY CLUSTERED 
		(
			[diary_queue_id] ASC
		)WITH (IGNORE_DUP_KEY = OFF)
		)'
	END
	
	--Create a procedure UpdateDiaryQueueState
	IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateDiaryQueueState]') AND type in (N'P', N'PC'))
	BEGIN
		DROP PROCEDURE [dbo].[UpdateDiaryQueueState]
	END
	EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UpdateDiaryQueueState]
	@tvpDiaryQueueState [dbo].[DiaryQueueTableType] READONLY
	AS
	BEGIN
		UPDATE [dbo].[DIARY_QUEUE]
		SET [dbo].[DIARY_QUEUE].diary_state = udq.diary_state
		FROM [dbo].[DIARY_QUEUE] INNER JOIN @tvpDiaryQueueState AS udq
		ON [dbo].[DIARY_QUEUE].diary_queue_id = udq.diary_queue_id;
	END'
END

IF @revision < 36
BEGIN
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'sw_device_id' AND OBJECT_ID = OBJECT_ID(N'SUBJECT_DEVICES'))
	BEGIN
		ALTER TABLE [dbo].[SUBJECT_DEVICES]
		ADD [sw_device_id] int NULL;
    END
END

IF @revision < 37
BEGIN
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'battery_level' AND OBJECT_ID = OBJECT_ID(N'DIARY_QUEUE'))
	BEGIN
		ALTER TABLE [dbo].[DIARY_QUEUE]
		ADD [battery_level] [nvarchar](50) NULL
    END
END

IF @revision < 38
BEGIN	
	IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ORIGINAL_CORE_VERSION')
	BEGIN
		CREATE TABLE [dbo].[ORIGINAL_CORE_VERSION](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[study_id] [int] NOT NULL FOREIGN KEY REFERENCES [STUDIES] ([study_id]),
		[major_version] [int] NOT NULL,
		[minor_version] [int] NOT NULL,
		[micro_version] [int] NOT NULL,
		[build_number] [int] NOT NULL,
		[creation_date] [datetime] NOT NULL,
		[modification_date] [datetime] NULL,
		 CONSTRAINT [PK_ORI_CORE_VERSION] PRIMARY KEY CLUSTERED 
		(
			[Id] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
		 CONSTRAINT [UNQ_ORI_CORE_VERSION] UNIQUE NONCLUSTERED 
		(
			[study_id] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]
	END
END

IF @revision < 39
BEGIN	
	IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'INK_SIGNATURE')
	BEGIN
		CREATE TABLE [dbo].[INK_SIGNATURE](
			[id] [bigint] IDENTITY(1,1) NOT NULL,
			[diary_queue_id] [bigint] NOT NULL,
			[author] [nvarchar](128) NOT NULL,
			[xsize] [int] NOT NULL,
			[ysize] [int] NOT NULL,
			[vector] [varchar](max) NOT NULL,
		 CONSTRAINT [PK_INK_SIGNATURE] PRIMARY KEY CLUSTERED 
		(
			[id] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]

		ALTER TABLE [dbo].[INK_SIGNATURE]  WITH NOCHECK ADD  CONSTRAINT [FK_INK_SIGNATURE_DIARY_QUEUE] FOREIGN KEY([diary_queue_id])
		REFERENCES [dbo].[DIARY_QUEUE] ([diary_queue_id])

		ALTER TABLE [dbo].[INK_SIGNATURE] CHECK CONSTRAINT [FK_INK_SIGNATURE_DIARY_QUEUE]
	END
END

IF @revision < 40
BEGIN
	IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'INK_SIGNATURE')
	BEGIN
		CREATE TABLE [dbo].[INK_SIGNATURE](
			[id] [bigint] IDENTITY(1,1) NOT NULL,
			[diary_queue_id] [bigint] NOT NULL,
			[author] [nvarchar](128) NOT NULL,
			[xsize] [int] NOT NULL,
			[ysize] [int] NOT NULL,
			[vector] [varchar](max) NOT NULL,
		 CONSTRAINT [PK_INK_SIGNATURE] PRIMARY KEY CLUSTERED 
		(
			[id] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]

		ALTER TABLE [dbo].[INK_SIGNATURE]  WITH NOCHECK ADD  CONSTRAINT [FK_INK_SIGNATURE_DIARY_QUEUE] FOREIGN KEY([diary_queue_id])
		REFERENCES [dbo].[DIARY_QUEUE] ([diary_queue_id])
		ALTER TABLE [dbo].[INK_SIGNATURE] CHECK CONSTRAINT [FK_INK_SIGNATURE_DIARY_QUEUE]
	END
END

IF @revision < 41 
BEGIN 
    -- Backwards compatibility with LogPad App 1.5.1 
    IF EXISTS (SELECT * FROM SYS.columns WHERE name = N'phase_start_date' AND OBJECT_ID = OBJECT_ID(N'DIARY_QUEUE')) 
      BEGIN  
            ALTER TABLE [dbo].[DIARY_QUEUE] 
            ALTER COLUMN [phase_start_date] [datetimeoffset](7) NULL; 
      END 
END

IF @revision < 42
BEGIN
	--Create a procedure GetStuckReportByStudyId
	IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetStuckReportByStudyId]') AND type in (N'P', N'PC'))
	BEGIN
		DROP PROCEDURE [dbo].[GetStuckReportByStudyId]
	END
	EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GetStuckReportByStudyId]
	@StudyId int
	AS
	BEGIN
	  SET NOCOUNT ON
	  	  SELECT dq.diary_queue_id, s.sw_patient_id, dq.su,
	  CASE diary_state
		WHEN 7 THEN ''Rejected''
		WHEN 9 THEN ''Corrupted''
	  END
	  AS diary_state,
	  dq.datetime_received, dq.started, dq.completed, dq.sw_error_code  
	  FROM DIARY_QUEUE dq INNER JOIN SUBJECTS s ON dq.subject_id = s.id   
	  WHERE s.study_id = @StudyId AND dq.diary_state in(7,9)
	END'
END

IF @revision < 43
BEGIN
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'sort_order' AND OBJECT_ID = OBJECT_ID(N'STUDY_SURVEYS'))
		ALTER TABLE [dbo].[STUDY_SURVEYS] ADD [sort_order] [int] NULL;
END

IF @revision < 44
BEGIN
	IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'NETPRO_DUPLICATE_REPORTS')
	BEGIN
		CREATE TABLE [dbo].[NETPRO_DUPLICATE_REPORTS](
			[Id] [bigint] IDENTITY(1,1) NOT NULL,
			[survey_id] [int] NOT NULL,
			[krpt] [varchar](36) NOT NULL,
			[study_name] [varchar](40) NOT NULL,
			[krsu] [varchar](36) NULL,
			[report_start_date_utc] [datetime] NULL,
			[report_start_date] [varchar](254) NULL,
			[sigid] [varchar](256) NOT NULL,
			[clin_record] [nvarchar](max) NOT NULL,
			[time_stamp_utc] [datetime] NOT NULL,
		 CONSTRAINT [PK_NETPRO_DUPLICATE_REPORTS] PRIMARY KEY CLUSTERED 
		(
			[Id] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]

		ALTER TABLE [dbo].[NETPRO_DUPLICATE_REPORTS] ADD  CONSTRAINT [DF_NETPRO_DUPLICATE_REPORTS_time_stamp_utc]  DEFAULT (getutcdate()) FOR [time_stamp_utc]

		ALTER TABLE [dbo].[NETPRO_DUPLICATE_REPORTS]  WITH CHECK ADD  CONSTRAINT [FK_NPDR_SURVEYS] FOREIGN KEY([survey_id])
		REFERENCES [dbo].[STUDY_SURVEYS] ([id])
	END
	
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'sigid' AND OBJECT_ID = OBJECT_ID(N'PRODUCT_SURVEY_DATA'))
		ALTER TABLE [dbo].[PRODUCT_SURVEY_DATA] ADD [sigid] [varchar](256) NULL
	
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'sigid' AND OBJECT_ID = OBJECT_ID(N'DIARY_QUEUE'))
		ALTER TABLE [dbo].[DIARY_QUEUE] ADD [sigid] [varchar](256) NULL;		
END

IF @revision < 45
BEGIN
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'subject_initials' AND OBJECT_ID = OBJECT_ID(N'STUDIES'))
	BEGIN
		ALTER TABLE [dbo].[STUDIES]
		ADD subject_initials bit NULL;
	END
END

IF @revision < 46
BEGIN
	IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'DIARY_HISTORY')
	BEGIN
		CREATE TABLE [dbo].[DIARY_HISTORY](
			[Id] [bigint] IDENTITY(1,1) NOT NULL,
			[sigid] [varchar](256) NOT NULL,
			[subject_id] [uniqueidentifier] NOT NULL,
			[submitted] [bit] NOT NULL,
			[created] [datetime] NOT NULL,
		 CONSTRAINT [PK_DIARY_HISTORY] PRIMARY KEY CLUSTERED 
		(
			[Id] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
		 CONSTRAINT [UQ_DIARY_HISTORY] UNIQUE NONCLUSTERED 
		(
			[sigid] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]
		
		ALTER TABLE [dbo].[DIARY_HISTORY] ADD  CONSTRAINT [DF_DIARY_HISTORY_submitted]  DEFAULT ((0)) FOR [submitted]
		ALTER TABLE [dbo].[DIARY_HISTORY] ADD  CONSTRAINT [DF_DIARY_HISTORY_created]  DEFAULT (getdate()) FOR [created]
	END
	
	--ALTER a procedure GetStuckReportByStudyId
	IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetStuckReportByStudyId]') AND type in (N'P', N'PC'))
	BEGIN
		EXEC dbo.sp_executesql @statement = N'ALTER PROCEDURE [dbo].[GetStuckReportByStudyId]
		@StudyId int
		AS
		BEGIN
		  SET NOCOUNT ON
  			  SELECT dq.diary_queue_id, s.sw_patient_id, dq.su,
		  CASE diary_state
			WHEN 7 THEN ''Rejected''
			WHEN 8 THEN ''Duplicate''
			WHEN 9 THEN ''Corrupted''
		  END
		  AS diary_state,
		  dq.datetime_received, dq.started, dq.completed, dq.sw_error_code  
		  FROM DIARY_QUEUE dq INNER JOIN SUBJECTS s ON dq.subject_id = s.id   
		  WHERE s.study_id = @StudyId AND dq.diary_state in(7,8,9)
		END'
	END
END

IF @revision < 47
BEGIN
	IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'DIARY_HISTORY_NO_SIGID')
	BEGIN
		CREATE TABLE [dbo].[DIARY_HISTORY_NO_SIGID](
			[subject_id] [uniqueidentifier] NOT NULL,
			[diary_id] [bigint] NOT NULL,
			[submitted] [bit] NOT NULL,
			[created] [datetime] NOT NULL,
		 CONSTRAINT [PK_DIARY_HISTORY_NO_SIGID] PRIMARY KEY CLUSTERED 
		(
			[subject_id] ASC,
			[diary_id] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]

		ALTER TABLE [dbo].[DIARY_HISTORY_NO_SIGID] ADD  CONSTRAINT [DF_DIARY_HISTORY_NO_SIGID_submitted]  DEFAULT ((0)) FOR [submitted]
		ALTER TABLE [dbo].[DIARY_HISTORY_NO_SIGID] ADD  CONSTRAINT [DF_DIARY_HISTORY_NO_SIGID_created]  DEFAULT (getdate()) FOR [created]
	END
END

IF @revision < 48
BEGIN
	IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'STUCK_REPORT_ERROR')
	BEGIN
		CREATE TABLE [dbo].[STUCK_REPORT_ERROR](
			[id] [bigint] IDENTITY(1,1) NOT NULL,
			[diary_queue_id] [bigint] NULL,
			[diary_state] [int] NULL,
			[message] [nvarchar](max) NULL,
			[inserted_utc] [datetime] NOT NULL,
		 CONSTRAINT [PK_STUCK_REPORT_ERROR] PRIMARY KEY CLUSTERED 
		(
			[id] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

		ALTER TABLE [dbo].[STUCK_REPORT_ERROR] ADD  CONSTRAINT [DF_STUCK_REPORT_ERROR_inserted_utc]  DEFAULT (getutcdate()) FOR [inserted_utc]
	END
	
	IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'STUCK_REPORT_TRACE')
	BEGIN
		CREATE TABLE [dbo].[STUCK_REPORT_TRACE](
		[id] [bigint] IDENTITY(1,1) NOT NULL,
		[diary_queue_id] [bigint] NOT NULL,
		[subject_id] [uniqueidentifier] NOT NULL,
		[su] [nvarchar](50) NULL,
		[diary] [nvarchar](max) NULL,
		[message] [varchar](1000) NULL,
		[inserted_utc] [datetime] NOT NULL,
		 CONSTRAINT [PK_STUCK_REPORT_TRACE] PRIMARY KEY CLUSTERED 
		(
			[id] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

		ALTER TABLE [dbo].[STUCK_REPORT_TRACE] ADD  CONSTRAINT [DF_STUCK_REPORT_TRACE_inserted_utc]  DEFAULT (getutcdate()) FOR [inserted_utc]
	END
	
	IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertStuckReportTrace]') AND type in (N'P', N'PC'))
	BEGIN
		DROP PROCEDURE [dbo].[InsertStuckReportTrace] 
	END
	EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[InsertStuckReportTrace]

	@DiaryQueueId bigint,
	@Message varchar(1000) = null
	 
	AS
	BEGIN
		SET NOCOUNT ON

		DECLARE @SubjectId uniqueidentifier;
		DECLARE @SU nvarchar(50);
		DECLARE @Xml xml;
		DECLARE @String nvarchar(max);

		SELECT @SubjectId = subject_id, @SU = su FROM DIARY_QUEUE WITH (NOLOCK) WHERE diary_queue_id = @DiaryQueueId;

		IF @SubjectId IS NOT NULL
		BEGIN
			SET @XML=(
				SELECT
					  dq.diary_queue_id,
					  dq.diary_state,
					  dq.queue_date,
					  dq.activity_date,
					  dq.subject_id,
					  ISNULL(dq.browser_platform,
					  '''') AS browser_platform,
					  ISNULL(dq.browser_type,
					  '''') AS browser_type,
					  ISNULL(dq.browser_version,
					  '''') AS browser_version,
					  ISNULL(dq.su,
					  '''') AS su,
					  dq.started,
					  dq.completed,
					  dq.completed_tz_offset,
					  dq.completed_epoch_t,
					  ISNULL(dq.phase,
					  '''') AS phase,
					  dq.change_phase,
					  dq.datetime_submitted,
					  dq.datetime_received,
					  dq.client_core_version,
					  dq.client_study_version,
					  dq.device_id,
					  dq.report_date,
					  dq.phase_start_date,
					  ISNULL(dq.sw_error_code,
					  '''') AS sw_error_code,
					  ISNULL(dq.battery_level,
					  '''') AS battery_level,
					  ISNULL(dq.sigid,
					  '''') AS sigid,
					  (SELECT
						 dqa.diary_queue_answer_id,
						 dqa.diary_queue_id,
						 dqa.data_changed,
						 ISNULL(dqa.response,
						 '''') AS response,
						 ISNULL(dqa.sw_alias,
						 '''') AS sw_alias,
						 ISNULL(dqa.question_id,
						 '''') AS question_id    
					  FROM
						 DIARY_QUEUE_ANSWERS dqa WITH (NOLOCK)    
					  WHERE
						 dqa.diary_queue_id = dq.diary_queue_id    FOR XML PATH (''DIARY_QUEUE_ANSWERS''), TYPE),
					  (SELECT
						 ink.id,
						 ink.diary_queue_id,
						 ink.author,
						 ink.xsize,
						 ink.ysize,
						 ink.vector    
					  FROM
						 INK_SIGNATURE ink WITH (NOLOCK)    
					  WHERE
						 ink.diary_queue_id = dq.diary_queue_id    FOR XML PATH (''INK_SIGNATURE''), TYPE)  
				   FROM
					  DIARY_QUEUE dq WITH (NOLOCK)  
				   WHERE
					  dq.diary_queue_id = @DiaryQueueId  FOR XML PATH (''DIARY_QUEUE''), ELEMENTS);
				  
			SET @String = CONVERT(nvarchar(MAX),@Xml);
			
			INSERT INTO [dbo].[STUCK_REPORT_TRACE]
			   (diary_queue_id
			   ,subject_id
			   ,su
			   ,diary
			   ,message)
			VALUES
					   (@DiaryQueueId
					   ,@SubjectId
					   ,@SU
					   ,@String
					   ,@Message);  
		END

	END'
END

IF @revision < 49
BEGIN
	IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'DEVICES')
	BEGIN
		CREATE TABLE [dbo].[DEVICES](
		[device_id] [bigint] IDENTITY(1,1) NOT NULL,
		[study_id] [int] NOT NULL,
		[site_code_at_registration] [varchar](254) NOT NULL,
		[device_uuid] [varchar](64) NOT NULL,
		[asset_tag] [nvarchar](32) NULL,
		[device_code] [uniqueidentifier] NOT NULL,
		[api_token] [varchar](36) NOT NULL,
		[krdom] [varchar](255) NOT NULL,
		[active] [bit] NOT NULL,
		[sw_device_id] [int] NULL,
		[registration_date_utc] [datetime] NOT NULL,
		 CONSTRAINT [PK_DEVICES] PRIMARY KEY CLUSTERED 
		(
			[device_id] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],

		CONSTRAINT [UQ_DEVICES_api_token] UNIQUE NONCLUSTERED 
		(
			[api_token] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
		) ON [PRIMARY]
		
		ALTER TABLE [dbo].[DEVICES]  WITH CHECK ADD  CONSTRAINT [FK_DEVICES_STUDIES] FOREIGN KEY([study_id])
		REFERENCES [dbo].[STUDIES] ([study_id])
		
		ALTER TABLE [dbo].[DEVICES] CHECK CONSTRAINT [FK_DEVICES_STUDIES]
	END
END

IF @revision < 50
BEGIN
	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'krpt' AND OBJECT_ID = OBJECT_ID(N'DIARY_QUEUE'))
	BEGIN
		ALTER TABLE [dbo].[DIARY_QUEUE]
		ADD [krpt] [varchar](36) NULL;
	END

	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'sw_device_id' AND OBJECT_ID = OBJECT_ID(N'DIARY_QUEUE'))
	BEGIN
		ALTER TABLE [dbo].[DIARY_QUEUE]
		ADD [sw_device_id] [int] NULL;
	END

	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'site_pad_app' AND OBJECT_ID = OBJECT_ID(N'DIARY_QUEUE'))
	BEGIN
		ALTER TABLE [dbo].[DIARY_QUEUE]
		ADD [site_pad_app] [bit] NULL;
	END

	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'responsible_party' AND OBJECT_ID = OBJECT_ID(N'DIARY_QUEUE'))
	BEGIN
		ALTER TABLE [dbo].[DIARY_QUEUE]
		ADD [responsible_party] [nvarchar](128) NULL;
	END
END

IF @revision < 51
BEGIN
	IF EXISTS (SELECT * FROM SYS.columns WHERE name = N'subject_id' AND OBJECT_ID = OBJECT_ID(N'BATCH_LOG_QUEUE')) 
	BEGIN
		ALTER TABLE [dbo].[BATCH_LOG_QUEUE]
		ALTER COLUMN [subject_id] [uniqueidentifier] NULL;
	END 
      
 	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'study_id' AND OBJECT_ID = OBJECT_ID(N'BATCH_LOG_QUEUE'))
	BEGIN
		ALTER TABLE [dbo].[BATCH_LOG_QUEUE]
		ADD [study_id] [int] NULL FOREIGN KEY REFERENCES [STUDIES] ([study_id]); 
	END

	IF(EXISTS(SELECT * FROM sys.objects where name = 'Pending_Logs' and [type] = 'V'))
		DROP VIEW Pending_Logs;
		
	EXEC dbo.sp_executesql @statement = N'
		CREATE VIEW [dbo].[Pending_Logs]
		AS
		SELECT ISNULL(lq.study_id, s.study_id) AS study_id, lq.batch_log_id 
		FROM	dbo.BATCH_LOG_QUEUE AS lq LEFT JOIN dbo.SUBJECTS AS s ON lq.subject_id = s.Id
		WHERE     (lq.queue_state = 3) OR (lq.queue_state = 4)';	
END

IF @revision < 52
BEGIN
 	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'last_security_question_answer_changed' AND OBJECT_ID = OBJECT_ID(N'SUBJECTS'))
	BEGIN
		ALTER TABLE [dbo].[SUBJECTS]
		ADD [last_security_question_answer_changed] [datetime] NULL;
	END
	
    --ALTER a procedure GetStuckReportByStudyId
	IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetStuckReportByStudyId]') AND type in (N'P', N'PC'))
	BEGIN
		EXEC dbo.sp_executesql @statement = N'ALTER PROCEDURE [dbo].[GetStuckReportByStudyId]
		@StudyId int
		AS
		BEGIN
		  SET NOCOUNT ON
  			  SELECT dq.diary_queue_id, s.sw_patient_id, dq.su,
		  CASE diary_state
			WHEN 7 THEN ''Rejected''
			WHEN 8 THEN ''Duplicate''
			WHEN 9 THEN ''Corrupted''
			WHEN 10 THEN ''No Affidavit''
		  END
		  AS diary_state,
		  dq.datetime_received, dq.started, dq.completed, dq.sw_error_code  
		  FROM DIARY_QUEUE dq INNER JOIN SUBJECTS s ON dq.subject_id = s.id   
		  WHERE s.study_id = @StudyId AND dq.diary_state in(7,8,9,10)
		END'
	END
	
	IF EXISTS(SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID('DEVICES') AND name = 'IX_DEVICES_device_uuid')
	DROP INDEX [IX_DEVICES_device_uuid] ON [dbo].[DEVICES]
	
	CREATE NONCLUSTERED INDEX [IX_DEVICES_device_uuid] ON [dbo].[DEVICES] 
	(
		[device_uuid] ASC,
		[active] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END

IF @revision < 53
BEGIN
 	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'client_password' AND OBJECT_ID = OBJECT_ID(N'SUBJECTS'))
	BEGIN
		ALTER TABLE [dbo].[SUBJECTS]
		ADD [client_password] [nvarchar](256) NULL;
	END

 	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'client_password' AND OBJECT_ID = OBJECT_ID(N'SUBJECT_DEVICES'))
	BEGIN
		ALTER TABLE [dbo].[SUBJECT_DEVICES]
		ADD [client_password] [nvarchar](256) NULL;
	END		
END

IF @revision < 54
BEGIN
	IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetUTC]') AND type in (N'P', N'PC'))
	BEGIN
		DROP PROCEDURE [dbo].[GetUTC] 
	END
	EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[GetUTC]
	AS
	BEGIN
		SET NOCOUNT ON;
		SELECT GETUTCDATE();
	END'	
END

IF @revision < 55
BEGIN
 	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'triggered_phase_at_initial_activation' AND OBJECT_ID = OBJECT_ID(N'STUDIES'))
	BEGIN
		ALTER TABLE [dbo].[STUDIES]
		ADD [triggered_phase_at_initial_activation] [varchar](2300) NULL;
	END
END

IF @revision < 56
BEGIN
	IF EXISTS(SELECT * FROM sys.triggers where name = 'DiaryStateTrigger')
	BEGIN
		DROP TRIGGER [dbo].[DiaryStateTrigger]
	END
	
	--Create a procedure DeleteSubmittedDiaries
	IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteSubmittedDiaries]') AND type in (N'P', N'PC'))
	BEGIN
		DROP PROCEDURE [dbo].[DeleteSubmittedDiaries]
	END
	EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[DeleteSubmittedDiaries]
	AS
	BEGIN
		SET NOCOUNT ON;

		DECLARE @NumberOfRows INT;
		DECLARE @CurrentRow INT;
		DECLARE @DiaryQueueId BIGINT;

		DECLARE @SubmittedDiaryState INT = 2;

		IF OBJECT_ID(N''tempdb..#DiaryQueueIdTable'') IS NOT NULL
			DROP TABLE #DiaryQueueIdTable;
			
		CREATE TABLE #DiaryQueueIdTable
		(RowNo INT NOT NULL PRIMARY KEY IDENTITY(1,1),
		 DiaryQueueId BIGINT);
		 
		INSERT INTO #DiaryQueueIdTable (DiaryQueueId)
		SELECT diary_queue_id
		FROM DIARY_QUEUE WITH (NOLOCK)
		WHERE diary_state = @SubmittedDiaryState;

		SELECT @NumberOfRows = COUNT(*) FROM #DiaryQueueIdTable;
		SET @CurrentRow = 0;

		WHILE @CurrentRow < @NumberOfRows
		BEGIN
			SET @CurrentRow = @CurrentRow + 1;
			
			SELECT @DiaryQueueId = DiaryQueueId
			FROM #DiaryQueueIdTable
			WHERE RowNo = @CurrentRow;

			IF EXISTS(SELECT 1 FROM DIARY_QUEUE WITH (NOLOCK) WHERE diary_queue_id = @DiaryQueueId AND diary_state = @SubmittedDiaryState)
			BEGIN		
				BEGIN TRY
					BEGIN TRANSACTION;
						DELETE FROM [dbo].[INK_SIGNATURE] WHERE diary_queue_id = @DiaryQueueId;
						DELETE FROM [dbo].[DIARY_QUEUE_ANSWERS] WHERE diary_queue_id = @DiaryQueueId;
						DELETE FROM [dbo].[DIARY_QUEUE] WHERE diary_queue_id = @DiaryQueueId;
					COMMIT TRANSACTION;
				END TRY
				BEGIN CATCH
					IF @@TRANCOUNT > 0
						ROLLBACK TRANSACTION;
				END CATCH
			END
		END
					
		DROP TABLE #DiaryQueueIdTable;
	END'	
END

--Create Job_DeleteSubmittedDiaries 
IF @revision < 56
BEGIN
	/* Drop Job_DeleteSubmittedDiaries */
	DECLARE @jobname2 nvarchar(200)
	SET @jobname2 = N'Job_DeleteSubmittedDiaries'
	-- Delete the [local] job 
	DECLARE @LocalJobId2 binary(16)
	SELECT @LocalJobId2 = job_id FROM msdb.dbo.sysjobs WHERE (name = @jobname2)
	IF (@LocalJobId2 IS NOT NULL)
	BEGIN
		EXEC msdb.dbo.sp_delete_job @LocalJobId2
	END
	
	/* Add job */
	BEGIN TRANSACTION
	DECLARE @ReturnCode2 INT
	SELECT @ReturnCode2 = 0

	IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
	BEGIN
	EXEC @ReturnCode2 = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
	IF (@@ERROR <> 0 OR @ReturnCode2 <> 0) GOTO QuitWithRollback2

	END

	DECLARE @jobId2 BINARY(16)
	EXEC @ReturnCode2 =  msdb.dbo.sp_add_job 
			@job_name=N'Job_DeleteSubmittedDiaries', 
			@enabled=1, 
			@notify_level_eventlog=0, 
			@notify_level_email=0, 
			@notify_level_netsend=0, 
			@notify_level_page=0, 
			@delete_level=0, 
			@description=N'Delete diaries submitted to StudyWorks successfully', 
			@category_name=N'[Uncategorized (Local)]', 
			@owner_login_name=NULL, 
			@job_id = @jobId2 OUTPUT
	IF (@@ERROR <> 0 OR @ReturnCode2 <> 0) GOTO QuitWithRollback2

	--Add job step
	EXEC @ReturnCode2 = msdb.dbo.sp_add_jobstep
			@job_id=@jobId2,
			@step_name=N'JobStep_DeleteSubmittedDiaries', 
			@step_id=1, 
			@cmdexec_success_code=0, 
			@on_success_action=1, 
			@on_success_step_id=0, 
			@on_fail_action=2, 
			@on_fail_step_id=0, 
			@retry_attempts=0, 
			@retry_interval=0, 
			@os_run_priority=0, @subsystem=N'TSQL', 
			@command=N'EXECUTE DeleteSubmittedDiaries ', 
			@database_name=N'WebDiary', 
			@flags=0
			
	IF (@@ERROR <> 0 OR @ReturnCode2 <> 0) GOTO QuitWithRollback2

	EXEC @ReturnCode2 = msdb.dbo.sp_update_job @job_id = @jobId2, @start_step_id = 1
	IF (@@ERROR <> 0 OR @ReturnCode2 <> 0) GOTO QuitWithRollback2

	--Add job schedule
	EXEC @ReturnCode2 = msdb.dbo.sp_add_jobschedule
			@job_id=@jobId2,
			@name=N'JobSchedule_DeleteSubmittedDiaries', 
			@enabled=1, 
			@freq_type=4, 
			@freq_interval=1, 
			@freq_subday_type=1, 
			@freq_subday_interval=0, 
			@freq_relative_interval=0, 
			@freq_recurrence_factor=0, 
			@active_start_date=20161213, 
			@active_end_date=99991231, 
			@active_start_time=0, 
			@active_end_time=235959 
	IF (@@ERROR <> 0 OR @ReturnCode2 <> 0) GOTO QuitWithRollback2

	EXEC @ReturnCode2 = msdb.dbo.sp_add_jobserver @job_id = @jobId2, @server_name = N'(local)'
	IF (@@ERROR <> 0 OR @ReturnCode2 <> 0) GOTO QuitWithRollback2

	COMMIT TRANSACTION
	GOTO EndSave2
	QuitWithRollback2:
		IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
	EndSave2:
END

IF @revision < 57
BEGIN
 	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'protocol_code_list_id' AND OBJECT_ID = OBJECT_ID(N'STUDIES'))
	BEGIN
		ALTER TABLE [dbo].[STUDIES]
		ADD [protocol_code_list_id] [varchar](255) NULL;
	END
END

IF @revision < 58
BEGIN
	IF EXISTS (SELECT * FROM SYS.columns WHERE name = N'response' AND OBJECT_ID = OBJECT_ID(N'DIARY_QUEUE_ANSWERS'))
	BEGIN
		ALTER TABLE [dbo].[DIARY_QUEUE_ANSWERS]
		ALTER COLUMN response [nvarchar](max) NULL;
	END
END

IF @revision < 59
BEGIN
 	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'authorized_browser' AND OBJECT_ID = OBJECT_ID(N'STUDIES'))
	BEGIN
		ALTER TABLE [dbo].[STUDIES]
		ADD [authorized_browser] [varchar](max) NULL;
	END
END

IF @revision < 60
BEGIN
 	IF NOT EXISTS (SELECT * FROM SYS.columns WHERE name = N'subject_language' AND OBJECT_ID = OBJECT_ID(N'STUDIES'))
	BEGIN
		ALTER TABLE [dbo].[STUDIES]
		ADD [subject_language] [varchar](max) NULL;
	END
END
GO





