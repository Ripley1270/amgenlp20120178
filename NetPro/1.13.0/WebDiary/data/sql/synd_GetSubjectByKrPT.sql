/*****************************************************************************
Stored procedure: synd_GetSubjectByKrPT 
Description: Get subject related data by KrPT 
Project: Syndication
History:
1.00 14SEP2017 Initial creation
*****************************************************************************/

IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'synd_GetSubjectByKrPT' AND TYPE = 'P')
   DROP PROCEDURE [dbo].[synd_GetSubjectByKrPT]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[synd_GetSubjectByKrPT]

@KrPT VARCHAR(36) 

AS

BEGIN

	SET NOCOUNT ON;

	DECLARE @Xml XML;
	DECLARE @XmlRow INT;
	DECLARE @String NVARCHAR(MAX);
	DECLARE @ActivationDate VARCHAR(254);
  	DECLARE @suOrigKrtz VARCHAR(36);
  	
	SET @String = ''; 

	CREATE TABLE #ServerTimeStamp
	(
		krpt VARCHAR(36) NOT NULL,
		receiptts DATETIME NULL
	)

	--Insert server timestamp from lookup_pt into temporary table
	INSERT INTO #ServerTimeStamp(krpt, receiptts)
	SELECT krpt, receiptts
	FROM lookup_pt WITH (NOLOCK)
	WHERE krpt =  @KrPT;	

	--Get latest server time stamp from lookup_su table whose su is assignment and activation  
	--Insert the server timestamp into temporary table
	--When some data such as language is updated, the time stamp in lookup_pt table is not updated and time stamp in lookup_su table is updated.
	--Thus get latest time stamp from lookup_su to cover these cases	
	INSERT INTO #ServerTimeStamp(krpt, receiptts)
	SELECT krpt, MAX(receiptts) AS receiptts
	FROM lookup_su WITH (NOLOCK) 
	WHERE krpt = @KrPT
	AND krsu in('assignment', 'activation')
    GROUP BY krpt;
	
	SELECT TOP (1) @ActivationDate = ReportStartDate, @suOrigKrtz = orig_krtz
	FROM lookup_su WITH (NOLOCK) 
	WHERE krpt = @KrPT
	AND krsu = 'activation'
	AND deleted <> 2 --2 means deleted
	ORDER BY orig_startts ASC	    

	SET @Xml = (SELECT ISNULL(pt.patientid, '') AS "@subjectId",
						pt.krpt AS "@krpt",
						ISNULL(pt.initials, '') AS "@initials",
						REPLACE(ISNULL(am.Language, ''), '_', '-') AS "@language",
						pt.krdom AS "@krdom", 
						ISNULL(dom.sitecode, '') AS "@siteCode",
						ISNULL(pt.krphase, '') AS "@phase",
						ISNULL(pt.phasedate, '') AS "@phaseDate",
						ISNULL(pt.deleted, '') AS "@deleted",
						ISNULL(pt.dob, '') AS "@dob",
						ISNULL(pt.gender, '') AS "@gender",
						ISNULL(pt.custom1, '') AS "@custom1",
						ISNULL(pt.custom2, '') AS "@custom2",
						ISNULL(pt.custom3, '') AS "@custom3",
						ISNULL(pt.custom4, '') AS "@custom4",
						ISNULL(pt.custom5, '') AS "@custom5",
						ISNULL(pt.custom6, '') AS "@custom6",
						ISNULL(pt.custom7, '') AS "@custom7",
						ISNULL(pt.custom8, '') AS "@custom8",
						ISNULL(pt.custom9, '') AS "@custom9",
						ISNULL(pt.custom10, '') AS "@custom10",
						ISNULL(pt.LPStartDate, '') AS "@LPStartDate",
						ISNULL(pt.LPEndDate, '') AS "@LPEndDate",
						ISNULL(pt.SPStartDate, '') AS "@SPStartDate",
						ISNULL(pt.SPEndDate, '') AS "@SPEndDate",
						ISNULL(pt.enrolldate, '') AS "@enrollDate",
						ISNULL(pt.PhaseTZOffset, '') AS "@phaseTZOffset",
						ISNULL(pt.orig_krtz, '') AS "@origKrtz",
						ISNULL(@ActivationDate, '') AS "@activationDate",						
						ISNULL(@suOrigKrtz, '') AS "@suOrigKrtz",						
						ISNULL(lst.updated, '') AS "@updated" 
				FROM lookup_pt pt WITH (NOLOCK) LEFT JOIN ig_Assignment am WITH (NOLOCK)
				ON pt.krpt = am.KRPT INNER JOIN lookup_domain dom WITH (NOLOCK) ON pt.krdom = dom.krdom
				INNER JOIN (SELECT krpt, MAX(receiptts) AS updated
				FROM #ServerTimeStamp
				GROUP BY krpt) lst ON pt.krpt = lst.krpt  
				WHERE pt.krpt = @KrPT
				FOR xml PATH('subject'));

	SELECT @XmlRow = COUNT(*) FROM @XML.nodes('subject') x(a);
		
	----SWAPI GetDataClin expects string data type 
	----Convert xml data type to string data type explicitly.
	IF @XmlRow > 0
		SET @String = CONVERT(NVARCHAR(MAX),@Xml);

	SELECT @String;
	
	DECLARE @ProcedureName varchar(50);
	SET @ProcedureName = OBJECT_NAME(@@PROCID);
	
	--Get version
	DECLARE @Version VARCHAR(10);
	SELECT @Version = procVersion
	FROM PDE_SQLVersion WITH (NOLOCK)
	WHERE procname = @ProcedureName;
	
	--Log data 
	EXEC [dbo].[insertStudySyncLogs] @ProcedureName, @Version, @KrPT, NULL, NULL, NULL, @String;
	
	DROP TABLE #ServerTimeStamp;
END
GO


SET ANSI_NULLS ON 
GO
SET QUOTED_IDENTIFIER OFF 
GO


IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'synd_GetSubjectByKrPT' AND TYPE = 'P')
BEGIN
	DECLARE @name as varchar(50)
	DECLARE @version as numeric(18,2)

	SELECT @name = 'synd_GetSubjectByKrPT', @version = 1.00
	EXEC [dbo].[PDE_UpdateSQLVersion] @name, @version
END
GO


GRANT EXECUTE ON [dbo].[synd_GetSubjectByKrPT] TO pht_server_read_only 
GO


IF NOT EXISTS (SELECT * FROM allowed_clin_procs WHERE proc_name='synd_GetSubjectByKrPT')
	INSERT INTO allowed_clin_procs (proc_name) values ('synd_GetSubjectByKrPT');
GO
