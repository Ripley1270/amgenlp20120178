/********************************************************************************************************
Stored procedure: synd_GetVisitsByKrPT
Description: Get latest visits for a subject 
Project: Syndication
History:
11Apr2016 Initial creation
26Apr2016 Use 0 instead of false if visit form is not deleted, 1 instead of true if visit form is deleted
*********************************************************************************************************/

IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'synd_GetVisitsByKrPT' AND TYPE = 'P')
   DROP PROCEDURE [dbo].[synd_GetVisitsByKrPT]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[synd_GetVisitsByKrPT]

@KrPT VARCHAR(36) 

AS

BEGIN

	SET NOCOUNT ON;

	DECLARE @VisitStart TABLE
	(
		visitId INT NULL,
		startDate DATETIME NULL,
		deleted TINYINT NULL,
		rk INT NULL
	);

	DECLARE @VisitEnd TABLE
	(
		visitId INT NULL,
		endDate DATETIME NULL,
		status VARCHAR(10) NULL, 
		deleted TINYINT NULL,
		rk INT NULL
	);

	INSERT INTO @VisitStart(visitId, startDate, deleted, rk)
	SELECT vs.EventID, 
	CAST(vs.EventStartDate AS DATETIME) AS startDate,
	CASE WHEN su.Deleted <> 0 THEN 1 ELSE su.deleted END AS deleted,
	ROW_NUMBER() OVER(PARTITION BY vs.eventId ORDER BY su.deleted, su.receiptts DESC) AS rk  
	FROM  ig_VisitStartDate vs WITH (NOLOCK) 
	INNER JOIN lookup_SU su WITH (NOLOCK)  ON su.sigorig = vs.sigorig 
	WHERE su.krPT = @KrPT;

	INSERT INTO @VisitEnd(visitId, endDate, status, deleted, rk)
	SELECT ve.EventID, 
	CAST(ve.EventEndDate AS DATETIME) AS endDate,
	ve.VisitStatus,
	CASE WHEN su.Deleted <> 0 THEN 1 ELSE su.deleted END AS deleted,
	ROW_NUMBER() OVER(PARTITION BY ve.eventId ORDER BY su.deleted, su.receiptts desc) as rk  
	FROM  ig_VisitEndDate ve WITH (NOLOCK) 
	INNER JOIN lookup_SU su WITH (NOLOCK)  ON su.sigorig = ve.sigorig 
	WHERE su.krPT = @KrPT;
	
	DECLARE @Xml XML;

	SELECT @Xml = (  
		SELECT visitId, startDate, endDate, status, startDeleted, endDeleted FROM (

		SELECT vs.visitId, vs.startDate, ve.endDate, ve.status, 
		vs.deleted AS startDeleted,
		--If visitStart is deleted and visitEnd exists, set endDeleted to 1 which is deleted   
		CASE WHEN (vs.deleted <> 0 and ve.rk IS NOT NULL) THEN 1 ELSE ve.deleted END AS endDeleted
		FROM @VisitStart vs LEFT JOIN @VisitEnd ve 
		ON vs.visitId = ve.visitId
		WHERE (vs.rk = 1 and ve.rk = 1) OR (vs.rk = 1 and ve.rk IS NULL)

		UNION

		SELECT ve.visitId, vs.startDate, ve.endDate, ve.status, vs.deleted AS startDeleted, ve.deleted AS endDeleted 
		FROM @VisitStart vs RIGHT JOIN @VisitEnd ve 
		ON vs.visitId = ve.visitId
		WHERE vs.rk IS NULL AND ve.rk = 1
    ) AS visits 
	FOR xml RAW('visit'), ROOT('visits')
	);
	
	--Return <visits/> 
	SET @Xml = ISNULL(@Xml, '<visits/>')
	SELECT @Xml;

	DECLARE @String NVARCHAR(MAX);
	SET @String = CONVERT(NVARCHAR(MAX),@Xml);

	DECLARE @ProcedureName varchar(50);
	SET @ProcedureName = OBJECT_NAME(@@PROCID);

	--Get version
	DECLARE @Version VARCHAR(10);
	SELECT @Version = procVersion
	FROM PDE_SQLVersion WITH (NOLOCK)
	WHERE procname = @ProcedureName;

	--Log data 
	EXEC [dbo].[insertStudySyncLogs] @ProcedureName, @Version, @KrPT, NULL, NULL, NULL, @String;

END

SET ANSI_NULLS ON 
GO
SET QUOTED_IDENTIFIER OFF 
GO


IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'synd_GetVisitsByKrPT' AND TYPE = 'P')
BEGIN
	DECLARE @name as varchar(50)
	DECLARE @version as numeric(18,2)

	SELECT @name = 'synd_GetVisitsByKrPT', @version = 1.00
	EXEC [dbo].[PDE_UpdateSQLVersion] @name, @version
END
GO


GRANT EXECUTE ON [dbo].[synd_GetVisitsByKrPT] TO pht_server_read_only 
GO


IF NOT EXISTS (SELECT * FROM allowed_clin_procs WHERE proc_name='synd_GetVisitsByKrPT')
	INSERT INTO allowed_clin_procs (proc_name) values ('synd_GetVisitsByKrPT');
GO

