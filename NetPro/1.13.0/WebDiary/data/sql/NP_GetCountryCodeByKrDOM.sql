/*****************************************************************************
Stored procedure: NP_GetCountryCodeByKrDom 
Description: Get country code by KrDom 
Project: NetPRO
History:
1.00 30SEP2017 Initial creation
*****************************************************************************/

IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'NP_GetCountryCodeByKrDom' AND TYPE = 'P')
   DROP PROCEDURE [dbo].[NP_GetCountryCodeByKrDom]
GO

CREATE PROCEDURE [dbo].[NP_GetCountryCodeByKrDom]

@KrDom VARCHAR(255) 

AS

BEGIN
	SET NOCOUNT ON;
	
	DECLARE @String VARCHAR(254);
	SET @String = '';
	
	SELECT @String = ISNULL(country, '')
	FROM lookup_domain WITH (NOLOCK)	
	WHERE krdom = @KrDOM;
	
	SELECT @String;
	
	DECLARE @ProcedureName varchar(50);
	SET @ProcedureName = OBJECT_NAME(@@PROCID);
	
	--Get version
	DECLARE @Version VARCHAR(10);
	SELECT @Version = procVersion
	FROM PDE_SQLVersion WITH (NOLOCK)
	WHERE procname = @ProcedureName;
	
	--Log data 
	EXEC [dbo].[insertStudySyncLogs] @ProcedureName, @Version, @KrDom, NULL, NULL, NULL, @String;	
END
GO

IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'NP_GetCountryCodeByKrDom' AND TYPE = 'P')
BEGIN
	DECLARE @name as varchar(50)
	DECLARE @version as numeric(18,2)

	SELECT @name = 'NP_GetCountryCodeByKrDom', @version = 1.00
	EXEC [dbo].[PDE_UpdateSQLVersion] @name, @version
END
GO


GRANT EXECUTE ON [dbo].[NP_GetCountryCodeByKrDom] TO pht_server_read_only 
GO


IF NOT EXISTS (SELECT * FROM allowed_clin_procs WHERE proc_name='NP_GetCountryCodeByKrDom')
	INSERT INTO allowed_clin_procs (proc_name) values ('NP_GetCountryCodeByKrDom');
GO
