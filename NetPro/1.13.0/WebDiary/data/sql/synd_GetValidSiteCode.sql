/*****************************************************************************
Stored procedure: synd_GetValidSiteCode
Description: Get valid sitecode and return xml as string data type
Project: Syndication
History:
17Jul2015 Initial creation
21Oct2015 Optimized sql
*****************************************************************************/

IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'synd_GetValidSiteCode' AND TYPE = 'P')
   DROP PROCEDURE [dbo].[synd_GetValidSiteCode]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[synd_GetValidSiteCode]

@KrDom varchar(255) = NULL

AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @Xml XML;
	DECLARE @XmlRow INT;
	DECLARE @String NVARCHAR(MAX);
		
	SET @String = '';

	SET @Xml = (SELECT krdom, ISNULL(sitecode, '') AS siteCode 
		FROM lookup_domain WITH (NOLOCK) 
		WHERE issite = 1 AND deleted = 0 AND siteCode != 'mlpadmin' AND krdom = IsNull(@KrDom, krdom) 
		ORDER BY sitecode ASC FOR xml PATH('site'), ROOT('sites'));

	SELECT @XmlRow = COUNT(*) FROM @XML.nodes('/sites/*') x(a);
	--SWAPI GetDataClin expects string data type 
	--Convert xml data type to string data type explicitly. Implicit conversion is not allowed
	IF @XmlRow > 0
		SET @String = CONVERT(NVARCHAR(MAX),@Xml);

	SELECT @String;
END
GO

SET ANSI_NULLS ON 
GO
SET QUOTED_IDENTIFIER OFF 
GO

IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'synd_GetValidSiteCode' AND TYPE = 'P')
BEGIN
	DECLARE @name as varchar(50)
	DECLARE @version as numeric(18,2)

	SELECT @name = 'synd_GetValidSiteCode', @version = 1.00
	EXEC [dbo].[PDE_UpdateSQLVersion] @name, @version
END
GO


GRANT EXECUTE ON [dbo].[synd_GetValidSiteCode] TO pht_server_read_only 
GO


IF NOT EXISTS (SELECT * FROM allowed_clin_procs WHERE proc_name='synd_GetValidSiteCode')
	INSERT INTO allowed_clin_procs (proc_name) values ('synd_GetValidSiteCode');
GO





   




