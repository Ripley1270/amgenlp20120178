﻿namespace Checkbox.Forms.Items.Configuration
{
    using Prezza.Framework.Common;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class RedirectItemTextDecorator : LocalizableResponseItemTextDecorator
    {
        private string _linkText;
        private bool _linkTextChanged;

        public RedirectItemTextDecorator(RedirectItemData data, string language) : base(data, language)
        {
            this._linkText = string.Empty;
            this._linkTextChanged = false;
        }

        protected override void CopyLocalizedText(ItemData data)
        {
            base.CopyLocalizedText(data);
            ArgumentValidation.CheckExpectedType(data, typeof(RedirectItemData));
            string uRLTextID = ((RedirectItemData) data).URLTextID;
            if ((uRLTextID != null) && (uRLTextID.Trim() != string.Empty))
            {
                Dictionary<string, string> allTexts = this.GetAllTexts(uRLTextID);
                foreach (string str2 in allTexts.Keys)
                {
                    this.SetText(this.Data.URLTextID, allTexts[str2], str2);
                }
            }
        }

        protected override void SetLocalizedTexts()
        {
            base.SetLocalizedTexts();
            if (this._linkTextChanged)
            {
                this.SetText(this.Data.URLTextID, this._linkText);
            }
        }

        public RedirectItemData Data
        {
            get
            {
                return (RedirectItemData) base.Data;
            }
        }

        public string LinkText
        {
            get
            {
                if (!this._linkTextChanged && (this.Data.URLTextID != string.Empty))
                {
                    return this.GetText(this.Data.URLTextID);
                }
                return this._linkText;
            }
            set
            {
                this._linkText = value;
                this._linkTextChanged = true;
            }
        }
    }
}

