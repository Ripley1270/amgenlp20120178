﻿namespace Checkbox.Forms.Logic
{
    using Checkbox.Common;
    using System;
    using System.Globalization;

    public class OperandValue : IComparable<OperandValue>
    {
        private OperandDataType? _dataType;
        private object _value;

        protected virtual int CompareOtherValues(object value1, object value2)
        {
            throw new NotImplementedException("Comparison only supported when extending operands.");
        }

        public virtual int CompareTo(OperandValue other)
        {
            if ((this.Value == null) && (other.Value == null))
            {
                return 0;
            }
            if ((this.Value == null) && (other.Value != null))
            {
                return -1;
            }
            if ((this.Value != null) && (other.Value == null))
            {
                return 1;
            }
            return this.DoCompareTo(other);
        }

        public virtual bool Contains(OperandValue otherOperandValue)
        {
            return ((this.HasValue && otherOperandValue.HasValue) && this.Value.ToString().ToLower().Contains(otherOperandValue.Value.ToString()));
        }

        protected int DoCompareTo(OperandValue other)
        {
            OperandDataType type = this.DataType & other.DataType;
            string str = this.Value.ToString().Trim();
            string str2 = other.Value.ToString().Trim();
            if ((type & OperandDataType.Integer) == OperandDataType.Integer)
            {
                int num = Utilities.GetInt32(str).Value;
                int num2 = Utilities.GetInt32(str2).Value;
                return num.CompareTo(num2);
            }
            if ((type & OperandDataType.Double) == OperandDataType.Double)
            {
                double num3 = Utilities.GetDouble(str, new CultureInfo[0]).Value;
                double num4 = Utilities.GetDouble(str2, new CultureInfo[0]).Value;
                return num3.CompareTo(num4);
            }
            if ((type & OperandDataType.Date) == OperandDataType.Date)
            {
                DateTime? date;
                DateTime? nullable2;
                CultureInfo currentCulture = CultureInfo.CurrentCulture;
                CultureInfo info2 = new CultureInfo("en-US");
                CultureInfo info3 = new CultureInfo("fr-FR");
                if (Utilities.IsDate(str, new CultureInfo[] { currentCulture }) && Utilities.IsDate(str2, new CultureInfo[] { currentCulture }))
                {
                    date = Utilities.GetDate(str, new CultureInfo[] { currentCulture });
                    nullable2 = Utilities.GetDate(str2, new CultureInfo[] { currentCulture });
                }
                else if (Utilities.IsDate(str, new CultureInfo[] { info2 }) && Utilities.IsDate(str2, new CultureInfo[] { info2 }))
                {
                    date = Utilities.GetDate(str, new CultureInfo[] { info2 });
                    nullable2 = Utilities.GetDate(str2, new CultureInfo[] { info2 });
                }
                else if (Utilities.IsDate(str, new CultureInfo[] { info3 }) && Utilities.IsDate(str2, new CultureInfo[] { info3 }))
                {
                    date = Utilities.GetDate(str, new CultureInfo[] { info3 });
                    nullable2 = Utilities.GetDate(str2, new CultureInfo[] { info3 });
                }
                else
                {
                    date = Utilities.GetDate(str, new CultureInfo[0]);
                    nullable2 = Utilities.GetDate(str2, new CultureInfo[0]);
                }
                return date.Value.CompareTo(nullable2.Value);
            }
            if ((type & OperandDataType.Currency) == OperandDataType.Currency)
            {
                double num5 = Utilities.GetCurrencyNumericValue(str, new CultureInfo[0]).Value;
                double num6 = Utilities.GetCurrencyNumericValue(str2, new CultureInfo[0]).Value;
                return num5.CompareTo(num6);
            }
            if ((type & OperandDataType.Other) == OperandDataType.Other)
            {
                return this.CompareOtherValues(this.Value, other.Value);
            }
            return string.Compare(str, str2, true);
        }

        protected virtual OperandDataType GetDataType()
        {
            OperandDataType type = OperandDataType.String;
            if (this.Value != null)
            {
                string str = this.Value.ToString().Trim();
                if (Utilities.IsInt32(str))
                {
                    type |= OperandDataType.Integer;
                }
                if (Utilities.IsDouble(str, new CultureInfo[0]))
                {
                    type |= OperandDataType.Double;
                }
                if (Utilities.IsDate(str, new CultureInfo[0]))
                {
                    type |= OperandDataType.Date;
                }
                if (Utilities.IsCurrency(str, new CultureInfo[0]))
                {
                    type |= OperandDataType.Currency;
                }
            }
            return type;
        }

        public virtual void Initialize(object value)
        {
            this._value = value;
        }

        public OperandDataType DataType
        {
            get
            {
                if (!this._dataType.HasValue)
                {
                    this._dataType = new OperandDataType?(this.GetDataType());
                }
                return this._dataType.Value;
            }
        }

        public virtual bool HasValue
        {
            get
            {
                return ((this.Value != null) && (this.Value.ToString().Trim().Length != 0));
            }
        }

        public virtual object Value
        {
            get
            {
                return this._value;
            }
        }
    }
}

