﻿namespace Checkbox.Forms.Items.UI
{
    using System;

    public enum LabelPosition
    {
        Right,
        Left,
        Bottom,
        Top
    }
}

