﻿namespace Checkbox.Globalization.Text.Providers
{
    using Checkbox.Globalization.Text;
    using Prezza.Framework.Caching;
    using Prezza.Framework.Common;
    using Prezza.Framework.Configuration;
    using Prezza.Framework.Data;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Text.RegularExpressions;

    public class DefaultLanguageTextProvider : ITextProvider, IConfigurationProvider
    {
        private CacheManager _cacheManager;
        private DefaultLanguageTextProviderData _config;
        private string _configurationName;

        public Dictionary<string, string> GetAllTexts(string textIdentifier)
        {
            ArgumentValidation.CheckForEmptyString(textIdentifier, "textIdentifier");
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            string text = this.GetText(textIdentifier, this._config.DefaultLanguage);
            if (text != null)
            {
                dictionary.Add(this._config.DefaultLanguage, text);
            }
            return dictionary;
        }

        public DataTable GetMatchingTextData(params string[] matchExpressions)
        {
            DataTable table = new DataTable {
                TableName = "MatchingTexts"
            };
            table.Columns.Add("TextId", typeof(string));
            table.Columns.Add("LanguageCode", typeof(string));
            table.Columns.Add("TextValue", typeof(string));
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Text_GetAll");
            DataSet set = database.ExecuteDataSet(storedProcCommandWrapper);
            if (set.Tables.Count == 3)
            {
                DataTable table1 = set.Tables[0];
                DataTable table2 = set.Tables[1];
                DataTable table3 = set.Tables[2];
                foreach (DataRow row in table2.Rows)
                {
                    string input = (string) row["TextId"];
                    foreach (string str2 in matchExpressions)
                    {
                        Regex regex = new Regex(str2);
                        if (regex.Match(input).Success)
                        {
                            string str4;
                            string defaultLanguage = this._config.DefaultLanguage;
                            DataRow[] rowArray = table3.Select("LanguageCode = '" + defaultLanguage + "' AND TextId = '" + input + "'");
                            if (rowArray.Length > 0)
                            {
                                str4 = (string) rowArray[0]["TextValue"];
                            }
                            else
                            {
                                str4 = string.Empty;
                            }
                            DataRow row2 = table.NewRow();
                            row2["TextId"] = input;
                            row2["LanguageCode"] = defaultLanguage;
                            row2["TextValue"] = str4;
                            table.Rows.Add(row2);
                        }
                    }
                }
            }
            return table;
        }

        public string GetText(string textIdentifier, string languageCode)
        {
            ArgumentValidation.CheckForEmptyString(textIdentifier, "textIdentifier");
            if ((this._cacheManager != null) && this._cacheManager.Contains(textIdentifier.ToLower()))
            {
                return (string) this._cacheManager[textIdentifier.ToLower()];
            }
            string str = string.Empty;
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Text_GetText");
            storedProcCommandWrapper.AddInParameter("TextID", DbType.String, textIdentifier);
            storedProcCommandWrapper.AddInParameter("LanguageCode", DbType.String, this._config.DefaultLanguage);
            object obj2 = database.ExecuteScalar(storedProcCommandWrapper);
            if (obj2 != DBNull.Value)
            {
                str = (string) obj2;
            }
            str = str ?? string.Empty;
            if (this._cacheManager != null)
            {
                this._cacheManager.Add(textIdentifier.ToLower(), str);
            }
            return str;
        }

        public DataTable GetTextData(string textIdentifier)
        {
            DataTable table = new DataTable();
            table.Columns.Add("TextID", typeof(string));
            table.Columns.Add("LanguageCode", typeof(string));
            table.Columns.Add("TextValue", typeof(string));
            string text = this.GetText(textIdentifier, this._config.DefaultLanguage);
            if (text == null)
            {
                text = string.Empty;
            }
            table.Rows.Add(new object[] { textIdentifier, this._config.DefaultLanguage, text });
            table.AcceptChanges();
            return table;
        }

        public void Initialize(ConfigurationBase config)
        {
            this._config = (DefaultLanguageTextProviderData) config;
            if (this._config.CacheManagerName != string.Empty)
            {
                this._cacheManager = CacheFactory.GetCacheManager(this._config.CacheManagerName);
            }
            else
            {
                this._cacheManager = null;
            }
        }

        public void SetText(string textIdentifier, string languageCode, string textValue)
        {
            ArgumentValidation.CheckForEmptyString(textIdentifier, "textIdentifier");
            if (textValue == null)
            {
                textValue = string.Empty;
            }
            if (this._cacheManager != null)
            {
                this._cacheManager.Add(textIdentifier.ToLower(), textValue);
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Text_Set");
            storedProcCommandWrapper.AddInParameter("TextID", DbType.String, textIdentifier);
            storedProcCommandWrapper.AddInParameter("LanguageCode", DbType.String, this._config.DefaultLanguage);
            storedProcCommandWrapper.AddInParameter("TextValue", DbType.String, textValue);
            database.ExecuteNonQuery(storedProcCommandWrapper);
        }

        public string ConfigurationName
        {
            get
            {
                return this._configurationName;
            }
            set
            {
                this._configurationName = value;
            }
        }
    }
}

