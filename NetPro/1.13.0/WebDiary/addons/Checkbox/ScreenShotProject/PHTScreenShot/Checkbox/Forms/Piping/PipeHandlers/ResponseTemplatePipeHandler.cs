﻿namespace Checkbox.Forms.Piping.PipeHandlers
{
    using Checkbox.Forms;
    using Checkbox.Forms.Piping.Tokens;
    using Checkbox.Management;
    using System;

    public class ResponseTemplatePipeHandler : PipeHandler
    {
        public override string GetTokenValue(Token token, object context)
        {
            string pipePrefix = ApplicationManager.AppSettings.PipePrefix;
            if (token.TokenName.ToLower() == (pipePrefix + "responsetemplateguid"))
            {
                if (context is ResponseTemplate)
                {
                    return ((ResponseTemplate) context).GUID.ToString();
                }
                if (context is ResponseProperties)
                {
                    object objectValue = ((ResponseProperties) context).GetObjectValue("ResponseTemplateID");
                    if ((objectValue != null) && (objectValue is int))
                    {
                        Guid? responseTemplateGUID = ResponseTemplateManager.GetResponseTemplateGUID((int) objectValue);
                        if (responseTemplateGUID.HasValue)
                        {
                            return responseTemplateGUID.ToString();
                        }
                    }
                }
            }
            else if (token.TokenName.ToLower() == (pipePrefix + "responsetemplateid"))
            {
                if (context is ResponseProperties)
                {
                    return ((ResponseProperties) context).GetStringValue("ResponseTemplateID");
                }
                if (context is ResponseTemplate)
                {
                    return ((ResponseTemplate) context).ID.ToString();
                }
            }
            return string.Empty;
        }
    }
}

