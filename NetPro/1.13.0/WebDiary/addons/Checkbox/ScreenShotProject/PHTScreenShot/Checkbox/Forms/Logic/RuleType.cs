﻿namespace Checkbox.Forms.Logic
{
    using System;

    public enum RuleType
    {
        ItemCondition,
        PageCondition,
        PageBranchCondition
    }
}

