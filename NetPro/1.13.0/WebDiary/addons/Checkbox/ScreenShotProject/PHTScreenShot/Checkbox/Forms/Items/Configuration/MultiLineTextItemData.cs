﻿using Checkbox.Forms.Items;
using Prezza.Framework.Data;
using Prezza.Framework.ExceptionHandling;
using System;
using System.Data;

namespace Checkbox.Forms.Items.Configuration
{
    [Serializable]
    public class MultiLineTextItemData : TextItemData
    {
        private PHTScreenShotSession _screenShotSession;
        
        protected override void Create(IDbTransaction t)
        {
            base.Create(t);
            if (base.ID <= 0)
            {
                throw new Exception("No DataID specified.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_InsertMLText");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("TextID", DbType.String, this.TextID);
            storedProcCommandWrapper.AddInParameter("SubTextID", DbType.String, this.SubTextID);
            storedProcCommandWrapper.AddInParameter("IsRequired", DbType.Int32, this.IsRequired ? 1 : 0);
            storedProcCommandWrapper.AddInParameter("DefaultTextID", DbType.String, this.DefaultTextID);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        protected override Item CreateItem()
        {
            return new MultiLineTextBoxItem();
        }

        public override ItemTextDecorator CreateTextDecorator(string languageCode)
        {
            return new TextItemDecorator(this, languageCode);
        }

        protected override DataSet GetConcreteConfigurationDataSet()
        {
            if (base.ID <= 0)
            {
                throw new Exception("No DataID specified.");
            }
            try
            {
                string calledStoredProcedure = string.Empty;
                this._screenShotSession = new PHTScreenShotSession();
                if (this._screenShotSession.ScreenShotMode)
                    calledStoredProcedure = "PHTScreenShotItemDataGetMLText";
                else
                    calledStoredProcedure = "ckbx_ItemData_GetMLText";

                Database database = DatabaseFactory.CreateDatabase();
                //DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_GetMLText");
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper(calledStoredProcedure);
                storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
                DataSet concreteConfigurationDataSet = base.GetConcreteConfigurationDataSet();
                database.LoadDataSet(storedProcCommandWrapper, concreteConfigurationDataSet, this.DataTableName);
                return concreteConfigurationDataSet;
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessProtected"))
                {
                    throw;
                }
                return null;
            }
        }

        protected override void LoadFromDataRow(DataRow data)
        {
            if (data == null)
            {
                throw new Exception("DataRow cannot be null.");
            }
            try
            {
                this.IsRequired = (data["IsRequired"] != DBNull.Value) && (Convert.ToInt32(data["IsRequired"]) == 1);
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessProtected"))
                {
                    throw;
                }
            }
        }

        protected override void Update(IDbTransaction t)
        {
            base.Update(t);
            if (base.ID <= 0)
            {
                throw new Exception("No DataID specified.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_UpdateMLText");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("TextID", DbType.String, this.TextID);
            storedProcCommandWrapper.AddInParameter("SubTextID", DbType.String, this.SubTextID);
            storedProcCommandWrapper.AddInParameter("IsRequired", DbType.Int32, this.IsRequired ? 1 : 0);
            storedProcCommandWrapper.AddInParameter("DefaultTextID", DbType.String, this.DefaultTextID);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        public override string DataTableName
        {
            get
            {
                return "MultiLineTextItemData";
            }
        }
    }
}

