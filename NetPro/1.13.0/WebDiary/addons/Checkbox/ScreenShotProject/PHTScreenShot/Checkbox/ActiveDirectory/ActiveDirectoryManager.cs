﻿namespace Checkbox.ActiveDirectory
{
    using Checkbox.Management;
    using System;
    using System.DirectoryServices;
    using System.Runtime.InteropServices;

    public class ActiveDirectoryManager
    {
        public bool CanConnectToNamingContext(string context)
        {
            try
            {
                DirectoryEntry entry = new DirectoryEntry("LDAP://" + context);
                string str = (string) entry.Properties["name"].Value;
                if ((str == null) || string.Empty.Equals(str.Trim()))
                {
                    return false;
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public string FindDefaultNamingContext()
        {
            string str2;
            try
            {
                DirectoryEntry entry = new DirectoryEntry("LDAP://RootDSE");
                str2 = (string) entry.Properties["defaultNamingContext"].Value;
            }
            catch (COMException exception)
            {
                if (exception.Message.StartsWith("The specified domain either does not exist or could not be contacted."))
                {
                    throw new Exception("The specified domain either does not exist or could not be contacted. It is possible this error has been caused by account permissions. Checkbox is currently running under the " + Environment.UserName + ".");
                }
                throw;
            }
            return str2;
        }

        public static string GetDomain()
        {
            string activeDirectoryNamingContext = ApplicationManager.AppSettings.ActiveDirectoryNamingContext;
            string str2 = string.Empty;
            if ((activeDirectoryNamingContext == null) || string.Empty.Equals(activeDirectoryNamingContext.Trim()))
            {
                return string.Empty;
            }
            foreach (string str3 in activeDirectoryNamingContext.Split(new char[] { ',' }))
            {
                if (str3.Trim().StartsWith("DC="))
                {
                    return str3.Replace("DC=", "");
                }
            }
            return str2;
        }

        public static string GetQualifiedUsername(string username)
        {
            if (username == null)
            {
                return null;
            }
            username = username.Replace("/", @"\");
            if (username.Contains(@"\"))
            {
                return username;
            }
            return string.Format(@"{0}\{1}", GetDomain(), username);
        }

        public static string GetUsername(string username)
        {
            char? nullable = null;
            if (username == null)
            {
                return null;
            }
            if (username.Contains(@"\"))
            {
                nullable = '\\';
            }
            else if (username.Contains("/"))
            {
                nullable = '/';
            }
            char? nullable2 = nullable;
            int? nullable4 = nullable2.HasValue ? new int?(nullable2.GetValueOrDefault()) : null;
            if (nullable4.HasValue)
            {
                string[] strArray = username.Split(new char[] { nullable.Value });
                if (strArray.Length > 1)
                {
                    return strArray[1];
                }
            }
            return username;
        }

        public static AuthenticationTypes FastAuthenticationFlags
        {
            get
            {
                return (AuthenticationTypes.ServerBind | AuthenticationTypes.FastBind);
            }
        }

        public static AuthenticationTypes SecureAuthenticationFlags
        {
            get
            {
                return (AuthenticationTypes.Sealing | AuthenticationTypes.Signing | AuthenticationTypes.Secure);
            }
        }
    }
}

