﻿namespace Checkbox.Forms.Logic
{
    using Checkbox.Forms.Items;
    using System;
    using System.Collections.Generic;

    public class SelectItemOperand : Operand
    {
        private readonly SelectItem _item;

        public SelectItemOperand(SelectItem item)
        {
            this._item = item;
        }

        public override OperandValue CreateOperandValue()
        {
            return new SelectOperandValue();
        }

        protected List<int> GetOptionIds()
        {
            List<ListOption> selectedOptions = this._item.SelectedOptions;
            List<int> list2 = new List<int>();
            foreach (ListOption option in selectedOptions)
            {
                list2.Add(option.ID);
            }
            return list2;
        }

        public override void InitializeOperandValue(OperandValue operandValue, object initializationValue)
        {
            base.InitializeOperandValue(operandValue, initializationValue);
            if (operandValue is SelectOperandValue)
            {
                foreach (ListOption option in this._item.Options)
                {
                    ((SelectOperandValue) operandValue).TextValues[option.ID] = option.Text;
                }
            }
        }

        protected override object ProtectedGetValue(RuleEventTrigger trigger)
        {
            return this.GetOptionIds();
        }
    }
}

