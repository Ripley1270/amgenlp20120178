﻿namespace Checkbox.Forms.Items
{
    using Checkbox.Forms.Items.Configuration;
    using Checkbox.Forms.Validation;
    using Prezza.Framework.Common;
    using System;
    using System.Collections.Specialized;

    [Serializable]
    public abstract class TextItem : LabelledItem
    {
        private string _customFormatId;
        private string _defaultText;
        private AnswerFormat _format;
        private int? _maxLength;

        protected TextItem()
        {
        }

        public override void Configure(ItemData configuration, string languageCode)
        {
            base.Configure(configuration, languageCode);
            ArgumentValidation.CheckExpectedType(configuration, typeof(TextItemData));
            TextItemData data = (TextItemData) configuration;
            this._defaultText = this.GetText(data.DefaultTextID);
            this._format = data.Format;
            this._customFormatId = data.CustomFormatId;
            this._maxLength = data.MaxLength;
        }

        public override string GetAnswer()
        {
            if (this.HasAnswer)
            {
                return base.GetAnswer();
            }
            return this.DefaultText;
        }

        protected override NameValueCollection GetMetaDataValuesForXmlSerialization()
        {
            NameValueCollection metaDataValuesForXmlSerialization = base.GetMetaDataValuesForXmlSerialization();
            metaDataValuesForXmlSerialization["answerFormat"] = this.Format.ToString();
            metaDataValuesForXmlSerialization["maxLength"] = this.MaxLength.HasValue ? this.MaxLength.ToString() : null;
            metaDataValuesForXmlSerialization["defaultText"] = this.DefaultText;
            return metaDataValuesForXmlSerialization;
        }

        protected override bool ValidateAnswers()
        {
            TextAnswerValidator validator = new TextAnswerValidator();
            if (!validator.Validate(this))
            {
                base.ValidationErrors.Add(validator.GetMessage(base.LanguageCode));
                return false;
            }
            return true;
        }

        public string CustomFormatId
        {
            get
            {
                return this._customFormatId;
            }
        }

        public string DefaultText
        {
            get
            {
                return this.GetPipedText("DefaultText", this._defaultText);
            }
            set
            {
                this._defaultText = value;
            }
        }

        public AnswerFormat Format
        {
            get
            {
                return this._format;
            }
            protected set
            {
                this._format = value;
            }
        }

        public int? MaxLength
        {
            get
            {
                return this._maxLength;
            }
            protected set
            {
                this._maxLength = value;
            }
        }
    }
}

