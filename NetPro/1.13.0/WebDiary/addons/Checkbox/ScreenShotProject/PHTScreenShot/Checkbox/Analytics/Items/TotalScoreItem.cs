﻿namespace Checkbox.Analytics.Items
{
    using Checkbox.Analytics.Data;
    using System;

    [Serializable]
    public class TotalScoreItem : AnalysisItem
    {
        protected override object GeneratePreviewData()
        {
            return this.ProcessData(true);
        }

        protected override object ProcessData()
        {
            return this.ProcessData(false);
        }

        protected virtual object ProcessData(bool isPreview)
        {
            FrequencyAnalysisData data = new FrequencyAnalysisData();
            long answerId = 0x3e8L;
            foreach (int num2 in this.SourceItemIDs)
            {
                if (!isPreview)
                {
                    foreach (ItemAnswer answer in base.GetItemAnswers(num2))
                    {
                        if (answer.AnswerId > answerId)
                        {
                            answerId = answer.AnswerId;
                        }
                        if (answer.OptionId.HasValue)
                        {
                            data.Increment(base.GetOptionPoints(num2, answer.OptionId.Value).ToString());
                        }
                    }
                }
                else
                {
                    for (int i = 1; i < 100; i += 5)
                    {
                        double a = (0.062831853071795868 * i) - 1.5707963267948966;
                        double num5 = Math.Sin(a);
                        int num6 = Convert.ToInt32((double) ((100.0 * num5) + 100.0));
                        for (int j = 0; j < num6; j++)
                        {
                            data.Increment(i.ToString());
                        }
                    }
                }
                base.ResponseCounts[num2] = data.ResponseCount;
                answerId += 1L;
            }
            base.DataProcessed = true;
            return data;
        }
    }
}

