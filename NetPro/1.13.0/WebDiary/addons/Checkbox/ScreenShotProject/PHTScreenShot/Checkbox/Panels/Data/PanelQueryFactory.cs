﻿namespace Checkbox.Panels.Data
{
    using Checkbox.Security;
    using Checkbox.Security.Data;
    using Prezza.Framework.Data;
    using Prezza.Framework.Data.QueryCriteria;
    using Prezza.Framework.Data.QueryParameters;
    using Prezza.Framework.Security.Principal;
    using System;

    internal class PanelQueryFactory
    {
        private static QueryCriterion CreateFilterCriterion(string filterField, string filterValue, string tableName)
        {
            return new QueryCriterion(new SelectParameter(filterField, string.Empty, tableName), CriteriaOperator.Like, new LiteralParameter("'%" + filterValue + "%'"));
        }

        private static SubqueryParameter CreatePagingSubQueryParameter(SelectQuery query, string selectFieldName, string selectFieldTable, int pageNumber, int resultsPerPage)
        {
            int count = (pageNumber - 1) * resultsPerPage;
            SelectQuery subQuery = (SelectQuery) query.Clone();
            subQuery.SelectParameters = null;
            subQuery.AddParameter(selectFieldName, string.Empty, selectFieldTable);
            if (count > 0)
            {
                subQuery.AddSelectTopCriterion(count);
            }
            return new SubqueryParameter(subQuery);
        }

        public static SelectQuery GetAvailableEmailListCountQuery(ExtendedPrincipal currentPrincipal, string filterField, string filterValue, CriteriaCollection filterCollection)
        {
            SelectQuery query = GetSelectAvailableEmailListsQuery(currentPrincipal, -1, -1, filterField, filterValue, null, false, filterCollection, PermissionJoin.All, new string[] { "EmailList.View" });
            query.SelectParameters = null;
            query.SelectCriteria = null;
            query.AddParameter(new CountParameter("RecordCount", "PanelID", new SelectDistinctCriterion()));
            return query;
        }

        internal static SelectQuery GetSelectAllEmailListsQuery()
        {
            return GetSelectAllEmailListsQuery(null);
        }

        internal static SelectQuery GetSelectAllEmailListsQuery(CriteriaCollection filterCollection)
        {
            SelectQuery query = new SelectQuery("ckbx_Panel");
            query.AddAllParameter("ckbx_Panel");
            SelectQuery query2 = new SelectQuery("ckbx_EmailListPanel");
            query2.AddCountParameter("PanelID", null);
            query2.AddCriterion("ckbx_EmailListPanel", "PanelID", CriteriaOperator.EqualTo, "ckbx_Panel", "PanelID");
            query.AddLiteralParameter("(" + query2.ToString() + ")", "AddressCount");
            query.AddCriterion(new QueryCriterion(new SelectParameter("PanelTypeID", null, "ckbx_Panel"), CriteriaOperator.EqualTo, new LiteralParameter(3)));
            if (filterCollection != null)
            {
                query.AddCriteriaCollection(filterCollection);
            }
            return query;
        }

        internal static SelectQuery GetSelectAvailableEmailListsQuery(ExtendedPrincipal currentPrincipal, PermissionJoin permissionJoinType, params string[] permissions)
        {
            if ((permissions == null) || (permissions.Length == 0))
            {
                throw new ArgumentException("GetSelectAvailableEmailListsQuery was called with a null or empty permissions.", "permissions");
            }
            if (currentPrincipal == null)
            {
                throw new ArgumentException("GetSelectAvailableEmailListsQuery was called with a null principal.");
            }
            if (currentPrincipal.IsInRole("System Administrator"))
            {
                return GetSelectAllEmailListsQuery();
            }
            return QueryFactory.GetSelectEntitiesWithPermissionQuery("ckbx_Panel", "PanelID", "AclID", currentPrincipal.AclTypeIdentifier, currentPrincipal.AclEntryIdentifier, "DefaultPolicy", permissionJoinType, permissions);
        }

        internal static SelectQuery GetSelectAvailableEmailListsQuery(ExtendedPrincipal currentPrincipal, int resultsPerPage, int pageNumber, string filterField, string filterValue, string sortField, bool sortAscending, CriteriaCollection filterCollection, PermissionJoin permissionJoinType, params string[] permissions)
        {
            SelectQuery selectAllEmailListsQuery;
            if ((permissions == null) || (permissions.Length == 0))
            {
                throw new ArgumentException("GetSelectAvailableEmailListsQuery was called with a null or empty permissions.", "permissions");
            }
            if (currentPrincipal == null)
            {
                throw new ArgumentException("GetSelectAvailableEmailListsQuery was called with a null principal.");
            }
            if (currentPrincipal.IsInRole("System Administrator"))
            {
                selectAllEmailListsQuery = GetSelectAllEmailListsQuery();
            }
            else
            {
                selectAllEmailListsQuery = QueryFactory.GetSelectEntitiesWithPermissionQuery("ckbx_Panel", "PanelID", "AclID", currentPrincipal.AclTypeIdentifier, currentPrincipal.AclEntryIdentifier, "DefaultPolicy", permissionJoinType, permissions);
            }
            if (selectAllEmailListsQuery != null)
            {
                if (filterCollection != null)
                {
                    selectAllEmailListsQuery.AddCriteriaCollection(filterCollection);
                }
                bool flag = (filterField == null) || (filterField == string.Empty);
                bool flag2 = (filterValue == null) || (filterValue == string.Empty);
                if (!flag && !flag2)
                {
                    selectAllEmailListsQuery.AddCriterion(CreateFilterCriterion(filterField, filterValue, "ckbx_Panel"));
                }
                if ((resultsPerPage > 0) && (pageNumber > 0))
                {
                    selectAllEmailListsQuery.AddSelectTopCriterion(resultsPerPage);
                    selectAllEmailListsQuery.CriteriaJoinType = CriteriaJoinType.And;
                    if (pageNumber > 1)
                    {
                        selectAllEmailListsQuery.AddCriterion(new QueryCriterion(new SelectParameter("PanelID", string.Empty, "ckbx_Panel"), CriteriaOperator.NotIn, CreatePagingSubQueryParameter(selectAllEmailListsQuery, "PanelID", "ckbx_Panel", pageNumber, resultsPerPage)));
                    }
                }
            }
            return selectAllEmailListsQuery;
        }
    }
}

