﻿namespace Checkbox.Forms
{
    using Checkbox.Management;
    using Prezza.Framework.Security.Principal;
    using System;
    using System.Runtime.CompilerServices;

    public abstract class ResponseSession : IResponseSession
    {
        private Guid? _anonymousRespondentGuid;
        private readonly bool _isFormPost;

        protected ResponseSession(bool isFormPost)
        {
            this._isFormPost = isFormPost;
        }

        public abstract void ClearPersistedValues();
        protected abstract Guid? GetAnonymousRespondentGuid();
        public abstract string GetRespondentLanguageCode(string source, string variableName);
        public abstract void Load();
        public abstract void Save();
        protected abstract void SetAnonymousRespondentGuid(Guid theGuid, bool setCookie);

        public virtual bool AdminEditMode { get; set; }

        public virtual Guid? AnonymousRespondentGuid
        {
            get
            {
                if (this._anonymousRespondentGuid.HasValue)
                {
                    return this._anonymousRespondentGuid;
                }
                Guid? anonymousRespondentGuid = null;
                if (ApplicationManager.AppSettings.SessionMode != AppSettings.SessionType.Cookieless)
                {
                    anonymousRespondentGuid = this.GetAnonymousRespondentGuid();
                }
                if (!anonymousRespondentGuid.HasValue)
                {
                    anonymousRespondentGuid = new Guid?(Guid.NewGuid());
                }
                this.AnonymousRespondentGuid = anonymousRespondentGuid;
                return anonymousRespondentGuid;
            }
            set
            {
                if (value.HasValue)
                {
                    this._anonymousRespondentGuid = value;
                    if (ApplicationManager.AppSettings.SessionMode != AppSettings.SessionType.Cookieless)
                    {
                        this.SetAnonymousRespondentGuid(value.Value, true);
                    }
                    else
                    {
                        this.SetAnonymousRespondentGuid(value.Value, false);
                    }
                }
            }
        }

        public virtual int? CurrentPageId { get; set; }

        public virtual bool ForceNew { get; set; }

        public virtual bool IsFormPost
        {
            get
            {
                return this._isFormPost;
            }
        }

        public virtual bool IsTest { get; set; }

        public virtual string LanguageCode { get; set; }

        public virtual string Password { get; set; }

        public virtual Guid? RecipientGuid { get; set; }

        public virtual ExtendedPrincipal Respondent { get; set; }

        public virtual bool RespondentEditMode { get; set; }

        public virtual string RespondentIPAddress { get; set; }

        public virtual Guid? ResponseGuid { get; set; }

        public virtual string ServerUserContext { get; set; }

        public virtual Guid? SurveyGuid { get; set; }

        public virtual Guid? UserGuid { get; set; }
    }
}

