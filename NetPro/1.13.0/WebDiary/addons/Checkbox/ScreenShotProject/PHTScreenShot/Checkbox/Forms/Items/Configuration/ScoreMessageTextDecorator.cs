﻿namespace Checkbox.Forms.Items.Configuration
{
    using Checkbox.Globalization.Text;
    using Prezza.Framework.Common;
    using Prezza.Framework.Data;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Text;

    [Serializable]
    public class ScoreMessageTextDecorator : LocalizableResponseItemTextDecorator
    {
        private readonly Dictionary<string, string> _scoreTexts;
        private readonly Dictionary<string, bool> _scoreTextsModified;

        public ScoreMessageTextDecorator(ItemData data, string language) : base(data, language)
        {
            this._scoreTexts = new Dictionary<string, string>();
            this._scoreTextsModified = new Dictionary<string, bool>();
        }

        protected override void CopyLocalizedText(ItemData data)
        {
            base.CopyLocalizedText(data);
            ArgumentValidation.CheckExpectedType(data, typeof(ScoreMessageItemData));
            ScoreMessageItemData data2 = (ScoreMessageItemData) data;
            foreach (DataRow row in data2.ScoreRanges.Select(null, null, DataViewRowState.CurrentRows))
            {
                int scoreMessageID = (int) row["ScoreMessageID"];
                string messageTextID = data2.GetMessageTextID(scoreMessageID);
                double? defaultValue = null;
                double? nullable = DbUtility.GetValueFromDataRow<double?>(row, "LowScore", defaultValue);
                double? nullable4 = null;
                double? nullable2 = DbUtility.GetValueFromDataRow<double?>(row, "HigheScore", nullable4);
                if ((messageTextID != null) && (messageTextID.Trim() != string.Empty))
                {
                    Dictionary<string, string> allTexts = this.GetAllTexts(messageTextID);
                    StringBuilder builder = new StringBuilder();
                    if (nullable.HasValue)
                    {
                        builder.Append("LowScore = ");
                        builder.Append(nullable.ToString());
                    }
                    else
                    {
                        builder.Append("LowScore IS NULL");
                    }
                    builder.Append(" AND ");
                    if (nullable2.HasValue)
                    {
                        builder.Append("HighScore = ");
                        builder.Append(nullable2.ToString());
                    }
                    else
                    {
                        builder.Append("HighScore IS NULL");
                    }
                    DataRow[] rowArray2 = this.Data.ScoreRanges.Select(builder.ToString(), null, DataViewRowState.CurrentRows);
                    if (rowArray2.Length > 0)
                    {
                        int num2 = (int) rowArray2[0]["ScoreMessageID"];
                        string textID = this.Data.GetMessageTextID(num2);
                        foreach (string str3 in allTexts.Keys)
                        {
                            this.SetText(textID, allTexts[str3], str3);
                        }
                    }
                }
            }
        }

        protected static string GetKey(double? lowScore, double? highScore)
        {
            if (lowScore.HasValue && highScore.HasValue)
            {
                return (lowScore + "_" + highScore);
            }
            if (lowScore.HasValue)
            {
                return (lowScore + "+");
            }
            if (highScore.HasValue)
            {
                return (highScore + "-");
            }
            return "DEFAULT";
        }

        public string GetMessageText(int scoreMessageID)
        {
            DataRow[] rowArray = this.Data.ScoreRanges.Select("ScoreMessageID = " + scoreMessageID, null, DataViewRowState.CurrentRows);
            if (rowArray.Length <= 0)
            {
                return string.Empty;
            }
            double? lowScore = null;
            double? highScore = null;
            if (rowArray[0]["LowScore"] != DBNull.Value)
            {
                lowScore = new double?(Convert.ToDouble(rowArray[0]["LowScore"]));
            }
            if (rowArray[0]["HighScore"] != DBNull.Value)
            {
                highScore = new double?(Convert.ToDouble(rowArray[0]["HighScore"]));
            }
            return this.GetMessageText(lowScore, highScore);
        }

        public string GetMessageText(double? lowScore, double? highScore)
        {
            string key = GetKey(lowScore, highScore);
            if (this._scoreTextsModified.ContainsKey(key) && this._scoreTextsModified[key])
            {
                if (this._scoreTexts.ContainsKey(key))
                {
                    return this._scoreTexts[key];
                }
                return string.Empty;
            }
            string messageTextID = this.Data.GetMessageTextID(lowScore, highScore);
            if (messageTextID != string.Empty)
            {
                return this.GetText(messageTextID);
            }
            return string.Empty;
        }

        public override void ImportText(DataSet importData, int oldItemID)
        {
            string textTableName = this.Data.TextTableName;
            if ((importData != null) && importData.Tables.Contains(textTableName))
            {
                string filterExpression = this.Data.IdentityColumnName + " = " + oldItemID;
                foreach (DataRow row in importData.Tables[textTableName].Select(filterExpression, null, DataViewRowState.CurrentRows))
                {
                    if (row["TextValue"] != DBNull.Value)
                    {
                        string str4;
                        string textValue = (string) row["TextValue"];
                        double? defaultValue = null;
                        double? nullable = DbUtility.GetValueFromDataRow<double?>(row, "LowScore", defaultValue);
                        double? nullable4 = null;
                        double? nullable2 = DbUtility.GetValueFromDataRow<double?>(row, "HighScore", nullable4);
                        if (!nullable.HasValue)
                        {
                            str4 = "LowScore IS NULL";
                        }
                        else
                        {
                            str4 = "LowScore = " + nullable;
                        }
                        if (!nullable2.HasValue)
                        {
                            str4 = str4 + " AND HighScore IS NULL";
                        }
                        else
                        {
                            str4 = str4 + " AND HighScore = " + nullable2;
                        }
                        foreach (DataRow row2 in this.Data.ScoreRanges.Select(str4, null, DataViewRowState.CurrentRows))
                        {
                            if (row2["ScoreMessageID"] != DBNull.Value)
                            {
                                int scoreMessageID = (int) row2["ScoreMessageID"];
                                string messageTextID = this.Data.GetMessageTextID(scoreMessageID);
                                if ((messageTextID != null) && (messageTextID.Trim() != string.Empty))
                                {
                                    TextManager.SetText(messageTextID, base.Language, textValue);
                                }
                            }
                        }
                    }
                }
            }
        }

        protected override void SetLocalizedTexts()
        {
            base.SetLocalizedTexts();
            foreach (string str in this._scoreTextsModified.Keys)
            {
                if (this._scoreTextsModified[str] && this._scoreTexts.ContainsKey(str))
                {
                    double? lowScore = null;
                    double? highScore = null;
                    if (str.ToLower() != "default")
                    {
                        if (str.Contains("_"))
                        {
                            lowScore = new double?(Convert.ToDouble(str.Split(new char[] { '_' })[0]));
                            highScore = new double?(Convert.ToDouble(str.Split(new char[] { '_' })[1]));
                        }
                        else if (str.Contains("+"))
                        {
                            lowScore = new double?(Convert.ToDouble(str.Replace("+", "")));
                        }
                        else if (str.Contains("-"))
                        {
                            highScore = new double?(Convert.ToDouble(str.Replace("-", "")));
                        }
                    }
                    string messageTextID = this.Data.GetMessageTextID(lowScore, highScore);
                    if (messageTextID != string.Empty)
                    {
                        this.SetText(messageTextID, this._scoreTexts[str]);
                    }
                }
            }
        }

        public void SetMessageText(int scoreMessageID, string text)
        {
            DataRow[] rowArray = this.Data.ScoreRanges.Select("ScoreMessageID = " + scoreMessageID, null, DataViewRowState.CurrentRows);
            if (rowArray.Length > 0)
            {
                double? lowScore = null;
                double? highScore = null;
                if (rowArray[0]["LowScore"] != DBNull.Value)
                {
                    lowScore = new double?(Convert.ToDouble(rowArray[0]["LowScore"]));
                }
                if (rowArray[0]["HighScore"] != DBNull.Value)
                {
                    highScore = new double?(Convert.ToDouble(rowArray[0]["HighScore"]));
                }
                this.SetMessageText(lowScore, highScore, text);
            }
        }

        public void SetMessageText(double? lowScore, double? highScore, string text)
        {
            string key = GetKey(lowScore, highScore);
            this._scoreTexts[key] = text;
            this._scoreTextsModified[key] = true;
        }

        public ScoreMessageItemData Data
        {
            get
            {
                return (ScoreMessageItemData) base.Data;
            }
        }
    }
}

