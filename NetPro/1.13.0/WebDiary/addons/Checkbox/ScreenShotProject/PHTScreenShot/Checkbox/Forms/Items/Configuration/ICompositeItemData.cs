﻿namespace Checkbox.Forms.Items.Configuration
{
    using System.Collections.ObjectModel;

    public interface ICompositeItemData
    {
        ReadOnlyCollection<int> GetChildItemDataIDs();
        ReadOnlyCollection<ItemData> GetChildItemDatas();
    }
}

