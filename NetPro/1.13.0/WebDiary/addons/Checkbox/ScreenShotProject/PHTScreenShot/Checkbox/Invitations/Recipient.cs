﻿namespace Checkbox.Invitations
{
    using Checkbox.Common;
    using Checkbox.Messaging.Email;
    using Checkbox.Panels;
    using Checkbox.Users;
    using Prezza.Framework.Data;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Security.Principal;

    [Serializable]
    public class Recipient
    {
        private string _emailToAddress;
        private readonly Checkbox.Panels.Panelist _panelist;
        private Dictionary<string, string> _propertyCache;
        private bool _success;
        private string _uniqueIdentifier;

        public Recipient()
        {
        }

        public Recipient(Checkbox.Panels.Panelist panelist)
        {
            this._panelist = panelist;
        }

        public Recipient(IDataReader recipientDataReader)
        {
            this.LoadFromDataRow(recipientDataReader);
        }

        public string ComputeHashKey()
        {
            string str = this.PanelID.ToString();
            if (Utilities.IsNotNullOrEmpty(this.UniqueIdentifier))
            {
                return (str + "__" + this.UniqueIdentifier);
            }
            return (str + "__" + this.EmailToAddress);
        }

        protected virtual void Create()
        {
            this.InsertRecipient();
        }

        protected virtual InvitationPipeMediator GetPipeMeditor()
        {
            return new InvitationPipeMediator();
        }

        protected virtual string GetRecipientProfileProperty(string recipientIdentifier, string propertyKey)
        {
            if (this.PropertyCache.ContainsKey(propertyKey))
            {
                return this.PropertyCache[propertyKey];
            }
            string str = string.Empty;
            IIdentity userIdentity = UserManager.GetUserIdentity(recipientIdentifier);
            if (userIdentity != null)
            {
                object profileProperty = UserManager.GetProfileProperty(userIdentity, propertyKey);
                str = (profileProperty != null) ? profileProperty.ToString() : string.Empty;
            }
            this.PropertyCache[propertyKey] = str;
            return str;
        }

        protected virtual void InsertRecipient()
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Invitation_InsertRecipient");
            storedProcCommandWrapper.AddInParameter("InvitationID", DbType.Int32, this.InvitationID);
            storedProcCommandWrapper.AddInParameter("PanelID", DbType.Int32, this.PanelID);
            storedProcCommandWrapper.AddInParameter("EmailToAddress", DbType.String, this.EmailToAddress);
            storedProcCommandWrapper.AddInParameter("UniqueIdentifier", DbType.String, null);
            storedProcCommandWrapper.AddInParameter("GUID", DbType.Guid, this.GUID);
            storedProcCommandWrapper.AddInParameter("LastSent", DbType.DateTime, this.LastSent);
            storedProcCommandWrapper.AddInParameter("Success", DbType.Boolean, this.SuccessfullySent);
            storedProcCommandWrapper.AddInParameter("Error", DbType.String, this.Error);
            storedProcCommandWrapper.AddInParameter("LastBatchMessageId", DbType.Int64, this.BatchMessageId);
            storedProcCommandWrapper.AddOutParameter("RecipientID", DbType.Int64, 8);
            database.ExecuteNonQuery(storedProcCommandWrapper);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("RecipientID");
            if (parameterValue == DBNull.Value)
            {
                throw new Exception("Unable to insert recipient into database.");
            }
            this.ID = new long?((long) Convert.ToInt32(parameterValue));
        }

        protected virtual void LoadFromDataRow(IDataReader recipientDataReader)
        {
            this.ID = new long?(DbUtility.GetValueFromDataReader<long>(recipientDataReader, "RecipientId", 0L));
            this.InvitationID = DbUtility.GetValueFromDataReader<int>(recipientDataReader, "InvitationId", 0);
            this.PanelID = DbUtility.GetValueFromDataReader<int>(recipientDataReader, "PanelId", 0);
            this.PanelTypeId = DbUtility.GetValueFromDataReader<int>(recipientDataReader, "PanelTypeId", 0);
            this.EmailToAddress = DbUtility.GetValueFromDataReader<string>(recipientDataReader, "EmailAddress", string.Empty);
            this.UniqueIdentifier = DbUtility.GetValueFromDataReader<string>(recipientDataReader, "UniqueIdentifier", string.Empty);
            this.GUID = DbUtility.GetValueFromDataReader<Guid>(recipientDataReader, "Guid", Guid.Empty);
            this.HasResponded = DbUtility.GetValueFromDataReader<bool>(recipientDataReader, "HasResponded", false);
            this.NumberOfMessagesSent = DbUtility.GetValueFromDataReader<int>(recipientDataReader, "TotalSent", 0);
            this.SuccessfullySent = DbUtility.GetValueFromDataReader<bool>(recipientDataReader, "SuccessfullySent", false);
            this.LastSent = DbUtility.GetValueFromDataReader<DateTime?>(recipientDataReader, "LastSent", null);
            this.Error = DbUtility.GetValueFromDataReader<string>(recipientDataReader, "ErrorMessage", string.Empty);
            this.Deleted = DbUtility.GetValueFromDataReader<bool>(recipientDataReader, "Deleted", false);
            this.OptedOut = DbUtility.GetValueFromDataReader<bool>(recipientDataReader, "OptOut", false);
            this.BatchMessageId = DbUtility.GetValueFromDataReader<long?>(recipientDataReader, "LastBatchMessageId", null);
        }

        public virtual void PersonalizeTemplate(Invitation invitation, InvitationTemplate template, List<string> customUserFieldNames, string baseSurveyUrl)
        {
            if (!this.ID.HasValue)
            {
                this.Create();
            }
            InvitationPipeMediator pipeMeditor = this.GetPipeMeditor();
            if (pipeMeditor != null)
            {
                pipeMeditor.Initialize(invitation, this, customUserFieldNames, baseSurveyUrl);
                if (!template.IncludeOptOutLink)
                {
                    pipeMeditor.RegisterText("body", template.Body);
                }
                else if (template.Format == MailFormat.Html)
                {
                    pipeMeditor.RegisterText("body", template.Body + "<br /><br />" + template.OptOutText);
                }
                else
                {
                    pipeMeditor.RegisterText("body", template.Body + Environment.NewLine + Environment.NewLine + template.OptOutText);
                }
                pipeMeditor.RegisterText("subject", template.Subject);
                template.Body = pipeMeditor.GetText("body");
                template.Subject = pipeMeditor.GetText("subject");
            }
        }

        private void RecordSend()
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Invitation_Send2Recipient");
            storedProcCommandWrapper.AddInParameter("RecipientID", DbType.Int64, this.ID);
            storedProcCommandWrapper.AddInParameter("InvitationId", DbType.Int32, this.InvitationID);
            storedProcCommandWrapper.AddInParameter("LastSent", DbType.DateTime, this.LastSent);
            storedProcCommandWrapper.AddInParameter("Success", DbType.Boolean, this.SuccessfullySent);
            storedProcCommandWrapper.AddInParameter("Error", DbType.String, this.Error);
            storedProcCommandWrapper.AddInParameter("LastBatchMessageId", DbType.Int64, this.BatchMessageId);
            database.ExecuteNonQuery(storedProcCommandWrapper);
        }

        public void Save()
        {
            if (!this.ID.HasValue)
            {
                this.Create();
            }
            else
            {
                this.Update();
            }
        }

        protected virtual void Update()
        {
            this.RecordSend();
        }

        public long? BatchMessageId { get; set; }

        public bool Deleted { get; set; }

        public string EmailToAddress
        {
            get
            {
                if (Utilities.IsNullOrEmpty(this._emailToAddress) && (this._panelist != null))
                {
                    this._emailToAddress = this._panelist.Email;
                }
                return this._emailToAddress;
            }
            set
            {
                this._emailToAddress = value;
            }
        }

        public string Error { get; set; }

        public Guid GUID { get; set; }

        public bool HasResponded { get; set; }

        public long? ID { get; set; }

        public int InvitationID { get; set; }

        public string this[string key]
        {
            get
            {
                if (this._panelist != null)
                {
                    return this._panelist.GetProperty(key);
                }
                if (Utilities.IsNotNullOrEmpty(this.UniqueIdentifier))
                {
                    return this.GetRecipientProfileProperty(this.UniqueIdentifier, key);
                }
                return null;
            }
        }

        public DateTime? LastSent { get; set; }

        public bool Modified { get; set; }

        public int NumberOfMessagesSent { get; set; }

        public bool OptedOut { get; set; }

        public int PanelID { get; set; }

        protected Checkbox.Panels.Panelist Panelist
        {
            get
            {
                return this._panelist;
            }
        }

        public int PanelTypeId { get; set; }

        protected Dictionary<string, string> PropertyCache
        {
            get
            {
                if (this._propertyCache == null)
                {
                    this._propertyCache = new Dictionary<string, string>();
                }
                return this._propertyCache;
            }
        }

        public bool Sent { get; set; }

        public bool SuccessfullySent
        {
            get
            {
                return this._success;
            }
            set
            {
                this._success = value;
                if (this._success)
                {
                    this.Error = string.Empty;
                }
            }
        }

        public string UniqueIdentifier
        {
            get
            {
                if (Utilities.IsNotNullOrEmpty(this._uniqueIdentifier))
                {
                    return this._uniqueIdentifier;
                }
                if (this.Panelist != null)
                {
                    return this.Panelist.GetProperty("UniqueIdentifier");
                }
                return string.Empty;
            }
            set
            {
                this._uniqueIdentifier = value;
            }
        }
    }
}

