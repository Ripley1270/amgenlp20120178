﻿namespace Checkbox.Forms
{
    using System;
    using System.Data;
    using System.Runtime.CompilerServices;

    public interface IAnswerData
    {
        event EventHandler Saved;

        void AddAnswerRow(DataRow row);
        DataRow CreateAnswerRow(int itemID);
        void DeleteAnswerRow(long answerID);
        DataRow GetAnswerRow(long answerID);
        DataRow[] GetAnswerRowsForItem(int itemID);
    }
}

