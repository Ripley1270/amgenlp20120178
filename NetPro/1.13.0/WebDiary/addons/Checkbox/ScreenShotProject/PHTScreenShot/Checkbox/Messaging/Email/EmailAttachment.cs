﻿namespace Checkbox.Messaging.Email
{
    using Checkbox.Common;
    using System;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Text;

    public class EmailAttachment : IEmailAttachment
    {
        private readonly byte[] _contentBytes;
        private readonly string _contentString;

        public EmailAttachment()
        {
        }

        public EmailAttachment(string contentType, string content)
        {
            this.MimeContentTypeString = contentType;
            this._contentString = content;
        }

        public EmailAttachment(string contentType, byte[] content)
        {
            this.MimeContentTypeString = contentType;
            this._contentBytes = content;
        }

        public Stream GetContentStream()
        {
            if (Utilities.IsNotNullOrEmpty(this._contentString))
            {
                return GetContentStream(this._contentString);
            }
            if (this._contentBytes != null)
            {
                return GetContentStream(this._contentBytes);
            }
            return null;
        }

        private static Stream GetContentStream(string content)
        {
            MemoryStream stream = new MemoryStream();
            stream.Write(Encoding.UTF8.GetBytes(content), 0, content.Length);
            stream.Seek(0L, SeekOrigin.Begin);
            return stream;
        }

        private static Stream GetContentStream(byte[] content)
        {
            MemoryStream stream = new MemoryStream();
            stream.Write(content, 0, content.Length);
            stream.Seek(0L, SeekOrigin.Begin);
            return stream;
        }

        public string FileName { get; set; }

        public string MimeContentTypeString { get; set; }
    }
}

