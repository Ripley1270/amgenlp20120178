﻿namespace Checkbox.Forms.Items.Configuration
{
    using Checkbox.Common;
    using Checkbox.Forms.Items;
    using Prezza.Framework.Data;
    using System;
    using System.Data;

    [Serializable]
    public class ProfileUpdaterItemData : ResponseItemData
    {
        private DataSet _itemData;

        public ProfileUpdaterItemData()
        {
            this.InitializeDataSet();
        }

        public void AddProperty(int sourceItemID, string providerName, string propertyName)
        {
            if ((((sourceItemID >= 0) && !Utilities.IsNullOrEmpty(providerName)) && !Utilities.IsNullOrEmpty(propertyName)) && (this._itemData.Tables[this.DataTableName].Select(string.Format("SourceItemID = {0} AND ProviderName = '{1}' AND PropertyName = '{2}'", sourceItemID, providerName, propertyName), null, DataViewRowState.CurrentRows).Length <= 0))
            {
                DataRow row = this._itemData.Tables[this.DataTableName].NewRow();
                row["SourceItemID"] = sourceItemID;
                row["ProviderName"] = providerName;
                row["PropertyName"] = propertyName;
                this._itemData.Tables[this.DataTableName].Rows.Add(row);
            }
        }

        protected override void Create(IDbTransaction t)
        {
            base.Create(t);
            if (base.ID <= 0)
            {
                throw new Exception("Unable to create item.  DataID must be greater than zero.");
            }
            Database db = DatabaseFactory.CreateDatabase();
            db.UpdateDataSet(this._itemData, this.DataTableName, this.GetPropertyInsertCommand(db), null, this.GetPropertyDeleteCommand(db), t);
        }

        protected override void CreateDataRelationsInternal(DataSet ds)
        {
            base.CreateDataRelationsInternal(ds);
            if ((!ds.Relations.Contains(this.ParentDataTableName + "_ProfileUpdaterSourceItems") && ds.Tables.Contains(this.ParentDataTableName)) && ds.Tables.Contains(this.DataTableName))
            {
                DataRelation relation = new DataRelation(this.ParentDataTableName + "_ProfileUpdaterSourceItems", ds.Tables[this.ParentDataTableName].Columns["ItemID"], ds.Tables[this.DataTableName].Columns["SourceItemID"]);
                ds.Tables[this.DataTableName].Constraints.Add(new ForeignKeyConstraint(ds.Tables[this.ParentDataTableName].Columns["ItemID"], ds.Tables[this.DataTableName].Columns["SourceItemID"]));
                relation.Nested = true;
                ds.Relations.Add(relation);
            }
        }

        protected override Item CreateItem()
        {
            return new ProfileUpdater();
        }

        protected override DataSet GetConcreteConfigurationDataSet()
        {
            if (base.ID <= 0)
            {
                throw new Exception("Unable to get item data.   DataID was not greater than zero.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_GetProfileUpdater");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            DataSet dataSet = new DataSet();
            database.LoadDataSet(storedProcCommandWrapper, dataSet, this.DataTableName);
            return dataSet;
        }

        private DBCommandWrapper GetPropertyDeleteCommand(Database db)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_ItemData_DeletePUProp");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("SourceItemID", DbType.Int32, "SourceItemID", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("ProviderName", DbType.String, "ProviderName", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("PropertyName", DbType.String, "PropertyName", DataRowVersion.Current);
            return storedProcCommandWrapper;
        }

        private DBCommandWrapper GetPropertyInsertCommand(Database db)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_ItemData_InsertPUProp");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("SourceItemID", DbType.Int32, "SourceItemID", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("ProviderName", DbType.String, "ProviderName", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("PropertyName", DbType.String, "PropertyName", DataRowVersion.Current);
            return storedProcCommandWrapper;
        }

        private void InitializeDataSet()
        {
            this._itemData = new DataSet();
            DataTable table = new DataTable {
                TableName = this.DataTableName
            };
            table.Columns.Add("ItemID", typeof(int));
            table.Columns.Add("SourceItemID", typeof(int));
            table.Columns.Add("ProviderName", typeof(string));
            table.Columns.Add("PropertyName", typeof(string));
            this._itemData.Tables.Add(table);
        }

        public override void Load(DataSet data)
        {
            base.Load(data);
            if (data == null)
            {
                throw new Exception("Unable to load profile updater. DataSet was null.");
            }
            if (data.Tables.Count <= 0)
            {
                throw new Exception("Unable to load profile updater. DataSet had no tables.");
            }
            this._itemData.Tables[this.DataTableName].Clear();
            foreach (DataRow row in data.Tables[this.DataTableName].Select("ItemID = " + base.ID, null, DataViewRowState.CurrentRows))
            {
                this._itemData.Tables[this.DataTableName].ImportRow(row);
            }
        }

        public void RemoveProperty(int sourceItemID, string providerName, string propertyName)
        {
            if (((sourceItemID >= 0) && !Utilities.IsNullOrEmpty(providerName)) && !Utilities.IsNullOrEmpty(propertyName))
            {
                DataRow[] rowArray = this._itemData.Tables[this.DataTableName].Select(string.Format("SourceItemID = {0} AND ProviderName = '{1}' AND PropertyName = '{2}'", sourceItemID, providerName, propertyName), null, DataViewRowState.CurrentRows);
                if (rowArray.Length > 0)
                {
                    rowArray[0].Delete();
                }
            }
        }

        protected override void Update(IDbTransaction t)
        {
            base.Update(t);
            if (base.ID <= 0)
            {
                throw new Exception("Unable to update item.  DataID must be greater than zero.");
            }
            Database db = DatabaseFactory.CreateDatabase();
            db.UpdateDataSet(this._itemData, this.DataTableName, this.GetPropertyInsertCommand(db), null, this.GetPropertyDeleteCommand(db), t);
        }

        public override string DataTableName
        {
            get
            {
                return "PUProperties";
            }
        }

        public override bool IsExportable
        {
            get
            {
                return false;
            }
        }

        public DataTable PropertyData
        {
            get
            {
                return this._itemData.Tables[this.DataTableName].Copy();
            }
        }
    }
}

