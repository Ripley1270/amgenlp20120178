﻿namespace Checkbox.Panels
{
    using Checkbox.Users;
    using System;
    using System.Security.Principal;

    [Serializable]
    public class UserPanelist : Panelist
    {
        protected virtual void GetProfileProperty(string propertyName)
        {
            IIdentity userIdentity = UserManager.GetUserIdentity(this.UniqueIdentifier);
            if (userIdentity != null)
            {
                object profileProperty = UserManager.GetProfileProperty(userIdentity, propertyName);
                this.SetProperty(propertyName, (profileProperty != null) ? profileProperty.ToString() : string.Empty);
            }
            else
            {
                this.SetProperty(propertyName, string.Empty);
            }
        }

        public override string GetProperty(string propertyName)
        {
            if (!base.PropertiesDictionary.ContainsKey(propertyName))
            {
                this.GetProfileProperty(propertyName);
            }
            return base.PropertiesDictionary[propertyName];
        }

        public string UniqueIdentifier
        {
            get
            {
                return this.GetProperty("UniqueIdentifier");
            }
            set
            {
                this.SetProperty("UniqueIdentifier", value);
            }
        }
    }
}

