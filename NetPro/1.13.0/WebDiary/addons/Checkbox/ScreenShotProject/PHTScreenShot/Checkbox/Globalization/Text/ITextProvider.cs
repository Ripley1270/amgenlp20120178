﻿namespace Checkbox.Globalization.Text
{
    using Prezza.Framework.Configuration;
    using System;
    using System.Collections.Generic;
    using System.Data;

    public interface ITextProvider : IConfigurationProvider
    {
        Dictionary<string, string> GetAllTexts(string textIdentifier);
        DataTable GetMatchingTextData(params string[] matchExpressions);
        string GetText(string textIdentifier, string languageCode);
        DataTable GetTextData(string textIdentifier);
        void SetText(string textIdentifier, string languageCode, string textValue);
    }
}

