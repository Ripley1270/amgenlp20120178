﻿namespace Checkbox.Forms.Logic.Configuration
{
    using Checkbox.Forms;
    using Checkbox.Forms.Logic;
    using Prezza.Framework.Common;
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Data;

    [Serializable]
    public class ResponseOperandData : OperandData
    {
        private string _key;

        public ResponseOperandData(ResponseTemplate context) : this(context, null)
        {
        }

        public ResponseOperandData(ResponseTemplate context, string key) : base(context)
        {
            this._key = key;
        }

        protected override void Create(IDbTransaction t)
        {
            base.Create(t);
            DataRow row = base.Context.ResponseOperandTable.NewRow();
            row["OperandID"] = base.ID.Value;
            row["ResponseKey"] = this.Key;
            base.Context.ResponseOperandTable.Rows.Add(row);
        }

        public override Operand CreateOperand(Response context, string languageCode)
        {
            return new ResponseOperand(this._key, context);
        }

        public override void Delete(IDbTransaction transaction)
        {
            DataRow[] rowArray = base.Context.ResponseOperandTable.Select(this.IdentityColumnName + "=" + base.ID.Value);
            if (rowArray.Length > 0)
            {
                rowArray[0].Delete();
            }
            base.Delete(transaction);
        }

        protected override DataSet GetConcreteConfigurationDataSet()
        {
            if (base.ID <= 0)
            {
                throw new ApplicationException("No DataID specified.");
            }
            try
            {
                Database database = DatabaseFactory.CreateDatabase();
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ResponseOperand_GetOperand");
                storedProcCommandWrapper.AddInParameter("OperandID", DbType.Int32, base.ID);
                DataSet dataSet = new DataSet();
                database.LoadDataSet(storedProcCommandWrapper, dataSet, new string[] { this.DataTableName });
                return dataSet;
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessProtected"))
                {
                    throw;
                }
                return null;
            }
        }

        protected override void LoadFromDataRow(DataRow data)
        {
            ArgumentValidation.CheckForNullReference(data, "data");
            try
            {
                this._key = (data["ResponseKey"] != DBNull.Value) ? ((string) data["ResponseKey"]) : string.Empty;
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessProtected"))
                {
                    throw;
                }
            }
        }

        public override string ToString()
        {
            return this._key;
        }

        public override void Validate()
        {
        }

        public override string DataTableName
        {
            get
            {
                return "ResponseOperand";
            }
        }

        public string Key
        {
            get
            {
                return this._key;
            }
            set
            {
                this._key = value;
            }
        }

        public override string OperandTypeName
        {
            get
            {
                return "ResponseOperand";
            }
        }
    }
}

