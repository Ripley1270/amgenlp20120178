﻿namespace Checkbox.Content
{
    using Prezza.Framework.Data;
    using Prezza.Framework.Data.Sprocs;
    using System;
    using System.Data;

    [FetchProcedure("ckbx_Content_GetItem"), InsertProcedure("ckbx_Content_CreateItem"), UpdateProcedure("ckbx_Content_UpdateItem"), DeleteProcedure("ckbx_Content_DeleteItem")]
    public class DBContentItem : ContentItem
    {
        private string _createdBy;
        private int? _folderID;
        private int? _itemID;

        public DBContentItem(int? itemID)
        {
            this._itemID = itemID;
        }

        public void Delete()
        {
            StoredProcedureCommandExtractor.ExecuteProcedure(ProcedureType.Delete, this);
        }

        public void Load()
        {
            StoredProcedureCommandExtractor.ExecuteProcedure(ProcedureType.Select, this);
        }

        protected override byte[] LoadItemData()
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Content_GetItemData");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, this.ItemID);
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    if (reader.Read() && (reader["ItemData"] != DBNull.Value))
                    {
                        return (byte[]) reader["ItemData"];
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return null;
        }

        public void Save()
        {
            if (this.ItemID.HasValue)
            {
                StoredProcedureCommandExtractor.ExecuteProcedure(ProcedureType.Update, this);
            }
            else
            {
                StoredProcedureCommandExtractor.ExecuteProcedure(ProcedureType.Insert, this);
            }
        }

        [InsertParameter(Name="MIMEContentType", DbType=DbType.String, Direction=ParameterDirection.Input), UpdateParameter(Name="MIMEContentType", DbType=DbType.String, Direction=ParameterDirection.Input), FetchParameter(Name="MIMEContentType", DbType=DbType.String, Direction=ParameterDirection.ReturnValue)]
        public override string ContentType
        {
            get
            {
                return base.ContentType;
            }
            set
            {
                base.ContentType = value;
            }
        }

        [FetchParameter(Name="CreatedBy", DbType=DbType.String, Direction=ParameterDirection.ReturnValue), InsertParameter(Name="CreatedBy", DbType=DbType.String, Direction=ParameterDirection.Input), UpdateParameter(Name="CreatedBy", DbType=DbType.String, Direction=ParameterDirection.Input)]
        public string CreatedBy
        {
            get
            {
                return this._createdBy;
            }
            set
            {
                this._createdBy = value;
            }
        }

        [UpdateParameter(Name="ItemData", DbType=DbType.Binary, Direction=ParameterDirection.Input), InsertParameter(Name="ItemData", DbType=DbType.Binary, Direction=ParameterDirection.Input)]
        public override byte[] Data
        {
            get
            {
                return base.Data;
            }
            set
            {
                base.Data = value;
            }
        }

        [InsertParameter(Name="FolderID", DbType=DbType.Int32, Direction=ParameterDirection.Input), UpdateParameter(Name="FolderID", DbType=DbType.Int32, Direction=ParameterDirection.Input), FetchParameter(Name="FolderID", DbType=DbType.Int32, Direction=ParameterDirection.ReturnValue)]
        public int? FolderID
        {
            get
            {
                return this._folderID;
            }
            set
            {
                this._folderID = value;
            }
        }

        [FetchParameter(Name="IsPublic", DbType=DbType.Boolean, Direction=ParameterDirection.ReturnValue), UpdateParameter(Name="IsPublic", DbType=DbType.Boolean, Direction=ParameterDirection.Input), InsertParameter(Name="IsPublic", DbType=DbType.Boolean, Direction=ParameterDirection.Input)]
        public override bool IsPublic
        {
            get
            {
                return base.IsPublic;
            }
            set
            {
                base.IsPublic = value;
            }
        }

        [UpdateParameter(Name="ItemID", DbType=DbType.Int32, Direction=ParameterDirection.Input), InsertParameter(Name="ItemID", DbType=DbType.Int32, Direction=ParameterDirection.Output, Size=4), FetchParameter(Name="ItemID", DbType=DbType.Int32, Direction=ParameterDirection.Input), DeleteParameter(Name="ItemID", DbType=DbType.Int32, Direction=ParameterDirection.Input)]
        public int? ItemID
        {
            get
            {
                return this._itemID;
            }
            set
            {
                this._itemID = value;
            }
        }

        [UpdateParameter(Name="ItemName", DbType=DbType.String, Direction=ParameterDirection.Input), InsertParameter(Name="ItemName", DbType=DbType.String, Direction=ParameterDirection.Input), FetchParameter(Name="ItemName", DbType=DbType.String, Direction=ParameterDirection.ReturnValue)]
        public override string ItemName
        {
            get
            {
                return base.ItemName;
            }
            set
            {
                base.ItemName = value;
            }
        }

        [InsertParameter(Name="ItemUrl", DbType=DbType.String, Direction=ParameterDirection.Input), UpdateParameter(Name="ItemUrl", DbType=DbType.String, Direction=ParameterDirection.Input), FetchParameter(Name="ItemUrl", DbType=DbType.String, Direction=ParameterDirection.ReturnValue)]
        public override string ItemUrl
        {
            get
            {
                return base.ItemUrl;
            }
            set
            {
                base.ItemUrl = value;
            }
        }

        [FetchParameter(Name="LastUpdated", DbType=DbType.DateTime, Direction=ParameterDirection.ReturnValue), InsertParameter(Name="LastUpdated", DbType=DbType.DateTime, Direction=ParameterDirection.Input), UpdateParameter(Name="LastUpdated", DbType=DbType.DateTime, Direction=ParameterDirection.Input)]
        public override DateTime LastUpdated
        {
            get
            {
                return base.LastUpdated;
            }
            set
            {
                base.LastUpdated = value;
            }
        }
    }
}

