﻿namespace Checkbox.Invitations.Data
{
    using Checkbox.Common;
    using Checkbox.Forms;
    using Checkbox.Invitations;
    using Checkbox.Security;
    using Checkbox.Security.IdentityFilters;
    using Checkbox.Users;
    using Prezza.Framework.Data;
    using Prezza.Framework.Data.QueryCriteria;
    using Prezza.Framework.Data.QueryParameters;
    using Prezza.Framework.Security;
    using Prezza.Framework.Security.Principal;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Security.Principal;
    using System.Text;

    internal class InvitationQueryFactory
    {
        private InvitationQueryFactory()
        {
        }

        internal static CriteriaCollection CreateInvitationEmailListPanelFilter(Invitation invitation)
        {
            SelectQuery subQuery = new SelectQuery("ckbx_InvitationPanels");
            subQuery.AddParameter("PanelID", string.Empty, "ckbx_InvitationPanels");
            subQuery.AddCriterion(new QueryCriterion(new SelectParameter("InvitationID", string.Empty, "ckbx_InvitationPanels"), CriteriaOperator.EqualTo, new LiteralParameter(invitation.ID.Value)));
            CriteriaCollection criterias = new CriteriaCollection(CriteriaJoinType.And);
            criterias.AddCriterion(new QueryCriterion(new SelectParameter("PanelID", null, "ckbx_Panel"), CriteriaOperator.NotIn, new SubqueryParameter(subQuery)));
            return criterias;
        }

        internal static CriteriaCollection CreateInvitationGroupPanelFilter(ExtendedPrincipal callingPrincipal, int invitationId, int invitationParentId)
        {
            new SelectQuery("ckbx_Group").AddAllParameter("ckbx_Group");
            SelectQuery subQuery = new SelectQuery("ckbx_InvitationPanels");
            subQuery.AddParameter("PanelID", string.Empty, "ckbx_InvitationPanels");
            subQuery.AddCriterion(new QueryCriterion(new SelectParameter("InvitationID", string.Empty, "ckbx_InvitationPanels"), CriteriaOperator.EqualTo, new LiteralParameter(invitationId)));
            SelectQuery query3 = new SelectQuery("ckbx_GroupPanel");
            query3.AddParameter("GroupID", string.Empty, "ckbx_GroupPanel");
            query3.AddCriterion(new QueryCriterion(new SelectParameter("PanelID", string.Empty, "ckbx_GroupPanel"), CriteriaOperator.In, new SubqueryParameter(subQuery)));
            CriteriaCollection criterias = new CriteriaCollection(CriteriaJoinType.And);
            criterias.AddCriterion(new QueryCriterion(new SelectParameter("GroupID", null, "ckbx_Group"), CriteriaOperator.NotIn, new SubqueryParameter(query3)));
            LightweightResponseTemplate lightweightResponseTemplate = ResponseTemplateManager.GetLightweightResponseTemplate(invitationParentId);
            if ((lightweightResponseTemplate != null) && (lightweightResponseTemplate.SecurityType == SecurityType.AccessControlList))
            {
                SecurityEditor editor = lightweightResponseTemplate.GetEditor();
                editor.Initialize(callingPrincipal);
                List<IAccessControlEntry> list = editor.List(new string[] { "Form.Fill" });
                int num = 0;
                StringBuilder builder = new StringBuilder();
                foreach (IAccessControlEntry entry in list)
                {
                    if (entry.AclEntryTypeIdentifier.Equals("Checkbox.Users.Group", StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (num > 0)
                        {
                            builder.Append(",");
                        }
                        builder.Append(entry.AclEntryIdentifier);
                        num++;
                    }
                }
                if (num == 0)
                {
                    builder.Append("0");
                }
                QueryCriterion criterion = new QueryCriterion(new SelectParameter("GroupID"), CriteriaOperator.In, new LiteralParameter(builder.ToString()));
                criterias.AddCriterion(criterion);
            }
            return criterias;
        }

        internal static IdentityFilterCollection CreateInvitationUserPanelFilter(ExtendedPrincipal callingPrincipal, int invitationId, int invitationParentId)
        {
            SelectQuery subQuery = new SelectQuery("ckbx_InvitationRecipients");
            subQuery.AddParameter("UniqueIdentifier", string.Empty, "ckbx_InvitationRecipients");
            subQuery.AddCriterion(new QueryCriterion(new SelectParameter("InvitationID", string.Empty, "ckbx_InvitationRecipients"), CriteriaOperator.EqualTo, new LiteralParameter(invitationId)));
            subQuery.AddCriterion(new QueryCriterion(new SelectParameter("UniqueIdentifier", string.Empty, "ckbx_InvitationRecipients"), CriteriaOperator.IsNot, new LiteralParameter("NULL")));
            CriteriaCollection criteria = new CriteriaCollection(CriteriaJoinType.Or);
            criteria.AddCriterion(new QueryCriterion(new SelectParameter("Deleted", null, "ckbx_InvitationRecipients"), CriteriaOperator.Is, new LiteralParameter("NULL")));
            criteria.AddCriterion(new QueryCriterion(new SelectParameter("Deleted", null, "ckbx_InvitationRecipients"), CriteriaOperator.EqualTo, new LiteralParameter("0")));
            subQuery.AddCriteriaCollection(criteria);
            SelectQuery query2 = new SelectQuery("ckbx_InvitationPanels");
            query2.AddParameter("PanelID", string.Empty, "ckbx_InvitationPanels");
            query2.AddCriterion(new QueryCriterion(new SelectParameter("InvitationID", string.Empty, "ckbx_InvitationPanels"), CriteriaOperator.EqualTo, new LiteralParameter(invitationId)));
            SelectQuery query3 = new SelectQuery("ckbx_UserPanel");
            query3.AddParameter("UserIdentifier", string.Empty, "ckbx_UserPanel");
            query3.AddCriterion(new QueryCriterion(new SelectParameter("PanelID", string.Empty, "ckbx_UserPanel"), CriteriaOperator.In, new SubqueryParameter(query2)));
            CriteriaCollection criterias2 = new CriteriaCollection(CriteriaJoinType.Or);
            criterias2.AddCriterion(new QueryCriterion(new SelectParameter("UniqueIdentifier", string.Empty, "ckbx_Credential"), CriteriaOperator.In, new SubqueryParameter(subQuery)));
            criterias2.AddCriterion(new QueryCriterion(new SelectParameter("UniqueIdentifier", string.Empty, "ckbx_Credential"), CriteriaOperator.In, new SubqueryParameter(query3)));
            SelectQuery query4 = new SelectQuery("ckbx_Credential");
            query4.AddParameter("UniqueIdentifier", string.Empty, string.Empty);
            query4.AddCriteriaCollection(criterias2);
            List<string> list = new List<string>();
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query4.ToString());
            using (IDataReader reader = database.ExecuteReader(sqlStringCommandWrapper))
            {
                try
                {
                    while (reader.Read())
                    {
                        if (reader["UniqueIdentifier"] != DBNull.Value)
                        {
                            list.Add((string) reader["UniqueIdentifier"]);
                        }
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            InFilter filter = null;
            LightweightResponseTemplate lightweightResponseTemplate = ResponseTemplateManager.GetLightweightResponseTemplate(invitationParentId);
            if ((lightweightResponseTemplate != null) && (lightweightResponseTemplate.SecurityType == SecurityType.AccessControlList))
            {
                list.Sort();
                SecurityEditor editor = lightweightResponseTemplate.GetEditor();
                editor.Initialize(callingPrincipal);
                List<IAccessControlEntry> list2 = editor.List(new string[] { "Form.Fill" });
                IAuthorizationProvider authorizationProvider = AuthorizationFactory.GetAuthorizationProvider();
                List<string> list3 = new List<string>();
                foreach (IAccessControlEntry entry in list2)
                {
                    if (entry.AclEntryTypeIdentifier.Equals("Checkbox.Users.Group", StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (Utilities.AsInt(entry.AclEntryIdentifier).HasValue)
                        {
                            Group group = Group.GetGroup(Convert.ToInt32(entry.AclEntryIdentifier));
                            if (group != null)
                            {
                                foreach (IPrincipal principal in group.GetUsers())
                                {
                                    if (((list.BinarySearch(principal.Identity.Name) < 0) && !list3.Contains(principal.Identity.Name)) && authorizationProvider.Authorize(principal as ExtendedPrincipal, lightweightResponseTemplate, "Form.Fill"))
                                    {
                                        list3.Add(principal.Identity.Name);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        list3.Add(entry.AclEntryIdentifier);
                    }
                }
                if (list3.Count == 0)
                {
                    list3.Add(string.Empty);
                }
                filter = new InFilter("UniqueIdentifier", list3.ToArray(), IdentityFilterPropertyType.IdentityProperty);
            }
            NotInFilter filter2 = new NotInFilter("UniqueIdentifier", list.ToArray(), IdentityFilterPropertyType.IdentityProperty);
            NotNullFilter filter3 = new NotNullFilter("Email", IdentityFilterPropertyType.ProfileProperty);
            IdentityFilterCollection filters = new IdentityFilterCollection(IdentityFilterJoin.And);
            filters.AddFilter(filter3);
            if (list.Count > 0)
            {
                filters.AddFilter(filter2);
            }
            if (filter != null)
            {
                filters.AddFilter(filter);
            }
            return filters;
        }

        public static SelectQuery GetAvailableInvitations(string filterField, string filterValue)
        {
            SelectQuery query = new SelectQuery("ckbx_Invitation") {
                CriteriaJoinType = CriteriaJoinType.And
            };
            query.AddAllParameter("ckbx_Invitation");
            query.AddParameter("TemplateName", string.Empty, "ckbx_ResponseTemplate");
            SelectQuery subQuery = new SelectQuery("ckbx_InvitationRecipients");
            subQuery.AddCountParameter("RecipientID", null);
            subQuery.AddCriterion("ckbx_InvitationRecipients", "InvitationID", CriteriaOperator.EqualTo, "ckbx_Invitation", "InvitationID");
            subQuery.AddCriterion(new QueryCriterion(new SelectParameter("SuccessfullySent", null, "ckbx_InvitationRecipients"), CriteriaOperator.EqualTo, new LiteralParameter(1)));
            query.AddParameter(new LiteralParameter("(" + new SubqueryParameter(subQuery) + ")", "Sent", true));
            SelectQuery query3 = new SelectQuery("ckbx_InvitationRecipients");
            query3.AddCountParameter("RecipientID", null);
            query3.AddCriterion("ckbx_InvitationRecipients", "InvitationID", CriteriaOperator.EqualTo, "ckbx_Invitation", "InvitationID");
            query3.AddCriterion(new QueryCriterion(new SelectParameter("SuccessfullySent", null, "ckbx_InvitationRecipients"), CriteriaOperator.EqualTo, new LiteralParameter(0)));
            query.AddParameter(new LiteralParameter("(" + new SubqueryParameter(query3) + ")", "Failed", true));
            SelectQuery query4 = new SelectQuery("ckbx_InvitationRecipients");
            query4.AddCountParameter("RecipientID", null);
            query4.AddCriterion("ckbx_InvitationRecipients", "InvitationID", CriteriaOperator.EqualTo, "ckbx_Invitation", "InvitationID");
            query4.AddCriterion(new QueryCriterion(new SelectParameter("HasResponded", null, "ckbx_InvitationRecipients"), CriteriaOperator.EqualTo, new LiteralParameter(1)));
            query.AddParameter(new LiteralParameter("(" + new SubqueryParameter(query4) + ")", "ResponseCount", true));
            query.AddTableJoin("ckbx_ResponseTemplate", QueryJoinType.Inner, "ResponseTemplateID", "ckbx_Invitation", "ResponseTemplateID");
            if (Utilities.IsNotNullOrEmpty(filterField) && Utilities.IsNotNullOrEmpty(filterValue))
            {
                query.AddCriterion(new QueryCriterion(new SelectParameter(filterField, string.Empty, "ckbx_Invitation"), CriteriaOperator.Like, new LiteralParameter("'%" + filterValue + "%'")));
            }
            return query;
        }

        public static SelectQuery GetAvailableInvitationsByTemplateId(int templateId, string filterField, string filterValue)
        {
            SelectQuery availableInvitations = GetAvailableInvitations(filterField, filterValue);
            availableInvitations.AddCriterion("ckbx_Invitation", "ResponseTemplateID", CriteriaOperator.EqualTo, "", templateId.ToString());
            return availableInvitations;
        }
    }
}

