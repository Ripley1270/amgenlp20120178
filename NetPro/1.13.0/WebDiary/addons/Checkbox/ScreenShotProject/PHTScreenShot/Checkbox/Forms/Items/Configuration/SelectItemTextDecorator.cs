﻿namespace Checkbox.Forms.Items.Configuration
{
    using Checkbox.Common;
    using Checkbox.Globalization.Text;
    using Prezza.Framework.Common;
    using Prezza.Framework.Data;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class SelectItemTextDecorator : LabelledItemTextDecorator
    {
        private Dictionary<int, string> _optionTexts;
        private string _otherText;
        private bool _otherTextModified;

        public SelectItemTextDecorator(SelectItemData data, string language) : base(data, language)
        {
            this._otherText = string.Empty;
            this._otherTextModified = false;
            this._optionTexts = new Dictionary<int, string>();
            this.TextsLoaded = false;
        }

        protected override void CopyLocalizedText(ItemData data)
        {
            base.CopyLocalizedText(data);
            ArgumentValidation.CheckExpectedType(data, typeof(SelectItemData));
            if (this.Data.OtherTextID != string.Empty)
            {
                Dictionary<string, string> allTexts = this.GetAllTexts(((SelectItemData) data).OtherTextID);
                if (allTexts != null)
                {
                    foreach (string str in allTexts.Keys)
                    {
                        this.SetText(this.Data.OtherTextID, allTexts[str], str);
                    }
                }
            }
            for (int i = 0; i < this.Data.Options.Count; i++)
            {
                ListOptionData data2 = this.Data.Options[i];
                if ((data2.TextID != string.Empty) && (((SelectItemData) data).Options.Count > i))
                {
                    string textID = ((SelectItemData) data).Options[i].TextID;
                    Dictionary<string, string> dictionary2 = this.GetAllTexts(textID);
                    if (dictionary2 != null)
                    {
                        foreach (string str3 in dictionary2.Keys)
                        {
                            this.SetText(data2.TextID, dictionary2[str3], str3);
                        }
                    }
                }
            }
        }

        protected override void EnsureTextsLoaded()
        {
            if (!this.Data.ID.HasValue || this.TextsLoaded)
            {
                this.TextsLoaded = true;
            }
            else
            {
                Database database = DatabaseFactory.CreateDatabase();
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_GetText");
                storedProcCommandWrapper.AddInParameter("ItemId", DbType.Int32, this.Data.ID.Value);
                storedProcCommandWrapper.AddInParameter("TextPrefix", DbType.String, this.Data.TextIDPrefix);
                storedProcCommandWrapper.AddInParameter("LanguageCode", DbType.String, base.Language);
                using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
                {
                    try
                    {
                        if (!base.TextModified)
                        {
                            string text = reader.Read() ? DbUtility.GetValueFromDataReader<string>(reader, "TextValue", string.Empty) : string.Empty;
                            if (string.IsNullOrEmpty(text))
                            {
                                text = this.GetText(this.Data.TextID);
                            }
                            base.Text = text;
                        }
                        if (!base.SubTextModified)
                        {
                            string str2 = (reader.NextResult() && reader.Read()) ? DbUtility.GetValueFromDataReader<string>(reader, "TextValue", string.Empty) : string.Empty;
                            if (string.IsNullOrEmpty(str2))
                            {
                                str2 = this.GetText(this.Data.SubTextID);
                            }
                            base.SubText = str2;
                        }
                        if (reader.NextResult())
                        {
                            while (reader.Read())
                            {
                                int key = DbUtility.GetValueFromDataReader<int>(reader, "Position", -1);
                                string str3 = DbUtility.GetValueFromDataReader<string>(reader, "TextValue", string.Empty);
                                string str4 = DbUtility.GetValueFromDataReader<string>(reader, "TextId", string.Empty);
                                if (string.IsNullOrEmpty(str3) && !string.IsNullOrEmpty(str4))
                                {
                                    str3 = this.GetText(str4);
                                }
                                if ((key > 0) && !this._optionTexts.ContainsKey(key))
                                {
                                    this.SetOptionText(key, str3);
                                }
                            }
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
                this.TextsLoaded = true;
            }
        }

        public string GetOptionText(int position)
        {
            this.EnsureTextsLoaded();
            if (this._optionTexts.ContainsKey(position))
            {
                return this._optionTexts[position];
            }
            foreach (ListOptionData data in this.Data.Options)
            {
                if ((data.Position == position) && (data.TextID != string.Empty))
                {
                    if (data.IsOther)
                    {
                        if (Utilities.IsNotNullOrEmpty(this.OtherText))
                        {
                            return this.OtherText;
                        }
                        if (Utilities.IsNotNullOrEmpty(data.Alias))
                        {
                            return data.Alias;
                        }
                    }
                    return this.GetText(data.TextID);
                }
            }
            return string.Empty;
        }

        public override void ImportText(DataSet importData, int oldItemID)
        {
            base.ImportText(importData, oldItemID);
            Dictionary<int, string> dictionary = new Dictionary<int, string>();
            foreach (ListOptionData data in this.Data.Options)
            {
                dictionary[data.Position] = data.TextID;
            }
            if (importData.Tables.Contains("OptionTexts") && importData.Tables.Contains("ItemOptions"))
            {
                foreach (DataRow row in importData.Tables["ItemOptions"].Select("ItemID = " + oldItemID, null, DataViewRowState.CurrentRows))
                {
                    int key = (int) row["Position"];
                    int num2 = (int) row["OptionID"];
                    foreach (DataRow row2 in importData.Tables["OptionTexts"].Select("OptionID = " + num2, null, DataViewRowState.CurrentRows))
                    {
                        string textValue = (string) row2["TextValue"];
                        string languageCode = (string) row2["LanguageCode"];
                        if (dictionary.ContainsKey(key))
                        {
                            TextManager.SetText(dictionary[key], languageCode, textValue);
                        }
                    }
                }
            }
        }

        protected override void SetLocalizedTexts()
        {
            base.SetLocalizedTexts();
            if (this.Data.OtherTextID != string.Empty)
            {
                this.SetText(this.Data.OtherTextID, this.OtherText);
            }
            foreach (ListOptionData data in this.Data.Options)
            {
                if ((data.TextID != string.Empty) && this._optionTexts.ContainsKey(data.Position))
                {
                    this.SetText(data.TextID, this._optionTexts[data.Position]);
                }
            }
        }

        public void SetOptionText(int position, string text)
        {
            this._optionTexts[position] = text;
        }

        public SelectItemData Data
        {
            get
            {
                return (SelectItemData) base.Data;
            }
        }

        public string OtherText
        {
            get
            {
                if ((this.Data.OtherTextID != string.Empty) && !this._otherTextModified)
                {
                    return this.GetText(this.Data.OtherTextID);
                }
                return this._otherText;
            }
            set
            {
                this._otherText = value;
                this._otherTextModified = true;
            }
        }

        protected bool TextsLoaded { get; set; }
    }
}

