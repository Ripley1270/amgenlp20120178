﻿namespace Checkbox.Security.IdentityFilters
{
    using System;

    public class NotLikeFilter : IdentityFilter
    {
        private object propertyValue;

        public NotLikeFilter(string filterProperty) : base(IdentityFilterType.NotLike, IdentityFilterPropertyType.IdentityProperty, filterProperty)
        {
        }

        public NotLikeFilter(string filterProperty, object propertyValue) : base(IdentityFilterType.NotLike, IdentityFilterPropertyType.IdentityProperty, filterProperty)
        {
            this.propertyValue = propertyValue;
        }

        public NotLikeFilter(string filterProperty, object propertyValue, IdentityFilterPropertyType filterPropertyType) : base(IdentityFilterType.NotLike, filterPropertyType, filterProperty)
        {
            this.propertyValue = propertyValue;
        }

        public object PropertyValue
        {
            get
            {
                return this.propertyValue;
            }
        }
    }
}

