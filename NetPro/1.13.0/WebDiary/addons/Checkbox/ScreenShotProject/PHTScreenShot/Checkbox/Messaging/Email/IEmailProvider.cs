﻿namespace Checkbox.Messaging.Email
{
    using Prezza.Framework.Configuration;
    using System;

    public interface IEmailProvider : IConfigurationProvider
    {
        long AddEmailMessageToBatch(long batchId, IEmailMessage message);
        void CloseEmailMessageBatch(long batchId);
        long CreateEmailMessageBatch(string name, string createdBy, DateTime? earliestSendDate);
        void DeleteEmailMessage(long messageId);
        void DeleteEmailMessageBatch(long batchId);
        void DeleteMessageAttachment(long attachmentId);
        IEmailMessageBatchStatus GetEmailMessageBatchStatus(long batchId);
        IEmailMessageStatus GetEmailMessageStatus(long emailId);
        IEmailMessage GetMessage(long emailId);
        long RegisterMessageAttachment(IEmailAttachment attachment);
        long? Send(IEmailMessage message);
        long SendMessage(string name, string createdBy, IEmailMessage message, DateTime? earliestSendDate);

        bool SupportsBatches { get; }
    }
}

