﻿namespace Checkbox.Forms
{
    using System;

    public class TemplateDoesNotExist : TemplateException
    {
        public TemplateDoesNotExist(int templateID) : base(templateID)
        {
        }

        public TemplateDoesNotExist(string templateGuid) : base(templateGuid)
        {
        }
    }
}

