﻿namespace Checkbox.Forms.Caching.Expirations
{
    using Checkbox.Forms;
    using Prezza.Framework.Caching;
    using System;

    [Serializable]
    public class ResponseTemplateUpdatedExpiration : ICacheItemExpiration
    {
        private bool _expired;

        public ResponseTemplateUpdatedExpiration(ResponseTemplate rt)
        {
            if (rt != null)
            {
                rt.Updated += new TemplateUpdated(this.rt_Updated);
            }
        }

        public bool HasExpired()
        {
            return this._expired;
        }

        public void Initialize(CacheItem owningCacheItem)
        {
        }

        public void Notify()
        {
        }

        private void rt_Updated(Template source, EventArgs e)
        {
            this._expired = true;
            source.Updated -= new TemplateUpdated(this.rt_Updated);
        }
    }
}

