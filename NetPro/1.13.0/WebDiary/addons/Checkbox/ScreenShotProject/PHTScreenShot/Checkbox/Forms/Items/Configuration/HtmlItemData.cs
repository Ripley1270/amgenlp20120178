﻿namespace Checkbox.Forms.Items.Configuration
{
    using Checkbox.Forms.Items;
    using Prezza.Framework.Data;
    using System;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class HtmlItemData : LocalizableResponseItemData
    {
        protected override void Create(IDbTransaction t)
        {
            base.Create(t);
            if (base.ID <= 0)
            {
                throw new ApplicationException("No DataID specified for Create()");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_InsertHtml");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("Html", DbType.String, this.HTML);
            storedProcCommandWrapper.AddInParameter("InlineCss", DbType.String, this.InlineCSS);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        protected override Item CreateItem()
        {
            return new HtmlItem();
        }

        public override ItemTextDecorator CreateTextDecorator(string languageCode)
        {
            return new HtmlItemTextDecorator(this, languageCode);
        }

        protected override DataSet GetConcreteConfigurationDataSet()
        {
            if (base.ID <= 0)
            {
                throw new ApplicationException("No DataID specified.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_GetHtml");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            DataSet dataSet = new DataSet();
            string[] tableNames = new string[] { this.DataTableName };
            database.LoadDataSet(storedProcCommandWrapper, dataSet, tableNames);
            return dataSet;
        }

        public override void Load(DataSet data)
        {
            base.Load(data);
            if (!data.Tables.Contains(this.DataTableName) || (data.Tables[this.DataTableName].Rows.Count <= 0))
            {
                throw new Exception("Dataset contains no data table for this item.");
            }
            DataRow[] rowArray = data.Tables[this.DataTableName].Select("ItemID = " + base.ID);
            if (rowArray.Length != 1)
            {
                throw new Exception("Dataset contains an improper number of rows for this item");
            }
            DataRow row = rowArray[0];
            this.HTML = (row["Html"] != DBNull.Value) ? ((string) row["Html"]) : string.Empty;
            this.InlineCSS = (row["InlineCss"] != DBNull.Value) ? ((string) row["InlineCss"]) : string.Empty;
        }

        protected override void Update(IDbTransaction t)
        {
            base.Update(t);
            if (base.ID <= 0)
            {
                throw new ApplicationException("No DataID specified for Update()");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_UpdateHtml");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("Html", DbType.String, this.HTML);
            storedProcCommandWrapper.AddInParameter("InlineCss", DbType.String, this.InlineCSS);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        public override string DataTableName
        {
            get
            {
                return "HtmlItemData";
            }
        }

        public string HTML { get; set; }

        public string InlineCSS { get; set; }

        public override string TextIDPrefix
        {
            get
            {
                return "htmlItemData";
            }
        }
    }
}

