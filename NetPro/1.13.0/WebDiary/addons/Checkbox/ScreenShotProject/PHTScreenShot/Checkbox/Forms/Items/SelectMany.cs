﻿namespace Checkbox.Forms.Items
{
    using Checkbox.Forms.Items.Configuration;
    using Checkbox.Forms.Validation;
    using Prezza.Framework.Common;
    using System;
    using System.Collections.Specialized;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class SelectMany : SelectItem
    {
        public override void Configure(ItemData configuration, string languageCode)
        {
            ArgumentValidation.CheckExpectedType(configuration, typeof(SelectManyData));
            SelectManyData data = (SelectManyData) configuration;
            base.Configure(configuration, languageCode);
            this.MinToSelect = data.MinToSelect;
            this.MaxToSelect = data.MaxToSelect;
        }

        protected override NameValueCollection GetMetaDataValuesForXmlSerialization()
        {
            NameValueCollection metaDataValuesForXmlSerialization = base.GetMetaDataValuesForXmlSerialization();
            metaDataValuesForXmlSerialization["minToSelect"] = this.MinToSelect.HasValue ? this.MinToSelect.ToString() : null;
            metaDataValuesForXmlSerialization["maxToSelect"] = this.MaxToSelect.HasValue ? this.MaxToSelect.ToString() : null;
            return metaDataValuesForXmlSerialization;
        }

        public override void Select(string otherText, params int[] optionIDs)
        {
            base.Select(otherText, optionIDs);
            if (optionIDs.Length == 0)
            {
                this.AddPlaceHolderAnswer();
            }
            else
            {
                this.RemovePlaceHolderAnswer();
            }
            this.SynchronizeSelectedOptions();
        }

        protected override bool ValidateAnswers()
        {
            SelectManyValidator validator = new SelectManyValidator();
            if (!validator.Validate(this))
            {
                base.ValidationErrors.Add(validator.GetMessage(base.LanguageCode));
                return false;
            }
            return true;
        }

        public int? MaxToSelect { get; private set; }

        public int? MinToSelect { get; private set; }

        public override bool Required
        {
            get
            {
                return (this.MinToSelect > 0);
            }
        }
    }
}

