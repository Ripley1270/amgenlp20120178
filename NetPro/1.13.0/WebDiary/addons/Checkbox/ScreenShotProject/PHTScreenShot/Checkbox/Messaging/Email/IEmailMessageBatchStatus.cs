﻿namespace Checkbox.Messaging.Email
{
    using System;
    using System.Collections.Generic;

    public interface IEmailMessageBatchStatus
    {
        int AttemptedCount { get; }

        DateTime? BatchSendCompleted { get; }

        DateTime? BatchSendStarted { get; }

        BatchStatus CurrentStatus { get; set; }

        int FailedCount { get; }

        List<long> FailedMessages { get; }

        DateTime? NextSendAttempt { get; }

        int SucceededCount { get; }

        bool SuccessfullySent { get; }
    }
}

