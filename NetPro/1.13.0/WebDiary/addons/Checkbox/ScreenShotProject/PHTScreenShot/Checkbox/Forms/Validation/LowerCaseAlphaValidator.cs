﻿namespace Checkbox.Forms.Validation
{
    using Checkbox.Globalization.Text;
    using System;

    public class LowerCaseAlphaValidator : RegularExpressionValidator
    {
        public LowerCaseAlphaValidator()
        {
            base._regex = @"^[a-z\s]+$";
        }

        public override string GetMessage(string languageCode)
        {
            return TextManager.GetText("/validationMessages/regex/lowerCase", languageCode);
        }
    }
}

