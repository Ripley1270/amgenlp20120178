﻿namespace Checkbox.Panels.Security
{
    using Checkbox.Panels;
    using Checkbox.Security;
    using System;

    public class EmailListSecurityEditor : AccessControllablePDOSecurityEditor
    {
        public EmailListSecurityEditor(EmailListPanel emailListPanel) : base(emailListPanel)
        {
        }

        protected override string RequiredEditorPermission
        {
            get
            {
                return "EmailList.Edit";
            }
        }
    }
}

