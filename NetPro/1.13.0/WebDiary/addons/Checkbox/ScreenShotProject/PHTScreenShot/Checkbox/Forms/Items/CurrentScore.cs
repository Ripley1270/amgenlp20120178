﻿namespace Checkbox.Forms.Items
{
    using System;

    [Serializable]
    public class CurrentScore : Message
    {
        protected double GetScore()
        {
            double num = 0.0;
            if (base.Response == null)
            {
                return num;
            }
            double? totalScore = base.Response.TotalScore;
            if (!totalScore.HasValue)
            {
                return 0.0;
            }
            return totalScore.GetValueOrDefault();
        }

        public override string Text
        {
            get
            {
                return (base.Text + this.GetScore());
            }
        }
    }
}

