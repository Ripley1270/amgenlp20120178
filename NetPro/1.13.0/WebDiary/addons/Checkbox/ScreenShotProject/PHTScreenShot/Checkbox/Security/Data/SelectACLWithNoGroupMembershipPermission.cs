﻿namespace Checkbox.Security.Data
{
    using Checkbox.Security;
    using Prezza.Framework.Data;
    using Prezza.Framework.Data.QueryCriteria;
    using Prezza.Framework.Data.QueryParameters;
    using System;

    internal static class SelectACLWithNoGroupMembershipPermission
    {
        internal static SelectQuery GetQuery(string entityTable, string entityTableAclIDColumn, string aclEntryType, string aclEntryTypeID, string permission)
        {
            SelectQuery query = new SelectQuery(entityTable);
            query.AddParameter(entityTableAclIDColumn, string.Empty, entityTable);
            query.AddTableJoin("ckbx_AccessControlEntries", QueryJoinType.Inner, "AclID", entityTable, entityTableAclIDColumn);
            SelectQuery query2 = SelectPolicyWithPermissionQuery.GetQuery(permission);
            query2.AddCriterion("ckbx_PolicyPermissions", "PolicyID", CriteriaOperator.EqualTo, "ckbx_AccessControlEntry", "PolicyID");
            SelectQuery query3 = new SelectQuery("ckbx_AccessControlEntry");
            query3.AddParameter("EntryID", string.Empty, "ckbx_AccessControlEntry");
            query3.AddTableJoin("ckbx_Group", QueryJoinType.Inner, "CONVERT(nvarchar, ckbx_Group.GroupID)", "ckbx_AccessControlEntry", "EntryIdentifier");
            query3.AddTableJoin("ckbx_GroupMembers", QueryJoinType.Inner, "GroupID", "ckbx_Group", "GroupID");
            query3.AddCriterion(new NotExistsCriterion(query2));
            query3.AddCriterion(new QueryCriterion(new SelectParameter("EntryType", string.Empty, "ckbx_AccessControlEntry"), CriteriaOperator.EqualTo, new LiteralParameter("'Checkbox.Users.Group'")));
            query3.AddCriterion(new QueryCriterion(new SelectParameter("MemberUniqueIdentifier", string.Empty, "ckbx_GroupMembers"), CriteriaOperator.EqualTo, new LiteralParameter("'" + aclEntryTypeID + "'")));
            query3.AddCriterion("ckbx_AccessControlEntry", "EntryID", CriteriaOperator.EqualTo, "ckbx_AccessControlEntries", "EntryID");
            query.AddCriterion(new ExistsCriterion(query3));
            return query;
        }

        internal static SelectQuery GetQuery(string entityTable, string entityTableAclIDColumn, string aclEntryType, string aclEntryTypeID, PermissionJoin permissionJoinType, params string[] permissions)
        {
            SelectQuery query = new SelectQuery(entityTable);
            query.AddParameter(entityTableAclIDColumn, string.Empty, entityTable);
            query.AddTableJoin("ckbx_AccessControlEntries", QueryJoinType.Inner, "AclID", entityTable, entityTableAclIDColumn);
            SelectQuery query2 = SelectPolicyWithPermissionQuery.GetQuery(permissionJoinType, permissions);
            query2.AddCriterion("ckbx_PolicyPermissions", "PolicyID", CriteriaOperator.EqualTo, "ckbx_AccessControlEntry", "PolicyID");
            SelectQuery query3 = new SelectQuery("ckbx_AccessControlEntry");
            query3.AddParameter("EntryID", string.Empty, "ckbx_AccessControlEntry");
            CriteriaCollection joinCriteria = new CriteriaCollection(CriteriaJoinType.And);
            joinCriteria.AddCriterion(new QueryCriterion(new LiteralParameter("CONVERT(nvarchar, g2.GroupID)"), CriteriaOperator.EqualTo, new SelectParameter("EntryIdentifier", string.Empty, "ckbx_AccessControlEntry")));
            QueryTableJoin join = new QueryTableJoin("ckbx_Group", QueryJoinType.Inner, joinCriteria, "g2");
            query3.AddTableJoin(join);
            query3.AddTableJoin("ckbx_GroupMembers", QueryJoinType.Inner, "GroupID", "g2", "GroupID");
            query3.AddCriterion(new NotExistsCriterion(query2));
            query3.AddCriterion(new QueryCriterion(new SelectParameter("EntryType", string.Empty, "ckbx_AccessControlEntry"), CriteriaOperator.EqualTo, new LiteralParameter("'Checkbox.Users.Group'")));
            query3.AddCriterion(new QueryCriterion(new SelectParameter("MemberUniqueIdentifier", string.Empty, "ckbx_GroupMembers"), CriteriaOperator.EqualTo, new LiteralParameter("'" + aclEntryTypeID + "'")));
            query3.AddCriterion("ckbx_AccessControlEntry", "EntryID", CriteriaOperator.EqualTo, "ckbx_AccessControlEntries", "EntryID");
            query.AddCriterion(new ExistsCriterion(query3));
            return query;
        }
    }
}

