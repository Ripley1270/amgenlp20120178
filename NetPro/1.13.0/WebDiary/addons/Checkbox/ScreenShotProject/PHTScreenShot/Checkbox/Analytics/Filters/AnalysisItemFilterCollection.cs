﻿namespace Checkbox.Analytics.Filters
{
    using System;

    [Serializable]
    public class AnalysisItemFilterCollection : FilterDataCollection
    {
        public override string ParentType
        {
            get
            {
                return "AnalysisItem";
            }
            set
            {
            }
        }
    }
}

