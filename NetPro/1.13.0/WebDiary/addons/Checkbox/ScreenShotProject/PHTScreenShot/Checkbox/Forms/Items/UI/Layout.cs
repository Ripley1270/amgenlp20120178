﻿namespace Checkbox.Forms.Items.UI
{
    using System;

    public enum Layout
    {
        Horizontal = 1,
        Vertical = 2
    }
}

