﻿namespace Checkbox.Security.Configuration
{
    using Prezza.Framework.Common;
    using Prezza.Framework.Configuration;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Collections;
    using System.Xml;

    public class SecurityConfiguration : ConfigurationBase, IXmlConfigurationBase
    {
        protected string defaultIdentityProvider;
        protected Hashtable identityProviders;

        public SecurityConfiguration() : this(string.Empty)
        {
        }

        public SecurityConfiguration(string sectionName) : base(sectionName)
        {
            try
            {
                this.identityProviders = new Hashtable();
                this.defaultIdentityProvider = string.Empty;
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
            }
        }

        internal ProviderData GetIdentityProviderConfig(string providerName)
        {
            try
            {
                ArgumentValidation.CheckForEmptyString(providerName, "providerName");
                if (!this.identityProviders.Contains(providerName))
                {
                    throw new Exception("No configuration for specified identification provider exists.  Specified provider was: " + providerName);
                }
                return (ProviderData) this.identityProviders[providerName];
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessInternal"))
                {
                    throw;
                }
                return null;
            }
        }

        public void LoadFromXml(XmlNode node)
        {
            try
            {
                ArgumentValidation.CheckForNullReference(node, "node");
                this.identityProviders = new Hashtable();
                foreach (XmlNode node2 in node.SelectNodes("/securityConfiguration/providers/provider[@type='identity']"))
                {
                    string attributeText = XmlUtility.GetAttributeText(node2, "name");
                    string filePath = XmlUtility.GetAttributeText(node2, "filePath");
                    string typeName = XmlUtility.GetAttributeText(node2, "configDataType");
                    bool attributeBool = XmlUtility.GetAttributeBool(node2, "default");
                    if (attributeText == string.Empty)
                    {
                        throw new Exception("An identity provider was defined in the security configuration file, but the name was not specified: " + attributeText);
                    }
                    if (filePath == string.Empty)
                    {
                        throw new Exception("An identity provider was defined in the security configuration file, but the provider's configuration file path was not specified: " + attributeText);
                    }
                    if (typeName == string.Empty)
                    {
                        throw new Exception("An identity provider was defined in the security configuration file, but the provider's configuration object type name was not specified: " + attributeText);
                    }
                    object[] extraParams = new object[] { attributeText };
                    ProviderData data = (ProviderData) ConfigurationManager.GetConfiguration(filePath, typeName, extraParams);
                    if (data == null)
                    {
                        throw new Exception("Identity provider [" + attributeText + "] was specified but it's configuration could not be loaded: " + attributeText);
                    }
                    this.identityProviders.Add(data.Name, data);
                    if (attributeBool)
                    {
                        this.defaultIdentityProvider = data.Name;
                    }
                }
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
            }
        }

        internal string DefaultIdentityProvider
        {
            get
            {
                return this.defaultIdentityProvider;
            }
            set
            {
                this.defaultIdentityProvider = value;
            }
        }
    }
}

