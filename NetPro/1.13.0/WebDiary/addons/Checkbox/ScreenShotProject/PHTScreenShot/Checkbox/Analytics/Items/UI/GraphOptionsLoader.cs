﻿namespace Checkbox.Analytics.Items.UI
{
    using Checkbox.Forms.Items.UI;
    using Prezza.Framework.Common;
    using System;
    using System.IO;
    using System.Xml;

    public static class GraphOptionsLoader
    {
        private static string GetGraphTypeString(AppearanceData appearance)
        {
            switch (appearance.GraphType)
            {
                case GraphType.SummaryTable:
                    return "SummaryTableOptions";

                case GraphType.ColumnGraph:
                    return "ColumnGraphOptions";

                case GraphType.PieGraph:
                    return "PieGraphOptions";

                case GraphType.LineGraph:
                    return "LineGraphOptions";

                case GraphType.BarGraph:
                    return "BarGraphOptions";

                case GraphType.CrossTab:
                    return "CrossTabOptions";

                case GraphType.Doughnut:
                    return "PieGraphOptions";
            }
            return "OtherGraphTypeOptions";
        }

        public static void PopulateAppearanceData(AnalysisItemAppearanceData appearance, string graphOptionsPath)
        {
            try
            {
                if (File.Exists(graphOptionsPath))
                {
                    XmlDocument document = new XmlDocument();
                    document.Load(graphOptionsPath);
                    string graphTypeString = GetGraphTypeString(appearance);
                    appearance.BackgroundColor = XmlUtility.GetNodeText(document.SelectSingleNode("/GraphOptions/" + graphTypeString + "/background/color"));
                    appearance.BackgroundGradient = XmlUtility.GetNodeBool(document.SelectSingleNode("/GraphOptions/" + graphTypeString + "/background/use_gradient"));
                    appearance.Color = XmlUtility.GetNodeText(document.SelectSingleNode("/GraphOptions/" + graphTypeString + "/color"));
                    appearance.Explosion = new int?(XmlUtility.GetNodeInt(document.SelectSingleNode("/GraphOptions/" + graphTypeString + "/explosion")));
                    appearance.ForegroundColor = XmlUtility.GetNodeText(document.SelectSingleNode("/GraphOptions/" + graphTypeString + "/foreColor"));
                    appearance.GraphHeight = new int?(XmlUtility.GetNodeInt(document.SelectSingleNode("/GraphOptions/" + graphTypeString + "/chart_height")));
                    appearance.GraphSpacing = new int?(XmlUtility.GetNodeInt(document.SelectSingleNode("/GraphOptions/" + graphTypeString + "/spacing")));
                    appearance.GraphWidth = new int?(XmlUtility.GetNodeInt(document.SelectSingleNode("/GraphOptions/" + graphTypeString + "/chart_width")));
                    appearance.LegendWidth = new int?(XmlUtility.GetNodeInt(document.SelectSingleNode("/GraphOptions/" + graphTypeString + "/legend/width")));
                    appearance.MaxColumnWidth = new int?(XmlUtility.GetNodeInt(document.SelectSingleNode("/GraphOptions/" + graphTypeString + "/max_column_width")));
                    appearance.PieGraphColors = XmlUtility.GetNodeTextAsCSV(document.SelectNodes("/GraphOptions/" + graphTypeString + "/colors/color"));
                    appearance.Precision = new int?(XmlUtility.GetNodeInt(document.SelectSingleNode("/GraphOptions/precision")));
                    appearance.Separator = XmlUtility.GetNodeText(document.SelectSingleNode("/GraphOptions/" + graphTypeString + "/data_labels/separator"));
                    appearance.ShowDataLabels = XmlUtility.GetNodeBool(document.SelectSingleNode("/GraphOptions/" + graphTypeString + "/data_labels/visible"));
                    appearance.ShowDataLabelsXAxisTitle = XmlUtility.GetNodeBool(document.SelectSingleNode("/GraphOptions/" + graphTypeString + "/data_labels/showXTitle"));
                    appearance.ShowDataLabelValues = XmlUtility.GetNodeBool(document.SelectSingleNode("/GraphOptions/" + graphTypeString + "/data_labels/show_value"));
                    appearance.ShowDataLabelZeroValues = XmlUtility.GetNodeBool(document.SelectSingleNode("/GraphOptions/" + graphTypeString + "/data_labels/show_zero_values"));
                    appearance.ShowHeader = XmlUtility.GetNodeBool(document.SelectSingleNode("/GraphOptions/" + graphTypeString + "/text_header/visible"));
                    appearance.ShowLegend = XmlUtility.GetNodeBool(document.SelectSingleNode("/GraphOptions/" + graphTypeString + "/legend/visible"));
                    appearance.ShowPercent = XmlUtility.GetNodeBool(document.SelectSingleNode("/GraphOptions/" + graphTypeString + "/show_percent"));
                    appearance.ShowResponseCountInTitle = XmlUtility.GetNodeBool(document.SelectSingleNode("/GraphOptions/" + graphTypeString + "/title/show_response_count"));
                    appearance.ShowTitle = XmlUtility.GetNodeBool(document.SelectSingleNode("/GraphOptions/" + graphTypeString + "/title/visible"));
                    appearance.TitleColor = XmlUtility.GetNodeText(document.SelectSingleNode("/GraphOptions/" + graphTypeString + "/title/color"));
                    appearance.TitleFont = XmlUtility.GetNodeText(document.SelectSingleNode("/GraphOptions/" + graphTypeString + "/title/font"));
                    appearance.LegendFont = XmlUtility.GetNodeText(document.SelectSingleNode("/GraphOptions/" + graphTypeString + "/legend/font"));
                }
            }
            catch
            {
            }
        }
    }
}

