﻿namespace Checkbox.Security
{
    using Checkbox.Common;
    using Checkbox.Security.Data;
    using Checkbox.Users;
    using Prezza.Framework.Common;
    using Prezza.Framework.Data;
    using Prezza.Framework.Data.QueryCriteria;
    using Prezza.Framework.Data.QueryParameters;
    using Prezza.Framework.Security;
    using Prezza.Framework.Security.Principal;
    using System;
    using System.Collections.Generic;
    using System.Data;

    public abstract class AccessControllablePDOSecurityEditor : SecurityEditor
    {
        protected AccessControllablePDOSecurityEditor(IAccessControllable resource) : base(resource)
        {
            base.GetPermissible = new PermissibleResourceDelegate(AccessControllablePDOSecurityEditor.GetPermissbleEntity);
        }

        public override List<IAccessControlEntry> GetAccessPermissible(params string[] permissions)
        {
            string[] strArray;
            string[] supportedPermissions;
            if (!base.AuthorizationProvider.Authorize(base.CurrentPrincipal, base.ControllableResource, this.RequiredEditorPermission))
            {
                throw new AuthorizationException();
            }
            if (((permissions == null) || (permissions.Length == 0)) || ((permissions.Length == 1) && Utilities.IsNullOrEmpty(permissions[0])))
            {
                strArray = null;
                supportedPermissions = base.ControllableResource.SupportedPermissions;
            }
            else
            {
                strArray = permissions;
                supportedPermissions = permissions;
            }
            SelectQuery query = QueryFactory.GetSelectAvailableIdentitiesNotInAclQuery(base.ControllableResource.ACL, base.CurrentPrincipal, strArray, supportedPermissions);
            SelectQuery selectGroupsWithPermissionAndNotInAclQuery = QueryFactory.GetSelectGroupsWithPermissionAndNotInAclQuery(base.ControllableResource.ACL, base.CurrentPrincipal);
            List<IAccessControlEntry> list = new List<IAccessControlEntry>();
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(selectGroupsWithPermissionAndNotInAclQuery + ";" + query);
            using (IDataReader reader = database.ExecuteReader(sqlStringCommandWrapper))
            {
                try
                {
                    while (reader.Read())
                    {
                        list.Add(new AccessControlEntry("Checkbox.Users.Group", DbUtility.GetValueFromDataReader<int>(reader, "GroupID", -1).ToString(), DbUtility.GetValueFromDataReader<string>(reader, "GroupName", string.Empty), -1));
                    }
                    reader.NextResult();
                    while (reader.Read())
                    {
                        list.Add(new AccessControlEntry("Prezza.Framework.Security.ExtendedPrincipal", DbUtility.GetValueFromDataReader<string>(reader, "UniqueIdentifier", string.Empty), DbUtility.GetValueFromDataReader<string>(reader, "UniqueIdentifier", string.Empty), -1));
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return list;
        }

        protected static IAccessPermissible GetPermissbleEntity(IAccessControlEntry entryData)
        {
            int num;
            if (entryData.AclEntryTypeIdentifier.Equals("Prezza.Framework.Security.ExtendedPrincipal", StringComparison.InvariantCultureIgnoreCase))
            {
                return (ExtendedPrincipal) UserManager.GetUser(entryData.AclEntryIdentifier);
            }
            if (entryData.AclEntryTypeIdentifier.Equals("Checkbox.Users.Group", StringComparison.InvariantCultureIgnoreCase) && int.TryParse(entryData.AclEntryIdentifier, out num))
            {
                return Group.GetGroup(num);
            }
            return null;
        }

        public override void SetDefaultPolicy(Policy p)
        {
            if (!base.AuthorizationProvider.Authorize(base.CurrentPrincipal, base.ControllableResource, this.RequiredEditorPermission))
            {
                throw new AuthorizationException();
            }
            ArgumentValidation.CheckForNullReference(p, "p");
            ArgumentValidation.CheckExpectedType(base.ControllableResource, typeof(AccessControllablePersistedDomainObject));
            AccessControllablePersistedDomainObject controllableResource = (AccessControllablePersistedDomainObject) base.ControllableResource;
            if (controllableResource.DefaultPolicyID > 0)
            {
                AccessManager.UpdatePolicy(controllableResource.DefaultPolicyID.Value, p);
                controllableResource.DefaultPolicyID = controllableResource.DefaultPolicyID;
            }
            else
            {
                controllableResource.DefaultPolicyID = new int?(AccessManager.CreatePolicy(p));
            }
            UpdateQuery query = new UpdateQuery(controllableResource.DomainDBTableName);
            query.AddValueParameter(this.DefaultPolicyIDFieldName, controllableResource.DefaultPolicyID);
            query.AddCriterion(new QueryCriterion(new SelectParameter(controllableResource.DomainDBIdentityColumnName), CriteriaOperator.EqualTo, new LiteralParameter(controllableResource.ID)));
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
            database.ExecuteNonQuery(sqlStringCommandWrapper);
            controllableResource.ReloadDefaultPolicy();
        }

        protected virtual string DefaultPolicyIDFieldName
        {
            get
            {
                return "DefaultPolicy";
            }
        }
    }
}

