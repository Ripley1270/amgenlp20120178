﻿namespace Checkbox.Forms.Items.UI
{
    using Prezza.Framework.Data;
    using System;
    using System.Data;

    [Serializable]
    public abstract class RatingScale : SelectLayout
    {
        protected RatingScale()
        {
            this.LayoutDirection = Layout.Horizontal;
        }

        protected override void Create(IDbTransaction transaction)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Appearance_CreateRS");
            storedProcCommandWrapper.AddInParameter("AppearanceCode", DbType.String, this.AppearanceCode);
            storedProcCommandWrapper.AddInParameter("LayoutStyle", DbType.String, this.LayoutDirection);
            storedProcCommandWrapper.AddInParameter("ShowNumberLabels", DbType.Int32, this.ShowNumberLabels ? 1 : 0);
            storedProcCommandWrapper.AddInParameter("ItemPosition", DbType.String, base.ItemPosition);
            storedProcCommandWrapper.AddInParameter("LabelPosition", DbType.String, this.LabelPosition.ToString());
            storedProcCommandWrapper.AddInParameter("Width", DbType.Int32, this.Width.HasValue ? new int?(this.Width.Value) : null);
            storedProcCommandWrapper.AddInParameter("GridLines", DbType.String, this.GridLines);
            storedProcCommandWrapper.AddOutParameter("AppearanceID", DbType.Int32, 4);
            database.ExecuteNonQuery(storedProcCommandWrapper, transaction);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("AppearanceID");
            if ((parameterValue == null) || (parameterValue == DBNull.Value))
            {
                throw new Exception("Unable to save appearance data.");
            }
            base.ID = new int?((int) parameterValue);
        }

        protected override void Update(IDbTransaction transaction)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Appearance_UpdateRS");
            storedProcCommandWrapper.AddInParameter("AppearanceID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("LayoutStyle", DbType.String, this.Layout);
            storedProcCommandWrapper.AddInParameter("ShowNumberLabels", DbType.Int32, this.ShowNumberLabels ? 1 : 0);
            storedProcCommandWrapper.AddInParameter("ItemPosition", DbType.String, base.ItemPosition);
            storedProcCommandWrapper.AddInParameter("LabelPosition", DbType.String, this.LabelPosition.ToString());
            storedProcCommandWrapper.AddInParameter("Width", DbType.Int32, this.Width.HasValue ? new int?(this.Width.Value) : null);
            storedProcCommandWrapper.AddInParameter("GridLines", DbType.String, this.GridLines);
            database.ExecuteNonQuery(storedProcCommandWrapper);
        }

        public override int? Columns
        {
            get
            {
                return 1;
            }
            set
            {
            }
        }
    }
}

