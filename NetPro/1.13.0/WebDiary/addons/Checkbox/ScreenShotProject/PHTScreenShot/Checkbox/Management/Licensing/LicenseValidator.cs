﻿namespace Checkbox.Management.Licensing
{
    using System;
    using Xheo.Licensing;

    public class LicenseValidator : IDisposable
    {
        private readonly ExtendedLicense _license;
        private readonly string _licenseError;
        private readonly bool _licenseValid;

        public LicenseValidator(ExtendedLicense license, bool licenseValid, string licenseError)
        {
            this._license = license;
            this._licenseValid = licenseValid;
            this._licenseError = licenseError;
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing && (this._license != null))
            {
                this._license.Dispose();
            }
        }

        public string GetLicenseValue(string valueName)
        {
            if (this._license != null)
            {
                return this._license.Values[valueName];
            }
            return null;
        }

        public ExtendedLicense ActiveLicense
        {
            get
            {
                return this._license;
            }
        }

        public bool IsLicenseValid
        {
            get
            {
                return this._licenseValid;
            }
        }

        public bool IsTrial
        {
            get
            {
                if (this._license != null)
                {
                    return this._license.IsTrial;
                }
                return true;
            }
        }

        public bool LicenseActivated
        {
            get
            {
                return ((this._license != null) && this._license.IsActivated);
            }
        }

        public string LicenseError
        {
            get
            {
                return this._licenseError;
            }
        }

        public string LicenseType
        {
            get
            {
                if (this._license != null)
                {
                    return this._license.Type;
                }
                return string.Empty;
            }
        }

        public string SerialNumber
        {
            get
            {
                if (this._license != null)
                {
                    return this._license.SerialNumber;
                }
                return string.Empty;
            }
        }
    }
}

