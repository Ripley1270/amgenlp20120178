﻿namespace Checkbox.Panels.Security
{
    using Prezza.Framework.Security;
    using System;

    [Serializable]
    internal class EmailListPolicy : Policy
    {
        public EmailListPolicy()
        {
        }

        public EmailListPolicy(string[] permissions) : base(permissions)
        {
        }
    }
}

