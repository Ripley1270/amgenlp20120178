﻿using Checkbox.Forms.Items;
using Prezza.Framework.Data;
using Prezza.Framework.ExceptionHandling;
using System;
using System.Data;
using System.Runtime.CompilerServices;

namespace Checkbox.Forms.Items.Configuration
{
    [Serializable]
    public class SelectManyData : SelectItemData
    {
        //private Checkbox.Web.Forms.WebResponseSession _responseSession;

        private PHTScreenShotSession _screenShotSession;

        protected override ItemData Copy()
        {
            SelectManyData data = (SelectManyData) base.Copy();
            if (data != null)
            {
                data.MinToSelect = this.MinToSelect;
                data.MaxToSelect = this.MaxToSelect;
            }
            return data;
        }

        protected override void Create(IDbTransaction transaction)
        {
            base.Create(transaction);
            if (base.ID <= 0)
            {
                throw new ApplicationException("No DataID specified.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_InsertSelectMany");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("TextID", DbType.String, this.TextID);
            storedProcCommandWrapper.AddInParameter("SubTextID", DbType.String, this.SubTextID);
            storedProcCommandWrapper.AddInParameter("IsRequired", DbType.Int32, this.IsRequired);
            storedProcCommandWrapper.AddInParameter("AllowOther", DbType.Int32, this.AllowOther);
            storedProcCommandWrapper.AddInParameter("OtherTextID", DbType.String, this.OtherTextID);
            storedProcCommandWrapper.AddInParameter("Randomize", DbType.Int32, this.Randomize);
            storedProcCommandWrapper.AddInParameter("MinToSelect", DbType.Int32, this.MinToSelect);
            storedProcCommandWrapper.AddInParameter("MaxToSelect", DbType.Int32, this.MaxToSelect);
            database.ExecuteNonQuery(storedProcCommandWrapper, transaction);
            this.UpdateLists(transaction);
        }

        protected override Item CreateItem()
        {
            return new SelectMany();
        }

        protected override DataSet GetConcreteConfigurationDataSet()
        {
            if (base.ID <= 0)
            {
                throw new ApplicationException("No DataID specified.");
            }
            try
            {
                string calledStoredProcedure = string.Empty;
                this._screenShotSession = new PHTScreenShotSession();
                if (this._screenShotSession.ScreenShotMode)
                    calledStoredProcedure = "PHTScreenShotItemDataGetSelectMany";
                else
                    calledStoredProcedure = "ckbx_ItemData_GetSelectMany";

                Database database = DatabaseFactory.CreateDatabase();
                //DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_GetSelectMany");
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper(calledStoredProcedure);
                storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
                DataSet concreteConfigurationDataSet = base.GetConcreteConfigurationDataSet();
                database.LoadDataSet(storedProcCommandWrapper, concreteConfigurationDataSet, this.ConfigurationDataTableNames);
                if (concreteConfigurationDataSet.Tables.Contains("ItemOptions"))
                {
                    concreteConfigurationDataSet.Tables["ItemOptions"].PrimaryKey = new DataColumn[] { concreteConfigurationDataSet.Tables["ItemOptions"].Columns["OptionID"], concreteConfigurationDataSet.Tables["ItemOptions"].Columns["ItemID"] };
                }
                if (concreteConfigurationDataSet.Tables.Contains("ItemLists"))
                {
                    concreteConfigurationDataSet.Tables["ItemLists"].PrimaryKey = new DataColumn[] { concreteConfigurationDataSet.Tables["ItemLists"].Columns["ItemID"], concreteConfigurationDataSet.Tables["ItemLists"].Columns["ListID"] };
                }
                return concreteConfigurationDataSet;
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessProtected"))
                {
                    throw;
                }
                return null;
            }
        }

        protected override void LoadFromDataRow(DataRow data)
        {
            try
            {
                this.IsRequired = DbUtility.GetValueFromDataRow<int>(data, "IsRequired", 0) == 1;
                this.AllowOther = DbUtility.GetValueFromDataRow<int>(data, "AllowOther", 0) == 1;
                this.Randomize = DbUtility.GetValueFromDataRow<int>(data, "Randomize", 0) == 1;
                this.MinToSelect = DbUtility.GetValueFromDataRow<int?>(data, "MinToSelect", null);
                this.MaxToSelect = DbUtility.GetValueFromDataRow<int?>(data, "MaxToSelect", null);
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessProtected"))
                {
                    throw;
                }
            }
        }

        protected override void Update(IDbTransaction transaction)
        {
            base.Update(transaction);
            if (base.ID <= 0)
            {
                throw new ApplicationException("No DataID specified.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_UpdateSelectMany");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("TextID", DbType.String, this.TextID);
            storedProcCommandWrapper.AddInParameter("SubTextID", DbType.String, this.SubTextID);
            storedProcCommandWrapper.AddInParameter("IsRequired", DbType.Int32, this.IsRequired);
            storedProcCommandWrapper.AddInParameter("AllowOther", DbType.Int32, this.AllowOther);
            storedProcCommandWrapper.AddInParameter("OtherTextID", DbType.String, this.OtherTextID);
            storedProcCommandWrapper.AddInParameter("Randomize", DbType.Int32, this.Randomize);
            storedProcCommandWrapper.AddInParameter("MinToSelect", DbType.Int32, this.MinToSelect);
            storedProcCommandWrapper.AddInParameter("MaxToSelect", DbType.Int32, this.MaxToSelect);
            database.ExecuteNonQuery(storedProcCommandWrapper, transaction);
            this.UpdateLists(transaction);
        }

        public override string DataTableName
        {
            get
            {
                return "SelectManyData";
            }
        }

        public override bool IsRequired
        {
            get
            {
                return (this.MinToSelect.HasValue && (this.MinToSelect > 0));
            }
            set
            {
            }
        }

        protected DataTable ListData { get; set; }

        public int? MaxToSelect { get; set; }

        public int? MinToSelect { get; set; }
    }
}

