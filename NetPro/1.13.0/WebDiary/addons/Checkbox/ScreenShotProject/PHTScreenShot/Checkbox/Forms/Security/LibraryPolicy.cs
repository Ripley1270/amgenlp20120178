﻿namespace Checkbox.Forms.Security
{
    using Prezza.Framework.Security;
    using System;

    [Serializable]
    internal class LibraryPolicy : Policy
    {
        private static string[] allowablePermissions = new string[] { "Library.Edit", "Library.View", "Library.Delete" };

        public LibraryPolicy() : base(allowablePermissions)
        {
        }

        public LibraryPolicy(string[] permissions) : base(permissions)
        {
        }

        public static string[] AllowablePermissions
        {
            get
            {
                return allowablePermissions;
            }
        }
    }
}

