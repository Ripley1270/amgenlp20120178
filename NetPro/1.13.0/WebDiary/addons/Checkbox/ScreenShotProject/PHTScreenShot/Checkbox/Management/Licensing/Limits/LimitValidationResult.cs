﻿namespace Checkbox.Management.Licensing.Limits
{
    using System;

    public enum LimitValidationResult
    {
        LimitExceeded = 3,
        LimitNotReached = 1,
        LimitReached = 2,
        UnableToEvaluate = 4
    }
}

