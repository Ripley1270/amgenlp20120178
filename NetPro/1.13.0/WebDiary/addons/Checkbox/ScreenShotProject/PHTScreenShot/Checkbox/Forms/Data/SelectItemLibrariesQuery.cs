﻿namespace Checkbox.Forms.Data
{
    using Checkbox.Security.Data;
    using Prezza.Framework.Data;
    using System;

    internal class SelectItemLibrariesQuery
    {
        private SelectItemLibrariesQuery()
        {
        }

        internal static SelectQuery GetQuery(string entryType, string entryIdentifier)
        {
            return Checkbox.Security.Data.QueryFactory.GetSelectEntitiesWithPermissionQuery("ckbx_Library", "LibraryID", "AclID", entryType, entryIdentifier, "Library.View", "DefaultPolicy");
        }
    }
}

