﻿namespace Checkbox.Forms
{
    using Checkbox.Common;
    using Checkbox.Forms.Items;
    using Checkbox.Forms.Logic;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    using System.Xml;
    using System.Xml.Schema;
    using System.Xml.Serialization;

    [Serializable]
    public class ResponsePage : Page, IXmlSerializable
    {
        private bool _exclude;
        private List<Item> _items;
        private readonly int? _layoutTemplateID;
        private readonly bool _randomize;
        private List<Rule> _rules;
        private Dictionary<int, List<string>> _validationErrors;
        private static readonly object EventPageLoad = new object();
        private static readonly object EventPageUnLoad = new object();

        public event EventHandler Load
        {
            add
            {
                this.Events.AddHandler(EventPageLoad, value);
            }
            remove
            {
                this.Events.RemoveHandler(EventPageLoad, value);
            }
        }

        public event EventHandler UnLoad
        {
            add
            {
                this.Events.AddHandler(EventPageUnLoad, value);
            }
            remove
            {
                this.Events.RemoveHandler(EventPageUnLoad, value);
            }
        }

        internal ResponsePage(int pageID, int position, bool randomize, int? layoutTemplateID) : base(pageID, position)
        {
            this._randomize = randomize;
            this._layoutTemplateID = layoutTemplateID;
        }

        protected void AddValidationErrors(int itemId, List<string> errors)
        {
            int? itemNumber = this.Parent.GetItemNumber(itemId);
            if (itemNumber.HasValue)
            {
                this.ValidationErrors[itemNumber.Value] = errors;
            }
        }

        private List<Item> BuildItemList()
        {
            List<int> list = this._randomize ? this.RandomizeItems() : base.ItemIDs;
            List<Item> list2 = new List<Item>();
            foreach (int num in list)
            {
                if (this.Parent.Items.ContainsKey(num) && this.Parent.Items[num].IsActive)
                {
                    list2.Add(this.Parent.Items[num]);
                }
            }
            foreach (Item item in list2)
            {
                item.ContainingPagePosition = base.Position;
                this.Load += new EventHandler(item.Page_Load);
            }
            return list2;
        }

        public XmlSchema GetSchema()
        {
            return null;
        }

        internal void OnLoad(bool fireEvents)
        {
            this.RunRules(RuleEventTrigger.Load);
            if (!this.Excluded)
            {
                if (fireEvents)
                {
                    EventHandler handler = (EventHandler) this.Events[EventPageLoad];
                    if (handler != null)
                    {
                        handler(this, EventArgs.Empty);
                    }
                }
                this.RunItemRules();
                bool flag = true;
                foreach (Item item in this.Items)
                {
                    flag = flag && item.Excluded;
                }
                this.Excluded = flag;
            }
        }

        internal void OnUnLoad()
        {
            this.RunRules(RuleEventTrigger.UnLoad);
            if (!this.Excluded)
            {
                EventHandler handler = (EventHandler) this.Events[EventPageUnLoad];
                if (handler != null)
                {
                    handler(this, EventArgs.Empty);
                }
            }
        }

        private List<int> RandomizeItems()
        {
            List<int> pageItemOrder = this.Parent.GetPageItemOrder(base.PageId);
            if (pageItemOrder.Count == 0)
            {
                pageItemOrder = Utilities.RandomizeList<int>(base.ItemIDs);
                for (int i = 0; i < pageItemOrder.Count; i++)
                {
                    this.Parent.SavePageItemOrder(base.PageId, pageItemOrder[i], i + 1);
                }
            }
            return pageItemOrder;
        }

        public void ReadXml(XmlReader reader)
        {
            throw new NotImplementedException();
        }

        protected void RunItemRules()
        {
            foreach (Item item in this.Items)
            {
                if (item is ResponseItem)
                {
                    ((ResponseItem) item).RunRules();
                }
            }
        }

        public void RunRules(RuleEventTrigger trigger)
        {
            this.Excluded = false;
            this.Parent.RulesEngine.RunRules(this, trigger);
        }

        protected virtual bool ValidateItems()
        {
            this.ValidationErrors.Clear();
            bool flag = true;
            foreach (Item item in this.Items)
            {
                if (!item.Excluded && (item is ResponseItem))
                {
                    ((ResponseItem) item).Validate();
                    if (!((ResponseItem) item).Valid)
                    {
                        flag = false;
                        this.AddValidationErrors(item.ID, ((ResponseItem) item).ValidationErrors);
                    }
                }
            }
            return flag;
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("page");
            writer.WriteAttributeString("pageId", base.PageId.ToString());
            writer.WriteAttributeString("position", base.Position.ToString());
            writer.WriteAttributeString("isExcluded", this.Excluded.ToString());
            writer.WriteAttributeString("layoutTemplateId", this.LayoutTemplateID.ToString());
            foreach (Item item in this.Items)
            {
                item.WriteXml(writer);
            }
            writer.WriteEndElement();
        }

        protected EventHandlerList Events
        {
            get
            {
                if (base.EventHandlers == null)
                {
                    base.EventHandlers = new EventHandlerList();
                }
                return base.EventHandlers;
            }
        }

        public bool Excluded
        {
            get
            {
                return this._exclude;
            }
            internal set
            {
                if (value != this._exclude)
                {
                    this._exclude = value;
                    foreach (Item item in this.Items)
                    {
                        item.Excluded = value;
                    }
                }
            }
        }

        public override List<Item> Items
        {
            get
            {
                if (this._items == null)
                {
                    this._items = this.BuildItemList();
                }
                return this._items;
            }
        }

        public int? LayoutTemplateID
        {
            get
            {
                return this._layoutTemplateID;
            }
        }

        internal Response Parent { get; set; }

        internal List<Rule> Rules
        {
            get
            {
                if (this._rules == null)
                {
                    this._rules = new List<Rule>();
                }
                return this._rules;
            }
        }

        public bool Valid
        {
            get
            {
                return this.ValidateItems();
            }
        }

        public Dictionary<int, List<string>> ValidationErrors
        {
            get
            {
                if (this._validationErrors == null)
                {
                    this._validationErrors = new Dictionary<int, List<string>>();
                }
                return this._validationErrors;
            }
        }
    }
}

