﻿namespace Checkbox.Forms
{
    using Checkbox.Globalization.Text;
    using System;
    using System.IO;
    using System.Text;
    using System.Xml;

    public static class ResponseFormatter
    {
        public static string Format(Response response, string format)
        {
            if (format.Equals("Xml", StringComparison.InvariantCultureIgnoreCase))
            {
                return FormatXml(response);
            }
            if (format.Equals("Html", StringComparison.InvariantCultureIgnoreCase))
            {
                return FormatHtml(response);
            }
            return FormatText(response);
        }

        private static string FormatHtml(Response response)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("<div class=\"title\">");
            builder.Append(response.Title);
            builder.AppendLine("</div>");
            builder.AppendLine("<hr size=\"1\" />");
            builder.Append("<div style=\"padding:3px;\"><span style=\"width:150px;font-weight:bold;\">");
            builder.Append(TextManager.GetText("/responseProperty/uniqueIdentifier/text", response.LanguageCode));
            builder.Append(":&nbsp;&nbsp;");
            builder.Append("</span><span>");
            builder.Append(response.UniqueIdentifier);
            builder.Append("</span></div>");
            builder.Append("<div style=\"padding:3px;\"><span style=\"width:150px;font-weight:bold;\">");
            builder.Append(TextManager.GetText("/responseProperty/responseId/text", response.LanguageCode));
            builder.Append(":&nbsp;&nbsp;");
            builder.Append("</span><span>");
            builder.Append(response.ID.ToString());
            builder.Append("</span></div>");
            builder.Append("<div style=\"padding:3px;\"><span style=\"width:150px;font-weight:bold;\">");
            builder.Append(TextManager.GetText("/responseProperty/responseGuid/text", response.LanguageCode));
            builder.Append(":&nbsp;&nbsp;");
            builder.Append("</span><span>");
            builder.Append(response.GUID.ToString());
            builder.Append("</span></div>");
            builder.Append("<div style=\"padding:3px;\"><span style=\"width:150px;font-weight:bold;\">");
            builder.Append(TextManager.GetText("/responseProperty/started/text", response.LanguageCode));
            builder.Append(":&nbsp;&nbsp;");
            builder.Append("</span><span>");
            builder.Append(response.DateCreated.Value.ToLongDateString());
            builder.Append("&nbsp;");
            builder.Append(response.DateCreated.Value.ToLongTimeString());
            builder.Append("</span></div>");
            builder.Append("<div style=\"padding:3px;\"><span style=\"width:150px;font-weight:bold;\">");
            builder.Append(TextManager.GetText("/responseProperty/ended/text", response.LanguageCode));
            builder.Append(":&nbsp;&nbsp;");
            builder.Append("</span><span>");
            builder.Append(response.DateCompleted.HasValue ? response.DateCompleted.Value.ToLongDateString() : string.Empty);
            builder.Append("&nbsp;");
            builder.Append(response.DateCompleted.HasValue ? response.DateCompleted.Value.ToLongTimeString() : string.Empty);
            builder.Append("</span></div>");
            builder.AppendLine("<hr size=\"1\" />");
            foreach (ResponsePage page in response.Pages)
            {
                builder.Append(ResponsePageFormatter.Format(page, "Html"));
            }
            return builder.ToString();
        }

        private static string FormatText(Response response)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(response.Title);
            builder.Append('-', response.Title.Length);
            builder.AppendLine();
            builder.Append(TextManager.GetText("/responseProperty/uniqueIdentifier/text", response.LanguageCode));
            builder.Append(":  ");
            builder.AppendLine(response.UniqueIdentifier);
            builder.Append(TextManager.GetText("/responseProperty/responseId/text", response.LanguageCode));
            builder.Append(":  ");
            builder.AppendLine(response.ID.ToString());
            builder.Append(TextManager.GetText("/responseProperty/responseGuid/text", response.LanguageCode));
            builder.Append(":  ");
            builder.AppendLine(response.GUID.ToString());
            builder.Append(TextManager.GetText("/responseProperty/started/text", response.LanguageCode));
            builder.Append(":  ");
            builder.Append(response.DateCreated.Value.ToLongDateString());
            builder.Append(" ");
            builder.AppendLine(response.DateCreated.Value.ToLongTimeString());
            builder.Append(TextManager.GetText("/responseProperty/ended/text", response.LanguageCode));
            builder.Append(":  ");
            builder.Append(response.DateCompleted.HasValue ? response.DateCompleted.Value.ToLongDateString() : string.Empty);
            builder.Append(" ");
            builder.AppendLine(response.DateCompleted.HasValue ? response.DateCompleted.Value.ToLongTimeString() : string.Empty);
            builder.AppendLine();
            foreach (ResponsePage page in response.Pages)
            {
                builder.Append(ResponsePageFormatter.Format(page, "Text"));
            }
            return builder.ToString();
        }

        private static string FormatXml(Response response)
        {
            StringWriter w = new StringWriter();
            XmlTextWriter writer = new XmlTextWriter(w) {
                Formatting = Formatting.Indented
            };
            response.WriteXml(writer);
            writer.Flush();
            writer.Close();
            return w.ToString();
        }
    }
}

