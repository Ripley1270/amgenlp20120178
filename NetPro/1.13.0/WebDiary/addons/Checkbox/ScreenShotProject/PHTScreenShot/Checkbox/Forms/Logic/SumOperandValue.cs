﻿namespace Checkbox.Forms.Logic
{
    using System;

    public class SumOperandValue : GroupOperandValue<double>
    {
        protected override int CompareTo(GroupOperandValue<double> other)
        {
            throw new NotImplementedException("Comparison against lists of items not supported.");
        }

        public override object Value
        {
            get
            {
                double num = 0.0;
                foreach (double num2 in base.Values)
                {
                    num += num2;
                }
                return num;
            }
        }
    }
}

