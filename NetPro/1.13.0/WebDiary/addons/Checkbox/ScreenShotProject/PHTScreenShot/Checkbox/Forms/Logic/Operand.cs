﻿namespace Checkbox.Forms.Logic
{
    using System;

    [Serializable]
    public abstract class Operand
    {
        private OperandValue _value;

        protected Operand()
        {
        }

        public int CompareTo(Operand other, RuleEventTrigger eventTrigger)
        {
            return this.GetValue(eventTrigger).CompareTo(other.GetValue(eventTrigger));
        }

        public virtual OperandValue CreateOperandValue()
        {
            return new OperandValue();
        }

        public virtual OperandValue GetValue(RuleEventTrigger trigger)
        {
            if (this._value == null)
            {
                this._value = this.CreateOperandValue();
                this.InitializeOperandValue(this._value, this.ProtectedGetValue(trigger));
            }
            return this._value;
        }

        public virtual void InitializeOperandValue(OperandValue operandValue, object initializationValue)
        {
            if (operandValue != null)
            {
                operandValue.Initialize(initializationValue);
            }
        }

        protected abstract object ProtectedGetValue(RuleEventTrigger trigger);
    }
}

