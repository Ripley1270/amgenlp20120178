﻿namespace Checkbox.Forms.Items
{
    using System.Collections.ObjectModel;

    public interface ICompositeItem
    {
        ReadOnlyCollection<Item> Items { get; }
    }
}

