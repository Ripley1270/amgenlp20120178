﻿namespace Checkbox.Forms
{
    using Checkbox.Common;
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Data;

    public static class UrlMapper
    {
        public static void AddMapping(string sourceUrl, string destinationUrl)
        {
            SaveMapping(sourceUrl, destinationUrl);
        }

        private static void DeleteMapping(string sourceUrl)
        {
            try
            {
                if (Utilities.IsNotNullOrEmpty(sourceUrl))
                {
                    Database database = DatabaseFactory.CreateDatabase();
                    DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_UrlMappings_Delete");
                    storedProcCommandWrapper.AddInParameter("SourceUrl", DbType.String, sourceUrl);
                    database.ExecuteNonQuery(storedProcCommandWrapper);
                }
            }
            catch (Exception exception)
            {
                ExceptionPolicy.HandleException(exception, "BusinessProtected");
            }
        }

        public static string GetMapping(string sourceUrl)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_UrlMappings_GetDestination");
            storedProcCommandWrapper.AddInParameter("SourceUrl", DbType.String, sourceUrl);
            string str = string.Empty;
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    if (reader.Read())
                    {
                        str = DbUtility.GetValueFromDataReader<string>(reader, "DestinationUrl", string.Empty);
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            if (Utilities.IsNotNullOrEmpty(str))
            {
                return str;
            }
            return sourceUrl;
        }

        public static string GetSource(string destinationUrl)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_UrlMappings_GetSource");
            storedProcCommandWrapper.AddInParameter("DestinationUrl", DbType.String, destinationUrl);
            string str = string.Empty;
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    if (reader.Read())
                    {
                        str = DbUtility.GetValueFromDataReader<string>(reader, "SourceUrl", string.Empty);
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return str;
        }

        public static void RemoveMapping(string destinationUrl)
        {
            DeleteMapping(GetSource(destinationUrl));
        }

        private static void SaveMapping(string sourceUrl, string destinationUrl)
        {
            try
            {
                if (Utilities.IsNotNullOrEmpty(sourceUrl) && Utilities.IsNotNullOrEmpty(destinationUrl))
                {
                    Database database = DatabaseFactory.CreateDatabase();
                    DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_UrlMappings_Upsert");
                    storedProcCommandWrapper.AddInParameter("SourceUrl", DbType.String, sourceUrl);
                    storedProcCommandWrapper.AddInParameter("DestinationUrl", DbType.String, destinationUrl);
                    database.ExecuteNonQuery(storedProcCommandWrapper);
                }
            }
            catch (Exception exception)
            {
                ExceptionPolicy.HandleException(exception, "BusinessProtected");
                throw;
            }
        }

        public static bool SourceMappingExists(string sourceUrl)
        {
            return (GetMapping(sourceUrl) != sourceUrl);
        }
    }
}

