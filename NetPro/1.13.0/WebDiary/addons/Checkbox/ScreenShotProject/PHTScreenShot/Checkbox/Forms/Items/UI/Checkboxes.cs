﻿namespace Checkbox.Forms.Items.UI
{
    using System;

    [Serializable]
    public class Checkboxes : SelectLayout
    {
        public override string AppearanceCode
        {
            get
            {
                return "CHECKBOXES";
            }
        }
    }
}

