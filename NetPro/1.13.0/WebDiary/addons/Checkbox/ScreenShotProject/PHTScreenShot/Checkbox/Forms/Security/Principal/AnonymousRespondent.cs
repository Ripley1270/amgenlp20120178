﻿using Prezza.Framework.Security.Principal;
using System;
using System.Runtime.CompilerServices;
using System.Security.Principal;

namespace Checkbox.Forms.Security.Principal
{
    //using Prezza.Framework.Security.Principal;
    //using System;
    //using System.Runtime.CompilerServices;

    [Serializable]
    public class AnonymousRespondent : ExtendedPrincipal
    {
        public AnonymousRespondent(System.Guid? respondentGuid) : base(new GenericIdentity("AnonymousRespondent", "Anonymous Respondent"), DefaultRoles)
        {
            this.Guid = respondentGuid;
        }

        protected static string[] DefaultRoles
        {
            get
            {
                return new string[] { "Respondent", "Report Viewer" };
            }
        }

        public System.Guid? Guid { get; set; }
    }
}

