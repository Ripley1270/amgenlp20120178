﻿namespace Checkbox.Forms.Items
{
    using Checkbox.Forms.Items.Configuration;
    using Checkbox.Forms.Logic;
    using Prezza.Framework.Common;
    using System;

    [Serializable]
    public class MatrixSumTotalItem : SingleLineTextBoxItem
    {
        private LogicalOperator _operator;
        private double _totalValue;

        public override void Configure(ItemData configuration, string languageCode)
        {
            ArgumentValidation.CheckForNullReference(configuration, "MatrixSumTotalItemData");
            ArgumentValidation.CheckExpectedType(configuration, typeof(MatrixSumTotalItemData));
            base.Configure(configuration, languageCode);
            this._totalValue = ((MatrixSumTotalItemData) configuration).TotalValue;
            this._operator = ((MatrixSumTotalItemData) configuration).ComparisonOperator;
        }

        public LogicalOperator ComparisonOperator
        {
            get
            {
                return this._operator;
            }
        }

        public double TotalValue
        {
            get
            {
                return this._totalValue;
            }
        }
    }
}

