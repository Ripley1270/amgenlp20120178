﻿namespace Checkbox.Forms.Logic.Configuration
{
    using Checkbox.Common;
    using Checkbox.Forms;
    using Checkbox.Forms.Items.Configuration;
    using Checkbox.Forms.Logic;
    using Checkbox.Globalization.Text;
    using Prezza.Framework.Common;
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class OptionOperandData : OperandData
    {
        private string _answerValue;
        private int? _itemID;

        public OptionOperandData(ResponseTemplate context) : this(context, null, null)
        {
        }

        public OptionOperandData(ResponseTemplate context, int? itemID, int? optionID) : this(context, itemID, optionID, null)
        {
        }

        public OptionOperandData(ResponseTemplate context, int? itemID, int? optionID, string answerValue) : base(context)
        {
            this.OptionID = optionID;
            this._itemID = itemID;
            this._answerValue = answerValue;
        }

        protected override void Create(IDbTransaction t)
        {
            base.Create(t);
            DataRow row = base.Context.ValueOperandTable.NewRow();
            row["OperandID"] = base.ID.Value;
            row["OptionID"] = this.OptionID.Value;
            row["ItemID"] = this._itemID.Value;
            row["AnswerValue"] = this._answerValue;
            base.Context.ValueOperandTable.Rows.Add(row);
        }

        public override Operand CreateOperand(Response context, string languageCode)
        {
            return new OptionOperand(this.OptionID.Value);
        }

        public override void Delete(IDbTransaction transaction)
        {
            DataRow[] rowArray = base.Context.ValueOperandTable.Select(this.IdentityColumnName + "=" + base.ID.Value);
            if (rowArray.Length > 0)
            {
                rowArray[0].Delete();
            }
            base.Delete(transaction);
        }

        protected override DataSet GetConcreteConfigurationDataSet()
        {
            if (base.ID <= 0)
            {
                throw new ApplicationException("No DataID specified.");
            }
            try
            {
                Database database = DatabaseFactory.CreateDatabase();
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ValueOperand_GetOperand");
                storedProcCommandWrapper.AddInParameter("OperandID", DbType.Int32, base.ID);
                DataSet dataSet = new DataSet();
                database.LoadDataSet(storedProcCommandWrapper, dataSet, new string[] { this.DataTableName });
                return dataSet;
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessProtected"))
                {
                    throw;
                }
                return null;
            }
        }

        protected override void LoadFromDataRow(DataRow data)
        {
            ArgumentValidation.CheckForNullReference(data, "data");
            try
            {
                this._itemID = new int?((int) data["ItemID"]);
                if (data["OptionID"] != DBNull.Value)
                {
                    this.OptionID = new int?((int) data["OptionID"]);
                }
                if (data["AnswerValue"] != DBNull.Value)
                {
                    this._answerValue = (string) data["AnswerValue"];
                }
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessProtected"))
                {
                    throw;
                }
            }
        }

        public override string ToString()
        {
            string text;
            ItemData item = base.Context.GetItem(this._itemID.Value);
            if ((item == null) || !this.OptionID.HasValue)
            {
                return string.Empty;
            }
            if (item is SelectItemData)
            {
                ListOptionData option = ((SelectItemData) item).GetOption(this.OptionID.Value);
                if (option == null)
                {
                    return string.Empty;
                }
                if (option.IsOther)
                {
                    text = TextManager.GetText("/controlText/ruleDataService/other", base.Context.EditLanguage);
                    if (Utilities.IsNullOrEmpty(text))
                    {
                        text = TextManager.GetText("/controlText/ruleDataService/other", "en-US");
                    }
                    if (Utilities.IsNullOrEmpty(text))
                    {
                        text = "Other";
                    }
                }
                else
                {
                    text = TextManager.GetText(option.TextID, base.Context.EditLanguage);
                }
            }
            else
            {
                text = TextManager.GetText("/listOption/" + this.OptionID + "/text", base.Context.EditLanguage);
            }
            return Utilities.StripHtml(text, null);
        }

        public override void Validate()
        {
            SelectItemData item = (SelectItemData) base.Context.GetItem(this._itemID.Value);
            if (item != null)
            {
                foreach (ListOptionData data2 in item.Options)
                {
                    if (data2.OptionID == this.OptionID.Value)
                    {
                        return;
                    }
                }
            }
            this.OnValidationFailed();
        }

        public override string DataTableName
        {
            get
            {
                return "ValueOperand";
            }
        }

        public override string OperandTypeName
        {
            get
            {
                return "OptionOperand";
            }
        }

        public int? OptionID { get; private set; }
    }
}

