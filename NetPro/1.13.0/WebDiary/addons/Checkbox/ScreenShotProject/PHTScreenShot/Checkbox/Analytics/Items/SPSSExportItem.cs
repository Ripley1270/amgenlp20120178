﻿namespace Checkbox.Analytics.Items
{
    using Checkbox.Analytics.Data;
    using Checkbox.Analytics.Items.Configuration;
    using Checkbox.Common;
    using Checkbox.Forms.Items.Configuration;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    [Serializable]
    public class SPSSExportItem : ExportItem
    {
        private bool _includeOpenEnded;
        private Dictionary<string, int> _itemMappings;
        private Dictionary<int, Dictionary<int, string>> _optionTextOverrides;
        private int _questionCount;
        private Dictionary<string, string> _textMappings;

        protected override void AddColumns(string progressKey, string languageCode, int startProgress, int endProgress)
        {
            this._questionCount = 0;
            base.AddColumns(progressKey, languageCode, startProgress, endProgress);
        }

        protected override void AddColumnsForMatrixItem(MatrixItemData itemData, string prefix)
        {
            string str = base.DetermineColumnTextForItem(itemData, prefix);
            MatrixItemTextDecorator decorator = (MatrixItemTextDecorator) itemData.CreateTextDecorator(base.LanguageCode);
            for (int i = 1; i <= itemData.RowCount; i++)
            {
                if (!itemData.IsRowSubheading(i))
                {
                    string rowAlias = string.Empty;
                    if (base.UseAliases)
                    {
                        rowAlias = decorator.Data.GetRowAlias(i);
                    }
                    if (Utilities.IsNullOrEmpty(rowAlias))
                    {
                        rowAlias = decorator.GetRowText(i);
                    }
                    if (Utilities.IsNullOrEmpty(rowAlias))
                    {
                        rowAlias = "Row" + i;
                    }
                    for (int j = 1; j <= itemData.ColumnCount; j++)
                    {
                        ItemData itemAt = itemData.GetItemAt(i, j);
                        if (j == itemData.PrimaryKeyColumnIndex)
                        {
                            if (((itemAt != null) && itemAt.ItemIsIAnswerable) && this.IncludeOpenEnded)
                            {
                                this._questionCount++;
                                string columnName = "Q" + this._questionCount;
                                this._textMappings[columnName] = string.Concat(new object[] { str, " -- ", rowAlias, " -- Other ", i });
                                this._itemMappings[columnName] = itemData.ID.Value;
                                int? optionId = null;
                                this.AddColumn(this.ColumnCount, columnName, itemData.ID.Value, optionId);
                            }
                        }
                        else
                        {
                            string alias = string.Empty;
                            if (itemData.ColumnCount == 2)
                            {
                                alias = str + " -- " + rowAlias;
                            }
                            else
                            {
                                if (base.UseAliases && (itemData.GetColumnPrototype(j) != null))
                                {
                                    alias = itemData.GetColumnPrototype(j).Alias;
                                }
                                if ((alias == null) || (alias.Trim() == string.Empty))
                                {
                                    alias = decorator.GetColumnText(j);
                                }
                                if ((alias == null) || (alias.Trim() == string.Empty))
                                {
                                    alias = "Column" + j;
                                }
                                alias = str + " -- " + rowAlias + " -- " + alias;
                            }
                            if ((itemAt is TextItemData) && this.IncludeOpenEnded)
                            {
                                this._questionCount++;
                                int? nullable5 = null;
                                this.AddColumn(this.ColumnCount, "Q" + this._questionCount, itemAt.ID.Value, nullable5);
                                this._textMappings["Q" + this._questionCount] = alias;
                                this._itemMappings["Q" + this._questionCount] = itemAt.ID.Value;
                            }
                            else if (itemAt is SelectItemData)
                            {
                                this._questionCount++;
                                if (!(itemAt is SelectManyData))
                                {
                                    int? nullable8 = null;
                                    this.AddColumn(this.ColumnCount, "Q" + this._questionCount, itemAt.ID.Value, nullable8);
                                    this._textMappings["Q" + this._questionCount] = alias;
                                    this._itemMappings["Q" + this._questionCount] = itemAt.ID.Value;
                                }
                                this.AddColumnsForSelectItemOptions((SelectItemData) itemAt, alias + "_");
                            }
                        }
                    }
                }
            }
        }

        protected override void AddColumnsForSelectItemOptions(SelectItemData itemData, string prefix)
        {
            ReadOnlyCollection<ListOptionData> options = itemData.Options;
            for (int i = 1; i <= options.Count; i++)
            {
                if (itemData is SelectManyData)
                {
                    string columnName = string.Concat(new object[] { "Q", this._questionCount, "_", i });
                    this.AddColumn(this.ColumnCount, columnName, itemData.ID.Value, new int?(options[i - 1].OptionID));
                    this._textMappings[columnName] = this.GetItemText(itemData.ID.Value) + " -- " + this.GetOptionText(itemData.ID.Value, options[i - 1].OptionID);
                }
                this.SetOptionText(itemData.ID.Value, options[i - 1].OptionID, i.ToString());
            }
        }

        protected override void AddColumnsForUploadItem(ItemData itemData, string prefix, string textOverride)
        {
            if (this.IncludeOpenEnded)
            {
                base.AddColumnsForUploadItem(itemData, prefix, textOverride);
            }
        }

        public override void Configure(ItemData itemData, string languageCode)
        {
            base.Configure(itemData, languageCode);
            this._includeOpenEnded = ((SPSSExportItemData) itemData).IncludeOpenEnded;
            this._textMappings = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);
            this._itemMappings = new Dictionary<string, int>(StringComparer.InvariantCultureIgnoreCase);
            base.UseAliases = false;
        }

        protected override string DetermineColumnTextForItem(ItemData itemData, string prefix)
        {
            this._questionCount++;
            string str = "Q" + this._questionCount;
            this._textMappings[str] = base.DetermineColumnTextForItem(itemData, prefix);
            this._itemMappings[str] = itemData.ID.Value;
            return str;
        }

        public string GetActualText(string columnHeader)
        {
            if (this._textMappings.ContainsKey(columnHeader))
            {
                return this._textMappings[columnHeader];
            }
            return string.Empty;
        }

        protected override string GetAnswerText(ItemAnswer answerDataObject)
        {
            if (!answerDataObject.OptionId.HasValue)
            {
                return base.GetAnswerText(answerDataObject);
            }
            return this.GetOptionText(answerDataObject.ItemId, answerDataObject.OptionId.Value);
        }

        public int GetItemId(string columnHeader)
        {
            if (this._itemMappings.ContainsKey(columnHeader))
            {
                return this._itemMappings[columnHeader];
            }
            return -1;
        }

        public override string GetOptionText(int itemID, int optionID)
        {
            if (this.OptionTextOverrides.ContainsKey(itemID) && this.OptionTextOverrides[itemID].ContainsKey(optionID))
            {
                return this.OptionTextOverrides[itemID][optionID];
            }
            return base.GetOptionText(itemID, optionID);
        }

        protected virtual void SetOptionText(int itemId, int optionId, string optionText)
        {
            if (!this.OptionTextOverrides.ContainsKey(itemId))
            {
                this.OptionTextOverrides[itemId] = new Dictionary<int, string>();
            }
            this.OptionTextOverrides[itemId][optionId] = optionText;
        }

        public override bool IncludeHidden
        {
            get
            {
                return this._includeOpenEnded;
            }
        }

        protected override bool IncludeOpenEnded
        {
            get
            {
                return this._includeOpenEnded;
            }
        }

        protected override bool MergeSelectMany
        {
            get
            {
                return false;
            }
        }

        protected Dictionary<int, Dictionary<int, string>> OptionTextOverrides
        {
            get
            {
                if (this._optionTextOverrides == null)
                {
                    this._optionTextOverrides = new Dictionary<int, Dictionary<int, string>>();
                }
                return this._optionTextOverrides;
            }
        }
    }
}

