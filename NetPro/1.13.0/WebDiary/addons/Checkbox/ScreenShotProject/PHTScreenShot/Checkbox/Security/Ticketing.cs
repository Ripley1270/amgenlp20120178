﻿namespace Checkbox.Security
{
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Data;

    public static class Ticketing
    {
        public static void CreateTicket(Guid ticketID, DateTime expiration)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Ticket_Insert");
            storedProcCommandWrapper.AddInParameter("TicketGUID", DbType.Guid, ticketID);
            storedProcCommandWrapper.AddInParameter("Expiration", DbType.DateTime, expiration);
            database.ExecuteNonQuery(storedProcCommandWrapper);
        }

        public static bool ValidateTicket(Guid ticketID)
        {
            bool flag = false;
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Ticket_Get");
            storedProcCommandWrapper.AddInParameter("TicketGUID", DbType.Guid, ticketID);
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    try
                    {
                        if ((reader.Read() && (reader["Expiration"] != DBNull.Value)) && (DateTime.Now < ((DateTime) reader["Expiration"])))
                        {
                            flag = true;
                        }
                    }
                    catch (Exception exception)
                    {
                        ExceptionPolicy.HandleException(exception, "BusinessProtected");
                    }
                    return flag;
                }
                finally
                {
                    reader.Close();
                }
            }
            return flag;
        }
    }
}

