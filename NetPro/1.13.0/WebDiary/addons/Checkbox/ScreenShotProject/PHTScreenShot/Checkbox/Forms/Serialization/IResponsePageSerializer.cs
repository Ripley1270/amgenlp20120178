﻿namespace Checkbox.Forms.Serialization
{
    using Checkbox.Forms;
    using System;

    public interface IResponsePageSerializer
    {
        string SerializePage(ResponsePage page, string format);
    }
}

