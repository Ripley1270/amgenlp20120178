﻿namespace Checkbox.Analytics.Computation
{
    using Prezza.Framework.Common;
    using System;
    using System.Collections.Generic;
    using System.Data;

    public class TabularReportDataCalculator : ReportDataCalculator
    {
        private readonly Dictionary<int, Coordinate> _resultsPositions = new Dictionary<int, Coordinate>();

        protected override DataTable Calculate(ItemAnswerAggregator answerAggregator)
        {
            DataTable table = this.CreateOutputTable();
            foreach (int num in this._resultsPositions.Keys)
            {
                DataRow row = table.NewRow();
                int itemID = answerAggregator.GetItemID(num);
                ItemResult optionResult = this.GetOptionResult(num, answerAggregator);
                row["ItemID"] = itemID;
                row["ItemText"] = answerAggregator.GetItemText(itemID);
                row["OptionID"] = num;
                row["OptionText"] = answerAggregator.GetOptionText(num);
                row["AnswerCount"] = optionResult.Count;
                row["AnswerPercent"] = optionResult.Percentage;
                table.Rows.Add(row);
            }
            return table;
        }

        protected override DataTable CreateOutputTable()
        {
            DataTable table = base.CreateOutputTable();
            table.Columns.Add("Row", typeof(int));
            table.Columns.Add("Column", typeof(int));
            return table;
        }

        protected int GetColumnCount()
        {
            List<int> list = new List<int>();
            foreach (Coordinate coordinate in this._resultsPositions.Values)
            {
                if (!list.Contains(coordinate.X))
                {
                    list.Add(coordinate.X);
                }
            }
            return list.Count;
        }

        protected int? GetOptionIDAtPosition(Coordinate position)
        {
            foreach (int num in this._resultsPositions.Keys)
            {
                if (this._resultsPositions[num].Equals(position))
                {
                    return new int?(num);
                }
            }
            return null;
        }

        public Coordinate GetOptionResultPosition(int optionID)
        {
            if (this._resultsPositions.ContainsKey(optionID))
            {
                return this._resultsPositions[optionID];
            }
            return null;
        }

        protected int GetRowCount()
        {
            List<int> list = new List<int>();
            foreach (Coordinate coordinate in this._resultsPositions.Values)
            {
                if (!list.Contains(coordinate.Y))
                {
                    list.Add(coordinate.Y);
                }
            }
            return list.Count;
        }

        public void SetOptionResultsPosition(int optionID, Coordinate position)
        {
            this._resultsPositions[optionID] = position;
        }
    }
}

