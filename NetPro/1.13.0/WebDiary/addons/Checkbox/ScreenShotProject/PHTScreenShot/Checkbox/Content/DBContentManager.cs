﻿namespace Checkbox.Content
{
    using Prezza.Framework.Data;
    using Prezza.Framework.Security.Principal;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Text;

    public static class DBContentManager
    {
        public static DBContentFolder GetFolder(int contentFolderID, ExtendedPrincipal currentPrincipal)
        {
            DBContentFolder folder = new DBContentFolder(new int?(contentFolderID), null, currentPrincipal);
            folder.Load();
            return folder;
        }

        public static DBContentFolder GetFolder(string folderPath, ExtendedPrincipal currentPrincipal)
        {
            DBContentFolder folder = new DBContentFolder(null, folderPath, currentPrincipal);
            folder.Load();
            return folder;
        }

        public static DBContentItem GetItem(int itemID)
        {
            DBContentItem item = new DBContentItem(new int?(itemID));
            item.Load();
            return item;
        }

        public static Dictionary<string, ContentFolder> ListFolders(string folderPath, ExtendedPrincipal currentUser)
        {
            Dictionary<string, ContentFolder> dictionary = new Dictionary<string, ContentFolder>();
            DBContentFolder folder = GetFolder(folderPath, currentUser);
            if (folder.FolderID.HasValue)
            {
                Database database = DatabaseFactory.CreateDatabase();
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Content_ListFolders");
                storedProcCommandWrapper.AddInParameter("ParentFolderID", DbType.Int32, folder.FolderID);
                storedProcCommandWrapper.AddInParameter("CreatedBy", DbType.String, currentUser.Identity.Name);
                storedProcCommandWrapper.AddInParameter("ForceListAll", DbType.Boolean, currentUser.IsInRole("System Administrator"));
                using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
                {
                    try
                    {
                        while (reader.Read())
                        {
                            int num = (int) reader["FolderID"];
                            DBContentFolder folder2 = new DBContentFolder(new int?(num), string.Empty, currentUser);
                            folder2.Load();
                            dictionary[folder2.FolderName] = folder2;
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
            }
            return dictionary;
        }

        public static Dictionary<string, ContentItem> ListItems(string folderPath, ExtendedPrincipal currentUser, params string[] contentFilters)
        {
            Dictionary<string, ContentItem> dictionary = new Dictionary<string, ContentItem>();
            DBContentFolder folder = GetFolder(folderPath, currentUser);
            if (folder.FolderID.HasValue)
            {
                StringBuilder builder = new StringBuilder();
                foreach (string str in contentFilters)
                {
                    builder.Append("'");
                    builder.Append(str.Replace("'", "''"));
                    builder.Append("'");
                }
                Database database = DatabaseFactory.CreateDatabase();
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Content_ListFolderContent");
                storedProcCommandWrapper.AddInParameter("ParentFolderID", DbType.Int32, folder.FolderID);
                storedProcCommandWrapper.AddInParameter("CreatedBy", DbType.String, currentUser.Identity.Name);
                storedProcCommandWrapper.AddInParameter("MimeContentTypes", DbType.String, builder.ToString());
                storedProcCommandWrapper.AddInParameter("ForceListAll", DbType.Boolean, currentUser.IsInRole("System Administrator"));
                using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
                {
                    try
                    {
                        while (reader.Read())
                        {
                            int num = (int) reader["ItemID"];
                            DBContentItem item = new DBContentItem(new int?(num));
                            item.Load();
                            dictionary[item.ItemName] = item;
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
            }
            return dictionary;
        }
    }
}

