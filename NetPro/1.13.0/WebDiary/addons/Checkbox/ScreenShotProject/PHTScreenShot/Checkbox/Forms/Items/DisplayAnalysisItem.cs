﻿namespace Checkbox.Forms.Items
{
    using Checkbox.Management;
    using Checkbox.Security;
    using System;

    [Serializable]
    public class DisplayAnalysisItem : RedirectItem
    {
        public override bool RestartSurvey
        {
            get
            {
                return false;
            }
        }

        public override string URL
        {
            get
            {
                try
                {
                    if (base.Response != null)
                    {
                        Ticketing.CreateTicket(base.Response.GUID.Value, DateTime.Now.AddMinutes((double) ApplicationManager.AppSettings.ViewReportTicketDuration));
                        return (base.URL + "&tg=" + base.Response.GUID);
                    }
                }
                catch
                {
                }
                return base.URL;
            }
        }
    }
}

