﻿namespace Checkbox.Forms.Validation
{
    using Checkbox.Globalization.Text;
    using System;
    using System.Globalization;

    public class ROTWDateValidator : Validator<string>
    {
        public override string GetMessage(string languageCode)
        {
            return TextManager.GetText("/validationMessages/regex/dateROTW", languageCode);
        }

        public override bool Validate(string input)
        {
            DateTime result = new DateTime();
            return DateTime.TryParse(input, CultureInfo.GetCultureInfo("en-GB").DateTimeFormat, DateTimeStyles.None, out result);
        }
    }
}

