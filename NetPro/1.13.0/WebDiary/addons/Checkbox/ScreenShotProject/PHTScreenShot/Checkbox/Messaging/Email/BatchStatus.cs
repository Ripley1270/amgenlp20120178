﻿namespace Checkbox.Messaging.Email
{
    using System;

    public enum BatchStatus
    {
        Unknown,
        Pending,
        Closed,
        Sending,
        Completed
    }
}

