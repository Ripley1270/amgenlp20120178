﻿namespace Checkbox.Analytics
{
    using Checkbox.Forms;
    using Checkbox.Forms.Items;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class AnalysisPage : Page
    {
        private readonly int? _layoutTemplateID;

        internal AnalysisPage(int pageID, int position, int? layoutTemplateID) : base(pageID, position)
        {
            this._layoutTemplateID = layoutTemplateID;
        }

        public override List<Item> Items
        {
            get
            {
                List<Item> list = new List<Item>();
                foreach (int num in base.ItemIDs)
                {
                    Item item = this.Parent.GetItem(num);
                    if (item != null)
                    {
                        list.Add(item);
                    }
                }
                return list;
            }
        }

        public int? LayoutTemplateID
        {
            get
            {
                return this._layoutTemplateID;
            }
        }

        internal Analysis Parent { get; set; }
    }
}

