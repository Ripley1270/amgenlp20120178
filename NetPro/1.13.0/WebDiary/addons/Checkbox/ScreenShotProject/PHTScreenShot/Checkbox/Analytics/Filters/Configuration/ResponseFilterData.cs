﻿namespace Checkbox.Analytics.Filters.Configuration
{
    using Checkbox.Analytics.Filters;
    using Prezza.Framework.Data;
    using System;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class ResponseFilterData : FilterData
    {
        protected override void Create(IDbTransaction t)
        {
            base.Create(t);
            Database db = DatabaseFactory.CreateDatabase();
            db.ExecuteNonQuery(this.GetCreateCommand(db), t);
        }

        protected override Checkbox.Analytics.Filters.Filter CreateFilterObject()
        {
            return new ResponseFilter();
        }

        public override void Delete(IDbTransaction t)
        {
            Database db = DatabaseFactory.CreateDatabase();
            db.ExecuteNonQuery(this.GetDeleteCommand(db), t);
            base.Delete(t);
        }

        public override DataSet GetConfigurationDataSet()
        {
            if (!base.ID.HasValue || (base.ID <= 0))
            {
                throw new Exception("Unable to load filter data with no id.");
            }
            Database db = DatabaseFactory.CreateDatabase();
            DataSet dataSet = new DataSet();
            db.LoadDataSet(this.GetLoadConfigurationCommand(db), dataSet, this.DataTableName);
            return dataSet;
        }

        protected virtual DBCommandWrapper GetCreateCommand(Database db)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_Filter_InsertResponse");
            storedProcCommandWrapper.AddInParameter("FilterID", DbType.Int32, base.ID.Value);
            storedProcCommandWrapper.AddInParameter("ResponseProperty", DbType.String, this.PropertyName);
            return storedProcCommandWrapper;
        }

        protected virtual DBCommandWrapper GetDeleteCommand(Database db)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_Filter_DeleteResponse");
            storedProcCommandWrapper.AddInParameter("FilterID", DbType.Int32, base.ID.Value);
            return storedProcCommandWrapper;
        }

        protected override string GetFilterLeftOperandText(string languageCode)
        {
            return this.PropertyName;
        }

        protected virtual DBCommandWrapper GetLoadConfigurationCommand(Database db)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_Filter_GetResponse");
            storedProcCommandWrapper.AddInParameter("FilterID", DbType.Int32, base.ID);
            return storedProcCommandWrapper;
        }

        protected override void Load(DataRow data)
        {
            base.Load(data);
            this.PropertyName = DbUtility.GetValueFromDataRow<string>(data, this.PropertyFieldName, null);
        }

        public override string DataTableName
        {
            get
            {
                return "ResponseFilterData";
            }
        }

        public override string IdentityColumnName
        {
            get
            {
                return "FilterID";
            }
        }

        protected virtual string PropertyFieldName
        {
            get
            {
                return "ResponseProperty";
            }
        }

        public string PropertyName { get; set; }
    }
}

