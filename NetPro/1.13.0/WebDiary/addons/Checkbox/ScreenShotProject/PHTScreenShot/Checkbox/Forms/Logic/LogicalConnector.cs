﻿namespace Checkbox.Forms.Logic
{
    using System;

    [Serializable]
    public enum LogicalConnector
    {
        OR,
        AND
    }
}

