﻿namespace Checkbox.Forms.Items.Configuration
{
    using Checkbox.Forms.Items;
    using Checkbox.Globalization.Text;
    using System;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    public abstract class TextItemData : LabelledItemData
    {
        protected TextItemData()
        {
        }

        protected override ItemData Copy()
        {
            TextItemData data = (TextItemData) base.Copy();
            if (data != null)
            {
                data.Format = this.Format;
                data.MaxLength = this.MaxLength;
                data.CustomFormatId = this.CustomFormatId;
            }
            return data;
        }

        public override DataSet LoadTextData()
        {
            DataSet ds = base.LoadTextData();
            this.MergeTextData(ds, TextManager.GetTextData(this.DefaultTextID), this.TextTableName, "defaultText", this.TextIDPrefix, base.ID.Value);
            return ds;
        }

        public virtual string CustomFormatId { get; set; }

        public virtual string DefaultTextID
        {
            get
            {
                return this.GetTextID("defaultText");
            }
        }

        public virtual AnswerFormat Format { get; set; }

        public override bool ItemIsIAnswerable
        {
            get
            {
                return true;
            }
        }

        public virtual int? MaxLength { get; set; }

        public override string TextIDPrefix
        {
            get
            {
                return "textItemData";
            }
        }
    }
}

