﻿namespace Checkbox.Globalization.Configuration
{
    using System;

    [Serializable]
    public class ISOCode
    {
        private string code;
        private string name;

        public ISOCode()
        {
            this.code = string.Empty;
            this.name = string.Empty;
        }

        public ISOCode(string name, string code)
        {
            this.name = name;
            this.code = code;
        }

        public string Code
        {
            get
            {
                return this.code;
            }
            set
            {
                this.code = value;
            }
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }
    }
}

