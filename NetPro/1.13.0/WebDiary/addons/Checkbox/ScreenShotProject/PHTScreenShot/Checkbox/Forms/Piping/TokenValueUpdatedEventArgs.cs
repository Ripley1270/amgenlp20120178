﻿namespace Checkbox.Forms.Piping
{
    using System;
    using System.Runtime.CompilerServices;

    public class TokenValueUpdatedEventArgs : EventArgs
    {
        internal TokenValueUpdatedEventArgs(string tokenName, string newValue)
        {
            this.TokenName = tokenName;
            this.NewValue = newValue;
        }

        internal string NewValue { get; private set; }

        internal string TokenName { get; private set; }
    }
}

