﻿namespace Checkbox.Forms.Validation
{
    using Checkbox.Forms.Items;
    using Checkbox.Forms.Logic;
    using Checkbox.Globalization.Text;
    using System;
    using System.Collections.Generic;

    public class ItemSumValidator : Validator<List<IAnswerable>>
    {
        private LogicalOperator _operator;
        private double _sumValue;

        public ItemSumValidator(double sumValue, LogicalOperator comparisonOperator)
        {
            this._sumValue = sumValue;
            this._operator = comparisonOperator;
        }

        public override string GetMessage(string languageCode)
        {
            string text = TextManager.GetText("/validationMessages/itemSumTotal/sumError", languageCode);
            if (text == null)
            {
                return text;
            }
            string newValue = TextManager.GetText("/validationMessages/itemSumTotal/" + this._operator.ToString(), languageCode);
            if (newValue != null)
            {
                text = text.Replace("{operator}", newValue);
            }
            return text.Replace("{value}", this._sumValue.ToString());
        }

        public override bool Validate(List<IAnswerable> itemsToValiate)
        {
            bool flag = false;
            foreach (IAnswerable answerable in itemsToValiate)
            {
                if (answerable.HasAnswer)
                {
                    flag = true;
                    break;
                }
            }
            if (flag)
            {
                SumOperand left = new SumOperand(itemsToValiate);
                StringOperand right = new StringOperand(this.SumValue.ToString());
                return OperandComparer.Compare(left, this.Operator, right, RuleEventTrigger.Load);
            }
            return true;
        }

        public LogicalOperator Operator
        {
            get
            {
                return this._operator;
            }
        }

        public double SumValue
        {
            get
            {
                return this._sumValue;
            }
        }
    }
}

