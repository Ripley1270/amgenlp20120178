﻿namespace Checkbox.Management
{
    using Checkbox.Common;
    using Checkbox.Forms;
    using Checkbox.Management.Licensing.Limits;
    using Checkbox.Management.Licensing.Limits.Static;
    using Prezza.Framework.Data;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;

    public class AppSettings
    {
        //public T GetValue<T>(string settingName, T defaultValue)
        //{
        //    object appSetting = ApplicationManager.GetAppSetting(settingName);
        //    if (appSetting == null)
        //    {
        //        return defaultValue;
        //    }
        //    try
        //    {
        //        if (typeof(T).IsEnum)
        //        {
        //            return (T) Enum.Parse(typeof(T), (string) appSetting);
        //        }
        //        if (typeof(T) == typeof(int))
        //        {
        //            return (T) Convert.ToInt32(appSetting);
        //        }
        //        if (typeof(T) == typeof(bool))
        //        {
        //            return (T) Convert.ToBoolean(appSetting);
        //        }
        //        if (typeof(T) == typeof(string))
        //        {
        //            return (T) Convert.ToString(appSetting);
        //        }
        //        if (typeof(T) == typeof(double))
        //        {
        //            return (T) Convert.ToDouble(appSetting);
        //        }
        //        return (T) appSetting;
        //    }
        //    catch
        //    {
        //        return defaultValue;
        //    }
        //}

        public T GetValue<T>(string settingName, T defaultValue)
        {
            T str;
            object appSetting = ApplicationManager.GetAppSetting(settingName);
            if (appSetting != null)
            {
                try
                {
                    if (!typeof(T).IsEnum)
                    {
                        if (typeof(T) != typeof(int))
                        {
                            if (typeof(T) != typeof(bool))
                            {
                                if (typeof(T) != typeof(string))
                                {
                                    str = (typeof(T) != typeof(double) ? (T)appSetting : (T)(object)Convert.ToDouble(appSetting));
                                }
                                else
                                {
                                    //str = (T)Convert.ToString(appSetting);
                                    str = (T)(object)Convert.ToString(appSetting);
                                }
                            }
                            else
                            {
                                str = (T)(object)Convert.ToBoolean(appSetting);
                            }
                        }
                        else
                        {
                            str = (T)(object)Convert.ToInt32(appSetting);
                        }
                    }
                    else
                    {
                        str = (T)Enum.Parse(typeof(T), (string)appSetting);
                    }
                }
                catch
                {
                    str = defaultValue;
                }
                return str;
            }
            else
            {
                return defaultValue;
            }
        }

        public void SetValue<T>(string settingName, T settingValue)
        {
            if (ApplicationManager.GetAppSetting(settingName) == null)
            {
                if (settingValue != null)
                {
                    ApplicationManager.AddNewAppSetting(settingName, settingValue.ToString());
                }
                else
                {
                    ApplicationManager.AddNewAppSetting(settingName, string.Empty);
                }
            }
            else if (settingValue != null)
            {
                ApplicationManager.UpdateAppSetting(settingName, settingValue.ToString());
            }
            else
            {
                ApplicationManager.UpdateAppSetting(settingName, string.Empty);
            }
        }

        public string ActiveDirectoryNamingContext
        {
            get
            {
                return this.GetValue<string>("ActiveDirectoryNamingContext", string.Empty);
            }
            set
            {
                this.SetValue<string>("ActiveDirectoryNamingContext", value);
            }
        }

        public bool AllowDeviceAutoRegister
        {
            get
            {
                return this.GetValue<bool>("AllowDeviceAutoRegister", false);
            }
            set
            {
                this.SetValue<bool>("AllowDeviceAutoRegister", value);
            }
        }

        public bool AllowEditActiveSurvey
        {
            get
            {
                return this.GetValue<bool>("AllowEditActiveSurvey", true);
            }
            set
            {
                this.SetValue<bool>("AllowEditActiveSurvey", value);
            }
        }

        public bool AllowEditSelf
        {
            get
            {
                return this.GetValue<bool>("AllowEditSelf", true);
            }
            set
            {
                this.SetValue<bool>("AllowEditSelf", value);
            }
        }

        public bool AllowEditSurveyStyleTemplate
        {
            get
            {
                return this.GetValue<bool>("AllowEditSurveyStyleTemplate", true);
            }
            set
            {
                this.SetValue<bool>("AllowEditSurveyStyleTemplate", value);
            }
        }

        public List<string> AllowedUrlRewriteExtensions
        {
            get
            {
                List<string> list = new List<string>();
                if ((ConfigurationManager.AppSettings["AllowedUrlRewriteExtensions"] != null) && (ConfigurationManager.AppSettings["AllowedUrlRewriteExtensions"].Trim() != string.Empty))
                {
                    foreach (string str in ConfigurationManager.AppSettings["AllowedUrlRewriteExtensions"].Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        list.Add(str.Trim());
                    }
                }
                return list;
            }
        }

        public bool AllowExclusionaryAclEntries
        {
            get
            {
                return this.GetValue<bool>("AllowExclusionaryAclEntries", true);
            }
            set
            {
                this.SetValue<bool>("AllowExclusionaryAclEntries", value);
            }
        }

        public bool AllowHTMLNames
        {
            get
            {
                return this.GetValue<bool>("AllowHTMLNames", false);
            }
            set
            {
                this.SetValue<bool>("AllowHTMLNames", value);
            }
        }

        public bool AllowHttps
        {
            get
            {
                return this.GetValue<bool>("AllowHttps", true);
            }
            set
            {
                this.SetValue<bool>("AllowHttps", value);
            }
        }

        public bool AllowInteractiveLogin
        {
            get
            {
                return (this.GetValue<bool>("AllowInteractiveLogin", false) && this.AllowNetworkUsers);
            }
            set
            {
                this.SetValue<bool>("AllowInteractiveLogin", value);
            }
        }

        public bool AllowInvitations
        {
            get
            {
                string str;
                InvitationLimit limit = new InvitationLimit();
                return (limit.Validate(out str) == LimitValidationResult.LimitNotReached);
            }
        }

        public bool AllowMultiLanguage
        {
            get
            {
                string str;
                MultiLanguageLimit limit = new MultiLanguageLimit();
                return (limit.Validate(out str) == LimitValidationResult.LimitNotReached);
            }
        }

        public bool AllowNativeSpssExport
        {
            get
            {
                string str;
                SpssLimit limit = new SpssLimit();
                return (limit.Validate(out str) == LimitValidationResult.LimitNotReached);
            }
        }

        public bool AllowNetworkUsers
        {
            get
            {
                string str;
                NetworkUserLimit limit = new NetworkUserLimit();
                return (limit.Validate(out str) == LimitValidationResult.LimitNotReached);
            }
        }

        public bool AllowPasswordReset
        {
            get
            {
                return this.GetValue<bool>("AllowPasswordReset", true);
            }
            set
            {
                this.SetValue<bool>("AllowPasswordReset", value);
            }
        }

        public bool AllowPublicRegistration
        {
            get
            {
                return this.GetValue<bool>("AllowPublicRegistration", false);
            }
            set
            {
                this.SetValue<bool>("AllowPublicRegistration", value);
            }
        }

        public bool AllowResponseTemplateIDLookup
        {
            get
            {
                return this.GetValue<bool>("AllowResponseTemplateIDLookup", true);
            }
            set
            {
                this.SetValue<bool>("AllowResponseTemplateIDLookup", value);
            }
        }

        public bool AllowSurveyUrlRewriting
        {
            get
            {
                return this.GetValue<bool>("AllowSurveyUrlRewriting", false);
            }
            set
            {
                this.SetValue<bool>("AllowSurveyUrlRewriting", value);
            }
        }

        public bool AllowUnAuthenticatedNetworkUsers
        {
            get
            {
                return this.GetValue<bool>("AllowUnAuthenticatedNetworkUsers", true);
            }
            set
            {
                this.SetValue<bool>("AllowUnAuthenticatedNetworkUsers", value);
            }
        }

        public bool AllowUploadItem
        {
            get
            {
                return this.GetValue<bool>("AllowUploadItem", true);
            }
            set
            {
                this.SetValue<bool>("AllowUploadItem", value);
            }
        }

        public bool AnonymizeResponses
        {
            get
            {
                return this.GetValue<bool>("AnonymizeResponses", false);
            }
            set
            {
                this.SetValue<bool>("AnonymizeResponses", value);
            }
        }

        public string AutogenReportDefaultCheckboxes
        {
            get
            {
                return this.GetValue<string>("AutogenReportDefaultCheckboxes", "ColumnGraph");
            }
            set
            {
                this.SetValue<string>("AutogenReportDefaultCheckboxes", value);
            }
        }

        public string AutogenReportDefaultDropDownList
        {
            get
            {
                return this.GetValue<string>("AutogenReportDefaultDropDownList", "PieGraph");
            }
            set
            {
                this.SetValue<string>("AutogenReportDefaultDropDownList", value);
            }
        }

        public string AutogenReportDefaultHiddenItems
        {
            get
            {
                return this.GetValue<string>("AutogenReportDefaultHiddenItems", "SummaryTable");
            }
            set
            {
                this.SetValue<string>("AutogenReportDefaultHiddenItems", value);
            }
        }

        public string AutogenReportDefaultItemPosition
        {
            get
            {
                return this.GetValue<string>("AutogenReportDefaultItemPosition", "Left");
            }
            set
            {
                this.SetValue<string>("AutogenReportDefaultItemPosition", value);
            }
        }

        public string AutogenReportDefaultMatrix
        {
            get
            {
                return this.GetValue<string>("AutogenReportDefaultMatrix", "MatrixSummary");
            }
            set
            {
                this.SetValue<string>("AutogenReportDefaultMatrix", value);
            }
        }

        public int AutogenReportDefaultMaxOptions
        {
            get
            {
                return this.GetValue<int>("AutogenReportDefaultMaxOptions", 10);
            }
            set
            {
                this.SetValue<int>("AutogenReportDefaultMaxOptions", value);
            }
        }

        public string AutogenReportDefaultMultiLineText
        {
            get
            {
                return this.GetValue<string>("AutogenReportDefaultMultiLineText", "Details");
            }
            set
            {
                this.SetValue<string>("AutogenReportDefaultMultiLineText", value);
            }
        }

        public bool AutogenReportDefaultMultiplePages
        {
            get
            {
                return this.GetValue<bool>("AutogenReportDefaultMultiplePages", false);
            }
            set
            {
                this.SetValue<bool>("AutogenReportDefaultMultiplePages", value);
            }
        }

        public string AutogenReportDefaultPieLabelStyle
        {
            get
            {
                return this.GetValue<string>("AutogenReportDefaultPieLabelStyle", "Inside");
            }
            set
            {
                this.SetValue<string>("AutogenReportDefaultPieLabelStyle", value);
            }
        }

        public string AutogenReportDefaultRadioButtons
        {
            get
            {
                return this.GetValue<string>("AutogenReportDefaultRadioButtons", "PieGraph");
            }
            set
            {
                this.SetValue<string>("AutogenReportDefaultRadioButtons", value);
            }
        }

        public string AutogenReportDefaultRadioButtonScale
        {
            get
            {
                return this.GetValue<string>("AutogenReportDefaultRadioButtonScale", "PieGraph");
            }
            set
            {
                this.SetValue<string>("AutogenReportDefaultRadioButtonScale", value);
            }
        }

        public string AutogenReportDefaultSingleLineText
        {
            get
            {
                return this.GetValue<string>("AutogenReportDefaultSingleLineText", "Details");
            }
            set
            {
                this.SetValue<string>("AutogenReportDefaultSingleLineText", value);
            }
        }

        public bool AutogenReportDefaultUseAliases
        {
            get
            {
                return this.GetValue<bool>("AutogenReportDefaultUseAliases", false);
            }
            set
            {
                this.SetValue<bool>("AutogenReportDefaultUseAliases", value);
            }
        }

        public bool BufferResponseExport
        {
            get
            {
                return this.GetValue<bool>("BufferResponseExport", true);
            }
            set
            {
                this.SetValue<bool>("BufferResponseExport", value);
            }
        }

        public bool CacheAppSettings
        {
            get
            {
                return (string.Compare(ConfigurationManager.AppSettings["CacheAppSettings"], "true", true) == 0);
            }
        }

        public bool CacheResponseTemplates
        {
            get
            {
                return this.GetValue<bool>("EnableResponseTemplateCaching", false);
            }
            set
            {
                this.SetValue<bool>("EnableResponseTemplateCaching", value);
            }
        }

        public bool CacheRolePermissions
        {
            get
            {
                return (string.Compare(ConfigurationManager.AppSettings["CacheRolePermissions"], "true", true) == 0);
            }
        }

        public bool CacheVolatileDataInApplication
        {
            get
            {
                return (string.Compare(ConfigurationManager.AppSettings["CacheVolatileDataInApplication"], "true", true) == 0);
            }
        }

        public string CenterMenuTextColor
        {
            get
            {
                return this.GetValue<string>("CenterMenuTextColor", string.Empty);
            }
            set
            {
                this.SetValue<string>("CenterMenuTextColor", value);
            }
        }

        public Checkbox.Management.ConcurrentLoginMode ConcurrentLoginMode
        {
            get
            {
                return Checkbox.Management.ConcurrentLoginMode.LogoutCurrent;
            }
            set
            {
                this.SetValue<Checkbox.Management.ConcurrentLoginMode>("ConcurrentLoginMode", value);
            }
        }

        public string CookieName
        {
            get
            {
                return this.GetValue<string>("CookieName", "CheckboxWebSurvey");
            }
            set
            {
                this.SetValue<string>("CookieName", value);
            }
        }

        public ArrayList CustomFields
        {
            get
            {
                ArrayList list = new ArrayList();
                Database database = DatabaseFactory.CreateDatabase();
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Profile_GetFields");
                using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
                {
                    try
                    {
                        while (reader.Read())
                        {
                            list.Add(reader[0]);
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
                return list;
            }
        }

        public bool DefaultAlias
        {
            get
            {
                return this.GetValue<bool>("DefaultAlias", false);
            }
            set
            {
                this.SetValue<bool>("DefaultAlias", value);
            }
        }

        public bool DefaultAllowEditResponse
        {
            get
            {
                return this.GetValue<bool>("DefaultAllowEditResponse", false);
            }
            set
            {
                this.SetValue<bool>("DefaultAllowEditResponse", value);
            }
        }

        public bool DefaultAllowResumeResponse
        {
            get
            {
                return this.GetValue<bool>("DefaultAllowResumeResponse", false);
            }
            set
            {
                this.SetValue<bool>("DefaultAllowResumeResponse", value);
            }
        }

        public string DefaultConnectionString
        {
            get
            {
                if (ConfigurationManager.ConnectionStrings["DefaultConnectionString"] != null)
                {
                    return ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
                }
                return string.Empty;
            }
        }

        public EditSurveyView DefaultEditView
        {
            get
            {
                return this.GetValue<EditSurveyView>("DefaultEditView", EditSurveyView.IndividualPages);
            }
            set
            {
                this.SetValue<EditSurveyView>("DefaultEditView", value);
            }
        }

        public string DefaultEmailFromName
        {
            get
            {
                return this.GetValue<string>("DefaultEmailFromName", string.Empty);
            }
            set
            {
                this.SetValue<string>("DefaultEmailFromName", value);
            }
        }

        public string DefaultEmailInvitationBody
        {
            get
            {
                return this.GetValue<string>("DefaultEmailInvitationBody", string.Empty);
            }
            set
            {
                this.SetValue<string>("DefaultEmailInvitationBody", value);
            }
        }

        public string DefaultEmailInvitationSubject
        {
            get
            {
                return this.GetValue<string>("DefaultEmailInvitationSubject", string.Empty);
            }
            set
            {
                this.SetValue<string>("DefaultEmailInvitationSubject", value);
            }
        }

        public string DefaultExportType
        {
            get
            {
                return this.GetValue<string>("DefaultExportType", "Standard");
            }
            set
            {
                this.SetValue<string>("DefaultExportType", value);
            }
        }

        public string DefaultFromEmailAddress
        {
            get
            {
                return this.GetValue<string>("DefaultFromEmailAddress", string.Empty);
            }
            set
            {
                this.SetValue<string>("DefaultFromEmailAddress", value);
            }
        }

        public string DefaultLoginPassword
        {
            get
            {
                return this.GetValue<string>("DefaultLoginPassword", string.Empty);
            }
        }

        public string DefaultLoginUsername
        {
            get
            {
                return this.GetValue<string>("DefaultLoginUsername", string.Empty);
            }
        }

        public OrderedDictionary DefaultMappingFields
        {
            get
            {
                OrderedDictionary dictionary = new OrderedDictionary();
                Database database = DatabaseFactory.CreateDatabase();
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("rsp_GetBulkRegistrationMappingList");
                using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
                {
                    try
                    {
                        while (reader.Read())
                        {
                            dictionary.Add(reader[0], reader[1]);
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
                return dictionary;
            }
        }

        public OptionEntryType DefaultOptionEntryType
        {
            get
            {
                return this.GetValue<OptionEntryType>("DefaultOptionEntryType", OptionEntryType.Normal);
            }
            set
            {
                this.SetValue<OptionEntryType>("DefaultOptionEntryType", value);
            }
        }

        public string DefaultQuestionEditorView
        {
            get
            {
                return this.GetValue<string>("DefaultQuestionEditorView", "HTML");
            }
            set
            {
                this.SetValue<string>("DefaultQuestionEditorView", value);
            }
        }

        public ReportSecurityType DefaultReportSecurityType
        {
            get
            {
                return this.GetValue<ReportSecurityType>("DefaultReportSecurityType", ReportSecurityType.SummaryPrivate);
            }
            set
            {
                this.SetValue<ReportSecurityType>("DefaultReportSecurityType", value);
            }
        }

        public string[] DefaultRolesForUnAuthenticatedNetworkUsers
        {
            get
            {
                if (ApplicationManager.GetAppSetting("DefaultRolesForUnAuthenticatedNetworkUsers") == null)
                {
                    ApplicationManager.AddNewAppSetting("DefaultRolesForUnAuthenticatedNetworkUsers", "Respondent,Report Viewer");
                }
                string appSetting = ApplicationManager.GetAppSetting("DefaultRolesForUnAuthenticatedNetworkUsers");
                if (appSetting == string.Empty)
                {
                    ArrayList list = new ArrayList();
                    return (string[]) list.ToArray(typeof(string));
                }
                return appSetting.Split(new char[] { ',' });
            }
            set
            {
                if (value == null)
                {
                    ApplicationManager.UpdateAppSetting("DefaultRolesForUnAuthenticatedNetworkUsers", string.Empty);
                }
                else
                {
                    string property = string.Empty;
                    for (int i = 0; i < value.Length; i++)
                    {
                        if (i > 0)
                        {
                            property = property + ",";
                        }
                        property = property + value[i];
                    }
                    ApplicationManager.UpdateAppSetting("DefaultRolesForUnAuthenticatedNetworkUsers", property);
                }
            }
        }

        public int DefaultSingleLineTextWidth
        {
            get
            {
                return this.GetValue<int>("DefaultSingleLineTextWidth", 150);
            }
            set
            {
                this.SetValue<int>("DefaultSingleLineTextWidth", value);
            }
        }

        public int DefaultStyleTemplate
        {
            get
            {
                return this.GetValue<int>("DefaultStyleTemplate", -1);
            }
            set
            {
                this.SetValue<int>("DefaultStyleTemplate", value);
            }
        }

        public SecurityType DefaultSurveySecurityType
        {
            get
            {
                return this.GetValue<SecurityType>("DefaultSurveySecurityType", SecurityType.Public);
            }
            set
            {
                this.SetValue<SecurityType>("DefaultSurveySecurityType", value);
            }
        }

        public int DefaultTotalResponseLimit
        {
            get
            {
                return this.GetValue<int>("DefaultTotalResponseLimit", -1);
            }
            set
            {
                this.SetValue<int>("DefaultTotalResponseLimit", value);
            }
        }

        public int DefaultUserResponseLimit
        {
            get
            {
                return this.GetValue<int>("DefaultUserResponseLimit", -1);
            }
            set
            {
                this.SetValue<int>("DefaultUserResponseLimit", value);
            }
        }

        public string[] DefaultUserRoles
        {
            get
            {
                if (ApplicationManager.GetAppSetting("DefaultUserRoles") == null)
                {
                    ApplicationManager.AddNewAppSetting("DefaultUserRoles", "Respondent,Report Viewer");
                }
                string appSetting = ApplicationManager.GetAppSetting("DefaultUserRoles");
                if (appSetting == string.Empty)
                {
                    ArrayList list = new ArrayList();
                    return (string[]) list.ToArray(typeof(string));
                }
                return appSetting.Split(new char[] { ',' });
            }
            set
            {
                if (value == null)
                {
                    ApplicationManager.UpdateAppSetting("DefaultUserRoles", string.Empty);
                }
                else
                {
                    string property = string.Empty;
                    for (int i = 0; i < value.Length; i++)
                    {
                        if (i > 0)
                        {
                            property = property + ",";
                        }
                        property = property + value[i];
                    }
                    ApplicationManager.UpdateAppSetting("DefaultUserRoles", property);
                }
            }
        }

        public bool DisplayAvailableSurveyList
        {
            get
            {
                return this.GetValue<bool>("DisplayAvailableSurveyList", true);
            }
            set
            {
                this.SetValue<bool>("DisplayAvailableSurveyList", value);
            }
        }

        public bool DisplayHtmlItemsAsPlainText
        {
            get
            {
                return this.GetValue<bool>("DisplayHtmlItemsAsPlainText", false);
            }
            set
            {
                this.SetValue<bool>("DisplayHtmlItemsAsPlainText", value);
            }
        }

        public bool DisplayProductTour
        {
            get
            {
                return this.GetValue<bool>("DisplayProductTour", false);
            }
            set
            {
                this.SetValue<bool>("DisplayProductTour", value);
            }
        }

        public string EmailAddressOptionalCharacters
        {
            get
            {
                return this.GetValue<string>("EmailAddressOptionalCharacters", "!,#,$,%,&,',*,+,-,/,=,?,^,_,`,{,|,},~,.");
            }
            set
            {
                this.SetValue<string>("EmailAddressOptionalCharacters", value);
            }
        }

        public bool EmailEnabled
        {
            get
            {
                return this.GetValue<bool>("EmailEnabled", true);
            }
            set
            {
                this.SetValue<bool>("EmailEnabled", value);
            }
        }

        public bool EmailSurveyResumeUrlEnabled
        {
            get
            {
                if (this.EmailEnabled)
                {
                    return this.GetValue<bool>("EmailSurveyResumeUrlEnabled", true);
                }
                return false;
            }
            set
            {
                this.SetValue<bool>("EmailSurveyResumeUrlEnabled", value);
            }
        }

        public bool EmailTestModeEnabled
        {
            get
            {
                return this.GetValue<bool>("EmailTestModeEnabled", false);
            }
            set
            {
                this.SetValue<bool>("EmailTestModeEnabled", value);
            }
        }

        public int EmailTestModeSleepTime
        {
            get
            {
                return this.GetValue<int>("EmailTestModeSleepTime", 0x3e8);
            }
            set
            {
                this.SetValue<int>("EmailTestModeSleepTime", value);
            }
        }

        public bool EnableAjax
        {
            get
            {
                return this.GetValue<bool>("EnableAjax", false);
            }
            set
            {
                this.SetValue<bool>("EnableAjax", value);
            }
        }

        public bool EnableEmailAddressValidation
        {
            get
            {
                return this.GetValue<bool>("EnableEmailAddressValidation", true);
            }
            set
            {
                this.SetValue<bool>("EnableEmailAddressValidation", value);
            }
        }

        public bool EnableMultiDatabase
        {
            get
            {
                bool flag;
                return ((!string.IsNullOrEmpty(ConfigurationManager.AppSettings["EnableMultiDatabase"]) && bool.TryParse(ConfigurationManager.AppSettings["EnableMultiDatabase"], out flag)) && flag);
            }
        }

        public bool EnableSimpleSecurity
        {
            get
            {
                return this.GetValue<bool>("EnableSimpleSecurity", false);
            }
            set
            {
                this.SetValue<bool>("EnableSimpleSecurity", value);
            }
        }

        public bool EnableSmtpAuthentication
        {
            get
            {
                return this.GetValue<bool>("EnableSmtpAuthentication", false);
            }
            set
            {
                this.SetValue<bool>("EnableSmtpAuthentication", value);
            }
        }

        public bool EnableSmtpSsl
        {
            get
            {
                return this.GetValue<bool>("EnableSmtpSsl", false);
            }
            set
            {
                this.SetValue<bool>("EnableSmtpSsl", value);
            }
        }

        public bool EnableUploadItem
        {
            get
            {
                return this.GetValue<bool>("EnableUploadItem", true);
            }
            set
            {
                this.SetValue<bool>("EnableUploadItem", value);
            }
        }

        public int GroupCacheSize
        {
            get
            {
                return this.GetValue<int>("GroupCacheSize", 20);
            }
        }

        public string HeaderFont
        {
            get
            {
                return this.GetValue<string>("HeaderFont", string.Empty);
            }
            set
            {
                this.SetValue<string>("HeaderFont", value);
            }
        }

        public int HeaderFontSize
        {
            get
            {
                return this.GetValue<int>("HeaderFontSize", 0x12);
            }
            set
            {
                this.SetValue<int>("HeaderFontSize", value);
            }
        }

        public string HeaderLogo
        {
            get
            {
                return this.GetValue<string>("HeaderLogo", "~/Images/ckbx_Header_Logo.gif");
            }
            set
            {
                this.SetValue<string>("HeaderLogo", value);
            }
        }

        public string HeaderTextColor
        {
            get
            {
                return this.GetValue<string>("HeaderTextColor", string.Empty);
            }
            set
            {
                this.SetValue<string>("HeaderTextColor", value);
            }
        }

        public HeaderType HeaderTypeChosen
        {
            get
            {
                return this.GetValue<HeaderType>("HeaderType", HeaderType.Logo);
            }
            set
            {
                this.SetValue<HeaderType>("HeaderType", value);
            }
        }

        public string ImageUploadPreviewPath
        {
            get
            {
                return this.GetValue<string>("ImageUploadPreviewPath", string.Empty);
            }
            set
            {
                this.SetValue<string>("ImageUploadPreviewPath", value);
            }
        }

        public string ImageUploadPreviewUrl
        {
            get
            {
                return this.GetValue<string>("ImageUploadPreviewUrl", string.Empty);
            }
            set
            {
                this.SetValue<string>("ImageUploadPreviewUrl", value);
            }
        }

        public string JQueryIncludeUrl
        {
            get
            {
                return this.GetValue<string>("JQueryIncludeUrl", string.Empty);
            }
            set
            {
                this.SetValue<string>("JQueryIncludeUrl", value);
            }
        }

        public int KeepAlive
        {
            get
            {
                return this.GetValue<int>("KeepAlive", 0);
            }
            set
            {
                this.SetValue<int>("KeepAlive", value);
            }
        }

        public bool LanguageDebugMode
        {
            get
            {
                if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["LanguageDebugMode"]))
                {
                    return false;
                }
                return (string.Compare(ConfigurationManager.AppSettings["LanguageDebugMode"], "true", true) == 0);
            }
        }

        public bool LimitDebugMode
        {
            get
            {
                if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["LimitDebugMode"]))
                {
                    return false;
                }
                return (string.Compare(ConfigurationManager.AppSettings["LimitDebugMode"], "true", true) == 0);
            }
        }

        public bool LimitEmailMessageLineLength
        {
            get
            {
                return this.GetValue<bool>("InsertLineBreaksInEmails", false);
            }
            set
            {
                this.SetValue<bool>("InsertLineBreaksInEmails", value);
            }
        }

        public bool LogErrorsToUI
        {
            get
            {
                return this.GetValue<bool>("LogErrorsToUI", true);
            }
            set
            {
                this.SetValue<bool>("LogErrorsToUI", value);
            }
        }

        public bool LogIpAddresses
        {
            get
            {
                return this.GetValue<bool>("LogIpAddresses", true);
            }
            set
            {
                this.SetValue<bool>("LogIpAddresses", value);
            }
        }

        public bool LogNetworkUser
        {
            get
            {
                return this.GetValue<bool>("LogNetworkUser", true);
            }
            set
            {
                this.SetValue<bool>("LogNetworkUser", value);
            }
        }

        public bool LogServerParamsToUI
        {
            get
            {
                return this.GetValue<bool>("LogServerParamsToUI", true);
            }
            set
            {
                this.SetValue<bool>("LogServerParamsToUI", value);
            }
        }

        public bool LogStackTraceToUI
        {
            get
            {
                return this.GetValue<bool>("LogStackTraceToUI", true);
            }
            set
            {
                this.SetValue<bool>("LogStackTraceToUI", value);
            }
        }

        public int MaxEmailMessageLineLength
        {
            get
            {
                return this.GetValue<int>("MaximumEmailLineLength", 0x7d0);
            }
            set
            {
                this.SetValue<int>("MaximumEmailLineLength", value);
            }
        }

        public int MaxIdentityInClauseSize
        {
            get
            {
                return this.GetValue<int>("MaxIdentityInClauseSize", 0x3e8);
            }
            set
            {
                this.SetValue<int>("MaxIdentityInClauseSize", value);
            }
        }

        public int MaxReportPreviewOptions
        {
            get
            {
                return this.GetValue<int>("MaxReportPreviewOptions", 10);
            }
            set
            {
                this.SetValue<int>("MaxReportPreviewOptions", value);
            }
        }

        public int MessageThrottle
        {
            get
            {
                return this.GetValue<int>("MessageThrottle", 0x19);
            }
            set
            {
                this.SetValue<int>("MessageThrottle", value);
            }
        }

        public int MessageThrottleDelay
        {
            get
            {
                return this.GetValue<int>("MessageThrottleDelay", 1);
            }
            set
            {
                this.SetValue<int>("MessageThrottleDelay", value);
            }
        }

        public string NewLineReplacement
        {
            get
            {
                return this.GetValue<string>("NewLineReplacement", " ");
            }
            set
            {
                this.SetValue<string>("NewLineReplacement", value);
            }
        }

        public bool NTAuthentication
        {
            get
            {
                return this.GetValue<bool>("NTAuthentication", false);
            }
            set
            {
                this.SetValue<bool>("NTAuthentication", value);
            }
        }

        public string NTAuthenticationVariableName
        {
            get
            {
                return this.GetValue<string>("NTAuthenticationVariableName", "LOGON_USER");
            }
            set
            {
                this.SetValue<string>("NTAuthenticationVariableName", value);
            }
        }

        public int PagingResultsPerPage
        {
            get
            {
                return this.GetValue<int>("PagingResultsPerPage", 0x19);
            }
            set
            {
                this.SetValue<int>("PagingResultsPerPage", value);
            }
        }

        public bool PersistViewStateToDb
        {
            get
            {
                return this.GetValue<bool>("PersistViewStateToDb", false);
            }
            set
            {
                this.SetValue<bool>("PersistViewStateToDb", value);
            }
        }

        public string PipePrefix
        {
            get
            {
                return this.GetValue<string>("PipePrefix", "@@");
            }
            set
            {
                this.SetValue<string>("PipePrefix", value);
            }
        }

        public bool PreventRatingScaleWidthOverride
        {
            get
            {
                return (string.Compare(ConfigurationManager.AppSettings["PreventRatingScaleWidthOverride"], "true", true) == 0);
            }
        }

        public bool ReportIncompleteResponses
        {
            get
            {
                return this.GetValue<bool>("ReportIncompleteResponses", false);
            }
            set
            {
                this.SetValue<bool>("ReportIncompleteResponses", value);
            }
        }

        public bool RequireEmailAddressOnRegistration
        {
            get
            {
                return this.GetValue<bool>("RequireEmailAddressOnRegistration", true);
            }
            set
            {
                this.SetValue<bool>("RequireEmailAddressOnRegistration", value);
            }
        }

        public bool RequireTermsofUseAgreement
        {
            get
            {
                return this.GetValue<bool>("TermsofUseAgreement", false);
            }
            set
            {
                this.SetValue<bool>("TermsofUseAgreement", value);
            }
        }

        public int ResponseDataExportChunkSize
        {
            get
            {
                return this.GetValue<int>("ResponseDataExportChunkSize", 50);
            }
            set
            {
                this.SetValue<int>("ResponseDataExportChunkSize", value);
            }
        }

        public bool RestrictUploadFileExport
        {
            get
            {
                return this.GetValue<bool>("RestrictUploadFileExport", true);
            }
            set
            {
                this.SetValue<bool>("RestrictUploadFileExport", value);
            }
        }

        public string S3AccessKeyID
        {
            get
            {
                return this.GetValue<string>("S3AccessKeyID", string.Empty);
            }
            set
            {
                this.SetValue<string>("S3AccessKeyID", value);
            }
        }

        public string S3BucketName
        {
            get
            {
                return this.GetValue<string>("S3BucketName", string.Empty);
            }
            set
            {
                this.SetValue<string>("S3BucketName", value);
            }
        }

        public string S3CallingFormat
        {
            get
            {
                return this.GetValue<string>("S3CallingFormat", string.Empty);
            }
            set
            {
                this.SetValue<string>("S3CallingFormat", value);
            }
        }

        public string S3Endpoint
        {
            get
            {
                return this.GetValue<string>("S3Endpoint", string.Empty);
            }
            set
            {
                this.SetValue<string>("S3Endpoint", value);
            }
        }

        public string S3SecretAccessKey
        {
            get
            {
                return this.GetValue<string>("S3SecretAccessKey", string.Empty);
            }
            set
            {
                this.SetValue<string>("S3SecretAccessKey", value);
            }
        }

        public string S3TempBucketName
        {
            get
            {
                return this.GetValue<string>("S3TempBucketName", string.Empty);
            }
            set
            {
                this.SetValue<string>("S3TempBucketName", value);
            }
        }

        public bool SavePartialResponsesOnBackNavigation
        {
            get
            {
                return this.GetValue<bool>("SavePartialResponsesOnBackNavigation", false);
            }
            set
            {
                this.SetValue<bool>("SavePartialResponsesOnBackNavigation", value);
            }
        }

        public SessionType SessionMode
        {
            get
            {
                return this.GetValue<SessionType>("SessionMode", SessionType.Cookies);
            }
            set
            {
                this.SetValue<SessionType>("SessionMode", value);
            }
        }

        public int SessionTimeOut
        {
            get
            {
                return this.GetValue<int>("SessionTimeOut", 30);
            }
            set
            {
                this.SetValue<int>("SessionTimeOut", value);
            }
        }

        public bool SetSurveyDefaultButton
        {
            get
            {
                return this.GetValue<bool>("SetSurveyDefaultButton", true);
            }
            set
            {
                this.SetValue<bool>("SetSurveyDefaultButton", value);
            }
        }

        public bool ShowDeleteConfirmationPopups
        {
            get
            {
                return this.GetValue<bool>("ShowConfirmationPopups", true);
            }
            set
            {
                this.SetValue<bool>("ShowConfirmationPopups", value);
            }
        }

        public bool ShowNavWhenNotAuthenticated
        {
            get
            {
                return this.GetValue<bool>("ShowNavWhenNotAuthenticated", true);
            }
            set
            {
                this.SetValue<bool>("ShowNavWhenNotAuthenticated", value);
            }
        }

        public bool ShowPreviewInAnalysis
        {
            get
            {
                return this.GetValue<bool>("ShowPreviewInAnalysis", false);
            }
            set
            {
                this.SetValue<bool>("ShowPreviewInAnalysis", value);
            }
        }

        public bool ShowReportItemPreviewWatermark
        {
            get
            {
                return this.GetValue<bool>("ShowReportItemPreviewWatermark", true);
            }
            set
            {
                this.SetValue<bool>("ShowReportItemPreviewWatermark", value);
            }
        }

        public string SmtpPassword
        {
            get
            {
                return this.GetValue<string>("SmtpPassword", string.Empty);
            }
            set
            {
                this.SetValue<string>("SmtpPassword", value);
            }
        }

        public int SmtpPort
        {
            get
            {
                return this.GetValue<int>("SmtpPort", 0x19);
            }
            set
            {
                this.SetValue<int>("SmtpPort", value);
            }
        }

        public string SmtpServer
        {
            get
            {
                return this.GetValue<string>("SmtpServer", string.Empty);
            }
            set
            {
                this.SetValue<string>("SmtpServer", value);
            }
        }

        public string SmtpUserName
        {
            get
            {
                return this.GetValue<string>("SmtpUserName", string.Empty);
            }
            set
            {
                this.SetValue<string>("SmtpUserName", value);
            }
        }

        public bool StoreImagesInDatabase
        {
            get
            {
                return this.GetValue<bool>("StoreImagesInDatabase", true);
            }
            set
            {
                this.SetValue<bool>("StoreImagesInDatabase", value);
            }
        }

        public string SystemEmailAddress
        {
            get
            {
                return this.GetValue<string>("SystemEmailAddress", string.Empty);
            }
            set
            {
                this.SetValue<string>("SystemEmailAddress", value);
            }
        }

        public string TimeZone
        {
            get
            {
                return this.GetValue<string>("TimeZone", "UTC");
            }
            set
            {
                this.SetValue<string>("TimeZone", value);
            }
        }

        public string UploadedImagesFolder
        {
            get
            {
                return this.GetValue<string>("UploadedImagesFolder", string.Empty);
            }
            set
            {
                this.SetValue<string>("UploadedImagesFolder", value);
            }
        }

        public string UploadedImagesUrl
        {
            get
            {
                return this.GetValue<string>("UploadedImagesURL", string.Empty);
            }
            set
            {
                this.SetValue<string>("UploadedImagesURL", value);
            }
        }

        public string UploadItemExportPath
        {
            get
            {
                return this.GetValue<string>("UploadItemExportPath", "");
            }
            set
            {
                this.SetValue<string>("UploadItemExportPath", value);
            }
        }

        public bool UseDatePicker
        {
            get
            {
                return this.GetValue<bool>("UseDatePicker", true);
            }
            set
            {
                this.SetValue<bool>("UseDatePicker", value);
            }
        }

        public bool UseEncryption
        {
            get
            {
                return this.GetValue<bool>("UseEncryption", false);
            }
            set
            {
                this.SetValue<bool>("UseEncryption", value);
            }
        }

        public bool UseHTMLEditor
        {
            get
            {
                return this.GetValue<bool>("UseHTMLEditor", true);
            }
            set
            {
                this.SetValue<bool>("UseHTMLEditor", value);
            }
        }

        public bool UseReportingService
        {
            get
            {
                return this.GetValue<bool>("UseReportingService", false);
            }
            set
            {
                this.SetValue<bool>("UseReportingService", value);
            }
        }

        public ArrayList UserMapColumnNames
        {
            get
            {
                ArrayList list = new ArrayList();
                list.Add("UserName");
                list.Add("Password");
                list.Add("Domain");
                list.Add("UniqueIdentifier");
                return list;
            }
        }

        public bool UseS3ForTempFiles
        {
            get
            {
                return this.GetValue<bool>("UseS3ForTempFiles", false);
            }
            set
            {
                this.SetValue<bool>("UseS3ForTempFiles", value);
            }
        }

        public bool UseS3ForUploadedFiles
        {
            get
            {
                return this.GetValue<bool>("UseS3ForUploadedFiles", false);
            }
            set
            {
                this.SetValue<bool>("UseS3ForUploadedFiles", value);
            }
        }

        public int ViewReportTicketDuration
        {
            get
            {
                return this.GetValue<int>("ViewReportTicketDuration", 30);
            }
            set
            {
                this.SetValue<int>("ViewReportTicketDuration", value);
            }
        }

        public bool WebFarm
        {
            get
            {
                bool flag;
                return ((!string.IsNullOrEmpty(ConfigurationManager.AppSettings["WebFarm"]) && bool.TryParse(ConfigurationManager.AppSettings["WebFarm"], out flag)) && flag);
            }
        }

        public enum EditSurveyView
        {
            AllPages = 1,
            IndividualPages = 2
        }

        public enum HeaderType
        {
            Logo = 1,
            Text = 2
        }

        public enum OptionEntryType
        {
            Normal = 1,
            QuickEntry = 2,
            XMLImport = 3
        }

        public enum SessionType
        {
            Cookieless = 2,
            Cookies = 1
        }
    }
}

