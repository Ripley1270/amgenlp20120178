﻿using Checkbox.Forms.Items;
using Prezza.Framework.Data;
using Prezza.Framework.ExceptionHandling;
using System;
using System.Data;
using System.Runtime.CompilerServices;

namespace Checkbox.Forms.Items.Configuration
{
    [Serializable]
    public class SingleLineTextItemData : TextItemData
    {
        private PHTScreenShotSession _screenShotSession;

        protected override ItemData Copy()
        {
            SingleLineTextItemData data = (SingleLineTextItemData) base.Copy();
            if (data != null)
            {
                data.MaxValue = this.MaxValue;
                data.MinValue = this.MinValue;
            }
            return data;
        }

        protected override void Create(IDbTransaction t)
        {
            base.Create(t);
            if (base.ID <= 0)
            {
                throw new Exception("No DataID specified.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_InsertSLText");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("TextID", DbType.String, this.TextID);
            storedProcCommandWrapper.AddInParameter("SubTextID", DbType.String, this.SubTextID);
            storedProcCommandWrapper.AddInParameter("IsRequired", DbType.Int32, this.IsRequired ? 1 : 0);
            storedProcCommandWrapper.AddInParameter("DefaultTextID", DbType.String, this.DefaultTextID);
            storedProcCommandWrapper.AddInParameter("TextFormat", DbType.Int32, (int) this.Format);
            storedProcCommandWrapper.AddInParameter("MaxLength", DbType.Int32, this.MaxLength);
            storedProcCommandWrapper.AddInParameter("MaxValue", DbType.Double, this.MaxValue);
            storedProcCommandWrapper.AddInParameter("MinValue", DbType.Double, this.MinValue);
            storedProcCommandWrapper.AddInParameter("CustomFormatId", DbType.String, this.CustomFormatId);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        protected override Item CreateItem()
        {
            return new SingleLineTextBoxItem();
        }

        public override ItemTextDecorator CreateTextDecorator(string languageCode)
        {
            return new TextItemDecorator(this, languageCode);
        }

        protected override DataSet GetConcreteConfigurationDataSet()
        {
            if (base.ID <= 0)
            {
                throw new Exception("No DataID specified.");
            }
            try
            {
                string calledStoredProcedure = string.Empty;
                this._screenShotSession = new PHTScreenShotSession();
                if (this._screenShotSession.ScreenShotMode)
                    calledStoredProcedure = "PHTScreenShotItemDataGetSLText";
                else
                    calledStoredProcedure = "ckbx_ItemData_GetSLText";

                Database database = DatabaseFactory.CreateDatabase();
                //DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_GetSLText");
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper(calledStoredProcedure);
                storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
                DataSet concreteConfigurationDataSet = base.GetConcreteConfigurationDataSet();
                database.LoadDataSet(storedProcCommandWrapper, concreteConfigurationDataSet, this.DataTableName);
                return concreteConfigurationDataSet;
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessProtected"))
                {
                    throw;
                }
                return null;
            }
        }

        protected override void LoadFromDataRow(DataRow data)
        {
            if (data == null)
            {
                throw new Exception("DataRow cannot be null.");
            }
            try
            {
                this.IsRequired = DbUtility.GetValueFromDataRow<int>(data, "IsRequired", 0) == 1;
                this.Format = (AnswerFormat) Convert.ToInt32(DbUtility.GetValueFromDataRow<int>(data, "TextFormat", 0));
                this.CustomFormatId = DbUtility.GetValueFromDataRow<string>(data, "CustomFormatId", string.Empty);
                this.MinValue = DbUtility.GetValueFromDataRow<double?>(data, "MinValue", null);
                this.MaxValue = DbUtility.GetValueFromDataRow<double?>(data, "MaxValue", null);
                this.MaxLength = DbUtility.GetValueFromDataRow<int?>(data, "MaxLength", null);
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessProtected"))
                {
                    throw;
                }
            }
        }

        protected override void Update(IDbTransaction t)
        {
            base.Update(t);
            if (base.ID <= 0)
            {
                throw new Exception("No DataID specified.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_UpdateSLText");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("TextID", DbType.String, this.TextID);
            storedProcCommandWrapper.AddInParameter("SubTextID", DbType.String, this.SubTextID);
            storedProcCommandWrapper.AddInParameter("IsRequired", DbType.Int32, this.IsRequired ? 1 : 0);
            storedProcCommandWrapper.AddInParameter("DefaultTextID", DbType.String, this.DefaultTextID);
            storedProcCommandWrapper.AddInParameter("TextFormat", DbType.Int32, (int) this.Format);
            storedProcCommandWrapper.AddInParameter("MaxLength", DbType.Int32, this.MaxLength);
            storedProcCommandWrapper.AddInParameter("MaxValue", DbType.Double, this.MaxValue);
            storedProcCommandWrapper.AddInParameter("MinValue", DbType.Double, this.MinValue);
            storedProcCommandWrapper.AddInParameter("CustomFormatId", DbType.String, this.CustomFormatId);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        public override string DataTableName
        {
            get
            {
                return "SingleLineTextItemData";
            }
        }

        public double? MaxValue { get; set; }

        public double? MinValue { get; set; }
    }
}

