﻿namespace Checkbox.Messaging.Email
{
    using System;

    public interface IEmailMessageStatus
    {
        DateTime? LastSendAttempt { get; }

        string LastSendError { get; }

        DateTime? NextSendAttempt { get; }

        DateTime? QueuedDate { get; }

        bool SuccessfullySent { get; }
    }
}

