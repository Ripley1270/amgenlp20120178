﻿namespace Checkbox.Forms.Validation
{
    using Checkbox.Globalization.Text;
    using System;

    public class IPAddressValidator : RegularExpressionValidator
    {
        public IPAddressValidator()
        {
            base._regex = @"\b(?:\d{1,3}\.){3}\d{1,3}\b";
        }

        public override string GetMessage(string languageCode)
        {
            return TextManager.GetText("/validationMessages/regex/ipAddress", languageCode);
        }
    }
}

