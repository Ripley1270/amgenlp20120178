﻿namespace Checkbox.Forms.Items
{
    using System;

    public interface IScored
    {
        double GetScore();
    }
}

