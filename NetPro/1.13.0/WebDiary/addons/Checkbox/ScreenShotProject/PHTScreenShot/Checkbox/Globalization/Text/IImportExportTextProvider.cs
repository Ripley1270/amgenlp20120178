﻿namespace Checkbox.Globalization.Text
{
    using System;
    using System.IO;

    public interface IImportExportTextProvider
    {
        void ExportAllTexts(TextWriter writer);
        void ExportFilteredTexts(TextWriter writer, string[] languageCodes, params string[] partialTextId);
        void ExportTextsById(TextWriter writer, string[] languageCodes, string[] textIds);
        void ImportTexts(TextReader reader);
    }
}

