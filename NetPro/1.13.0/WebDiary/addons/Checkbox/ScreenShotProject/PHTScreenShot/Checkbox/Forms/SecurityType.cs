﻿namespace Checkbox.Forms
{
    using System;

    [Serializable]
    public enum SecurityType
    {
        AccessControlList = 3,
        AllRegisteredUsers = 4,
        InvitationOnly = 5,
        PasswordProtected = 2,
        Public = 1
    }
}

