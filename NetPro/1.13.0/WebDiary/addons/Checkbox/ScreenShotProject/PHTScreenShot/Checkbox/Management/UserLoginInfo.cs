﻿namespace Checkbox.Management
{
    using Prezza.Framework.Security;
    using System;

    [Serializable]
    public class UserLoginInfo : UserContext
    {
        private string userName;
        private IToken userToken;

        public UserLoginInfo(string userName, string userHostName, string userHostIP, string userAgent, IToken userToken) : base(userHostName, userHostIP, userAgent)
        {
            this.userToken = userToken;
            this.userName = userName;
        }

        public string UserName
        {
            get
            {
                return this.userName;
            }
        }

        public IToken UserToken
        {
            get
            {
                return this.userToken;
            }
        }
    }
}

