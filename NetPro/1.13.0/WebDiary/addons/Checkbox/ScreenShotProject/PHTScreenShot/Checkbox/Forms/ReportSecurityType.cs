﻿namespace Checkbox.Forms
{
    using System;

    [Serializable]
    public enum ReportSecurityType
    {
        DetailsAcl = 8,
        DetailsPrivate = 4,
        DetailsPublic = 6,
        DetailsRegisteredUsers = 10,
        Private = 1,
        Public = 2,
        SummaryAcl = 7,
        SummaryPrivate = 3,
        SummaryPublic = 5,
        SummaryRegisteredUsers = 9
    }
}

