﻿namespace Checkbox.Forms.Items
{
    using Checkbox.Forms;
    using Checkbox.Forms.Items.Configuration;
    using Prezza.Framework.Common;
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Xml;

    [Serializable]
    public abstract class ResponseItem : Item
    {
        private IAnswerData _answerData;
        private bool _required;
        private Checkbox.Forms.Response _response;
        private bool _valid = true;
        private List<string> _validationErrors;

        protected ResponseItem()
        {
        }

        private void _response_StateRestored(object sender, ResponseStateEventArgs e)
        {
            this.OnStateRestored();
        }

        public override void Configure(ItemData configuration, string languageCode)
        {
            base.Configure(configuration, languageCode);
            ArgumentValidation.CheckExpectedType(configuration, typeof(ResponseItemData));
            this._required = ((ResponseItemData) configuration).IsRequired;
        }

        protected virtual bool DoValidateItem()
        {
            return true;
        }

        protected virtual IAnswerData GetAnswerData()
        {
            return this._answerData;
        }

        protected override NameValueCollection GetInstanceDataValuesForXmlSerialization()
        {
            NameValueCollection instanceDataValuesForXmlSerialization = base.GetInstanceDataValuesForXmlSerialization();
            instanceDataValuesForXmlSerialization["isValid"] = this.Valid.ToString();
            return instanceDataValuesForXmlSerialization;
        }

        protected virtual string GetPipedText(string key, string text)
        {
            if ((this.Response != null) && (this.Response.PipeMediator != null))
            {
                return this.Response.PipeMediator.GetText(base.ID, key, text);
            }
            return text;
        }

        protected virtual Checkbox.Forms.Response GetResponse()
        {
            return this._response;
        }

        protected virtual void OnAnswerDataSet()
        {
            this._valid = true;
        }

        protected override void OnPageLoad()
        {
            this.RunRules();
            base.OnPageLoad();
        }

        protected virtual void OnResponseSet()
        {
        }

        protected virtual void OnStateRestored()
        {
        }

        internal virtual void RunRules()
        {
            this._response.RulesEngine.RunRules(this);
        }

        protected virtual void SetAnswerData(IAnswerData answerData)
        {
            this._answerData = answerData;
        }

        protected virtual void SetResponse(Checkbox.Forms.Response response)
        {
            this._response = response;
            this._response.StateRestored += new Checkbox.Forms.Response.StateRestoredHandler(this._response_StateRestored);
        }

        public virtual void Validate()
        {
            this.ValidationErrors.Clear();
            this.Valid = this.DoValidateItem();
        }

        public override void WriteXmlInstanceData(XmlWriter writer)
        {
            base.WriteXmlInstanceData(writer);
            writer.WriteStartElement("validationErrors");
            foreach (string str in this.ValidationErrors)
            {
                writer.WriteElementString("validationError", str);
            }
            writer.WriteEndElement();
        }

        public IAnswerData AnswerData
        {
            get
            {
                return this.GetAnswerData();
            }
            set
            {
                this.SetAnswerData(value);
                this.OnAnswerDataSet();
            }
        }

        public bool EditMode
        {
            get
            {
                return (this.Response == null);
            }
        }

        public virtual bool Required
        {
            get
            {
                return this._required;
            }
        }

        public Checkbox.Forms.Response Response
        {
            get
            {
                return this.GetResponse();
            }
            set
            {
                this.SetResponse(value);
                this.OnResponseSet();
            }
        }

        public bool Valid
        {
            get
            {
                return this._valid;
            }
            protected set
            {
                this._valid = value;
            }
        }

        public List<string> ValidationErrors
        {
            get
            {
                if (this._validationErrors == null)
                {
                    this._validationErrors = new List<string>();
                }
                return this._validationErrors;
            }
        }
    }
}

