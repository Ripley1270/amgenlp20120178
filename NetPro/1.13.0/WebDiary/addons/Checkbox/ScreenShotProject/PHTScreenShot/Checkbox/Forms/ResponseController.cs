﻿namespace Checkbox.Forms
{
    using Checkbox.Common;
    using Checkbox.Forms.Security.Principal;
    using Checkbox.Forms.Security.Providers;
    using Checkbox.Globalization.Text;
    using Checkbox.Invitations;
    using Checkbox.Users;
    using Prezza.Framework.Common;
    using Prezza.Framework.ExceptionHandling;
    using Prezza.Framework.Security;
    using Prezza.Framework.Security.Principal;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class ResponseController : IDisposable
    {
        private DataTable _completedResponseList;
        private bool _responseLimitReached;
        private IResponseSession _responseSession;
        private ResponseViewDisplayFlags _staticDisplayFlags;
        private Dictionary<string, string> _supportedLanguages;

        public ResponseController()
        {
            this.SessionState = ResponseSessionState.None;
        }

        private void _response_ResponseCompleted(object sender, ResponseStateEventArgs e)
        {
            this.Response.ResponseCompleted -= new Checkbox.Forms.Response.ResponseCompletedHandler(this._response_ResponseCompleted);
            if ((this.Response != null) && this._responseSession.RecipientGuid.HasValue)
            {
                long? recipientId = InvitationManager.GetRecipientId(this._responseSession.RecipientGuid.Value);
                if (recipientId.HasValue)
                {
                    Invitation.RecordResponse(recipientId.Value, this.Response.ID.Value);
                }
            }
            this._responseSession.ClearPersistedValues();
        }

        private bool AuthorizeRespondent(ResponseTemplate rt, ExtendedPrincipal respondent, out string message)
        {
            bool flag;
            message = string.Empty;
            string[] supportedLanguages = rt.SupportedLanguages;
            string languageCode = this.GetLanguageCode(rt);
            if (Utilities.IsNullOrEmpty(languageCode))
            {
                languageCode = rt.DefaultLanguage;
            }
            if (rt.IsDeleted)
            {
                message = TextManager.GetText("/pageText/survey.aspx/surveyDeleted", languageCode, "This survey is no longer available.", supportedLanguages);
                return false;
            }
            IAuthorizationProvider authorizationProvider = AuthorizationFactory.GetAuthorizationProvider("ResponseTemplateAuthorizationProvider");
            if (authorizationProvider.Authorize(respondent, rt, "Form.Edit"))
            {
                return true;
            }
            string context = this._responseSession.AdminEditMode ? "Analysis.Responses.Edit" : "Form.Fill";
            if (authorizationProvider is ResponseTemplateAuthorizationProvider)
            {
                flag = ((ResponseTemplateAuthorizationProvider) authorizationProvider).Authorize(respondent, rt, context, this._responseSession.RecipientGuid);
            }
            else
            {
                flag = authorizationProvider.Authorize(respondent, rt, context);
            }
            string str3 = authorizationProvider.GetAuthorizationErrorType().ToLower();
            if ((this._responseSession.RecipientGuid.HasValue && (rt.SecurityType == SecurityType.InvitationOnly)) && rt.MaxResponsesPerUser.HasValue)
            {
                int recipientResponseCount = InvitationManager.GetRecipientResponseCount(this._responseSession.RecipientGuid.Value);
                if (recipientResponseCount >= rt.MaxResponsesPerUser)
                {
                    flag = false;
                    str3 = rt.AllowEdit ? "editonly" : "responselimitreached";
                }
            }
            if (flag)
            {
                return true;
            }
            if (str3 == "notauthorized")
            {
                message = TextManager.GetText("/pageText/survey.aspx/respondentNotAuthorized", languageCode, "You are not authorized to take this survey.", supportedLanguages);
            }
            else if (str3 == "notactive")
            {
                message = TextManager.GetText("/pageText/survey.aspx/surveyNotActive", languageCode, "This survey has not been activated.", supportedLanguages);
            }
            else if (str3 == "loginrequired")
            {
                this.SessionState = ResponseSessionState.LoginRequired;
                message = str3;
            }
            else if (str3 == "beforestartdate")
            {
                message = TextManager.GetText("/pageText/survey.aspx/surveyNotYetActive", languageCode, "The activation period for this survey has not started.", supportedLanguages);
            }
            else if (str3 == "afterenddate")
            {
                message = TextManager.GetText("/pageText/survey.aspx/surveyActivationEnded", languageCode, "The activation period for this survey has ended.", supportedLanguages);
            }
            else if (str3 == "responselimitreached")
            {
                message = TextManager.GetText("/pageText/survey.aspx/MaxResponses", languageCode, "No more responses are permitted for this survey.", supportedLanguages);
            }
            else
            {
                if (str3 == "editonly")
                {
                    this._responseLimitReached = true;
                    return true;
                }
                if (str3 == "noinvitation")
                {
                    message = TextManager.GetText("/pageText/survey.aspx/noInvitation", languageCode, "This survey is only available with a valid invitation.", supportedLanguages);
                }
                else if (str3 == "invalidinvitation")
                {
                    message = TextManager.GetText("/pageText/survey.aspx/noInvitation", languageCode, "The specified invitation is not valid for this survey.", supportedLanguages);
                }
            }
            return false;
        }

        private bool CheckPassword(ResponseTemplate rt)
        {
            return ((rt.SecurityType != SecurityType.PasswordProtected) || ((rt.Password == this._responseSession.Password) && (this._responseSession.Password == rt.Password)));
        }

        public void Dispose()
        {
            this.Dispose(true);
        }

        protected virtual void Dispose(bool isDisposing)
        {
            if (isDisposing && (this.Response != null))
            {
                this.Response = null;
            }
        }

        public void Finish()
        {
            if (!this.IsNavigationAllowed())
            {
                this.Response.GoToCompletionPage();
            }
            else
            {
                this.Response.MoveNext();
                if (this.Response.CurrentPage != null)
                {
                    this._responseSession.CurrentPageId = new int?(this.Response.CurrentPage.PageId);
                }
                else
                {
                    this._responseSession.CurrentPageId = null;
                }
            }
        }

        public DataTable GetCompletedResponseList()
        {
            return this._completedResponseList;
        }

        private string GetLanguageCode(ResponseTemplate rt)
        {
            string languageCode = this._responseSession.LanguageCode;
            Dictionary<string, string> dictionary = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);
            List<string> list = new List<string>(rt.SupportedLanguages);
            foreach (string str2 in list)
            {
                dictionary[str2] = str2;
            }
            if (Utilities.IsNotNullOrEmpty(languageCode) && dictionary.ContainsKey(languageCode))
            {
                return languageCode;
            }
            languageCode = this._responseSession.GetRespondentLanguageCode(rt.LanguageSource, rt.LanguageSourceToken);
            if (Utilities.IsNotNullOrEmpty(languageCode) && dictionary.ContainsKey(languageCode))
            {
                this._responseSession.LanguageCode = languageCode;
                return languageCode;
            }
            if (list.Count == 1)
            {
                this._responseSession.LanguageCode = rt.DefaultLanguage;
                return rt.DefaultLanguage;
            }
            return null;
        }

        private ExtendedPrincipal GetRespondent()
        {
            ExtendedPrincipal userByGUID = null;
            if (this._responseSession.UserGuid.HasValue)
            {
                userByGUID = (ExtendedPrincipal) UserManager.GetUserByGUID(this._responseSession.UserGuid.Value);
            }
            if (userByGUID == null)
            {
                if (this._responseSession.Respondent != null)
                {
                    userByGUID = this._responseSession.Respondent;
                }
                else if (this._responseSession.RecipientGuid.HasValue)
                {
                    Invitation invitationForRecipient = InvitationManager.GetInvitationForRecipient(this._responseSession.RecipientGuid.Value);
                    if ((invitationForRecipient != null) && (invitationForRecipient.Template.LoginOption == LoginOption.Auto))
                    {
                        string recipientUniqueIdentifier = InvitationManager.GetRecipientUniqueIdentifier(this._responseSession.RecipientGuid.Value);
                        if (Utilities.IsNotNullOrEmpty(recipientUniqueIdentifier))
                        {
                            userByGUID = (ExtendedPrincipal) UserManager.GetUser(recipientUniqueIdentifier);
                        }
                    }
                    else
                    {
                        userByGUID = new AnonymousRespondent(this._responseSession.RecipientGuid);
                    }
                }
            }
            if (userByGUID == null)
            {
                userByGUID = new AnonymousRespondent(this._responseSession.AnonymousRespondentGuid);
            }
            this._responseSession.Respondent = userByGUID;
            return userByGUID;
        }

        private ResponseState GetResponseState(ResponseTemplate rt)
        {
            if (this._responseSession.ResponseGuid.HasValue)
            {
                if ((this._responseSession.RespondentEditMode || this._responseSession.AdminEditMode) && (!AuthorizationFactory.GetAuthorizationProvider("ResponseTemplateAuthorizationProvider").Authorize(this._responseSession.Respondent, rt, "Analysis.Responses.Edit") && !rt.AllowEdit))
                {
                    throw new Exception("User does not have permission to edit survey responses and/or survey is not configured to allow respondents to edit their responses.");
                }
                return ResponseStateManager.GetResponseState(this._responseSession.ResponseGuid.Value);
            }
            if (rt.AllowContinue)
            {
                return ResponseStateManager.GetLastIncompleteResponse(this._responseSession.Respondent, rt.ID.Value, rt.AnonymizeResponses);
            }
            return null;
        }

        public void GoToPage(int pageId)
        {
            if (this.Response != null)
            {
                this.Response.MoveToPage(pageId);
            }
        }

        public bool Initialize(IResponseSession responseSession, out string message)
        {
            bool flag = false;
            try
            {
                ArgumentValidation.CheckForNullReference(responseSession, "Response Session");
                this._responseSession = responseSession;
                flag = this.InitializeResponse(out message);
            }
            catch (Exception exception)
            {
                ExceptionPolicy.HandleException(exception, "BusinessProtected");
                this.SessionState = ResponseSessionState.Error;
                message = exception.Message;
            }
            return flag;
        }

        private bool InitializeResponse(out string message)
        {
            string invitee = string.Empty;
            ResponseTemplate rt = this.LoadResponseTemplate();
            this.SurveyDefaultLanguage = rt.DefaultLanguage;
            this.StyleTemplateId = rt.StyleTemplateID;
            this.EnablePageValidationAlert = rt.ShowValidationErrorAlert;
            if (this._responseSession.ForceNew && !this._responseSession.IsFormPost)
            {
                this._responseSession.ClearPersistedValues();
            }
            ExtendedPrincipal respondent = this.GetRespondent();
            if (!this.AuthorizeRespondent(rt, respondent, out message))
            {
                return false;
            }
            if (this._responseSession.RecipientGuid.HasValue)
            {
                invitee = InvitationManager.GetRecipientEmail(this._responseSession.RecipientGuid.Value);
            }
            foreach (string str2 in rt.SupportedLanguages)
            {
                this.SupportedLanguages[str2] = TextManager.GetText("/languageText/" + str2, str2, str2, rt.SupportedLanguages);
            }
            if (this._responseSession.ForceNew && !this._responseSession.IsFormPost)
            {
                this.Response = rt.CreateResponse(rt.DefaultLanguage);
                this.Response.Initialize(this._responseSession.RespondentIPAddress, this._responseSession.ServerUserContext, rt.DefaultLanguage, this._responseSession.IsTest, invitee, this._responseSession.Respondent);
                this._responseSession.ResponseGuid = this.Response.GUID;
            }
            string languageCode = this.GetLanguageCode(rt);
            if (Utilities.IsNullOrEmpty(languageCode))
            {
                this.SessionState = ResponseSessionState.SelectLanguage;
                return false;
            }
            if (!this.CheckPassword(rt))
            {
                this.SessionState = ResponseSessionState.EnterPassword;
                return false;
            }
            if (this.Response == null)
            {
                this.Response = rt.CreateResponse(languageCode);
                ResponseState responseState = this.GetResponseState(rt);
                if (responseState == null)
                {
                    if (rt.AllowEdit)
                    {
                        this._completedResponseList = ResponseStateManager.GetCompletedResponseList(this._responseSession.Respondent, rt.ID.Value, rt.AnonymizeResponses);
                        if (this._responseLimitReached || ((this._completedResponseList != null) && (this._completedResponseList.Select().Length > 0)))
                        {
                            this.SessionState = ResponseSessionState.EditResponse;
                            return false;
                        }
                    }
                    this.Response.Initialize(this._responseSession.RespondentIPAddress, this._responseSession.ServerUserContext, languageCode, this._responseSession.IsTest, invitee, this._responseSession.Respondent);
                    this._responseSession.ResponseGuid = this.Response.GUID;
                }
                else
                {
                    this.Response.Restore(responseState);
                    if ((!this._responseSession.IsFormPost || (this.SessionState == ResponseSessionState.SelectLanguage)) || (this.SessionState == ResponseSessionState.EnterPassword))
                    {
                        if (this._responseSession.AdminEditMode || this._responseSession.RespondentEditMode)
                        {
                            this.Response.MoveToStart();
                        }
                        else
                        {
                            this.Response.LoadCurrentPage();
                        }
                    }
                }
            }
            this.Response.LanguageCode = languageCode;
            this.Response.ResponseCompleted += new Checkbox.Forms.Response.ResponseCompletedHandler(this._response_ResponseCompleted);
            this.SetStaticDisplayFlags(rt);
            this.SessionState = ResponseSessionState.TakeSurvey;
            return true;
        }

        public bool IsNavigationAllowed()
        {
            if (this.Response.Completed && !this._responseSession.AdminEditMode)
            {
                return this._responseSession.RespondentEditMode;
            }
            return true;
        }

        private ResponseTemplate LoadResponseTemplate()
        {
            ResponseTemplate responseTemplate = null;
            if (this._responseSession.SurveyGuid.HasValue)
            {
                responseTemplate = ResponseTemplateManager.GetResponseTemplate(this._responseSession.SurveyGuid.Value);
            }
            else if (this._responseSession.RecipientGuid.HasValue)
            {
                this._responseSession.SurveyGuid = InvitationManager.GetResponseTemplateGuidForInvitation(this._responseSession.RecipientGuid.Value);
                if (this._responseSession.SurveyGuid.HasValue)
                {
                    responseTemplate = ResponseTemplateManager.GetResponseTemplate(this._responseSession.SurveyGuid.Value);
                }
            }
            if (responseTemplate == null)
            {
                throw new Exception(TextManager.GetText("/controlText/responseController/unableToLoadSurvey", TextManager.DefaultLanguage, "Unable to load a survey.  The survey or invitation id was not present in the URL or refers to a deleted survey or invitation.", new string[0]));
            }
            return responseTemplate;
        }

        public bool MoveNext()
        {
            if (!this.IsNavigationAllowed())
            {
                this.Response.GoToCompletionPage();
                return true;
            }
            int? nullable = (this.Response.CurrentPage != null) ? new int?(this.Response.CurrentPage.PageId) : null;
            this.Response.MoveNext();
            if (this.Response.CurrentPage != null)
            {
                this._responseSession.CurrentPageId = new int?(this.Response.CurrentPage.PageId);
            }
            else
            {
                this._responseSession.CurrentPageId = null;
            }
            if ((this.Response.CurrentPage != null) && (nullable == this._responseSession.CurrentPageId))
            {
                return this.Response.CurrentPage.Valid;
            }
            return true;
        }

        public void MovePrevious()
        {
            if (!this.IsNavigationAllowed())
            {
                this.Response.GoToCompletionPage();
            }
            else
            {
                this.Response.MovePrevious();
                if (this.Response.CurrentPage != null)
                {
                    this._responseSession.CurrentPageId = new int?(this.Response.CurrentPage.PageId);
                }
                else
                {
                    this._responseSession.CurrentPageId = null;
                }
            }
        }

        private void SetStaticDisplayFlags(ResponseTemplate rt)
        {
            this._staticDisplayFlags = ResponseViewDisplayFlags.None;
            if (rt.ShowItemNumbers)
            {
                this._staticDisplayFlags |= ResponseViewDisplayFlags.ItemNumbers;
            }
            if (rt.ShowPageNumbers)
            {
                this._staticDisplayFlags |= ResponseViewDisplayFlags.PageNumbers;
            }
            if (rt.ShowProgressBar)
            {
                this._staticDisplayFlags |= ResponseViewDisplayFlags.ProgressBar;
            }
            if (rt.ShowSaveAndQuit && rt.AllowContinue)
            {
                this._staticDisplayFlags |= ResponseViewDisplayFlags.SaveButton;
            }
            if (rt.ShowTitle)
            {
                this._staticDisplayFlags |= ResponseViewDisplayFlags.Title;
            }
        }

        public ResponseViewDisplayFlags DisplayFlags
        {
            get
            {
                if ((this.SessionState == ResponseSessionState.EditResponse) && !this._responseLimitReached)
                {
                    return ResponseViewDisplayFlags.CreateNewButton;
                }
                if (this.SessionState != ResponseSessionState.TakeSurvey)
                {
                    return ResponseViewDisplayFlags.None;
                }
                ResponseViewDisplayFlags flags = this._staticDisplayFlags;
                if ((this.Response != null) && this.Response.StateIsValid)
                {
                    if (!this.Response.CanNavigateForward)
                    {
                        flags &= ~ResponseViewDisplayFlags.ProgressBar;
                        flags &= ~ResponseViewDisplayFlags.PageNumbers;
                        flags &= ~ResponseViewDisplayFlags.SaveButton;
                    }
                    if (this.Response.CanNavigateForward && !this.Response.CompleteOnNext)
                    {
                        flags |= ResponseViewDisplayFlags.NextButton;
                    }
                    if (this.Response.CanNavigateForward && this.Response.CompleteOnNext)
                    {
                        flags |= ResponseViewDisplayFlags.FinishButton;
                    }
                    if (this.Response.CanNavigateBack)
                    {
                        flags |= ResponseViewDisplayFlags.BackButton;
                    }
                }
                return flags;
            }
        }

        public bool EnablePageValidationAlert { get; private set; }

        public Checkbox.Forms.Response Response { get; private set; }

        public ResponseSessionState SessionState { get; private set; }

        public int? StyleTemplateId { get; private set; }

        public Dictionary<string, string> SupportedLanguages
        {
            get
            {
                if (this._supportedLanguages == null)
                {
                    this._supportedLanguages = new Dictionary<string, string>();
                }
                return this._supportedLanguages;
            }
        }

        public string SurveyDefaultLanguage { get; private set; }
    }
}

