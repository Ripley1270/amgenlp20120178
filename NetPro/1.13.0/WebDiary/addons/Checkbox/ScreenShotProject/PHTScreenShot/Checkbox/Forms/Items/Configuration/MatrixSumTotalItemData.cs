﻿namespace Checkbox.Forms.Items.Configuration
{
    using Checkbox.Forms.Items;
    using Checkbox.Forms.Logic;
    using Checkbox.Globalization.Text;
    using Prezza.Framework.Data;
    using System;
    using System.Data;

    [Serializable]
    public class MatrixSumTotalItemData : SingleLineTextItemData
    {
        private LogicalOperator _comparisonOperator = LogicalOperator.Equal;
        private double _totalValue = 0.0;

        protected override void Create(IDbTransaction t)
        {
            base.Create(t);
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_InsertMatrixSumTotal");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("Operator", DbType.String, this.ComparisonOperator.ToString());
            storedProcCommandWrapper.AddInParameter("SumValue", DbType.Double, this.TotalValue);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        protected override Item CreateItem()
        {
            return new MatrixSumTotalItem();
        }

        protected override DataSet GetConcreteConfigurationDataSet()
        {
            if (!base.ID.HasValue)
            {
                throw new Exception("Unable to load matrix sum total item data with no id.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_GetMatrixSumTotal");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            DataSet set = database.ExecuteDataSet(storedProcCommandWrapper);
            if (set.Tables.Count > 0)
            {
                set.Tables[0].TableName = this.DataTableName;
            }
            return set;
        }

        protected override void LoadFromDataRow(DataRow data)
        {
            base.LoadFromDataRow(data);
            if ((data["SumValue"] != DBNull.Value) && (data["SumValue"].ToString().Trim() != string.Empty))
            {
                this.TotalValue = Convert.ToDouble(data["SumValue"]);
            }
            else
            {
                this.TotalValue = 0.0;
            }
            if ((data["Operator"] != DBNull.Value) && (data["Operator"].ToString().Trim() != string.Empty))
            {
                this.ComparisonOperator = (LogicalOperator) Enum.Parse(typeof(LogicalOperator), (string) data["Operator"]);
            }
            else
            {
                this.ComparisonOperator = LogicalOperator.Equal;
            }
        }

        public override DataSet LoadTextData()
        {
            DataSet ds = base.LoadTextData();
            if (!string.IsNullOrEmpty(this.TextID))
            {
                this.MergeTextData(ds, TextManager.GetTextData(this.TextID), this.TextTableName, "text", this.TextIDPrefix, base.ID.Value);
                this.MergeTextData(ds, TextManager.GetTextData(this.DefaultTextID), this.TextTableName, "defaultText", this.TextIDPrefix, base.ID.Value);
            }
            return ds;
        }

        protected override void Update(IDbTransaction t)
        {
            base.Update(t);
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_UpdateMatrixSumTotal");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("Operator", DbType.String, this.ComparisonOperator.ToString());
            storedProcCommandWrapper.AddInParameter("SumValue", DbType.Double, this.TotalValue);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        public virtual LogicalOperator ComparisonOperator
        {
            get
            {
                return this._comparisonOperator;
            }
            set
            {
                if (((value != LogicalOperator.Equal) && (value != LogicalOperator.GreaterThan)) && (((value != LogicalOperator.GreaterThanEqual) && (value != LogicalOperator.LessThan)) && (value != LogicalOperator.LessThanEqual)))
                {
                    throw new Exception("Operator type " + value + " is not supported for matrix sum total columns.");
                }
                this._comparisonOperator = value;
            }
        }

        public override string DataTableName
        {
            get
            {
                return "MatrixSumTotalItemData";
            }
        }

        public override AnswerFormat Format
        {
            get
            {
                if (((base.Format != AnswerFormat.Decimal) && (base.Format != AnswerFormat.Integer)) && (base.Format != AnswerFormat.Numeric))
                {
                    return AnswerFormat.Numeric;
                }
                return base.Format;
            }
            set
            {
                if (((value != AnswerFormat.Decimal) && (value != AnswerFormat.Integer)) && (value != AnswerFormat.Numeric))
                {
                    base.Format = AnswerFormat.Numeric;
                }
                else
                {
                    base.Format = value;
                }
            }
        }

        public virtual double TotalValue
        {
            get
            {
                return this._totalValue;
            }
            set
            {
                this._totalValue = value;
            }
        }
    }
}

