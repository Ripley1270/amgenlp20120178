﻿namespace Checkbox.Forms.Logic.Configuration
{
    using Checkbox.Forms;
    using Checkbox.Forms.Logic;
    using Prezza.Framework.Common;
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class StringOperandData : OperandData
    {
        public StringOperandData(ResponseTemplate context) : this(context, null)
        {
        }

        public StringOperandData(ResponseTemplate context, string value) : base(context)
        {
            this.Value = value;
        }

        protected override void Create(IDbTransaction t)
        {
            base.Create(t);
            DataRow row = base.Context.ValueOperandTable.NewRow();
            row["OperandID"] = base.ID.Value;
            row["AnswerValue"] = this.Value;
            base.Context.ValueOperandTable.Rows.Add(row);
        }

        public override Operand CreateOperand(Response context, string languageCode)
        {
            return new StringOperand(this.Value);
        }

        public override void Delete(IDbTransaction transaction)
        {
            DataRow[] rowArray = base.Context.ValueOperandTable.Select(this.IdentityColumnName + "=" + base.ID.Value);
            if (rowArray.Length > 0)
            {
                rowArray[0].Delete();
            }
            base.Delete(transaction);
        }

        protected override DataSet GetConcreteConfigurationDataSet()
        {
            if (base.ID <= 0)
            {
                throw new ApplicationException("No DataID specified.");
            }
            try
            {
                Database database = DatabaseFactory.CreateDatabase();
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ValueOperand_GetOperand");
                storedProcCommandWrapper.AddInParameter("OperandID", DbType.Int32, base.ID);
                DataSet dataSet = new DataSet();
                database.LoadDataSet(storedProcCommandWrapper, dataSet, new string[] { this.DataTableName });
                return dataSet;
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessProtected"))
                {
                    throw;
                }
                return null;
            }
        }

        protected override void LoadFromDataRow(DataRow data)
        {
            ArgumentValidation.CheckForNullReference(data, "data");
            try
            {
                this.Value = (data["AnswerValue"] != DBNull.Value) ? ((string) data["AnswerValue"]) : null;
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessProtected"))
                {
                    throw;
                }
            }
        }

        public override string ToString()
        {
            return this.Value;
        }

        public override void Validate()
        {
        }

        public override string DataTableName
        {
            get
            {
                return "ValueOperand";
            }
        }

        public override string OperandTypeName
        {
            get
            {
                return "StringOperand";
            }
        }

        public string Value { get; set; }
    }
}

