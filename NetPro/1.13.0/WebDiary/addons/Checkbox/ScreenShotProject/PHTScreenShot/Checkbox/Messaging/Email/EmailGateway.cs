﻿namespace Checkbox.Messaging.Email
{
    using Checkbox.Management;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Threading;

    public static class EmailGateway
    {
        public static long? AddEmailMessageToBatch(long batchId, IEmailMessage message)
        {
            if (ApplicationManager.AppSettings.EmailEnabled)
            {
                return new long?(EmailFactory.GetEmailProvider().AddEmailMessageToBatch(batchId, message));
            }
            return null;
        }

        public static long? CreateEmailBatch(string batchName, string createdBy, DateTime? earliestSendDate)
        {
            if (ApplicationManager.AppSettings.EmailEnabled)
            {
                return new long?(EmailFactory.GetEmailProvider().CreateEmailMessageBatch(batchName, createdBy, earliestSendDate));
            }
            return null;
        }

        public static void DeleteMessage(long messageId)
        {
            if (ApplicationManager.AppSettings.EmailEnabled)
            {
                EmailFactory.GetEmailProvider().DeleteEmailMessage(messageId);
            }
        }

        public static void DeleteMessageBatch(long batchId)
        {
            if (ApplicationManager.AppSettings.EmailEnabled)
            {
                EmailFactory.GetEmailProvider().DeleteEmailMessage(batchId);
            }
        }

        public static IEmailMessageBatchStatus GetMessageBatchStatus(long batchId)
        {
            if (ApplicationManager.AppSettings.EmailEnabled)
            {
                return EmailFactory.GetEmailProvider().GetEmailMessageBatchStatus(batchId);
            }
            return null;
        }

        public static IEmailMessage GetMessageFromBatch(long messageId)
        {
            if (ApplicationManager.AppSettings.EmailEnabled)
            {
                return EmailFactory.GetEmailProvider().GetMessage(messageId);
            }
            return null;
        }

        public static IEmailMessageStatus GetMessageStatus(long messageId)
        {
            if (ApplicationManager.AppSettings.EmailEnabled)
            {
                return EmailFactory.GetEmailProvider().GetEmailMessageStatus(messageId);
            }
            return null;
        }

        public static void MarkEmailBatchReady(long batchId)
        {
            if (ApplicationManager.AppSettings.EmailEnabled)
            {
                EmailFactory.GetEmailProvider().CloseEmailMessageBatch(batchId);
            }
        }

        public static long? RegisterAttachment(IEmailAttachment attachment)
        {
            if (ApplicationManager.AppSettings.EmailEnabled)
            {
                return new long?(EmailFactory.GetEmailProvider().RegisterMessageAttachment(attachment));
            }
            return null;
        }

        public static long? Send(IEmailMessage message)
        {
            try
            {
                if (ApplicationManager.AppSettings.EmailEnabled)
                {
                    if (ApplicationManager.AppSettings.EmailTestModeEnabled)
                    {
                        Thread.Sleep(ApplicationManager.AppSettings.EmailTestModeSleepTime);
                        return -1L;
                    }
                    return EmailFactory.GetEmailProvider().Send(message);
                }
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
            }
            return null;
        }

        public static bool ProviderSupportsBatches
        {
            get
            {
                return EmailFactory.GetEmailProvider().SupportsBatches;
            }
        }
    }
}

