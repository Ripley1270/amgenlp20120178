﻿namespace Checkbox.ActiveDirectory.Providers
{
    using Checkbox.ActiveDirectory;
    using Checkbox.Management;
    using Prezza.Framework.Common;
    using Prezza.Framework.Configuration;
    using Prezza.Framework.Logging;
    using Prezza.Framework.Security;
    using System;
    using System.DirectoryServices;
    using System.Runtime.InteropServices;
    using System.Security.Principal;

    public class LDAPAuthenticationProvider : IAuthenticationProvider, IConfigurationProvider
    {
        private LDAPAuthenticationProviderData config;
        private string configurationName;

        //protected static bool Authenticate(NamePasswordCredential credentials)
        //{
        //    bool flag;
        //    string activeDirectoryNamingContext = ApplicationManager.AppSettings.ActiveDirectoryNamingContext;
        //    string name = credentials.Name;
        //    string password = credentials.Password;
        //    switch (activeDirectoryNamingContext)
        //    {
        //        case null:
        //        case string.Empty:
        //            return false;
        //    }
        //    switch (name)
        //    {
        //        case null:
        //        case string.Empty:
        //            return false;
        //    }
        //    DirectoryEntry searchRoot = new DirectoryEntry("LDAP://" + activeDirectoryNamingContext, ActiveDirectoryManager.GetQualifiedUsername(name), password, ActiveDirectoryManager.SecureAuthenticationFlags | ActiveDirectoryManager.FastAuthenticationFlags);
        //    try
        //    {
        //        object nativeObject = searchRoot.NativeObject;
        //        DirectorySearcher searcher = new DirectorySearcher(searchRoot) {
        //            Filter = string.Format("(SAMAccountName={0})", ActiveDirectoryManager.GetUsername(name))
        //        };
        //        searcher.PropertiesToLoad.Add("cn");
        //        if (searcher.FindOne() == null)
        //        {
        //            return false;
        //        }
        //        flag = true;
        //    }
        //    catch (Exception exception)
        //    {
        //        if (exception.Message.Contains("unknown user name or bad password"))
        //        {
        //            throw;
        //        }
        //        throw new Exception("Error authenticating user. " + exception.Message);
        //    }
        //    return flag;
        //}

        protected static bool Authenticate(NamePasswordCredential credentials)
        {
            bool flag;
            string activeDirectoryNamingContext = ApplicationManager.AppSettings.ActiveDirectoryNamingContext;
            string name = credentials.Name;
            string password = credentials.Password;
            if (activeDirectoryNamingContext == null || activeDirectoryNamingContext == string.Empty)
            {
                return false;
            }
            else
            {
                if (name == null || name == string.Empty)
                {
                    return false;
                }
                else
                {
                    string str = string.Concat("LDAP://", activeDirectoryNamingContext);
                    DirectoryEntry directoryEntry = new DirectoryEntry(str, ActiveDirectoryManager.GetQualifiedUsername(name), password, ActiveDirectoryManager.SecureAuthenticationFlags | ActiveDirectoryManager.FastAuthenticationFlags);
                    try
                    {
                        object nativeObject = directoryEntry.NativeObject;
                        DirectorySearcher directorySearcher = new DirectorySearcher(directoryEntry);
                        directorySearcher.Filter = string.Format("(SAMAccountName={0})", ActiveDirectoryManager.GetUsername(name));
                        directorySearcher.PropertiesToLoad.Add("cn");
                        SearchResult searchResult = directorySearcher.FindOne();
                        flag = (searchResult != null ? true : false);
                    }
                    catch (Exception exception1)
                    {
                        Exception exception = exception1;
                        if (!exception.Message.Contains("unknown user name or bad password"))
                        {
                            throw new Exception(string.Concat("Error authenticating user. ", exception.Message));
                        }
                        else
                        {
                            throw;
                        }
                    }
                    return flag;
                }
            }
        }
        

        public bool Authenticate(object credentials, out IIdentity identity)
        {
            Logger.Write("Authenticating based on credentials type: " + credentials.GetType(), "Info", 1, -1, Severity.Information);
            ArgumentValidation.CheckForNullReference(credentials, "credentials");
            if (!(credentials is NamePasswordCredential))
            {
                throw new ArgumentException("Passed in credentials object was not the expected type [NamePasswordCredential].");
            }
            if (!Authenticate((NamePasswordCredential) credentials))
            {
                identity = null;
                if (((NamePasswordCredential) credentials).Name != null)
                {
                    Logger.Write("Authentication failed for " + ((NamePasswordCredential) credentials).Name, "Warning", 1, -1, Severity.Warning);
                }
                else
                {
                    Logger.Write("Authentication failed for [NULL name].", "Warning", 1, -1, Severity.Warning);
                }
                return false;
            }
            string domain = ActiveDirectoryManager.GetDomain();
            identity = new GenericIdentity(domain + "/" + ActiveDirectoryManager.GetUsername(((NamePasswordCredential) credentials).Name), this.config.Name);
            Logger.Write("Identity name " + identity.Name + "successfully authenticated.", "Info", 1, -1, Severity.Information);
            return true;
        }

        public void Initialize(ConfigurationBase baseConfig)
        {
            this.config = (LDAPAuthenticationProviderData) baseConfig;
        }

        public string ConfigurationName
        {
            get
            {
                return this.configurationName;
            }
            set
            {
                this.configurationName = value;
            }
        }
    }
}

