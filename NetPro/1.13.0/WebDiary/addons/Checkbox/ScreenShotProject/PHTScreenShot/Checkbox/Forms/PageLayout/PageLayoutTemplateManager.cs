﻿namespace Checkbox.Forms.PageLayout
{
    using Checkbox.Common;
    using Checkbox.Forms.PageLayout.Configuration;
    using Prezza.Framework.Common;
    using Prezza.Framework.Data;
    using System;
    using System.Collections.ObjectModel;
    using System.Data;

    public static class PageLayoutTemplateManager
    {
        public static PageLayoutTemplateData CreatePageLayoutTemplateData(string typeName)
        {
            ArgumentValidation.CheckForEmptyString(typeName, "Page layout type name");
            DataRow[] rowArray = GetTypeNamesDataTable().Select("LayoutTemplateTypeName = '" + typeName.Replace("'", "''") + "'", null, DataViewRowState.CurrentRows);
            if (rowArray.Length <= 0)
            {
                return null;
            }
            string str = DbUtility.GetValueFromDataRow<string>(rowArray[0], "TemplateAssembly", string.Empty);
            string str2 = DbUtility.GetValueFromDataRow<string>(rowArray[0], "TemplateClassName", string.Empty);
            if (Utilities.IsNullOrEmpty(str) || Utilities.IsNullOrEmpty(str2))
            {
                throw new Exception("Unable to get type information for layout template name: " + typeName);
            }
            return PageLayoutConfigurationFactory.CreatePageLayoutTemplateData(str2 + "," + str);
        }

        public static PageLayoutTemplateData GetPageLayoutTemplateData(int layoutTemplateID)
        {
            string pageLayoutTemplateTypeName = GetPageLayoutTemplateTypeName(layoutTemplateID);
            if (Utilities.IsNotNullOrEmpty(pageLayoutTemplateTypeName))
            {
                PageLayoutTemplateData data = CreatePageLayoutTemplateData(pageLayoutTemplateTypeName);
                data.SetIdentity(layoutTemplateID);
                data.Load();
                return data;
            }
            return null;
        }

        public static string GetPageLayoutTemplateTypeName(int layoutTemplateID)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_LayoutTemplate_Get");
            storedProcCommandWrapper.AddInParameter("LayoutTemplateID", DbType.Int32, layoutTemplateID);
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    if (reader.Read())
                    {
                        return DbUtility.GetValueFromDataReader<string>(reader, "LayoutTemplateTypeName", null);
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return null;
        }

        public static ReadOnlyCollection<string> GetTypeNames()
        {
            return new ReadOnlyCollection<string>(DbUtility.ListDataColumnValues<string>(GetTypeNamesDataTable(), "LayoutTemplateTypeName", null, null, true));
        }

        private static DataTable GetTypeNamesDataTable()
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_LayoutTemplate_ListTypes");
            return database.ExecuteDataSet(storedProcCommandWrapper).Tables[0];
        }
    }
}

