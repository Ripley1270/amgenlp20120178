﻿namespace Checkbox.Forms.Logic
{
    using Checkbox.Common;
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class SelectOperandValue : GroupOperandValue<int>
    {
        private Dictionary<int, string> _textValues;

        public override int CompareTo(OperandValue other)
        {
            if (((other.Value != null) && other.HasValue) && Utilities.IsNumeric(other.Value.ToString()))
            {
                if (((other.DataType & OperandDataType.Option) == OperandDataType.Option) && Utilities.IsInt32(other.Value.ToString()))
                {
                    return this.CompareTo(Utilities.GetInt32(other.Value.ToString()).Value);
                }
                double? nullable = Utilities.AsDouble(this.GetValueTextsString());
                double? nullable2 = Utilities.AsDouble(other.Value.ToString());
                if (nullable.HasValue && nullable2.HasValue)
                {
                    return nullable.Value.CompareTo(nullable2.Value);
                }
            }
            return base.CompareTo(other);
        }

        protected virtual int CompareTo(int other)
        {
            if (!base.Values.Contains(other))
            {
                return 1;
            }
            return 0;
        }

        protected override int CompareTo(GroupOperandValue<int> other)
        {
            throw new NotImplementedException("Option list to option list comparison not supported.");
        }

        public override bool Contains(OperandValue otherOperandValue)
        {
            return ((this.HasValue && otherOperandValue.HasValue) && this.GetValueTextsString().ToLower().Contains(otherOperandValue.Value.ToString().ToLower()));
        }

        protected override OperandDataType GetDataType()
        {
            return (base.GetDataType() | OperandDataType.Option);
        }

        protected string GetValueTextsString()
        {
            StringBuilder builder = new StringBuilder();
            int num = 0;
            foreach (int num2 in base.Values)
            {
                if (this.TextValues.ContainsKey(num2))
                {
                    if (num > 0)
                    {
                        builder.Append(",");
                    }
                    builder.Append(this.TextValues[num2]);
                }
            }
            return builder.ToString();
        }

        public Dictionary<int, string> TextValues
        {
            get
            {
                if (this._textValues == null)
                {
                    this._textValues = new Dictionary<int, string>();
                }
                return this._textValues;
            }
        }

        public override object Value
        {
            get
            {
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < base.Values.Count; i++)
                {
                    if (i > 0)
                    {
                        builder.Append(",");
                    }
                    builder.Append(base.Values[i].ToString());
                }
                return builder.ToString();
            }
        }
    }
}

