﻿namespace Checkbox.Analytics.Security
{
    using Prezza.Framework.Security;
    using System;

    [Serializable]
    internal class AnalysisPolicy : Policy
    {
        private static string[] allowablePermissions = new string[] { "Analysis.Edit", "Analysis.Delete", "Analysis.Run" };

        public AnalysisPolicy() : base(allowablePermissions)
        {
        }

        public AnalysisPolicy(string[] permissions) : base(permissions)
        {
        }
    }
}

