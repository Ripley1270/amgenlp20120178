﻿namespace Checkbox.Forms.Items.UI
{
    using System;

    [Serializable]
    public class DropDown : SelectLayout
    {
        public override string AppearanceCode
        {
            get
            {
                return "DROPDOWN_LIST";
            }
        }

        public override Layout LayoutDirection
        {
            get
            {
                return Layout.Vertical;
            }
            set
            {
            }
        }
    }
}

