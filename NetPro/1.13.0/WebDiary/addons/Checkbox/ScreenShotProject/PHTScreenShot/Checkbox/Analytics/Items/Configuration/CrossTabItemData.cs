﻿namespace Checkbox.Analytics.Items.Configuration
{
    using Checkbox.Analytics.Items;
    using Checkbox.Forms.Items;
    using Prezza.Framework.Data;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;

    [Serializable]
    public class CrossTabItemData : AnalysisItemData
    {
        public virtual void AddXAxisItem(int itemID)
        {
            if (this.AxisItemTable.Select("ItemID = " + itemID + " AND Axis = 'x'", null, DataViewRowState.CurrentRows).Length == 0)
            {
                DataRow row = this.AxisItemTable.NewRow();
                row["ItemID"] = itemID;
                row["Axis"] = "x";
                this.AxisItemTable.Rows.Add(row);
                this.AddSourceItem(itemID);
            }
        }

        public virtual void AddYAxisItem(int itemID)
        {
            if (this.AxisItemTable.Select("ItemID = " + itemID + " AND Axis = 'y'", null, DataViewRowState.CurrentRows).Length == 0)
            {
                DataRow row = this.AxisItemTable.NewRow();
                row["ItemID"] = itemID;
                row["Axis"] = "y";
                this.AxisItemTable.Rows.Add(row);
                this.AddSourceItem(itemID);
            }
        }

        protected override void Create(IDbTransaction t)
        {
            base.Create(t);
            if (base.ID <= 0)
            {
                throw new Exception("Unable to save crosstab item.  DataID is <= 0.");
            }
            Database db = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_ItemData_InsertCrosstab");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("UseAliases", DbType.Boolean, base.UseAliases);
            db.ExecuteNonQuery(storedProcCommandWrapper, t);
            this.UpdateDataTables(t);
            db.UpdateDataSet(base.Data, AxisItemTableName, this.GetInsertAxisItemCommand(db), this.GetUpdateAxisItemCommand(db), this.GetDeleteAxisItemCommand(db), t);
        }

        protected override void CreateDataRelationsInternal(DataSet ds)
        {
            base.CreateDataRelationsInternal(ds);
            if (!ds.Relations.Contains(this.DataTableName + "_" + AxisItemTableName))
            {
                DataRelation relation = new DataRelation(this.DataTableName + "_" + AxisItemTableName, ds.Tables[this.DataTableName].Columns[this.IdentityColumnName], ds.Tables[AxisItemTableName].Columns["AnalysisItemID"]);
                ds.Tables[AxisItemTableName].Constraints.Add(new ForeignKeyConstraint(ds.Tables[this.DataTableName].Columns[this.IdentityColumnName], ds.Tables[AxisItemTableName].Columns["AnalysisItemID"]));
                relation.Nested = true;
                ds.Relations.Add(relation);
            }
        }

        protected override Item CreateItem()
        {
            return new CrossTabItem();
        }

        protected override DataSet GetConcreteConfigurationDataSet()
        {
            if (!base.ID.HasValue || (base.ID <= 0))
            {
                throw new Exception("Unable to load crosstab item data, no ID was specified.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_GetCrossTab");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            DataSet dataSet = new DataSet();
            database.LoadDataSet(storedProcCommandWrapper, dataSet, new string[] { this.DataTableName, AxisItemTableName, AnalysisItemData.SourceItemsTableName, AnalysisItemData.ResponseTemplatesTableName });
            return dataSet;
        }

        protected virtual DBCommandWrapper GetDeleteAxisItemCommand(Database db)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_ItemData_CrossTab_DeleteMap");
            storedProcCommandWrapper.AddInParameter("AnalysisItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, "ItemID", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("Axis", DbType.String, "Axis", DataRowVersion.Current);
            return storedProcCommandWrapper;
        }

        protected virtual DBCommandWrapper GetInsertAxisItemCommand(Database db)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_ItemData_CrossTab_InsertMap");
            storedProcCommandWrapper.AddInParameter("AnalysisItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, "ItemID", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("Axis", DbType.String, "Axis", DataRowVersion.Current);
            return storedProcCommandWrapper;
        }

        protected virtual DBCommandWrapper GetUpdateAxisItemCommand(Database db)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_ItemData_CrossTab_UpdateMap");
            storedProcCommandWrapper.AddInParameter("AnalysisItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, "ItemID", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("Axis", DbType.String, "Axis", DataRowVersion.Current);
            return storedProcCommandWrapper;
        }

        protected override void InitializeData()
        {
            base.InitializeData();
            DataTable table = new DataTable();
            table.Columns.Add("AnalysisItemID", typeof(int));
            table.Columns.Add("ItemID", typeof(int));
            table.Columns.Add("Axis", typeof(string));
            table.TableName = AxisItemTableName;
            base.Data.Tables.Add(table);
        }

        public override void Load(DataSet data)
        {
            base.Load(data);
            if (!data.Tables.Contains(AxisItemTableName))
            {
                throw new Exception("Unable to load crosstab item data.  DataSet did not contain axis item data.");
            }
            this.AxisItemTable.Clear();
            foreach (DataRow row in data.Tables[AxisItemTableName].Select("AnalysisItemID = " + base.ID, null, DataViewRowState.CurrentRows))
            {
                this.AxisItemTable.ImportRow(row);
            }
        }

        public override void RemoveSourceItem(int sourceItemID)
        {
            base.RemoveSourceItem(sourceItemID);
            this.RemoveXAxisItem(sourceItemID);
            this.RemoveYAxisItem(sourceItemID);
        }

        public virtual void RemoveXAxisItem(int itemID)
        {
            DataRow[] rowArray = this.AxisItemTable.Select("ItemID = " + itemID + " AND Axis = 'x'", null, DataViewRowState.CurrentRows);
            if (rowArray.Length > 0)
            {
                rowArray[0].Delete();
                this.RemoveSourceItem(itemID);
            }
        }

        public virtual void RemoveYAxisItem(int itemID)
        {
            DataRow[] rowArray = this.AxisItemTable.Select("ItemID = " + itemID + " AND Axis = 'y'", null, DataViewRowState.CurrentRows);
            if (rowArray.Length > 0)
            {
                rowArray[0].Delete();
                this.RemoveSourceItem(itemID);
            }
        }

        protected override void Update(IDbTransaction t)
        {
            base.Update(t);
            if (base.ID <= 0)
            {
                throw new Exception("Unable to update crosstab item.  DataID is <= 0.");
            }
            Database db = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_ItemData_UpdateCrosstab");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("UseAliases", DbType.Boolean, base.UseAliases);
            db.ExecuteNonQuery(storedProcCommandWrapper, t);
            this.UpdateDataTables(t);
            db.UpdateDataSet(base.Data, AxisItemTableName, this.GetInsertAxisItemCommand(db), this.GetUpdateAxisItemCommand(db), this.GetDeleteAxisItemCommand(db), t);
        }

        protected DataTable AxisItemTable
        {
            get
            {
                return base.Data.Tables[AxisItemTableName];
            }
        }

        protected static string AxisItemTableName
        {
            get
            {
                return "AxisItems";
            }
        }

        public override string DataTableName
        {
            get
            {
                return "CrossTabItemData";
            }
        }

        public virtual ReadOnlyCollection<int> XAxisItemIDs
        {
            get
            {
                List<int> list = new List<int>();
                foreach (DataRow row in this.AxisItemTable.Select("Axis = 'x'", null, DataViewRowState.CurrentRows))
                {
                    if (row["ItemID"] != DBNull.Value)
                    {
                        list.Add(Convert.ToInt32(row["ItemID"]));
                    }
                }
                return new ReadOnlyCollection<int>(list);
            }
        }

        public virtual ReadOnlyCollection<int> YAxisItemIDs
        {
            get
            {
                List<int> list = new List<int>();
                foreach (DataRow row in this.AxisItemTable.Select("Axis = 'y'", null, DataViewRowState.CurrentRows))
                {
                    if (row["ItemID"] != DBNull.Value)
                    {
                        list.Add(Convert.ToInt32(row["ItemID"]));
                    }
                }
                return new ReadOnlyCollection<int>(list);
            }
        }
    }
}

