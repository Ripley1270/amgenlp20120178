﻿using Checkbox.Analytics.Items.Configuration;
using Checkbox.Common;
using Checkbox.Forms.Items.Configuration;
using Checkbox.Forms.Items.UI;
using Checkbox.Globalization.Text;
using Checkbox.Progress;
using Prezza.Framework.Data;
using Prezza.Framework.ExceptionHandling;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Xml;

namespace Checkbox.Forms
{
    [Serializable]
    public abstract class Template : AccessControllablePersistedDomainObject
    {
        protected Dictionary<int, int> _itemAppearanceMap;
        protected Dictionary<int, AppearanceData> _itemAppearances;
        private Dictionary<int, ItemData> _itemData;
        private bool _loaded;
        protected DataSet _templateData;
        protected List<TemplatePage> _templatePages;

        public event TemplateUpdated Updated;

        protected Template(string[] supportedPermissionMasks, string[] supportedPermissions) : base(supportedPermissionMasks, supportedPermissions)
        {
            this.InitializeData();
            this.CreatePrimaryKeys(this._templateData);
            base.CreateDataRelations(this._templateData);
        }

        public virtual void AddItemToPage(TemplatePage page, ItemData item)
        {
            this.AddItemToPage(page, item, true);
        }

        protected virtual void AddItemToPage(TemplatePage page, ItemData item, bool addItemToTemplate)
        {
            if ((this.PageItemsTable != null) && page.Identity.HasValue)
            {
                int num = page.Identity.Value;
                int num2 = item.ID.Value;
                if (this.PageItemsTable.Select(string.Concat(new object[] { "PageID = ", num, " AND ItemID = ", num2 }), string.Empty, DataViewRowState.CurrentRows).Length > 0)
                {
                    throw new ArgumentException("This ItemID already exists in this Page");
                }
                if (addItemToTemplate)
                {
                    this.AddItemToTemplate(item);
                }
                DataRow row = this.PageItemsTable.NewRow();
                row["PageID"] = num;
                row["ItemID"] = num2;
                int num3 = this.PageItemsTable.Select("PageID = " + num, string.Empty, DataViewRowState.CurrentRows).Length + 1;
                row["Position"] = num3;
                this.PageItemsTable.Rows.Add(row);
                this.SavePageItemData();
            }
        }

        public virtual void AddItemToPage(TemplatePage page, ItemData item, int position)
        {
            this.AddItemToPage(page, item);
            this.MoveItem(page, item, position);
        }

        private void AddItemToTemplate(ItemData item)
        {
            if ((this.TemplateItemsTable != null) && (this.TemplateItemsTable.Select("ItemID = " + item.ID, null, DataViewRowState.CurrentRows).Length == 0))
            {
                DataRow row = this.TemplateItemsTable.NewRow();
                row["ItemID"] = item.ID;
                this.TemplateItemsTable.Rows.Add(row);
            }
            this.SaveTemplateItemData();
            this._itemData[item.ID.Value] = item;
        }

        public virtual void AddTemplatePage(TemplatePage page)
        {
            if (this._templatePages == null)
            {
                this.GetPages(null);
            }
            page.Parent = this;
            if ((this._templatePages != null) && !this._templatePages.Contains(page))
            {
                this._templatePages.Add(page);
                this.SortTemplatePages();
            }
        }

        public bool ContainsItem(int itemID)
        {
            return this._itemData.ContainsKey(itemID);
        }

        protected override void Create(IDbTransaction t)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Template_Insert");
            storedProcCommandWrapper.AddInParameter("ModifiedDate", DbType.DateTime, base.LastModified);
            DateTime? createdDate = base.CreatedDate;
            storedProcCommandWrapper.AddInParameter("CreatedDate", DbType.DateTime, createdDate.HasValue ? createdDate.GetValueOrDefault() : DateTime.Now);
            storedProcCommandWrapper.AddInParameter("DefaultPolicy", DbType.Int32, base.DefaultPolicyID);
            storedProcCommandWrapper.AddInParameter("AclID", DbType.Int32, base.AclID);
            storedProcCommandWrapper.AddOutParameter("TemplateID", DbType.Int32, 4);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("TemplateID");
            if ((parameterValue == null) || (parameterValue == DBNull.Value))
            {
                throw new Exception("Unable to save template data.");
            }
            base.ID = new int?(Convert.ToInt32(parameterValue));
            this.SaveItemData(t);
        }

        protected override void CreateDataRelationsInternal(DataSet ds)
        {
            if ((ds.Tables.Contains(this.TemplateDataTableName) && ds.Tables.Contains(this.TemplateItemsTableName)) && !ds.Relations.Contains(this.DataTableName + "_" + this.TemplateItemsTableName))
            {
                DataRelation relation = new DataRelation(this.DataTableName + "_" + this.TemplateItemsTableName, ds.Tables[this.TemplateDataTableName].Columns["TemplateID"], ds.Tables[this.TemplateItemsTableName].Columns["TemplateID"]);
                ds.Tables[this.TemplateItemsTableName].Constraints.Add(new ForeignKeyConstraint(ds.Tables[this.TemplateDataTableName].Columns["TemplateID"], ds.Tables[this.TemplateItemsTableName].Columns["TemplateID"]));
                relation.Nested = true;
                ds.Relations.Add(relation);
            }
            if ((ds.Tables.Contains(this.TemplateDataTableName) && ds.Tables.Contains(this.PageDataTableName)) && !ds.Relations.Contains(this.DataTableName + "_" + this.PageDataTableName))
            {
                DataRelation relation2 = new DataRelation(this.DataTableName + "_" + this.PageDataTableName, ds.Tables[this.TemplateDataTableName].Columns["TemplateID"], ds.Tables[this.PageDataTableName].Columns["TemplateID"]);
                ds.Tables[this.PageDataTableName].Constraints.Add(new ForeignKeyConstraint(ds.Tables[this.TemplateDataTableName].Columns["TemplateID"], ds.Tables[this.PageDataTableName].Columns["TemplateID"]));
                relation2.Nested = true;
                ds.Relations.Add(relation2);
            }
            if ((ds.Tables.Contains(this.PageDataTableName) && ds.Tables.Contains(this.PageItemsTableName)) && !ds.Relations.Contains(this.PageDataTableName + "_" + this.PageItemsTableName))
            {
                DataRelation relation3 = new DataRelation(this.PageDataTableName + "_" + this.PageItemsTableName, ds.Tables[this.PageDataTableName].Columns["PageID"], ds.Tables[this.PageItemsTableName].Columns["PageID"]);
                ds.Tables[this.PageItemsTableName].Constraints.Add(new ForeignKeyConstraint(ds.Tables[this.PageDataTableName].Columns["PageID"], ds.Tables[this.PageItemsTableName].Columns["PageID"]));
                ds.Tables[this.PageItemsTableName].Constraints.Add(new ForeignKeyConstraint(ds.Tables[this.TemplateItemsTableName].Columns["ItemID"], ds.Tables[this.PageItemsTableName].Columns["ItemID"]));
                relation3.Nested = true;
                ds.Relations.Add(relation3);
            }
            if ((ds.Tables.Contains(this.ItemAppearanceTableName) && ds.Tables.Contains(this.ItemAppearanceMapTableName)) && !ds.Tables[this.ItemAppearanceMapTableName].Constraints.Contains("FK_" + this.ItemAppearanceTableName + "_" + this.ItemAppearanceMapTableName))
            {
                ds.Tables[this.ItemAppearanceMapTableName].Constraints.Add(new ForeignKeyConstraint("FK_" + this.ItemAppearanceTableName + "_" + this.ItemAppearanceMapTableName, ds.Tables[this.ItemAppearanceTableName].Columns["AppearanceID"], ds.Tables[this.ItemAppearanceMapTableName].Columns["AppearanceID"]));
            }
            if ((ds.Tables.Contains(this.ItemAppearanceTableName) && ds.Tables.Contains(this.ItemAppearanceMapTableName)) && !ds.Relations.Contains(this.ItemAppearanceTableName + "_" + this.ItemAppearanceMapTableName))
            {
                DataRelation relation4 = new DataRelation(this.ItemAppearanceTableName + "_" + this.ItemAppearanceMapTableName, ds.Tables[this.ItemAppearanceTableName].Columns["AppearanceID"], ds.Tables[this.ItemAppearanceMapTableName].Columns["AppearanceID"]);
                ds.Tables[this.ItemAppearanceMapTableName].Constraints.Add(new ForeignKeyConstraint(ds.Tables[this.TemplateItemsTableName].Columns["ItemID"], ds.Tables[this.ItemAppearanceMapTableName].Columns["ItemID"]));
                ds.Relations.Add(relation4);
            }
            if ((ds.Tables.Contains(this.TemplateDataTableName) && ds.Tables.Contains(this.TemplateTextTableName)) && !ds.Tables[this.TemplateTextTableName].Constraints.Contains("FK_" + this.TemplateDataTableName + "_" + this.TemplateTextTableName))
            {
                DataRelation relation5 = new DataRelation(this.TemplateDataTableName + "_" + this.TemplateTextTableName, ds.Tables[this.TemplateDataTableName].Columns["TemplateID"], ds.Tables[this.TemplateTextTableName].Columns["TemplateID"]);
                ds.Tables[this.TemplateTextTableName].Constraints.Add(new ForeignKeyConstraint("FK_" + this.TemplateDataTableName + "_" + this.TemplateTextTableName, ds.Tables[this.TemplateDataTableName].Columns["TemplateID"], ds.Tables[this.TemplateTextTableName].Columns["TemplateID"]));
                relation5.Nested = true;
                ds.Relations.Add(relation5);
            }
            if (ds.Tables.Contains(this.TemplateItemsTableName) && (ds.Tables[this.ItemTextTableName] != null))
            {
                string name = this.TemplateItemsTableName + "_" + this.ItemTextTableName;
                if (!ds.Tables[this.ItemTextTableName].Constraints.Contains(name))
                {
                    ds.Tables[this.ItemTextTableName].Constraints.Add(new ForeignKeyConstraint(name, ds.Tables[this.TemplateItemsTableName].Columns["ItemID"], ds.Tables[this.ItemTextTableName].Columns["ItemID"]));
                }
            }
            foreach (ItemData data in this._itemData.Values)
            {
                data.CreateDataRelations(this._templateData);
            }
        }

        public virtual void CreatePrimaryKeys(DataSet templateData)
        {
            if (((this.PageDataTable != null) && this.PageDataTable.Columns.Contains("PageID")) && ((this.PageDataTable.PrimaryKey == null) || (this.PageDataTable.PrimaryKey.Length == 0)))
            {
                this.PageDataTable.PrimaryKey = new DataColumn[] { this.PageDataTable.Columns["PageID"] };
                this.PageDataTable.Columns["PageID"].AutoIncrement = true;
                this.PageDataTable.Columns["PageID"].AutoIncrementSeed = -1L;
                this.PageDataTable.Columns["PageID"].AutoIncrementStep = -1L;
            }
            if (((this.TemplateItemsTable != null) && this.TemplateItemsTable.Columns.Contains("ItemID")) && ((this.TemplateItemsTable.PrimaryKey == null) || (this.TemplateItemsTable.PrimaryKey.Length == 0)))
            {
                this.TemplateItemsTable.PrimaryKey = new DataColumn[] { this.TemplateItemsTable.Columns["ItemID"] };
            }
        }

        public override void Delete(IDbTransaction t)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Template_Delete");
            storedProcCommandWrapper.AddInParameter("TemplateID", DbType.Int32, base.ID.Value);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        public virtual void DeleteItemFromPage(TemplatePage page, ItemData item)
        {
            this.RemoveItemFromPage(page, item);
            this.RemoveItemFromTemplate(item);
        }

        public virtual void DeletePage(TemplatePage page)
        {
            if (page.Identity.HasValue && (this.PageDataTable != null))
            {
                this.RemoveTemplatePageItems(page);
                int num = page.Identity.Value;
                DataRow row = this.PageDataTable.Select("PageID = " + num, string.Empty, DataViewRowState.CurrentRows)[0];
                foreach (DataRow row2 in this.PageDataTable.Select("PagePosition > " + row["PagePosition"], "PagePosition ASC", DataViewRowState.CurrentRows))
                {
                    row2["PagePosition"] = ((int) row2["PagePosition"]) - 1;
                }
                row.Delete();
                try
                {
                    using (IDbConnection connection = DatabaseFactory.CreateDatabase().GetConnection())
                    {
                        connection.Open();
                        IDbTransaction transaction = connection.BeginTransaction();
                        try
                        {
                            this.SavePageData(transaction);
                            transaction.Commit();
                            this.RemoveTemplatePage(page);
                        }
                        catch (Exception exception)
                        {
                            ExceptionPolicy.HandleException(exception, "BusinessProtected");
                            transaction.Rollback();
                            throw new Exception("Unable to save data.");
                        }
                        finally
                        {
                            connection.Close();
                        }
                    }
                }
                catch (Exception exception2)
                {
                    if (ExceptionPolicy.HandleException(exception2, "BusinessProtected"))
                    {
                        throw;
                    }
                }
            }
        }

        public virtual void DeletePageAt(int position)
        {
            List<TemplatePage> pages = this.GetPages("PagePosition = " + position);
            if (pages.Count > 0)
            {
                this.DeletePage(pages[0]);
            }
        }

        //public virtual void EnsureItemsLoaded()
        //{
        //    if ((this._templatePages != null) && (this._itemData != null))
        //    {
        //        foreach (int num in from templatePage in this._templatePages select templatePage.ItemIds)
        //        {
        //            this.GetItem(num);
        //        }
        //    }
        //}

        public virtual void EnsureItemsLoaded()
        {
            if (this._templatePages == null || this._itemData == null)
            {
                return;
            }
            else
            {
                List<TemplatePage> templatePages = this._templatePages;
                foreach (int num in templatePages.SelectMany<TemplatePage, int>((TemplatePage templatePage) => templatePage.ItemIds))
                {
                    this.GetItem(num);
                }
                return;
            }
        }

        private static ItemData FindChildItem(int idOfItemToFind, ICompositeItemData container)
        {
            foreach (ItemData data in container.GetChildItemDatas())
            {
                if (data is ICompositeItemData)
                {
                    return FindChildItem(idOfItemToFind, (ICompositeItemData) data);
                }
                if (data.ID.HasValue && (data.ID.Value == idOfItemToFind))
                {
                    return data;
                }
            }
            return null;
        }

        protected virtual DBCommandWrapper GetAddItemToPageCommand()
        {
            DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_TemplatePage_AddItem");
            storedProcCommandWrapper.AddInParameter("PageID", DbType.Int32, "PageID", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, "ItemID", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("Position", DbType.Int32, "Position", DataRowVersion.Current);
            return storedProcCommandWrapper;
        }

        protected virtual DBCommandWrapper GetAddTemplateItemCommand()
        {
            DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_Template_AddItem");
            storedProcCommandWrapper.AddInParameter("TemplateID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, "ItemID", DataRowVersion.Current);
            return storedProcCommandWrapper;
        }

        protected virtual DBCommandWrapper GetDeleteItemFromPageCommand()
        {
            DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_TemplatePage_DeleteItem");
            storedProcCommandWrapper.AddInParameter("PageID", DbType.Int32, "PageID", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, "ItemID", DataRowVersion.Current);
            return storedProcCommandWrapper;
        }

        protected virtual DBCommandWrapper GetDeleteItemFromTemplateCommand()
        {
            DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_Template_DeleteItem");
            storedProcCommandWrapper.AddInParameter("TemplateID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, "ItemID", DataRowVersion.Current);
            return storedProcCommandWrapper;
        }

        protected virtual DBCommandWrapper GetDeletePageCommand()
        {
            DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_Template_DeletePage");
            storedProcCommandWrapper.AddInParameter("TemplateID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("PageID", DbType.Int32, "PageID", DataRowVersion.Current);
            return storedProcCommandWrapper;
        }

        private DBCommandWrapper GetInsertPageCommand()
        {
            DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_Template_InsertPage");
            storedProcCommandWrapper.AddInParameter("TemplateID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("PagePosition", DbType.Int32, "PagePosition", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("LayoutTemplateID", DbType.Int32, "LayoutTemplateID", DataRowVersion.Current);
            storedProcCommandWrapper.AddParameter("PageID", DbType.Int32, ParameterDirection.Output, "PageID", DataRowVersion.Current, null);
            return storedProcCommandWrapper;
        }

        public virtual ItemData GetItem(int itemDataID)
        {
            if (this._itemData.ContainsKey(itemDataID))
            {
                if (this._itemData[itemDataID] == null)
                {
                    this._itemData[itemDataID] = ItemConfigurationManager.GetConfigurationData(itemDataID);
                }
                return this._itemData[itemDataID];
            }
            foreach (ItemData data in this._itemData.Values)
            {
                if (data is ICompositeItemData)
                {
                    ItemData data2 = FindChildItem(itemDataID, (ICompositeItemData) data);
                    if (data2 != null)
                    {
                        return data2;
                    }
                }
            }
            return null;
        }

        public virtual int? GetItemPositionWithinPage(ItemData item)
        {
            if (this.PageItemsTable != null)
            {
                DataRow[] rowArray = this.PageItemsTable.Select("ItemID = " + item.ID.Value, string.Empty, DataViewRowState.CurrentRows);
                if ((rowArray.Length > 0) && (rowArray[0]["Position"] != DBNull.Value))
                {
                    return new int?((int) rowArray[0]["Position"]);
                }
            }
            return null;
        }

        public virtual TemplatePage GetPage(int id)
        {
            if (this.PageDataTable == null)
            {
                return null;
            }
            if (this.PageDataTable.Select("PageID=" + id).Length == 0)
            {
                return null;
            }
            return this.GetPages("PageID=" + id)[0];
        }

        public virtual int? GetPageIDAtPosition(int position)
        {
            if (this.PageDataTable != null)
            {
                DataRow[] rowArray = this.PageDataTable.Select("PagePosition = " + position, string.Empty, DataViewRowState.CurrentRows);
                if (rowArray.Length > 0)
                {
                    return new int?(Convert.ToInt32(rowArray[0]["PageID"]));
                }
            }
            return null;
        }

        internal virtual List<int> GetPageItemDataIDs(int pageId)
        {
            List<int> list = new List<int>();
            if (this.PageItemsTable != null)
            {
                foreach (DataRow row in this.PageItemsTable.Select("PageID = " + pageId, "Position ASC", DataViewRowState.CurrentRows))
                {
                    list.Add((int) row["ItemID"]);
                }
            }
            return list;
        }

        public virtual int? GetPagePositionForItem(ItemData item)
        {
            if (this.PageItemsTable != null)
            {
                DataRow[] rowArray = this.PageItemsTable.Select("ItemID = " + item.ID.Value, string.Empty, DataViewRowState.CurrentRows);
                if (rowArray.Length > 0)
                {
                    int num = (int) rowArray[0]["PageID"];
                    DataRow[] rowArray2 = this.PageDataTable.Select("PageID = " + num, string.Empty, DataViewRowState.CurrentRows);
                    if (rowArray2.Length > 0)
                    {
                        return new int?((int) rowArray2[0]["PagePosition"]);
                    }
                }
                else
                {
                    foreach (ItemData data in this.Items)
                    {
                        if ((data is ICompositeItemData) && (FindChildItem(item.ID.Value, (ICompositeItemData) data) != null))
                        {
                            return this.GetPagePositionForItem(data);
                        }
                    }
                }
            }
            return null;
        }

        protected virtual List<TemplatePage> GetPages(string filter)
        {
            List<TemplatePage> collection = new List<TemplatePage>();
            if ((this._templatePages != null) && ((filter == null) || (filter.Trim() == string.Empty)))
            {
                return this._templatePages;
            }
            if (this.PageDataTable != null)
            {
                foreach (DataRow row in this.PageDataTable.Select(filter, "PagePosition ASC", DataViewRowState.CurrentRows))
                {
                    TemplatePage page2 = new TemplatePage((int) row["PagePosition"]) {
                        Parent = this,
                        Identity = new int?((int) row["PageID"])
                    };
                    int? defaultValue = null;
                    page2.LayoutTemplateID = DbUtility.GetValueFromDataRow<int?>(row, "LayoutTemplateID", defaultValue);
                    TemplatePage item = page2;
                    collection.Add(item);
                }
            }
            if ((this._templatePages == null) && ((filter == null) || (filter.Trim() == string.Empty)))
            {
                this._templatePages = new List<TemplatePage>(collection);
            }
            return collection;
        }

        protected virtual DBCommandWrapper GetSetPageItemPositionCommand()
        {
            DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_TemplatePage_SetItemPos");
            storedProcCommandWrapper.AddInParameter("PageID", DbType.Int32, "PageID", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, "ItemID", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("Position", DbType.Int32, "Position", DataRowVersion.Current);
            return storedProcCommandWrapper;
        }

        public DataTable GetTextData()
        {
            return this.TemplateTextTable.Copy();
        }

        protected virtual DBCommandWrapper GetUpdatePageCommand()
        {
            DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_Template_UpdatePage");
            storedProcCommandWrapper.AddInParameter("TemplateID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("PageID", DbType.Int32, "PageID", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("PagePosition", DbType.Int32, "PagePosition", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("LayoutTemplateID", DbType.Int32, "LayoutTemplateID", DataRowVersion.Current);
            return storedProcCommandWrapper;
        }

        public override void Import(DataSet ds)
        {
            this.Import(ds, string.Empty, string.Empty);
        }

        public void Import(DataSet ds, string progressKey, string progressLanguage)
        {
            this.ValidateDataSetForImport(ds);
            base.NegateRelationalIDs(ds);
            DataRow[] rowArray = ds.Tables[this.TemplateDataTableName].Select(null, null, DataViewRowState.CurrentRows);
            if (rowArray.Length > 0)
            {
                rowArray[0]["TemplateID"] = base.ID;
                rowArray[0][this.IdentityColumnName] = base.ID;
            }
            ProgressProvider.SetProgress(progressKey, TextManager.GetText("/controlText/templateImport/importingTemplateData", progressLanguage), 5);
            this.ImportTemplateData(ds);
            this.ImportTemplateItems(ds, progressKey, progressLanguage, 0x37, 60);
            this.ImportTemplatePages(ds, progressKey, progressLanguage, 15, 0x4b);
            ProgressProvider.SetProgress(progressKey, TextManager.GetText("/controlText/templateImport/importingPageItems", progressLanguage), 0x4b);
            this.ImportTemplatePageItems(ds);
            this.ImportItemText(ds, progressKey, progressLanguage, 10, 0x55);
            ProgressProvider.SetProgress(progressKey, TextManager.GetText("/controlText/templateImport/importingTemplateText", progressLanguage), 0x55);
            this.ImportTemplateText(ds);
        }

        protected virtual ItemData ImportItem(int itemID, DataSet ds)
        {
            ItemData item = ItemConfigurationManager.ImportItem(itemID, ds, true);
            if (item != null)
            {
                this._itemData[item.ID.Value] = item;
                this.AddItemToTemplate(item);
            }
            return item;
        }

        protected virtual void ImportItemText(DataSet ds, string progressKey, string progressLangauge, int magnitude, int completedPercent)
        {
            if ((this.ItemTextTable != null) && ds.Tables.Contains(this.ItemTextTableName))
            {
                string text = TextManager.GetText("/controlText/templateImport/importingItemText", progressLangauge);
                int count = this.ItemTextTable.Rows.Count;
                int currentCounterItem = 1;
                foreach (DataRow row in ds.Tables[this.ItemTextTableName].Rows)
                {
                    ProgressProvider.SetProgressCounter(progressKey, text, currentCounterItem, count, magnitude, completedPercent);
                    if (((row["ComputedTextID"] != DBNull.Value) && (row["LanguageCode"] != DBNull.Value)) && (row["TextValue"] != DBNull.Value))
                    {
                        TextManager.SetText((string) row["ComputedTextID"], (string) row["LanguageCode"], (string) row["TextValue"]);
                    }
                    this.ItemTextTable.ImportRow(row);
                    currentCounterItem++;
                }
            }
        }

        protected virtual void ImportTemplateData(DataSet ds)
        {
        }

        protected virtual void ImportTemplateItems(DataSet ds, string progressKey, string progressLanguage, int magnitude, int completedPercent)
        {
            if (((this.TemplateItemsTable != null) && ds.Tables.Contains(this.TemplateItemsTableName)) && ds.Tables.Contains(this.PageItemsTableName))
            {
                if (ds.Tables.Contains(this.ItemAppearanceTableName) && !ds.Tables[this.ItemAppearanceTableName].Columns.Contains("ItemID"))
                {
                    ds.Tables[this.ItemAppearanceTableName].Columns.Add("ItemID", typeof(int));
                }
                string text = TextManager.GetText("/controlText/templateImport/importingItems", progressLanguage);
                int count = ds.Tables[this.TemplateItemsTableName].Rows.Count;
                int currentCounterItem = 1;
                foreach (DataRow row in ds.Tables[this.PageDataTableName].Select(string.Empty, "PagePosition ASC"))
                {
                    foreach (DataRow row2 in ds.Tables[this.PageItemsTableName].Select("PageID=" + row["PageID"], "Position ASC"))
                    {
                        int itemID = Convert.ToInt32(row2["ItemID"]);
                        if (ds.Tables[this.PageItemsTableName].Select("ItemID = " + itemID).Length > 0)
                        {
                            if (ds.Tables.Contains(this.ItemAppearanceMapTableName) && ds.Tables.Contains(this.ItemAppearanceTableName))
                            {
                                DataRow[] rowArray3 = ds.Tables[this.ItemAppearanceMapTableName].Select("ItemID = " + itemID);
                                if ((rowArray3.Length > 0) && (rowArray3[0]["AppearanceID"] != DBNull.Value))
                                {
                                    DataRow[] rowArray4 = ds.Tables[this.ItemAppearanceTableName].Select("AppearanceID = " + rowArray3[0]["AppearanceID"]);
                                    if (rowArray4.Length > 0)
                                    {
                                        rowArray4[0]["ItemID"] = itemID;
                                    }
                                }
                            }
                            ProgressProvider.SetProgressCounter(progressKey, text, currentCounterItem, count, magnitude, completedPercent);
                            ItemData data = this.ImportItem(itemID, ds);
                            if (data != null)
                            {
                                DataRow[] rowArray5 = ds.Tables[this.TemplateItemsTableName].Select("ItemID=" + itemID);
                                if (rowArray5.Length > 0)
                                {
                                    rowArray5[0]["ItemID"] = data.ID;
                                }
                            }
                            currentCounterItem++;
                        }
                    }
                }
            }
        }

        protected virtual void ImportTemplatePageItems(DataSet ds)
        {
            if ((this.PageItemsTable != null) && ds.Tables.Contains(this.PageItemsTableName))
            {
                this.PageItemsTable.Rows.Clear();
                foreach (DataRow row in ds.Tables[this.PageItemsTableName].Select(null, "Position ASC", DataViewRowState.CurrentRows))
                {
                    int num = Convert.ToInt32(row["PageID"]);
                    int key = Convert.ToInt32(row["ItemID"]);
                    foreach (TemplatePage page in this._templatePages)
                    {
                        if ((page.Identity.Value == num) && this._itemData.ContainsKey(key))
                        {
                            this.AddItemToPage(page, this.GetItem(key));
                        }
                    }
                }
            }
        }

        protected virtual void ImportTemplatePages(DataSet ds, string progressKey, string progressLanguage, int magnitude, int completedPercent)
        {
            if ((this.PageDataTable != null) && ds.Tables.Contains(this.PageDataTableName))
            {
                this.PageDataTable.Rows.Clear();
                string text = TextManager.GetText("/controlText/templateImport/importingPages", progressLanguage);
                int count = ds.Tables[this.PageDataTableName].Rows.Count;
                int currentCounterItem = 1;
                foreach (DataRow row in ds.Tables[this.PageDataTableName].Rows)
                {
                    ProgressProvider.SetProgressCounter(progressKey, text, currentCounterItem, count, magnitude, completedPercent);
                    int position = Convert.ToInt32(row["PagePosition"]);
                    TemplatePage page = this.NewPage(position);
                    row["PageID"] = page.GetID;
                    currentCounterItem++;
                }
            }
        }

        protected virtual void ImportTemplateText(DataSet ds)
        {
            if ((this.TemplateTextTable != null) && ds.Tables.Contains(this.TemplateTextTableName))
            {
                foreach (DataRow row in ds.Tables[this.TemplateTextTableName].Rows)
                {
                    if (((row["ComputedTextID"] != DBNull.Value) && (row["LanguageCode"] != DBNull.Value)) && (row["TextValue"] != DBNull.Value))
                    {
                        TextManager.SetText((string) row["ComputedTextID"], (string) row["LanguageCode"], (string) row["TextValue"]);
                    }
                    this.TemplateTextTable.ImportRow(row);
                }
            }
        }

        protected virtual void InitializeData()
        {
            this._itemData = new Dictionary<int, ItemData>();
            this._itemAppearances = new Dictionary<int, AppearanceData>();
            this._itemAppearanceMap = new Dictionary<int, int>();
            this._templateData = new DataSet();
            this.InitializeTemplateData();
            this.InitializeItemData();
            this.InitializePageData();
            this.InitializeTextData();
        }

        protected virtual void InitializeItemData()
        {
            DataTable table = new DataTable();
            table.Columns.Add("TemplateID", typeof(int));
            table.Columns.Add("ItemID", typeof(int));
            table.Columns.Add("ItemDataClassName", typeof(string));
            table.Columns.Add("ItemDataAssemblyName", typeof(string));
            table.Columns.Add("ItemName", typeof(string));
            table.Columns.Add("ItemTypeID", typeof(int));
            table.TableName = this.TemplateItemsTableName;
            DataTable table2 = new DataTable();
            table2.Columns.Add("ItemID", typeof(int));
            table2.Columns.Add("AppearanceID", typeof(int));
            table2.TableName = this.ItemAppearanceMapTableName;
            DataTable table3 = new DataTable();
            table3.Columns.Add("AppearanceID", typeof(int));
            table3.Columns.Add("AppearanceCode", typeof(string));
            table3.Columns.Add("LayoutStyle", typeof(string));
            table3.Columns.Add("Columns", typeof(int));
            table3.Columns.Add("Width", typeof(int));
            table3.Columns.Add("Height", typeof(int));
            table3.Columns.Add("ShowNumberLabels", typeof(bool));
            table3.Columns.Add("FontColor", typeof(string));
            table3.Columns.Add("FontSize", typeof(string));
            table3.Columns.Add("ItemPosition", typeof(string));
            table3.Columns.Add("Rows", typeof(int));
            table3.Columns.Add("LabelPosition", typeof(string));
            table3.Columns.Add("UseAliases", typeof(bool));
            table3.Columns.Add("GraphType", typeof(string));
            table3.TableName = this.ItemAppearanceTableName;
            this._templateData.Tables.Add(table);
            this._templateData.Tables.Add(table3);
            this._templateData.Tables.Add(table2);
        }

        protected virtual void InitializePageData()
        {
            DataTable table = new DataTable();
            table.Columns.Add("TemplateID", typeof(int));
            table.Columns.Add("PageID", typeof(int));
            table.Columns.Add("PagePosition", typeof(int));
            table.Columns.Add("RandomizeItems", typeof(int));
            table.Columns.Add("LayoutTemplateID", typeof(int));
            table.TableName = this.PageDataTableName;
            DataTable table2 = new DataTable();
            table2.Columns.Add("PageID", typeof(int));
            table2.Columns.Add("ItemID", typeof(int));
            table2.Columns.Add("Position", typeof(int));
            table2.TableName = this.PageItemsTableName;
            this._templateData.Tables.Add(table);
            this._templateData.Tables.Add(table2);
        }

        protected virtual void InitializeTemplateData()
        {
            DataTable table = new DataTable {
                TableName = this.TemplateDataTableName
            };
            table.Columns.Add("TemplateID", typeof(int));
            table.Columns.Add("ModifiedDate", typeof(DateTime));
            table.Columns.Add("Deleted", typeof(bool));
            table.Columns.Add("DefaultPolicy", typeof(int));
            table.Columns.Add("AclID", typeof(int));
            table.Columns.Add("CreatedDate", typeof(DateTime));
            table.Columns.Add("CreatedBy", typeof(string));
            this._templateData.Tables.Add(table);
        }

        protected virtual void InitializeTextData()
        {
            DataTable table = new DataTable {
                TableName = this.ItemTextTableName
            };
            table.Columns.Add("ItemID", typeof(int));
            table.Columns.Add("TextIDSuffix", typeof(string));
            table.Columns.Add("TextIDPrefix", typeof(string));
            table.Columns.Add("ComputedTextID", typeof(string));
            table.Columns.Add("TextID", typeof(string));
            table.Columns.Add("LanguageCode", typeof(string));
            table.Columns.Add("TextValue", typeof(string));
            table.Columns["ComputedTextID"].Expression = "Iif(ItemID IS NULL OR TextIDSuffix IS NULL OR TextIDPrefix IS NULL, '', '/' + TextIDPrefix + '/' + ItemID + '/' + TextIDSuffix)";
            DataTable table2 = new DataTable {
                TableName = this.TemplateTextTableName
            };
            table2.Columns.Add("TemplateID", typeof(int));
            table2.Columns.Add("TextIDSuffix", typeof(string));
            table2.Columns.Add("ComputedTextID", typeof(string));
            table2.Columns.Add("TextID", typeof(string));
            table2.Columns.Add("LanguageCode", typeof(string));
            table2.Columns.Add("TextValue", typeof(string));
            table2.Columns["ComputedTextID"].Expression = "Iif(TemplateID IS NULL OR TextIDSuffix IS NULL, '', '/" + this.TextIDPrefix + "/' + TemplateID + '/' + TextIDSuffix)";
            this._templateData.Tables.Add(table);
            this._templateData.Tables.Add(table2);
        }

        public void ItemSaved(ItemData itemData, IDbTransaction transaction)
        {
            this.OnItemSaved(itemData, transaction);
        }

        public override void Load()
        {
            try
            {
                if (base.ID <= 0)
                {
                    throw new Exception("DataID must be greater than zero.");
                }
                if (!this.Loaded)
                {
                    this._templateData = this.GetConfigurationDataSet();
                }
                this.Load(this._templateData);
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
            }
        }

        public override void Load(DataSet ds)
        {
            try
            {
                if (ds != null)
                {
                    this.CreatePrimaryKeys(ds);
                    base.CreateDataRelations(ds);
                    this.LoadTemplateData(ds);
                    this.LoadTemplateItems(ds);
                    this.LoadTemplateTextData(ds);
                    this._loaded = true;
                }
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
            }
        }

        protected virtual void LoadTemplateData(DataSet ds)
        {
        }

        protected virtual void LoadTemplateItems(DataSet ds)
        {
            if (((this.PageItemsTable != null) && ds.Tables.Contains(this.PageItemsTableName)) && ((this.TemplateItemsTable != null) && ds.Tables.Contains(this.TemplateItemsTableName)))
            {
                foreach (DataRow row in ds.Tables[this.PageItemsTableName].Select(null, null, DataViewRowState.CurrentRows))
                {
                    int? defaultValue = null;
                    int? nullable = DbUtility.GetValueFromDataRow<int?>(row, "ItemID", defaultValue);
                    if (nullable.HasValue && (ds.Tables[this.TemplateItemsTableName].Select("ItemID = " + nullable, null, DataViewRowState.CurrentRows).Length > 0))
                    {
                        this._itemData[nullable.Value] = null;
                    }
                }
            }
        }

        protected virtual void LoadTemplatePageItems(DataSet ds)
        {
            if ((this.PageItemsTable != null) && ds.Tables.Contains(this.PageItemsTableName))
            {
                this.PageItemsTable.Rows.Clear();
                foreach (DataRow row in ds.Tables[this.PageItemsTableName].Rows)
                {
                    this.PageItemsTable.ImportRow(row);
                }
            }
        }

        protected virtual void LoadTemplatePages(DataSet ds)
        {
            if ((this.PageDataTable != null) && ds.Tables.Contains(this.PageDataTableName))
            {
                this.PageDataTable.Rows.Clear();
                foreach (DataRow row in ds.Tables[this.PageDataTableName].Rows)
                {
                    this.PageDataTable.ImportRow(row);
                }
            }
        }

        protected virtual void LoadTemplateTextData(DataSet ds)
        {
        }

        protected virtual DataSet MergeDataForExport()
        {
            DataSet set3;
            DataSet ds = this._templateData.Copy();
            try
            {
                if (ds.Tables.Contains(this.ItemAppearanceTableName) && (ds.Tables[this.ItemAppearanceTableName].PrimaryKey.Length == 0))
                {
                    ds.Tables[this.ItemAppearanceTableName].PrimaryKey = new DataColumn[] { ds.Tables[this.ItemAppearanceTableName].Columns["AppearanceID"] };
                }
                if (ds.Tables.Contains("AnalysisItemAppearanceData") && (ds.Tables["AnalysisItemAppearanceData"].PrimaryKey.Length == 0))
                {
                    ds.Tables["AnalysisItemAppearanceData"].PrimaryKey = new DataColumn[] { ds.Tables["AnalysisItemAppearanceData"].Columns["AppearanceID"] };
                }
                List<int> list = new List<int>(this._itemData.Keys);
                foreach (int num in list)
                {
                    ItemData configurationData = ItemConfigurationManager.GetConfigurationData(num, ds);
                    if (configurationData != null)
                    {
                        if (configurationData is LocalizableResponseItemData)
                        {
                            ds.Merge(((LocalizableResponseItemData) configurationData).LoadTextData());
                        }
                        configurationData.CreateDataRelations(ds);
                        if (configurationData is LocalizableResponseItemData)
                        {
                            ((LocalizableResponseItemData) configurationData).CreateTextDataRelations(ds);
                        }
                        if (configurationData is AnalysisItemData)
                        {
                            DataSet exportAppearance = ((AnalysisItemData) configurationData).GetExportAppearance();
                            if ((exportAppearance != null) && exportAppearance.Tables.Contains("AnalysisItemAppearanceData"))
                            {
                                ds.Merge(exportAppearance.Tables["AnalysisItemAppearanceData"]);
                            }
                        }
                    }
                }
                set3 = ds;
            }
            catch (ConstraintException exception)
            {
                if (ds == null)
                {
                    throw;
                }
                foreach (DataTable table in ds.Tables)
                {
                    if (table.HasErrors)
                    {
                        DataRow[] errors = table.GetErrors();
                        for (int i = 0; i < errors.Length; i++)
                        {
                            exception.Data[string.Format("{0}_{1}", table.TableName, i)] = errors[i].RowError;
                        }
                    }
                }
                throw;
            }
            return set3;
        }

        protected void MergeTextData(DataSet templateData, DataTable textData, string suffix)
        {
            if ((templateData != null) && (textData != null))
            {
                textData.TableName = this.TemplateTextTableName;
                if (!textData.Columns.Contains("TemplateID"))
                {
                    textData.Columns.Add("TemplateID", typeof(int));
                }
                if (!textData.Columns.Contains("TextIDSuffix"))
                {
                    textData.Columns.Add("TextIDSuffix", typeof(string));
                }
                foreach (DataRow row in textData.Rows)
                {
                    row["TemplateID"] = base.ID;
                    row["TextIDSuffix"] = suffix;
                }
                templateData.Merge(textData);
            }
        }

        public void MoveItem(TemplatePage page, ItemData item, int position)
        {
            if ((this.PageItemsTable != null) && page.Identity.HasValue)
            {
                int num = page.Identity.Value;
                int num2 = item.ID.Value;
                if (position > this.PageItemsTable.Rows.Count)
                {
                    throw new IndexOutOfRangeException();
                }
                DataRow[] rowArray = this.PageItemsTable.Select(string.Concat(new object[] { "PageID = ", num, " AND ItemID = ", num2 }), string.Empty, DataViewRowState.CurrentRows);
                if (rowArray.Length > 0)
                {
                    int num3 = (int) rowArray[0]["Position"];
                    if (num3 < position)
                    {
                        foreach (DataRow row in this.PageItemsTable.Select(string.Concat(new object[] { "PageID = ", num, " AND Position > ", num3, " AND Position <= ", position }), "Position ASC", DataViewRowState.CurrentRows))
                        {
                            row["Position"] = ((int) row["Position"]) - 1;
                        }
                    }
                    else if (num3 > position)
                    {
                        foreach (DataRow row2 in this.PageItemsTable.Select(string.Concat(new object[] { "PageID = ", num, " AND Position >= ", position, " AND Position < ", num3 }), "Position ASC", DataViewRowState.CurrentRows))
                        {
                            row2["Position"] = ((int) row2["Position"]) + 1;
                        }
                    }
                    rowArray[0]["Position"] = position;
                    try
                    {
                        using (IDbConnection connection = DatabaseFactory.CreateDatabase().GetConnection())
                        {
                            connection.Open();
                            IDbTransaction transaction = connection.BeginTransaction();
                            try
                            {
                                this.SavePageItemData(transaction);
                                transaction.Commit();
                            }
                            catch (Exception exception)
                            {
                                bool flag = ExceptionPolicy.HandleException(exception, "BusinessProtected");
                                transaction.Rollback();
                                if (flag)
                                {
                                    throw;
                                }
                            }
                            finally
                            {
                                connection.Close();
                            }
                        }
                    }
                    catch (Exception exception2)
                    {
                        if (ExceptionPolicy.HandleException(exception2, "BusinessProtected"))
                        {
                            throw;
                        }
                    }
                }
            }
        }

        public virtual void MoveItemToPage(ItemData item, TemplatePage fromPage, TemplatePage toPage)
        {
            if (((item != null) && (fromPage != null)) && (toPage != null))
            {
                this.RemoveItemFromPage(fromPage, item);
                this.AddItemToPage(toPage, item, false);
            }
            try
            {
                using (IDbConnection connection = DatabaseFactory.CreateDatabase().GetConnection())
                {
                    connection.Open();
                    IDbTransaction transaction = connection.BeginTransaction();
                    try
                    {
                        this.SavePageItemData(transaction);
                        transaction.Commit();
                    }
                    catch (Exception exception)
                    {
                        bool flag = ExceptionPolicy.HandleException(exception, "BusinessProtected");
                        transaction.Rollback();
                        if (flag)
                        {
                            throw;
                        }
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            catch (Exception exception2)
            {
                if (ExceptionPolicy.HandleException(exception2, "BusinessProtected"))
                {
                    throw;
                }
            }
        }

        public virtual void MovePage(TemplatePage page, int position)
        {
            if ((this.PageDataTable != null) && page.Identity.HasValue)
            {
                int num = page.Identity.Value;
                DataRow row = this.PageDataTable.Select("PageID = " + num, string.Empty, DataViewRowState.CurrentRows)[0];
                int num2 = (int) row["PagePosition"];
                if (num2 < position)
                {
                    foreach (DataRow row2 in this.PageDataTable.Select(string.Concat(new object[] { "PagePosition > ", num2, " AND PagePosition <= ", position }), "PagePosition ASC", DataViewRowState.CurrentRows))
                    {
                        row2["PagePosition"] = ((int) row2["PagePosition"]) - 1;
                    }
                }
                else if (num2 > position)
                {
                    foreach (DataRow row3 in this.PageDataTable.Select(string.Concat(new object[] { "PagePosition >= ", position, " AND PagePosition < ", num2 }), "PagePosition ASC", DataViewRowState.CurrentRows))
                    {
                        row3["PagePosition"] = ((int) row3["PagePosition"]) + 1;
                    }
                }
                row["PagePosition"] = position;
                page.Position = position;
                this.SortTemplatePages();
                try
                {
                    using (IDbConnection connection = DatabaseFactory.CreateDatabase().GetConnection())
                    {
                        connection.Open();
                        IDbTransaction transaction = connection.BeginTransaction();
                        try
                        {
                            this.SavePageData(transaction);
                            transaction.Commit();
                        }
                        catch (Exception exception)
                        {
                            bool flag = ExceptionPolicy.HandleException(exception, "BusinessProtected");
                            transaction.Rollback();
                            if (flag)
                            {
                                throw;
                            }
                        }
                        finally
                        {
                            connection.Close();
                        }
                    }
                }
                catch (Exception exception2)
                {
                    if (ExceptionPolicy.HandleException(exception2, "BusinessProtected"))
                    {
                        throw;
                    }
                }
            }
        }

        public virtual TemplatePage NewPage()
        {
            if (this.PageDataTable == null)
            {
                return null;
            }
            DataRow row = this.PageDataTable.NewRow();
            this.PageDataTable.Rows.Add(row);
            DataRow[] rowArray = this.PageDataTable.Select("PagePosition IS NOT NULL", "PagePosition DESC", DataViewRowState.CurrentRows);
            int num = -1;
            if ((rowArray.Length > 0) && (rowArray[0]["PagePosition"] != DBNull.Value))
            {
                num = Convert.ToInt32(rowArray[0]["PagePosition"]);
            }
            row["PagePosition"] = num + 1;
            this.SavePageData();
            TemplatePage page = new TemplatePage((int) row["PagePosition"]) {
                Parent = this,
                Identity = new int?((int) row["PageID"]),
                Position = (int) row["PagePosition"]
            };
            this.AddTemplatePage(page);
            return page;
        }

        public virtual TemplatePage NewPage(int position)
        {
            TemplatePage page = this.NewPage();
            this.MovePage(page, position);
            return page;
        }

        protected virtual void OnItemRemoved(ItemData item)
        {
        }

        protected virtual void OnItemSaved(ItemData itemData, IDbTransaction transaction)
        {
            if (this._itemData.ContainsKey(itemData.ID.Value))
            {
                this._itemData[itemData.ID.Value] = itemData;
            }
        }

        protected virtual void OnPageRemoved(TemplatePage page)
        {
        }

        protected virtual void OnTemplateUpdated()
        {
            if (this.Updated != null)
            {
                this.Updated(this, new EventArgs());
            }
        }

        public virtual void RemoveItemFromPage(TemplatePage page, ItemData item)
        {
            if ((this.PageItemsTable != null) && page.Identity.HasValue)
            {
                int num = page.Identity.Value;
                int num2 = item.ID.Value;
                DataRow[] rowArray = this.PageItemsTable.Select(string.Concat(new object[] { "PageID = ", num, " AND ItemID = ", num2 }), string.Empty, DataViewRowState.CurrentRows);
                if (rowArray.Length > 0)
                {
                    string str = rowArray[0]["Position"].ToString();
                    foreach (DataRow row in rowArray)
                    {
                        row.Delete();
                    }
                    foreach (DataRow row2 in this.PageItemsTable.Select(string.Concat(new object[] { "PageID = ", num, " AND Position > ", str }), "Position ASC", DataViewRowState.CurrentRows))
                    {
                        row2["Position"] = ((int) row2["Position"]) - 1;
                    }
                }
                try
                {
                    using (IDbConnection connection = DatabaseFactory.CreateDatabase().GetConnection())
                    {
                        connection.Open();
                        IDbTransaction transaction = connection.BeginTransaction();
                        try
                        {
                            this.SavePageItemData(transaction);
                            transaction.Commit();
                        }
                        catch (Exception exception)
                        {
                            bool flag = ExceptionPolicy.HandleException(exception, "BusinessProtected");
                            transaction.Rollback();
                            if (flag)
                            {
                                throw;
                            }
                        }
                        finally
                        {
                            connection.Close();
                        }
                    }
                }
                catch (Exception exception2)
                {
                    if (ExceptionPolicy.HandleException(exception2, "BusinessProtected"))
                    {
                        throw;
                    }
                }
            }
        }

        private void RemoveItemFromTemplate(ItemData item)
        {
            if (this._itemData.ContainsKey(item.ID.Value))
            {
                this._itemData.Remove(item.ID.Value);
                this.OnItemRemoved(item);
            }
            if (this.TemplateItemsTable != null)
            {
                DataRow[] rowArray = this.TemplateItemsTable.Select("ItemID = " + item.ID.Value, null, DataViewRowState.CurrentRows);
                if (rowArray.Length > 0)
                {
                    rowArray[0].Delete();
                }
            }
            try
            {
                using (IDbConnection connection = DatabaseFactory.CreateDatabase().GetConnection())
                {
                    connection.Open();
                    IDbTransaction transaction = connection.BeginTransaction();
                    try
                    {
                        this.SaveTemplateItemData(transaction);
                        this.SavePageItemData(transaction);
                        transaction.Commit();
                    }
                    catch (Exception exception)
                    {
                        bool flag = ExceptionPolicy.HandleException(exception, "BusinessProtected");
                        transaction.Rollback();
                        if (flag)
                        {
                            throw;
                        }
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            catch (Exception exception2)
            {
                if (ExceptionPolicy.HandleException(exception2, "BusinessProtected"))
                {
                    throw;
                }
            }
        }

        public virtual void RemoveTemplatePage(TemplatePage page)
        {
            if ((this._templatePages != null) && this._templatePages.Contains(page))
            {
                this._templatePages.Remove(page);
                this.SortTemplatePages();
                this.OnPageRemoved(page);
            }
        }

        public virtual void RemoveTemplatePageItems(TemplatePage page)
        {
            foreach (ItemData data in page.Items)
            {
                this.DeleteItemFromPage(page, data);
            }
        }

        protected void SaveItemData(IDbTransaction t)
        {
            if (this._templatePages != null)
            {
                foreach (TemplatePage page in this._templatePages)
                {
                    foreach (ItemData data in page.Items)
                    {
                        base.TransactionAborted += new EventHandler(data.NotifyAbort);
                        base.TransactionCommitted += new EventHandler(data.NotifyCommit);
                        data.Save(t);
                    }
                }
            }
            this.OnTemplateUpdated();
        }

        protected virtual void SavePageData()
        {
            try
            {
                using (IDbConnection connection = DatabaseFactory.CreateDatabase().GetConnection())
                {
                    connection.Open();
                    IDbTransaction transaction = connection.BeginTransaction();
                    try
                    {
                        this.SavePageData(transaction);
                        transaction.Commit();
                    }
                    catch (Exception exception)
                    {
                        bool flag = ExceptionPolicy.HandleException(exception, "BusinessProtected");
                        transaction.Rollback();
                        if (flag)
                        {
                            throw;
                        }
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            catch (Exception exception2)
            {
                if (ExceptionPolicy.HandleException(exception2, "BusinessProtected"))
                {
                    throw;
                }
            }
        }

        protected virtual void SavePageData(IDbTransaction transaction)
        {
            if (this.PageDataTable != null)
            {
                DatabaseFactory.CreateDatabase().UpdateDataSet(this._templateData, this.PageDataTableName, this.GetInsertPageCommand(), this.GetUpdatePageCommand(), this.GetDeletePageCommand(), transaction);
            }
            this.OnTemplateUpdated();
        }

        protected virtual void SavePageItemData()
        {
            try
            {
                using (IDbConnection connection = DatabaseFactory.CreateDatabase().GetConnection())
                {
                    connection.Open();
                    IDbTransaction transaction = connection.BeginTransaction();
                    try
                    {
                        this.SavePageItemData(transaction);
                        transaction.Commit();
                    }
                    catch (Exception exception)
                    {
                        bool flag = ExceptionPolicy.HandleException(exception, "BusinessProtected");
                        transaction.Rollback();
                        if (flag)
                        {
                            throw;
                        }
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            catch (Exception exception2)
            {
                if (ExceptionPolicy.HandleException(exception2, "BusinessProtected"))
                {
                    throw;
                }
            }
        }

        protected virtual void SavePageItemData(IDbTransaction transaction)
        {
            if (this.PageItemsTable != null)
            {
                DatabaseFactory.CreateDatabase().UpdateDataSet(this._templateData, this.PageItemsTableName, this.GetAddItemToPageCommand(), this.GetSetPageItemPositionCommand(), this.GetDeleteItemFromPageCommand(), transaction);
            }
            this.OnTemplateUpdated();
        }

        protected virtual void SaveTemplateItemData()
        {
            try
            {
                using (IDbConnection connection = DatabaseFactory.CreateDatabase().GetConnection())
                {
                    connection.Open();
                    IDbTransaction transaction = connection.BeginTransaction();
                    try
                    {
                        this.SaveTemplateItemData(transaction);
                        transaction.Commit();
                    }
                    catch (Exception exception)
                    {
                        bool flag = ExceptionPolicy.HandleException(exception, "BusinessProtected");
                        transaction.Rollback();
                        if (flag)
                        {
                            throw;
                        }
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            catch (Exception exception2)
            {
                if (ExceptionPolicy.HandleException(exception2, "BusinessProtected"))
                {
                    throw;
                }
            }
        }

        protected virtual void SaveTemplateItemData(IDbTransaction transaction)
        {
            if (this.TemplateItemsTable != null)
            {
                DatabaseFactory.CreateDatabase().UpdateDataSet(this._templateData, this.TemplateItemsTableName, this.GetAddTemplateItemCommand(), null, this.GetDeleteItemFromTemplateCommand(), transaction);
            }
            this.OnTemplateUpdated();
        }

        public void SetItem(ItemData itemData)
        {
            this.SetItem(itemData, false);
        }

        public void SetItem(ItemData itemData, bool replace)
        {
            if (itemData.ID.HasValue && (replace || !this._itemData.ContainsKey(itemData.ID.Value)))
            {
                this._itemData[itemData.ID.Value] = itemData;
            }
        }

        public virtual void SetPageLayoutTemplate(int pageID, int? layoutTemplateID)
        {
            if (this.PageDataTable != null)
            {
                DataRow[] rowArray = this.PageDataTable.Select("PageID = " + pageID, null, DataViewRowState.CurrentRows);
                if (rowArray.Length > 0)
                {
                    if (layoutTemplateID.HasValue)
                    {
                        rowArray[0]["LayoutTemplateID"] = layoutTemplateID;
                    }
                    else
                    {
                        rowArray[0]["LayoutTemplateID"] = DBNull.Value;
                    }
                    this.SavePageData();
                }
            }
            TemplatePage page = this.GetPage(pageID);
            if (page != null)
            {
                page.LayoutTemplateID = layoutTemplateID;
                this._templatePages = null;
            }
        }

        private void SortTemplatePages()
        {
            if (this._templatePages == null)
            {
                this.GetPages(null);
            }
            if (this._templatePages != null)
            {
                this._templatePages.Sort(new TemplatePageComparer());
                for (int i = 0; i < this._templatePages.Count; i++)
                {
                    if (this._templatePages[i].Position > 0)
                    {
                        this._templatePages[i].Position = i;
                    }
                }
            }
        }

        protected override void Update(IDbTransaction t)
        {
            this.EnsureItemsLoaded();
            base.LastModified = new DateTime?(DateTime.Now);
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Template_Update");
            storedProcCommandWrapper.AddInParameter("TemplateID", DbType.Int32, base.ID.Value);
            storedProcCommandWrapper.AddInParameter("ModifiedDate", DbType.DateTime, base.LastModified);
            storedProcCommandWrapper.AddInParameter("DefaultPolicy", DbType.Int32, base.DefaultPolicyID);
            storedProcCommandWrapper.AddInParameter("AclID", DbType.Int32, base.AclID);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
            this.SaveItemData(t);
        }

        protected virtual void ValidateDataSetForImport(DataSet ds)
        {
            if (!ds.Tables.Contains(this.TemplateDataTableName))
            {
                throw new Exception("XML file does not contain a TemplateData node.");
            }
            if (ds.Tables[this.TemplateDataTableName].Rows.Count != 1)
            {
                throw new Exception("XML file must contain only one TemplateData node.");
            }
            if (!ds.Tables.Contains(this.PageDataTableName))
            {
                throw new Exception("XML file does not contain any Pages nodes.");
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            this._templateData.DataSetName = "ConfigurationData";
            this._templateData.SchemaSerializationMode = SchemaSerializationMode.IncludeSchema;
            try
            {
                this.MergeDataForExport().WriteXml(writer, XmlWriteMode.WriteSchema);
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
            }
        }

        public override string DataTableName
        {
            get
            {
                return this.TemplateDataTableName;
            }
        }

        public override string DomainDBIdentityColumnName
        {
            get
            {
                return "TemplateID";
            }
        }

        public override string DomainDBTableName
        {
            get
            {
                return "ckbx_Template";
            }
        }

        protected virtual DataTable ItemAppearanceMapTable
        {
            get
            {
                if ((this._templateData != null) && this._templateData.Tables.Contains(this.ItemAppearanceMapTableName))
                {
                    return this._templateData.Tables[this.ItemAppearanceMapTableName];
                }
                return null;
            }
        }

        protected virtual string ItemAppearanceMapTableName
        {
            get
            {
                return "ItemAppearanceMap";
            }
        }

        protected virtual DataTable ItemAppearanceTable
        {
            get
            {
                if ((this._templateData != null) && this._templateData.Tables.Contains(this.ItemAppearanceTableName))
                {
                    return this._templateData.Tables[this.ItemAppearanceTableName];
                }
                return null;
            }
        }

        protected virtual string ItemAppearanceTableName
        {
            get
            {
                return "AppearanceData";
            }
        }

        public int ItemCount
        {
            get
            {
                return this._itemData.Count;
            }
        }

        public ReadOnlyCollection<ItemData> Items
        {
            get
            {
                List<ItemData> list = new List<ItemData>();
                List<int> list2 = new List<int>(this._itemData.Keys);
                foreach (int num in list2)
                {
                    list.Add(this.GetItem(num));
                }
                return new ReadOnlyCollection<ItemData>(list);
            }
        }

        protected virtual DataTable ItemTextTable
        {
            get
            {
                if ((this._templateData != null) && this._templateData.Tables.Contains(this.ItemTextTableName))
                {
                    return this._templateData.Tables[this.ItemTextTableName];
                }
                return null;
            }
        }

        protected virtual string ItemTextTableName
        {
            get
            {
                return "ItemTexts";
            }
        }

        protected bool Loaded
        {
            get
            {
                return this._loaded;
            }
        }

        protected virtual DataTable OptionTextTable
        {
            get
            {
                if ((this._templateData != null) && this._templateData.Tables.Contains(this.OptionTextTableName))
                {
                    return this._templateData.Tables[this.OptionTextTableName];
                }
                return null;
            }
        }

        protected virtual string OptionTextTableName
        {
            get
            {
                return "OptionTexts";
            }
        }

        protected virtual DataTable PageDataTable
        {
            get
            {
                if ((this._templateData != null) && this._templateData.Tables.Contains(this.PageDataTableName))
                {
                    return this._templateData.Tables[this.PageDataTableName];
                }
                return null;
            }
        }

        protected virtual string PageDataTableName
        {
            get
            {
                return "Pages";
            }
        }

        protected virtual DataTable PageItemsTable
        {
            get
            {
                if ((this._templateData != null) && this._templateData.Tables.Contains(this.PageItemsTableName))
                {
                    return this._templateData.Tables[this.PageItemsTableName];
                }
                return null;
            }
        }

        protected virtual string PageItemsTableName
        {
            get
            {
                return "PageItems";
            }
        }

        protected virtual string TemplateDataTableName
        {
            get
            {
                return "TemplateData";
            }
        }

        protected virtual DataTable TemplateItemsTable
        {
            get
            {
                if ((this._templateData != null) && this._templateData.Tables.Contains(this.TemplateItemsTableName))
                {
                    return this._templateData.Tables[this.TemplateItemsTableName];
                }
                return null;
            }
        }

        protected virtual string TemplateItemsTableName
        {
            get
            {
                return "Items";
            }
        }

        public virtual ReadOnlyCollection<TemplatePage> TemplatePages
        {
            get
            {
                return new ReadOnlyCollection<TemplatePage>(this.GetPages(null));
            }
        }

        protected virtual DataTable TemplateTextTable
        {
            get
            {
                if ((this._templateData != null) && this._templateData.Tables.Contains(this.TemplateTextTableName))
                {
                    return this._templateData.Tables[this.TemplateTextTableName];
                }
                return null;
            }
        }

        protected virtual string TemplateTextTableName
        {
            get
            {
                return "TemplateTexts";
            }
        }

        protected virtual string TextIDPrefix
        {
            get
            {
                return "templateData";
            }
        }
    }
}

