﻿namespace Checkbox.Analytics.Filters
{
    using Checkbox.Analytics.Data;
    using Checkbox.Common;
    using Checkbox.Forms;
    using Checkbox.Users;
    using System;
    using System.Runtime.InteropServices;
    using System.Security.Principal;

    public class ProfileFilter : ResponseFilter
    {
        public override bool EvaluateFilter(ItemAnswer answer, ResponseProperties responseProperties, out bool answerHasValue)
        {
            string str = null;
            string stringValue = responseProperties.GetStringValue("UniqueIdentifier");
            if (Utilities.IsNotNullOrEmpty(stringValue))
            {
                IIdentity userIdentity = UserManager.GetUserIdentity(stringValue);
                if (userIdentity != null)
                {
                    object profileProperty = UserManager.GetProfileProperty(userIdentity, base.PropertyName);
                    if (profileProperty != null)
                    {
                        str = profileProperty.ToString();
                    }
                }
            }
            answerHasValue = Utilities.IsNotNullOrEmpty(str);
            return this.CompareValue(str);
        }
    }
}

