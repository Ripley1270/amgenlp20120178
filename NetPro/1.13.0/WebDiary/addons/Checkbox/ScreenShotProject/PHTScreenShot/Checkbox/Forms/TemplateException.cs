﻿namespace Checkbox.Forms
{
    using System;

    public abstract class TemplateException : ApplicationException
    {
        private string _templateGuid;
        private int? _templateID;

        public TemplateException(int templateID)
        {
            this._templateID = new int?(templateID);
        }

        public TemplateException(string templateGuid)
        {
            this._templateGuid = templateGuid;
        }

        public string TemplateGuid
        {
            get
            {
                return this._templateGuid;
            }
        }

        public int? TemplateID
        {
            get
            {
                return this._templateID;
            }
        }
    }
}

