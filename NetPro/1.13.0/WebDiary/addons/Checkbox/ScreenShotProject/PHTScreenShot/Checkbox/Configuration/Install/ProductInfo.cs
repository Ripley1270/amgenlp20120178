﻿namespace Checkbox.Configuration.Install
{
    using System;

    public class ProductInfo : IComparable<ProductInfo>
    {
        private string _name;
        private string _version;

        public ProductInfo(string name, string version)
        {
            this._name = name;
            this._version = version;
        }

        public int CompareTo(ProductInfo other)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public string Name
        {
            get
            {
                return this._name;
            }
            set
            {
                this._name = value;
            }
        }

        public string Version
        {
            get
            {
                return this._version;
            }
            set
            {
                this._version = value;
            }
        }
    }
}

