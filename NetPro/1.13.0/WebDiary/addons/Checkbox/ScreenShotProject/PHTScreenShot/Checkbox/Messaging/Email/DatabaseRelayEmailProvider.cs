﻿namespace Checkbox.Messaging.Email
{
    using Prezza.Framework.Common;
    using Prezza.Framework.Configuration;
    using Prezza.Framework.Data;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;

    public class DatabaseRelayEmailProvider : BaseEmailProvider
    {
        private string _mailDbInstanceName;

        private void AddAttachmentToMessage(long messageId, long attachmentId, IDbTransaction transaction)
        {
            Database databaseProvider = this.GetDatabaseProvider();
            DBCommandWrapper storedProcCommandWrapper = databaseProvider.GetStoredProcCommandWrapper("ckbx_DatabaseRelayEmail_AddAttachmentToMessage");
            storedProcCommandWrapper.AddInParameter("MessageId", DbType.Int64, messageId);
            storedProcCommandWrapper.AddInParameter("AttachmentId", DbType.Int64, attachmentId);
            databaseProvider.ExecuteNonQuery(storedProcCommandWrapper, transaction);
        }

        protected override long DoAddEmailMessageToBatch(long batchId, IEmailMessage message)
        {
            return this.QueueEmailMessage(new long?(batchId), message);
        }

        protected override void DoCloseEmailMessageBatch(long batchId)
        {
            Database databaseProvider = this.GetDatabaseProvider();
            DBCommandWrapper storedProcCommandWrapper = databaseProvider.GetStoredProcCommandWrapper("ckbx_DatabaseRelayEmail_CloseBatch");
            storedProcCommandWrapper.AddInParameter("BatchId", DbType.Int64, batchId);
            databaseProvider.ExecuteNonQuery(storedProcCommandWrapper);
        }

        protected override long DoCreateEmailMessageBatch(string name, string createdBy, DateTime? earliestSendDate)
        {
            Database databaseProvider = this.GetDatabaseProvider();
            DBCommandWrapper storedProcCommandWrapper = databaseProvider.GetStoredProcCommandWrapper("ckbx_DatabaseRelayEmail_CreateBatch");
            storedProcCommandWrapper.AddInParameter("BatchName", DbType.String, name);
            storedProcCommandWrapper.AddInParameter("CreatedBy", DbType.String, createdBy);
            storedProcCommandWrapper.AddInParameter("EarliestSendDate", DbType.DateTime, earliestSendDate);
            storedProcCommandWrapper.AddOutParameter("BatchId", DbType.Int64, 8);
            databaseProvider.ExecuteNonQuery(storedProcCommandWrapper);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("BatchId");
            if ((parameterValue != null) && (parameterValue != DBNull.Value))
            {
                return (long) parameterValue;
            }
            return -1L;
        }

        protected override void DoDeleteEmailMessage(long messageId)
        {
            Database databaseProvider = this.GetDatabaseProvider();
            DBCommandWrapper storedProcCommandWrapper = databaseProvider.GetStoredProcCommandWrapper("ckbx_DatabaseRelayEmail_DeleteMessage");
            storedProcCommandWrapper.AddInParameter("MessageId", DbType.Int64, messageId);
            databaseProvider.ExecuteNonQuery(storedProcCommandWrapper);
        }

        public override void DoDeleteEmailMessageBatch(long batchId)
        {
            Database databaseProvider = this.GetDatabaseProvider();
            DBCommandWrapper storedProcCommandWrapper = databaseProvider.GetStoredProcCommandWrapper("ckbx_DatabaseRelayEmail_DeleteMessageBatch");
            storedProcCommandWrapper.AddInParameter("BatchId", DbType.Int64, batchId);
            databaseProvider.ExecuteNonQuery(storedProcCommandWrapper);
        }

        protected override void DoDeleteMessageAttachment(long attachmentId)
        {
            Database databaseProvider = this.GetDatabaseProvider();
            DBCommandWrapper storedProcCommandWrapper = databaseProvider.GetStoredProcCommandWrapper("ckbx_DatabaseRelayEmail_DeleteMessageAttachment");
            storedProcCommandWrapper.AddInParameter("AttachmentId", DbType.Int64, attachmentId);
            databaseProvider.ExecuteNonQuery(storedProcCommandWrapper);
        }

        protected override IEmailMessageBatchStatus DoGetEmailMessageBatchStatus(long batchId)
        {
            Database databaseProvider = this.GetDatabaseProvider();
            DBCommandWrapper storedProcCommandWrapper = databaseProvider.GetStoredProcCommandWrapper("ckbx_DatabaseRelayEmail_GetBatchStatus");
            EmailMessageBatchStatus status = null;
            using (IDataReader reader = databaseProvider.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    if (reader.Read())
                    {
                        status = new EmailMessageBatchStatus {
                            CurrentStatus = (BatchStatus) Enum.Parse(typeof(BatchStatus), DbUtility.GetValueFromDataReader<string>(reader, "BatchStatus", BatchStatus.Unknown.ToString())),
                            BatchSendStarted = DbUtility.GetValueFromDataReader<DateTime?>(reader, "BatchSendStarted", null),
                            BatchSendCompleted = DbUtility.GetValueFromDataReader<DateTime?>(reader, "BatchSendCompleted", null),
                            NextSendAttempt = DbUtility.GetValueFromDataReader<DateTime?>(reader, "NextSendAttempt", null),
                            SuccessfullySent = DbUtility.GetValueFromDataReader<bool>(reader, "SuccessfullySent", false),
                            AttemptedCount = DbUtility.GetValueFromDataReader<int>(reader, "AttemptedCount", 0),
                            SucceededCount = DbUtility.GetValueFromDataReader<int>(reader, "SucceededCount", 0),
                            FailedCount = DbUtility.GetValueFromDataReader<int>(reader, "FailedCount", 0)
                        };
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            if ((status != null) && (status.FailedCount > 0))
            {
                List<long> list = new List<long>();
                DBCommandWrapper command = databaseProvider.GetStoredProcCommandWrapper("ckbx_DatabaseRelayEmail_GetBatchFailures");
                command.AddInParameter("BatchId", DbType.Int64, batchId);
                using (IDataReader reader2 = databaseProvider.ExecuteReader(command))
                {
                    while (reader2.Read())
                    {
                        long item = DbUtility.GetValueFromDataReader<long>(reader2, "MessageId", -1L);
                        if (item > 0L)
                        {
                            list.Add(item);
                        }
                    }
                }
                status.FailedMessages = list;
            }
            return status;
        }

        protected override IEmailMessageStatus DoGetEmailMessageStatus(long emailId)
        {
            Database databaseProvider = this.GetDatabaseProvider();
            DBCommandWrapper storedProcCommandWrapper = databaseProvider.GetStoredProcCommandWrapper("ckbx_DatabaseRelayEmail_GetMessageStatus");
            storedProcCommandWrapper.AddInParameter("MessageId", DbType.Int64, emailId);
            EmailMessageStatus status = null;
            using (IDataReader reader = databaseProvider.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    if (reader.Read())
                    {
                        status = new EmailMessageStatus {
                            LastSendAttempt = DbUtility.GetValueFromDataReader<DateTime?>(reader, "LastSendAttempt", null),
                            NextSendAttempt = DbUtility.GetValueFromDataReader<DateTime?>(reader, "NextSendAttempt", null),
                            SuccessfullySent = DbUtility.GetValueFromDataReader<bool>(reader, "SuccessfullySent", false),
                            LastSendError = DbUtility.GetValueFromDataReader<string>(reader, "LastSendError", string.Empty),
                            QueuedDate = DbUtility.GetValueFromDataReader<DateTime?>(reader, "DateCreated", null)
                        };
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return status;
        }

        protected override IEmailMessage DoGetMessage(long messageId)
        {
            Database databaseProvider = this.GetDatabaseProvider();
            DBCommandWrapper storedProcCommandWrapper = databaseProvider.GetStoredProcCommandWrapper("ckbx_DatabaseRelayEmail_GetMessage");
            storedProcCommandWrapper.AddInParameter("MessageId", DbType.Int64, messageId);
            EmailMessage message = null;
            using (IDataReader reader = databaseProvider.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    if (reader.Read())
                    {
                        message = new EmailMessage {
                            From = DbUtility.GetValueFromDataReader<string>(reader, "FromEmail", string.Empty),
                            To = DbUtility.GetValueFromDataReader<string>(reader, "ToEmail", string.Empty),
                            Body = DbUtility.GetValueFromDataReader<string>(reader, "MessageBody", string.Empty),
                            Subject = DbUtility.GetValueFromDataReader<string>(reader, "MessageSubject", string.Empty),
                            Format = DbUtility.GetValueFromDataReader<bool>(reader, "MessageIsHtml", false) ? MailFormat.Html : MailFormat.Text
                        };
                        if (!reader.NextResult())
                        {
                            return message;
                        }
                        while (reader.Read())
                        {
                            EmailAttachment item = new EmailAttachment {
                                MimeContentTypeString = DbUtility.GetValueFromDataReader<string>(reader, "MimeContentType", string.Empty),
                                FileName = DbUtility.GetValueFromDataReader<string>(reader, "AttachmentName", string.Empty)
                            };
                            message.Attachments.Add(item);
                        }
                    }
                    return message;
                }
                finally
                {
                    reader.Close();
                }
            }
            return message;
        }

        protected override void DoInitialize(ConfigurationBase config)
        {
            ArgumentValidation.CheckExpectedType(config, typeof(DatabaseRelayEmailProviderData));
            this._mailDbInstanceName = ((DatabaseRelayEmailProviderData) config).MailDbInstanceName;
        }

        protected override long DoRegisterEmailAttachment(IEmailAttachment attachment)
        {
            return this.RegisterEmailMessageAttachment(null, attachment, null);
        }

        protected override long? DoSendMessage(IEmailMessage message)
        {
            return new long?(this.DoSendMessage("Ad Hoc Email Message " + DateTime.Now, "DatabaseRelayEmailProvider", message, null));
        }

        protected override long DoSendMessage(string name, string createdBy, IEmailMessage message, DateTime? earliestSendDate)
        {
            long num = base.CreateEmailMessageBatch("Ad Hoc Message Batch " + DateTime.Now, "DatabaseRelayEmailProvider", earliestSendDate);
            if (num <= 0L)
            {
                throw new Exception("Unable to send email:  Create batch returned an invalid batch id [" + num + "].");
            }
            long num2 = this.QueueEmailMessage(new long?(num), message);
            this.DoCloseEmailMessageBatch(num);
            return num2;
        }

        private Database GetDatabaseProvider()
        {
            return DatabaseFactory.CreateDatabase(this._mailDbInstanceName);
        }

        private long QueueEmailMessage(long? batchId, IEmailMessage message)
        {
            Database databaseProvider = this.GetDatabaseProvider();
            long messageId = -1L;
            using (IDbConnection connection = databaseProvider.GetConnection())
            {
                try
                {
                    connection.Open();
                    using (IDbTransaction transaction = connection.BeginTransaction())
                    {
                        try
                        {
                            messageId = this.QueueEmailMessage(batchId, message, transaction);
                            this.QueueEmailMessageAttachments(messageId, message.Attachments, transaction);
                            this.QueueEmailMessageAttachments(messageId, message.AttachmentsByRef, transaction);
                            transaction.Commit();
                        }
                        catch
                        {
                            transaction.Rollback();
                        }
                        return messageId;
                    }
                }
                finally
                {
                    connection.Close();
                }
            }
            return messageId;
        }

        private long QueueEmailMessage(long? batchId, IEmailMessage message, IDbTransaction transaction)
        {
            Database databaseProvider = this.GetDatabaseProvider();
            DBCommandWrapper storedProcCommandWrapper = databaseProvider.GetStoredProcCommandWrapper("ckbx_DatabaseRelayEmail_InsertMessage");
            storedProcCommandWrapper.AddInParameter("BatchId", DbType.Int64, batchId);
            storedProcCommandWrapper.AddInParameter("FromName", DbType.String, message.From);
            storedProcCommandWrapper.AddInParameter("FromEmail", DbType.String, message.From);
            storedProcCommandWrapper.AddInParameter("ToEmail", DbType.String, message.To);
            storedProcCommandWrapper.AddInParameter("MessageSubject", DbType.String, message.Subject);
            storedProcCommandWrapper.AddInParameter("MessageBody", DbType.String, message.Body);
            storedProcCommandWrapper.AddInParameter("MessageIsHTML", DbType.Boolean, message.IsBodyHtml);
            storedProcCommandWrapper.AddOutParameter("MessageId", DbType.Int64, 8);
            databaseProvider.ExecuteNonQuery(storedProcCommandWrapper, transaction);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("MessageId");
            if ((parameterValue != null) && (parameterValue != DBNull.Value))
            {
                return (long) parameterValue;
            }
            return -1L;
        }

        private void QueueEmailMessageAttachments(long messageId, List<IEmailAttachment> attachments, IDbTransaction transaction)
        {
            foreach (IEmailAttachment attachment in attachments)
            {
                long attachmentId = this.RegisterEmailMessageAttachment(new long?(messageId), attachment, transaction);
                if (attachmentId > 0L)
                {
                    this.AddAttachmentToMessage(messageId, attachmentId, transaction);
                }
            }
        }

        private void QueueEmailMessageAttachments(long messageId, List<long> attachments, IDbTransaction transaction)
        {
            foreach (long num in attachments)
            {
                this.AddAttachmentToMessage(messageId, num, transaction);
            }
        }

        private long RegisterEmailMessageAttachment(long? messageId, IEmailAttachment attachment, IDbTransaction transaction)
        {
            MemoryStream contentStream = attachment.GetContentStream() as MemoryStream;
            if (contentStream != null)
            {
                Database databaseProvider = this.GetDatabaseProvider();
                DBCommandWrapper storedProcCommandWrapper = databaseProvider.GetStoredProcCommandWrapper("ckbx_DatabaseRelayEmail_RegisterMessageAttachment");
                storedProcCommandWrapper.AddInParameter("AttachmentName", DbType.String, attachment.FileName);
                storedProcCommandWrapper.AddInParameter("MimeContentType", DbType.String, attachment.MimeContentTypeString);
                storedProcCommandWrapper.AddInParameter("AttachmentBytes", DbType.Binary, contentStream.ToArray());
                storedProcCommandWrapper.AddInParameter("MessageId", DbType.Int64, messageId);
                storedProcCommandWrapper.AddOutParameter("AttachmentId", DbType.Int64, 8);
                if (transaction != null)
                {
                    databaseProvider.ExecuteNonQuery(storedProcCommandWrapper, transaction);
                }
                else
                {
                    databaseProvider.ExecuteNonQuery(storedProcCommandWrapper);
                }
                object parameterValue = storedProcCommandWrapper.GetParameterValue("AttachmentId");
                if ((parameterValue != null) && (parameterValue != DBNull.Value))
                {
                    return (long) parameterValue;
                }
            }
            return -1L;
        }

        public override bool SupportsBatches
        {
            get
            {
                return true;
            }
        }
    }
}

