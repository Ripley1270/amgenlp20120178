﻿namespace Checkbox.Forms.Logic.Configuration
{
    using Checkbox.Common;
    using Checkbox.Forms;
    using Checkbox.Forms.Items;
    using Checkbox.Forms.Items.Configuration;
    using Checkbox.Forms.Logic;
    using Prezza.Framework.Common;
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Data;
    using System.Runtime.CompilerServices;
    using System.Text;

    [Serializable]
    public class ItemOperandData : OperandData
    {
        public ItemOperandData(ResponseTemplate context) : this(context, null)
        {
        }

        public ItemOperandData(ResponseTemplate context, int? itemID) : base(context)
        {
            this.ItemId = itemID;
        }

        protected override void Create(IDbTransaction t)
        {
            base.Create(t);
            DataRow row = base.Context.ItemOperandTable.NewRow();
            row["OperandID"] = base.ID.Value;
            row["ItemID"] = this.ItemId.Value;
            base.Context.ItemOperandTable.Rows.Add(row);
        }

        public override Operand CreateOperand(Response context, string languageCode)
        {
            Item item = context.Items[this.ItemId.Value];
            if (item is SelectItem)
            {
                return new SelectItemOperand((SelectItem) item);
            }
            if (item is IAnswerable)
            {
                return new AnswerableOperand((IAnswerable) item);
            }
            return null;
        }

        public override void Delete(IDbTransaction transaction)
        {
            DataRow[] rowArray = base.Context.ItemOperandTable.Select(this.IdentityColumnName + "=" + base.ID.Value);
            if (rowArray.Length > 0)
            {
                rowArray[0].Delete();
            }
            base.Delete(transaction);
        }

        protected override DataSet GetConcreteConfigurationDataSet()
        {
            if (base.ID <= 0)
            {
                throw new ApplicationException("No DataID specified.");
            }
            try
            {
                Database database = DatabaseFactory.CreateDatabase();
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemOperand_GetOperand");
                storedProcCommandWrapper.AddInParameter("OperandID", DbType.Int32, base.ID);
                DataSet dataSet = new DataSet();
                database.LoadDataSet(storedProcCommandWrapper, dataSet, new string[] { this.DataTableName });
                return dataSet;
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessProtected"))
                {
                    throw;
                }
                return null;
            }
        }

        protected override void LoadFromDataRow(DataRow data)
        {
            ArgumentValidation.CheckForNullReference(data, "data");
            try
            {
                if (data["ItemID"] != DBNull.Value)
                {
                    this.ItemId = new int?((int) data["ItemID"]);
                }
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessProtected"))
                {
                    throw;
                }
            }
        }

        public override string ToString()
        {
            ItemData item = base.Context.GetItem(this.ItemId.Value);
            if (item == null)
            {
                return ("Unable to load item with id: " + this.ItemId);
            }
            StringBuilder builder = new StringBuilder();
            StringBuilder builder2 = new StringBuilder();
            foreach (TemplatePage page in base.Context.TemplatePages)
            {
                if (page.Items.Contains(item))
                {
                    builder.Append(page.Position.ToString());
                    builder.Append(".");
                    builder.Append((page.Items.IndexOf(item) + 1).ToString());
                    builder.Append(" ");
                    break;
                }
            }
            if (item is LabelledItemData)
            {
                LabelledItemTextDecorator decorator = (LabelledItemTextDecorator) item.CreateTextDecorator(base.Context.EditLanguage);
                builder2.Append(decorator.Text);
                if (builder2.Length == 0)
                {
                    builder2.Append(decorator.SubText);
                }
            }
            else
            {
                builder2.Append(item.Alias);
            }
            builder2.Insert(0, builder.ToString());
            return Utilities.StripHtml(builder2.ToString(), 0x40);
        }

        public override void Validate()
        {
            if (base.Context.GetItem(this.ItemId.Value) == null)
            {
                this.OnValidationFailed();
            }
        }

        public override string DataTableName
        {
            get
            {
                return "ItemOperand";
            }
        }

        public int? ItemId { get; set; }

        public override string OperandTypeName
        {
            get
            {
                return "ItemOperand";
            }
        }
    }
}

