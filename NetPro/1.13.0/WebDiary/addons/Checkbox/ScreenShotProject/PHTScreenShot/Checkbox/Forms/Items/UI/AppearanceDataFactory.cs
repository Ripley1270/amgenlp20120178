﻿namespace Checkbox.Forms.Items.UI
{
    using Prezza.Framework.Common;
    using System;
    using System.Reflection;

    internal class AppearanceDataFactory
    {
        public AppearanceData CreateAppearanceData(string typeName)
        {
            AppearanceData data;
            try
            {
                data = this.CreateObject(this.GetType(typeName));
            }
            catch (Exception)
            {
                throw;
            }
            return data;
        }

        private AppearanceData CreateObject(Type type)
        {
            ArgumentValidation.CheckForNullReference(type, "type");
            this.ValidateTypeIsAppearanceData(type);
            ConstructorInfo constructor = type.GetConstructor(new Type[0]);
            if (constructor == null)
            {
                throw new Exception("ItemData does not have a constructor: " + type.FullName);
            }
            object obj2 = null;
            try
            {
                obj2 = constructor.Invoke(null);
            }
            catch (MethodAccessException exception)
            {
                throw new Exception(exception.Message, exception);
            }
            catch (TargetInvocationException exception2)
            {
                throw new Exception(exception2.Message, exception2);
            }
            catch (TargetParameterCountException exception3)
            {
                throw new Exception(exception3.Message, exception3);
            }
            catch (Exception exception4)
            {
                throw exception4;
            }
            return (AppearanceData) obj2;
        }

        private Type GetType(string typeName)
        {
            Type type;
            ArgumentValidation.CheckForEmptyString(typeName, "typeName");
            try
            {
                type = Type.GetType(typeName, true, false);
            }
            catch (TypeLoadException exception)
            {
                throw new Exception("A type-loading error occurred.  Type was: " + typeName, exception);
            }
            return type;
        }

        private void ValidateTypeIsAppearanceData(Type type)
        {
            ArgumentValidation.CheckForNullReference(type, "type");
            if (!typeof(AppearanceData).IsAssignableFrom(type))
            {
                throw new Exception("Type mismatch between ItemData type [" + typeof(AppearanceData).AssemblyQualifiedName + "] and requested type [" + type.AssemblyQualifiedName);
            }
        }
    }
}

