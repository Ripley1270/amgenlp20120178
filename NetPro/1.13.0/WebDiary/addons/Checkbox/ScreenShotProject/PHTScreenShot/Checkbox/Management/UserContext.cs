﻿namespace Checkbox.Management
{
    using System;

    [Serializable]
    public class UserContext
    {
        private string currentUrl;
        private string loginTime;
        private string userAgent;
        private string userHostIP;
        private string userHostName;

        public UserContext(string userHostName, string userHostIP, string userAgent)
        {
            this.userHostName = userHostName;
            this.userHostIP = userHostIP;
            this.userAgent = userAgent;
            this.loginTime = DateTime.Now.ToString("s");
            this.currentUrl = string.Empty;
        }

        public string CurrentUrl
        {
            get
            {
                return this.currentUrl;
            }
            set
            {
                this.currentUrl = value;
            }
        }

        public string LoginTime
        {
            get
            {
                return this.loginTime;
            }
        }

        public string UserAgent
        {
            get
            {
                return this.userAgent;
            }
        }

        public string UserHostIP
        {
            get
            {
                return this.userHostIP;
            }
        }

        public string UserHostName
        {
            get
            {
                return this.userHostName;
            }
        }
    }
}

