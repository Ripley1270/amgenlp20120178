﻿namespace Checkbox.Forms.Validation
{
    using Checkbox.Common;
    using Checkbox.Forms.Items;
    using Checkbox.Globalization.Text;
    using System;

    public class SelectItemValidator : Validator<SelectItem>
    {
        private string _text;

        public override string GetMessage(string languageCode)
        {
            return this.ErrorMessage;
        }

        public override bool Validate(SelectItem input)
        {
            this.ErrorMessage = string.Empty;
            if (!this.ValidateRequired(input))
            {
                return false;
            }
            foreach (ListOption option in input.SelectedOptions)
            {
                if (option.IsOther && Utilities.IsNullOrEmpty(input.OtherText))
                {
                    this.ErrorMessage = TextManager.GetText("/validationMessages/selectOne/otherIsRequired", input.LanguageCode);
                    return false;
                }
            }
            return true;
        }

        protected virtual bool ValidateRequired(SelectItem input)
        {
            bool flag = !input.Required || (input.SelectedOptions.Count > 0);
            if (!flag)
            {
                this.ErrorMessage = TextManager.GetText("/validationMessages/regex/required", input.LanguageCode);
            }
            return flag;
        }

        protected string ErrorMessage
        {
            get
            {
                return this._text;
            }
            set
            {
                this._text = value;
            }
        }
    }
}

