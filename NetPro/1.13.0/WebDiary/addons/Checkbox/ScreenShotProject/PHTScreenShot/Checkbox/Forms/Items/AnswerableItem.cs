﻿namespace Checkbox.Forms.Items
{
    using Prezza.Framework.Data;
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Data;
    using System.Runtime.CompilerServices;
    using System.Xml;

    [Serializable]
    public abstract class AnswerableItem : ResponseItem, IAnswerable
    {
        public event EventHandler AnswerChanged;

        protected AnswerableItem()
        {
        }

        public virtual void DeleteAnswers()
        {
            if (base.AnswerData != null)
            {
                List<long> list = new List<long>();
                foreach (DataRow row in base.AnswerData.GetAnswerRowsForItem(base.ID))
                {
                    list.Add(DbUtility.GetValueFromDataRow<long>(row, "AnswerID", -1L));
                }
                foreach (long num in list)
                {
                    base.AnswerData.DeleteAnswerRow(num);
                }
                this.OnAnswerChanged();
            }
        }

        protected override bool DoValidateItem()
        {
            return this.ValidateAnswers();
        }

        public virtual string GetAnswer()
        {
            if (base.AnswerData != null)
            {
                DataRow[] answerRowsForItem = base.AnswerData.GetAnswerRowsForItem(base.ID);
                if (answerRowsForItem.Length > 0)
                {
                    return DbUtility.GetValueFromDataRow<string>(answerRowsForItem[0], "AnswerText", string.Empty);
                }
            }
            return string.Empty;
        }

        protected override NameValueCollection GetMetaDataValuesForXmlSerialization()
        {
            NameValueCollection metaDataValuesForXmlSerialization = base.GetMetaDataValuesForXmlSerialization();
            metaDataValuesForXmlSerialization["answerRequired"] = this.Required.ToString();
            return metaDataValuesForXmlSerialization;
        }

        protected virtual void OnAnswerChanged()
        {
            if (this.AnswerChanged != null)
            {
                this.AnswerChanged(this, new EventArgs());
            }
        }

        protected override void OnItemExcluded()
        {
            base.OnItemExcluded();
            if ((base.Response != null) && ((base.Response.CurrentPage == null) || (base.ContainingPagePosition >= base.Response.CurrentPage.Position)))
            {
                this.DeleteAnswers();
            }
        }

        protected override void OnStateRestored()
        {
            if ((base.AnswerData != null) && (base.AnswerData.GetAnswerRowsForItem(base.ID).Length > 0))
            {
                this.OnAnswerChanged();
            }
        }

        public virtual void SetAnswer(object answer)
        {
            if (base.AnswerData != null)
            {
                DataRow[] answerRowsForItem = base.AnswerData.GetAnswerRowsForItem(base.ID);
                if (answerRowsForItem.Length == 0)
                {
                    DataRow row = base.AnswerData.CreateAnswerRow(base.ID);
                    if (answer != null)
                    {
                        row["AnswerText"] = answer.ToString().Trim();
                    }
                    base.AnswerData.AddAnswerRow(row);
                }
                else if (answer != null)
                {
                    answerRowsForItem[0]["AnswerText"] = answer.ToString().Trim();
                }
                this.OnAnswerChanged();
            }
        }

        protected abstract bool ValidateAnswers();
        protected virtual void WriteAnswers(XmlWriter writer)
        {
            if (this.HasAnswer)
            {
                writer.WriteStartElement("answer");
                writer.WriteCData(this.GetAnswer());
                writer.WriteEndElement();
            }
        }

        public override void WriteXmlInstanceData(XmlWriter writer)
        {
            base.WriteXmlInstanceData(writer);
            writer.WriteStartElement("answers");
            this.WriteAnswers(writer);
            writer.WriteEndElement();
        }

        public virtual bool HasAnswer
        {
            get
            {
                if (base.AnswerData == null)
                {
                    return false;
                }
                DataRow[] answerRowsForItem = base.AnswerData.GetAnswerRowsForItem(base.ID);
                if (answerRowsForItem.Length == 0)
                {
                    return false;
                }
                return (DbUtility.GetValueFromDataRow<string>(answerRowsForItem[0], "AnswerText", string.Empty) != string.Empty);
            }
        }
    }
}

