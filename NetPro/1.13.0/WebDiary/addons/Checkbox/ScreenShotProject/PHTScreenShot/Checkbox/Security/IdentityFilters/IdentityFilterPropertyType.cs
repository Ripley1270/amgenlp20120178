﻿namespace Checkbox.Security.IdentityFilters
{
    using System;

    public enum IdentityFilterPropertyType
    {
        IdentityProperty,
        ProfileProperty,
        Other
    }
}

