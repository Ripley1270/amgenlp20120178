﻿namespace Checkbox.Forms
{
    using Prezza.Framework.Data.Sprocs;
    using Prezza.Framework.Security;
    using System;
    using System.Data;

    [FetchProcedure("ckbx_Template_GetAccessControllableResource")]
    public class LightweightTemplate : LightweightAccessControllable
    {
        private int _templateID;

        internal LightweightTemplate(int templateID)
        {
            this._templateID = templateID;
        }

        [FetchParameter(Name="TemplateID", DbType=DbType.Int32, Direction=ParameterDirection.Input)]
        public int TemplateID
        {
            get
            {
                return this._templateID;
            }
            set
            {
                this._templateID = value;
            }
        }
    }
}

