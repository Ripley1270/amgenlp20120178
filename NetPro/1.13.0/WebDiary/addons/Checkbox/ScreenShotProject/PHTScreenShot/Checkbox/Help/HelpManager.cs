﻿namespace Checkbox.Help
{
    using Checkbox.Management;
    using Checkbox.Users;
    using Prezza.Framework.Data;
    using System;
    using System.Collections;
    using System.Data;
    using System.Security.Principal;
    using System.Text;

    public static class HelpManager
    {
        private static Hashtable _helpContext = new Hashtable();

        private static int GetContextId(string pageContext, string skinName)
        {
            object obj2;
            int num;
            try
            {
                Database database = DatabaseFactory.CreateDatabase();
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Help_GetContextLink");
                storedProcCommandWrapper.AddInParameter("@SourceFilePath", DbType.String, pageContext);
                storedProcCommandWrapper.AddInParameter("@SkinName", DbType.String, skinName);
                obj2 = database.ExecuteScalar(storedProcCommandWrapper);
            }
            catch (Exception)
            {
                return 0;
            }
            if ((obj2 != null) && int.TryParse(obj2.ToString(), out num))
            {
                return num;
            }
            return 0;
        }

        public static string GetHelpContext(string path)
        {
            IPrincipal currentPrincipal = UserManager.GetCurrentPrincipal();
            string str = (currentPrincipal == null) ? "Public" : "Admin";
            string pageContext = path.Substring(ApplicationManager.ApplicationRoot.Length + 1);
            string str3 = string.Format("{0}/{1}", str, pageContext.ToLower());
            if (HelpContext[str3] != null)
            {
                return HelpContext[str3].ToString();
            }
            int contextId = GetContextId(pageContext, str);
            if (contextId == 0)
            {
                contextId = (currentPrincipal == null) ? 1 : 5;
            }
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("{0}/Help/{1}/Default_CSH.htm#{2}", ApplicationManager.ApplicationRoot, str, contextId);
            if ((pageContext != string.Empty) && (builder.Length > 0))
            {
                lock (HelpContext.SyncRoot)
                {
                    HelpContext[str3] = builder.ToString();
                }
            }
            return builder.ToString();
        }

        private static Hashtable HelpContext
        {
            get
            {
                return _helpContext;
            }
            set
            {
                _helpContext = value;
            }
        }
    }
}

