﻿namespace Checkbox.Forms.Data
{
    using Checkbox.Common;
    using Checkbox.Forms.Items.Configuration;
    using Prezza.Framework.Common;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LightweightItemMetaData
    {
        private List<int> _children;
        private Dictionary<int, Prezza.Framework.Common.Coordinate> _matrixChildCoordindates;
        private Dictionary<int, int> _matrixColumnPrototypeIds;
        private Dictionary<int, string> _matrixColumnTypes;
        private Dictionary<int, int> _matrixRowKeyItems;
        private Dictionary<int, string> _matrixRowTypes;
        private List<int> _options;
        private Dictionary<string, string> _textDictionary;

        public void AddColumn(int columnNumber, int columnPrototypeId, string type)
        {
            this.ColumnTypes[columnNumber] = type;
            this.ColumnPrototypeIds[columnNumber] = columnPrototypeId;
        }

        public void AddRow(int rowNumber, int rowKeyItemId, string type)
        {
            this.RowTypes[rowNumber] = type;
            this.RowKeyItems[rowNumber] = rowKeyItemId;
        }

        public Prezza.Framework.Common.Coordinate GetChildCoordinate(int childItemId)
        {
            if (this.ChildCoordinates.ContainsKey(childItemId))
            {
                return this.ChildCoordinates[childItemId];
            }
            return null;
        }

        public int? GetColumnPrototypeID(int columnNumber)
        {
            if (this.ColumnPrototypeIds.ContainsKey(columnNumber))
            {
                return new int?(this.ColumnPrototypeIds[columnNumber]);
            }
            return null;
        }

        public string GetColumnType(int columnNumber)
        {
            if (this.ColumnTypes.ContainsKey(columnNumber))
            {
                return this.ColumnTypes[columnNumber];
            }
            return string.Empty;
        }

        public string GetDescription(string languageCode)
        {
            if (this.TextDictionary.ContainsKey("Description_" + languageCode))
            {
                return this.TextDictionary["Description_" + languageCode];
            }
            return string.Empty;
        }

        public int? GetRowPkItemId(int rowNumber)
        {
            if (this.RowKeyItems.ContainsKey(rowNumber))
            {
                return new int?(this.RowKeyItems[rowNumber]);
            }
            return null;
        }

        public string GetRowType(int rowNumber)
        {
            if (this.RowTypes.ContainsKey(rowNumber))
            {
                return this.RowTypes[rowNumber];
            }
            return string.Empty;
        }

        public string GetText(string languageCode)
        {
            if (this.TextDictionary.ContainsKey("Text_" + languageCode))
            {
                return this.TextDictionary["Text_" + languageCode];
            }
            return string.Empty;
        }

        public string GetText(bool preferAlias, string languageCode)
        {
            string str = preferAlias ? this.Alias : this.GetText(languageCode);
            if (Utilities.IsNullOrEmpty(str))
            {
                str = preferAlias ? this.GetText(languageCode) : this.GetDescription(languageCode);
            }
            if (Utilities.IsNullOrEmpty(str))
            {
                str = preferAlias ? this.GetDescription(languageCode) : this.Alias;
            }
            return str;
        }

        public void SetChildCoordinate(int childItemId, Prezza.Framework.Common.Coordinate coordinate)
        {
            this.ChildCoordinates[childItemId] = coordinate;
        }

        public void SetDescription(string languageCode, string description)
        {
            this.TextDictionary["Description_" + languageCode] = description;
        }

        public void SetText(string languageCode, string text)
        {
            this.TextDictionary["Text_" + languageCode] = text;
        }

        public bool Validate()
        {
            return !ItemConfigurationManager.CheckItemDataUpdated(this.ItemId, new DateTime?(this.LastModified));
        }

        public string Alias { get; set; }

        public bool AllowOther { get; set; }

        public int AncestorId { get; set; }

        private Dictionary<int, Prezza.Framework.Common.Coordinate> ChildCoordinates
        {
            get
            {
                if (this._matrixChildCoordindates == null)
                {
                    this._matrixChildCoordindates = new Dictionary<int, Prezza.Framework.Common.Coordinate>();
                }
                return this._matrixChildCoordindates;
            }
        }

        public List<int> Children
        {
            get
            {
                if (this._children == null)
                {
                    this._children = new List<int>();
                }
                return this._children;
            }
        }

        public int ColumnCount
        {
            get
            {
                return this.ColumnTypes.Count;
            }
        }

        private Dictionary<int, int> ColumnPrototypeIds
        {
            get
            {
                if (this._matrixColumnPrototypeIds == null)
                {
                    this._matrixColumnPrototypeIds = new Dictionary<int, int>();
                }
                return this._matrixColumnPrototypeIds;
            }
        }

        private Dictionary<int, string> ColumnTypes
        {
            get
            {
                if (this._matrixColumnTypes == null)
                {
                    this._matrixColumnTypes = new Dictionary<int, string>();
                }
                return this._matrixColumnTypes;
            }
        }

        public Prezza.Framework.Common.Coordinate Coordinate { get; set; }

        public bool IsAnswerable { get; set; }

        public bool IsScored { get; set; }

        public int ItemId { get; set; }

        public int ItemPosition { get; set; }

        public string ItemType { get; set; }

        public DateTime LastModified { get; set; }

        public List<int> Options
        {
            get
            {
                if (this._options == null)
                {
                    this._options = new List<int>();
                }
                return this._options;
            }
        }

        public int PagePosition { get; set; }

        public string PositionText
        {
            get
            {
                if ((this.PagePosition == 0) || (this.ItemPosition == 0))
                {
                    return string.Empty;
                }
                if (this.Coordinate == null)
                {
                    return string.Format("{0}.{1}", this.PagePosition, this.ItemPosition);
                }
                return string.Format("{0}.{1}.{2}.{3}", new object[] { this.PagePosition, this.ItemPosition, this.Coordinate.Y, this.Coordinate.X });
            }
        }

        public int RowCount
        {
            get
            {
                return this.RowTypes.Count;
            }
        }

        private Dictionary<int, int> RowKeyItems
        {
            get
            {
                if (this._matrixRowKeyItems == null)
                {
                    this._matrixRowKeyItems = new Dictionary<int, int>();
                }
                return this._matrixRowKeyItems;
            }
        }

        private Dictionary<int, string> RowTypes
        {
            get
            {
                if (this._matrixRowTypes == null)
                {
                    this._matrixRowTypes = new Dictionary<int, string>();
                }
                return this._matrixRowTypes;
            }
        }

        public int TemplateId { get; set; }

        private Dictionary<string, string> TextDictionary
        {
            get
            {
                if (this._textDictionary == null)
                {
                    this._textDictionary = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);
                }
                return this._textDictionary;
            }
        }
    }
}

