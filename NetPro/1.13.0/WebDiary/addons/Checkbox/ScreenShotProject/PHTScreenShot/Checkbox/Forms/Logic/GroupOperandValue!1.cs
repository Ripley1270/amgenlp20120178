﻿namespace Checkbox.Forms.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public abstract class GroupOperandValue<T> : OperandValue
    {
        protected GroupOperandValue()
        {
        }

        protected abstract int CompareTo(GroupOperandValue<T> other);
        public override int CompareTo(OperandValue other)
        {
            if (other is GroupOperandValue<T>)
            {
                return this.CompareTo((GroupOperandValue<T>) other);
            }
            return base.CompareTo(other);
        }

        public override void Initialize(object value)
        {
            if (value is List<T>)
            {
                this.Values = (List<T>) value;
            }
            else
            {
                List<T> list = new List<T> {
                    (T) value
                };
                this.Values = list;
            }
        }

        public override bool HasValue
        {
            get
            {
                return (this.Values.Count > 0);
            }
        }

        public List<T> Values { get; private set; }
    }
}

