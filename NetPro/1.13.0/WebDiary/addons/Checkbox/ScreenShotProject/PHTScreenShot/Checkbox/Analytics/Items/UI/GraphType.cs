﻿namespace Checkbox.Analytics.Items.UI
{
    using System;

    public enum GraphType
    {
        BarGraph = 5,
        ColumnGraph = 2,
        CrossTab = 6,
        Doughnut = 7,
        LineGraph = 4,
        PieGraph = 3,
        SummaryTable = 1
    }
}

