﻿namespace Checkbox.Forms.Logic.Configuration
{
    using Checkbox.Common;
    using Checkbox.Forms;
    using Checkbox.Forms.Logic;
    using Checkbox.Globalization.Text;
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Data;
    using System.Runtime.CompilerServices;
    using System.Text;

    [Serializable]
    public class ExpressionData : PersistedDomainObject
    {
        private ResponseTemplate _context;
        private OperandData _left;
        private LogicalOperator _operator;
        private OperandData _right;
        private static readonly object EventValidationFailed = new object();

        public event EventHandler ValidationFailed
        {
            add
            {
                base.Events.AddHandler(EventValidationFailed, value);
            }
            remove
            {
                base.Events.RemoveHandler(EventValidationFailed, value);
            }
        }

        public ExpressionData(ResponseTemplate context) : this(context, null, LogicalOperator.OperatorNotSpecified, null)
        {
        }

        public ExpressionData(ResponseTemplate context, OperandData left, LogicalOperator operation, OperandData right)
        {
            this._context = context;
            this._operator = operation;
            this._left = left;
            if (this._left != null)
            {
                this._left.ValidationFailed += new EventHandler(this._left_ValidationFailed);
            }
            this._right = right;
            if (this._right != null)
            {
                this._right.ValidationFailed += new EventHandler(this._right_ValidationFailed);
            }
        }

        private void _left_ValidationFailed(object sender, EventArgs e)
        {
            this.OnValidationFailed();
        }

        private void _right_ValidationFailed(object sender, EventArgs e)
        {
            this.OnValidationFailed();
        }

        protected override void Create(IDbTransaction transaction)
        {
            this.Left.Save(transaction);
            this.Right.Save(transaction);
            DataRow expressionDR = this._context.ExpressionTable.NewRow();
            this.UpdateExpressionRow(expressionDR);
            this._context.ExpressionTable.Rows.Add(expressionDR);
            base.ID = new int?((int) expressionDR["ExpressionID"]);
        }

        internal virtual Expression CreateExpression(Response response, string languageCode)
        {
            Operand left = this.Left.CreateOperand(response, languageCode);
            return new Expression(left, this.Right.CreateOperand(response, languageCode), this._operator);
        }

        public override void Delete(IDbTransaction transaction)
        {
            if (base.ID.HasValue)
            {
                if (this.Left != null)
                {
                    this.Left.Delete(transaction);
                }
                if (this.Right != null)
                {
                    this.Right.Delete(transaction);
                }
                DataRow[] rowArray = this._context.ExpressionTable.Select(this.IdentityColumnName + "=" + base.ID.Value);
                if (rowArray.Length > 0)
                {
                    rowArray[0].Delete();
                }
            }
        }

        public override DataSet GetConfigurationDataSet()
        {
            if (!base.ID.HasValue)
            {
                throw new ApplicationException("No Identity specified.");
            }
            try
            {
                string[] tableNames = new string[] { "Expression", "Operand" };
                Database database = DatabaseFactory.CreateDatabase();
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Rules_GetExpressionData");
                storedProcCommandWrapper.AddInParameter("ExpressionID", DbType.Int32, base.ID.Value);
                DataSet dataSet = new DataSet();
                database.LoadDataSet(storedProcCommandWrapper, dataSet, tableNames);
                return dataSet;
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessProtected"))
                {
                    throw;
                }
                return null;
            }
        }

        public override void Load(DataSet data)
        {
            try
            {
                if (!data.Tables.Contains(this.DataTableName))
                {
                    throw new ApplicationException("Cannot find DataTable for Expression");
                }
                DataRow[] rowArray = data.Tables[this.DataTableName].Select(this.IdentityColumnName + " = " + base.ID);
                if (rowArray.Length <= 0)
                {
                    throw new ApplicationException("Expression data set did not contain a row for the ExpressionData.");
                }
                int num = (int) rowArray[0]["Operator"];
                this._operator = (LogicalOperator) Enum.Parse(typeof(LogicalOperator), num.ToString());
                if (data.Tables.Contains("Operand"))
                {
                    DataTable table = data.Tables["Operand"];
                    DataRow row = table.Select("OperandID = " + ((int) rowArray[0]["LeftOperand"]))[0];
                    DataRow row2 = table.Select("OperandID = " + ((int) rowArray[0]["RightOperand"]))[0];
                    string typeName = ((string) row["TypeName"]) + "," + ((string) row["TypeAssembly"]);
                    string str2 = ((string) row2["TypeName"]) + "," + ((string) row2["TypeAssembly"]);
                    OperandDataFactory factory = new OperandDataFactory();
                    this._left = factory.CreateOperandData(typeName, new object[] { this._context });
                    this._left.ValidationFailed += new EventHandler(this._left_ValidationFailed);
                    this._right = factory.CreateOperandData(str2, new object[] { this._context });
                    this._right.ValidationFailed += new EventHandler(this._right_ValidationFailed);
                    this.Left.SetID = (int) row["OperandID"];
                    this.Right.SetID = (int) row2["OperandID"];
                    this.Left.Load(data);
                    this.Right.Load(data);
                }
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
            }
        }

        protected virtual void OnValidationFailed()
        {
            EventHandler handler = (EventHandler) base.Events[EventValidationFailed];
            if (handler != null)
            {
                handler(this, null);
            }
        }

        public override void Save()
        {
            this.Save(null);
        }

        public override void Save(IDbTransaction t)
        {
            if (base.ID.HasValue)
            {
                this.Update(t);
            }
            else
            {
                this.Create(t);
            }
            this.OnSaved();
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("'");
            builder.Append(this.Left.ToString());
            builder.Append("' ");
            builder.Append(TextManager.GetText("/enum/logicalOperator/" + this._operator, TextManager.DefaultLanguage));
            if ((this._operator != LogicalOperator.Answered) && (this._operator != LogicalOperator.NotAnswered))
            {
                builder.Append(" '");
                builder.Append(this.Right.ToString());
            }
            builder.Append("'");
            return builder.ToString();
        }

        protected override void Update(IDbTransaction transaction)
        {
            if (!base.ID.HasValue)
            {
                throw new Exception("Unable to update expression: Expression has no ID");
            }
            this.Left.Save(transaction);
            this.Right.Save(transaction);
            DataRow[] rowArray = this._context.ExpressionTable.Select("ExpressionID=" + base.ID);
            if (rowArray.Length > 0)
            {
                this.UpdateExpressionRow(rowArray[0]);
            }
        }

        private void UpdateExpressionRow(DataRow expressionDR)
        {
            expressionDR["Operator"] = (int) this._operator;
            expressionDR["LeftOperand"] = this.Left.ID;
            expressionDR["RightOperand"] = this.Right.ID;
            int num = 0;
            StringBuilder builder = new StringBuilder();
            builder.Append("/");
            CompositeExpressionData parent = this.Parent;
            CompositeExpressionData data2 = null;
            while (parent != null)
            {
                num++;
                builder.Insert(0, "/" + parent.ID.Value);
                if (parent.Parent == null)
                {
                    data2 = parent;
                }
                parent = parent.Parent;
            }
            if (this.Parent != null)
            {
                expressionDR["Parent"] = this.Parent.ID.Value;
            }
            expressionDR["Depth"] = num;
            expressionDR["Lineage"] = builder.ToString();
            if (data2 != null)
            {
                expressionDR["Root"] = data2.ID.Value;
            }
        }

        public virtual void Validate()
        {
            this.Left.Validate();
            this.Right.Validate();
        }

        public ResponseTemplate Context
        {
            get
            {
                return this._context;
            }
            set
            {
                this._context = value;
            }
        }

        public override string DataTableName
        {
            get
            {
                return "Expression";
            }
        }

        public override string IdentityColumnName
        {
            get
            {
                return "ExpressionID";
            }
        }

        public OperandData Left
        {
            get
            {
                return this._left;
            }
        }

        public string LeftOperandText
        {
            get
            {
                if (this.Left != null)
                {
                    return this.Left.ToString();
                }
                return string.Empty;
            }
        }

        public LogicalOperator Operator
        {
            get
            {
                return this._operator;
            }
        }

        public string OperatorText
        {
            get
            {
                return TextManager.GetText("/enum/logicalOperator/" + this._operator, TextManager.DefaultLanguage);
            }
        }

        public virtual CompositeExpressionData Parent { get; set; }

        public OperandData Right
        {
            get
            {
                return this._right;
            }
        }

        public string RightOperandText
        {
            get
            {
                if (this.Right != null)
                {
                    return this.Right.ToString();
                }
                return string.Empty;
            }
        }

        internal int SetID
        {
            set
            {
                base.ID = new int?(value);
            }
        }

        public string Text
        {
            get
            {
                return this.ToString();
            }
        }
    }
}

