﻿namespace Checkbox.Forms.Validation
{
    using Checkbox.Common;
    using Checkbox.Forms.Items;
    using Checkbox.Globalization.Text;
    using System;
    using System.Runtime.CompilerServices;

    public class UploadItemValidator : Validator<UploadItem>
    {
        public override string GetMessage(string languageCode)
        {
            return this.ErrorMessage;
        }

        public override bool Validate(UploadItem input)
        {
            this.ErrorMessage = string.Empty;
            if (!this.ValidateHasAnswer(input))
            {
                return false;
            }
            return this.ValidateFileType(input);
        }

        private bool ValidateFileType(UploadItem input)
        {
            if (!input.HasAnswer)
            {
                return true;
            }
            if (Utilities.IsNotNullOrEmpty(input.FileType))
            {
                foreach (string str in input.AllowedFileTypes)
                {
                    if (str.Equals(input.FileType, StringComparison.InvariantCultureIgnoreCase))
                    {
                        return true;
                    }
                }
            }
            this.ErrorMessage = TextManager.GetText("/validationMessages/text/uploadItemInvalidFileType", input.LanguageCode);
            return false;
        }

        protected virtual bool ValidateHasAnswer(UploadItem input)
        {
            if (input.Required)
            {
                RequiredItemValidator validator = new RequiredItemValidator();
                if (!validator.Validate(input))
                {
                    this.ErrorMessage = validator.GetMessage(input.LanguageCode);
                    return false;
                }
            }
            return true;
        }

        protected string ErrorMessage { get; set; }
    }
}

