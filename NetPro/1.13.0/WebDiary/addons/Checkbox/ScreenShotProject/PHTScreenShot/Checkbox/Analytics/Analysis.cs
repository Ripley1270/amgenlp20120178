﻿namespace Checkbox.Analytics
{
    using Checkbox.Analytics.Data;
    using Checkbox.Analytics.Filters;
    using Checkbox.Analytics.Items;
    using Checkbox.Forms.Items;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class Analysis
    {
        private AnalysisAnswerData _answerData;
        private readonly Dictionary<int, Item> _items = new Dictionary<int, Item>();
        private readonly List<AnalysisPage> _pages = new List<AnalysisPage>();
        private readonly List<int> _responseTemplateIDs;
        private DateTime? _resultCacheReferenceDate;
        private bool _skipFreshnessChecks;

        public Analysis()
        {
            this.Filters = new AnalysisFilterCollection();
            this._responseTemplateIDs = new List<int>();
        }

        public void AddItem(Item item)
        {
            if (item != null)
            {
                this._items[item.ID] = item;
            }
        }

        public void AddPage(AnalysisPage page)
        {
            this._pages.Add(page);
        }

        public void AddResponseTemplateID(int responseTemplateID)
        {
            if (!this._responseTemplateIDs.Contains(responseTemplateID))
            {
                this._responseTemplateIDs.Add(responseTemplateID);
            }
        }

        public Item GetItem(int itemID)
        {
            if (this._items.ContainsKey(itemID))
            {
                return this._items[itemID];
            }
            return null;
        }

        public void Initialize(AnalysisFilterCollection filterCollection, string languageCode, DateTime? startFilter, DateTime? endFilter, bool includeIncompleteResponses, string progressKey, DateTime? analysisModifiedDate)
        {
            this.LanguageCode = languageCode;
            this.Filters = filterCollection;
            DateTime? surveyReferenceDate = AnalysisDataProxy.GetSurveyReferenceDate(this.ResponseTemplateIDs);
            if (surveyReferenceDate.HasValue && !analysisModifiedDate.HasValue)
            {
                this._resultCacheReferenceDate = surveyReferenceDate;
            }
            else if (!surveyReferenceDate.HasValue && analysisModifiedDate.HasValue)
            {
                this._resultCacheReferenceDate = analysisModifiedDate;
            }
            else if (!surveyReferenceDate.HasValue)
            {
                this._resultCacheReferenceDate = null;
            }
            else
            {
                this._resultCacheReferenceDate = (surveyReferenceDate.Value > analysisModifiedDate.Value) ? surveyReferenceDate : analysisModifiedDate;
            }
            this._skipFreshnessChecks = true;
            foreach (Item item in this._items.Values)
            {
                if (item is AnalysisItem)
                {
                    ((AnalysisItem) item).Analysis = this;
                    ((AnalysisItem) item).ResultValidationReferenceDate = this._resultCacheReferenceDate;
                    if (this._skipFreshnessChecks)
                    {
                        this._skipFreshnessChecks = this._skipFreshnessChecks && ((AnalysisItem) item).TemplatesValidated;
                    }
                }
            }
            this.MinResponseDate = startFilter;
            this.MaxResponseDate = endFilter;
            this.IncludeIncompleteResponses = includeIncompleteResponses;
            this.ProgressKey = progressKey;
        }

        private AnalysisAnswerData LoadAnswerData(bool includeIncompleteResponses)
        {
            AnalysisAnswerData data = new AnalysisAnswerData(this._skipFreshnessChecks);
            data.Initialize(this.LanguageCode);
            data.Load(this.ResponseTemplateIDs, this.Filters, this.MinResponseDate, this.MaxResponseDate, includeIncompleteResponses, this.ProgressKey);
            return data;
        }

        public void SetId(int analysisId)
        {
            this.ID = analysisId;
            if (this.Filters != null)
            {
                this.Filters.ParentID = analysisId;
            }
        }

        public AnalysisAnswerData Data
        {
            get
            {
                if (this._answerData == null)
                {
                    this._answerData = this.LoadAnswerData(this.IncludeIncompleteResponses);
                }
                return this._answerData;
            }
        }

        public AnalysisFilterCollection Filters { get; private set; }

        public int ID { get; private set; }

        public bool IncludeIncompleteResponses { get; private set; }

        public ReadOnlyCollection<Item> Items
        {
            get
            {
                return new ReadOnlyCollection<Item>(new List<Item>(this._items.Values));
            }
        }

        public string LanguageCode { get; private set; }

        public DateTime? MaxResponseDate { get; set; }

        public DateTime? MinResponseDate { get; set; }

        public string Name { get; set; }

        public ReadOnlyCollection<AnalysisPage> Pages
        {
            get
            {
                return new ReadOnlyCollection<AnalysisPage>(this._pages);
            }
        }

        public string ProgressKey { get; private set; }

        public List<int> ResponseTemplateIDs
        {
            get
            {
                return this._responseTemplateIDs;
            }
        }
    }
}

