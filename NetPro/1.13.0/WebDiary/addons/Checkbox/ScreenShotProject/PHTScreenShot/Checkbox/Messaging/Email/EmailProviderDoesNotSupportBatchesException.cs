﻿namespace Checkbox.Messaging.Email
{
    using System;

    public class EmailProviderDoesNotSupportBatchesException : Exception
    {
        public override string Message
        {
            get
            {
                return ("This email provider [" + base.GetType().AssemblyQualifiedName + "] does not support batch operations.");
            }
        }
    }
}

