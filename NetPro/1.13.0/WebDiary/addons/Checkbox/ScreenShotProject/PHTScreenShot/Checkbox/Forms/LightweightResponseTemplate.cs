﻿namespace Checkbox.Forms
{
    using Checkbox.Forms.Security;
    using Prezza.Framework.Data.Sprocs;
    using Prezza.Framework.Security;
    using System;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable, FetchProcedure("ckbx_RT_GetAccessControllableResource")]
    public class LightweightResponseTemplate : LightweightAccessControllable
    {
        internal LightweightResponseTemplate(int id)
        {
            this.ID = id;
        }

        public override Policy CreatePolicy(string[] permissions)
        {
            return new FormPolicy(permissions);
        }

        public override SecurityEditor GetEditor()
        {
            return new FormSecurityEditor(this);
        }

        [FetchParameter(Name="ActivationEnd", DbType=DbType.DateTime, Direction=ParameterDirection.ReturnValue)]
        public DateTime? ActivationEndDate { get; set; }

        [FetchParameter(Name="ActivationStart", DbType=DbType.DateTime, Direction=ParameterDirection.ReturnValue)]
        public DateTime? ActivationStartDate { get; set; }

        [FetchParameter(Name="AllowEdit", DbType=DbType.Boolean, Direction=ParameterDirection.ReturnValue)]
        public bool AllowEdit { get; set; }

        [FetchParameter(Name="AllowSurveyEditWhileActive", DbType=DbType.Boolean, Direction=ParameterDirection.ReturnValue)]
        public bool AllowEditWhileActive { get; set; }

        [FetchParameter(Name="AnonymizeResponses", DbType=DbType.Boolean, Direction=ParameterDirection.ReturnValue)]
        public bool AnonymizeResponses { get; set; }

        [FetchParameter(Name="DefaultLanguage", DbType=DbType.String, Direction=ParameterDirection.ReturnValue)]
        public string DefaultLanguage { get; set; }

        [FetchParameter(Name="GUID", DbType=DbType.Guid, Direction=ParameterDirection.ReturnValue)]
        public Guid GUID { get; set; }

        [FetchParameter(Name="TemplateID", DbType=DbType.Int32, Direction=ParameterDirection.Input)]
        public int ID { get; set; }

        [FetchParameter(Name="IsActive", DbType=DbType.Boolean, Direction=ParameterDirection.ReturnValue)]
        public bool IsActive { get; set; }

        [FetchParameter(Name="MaxResponsesPerUser", DbType=DbType.Int32, Direction=ParameterDirection.ReturnValue)]
        public int MaxResponsesPerUser { get; set; }

        [FetchParameter(Name="MaxTotalResponses", DbType=DbType.Int32, Direction=ParameterDirection.ReturnValue)]
        public int MaxTotalResponses { get; set; }

        [FetchParameter(Name="Name", DbType=DbType.String, Direction=ParameterDirection.ReturnValue)]
        public string Name { get; set; }

        public Checkbox.Forms.SecurityType SecurityType
        {
            get
            {
                return (Checkbox.Forms.SecurityType) Enum.ToObject(typeof(Checkbox.Forms.SecurityType), this.SecurityTypeValue);
            }
        }

        [FetchParameter(Name="SecurityTypeValue", DbType=DbType.Int32, Direction=ParameterDirection.ReturnValue)]
        public int SecurityTypeValue { get; set; }

        [FetchParameter(Name="StyleTemplateId", DbType=DbType.Int32, Direction=ParameterDirection.ReturnValue)]
        public int? StyleTemplateId { get; set; }

        public string[] SupportedLanguages
        {
            get
            {
                return this.SupportedLanguagesString.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            }
        }

        [FetchParameter(Name="SupportedLanguagesString", DbType=DbType.String, Direction=ParameterDirection.ReturnValue)]
        public string SupportedLanguagesString { get; set; }

        public override string[] SupportedPermissionMasks
        {
            get
            {
                return new string[] { "Form.Administer", "Form.Edit", "Form.Fill", "Analysis.ViewResponses", "Analysis.Analyze" };
            }
        }

        public override string[] SupportedPermissions
        {
            get
            {
                return new string[] { "Form.Administer", "Form.Create", "Form.Delete", "Form.Edit", "Form.Fill", "Analysis.Create", "Analysis.Responses.View", "Analysis.Responses.Export", "Analysis.Responses.Edit", "Analysis.ManageFilters" };
            }
        }

        [FetchParameter(Name="TitleTextId", DbType=DbType.String, Direction=ParameterDirection.ReturnValue)]
        public string TitleTextId { get; set; }
    }
}

