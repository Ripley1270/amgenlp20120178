﻿namespace Checkbox.Forms.Items.Configuration
{
    using Checkbox.Forms.Items;
    using Checkbox.Forms.Items.UI;
    using Checkbox.Globalization.Text;
    using Prezza.Framework.Common;
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;

    [Serializable]
    public class MatrixItemData : TabularItemData
    {
        private readonly Dictionary<int, ItemData> _columnPrototypes;
        private const string _columnProtoypeItemTableName = "Items";
        private const string _columnTypesTableName = "ColumnTypes";
        private readonly List<int> _deletedItemIDs;
        private bool _dirty;
        private bool _imported;
        private const string _itemPositionsTableName = "ItemPositions";
        private DataSet _matrixData;
        private Dictionary<Coordinate, ItemData> _matrixItems;
        private int _pkIndex;
        private const string _rowDataTableName = "RowData";
        private List<ItemData> _savedItems;

        public MatrixItemData()
        {
            this.InitializeItemData();
            this._deletedItemIDs = new List<int>();
            this._columnPrototypes = new Dictionary<int, ItemData>();
            this._matrixItems = new Dictionary<Coordinate, ItemData>(new CoordinateComparer());
            this._dirty = true;
            this._pkIndex = 1;
            this.AddPKColumn();
            this._imported = false;
        }

        public int AddColumn(ItemData itemData)
        {
            return this.AddColumn(itemData, this.GetNextColumnPosition());
        }

        public int AddColumn(ItemData itemData, int position)
        {
            int nextTempItemID;
            if ((this._columnPrototypes.ContainsKey(position) || (itemData == null)) || ((position < 1) || (position > this.GetNextColumnPosition())))
            {
                return -1;
            }
            if (!itemData.ID.HasValue || (itemData.ID <= 0))
            {
                nextTempItemID = this.GetNextTempItemID();
                itemData.ID = new int?(nextTempItemID);
            }
            else
            {
                nextTempItemID = itemData.ID.Value;
            }
            this._matrixData.Tables["ColumnTypes"].Rows.Add(new object[] { base.ID, position, nextTempItemID });
            this._columnPrototypes[position] = itemData;
            this.FillPositionTable();
            this._dirty = true;
            return position;
        }

        private void AddPKColumn()
        {
            this._matrixData.Tables["ColumnTypes"].Rows.Add(new object[] { base.ID, this._pkIndex, -1 });
        }

        public int AddRow()
        {
            return this.AddRow(string.Empty, false, false, this.GetNextRowPosition());
        }

        public int AddRow(string alias)
        {
            return this.AddRow(alias, false, false, this.GetNextRowPosition());
        }

        public int AddRow(string alias, bool isSubheading)
        {
            return this.AddRow(alias, isSubheading, false, this.GetNextRowPosition());
        }

        public int AddRow(string alias, bool isSubHeader, bool isOther)
        {
            return this.AddRow(alias, isSubHeader, isOther, this.GetNextRowPosition());
        }

        public int AddRow(string alias, bool isSubHeader, int position)
        {
            return this.AddRow(alias, isSubHeader, false, position);
        }

        public int AddRow(string alias, bool isSubHeader, bool isOther, int position)
        {
            if ((position <= 0) || (position > this.GetNextRowPosition()))
            {
                return -1;
            }
            if (isSubHeader && isOther)
            {
                return -1;
            }
            this.RemoveRow(position);
            string filter = "Row >= " + position;
            this.AddValueToTableData("ItemPositions", "Row", 1, filter);
            this.AddValueToTableData("RowData", "Row", 1, filter);
            string str2 = isOther ? "SingleLineText" : "Message";
            int nextTempItemID = this.GetNextTempItemID();
            this._matrixData.Tables["ItemPositions"].Rows.Add(new object[] { base.ID, position, this._pkIndex, nextTempItemID, str2 });
            this._matrixData.Tables["RowData"].Rows.Add(new object[] { base.ID, position, alias, isSubHeader, isOther });
            this.FillPositionTable();
            this._dirty = true;
            return position;
        }

        private void AddValueToTableData(string tableName, string fieldName, int value, string filter)
        {
            if (((this._matrixData != null) && this._matrixData.Tables.Contains(tableName)) && this._matrixData.Tables[tableName].Columns.Contains(fieldName))
            {
                foreach (DataRow row in this._matrixData.Tables[tableName].Select(filter, fieldName + " ASC", DataViewRowState.CurrentRows))
                {
                    row[fieldName] = Convert.ToInt32(row[fieldName]) + value;
                }
            }
        }

        protected override void Create(IDbTransaction t)
        {
            base.Create(t);
            if (base.ID <= 0)
            {
                throw new ApplicationException("No DataID specified.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_InsertMatrix");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("TextID", DbType.String, base.TextID);
            storedProcCommandWrapper.AddInParameter("SubTextID", DbType.String, base.SubTextID);
            storedProcCommandWrapper.AddInParameter("IsRequired", DbType.Boolean, base.IsRequired);
            storedProcCommandWrapper.AddInParameter("PKIndex", DbType.Int32, this._pkIndex);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
            this.SaveItems(t);
            if (this._imported)
            {
                this.SaveCategoryTexts();
                this.SaveColumnTexts();
            }
        }

        protected override void CreateDataRelationsInternal(DataSet ds)
        {
            base.CreateDataRelationsInternal(ds);
            if (!ds.Relations.Contains(this.DataTableName + "_ItemPositions"))
            {
                DataRelation relation = new DataRelation(this.DataTableName + "_ItemPositions", ds.Tables[this.DataTableName].Columns[this.IdentityColumnName], ds.Tables["ItemPositions"].Columns["MatrixID"]);
                ds.Tables["ItemPositions"].Constraints.Add(new ForeignKeyConstraint(ds.Tables[this.DataTableName].Columns[this.IdentityColumnName], ds.Tables["ItemPositions"].Columns["MatrixID"]));
                relation.Nested = true;
                ds.Relations.Add(relation);
            }
            if (!ds.Relations.Contains(this.DataTableName + "_ColumnTypes"))
            {
                DataRelation relation2 = new DataRelation(this.DataTableName + "_ColumnTypes", ds.Tables[this.DataTableName].Columns[this.IdentityColumnName], ds.Tables["ColumnTypes"].Columns["MatrixID"]);
                ds.Tables["ColumnTypes"].Constraints.Add(new ForeignKeyConstraint(ds.Tables[this.DataTableName].Columns[this.IdentityColumnName], ds.Tables["ColumnTypes"].Columns["MatrixID"]));
                relation2.Nested = true;
                ds.Relations.Add(relation2);
            }
            if (!ds.Relations.Contains(this.DataTableName + "_RowData"))
            {
                DataRelation relation3 = new DataRelation(this.DataTableName + "_RowData", ds.Tables[this.DataTableName].Columns[this.IdentityColumnName], ds.Tables["RowData"].Columns["MatrixID"]);
                ds.Tables["RowData"].Constraints.Add(new ForeignKeyConstraint(ds.Tables[this.DataTableName].Columns[this.IdentityColumnName], ds.Tables["RowData"].Columns["MatrixID"]));
                relation3.Nested = true;
                ds.Relations.Add(relation3);
            }
            if (!ds.Relations.Contains("Items_MatrixColumnPrototypes") && ds.Tables.Contains("Items"))
            {
                DataRelation relation4 = new DataRelation("Items_MatrixColumnPrototypes", ds.Tables["Items"].Columns["ItemID"], ds.Tables["ColumnTypes"].Columns["ColumnPrototypeID"]);
                ds.Relations.Add(relation4);
            }
            this.CreateTextDataRelations(ds);
        }

        protected override Item CreateItem()
        {
            return new MatrixItem();
        }

        public override void CreateTextDataRelations(DataSet ds)
        {
            base.CreateTextDataRelations(ds);
            if (!ds.Relations.Contains("ItemPositions_" + this.CategoryTextDataTableName) && ds.Tables.Contains(this.CategoryTextDataTableName))
            {
                DataRelation relation = new DataRelation("ItemPositions_" + this.CategoryTextDataTableName, ds.Tables["ItemPositions"].Columns["ItemID"], ds.Tables[this.CategoryTextDataTableName].Columns["ItemID"]);
                ds.Relations.Add(relation);
            }
            foreach (ItemData data in this._columnPrototypes.Values)
            {
                if (data is LocalizableResponseItemData)
                {
                    ((LocalizableResponseItemData) data).CreateTextDataRelations(ds);
                }
            }
        }

        public override ItemTextDecorator CreateTextDecorator(string languageCode)
        {
            return new MatrixItemTextDecorator(this, languageCode);
        }

        private void FillPositionTable()
        {
            foreach (DataRow row in this._matrixData.Tables["RowData"].Select(null, "Row ASC", DataViewRowState.CurrentRows))
            {
                int num = Convert.ToInt32(row["Row"]);
                bool flag = (row["IsSubheading"] != DBNull.Value) ? Convert.ToBoolean(row["IsSubheading"]) : false;
                foreach (DataRow row2 in this._matrixData.Tables["ColumnTypes"].Select(null, "Column ASC", DataViewRowState.CurrentRows))
                {
                    int key = Convert.ToInt32(row2["Column"]);
                    ItemData data = null;
                    if (this._columnPrototypes.ContainsKey(key))
                    {
                        data = this._columnPrototypes[key];
                    }
                    if (data != null)
                    {
                        DataRow[] rowArray3 = this._matrixData.Tables["ItemPositions"].Select(string.Concat(new object[] { "Row = ", num, " AND Column = ", key }), null, DataViewRowState.CurrentRows);
                        if (flag)
                        {
                            if (key != this._pkIndex)
                            {
                                foreach (DataRow row3 in rowArray3)
                                {
                                    row3.Delete();
                                }
                            }
                        }
                        else if (rowArray3.Length > 0)
                        {
                            foreach (DataRow row4 in rowArray3)
                            {
                                if (row4["ItemTypeName"] != DBNull.Value)
                                {
                                    string str = (string) row4["ItemTypeName"];
                                    if (str != data.ItemTypeName)
                                    {
                                        row4["ItemTypeName"] = data.ItemTypeName;
                                    }
                                }
                            }
                        }
                        else
                        {
                            DataRow row5 = this._matrixData.Tables["ItemPositions"].NewRow();
                            row5["Row"] = num;
                            row5["Column"] = key;
                            row5["ItemID"] = this.GetNextTempItemID();
                            row5["ItemTypeName"] = data.ItemTypeName;
                            this._matrixData.Tables["ItemPositions"].Rows.Add(row5);
                        }
                    }
                }
            }
        }

        public override ReadOnlyCollection<int> GetChildItemDataIDs()
        {
            List<int> list = new List<int>();
            for (int i = 1; i <= this.RowCount; i++)
            {
                for (int j = 1; j <= this.ColumnCount; j++)
                {
                    int? itemIDAt = this.GetItemIDAt(i, j);
                    if (itemIDAt.HasValue)
                    {
                        list.Add(itemIDAt.Value);
                    }
                }
            }
            return new ReadOnlyCollection<int>(list);
        }

        public override ReadOnlyCollection<ItemData> GetChildItemDatas()
        {
            List<ItemData> list = new List<ItemData>();
            for (int i = 1; i <= this.RowCount; i++)
            {
                for (int j = 1; j <= this.ColumnCount; j++)
                {
                    ItemData itemAt = this.GetItemAt(i, j);
                    if (itemAt != null)
                    {
                        list.Add(itemAt);
                    }
                }
            }
            return new ReadOnlyCollection<ItemData>(list);
        }

        public ItemData GetColumnPrototype(int position)
        {
            if ((((position >= 1) && (position < this.GetNextColumnPosition())) && (position != this._pkIndex)) && this._columnPrototypes.ContainsKey(position))
            {
                return this._columnPrototypes[position];
            }
            return null;
        }

        public int GetColumnPrototypeID(int position)
        {
            if (this._columnPrototypes.ContainsKey(position))
            {
                return this._columnPrototypes[position].ID.Value;
            }
            return -1;
        }

        public bool GetColumnUniqueness(int column)
        {
            DataRow[] rowArray = this._matrixData.Tables["ColumnTypes"].Select("Column = " + column, null, DataViewRowState.CurrentRows);
            return (((rowArray.Length > 0) && (rowArray[0]["UniqueAnswers"] != DBNull.Value)) && ((bool) rowArray[0]["UniqueAnswers"]));
        }

        public int? GetColumnWidth(int column)
        {
            DataRow[] rowArray = this._matrixData.Tables["ColumnTypes"].Select("Column = " + column, null, DataViewRowState.CurrentRows);
            if ((rowArray.Length > 0) && (rowArray[0]["Width"] != DBNull.Value))
            {
                return new int?((int) rowArray[0]["Width"]);
            }
            return null;
        }

        protected override DataSet GetConcreteConfigurationDataSet()
        {
            try
            {
                if (base.ID <= 0)
                {
                    throw new Exception("DataID must be > 0 to load matrix.");
                }
                string[] tableNames = new string[] { this.DataTableName, "ItemPositions", "ColumnTypes", "RowData", "Items" };
                Database database = DatabaseFactory.CreateDatabase();
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_GetMatrix");
                storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
                DataSet concreteConfigurationDataSet = base.GetConcreteConfigurationDataSet();
                database.LoadDataSet(storedProcCommandWrapper, concreteConfigurationDataSet, tableNames);
                return concreteConfigurationDataSet;
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessProtected"))
                {
                    throw;
                }
                return null;
            }
        }

        public override ItemData GetItemAt(int row, int column)
        {
            return this.GetItemAt(row, column, true);
        }

        protected virtual ItemData GetItemAt(int row, int column, bool useInternalCollection)
        {
            if (!this._dirty && useInternalCollection)
            {
                Coordinate key = new Coordinate(row, column);
                if (!this._matrixItems.ContainsKey(key))
                {
                    return null;
                }
                return this._matrixItems[key];
            }
            if (this.IsRowSubheading(row) && (column != this._pkIndex))
            {
                return null;
            }
            int? itemIDAt = this.GetItemIDAt(row, column);
            ItemData configurationData = null;
            ItemData data2 = null;
            if (this._columnPrototypes.ContainsKey(column) && (column != this._pkIndex))
            {
                data2 = this._columnPrototypes[column];
            }
            if (itemIDAt.HasValue && (itemIDAt > 0))
            {
                configurationData = ItemConfigurationManager.GetConfigurationData(itemIDAt.Value);
                if ((configurationData is SelectItemData) && (data2 is SelectItemData))
                {
                    SyncOptions((SelectItemData) data2, (SelectItemData) configurationData);
                }
                if ((configurationData is SingleLineTextItemData) && (data2 is SingleLineTextItemData))
                {
                    ((SingleLineTextItemData) configurationData).IsRequired = ((SingleLineTextItemData) data2).IsRequired;
                    ((SingleLineTextItemData) configurationData).Format = ((SingleLineTextItemData) data2).Format;
                    ((SingleLineTextItemData) configurationData).CustomFormatId = ((SingleLineTextItemData) data2).CustomFormatId;
                    ((SingleLineTextItemData) configurationData).MaxLength = ((SingleLineTextItemData) data2).MaxLength;
                    ((SingleLineTextItemData) configurationData).MinValue = ((SingleLineTextItemData) data2).MinValue;
                    ((SingleLineTextItemData) configurationData).MaxValue = ((SingleLineTextItemData) data2).MaxValue;
                }
            }
            if (configurationData == null)
            {
                if (column == this._pkIndex)
                {
                    DataRow[] rowArray = this._matrixData.Tables["ItemPositions"].Select(string.Concat(new object[] { "Row = ", row, " AND Column = ", column }), null, DataViewRowState.CurrentRows);
                    if ((rowArray.Length > 0) && (rowArray[0]["ItemTypeName"] != DBNull.Value))
                    {
                        configurationData = ItemConfigurationManager.CreateConfigurationData(rowArray[0]["ItemTypeName"].ToString());
                    }
                    if (configurationData == null)
                    {
                        configurationData = ItemConfigurationManager.CreateConfigurationData("Message");
                    }
                    return configurationData;
                }
                if (data2 != null)
                {
                    configurationData = data2.Copy(null);
                }
            }
            return configurationData;
        }

        public override Coordinate GetItemCoordinate(ItemData item)
        {
            if (item == null)
            {
                return null;
            }
            return this.GetItemCoordinate(item.ID.Value);
        }

        public Coordinate GetItemCoordinate(int itemID)
        {
            DataRow[] rowArray = this._matrixData.Tables["ItemPositions"].Select("ItemID = " + itemID, null, DataViewRowState.CurrentRows);
            if (rowArray.Length > 0)
            {
                int x = (int) rowArray[0]["Column"];
                return new Coordinate(x, (int) rowArray[0]["Row"]);
            }
            return null;
        }

        public int? GetItemIDAt(int row, int column)
        {
            if (((row >= 1) && (row <= this.RowCount)) && ((column >= 1) && (column <= this.ColumnCount)))
            {
                DataRow[] rowArray = this._matrixData.Tables["ItemPositions"].Select(string.Concat(new object[] { "Row = ", row, " AND Column = ", column }), null, DataViewRowState.CurrentRows);
                if (rowArray.Length > 0)
                {
                    return (int?) rowArray[0]["ItemID"];
                }
            }
            return null;
        }

        private int GetNextColumnPosition()
        {
            lock (this._matrixData)
            {
                return (this._matrixData.Tables["ColumnTypes"].Select(null, null, DataViewRowState.CurrentRows).Length + 1);
            }
        }

        private int GetNextRowPosition()
        {
            lock (this._matrixData)
            {
                return (this._matrixData.Tables["RowData"].Select(null, null, DataViewRowState.CurrentRows).Length + 1);
            }
        }

        private int GetNextTempItemID()
        {
            DataRow[] rowArray = this._matrixData.Tables["ItemPositions"].Select(null, "ItemID ASC", DataViewRowState.CurrentRows);
            DataRow[] rowArray2 = this._matrixData.Tables["ColumnTypes"].Select(null, "ColumnPrototypeID ASC", DataViewRowState.CurrentRows);
            int num = 0;
            int num2 = 0;
            if (rowArray.Length > 0)
            {
                num = Convert.ToInt32(rowArray[0]["ItemID"]);
            }
            if (rowArray2.Length > 0)
            {
                num2 = Convert.ToInt32(rowArray2[0]["ColumnPrototypeID"]);
            }
            int num3 = (num < num2) ? num : num2;
            if (num3 >= 0)
            {
                return -1;
            }
            num3--;
            return num3;
        }

        public string GetRowAlias(int position)
        {
            return this.GetRowProp(position, "Alias").ToString();
        }

        protected object GetRowProp(int position, string propName)
        {
            if ((position < 1) || (position >= this.GetNextRowPosition()))
            {
                return string.Empty;
            }
            DataRow[] rowArray = this._matrixData.Tables["RowData"].Select("Row = " + position, null, DataViewRowState.CurrentRows);
            if (rowArray.Length == 0)
            {
                return string.Empty;
            }
            return rowArray[0][propName];
        }

        private void InitializeItemData()
        {
            this._matrixData = new DataSet();
            DataTable table = new DataTable {
                TableName = "ItemPositions"
            };
            table.Columns.Add("MatrixID", typeof(int));
            table.Columns.Add("Row", typeof(int));
            table.Columns.Add("Column", typeof(int));
            table.Columns.Add("ItemID", typeof(int));
            table.Columns.Add("ItemTypeName", typeof(string));
            DataTable table2 = new DataTable {
                TableName = "ColumnTypes"
            };
            table2.Columns.Add("MatrixID", typeof(int));
            table2.Columns.Add("Column", typeof(int));
            table2.Columns.Add("ColumnPrototypeID", typeof(int));
            table2.Columns.Add("UniqueAnswers", typeof(bool));
            table2.Columns.Add("Width", typeof(int));
            DataTable table3 = new DataTable {
                TableName = "RowData"
            };
            table3.Columns.Add("MatrixID", typeof(int));
            table3.Columns.Add("Row", typeof(int));
            table3.Columns.Add("Alias", typeof(string));
            table3.Columns.Add("IsSubHeading", typeof(bool));
            table3.Columns.Add("IsOther", typeof(bool));
            DataTable table4 = new DataTable {
                TableName = this.CategoryTextDataTableName
            };
            table4.Columns.Add("ItemID", typeof(int));
            table4.Columns.Add("TextIDSuffix", typeof(string));
            table4.Columns.Add("TextIDPrefix", typeof(string));
            table4.Columns.Add("ComputedTextID", typeof(string));
            table4.Columns.Add("TextID", typeof(string));
            table4.Columns.Add("LanguageCode", typeof(string));
            table4.Columns.Add("TextValue", typeof(string));
            table4.Columns["ComputedTextID"].Expression = "Iif(ItemID IS NULL OR TextIDSuffix IS NULL OR TextIDPrefix IS NULL, '', '/' + TextIDPrefix + '/' + ItemID + '/' + TextIDSuffix)";
            DataTable table5 = new DataTable {
                TableName = this.ColumnTextDataTableName
            };
            table5.Columns.Add("ItemID", typeof(int));
            table5.Columns.Add("TextIDSuffix", typeof(string));
            table5.Columns.Add("TextIDPrefix", typeof(string));
            table5.Columns.Add("ComputedTextID", typeof(string));
            table5.Columns.Add("TextID", typeof(string));
            table5.Columns.Add("LanguageCode", typeof(string));
            table5.Columns.Add("TextValue", typeof(string));
            table5.Columns["ComputedTextID"].Expression = "Iif(ItemID IS NULL OR TextIDSuffix IS NULL OR TextIDPrefix IS NULL, '', '/' + TextIDPrefix + '/' + ItemID + '/' + TextIDSuffix)";
            this._matrixData.Tables.Add(table);
            this._matrixData.Tables.Add(table2);
            this._matrixData.Tables.Add(table3);
            this._matrixData.Tables.Add(table4);
            this._matrixData.Tables.Add(table5);
            if (!this._matrixData.Relations.Contains("ItemPositions_" + this.CategoryTextDataTableName) && this._matrixData.Tables.Contains(this.CategoryTextDataTableName))
            {
                DataRelation relation = new DataRelation("ItemPositions_" + this.CategoryTextDataTableName, this._matrixData.Tables["ItemPositions"].Columns["ItemID"], this._matrixData.Tables[this.CategoryTextDataTableName].Columns["ItemID"]);
                this._matrixData.Relations.Add(relation);
            }
            if (!this._matrixData.Relations.Contains("ColumnTypes_" + this.ColumnTextDataTableName) && this._matrixData.Tables.Contains(this.ColumnTextDataTableName))
            {
                DataRelation relation2 = new DataRelation("ColumnTypes_" + this.ColumnTextDataTableName, this._matrixData.Tables["ColumnTypes"].Columns["ColumnPrototypeID"], this._matrixData.Tables[this.ColumnTextDataTableName].Columns["ItemID"]);
                this._matrixData.Relations.Add(relation2);
            }
        }

        public bool IsRowOther(int row)
        {
            DataRow[] rowArray = this._matrixData.Tables["RowData"].Select("Row = " + row, null, DataViewRowState.CurrentRows);
            return (((rowArray.Length > 0) && (rowArray[0]["IsOther"] != DBNull.Value)) && Convert.ToBoolean(rowArray[0]["IsOther"]));
        }

        public bool IsRowSubheading(int row)
        {
            DataRow[] rowArray = this._matrixData.Tables["RowData"].Select("Row = " + row, null, DataViewRowState.CurrentRows);
            return (((rowArray.Length > 0) && (rowArray[0]["IsSubheading"] != DBNull.Value)) && Convert.ToBoolean(rowArray[0]["IsSubheading"]));
        }

        public override void Load(DataSet data)
        {
            base.Load(data);
            if (data == null)
            {
                throw new Exception("DataSet cannot be null.");
            }
            try
            {
                if (!base.ID.HasValue || (base.ID < 0))
                {
                    this._imported = true;
                }
                if ((((data.Tables.Count < 5) || !data.Tables.Contains(this.DataTableName)) || (!data.Tables.Contains("ItemPositions") || !data.Tables.Contains("ColumnTypes"))) || (!data.Tables.Contains("RowData") || !data.Tables.Contains("Items")))
                {
                    throw new Exception("Matrix dataset is incomplete.");
                }
                DataRow[] rowArray = data.Tables[this.DataTableName].Select(this.IdentityColumnName + " = " + base.ID);
                if (rowArray.Length <= 0)
                {
                    throw new Exception("Matrix data set did not contain a row for the matrix.");
                }
                this.LoadFromDataRow(rowArray[0]);
                this.InitializeItemData();
                foreach (DataRow row in data.Tables["ItemPositions"].Select("MatrixID = " + base.ID, null, DataViewRowState.CurrentRows))
                {
                    this._matrixData.Tables["ItemPositions"].ImportRow(row);
                }
                foreach (DataRow row2 in data.Tables["ColumnTypes"].Select("MatrixID = " + base.ID, null, DataViewRowState.CurrentRows))
                {
                    this._matrixData.Tables["ColumnTypes"].ImportRow(row2);
                    int num = Convert.ToInt32(row2["Column"]);
                    if (row2["ColumnPrototypeID"] != DBNull.Value)
                    {
                        int identity = Convert.ToInt32(row2["ColumnPrototypeID"]);
                        ItemData configurationData = null;
                        if (identity > 0)
                        {
                            configurationData = ItemConfigurationManager.GetConfigurationData(identity, data);
                        }
                        else if (data.Tables.Contains("Items"))
                        {
                            DataRow[] rowArray2 = data.Tables["Items"].Select("ItemID = " + identity, null, DataViewRowState.CurrentRows);
                            if (rowArray2.Length > 0)
                            {
                                string itemName = Convert.ToString(rowArray2[0]["ItemName"]);
                                if ((itemName != null) && (itemName.Trim() != string.Empty))
                                {
                                    configurationData = ItemConfigurationManager.GetConfigurationData(itemName, identity, data);
                                }
                            }
                        }
                        if (configurationData != null)
                        {
                            configurationData.CreateDataRelations(data);
                            this._columnPrototypes[num] = configurationData;
                            if (data.Tables.Contains("Items") && (data.Tables["Items"].Select("ItemID = " + configurationData.ID, null, DataViewRowState.CurrentRows).Length == 0))
                            {
                                DataRow row3 = data.Tables["Items"].NewRow();
                                row3["ItemID"] = configurationData.ID.Value;
                                row3["ItemDataClassName"] = configurationData.GetType().FullName;
                                row3["ItemDataAssemblyName"] = configurationData.GetType().Assembly.FullName;
                                row3["ItemName"] = configurationData.ItemTypeName;
                                row3["ItemTypeID"] = configurationData.ItemTypeID;
                                data.Tables["Items"].Rows.Add(row3);
                            }
                        }
                    }
                }
                this.AddPKColumn();
                foreach (DataRow row4 in data.Tables["RowData"].Select("MatrixID = " + base.ID, null, DataViewRowState.CurrentRows))
                {
                    int num3 = Convert.ToInt32(row4["Row"]);
                    bool flag = (row4["IsSubheading"] != DBNull.Value) ? Convert.ToBoolean(row4["IsSubheading"]) : false;
                    bool flag2 = (row4["IsOther"] != DBNull.Value) ? Convert.ToBoolean(row4["IsOther"]) : false;
                    string str2 = (row4["Alias"] != DBNull.Value) ? row4["Alias"].ToString() : string.Empty;
                    this._matrixData.Tables["RowData"].Rows.Add(new object[] { base.ID, num3, str2, flag, flag2 });
                }
                if (base.ID.HasValue && (base.ID > 0))
                {
                    this._matrixData.Tables["RowData"].AcceptChanges();
                }
                foreach (DataRow row5 in this._matrixData.Tables["ItemPositions"].Select("Column = " + this._pkIndex, null, DataViewRowState.CurrentRows))
                {
                    int num4 = Convert.ToInt32(row5["ItemID"]);
                    DataRow[] rowArray4 = null;
                    if (data.Tables.Contains(this.CategoryTextDataTableName))
                    {
                        rowArray4 = data.Tables[this.CategoryTextDataTableName].Select("ItemID =" + num4, null, DataViewRowState.CurrentRows);
                    }
                    if ((rowArray4 != null) && (rowArray4.Length > 0))
                    {
                        foreach (DataRow row6 in rowArray4)
                        {
                            this._matrixData.Tables[this.CategoryTextDataTableName].ImportRow(row6);
                        }
                    }
                }
                foreach (ItemData data3 in this._columnPrototypes.Values)
                {
                    DataRow[] rowArray5 = null;
                    if (data.Tables.Contains(this.TextTableName))
                    {
                        rowArray5 = data.Tables[this.TextTableName].Select("ItemID =" + data3.ID, null, DataViewRowState.CurrentRows);
                    }
                    if ((rowArray5 != null) && (rowArray5.Length > 0))
                    {
                        foreach (DataRow row7 in rowArray5)
                        {
                            this._matrixData.Tables[this.ColumnTextDataTableName].ImportRow(row7);
                        }
                    }
                }
                this.PopulateItemDataDictionary(data);
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
            }
        }

        protected override void LoadFromDataRow(DataRow data)
        {
            if (data == null)
            {
                throw new Exception("DataRow cannot be null.");
            }
            try
            {
                this.IsRequired = (data["IsRequired"] != DBNull.Value) ? Convert.ToBoolean(data["IsRequired"]) : false;
                this._pkIndex = (data["PKIndex"] != DBNull.Value) ? Convert.ToInt32(data["PKIndex"]) : 0;
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessProtected"))
                {
                    throw;
                }
            }
        }

        public override DataSet LoadTextData()
        {
            DataSet ds = base.LoadTextData();
            foreach (DataRow row in this._matrixData.Tables["ItemPositions"].Select("Column = " + this._pkIndex, null, DataViewRowState.CurrentRows))
            {
                int itemID = (int) row["ItemID"];
                int num2 = (int) row["Row"];
                if (!this.IsRowOther(num2))
                {
                    this.MergeTextData(ds, TextManager.GetTextData("/messageItemData/" + itemID + "/text"), this.CategoryTextDataTableName, itemID);
                }
                else
                {
                    this.MergeTextData(ds, TextManager.GetTextData("/textItemData/" + itemID + "/defaultText"), this.CategoryTextDataTableName, itemID);
                }
            }
            foreach (ItemData data in this._columnPrototypes.Values)
            {
                if (data is LocalizableResponseItemData)
                {
                    ds.Merge(((LocalizableResponseItemData) data).LoadTextData());
                }
            }
            return ds;
        }

        protected virtual void MergeTextData(DataSet ds, DataTable data, string tableName, int itemID)
        {
            if (data != null)
            {
                string prefix = null;
                string suffix = null;
                DataRow[] rowArray = data.Select(null, null, DataViewRowState.CurrentRows);
                if (((rowArray.Length > 0) && data.Columns.Contains("TextID")) && (rowArray[0]["TextID"] != DBNull.Value))
                {
                    string[] strArray = ((string) rowArray[0]["TextID"]).Split(new char[] { '/' });
                    if (strArray.Length == 4)
                    {
                        suffix = strArray[3];
                        prefix = strArray[1];
                    }
                }
                if ((suffix != null) && (prefix != null))
                {
                    base.MergeTextData(ds, data, tableName, suffix, prefix, itemID);
                }
            }
        }

        public void MoveColumn(int currentPosition, int newPosition)
        {
            if (((currentPosition >= 1) && (currentPosition < this.GetNextColumnPosition())) && ((newPosition >= 1) && (newPosition < this.GetNextColumnPosition())))
            {
                string str;
                int num;
                DataRow[] rowArray = this._matrixData.Tables["ColumnTypes"].Select("Column = " + currentPosition, null, DataViewRowState.CurrentRows);
                DataRow[] rowArray2 = this._matrixData.Tables["ItemPositions"].Select("Column = " + currentPosition, null, DataViewRowState.CurrentRows);
                if (newPosition < currentPosition)
                {
                    str = string.Concat(new object[] { "Column >= ", newPosition, " AND Column < ", currentPosition });
                    num = 1;
                }
                else
                {
                    str = string.Concat(new object[] { "Column > ", currentPosition, " AND Column <= ", newPosition });
                    num = -1;
                }
                if (this._pkIndex == currentPosition)
                {
                    this._pkIndex = newPosition;
                }
                else if (((newPosition > currentPosition) && (this._pkIndex > currentPosition)) && (this._pkIndex <= newPosition))
                {
                    this._pkIndex--;
                }
                else if (((newPosition < currentPosition) && (this._pkIndex >= newPosition)) && (this._pkIndex < currentPosition))
                {
                    this._pkIndex++;
                }
                this.AddValueToTableData("ColumnTypes", "Column", num, str);
                this.AddValueToTableData("ItemPositions", "Column", num, str);
                foreach (DataRow row in rowArray)
                {
                    row["Column"] = newPosition;
                }
                foreach (DataRow row2 in rowArray2)
                {
                    row2["Column"] = newPosition;
                }
                ItemData data = null;
                ItemData data2 = null;
                if (this._columnPrototypes.ContainsKey(currentPosition))
                {
                    data = this._columnPrototypes[currentPosition];
                }
                if (this._columnPrototypes.ContainsKey(newPosition))
                {
                    data2 = this._columnPrototypes[newPosition];
                }
                if (data == null)
                {
                    this._columnPrototypes.Remove(newPosition);
                }
                else
                {
                    this._columnPrototypes[newPosition] = data;
                }
                if (data2 == null)
                {
                    this._columnPrototypes.Remove(currentPosition);
                }
                else
                {
                    this._columnPrototypes[currentPosition] = data2;
                }
                this._dirty = true;
            }
        }

        public void MoveRow(int currentPosition, int newPosition)
        {
            if (((currentPosition >= 0) && (currentPosition < this.GetNextRowPosition())) && (((newPosition < this.GetNextRowPosition()) && (newPosition >= 1)) && (currentPosition != newPosition)))
            {
                string str;
                int num;
                DataRow[] rowArray = this._matrixData.Tables["RowData"].Select("Row = " + currentPosition, null, DataViewRowState.CurrentRows);
                DataRow[] rowArray2 = this._matrixData.Tables["ItemPositions"].Select("Row = " + currentPosition, null, DataViewRowState.CurrentRows);
                if (newPosition < currentPosition)
                {
                    str = string.Concat(new object[] { "Row >= ", newPosition, " AND Row < ", currentPosition });
                    num = 1;
                }
                else
                {
                    str = string.Concat(new object[] { "Row > ", currentPosition, " AND Row <= ", newPosition });
                    num = -1;
                }
                this.AddValueToTableData("RowData", "Row", num, str);
                this.AddValueToTableData("ItemPositions", "Row", num, str);
                foreach (DataRow row in rowArray)
                {
                    row["Row"] = newPosition;
                }
                foreach (DataRow row2 in rowArray2)
                {
                    row2["Row"] = newPosition;
                }
                this._dirty = true;
            }
        }

        protected override void OnAbort(object sender, EventArgs e)
        {
            base.OnAbort(sender, e);
            if (this._savedItems != null)
            {
                foreach (ItemData data in this._savedItems)
                {
                    data.NotifyAbort(sender, e);
                }
            }
        }

        protected override void OnCommit(object sender, EventArgs e)
        {
            base.OnCommit(sender, e);
            if (this._savedItems != null)
            {
                foreach (ItemData data in this._savedItems)
                {
                    data.NotifyCommit(sender, e);
                }
            }
            foreach (int num in this._deletedItemIDs)
            {
                try
                {
                    ItemData configurationData = ItemConfigurationManager.GetConfigurationData(num);
                    if (configurationData != null)
                    {
                        configurationData.Delete();
                    }
                }
                catch (Exception exception)
                {
                    ExceptionPolicy.HandleException(exception, "BusinessProtected");
                }
            }
            this._deletedItemIDs.Clear();
        }

        protected virtual void PopulateItemDataDictionary(DataSet data)
        {
            this._matrixItems = new Dictionary<Coordinate, ItemData>(new CoordinateComparer());
            foreach (DataRow row in this._matrixData.Tables["ItemPositions"].Select(null, null, DataViewRowState.CurrentRows))
            {
                int position = (int) row["Row"];
                int y = (int) row["Column"];
                int identity = (int) row["ItemID"];
                if (identity > 0)
                {
                    ItemData configurationData = ItemConfigurationManager.GetConfigurationData(identity, data);
                    if (configurationData != null)
                    {
                        if (y == this.PrimaryKeyColumnIndex)
                        {
                            configurationData.Alias = this.GetRowAlias(position);
                        }
                        this._matrixItems[new Coordinate(position, y)] = configurationData;
                    }
                }
            }
            this._dirty = false;
        }

        public void RemoveColumn(int position)
        {
            if (((position >= 1) && (position <= this.GetNextColumnPosition())) && (position != this._pkIndex))
            {
                DataRow[] rowArray = this._matrixData.Tables["ColumnTypes"].Select("Column = " + position, null, DataViewRowState.CurrentRows);
                if (rowArray.Length > 0)
                {
                    rowArray[0].Delete();
                }
                foreach (DataRow row in this._matrixData.Tables["ItemPositions"].Select("Column = " + position, null, DataViewRowState.CurrentRows))
                {
                    int item = Convert.ToInt32(row["ItemID"]);
                    row.Delete();
                    if (item > 0)
                    {
                        this._deletedItemIDs.Add(item);
                    }
                }
                this.AddValueToTableData("ItemPositions", "Column", -1, "Column > " + position);
                this.AddValueToTableData("ColumnTypes", "Column", -1, "Column > " + position);
                if (this._columnPrototypes.ContainsKey(position))
                {
                    this._deletedItemIDs.Add(this._columnPrototypes[position].ID.Value);
                    this._columnPrototypes.Remove(position);
                }
                List<int> list = new List<int>();
                int key = -1;
                foreach (int num3 in this._columnPrototypes.Keys)
                {
                    if (num3 > position)
                    {
                        list.Add(num3);
                    }
                }
                foreach (int num4 in list)
                {
                    if (num4 > key)
                    {
                        key = num4;
                    }
                    if (num4 > position)
                    {
                        this._columnPrototypes[num4 - 1] = this._columnPrototypes[num4];
                    }
                }
                if (this._columnPrototypes.ContainsKey(key))
                {
                    this._columnPrototypes.Remove(key);
                }
                if (this._pkIndex > position)
                {
                    this._pkIndex--;
                }
                this.FillPositionTable();
                this._dirty = true;
            }
        }

        public void RemoveRow(int rowPosition)
        {
            DataRow[] rowArray = this._matrixData.Tables["RowData"].Select("Row = " + rowPosition, null, DataViewRowState.CurrentRows);
            if (rowArray.Length > 0)
            {
                rowArray[0].Delete();
            }
            this.AddValueToTableData("RowData", "Row", -1, "Row > " + rowPosition);
            foreach (DataRow row in this._matrixData.Tables["ItemPositions"].Select("Row = " + rowPosition, null, DataViewRowState.CurrentRows))
            {
                int item = Convert.ToInt32(row["ItemID"]);
                row.Delete();
                if (item > 0)
                {
                    this._deletedItemIDs.Add(item);
                }
            }
            this.AddValueToTableData("ItemPositions", "Row", -1, "Row > " + rowPosition);
            this.FillPositionTable();
            this._dirty = true;
        }

        protected virtual void SaveCategoryTexts()
        {
            if (this._matrixData.Tables.Contains(this.CategoryTextDataTableName))
            {
                foreach (DataRow row in this._matrixData.Tables[this.CategoryTextDataTableName].Select(null, null, DataViewRowState.CurrentRows))
                {
                    if (((row["LanguageCode"] != DBNull.Value) && (row["TextValue"] != DBNull.Value)) && (row["ComputedTextID"] != DBNull.Value))
                    {
                        string languageCode = (string) row["LanguageCode"];
                        string textValue = (string) row["TextValue"];
                        string textIdentifier = (string) row["ComputedTextID"];
                        TextManager.SetText(textIdentifier, languageCode, textValue);
                    }
                }
            }
        }

        protected void SaveColumnData(IDbTransaction t)
        {
            foreach (ItemData data in this._columnPrototypes.Values)
            {
                if (data is SelectItemData)
                {
                    ((SelectItemData) data).AllowOther = false;
                }
                int num = data.ID.Value;
                data.Save(t);
                this._savedItems.Add(data);
                DataRow[] rowArray = this._matrixData.Tables["ColumnTypes"].Select("ColumnPrototypeID = " + num, null, DataViewRowState.CurrentRows);
                if (rowArray.Length > 0)
                {
                    if (rowArray[0]["ColumnPrototypeID"] != DBNull.Value)
                    {
                        int num2 = Convert.ToInt32(rowArray[0]["ColumnPrototypeID"]);
                        if (num2 == data.ID)
                        {
                            continue;
                        }
                    }
                    rowArray[0]["ColumnPrototypeID"] = data.ID;
                }
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_RemoveColumnsFromMatrix");
            storedProcCommandWrapper.AddInParameter("MatrixID", DbType.Int32, base.ID);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
            foreach (DataRow row in this._matrixData.Tables["ColumnTypes"].Select(null, null, DataViewRowState.CurrentRows))
            {
                DBCommandWrapper command = database.GetStoredProcCommandWrapper("ckbx_ItemData_InsertMColType");
                command.AddInParameter("MatrixID", DbType.Int32, base.ID);
                command.AddInParameter("Column", DbType.Int32, row["Column"]);
                command.AddInParameter("PrototypeID", DbType.Int32, row["ColumnPrototypeID"]);
                command.AddInParameter("UniqueAnswers", DbType.Boolean, row["UniqueAnswers"]);
                command.AddInParameter("Width", DbType.Int32, row["Width"]);
                database.ExecuteNonQuery(command, t);
            }
        }

        protected virtual void SaveColumnTexts()
        {
            if (this._matrixData.Tables.Contains(this.ColumnTextDataTableName))
            {
                foreach (DataRow row in this._matrixData.Tables[this.ColumnTextDataTableName].Select(null, null, DataViewRowState.CurrentRows))
                {
                    if (((row["LanguageCode"] != DBNull.Value) && (row["TextValue"] != DBNull.Value)) && (row["ComputedTextID"] != DBNull.Value))
                    {
                        string languageCode = (string) row["LanguageCode"];
                        string textValue = (string) row["TextValue"];
                        string textIdentifier = (string) row["ComputedTextID"];
                        TextManager.SetText(textIdentifier, languageCode, textValue);
                    }
                }
            }
        }

        protected void SaveItems(IDbTransaction t)
        {
            if (this._dirty || this._imported)
            {
                this._savedItems = new List<ItemData>();
                this.SaveColumnData(t);
                this.SaveRowData(t);
                this.FillPositionTable();
                DataRow[] rowArray = this._matrixData.Tables["ItemPositions"].Select(null, null, DataViewRowState.CurrentRows);
                this._matrixItems = new Dictionary<Coordinate, ItemData>(new CoordinateComparer());
                foreach (DataRow row in rowArray)
                {
                    int column = Convert.ToInt32(row["Column"]);
                    int num2 = Convert.ToInt32(row["Row"]);
                    int itemId = Convert.ToInt32(row["ItemID"]);
                    string str = string.Empty;
                    DataRow[] rowArray2 = this._matrixData.Tables["RowData"].Select("Row = " + num2, null, DataViewRowState.CurrentRows);
                    if (((rowArray2.Length > 0) && (rowArray2[0]["Alias"] != DBNull.Value)) && (((string) rowArray2[0]["Alias"]) != string.Empty))
                    {
                        str = (string) rowArray2[0]["Alias"];
                    }
                    ItemData item = this.GetItemAt(num2, column, false);
                    if (item != null)
                    {
                        AppearanceData appearanceDataForCode = null;
                        AppearanceData appearanceDataForItem = null;
                        if (((column != this.PrimaryKeyColumnIndex) && this._columnPrototypes.ContainsKey(column)) && this._columnPrototypes[column].ID.HasValue)
                        {
                            appearanceDataForItem = AppearanceDataManager.GetAppearanceDataForItem(this._columnPrototypes[column].ID.Value, t);
                        }
                        if (itemId > 0)
                        {
                            appearanceDataForCode = AppearanceDataManager.GetAppearanceDataForItem(itemId, t);
                        }
                        if ((appearanceDataForCode == null) || ((appearanceDataForItem != null) && (appearanceDataForItem.GetType() != appearanceDataForCode.GetType())))
                        {
                            string defaultAppearanceCodeForType = AppearanceDataManager.GetDefaultAppearanceCodeForType(item.ItemTypeID);
                            if (defaultAppearanceCodeForType != "MESSAGE")
                            {
                                if ((defaultAppearanceCodeForType != null) && (defaultAppearanceCodeForType.Trim() != string.Empty))
                                {
                                    appearanceDataForCode = AppearanceDataManager.GetAppearanceDataForCode("MATRIX_" + defaultAppearanceCodeForType);
                                }
                            }
                            else
                            {
                                appearanceDataForCode = AppearanceDataManager.GetDefaultAppearanceDataForType(item.ItemTypeID);
                            }
                            if (appearanceDataForCode == null)
                            {
                                throw new Exception("Unable to create appearance data for item with type: " + item.ItemTypeID);
                            }
                        }
                        if (appearanceDataForCode is MatrixSingleLineText)
                        {
                            appearanceDataForCode.Width = this.GetColumnWidth(column);
                        }
                        if (column == this.PrimaryKeyColumnIndex)
                        {
                            item.Alias = str;
                        }
                        item.Save(t);
                        appearanceDataForCode.Save(t, item.ID.Value);
                        this._savedItems.Add(item);
                        this._matrixItems[new Coordinate(num2, column)] = item;
                        if ((row["ItemID"] == DBNull.Value) || (Convert.ToInt32(row["ItemID"]) != item.ID.Value))
                        {
                            row["ItemID"] = item.ID.Value;
                        }
                    }
                }
                Database database = DatabaseFactory.CreateDatabase();
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_RemoveItemsFromMatrix");
                storedProcCommandWrapper.AddInParameter("MatrixID", DbType.Int32, base.ID);
                database.ExecuteNonQuery(storedProcCommandWrapper, t);
                foreach (DataRow row2 in this._matrixData.Tables["ItemPositions"].Select(null, null, DataViewRowState.CurrentRows))
                {
                    DBCommandWrapper command = database.GetStoredProcCommandWrapper("ckbx_ItemData_InsertMItem");
                    command.AddInParameter("MatrixID", DbType.Int32, base.ID);
                    command.AddInParameter("Row", DbType.Int32, row2["Row"]);
                    command.AddInParameter("Column", DbType.Int32, row2["Column"]);
                    command.AddInParameter("ItemID", DbType.Int32, row2["ItemID"]);
                    database.ExecuteNonQuery(command, t);
                }
            }
            this._dirty = false;
        }

        protected void SaveRowData(IDbTransaction t)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_DeleteMRows");
            storedProcCommandWrapper.AddInParameter("MatrixID", DbType.Int32, base.ID);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
            foreach (DataRow row in this._matrixData.Tables["RowData"].Select(null, null, DataViewRowState.CurrentRows))
            {
                DBCommandWrapper command = database.GetStoredProcCommandWrapper("ckbx_ItemData_InsertMRow");
                command.AddInParameter("MatrixID", DbType.Int32, base.ID.Value);
                command.AddInParameter("Row", DbType.Int32, row["Row"]);
                command.AddInParameter("IsSubheading", DbType.Boolean, row["IsSubheading"]);
                command.AddInParameter("IsOther", DbType.Boolean, row["IsOther"]);
                database.ExecuteNonQuery(command, t);
            }
        }

        public void SetColumnUniqueness(int column, bool unique)
        {
            DataRow[] rowArray = this._matrixData.Tables["ColumnTypes"].Select("Column = " + column, null, DataViewRowState.CurrentRows);
            if (rowArray.Length > 0)
            {
                rowArray[0]["UniqueAnswers"] = unique;
            }
        }

        public void SetColumnWidth(int column, int? width)
        {
            DataRow[] rowArray = this._matrixData.Tables["ColumnTypes"].Select("Column = " + column, null, DataViewRowState.CurrentRows);
            if (rowArray.Length > 0)
            {
                if (!width.HasValue)
                {
                    rowArray[0]["Width"] = DBNull.Value;
                }
                else
                {
                    rowArray[0]["Width"] = width;
                }
            }
        }

        public void SetRowAlias(int position, string alias)
        {
            this.SetRowProp(position, "Alias", alias);
        }

        protected void SetRowProp(int position, string propName, object propValue)
        {
            if ((position >= 1) && (position < this.GetNextRowPosition()))
            {
                DataRow[] rowArray = this._matrixData.Tables["RowData"].Select("Row = " + position, null, DataViewRowState.CurrentRows);
                if (rowArray.Length != 0)
                {
                    rowArray[0][propName] = propValue;
                }
            }
        }

        private static void SyncOptions(SelectItemData source, SelectItemData target)
        {
            Dictionary<int, ListOptionData> dictionary = new Dictionary<int, ListOptionData>();
            Dictionary<int, ListOptionData> dictionary2 = new Dictionary<int, ListOptionData>();
            foreach (ListOptionData data in source.Options)
            {
                dictionary[data.Position] = data;
            }
            foreach (ListOptionData data2 in target.Options)
            {
                dictionary2[data2.Position] = data2;
                if (dictionary.ContainsKey(data2.Position))
                {
                    ListOptionData data3 = dictionary[data2.Position];
                    target.UpdateOption(data2.OptionID, data3.Alias, data3.IsDefault, data3.Position, data3.Points, data3.IsOther);
                }
                else
                {
                    target.RemoveOption(data2.OptionID);
                }
            }
            foreach (ListOptionData data4 in dictionary.Values)
            {
                if (!dictionary2.ContainsKey(data4.Position))
                {
                    target.AddOption(data4.Alias, data4.IsDefault, data4.Position, data4.Points, data4.IsOther);
                }
            }
            target.Alias = source.Alias;
            target.IsRequired = source.IsRequired;
            if ((target is SelectManyData) && (source is SelectManyData))
            {
                ((SelectManyData) target).MinToSelect = ((SelectManyData) source).MinToSelect;
                ((SelectManyData) target).MaxToSelect = ((SelectManyData) source).MaxToSelect;
            }
            if ((target is RatingScaleItemData) && (source is RatingScaleItemData))
            {
                ((RatingScaleItemData) target).StartValue = ((RatingScaleItemData) source).StartValue;
                ((RatingScaleItemData) target).EndValue = ((RatingScaleItemData) source).EndValue;
                ((RatingScaleItemData) target).DisplayNotApplicable = ((RatingScaleItemData) source).DisplayNotApplicable;
            }
        }

        protected override void Update(IDbTransaction t)
        {
            base.Update(t);
            if (base.ID <= 0)
            {
                throw new ApplicationException("No DataID specified.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_UpdateMatrix");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("TextID", DbType.String, base.TextID);
            storedProcCommandWrapper.AddInParameter("SubTextID", DbType.String, base.SubTextID);
            storedProcCommandWrapper.AddInParameter("IsRequired", DbType.Boolean, base.IsRequired);
            storedProcCommandWrapper.AddInParameter("PKIndex", DbType.Int32, this._pkIndex);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
            this.SaveItems(t);
        }

        public void UpdateColumn(ItemData itemData, int position)
        {
            if (((itemData != null) && (position >= 1)) && (position <= this.ColumnCount))
            {
                this._columnPrototypes[position] = itemData;
                DataRow[] rowArray = this._matrixData.Tables["ColumnTypes"].Select("Column = " + position, null, DataViewRowState.CurrentRows);
                if (rowArray.Length > 0)
                {
                    rowArray[0]["ColumnPrototypeID"] = itemData.ID.Value;
                }
                this.FillPositionTable();
                this._dirty = true;
            }
        }

        public void UpdateRow(int rowPosition, string alias, bool isSubheading, bool isOther)
        {
            if (((rowPosition >= 1) && (rowPosition <= this.GetNextRowPosition())) && (!isSubheading || !isOther))
            {
                if (rowPosition == this.GetNextRowPosition())
                {
                    this.AddRow(alias, isSubheading, isOther, rowPosition);
                }
                else
                {
                    DataRow[] rowArray = this._matrixData.Tables["RowData"].Select("Row = " + rowPosition, null, DataViewRowState.CurrentRows);
                    if (rowArray.Length > 0)
                    {
                        rowArray[0]["Alias"] = alias;
                        rowArray[0]["IsSubheading"] = isSubheading;
                        rowArray[0]["IsOther"] = isOther;
                    }
                    string str = isOther ? "SingleLineText" : "Message";
                    rowArray = this._matrixData.Tables["ItemPositions"].Select(string.Concat(new object[] { "Row = ", rowPosition, " AND Column = ", this._pkIndex }), null, DataViewRowState.CurrentRows);
                    if ((rowArray.Length > 0) && ((rowArray[0]["ItemTypeName"] == DBNull.Value) || (rowArray[0]["ItemTypeName"].ToString().ToLower() != str.ToLower())))
                    {
                        if ((rowArray[0]["ItemID"] != DBNull.Value) && (Convert.ToInt32(rowArray[0]["ItemID"]) > 0))
                        {
                            this._deletedItemIDs.Add(Convert.ToInt32(rowArray[0]["ItemID"]));
                        }
                        rowArray[0]["ItemID"] = this.GetNextTempItemID();
                        rowArray[0]["ItemTypeName"] = str;
                    }
                }
                this.FillPositionTable();
                this._dirty = true;
            }
        }

        public string CategoryTextDataTableName
        {
            get
            {
                return "MatrixCategoryTexts";
            }
        }

        public override int ColumnCount
        {
            get
            {
                return (this.GetNextColumnPosition() - 1);
            }
        }

        public string ColumnTextDataTableName
        {
            get
            {
                return "MatrixColumnTexts";
            }
        }

        public override string DataTableName
        {
            get
            {
                return "MatrixItemData";
            }
        }

        public override bool ItemIsIAnswerable
        {
            get
            {
                return true;
            }
        }

        public DataTable MatrixItems
        {
            get
            {
                return this._matrixData.Tables["ItemPositions"].Copy();
            }
        }

        public DataTable MatrixRows
        {
            get
            {
                return this._matrixData.Tables["RowData"].Copy();
            }
        }

        public int PrimaryKeyColumnIndex
        {
            get
            {
                return this._pkIndex;
            }
            set
            {
                this._pkIndex = value;
            }
        }

        public override int RowCount
        {
            get
            {
                return (this.GetNextRowPosition() - 1);
            }
        }

        public override string TextIDPrefix
        {
            get
            {
                return "matrixItemData";
            }
        }
    }
}

