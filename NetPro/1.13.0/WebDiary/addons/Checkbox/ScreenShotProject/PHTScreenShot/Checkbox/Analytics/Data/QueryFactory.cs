﻿namespace Checkbox.Analytics.Data
{
    using Checkbox.Common;
    using Prezza.Framework.Data;
    using Prezza.Framework.Data.QueryCriteria;
    using Prezza.Framework.Data.QueryParameters;
    using System;
    using System.Globalization;

    internal static class QueryFactory
    {
        private static QueryCriterion CreateFilterCriterion(string filterField, string filterValue, string tableName)
        {
            return new QueryCriterion(new SelectParameter(filterField, string.Empty, tableName), CriteriaOperator.Like, new LiteralParameter("'%" + filterValue + "%'"));
        }

        private static SubqueryParameter CreatePagingSubQueryParameter(SelectQuery query, string selectFieldName, string selectFieldTable, int pageNumber, int resultsPerPage)
        {
            int count = (pageNumber - 1) * resultsPerPage;
            SelectQuery subQuery = (SelectQuery) query.Clone();
            subQuery.SelectParameters = null;
            subQuery.AddParameter(selectFieldName, string.Empty, selectFieldTable);
            if (count > 0)
            {
                subQuery.AddSelectTopCriterion(count);
            }
            return new SubqueryParameter(subQuery);
        }

        public static SelectQuery GetAnalysesForFormQuery(int responseTemplateID, string filterField, string filterValue)
        {
            SelectQuery query = new SelectQuery("ckbx_Template");
            query.AddTableJoin("ckbx_AnalysisTemplate", QueryJoinType.Inner, "AnalysisTemplateID", "ckbx_Template", "TemplateID");
            query.AddSelectDistinctCriterion();
            query.AddAllParameter("ckbx_Template");
            query.AddAllParameter("ckbx_AnalysisTemplate");
            query.AddCriterion(new QueryCriterion(new SelectParameter("ResponseTemplateID", string.Empty, "ckbx_AnalysisTemplate"), CriteriaOperator.EqualTo, new LiteralParameter(responseTemplateID)));
            CriteriaCollection criteria = new CriteriaCollection(CriteriaJoinType.Or);
            criteria.AddCriterion(new QueryCriterion(new SelectParameter("Deleted", string.Empty, "ckbx_Template"), CriteriaOperator.NotEqualTo, new LiteralParameter("1")));
            criteria.AddCriterion(new QueryCriterion(new SelectParameter("Deleted", string.Empty, "ckbx_Template"), CriteriaOperator.Is, new LiteralParameter("NULL")));
            query.AddCriteriaCollection(criteria);
            if (Utilities.IsNotNullOrEmpty(filterField) && Utilities.IsNotNullOrEmpty(filterValue))
            {
                query.AddCriterion(CreateFilterCriterion(filterField, filterValue, "ckbx_AnalysisTemplate"));
            }
            return query;
        }

        public static SelectQuery GetResponsesCountQuery(int responseTemplateID, bool includeIncomplete, bool includeDeleted, string filterField, string filterValue)
        {
            SelectQuery query = GetResponsesQuery(responseTemplateID, includeIncomplete, includeDeleted, null, null, filterField, filterValue, string.Empty, true);
            query.SelectParameters = null;
            query.AddCountParameter("ResponseID", "ResponseCount");
            return query;
        }

        public static SelectQuery GetResponsesQuery(int responseTemplateID, bool includeIncomplete, bool includeDeleted)
        {
            SelectQuery query = new SelectQuery("ckbx_Response");
            query.AddParameter(new SelectParameter("ResponseID"));
            query.AddParameter(new SelectParameter("GUID"));
            query.AddParameter(new SelectParameter("ResponseTemplateID"));
            query.AddParameter(new SelectParameter("IsComplete"));
            query.AddParameter(new SelectParameter("LastPageViewed"));
            query.AddParameter(new SelectParameter("Started"));
            query.AddParameter(new SelectParameter("Ended"));
            query.AddParameter(new SelectParameter("IP"));
            query.AddParameter(new SelectParameter("LastEdit"));
            query.AddParameter(new SelectParameter("NetworkUser"));
            query.AddParameter(new SelectParameter("Language"));
            query.AddParameter(new SelectParameter("UniqueIdentifier"));
            query.AddParameter(new SelectParameter("IsTest"));
            query.AddParameter(new SelectParameter("Invitee"));
            query.CriteriaJoinType = CriteriaJoinType.And;
            query.AddCriterion(new QueryCriterion(new SelectParameter("ResponseTemplateID"), CriteriaOperator.EqualTo, new LiteralParameter(responseTemplateID.ToString())));
            if (!includeIncomplete)
            {
                query.AddCriterion(new QueryCriterion(new SelectParameter("IsComplete"), CriteriaOperator.EqualTo, new LiteralParameter("1")));
            }
            if (!includeDeleted)
            {
                CriteriaCollection criteria = new CriteriaCollection(CriteriaJoinType.Or);
                criteria.AddCriterion(new QueryCriterion(new SelectParameter("Deleted"), CriteriaOperator.Is, new LiteralParameter("NULL")));
                criteria.AddCriterion(new QueryCriterion(new SelectParameter("Deleted"), CriteriaOperator.EqualTo, new LiteralParameter("0")));
                query.AddCriteriaCollection(criteria);
            }
            return query;
        }

        public static SelectQuery GetResponsesQuery(int responseTemplateID, bool includeIncomplete, bool includeDeleted, string filterField, string filterValue)
        {
            SelectQuery query = GetResponsesQuery(responseTemplateID, includeIncomplete, includeDeleted);
            if (Utilities.IsNotNullOrEmpty(filterField) && Utilities.IsNotNullOrEmpty(filterValue))
            {
                query.AddCriterion(CreateFilterCriterion(filterField, filterValue, "ckbx_Response"));
            }
            return query;
        }

        public static SelectQuery GetResponsesQuery(int responseTemplateID, bool includeIncomplete, bool includeDeleted, int? pageNumber, int? resultsPerPage, string filterField, string filterValue, string sortField, bool sortAscending)
        {
            SelectQuery query = GetResponsesQuery(responseTemplateID, includeIncomplete, includeDeleted, filterField, filterValue);
            if (Utilities.IsNotNullOrEmpty(sortField))
            {
                query.AddSortField(new SortOrder(sortField, sortAscending));
            }
            if ((pageNumber.HasValue && (pageNumber.Value > 0)) && (resultsPerPage.HasValue && (resultsPerPage.Value > 0)))
            {
                query.AddSelectTopCriterion(resultsPerPage.Value);
                query.CriteriaJoinType = CriteriaJoinType.And;
                if (pageNumber.Value > 1)
                {
                    query.AddCriterion(new QueryCriterion(new SelectParameter("ResponseID", string.Empty, "ckbx_Response"), CriteriaOperator.NotIn, CreatePagingSubQueryParameter(query, "ResponseID", "ckbx_Response", pageNumber.Value, resultsPerPage.Value)));
                }
            }
            return query;
        }

        public static SelectQuery GetResponsesQuery(int responseTemplateID, bool includeIncomplete, bool includeDeleted, int? pageNumber, int? resultsPerPage, string filterField, string filterValue, string sortField, bool sortAscending, DateTime? startDate, DateTime? endDate)
        {
            SelectQuery query = GetResponsesQuery(responseTemplateID, includeIncomplete, includeDeleted, pageNumber, resultsPerPage, filterField, filterValue, sortField, sortAscending);
            string str = startDate.HasValue ? startDate.Value.ToString("s", DateTimeFormatInfo.InvariantInfo) : string.Empty;
            string str2 = endDate.HasValue ? endDate.Value.ToString("s", DateTimeFormatInfo.InvariantInfo) : string.Empty;
            if (str != string.Empty)
            {
                query.AddCriterion(new QueryCriterion(new SelectParameter("Started", string.Empty, "ckbx_Response"), CriteriaOperator.GreaterThan, new LiteralParameter("'" + str + "'")));
            }
            if (str2 != string.Empty)
            {
                CriteriaCollection criteria = new CriteriaCollection(CriteriaJoinType.Or);
                criteria.AddCriterion(new QueryCriterion(new SelectParameter("Ended", string.Empty, "ckbx_Response"), CriteriaOperator.Is, new LiteralParameter("NULL")));
                criteria.AddCriterion(new QueryCriterion(new SelectParameter("Ended", string.Empty, "ckbx_Response"), CriteriaOperator.LessEqual, new LiteralParameter("'" + endDate + "'")));
                query.AddCriteriaCollection(criteria);
            }
            return query;
        }
    }
}

