﻿namespace Checkbox.Progress
{
    using System;

    [Serializable]
    public enum ProgressStatus
    {
        Pending,
        Running,
        Completed,
        Error
    }
}

