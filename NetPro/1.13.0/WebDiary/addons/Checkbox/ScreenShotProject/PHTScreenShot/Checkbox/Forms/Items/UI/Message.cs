﻿namespace Checkbox.Forms.Items.UI
{
    using Prezza.Framework.Data;
    using System;
    using System.Data;

    [Serializable]
    public class Message : AppearanceData
    {
        protected virtual void AddParameters(DBCommandWrapper command)
        {
            command.AddInParameter("FontSize", DbType.String, this.FontSize);
            command.AddInParameter("FontColor", DbType.String, this.FontColor);
            command.AddInParameter("ItemPosition", DbType.String, base.ItemPosition);
        }

        protected override void Create(IDbTransaction transaction)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Appearance_CreateMsg");
            this.AddParameters(storedProcCommandWrapper);
            storedProcCommandWrapper.AddOutParameter("AppearanceID", DbType.Int32, 4);
            database.ExecuteNonQuery(storedProcCommandWrapper, transaction);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("AppearanceID");
            if ((parameterValue == null) || (parameterValue == DBNull.Value))
            {
                throw new Exception("Unable to save appearance data.");
            }
            base.ID = new int?((int) parameterValue);
        }

        protected override void Update(IDbTransaction transaction)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Appearance_UpdateMsg");
            storedProcCommandWrapper.AddInParameter("AppearanceID", DbType.Int32, base.ID);
            this.AddParameters(storedProcCommandWrapper);
            database.ExecuteNonQuery(storedProcCommandWrapper, transaction);
        }

        public override string AppearanceCode
        {
            get
            {
                return "MESSAGE";
            }
        }
    }
}

