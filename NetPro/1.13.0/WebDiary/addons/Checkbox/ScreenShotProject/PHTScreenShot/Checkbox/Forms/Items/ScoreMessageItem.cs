﻿namespace Checkbox.Forms.Items
{
    using Checkbox.Forms.Items.Configuration;
    using Prezza.Framework.Data;
    using System;
    using System.Data;
    using System.Text;

    [Serializable]
    public class ScoreMessageItem : CurrentScore
    {
        private DataTable _scoreMessages;

        public override void Configure(ItemData configuration, string languageCode)
        {
            base.Configure(configuration, languageCode);
            this._scoreMessages = ((ScoreMessageItemData) configuration).ScoreRanges;
            this._scoreMessages.Columns.Add("Message", typeof(string));
            foreach (DataRow row in this._scoreMessages.Select(null, null, DataViewRowState.CurrentRows))
            {
                row["Message"] = this.GetText("/scoreMessageData/" + base.ID.ToString() + "/" + Convert.ToInt32(row["ScoreMessageID"]).ToString());
            }
        }

        private string GetScoreFilter()
        {
            double score = base.GetScore();
            StringBuilder builder = new StringBuilder();
            builder.Append("(LowScore IS NULL AND HighScore IS NOT NULL AND ");
            builder.Append(score);
            builder.Append(" <= HighScore) ");
            builder.Append("OR (LowScore IS NOT NULL AND HighScore IS NOT NULL AND ");
            builder.Append(score);
            builder.Append(" >= LowScore AND ");
            builder.Append(score);
            builder.Append(" <= HighScore) ");
            builder.Append("OR (LowScore IS NOT NULL AND HighScore IS NULL AND ");
            builder.Append(score);
            builder.Append(" >= LowScore)");
            return builder.ToString();
        }

        public override string Text
        {
            get
            {
                if (base.Response != null)
                {
                    DataRow[] rowArray = this._scoreMessages.Select(this.GetScoreFilter(), "LowScore DESC", DataViewRowState.CurrentRows);
                    if (rowArray.Length > 0)
                    {
                        int num = DbUtility.GetValueFromDataRow<int>(rowArray[0], "ScoreMessageID", -1);
                        string text = DbUtility.GetValueFromDataRow<string>(rowArray[0], "Message", string.Empty);
                        return this.GetPipedText("ScoreMessage_" + num.ToString(), text);
                    }
                    return string.Empty;
                }
                StringBuilder builder = new StringBuilder();
                foreach (DataRow row in this._scoreMessages.Select(null, "LowScore ASC", DataViewRowState.CurrentRows))
                {
                    builder.Append(DbUtility.GetValueFromDataRow<string>(row, "ScoreText", string.Empty));
                    builder.Append(" -- ");
                    builder.Append(DbUtility.GetValueFromDataRow<string>(row, "Message", string.Empty));
                    builder.Append("<br />");
                }
                return builder.ToString();
            }
        }
    }
}

