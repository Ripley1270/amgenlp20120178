﻿namespace Checkbox.Panels
{
    using Checkbox.Users;
    using Prezza.Framework.Data;
    using Prezza.Framework.Security.Principal;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    internal class GroupPanel : Panel
    {
        public GroupPanel() : base(new string[0], new string[0])
        {
            base.PanelType = PanelType.GroupPanel;
        }

        protected override void Create(ExtendedPrincipal principal, IDbTransaction t)
        {
            base.Create(principal, t);
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Panel_InsertGroupPanel");
            storedProcCommandWrapper.AddInParameter("PanelID", DbType.Int32, base.ID.Value);
            storedProcCommandWrapper.AddInParameter("GroupID", DbType.Int32, this.Group.ID);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        public override Panelist GetPanelist(string identifier)
        {
            return base.Panelists.Find(p => ((UserPanelist) p).UniqueIdentifier.Equals(identifier, StringComparison.InvariantCultureIgnoreCase));
        }

        protected override List<Panelist> GetPanelists()
        {
            List<Panelist> list = new List<Panelist>();
            foreach (string str in this.Group.GetUserIdentifiers())
            {
                UserPanelist item = new UserPanelist {
                    UniqueIdentifier = str
                };
                list.Add(item);
            }
            return list;
        }

        protected override void Load(DataRow data)
        {
            base.Load(data);
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Panel_GetGroupPanel");
            storedProcCommandWrapper.AddInParameter("PanelID", DbType.Int32, base.ID.Value);
            object obj2 = database.ExecuteScalar(storedProcCommandWrapper);
            if ((obj2 != DBNull.Value) && (obj2 != null))
            {
                this.Group = Checkbox.Users.Group.GetGroup((int) obj2);
            }
        }

        public Checkbox.Users.Group Group { get; set; }
    }
}

