﻿namespace Checkbox.Forms.Items
{
    using Checkbox.Common;
    using Checkbox.Forms.Items.Configuration;
    using Prezza.Framework.Common;
    using System;
    using System.Xml;

    [Serializable]
    public abstract class LabelledItem : AnswerableItem
    {
        private string _subText;
        private string _text;

        protected LabelledItem()
        {
        }

        public override void Configure(ItemData configuration, string languageCode)
        {
            base.Configure(configuration, languageCode);
            ArgumentValidation.CheckExpectedType(configuration, typeof(LabelledItemData));
            LabelledItemData data = (LabelledItemData) configuration;
            this._text = this.GetText(data.TextID);
            this._subText = this.GetText(data.SubTextID);
        }

        public virtual string GetText()
        {
            string text = this.Text;
            if (Utilities.IsNotNullOrEmpty(text))
            {
                return text;
            }
            return this.SubText;
        }

        public override void WriteXmlInstanceData(XmlWriter writer)
        {
            writer.WriteStartElement("texts");
            writer.WriteStartElement("text");
            writer.WriteCData(this.Text);
            writer.WriteEndElement();
            writer.WriteStartElement("subText");
            writer.WriteCData(this.SubText);
            writer.WriteEndElement();
            writer.WriteEndElement();
            base.WriteXmlInstanceData(writer);
        }

        public virtual string SubText
        {
            get
            {
                return this.GetPipedText("SubText", this._subText);
            }
        }

        public virtual string Text
        {
            get
            {
                return this.GetPipedText("Text", this._text);
            }
        }
    }
}

