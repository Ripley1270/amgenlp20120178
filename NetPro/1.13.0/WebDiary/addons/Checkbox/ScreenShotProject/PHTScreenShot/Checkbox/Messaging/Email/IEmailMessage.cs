﻿namespace Checkbox.Messaging.Email
{
    using System;
    using System.Collections.Generic;

    public interface IEmailMessage
    {
        List<IEmailAttachment> Attachments { get; }

        List<long> AttachmentsByRef { get; }

        string Body { get; }

        string From { get; }

        bool IsBodyHtml { get; }

        string Subject { get; }

        string To { get; }
    }
}

