﻿namespace Checkbox.Forms.Logic.Configuration
{
    using Checkbox.Forms;
    using Checkbox.Forms.Items.Configuration;
    using Checkbox.Forms.Logic;
    using Checkbox.Globalization.Text;
    using Prezza.Framework.Common;
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Text;

    [Serializable]
    public class RuleDataService
    {
        private readonly ResponseTemplate _template;
        private readonly DataSet _templateData;

        public RuleDataService(ResponseTemplate template, DataSet templateData)
        {
            this._template = template;
            this._templateData = templateData;
        }

        private void AcceptChanges()
        {
            this.RuleTable.AcceptChanges();
            this.ExpressionTable.AcceptChanges();
            this.OperandTable.AcceptChanges();
            this.ItemOperandTable.AcceptChanges();
            this.ValueOperandTable.AcceptChanges();
            this.ProfileOperandTable.AcceptChanges();
            this.ResponseOperandTable.AcceptChanges();
            this.ActionTable.AcceptChanges();
            this.BranchActionTable.AcceptChanges();
            this.RuleActionsTable.AcceptChanges();
            this.ItemRulesTable.AcceptChanges();
            this.PageRulesTable.AcceptChanges();
        }

        private void AddActionToRule(int ruleID, int actionID)
        {
            DataRow row = this.RuleActionsTable.NewRow();
            row["RuleID"] = ruleID;
            row["ActionID"] = actionID;
            DataRow row2 = this.ActionTable.Select("ActionID=" + actionID)[0];
            row2["RuleID"] = ruleID;
            this.RuleActionsTable.Rows.Add(row);
        }

        internal virtual void AddRuleToItem(int itemID, int ruleID)
        {
            if (this.ItemRulesTable.Select(string.Concat(new object[] { "RuleID = ", ruleID, " AND ItemID = ", itemID })).Length == 0)
            {
                DataRow row = this.ItemRulesTable.NewRow();
                row["ItemID"] = itemID;
                row["RuleID"] = ruleID;
                this.ItemRulesTable.Rows.Add(row);
            }
        }

        internal virtual void AddRuleToPage(TemplatePage page, RuleData rule)
        {
            this.AddRuleToPage(page, rule.ID.Value, rule.Trigger);
        }

        internal virtual void AddRuleToPage(TemplatePage page, int ruleID, RuleEventTrigger trigger)
        {
            if (((this.PageRulesTable != null) && page.Identity.HasValue) && (this.PageRulesTable.Select(string.Concat(new object[] { "PageID = ", page.Identity.Value, " AND RuleID = ", ruleID })).Length == 0))
            {
                DataRow row = this.PageRulesTable.NewRow();
                row["PageID"] = page.Identity.Value;
                row["RuleID"] = ruleID;
                row["EventTrigger"] = trigger.ToString();
                this.PageRulesTable.Rows.Add(row);
            }
        }

        public int CreateANDCompositeExpression(int root)
        {
            return this.InsertCompositeExpression(new int?(root), 1, null, new int?(root), LogicalConnector.AND);
        }

        public int CreateBranchAction(TemplatePage target)
        {
            int actionID = this.InsertAction(typeof(BranchPageActionData));
            this.InsertBranchAction(actionID, target.Identity.Value);
            return actionID;
        }

        public int CreateBranchRule(TemplatePage dependent, TemplatePage target)
        {
            int actionID = this.CreateBranchAction(target);
            int ruleID = this.CreateRootLevelRule();
            this.AddActionToRule(ruleID, actionID);
            this.AddRuleToPage(dependent, ruleID, RuleEventTrigger.UnLoad);
            return ruleID;
        }

        public int CreateConditionAction()
        {
            return this.InsertAction(typeof(IncludeExcludeActionData));
        }

        internal void CreateDataRelations(DataSet ds)
        {
            this.FlattenActionRuleTableMapping();
            if (!ds.Relations.Contains(this.RuleTable.TableName + "_" + this.ActionTable.TableName))
            {
                DataRelation relation = new DataRelation(this.RuleTable.TableName + "_" + this.ActionTable.TableName, ds.Tables[this.RuleTable.TableName].Columns["RuleID"], ds.Tables[this.ActionTable.TableName].Columns["RuleID"]);
                ds.Relations.Add(relation);
            }
            if (!ds.Relations.Contains(this.PageDataTable.TableName + "_" + this.PageRulesTable.TableName))
            {
                DataRelation relation2 = new DataRelation(this.PageDataTable.TableName + "_" + this.PageRulesTable.TableName, ds.Tables[this.PageDataTable.TableName].Columns["PageID"], ds.Tables[this.PageRulesTable.TableName].Columns["PageID"]);
                ds.Relations.Add(relation2);
            }
            if (!ds.Relations.Contains(this.RuleTable.TableName + "_" + this.PageRulesTable.TableName))
            {
                DataRelation relation3 = new DataRelation(this.RuleTable.TableName + "_" + this.PageRulesTable.TableName, ds.Tables[this.RuleTable.TableName].Columns["RuleID"], ds.Tables[this.PageRulesTable.TableName].Columns["RuleID"]);
                ds.Relations.Add(relation3);
            }
            if (!ds.Relations.Contains(this.RuleTable.TableName + "_" + this.ItemRulesTable.TableName))
            {
                DataRelation relation4 = new DataRelation(this.RuleTable.TableName + "_" + this.ItemRulesTable.TableName, ds.Tables[this.RuleTable.TableName].Columns["RuleID"], ds.Tables[this.ItemRulesTable.TableName].Columns["RuleID"]);
                ds.Relations.Add(relation4);
            }
            if (!ds.Relations.Contains(this.ExpressionTable.TableName + "_" + this.RuleTable.TableName))
            {
                DataRelation relation5 = new DataRelation(this.ExpressionTable.TableName + "_" + this.RuleTable.TableName, ds.Tables[this.ExpressionTable.TableName].Columns["ExpressionID"], ds.Tables[this.RuleTable.TableName].Columns["ExpressionID"]);
                ds.Relations.Add(relation5);
            }
            if (!ds.Relations.Contains(this.RuleTable.TableName + "_" + this.RuleActionsTable.TableName))
            {
                DataRelation relation6 = new DataRelation(this.RuleTable.TableName + "_" + this.RuleActionsTable.TableName, ds.Tables[this.RuleTable.TableName].Columns["RuleID"], ds.Tables[this.RuleActionsTable.TableName].Columns["RuleID"]);
                ds.Relations.Add(relation6);
            }
            if (!ds.Relations.Contains(this.ActionTable.TableName + "_" + this.RuleActionsTable.TableName))
            {
                DataRelation relation7 = new DataRelation(this.ActionTable.TableName + "_" + this.RuleActionsTable.TableName, ds.Tables[this.ActionTable.TableName].Columns["ActionID"], ds.Tables[this.RuleActionsTable.TableName].Columns["ActionID"]);
                ds.Relations.Add(relation7);
            }
            if (!ds.Relations.Contains(this.ExpressionTable.TableName + "_Root" + this.ExpressionTable.TableName))
            {
                DataRelation relation8 = new DataRelation(this.ExpressionTable.TableName + "_Root" + this.ExpressionTable.TableName, ds.Tables[this.ExpressionTable.TableName].Columns["ExpressionID"], ds.Tables[this.ExpressionTable.TableName].Columns["Root"]);
                ds.Relations.Add(relation8);
            }
            if (!ds.Relations.Contains(this.ExpressionTable.TableName + "_Parent" + this.ExpressionTable.TableName))
            {
                DataRelation relation9 = new DataRelation(this.ExpressionTable.TableName + "_Parent" + this.ExpressionTable.TableName, ds.Tables[this.ExpressionTable.TableName].Columns["ExpressionID"], ds.Tables[this.ExpressionTable.TableName].Columns["Parent"]);
                ds.Relations.Add(relation9);
            }
            if (!ds.Relations.Contains(this.OperandTable.TableName + "_LEFT_" + this.ExpressionTable.TableName))
            {
                DataRelation relation10 = new DataRelation(this.OperandTable.TableName + "_LEFT_" + this.ExpressionTable.TableName, ds.Tables[this.OperandTable.TableName].Columns["OperandID"], ds.Tables[this.ExpressionTable.TableName].Columns["LeftOperand"]);
                ds.Relations.Add(relation10);
            }
            if (!ds.Relations.Contains(this.OperandTable.TableName + "_Right_" + this.ExpressionTable.TableName))
            {
                DataRelation relation11 = new DataRelation(this.OperandTable.TableName + "_Right_" + this.ExpressionTable.TableName, ds.Tables[this.OperandTable.TableName].Columns["OperandID"], ds.Tables[this.ExpressionTable.TableName].Columns["RightOperand"]);
                ds.Relations.Add(relation11);
            }
            if (!ds.Relations.Contains(this.OperandTable.TableName + "_" + this.ItemOperandTable.TableName))
            {
                DataRelation relation12 = new DataRelation(this.OperandTable.TableName + "_" + this.ItemOperandTable.TableName, ds.Tables[this.OperandTable.TableName].Columns["OperandID"], ds.Tables[this.ItemOperandTable.TableName].Columns["OperandID"]);
                ds.Relations.Add(relation12);
            }
            if (!ds.Relations.Contains(this.OperandTable.TableName + "_" + this.ValueOperandTable.TableName))
            {
                DataRelation relation13 = new DataRelation(this.OperandTable.TableName + "_" + this.ValueOperandTable.TableName, ds.Tables[this.OperandTable.TableName].Columns["OperandID"], ds.Tables[this.ValueOperandTable.TableName].Columns["OperandID"]);
                ds.Relations.Add(relation13);
            }
            if (!ds.Relations.Contains(this.OperandTable.TableName + "_" + this.ProfileOperandTable.TableName))
            {
                DataRelation relation14 = new DataRelation(this.OperandTable.TableName + "_" + this.ProfileOperandTable.TableName, ds.Tables[this.OperandTable.TableName].Columns["OperandID"], ds.Tables[this.ProfileOperandTable.TableName].Columns["OperandID"]);
                ds.Relations.Add(relation14);
            }
            if (!ds.Relations.Contains(this.OperandTable.TableName + "_" + this.ResponseOperandTable.TableName))
            {
                DataRelation relation15 = new DataRelation(this.OperandTable.TableName + "_" + this.ResponseOperandTable.TableName, ds.Tables[this.OperandTable.TableName].Columns["OperandID"], ds.Tables[this.ResponseOperandTable.TableName].Columns["OperandID"]);
                ds.Relations.Add(relation15);
            }
            if (!ds.Relations.Contains(this.ActionTable.TableName + "_" + this.BranchActionTable.TableName))
            {
                DataRelation relation16 = new DataRelation(this.ActionTable.TableName + "_" + this.BranchActionTable.TableName, ds.Tables[this.ActionTable.TableName].Columns["ActionID"], ds.Tables[this.BranchActionTable.TableName].Columns["ActionID"]);
                ds.Relations.Add(relation16);
            }
            if (!ds.Relations.Contains(this.BranchActionTable.TableName + "_" + this.PageDataTable.TableName))
            {
                DataRelation relation17 = new DataRelation(this.BranchActionTable.TableName + "_" + this.PageDataTable.TableName, ds.Tables[this.PageDataTable.TableName].Columns["PageID"], ds.Tables[this.BranchActionTable.TableName].Columns["GoToPageID"]);
                ds.Relations.Add(relation17);
            }
            this.OperandTable.Columns["OperandID"].AutoIncrement = true;
            this.OperandTable.Columns["OperandID"].AutoIncrementSeed = -1L;
            this.OperandTable.Columns["OperandID"].AutoIncrementStep = -1L;
            this.OperandTable.PrimaryKey = new DataColumn[] { this.OperandTable.Columns["OperandID"] };
            this.ExpressionTable.Columns["ExpressionID"].AutoIncrement = true;
            this.ExpressionTable.Columns["ExpressionID"].AutoIncrementSeed = -1L;
            this.ExpressionTable.Columns["ExpressionID"].AutoIncrementStep = -1L;
            this.ExpressionTable.PrimaryKey = new DataColumn[] { this.ExpressionTable.Columns["ExpressionID"] };
            this.ActionTable.Columns["ActionID"].AutoIncrement = true;
            this.ActionTable.Columns["ActionID"].AutoIncrementSeed = -1L;
            this.ActionTable.Columns["ActionID"].AutoIncrementStep = -1L;
            this.ActionTable.PrimaryKey = new DataColumn[] { this.ActionTable.Columns["ActionID"] };
            this.RuleTable.Columns["RuleID"].AutoIncrement = true;
            this.RuleTable.Columns["RuleID"].AutoIncrementSeed = -1L;
            this.RuleTable.Columns["RuleID"].AutoIncrementStep = -1L;
            this.RuleTable.PrimaryKey = new DataColumn[] { this.RuleTable.Columns["RuleID"] };
        }

        public int CreateEmptyBranchRule(TemplatePage dependent)
        {
            return this.CreateBranchRule(dependent, this.Template.TemplatePages[this.Template.TemplatePages.Count - 1]);
        }

        public int CreateEmptyConditionRule(ItemData dependent)
        {
            int actionID = this.CreateConditionAction();
            int ruleID = this.CreateRootLevelRule();
            this.AddActionToRule(ruleID, actionID);
            this.AddRuleToItem(dependent.ID.Value, ruleID);
            return ruleID;
        }

        public int CreateEmptyConditionRule(TemplatePage dependent)
        {
            int actionID = this.CreateConditionAction();
            int ruleID = this.CreateRootLevelRule();
            this.AddActionToRule(ruleID, actionID);
            this.AddRuleToPage(dependent, ruleID, RuleEventTrigger.Load);
            return ruleID;
        }

        public int CreateItemExpression(int parentExpressionID, ItemData source, LogicalOperator condition, string valueToCompare)
        {
            return this.CreateItemExpression(parentExpressionID, source, condition, null, valueToCompare);
        }

        public int CreateItemExpression(int parentExpressionID, ItemData source, LogicalOperator condition, int? valueToCompare, string answerText)
        {
            int num3;
            int root = (int) this.ExpressionTable.Select("ExpressionID=" + parentExpressionID)[0]["Root"];
            int operandID = this.InsertOperand(typeof(ItemOperandData));
            this.InsertItemOperand(operandID, source.ID.Value, null);
            if (valueToCompare.HasValue)
            {
                num3 = this.InsertOperand(typeof(OptionOperandData));
                this.InsertValueOperand(num3, new int?(source.ID.Value), new int?(valueToCompare.Value), answerText);
            }
            else
            {
                num3 = this.InsertOperand(typeof(StringOperandData));
                this.InsertValueOperand(num3, new int?(source.ID.Value), null, answerText);
            }
            return this.InsertExpression(condition, operandID, num3, parentExpressionID, 2, null, root);
        }

        public int CreateMatrixItemExpression(int parentExpressionID, MatrixItemData parentItem, ItemData source, LogicalOperator condition, string valueToCompare)
        {
            int root = (int) this.ExpressionTable.Select("ExpressionID=" + parentExpressionID)[0]["Root"];
            int operandID = this.InsertOperand(typeof(MatrixItemOperandData));
            this.InsertItemOperand(operandID, source.ID.Value, new int?(parentItem.ID.Value));
            int num3 = this.InsertOperand(typeof(StringOperandData));
            this.InsertValueOperand(num3, new int?(source.ID.Value), null, valueToCompare);
            return this.InsertExpression(condition, operandID, num3, parentExpressionID, 2, null, root);
        }

        public int CreateMatrixItemExpression(int parentExpressionID, MatrixItemData parentItem, ItemData source, LogicalOperator condition, int? valueToCompare)
        {
            int root = (int) this.ExpressionTable.Select("ExpressionID=" + parentExpressionID)[0]["Root"];
            int operandID = this.InsertOperand(typeof(MatrixItemOperandData));
            this.InsertItemOperand(operandID, source.ID.Value, new int?(parentItem.ID.Value));
            int num3 = this.InsertOperand(typeof(OptionOperandData));
            if (valueToCompare.HasValue)
            {
                this.InsertValueOperand(num3, new int?(source.ID.Value), new int?(valueToCompare.Value), null);
            }
            else
            {
                this.InsertValueOperand(num3, new int?(source.ID.Value), null, null);
            }
            return this.InsertExpression(condition, operandID, num3, parentExpressionID, 2, null, root);
        }

        public int CreateProfileExpression(int parentExpressionID, string key, LogicalOperator condition, string valueToCompare)
        {
            int root = (int) this.ExpressionTable.Select("ExpressionID=" + parentExpressionID)[0]["Root"];
            int operandID = this.InsertOperand(typeof(ProfileOperandData));
            this.InsertProfileOperand(operandID, key);
            int num3 = this.InsertOperand(typeof(StringOperandData));
            this.InsertValueOperand(num3, null, null, valueToCompare);
            return this.InsertExpression(condition, operandID, num3, parentExpressionID, 2, null, root);
        }

        public int CreateResponseExpression(int parentExpressionID, string key, LogicalOperator condition, string valueToCompare)
        {
            int root = (int) this.ExpressionTable.Select("ExpressionID=" + parentExpressionID)[0]["Root"];
            int operandID = this.InsertOperand(typeof(ResponseOperandData));
            this.InsertResponseOperand(operandID, key);
            int num3 = this.InsertOperand(typeof(StringOperandData));
            this.InsertValueOperand(num3, null, null, valueToCompare);
            return this.InsertExpression(condition, operandID, num3, parentExpressionID, 2, null, root);
        }

        private int CreateRootExpression()
        {
            return this.InsertCompositeExpression(null, 0, "/", null, LogicalConnector.OR);
        }

        private int CreateRootLevelRule()
        {
            return this.InsertRule(this.CreateRootExpression());
        }

        internal RuleData CreateRuleData(int? ruleID)
        {
            if (ruleID.HasValue)
            {
                return this.GetRuleData(ruleID.Value);
            }
            CompositeExpressionData expression = new CompositeExpressionData(this.Template);
            return new RuleData(this.Template, expression, new List<ActionData>());
        }

        public void DeleteExpression(int expressionID)
        {
            DataRow[] rowArray = this.ExpressionTable.Select("ExpressionID=" + expressionID);
            if (rowArray.Length > 0)
            {
                DataRow row = rowArray[0];
                if (row["Parent"] == DBNull.Value)
                {
                    this.DeleteExpressionTree(expressionID);
                }
                else
                {
                    int num = (int) row["Parent"];
                    if (this.ExpressionTable.Select("[Parent]=" + num).Length > 1)
                    {
                        this.DeleteExpressionTree(expressionID);
                    }
                    else
                    {
                        this.DeleteExpression(num);
                    }
                }
            }
        }

        private void DeleteExpressionTree(int expressionID)
        {
            foreach (DataRow row in this.ExpressionTable.Select("[Parent]=" + expressionID))
            {
                this.DeleteExpressionTree((int) row["ExpressionID"]);
            }
            foreach (DataRow row2 in this.ExpressionTable.Select("ExpressionID=" + expressionID))
            {
                int? nullable = null;
                int? nullable2 = null;
                if (row2["LeftOperand"] != DBNull.Value)
                {
                    nullable = new int?((int) row2["LeftOperand"]);
                }
                if (row2["RightOperand"] != DBNull.Value)
                {
                    nullable2 = new int?((int) row2["RightOperand"]);
                }
                row2.Delete();
                if (nullable.HasValue)
                {
                    this.DeleteOperand(nullable.Value);
                }
                if (nullable2.HasValue)
                {
                    this.DeleteOperand(nullable2.Value);
                }
            }
        }

        internal bool DeleteInvalidSubscriberExpressions(ItemData item)
        {
            bool flag = false;
            if (item is ICompositeItemData)
            {
                foreach (ItemData data in ((ICompositeItemData) item).GetChildItemDatas())
                {
                    bool flag2 = this.DeleteInvalidSubscriberExpressions(data);
                    if (!flag)
                    {
                        flag = flag2;
                    }
                }
            }
            if (this.ItemHasSubscriberExpressions(item))
            {
                if (!(item is SelectItemData) || !item.ID.HasValue)
                {
                    return flag;
                }
                foreach (DataRow row in this.ItemOperandTable.Select("ItemID=" + item.ID))
                {
                    DataRow[] rowArray2 = this.ExpressionTable.Select("LeftOperand=" + ((int) row["OperandID"]));
                    if (rowArray2.Length > 0)
                    {
                        DataRow row2 = rowArray2[0];
                        int num = (int) row2["RightOperand"];
                        DataRow[] rowArray3 = this.ValueOperandTable.Select("OperandID=" + num);
                        if (rowArray3.Length > 0)
                        {
                            DataRow row3 = rowArray3[0];
                            if (row3["OptionID"] != DBNull.Value)
                            {
                                int num2 = (int) row3["OptionID"];
                                ReadOnlyCollection<ListOptionData> options = ((SelectItemData) item).Options;
                                for (int i = 0; i < options.Count; i++)
                                {
                                    if (options[i].OptionID == num2)
                                    {
                                        break;
                                    }
                                    if ((i + 1) == options.Count)
                                    {
                                        this.DeleteExpression((int) row2["ExpressionID"]);
                                        flag = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return flag;
        }

        internal bool DeleteInvalidSubscriberExpressions(ReadOnlyCollection<ItemData> validItems)
        {
            bool flag = false;
            List<int> list = new List<int>();
            foreach (ItemData data in validItems)
            {
                list.Add(data.ID.Value);
            }
            foreach (DataRow row in this.ItemOperandTable.Select(null, null, DataViewRowState.CurrentRows))
            {
                if (row.RowState != DataRowState.Deleted)
                {
                    int item = (int) row["ItemID"];
                    if (!list.Contains(item))
                    {
                        flag = true;
                        this.DeleteSubscriberExpressions(item);
                    }
                }
            }
            foreach (ItemData data2 in validItems)
            {
                flag = flag || this.DeleteInvalidSubscriberExpressions(data2);
            }
            return flag;
        }

        private void DeleteOperand(int operandID)
        {
            foreach (DataRow row in this.OperandTable.Select("OperandID=" + operandID))
            {
                row.Delete();
            }
        }

        internal void DeletePageTargetExpressions(TemplatePage page)
        {
            foreach (DataRow row in this.FindPageActionExpressions(Convert.ToInt32(page.GetID)))
            {
                this.DeleteRule(Convert.ToInt32(row["RuleID"]));
            }
        }

        public void DeleteRule(int ruleID)
        {
            foreach (DataRow row in this.RuleTable.Select("RuleID=" + ruleID))
            {
                this.DeleteExpression((int) row["ExpressionID"]);
                row.Delete();
            }
        }

        internal void DeleteRules(ItemData item)
        {
            if (item is ICompositeItemData)
            {
                foreach (ItemData data in ((ICompositeItemData) item).GetChildItemDatas())
                {
                    this.DeleteRules(data);
                }
            }
            if (this.ItemHasCondition(item))
            {
                this.DeleteRule(this.GetConditionRuleID(item));
            }
        }

        internal void DeleteRules(TemplatePage page)
        {
            if (this.PageHasCondition(page))
            {
                this.DeleteRule(this.GetConditionRuleID(page));
            }
            if (this.PageHasBranches(page))
            {
                int[] branchRuleIDs = this.GetBranchRuleIDs(page);
                for (int i = 0; i < branchRuleIDs.Length; i++)
                {
                    this.DeleteRule(branchRuleIDs[i]);
                }
            }
            foreach (ItemData data in page.Items)
            {
                this.DeleteRules(data);
            }
        }

        internal void DeleteSubscriberExpressions(ItemData item)
        {
            if (item is ICompositeItemData)
            {
                foreach (ItemData data in ((ICompositeItemData) item).GetChildItemDatas())
                {
                    this.DeleteSubscriberExpressions(data);
                }
            }
            this.DeleteSubscriberExpressions(item.ID.Value);
        }

        internal void DeleteSubscriberExpressions(TemplatePage page)
        {
            foreach (ItemData data in page.Items)
            {
                this.DeleteSubscriberExpressions(data);
            }
        }

        internal void DeleteSubscriberExpressions(int itemID)
        {
            foreach (DataRow row in this.FindItemOperandSubscribersToItem(itemID))
            {
                this.DeleteExpression((int) row["ExpressionID"]);
            }
        }

        private int DetermineContainingPagePosition(int ruleID)
        {
            if (this.PageRulesTable.Select("RuleID=" + ruleID).Length > 0)
            {
                int id = (int) this.PageRulesTable.Select("RuleID=" + ruleID)[0]["PageID"];
                return this._template.GetPage(id).Position;
            }
            int itemDataID = (int) this.ItemRulesTable.Select("RuleID=" + ruleID)[0]["ItemID"];
            return this._template.GetPagePositionForItem(this._template.GetItem(itemDataID)).Value;
        }

        private DataRow[] FindItemOperandSubscribers(int rootExpressionID)
        {
            DataRow[] rowArray = this.ExpressionTable.Select("Root=" + rootExpressionID + " AND LeftOperand IS NOT NULL");
            List<DataRow> list = new List<DataRow>();
            for (int i = 0; i < rowArray.Length; i++)
            {
                int num2 = (int) rowArray[i]["LeftOperand"];
                if (this.ItemOperandTable.Select("OperandID=" + num2).Length > 0)
                {
                    list.Add(rowArray[i]);
                }
            }
            return list.ToArray();
        }

        private DataRow[] FindItemOperandSubscribersToItem(int itemID)
        {
            DataRow[] rowArray = this.ItemOperandTable.Select("ItemID=" + itemID);
            List<DataRow> list = new List<DataRow>();
            foreach (DataRow row in rowArray)
            {
                list.Add(this.ExpressionTable.Select("LeftOperand=" + ((int) row["OperandID"]))[0]);
            }
            return list.ToArray();
        }

        private DataRow[] FindPageActionExpressions(int pageID)
        {
            List<DataRow> list = new List<DataRow>();
            foreach (DataRow row in this.BranchActionTable.Select("GoToPageID=" + pageID))
            {
                foreach (DataRow row2 in this.RuleActionsTable.Select("ActionID=" + row["ActionID"]))
                {
                    if (row2["RuleID"] != DBNull.Value)
                    {
                        list.Add(row2);
                    }
                }
            }
            return list.ToArray();
        }

        private void FlattenActionRuleTableMapping()
        {
            DataColumn column = new DataColumn("RuleID", typeof(int));
            this.ActionTable.Columns.Add(column);
            foreach (DataRow row in this.ActionTable.Rows)
            {
                int num = (int) this.RuleActionsTable.Select("ActionID=" + ((int) row["ActionID"]))[0]["RuleID"];
                row["RuleID"] = num;
            }
        }

        public int GetActionID(int ruleID)
        {
            return (int) this.RuleActionsTable.Select("RuleID=" + ruleID)[0]["ActionID"];
        }

        public int GetBranchActionTargetPage(int actionID)
        {
            return (int) this.BranchActionTable.Select("ActionID=" + actionID)[0]["GoToPageID"];
        }

        public int[] GetBranchRuleIDs(TemplatePage page)
        {
            if (!this.PageHasBranches(page))
            {
                this.CreateEmptyBranchRule(page);
            }
            List<int> list = new List<int>();
            foreach (RuleData data in this.GetBranchRulesForPage(page))
            {
                list.Add(data.ID.Value);
            }
            return list.ToArray();
        }

        public RuleData[] GetBranchRulesForPage(TemplatePage page)
        {
            RuleData data;
            DataRow[] rowArray = new DataRow[0];
            if (page.Identity.HasValue)
            {
                rowArray = this.PageRulesTable.Select("PageID=" + page.GetID);
            }
            List<RuleData> list = new List<RuleData>();
            if (rowArray.Length == 0)
            {
                data = this.CreateRuleData(null);
                data.Trigger = RuleEventTrigger.UnLoad;
                data.Actions.Add(new BranchPageActionData(this.Template, page));
                list.Add(data);
            }
            else
            {
                foreach (DataRow row in rowArray)
                {
                    DataRow row2 = this.RuleActionsTable.Select("RuleID=" + ((int) row["RuleID"]))[0];
                    DataRow row3 = this.ActionTable.Select("ActionID=" + ((int) row2["ActionID"]))[0];
                    string str = (string) row3["ActionTypeName"];
                    string str2 = (string) row3["ActionAssembly"];
                    if (Type.GetType(str + "," + str2) == typeof(BranchPageActionData))
                    {
                        data = new RuleData(this.Template, new CompositeExpressionData(this.Template), new List<ActionData>()) {
                            Trigger = RuleEventTrigger.UnLoad,
                            SetID = (int) row["RuleID"],
                            Parent = page
                        };
                        data.Load(this._templateData);
                        list.Add(data);
                    }
                }
                if (list.Count > 0)
                {
                    return list.ToArray();
                }
                data = this.CreateRuleData(null);
                data.Trigger = RuleEventTrigger.UnLoad;
                data.Actions.Add(new BranchPageActionData(this.Template, page));
                list.Add(data);
            }
            return list.ToArray();
        }

        public RuleData GetConditionForItem(ItemData item)
        {
            RuleData data;
            DataRow[] rowArray = new DataRow[0];
            if (item.ID.HasValue)
            {
                rowArray = this.ItemRulesTable.Select("ItemID=" + item.ID);
            }
            if (rowArray.Length == 0)
            {
                data = this.CreateRuleData(null);
                data.Actions.Add(new IncludeExcludeActionData(this.Template, item));
                return data;
            }
            data = new RuleData(this.Template, new CompositeExpressionData(this.Template), new List<ActionData>()) {
                SetID = (int) rowArray[0]["RuleID"],
                Parent = item
            };
            data.Load(this._templateData);
            return data;
        }

        public RuleData GetConditionForPage(TemplatePage page)
        {
            RuleData data;
            DataRow[] rowArray = new DataRow[0];
            if (page.Identity.HasValue)
            {
                rowArray = this.PageRulesTable.Select("PageID=" + page.GetID);
            }
            if (rowArray.Length == 0)
            {
                data = this.CreateRuleData(null);
                data.Trigger = RuleEventTrigger.Load;
                data.Actions.Add(new IncludeExcludeActionData(this.Template, page));
                return data;
            }
            foreach (DataRow row in rowArray)
            {
                DataRow row2 = this.RuleActionsTable.Select("RuleID=" + ((int) row["RuleID"]))[0];
                DataRow row3 = this.ActionTable.Select("ActionID=" + ((int) row2["ActionID"]))[0];
                string str = (string) row3["ActionTypeName"];
                string str2 = (string) row3["ActionAssembly"];
                if (Type.GetType(str + "," + str2) == typeof(IncludeExcludeActionData))
                {
                    data = new RuleData(this.Template, new CompositeExpressionData(this.Template), new List<ActionData>()) {
                        Trigger = RuleEventTrigger.Load,
                        SetID = (int) row["RuleID"],
                        Parent = page
                    };
                    data.Load(this._templateData);
                    return data;
                }
            }
            data = this.CreateRuleData(null);
            data.Trigger = RuleEventTrigger.Load;
            data.Actions.Add(new IncludeExcludeActionData(this.Template, page));
            return data;
        }

        public int GetConditionRuleID(ItemData item)
        {
            if (!this.ItemHasCondition(item))
            {
                this.CreateEmptyConditionRule(item);
            }
            return this.GetConditionForItem(item).ID.Value;
        }

        public int GetConditionRuleID(TemplatePage page)
        {
            if (!this.PageHasCondition(page))
            {
                this.CreateEmptyConditionRule(page);
            }
            return this.GetConditionForPage(page).ID.Value;
        }

        public DataTable GetExpressionAsTable(int expressionID)
        {
            DataTable table = new DataTable();
            DataColumn column = new DataColumn("ExpressionID", typeof(int));
            DataColumn column2 = new DataColumn("ExpressionText", typeof(string));
            table.Columns.Add(column);
            table.Columns.Add(column2);
            foreach (DataRow row in this.SelectChildExpressions(expressionID))
            {
                int num = (int) row["ExpressionID"];
                DataRow row2 = table.NewRow();
                row2["ExpressionID"] = num;
                row2["ExpressionText"] = this.PrepareExpressionText(row);
                table.Rows.Add(row2);
            }
            return table;
        }

        public int? GetExpressionParentID(int expressionID)
        {
            return DbUtility.GetValueFromDataTable<int?>(this.ExpressionTable, "Parent", "ExpressionID=" + expressionID, null, null);
        }

        public Dictionary<int, DataTable> GetExpressionTables(ItemData item)
        {
            int conditionRuleID = this.GetConditionRuleID(item);
            int rootExpressionIDForRule = this.GetRootExpressionIDForRule(conditionRuleID);
            DataRow[] rowArray = this.SelectChildExpressions(rootExpressionIDForRule);
            Dictionary<int, DataTable> dictionary = new Dictionary<int, DataTable>();
            foreach (DataRow row in rowArray)
            {
                int expressionID = (int) row["ExpressionID"];
                DataTable expressionAsTable = this.GetExpressionAsTable(expressionID);
                dictionary.Add(expressionID, expressionAsTable);
            }
            return dictionary;
        }

        public Dictionary<int, DataTable> GetExpressionTables(TemplatePage page)
        {
            int conditionRuleID = this.GetConditionRuleID(page);
            int rootExpressionIDForRule = this.GetRootExpressionIDForRule(conditionRuleID);
            DataRow[] rowArray = this.SelectChildExpressions(rootExpressionIDForRule);
            Dictionary<int, DataTable> dictionary = new Dictionary<int, DataTable>();
            foreach (DataRow row in rowArray)
            {
                int expressionID = (int) row["ExpressionID"];
                DataTable expressionAsTable = this.GetExpressionAsTable(expressionID);
                dictionary.Add(expressionID, expressionAsTable);
            }
            return dictionary;
        }

        private string GetIndexedItemText(ItemData itemData)
        {
            if (this.Template.GetPagePositionForItem(itemData).HasValue)
            {
                int num = this.Template.GetPagePositionForItem(itemData).Value;
                TemplatePage page = this.Template.TemplatePages[num];
                int num2 = page.Items.IndexOf(itemData) + 1;
                return this.GetItemText(itemData, this.Template.EditLanguage, string.Concat(new object[] { num, ".", num2, " " }), 0x40);
            }
            return string.Empty;
        }

        private int GetItemPagePosition(int itemID)
        {
            return this._template.GetPagePositionForItem(this._template.GetItem(itemID)).Value;
        }

        public string GetItemText(ItemData itemData, string languageCode, string prefix, int? maxLength)
        {
            StringBuilder builder = new StringBuilder();
            if (itemData is LabelledItemData)
            {
                builder.Append(TextManager.GetText(((LabelledItemData) itemData).TextID, languageCode));
                if (builder.Length == 0)
                {
                    builder.Append(TextManager.GetText(((LabelledItemData) itemData).SubTextID, languageCode));
                }
            }
            else if (itemData is MatrixItemData)
            {
                MatrixItemData data = (MatrixItemData) itemData;
                MatrixItemTextDecorator decorator = (MatrixItemTextDecorator) data.CreateTextDecorator(this.Template.EditLanguage);
                builder.Append(decorator.Text);
            }
            if (builder.Length == 0)
            {
                builder.Append(itemData.Alias);
            }
            builder.Insert(0, prefix);
            if (maxLength.HasValue && (maxLength.Value < builder.Length))
            {
                builder.Remove(maxLength.Value, builder.Length - maxLength.Value);
                builder.Append("...");
            }
            return builder.ToString();
        }

        public string GetOperandText(int operandID)
        {
            DataRow row = this.OperandTable.Select("OperandID=" + operandID)[0];
            if (row["TypeName"] == DBNull.Value)
            {
                return string.Empty;
            }
            if (((string) row["TypeName"]) == "Checkbox.Forms.Logic.Configuration.ItemOperandData")
            {
                DataRow row2 = this.ItemOperandTable.Select("OperandID=" + operandID)[0];
                int itemDataID = (int) row2["ItemID"];
                ItemData item = this.Template.GetItem(itemDataID);
                return this.GetIndexedItemText(item);
            }
            if (((string) row["TypeName"]) == "Checkbox.Forms.Logic.Configuration.MatrixItemOperandData")
            {
                DataRow row3 = this.ItemOperandTable.Select("OperandID=" + operandID)[0];
                int num2 = (int) row3["ParentItemID"];
                MatrixItemData itemData = (MatrixItemData) this.Template.GetItem(num2);
                if (itemData == null)
                {
                    return string.Empty;
                }
                MatrixItemTextDecorator decorator = (MatrixItemTextDecorator) itemData.CreateTextDecorator(this.Template.EditLanguage);
                int identity = (int) row3["ItemID"];
                ItemData configurationData = ItemConfigurationManager.GetConfigurationData(identity);
                StringBuilder builder = new StringBuilder();
                builder.Append(this.GetIndexedItemText(itemData));
                builder.Append(" -- ");
                Coordinate itemCoordinate = itemData.GetItemCoordinate(configurationData);
                if (itemCoordinate != null)
                {
                    builder.Append(decorator.GetRowText(itemCoordinate.Y));
                    builder.Append(" -- ");
                    builder.Append(decorator.GetColumnText(itemCoordinate.X));
                }
                return builder.ToString();
            }
            if (((string) row["TypeName"]) == "Checkbox.Forms.Logic.Configuration.OptionOperandData")
            {
                DataRow[] rowArray = this.ValueOperandTable.Select("OperandID=" + operandID);
                if (rowArray.Length > 0)
                {
                    DataRow row4 = rowArray[0];
                    if (row4["OptionID"] == DBNull.Value)
                    {
                        return string.Empty;
                    }
                    int optionID = (int) row4["OptionID"];
                    int num5 = (int) row4["ItemID"];
                    ItemData data4 = this.Template.GetItem(num5);
                    if ((data4 != null) && (data4 is SelectItemData))
                    {
                        ListOptionData option = ((SelectItemData) data4).GetOption(optionID);
                        if (option != null)
                        {
                            if (option.IsOther)
                            {
                                return TextManager.GetText("/controlText/ruleDataService/other", "Other", this._template.EditLanguage, new string[] { "en-US" });
                            }
                            return TextManager.GetText(option.TextID, this.Template.EditLanguage);
                        }
                    }
                }
            }
            else if (((string) row["TypeName"]) == "Checkbox.Forms.Logic.Configuration.StringOperandData")
            {
                DataRow[] rowArray2 = this.ValueOperandTable.Select("OperandID=" + operandID);
                if (rowArray2.Length > 0)
                {
                    DataRow row5 = rowArray2[0];
                    if (row5["AnswerValue"] != DBNull.Value)
                    {
                        return (string) row5["AnswerValue"];
                    }
                    return string.Empty;
                }
            }
            else if (((string) row["TypeName"]) == "Checkbox.Forms.Logic.Configuration.ResponseOperandData")
            {
                DataRow[] rowArray3 = this.ResponseOperandTable.Select("OperandID=" + operandID);
                if (rowArray3.Length > 0)
                {
                    DataRow row6 = rowArray3[0];
                    if (row6["ResponseKey"] != DBNull.Value)
                    {
                        return (string) row6["ResponseKey"];
                    }
                    return string.Empty;
                }
            }
            else if (((string) row["TypeName"]) == "Checkbox.Forms.Logic.Configuration.ProfileOperandData")
            {
                DataRow[] rowArray4 = this.ProfileOperandTable.Select("OperandID=" + operandID);
                if (rowArray4.Length > 0)
                {
                    DataRow row7 = rowArray4[0];
                    if (row7["ProfileKey"] != DBNull.Value)
                    {
                        return (string) row7["ProfileKey"];
                    }
                    return string.Empty;
                }
            }
            return "No Text Specified";
        }

        public int GetRootExpressionIDForRule(int ruleID)
        {
            return (int) this.RuleTable.Select("RuleID=" + ruleID)[0]["ExpressionID"];
        }

        internal RuleData GetRuleData(int ruleID)
        {
            RuleData data = new RuleData(this.Template, new CompositeExpressionData(this.Template), new List<ActionData>()) {
                SetID = ruleID
            };
            data.Load(this._templateData);
            return data;
        }

        private void HandleForItem(ItemData item, int newPagePosition)
        {
            if (item is ICompositeItemData)
            {
                foreach (ItemData data in ((ICompositeItemData) item).GetChildItemDatas())
                {
                    this.HandleForItem(data, newPagePosition);
                }
            }
            if (item.ID.HasValue)
            {
                DataRow[] rowArray = this.ItemRulesTable.Select("ItemID=" + item.ID.Value);
                if (rowArray.Length > 0)
                {
                    int num = (int) rowArray[0]["RuleID"];
                    int rootExpressionID = (int) this.RuleTable.Select("RuleID=" + num)[0]["ExpressionID"];
                    DataRow[] rowArray2 = this.FindItemOperandSubscribers(rootExpressionID);
                    for (int i = 0; i < rowArray2.Length; i++)
                    {
                        int itemID = (int) this.ItemOperandTable.Select("OperandID=" + ((int) rowArray2[i]["LeftOperand"]))[0]["ItemID"];
                        if (newPagePosition <= this.GetItemPagePosition(itemID))
                        {
                            this.DeleteExpression((int) rowArray2[i]["ExpressionID"]);
                        }
                    }
                }
            }
        }

        internal void HandleSubscriberMoved(ItemData item, int newPagePosition)
        {
            this.HandleForItem(item, newPagePosition);
        }

        internal void HandleSubscriberMoved(TemplatePage page, int newPagePosition)
        {
            if (page.Identity.HasValue)
            {
                foreach (ItemData data in page.Items)
                {
                    this.HandleForItem(data, newPagePosition);
                }
                DataRow[] rowArray = this.PageRulesTable.Select("PageID=" + page.Identity.Value);
                if (rowArray.Length > 0)
                {
                    foreach (DataRow row in rowArray)
                    {
                        int ruleID = (int) row["RuleID"];
                        int rootExpressionID = (int) this.RuleTable.Select("RuleID=" + ruleID)[0]["ExpressionID"];
                        bool flag = ((string) row["EventTrigger"]) == "UnLoad";
                        if (flag)
                        {
                            int num3 = (int) this.ActionTable.Select("RuleID=" + ruleID)[0]["ActionID"];
                            int id = (int) this.BranchActionTable.Select("ActionID=" + num3)[0]["GoToPageID"];
                            if (this._template.GetPage(id).Position <= newPagePosition)
                            {
                                this.DeleteRule(ruleID);
                                continue;
                            }
                        }
                        DataRow[] rowArray2 = this.FindItemOperandSubscribers(rootExpressionID);
                        for (int i = 0; i < rowArray2.Length; i++)
                        {
                            int itemID = (int) this.ItemOperandTable.Select("OperandID=" + ((int) rowArray2[i]["LeftOperand"]))[0]["ItemID"];
                            int itemPagePosition = this.GetItemPagePosition(itemID);
                            if (flag)
                            {
                                if (page.Position < itemPagePosition)
                                {
                                    this.DeleteExpression((int) rowArray2[i]["ExpressionID"]);
                                }
                            }
                            else if (page.Position <= itemPagePosition)
                            {
                                this.DeleteExpression((int) rowArray2[i]["ExpressionID"]);
                            }
                        }
                    }
                }
            }
        }

        public void ImportTemplateRules(DataSet ds)
        {
        }

        private int InsertAction(Type type)
        {
            DataRow row = this.ActionTable.NewRow();
            row["ActionTypeName"] = type.FullName;
            row["ActionAssembly"] = type.Assembly.GetName().Name;
            this.ActionTable.Rows.Add(row);
            return (int) row["ActionID"];
        }

        private void InsertBranchAction(int actionID, int targetPageID)
        {
            DataRow row = this.BranchActionTable.NewRow();
            row["ActionID"] = actionID;
            row["GoToPageID"] = targetPageID;
            this.BranchActionTable.Rows.Add(row);
        }

        private int InsertCompositeExpression(int? parent, int depth, string lineage, int? root, LogicalConnector childRelation)
        {
            DataRow row = this.ExpressionTable.NewRow();
            row["Operator"] = LogicalOperator.OperatorNotSpecified;
            if (parent.HasValue)
            {
                row["Parent"] = parent.Value;
            }
            row["Depth"] = depth;
            row["Lineage"] = lineage;
            if (root.HasValue)
            {
                row["Root"] = root.Value;
            }
            row["ChildRelation"] = childRelation.ToString();
            this.ExpressionTable.Rows.Add(row);
            return (int) row["ExpressionID"];
        }

        private int InsertExpression(LogicalOperator operation, int leftOperandID, int rightOperandID, int parentID, int depth, string lineage, int root)
        {
            DataRow row = this.ExpressionTable.NewRow();
            row["Operator"] = (int) operation;
            row["LeftOperand"] = leftOperandID;
            row["RightOperand"] = rightOperandID;
            row["Parent"] = parentID;
            row["Depth"] = depth;
            row["Lineage"] = lineage;
            row["Root"] = root;
            this.ExpressionTable.Rows.Add(row);
            return (int) row["ExpressionID"];
        }

        private void InsertItemOperand(int operandID, int itemID, int? parentItemID)
        {
            DataRow row = this.ItemOperandTable.NewRow();
            row["OperandID"] = operandID;
            row["ItemID"] = itemID;
            if (parentItemID.HasValue)
            {
                row["ParentItemID"] = parentItemID.Value;
            }
            this.ItemOperandTable.Rows.Add(row);
        }

        private int InsertOperand(Type type)
        {
            DataRow row = this.OperandTable.NewRow();
            row["TypeName"] = type.FullName;
            row["TypeAssembly"] = type.Assembly.GetName().Name;
            this.OperandTable.Rows.Add(row);
            return (int) row["OperandID"];
        }

        private void InsertProfileOperand(int operandID, string attribute)
        {
            DataRow row = this.ProfileOperandTable.NewRow();
            row["OperandID"] = operandID;
            row["ProfileKey"] = attribute;
            this.ProfileOperandTable.Rows.Add(row);
        }

        private void InsertResponseOperand(int operandID, string attribute)
        {
            DataRow row = this.ResponseOperandTable.NewRow();
            row["OperandID"] = operandID;
            row["ResponseKey"] = attribute;
            this.ResponseOperandTable.Rows.Add(row);
        }

        private int InsertRule(int expressionID)
        {
            DataRow row = this.RuleTable.NewRow();
            row["ExpressionID"] = expressionID;
            this.RuleTable.Rows.Add(row);
            return (int) row["RuleID"];
        }

        private void InsertValueOperand(int operandID, int? itemID, int? optionID, string answerValue)
        {
            DataRow row = this.ValueOperandTable.NewRow();
            row["OperandID"] = operandID;
            if (itemID.HasValue)
            {
                row["ItemID"] = itemID;
            }
            if (optionID.HasValue)
            {
                row["OptionID"] = optionID.Value;
            }
            if (answerValue != null)
            {
                row["AnswerValue"] = answerValue;
            }
            this.ValueOperandTable.Rows.Add(row);
        }

        public bool ItemHasCondition(ItemData item)
        {
            if (!item.ID.HasValue)
            {
                return false;
            }
            return (this.ItemRulesTable.Select("ItemID=" + item.ID).Length > 0);
        }

        private bool ItemHasSubscriberExpressions(ItemData item)
        {
            if (!item.ID.HasValue)
            {
                return false;
            }
            if (this.ItemOperandTable.Select("ItemID=" + item.ID).Length <= 0)
            {
                return false;
            }
            return true;
        }

        internal void NotifySubscribingExpressionsOfPublisherItemMoved(ItemData item, int newPagePosition)
        {
            if (item is ICompositeItemData)
            {
                foreach (ItemData data in ((ICompositeItemData) item).GetChildItemDatas())
                {
                    this.NotifySubscribingExpressionsOfPublisherItemMoved(data, newPagePosition);
                }
            }
            foreach (DataRow row in this.FindItemOperandSubscribersToItem(item.ID.Value))
            {
                int num = (int) row["Root"];
                int ruleID = (int) this.RuleTable.Select("ExpressionID=" + num)[0]["RuleID"];
                if (this.PageRulesTable.Select("RuleID=" + ruleID + " AND EventTrigger='UnLoad'").Length > 0)
                {
                    if (this.DetermineContainingPagePosition(ruleID) < newPagePosition)
                    {
                        this.DeleteExpression((int) row["ExpressionID"]);
                    }
                }
                else if (this.DetermineContainingPagePosition(ruleID) <= newPagePosition)
                {
                    this.DeleteExpression((int) row["ExpressionID"]);
                }
            }
        }

        public bool PageHasBranches(TemplatePage page)
        {
            if (!page.Identity.HasValue)
            {
                return false;
            }
            return (this.PageRulesTable.Select(string.Concat(new object[] { "PageID=", page.Identity.Value, " AND EventTrigger='", RuleEventTrigger.UnLoad, "'" })).Length > 0);
        }

        public bool PageHasCondition(TemplatePage page)
        {
            if (!page.Identity.HasValue)
            {
                return false;
            }
            return (this.PageRulesTable.Select(string.Concat(new object[] { "PageID=", page.Identity.Value, " AND EventTrigger='", RuleEventTrigger.Load, "'" })).Length > 0);
        }

        private string PrepareExpressionText(DataRow expressionData)
        {
            LogicalOperator @operator = (LogicalOperator) Enum.Parse(typeof(LogicalOperator), expressionData["Operator"].ToString());
            string text = TextManager.GetText("/enum/logicalOperator/" + @operator, this.Template.EditLanguage);
            string operandText = this.GetOperandText((int) expressionData["LeftOperand"]);
            string str3 = this.GetOperandText((int) expressionData["RightOperand"]);
            StringBuilder builder = new StringBuilder();
            builder.Append("IF \"");
            builder.Append(operandText);
            builder.Append("\" ");
            builder.Append(text);
            switch (@operator)
            {
                case LogicalOperator.Contains:
                case LogicalOperator.DoesNotContain:
                case LogicalOperator.Equal:
                case LogicalOperator.GreaterThan:
                case LogicalOperator.GreaterThanEqual:
                case LogicalOperator.LessThan:
                case LogicalOperator.LessThanEqual:
                case LogicalOperator.NotEqual:
                    builder.Append(" \" ");
                    builder.Append(str3);
                    builder.Append("\" ");
                    break;
            }
            builder.Append(TextManager.GetText("/controlText/simplifiedRuleBuilder/displayItem", TextManager.DefaultLanguage));
            return builder.ToString();
        }

        internal virtual void RemoveRuleFromItem(ItemData item, RuleData rule)
        {
            if (((this.ItemRulesTable != null) && item.ID.HasValue) && rule.ID.HasValue)
            {
                foreach (DataRow row in this.ItemRulesTable.Select(string.Concat(new object[] { "ItemID=", item.ID.Value, " AND RuleID=", rule.ID.Value }), string.Empty, DataViewRowState.CurrentRows))
                {
                    row.Delete();
                }
            }
        }

        internal virtual void RemoveRuleFromPage(TemplatePage page, RuleData rule)
        {
            if (((this.PageRulesTable != null) && page.Identity.HasValue) && rule.ID.HasValue)
            {
                foreach (DataRow row in this.PageRulesTable.Select(string.Format("PageID = {0} AND RuleID = {1}", page.Identity, rule.ID), string.Empty, DataViewRowState.CurrentRows))
                {
                    row.Delete();
                }
            }
        }

        public void ReplaceExpressionWithItemExpression(int expressionId, ItemData source, MatrixItemData sourceParent, LogicalOperator condition, int? valueToCompare, string answerText)
        {
            int? expressionParentID = this.GetExpressionParentID(expressionId);
            if (expressionParentID.HasValue)
            {
                if (sourceParent != null)
                {
                    if (valueToCompare.HasValue)
                    {
                        this.CreateMatrixItemExpression(expressionParentID.Value, sourceParent, source, condition, valueToCompare);
                    }
                    else
                    {
                        this.CreateMatrixItemExpression(expressionParentID.Value, sourceParent, source, condition, answerText);
                    }
                }
                else if (valueToCompare.HasValue)
                {
                    this.CreateItemExpression(expressionParentID.Value, source, condition, valueToCompare, answerText);
                }
                else
                {
                    this.CreateItemExpression(expressionParentID.Value, source, condition, answerText);
                }
                this.DeleteExpression(expressionId);
            }
        }

        public void ReplaceExpressionWithProfileExpression(int expressionId, string profileKey, LogicalOperator condition, string valueToCompare)
        {
            int? expressionParentID = this.GetExpressionParentID(expressionId);
            if (expressionParentID.HasValue)
            {
                this.CreateProfileExpression(expressionParentID.Value, profileKey, condition, valueToCompare);
                this.DeleteExpression(expressionId);
            }
        }

        public void ReplaceExpressionWithResponseExpression(int expressionId, string responseKey, LogicalOperator condition, string valueToCompare)
        {
            int? expressionParentID = this.GetExpressionParentID(expressionId);
            if (expressionParentID.HasValue)
            {
                this.CreateResponseExpression(expressionParentID.Value, responseKey, condition, valueToCompare);
                this.DeleteExpression(expressionId);
            }
        }

        public void Save()
        {
            try
            {
                using (IDbConnection connection = DatabaseFactory.CreateDatabase().GetConnection())
                {
                    connection.Open();
                    IDbTransaction t = connection.BeginTransaction();
                    try
                    {
                        this.Save(t);
                        t.Commit();
                        if (this.Template != null)
                        {
                            ResponseTemplateManager.MarkTemplateUpdated(this.Template.ID.Value);
                        }
                    }
                    catch (Exception exception)
                    {
                        ExceptionPolicy.HandleException(exception, "BusinessProtected");
                        t.Rollback();
                        throw new Exception("Unable to save data.");
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            catch (Exception exception2)
            {
                if (ExceptionPolicy.HandleException(exception2, "BusinessPublic"))
                {
                    throw;
                }
            }
        }

        public void Save(IDbTransaction t)
        {
            Database database = DatabaseFactory.CreateDatabase();
            if (this.ItemOperandTable != null)
            {
                database.UpdateDataSet(this._templateData, this.ItemOperandTable.TableName, null, null, DeleteItemOperandCommand, t, DataViewRowState.Deleted);
            }
            if (this.ValueOperandTable != null)
            {
                database.UpdateDataSet(this._templateData, this.ValueOperandTable.TableName, null, null, DeleteValueOperandCommand, t, DataViewRowState.Deleted);
            }
            if (this.ProfileOperandTable != null)
            {
                database.UpdateDataSet(this._templateData, this.ProfileOperandTable.TableName, null, null, DeleteProfileOperandCommand, t, DataViewRowState.Deleted);
            }
            if (this.ResponseOperandTable != null)
            {
                database.UpdateDataSet(this._templateData, this.ResponseOperandTable.TableName, null, null, DeleteResponseOperandCommand, t, DataViewRowState.Deleted);
            }
            if (this.RuleActionsTable != null)
            {
                database.UpdateDataSet(this._templateData, this.RuleActionsTable.TableName, null, null, DeleteActionFromRuleCommand, t, DataViewRowState.Deleted);
            }
            if (this.BranchActionTable != null)
            {
                database.UpdateDataSet(this._templateData, this.BranchActionTable.TableName, null, null, DeleteBranchActionCommand, t, DataViewRowState.Deleted);
            }
            if (this.ActionTable != null)
            {
                database.UpdateDataSet(this._templateData, this.ActionTable.TableName, null, null, DeleteActionCommand, t, DataViewRowState.Deleted);
            }
            if (this.PageRulesTable != null)
            {
                database.UpdateDataSet(this._templateData, this.PageRulesTable.TableName, null, null, DeleteRuleFromPageCommand, t, DataViewRowState.Deleted);
            }
            if (this.ItemRulesTable != null)
            {
                database.UpdateDataSet(this._templateData, this.ItemRulesTable.TableName, null, null, DeleteRuleFromItemCommand, t, DataViewRowState.Deleted);
            }
            if (this.RuleTable != null)
            {
                database.UpdateDataSet(this._templateData, this.RuleTable.TableName, null, null, DeleteRuleCommand, t, DataViewRowState.Deleted);
            }
            if (this.ExpressionTable != null)
            {
                foreach (DataRow row in this.ExpressionTable.Select("ExpressionID > 0", "ExpressionID DESC", DataViewRowState.Deleted))
                {
                    DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Rules_DeleteExpression");
                    storedProcCommandWrapper.AddInParameter("ExpressionID", DbType.Int32, row["ExpressionID", DataRowVersion.Original]);
                    database.ExecuteNonQuery(storedProcCommandWrapper, t);
                }
            }
            if (this.OperandTable != null)
            {
                database.UpdateDataSet(this._templateData, this.OperandTable.TableName, null, null, DeleteOperandCommand, t, DataViewRowState.Deleted);
            }
            if (this.OperandTable != null)
            {
                database.UpdateDataSet(this._templateData, this.OperandTable.TableName, InsertOperandCommand, null, null, t, DataViewRowState.Added);
            }
            if (this.ItemOperandTable != null)
            {
                database.UpdateDataSet(this._templateData, this.ItemOperandTable.TableName, InsertItemOperandCommand, UpdateItemOperandCommand, null, t, DataViewRowState.ModifiedCurrent | DataViewRowState.Added);
            }
            if (this.ValueOperandTable != null)
            {
                database.UpdateDataSet(this._templateData, this.ValueOperandTable.TableName, InsertValueOperandCommand, UpdateValueOperandCommand, null, t, DataViewRowState.ModifiedCurrent | DataViewRowState.Added);
            }
            if (this.ProfileOperandTable != null)
            {
                database.UpdateDataSet(this._templateData, this.ProfileOperandTable.TableName, InsertProfileOperandCommand, null, null, t, DataViewRowState.Added);
            }
            if (this.ResponseOperandTable != null)
            {
                database.UpdateDataSet(this._templateData, this.ResponseOperandTable.TableName, InsertResponseOperandCommand, null, null, t, DataViewRowState.Added);
            }
            if (this.ExpressionTable != null)
            {
                foreach (DataRow row2 in this.ExpressionTable.Select("ExpressionID < 0", "ExpressionID DESC", DataViewRowState.CurrentRows))
                {
                    DBCommandWrapper command = database.GetStoredProcCommandWrapper("ckbx_Rules_CreateExpression");
                    command.AddInParameter("Operator", DbType.Int32, row2["Operator"]);
                    command.AddInParameter("Left", DbType.Int32, row2["LeftOperand"]);
                    command.AddInParameter("Right", DbType.Int32, row2["RightOperand"]);
                    command.AddInParameter("Parent", DbType.Int32, row2["Parent"]);
                    command.AddInParameter("Depth", DbType.Int32, row2["Depth"]);
                    command.AddInParameter("Lineage", DbType.String, row2["Lineage"]);
                    command.AddInParameter("Root", DbType.Int32, row2["Root"]);
                    command.AddInParameter("LogicalConnector", DbType.String, row2["ChildRelation"]);
                    command.AddOutParameter("ExpressionID", DbType.Int32, 4);
                    database.ExecuteNonQuery(command, t);
                    row2["ExpressionID"] = (int) command.GetParameterValue("ExpressionID");
                }
                foreach (DataRow row3 in this.ExpressionTable.Select("ExpressionID > 0", "ExpressionID ASC", DataViewRowState.ModifiedCurrent))
                {
                    DBCommandWrapper wrapper3 = database.GetStoredProcCommandWrapper("ckbx_Rules_UpdateComposite");
                    wrapper3.AddInParameter("ExpressionID", DbType.Int32, row3["ExpressionID"]);
                    wrapper3.AddInParameter("Operator", DbType.Int32, row3["Operator"]);
                    wrapper3.AddInParameter("Left", DbType.Int32, row3["LeftOperand"]);
                    wrapper3.AddInParameter("Right", DbType.Int32, row3["RightOperand"]);
                    wrapper3.AddInParameter("Parent", DbType.Int32, row3["Parent"]);
                    wrapper3.AddInParameter("Depth", DbType.Int32, row3["Depth"]);
                    wrapper3.AddInParameter("Lineage", DbType.String, row3["Lineage"]);
                    wrapper3.AddInParameter("Root", DbType.Int32, row3["Root"]);
                    wrapper3.AddInParameter("LogicalConnector", DbType.String, row3["ChildRelation"]);
                    database.ExecuteNonQuery(wrapper3, t);
                }
            }
            if (this.ActionTable != null)
            {
                database.UpdateDataSet(this._templateData, this.ActionTable.TableName, InsertActionCommand, null, null, t, DataViewRowState.Added);
            }
            if (this.BranchActionTable != null)
            {
                database.UpdateDataSet(this._templateData, this.BranchActionTable.TableName, null, UpdateBranchActionCommand, null, t, DataViewRowState.ModifiedCurrent);
            }
            if (this.BranchActionTable != null)
            {
                database.UpdateDataSet(this._templateData, this.BranchActionTable.TableName, InsertBranchActionCommand, null, null, t, DataViewRowState.Added);
            }
            if (this.RuleTable != null)
            {
                database.UpdateDataSet(this._templateData, this.RuleTable.TableName, InsertRuleCommand, null, null, t, DataViewRowState.Added);
            }
            if (this.RuleActionsTable != null)
            {
                database.UpdateDataSet(this._templateData, this.RuleActionsTable.TableName, AddActionToRuleCommand, null, null, t, DataViewRowState.Added);
            }
            if (this.PageRulesTable != null)
            {
                database.UpdateDataSet(this._templateData, this.PageRulesTable.TableName, AddRuleToPageCommand, null, null, t, DataViewRowState.Added);
            }
            if (this.ItemRulesTable != null)
            {
                database.UpdateDataSet(this._templateData, this.ItemRulesTable.TableName, AddRuleToItemCommand, null, null, t, DataViewRowState.Added);
            }
            this.AcceptChanges();
        }

        public DataRow[] SelectChildExpressions(int expressionID)
        {
            return this.ExpressionTable.Select("[Parent]=" + expressionID);
        }

        public void SetBranchActionTargetPage(int actionID, int targetPageID)
        {
            this.BranchActionTable.Select("ActionID=" + actionID)[0]["GoToPageID"] = targetPageID;
        }

        private DataTable ActionTable
        {
            get
            {
                return this._template.ActionTable;
            }
        }

        private static DBCommandWrapper AddActionToRuleCommand
        {
            get
            {
                DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_Rules_AddAction");
                storedProcCommandWrapper.AddInParameter("RuleID", DbType.Int32, "RuleID", DataRowVersion.Current);
                storedProcCommandWrapper.AddInParameter("ActionID", DbType.Int32, "ActionID", DataRowVersion.Current);
                return storedProcCommandWrapper;
            }
        }

        private static DBCommandWrapper AddRuleToItemCommand
        {
            get
            {
                DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_ItemData_AddRule");
                storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, "ItemID", DataRowVersion.Current);
                storedProcCommandWrapper.AddInParameter("RuleID", DbType.Int32, "RuleID", DataRowVersion.Current);
                return storedProcCommandWrapper;
            }
        }

        private static DBCommandWrapper AddRuleToPageCommand
        {
            get
            {
                DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_TemplatePage_AddRule");
                storedProcCommandWrapper.AddInParameter("PageID", DbType.Int32, "PageID", DataRowVersion.Current);
                storedProcCommandWrapper.AddInParameter("RuleID", DbType.Int32, "RuleID", DataRowVersion.Current);
                storedProcCommandWrapper.AddInParameter("EventTrigger", DbType.String, "EventTrigger", DataRowVersion.Current);
                return storedProcCommandWrapper;
            }
        }

        private DataTable BranchActionTable
        {
            get
            {
                return this._template.BranchActionTable;
            }
        }

        private static DBCommandWrapper DeleteActionCommand
        {
            get
            {
                DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_Rules_DeleteAction");
                storedProcCommandWrapper.AddInParameter("ActionID", DbType.Int32, "ActionID", DataRowVersion.Current);
                return storedProcCommandWrapper;
            }
        }

        private static DBCommandWrapper DeleteActionFromRuleCommand
        {
            get
            {
                DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_Rules_RemoveAction");
                storedProcCommandWrapper.AddInParameter("RuleID", DbType.Int32, "RuleID", DataRowVersion.Current);
                storedProcCommandWrapper.AddInParameter("ActionID", DbType.Int32, "ActionID", DataRowVersion.Current);
                return storedProcCommandWrapper;
            }
        }

        private static DBCommandWrapper DeleteBranchActionCommand
        {
            get
            {
                DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_BranchAction_Delete");
                storedProcCommandWrapper.AddInParameter("ActionID", DbType.Int32, "ActionID", DataRowVersion.Current);
                return storedProcCommandWrapper;
            }
        }

        private static DBCommandWrapper DeleteItemOperandCommand
        {
            get
            {
                DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_ItemOperand_Delete");
                storedProcCommandWrapper.AddInParameter("OperandID", DbType.Int32, "OperandID", DataRowVersion.Current);
                return storedProcCommandWrapper;
            }
        }

        private static DBCommandWrapper DeleteOperandCommand
        {
            get
            {
                DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_Rules_DeleteOperand");
                storedProcCommandWrapper.AddInParameter("OperandID", DbType.Int32, "OperandID", DataRowVersion.Current);
                return storedProcCommandWrapper;
            }
        }

        private static DBCommandWrapper DeleteProfileOperandCommand
        {
            get
            {
                DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_ProfileOperand_Delete");
                storedProcCommandWrapper.AddInParameter("OperandID", DbType.Int32, "OperandID", DataRowVersion.Current);
                return storedProcCommandWrapper;
            }
        }

        private static DBCommandWrapper DeleteResponseOperandCommand
        {
            get
            {
                DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_ResponseOperand_Delete");
                storedProcCommandWrapper.AddInParameter("OperandID", DbType.Int32, "OperandID", DataRowVersion.Current);
                return storedProcCommandWrapper;
            }
        }

        private static DBCommandWrapper DeleteRuleCommand
        {
            get
            {
                DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_Rules_Delete");
                storedProcCommandWrapper.AddInParameter("RuleID", DbType.Int32, "RuleID", DataRowVersion.Current);
                return storedProcCommandWrapper;
            }
        }

        private static DBCommandWrapper DeleteRuleFromItemCommand
        {
            get
            {
                DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_ItemData_RemoveRule");
                storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, "ItemID", DataRowVersion.Current);
                return storedProcCommandWrapper;
            }
        }

        private static DBCommandWrapper DeleteRuleFromPageCommand
        {
            get
            {
                DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_TemplatePage_DeleteRule");
                storedProcCommandWrapper.AddInParameter("PageID", DbType.Int32, "PageID", DataRowVersion.Current);
                storedProcCommandWrapper.AddInParameter("RuleID", DbType.Int32, "RuleID", DataRowVersion.Current);
                return storedProcCommandWrapper;
            }
        }

        private static DBCommandWrapper DeleteValueOperandCommand
        {
            get
            {
                DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_ValueOperand_Delete");
                storedProcCommandWrapper.AddInParameter("OperandID", DbType.Int32, "OperandID", DataRowVersion.Current);
                return storedProcCommandWrapper;
            }
        }

        private DataTable ExpressionTable
        {
            get
            {
                return this._template.ExpressionTable;
            }
        }

        private static DBCommandWrapper InsertActionCommand
        {
            get
            {
                DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_Rules_CreateAction");
                storedProcCommandWrapper.AddInParameter("ActionTypeName", DbType.String, "ActionTypeName", DataRowVersion.Current);
                storedProcCommandWrapper.AddInParameter("ActionTypeAssembly", DbType.String, "ActionAssembly", DataRowVersion.Current);
                storedProcCommandWrapper.AddParameter("ActionID", DbType.Int32, ParameterDirection.Output, "ActionID", DataRowVersion.Current, null);
                return storedProcCommandWrapper;
            }
        }

        private static DBCommandWrapper InsertBranchActionCommand
        {
            get
            {
                DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_BranchAction_Create");
                storedProcCommandWrapper.AddInParameter("ActionID", DbType.String, "ActionID", DataRowVersion.Current);
                storedProcCommandWrapper.AddInParameter("GoToPageID", DbType.String, "GoToPageID", DataRowVersion.Current);
                return storedProcCommandWrapper;
            }
        }

        private static DBCommandWrapper InsertItemOperandCommand
        {
            get
            {
                DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_ItemOperand_Create");
                storedProcCommandWrapper.AddInParameter("OperandID", DbType.Int32, "OperandID", DataRowVersion.Current);
                storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, "ItemID", DataRowVersion.Current);
                storedProcCommandWrapper.AddInParameter("ParentItemID", DbType.Int32, "ParentItemID", DataRowVersion.Current);
                return storedProcCommandWrapper;
            }
        }

        private static DBCommandWrapper InsertOperandCommand
        {
            get
            {
                DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_Rules_InsertOperand");
                storedProcCommandWrapper.AddInParameter("TypeName", DbType.String, "TypeName", DataRowVersion.Current);
                storedProcCommandWrapper.AddInParameter("TypeAssembly", DbType.String, "TypeAssembly", DataRowVersion.Current);
                storedProcCommandWrapper.AddParameter("OperandID", DbType.Int32, ParameterDirection.Output, "OperandID", DataRowVersion.Current, null);
                return storedProcCommandWrapper;
            }
        }

        private static DBCommandWrapper InsertProfileOperandCommand
        {
            get
            {
                DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_ProfileOperand_Create");
                storedProcCommandWrapper.AddInParameter("OperandID", DbType.Int32, "OperandID", DataRowVersion.Current);
                storedProcCommandWrapper.AddInParameter("ProfileKey", DbType.String, "ProfileKey", DataRowVersion.Current);
                return storedProcCommandWrapper;
            }
        }

        private static DBCommandWrapper InsertResponseOperandCommand
        {
            get
            {
                DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_ResponseOperand_Create");
                storedProcCommandWrapper.AddInParameter("OperandID", DbType.Int32, "OperandID", DataRowVersion.Current);
                storedProcCommandWrapper.AddInParameter("ResponseKey", DbType.String, "ResponseKey", DataRowVersion.Current);
                return storedProcCommandWrapper;
            }
        }

        private static DBCommandWrapper InsertRuleCommand
        {
            get
            {
                DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_Rules_Create");
                storedProcCommandWrapper.AddInParameter("ExpressionID", DbType.Int32, "ExpressionID", DataRowVersion.Current);
                storedProcCommandWrapper.AddParameter("RuleID", DbType.Int32, ParameterDirection.Output, "RuleID", DataRowVersion.Current, null);
                return storedProcCommandWrapper;
            }
        }

        private static DBCommandWrapper InsertValueOperandCommand
        {
            get
            {
                DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_ValueOperand_Create");
                storedProcCommandWrapper.AddInParameter("OperandID", DbType.Int32, "OperandID", DataRowVersion.Current);
                storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, "ItemID", DataRowVersion.Current);
                storedProcCommandWrapper.AddInParameter("OptionID", DbType.Int32, "OptionID", DataRowVersion.Current);
                storedProcCommandWrapper.AddInParameter("AnswerValue", DbType.String, "AnswerValue", DataRowVersion.Current);
                return storedProcCommandWrapper;
            }
        }

        private DataTable ItemOperandTable
        {
            get
            {
                return this._template.ItemOperandTable;
            }
        }

        private DataTable ItemRulesTable
        {
            get
            {
                return this._template.ItemRulesTable;
            }
        }

        private DataTable OperandTable
        {
            get
            {
                return this._template.OperandTable;
            }
        }

        private DataTable PageDataTable
        {
            get
            {
                return this._template.PageTable;
            }
        }

        private DataTable PageRulesTable
        {
            get
            {
                return this._template.PageRulesTable;
            }
        }

        private DataTable ProfileOperandTable
        {
            get
            {
                return this._template.ProfileOperandTable;
            }
        }

        private DataTable ResponseOperandTable
        {
            get
            {
                return this._template.ResponseOperandTable;
            }
        }

        private DataTable RuleActionsTable
        {
            get
            {
                return this._template.RuleActionsTable;
            }
        }

        private DataTable RuleTable
        {
            get
            {
                return this._template.RuleTable;
            }
        }

        public ResponseTemplate Template
        {
            get
            {
                return this._template;
            }
        }

        private static DBCommandWrapper UpdateBranchActionCommand
        {
            get
            {
                DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_BranchAction_Update");
                storedProcCommandWrapper.AddInParameter("ActionID", DbType.String, "ActionID", DataRowVersion.Current);
                storedProcCommandWrapper.AddInParameter("GoToPageID", DbType.String, "GoToPageID", DataRowVersion.Current);
                return storedProcCommandWrapper;
            }
        }

        private static DBCommandWrapper UpdateItemOperandCommand
        {
            get
            {
                DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_ItemOperand_Update");
                storedProcCommandWrapper.AddInParameter("OperandID", DbType.Int32, "OperandID", DataRowVersion.Current);
                storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, "ItemID", DataRowVersion.Current);
                storedProcCommandWrapper.AddInParameter("ParentItemID", DbType.Int32, "ParentItemID", DataRowVersion.Current);
                return storedProcCommandWrapper;
            }
        }

        private static DBCommandWrapper UpdateValueOperandCommand
        {
            get
            {
                DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_ValueOperand_Update");
                storedProcCommandWrapper.AddInParameter("OperandID", DbType.Int32, "OperandID", DataRowVersion.Current);
                storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, "ItemID", DataRowVersion.Current);
                storedProcCommandWrapper.AddInParameter("OptionID", DbType.Int32, "OptionID", DataRowVersion.Current);
                storedProcCommandWrapper.AddInParameter("AnswerValue", DbType.String, "AnswerValue", DataRowVersion.Current);
                return storedProcCommandWrapper;
            }
        }

        private DataTable ValueOperandTable
        {
            get
            {
                return this._template.ValueOperandTable;
            }
        }
    }
}

