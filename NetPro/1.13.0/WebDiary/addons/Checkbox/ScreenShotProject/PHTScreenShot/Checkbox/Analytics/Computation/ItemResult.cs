﻿namespace Checkbox.Analytics.Computation
{
    using System;
    using System.Runtime.CompilerServices;

    public class ItemResult
    {
        public ItemResult()
        {
        }

        public ItemResult(int count, decimal percentage)
        {
            this.Count = count;
            this.Percentage = percentage;
        }

        public int Count { get; set; }

        public decimal Percentage { get; set; }
    }
}

