﻿namespace Checkbox.Forms
{
    using Checkbox.Forms.Items.Configuration;
    using Checkbox.Forms.Security;
    using Checkbox.Globalization.Text;
    using Prezza.Framework.Common;
    using Prezza.Framework.Data;
    using Prezza.Framework.Security;
    using System;
    using System.Data;

    [Serializable]
    public class LibraryTemplate : Template
    {
        public LibraryTemplate() : base(new string[] { "Library.Edit", "Library.View" }, new string[] { "Library.Create", "Library.Delete", "Library.Edit", "Library.View" })
        {
        }

        public void AddItem(ItemData data)
        {
            if (this.TemplatePages.Count == 0)
            {
                this.NewPage();
            }
            this.AddItemToPage(this.TemplatePages[0], data);
        }

        protected override void Create(IDbTransaction t)
        {
            base.Create(t);
            if (base.ID <= 0)
            {
                throw new Exception("Unable to save library template data.  DataID <= 0.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Library_Insert");
            storedProcCommandWrapper.AddInParameter("LibraryTemplateID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("NameTextID", DbType.String, this.NameTextID);
            storedProcCommandWrapper.AddInParameter("DescriptionTextID", DbType.String, this.DescriptionTextID);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        public override Policy CreatePolicy(string[] permissions)
        {
            return new LibraryPolicy(permissions);
        }

        public override DataSet GetConfigurationDataSet()
        {
            string[] tableNames = new string[] { this.TemplateDataTableName, this.TemplateItemsTableName, this.PageDataTableName, this.PageItemsTableName, this.ItemAppearanceMapTableName, this.ItemAppearanceTableName };
            DataSet dataSet = new DataSet();
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Library_Get");
            storedProcCommandWrapper.AddInParameter("LibraryTemplateID", DbType.Int32, base.ID);
            database.LoadDataSet(storedProcCommandWrapper, dataSet, tableNames);
            if (((dataSet.Tables.Count == 0) || !dataSet.Tables.Contains(this.DataTableName)) || (dataSet.Tables[ResponseTemplate.DataTableName].Rows.Count <= 0))
            {
                throw new TemplateDoesNotExist(base.ID.Value);
            }
            this.SetConfigurationDataSetColumnMappings(dataSet);
            base.MergeTextData(dataSet, TextManager.GetTextData(this.NameTextID), "name");
            base.MergeTextData(dataSet, TextManager.GetTextData(this.DescriptionTextID), "description");
            if (dataSet.Tables.Contains(this.TemplateTextTableName) && !dataSet.Tables[this.TemplateTextTableName].Columns.Contains("ComputedTextID"))
            {
                dataSet.Tables[this.TemplateTextTableName].Columns.Add("ComputedTextID", typeof(string));
                dataSet.Tables[this.TemplateTextTableName].Columns["ComputedTextID"].Expression = "Iif(TemplateID IS NULL OR TextIDSuffix IS NULL, '', '/" + this.TextIDPrefix + "/' + TemplateID + '/' + TextIDSuffix)";
            }
            return dataSet;
        }

        public override SecurityEditor GetEditor()
        {
            return new LibrarySecurityEditor(this);
        }

        internal void InitializeAccess(Policy defaultPolicy, AccessControlList acl)
        {
            if (base.ID.HasValue && (base.ID > 0))
            {
                throw new Exception("Access can only be initialized for a new library template.");
            }
            ArgumentValidation.CheckExpectedType(defaultPolicy, typeof(LibraryPolicy));
            base.SetAccess(defaultPolicy, acl);
        }

        public override void Load(DataSet ds)
        {
            base.Load(ds);
            if (((ds.Tables.Count > 0) && ds.Tables.Contains(this.TemplateDataTableName)) && (ds.Tables[this.TemplateDataTableName].Rows.Count > 0))
            {
                DataRow tableRow = ds.Tables[this.TemplateDataTableName].Rows[0];
                base.AclID = DbUtility.GetValueFromDataRow<int?>(tableRow, "AclID", null);
                base.DefaultPolicyID = DbUtility.GetValueFromDataRow<int?>(tableRow, "DefaultPolicy", null);
                base.LastModified = DbUtility.GetValueFromDataRow<DateTime?>(tableRow, "ModifiedDate", null);
            }
        }

        public void RemoveItem(ItemData data)
        {
            this.RemoveItemFromPage(this.TemplatePages[0], data);
        }

        public string DescriptionTextID
        {
            get
            {
                if (base.ID > 0)
                {
                    return ("/library/" + base.ID + "/description");
                }
                return string.Empty;
            }
        }

        public override string IdentityColumnName
        {
            get
            {
                return "LibraryTemplateID";
            }
        }

        public string NameTextID
        {
            get
            {
                if (base.ID > 0)
                {
                    return ("/library/" + base.ID + "/name");
                }
                return string.Empty;
            }
        }
    }
}

