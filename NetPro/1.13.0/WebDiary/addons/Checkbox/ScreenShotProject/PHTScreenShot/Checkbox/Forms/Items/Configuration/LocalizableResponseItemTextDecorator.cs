﻿namespace Checkbox.Forms.Items.Configuration
{
    using Checkbox.Globalization.Text;
    using System;
    using System.Data;

    [Serializable]
    public class LocalizableResponseItemTextDecorator : ItemTextDecorator
    {
        public LocalizableResponseItemTextDecorator(ItemData data, string languageCode) : base(data, languageCode)
        {
        }

        public override void ImportText(DataSet importData, int oldItemID)
        {
            base.ImportText(importData, oldItemID);
            string textTableName = ((LocalizableResponseItemData) base.Data).TextTableName;
            if ((importData != null) && importData.Tables.Contains(textTableName))
            {
                string filterExpression = base.Data.IdentityColumnName + " = " + oldItemID;
                foreach (DataRow row in importData.Tables[textTableName].Select(filterExpression, null, DataViewRowState.CurrentRows))
                {
                    if ((((row["TextIDPrefix"] != DBNull.Value) && (row["TextIDSuffix"] != DBNull.Value)) && ((row["LanguageCode"] != DBNull.Value) && (row["LanguageCode"] != DBNull.Value))) && (row["TextValue"] != DBNull.Value))
                    {
                        string str3 = (string) row["TextIDPrefix"];
                        string str4 = (string) row["TextIDSuffix"];
                        string languageCode = (string) row["LanguageCode"];
                        string textValue = (string) row["TextValue"];
                        TextManager.SetText(string.Concat(new object[] { "/", str3, "/", base.Data.ID, "/", str4 }), languageCode, textValue);
                    }
                }
            }
        }
    }
}

