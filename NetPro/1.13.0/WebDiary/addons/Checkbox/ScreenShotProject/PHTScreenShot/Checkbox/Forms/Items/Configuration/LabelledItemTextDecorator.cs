﻿namespace Checkbox.Forms.Items.Configuration
{
    using Prezza.Framework.Common;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LabelledItemTextDecorator : LocalizableResponseItemTextDecorator
    {
        private string _subText;
        private string _text;

        public LabelledItemTextDecorator(LabelledItemData itemData, string language) : base(itemData, language)
        {
            this._text = string.Empty;
            this._subText = string.Empty;
            this.TextModified = false;
            this.SubTextModified = false;
        }

        protected override void CopyLocalizedText(ItemData data)
        {
            base.CopyLocalizedText(data);
            ArgumentValidation.CheckExpectedType(data, typeof(LabelledItemData));
            if (this.Data.TextID != string.Empty)
            {
                Dictionary<string, string> allTexts = this.GetAllTexts(((LabelledItemData) data).TextID);
                if (allTexts != null)
                {
                    foreach (string str in allTexts.Keys)
                    {
                        this.SetText(this.Data.TextID, allTexts[str], str);
                    }
                }
            }
            if (this.Data.SubTextID != string.Empty)
            {
                Dictionary<string, string> dictionary2 = this.GetAllTexts(((LabelledItemData) data).SubTextID);
                if (dictionary2 != null)
                {
                    foreach (string str2 in dictionary2.Keys)
                    {
                        this.SetText(this.Data.SubTextID, dictionary2[str2], str2);
                    }
                }
            }
        }

        protected virtual void EnsureTextsLoaded()
        {
        }

        protected override void SetLocalizedTexts()
        {
            if (this.Data.TextID != string.Empty)
            {
                this.SetText(this.Data.TextID, this.Text);
            }
            if (this.Data.SubTextID != string.Empty)
            {
                this.SetText(this.Data.SubTextID, this.SubText);
            }
        }

        public LabelledItemData Data
        {
            get
            {
                return (LabelledItemData) base.Data;
            }
        }

        public string SubText
        {
            get
            {
                this.EnsureTextsLoaded();
                if ((this.Data.SubTextID != string.Empty) && !this.SubTextModified)
                {
                    return this.GetText(this.Data.SubTextID);
                }
                return this._subText;
            }
            set
            {
                this._subText = value;
                this.SubTextModified = true;
            }
        }

        protected bool SubTextModified { get; private set; }

        public string Text
        {
            get
            {
                this.EnsureTextsLoaded();
                if ((this.Data.TextID != string.Empty) && !this.TextModified)
                {
                    return this.GetText(this.Data.TextID);
                }
                return this._text;
            }
            set
            {
                this._text = value;
                this.TextModified = true;
            }
        }

        protected bool TextModified { get; private set; }
    }
}

