﻿namespace Checkbox.Users
{
    using Checkbox.Management;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    internal class GroupCache
    {
        private List<Group> _groupList;
        private List<int> _membershipList;

        public void AddGroup(Group groupToAdd)
        {
            if (groupToAdd != null)
            {
                int num = this.GroupList.FindIndex(group => group.ID == groupToAdd.ID);
                if (num >= 0)
                {
                    this.GroupList[num] = groupToAdd;
                }
                while ((this.GroupList.Count >= ApplicationManager.AppSettings.GroupCacheSize) && (this.GroupList.Count > 0))
                {
                    this.GroupList.RemoveAt(0);
                }
                this.GroupList.Add(groupToAdd);
            }
        }

        public Group GetGroup(int groupId)
        {
            return this.GroupList.Find(delegate (Group group) {
                int? iD = group.ID;
                int num1 = groupId;
                return (iD.GetValueOrDefault() == num1) && iD.HasValue;
            });
        }

        public void RemoveGroup(int groupId)
        {
            int index = this.GroupList.FindIndex(delegate (Group group) {
                int? iD = group.ID;
                int num1 = groupId;
                return (iD.GetValueOrDefault() == num1) && iD.HasValue;
            });
            if (index >= 0)
            {
                this.GroupList.RemoveAt(index);
            }
        }

        protected List<Group> GroupList
        {
            get
            {
                if (this._groupList == null)
                {
                    this._groupList = new List<Group>();
                }
                return this._groupList;
            }
        }

        public List<int> GroupMemberships
        {
            get
            {
                if (this._membershipList == null)
                {
                    this._membershipList = new List<int>();
                }
                return this._membershipList;
            }
            set
            {
                this._membershipList = value;
            }
        }

        public string IdentityName { get; set; }
    }
}

