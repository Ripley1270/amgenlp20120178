﻿namespace Checkbox.Configuration
{
    using Prezza.Framework.Common;
    using System;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Xml;

    public static class ConfigurationWriter
    {
        public static bool UpdateConfigFiles(string applicationFolder, string applicationConfigFolder, string connectionString, string dbProvider, out string status)
        {
            string str = string.Empty;
            try
            {
                if (((applicationFolder == null) || (applicationConfigFolder == null)) || ((applicationFolder == string.Empty) || (applicationConfigFolder == string.Empty)))
                {
                    status = "Application folder and application config folder must be specified.";
                    return false;
                }
                if (!Directory.Exists(applicationFolder))
                {
                    status = "Specified application folder [" + applicationFolder + "] does not exist.";
                    return false;
                }
                if (!Directory.Exists(applicationConfigFolder))
                {
                    status = "Specified application configuration folder [" + applicationConfigFolder + "] does not exist.";
                    return false;
                }
                if (!UpdateDatabaseConfiguration(applicationConfigFolder, dbProvider, out status))
                {
                    return false;
                }
                foreach (string str2 in Directory.GetFiles(applicationConfigFolder, "*.xml"))
                {
                    XmlDocument document = new XmlDocument();
                    document.Load(str2);
                    foreach (XmlNode node in document.SelectNodes("//*[@filePath]"))
                    {
                        if (!UpdateFilePath(node, applicationConfigFolder, out status))
                        {
                            return false;
                        }
                    }
                    document.Save(str2);
                }
                if (!UpdateWebConfig(applicationFolder, applicationConfigFolder, out status))
                {
                    return false;
                }
                return true;
            }
            catch (Exception exception)
            {
                if (str != string.Empty)
                {
                    status = "An exception occurred while updating " + str + ".<br />  The exception was [" + exception.Message + "].";
                }
                else
                {
                    status = "An exception occurred while updating file paths.  The exception was [" + exception.Message + "].";
                }
                return false;
            }
        }

        private static bool UpdateDatabaseConfiguration(string applicationConfigFolder, string dbProvider, out string status)
        {
            status = string.Empty;
            applicationConfigFolder = applicationConfigFolder + Path.DirectorySeparatorChar.ToString();
            if (!File.Exists(applicationConfigFolder + "DatabaseConfiguration.xml"))
            {
                status = "Unable to find DatabaseConfiguration.xml in specified configuration directory [" + applicationConfigFolder + "].";
                return false;
            }
            XmlDocument document = new XmlDocument();
            document.Load(applicationConfigFolder + "DatabaseConfiguration.xml");
            foreach (XmlNode node in document.SelectNodes("/databaseSettings/instances/instance"))
            {
                node.Attributes["type"].InnerText = dbProvider;
            }
            document.Save(applicationConfigFolder + "DatabaseConfiguration.xml");
            return true;
        }

        private static bool UpdateFilePath(XmlNode configNode, string applicationConfigFolder, out string status)
        {
            status = string.Empty;
            XmlAttribute attribute = configNode.Attributes["filePath"];
            string str = attribute.Value.Replace('/', Path.DirectorySeparatorChar);
            if (str.LastIndexOf(Path.DirectorySeparatorChar) < (str.Length - 1))
            {
                attribute.Value = str.Substring(str.LastIndexOf(Path.DirectorySeparatorChar) + 1);
            }
            return true;
        }

        private static bool UpdateWebConfig(string applicationFolder, string applicationConfigFolder, out string status)
        {
            XmlNamespaceManager manager;
            status = string.Empty;
            if (!File.Exists(applicationFolder + "/web.config"))
            {
                status = "Unable to find web.config in specified application folder [" + applicationFolder + "].";
                return false;
            }
            XmlDocument document = new XmlDocument();
            try
            {
                document.Load(applicationFolder + "/web.config");
                manager = new XmlNamespaceManager(document.NameTable);
                manager.AddNamespace("ms", "http://schemas.microsoft.com/.NetConfiguration/v2.0");
            }
            catch (Exception exception)
            {
                status = "Unable to load [" + applicationFolder + "/web.config].  Error was [" + exception.Message + "].";
                return false;
            }
            foreach (XmlNode node in document.SelectNodes("/ms:configuration/ms:configSections/ms:section", manager))
            {
                string attributeText = XmlUtility.GetAttributeText(node, "name");
                if (attributeText != string.Empty)
                {
                    XmlNode configNode = document.SelectSingleNode("/ms:configuration/ms:" + attributeText, manager);
                    if ((configNode != null) && !UpdateFilePath(configNode, applicationConfigFolder, out status))
                    {
                        return false;
                    }
                }
            }
            document.Save(applicationFolder + "/web.config");
            return true;
        }
    }
}

