﻿namespace Checkbox.Analytics
{
    using Checkbox.Analytics.Data;
    using Prezza.Framework.Data;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Runtime.InteropServices;

    public static class ResponseManager
    {
        public static void DeleteAllResponses(int responseTemplateID)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Response_DeleteForRT");
            storedProcCommandWrapper.AddInParameter("ResponseTemplateID", DbType.Int32, responseTemplateID);
            database.ExecuteNonQuery(storedProcCommandWrapper);
        }

        public static void DeleteAllTestResponses(int responseTemplateID)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_RT_DeleteTestResponses");
            storedProcCommandWrapper.AddInParameter("ResponseTemplateID", DbType.Int32, responseTemplateID);
            database.ExecuteNonQuery(storedProcCommandWrapper);
        }

        public static void DeleteResponses(List<long> responseIDs)
        {
            Database database = DatabaseFactory.CreateDatabase();
            foreach (int num in responseIDs)
            {
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Response_Delete");
                storedProcCommandWrapper.AddInParameter("ResponseID", DbType.Int64, num);
                database.ExecuteNonQuery(storedProcCommandWrapper);
            }
        }

        public static void DeleteUserResponses(string uniqueIdentifier)
        {
        }

        //public static void GetMinMaxResponseDates(int? responseTemplateId, int? invitationId, out DateTime? minDate, out DateTime? maxDate)
        //{
        //    Database database = DatabaseFactory.CreateDatabase();
        //    DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ResponseTemplate_GetMinMaxResponseDate");
        //    storedProcCommandWrapper.AddInParameter("ResponseTemplateId", DbType.Int32, responseTemplateId);
        //    storedProcCommandWrapper.AddInParameter("InvitationId", DbType.Int32, invitationId);
        //    //minDate = 0;
        //    //maxDate = 0;
        //    minDate = null;
        //    maxDate = null; 
        //    using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
        //    {
        //        if (reader.Read())
        //        {
        //            minDate = DbUtility.GetValueFromDataReader<DateTime?>(reader, "MinStartDate", null);
        //            DateTime? nullable = DbUtility.GetValueFromDataReader<DateTime?>(reader, "MaxStartDate", null);
        //            DateTime? nullable2 = DbUtility.GetValueFromDataReader<DateTime?>(reader, "MaxEndDate", null);
        //            if (nullable2.HasValue)
        //            {
        //                DateTime? nullable6 = nullable2;
        //                DateTime? nullable7 = nullable;
        //                maxDate = ((nullable6.HasValue & nullable7.HasValue) ? ((DateTime?) (nullable6.GetValueOrDefault() > nullable7.GetValueOrDefault())) : ((DateTime?) false)) ? nullable2 : nullable;
        //            }
        //            else
        //            {
        //                maxDate = nullable;
        //            }
        //        }
        //    }
        //}

        public static void GetMinMaxResponseDates(int? responseTemplateId, int? invitationId, out DateTime? minDate, out DateTime? maxDate)
        {
            bool flag;
            DateTime? nullable;
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ResponseTemplate_GetMinMaxResponseDate");
            storedProcCommandWrapper.AddInParameter("ResponseTemplateId", DbType.Int32, responseTemplateId);
            storedProcCommandWrapper.AddInParameter("InvitationId", DbType.Int32, invitationId);
            minDate = null;
            maxDate = null;
            IDataReader dataReader = database.ExecuteReader(storedProcCommandWrapper);
            using (dataReader)
            {
                if (dataReader.Read())
                {
                    DateTime? nullable1 = null;
                    minDate = DbUtility.GetValueFromDataReader<DateTime?>(dataReader, "MinStartDate", nullable1);
                    DateTime? nullable2 = null;
                    DateTime? valueFromDataReader = DbUtility.GetValueFromDataReader<DateTime?>(dataReader, "MaxStartDate", nullable2);
                    DateTime? nullable3 = null;
                    DateTime? valueFromDataReader1 = DbUtility.GetValueFromDataReader<DateTime?>(dataReader, "MaxEndDate", nullable3);
                    if (!valueFromDataReader1.HasValue)
                    {
                        maxDate = valueFromDataReader;
                    }
                    else
                    {
                        DateTime? nullable4 = valueFromDataReader1;
                        DateTime? nullable5 = valueFromDataReader;
                        flag = (nullable4.HasValue & nullable5.HasValue ? nullable4.GetValueOrDefault() > nullable5.GetValueOrDefault() : false);
                        nullable = (flag ? valueFromDataReader1 : valueFromDataReader);
                        maxDate = nullable;
                    }
                }
            }
        }

        public static int GetResponseCount(int responseTemplateID, bool includeIncomplete, bool includeDeleted, string filterField, string filterValue)
        {
            SelectQuery query = QueryFactory.GetResponsesCountQuery(responseTemplateID, includeIncomplete, includeDeleted, filterField, filterValue);
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
            int num = 0;
            using (IDataReader reader = database.ExecuteReader(sqlStringCommandWrapper))
            {
                try
                {
                    if (reader.Read())
                    {
                        num = Convert.ToInt32(reader["ResponseCount"]);
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return num;
        }

        public static DataRow GetResponseDataRow(Guid responseGuid)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Response_GetDataRowForGuid");
            storedProcCommandWrapper.AddInParameter("ResponseGuid", DbType.Guid, responseGuid);
            DataSet set = database.ExecuteDataSet(storedProcCommandWrapper);
            if (((set != null) && (set.Tables.Count == 1)) && (set.Tables[0].Rows.Count == 1))
            {
                return set.Tables[0].Rows[0];
            }
            return null;
        }

        public static DataRow GetResponseDataRow(long responseId)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Response_GetDataRowForId");
            storedProcCommandWrapper.AddInParameter("ResponseId", DbType.Int64, responseId);
            DataSet set = database.ExecuteDataSet(storedProcCommandWrapper);
            if (((set != null) && (set.Tables.Count == 1)) && (set.Tables[0].Rows.Count == 1))
            {
                return set.Tables[0].Rows[0];
            }
            return null;
        }

        public static DataSet GetResponseHistogram(int? responseTemplateId, int? invitationId, DateTime minDate, DateTime maxDate, string granularity)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ResponseTemplate_GenerateActivityHistogram");
            storedProcCommandWrapper.AddInParameter("ResponseTemplateId", DbType.Int32, responseTemplateId);
            storedProcCommandWrapper.AddInParameter("InvitationId", DbType.Int32, invitationId);
            storedProcCommandWrapper.AddInParameter("StartDate", DbType.DateTime, minDate);
            storedProcCommandWrapper.AddInParameter("EndDate", DbType.DateTime, maxDate);
            storedProcCommandWrapper.AddInParameter("Granularity", DbType.String, granularity);
            return database.ExecuteDataSet(storedProcCommandWrapper);
        }

        public static DataTable GetResponseList(int responseTemplateID, bool includeIncomplete, bool includeDeleted, int? pageNumber, int? resultsPerPage, string filterField, string filterValue, string sortField, bool sortAscending)
        {
            SelectQuery query = QueryFactory.GetResponsesQuery(responseTemplateID, includeIncomplete, includeDeleted, pageNumber, resultsPerPage, filterField, filterValue, sortField, sortAscending);
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
            DataSet set = database.ExecuteDataSet(sqlStringCommandWrapper);
            if ((set != null) && (set.Tables.Count > 0))
            {
                return set.Tables[0];
            }
            return null;
        }

        public static DataTable GetResponseList(int responseTemplateID, bool includeIncomplete, bool includeDeleted, int? pageNumber, int? resultsPerPage, string filterField, string filterValue, string sortField, bool sortAscending, DateTime? startDate, DateTime? endDate)
        {
            SelectQuery query = QueryFactory.GetResponsesQuery(responseTemplateID, includeIncomplete, includeDeleted, pageNumber, resultsPerPage, filterField, filterValue, sortField, sortAscending, startDate, endDate);
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
            DataSet set = database.ExecuteDataSet(sqlStringCommandWrapper);
            if ((set != null) && (set.Tables.Count > 0))
            {
                return set.Tables[0];
            }
            return null;
        }

        public static int? GetSurveyIdFromResponseGuid(Guid responseGuid)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Response_GetSurveyIdFromResponseGuid");
            storedProcCommandWrapper.AddInParameter("ResponseGuid", DbType.Int64, responseGuid);
            storedProcCommandWrapper.AddOutParameter("SurveyId", DbType.Int32, 4);
            database.ExecuteNonQuery(storedProcCommandWrapper);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("SurveyId");
            if ((parameterValue != null) && (parameterValue != DBNull.Value))
            {
                return new int?((int) parameterValue);
            }
            return null;
        }

        public static int? GetSurveyIdFromResponseId(long responseId)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Response_GetSurveyIdFromResponseId");
            storedProcCommandWrapper.AddInParameter("ResponseId", DbType.Int64, responseId);
            storedProcCommandWrapper.AddOutParameter("SurveyId", DbType.Int32, 4);
            database.ExecuteNonQuery(storedProcCommandWrapper);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("SurveyId");
            if ((parameterValue != null) && (parameterValue != DBNull.Value))
            {
                return new int?((int) parameterValue);
            }
            return null;
        }

        public static DataSet ListResponseAnswers(Guid responseGuid)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Response_ListAnswersForGuid");
            storedProcCommandWrapper.AddInParameter("ResponseGuid", DbType.Guid, responseGuid);
            return database.ExecuteDataSet(storedProcCommandWrapper);
        }

        public static DataSet ListResponseAnswers(long responseId)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Response_ListAnswersForId");
            storedProcCommandWrapper.AddInParameter("ResponseId", DbType.Int64, responseId);
            return database.ExecuteDataSet(storedProcCommandWrapper);
        }
    }
}

