﻿namespace Checkbox.Analytics.Filters
{
    using Checkbox.Analytics.Data;
    using Checkbox.Analytics.Filters.Configuration;
    using Checkbox.Common;
    using Checkbox.Forms;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class ResponseFilter : AnswerDataObjectFilter
    {
        public override void Configure(FilterData filterData, string languageCode)
        {
            base.Configure(filterData, languageCode);
            if (filterData is ResponseFilterData)
            {
                this.PropertyName = ((ResponseFilterData) filterData).PropertyName;
            }
        }

        public override bool EvaluateFilter(ItemAnswer answer, ResponseProperties responseProperties, out bool hasValue)
        {
            string stringValue = responseProperties.GetStringValue(this.PropertyName);
            hasValue = Utilities.IsNotNullOrEmpty(stringValue);
            return this.CompareValue(stringValue);
        }

        protected string PropertyName { get; private set; }
    }
}

