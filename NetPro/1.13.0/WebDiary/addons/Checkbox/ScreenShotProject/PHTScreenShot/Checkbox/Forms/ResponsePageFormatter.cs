﻿namespace Checkbox.Forms
{
    using Checkbox.Common;
    using Checkbox.Forms.Items;
    using Checkbox.Globalization.Text;
    using System;
    using System.Collections.Generic;
    using System.Text;

    public static class ResponsePageFormatter
    {
        public static string Format(ResponsePage page, string format)
        {
            if (format.Equals("Html", StringComparison.InvariantCultureIgnoreCase))
            {
                return FormatHtml(page);
            }
            return FormatText(page);
        }

        private static string FormatHtml(ResponsePage page)
        {
            if (!page.Excluded && (page.Items.Count != 0))
            {
                StringBuilder builder = new StringBuilder();
                builder.Append("<div style=\"border:1px solid #B2B2B2;margin-top:25px;padding:5px;\">");
                builder.Append("<div style=\"font-size:14px;font-weight:bold;text-decoration:underline;\">");
                if (page.Position > 0)
                {
                    builder.Append(TextManager.GetText("/controlText/templatePageEditor/page", page.Parent.LanguageCode));
                    builder.Append(" ");
                    builder.Append(page.Position);
                }
                else
                {
                    builder.Append(TextManager.GetText("/pageText/formEditor.aspx/hiddenItems", page.Parent.LanguageCode));
                }
                builder.Append("</div>");
                List<Item> items = page.Items;
                bool flag = false;
                foreach (Item item in items)
                {
                    if (!item.Excluded && (item is IAnswerable))
                    {
                        IItemFormatter itemFormatter = ItemFormatterFactory.GetItemFormatter(item.TypeID, "Html");
                        if (itemFormatter != null)
                        {
                            string str = itemFormatter.Format(item, "Html");
                            if (Utilities.IsNotNullOrEmpty(str))
                            {
                                flag = true;
                                builder.Append("<br /><br />");
                                builder.Append(str);
                            }
                        }
                    }
                }
                builder.Append("</div>");
                if (flag)
                {
                    return builder.ToString();
                }
            }
            return string.Empty;
        }

        private static string FormatText(ResponsePage page)
        {
            if (!page.Excluded && (page.Items.Count != 0))
            {
                StringBuilder builder = new StringBuilder();
                builder.AppendLine();
                builder.AppendLine();
                if (page.Position > 0)
                {
                    builder.Append(TextManager.GetText("/controlText/templatePageEditor/page", page.Parent.LanguageCode));
                    builder.Append(" ");
                    builder.Append(page.Position.ToString());
                }
                else
                {
                    builder.Append(TextManager.GetText("/pageText/formEditor.aspx/hiddenItems", page.Parent.LanguageCode));
                }
                List<Item> items = page.Items;
                bool flag = false;
                foreach (Item item in items)
                {
                    if (!item.Excluded && (item is IAnswerable))
                    {
                        IItemFormatter itemFormatter = ItemFormatterFactory.GetItemFormatter(item.TypeID, "Text");
                        if (itemFormatter != null)
                        {
                            string str = itemFormatter.Format(item, "Text");
                            if (Utilities.IsNotNullOrEmpty(str))
                            {
                                flag = true;
                                builder.AppendLine();
                                builder.AppendLine(str);
                            }
                        }
                    }
                }
                if (flag)
                {
                    return builder.ToString();
                }
            }
            return string.Empty;
        }
    }
}

