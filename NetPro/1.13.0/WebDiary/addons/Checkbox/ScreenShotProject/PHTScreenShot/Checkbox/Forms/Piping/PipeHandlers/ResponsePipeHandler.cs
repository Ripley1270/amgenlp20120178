﻿namespace Checkbox.Forms.Piping.PipeHandlers
{
    using Checkbox.Forms;
    using Checkbox.Forms.Piping.Tokens;
    using System;

    public class ResponsePipeHandler : PipeHandler
    {
        public override string GetTokenValue(Token token, object context)
        {
            if ((context is ResponseProperties) && (context != null))
            {
                return ((ResponseProperties) context).GetStringValue(token.CleanName);
            }
            return string.Empty;
        }
    }
}

