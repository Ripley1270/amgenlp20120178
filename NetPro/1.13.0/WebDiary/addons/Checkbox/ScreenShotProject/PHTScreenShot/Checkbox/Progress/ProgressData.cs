﻿namespace Checkbox.Progress
{
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class ProgressData
    {
        public string AdditionalData { get; set; }

        public int CurrentItem { get; set; }

        public string ErrorMessage { get; set; }

        public string Message { get; set; }

        public double PercentComplete
        {
            get
            {
                if (this.TotalItemCount == 0)
                {
                    return 0.0;
                }
                return ((((double) this.CurrentItem) / ((double) this.TotalItemCount)) * 100.0);
            }
        }

        public ProgressStatus Status { get; set; }

        public int TotalItemCount { get; set; }
    }
}

