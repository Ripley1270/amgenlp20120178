﻿namespace Checkbox.Forms.Items.Configuration
{
    using System;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class ListOptionData : IEquatable<ListOptionData>, IComparable<ListOptionData>
    {
        public ListOptionData()
        {
        }

        public ListOptionData(DataRow data)
        {
            this.Load(data);
        }

        public int CompareTo(ListOptionData other)
        {
            if (this.Position > other.Position)
            {
                return 1;
            }
            if (this.Position < other.Position)
            {
                return -1;
            }
            return 0;
        }

        public void Load(DataRow data)
        {
            this.OptionID = (data["OptionID"] != DBNull.Value) ? Convert.ToInt32(data["OptionID"]) : -1;
            this.Alias = (data["Alias"] != DBNull.Value) ? ((string) data["Alias"]) : string.Empty;
            object obj2 = data["IsDefault"];
            if (obj2 == DBNull.Value)
            {
                this.IsDefault = false;
            }
            else
            {
                this.IsDefault = Convert.ToInt32(obj2) == 1;
            }
            object obj3 = data["IsOther"];
            if (obj3 == DBNull.Value)
            {
                this.IsOther = false;
            }
            else
            {
                this.IsOther = Convert.ToInt32(obj3) == 1;
            }
            object obj4 = data["Deleted"];
            if (obj4 == DBNull.Value)
            {
                this.Deleted = false;
            }
            else
            {
                this.Deleted = Convert.ToInt32(obj4) == 1;
            }
            this.Position = (data["Position"] != DBNull.Value) ? Convert.ToInt32(data["Position"]) : 1;
            this.Points = (data["Points"] != DBNull.Value) ? Convert.ToDouble(data["Points"]) : 0.0;
        }

        bool IEquatable<ListOptionData>.Equals(ListOptionData other)
        {
            return (other.OptionID == this.OptionID);
        }

        public string Alias { get; set; }

        public bool Deleted { get; private set; }

        public bool Disabled { get; set; }

        public bool IsDefault { get; set; }

        public bool IsOther { get; set; }

        public int OptionID { get; set; }

        public double Points { get; set; }

        public int Position { get; set; }

        public string TextID
        {
            get
            {
                if (this.OptionID > 0)
                {
                    return ("/listOption/" + this.OptionID + "/text");
                }
                return string.Empty;
            }
        }
    }
}

