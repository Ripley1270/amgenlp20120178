﻿using Checkbox.Forms.Items.Configuration;
using Checkbox.Forms.Logic.Configuration;
using Prezza.Framework.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Checkbox.Forms
{
    //using Checkbox.Forms.Logic.Configuration;
    //using Prezza.Framework.Common;
    //using System;
    //using System.Collections.Generic;
    //using System.ComponentModel;
    //using System.Runtime.CompilerServices;

    [Serializable]
    public class TemplatePage : IEquatable<TemplatePage>, ITransactional
    {
        [NonSerialized]
        private EventHandlerList _eventHandlers;
        private Template _parent;
        private static readonly object EventTransactionAborted = new object();
        private static readonly object EventTransactionCommitted = new object();

        public event EventHandler TransactionAborted
        {
            add
            {
                this.Events.AddHandler(EventTransactionAborted, value);
            }
            remove
            {
                this.Events.RemoveHandler(EventTransactionAborted, value);
            }
        }

        public event EventHandler TransactionCommitted
        {
            add
            {
                this.Events.AddHandler(EventTransactionCommitted, value);
            }
            remove
            {
                this.Events.RemoveHandler(EventTransactionCommitted, value);
            }
        }

        public TemplatePage(int position)
        {
            this.Position = position;
        }

        public void AddRule(RuleData rule)
        {
            if (this.Parent is ResponseTemplate)
            {
                ((ResponseTemplate) this.Parent).CreateRuleDataService().AddRuleToPage(this, rule);
            }
        }

        public bool Equals(TemplatePage other)
        {
            if (!this.Identity.HasValue)
            {
                return false;
            }
            return (this.Identity == other.Identity);
        }

        public void NotifyAbort(object sender, EventArgs e)
        {
            this.Rollback();
            ((ITransactional) sender).TransactionAborted -= new EventHandler(this.NotifyAbort);
            ((ITransactional) sender).TransactionCommitted -= new EventHandler(this.NotifyCommit);
            EventHandler handler = (EventHandler) this.Events[EventTransactionAborted];
            if (handler != null)
            {
                handler(this, null);
            }
        }

        public void NotifyCommit(object sender, EventArgs e)
        {
            ((ITransactional) sender).TransactionAborted -= new EventHandler(this.NotifyAbort);
            ((ITransactional) sender).TransactionCommitted -= new EventHandler(this.NotifyCommit);
            EventHandler handler = (EventHandler) this.Events[EventTransactionCommitted];
            if (handler != null)
            {
                handler(this, null);
            }
        }

        public void RemoveRule(RuleData rule)
        {
            if (this.Parent is ResponseTemplate)
            {
                ((ResponseTemplate) this.Parent).CreateRuleDataService().RemoveRuleFromPage(this, rule);
            }
        }

        public void Rollback()
        {
        }

        public RuleData[] BranchRules
        {
            get
            {
                if ((this.Parent is ResponseTemplate) && ((ResponseTemplate) this.Parent).CreateRuleDataService().PageHasBranches(this))
                {
                    return ((ResponseTemplate) this.Parent).CreateRuleDataService().GetBranchRulesForPage(this);
                }
                return null;
            }
        }

        protected EventHandlerList Events
        {
            get
            {
                if (this._eventHandlers == null)
                {
                    this._eventHandlers = new EventHandlerList();
                }
                return this._eventHandlers;
            }
        }

        public int? GetID
        {
            get
            {
                return this.Identity;
            }
        }

        internal int? Identity { get; set; }

        public RuleData IncludeRule
        {
            get
            {
                if ((this.Parent is ResponseTemplate) && ((ResponseTemplate) this.Parent).CreateRuleDataService().PageHasCondition(this))
                {
                    return ((ResponseTemplate) this.Parent).CreateRuleDataService().GetConditionForPage(this);
                }
                return null;
            }
        }

        public List<int> ItemIds
        {
            get
            {
                return this._parent.GetPageItemDataIDs(this.Identity.Value);
            }
        }

        public List<ItemData> Items
        {
            get
            {
                List<ItemData> list = new List<ItemData>();
                foreach (int num in this._parent.GetPageItemDataIDs(this.Identity.Value))
                {
                    if (this._parent.ContainsItem(num))
                    {
                        list.Add(this._parent.GetItem(num));
                    }
                }
                return list;
            }
        }

        public int? LayoutTemplateID { get; set; }

        public Template Parent
        {
            get
            {
                return this._parent;
            }
            set
            {
                this._parent = value;
            }
        }

        public int Position { get; set; }
    }
}

