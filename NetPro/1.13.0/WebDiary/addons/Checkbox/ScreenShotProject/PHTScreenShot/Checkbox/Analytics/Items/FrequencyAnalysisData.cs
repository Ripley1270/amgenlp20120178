﻿namespace Checkbox.Analytics.Items
{
    using Checkbox.Analytics.Items.Configuration;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class FrequencyAnalysisData
    {
        private readonly Dictionary<string, int> _answerCounts = new Dictionary<string, int>();
        private readonly List<string> _otherTexts = new List<string>();

        public FrequencyAnalysisData()
        {
            this.TotalAnswers = 0;
            this.OtherOption = Checkbox.Analytics.Items.Configuration.OtherOption.Aggregate;
        }

        public void AddOtherText(string text)
        {
            this._otherTexts.Add(text);
            if (this.OtherOption == Checkbox.Analytics.Items.Configuration.OtherOption.Display)
            {
                this.Increment(text);
            }
            else
            {
                this.Increment(this.OtherText);
            }
        }

        public void AddPossibleAnswer(string answer)
        {
            if (!this._answerCounts.ContainsKey(answer))
            {
                this._answerCounts[answer] = 0;
            }
        }

        public void Increment(string answer)
        {
            if (answer == null)
            {
                answer = string.Empty;
            }
            if (this._answerCounts.ContainsKey(answer))
            {
                this._answerCounts[answer] += 1;
            }
            else
            {
                this._answerCounts[answer] = 1;
            }
            this.TotalAnswers++;
        }

        public Dictionary<string, int> AnswerCounts
        {
            get
            {
                return this._answerCounts;
            }
        }

        public Checkbox.Analytics.Items.Configuration.OtherOption OtherOption { get; set; }

        public string OtherText { get; set; }

        public List<string> OtherTexts
        {
            get
            {
                return this._otherTexts;
            }
        }

        public int ResponseCount { get; set; }

        public static string ResultsCountColumn
        {
            get
            {
                return "Count";
            }
        }

        public DataTable ResultsData
        {
            get
            {
                DataTable table = new DataTable();
                table.Columns.Add(ResultsTextColumn, typeof(string));
                table.Columns.Add(ResultsCountColumn, typeof(int));
                table.Columns.Add(ResultsPercentColumn, typeof(double));
                table.Columns.Add(ResultsPercentTextColumn, typeof(string));
                foreach (string str in this._answerCounts.Keys)
                {
                    double num;
                    if (this.TotalAnswers > 0)
                    {
                        num = 100.0 * (((double) this._answerCounts[str]) / ((double) this.TotalAnswers));
                    }
                    else
                    {
                        num = 0.0;
                    }
                    DataRow row = table.NewRow();
                    row[ResultsTextColumn] = str;
                    row[ResultsCountColumn] = this._answerCounts[str];
                    row[ResultsPercentColumn] = Math.Round(num, 2);
                    row[ResultsPercentTextColumn] = str + " - " + Convert.ToString(Math.Round(num, 2)) + "%";
                    table.Rows.Add(row);
                }
                return table;
            }
        }

        public static string ResultsPercentColumn
        {
            get
            {
                return "Percent";
            }
        }

        public static string ResultsPercentTextColumn
        {
            get
            {
                return "PercentText";
            }
        }

        public static string ResultsTextColumn
        {
            get
            {
                return "Text";
            }
        }

        public int TotalAnswers { get; private set; }
    }
}

