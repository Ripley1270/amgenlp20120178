﻿namespace Checkbox.Management
{
    using System;

    public enum ErrorMessages
    {
        InvalidSurvey = 2,
        NoPermission = 1
    }
}

