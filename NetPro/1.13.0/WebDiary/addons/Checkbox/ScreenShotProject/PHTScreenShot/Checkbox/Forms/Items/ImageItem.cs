﻿namespace Checkbox.Forms.Items
{
    using Checkbox.Forms.Items.Configuration;
    using Prezza.Framework.Common;
    using System;

    [Serializable]
    public class ImageItem : ResponseItem
    {
        private string _altTextID;
        private string _imagePath;

        public override void Configure(ItemData configuration, string languageCode)
        {
            ArgumentValidation.CheckExpectedType(configuration, typeof(ImageItemData));
            base.Configure(configuration, languageCode);
            ImageItemData data = (ImageItemData) configuration;
            this._altTextID = data.AlternateTextID;
            this._imagePath = data.ImagePath;
        }

        public string AlternateText
        {
            get
            {
                return this.GetText(this._altTextID);
            }
        }

        public string ImagePath
        {
            get
            {
                return this._imagePath;
            }
        }
    }
}

