﻿namespace Checkbox.Forms.Items
{
    using Checkbox.Common;
    using Checkbox.Forms.Items.Configuration;
    using Checkbox.Forms.Validation;
    using Prezza.Framework.Common;
    using Prezza.Framework.Data;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.Data;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Text;
    using System.Xml;

    [Serializable]
    public abstract class SelectItem : LabelledItem, IScored
    {
        private Dictionary<int, ListOption> _optionsDictionary;
        private List<ListOption> _orderedOptions;

        protected SelectItem()
        {
        }

        protected virtual void AddAnswer(int optionId, string otherText)
        {
            DataRow row = base.AnswerData.CreateAnswerRow(base.ID);
            row["OptionID"] = optionId;
            if (this.OptionsDictionary.ContainsKey(optionId) && this.OptionsDictionary[optionId].IsOther)
            {
                row["AnswerText"] = otherText;
            }
            base.AnswerData.AddAnswerRow(row);
        }

        protected virtual void AddPlaceHolderAnswer()
        {
            foreach (DataRow row in base.AnswerData.GetAnswerRowsForItem(base.ID))
            {
                if (row["OptionId"] == DBNull.Value)
                {
                    return;
                }
            }
            DataRow row2 = base.AnswerData.CreateAnswerRow(base.ID);
            base.AnswerData.AddAnswerRow(row2);
        }

        public override void Configure(ItemData configuration, string languageCode)
        {
            ArgumentValidation.CheckExpectedType(configuration, typeof(SelectItemData));
            base.Configure(configuration, languageCode);
            SelectItemData data = (SelectItemData) configuration;
            this.OtherTextPrompt = this.GetText(data.OtherTextID);
            this.IsRandomized = data.Randomize;
            this.AllowOther = data.AllowOther;
            this.CreateOptions(data.Options);
        }

        protected virtual ListOption CreateOption(ListOptionData metaOption)
        {
            ListOption option = new ListOption {
                ID = metaOption.OptionID,
                Alias = metaOption.Alias,
                IsSelected = metaOption.IsDefault,
                IsDefault = metaOption.IsDefault,
                IsOther = metaOption.IsOther,
                Disabled = metaOption.Disabled,
                Points = metaOption.Points,
                Text = metaOption.IsOther ? this.OtherTextPrompt : this.GetText(metaOption.TextID)
            };
            option.OptionTextDelegate = (GetOptionTextDelegate) Delegate.Combine(option.OptionTextDelegate, new GetOptionTextDelegate(this.GetOptionText));
            return option;
        }

        protected virtual void CreateOptions(ReadOnlyCollection<ListOptionData> metaOptions)
        {
            this.OptionsDictionary.Clear();
            this.Options.Clear();
            foreach (ListOptionData data in metaOptions)
            {
                ListOption item = this.CreateOption(data);
                if (item != null)
                {
                    this.OptionsDictionary[item.ID] = item;
                    this.Options.Add(item);
                }
            }
        }

        protected virtual void DeleteAnswer(int optionId)
        {
            List<long> list = new List<long>();
            foreach (DataRow row in this.GetAnswerRowsForOption(optionId))
            {
                if (((row["OptionID"] != DBNull.Value) && (((int) row["OptionID"]) == optionId)) && (row["AnswerId"] != DBNull.Value))
                {
                    list.Add((long) row["AnswerId"]);
                }
            }
            foreach (long num in list)
            {
                base.AnswerData.DeleteAnswerRow(num);
            }
        }

        public override void DeleteAnswers()
        {
            base.DeleteAnswers();
            this.SynchronizeSelectedOptions();
        }

        public override string GetAnswer()
        {
            StringBuilder builder = new StringBuilder();
            List<ListOption> selectedOptions = this.SelectedOptions;
            for (int i = 0; i < selectedOptions.Count; i++)
            {
                if (i > 0)
                {
                    builder.Append(", ");
                }
                string text = selectedOptions[i].Text;
                if (selectedOptions[i].IsOther && Utilities.IsNotNullOrEmpty(this.OtherText))
                {
                    text = this.OtherText;
                }
                builder.Append(text);
            }
            return builder.ToString();
        }

        protected virtual List<DataRow> GetAnswerRowsForOption(int optionId)
        {
            List<DataRow> list = new List<DataRow>();
            foreach (DataRow row in base.AnswerData.GetAnswerRowsForItem(base.ID))
            {
                if (((row["OptionID"] != DBNull.Value) && (((int) row["OptionID"]) == optionId)) && (row["AnswerId"] != DBNull.Value))
                {
                    list.Add(row);
                }
            }
            return list;
        }

        protected override NameValueCollection GetInstanceDataValuesForXmlSerialization()
        {
            NameValueCollection instanceDataValuesForXmlSerialization = base.GetInstanceDataValuesForXmlSerialization();
            instanceDataValuesForXmlSerialization["otherText"] = this.OtherText;
            instanceDataValuesForXmlSerialization["otherPrompt"] = this.OtherTextPrompt;
            return instanceDataValuesForXmlSerialization;
        }

        protected override NameValueCollection GetMetaDataValuesForXmlSerialization()
        {
            NameValueCollection metaDataValuesForXmlSerialization = base.GetMetaDataValuesForXmlSerialization();
            metaDataValuesForXmlSerialization["randomizeOptionOrder"] = this.IsRandomized.ToString();
            metaDataValuesForXmlSerialization["allowOther"] = this.AllowOther.ToString();
            return metaDataValuesForXmlSerialization;
        }

        public virtual string GetOptionText(int optionId, string defaultText)
        {
            return this.GetPipedText(string.Concat(new object[] { "OptionText_", base.ID, "_", optionId }), defaultText);
        }

        public virtual double GetScore()
        {
            double num = 0.0;
            foreach (ListOption option in this.Options)
            {
                if (option.IsSelected)
                {
                    num += option.Points;
                }
            }
            return num;
        }

        protected override void OnAnswerDataSet()
        {
            base.OnAnswerDataSet();
            this.SynchronizeSelectedOptions();
        }

        protected override void OnStateRestored()
        {
            base.OnStateRestored();
            if (this.IsRandomized)
            {
                List<int> itemOptionOrder = base.Response.GetItemOptionOrder(base.ID);
                if (!this.VerifyOptionOrder(itemOptionOrder))
                {
                    this.RandomizeOptionOrder();
                    for (int i = 0; i < this.Options.Count; i++)
                    {
                        base.Response.SaveItemOptionOrder(base.ID, this.Options[i].ID, i + 1);
                    }
                }
                else if (itemOptionOrder.Count > 0)
                {
                    this.Options.Clear();
                    foreach (int num2 in itemOptionOrder)
                    {
                        if (this.OptionsDictionary.ContainsKey(num2))
                        {
                            this.Options.Add(this.OptionsDictionary[num2]);
                        }
                    }
                }
            }
        }

        protected virtual void RandomizeOptionOrder()
        {
            IEnumerable<ListOption> collection = from option in this.Options
                where option.IsOther
                select option;
            List<ListOption> list = new List<ListOption>();
            list.AddRange(Utilities.RandomizeList<ListOption>((from option in this.Options
                where !option.IsOther
                select option).ToList<ListOption>()));
            list.AddRange(collection);
            this.Options.Clear();
            this.Options.AddRange(list);
        }

        protected virtual void RemovePlaceHolderAnswer()
        {
            DataRow[] answerRowsForItem = base.AnswerData.GetAnswerRowsForItem(base.ID);
            long? nullable = null;
            foreach (DataRow row in answerRowsForItem)
            {
                if (row["OptionId"] == DBNull.Value)
                {
                    long? defaultValue = null;
                    nullable = DbUtility.GetValueFromDataRow<long?>(row, "AnswerID", defaultValue);
                }
            }
            if (nullable.HasValue)
            {
                base.AnswerData.DeleteAnswerRow(nullable.Value);
            }
        }

        public virtual void Select(params int[] optionIDs)
        {
            this.Select(null, optionIDs);
        }

        public virtual void Select(string otherText, params int[] optionIDs)
        {
            List<int> list = new List<int>(optionIDs);
            this.OtherText = otherText;
            DataRow[] answerRowsForItem = base.AnswerData.GetAnswerRowsForItem(base.ID);
            Dictionary<int, long> dictionary = new Dictionary<int, long>();
            foreach (DataRow row in answerRowsForItem)
            {
                if (row["OptionID"] != DBNull.Value)
                {
                    dictionary[(int) row["OptionID"]] = (long) row["AnswerID"];
                }
            }
            foreach (int num in list)
            {
                if (!dictionary.ContainsKey(num))
                {
                    this.AddAnswer(num, otherText);
                }
            }
            foreach (int num2 in dictionary.Keys)
            {
                if (!list.Contains(num2))
                {
                    this.DeleteAnswer(num2);
                }
            }
            foreach (ListOption option in this._optionsDictionary.Values)
            {
                if (option.IsOther)
                {
                    this.UpdateAnswer(option.ID, otherText);
                }
            }
            this.SynchronizeSelectedOptions();
            this.OnAnswerChanged();
        }

        public override void SetAnswer(object answer)
        {
            throw new Exception("Set answer not supported for select items.");
        }

        protected virtual void SynchronizeSelectedOptions()
        {
            DataRow[] answerRowsForItem = base.AnswerData.GetAnswerRowsForItem(base.ID);
            if (answerRowsForItem.Length == 0)
            {
                foreach (ListOption option in this.OptionsDictionary.Values)
                {
                    option.IsSelected = option.IsDefault;
                }
            }
            else
            {
                List<int> list = new List<int>();
                foreach (DataRow row in answerRowsForItem)
                {
                    if (row["OptionID"] != DBNull.Value)
                    {
                        int item = (int) row["OptionID"];
                        list.Add(item);
                        if (this.OptionsDictionary.ContainsKey(item) && this.OptionsDictionary[item].IsOther)
                        {
                            this.OtherText = DbUtility.GetValueFromDataRow<string>(row, "AnswerText", string.Empty);
                        }
                    }
                }
                foreach (ListOption option2 in this.OptionsDictionary.Values)
                {
                    option2.IsSelected = list.Contains(option2.ID);
                }
            }
        }

        protected virtual void UpdateAnswer(int optionId, string answerText)
        {
            foreach (DataRow row in this.GetAnswerRowsForOption(optionId))
            {
                if ((row["AnswerText"] == DBNull.Value) || (((string) row["AnswerText"]) != answerText))
                {
                    row["AnswerText"] = answerText;
                }
            }
        }

        protected override bool ValidateAnswers()
        {
            SelectItemValidator validator = new SelectItemValidator();
            if (!validator.Validate(this))
            {
                base.ValidationErrors.Add(validator.GetMessage(base.LanguageCode));
                return false;
            }
            return true;
        }

        protected virtual bool VerifyOptionOrder(List<int> optionOrder)
        {
            if (optionOrder.Count != this.Options.Count)
            {
                return false;
            }
            using (List<int>.Enumerator enumerator = optionOrder.GetEnumerator())
            {
                Predicate<ListOption> match = null;
                int optionId;
                while (enumerator.MoveNext())
                {
                    optionId = enumerator.Current;
                    if (match == null)
                    {
                        match = opt => opt.ID == optionId;
                    }
                    if (!this.Options.Exists(match))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        protected override void WriteAnswers(XmlWriter writer)
        {
            foreach (ListOption option in this.Options)
            {
                if (option.IsSelected)
                {
                    this.WriteOptionAnswer(option, writer);
                }
            }
        }

        protected virtual void WriteOptionAnswer(ListOption option, XmlWriter writer)
        {
            writer.WriteStartElement("answer");
            writer.WriteAttributeString("optionId", option.ID.ToString());
            if (option.IsOther)
            {
                writer.WriteCData(this.OtherText);
            }
            else
            {
                writer.WriteCData(option.Text);
            }
            writer.WriteEndElement();
        }

        public override void WriteXmlInstanceData(XmlWriter writer)
        {
            base.WriteXmlInstanceData(writer);
            List<ListOption> options = this.Options;
            writer.WriteStartElement("listOptions");
            foreach (ListOption option in options)
            {
                option.WriteXml(writer);
            }
            writer.WriteEndElement();
        }

        public bool AllowOther { get; private set; }

        public override bool HasAnswer
        {
            get
            {
                return (this.SelectedOptions.Count > 0);
            }
        }

        public bool IsRandomized { get; private set; }

        public List<ListOption> Options
        {
            get
            {
                if (this._orderedOptions == null)
                {
                    this._orderedOptions = new List<ListOption>();
                }
                return this._orderedOptions;
            }
        }

        protected Dictionary<int, ListOption> OptionsDictionary
        {
            get
            {
                if (this._optionsDictionary == null)
                {
                    this._optionsDictionary = new Dictionary<int, ListOption>();
                }
                return this._optionsDictionary;
            }
        }

        public string OtherText { get; set; }

        public string OtherTextPrompt { get; private set; }

        public List<ListOption> SelectedOptions
        {
            get
            {
                List<ListOption> options = this.Options;
                List<ListOption> list2 = new List<ListOption>();
                foreach (ListOption option in options)
                {
                    if (option.IsSelected)
                    {
                        list2.Add(option);
                    }
                }
                return list2;
            }
        }
    }
}

