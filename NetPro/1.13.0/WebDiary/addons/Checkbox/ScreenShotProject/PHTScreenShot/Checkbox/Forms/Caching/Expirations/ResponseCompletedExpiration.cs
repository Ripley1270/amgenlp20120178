﻿namespace Checkbox.Forms.Caching.Expirations
{
    using Checkbox.Forms;
    using Prezza.Framework.Caching;
    using Prezza.Framework.ExceptionHandling;
    using System;

    [Serializable]
    public class ResponseCompletedExpiration : ICacheItemExpiration
    {
        private bool _expired;

        public bool HasExpired()
        {
            return this._expired;
        }

        public void Initialize(CacheItem owningCacheItem)
        {
            try
            {
                if (!(owningCacheItem.Value is Response))
                {
                    throw new Exception("ResponseCompletedExpiration only valid for Response objects.");
                }
                if (owningCacheItem.Value != null)
                {
                    ((Response) owningCacheItem.Value).ResponseCompleted += new Response.ResponseCompletedHandler(this.ResponseCompletedExpiration_ResponseCompleted);
                }
            }
            catch (Exception exception)
            {
                ExceptionPolicy.HandleException(exception, "BusinessProtected");
            }
        }

        public void Notify()
        {
        }

        private void ResponseCompletedExpiration_ResponseCompleted(object sender, ResponseStateEventArgs e)
        {
            this._expired = true;
            ((Response) sender).ResponseCompleted -= new Response.ResponseCompletedHandler(this.ResponseCompletedExpiration_ResponseCompleted);
        }
    }
}

