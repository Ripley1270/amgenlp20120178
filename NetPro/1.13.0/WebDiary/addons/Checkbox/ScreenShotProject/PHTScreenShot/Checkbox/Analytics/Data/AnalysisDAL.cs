﻿namespace Checkbox.Analytics.Data
{
    using Checkbox.Analytics;
    using Checkbox.Common;
    using Prezza.Framework.Data;
    using Prezza.Framework.Security;
    using Prezza.Framework.Security.Principal;
    using System;
    using System.Data;
    using System.Runtime.InteropServices;

    internal static class AnalysisDAL
    {
        internal static DataTable ListAnalyses(ExtendedPrincipal principal, int responseTemplateID, out int analysisCount)
        {
            return ListAnalyses(principal, responseTemplateID, -1, -1, null, null, null, 0, out analysisCount);
        }

        internal static DataTable ListAnalyses(ExtendedPrincipal principal, int responseTemplateID, int pageNumber, int pageSize, string filterField, string filterCriteria, string sortField, int sortDirection, out int analysisCount)
        {
            SelectQuery query = QueryFactory.GetAnalysesForFormQuery(responseTemplateID, filterField, filterCriteria);
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
            DataSet set = database.ExecuteDataSet(sqlStringCommandWrapper);
            DataTable table = set.Tables[0].Clone();
            table.TableName = "Analyses";
            int num = ((pageNumber - 1) * pageSize) + 1;
            int num2 = (num + pageSize) - 1;
            int num3 = 0;
            IAuthorizationProvider authorizationProvider = AuthorizationFactory.GetAuthorizationProvider();
            string sort = string.Empty;
            if (Utilities.IsNotNullOrEmpty(sortField))
            {
                sort = sortField + ((sortDirection == 1) ? " ASC" : " DESC");
            }
            foreach (DataRow row in set.Tables[0].Select(null, sort, DataViewRowState.CurrentRows))
            {
                int analysisTemplateID = (int) row["AnalysisTemplateID"];
                LightweightAccessControllable lightweightAnalysisTemplate = AnalysisTemplateManager.GetLightweightAnalysisTemplate(analysisTemplateID);
                if ((lightweightAnalysisTemplate != null) && (authorizationProvider.Authorize(principal, lightweightAnalysisTemplate, "Analysis.Edit") || authorizationProvider.Authorize(principal, lightweightAnalysisTemplate, "Analysis.Run")))
                {
                    num3++;
                    if (((pageNumber <= 0) || (pageSize <= 0)) || ((num3 >= num) && (num3 <= num2)))
                    {
                        table.ImportRow(row);
                    }
                }
            }
            analysisCount = num3;
            return table;
        }
    }
}

