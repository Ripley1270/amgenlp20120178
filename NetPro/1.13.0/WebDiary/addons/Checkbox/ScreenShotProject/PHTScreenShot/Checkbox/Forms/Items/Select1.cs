﻿using Checkbox.Forms.Items.Configuration;
using System;
using System.Collections.ObjectModel;

namespace Checkbox.Forms.Items
{
    //using System;
    //using System.Collections.ObjectModel;

    [Serializable]
    public class Select1 : SelectItem
    {
        protected override void CreateOptions(ReadOnlyCollection<ListOptionData> metaOptions)
        {
            base.CreateOptions(metaOptions);
            this.EnsureSingleOptionSelected();
        }

        protected virtual void EnsureSingleOptionSelected()
        {
            bool flag = false;
            foreach (ListOption option in base.OptionsDictionary.Values)
            {
                option.IsSelected = option.IsSelected && !flag;
                flag = flag || option.IsSelected;
            }
        }

        public override void Select(string otherText, params int[] optionIDs)
        {
            if (optionIDs.Length > 1)
            {
                throw new Exception("Too many options were selected.");
            }
            base.Select(otherText, optionIDs);
        }

        protected override void SynchronizeSelectedOptions()
        {
            base.SynchronizeSelectedOptions();
            this.EnsureSingleOptionSelected();
        }
    }
}

