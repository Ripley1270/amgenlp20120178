﻿namespace Checkbox.Messaging.Email
{
    using System;
    using System.IO;

    public interface IEmailAttachment
    {
        Stream GetContentStream();

        string FileName { get; set; }

        string MimeContentTypeString { get; set; }
    }
}

