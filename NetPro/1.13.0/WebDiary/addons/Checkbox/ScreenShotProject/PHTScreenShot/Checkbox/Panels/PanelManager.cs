﻿namespace Checkbox.Panels
{
    using Prezza.Framework.Data;
    using Prezza.Framework.Data.Sprocs;
    using Prezza.Framework.Security;
    using System;
    using System.Data;

    public class PanelManager
    {
        public static EmailListPanel CreateEmailListPanel()
        {
            return (EmailListPanel) CreatePanel(3);
        }

        public static Panel CreatePanel(int panelTypeID)
        {
            return new PanelFactory().CreatePanel(panelTypeID);
        }

        public static int GetEmailListPanelAddressCount(int panelID)
        {
            int num;
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_EmailListPanel_GetEmailAddressCount");
            storedProcCommandWrapper.AddInParameter("PanelID", DbType.Int32, panelID);
            object obj2 = database.ExecuteScalar(storedProcCommandWrapper);
            if ((obj2 != null) && int.TryParse(obj2.ToString(), out num))
            {
                return num;
            }
            return 0;
        }

        public static LightweightAccessControllable GetLightWeightPanel(int panelID)
        {
            LightweightPanel theObject = new LightweightPanel(panelID);
            StoredProcedureCommandExtractor.ExecuteProcedure(ProcedureType.Select, theObject);
            return theObject;
        }

        public static Panel GetPanel(int panelID)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Panel_GetTypeID");
            storedProcCommandWrapper.AddInParameter("PanelID", DbType.Int32, panelID);
            int? nullable = null;
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    while (reader.Read())
                    {
                        nullable = new int?((int) reader[0]);
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            Panel panel = null;
            if (nullable.HasValue)
            {
                panel = CreatePanel(nullable.Value);
            }
            else
            {
                return panel;
            }
            panel.Identity = new int?(panelID);
            panel.Load();
            return panel;
        }
    }
}

