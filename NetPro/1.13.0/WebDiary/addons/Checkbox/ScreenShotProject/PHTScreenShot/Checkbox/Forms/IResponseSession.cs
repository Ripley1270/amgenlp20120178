﻿namespace Checkbox.Forms
{
    using Prezza.Framework.Security.Principal;
    using System;

    public interface IResponseSession
    {
        void ClearPersistedValues();
        string GetRespondentLanguageCode(string source, string token);
        void Load();
        void Save();

        bool AdminEditMode { get; set; }

        Guid? AnonymousRespondentGuid { get; set; }

        int? CurrentPageId { get; set; }

        bool ForceNew { get; set; }

        bool IsFormPost { get; }

        bool IsTest { get; set; }

        string LanguageCode { get; set; }

        string Password { get; set; }

        Guid? RecipientGuid { get; set; }

        ExtendedPrincipal Respondent { get; set; }

        bool RespondentEditMode { get; set; }

        string RespondentIPAddress { get; set; }

        Guid? ResponseGuid { get; set; }

        string ServerUserContext { get; set; }

        Guid? SurveyGuid { get; set; }

        Guid? UserGuid { get; set; }
    }
}

