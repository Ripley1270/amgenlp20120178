﻿namespace Checkbox.Forms.Logic.Configuration
{
    using Checkbox.Forms;
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Data;
    using System.Text;

    internal class OperandDataManager
    {
        public static OperandData GetOperandData(ResponseTemplate context, string languageCode, int id)
        {
            try
            {
                OperandData data = null;
                Database database = DatabaseFactory.CreateDatabase();
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Rules_GetOperand");
                storedProcCommandWrapper.AddInParameter("OperandID", DbType.Int32, id);
                using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
                {
                    try
                    {
                        while (reader.Read())
                        {
                            StringBuilder builder = new StringBuilder();
                            builder.Append((string) reader["TypeName"]);
                            builder.Append(",");
                            builder.Append((string) reader["TypeAssembly"]);
                            data = new OperandDataFactory().CreateOperandData(builder.ToString(), new object[] { context, languageCode });
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
                if (data != null)
                {
                    data.Load(id);
                }
                return data;
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessInternal"))
                {
                    throw;
                }
            }
            return null;
        }
    }
}

