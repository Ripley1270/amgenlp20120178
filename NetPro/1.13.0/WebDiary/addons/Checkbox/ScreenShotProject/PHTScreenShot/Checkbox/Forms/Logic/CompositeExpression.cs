﻿namespace Checkbox.Forms.Logic
{
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class CompositeExpression : Expression
    {
        private List<Expression> _children;
        private LogicalConnector _connector;

        public CompositeExpression(List<Expression> children, LogicalConnector connector) : base(null, null, LogicalOperator.OperatorNotSpecified)
        {
            this._children = children;
            this._connector = connector;
        }

        internal void Add(Expression child)
        {
            child.Parent = this;
            this._children.Add(child);
        }

        internal override bool Evaluate(RuleEventTrigger eventTrigger)
        {
            bool flag = false;
            foreach (Expression expression in this._children)
            {
                flag = expression.Evaluate(eventTrigger);
                if (flag)
                {
                    if (this.Connector != LogicalConnector.AND)
                    {
                        return flag;
                    }
                }
                else if (this.Connector != LogicalConnector.OR)
                {
                    return flag;
                }
            }
            return flag;
        }

        internal void Remove(Expression child)
        {
            this._children.Remove(child);
        }

        public List<Expression> Children
        {
            get
            {
                return this._children;
            }
        }

        public LogicalConnector Connector
        {
            get
            {
                return this._connector;
            }
        }
    }
}

