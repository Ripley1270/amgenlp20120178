﻿using Checkbox.Forms.Items;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Checkbox.Forms
{
    [Serializable]
    public class ResponseProperties
    {
        private readonly Dictionary<string, object> _properties;

        public object this[string key]
        {
            get
            {
                return this.GetObjectValue(key);
            }
            set
            {
                this.SetValue(key, value);
            }
        }

        public static ReadOnlyCollection<string> PropertyNames
        {
            get
            {
                List<string> strs = new List<string>();
                strs.Add("CurrentDateUS");
                strs.Add("CurrentDateROTW");
                strs.Add("CurrentScore");
                strs.Add("ResponseID");
                strs.Add("ResponseGuid");
                strs.Add("LastPageViewed");
                strs.Add("IP");
                strs.Add("UniqueIdentifier");
                strs.Add("NetworkUser");
                strs.Add("Language");
                strs.Add("Started");
                strs.Add("Ended");
                strs.Add("LastEdit");
                strs.Add("IsComplete");
                strs.Add("RespondentGuid");
                strs.Add("Invitee");
                List<string> strs1 = strs;
                strs1.Sort();
                return new ReadOnlyCollection<string>(strs1);
            }
        }

        public ResponseProperties()
        {
            this._properties = new Dictionary<string, object>(StringComparer.InvariantCultureIgnoreCase);
        }

        private static double GetCurrentScore(Response response, bool includeCurrentPage)
        {
            int num;
            double score = 0;
            if (response.Completed || response.CurrentPage != null)
            {
                if (response.PageCount != 0)
                {
                    if (response.Completed)
                    {
                        List<ResponsePage> pages = response.Pages;
                        num = pages.Max<ResponsePage>((ResponsePage page) => page.Position);
                    }
                    else
                    {
                        num = (includeCurrentPage ? response.CurrentPage.Position : response.CurrentPage.Position - 1);
                    }
                    int num1 = num;
                    foreach (ResponsePage responsePage in response.Pages.Where<ResponsePage>((ResponsePage page) => page.Position <= num1))
                    {
                        foreach (Checkbox.Forms.Items.Item item in responsePage.Items)
                        {
                            if (item as IScored == null)
                            {
                                continue;
                            }
                            score = score + ((IScored)item).GetScore();
                        }
                    }
                    return score;
                }
                else
                {
                    return score;
                }
            }
            else
            {
                return score;
            }
        }

        public object GetObjectValue(string key)
        {
            if (!this._properties.ContainsKey(key))
            {
                return null;
            }
            else
            {
                return this._properties[key];
            }
        }

        public string GetStringValue(string key)
        {
            object objectValue = this.GetObjectValue(key);
            if (objectValue != null)
            {
                return objectValue.ToString();
            }
            else
            {
                return string.Empty;
            }
        }

        public void Initialize(Response response)
        {
            double num;
            DateTime now = DateTime.Now;
            this._properties["CurrentDateUS"] = now.ToString("d", new CultureInfo("en-US"));
            DateTime dateTime = DateTime.Now;
            this._properties["CurrentDateROTW"] = dateTime.ToString("d", new CultureInfo("fr-FR"));
            Dictionary<string, object> strs = this._properties;
            string str = "CurrentScore";
            double? totalScore = response.TotalScore;
            num = (totalScore.HasValue ? (double)((double)totalScore.GetValueOrDefault()) : 0);
            strs[str] = num;
            this._properties["CurrentScoreInclusive"] = ResponseProperties.GetCurrentScore(response, true);
            this._properties["CurrentScoreExclusive"] = ResponseProperties.GetCurrentScore(response, false);
            this._properties["ResponseID"] = response.ID;
            this._properties["ResponseGuid"] = response.GUID;
            this._properties["LastPageViewed"] = response.LastPageViewed;
            this._properties["IP"] = response.IPAddress;
            this._properties["UniqueIdentifier"] = response.UniqueIdentifier;
            this._properties["NetworkUser"] = response.NetworkUser;
            this._properties["Language"] = response.LanguageCode;
            this._properties["Started"] = response.DateCreated;
            this._properties["Ended"] = response.DateCompleted;
            this._properties["LastEdit"] = response.LastModified;
            this._properties["IsComplete"] = response.Completed;
            this._properties["RespondentGuid"] = response.RespondentGuid;
            this._properties["ResponseTemplateID"] = response.ResponseTemplateID;
            this._properties["Invitee"] = response.Invitee;
        }

        public void SetValue(string propertyName, object value)
        {
            this._properties[propertyName] = value;
        }
    }
}

//namespace Checkbox.Forms
//{
//    using Checkbox.Forms.Items;
//    using System;
//    using System.Collections.Generic;
//    using System.Collections.ObjectModel;
//    using System.Globalization;
//    using System.Linq;
//    using System.Reflection;

//    [Serializable]
//    public class ResponseProperties
//    {
//        private readonly Dictionary<string, object> _properties = new Dictionary<string, object>(StringComparer.InvariantCultureIgnoreCase);

//        private static double GetCurrentScore(Response response, bool includeCurrentPage)
//        {
//            int maxPosition;
//            double num = 0.0;
//            if (response.Completed || (response.CurrentPage != null))
//            {
//                if (response.PageCount == 0)
//                {
//                    return num;
//                    if (CS$<>9__CachedAnonymousMethodDelegate3 == null)
//                    {
//                        CS$<>9__CachedAnonymousMethodDelegate3 = page => page.Position;
//                    }
//                }
//                maxPosition = response.Completed ? response.Pages.Max<ResponsePage>(CS$<>9__CachedAnonymousMethodDelegate3) : (includeCurrentPage ? response.CurrentPage.Position : (response.CurrentPage.Position - 1));
//                foreach (ResponsePage page in from page in response.Pages
//                    where page.Position <= maxPosition
//                    select page)
//                {
//                    foreach (Item item in page.Items)
//                    {
//                        if (item is IScored)
//                        {
//                            num += ((IScored) item).GetScore();
//                        }
//                    }
//                }
//            }
//            return num;
//        }

//        public object GetObjectValue(string key)
//        {
//            if (this._properties.ContainsKey(key))
//            {
//                return this._properties[key];
//            }
//            return null;
//        }

//        public string GetStringValue(string key)
//        {
//            object objectValue = this.GetObjectValue(key);
//            if (objectValue == null)
//            {
//                return string.Empty;
//            }
//            return objectValue.ToString();
//        }

//        public void Initialize(Response response)
//        {
//            this._properties["CurrentDateUS"] = DateTime.Now.ToString("d", new CultureInfo("en-US"));
//            this._properties["CurrentDateROTW"] = DateTime.Now.ToString("d", new CultureInfo("fr-FR"));
//            double? totalScore = response.TotalScore;
//            this._properties["CurrentScore"] = totalScore.HasValue ? totalScore.GetValueOrDefault() : 0.0;
//            this._properties["CurrentScoreInclusive"] = GetCurrentScore(response, true);
//            this._properties["CurrentScoreExclusive"] = GetCurrentScore(response, false);
//            this._properties["ResponseID"] = response.ID;
//            this._properties["ResponseGuid"] = response.GUID;
//            this._properties["LastPageViewed"] = response.LastPageViewed;
//            this._properties["IP"] = response.IPAddress;
//            this._properties["UniqueIdentifier"] = response.UniqueIdentifier;
//            this._properties["NetworkUser"] = response.NetworkUser;
//            this._properties["Language"] = response.LanguageCode;
//            this._properties["Started"] = response.DateCreated;
//            this._properties["Ended"] = response.DateCompleted;
//            this._properties["LastEdit"] = response.LastModified;
//            this._properties["IsComplete"] = response.Completed;
//            this._properties["RespondentGuid"] = response.RespondentGuid;
//            this._properties["ResponseTemplateID"] = response.ResponseTemplateID;
//            this._properties["Invitee"] = response.Invitee;
//        }

//        public void SetValue(string propertyName, object value)
//        {
//            this._properties[propertyName] = value;
//        }

//        public object this[string key]
//        {
//            get
//            {
//                return this.GetObjectValue(key);
//            }
//            set
//            {
//                this.SetValue(key, value);
//            }
//        }

//        public static ReadOnlyCollection<string> PropertyNames
//        {
//            get
//            {
//                List<string> list = new List<string> { "CurrentDateUS", "CurrentDateROTW", "CurrentScore", "ResponseID", "ResponseGuid", "LastPageViewed", "IP", "UniqueIdentifier", "NetworkUser", "Language", "Started", "Ended", "LastEdit", "IsComplete", "RespondentGuid", "Invitee" };
//                list.Sort();
//                return new ReadOnlyCollection<string>(list);
//            }
//        }
//    }
//}

