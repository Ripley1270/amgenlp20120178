﻿namespace Checkbox.Analytics.Items
{
    using Checkbox.Analytics.Computation;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class StatisticsItemResult : ItemResult
    {
        public double Mean { get; set; }

        public double Median { get; set; }

        public double Mode { get; set; }

        public double Response { get; set; }

        public double StandardDeviation { get; set; }
    }
}

