﻿namespace Checkbox.Panels
{
    using System;

    public enum PanelType
    {
        AdHocEmailListPanel = 4,
        EmailListPanel = 3,
        GroupPanel = 2,
        UserPanel = 1
    }
}

