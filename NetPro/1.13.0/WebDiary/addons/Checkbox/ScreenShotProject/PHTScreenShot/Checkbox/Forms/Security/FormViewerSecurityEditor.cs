﻿namespace Checkbox.Forms.Security
{
    using Prezza.Framework.Security;
    using System;
    using System.Collections.Generic;

    public class FormViewerSecurityEditor : FormSecurityEditor
    {
        public FormViewerSecurityEditor(IAccessControllable form) : base(form)
        {
        }

        public override List<IAccessControlEntry> GetAccessPermissible(params string[] permissions)
        {
            return this.GetMergedAccessPermissible(new string[] { "Analysis.Create", "Analysis.Delete", "Analysis.Edit", "Analysis.ManageFilters", "Analysis.Run" });
        }

        public override void GrantAccess(IAccessControlEntry[] pendingEntries, params string[] permissions)
        {
            base.UpdateAccess(pendingEntries, new string[] { "Analysis.Create", "Analysis.Delete", "Analysis.Edit", "Analysis.ManageFilters", "Analysis.Run" }, false);
        }

        public override void RemoveAccess(IAccessControlEntry[] pendingEntries)
        {
            base.UpdateAccess(pendingEntries, new string[] { "Analysis.Create", "Analysis.Delete", "Analysis.Edit", "Analysis.ManageFilters", "Analysis.Run" }, true);
        }
    }
}

