﻿namespace Checkbox.Messaging.Configuration
{
    using Prezza.Framework.Common;
    using Prezza.Framework.Configuration;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Xml;

    public class MessagingConfiguration : ConfigurationBase, IXmlConfigurationBase
    {
        private readonly Dictionary<string, ProviderData> _emailProviders;

        public MessagingConfiguration() : this(string.Empty)
        {
        }

        public MessagingConfiguration(string sectionName) : base(sectionName)
        {
            try
            {
                this._emailProviders = new Dictionary<string, ProviderData>();
                this.DefaultEmailProvider = string.Empty;
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
            }
        }

        public ProviderData GetEmailProviderConfig(string providerName)
        {
            try
            {
                ArgumentValidation.CheckForEmptyString(providerName, "providerName");
                if (!this._emailProviders.ContainsKey(providerName))
                {
                    throw new Exception("No configuration for specified email provider exists.  Specified provider was: " + providerName);
                }
                return this._emailProviders[providerName];
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
                return null;
            }
        }

        public void LoadFromXml(XmlNode node)
        {
            try
            {
                ArgumentValidation.CheckForNullReference(node, "node");
                foreach (XmlNode node2 in node.SelectNodes("/messagingConfiguration/emailProviders/providers/provider"))
                {
                    string attributeText = XmlUtility.GetAttributeText(node2, "name");
                    string filePath = XmlUtility.GetAttributeText(node2, "filePath");
                    string typeName = XmlUtility.GetAttributeText(node2, "configDataType");
                    bool attributeBool = XmlUtility.GetAttributeBool(node2, "default");
                    if (attributeText == string.Empty)
                    {
                        throw new Exception("An email provider was defined in the message configuration file, but the name was not specified: " + attributeText);
                    }
                    if (filePath == string.Empty)
                    {
                        throw new Exception("An email provider was defined in the message configuration file, but the provider's configuration file path was not specified: " + attributeText);
                    }
                    if (typeName == string.Empty)
                    {
                        throw new Exception("An email provider was defined in the message configuration file, but the provider's configuration object type name was not specified: " + attributeText);
                    }
                    object[] extraParams = new object[] { attributeText };
                    ProviderData data = (ProviderData) ConfigurationManager.GetConfiguration(filePath, typeName, extraParams);
                    this._emailProviders.Add(data.Name, data);
                    if (attributeBool)
                    {
                        this.DefaultEmailProvider = data.Name;
                    }
                }
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
            }
        }

        public string DefaultEmailProvider { get; private set; }
    }
}

