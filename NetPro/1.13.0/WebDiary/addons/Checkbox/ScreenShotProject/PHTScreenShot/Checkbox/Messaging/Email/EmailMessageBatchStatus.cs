﻿namespace Checkbox.Messaging.Email
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class EmailMessageBatchStatus : IEmailMessageBatchStatus
    {
        public int AttemptedCount { get; set; }

        public DateTime? BatchSendCompleted { get; set; }

        public DateTime? BatchSendStarted { get; set; }

        public BatchStatus CurrentStatus { get; set; }

        public int FailedCount { get; set; }

        public List<long> FailedMessages { get; set; }

        public DateTime? NextSendAttempt { get; set; }

        public int SucceededCount { get; set; }

        public bool SuccessfullySent { get; set; }
    }
}

