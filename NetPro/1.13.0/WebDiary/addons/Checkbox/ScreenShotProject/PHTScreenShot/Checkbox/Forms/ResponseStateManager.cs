﻿namespace Checkbox.Forms
{
    using Checkbox.Common;
    using Checkbox.Forms.Security.Principal;
    using Prezza.Framework.Data;
    using Prezza.Framework.Security.Principal;
    using System;
    using System.Data;

    public static class ResponseStateManager
    {
        private static DataTable GetAnonymizedResponseTable(string uniqueIdentifier, int responseTemplateID)
        {
            DataSet dataSet = new DataSet();
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Response_GetListForAnonymizedUser");
            storedProcCommandWrapper.AddInParameter("ResumeKey", DbType.String, Utilities.GetSaltedMD5Hash(uniqueIdentifier));
            storedProcCommandWrapper.AddInParameter("ResponseTemplateID", DbType.Int32, responseTemplateID);
            database.LoadDataSet(storedProcCommandWrapper, dataSet, "ResponseList");
            return dataSet.Tables["ResponseList"];
        }

        public static DataTable GetCompletedResponseList(ExtendedPrincipal principal, int responseTemplateID, bool isAnonymized)
        {
            DataTable responseTable;
            if ((principal is AnonymousRespondent) && ((AnonymousRespondent) principal).Guid.HasValue)
            {
                responseTable = GetResponseTable(((AnonymousRespondent) principal).Guid.Value, responseTemplateID);
            }
            else if (isAnonymized)
            {
                responseTable = GetAnonymizedResponseTable(principal.Identity.Name, responseTemplateID);
            }
            else
            {
                responseTable = GetResponseTable(principal.Identity.Name, responseTemplateID);
            }
            if (responseTable == null)
            {
                return null;
            }
            DataTable table2 = responseTable.Clone();
            foreach (DataRow row in responseTable.Select("IsComplete IS NOT NULL AND IsComplete = 1", "Started DESC", DataViewRowState.CurrentRows))
            {
                table2.ImportRow(row);
            }
            return table2;
        }

        public static ResponseState GetLastIncompleteResponse(ExtendedPrincipal principal, int responseTemplateID, bool isAnonymized)
        {
            DataTable responseTable;
            if ((principal is AnonymousRespondent) && ((AnonymousRespondent) principal).Guid.HasValue)
            {
                responseTable = GetResponseTable(((AnonymousRespondent) principal).Guid.Value, responseTemplateID);
            }
            else if (isAnonymized)
            {
                responseTable = GetAnonymizedResponseTable(principal.Identity.Name, responseTemplateID);
            }
            else
            {
                responseTable = GetResponseTable(principal.Identity.Name, responseTemplateID);
            }
            if (responseTable == null)
            {
                return null;
            }
            DataRow[] rowArray = responseTable.Select("IsComplete IS NULL OR IsComplete = 0", "Started DESC", DataViewRowState.CurrentRows);
            if (rowArray.Length == 0)
            {
                return null;
            }
            ResponseState state = new ResponseState();
            state.Load((Guid) rowArray[0]["Guid"]);
            return state;
        }

        public static ResponseState GetResponseState(Guid responseGuid)
        {
            ResponseState state = new ResponseState();
            state.Load(responseGuid);
            return state;
        }

        private static DataTable GetResponseTable(Guid anonymousUserGuid, int responseTemplateID)
        {
            DataSet dataSet = new DataSet();
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Response_GetListForAnonymousUser");
            storedProcCommandWrapper.AddInParameter("RespondentGuid", DbType.Guid, anonymousUserGuid);
            storedProcCommandWrapper.AddInParameter("ResponseTemplateID", DbType.Int32, responseTemplateID);
            database.LoadDataSet(storedProcCommandWrapper, dataSet, "ResponseList");
            return dataSet.Tables["ResponseList"];
        }

        private static DataTable GetResponseTable(string uniqueIdentifier, int responseTemplateID)
        {
            DataSet dataSet = new DataSet();
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Response_GetListForUser");
            storedProcCommandWrapper.AddInParameter("UniqueIdentifier", DbType.String, uniqueIdentifier);
            storedProcCommandWrapper.AddInParameter("ResponseTemplateID", DbType.Int32, responseTemplateID);
            database.LoadDataSet(storedProcCommandWrapper, dataSet, "ResponseList");
            return dataSet.Tables["ResponseList"];
        }
    }
}

