﻿namespace Checkbox.Analytics.Items
{
    using Checkbox.Analytics.Computation;
    using Checkbox.Analytics.Data;
    using Checkbox.Analytics.Items.Configuration;
    using Checkbox.Common;
    using Checkbox.Forms.Data;
    using Checkbox.Forms.Items.Configuration;
    using Checkbox.Globalization.Text;
    using Prezza.Framework.Data;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class DetailsItem : AnalysisItem
    {
        protected virtual DataTable AggregateAnswers(bool isPreview)
        {
            DetailsItemAnswerAggregator aggregator = new DetailsItemAnswerAggregator();
            long answerId = 0x3e8L;
            foreach (int num2 in this.SourceItemIDs)
            {
                aggregator.AddItem(num2, this.GetItemText(num2), this.GetSourceItemTypeName(num2));
                List<int> list = isPreview ? this.GetItemOptionIdsForPreview(num2) : this.GetItemOptionIdsForReport(num2);
                foreach (int num3 in list)
                {
                    aggregator.AddItemOption(num2, num3, this.GetOptionText(num2, num3));
                }
                List<ItemAnswer> list2 = isPreview ? this.GetItemPreviewAnswers(num2, new long?(answerId), 0x3e8L) : base.GetItemAnswers(num2);
                foreach (ItemAnswer answer in list2)
                {
                    string optionText;
                    if (answer.AnswerId > answerId)
                    {
                        answerId = answer.AnswerId;
                    }
                    if (answer.OptionId.HasValue && !answer.IsOther)
                    {
                        optionText = this.GetOptionText(num2, answer.OptionId.Value);
                    }
                    else
                    {
                        optionText = answer.AnswerText;
                    }
                    if (Utilities.IsNotNullOrEmpty(optionText))
                    {
                        aggregator.AddAnswer(answer.AnswerId, answer.ResponseId, answer.ResponseGuid, num2, answer.OptionId, optionText);
                    }
                    answerId += 1L;
                }
                base.ResponseCounts[num2] = aggregator.GetResponseCount(new int?(num2));
            }
            return aggregator.GetAggregatedAnswerData();
        }

        public override void Configure(ItemData itemData, string languageCode)
        {
            base.Configure(itemData, languageCode);
            this.GroupAnswers = ((DetailsItemData) itemData).GroupAnswers;
            this.LinkToResponseDetails = ((DetailsItemData) itemData).LinkToResponseDetails;
        }

        protected override object GeneratePreviewData()
        {
            return this.ProcessData(true);
        }

        protected override object[] GetAdditionalRsParams()
        {
            return new object[] { this.GroupAnswers };
        }

        protected override List<ItemAnswer> GetItemPreviewAnswers(int itemID, long? answerIdSeed, long? responseIdSeed)
        {
            List<ItemAnswer> list = new List<ItemAnswer>();
            List<int> itemOptionIdsForPreview = this.GetItemOptionIdsForPreview(itemID);
            long? nullable = answerIdSeed;
            long num = nullable.HasValue ? nullable.GetValueOrDefault() : 0x3e8L;
            long? nullable2 = responseIdSeed;
            long num2 = nullable2.HasValue ? nullable2.GetValueOrDefault() : 0x3e8L;
            if (itemOptionIdsForPreview.Count > 0)
            {
                int num3 = new Random().Next(0, itemOptionIdsForPreview.Count - 1);
                LightweightOptionMetaData data = SurveyMetaDataProxy.GetOptionData(itemOptionIdsForPreview[num3], new int?(itemID), this.TemplatesValidated);
                if (data != null)
                {
                    string str = string.Empty;
                    if (data.IsOther)
                    {
                        str = TextManager.GetText("/controlText/analysisItem/other", base.LanguageCode, "Other", new string[0]) + " 1";
                    }
                    ItemAnswer answer = new ItemAnswer {
                        AnswerId = num,
                        ResponseId = num2,
                        ItemId = itemID,
                        OptionId = new int?(itemOptionIdsForPreview[num3]),
                        IsOther = data.IsOther,
                        AnswerText = str
                    };
                    list.Add(answer);
                }
                return list;
            }
            ItemAnswer item = new ItemAnswer {
                AnswerId = num,
                ResponseId = num2,
                ItemId = itemID,
                AnswerText = TextManager.GetText("/controlText/analysisItem/sample1", base.LanguageCode, "Sample 1", new string[0])
            };
            list.Add(item);
            return list;
        }

        protected virtual DataTable GroupAggregatedAnswers(DataTable aggregatedAnswers)
        {
            DataTable table = new DataTable();
            table.Columns.Add("ResponseID", typeof(long));
            table.Columns.Add("ResponseGuid", typeof(Guid));
            ReadOnlyCollection<int> sourceItemIDs = this.SourceItemIDs;
            foreach (int num in sourceItemIDs)
            {
                if (!table.Columns.Contains(num.ToString()))
                {
                    table.Columns.Add(num.ToString(), typeof(string));
                }
            }
            foreach (long num2 in DbUtility.ListDataColumnValues<long>(aggregatedAnswers, "ResponseID", null, "ResponseID ASC", true))
            {
                DataRow row = table.NewRow();
                row["ResponseID"] = num2;
                foreach (int num3 in sourceItemIDs)
                {
                    string filterExpression = string.Concat(new object[] { "ResponseID = ", num2, " AND ItemID = ", num3 });
                    DataRow[] rowArray = aggregatedAnswers.Select(filterExpression, null, DataViewRowState.CurrentRows);
                    if (rowArray.Length > 0)
                    {
                        row[num3.ToString()] = rowArray[0]["AnswerText"];
                        Guid? defaultValue = null;
                        Guid? nullable = DbUtility.GetValueFromDataRow<Guid?>(rowArray[0], "ResponseGuid", defaultValue);
                        if (nullable.HasValue)
                        {
                            row["ResponseGuid"] = nullable.Value;
                        }
                    }
                    else
                    {
                        row[num3.ToString()] = string.Empty;
                    }
                }
                table.Rows.Add(row);
            }
            return table;
        }

        protected override object ProcessData()
        {
            return this.ProcessData(false);
        }

        protected object ProcessData(bool isPreview)
        {
            DataTable aggregatedAnswers = this.AggregateAnswers(isPreview);
            DataTable table = this.GroupAggregatedAnswers(aggregatedAnswers);
            aggregatedAnswers.TableName = AggregatedAnswersTableName;
            table.TableName = GroupedAnswersTableName;
            DataTable table3 = new DataTable {
                TableName = "ResponseCounts"
            };
            table3.Columns.Add("ItemId", typeof(int));
            table3.Columns.Add("ResponseCount", typeof(int));
            table3.Columns.Add("AnswerCount", typeof(int));
            foreach (int num in this.SourceItemIDs)
            {
                if (base.ResponseCounts.ContainsKey(num))
                {
                    table3.Rows.Add(new object[] { num, base.ResponseCounts[num], base.ResponseCounts[num] });
                }
            }
            table3.AcceptChanges();
            DataSet set = new DataSet();
            set.Merge(aggregatedAnswers);
            set.Merge(table);
            set.Merge(table3);
            base.DataProcessed = true;
            return set;
        }

        public static string AggregatedAnswersTableName
        {
            get
            {
                return "AggregatedAnswers";
            }
        }

        public bool GroupAnswers { get; private set; }

        public static string GroupedAnswersTableName
        {
            get
            {
                return "GroupedAnswers";
            }
        }

        public bool LinkToResponseDetails { get; private set; }
    }
}

