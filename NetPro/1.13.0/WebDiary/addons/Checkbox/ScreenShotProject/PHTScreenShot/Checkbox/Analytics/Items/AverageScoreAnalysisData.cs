﻿namespace Checkbox.Analytics.Items
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Runtime.InteropServices;

    [Serializable]
    public class AverageScoreAnalysisData
    {
        private readonly Dictionary<int, string> _questions = new Dictionary<int, string>();
        private readonly Dictionary<int, int> _responseCounts = new Dictionary<int, int>();
        private readonly Dictionary<int, Dictionary<int, int>> _scores = new Dictionary<int, Dictionary<int, int>>();

        internal AverageScoreAnalysisData()
        {
        }

        internal void AddScore(int itemID, string questionText, int score)
        {
            this._questions[itemID] = questionText;
            if (!this._scores.ContainsKey(itemID))
            {
                this._scores[itemID] = new Dictionary<int, int>();
            }
            if (!this._scores[itemID].ContainsKey(score))
            {
                this._scores[itemID][score] = 1;
            }
            else
            {
                Dictionary<int, int> dictionary;
                int num;
                (dictionary = this._scores[itemID])[num = score] = dictionary[num] + 1;
            }
            if (!this._responseCounts.ContainsKey(itemID))
            {
                this._responseCounts[itemID] = 1;
            }
            else
            {
                Dictionary<int, int> dictionary2;
                int num2;
                (dictionary2 = this._responseCounts)[num2 = itemID] = dictionary2[num2] + 1;
            }
        }

        public void GetItemScoreSummary(int itemID, out double total, out double average, out double responseCount)
        {
            total = 0.0;
            average = 0.0;
            responseCount = 0.0;
            if (this._scores.ContainsKey(itemID))
            {
                foreach (int num in this._scores[itemID].Keys)
                {
                    total += Convert.ToDouble((int) (num * this._scores[itemID][num]));
                    responseCount += Convert.ToDouble(this._scores[itemID][num]);
                }
                average = total / responseCount;
            }
        }

        public double AggregateAverage
        {
            get
            {
                double num = 0.0;
                double num2 = 0.0;
                foreach (int num3 in this._scores.Keys)
                {
                    foreach (int num4 in this._scores[num3].Keys)
                    {
                        num += Convert.ToDouble((int) (num4 * this._scores[num3][num4]));
                        num2 += Convert.ToDouble(this._scores[num3][num4]);
                    }
                }
                return (num / num2);
            }
        }

        public DataTable AllItemScoresTable
        {
            get
            {
                DataTable table = new DataTable();
                table.Columns.Add("ItemID", typeof(int));
                table.Columns.Add("QuestionText", typeof(string));
                table.Columns.Add("Score", typeof(int));
                foreach (int num in this._scores.Keys)
                {
                    foreach (int num2 in this._scores[num].Keys)
                    {
                        for (int i = 0; i < this._scores[num][num2]; i++)
                        {
                            DataRow row = table.NewRow();
                            row["ItemID"] = num;
                            row["QuestionText"] = this._questions.ContainsKey(num) ? this._questions[num] : num.ToString();
                            row["Score"] = num2;
                            row["ResponseCount"] = this._scores[num][num2];
                            table.Rows.Add(row);
                        }
                    }
                }
                return table;
            }
        }

        public double AverageOfItemTotals
        {
            get
            {
                double num = 0.0;
                double num2 = 0.0;
                foreach (int num3 in this._scores.Keys)
                {
                    double num4;
                    double num5;
                    double num6;
                    this.GetItemScoreSummary(num3, out num4, out num6, out num5);
                    num += num4;
                    num2 += num5;
                }
                return (num / num2);
            }
        }

        public DataTable GroupedItemScoresTable
        {
            get
            {
                DataTable table = new DataTable();
                table.Columns.Add("ItemID", typeof(int));
                table.Columns.Add("QuestionText", typeof(string));
                table.Columns.Add("Score", typeof(int));
                table.Columns.Add("ResponseCount", typeof(int));
                foreach (int num in this._scores.Keys)
                {
                    foreach (int num2 in this._scores[num].Keys)
                    {
                        DataRow row = table.NewRow();
                        row["ItemID"] = num;
                        row["QuestionText"] = this._questions.ContainsKey(num) ? this._questions[num] : num.ToString();
                        row["Score"] = num2;
                        row["ResponseCount"] = this._scores[num][num2];
                        table.Rows.Add(row);
                    }
                }
                return table;
            }
        }

        public DataTable ItemAveragesTable
        {
            get
            {
                DataTable table = new DataTable();
                table.Columns.Add("ItemID", typeof(int));
                table.Columns.Add("QuestionText", typeof(string));
                table.Columns.Add("ResponseCount", typeof(int));
                table.Columns.Add("ItemTotal", typeof(int));
                table.Columns.Add("ItemAverage", typeof(double));
                foreach (int num in this._scores.Keys)
                {
                    double num2;
                    double num3;
                    double num4;
                    this.GetItemScoreSummary(num, out num2, out num3, out num4);
                    DataRow row = table.NewRow();
                    row["ItemID"] = num;
                    row["QuestionText"] = this._questions.ContainsKey(num) ? this._questions[num] : num.ToString();
                    row["ResponseCount"] = Convert.ToInt32(num4);
                    row["ItemTotal"] = Convert.ToInt32(num2);
                    row["ItemAverage"] = num3;
                    table.Rows.Add(row);
                }
                return table;
            }
        }
    }
}

