﻿namespace Checkbox.Security
{
    using Checkbox.Common;
    using Checkbox.Security.Data;
    using Checkbox.Users;
    using Prezza.Framework.Common;
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using Prezza.Framework.Security;
    using Prezza.Framework.Security.Principal;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;

    public static class AccessManager
    {
        public static void AddEveryoneGroupWithPermissions(IAccessControllable accessControllable, ExtendedPrincipal principal, params string[] permissions)
        {
            if ((permissions != null) && (permissions.Length > 0))
            {
                List<string> list;
                EveryoneGroup everyone = Group.Everyone;
                Policy policy = accessControllable.ACL.GetPolicy(everyone);
                if (policy != null)
                {
                    list = new List<string>(policy.Permissions);
                    foreach (string str in permissions)
                    {
                        if (!list.Contains(str))
                        {
                            list.Add(str);
                        }
                    }
                }
                else
                {
                    list = new List<string>(permissions);
                }
                SecurityEditor editor = accessControllable.GetEditor();
                editor.Initialize(principal);
                editor.ReplaceAccess(everyone, list.ToArray());
                editor.SaveAcl();
            }
        }

        public static void AddPermissionsToAllAclEntries(IAccessControllable accessControllable, ExtendedPrincipal principal, params string[] permissions)
        {
            if ((permissions != null) && (permissions.Length > 0))
            {
                SecurityEditor editor = accessControllable.GetEditor();
                editor.Initialize(principal);
                List<IAccessControlEntry> list = editor.List(new string[0]);
                if (list != null)
                {
                    foreach (IAccessControlEntry entry in list)
                    {
                        string aclEntryTypeIdentifier = entry.AclEntryTypeIdentifier;
                        string aclEntryIdentifier = entry.AclEntryIdentifier;
                        IAccessPermissible permissibleEntity = null;
                        if (string.Compare(aclEntryTypeIdentifier, "Checkbox.Users.Group", true) == 0)
                        {
                            permissibleEntity = Group.GetGroup(Convert.ToInt32(aclEntryIdentifier));
                        }
                        else if (string.Compare(aclEntryTypeIdentifier, "Prezza.Framework.Security.ExtendedPrincipal", true) == 0)
                        {
                            permissibleEntity = (ExtendedPrincipal) UserManager.GetUser(aclEntryIdentifier);
                        }
                        if (permissibleEntity != null)
                        {
                            List<string> list2;
                            Policy policy = accessControllable.ACL.GetPolicy(permissibleEntity);
                            if (policy != null)
                            {
                                list2 = new List<string>(policy.Permissions);
                                foreach (string str3 in permissions)
                                {
                                    if (!list2.Contains(str3))
                                    {
                                        list2.Add(str3);
                                    }
                                }
                            }
                            else
                            {
                                list2 = new List<string>(permissions);
                            }
                            editor.ReplaceAccess(permissibleEntity, list2.ToArray());
                        }
                    }
                    editor.SaveAcl();
                }
            }
        }

        public static void AddPermissionsToDefaultPolicy(IAccessControllable accessControllable, ExtendedPrincipal principal, params string[] permissions)
        {
            if ((permissions != null) && (permissions.Length > 0))
            {
                List<string> list = (accessControllable.DefaultPolicy != null) ? new List<string>(accessControllable.DefaultPolicy.Permissions) : new List<string>();
                foreach (string str in permissions)
                {
                    if (!list.Contains(str))
                    {
                        list.Add(str);
                    }
                }
                Policy p = accessControllable.CreatePolicy(list.ToArray());
                SecurityEditor editor = accessControllable.GetEditor();
                editor.Initialize(principal);
                editor.SetDefaultPolicy(p);
            }
        }

        public static void AddPolicyPermissions(Policy policy, Database db, DBCommandWrapper command, IDbTransaction transaction)
        {
            if (policy.Permissions != null)
            {
                foreach (string str in policy.Permissions)
                {
                    DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_ACL_AddPolicyPermission");
                    storedProcCommandWrapper.AddInParameter("PolicyID", DbType.Int32, (int) command.GetParameterValue("PolicyID"));
                    storedProcCommandWrapper.AddInParameter("PermissionName", DbType.String, str);
                    db.ExecuteNonQuery(storedProcCommandWrapper, transaction);
                }
            }
        }

        public static int CreatePolicy(Policy p)
        {
            ArgumentValidation.CheckForNullReference(p, "p");
            try
            {
                Database db = DatabaseFactory.CreateDatabase();
                using (IDbConnection connection = db.GetConnection())
                {
                    connection.Open();
                    IDbTransaction transaction = connection.BeginTransaction();
                    try
                    {
                        DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_ACL_CreatePolicy");
                        storedProcCommandWrapper.AddInParameter("PolicyType", DbType.String, p.GetType().ToString());
                        storedProcCommandWrapper.AddInParameter("PolicyAssemblyName", DbType.String, p.GetType().Assembly.GetName().Name);
                        storedProcCommandWrapper.AddOutParameter("PolicyID", DbType.Int32, 4);
                        db.ExecuteNonQuery(storedProcCommandWrapper, transaction);
                        AddPolicyPermissions(p, db, storedProcCommandWrapper, transaction);
                        transaction.Commit();
                        return (int) storedProcCommandWrapper.GetParameterValue("PolicyID");
                    }
                    catch (Exception exception)
                    {
                        ExceptionPolicy.HandleException(exception, "BusinessProtected");
                        transaction.Rollback();
                        throw new Exception("Unable to save Policy.");
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            catch (Exception exception2)
            {
                if (ExceptionPolicy.HandleException(exception2, "BusinessPublic"))
                {
                    throw;
                }
            }
            return -1;
        }

        public static string GetPermissionMaskDisplayName(string maskName)
        {
            string str = maskName;
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(SelectMaskDisplayNameQuery.GetQuery(maskName).ToString());
            IDataReader reader = database.ExecuteReader(sqlStringCommandWrapper);
            try
            {
                if (reader.Read() && (reader["MaskDisplayName"] != DBNull.Value))
                {
                    str = (string) reader["MaskDisplayName"];
                }
            }
            catch
            {
            }
            finally
            {
                if (reader != null)
                {
                    reader.Dispose();
                }
            }
            return str;
        }

        public static List<string> GetPermissionMaskPermissions(string maskName)
        {
            SelectQuery query = SelectMaskPermissions.GetQuery(maskName);
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
            DataSet set = database.ExecuteDataSet(sqlStringCommandWrapper);
            if (set.Tables.Count > 0)
            {
                return DbUtility.ListDataColumnValues<string>(set.Tables[0], "PermissionName", null, null, true);
            }
            return new List<string>();
        }

        internal static ArrayList GetRoles()
        {
            ArrayList list = new ArrayList();
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Role_GetAll");
            DataSet set = database.ExecuteDataSet(storedProcCommandWrapper);
            DataTable table = set.Tables[0];
            DataTable table2 = set.Tables[1];
            foreach (DataRow row in table.Rows)
            {
                int num = (int) row["RoleID"];
                DataRow[] rowArray = table2.Select("RoleID = '" + num + "'");
                string[] permissions = new string[rowArray.Length];
                for (int i = 0; i < permissions.Length; i++)
                {
                    permissions[i] = rowArray[i]["PermissionName"].ToString();
                }
                Role role = new Role(permissions) {
                    Id = num,
                    Name = row["RoleName"].ToString()
                };
                list.Add(role);
            }
            return list;
        }

        public static void RemovePermissionsFromAllAclEntries(IAccessControllable accessControllable, ExtendedPrincipal principal, string entryIdToIgnore, params string[] permissions)
        {
            if ((permissions != null) && (permissions.Length > 0))
            {
                ExtendedPrincipal currentPrincipal = (ExtendedPrincipal) UserManager.GetCurrentPrincipal();
                if (currentPrincipal != null)
                {
                    SecurityEditor editor = accessControllable.GetEditor();
                    editor.Initialize(principal);
                    List<IAccessControlEntry> list = editor.List(new string[0]);
                    if (list != null)
                    {
                        foreach (IAccessControlEntry entry in list)
                        {
                            string aclEntryTypeIdentifier = entry.AclEntryTypeIdentifier;
                            string aclEntryIdentifier = entry.AclEntryIdentifier;
                            IAccessPermissible permissibleEntity = null;
                            if (string.Compare(aclEntryTypeIdentifier, "Checkbox.Users.Group", true) == 0)
                            {
                                permissibleEntity = Group.GetGroup(Convert.ToInt32(aclEntryIdentifier));
                            }
                            else if ((aclEntryTypeIdentifier.Equals("Prezza.Framework.Security.ExtendedPrincipal", StringComparison.InvariantCultureIgnoreCase) && Utilities.IsNotNullOrEmpty(entryIdToIgnore)) && (!aclEntryIdentifier.Equals(entryIdToIgnore, StringComparison.InvariantCultureIgnoreCase) && !aclEntryIdentifier.Equals(currentPrincipal.Identity.Name, StringComparison.InvariantCultureIgnoreCase)))
                            {
                                permissibleEntity = (ExtendedPrincipal) UserManager.GetUser(aclEntryIdentifier);
                            }
                            if (permissibleEntity != null)
                            {
                                Policy policy = accessControllable.ACL.GetPolicy(permissibleEntity);
                                if ((policy != null) && (policy.Permissions.Count > 0))
                                {
                                    List<string> list2 = new List<string>(policy.Permissions);
                                    foreach (string str3 in permissions)
                                    {
                                        if (list2.Contains(str3))
                                        {
                                            list2.Remove(str3);
                                        }
                                    }
                                    editor.ReplaceAccess(permissibleEntity, list2.ToArray());
                                }
                            }
                        }
                        editor.SaveAcl();
                    }
                }
            }
        }

        public static void RemovePermissionsFromDefaultPolicy(IAccessControllable accessControllable, ExtendedPrincipal principal, params string[] permissions)
        {
            if (((permissions != null) && (permissions.Length > 0)) && ((accessControllable.DefaultPolicy != null) && (accessControllable.DefaultPolicy.Permissions.Count > 0)))
            {
                List<string> list = new List<string>(accessControllable.DefaultPolicy.Permissions);
                foreach (string str in permissions)
                {
                    if (list.Contains(str))
                    {
                        list.Remove(str);
                    }
                }
                Policy p = accessControllable.CreatePolicy(list.ToArray());
                SecurityEditor editor = accessControllable.GetEditor();
                editor.Initialize(principal);
                editor.SetDefaultPolicy(p);
            }
        }

        public static void UpdatePolicy(int policyID, Policy p)
        {
            ArgumentValidation.CheckForNullReference(p, "p");
            try
            {
                Database db = DatabaseFactory.CreateDatabase();
                using (IDbConnection connection = db.GetConnection())
                {
                    connection.Open();
                    IDbTransaction transaction = connection.BeginTransaction();
                    try
                    {
                        DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_ACL_DelPolicyPermissions");
                        storedProcCommandWrapper.AddInParameter("PolicyID", DbType.Int32, policyID);
                        db.ExecuteNonQuery(storedProcCommandWrapper, transaction);
                        AddPolicyPermissions(p, db, storedProcCommandWrapper, transaction);
                        transaction.Commit();
                    }
                    catch (Exception exception)
                    {
                        ExceptionPolicy.HandleException(exception, "BusinessProtected");
                        transaction.Rollback();
                        throw new Exception("Unable to save Policy.");
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            catch (Exception exception2)
            {
                if (ExceptionPolicy.HandleException(exception2, "BusinessPublic"))
                {
                    throw;
                }
            }
        }
    }
}

