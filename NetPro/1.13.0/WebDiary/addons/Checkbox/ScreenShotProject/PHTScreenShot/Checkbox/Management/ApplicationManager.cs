﻿namespace Checkbox.Management
{
    using Prezza.Framework.Data;
    using System;
    using System.Collections;
    using System.Configuration;
    using System.Data;
    using System.Xml;

    public static class ApplicationManager
    {
        private static readonly Checkbox.Management.AppSettings _appSettings;
        private static bool? _cacheAppSettings;
        private static IDataContextProvider _dataContextProvider;
        private static readonly Hashtable _settingsCache;
        private static bool _useSimpleSecurity;

        static ApplicationManager()
        {
            lock (typeof(ApplicationManager))
            {
                _settingsCache = new Hashtable();
                _appSettings = new Checkbox.Management.AppSettings();
            }
        }

        public static void AddNewAppSetting(string key, string property)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Setting_Insert");
            storedProcCommandWrapper.AddInParameter("SettingName", DbType.String, key);
            storedProcCommandWrapper.AddInParameter("SettingValue", DbType.String, property);
            database.ExecuteNonQuery(storedProcCommandWrapper);
            if (CacheAppSettings && !string.IsNullOrEmpty(key))
            {
                lock (_settingsCache.SyncRoot)
                {
                    _settingsCache[GetSettingsCacheKey(key)] = property;
                }
            }
        }

        [Obsolete("Do not use")]
        public static void AddNewAppSetting(string key, string property, string path)
        {
        }

        internal static string GetAppSetting(string key)
        {
            if (!string.IsNullOrEmpty(key))
            {
                if (CacheAppSettings && _settingsCache.Contains(GetSettingsCacheKey(key)))
                {
                    object obj2 = _settingsCache[GetSettingsCacheKey(key)];
                    if ((obj2 != null) && (obj2 != DBNull.Value))
                    {
                        return obj2.ToString();
                    }
                    return null;
                }
                Database database = DatabaseFactory.CreateDatabase();
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Setting_Get");
                storedProcCommandWrapper.AddInParameter("SettingName", DbType.String, key);
                object obj3 = database.ExecuteScalar(storedProcCommandWrapper);
                if (CacheAppSettings)
                {
                    lock (_settingsCache.SyncRoot)
                    {
                        _settingsCache[GetSettingsCacheKey(key)] = obj3;
                    }
                }
                if ((obj3 != null) && (obj3 != DBNull.Value))
                {
                    return obj3.ToString();
                }
            }
            return null;
        }

        private static string GetSettingsCacheKey(string settingName)
        {
            return (ApplicationDataContext + "_" + settingName).ToLower();
        }

        public static void SetApplicationDataContextProvider(IDataContextProvider dataContextProvider)
        {
            _dataContextProvider = dataContextProvider;
        }

        internal static void UpdateAppSetting(string key, string property)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Setting_Update");
            storedProcCommandWrapper.AddInParameter("SettingName", DbType.String, key);
            storedProcCommandWrapper.AddInParameter("SettingValue", DbType.String, property);
            database.ExecuteNonQuery(storedProcCommandWrapper);
            if (CacheAppSettings && !string.IsNullOrEmpty(key))
            {
                lock (_settingsCache.SyncRoot)
                {
                    _settingsCache[GetSettingsCacheKey(key)] = property;
                }
            }
        }

        public static void UpdateAppSetting(string key, string property, string path)
        {
            XmlDocument document = new XmlDocument();
            document.Load(path);
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(document.NameTable);
            nsmgr.AddNamespace("ms", "http://schemas.microsoft.com/.NetConfiguration/v2.0");
            ((XmlElement) document.SelectSingleNode("//ms:configuration/ms:appSettings", nsmgr).SelectSingleNode("//ms:add[@key='" + key + "']", nsmgr)).SetAttribute("value", property);
            document.Save(path);
        }

        public static string ApplicationDataContext
        {
            get
            {
                if (_dataContextProvider == null)
                {
                    return string.Empty;
                }
                return (_dataContextProvider.ApplicationContext ?? string.Empty);
            }
        }

        public static string ApplicationPath
        {
            get
            {
                return (ApplicationURL + ApplicationRoot);
            }
        }

        public static string ApplicationRoot
        {
            get
            {
                return ConfigurationManager.AppSettings["ApplicationRoot"];
            }
        }

        public static string ApplicationURL
        {
            get
            {
                if (_dataContextProvider == null)
                {
                    return ConfigurationManager.AppSettings["ApplicationURL"];
                }
                if (_dataContextProvider.Secured)
                {
                    return string.Format("https://{0}", _dataContextProvider.ApplicationContext);
                }
                return string.Format("http://{0}", _dataContextProvider.ApplicationContext);
            }
        }

        public static Checkbox.Management.AppSettings AppSettings
        {
            get
            {
                return _appSettings;
            }
        }

        public static bool CacheAppSettings
        {
            get
            {
                if (!_cacheAppSettings.HasValue)
                {
                    _cacheAppSettings = new bool?(AppSettings.CacheAppSettings);
                }
                return _cacheAppSettings.Value;
            }
        }

        public static bool IsDataContextActive
        {
            get
            {
                if (!AppSettings.EnableMultiDatabase || (_dataContextProvider == null))
                {
                    return true;
                }
                try
                {
                    Database database = DatabaseFactory.CreateDatabase("MultiMasterDB");
                    DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_GetApplicationContext");
                    storedProcCommandWrapper.AddInParameter("ContextName", DbType.String, _dataContextProvider.ApplicationContext);
                    using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
                    {
                        try
                        {
                            if (reader.Read())
                            {
                                return DbUtility.GetValueFromDataReader<bool>(reader, "Active", false);
                            }
                        }
                        finally
                        {
                            reader.Close();
                        }
                    }
                }
                catch
                {
                }
                return false;
            }
        }

        public static bool UseSimpleSecurity
        {
            get
            {
                if (!_useSimpleSecurity)
                {
                    return AppSettings.EnableSimpleSecurity;
                }
                return true;
            }
            set
            {
                _useSimpleSecurity = value;
            }
        }
    }
}

