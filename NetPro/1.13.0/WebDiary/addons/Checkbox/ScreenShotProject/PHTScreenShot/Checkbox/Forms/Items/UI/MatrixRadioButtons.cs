﻿namespace Checkbox.Forms.Items.UI
{
    using System;

    [Serializable]
    public class MatrixRadioButtons : MatrixSelectLayout
    {
        public override string AppearanceCode
        {
            get
            {
                return "MATRIX_RADIO_BUTTONS";
            }
        }
    }
}

