﻿namespace Checkbox.Forms.Items.UI
{
    using Prezza.Framework.Data;
    using System;
    using System.Data;

    [Serializable]
    public abstract class SelectLayout : LabelledItemAppearanceData
    {
        protected SelectLayout()
        {
        }

        public virtual void CopyTo(SelectLayout appearanceCopy)
        {
            appearanceCopy.LayoutDirection = this.LayoutDirection;
            appearanceCopy.Columns = this.Columns;
            appearanceCopy.ShowNumberLabels = this.ShowNumberLabels;
            appearanceCopy.ItemPosition = base.ItemPosition;
            appearanceCopy.LabelPosition = this.LabelPosition;
            appearanceCopy.Width = this.Width;
            appearanceCopy.GridLines = this.GridLines;
        }

        protected override void Create(IDbTransaction transaction)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Appearance_CreateSelect");
            storedProcCommandWrapper.AddInParameter("AppearanceCode", DbType.String, this.AppearanceCode);
            storedProcCommandWrapper.AddInParameter("LayoutStyle", DbType.String, this.LayoutDirection);
            storedProcCommandWrapper.AddInParameter("Columns", DbType.Int32, this.Columns);
            storedProcCommandWrapper.AddInParameter("ShowNumberLabels", DbType.Int32, this.ShowNumberLabels ? 1 : 0);
            storedProcCommandWrapper.AddInParameter("ItemPosition", DbType.String, base.ItemPosition);
            storedProcCommandWrapper.AddInParameter("LabelPosition", DbType.String, this.LabelPosition.ToString());
            storedProcCommandWrapper.AddOutParameter("AppearanceID", DbType.Int32, 4);
            database.ExecuteNonQuery(storedProcCommandWrapper, transaction);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("AppearanceID");
            if ((parameterValue == null) || (parameterValue == DBNull.Value))
            {
                throw new Exception("Unable to save appearance data.");
            }
            base.ID = new int?((int) parameterValue);
        }

        protected override void Update(IDbTransaction transaction)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Appearance_UpdateSelect");
            storedProcCommandWrapper.AddInParameter("AppearanceID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("LayoutStyle", DbType.String, this.LayoutDirection);
            storedProcCommandWrapper.AddInParameter("Columns", DbType.Int32, this.Columns);
            storedProcCommandWrapper.AddInParameter("ShowNumberLabels", DbType.Int32, this.ShowNumberLabels ? 1 : 0);
            storedProcCommandWrapper.AddInParameter("ItemPosition", DbType.String, base.ItemPosition);
            storedProcCommandWrapper.AddInParameter("LabelPosition", DbType.String, this.LabelPosition.ToString());
            database.ExecuteNonQuery(storedProcCommandWrapper);
        }

        public virtual Layout LayoutDirection
        {
            get
            {
                return this.Layout;
            }
            set
            {
                this.Layout = value;
            }
        }
    }
}

