﻿namespace Checkbox.Analytics.Filters
{
    using System;

    [Serializable]
    public class NotAnsweredFilter : AnsweredFilter
    {
        public override bool UseNotIn
        {
            get
            {
                return true;
            }
        }

        protected override bool ValueRequired
        {
            get
            {
                return false;
            }
        }
    }
}

