﻿namespace Checkbox.Forms.Data
{
    using Checkbox.Common;
    using Checkbox.Forms;
    using Checkbox.Forms.Items.Configuration;
    using Checkbox.Globalization.Text;
    using Prezza.Framework.Caching;
    using Prezza.Framework.Common;
    using System;
    using System.Collections.Generic;

    public static class SurveyMetaDataProxy
    {
        private static readonly CacheManager _surveyItemCache;
        private static readonly CacheManager _surveyOptionCache;

        static SurveyMetaDataProxy()
        {
            lock (typeof(SurveyMetaDataProxy))
            {
                _surveyItemCache = CacheFactory.GetCacheManager("surveyItemMetaDataCache");
                _surveyOptionCache = CacheFactory.GetCacheManager("surveyItemOptionMetaDataCache");
            }
        }

        public static void AddItemToCache(LightweightItemMetaData lightweightItem)
        {
            _surveyItemCache.Add(GenerateItemCacheKey(lightweightItem.ItemId), lightweightItem);
        }

        private static void AddOptionToCache(LightweightOptionMetaData lightweightOption)
        {
            _surveyOptionCache.Add(GenerateItemOptionCacheKey(lightweightOption.OptionId), lightweightOption);
        }

        private static LightweightItemMetaData CreateLightweightItemMetaDataObject(ItemData itemData)
        {
            LightweightItemMetaData lightweightItemMetaData = new LightweightItemMetaData {
                ItemId = itemData.ID.Value,
                Alias = itemData.Alias,
                AncestorId = 0,
                ItemType = itemData.ItemTypeName,
                LastModified = itemData.LastModified.HasValue ? itemData.LastModified.Value : DateTime.Now,
                IsAnswerable = itemData.ItemIsIAnswerable,
                IsScored = itemData.ItemIsIScored
            };
            if (itemData is SelectItemData)
            {
                lightweightItemMetaData.AllowOther = ((SelectItemData) itemData).AllowOther;
            }
            if (itemData is MatrixItemData)
            {
                PopulateMatrixItemDetails(lightweightItemMetaData, (MatrixItemData) itemData);
            }
            return lightweightItemMetaData;
        }

        private static LightweightOptionMetaData CreateLightweightOptionMetaDataObject(ListOptionData listOptionData)
        {
            return new LightweightOptionMetaData { OptionId = listOptionData.OptionID, IsOther = listOptionData.IsOther, Points = listOptionData.Points, Alias = listOptionData.Alias, Position = listOptionData.Position };
        }

        private static string GenerateItemCacheKey(int itemId)
        {
            return itemId.ToString();
        }

        private static string GenerateItemOptionCacheKey(int optionId)
        {
            return optionId.ToString();
        }

        public static LightweightItemMetaData GetItemData(int itemId, bool skipDateValidation)
        {
            LightweightItemMetaData itemFromCache = GetItemFromCache(itemId, skipDateValidation);
            if (itemFromCache != null)
            {
                return itemFromCache;
            }
            return LoadItemData(ItemConfigurationManager.GetConfigurationData(itemId));
        }

        public static LightweightItemMetaData GetItemData(int itemId, bool skipDateValidation, ResponseTemplate containingTemplate)
        {
            LightweightItemMetaData itemFromCache = GetItemFromCache(itemId, skipDateValidation);
            if (itemFromCache != null)
            {
                return itemFromCache;
            }
            ItemData item = containingTemplate.GetItem(itemId);
            if (item == null)
            {
                return null;
            }
            return LoadItemData(item);
        }

        private static LightweightItemMetaData GetItemFromCache(int itemId, bool skipDateValidation)
        {
            string key = GenerateItemCacheKey(itemId);
            if (_surveyItemCache.Contains(key))
            {
                LightweightItemMetaData data = _surveyItemCache[key] as LightweightItemMetaData;
                if ((data != null) && (skipDateValidation || data.Validate()))
                {
                    return data;
                }
            }
            return null;
        }

        public static string GetItemText(int itemId, string languageCode, bool preferAlias, bool skipDateValidation)
        {
            LightweightItemMetaData itemData = GetItemData(itemId, skipDateValidation);
            if (itemData == null)
            {
                return string.Empty;
            }
            return itemData.GetText(preferAlias, languageCode);
        }

        public static string GetItemTypeName(int itemId, bool skipDateValidation)
        {
            LightweightItemMetaData itemData = GetItemData(itemId, skipDateValidation);
            if (itemData == null)
            {
                return string.Empty;
            }
            return itemData.ItemType;
        }

        public static LightweightOptionMetaData GetOptionData(int optionId, int? itemId, bool skipDateValidation)
        {
            LightweightOptionMetaData data = GetOptionFromCache(optionId, null, skipDateValidation);
            if ((data == null) && itemId.HasValue)
            {
                GetItemData(itemId.Value, skipDateValidation);
                data = GetOptionFromCache(optionId, null, skipDateValidation);
            }
            return data;
        }

        private static LightweightOptionMetaData GetOptionFromCache(int optionId, int? itemId, bool skipDateValidation)
        {
            LightweightItemMetaData itemData = null;
            if (!itemId.HasValue || (GetItemFromCache(itemId.Value, skipDateValidation) != null))
            {
                string key = GenerateItemOptionCacheKey(optionId);
                if (_surveyOptionCache.Contains(key))
                {
                    return (_surveyOptionCache[key] as LightweightOptionMetaData);
                }
                if (itemId.HasValue)
                {
                    RemoveItemFromCache(itemId.Value);
                    itemData = GetItemData(itemId.Value, skipDateValidation);
                    if (_surveyOptionCache.Contains(key))
                    {
                        return (_surveyOptionCache[key] as LightweightOptionMetaData);
                    }
                }
            }
            return null;
        }

        public static bool GetOptionIsOther(int optionId, bool skipDateValidation)
        {
            LightweightOptionMetaData data = GetOptionFromCache(optionId, null, skipDateValidation);
            if (data == null)
            {
                return false;
            }
            return data.IsOther;
        }

        public static double GetOptionPoints(int optionId, bool skipDateValidation)
        {
            LightweightOptionMetaData data = GetOptionData(optionId, null, skipDateValidation);
            if (data == null)
            {
                return 0.0;
            }
            return data.Points;
        }

        public static string GetOptionText(int? itemId, int optionId, string languageCode, bool preferAlias, bool skipDateValidation)
        {
            LightweightOptionMetaData data = GetOptionFromCache(optionId, itemId, skipDateValidation);
            if (data == null)
            {
                return string.Empty;
            }
            if (!data.IsOther)
            {
                return data.GetText(preferAlias, languageCode);
            }
            return TextManager.GetText("/controlText/exportItem/other", languageCode, "Other", new string[0]);
        }

        public static List<int> ListOptionIdsForItem(int itemId, bool skipDateValidation)
        {
            LightweightItemMetaData itemData = GetItemData(itemId, skipDateValidation);
            if (itemData == null)
            {
                return new List<int>();
            }
            return itemData.Options;
        }

        private static void LoadItemChildren(LightweightItemMetaData lightweightParentItemData, ItemData parentItemData)
        {
            if (parentItemData is MatrixItemData)
            {
                LoadMatrixItemChildren((MatrixItemData) parentItemData, lightweightParentItemData);
            }
            else if (parentItemData is ICompositeItemData)
            {
                foreach (ItemData data in ((ICompositeItemData) parentItemData).GetChildItemDatas())
                {
                    lightweightParentItemData.Children.Add(data.ID.Value);
                    LoadItemData(data, true);
                }
            }
        }

        private static LightweightItemMetaData LoadItemData(ItemData itemData)
        {
            if ((itemData == null) || !itemData.ID.HasValue)
            {
                return null;
            }
            LightweightItemMetaData lightweightItem = null;
            int? itemParent = ItemConfigurationManager.GetItemParent(itemData.ID.Value);
            if (itemParent.HasValue)
            {
                LightweightItemMetaData data2 = GetItemData(itemParent.Value, false);
                if ((data2 != null) && !data2.Children.Contains(itemData.ID.Value))
                {
                    data2.Children.Add(itemData.ID.Value);
                    AddItemToCache(data2);
                }
                lightweightItem = GetItemFromCache(itemData.ID.Value, true);
            }
            if (lightweightItem == null)
            {
                lightweightItem = LoadItemData(itemData, true);
            }
            if (lightweightItem != null)
            {
                AddItemToCache(lightweightItem);
            }
            return lightweightItem;
        }

        private static LightweightItemMetaData LoadItemData(ItemData itemData, bool populateText)
        {
            if (itemData == null)
            {
                return null;
            }
            LightweightItemMetaData lightweightItemMetaData = CreateLightweightItemMetaDataObject(itemData);
            if (itemData is SelectItemData)
            {
                foreach (ListOptionData data2 in ((SelectItemData) itemData).Options)
                {
                    LightweightOptionMetaData lightweightOption = CreateLightweightOptionMetaDataObject(data2);
                    lightweightOption.ItemId = itemData.ID.Value;
                    AddOptionToCache(lightweightOption);
                    lightweightItemMetaData.Options.Add(data2.OptionID);
                }
            }
            if (populateText)
            {
                PopulateItemText(lightweightItemMetaData, itemData, ResponseTemplateManager.ActiveSurveyLanguages);
            }
            AddItemToCache(lightweightItemMetaData);
            LoadItemChildren(lightweightItemMetaData, itemData);
            return GetItemFromCache(lightweightItemMetaData.ItemId, true);
        }

        private static void LoadMatrixItemChildren(MatrixItemData matrixItemData, LightweightItemMetaData lightweightMatrixItemData)
        {
            for (int i = 1; i <= matrixItemData.ColumnCount; i++)
            {
                LightweightItemMetaData data = (i != matrixItemData.PrimaryKeyColumnIndex) ? GetItemData(matrixItemData.GetColumnPrototypeID(i), false) : null;
                for (int j = 1; j <= matrixItemData.RowCount; j++)
                {
                    ItemData itemAt = matrixItemData.GetItemAt(j, i);
                    if (itemAt != null)
                    {
                        if (!lightweightMatrixItemData.Children.Contains(itemAt.ID.Value))
                        {
                            lightweightMatrixItemData.Children.Add(itemAt.ID.Value);
                        }
                        LightweightItemMetaData lightweightItem = LoadItemData(itemAt, i == matrixItemData.PrimaryKeyColumnIndex);
                        if (lightweightItem != null)
                        {
                            lightweightItem.Coordinate = new Coordinate(j, i);
                            if (data != null)
                            {
                                if (!string.IsNullOrEmpty(data.Alias))
                                {
                                    lightweightItem.Alias = string.IsNullOrEmpty(lightweightItem.Alias) ? data.Alias : string.Format("{0}_{1}", lightweightItem.Alias, data.Alias);
                                }
                                string[] activeSurveyLanguages = ResponseTemplateManager.ActiveSurveyLanguages;
                                List<LightweightOptionMetaData> list = new List<LightweightOptionMetaData>();
                                List<LightweightOptionMetaData> list2 = new List<LightweightOptionMetaData>();
                                foreach (int num3 in data.Options)
                                {
                                    int? itemId = null;
                                    LightweightOptionMetaData item = GetOptionFromCache(num3, itemId, true);
                                    if (item != null)
                                    {
                                        list.Add(item);
                                    }
                                }
                                foreach (int num4 in lightweightItem.Options)
                                {
                                    int? nullable4 = null;
                                    LightweightOptionMetaData data5 = GetOptionFromCache(num4, nullable4, true);
                                    if (data5 != null)
                                    {
                                        list2.Add(data5);
                                    }
                                }
                                foreach (string str in activeSurveyLanguages)
                                {
                                    lightweightItem.SetText(str, data.GetText(false, str));
                                    for (int k = 0; k < list2.Count; k++)
                                    {
                                        if (k < list.Count)
                                        {
                                            list2[k].SetText(str, list[k].GetText(false, str));
                                        }
                                    }
                                }
                                foreach (LightweightOptionMetaData data6 in list2)
                                {
                                    AddOptionToCache(data6);
                                }
                            }
                            AddItemToCache(lightweightItem);
                        }
                    }
                }
            }
            AddItemToCache(lightweightMatrixItemData);
        }

        private static void PopulateItemText(LightweightItemMetaData lightweightItemMetaData, ItemData itemData, IEnumerable<string> languageCodes)
        {
            foreach (string str in languageCodes)
            {
                ItemTextDecorator decorator = itemData.CreateTextDecorator(str);
                if (decorator is LabelledItemTextDecorator)
                {
                    int? maxLength = null;
                    lightweightItemMetaData.SetText(str, Utilities.StripHtml(((LabelledItemTextDecorator) decorator).Text, maxLength));
                    int? nullable2 = null;
                    lightweightItemMetaData.SetDescription(str, Utilities.StripHtml(((LabelledItemTextDecorator) decorator).SubText, nullable2));
                }
                if (decorator is MessageItemTextDecorator)
                {
                    int? nullable3 = null;
                    lightweightItemMetaData.SetText(str, Utilities.StripHtml(((MessageItemTextDecorator) decorator).Message, nullable3));
                }
                if (decorator is SelectItemTextDecorator)
                {
                    foreach (int num in lightweightItemMetaData.Options)
                    {
                        int? itemId = null;
                        LightweightOptionMetaData lightweightOption = GetOptionFromCache(num, itemId, false);
                        if (lightweightOption != null)
                        {
                            int? nullable5 = null;
                            lightweightOption.SetText(str, Utilities.StripHtml(((SelectItemTextDecorator) decorator).GetOptionText(lightweightOption.Position), nullable5));
                            AddOptionToCache(lightweightOption);
                        }
                    }
                }
            }
        }

        private static void PopulateMatrixItemDetails(LightweightItemMetaData lightweightItemMetaData, MatrixItemData itemData)
        {
            for (int i = 1; i <= itemData.ColumnCount; i++)
            {
                if (i == itemData.PrimaryKeyColumnIndex)
                {
                    lightweightItemMetaData.AddColumn(i, -1, "PK");
                }
                else
                {
                    int columnPrototypeID = itemData.GetColumnPrototypeID(i);
                    if (columnPrototypeID > 0)
                    {
                        LightweightItemMetaData data = GetItemData(columnPrototypeID, false);
                        if (data != null)
                        {
                            lightweightItemMetaData.AddColumn(i, columnPrototypeID, data.ItemType);
                        }
                    }
                }
            }
            for (int j = 1; j <= itemData.RowCount; j++)
            {
                int? itemIDAt = itemData.GetItemIDAt(j, itemData.PrimaryKeyColumnIndex);
                if (itemIDAt.HasValue && (itemIDAt > 0))
                {
                    string type = !itemData.IsRowSubheading(j) ? (itemData.IsRowOther(j) ? "Other" : "Normal") : "Subheading";
                    lightweightItemMetaData.AddRow(j, itemIDAt.Value, type);
                }
            }
            for (int k = 1; k <= itemData.RowCount; k++)
            {
                for (int m = 1; m <= itemData.ColumnCount; m++)
                {
                    int? nullable2 = itemData.GetItemIDAt(k, m);
                    if (nullable2.HasValue)
                    {
                        lightweightItemMetaData.SetChildCoordinate(nullable2.Value, new Coordinate(m, k));
                    }
                }
            }
        }

        public static void RemoveItemFromCache(int itemId)
        {
            string key = GenerateItemCacheKey(itemId);
            if (_surveyItemCache.Contains(key))
            {
                LightweightItemMetaData data = _surveyItemCache[key] as LightweightItemMetaData;
                if (data != null)
                {
                    foreach (int num in data.Options)
                    {
                        string str2 = GenerateItemOptionCacheKey(num);
                        if (_surveyOptionCache.Contains(str2))
                        {
                            _surveyOptionCache.Remove(str2);
                        }
                    }
                }
                _surveyItemCache.Remove(key);
            }
        }
    }
}

