﻿namespace Checkbox.Forms.Logic
{
    using System;

    public enum RuleEventTrigger
    {
        Load = 1,
        UnLoad = 2
    }
}

