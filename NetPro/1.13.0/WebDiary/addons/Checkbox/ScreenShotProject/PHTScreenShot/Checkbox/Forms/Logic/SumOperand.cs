﻿using Checkbox.Forms.Items;
using Checkbox.Common;
using Checkbox.Globalization.Text;
using System;
using System.Globalization;
using System.Collections.Generic;

namespace Checkbox.Forms.Logic
{
    //using Checkbox.Common;
    //using Checkbox.Forms.Items;
    //using System;
    //using System.Collections.Generic;

    public class SumOperand : Operand
    {
        private List<IAnswerable> _answerableItems;

        public SumOperand(List<IAnswerable> answerableItems)
        {
            this._answerableItems = answerableItems;
        }

        public override OperandValue CreateOperandValue()
        {
            return new SumOperandValue();
        }

        protected override object ProtectedGetValue(RuleEventTrigger trigger)
        {
            List<double> list = new List<double>();
            foreach (IAnswerable answerable in this._answerableItems)
            {
                if (answerable.HasAnswer)
                {
                    double? nullable = Utilities.GetDouble(answerable.GetAnswer(), new CultureInfo[0]);
                    if (nullable.HasValue)
                    {
                        list.Add(nullable.Value);
                    }
                }
            }
            return list;
        }
    }
}

