﻿namespace Checkbox.Analytics.Computation
{
    using System;
    using System.Collections.Generic;
    using System.Data;

    public class FrequencyDataCalculator : ReportDataCalculator
    {
        protected override DataTable Calculate(ItemAnswerAggregator answerAggregator)
        {
            DataTable table = base.CreateOutputTable();
            foreach (int num in answerAggregator.GetItemIDs())
            {
                List<int> optionIDs = answerAggregator.GetOptionIDs(num);
                string itemText = answerAggregator.GetItemText(num);
                if (optionIDs.Count > 0)
                {
                    foreach (int num2 in optionIDs)
                    {
                        ItemResult optionResult = this.GetOptionResult(num2, answerAggregator);
                        DataRow row = table.NewRow();
                        row["ItemID"] = num;
                        row["ItemText"] = itemText;
                        row["OptionID"] = num2;
                        row["OptionText"] = answerAggregator.GetOptionText(num2);
                        row["AnswerCount"] = optionResult.Count;
                        row["AnswerPercent"] = optionResult.Percentage;
                        row["AnswerText"] = row["OptionText"];
                        table.Rows.Add(row);
                    }
                }
                else
                {
                    foreach (string str2 in answerAggregator.GetAnswerTexts(num))
                    {
                        ItemResult result2 = this.GetItemResult(num, str2, answerAggregator);
                        DataRow row2 = table.NewRow();
                        row2["ItemID"] = num;
                        row2["ItemText"] = itemText;
                        row2["AnswerText"] = str2;
                        row2["AnswerCount"] = result2.Count;
                        row2["AnswerPercent"] = result2.Percentage;
                        table.Rows.Add(row2);
                    }
                }
            }
            return table;
        }
    }
}

