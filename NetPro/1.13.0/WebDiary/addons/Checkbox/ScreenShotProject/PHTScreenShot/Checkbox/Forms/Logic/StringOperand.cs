﻿namespace Checkbox.Forms.Logic
{
    using System;

    [Serializable]
    public class StringOperand : Operand
    {
        private string _value;

        public StringOperand(string value)
        {
            this._value = value;
        }

        protected override object ProtectedGetValue(RuleEventTrigger trigger)
        {
            return this._value;
        }
    }
}

