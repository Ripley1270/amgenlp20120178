﻿namespace Checkbox.Forms.Items.Configuration
{
    using Checkbox.Forms.Items;
    using Checkbox.Management;
    using Prezza.Framework.Data;
    using System;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class DisplayAnalysisItemData : RedirectItemData
    {
        protected override void Create(IDbTransaction t)
        {
            base.Create(t);
            if (!base.ID.HasValue || (base.ID <= 0))
            {
                throw new Exception("Unable to create display analysis item.  DataID was not specified.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_InsertDA");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("AnalysisGUID", DbType.Guid, this.AnalysisGUID);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        protected override Item CreateItem()
        {
            return new DisplayAnalysisItem();
        }

        protected override DataSet GetConcreteConfigurationDataSet()
        {
            if (!base.ID.HasValue || (base.ID <= 0))
            {
                throw new Exception("Unable to load data for display analysis item.  DataID not specified.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_GetDisplayAnalysis");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            DataSet concreteConfigurationDataSet = base.GetConcreteConfigurationDataSet();
            if (concreteConfigurationDataSet.Tables.Contains(this.DataTableName))
            {
                concreteConfigurationDataSet.Tables.Remove(this.DataTableName);
            }
            database.LoadDataSet(storedProcCommandWrapper, concreteConfigurationDataSet, this.DataTableName);
            return concreteConfigurationDataSet;
        }

        protected override void LoadFromDataRow(DataRow data)
        {
            base.LoadFromDataRow(data);
            if ((data != null) && (data["AnalysisGUID"] != DBNull.Value))
            {
                this.AnalysisGUID = new Guid(data["AnalysisGUID"].ToString());
            }
        }

        protected override void Update(IDbTransaction t)
        {
            base.Update(t);
            if (!base.ID.HasValue || (base.ID <= 0))
            {
                throw new Exception("Unable to update display analysis item.  DataID was not specified.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_UpdateDA");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("AnalysisGUID", DbType.Guid, this.AnalysisGUID);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        public Guid AnalysisGUID { get; set; }

        public override string DataTableName
        {
            get
            {
                return "DisplayAnalysisItemData";
            }
        }

        public override bool RestartSurvey
        {
            get
            {
                return false;
            }
            set
            {
            }
        }

        public override string URL
        {
            get
            {
                return (ApplicationManager.ApplicationRoot + "/RunAnalysis.aspx?a=" + this.AnalysisGUID);
            }
            set
            {
            }
        }
    }
}

