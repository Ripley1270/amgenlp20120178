﻿namespace Checkbox.Forms.Items
{
    using Checkbox.Forms;
    using System;
    using System.Text;

    [Serializable]
    public class EmailResponseItem : EmailItem
    {
        private bool _gettingBodyText;

        protected override string GetBodyText()
        {
            if (this._gettingBodyText)
            {
                return string.Empty;
            }
            this._gettingBodyText = true;
            StringBuilder builder = new StringBuilder();
            builder.Append(base.GetBodyText());
            builder.Append(this.GetPipedText("MessageBody", ResponseFormatter.Format(base.Response, this.MessageFormat)));
            this._gettingBodyText = false;
            return builder.ToString();
        }

        protected string GetdMessageBody(string format)
        {
            if (!this._gettingBodyText)
            {
                return this.GetPipedText("Body", ResponseFormatter.Format(base.Response, format));
            }
            return string.Empty;
        }
    }
}

