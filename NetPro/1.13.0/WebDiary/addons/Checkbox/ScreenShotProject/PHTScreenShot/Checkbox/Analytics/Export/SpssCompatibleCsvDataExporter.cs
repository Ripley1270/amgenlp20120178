﻿namespace Checkbox.Analytics.Export
{
    using Checkbox.Analytics;
    using Checkbox.Analytics.Items;
    using Checkbox.Common;
    using Checkbox.Forms;
    using Checkbox.Forms.Items;
    using System;

    public class SpssCompatibleCsvDataExporter : CsvDataExporter
    {
        protected override AnalysisTemplate CreateAnalysisTemplate()
        {
            return AnalysisTemplateManager.GenerateSPSSExportTemplate(ResponseTemplateManager.GetResponseTemplate(base.ResponseTemplateId), base.ProgressKey, base.LanguageCode, 0, 0x19);
        }

        public string GetActualTextForField(string fieldName)
        {
            string actualText = string.Empty;
            foreach (Item item in base.Analysis.Items)
            {
                if (item is SPSSExportItem)
                {
                    actualText = ((SPSSExportItem) item).GetActualText(fieldName);
                    break;
                }
            }
            if (Utilities.IsNullOrEmpty(actualText))
            {
                actualText = fieldName;
            }
            return actualText;
        }

        public int GetItemIdForField(string fieldName)
        {
            foreach (Item item in base.Analysis.Items)
            {
                if (item is SPSSExportItem)
                {
                    return ((SPSSExportItem) item).GetItemId(fieldName);
                }
            }
            return -1;
        }
    }
}

