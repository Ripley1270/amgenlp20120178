﻿namespace Checkbox.Forms.Validation
{
    using Checkbox.Common;
    using Checkbox.Forms.Items;
    using System;
    using System.Globalization;

    public class SingleLineTextAnswerValidator : TextAnswerValidator
    {
        public override bool Validate(TextItem input)
        {
            if (!base.Validate(input))
            {
                return false;
            }
            if (input.HasAnswer)
            {
                SingleLineTextBoxItem item = (SingleLineTextBoxItem) input;
                if (((item.Format == AnswerFormat.Date) || (item.Format == AnswerFormat.Date_ROTW)) || (item.Format == AnswerFormat.Date_USA))
                {
                    CultureInfo currentCulture;
                    if (item.Format == AnswerFormat.Date_USA)
                    {
                        currentCulture = new CultureInfo("en-US");
                    }
                    else if (item.Format == AnswerFormat.Date_ROTW)
                    {
                        currentCulture = new CultureInfo("en-GB");
                    }
                    else
                    {
                        currentCulture = CultureInfo.CurrentCulture;
                    }
                    DateTime time = DateTime.Parse(item.GetAnswer(), currentCulture);
                    DateTime? minDate = item.MinNumericValue.HasValue ? ((DateTime?) new DateTime((long) item.MinNumericValue.Value)) : null;
                    DateTime? maxDate = item.MaxNumericValue.HasValue ? ((DateTime?) new DateTime((long) item.MaxNumericValue.Value)) : null;
                    DateRangeValidator validator = new DateRangeValidator(minDate, maxDate, input.Format);
                    if (!validator.Validate(time))
                    {
                        base.ErrorMessage = validator.GetMessage(input.LanguageCode);
                        return false;
                    }
                }
                else if (item.Format == AnswerFormat.Money)
                {
                    double? currencyNumericValue = Utilities.GetCurrencyNumericValue(input.GetAnswer(), new CultureInfo[0]);
                    if (currencyNumericValue.HasValue)
                    {
                        DoubleRangeValidator validator2 = new DoubleRangeValidator(item.MinNumericValue, item.MaxNumericValue);
                        if (!validator2.Validate(currencyNumericValue.Value))
                        {
                            base.ErrorMessage = validator2.GetMessage(item.LanguageCode);
                            return false;
                        }
                    }
                }
                else
                {
                    double? nullable4 = Utilities.GetDouble(input.GetAnswer(), new CultureInfo[0]);
                    if (nullable4.HasValue)
                    {
                        DoubleRangeValidator validator3 = new DoubleRangeValidator(item.MinNumericValue, item.MaxNumericValue);
                        if (!validator3.Validate(nullable4.Value))
                        {
                            base.ErrorMessage = validator3.GetMessage(item.LanguageCode);
                            return false;
                        }
                    }
                }
            }
            return true;
        }
    }
}

