﻿namespace Checkbox.Security.Providers
{
    using Checkbox.Security;
    using Checkbox.Users;
    using Prezza.Framework.Common;
    using Prezza.Framework.Configuration;
    using Prezza.Framework.ExceptionHandling;
    using Prezza.Framework.Logging;
    using Prezza.Framework.Security;
    using Prezza.Framework.Security.Principal;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class DbAuthorizationProvider : IAuthorizationProvider, IConfigurationProvider
    {
        private DbAuthorizationProviderData _config;

        public bool Authorize(ExtendedPrincipal principal, string context)
        {
            ArgumentValidation.CheckForEmptyString(context, "context");
            if (principal != null)
            {
                string[] roles = principal.GetRoles();
                for (int i = 0; i < roles.Length; i++)
                {
                    if (roles[i] == "System Administrator")
                    {
                        return true;
                    }
                    if (RoleManager.GetRole(roles[i]).HasPermission(context))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool Authorize(ExtendedPrincipal principal, IAccessControllable resource, string context)
        {
            try
            {
                ArgumentValidation.CheckForNullReference(resource, "resource");
                ArgumentValidation.CheckForEmptyString(context, "context");
                if (this._config == null)
                {
                    throw new Exception("This authorization provider instance has no configuration information.");
                }
                if (principal != null)
                {
                    if (principal.IsInRole("System Administrator"))
                    {
                        return true;
                    }
                    if (!this.Authorize(principal, context))
                    {
                        Logger.Write("Authorization failed for " + principal.Identity.Name + " to " + context + " failed.", "Warning", 1, -1, Severity.Warning);
                        return false;
                    }
                    if (resource.ACL != null)
                    {
                        if (resource.ACL.IsInList(principal))
                        {
                            return CheckPolicy(resource.ACL.GetPolicy(principal), context, principal);
                        }
                        List<Group> groupMemberships = Group.GetGroupMemberships(principal);
                        bool flag = false;
                        int num = 0;
                        foreach (Group group in groupMemberships)
                        {
                            if ((group != null) && resource.ACL.IsInList(group))
                            {
                                num++;
                                if (CheckPolicy(resource.ACL.GetPolicy(group), context, principal))
                                {
                                    flag = true;
                                }
                            }
                        }
                        if (resource.ACL.IsInList(Group.Everyone))
                        {
                            num++;
                            if (CheckPolicy(resource.ACL.GetPolicy(Group.Everyone), context, principal))
                            {
                                flag = true;
                            }
                        }
                        if (num > 0)
                        {
                            return flag;
                        }
                    }
                }
                return CheckPolicy(resource.DefaultPolicy, context, principal);
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
                return false;
            }
        }

        private static bool CheckPolicy(Policy policy, string context, ExtendedPrincipal principal)
        {
            if ((policy == null) || !policy.HasPermission(context))
            {
                if (principal != null)
                {
                    Logger.Write("Authorization failed for " + principal.Identity.Name + " to " + context + ".", "Warning", 1, -1, Severity.Warning);
                }
                else
                {
                    Logger.Write("Authorization failed for null principal to " + context + ".", "Warning", 1, -1, Severity.Warning);
                }
                return false;
            }
            if (principal != null)
            {
                Logger.Write("Authorization succeeded for " + principal.Identity.Name + " to " + context + ".", "Info", 1, -1, Severity.Information);
            }
            else
            {
                Logger.Write("Authorization succeeded for null principal to " + context + ".", "Warning", 1, -1, Severity.Warning);
            }
            return true;
        }

        public string GetAuthorizationErrorType()
        {
            return string.Empty;
        }

        public void Initialize(ConfigurationBase config)
        {
            this._config = (DbAuthorizationProviderData) config;
        }

        public string ConfigurationName { get; set; }
    }
}

