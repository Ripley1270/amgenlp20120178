﻿namespace Checkbox.Forms
{
    using System;

    public enum ResponseSessionState
    {
        None,
        TakeSurvey,
        SelectLanguage,
        EnterPassword,
        SavedProgress,
        EditResponse,
        LoginRequired,
        Error
    }
}

