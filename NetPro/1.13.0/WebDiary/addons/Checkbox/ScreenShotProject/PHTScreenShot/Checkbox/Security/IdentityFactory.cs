﻿namespace Checkbox.Security
{
    using Checkbox.Security.Configuration;
    using Prezza.Framework.Configuration;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Collections;

    public sealed class IdentityFactory
    {
        private static readonly Hashtable _providers;

        static IdentityFactory()
        {
            lock (typeof(IdentityFactory))
            {
                _providers = new Hashtable();
            }
        }

        public static IIdentityProvider GetIdentityProvider()
        {
            try
            {
                lock (_providers.SyncRoot)
                {
                    if (_providers.Contains("[DEFAULT]"))
                    {
                        return (IIdentityProvider) _providers["[DEFAULT]"];
                    }
                }
                IIdentityProvider identityProvider = new IdentityProviderFactory("identityProviderFactory", (SecurityConfiguration) ConfigurationManager.GetConfiguration("identityProviderConfiguration")).GetIdentityProvider();
                lock (_providers.SyncRoot)
                {
                    if (!_providers.Contains("[DEFAULT]"))
                    {
                        _providers.Add("[DEFAULT]", identityProvider);
                    }
                }
                return identityProvider;
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
                return null;
            }
        }

        public static IIdentityProvider GetIdentityProvider(string identityProvider)
        {
            try
            {
                lock (_providers.SyncRoot)
                {
                    if (_providers.Contains(identityProvider))
                    {
                        return (IIdentityProvider) _providers[identityProvider];
                    }
                }
                IIdentityProvider provider = new IdentityProviderFactory("identityProviderFactory", (SecurityConfiguration) ConfigurationManager.GetConfiguration("identityProviderConfiguration")).GetIdentityProvider(identityProvider);
                lock (_providers.SyncRoot)
                {
                    if (!_providers.Contains(identityProvider))
                    {
                        _providers.Add(identityProvider, provider);
                    }
                }
                return provider;
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
                return null;
            }
        }
    }
}

