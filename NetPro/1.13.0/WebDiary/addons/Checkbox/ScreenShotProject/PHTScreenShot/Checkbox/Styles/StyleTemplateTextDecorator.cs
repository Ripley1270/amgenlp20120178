﻿namespace Checkbox.Styles
{
    using Checkbox.Common;
    using Checkbox.Globalization.Text;
    using Prezza.Framework.Common;
    using Prezza.Framework.Security.Principal;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Xml;

    public class StyleTemplateTextDecorator : TextDecorator
    {
        private string _footerText;
        private bool _footerTextModified;
        private string _headerText;
        private bool _headerTextModified;
        private readonly StyleTemplate _template;

        public StyleTemplateTextDecorator(StyleTemplate template, string language) : base(language)
        {
            ArgumentValidation.CheckForNullReference(template, "StyleTemplate");
            this._template = template;
        }

        public XmlDocument GetTemplateXml()
        {
            if (Utilities.IsNotNullOrEmpty(this._template.HeaderTextID) && Utilities.IsNotNullOrEmpty(this._template.FooterTextID))
            {
                Dictionary<string, string> allTexts = this.GetAllTexts(this._template.HeaderTextID);
                Dictionary<string, string> dictionary2 = this.GetAllTexts(this._template.FooterTextID);
                XmlDocument document = this._template.ToXml();
                if (document != null)
                {
                    XmlNode node = document.SelectSingleNode("/CssDocument/Header");
                    XmlNode node2 = document.SelectSingleNode("/CssDocument/Footer");
                    if ((node != null) && (node2 != null))
                    {
                        foreach (string str in allTexts.Keys)
                        {
                            XmlElement newChild = document.CreateElement("headerText");
                            XmlAttribute attribute = document.CreateAttribute("languageCode");
                            XmlCDataSection section = document.CreateCDataSection(allTexts[str]);
                            attribute.Value = str;
                            newChild.AppendChild(section);
                            newChild.Attributes.SetNamedItem(attribute);
                            node.AppendChild(newChild);
                        }
                        foreach (string str2 in dictionary2.Keys)
                        {
                            XmlElement element2 = document.CreateElement("footerText");
                            XmlAttribute attribute2 = document.CreateAttribute("languageCode");
                            XmlCDataSection section2 = document.CreateCDataSection(dictionary2[str2]);
                            attribute2.Value = str2;
                            element2.AppendChild(section2);
                            element2.Attributes.SetNamedItem(attribute2);
                            node2.AppendChild(element2);
                        }
                    }
                    return document;
                }
            }
            return null;
        }

        public void LoadTemplateFromXml(XmlDocument doc, ExtendedPrincipal currentPrincipal)
        {
            ArgumentValidation.CheckForNullReference(doc, "StyleTemplateXml");
            this._template.Load(doc.OuterXml);
            StyleTemplateManager.SaveTemplate(this._template, currentPrincipal);
            XmlNodeList list = doc.SelectNodes("/CssDocument/Header/headerText");
            XmlNodeList list2 = doc.SelectNodes("/CssDocument/Footer/footerText");
            if (Utilities.IsNotNullOrEmpty(this._template.HeaderTextID))
            {
                foreach (XmlNode node in list)
                {
                    XmlAttribute attribute = node.Attributes["languageCode"];
                    if ((attribute != null) && (attribute.Value != string.Empty))
                    {
                        this.SetText(this._template.HeaderTextID, node.InnerText, attribute.Value);
                    }
                }
            }
            if (Utilities.IsNotNullOrEmpty(this._template.FooterTextID))
            {
                foreach (XmlNode node2 in list2)
                {
                    XmlAttribute attribute2 = node2.Attributes["languageCode"];
                    if ((attribute2 != null) && (attribute2.Value != string.Empty))
                    {
                        this.SetText(this._template.FooterTextID, node2.InnerText, attribute2.Value);
                    }
                }
            }
        }

        public override void Save()
        {
            throw new Exception("Parameterless save method is not supported for style template.  Use overload instead.");
        }

        public override void Save(IDbTransaction t)
        {
            this.Save();
        }

        public void Save(ExtendedPrincipal currentPrincipal, IDbTransaction t)
        {
            StyleTemplateManager.SaveTemplate(this._template, currentPrincipal);
            this.SetLocalizedTexts();
        }

        protected override void SetLocalizedTexts()
        {
            if (Utilities.IsNotNullOrEmpty(this._template.HeaderTextID))
            {
                this.SetText(this._template.HeaderTextID, this.HeaderText);
            }
            if (Utilities.IsNotNullOrEmpty(this._template.FooterTextID))
            {
                this.SetText(this._template.FooterTextID, this.FooterText);
            }
        }

        public string FooterText
        {
            get
            {
                if (Utilities.IsNotNullOrEmpty(this._template.FooterTextID) && !this._footerTextModified)
                {
                    return this.GetText(this._template.FooterTextID);
                }
                return this._footerText;
            }
            set
            {
                this._footerText = value;
                this._footerTextModified = true;
            }
        }

        public string HeaderText
        {
            get
            {
                if (Utilities.IsNotNullOrEmpty(this._template.HeaderTextID) && !this._headerTextModified)
                {
                    return this.GetText(this._template.HeaderTextID);
                }
                return this._headerText;
            }
            set
            {
                this._headerText = value;
                this._headerTextModified = true;
            }
        }
    }
}

