﻿namespace Checkbox.Forms.Items.UI
{
    using Prezza.Framework.Data;
    using System;
    using System.Data;

    [Serializable]
    public class MultiLineText : LabelledItemAppearanceData
    {
        protected override void Create(IDbTransaction transaction)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Appearance_CreateMLText");
            storedProcCommandWrapper.AddInParameter("Rows", DbType.Int32, this.Rows);
            storedProcCommandWrapper.AddInParameter("Columns", DbType.Int32, this.Columns);
            storedProcCommandWrapper.AddInParameter("ItemPosition", DbType.String, base.ItemPosition);
            storedProcCommandWrapper.AddInParameter("LabelPosition", DbType.String, this.LabelPosition.ToString());
            storedProcCommandWrapper.AddOutParameter("AppearanceID", DbType.Int32, 4);
            database.ExecuteNonQuery(storedProcCommandWrapper, transaction);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("AppearanceID");
            if ((parameterValue == null) || (parameterValue == DBNull.Value))
            {
                throw new Exception("Unable to save item appearance data.");
            }
            base.ID = new int?(Convert.ToInt32(parameterValue));
        }

        protected override void Update(IDbTransaction transaction)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Appearance_UpdateMLText");
            storedProcCommandWrapper.AddInParameter("AppearanceID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("Rows", DbType.Int32, this.Rows);
            storedProcCommandWrapper.AddInParameter("Columns", DbType.Int32, this.Columns);
            storedProcCommandWrapper.AddInParameter("ItemPosition", DbType.String, base.ItemPosition);
            storedProcCommandWrapper.AddInParameter("LabelPosition", DbType.String, this.LabelPosition.ToString());
            database.ExecuteNonQuery(storedProcCommandWrapper, transaction);
        }

        public override string AppearanceCode
        {
            get
            {
                return "MULTI_LINE_TEXT";
            }
        }
    }
}

