﻿namespace Checkbox.Forms.Security
{
    using Prezza.Framework.Security;
    using System;

    [Serializable]
    public class FormPolicy : Policy
    {
        private static string[] allowablePermissions = new string[] { "Form.Administer", "Form.Edit", "Form.Fill", "Form.Delete", "Analysis.Create", "Analysis.Responses.View", "Analysis.Responses.Export", "Analysis.ManageFilters" };

        public FormPolicy() : base(allowablePermissions)
        {
        }

        public FormPolicy(string[] permissions) : base(permissions)
        {
        }
    }
}

