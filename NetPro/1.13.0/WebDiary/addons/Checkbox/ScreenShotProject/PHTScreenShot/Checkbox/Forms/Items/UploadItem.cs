﻿namespace Checkbox.Forms.Items
{
    using Checkbox.Common;
    using Checkbox.Forms;
    using Checkbox.Forms.Items.Configuration;
    using Checkbox.Forms.Validation;
    using Checkbox.Globalization.Text;
    using Checkbox.Management;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Text;

    public class UploadItem : LabelledItem
    {
        private List<string> _allowedFileTypes;
        private byte[] _data;
        private int _fileID;
        private string _fileName;
        private string _fileType;

        public UploadItem()
        {
        }

        public UploadItem(byte[] data, string fileName, string fileType)
        {
            this.Data = data ?? new byte[0];
            this.FileName = fileName;
            this.FileType = fileType;
        }

        private static string CaseInsensitiveString(string original)
        {
            StringBuilder builder = new StringBuilder();
            original = original.Replace(".", string.Empty);
            foreach (char ch in original)
            {
                builder.Append("([");
                string introduced4 = ch.ToString().ToLower();
                builder.Append(introduced4 + ch.ToString().ToUpper());
                builder.Append("])");
            }
            return builder.ToString();
        }

        public override void Configure(ItemData configuration, string languageCode)
        {
            if (!ApplicationManager.AppSettings.EnableUploadItem)
            {
                base.Excluded = true;
            }
            base.Configure(configuration, languageCode);
            this.FileID = ((UploadItemData) configuration).FileID;
            this.AllowedFileTypes = ((UploadItemData) configuration).AllowedFileTypes;
        }

        public virtual List<long> GetAllAnswerIds()
        {
            List<long> list = new List<long>();
            DataRow[] answerRowsForItem = base.AnswerData.GetAnswerRowsForItem(base.ID);
            if ((answerRowsForItem != null) && (answerRowsForItem.Length > 0))
            {
                foreach (DataRow row in answerRowsForItem)
                {
                    list.Add((long) row["AnswerID"]);
                }
            }
            return list;
        }

        public void UploadFile(string fileName, byte[] data, string fileType)
        {
            if (base.Response != null)
            {
                this.Data = data;
                this.FileName = fileName;
                this.FileType = fileType;
                this.SetAnswer(string.Format("{0} - {1}", this.FileName, this.FileSizeDisplay));
                if (this.ValidateFileType())
                {
                    base.Response.SaveCurrentState();
                    List<long> allAnswerIds = this.GetAllAnswerIds();
                    if ((((this.Data != null) && (this.FileName != null)) && ((this.FileType != null) && (allAnswerIds != null))) && (allAnswerIds.Count > 0))
                    {
                        UploadItemManager.Save(this.Data, this.FileName, this.FileType, base.ID, allAnswerIds);
                    }
                }
            }
        }

        protected override bool ValidateAnswers()
        {
            if (this.Required && !this.HasAnswer)
            {
                base.ValidationErrors.Add(TextManager.GetText("/validationMessages/regex/required", base.LanguageCode));
                return false;
            }
            return true;
        }

        protected bool ValidateFileType()
        {
            UploadItemValidator validator = new UploadItemValidator();
            if (!validator.Validate(this))
            {
                base.ValidationErrors.Add(validator.GetMessage(base.LanguageCode));
                return false;
            }
            return true;
        }

        public List<string> AllowedFileTypes
        {
            get
            {
                return (this._allowedFileTypes ?? new List<string>());
            }
            private set
            {
                this._allowedFileTypes = value;
            }
        }

        public string AllowedFileTypesCSV
        {
            get
            {
                return Utilities.ListToDelimitedString(',', this.AllowedFileTypes);
            }
        }

        public string AllowedFileTypesRegex
        {
            get
            {
                StringBuilder builder = new StringBuilder(@"^.+\.(");
                for (int i = 0; i < this.AllowedFileTypes.Count; i++)
                {
                    builder.Append(CaseInsensitiveString(this.AllowedFileTypes[i]));
                    if ((this.AllowedFileTypes.Count - 1) > i)
                    {
                        builder.Append("|");
                    }
                }
                builder.Append(")$");
                return builder.ToString();
            }
        }

        public byte[] Data
        {
            get
            {
                return this._data;
            }
            private set
            {
                this._data = value;
            }
        }

        public int FileID
        {
            get
            {
                return this._fileID;
            }
            private set
            {
                this._fileID = value;
            }
        }

        public string FileName
        {
            get
            {
                return this._fileName;
            }
            private set
            {
                this._fileName = value;
            }
        }

        public int FileSize
        {
            get
            {
                if (this.Data == null)
                {
                    return 0;
                }
                return this.Data.Length;
            }
        }

        public string FileSizeDisplay
        {
            get
            {
                if (0x400 > this.FileSize)
                {
                    return string.Format("{0} B", this.FileSize);
                }
                return string.Format("{0} KB", this.FileSize / 0x400);
            }
        }

        public string FileType
        {
            get
            {
                return this._fileType;
            }
            private set
            {
                this._fileType = value;
            }
        }
    }
}

