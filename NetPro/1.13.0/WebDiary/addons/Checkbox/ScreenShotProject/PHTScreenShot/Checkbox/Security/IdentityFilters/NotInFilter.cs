﻿namespace Checkbox.Security.IdentityFilters
{
    using Prezza.Framework.Common;
    using System;
    using System.Collections;

    public class NotInFilter : IdentityFilter
    {
        private ArrayList propertyValues;

        public NotInFilter(string filterProperty, object[] propertyValues) : base(IdentityFilterType.NotIn, IdentityFilterPropertyType.IdentityProperty, filterProperty)
        {
            ArgumentValidation.CheckForNullReference(propertyValues, "propertyValues");
            this.propertyValues = new ArrayList(propertyValues);
        }

        public NotInFilter(string filterProperty, object[] propertyValues, IdentityFilterPropertyType propertyType) : base(IdentityFilterType.NotIn, propertyType, filterProperty)
        {
            ArgumentValidation.CheckForNullReference(propertyValues, "propertyValues");
            this.propertyValues = new ArrayList(propertyValues);
        }

        public object[] PropertyValues
        {
            get
            {
                return (object[]) this.propertyValues.ToArray(typeof(object));
            }
        }
    }
}

