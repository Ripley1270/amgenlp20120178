﻿namespace Checkbox.Forms.Items
{
    using Checkbox.Common;
    using Checkbox.Forms.Items.Configuration;
    using Prezza.Framework.Common;
    using System;
    using System.Collections.Specialized;

    [Serializable]
    public class HiddenItem : LabelledItem
    {
        private string _variableName;
        private HiddenVariableSource _variableSource;

        public override void Configure(ItemData configuration, string languageCode)
        {
            base.Configure(configuration, languageCode);
            ArgumentValidation.CheckExpectedType(configuration, typeof(HiddenItemData));
            HiddenItemData data = (HiddenItemData) configuration;
            this._variableSource = data.VariableSource;
            this._variableName = data.VariableName;
        }

        protected override NameValueCollection GetMetaDataValuesForXmlSerialization()
        {
            NameValueCollection metaDataValuesForXmlSerialization = base.GetMetaDataValuesForXmlSerialization();
            metaDataValuesForXmlSerialization["VariableSource"] = this.VariableSource.ToString();
            metaDataValuesForXmlSerialization["VariableName"] = this.VariableName;
            return metaDataValuesForXmlSerialization;
        }

        protected override bool ValidateAnswers()
        {
            return true;
        }

        public override string Alias
        {
            get
            {
                if (Utilities.IsNotNullOrEmpty(base.Alias))
                {
                    return base.Alias;
                }
                return this.VariableName;
            }
        }

        public string VariableName
        {
            get
            {
                return this._variableName;
            }
        }

        public HiddenVariableSource VariableSource
        {
            get
            {
                return this._variableSource;
            }
        }

        public override bool Visible
        {
            get
            {
                return (base.Response == null);
            }
        }
    }
}

