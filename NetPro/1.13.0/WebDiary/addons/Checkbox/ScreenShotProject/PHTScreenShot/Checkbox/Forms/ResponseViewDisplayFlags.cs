﻿namespace Checkbox.Forms
{
    using System;

    [Flags]
    public enum ResponseViewDisplayFlags
    {
        BackButton = 0x40,
        CreateNewButton = 0x400,
        FinishButton = 0x200,
        ItemNumbers = 0x10,
        NextButton = 0x100,
        None = 0,
        PageNumbers = 8,
        ProgressBar = 0x20,
        SaveButton = 0x80,
        Title = 4
    }
}

