﻿namespace Checkbox.Forms.Validation
{
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Text.RegularExpressions;

    public class RegularExpressionValidator : Validator<string>
    {
        protected string _regex;

        public override string GetMessage(string languageCode)
        {
            return this._regex;
        }

        public override bool Validate(string input)
        {
            if ((this._regex == null) || (this._regex == string.Empty))
            {
                return true;
            }
            try
            {
                return this.RegularExpression.IsMatch(input);
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
                return false;
            }
        }

        public virtual Regex RegularExpression
        {
            get
            {
                return new Regex(this._regex, RegexOptions.None);
            }
        }
    }
}

