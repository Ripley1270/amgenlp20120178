﻿namespace Checkbox.Forms.Items.UI
{
    using System;

    [Serializable]
    public class Captcha : LabelledItemAppearanceData
    {
        public override string AppearanceCode
        {
            get
            {
                return "CAPTCHA";
            }
        }
    }
}

