﻿namespace Checkbox.Panels
{
    using Checkbox.Common;
    using Checkbox.Globalization.Text;
    using Checkbox.Panels.Data;
    using Checkbox.Panels.Security;
    using Checkbox.Security;
    using Prezza.Framework.Data;
    using Prezza.Framework.Data.QueryCriteria;
    using Prezza.Framework.Security;
    using Prezza.Framework.Security.Principal;
    using System;
    using System.Collections.Generic;
    using System.Data;

    [Serializable]
    public class EmailListPanel : Panel
    {
        public static string EMAIL_ADDRESS_KEY = "EmailAddress";
        public static string FIRST_NAME_KEY = "FName";
        public static string LAST_NAME_KEY = "LName";

        public EmailListPanel() : base(new string[] { "EmailList.View", "EmailList.Edit" }, new string[] { "EmailList.Edit", "EmailList.View", "EmailList.Delete" })
        {
            base.PanelType = PanelType.EmailListPanel;
        }

        public void AddPanelist(string emailAddress)
        {
            this.AddPanelist(emailAddress, string.Empty, string.Empty);
        }

        public void AddPanelist(string emailAddress, string fName, string lName)
        {
            if (!Utilities.IsNullOrEmpty(emailAddress))
            {
                string str = Utilities.SqlEncode(emailAddress);
                if (base.Panelists.Find(p => p.Email.Equals(emailAddress, StringComparison.InvariantCultureIgnoreCase)) == null)
                {
                    Panelist item = new Panelist {
                        Email = str
                    };
                    item.SetProperty("FName", fName);
                    item.SetProperty("LName", lName);
                    base.AddedPanelists.Add(item);
                    base.Panelists.Add(item);
                }
            }
        }

        public bool CanDelete()
        {
            int num;
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Panel_CanDeleteELEntries");
            storedProcCommandWrapper.AddInParameter("PanelID", DbType.Int32, base.ID.Value);
            int.TryParse(database.ExecuteScalar(storedProcCommandWrapper).ToString(), out num);
            return (num < 1);
        }

        public static EmailListPanel Copy(EmailListPanel panel, ExtendedPrincipal currentPrincipal, string languageCode)
        {
            int? nullable;
            int num = 1;
            string name = string.Concat(new object[] { panel.Name, " ", TextManager.GetText("/common/duplicate", languageCode), " ", num });
        Label_0092:
            nullable = null;
            if (EmailListNameExists(name, nullable))
            {
                num++;
                name = string.Concat(new object[] { panel.Name, " ", TextManager.GetText("/common/duplicate", languageCode), " ", num });
                goto Label_0092;
            }
            EmailListPanel panel2 = new EmailListPanel {
                Name = name,
                Description = panel.Description
            };
            foreach (Panelist panelist in panel.Panelists)
            {
                panel2.AddPanelist(panelist.Email, panelist.GetProperty("FName"), panelist.GetProperty("LName"));
            }
            AccessControlList acl = new AccessControlList();
            Policy policy = panel2.CreatePolicy(panel.SupportedPermissions);
            acl.Add(currentPrincipal, policy);
            acl.Save();
            Policy defaultPolicy = panel2.CreatePolicy(new string[0]);
            panel2.SetAccess(defaultPolicy, acl);
            panel2.Save(currentPrincipal);
            return panel2;
        }

        protected override void Create(ExtendedPrincipal principal, IDbTransaction t)
        {
            if (!AuthorizationFactory.GetAuthorizationProvider().Authorize(principal, "EmailList.Create"))
            {
                throw new AuthorizationException();
            }
            if (EmailListNameExists(base.Name, null))
            {
                throw new Exception("An email panel list with this name already exists");
            }
            base.Create(principal, t);
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Panel_CreateEmailListACL");
            storedProcCommandWrapper.AddInParameter("PanelID", DbType.Int32, base.ID.Value);
            if (principal != null)
            {
                storedProcCommandWrapper.AddInParameter("CreatedBy", DbType.String, principal.Identity.Name);
            }
            else
            {
                storedProcCommandWrapper.AddInParameter("CreatedBy", DbType.String, string.Empty);
            }
            storedProcCommandWrapper.AddOutParameter("AclID", DbType.Int32, 4);
            storedProcCommandWrapper.AddOutParameter("DefaultPolicyID", DbType.Int32, 4);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
            base.AclID = new int?((int) storedProcCommandWrapper.GetParameterValue("AclID"));
            base.DefaultPolicyID = new int?((int) storedProcCommandWrapper.GetParameterValue("DefaultPolicyID"));
        }

        public override void Delete(IDbTransaction transaction)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Panel_DeleteELEntries");
            storedProcCommandWrapper.AddInParameter("PanelID", DbType.Int32, base.ID.Value);
            database.ExecuteNonQuery(storedProcCommandWrapper, transaction);
            base.Delete(transaction);
        }

        public static bool EmailListNameExists(string name, int? idToIgnore)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Panel_CheckNameExists");
            storedProcCommandWrapper.AddInParameter("Name", DbType.String, name);
            storedProcCommandWrapper.AddInParameter("PanelTypeID", DbType.Int32, Convert.ToInt32(PanelType.EmailListPanel));
            storedProcCommandWrapper.AddInParameter("IDToIgnore", DbType.Int32, idToIgnore);
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    if (reader.Read() && (reader.GetInt32(0) > 0))
                    {
                        return true;
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return false;
        }

        public static DataSet GetAvailableEmailLists(ExtendedPrincipal principal)
        {
            SelectQuery query = PanelQueryFactory.GetSelectAvailableEmailListsQuery(principal, PermissionJoin.All, new string[] { "EmailList.View" });
            query.AddSortField(new SortOrder("Name", true));
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
            return database.ExecuteDataSet(sqlStringCommandWrapper);
        }

        public static DataSet GetAvailableEmailLists(ExtendedPrincipal currentPrincipal, int resultsPerPage, int pageNumber, string filterField, string filterValue, string sortField, bool sortAscending, CriteriaCollection filterCollection)
        {
            SelectQuery query = PanelQueryFactory.GetSelectAvailableEmailListsQuery(currentPrincipal, resultsPerPage, pageNumber, filterField, filterValue, sortField, sortAscending, filterCollection, PermissionJoin.All, new string[] { "EmailList.View" });
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
            return database.ExecuteDataSet(sqlStringCommandWrapper);
        }

        protected DBCommandWrapper GetDeletePanelistCommand(Database db, string panelistEmail)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_Panel_DeleteEmail");
            storedProcCommandWrapper.AddInParameter("PanelID", DbType.Int32, base.ID.Value);
            storedProcCommandWrapper.AddInParameter("EmailAddress", DbType.String, panelistEmail);
            return storedProcCommandWrapper;
        }

        public static DataSet GetEditableEmailLists(ExtendedPrincipal principal)
        {
            SelectQuery query = PanelQueryFactory.GetSelectAvailableEmailListsQuery(principal, PermissionJoin.All, new string[] { "EmailList.Edit" });
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
            return database.ExecuteDataSet(sqlStringCommandWrapper);
        }

        public override SecurityEditor GetEditor()
        {
            return new EmailListSecurityEditor(this);
        }

        public static int GetEmailListsCount(ExtendedPrincipal currentPrincipal, string filterField, string filterValue, CriteriaCollection filterCollection)
        {
            int num;
            SelectQuery query = PanelQueryFactory.GetAvailableEmailListCountQuery(currentPrincipal, filterField, filterValue, filterCollection);
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
            using (IDataReader reader = database.ExecuteReader(sqlStringCommandWrapper))
            {
                try
                {
                    if (reader.Read())
                    {
                        return Convert.ToInt32(reader["RecordCount"]);
                    }
                    num = 0;
                }
                finally
                {
                    reader.Close();
                }
            }
            return num;
        }

        protected DBCommandWrapper GetInsertPanelistCommand(Database db, string panelistEmail, string fName, string lName)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_Panel_InsertEmail");
            storedProcCommandWrapper.AddInParameter("PanelID", DbType.Int32, base.ID.Value);
            storedProcCommandWrapper.AddInParameter("EmailAddress", DbType.String, panelistEmail);
            storedProcCommandWrapper.AddInParameter("FName", DbType.String, fName);
            storedProcCommandWrapper.AddInParameter("LName", DbType.String, lName);
            return storedProcCommandWrapper;
        }

        protected override List<Panelist> GetPanelists()
        {
            List<Panelist> list = new List<Panelist>();
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Panel_GetEmailAddressData");
            storedProcCommandWrapper.AddInParameter("PanelID", DbType.Int32, base.ID);
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    while (reader.Read())
                    {
                        string str = DbUtility.GetValueFromDataReader<string>(reader, "EmailAddress", string.Empty);
                        if (Utilities.IsNotNullOrEmpty(str))
                        {
                            Panelist item = new Panelist {
                                Email = str
                            };
                            item.SetProperty("FName", DbUtility.GetValueFromDataReader<string>(reader, "FName", string.Empty));
                            item.SetProperty("LName", DbUtility.GetValueFromDataReader<string>(reader, "LName", string.Empty));
                            list.Add(item);
                        }
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return list;
        }

        protected override void Load(DataRow data)
        {
            base.Load(data);
            base.AclID = new int?((int) data["AclID"]);
            base.DefaultPolicyID = new int?((int) data["DefaultPolicy"]);
        }

        public void RemoveAllAddresses()
        {
            base.RemovedPanelists.Clear();
            base.RemovedPanelists.AddRange(base.Panelists);
        }

        public void RemovePanelist(IEnumerable<string> emailAddresses)
        {
            using (IEnumerator<string> enumerator = emailAddresses.GetEnumerator())
            {
                Predicate<Panelist> match = null;
                string emailAddress;
                while (enumerator.MoveNext())
                {
                    emailAddress = enumerator.Current;
                    if (Utilities.IsNullOrEmpty(emailAddress))
                    {
                        return;
                    }
                    if (match == null)
                    {
                        match = p => p.Email.Equals(emailAddress, StringComparison.InvariantCultureIgnoreCase);
                    }
                    int index = base.Panelists.FindIndex(match);
                    if (index >= 0)
                    {
                        base.RemovedPanelists.Add(base.Panelists[index]);
                        base.Panelists.RemoveAt(index);
                    }
                }
            }
        }

        public void SavePanelists()
        {
            using (IDbConnection connection = DatabaseFactory.CreateDatabase().GetConnection())
            {
                connection.Open();
                try
                {
                    using (IDbTransaction transaction = connection.BeginTransaction())
                    {
                        try
                        {
                            this.UpdatePanelists(transaction);
                            transaction.Commit();
                        }
                        catch
                        {
                            transaction.Rollback();
                        }
                    }
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        protected override void UpdatePanelists(IDbTransaction t)
        {
            Database db = DatabaseFactory.CreateDatabase();
            foreach (Panelist panelist in base.RemovedPanelists)
            {
                DBCommandWrapper deletePanelistCommand = this.GetDeletePanelistCommand(db, panelist.Email);
                db.ExecuteNonQuery(deletePanelistCommand, t);
            }
            foreach (Panelist panelist2 in base.AddedPanelists)
            {
                DBCommandWrapper command = this.GetInsertPanelistCommand(db, panelist2.Email, panelist2.GetProperty("FName"), panelist2.GetProperty("LName"));
                db.ExecuteNonQuery(command, t);
            }
            base.AddedPanelists.Clear();
            base.RemovedPanelists.Clear();
        }

        public int Count
        {
            get
            {
                return base.Panelists.Count;
            }
        }
    }
}

