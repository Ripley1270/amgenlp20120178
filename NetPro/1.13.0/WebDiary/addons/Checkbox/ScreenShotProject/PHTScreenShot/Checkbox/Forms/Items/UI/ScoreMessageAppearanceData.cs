﻿namespace Checkbox.Forms.Items.UI
{
    using System;

    [Serializable]
    public class ScoreMessageAppearanceData : Message
    {
        public override string AppearanceCode
        {
            get
            {
                return "SCORE_MESSAGE";
            }
        }
    }
}

