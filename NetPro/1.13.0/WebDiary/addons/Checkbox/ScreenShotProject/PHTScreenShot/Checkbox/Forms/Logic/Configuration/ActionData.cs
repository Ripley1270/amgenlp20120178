﻿namespace Checkbox.Forms.Logic.Configuration
{
    using Checkbox.Common;
    using Checkbox.Forms;
    using Prezza.Framework.Data;
    using System;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    public abstract class ActionData : AbstractPersistedDomainObject
    {
        protected ActionData(ResponseTemplate context, object receiver)
        {
            this.Context = context;
            this.Receiver = receiver;
        }

        protected override void Create(IDbTransaction t)
        {
            DataRow row = this.Context.ActionTable.NewRow();
            row["ActionTypeName"] = base.GetType().FullName;
            row["ActionAssembly"] = base.GetType().Assembly.GetName().Name;
            this.Context.ActionTable.Rows.Add(row);
            base.ID = new int?((int) row["ActionID"]);
        }

        public override void Delete(IDbTransaction t)
        {
            if (base.ID.HasValue)
            {
                DataRow[] rowArray = this.Context.ActionTable.Select(this.IdentityColumnName + "=" + base.ID.Value);
                if (rowArray.Length > 0)
                {
                    rowArray[0].Delete();
                }
            }
        }

        protected override void Load(DataRow data)
        {
            if (data == null)
            {
                throw new Exception("DataRow cannot be null.");
            }
            if (this.DataTableName == data.Table.TableName)
            {
                this.LoadFromDataRow(data);
            }
            else
            {
                bool flag1 = data.Table.TableName == this.ParentDataTableName;
            }
        }

        public override void Save()
        {
            this.Save(null);
        }

        public override void Save(IDbTransaction t)
        {
            if (base.ID.HasValue)
            {
                this.Update(t);
            }
            else
            {
                this.Create(t);
            }
            this.OnSaved();
        }

        protected override void Update(IDbTransaction t)
        {
        }

        protected override DBCommandWrapper ConfigurationDataSetCommand
        {
            get
            {
                DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_Rules_GetAction");
                storedProcCommandWrapper.AddInParameter("ActionID", DbType.Int32, base.ID.Value);
                return storedProcCommandWrapper;
            }
        }

        public ResponseTemplate Context { get; private set; }

        public override string DataTableName
        {
            get
            {
                return "Action";
            }
        }

        internal int Identity
        {
            set
            {
                base.ID = new int?(value);
            }
        }

        public override string IdentityColumnName
        {
            get
            {
                return "ActionID";
            }
        }

        public override string ParentDataTableName
        {
            get
            {
                return "Action";
            }
        }

        public object Receiver { get; private set; }
    }
}

