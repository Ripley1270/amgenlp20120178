﻿namespace Checkbox.Security
{
    using Checkbox.Security.IdentityFilters;
    using Prezza.Framework.Common;
    using System;
    using System.Collections;

    public class IdentityFilterCollection
    {
        private ArrayList _filterCollections;
        private ArrayList _filters;
        private readonly IdentityFilterJoin _joiner;

        public IdentityFilterCollection(IdentityFilterJoin joiner) : this(joiner, null, null)
        {
        }

        public IdentityFilterCollection(IdentityFilterJoin joiner, IdentityFilter[] filters) : this(joiner, filters, null)
        {
        }

        public IdentityFilterCollection(IdentityFilterJoin joiner, IdentityFilter[] filters, IdentityFilterCollection[] filterCollections)
        {
            this.InitializeCollections(filters, filterCollections);
            this._joiner = joiner;
        }

        public void AddFilter(IdentityFilter filter)
        {
            ArgumentValidation.CheckForNullReference(filter, "filter");
            this._filters.Add(filter);
        }

        public void AddFilterCollection(IdentityFilterCollection filterCollection)
        {
            ArgumentValidation.CheckForNullReference(filterCollection, "filterCollection");
            this._filterCollections.Add(filterCollection);
        }

        private void InitializeCollections(IdentityFilter[] filters, IdentityFilterCollection[] filterCollections)
        {
            this._filters = (filters == null) ? new ArrayList() : new ArrayList(filters);
            this._filterCollections = (filterCollections == null) ? new ArrayList() : new ArrayList(filterCollections);
        }

        public IdentityFilterCollection[] FilterCollections
        {
            get
            {
                return (IdentityFilterCollection[]) this._filterCollections.ToArray(typeof(IdentityFilterCollection));
            }
            set
            {
                this._filterCollections = (value == null) ? new ArrayList() : new ArrayList(value);
            }
        }

        public IdentityFilter[] Filters
        {
            get
            {
                return (IdentityFilter[]) this._filters.ToArray(typeof(IdentityFilter));
            }
            set
            {
                this._filters = (value == null) ? new ArrayList() : new ArrayList(value);
            }
        }

        public IdentityFilterJoin Joiner
        {
            get
            {
                return this._joiner;
            }
        }
    }
}

