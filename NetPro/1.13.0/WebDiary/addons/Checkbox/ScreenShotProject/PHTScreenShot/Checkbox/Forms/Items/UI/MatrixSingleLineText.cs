﻿namespace Checkbox.Forms.Items.UI
{
    using System;

    [Serializable]
    public class MatrixSingleLineText : SingleLineText
    {
        public override string AppearanceCode
        {
            get
            {
                return "MATRIX_SINGLE_LINE_TEXT";
            }
        }
    }
}

