﻿namespace Checkbox.Content
{
    using System;

    public abstract class ContentItem
    {
        private string _contentType;
        private bool _isPublic;
        private byte[] _itemData;
        private string _itemName;
        private string _itemUrl;
        private DateTime _lastUpdated;

        protected ContentItem()
        {
        }

        protected abstract byte[] LoadItemData();

        public virtual string ContentType
        {
            get
            {
                return this._contentType;
            }
            set
            {
                this._contentType = value;
            }
        }

        public virtual byte[] Data
        {
            get
            {
                if (this._itemData == null)
                {
                    this._itemData = this.LoadItemData();
                }
                return this._itemData;
            }
            set
            {
                this._itemData = value;
            }
        }

        public virtual bool IsPublic
        {
            get
            {
                return this._isPublic;
            }
            set
            {
                this._isPublic = value;
            }
        }

        public virtual string ItemName
        {
            get
            {
                return this._itemName;
            }
            set
            {
                this._itemName = value;
            }
        }

        public virtual string ItemUrl
        {
            get
            {
                return this._itemUrl;
            }
            set
            {
                this._itemUrl = value;
            }
        }

        public virtual DateTime LastUpdated
        {
            get
            {
                return this._lastUpdated;
            }
            set
            {
                this._lastUpdated = value;
            }
        }
    }
}

