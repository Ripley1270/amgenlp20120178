﻿namespace Checkbox.Analytics.Filters
{
    using System;
    using System.Text;

    public abstract class ItemQueryFilter : ItemFilter, IQueryFilter
    {
        protected ItemQueryFilter()
        {
        }

        protected virtual string GetEscapedValueString()
        {
            if (base.Value != null)
            {
                return base.Value.ToString().Replace("'", "''");
            }
            return string.Empty;
        }

        protected virtual string GetFilterString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("ItemID = ");
            builder.Append(this.ItemID);
            builder.Append(" AND (");
            builder.Append(this.GetValueFilterClause());
            builder.Append(")");
            return builder.ToString();
        }

        protected abstract string GetValueFilterClause();

        public string FilterString
        {
            get
            {
                return this.GetFilterString();
            }
        }

        public abstract bool UseNotIn { get; }
    }
}

