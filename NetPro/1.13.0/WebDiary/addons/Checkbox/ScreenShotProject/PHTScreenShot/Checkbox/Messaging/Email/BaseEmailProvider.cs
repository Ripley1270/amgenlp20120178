﻿namespace Checkbox.Messaging.Email
{
    using Checkbox.Management;
    using Prezza.Framework.Configuration;
    using System;
    using System.Runtime.CompilerServices;
    using System.Text;

    public abstract class BaseEmailProvider : IEmailProvider, IConfigurationProvider
    {
        protected BaseEmailProvider()
        {
        }

        public long AddEmailMessageToBatch(long batchId, IEmailMessage message)
        {
            if (!this.SupportsBatches)
            {
                throw new EmailProviderDoesNotSupportBatchesException();
            }
            return this.DoAddEmailMessageToBatch(batchId, message);
        }

        protected static string AddLineBreaks(string stringToBreak)
        {
            if (!ApplicationManager.AppSettings.LimitEmailMessageLineLength)
            {
                return stringToBreak;
            }
            StringBuilder builder = new StringBuilder();
            int startIndex = 0;
            while (startIndex >= 0)
            {
                int index = stringToBreak.IndexOf(Environment.NewLine, startIndex);
                if ((index > 0) && (index < (startIndex + ApplicationManager.AppSettings.MaxEmailMessageLineLength)))
                {
                    builder.Append(stringToBreak.Substring(startIndex, (index - startIndex) + 1));
                    startIndex = index + 1;
                }
                else if (stringToBreak.Length >= (startIndex + ApplicationManager.AppSettings.MaxEmailMessageLineLength))
                {
                    builder.Append(stringToBreak.Substring(startIndex, ApplicationManager.AppSettings.MaxEmailMessageLineLength));
                    startIndex += ApplicationManager.AppSettings.MaxEmailMessageLineLength;
                }
                else
                {
                    builder.Append(stringToBreak.Substring(startIndex));
                    startIndex = -1;
                }
                builder.Append(Environment.NewLine);
            }
            return builder.ToString();
        }

        public void CloseEmailMessageBatch(long batchId)
        {
            if (!this.SupportsBatches)
            {
                throw new EmailProviderDoesNotSupportBatchesException();
            }
            this.DoCloseEmailMessageBatch(batchId);
        }

        public long CreateEmailMessageBatch(string name, string createdBy, DateTime? earliestSendDate)
        {
            if (!this.SupportsBatches)
            {
                throw new EmailProviderDoesNotSupportBatchesException();
            }
            return this.DoCreateEmailMessageBatch(name, createdBy, earliestSendDate);
        }

        public void DeleteEmailMessage(long messageId)
        {
            if (!this.SupportsBatches)
            {
                throw new EmailProviderDoesNotSupportBatchesException();
            }
            this.DoDeleteEmailMessage(messageId);
        }

        public void DeleteEmailMessageBatch(long batchId)
        {
            if (!this.SupportsBatches)
            {
                throw new EmailProviderDoesNotSupportBatchesException();
            }
            this.DoDeleteEmailMessageBatch(batchId);
        }

        public void DeleteMessageAttachment(long attachmentId)
        {
            if (!this.SupportsBatches)
            {
                throw new EmailProviderDoesNotSupportBatchesException();
            }
            this.DoDeleteMessageAttachment(attachmentId);
        }

        protected virtual long DoAddEmailMessageToBatch(long batchId, IEmailMessage message)
        {
            return -1L;
        }

        protected virtual void DoCloseEmailMessageBatch(long batchId)
        {
        }

        protected virtual long DoCreateEmailMessageBatch(string name, string createdBy, DateTime? earliestSendDate)
        {
            return -1L;
        }

        protected virtual void DoDeleteEmailMessage(long messageId)
        {
        }

        public virtual void DoDeleteEmailMessageBatch(long batchId)
        {
        }

        protected virtual void DoDeleteMessageAttachment(long attachmentId)
        {
        }

        protected virtual IEmailMessageBatchStatus DoGetEmailMessageBatchStatus(long batchId)
        {
            return null;
        }

        protected virtual IEmailMessageStatus DoGetEmailMessageStatus(long emailId)
        {
            return null;
        }

        protected virtual IEmailMessage DoGetMessage(long messageId)
        {
            return null;
        }

        protected virtual void DoInitialize(ConfigurationBase config)
        {
        }

        protected virtual long DoRegisterEmailAttachment(IEmailAttachment attachment)
        {
            return -1L;
        }

        protected abstract long? DoSendMessage(IEmailMessage message);
        protected virtual long DoSendMessage(string name, string createdBy, IEmailMessage message, DateTime? earliestSendDate)
        {
            return -1L;
        }

        public IEmailMessageBatchStatus GetEmailMessageBatchStatus(long batchId)
        {
            if (!this.SupportsBatches)
            {
                throw new EmailProviderDoesNotSupportBatchesException();
            }
            return this.GetEmailMessageBatchStatus(batchId);
        }

        public IEmailMessageStatus GetEmailMessageStatus(long emailId)
        {
            if (!this.SupportsBatches)
            {
                throw new EmailProviderDoesNotSupportBatchesException();
            }
            return this.DoGetEmailMessageStatus(emailId);
        }

        public IEmailMessage GetMessage(long messageId)
        {
            if (!this.SupportsBatches)
            {
                return null;
            }
            return this.DoGetMessage(messageId);
        }

        public void Initialize(ConfigurationBase config)
        {
            this.DoInitialize(config);
        }

        public long RegisterMessageAttachment(IEmailAttachment attachment)
        {
            if (!this.SupportsBatches)
            {
                throw new EmailProviderDoesNotSupportBatchesException();
            }
            return this.DoRegisterEmailAttachment(attachment);
        }

        public long? Send(IEmailMessage message)
        {
            return this.DoSendMessage(message);
        }

        public long SendMessage(string name, string createdBy, IEmailMessage message, DateTime? earliestSendDate)
        {
            if (!this.SupportsBatches)
            {
                throw new EmailProviderDoesNotSupportBatchesException();
            }
            return this.DoSendMessage(name, createdBy, message, earliestSendDate);
        }

        public string ConfigurationName { get; set; }

        public abstract bool SupportsBatches { get; }
    }
}

