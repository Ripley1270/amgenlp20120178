﻿namespace Checkbox.Forms.Items
{
    using System;

    public interface IItemFormatter
    {
        string Format(Item item, string format);
    }
}

