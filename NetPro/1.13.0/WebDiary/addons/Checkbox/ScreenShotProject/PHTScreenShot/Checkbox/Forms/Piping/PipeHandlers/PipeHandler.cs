﻿namespace Checkbox.Forms.Piping.PipeHandlers
{
    using Checkbox.Forms.Piping.Tokens;
    using System;

    public abstract class PipeHandler
    {
        protected PipeHandler()
        {
        }

        public abstract string GetTokenValue(Token token, object context);
    }
}

