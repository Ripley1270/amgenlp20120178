﻿namespace Checkbox.Forms
{
    using Affirma.ThreeSharp;
    using Affirma.ThreeSharp.Model;
    using Affirma.ThreeSharp.Query;
    using Checkbox.Common;
    using Checkbox.Forms.Items;
    using Checkbox.Globalization.Text;
    using Checkbox.Management;
    using Checkbox.Progress;
    using Ionic.Zip;
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Text;

    public static class UploadItemManager
    {
        public static string DetermineContentType(string fileName)
        {
            string extension = Path.GetExtension(fileName);
            if (!string.IsNullOrEmpty(extension))
            {
                switch (extension.ToLower())
                {
                    case ".gif":
                        return "image/gif";

                    case ".png":
                        return "image/x-png";

                    case ".jpg":
                        return "image/jpeg";

                    case ".tif":
                    case ".tiff":
                        return "image/tiff";

                    case ".pdf":
                        return "application/pdf";

                    case ".doc":
                    case ".docx":
                        return "application/msword";

                    case ".xls":
                    case ".xlsx":
                    case ".csv":
                        return "application/ms-excel";
                }
            }
            return string.Empty;
        }

        public static UploadItem GetFileByAnswerID(long answerId)
        {
            Database database = DatabaseFactory.CreateDatabase();
            UploadItem item = null;
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_FileUpload_GetFileByAnswerID");
            storedProcCommandWrapper.AddInParameter("AnswerID", DbType.Int64, answerId);
            DataSet set = database.ExecuteDataSet(storedProcCommandWrapper);
            if (((set == null) || (set.Tables.Count <= 0)) || (set.Tables[0].Rows.Count <= 0))
            {
                return item;
            }
            DataRow tableRow = set.Tables[0].Rows[0];
            string fileName = DbUtility.GetValueFromDataRow<string>(tableRow, "FileName", string.Empty);
            string fileType = DbUtility.GetValueFromDataRow<string>(tableRow, "FileType", string.Empty);
            int fileId = DbUtility.GetValueFromDataRow<int>(tableRow, "FileID", -1);
            byte[] data = null;
            if (UseS3ForUploadedFiles)
            {
                data = GetFileFromS3(fileId, fileName);
            }
            if (((data == null) || (data.Length == 0)) && (tableRow["FileData"] != DBNull.Value))
            {
                data = (byte[]) tableRow["FileData"];
            }
            return new UploadItem(data, fileName, fileType);
        }

        public static int GetFileCount(int responseTemplateId)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_FileUpload_GetFileCountByResponseTemplateID");
            storedProcCommandWrapper.AddInParameter("ResponseTemplateId", DbType.Int32, responseTemplateId);
            return int.Parse(database.ExecuteScalar(storedProcCommandWrapper).ToString());
        }

        public static byte[] GetFileFromS3(int fileId, string fileName)
        {
            return GetFileFromS3(fileId, fileName, ApplicationManager.AppSettings.S3BucketName);
        }

        private static byte[] GetFileFromS3(int fileId, string fileName, string bucketName)
        {
            try
            {
                byte[] buffer;
                if (fileId <= 0)
                {
                    return new byte[0];
                }
                ThreeSharpConfig threeSharpConfig = new ThreeSharpConfig {
                    AwsAccessKeyID = ApplicationManager.AppSettings.S3AccessKeyID,
                    AwsSecretAccessKey = ApplicationManager.AppSettings.S3SecretAccessKey,
                    IsSecure = false
                };
                if (Utilities.IsNotNullOrEmpty(ApplicationManager.AppSettings.S3Endpoint))
                {
                    threeSharpConfig.Server = ApplicationManager.AppSettings.S3Endpoint;
                }
                IThreeSharp sharp = new ThreeSharpQuery(threeSharpConfig);
                using (ObjectGetRequest request = new ObjectGetRequest(bucketName, GetS3FileName(fileId, fileName)))
                {
                    using (ObjectGetResponse response = sharp.ObjectGet(request))
                    {
                        buffer = response.StreamResponseToBytes();
                    }
                }
                return buffer;
            }
            catch (Exception)
            {
                return new byte[0];
            }
        }

        public static byte[] GetFilesAsArchive(int responseTemplateId)
        {
            DataTable filesByResponseTemplateId = GetFilesByResponseTemplateId(responseTemplateId);
            if (filesByResponseTemplateId != null)
            {
                try
                {
                    ZipFile file = new ZipFile();
                    foreach (DataRow row in filesByResponseTemplateId.Rows)
                    {
                        UploadItem fileByAnswerID = GetFileByAnswerID(DbUtility.GetValueFromDataRow<long>(row, "AnswerId", -1L));
                        if (fileByAnswerID != null)
                        {
                            MemoryStream stream = new MemoryStream(fileByAnswerID.Data);
                            file.AddFileStream(GetUploadedFileExportName(row), string.Empty, stream);
                        }
                    }
                    MemoryStream outputStream = new MemoryStream();
                    file.Save(outputStream);
                    return outputStream.ToArray();
                }
                catch (Exception exception)
                {
                    ExceptionPolicy.HandleException(exception, "BusinessProtected");
                    throw;
                }
            }
            return null;
        }

        public static DataTable GetFilesByResponseTemplateId(int responseTemplateId)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_FileUpload_GetFileByResponseTemplateID");
            storedProcCommandWrapper.AddInParameter("ResponseTemplateId", DbType.Int32, responseTemplateId);
            try
            {
                DataSet set = database.ExecuteDataSet(storedProcCommandWrapper);
                if ((set != null) && (set.Tables.Count > 0))
                {
                    return set.Tables[0];
                }
            }
            catch (Exception exception)
            {
                ExceptionPolicy.HandleException(exception, "BusinessProtected");
                throw;
            }
            return null;
        }

        public static string GetS3FileDownloadLink(string fileName)
        {
            return GetS3FileDownloadLink(fileName, ApplicationManager.AppSettings.S3BucketName);
        }

        private static string GetS3FileDownloadLink(string fileName, string bucketName)
        {
            string str;
            ThreeSharpConfig threeSharpConfig = new ThreeSharpConfig {
                AwsAccessKeyID = ApplicationManager.AppSettings.S3AccessKeyID,
                AwsSecretAccessKey = ApplicationManager.AppSettings.S3SecretAccessKey,
                IsSecure = false
            };
            if (Utilities.IsNotNullOrEmpty(ApplicationManager.AppSettings.S3Endpoint))
            {
                threeSharpConfig.Server = ApplicationManager.AppSettings.S3Endpoint;
            }
            IThreeSharp sharp = new ThreeSharpQuery(threeSharpConfig);
            using (UrlGetRequest request = new UrlGetRequest(bucketName, fileName))
            {
                request.ExpiresIn = 0x1b7740L;
                using (UrlGetResponse response = sharp.UrlGet(request))
                {
                    str = response.StreamResponseToString();
                }
            }
            return str;
        }

        public static string GetS3FileName(int fileId, string fileName)
        {
            return string.Format("{0}.{1}.{2}", ApplicationManager.ApplicationDataContext, fileId, fileName);
        }

        public static string GetS3TempFileDownloadLink(string fileName)
        {
            return GetS3FileDownloadLink(fileName, ApplicationManager.AppSettings.S3TempBucketName);
        }

        public static byte[] GetTempFileFromS3(int fileId, string fileName)
        {
            return GetFileFromS3(fileId, fileName, ApplicationManager.AppSettings.S3TempBucketName);
        }

        private static string GetUploadedFileExportName(DataRow row)
        {
            long num = DbUtility.GetValueFromDataRow<long>(row, "ResponseId", -1L);
            long num2 = DbUtility.GetValueFromDataRow<long>(row, "AnswerId", -1L);
            string str = DbUtility.GetValueFromDataRow<string>(row, "FileName", string.Empty);
            StringBuilder builder = new StringBuilder();
            if (num > 0L)
            {
                builder.AppendFormat("{0}_", num);
            }
            if (num2 > 0L)
            {
                builder.AppendFormat("{0}_", num2);
            }
            if (str != string.Empty)
            {
                builder.Append(str);
            }
            return builder.ToString();
        }

        public static string SanitizeFileName(string fileName, string invalidCharReplacement)
        {
            char[] chArray = new char[] { 
                'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 
                'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', 
                '7', '8', '9', '0', '-', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 
                'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '.'
             };
            char[] chArray2 = fileName.ToCharArray(0, fileName.Length);
            string str = string.Empty;
            foreach (char ch in chArray2)
            {
                bool flag = false;
                foreach (char ch2 in chArray)
                {
                    if (ch2 == ch)
                    {
                        str = str + ch.ToString();
                        flag = true;
                    }
                }
                if (!flag)
                {
                    str = str + invalidCharReplacement;
                }
            }
            return str;
        }

        public static void Save(byte[] data, string fileName, string fileType, int itemId, List<long> answerIds)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }
            if (fileName == null)
            {
                throw new ArgumentNullException("fileName");
            }
            if (fileType == null)
            {
                throw new ArgumentNullException("fileType");
            }
            if (answerIds == null)
            {
                throw new ArgumentNullException("answerIds");
            }
            using (IDbConnection connection = DatabaseFactory.CreateDatabase().GetConnection())
            {
                connection.Open();
                IDbTransaction transaction = connection.BeginTransaction();
                try
                {
                    int fileId = SaveToDatabase(transaction, data, fileName, fileType);
                    foreach (long num2 in answerIds)
                    {
                        SaveItemDataRelationship(transaction, itemId, fileId, num2);
                    }
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public static void SaveFilesToDisk(int responseTemplateId)
        {
            SaveFilesToDisk(responseTemplateId, FileUtilities.JoinPathAndFileName(ApplicationManager.AppSettings.UploadItemExportPath, string.Format("UploadedSurveyFiles_{0}", responseTemplateId)), null, null);
        }

        public static void SaveFilesToDisk(int responseTemplateId, string tempFolderPath, string progressKey, string progressMessageLanguageCode)
        {
            string text = null;
            if (Utilities.IsNotNullOrEmpty(progressKey))
            {
                text = TextManager.GetText("/controlText/uploadItemManager/downloadFilesToTempFolder", progressMessageLanguageCode);
                ProgressProvider.SetProgress(progressKey, TextManager.GetText("/controlText/uploadItemManager/generatingFileList", progressMessageLanguageCode), string.Empty, ProgressStatus.Running, 0, 1);
            }
            DataTable filesByResponseTemplateId = GetFilesByResponseTemplateId(responseTemplateId);
            if (filesByResponseTemplateId != null)
            {
                try
                {
                    FileUtilities.CreateFolder(tempFolderPath, false);
                    int count = filesByResponseTemplateId.Rows.Count;
                    for (int i = 0; i < count; i++)
                    {
                        DataRow tableRow = filesByResponseTemplateId.Rows[i];
                        if (Utilities.IsNotNullOrEmpty(progressKey))
                        {
                            string message = text;
                            if ((Utilities.IsNotNullOrEmpty(text) && text.Contains("{0}")) && text.Contains("{1}"))
                            {
                                message = string.Format(text, i + 1, count);
                            }
                            ProgressProvider.SetProgress(progressKey, message, string.Empty, ProgressStatus.Running, (int) (0.8 * i), count);
                        }
                        UploadItem fileByAnswerID = GetFileByAnswerID(DbUtility.GetValueFromDataRow<long>(tableRow, "AnswerId", -1L));
                        if (((fileByAnswerID != null) && (fileByAnswerID.Data != null)) && (fileByAnswerID.Data.Length > 0))
                        {
                            FileUtilities.SaveFile(tempFolderPath, GetUploadedFileExportName(tableRow), fileByAnswerID.Data);
                        }
                    }
                }
                catch (Exception exception)
                {
                    ExceptionPolicy.HandleException(exception, "BusinessProtected");
                    throw;
                }
            }
        }

        public static string SaveFileToS3(string fileName, string filePath)
        {
            byte[] buffer = null;
            using (FileStream stream = File.OpenRead(filePath))
            {
                try
                {
                    buffer = new byte[stream.Length];
                    stream.Read(buffer, 0, buffer.Length);
                }
                finally
                {
                    stream.Close();
                }
            }
            SaveFileToS3(fileName, buffer);
            return GetS3FileDownloadLink(fileName);
        }

        private static void SaveFileToS3(string fileName, byte[] fileBytes)
        {
            SaveFileToS3(fileName, fileBytes, ApplicationManager.AppSettings.S3BucketName);
        }

        private static void SaveFileToS3(string fileName, byte[] fileBytes, string bucketName)
        {
            ThreeSharpConfig threeSharpConfig = new ThreeSharpConfig {
                AwsAccessKeyID = ApplicationManager.AppSettings.S3AccessKeyID,
                AwsSecretAccessKey = ApplicationManager.AppSettings.S3SecretAccessKey,
                IsSecure = false
            };
            if (Utilities.IsNotNullOrEmpty(ApplicationManager.AppSettings.S3Endpoint))
            {
                threeSharpConfig.Server = ApplicationManager.AppSettings.S3Endpoint;
            }
            if (Utilities.IsNotNullOrEmpty(ApplicationManager.AppSettings.S3CallingFormat))
            {
                try
                {
                    threeSharpConfig.Format = (CallingFormat) Enum.Parse(typeof(CallingFormat), ApplicationManager.AppSettings.S3CallingFormat);
                }
                catch
                {
                }
            }
            IThreeSharp sharp = new ThreeSharpQuery(threeSharpConfig);
            using (ObjectAddRequest request = new ObjectAddRequest(bucketName, fileName))
            {
                request.LoadStreamWithBytes(fileBytes, DetermineContentType(fileName));
                sharp.ObjectAdd(request);
            }
        }

        private static void SaveItemDataRelationship(IDbTransaction transaction, int itemId, int fileId, long answerId)
        {
            if (transaction == null)
            {
                throw new ArgumentNullException("transaction");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_UpsertFileUploadFiles");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, itemId);
            storedProcCommandWrapper.AddInParameter("FileID", DbType.Int32, fileId);
            storedProcCommandWrapper.AddInParameter("AnswerID", DbType.Int64, answerId);
            database.ExecuteNonQuery(storedProcCommandWrapper, transaction);
        }

        public static string SaveTempFileToS3(string fileName, string filePath)
        {
            byte[] buffer = null;
            using (FileStream stream = File.OpenRead(filePath))
            {
                try
                {
                    buffer = new byte[stream.Length];
                    stream.Read(buffer, 0, buffer.Length);
                }
                finally
                {
                    stream.Close();
                }
            }
            SaveTempFileToS3(fileName, buffer);
            return GetS3TempFileDownloadLink(fileName);
        }

        public static void SaveTempFileToS3(string fileName, byte[] fileBytes)
        {
            SaveFileToS3(fileName, fileBytes, ApplicationManager.AppSettings.S3TempBucketName);
        }

        private static int SaveToDatabase(IDbTransaction transaction, byte[] data, string fileName, string fileType)
        {
            if (transaction == null)
            {
                throw new ArgumentNullException("transaction");
            }
            if (fileName == null)
            {
                throw new ArgumentNullException("fileName");
            }
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }
            if (fileType == null)
            {
                throw new ArgumentNullException("fileType");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_FileUpload_InsertFile");
            if (UseS3ForUploadedFiles)
            {
                fileName = SanitizeFileName(fileName, ".");
                storedProcCommandWrapper.AddInParameter("FileData", DbType.Binary, DBNull.Value);
            }
            else
            {
                storedProcCommandWrapper.AddInParameter("FileData", DbType.Binary, data);
            }
            storedProcCommandWrapper.AddInParameter("FileName", DbType.String, fileName);
            storedProcCommandWrapper.AddInParameter("FileType", DbType.String, fileType);
            storedProcCommandWrapper.AddInParameter("FileSize", DbType.Int32, data.Length);
            storedProcCommandWrapper.AddOutParameter("FileID", DbType.Int32, 4);
            database.ExecuteNonQuery(storedProcCommandWrapper, transaction);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("FileID");
            if ((parameterValue == null) || (parameterValue == DBNull.Value))
            {
                throw new Exception("An error occurred while creating entry for file data.  ID returned was null.");
            }
            if (UseS3ForUploadedFiles)
            {
                SaveFileToS3(GetS3FileName((int) parameterValue, fileName), data);
            }
            return (int) parameterValue;
        }

        public static void UpdateAllowedFileTypes(IList<string> fileTypes)
        {
            if (fileTypes == null)
            {
                throw new ArgumentNullException("fileTypes");
            }
            List<string> allAllowedFileTypes = AllAllowedFileTypes;
            List<string> list2 = new List<string>();
            List<string> list3 = new List<string>();
            foreach (string str in allAllowedFileTypes)
            {
                if (!fileTypes.Contains(str))
                {
                    list2.Add(str);
                }
            }
            foreach (string str2 in fileTypes)
            {
                if (!allAllowedFileTypes.Contains(str2))
                {
                    list3.Add(str2);
                }
            }
            Database database = DatabaseFactory.CreateDatabase();
            using (IDbConnection connection = database.GetConnection())
            {
                connection.Open();
                IDbTransaction transaction = connection.BeginTransaction();
                try
                {
                    foreach (string str3 in list2)
                    {
                        DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_FileUpload_RemoveAllowedFileType");
                        storedProcCommandWrapper.AddInParameter("TypeExtension", DbType.String, str3);
                        database.ExecuteNonQuery(storedProcCommandWrapper, transaction);
                    }
                    foreach (string str4 in list3)
                    {
                        DBCommandWrapper command = database.GetStoredProcCommandWrapper("ckbx_FileUpload_AddAllowedFileType");
                        command.AddInParameter("TypeExtension", DbType.String, str4);
                        command.AddInParameter("TypeDescription", DbType.String, string.Empty);
                        database.ExecuteNonQuery(command, transaction);
                    }
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public static bool ValidateDownloadDirectory(string directory)
        {
            if (Utilities.IsNullOrEmpty(directory))
            {
                return false;
            }
            byte[] buffer2 = new byte[3];
            buffer2[1] = 1;
            buffer2[2] = 2;
            byte[] data = buffer2;
            string fileName = (directory.EndsWith(@"\") || directory.EndsWith("/")) ? string.Format("{0}_AccessValidation.txt", DateTime.Now.Ticks) : string.Format("/{0}_AccessValidation.txt", DateTime.Now.Ticks);
            string path = FileUtilities.JoinPathAndFileName(directory, fileName);
            try
            {
                FileUtilities.SaveFile(directory, fileName, data);
                FileUtilities.DeleteFile(path);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static List<string> AllAllowedFileTypes
        {
            get
            {
                List<string> list = new List<string>();
                Database database = DatabaseFactory.CreateDatabase();
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_FileUpload_GetAllowedFileTypes");
                using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
                {
                    try
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                list.Add((string) reader["TypeExtension"]);
                            }
                        }
                        catch (Exception exception)
                        {
                            ExceptionPolicy.HandleException(exception, "BusinessPublic");
                        }
                        return list;
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
                return list;
            }
        }

        public static bool UseS3ForTempFiles
        {
            get
            {
                if (!ApplicationManager.AppSettings.UseS3ForTempFiles)
                {
                    return false;
                }
                if (Utilities.IsNullOrEmpty(ApplicationManager.AppSettings.S3AccessKeyID))
                {
                    ExceptionPolicy.HandleException(new Exception("S3 upload file storage is enabled, but S3AccessKeyID is not specified."), "BusinessPublic");
                    return false;
                }
                if (Utilities.IsNullOrEmpty(ApplicationManager.AppSettings.S3SecretAccessKey))
                {
                    ExceptionPolicy.HandleException(new Exception("S3 upload file storage is enabled, but S3SecretAccessKey is not specified."), "BusinessPublic");
                    return false;
                }
                if (Utilities.IsNullOrEmpty(ApplicationManager.AppSettings.S3TempBucketName))
                {
                    ExceptionPolicy.HandleException(new Exception("S3 upload file storage is enabled, but S3TempBucketName is not specified."), "BusinessPublic");
                    return false;
                }
                return true;
            }
        }

        public static bool UseS3ForUploadedFiles
        {
            get
            {
                if (!ApplicationManager.AppSettings.UseS3ForUploadedFiles)
                {
                    return false;
                }
                if (Utilities.IsNullOrEmpty(ApplicationManager.AppSettings.S3AccessKeyID))
                {
                    ExceptionPolicy.HandleException(new Exception("S3 upload file storage is enabled, but S3AccessKeyID is not specified."), "BusinessPublic");
                    return false;
                }
                if (Utilities.IsNullOrEmpty(ApplicationManager.AppSettings.S3SecretAccessKey))
                {
                    ExceptionPolicy.HandleException(new Exception("S3 upload file storage is enabled, but S3SecretAccessKey is not specified."), "BusinessPublic");
                    return false;
                }
                if (Utilities.IsNullOrEmpty(ApplicationManager.AppSettings.S3BucketName))
                {
                    ExceptionPolicy.HandleException(new Exception("S3 upload file storage is enabled, but S3BucketName is not specified."), "BusinessPublic");
                    return false;
                }
                return true;
            }
        }
    }
}

