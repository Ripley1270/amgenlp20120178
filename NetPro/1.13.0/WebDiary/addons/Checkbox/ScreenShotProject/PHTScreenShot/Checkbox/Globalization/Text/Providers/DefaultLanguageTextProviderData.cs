﻿namespace Checkbox.Globalization.Text.Providers
{
    using Prezza.Framework.Common;
    using Prezza.Framework.Configuration;
    using System;
    using System.Runtime.CompilerServices;
    using System.Xml;

    public class DefaultLanguageTextProviderData : ProviderData, IXmlConfigurationBase
    {
        public DefaultLanguageTextProviderData(string providerName) : base(providerName, typeof(DefaultLanguageTextProvider).AssemblyQualifiedName)
        {
            ArgumentValidation.CheckForEmptyString(providerName, "providerName");
        }

        public void LoadFromXml(XmlNode providerNode)
        {
            ArgumentValidation.CheckForNullReference(providerNode, "providerNode");
            XmlNode node = providerNode.SelectSingleNode("defaultLanguage");
            if ((node == null) || (this.DefaultLanguage == string.Empty))
            {
                throw new Exception("No default language was specified in configuration for text provider:  " + providerNode.Name);
            }
            this.DefaultLanguage = node.InnerText;
            XmlNode node2 = providerNode.SelectSingleNode("cacheManager");
            this.CacheManagerName = (node2 == null) ? string.Empty : node2.InnerText;
        }

        public string CacheManagerName { get; set; }

        public string DefaultLanguage { get; private set; }
    }
}

