﻿namespace Checkbox.Forms.Items.UI
{
    using System;

    [Serializable]
    public class MatrixDropDownList : MatrixSelectLayout
    {
        public override string AppearanceCode
        {
            get
            {
                return "MATRIX_DROPDOWN_LIST";
            }
        }
    }
}

