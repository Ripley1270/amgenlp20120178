﻿namespace Checkbox.Forms.Items.Configuration
{
    using Prezza.Framework.Common;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class DisplayResponseItemTextDecorator : LocalizableResponseItemTextDecorator
    {
        private string _linkText;
        private bool _linkTextChanged;

        public DisplayResponseItemTextDecorator(DisplayResponseItemData data, string language) : base(data, language)
        {
            this._linkText = string.Empty;
            this._linkTextChanged = false;
        }

        protected override void CopyLocalizedText(ItemData data)
        {
            base.CopyLocalizedText(data);
            ArgumentValidation.CheckExpectedType(data, typeof(DisplayResponseItemData));
            string linkTextID = ((DisplayResponseItemData) data).LinkTextID;
            if ((linkTextID != null) && (linkTextID.Trim() != string.Empty))
            {
                Dictionary<string, string> allTexts = this.GetAllTexts(linkTextID);
                foreach (string str2 in allTexts.Keys)
                {
                    this.SetText(this.Data.LinkTextID, allTexts[str2], str2);
                }
            }
        }

        protected override void SetLocalizedTexts()
        {
            base.SetLocalizedTexts();
            if (this._linkTextChanged)
            {
                this.SetText(this.Data.LinkTextID, this._linkText);
            }
        }

        public DisplayResponseItemData Data
        {
            get
            {
                return (DisplayResponseItemData) base.Data;
            }
        }

        public string LinkText
        {
            get
            {
                if (!this._linkTextChanged && (this.Data.LinkTextID != string.Empty))
                {
                    return this.GetText(this.Data.LinkTextID);
                }
                return this._linkText;
            }
            set
            {
                this._linkText = value;
                this._linkTextChanged = true;
            }
        }
    }
}

