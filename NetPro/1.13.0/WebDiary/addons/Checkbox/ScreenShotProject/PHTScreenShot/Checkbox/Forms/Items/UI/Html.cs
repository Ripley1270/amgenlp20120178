﻿namespace Checkbox.Forms.Items.UI
{
    using System;

    [Serializable]
    public class Html : AppearanceData
    {
        public override string AppearanceCode
        {
            get
            {
                return "HTML";
            }
        }
    }
}

