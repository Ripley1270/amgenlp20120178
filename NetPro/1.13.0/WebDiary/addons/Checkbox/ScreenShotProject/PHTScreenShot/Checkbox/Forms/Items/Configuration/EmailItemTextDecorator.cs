﻿namespace Checkbox.Forms.Items.Configuration
{
    using Prezza.Framework.Common;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class EmailItemTextDecorator : LocalizableResponseItemTextDecorator
    {
        private string _body;
        private string _subject;
        private bool _subjectModified;

        public EmailItemTextDecorator(EmailItemData data, string language) : base(data, language)
        {
        }

        protected override void CopyLocalizedText(ItemData data)
        {
            base.CopyLocalizedText(data);
            ArgumentValidation.CheckExpectedType(data, typeof(EmailItemData));
            string subjectTextID = ((EmailItemData) data).SubjectTextID;
            if ((subjectTextID != null) && (subjectTextID.Trim() != string.Empty))
            {
                Dictionary<string, string> allTexts = this.GetAllTexts(subjectTextID);
                foreach (string str2 in allTexts.Keys)
                {
                    this.SetText(this.Data.SubjectTextID, allTexts[str2], str2);
                }
            }
            string bodyTextID = ((EmailItemData) data).BodyTextID;
            if ((bodyTextID != null) && (bodyTextID.Trim() != string.Empty))
            {
                Dictionary<string, string> dictionary2 = this.GetAllTexts(bodyTextID);
                foreach (string str4 in dictionary2.Keys)
                {
                    this.SetText(this.Data.BodyTextID, dictionary2[str4], str4);
                }
            }
        }

        protected override void SetLocalizedTexts()
        {
            if (this.Data.BodyTextID != string.Empty)
            {
                this.SetText(this.Data.BodyTextID, this.Body);
            }
            if (this.Data.SubjectTextID != string.Empty)
            {
                this.SetText(this.Data.SubjectTextID, this.Subject);
            }
        }

        public string Body
        {
            get
            {
                if ((this.Data.BodyTextID != string.Empty) && !this._subjectModified)
                {
                    return this.GetText(this.Data.BodyTextID);
                }
                return this._body;
            }
            set
            {
                this._body = value;
            }
        }

        public EmailItemData Data
        {
            get
            {
                return (EmailItemData) base.Data;
            }
        }

        public string Subject
        {
            get
            {
                if ((this.Data.SubjectTextID != string.Empty) && !this._subjectModified)
                {
                    return this.GetText(this.Data.SubjectTextID);
                }
                return this._subject;
            }
            set
            {
                this._subject = value;
                this._subjectModified = true;
            }
        }
    }
}

