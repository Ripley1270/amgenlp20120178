﻿namespace Checkbox.Forms.Items.Configuration
{
    using Checkbox.Globalization.Text;
    using System;
    using System.Data;

    [Serializable]
    public abstract class LabelledItemData : LocalizableResponseItemData
    {
        protected LabelledItemData()
        {
        }

        public override ItemTextDecorator CreateTextDecorator(string languageCode)
        {
            return new LabelledItemTextDecorator(this, languageCode);
        }

        public override DataSet LoadTextData()
        {
            DataSet ds = base.LoadTextData();
            this.MergeTextData(ds, TextManager.GetTextData(this.TextID), this.TextTableName, "text", this.TextIDPrefix, base.ID.Value);
            this.MergeTextData(ds, TextManager.GetTextData(this.SubTextID), this.TextTableName, "subText", this.TextIDPrefix, base.ID.Value);
            return ds;
        }

        public virtual string SubTextID
        {
            get
            {
                return this.GetTextID("subText");
            }
        }

        public virtual string TextID
        {
            get
            {
                return this.GetTextID("text");
            }
        }
    }
}

