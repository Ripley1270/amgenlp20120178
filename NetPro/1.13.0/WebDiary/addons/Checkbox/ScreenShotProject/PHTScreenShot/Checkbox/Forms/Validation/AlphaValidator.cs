﻿namespace Checkbox.Forms.Validation
{
    using Checkbox.Globalization.Text;
    using System;

    public class AlphaValidator : RegularExpressionValidator
    {
        public AlphaValidator()
        {
            base._regex = @"^[a-zA-Z\s]+$";
        }

        public override string GetMessage(string languageCode)
        {
            return TextManager.GetText("/validationMessages/regex/alpha", languageCode);
        }
    }
}

