﻿namespace Checkbox.Forms.Items.Configuration
{
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class RatingScaleItemTextDecorator : SelectItemTextDecorator
    {
        private string _endText;
        private bool _endTextModified;
        private string _midText;
        private bool _midTextModified;
        private string _notApplicableText;
        private bool _notApplicableTextModified;
        private string _startText;
        private bool _startTextModified;

        public RatingScaleItemTextDecorator(RatingScaleItemData itemData, string language) : base(itemData, language)
        {
            this._startText = string.Empty;
            this._midText = string.Empty;
            this._endText = string.Empty;
            this._notApplicableText = string.Empty;
            this._startTextModified = false;
            this._midTextModified = false;
            this._endTextModified = false;
            this._notApplicableTextModified = false;
        }

        protected override void CopyLocalizedText(ItemData data)
        {
            base.CopyLocalizedText(data);
            if (this.Data.StartTextID != string.Empty)
            {
                Dictionary<string, string> allTexts = this.GetAllTexts(((RatingScaleItemData) data).StartTextID);
                if (allTexts != null)
                {
                    foreach (string str in allTexts.Keys)
                    {
                        this.SetText(this.Data.StartTextID, allTexts[str], str);
                    }
                }
            }
            if (this.Data.MidTextID != string.Empty)
            {
                Dictionary<string, string> dictionary2 = this.GetAllTexts(((RatingScaleItemData) data).MidTextID);
                if (dictionary2 != null)
                {
                    foreach (string str2 in dictionary2.Keys)
                    {
                        this.SetText(this.Data.MidTextID, dictionary2[str2], str2);
                    }
                }
            }
            if (this.Data.EndTextID != string.Empty)
            {
                Dictionary<string, string> dictionary3 = this.GetAllTexts(((RatingScaleItemData) data).EndTextID);
                if (dictionary3 != null)
                {
                    foreach (string str3 in dictionary3.Keys)
                    {
                        this.SetText(this.Data.EndTextID, dictionary3[str3], str3);
                    }
                }
            }
            if (this.Data.EndTextID != string.Empty)
            {
                Dictionary<string, string> dictionary4 = this.GetAllTexts(((RatingScaleItemData) data).NotApplicableTextID);
                if (dictionary4 != null)
                {
                    foreach (string str4 in dictionary4.Keys)
                    {
                        this.SetText(this.Data.NotApplicableTextID, dictionary4[str4], str4);
                    }
                }
            }
        }

        public void PopulateOptionTexts()
        {
            foreach (ListOptionData data in this.Data.Options)
            {
                if (data.IsOther)
                {
                    base.SetOptionText(data.Position, this.GetText(this.Data.NotApplicableTextID));
                }
                else
                {
                    base.SetOptionText(data.Position, data.Points.ToString());
                }
            }
        }

        protected override void SetLocalizedTexts()
        {
            if (this._startTextModified)
            {
                this.SetText(this.Data.StartTextID, this._startText);
            }
            if (this._midTextModified)
            {
                this.SetText(this.Data.MidTextID, this._midText);
            }
            if (this._endTextModified)
            {
                this.SetText(this.Data.EndTextID, this._endText);
            }
            if (this._notApplicableTextModified)
            {
                this.SetText(this.Data.NotApplicableTextID, this._notApplicableText);
            }
            this.PopulateOptionTexts();
            base.SetLocalizedTexts();
        }

        public RatingScaleItemData Data
        {
            get
            {
                return (RatingScaleItemData) base.Data;
            }
        }

        public string EndText
        {
            get
            {
                if ((this.Data.EndTextID != string.Empty) && !this._endTextModified)
                {
                    return this.GetText(this.Data.EndTextID);
                }
                return this._endText;
            }
            set
            {
                this._endText = value;
                this._endTextModified = true;
            }
        }

        public string MidText
        {
            get
            {
                if ((this.Data.MidTextID != string.Empty) && !this._midTextModified)
                {
                    return this.GetText(this.Data.MidTextID);
                }
                return this._midText;
            }
            set
            {
                this._midText = value;
                this._midTextModified = true;
            }
        }

        public string NotApplicableText
        {
            get
            {
                if ((this.Data.NotApplicableTextID != string.Empty) && !this._notApplicableTextModified)
                {
                    return this.GetText(this.Data.NotApplicableTextID);
                }
                return this._notApplicableText;
            }
            set
            {
                this._notApplicableText = value;
                this._notApplicableTextModified = true;
            }
        }

        public string StartText
        {
            get
            {
                if ((this.Data.StartTextID != string.Empty) && !this._startTextModified)
                {
                    return this.GetText(this.Data.StartTextID);
                }
                return this._startText;
            }
            set
            {
                this._startText = value;
                this._startTextModified = true;
            }
        }
    }
}

