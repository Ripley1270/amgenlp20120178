﻿namespace Checkbox.Analytics.Items
{
    using Checkbox.Analytics.Data;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class FrequencyAnalysisDataCollection
    {
        private Dictionary<int, FrequencyAnalysisData> _data = new Dictionary<int, FrequencyAnalysisData>();

        public void AddAnalysisData(int itemID, FrequencyAnalysisData analysisData)
        {
            this._data[itemID] = analysisData;
        }

        public AnalysisAnswerData GetAggregateData()
        {
            throw new Exception("Method not supported.");
        }

        public FrequencyAnalysisData GetAnalysisData(int itemID)
        {
            if (this._data.ContainsKey(itemID))
            {
                return this._data[itemID];
            }
            return null;
        }

        public void RemoveAnalysisData(int itemID)
        {
            if (this._data.ContainsKey(itemID))
            {
                this._data.Remove(itemID);
            }
        }
    }
}

