﻿namespace Checkbox.Forms.Security.Providers
{
    using Checkbox.Forms;
    using Checkbox.Forms.Security.Principal;
    using Checkbox.Invitations;
    using Checkbox.Security;
    using Checkbox.Users;
    using Prezza.Framework.Common;
    using Prezza.Framework.Configuration;
    using Prezza.Framework.ExceptionHandling;
    using Prezza.Framework.Logging;
    using Prezza.Framework.Security;
    using Prezza.Framework.Security.Principal;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Security.Principal;

    public class ResponseTemplateAuthorizationProvider : IAuthorizationProvider, IConfigurationProvider
    {
        private string _lastError;

        public bool Authorize(ExtendedPrincipal principal, string context)
        {
            ArgumentValidation.CheckForEmptyString(context, "context");
            ArgumentValidation.CheckForNullReference(principal, "principal");
            string[] roles = principal.GetRoles();
            for (int i = 0; i < roles.Length; i++)
            {
                if (roles[i] == "System Administrator")
                {
                    return true;
                }
                if (RoleManager.GetRole(roles[i]).HasPermission(context))
                {
                    return true;
                }
            }
            return false;
        }

        public virtual bool Authorize(ExtendedPrincipal principal, IAccessControllable resource, string context)
        {
            return this.Authorize(principal, resource, context, null);
        }

        public virtual bool Authorize(ExtendedPrincipal principal, IAccessControllable resource, string context, Guid? recipientGuid)
        {
            try
            {
                ArgumentValidation.CheckExpectedType(resource, typeof(ResponseTemplate));
                ArgumentValidation.CheckForNullReference(resource, "resource");
                ArgumentValidation.CheckForEmptyString(context, "context");
                if (principal == null)
                {
                    throw new Exception("Unable to authorize null principal for form.");
                }
                if (principal.IsInRole("System Administrator"))
                {
                    return true;
                }
                ResponseTemplate template = (ResponseTemplate) resource;
                bool flag = false;
                if (string.Compare(context, "Form.Fill", true) == 0)
                {
                    if (!template.IsActive)
                    {
                        this._lastError = "notactive";
                        return false;
                    }
                    DateTime now = DateTime.Now;
                    if (template.ActivationStartDate.HasValue)
                    {
                        DateTime? activationStartDate = template.ActivationStartDate;
                        DateTime time2 = now;
                        if (activationStartDate.HasValue ? (activationStartDate.GetValueOrDefault() > time2) : false)
                        {
                            this._lastError = "beforestartdate";
                            return false;
                        }
                    }
                    if (template.ActivationEndDate.HasValue)
                    {
                        DateTime? activationEndDate = template.ActivationEndDate;
                        DateTime time3 = now;
                        if (activationEndDate.HasValue ? (activationEndDate.GetValueOrDefault() < time3) : false)
                        {
                            this._lastError = "afterenddate";
                            return false;
                        }
                    }
                    if (((template.SecurityType == SecurityType.AccessControlList) || (template.SecurityType == SecurityType.AllRegisteredUsers)) && (principal is AnonymousRespondent))
                    {
                        this._lastError = "loginrequired";
                        return false;
                    }
                    if ((template.SecurityType == SecurityType.InvitationOnly) && !this.ValidateInvitationRecipient(recipientGuid, template.GUID))
                    {
                        return false;
                    }
                    if (!ResponseTemplateManager.MoreResponsesAllowed(template.ID.Value, template.MaxTotalResponses, template.MaxResponsesPerUser, principal, template.AnonymizeResponses))
                    {
                        flag = true;
                    }
                }
                if (!template.AllowEdit && flag)
                {
                    this._lastError = "responselimitreached";
                    return false;
                }
                if (!this.Authorize(principal, context))
                {
                    Logger.Write("Authorization failed for " + principal.Identity.Name + " to " + context + " failed.", "Warning", 1, -1, Severity.Warning);
                    this._lastError = "notAuthorized";
                    return false;
                }
                if (resource.ACL != null)
                {
                    if (resource.ACL.IsInList(principal))
                    {
                        if (!CheckPolicy(resource.ACL.GetPolicy(principal), context, principal))
                        {
                            this._lastError = "notAuthorized";
                            return false;
                        }
                        if (flag)
                        {
                            this._lastError = "editonly";
                            return false;
                        }
                        return true;
                    }
                    List<Group> groupMemberships = Group.GetGroupMemberships(principal);
                    bool flag2 = false;
                    int num = 0;
                    foreach (Group group in groupMemberships)
                    {
                        if ((group != null) && resource.ACL.IsInList(group))
                        {
                            num++;
                            if (CheckPolicy(resource.ACL.GetPolicy(group), context, principal))
                            {
                                flag2 = true;
                            }
                        }
                    }
                    if (resource.ACL.IsInList(Group.Everyone))
                    {
                        num++;
                        if (CheckPolicy(resource.ACL.GetPolicy(Group.Everyone), context, principal))
                        {
                            flag2 = true;
                        }
                    }
                    if (num > 0)
                    {
                        if (flag2)
                        {
                            if (flag)
                            {
                                this._lastError = "editonly";
                                return false;
                            }
                            return true;
                        }
                        this._lastError = "notAuthorized";
                        return false;
                    }
                }
                if (CheckPolicy(resource.DefaultPolicy, context, principal))
                {
                    if (flag)
                    {
                        this._lastError = "editonly";
                        return false;
                    }
                    return true;
                }
                this._lastError = "notAuthorized";
                return false;
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
                return false;
            }
        }

        private static bool CheckPolicy(Role policy, string context, IPrincipal principal)
        {
            if ((policy == null) || !policy.HasPermission(context))
            {
                if (principal != null)
                {
                    Logger.Write("Authorization failed for " + principal.Identity.Name + " to " + context + ".", "Warning", 1, -1, Severity.Warning);
                }
                else
                {
                    Logger.Write("Authorization failed for null principal to " + context + ".", "Warning", 1, -1, Severity.Warning);
                }
                return false;
            }
            if (principal != null)
            {
                Logger.Write("Authorization succeeded for " + principal.Identity.Name + " to " + context + ".", "Info", 1, -1, Severity.Information);
            }
            else
            {
                Logger.Write("Authorization succeeded for null principal to " + context + ".", "Warning", 1, -1, Severity.Warning);
            }
            return true;
        }

        public string GetAuthorizationErrorType()
        {
            return this._lastError;
        }

        public virtual void Initialize(ConfigurationBase config)
        {
        }

        protected virtual bool ValidateInvitationRecipient(Guid? invitationRecipient, Guid rtGuid)
        {
            if (!invitationRecipient.HasValue)
            {
                this.LastError = "noInvitation";
                return false;
            }
            Guid? responseTemplateGuidForInvitation = InvitationManager.GetResponseTemplateGuidForInvitation(invitationRecipient.Value);
            if (responseTemplateGuidForInvitation.HasValue && (responseTemplateGuidForInvitation.Value == rtGuid))
            {
                return true;
            }
            this.LastError = "invalidInvitation";
            return false;
        }

        public string ConfigurationName { get; set; }

        protected string LastError
        {
            get
            {
                return this._lastError;
            }
            set
            {
                this._lastError = value;
            }
        }
    }
}

