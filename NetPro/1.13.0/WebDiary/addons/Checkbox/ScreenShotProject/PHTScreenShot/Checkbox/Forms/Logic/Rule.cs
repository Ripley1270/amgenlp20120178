﻿namespace Checkbox.Forms.Logic
{
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable]
    public sealed class Rule
    {
        [NonSerialized]
        private EventHandlerList _eventHandlers;
        private static readonly object EventBeginRun = new object();
        private static readonly object EventEndRun = new object();
        private static readonly object EventRuleChanged = new object();

        public event EventHandler BeginRun
        {
            add
            {
                this.Events.AddHandler(EventBeginRun, value);
            }
            remove
            {
                this.Events.RemoveHandler(EventBeginRun, value);
            }
        }

        public event EventHandler EndRun
        {
            add
            {
                this.Events.AddHandler(EventEndRun, value);
            }
            remove
            {
                this.Events.RemoveHandler(EventEndRun, value);
            }
        }

        public event EventHandler RuleChanged
        {
            add
            {
                this.Events.AddHandler(EventRuleChanged, value);
            }
            remove
            {
                this.Events.RemoveHandler(EventRuleChanged, value);
            }
        }

        public Rule(Checkbox.Forms.Logic.Expression expression, RuleEventTrigger trigger, params Checkbox.Forms.Logic.Action[] actions)
        {
            this.Expression = expression;
            this.Actions = actions;
            this.Trigger = trigger;
        }

        private void OnBeginRun()
        {
            EventHandler handler = (EventHandler) this.Events[EventBeginRun];
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        private void OnEndRun()
        {
            EventHandler handler = (EventHandler) this.Events[EventEndRun];
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        public bool Run()
        {
            this.OnBeginRun();
            bool directive = this.Expression.Evaluate(this.Trigger);
            foreach (Checkbox.Forms.Logic.Action action in this.Actions)
            {
                action.Execute(directive);
            }
            this.OnEndRun();
            return directive;
        }

        public Checkbox.Forms.Logic.Action[] Actions { get; private set; }

        private EventHandlerList Events
        {
            get
            {
                if (this._eventHandlers == null)
                {
                    this._eventHandlers = new EventHandlerList();
                }
                return this._eventHandlers;
            }
        }

        public Checkbox.Forms.Logic.Expression Expression { get; private set; }

        public RuleEventTrigger Trigger { get; private set; }
    }
}

