﻿namespace Checkbox.Management
{
    using Checkbox.Forms.Data;
    using Checkbox.Security.Data;
    using Prezza.Framework.Data;
    using Prezza.Framework.Data.QueryCriteria;
    using Prezza.Framework.Data.QueryParameters;
    using Prezza.Framework.Security.Principal;
    using System;

    public class SyncPermissionManager
    {
        private SyncPermissionManager()
        {
        }

        public static SelectQuery GetSynchableFolders(ExtendedPrincipal currentPrincipal)
        {
            if (currentPrincipal.IsInRole("System Administrator"))
            {
                return Checkbox.Forms.Data.QueryFactory.GetAllFormFoldersQuery();
            }
            return Checkbox.Security.Data.QueryFactory.GetSelectEntitiesWithPermissionQuery("ckbx_TemplatesAndFoldersView", "ItemID", "AclID", currentPrincipal.AclTypeIdentifier, currentPrincipal.AclEntryIdentifier, "FormFolder.Read", "DefaultPolicy");
        }

        public static SelectQuery GetSynchableForms(ExtendedPrincipal currentPrincipal)
        {
            if (currentPrincipal.IsInRole("System Administrator"))
            {
                SelectQuery query = new SelectQuery("ckbx_Template");
                query.AddAllParameter("ckbx_Template");
                query.AddTableJoin("ckbx_ResponseTemplate", QueryJoinType.Inner, "ResponseTemplateID", "ckbx_Template", "TemplateID");
                query.AddAllParameter("ckbx_ResponseTemplate");
                CriteriaCollection criteria = new CriteriaCollection(CriteriaJoinType.Or);
                criteria.AddCriterion(new QueryCriterion(new SelectParameter("Deleted", string.Empty, "ckbx_Template"), CriteriaOperator.Is, new LiteralParameter("NULL")));
                criteria.AddCriterion(new QueryCriterion(new SelectParameter("Deleted", string.Empty, "ckbx_Template"), CriteriaOperator.EqualTo, new LiteralParameter(0)));
                query.AddCriteriaCollection(criteria);
                return query;
            }
            SelectQuery query2 = Checkbox.Security.Data.QueryFactory.GetSelectEntitiesWithPermissionQuery("ckbx_Template", "TemplateID", "AclID", currentPrincipal.AclTypeIdentifier, currentPrincipal.AclEntryIdentifier, "Form.Fill", "DefaultPolicy");
            query2.AddTableJoin("ckbx_ResponseTemplate", QueryJoinType.Inner, "ResponseTemplateID", "ckbx_Template", "TemplateID");
            query2.AddParameter(new SelectAllParameter("ckbx_ResponseTemplate"));
            return query2;
        }
    }
}

