﻿namespace Checkbox.Forms.Logic.Configuration
{
    using Checkbox.Forms;
    using Checkbox.Forms.Logic;
    using Checkbox.Globalization.Text;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Runtime.CompilerServices;
    using System.Text;

    [Serializable]
    public class CompositeExpressionData : ExpressionData
    {
        private List<ExpressionData> _children;
        private List<ExpressionData> _deletedChildren;

        public CompositeExpressionData(ResponseTemplate context) : this(context, new List<ExpressionData>(), LogicalConnector.AND)
        {
        }

        public CompositeExpressionData(ResponseTemplate context, List<ExpressionData> children, LogicalConnector connector) : base(context)
        {
            this._children = children;
            this.Connector = connector;
        }

        public void Add(ExpressionData child)
        {
            child.Parent = this;
            child.ValidationFailed += new EventHandler(this.child_ValidationFailed);
            this.Children.Add(child);
        }

        private void child_ValidationFailed(object sender, EventArgs e)
        {
            this.OnValidationFailed();
        }

        protected override void Create(IDbTransaction transaction)
        {
            DataRow row = base.Context.ExpressionTable.NewRow();
            row["Operator"] = 0;
            row["LeftOperand"] = DBNull.Value;
            row["RightOperand"] = DBNull.Value;
            int num = 0;
            StringBuilder builder = new StringBuilder();
            builder.Append("/");
            CompositeExpressionData parent = this.Parent;
            CompositeExpressionData data2 = null;
            while (parent != null)
            {
                num++;
                builder.Insert(0, "/" + parent.ID.Value);
                if (parent.Parent == null)
                {
                    data2 = parent;
                }
                parent = parent.Parent;
            }
            row["Depth"] = num;
            row["Lineage"] = builder.ToString();
            if (data2 != null)
            {
                row["Root"] = data2.ID.Value;
            }
            if (this.Parent != null)
            {
                row["Parent"] = this.Parent.ID.Value;
            }
            row["ChildRelation"] = this.Connector.ToString();
            base.Context.ExpressionTable.Rows.Add(row);
            base.ID = new int?((int) row["ExpressionID"]);
        }

        internal override Expression CreateExpression(Response response, string languageCode)
        {
            CompositeExpression expression = new CompositeExpression(new List<Expression>(), this.Connector);
            foreach (ExpressionData data in this.Children)
            {
                expression.Add(data.CreateExpression(response, languageCode));
            }
            return expression;
        }

        public override void Delete(IDbTransaction transaction)
        {
            foreach (ExpressionData data in this.Children)
            {
                base.TransactionAborted += new EventHandler(data.NotifyAbort);
                base.TransactionCommitted += new EventHandler(data.NotifyCommit);
                data.Delete(transaction);
            }
            foreach (ExpressionData data2 in this.DeletedChildren)
            {
                base.TransactionAborted += new EventHandler(data2.NotifyAbort);
                base.TransactionCommitted += new EventHandler(data2.NotifyCommit);
                data2.Delete(transaction);
            }
            base.Delete(transaction);
        }

        public override void Load(DataSet data)
        {
            try
            {
                if (!data.Tables.Contains(this.DataTableName))
                {
                    throw new ApplicationException("Cannot find DataTable for Expression");
                }
                DataRow[] rowArray = data.Tables[this.DataTableName].Select(this.IdentityColumnName + " = " + base.ID);
                if (rowArray.Length <= 0)
                {
                    throw new ApplicationException("Could not find CompositeExpressionData");
                }
                this.Connector = (LogicalConnector) Enum.Parse(typeof(LogicalConnector), (string) rowArray[0]["ChildRelation"]);
                List<DataRow> list = new List<DataRow>();
                DataRow[] collection = data.Tables[this.DataTableName].Select("[Parent] = " + base.ID.Value + " AND ExpressionID > 0", "ExpressionID ASC");
                DataRow[] rowArray3 = data.Tables[this.DataTableName].Select("[Parent] = " + base.ID.Value + " AND ExpressionID < 0", "ExpressionID DESC");
                list.AddRange(collection);
                list.AddRange(rowArray3);
                foreach (DataRow row in list)
                {
                    if (row["ChildRelation"] != DBNull.Value)
                    {
                        LogicalConnector connector = (LogicalConnector) Enum.Parse(typeof(LogicalConnector), (string) row["ChildRelation"]);
                        CompositeExpressionData child = new CompositeExpressionData(base.Context, new List<ExpressionData>(), connector) {
                            SetID = (int) row["ExpressionID"]
                        };
                        child.Load(data);
                        this.Add(child);
                    }
                    else
                    {
                        ExpressionData data4 = new ExpressionData(base.Context) {
                            SetID = (int) row["ExpressionID"]
                        };
                        data4.Load(data);
                        this.Add(data4);
                    }
                }
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
            }
        }

        public void MoveChild(ExpressionData child, CompositeExpressionData newParent)
        {
            if ((child != null) && (newParent != null))
            {
                this.Children.Remove(child);
                child.ValidationFailed -= new EventHandler(this.child_ValidationFailed);
                newParent.Add(child);
            }
        }

        protected override void OnCommit(object sender, EventArgs e)
        {
            base.OnCommit(sender, e);
            this._deletedChildren.Clear();
        }

        public void RemoveChild(ExpressionData child)
        {
            this.Children.Remove(child);
            child.ValidationFailed -= new EventHandler(this.child_ValidationFailed);
            this.DeletedChildren.Add(child);
        }

        public override void Save(IDbTransaction transaction)
        {
            base.Save(transaction);
            foreach (ExpressionData data in this.Children)
            {
                data.Save(transaction);
            }
            foreach (ExpressionData data2 in this.DeletedChildren)
            {
                data2.Delete(transaction);
            }
            if ((this.Children.Count == 0) && (this.Parent != null))
            {
                this.Delete(transaction);
            }
        }

        public override string ToString()
        {
            try
            {
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < this.Children.Count; i++)
                {
                    ExpressionData data = this.Children[i];
                    if (i > 0)
                    {
                        builder.Append(Environment.NewLine);
                        builder.Append(TextManager.GetText("/logic/" + this.Connector, base.Context.EditLanguage));
                        builder.Append(" ");
                    }
                    if (this.Children.Count > 1)
                    {
                        builder.Append("(");
                    }
                    builder.Append(data.ToString());
                    if (this.Children.Count > 1)
                    {
                        builder.Append(")");
                    }
                }
                return builder.ToString();
            }
            catch (Exception exception)
            {
                return exception.Message;
            }
        }

        protected override void Update(IDbTransaction transaction)
        {
            DataRow[] rowArray = base.Context.ExpressionTable.Select("ExpressionID=" + base.ID.Value);
            if (rowArray.Length > 0)
            {
                rowArray[0]["ChildRelation"] = this.Connector.ToString();
            }
        }

        public override void Validate()
        {
            foreach (ExpressionData data in this.Children)
            {
                data.Validate();
            }
        }

        public List<ExpressionData> Children
        {
            get
            {
                if (this._children == null)
                {
                    this._children = new List<ExpressionData>();
                }
                return this._children;
            }
        }

        public LogicalConnector Connector { get; set; }

        private List<ExpressionData> DeletedChildren
        {
            get
            {
                if (this._deletedChildren == null)
                {
                    this._deletedChildren = new List<ExpressionData>();
                }
                return this._deletedChildren;
            }
        }
    }
}

