﻿namespace Checkbox.Forms.Items.Configuration
{
    using Checkbox.Globalization.Text;
    using Prezza.Framework.Data;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Linq;
    using System.Runtime.CompilerServices;

    [Serializable]
    public abstract class SelectItemData : LabelledItemData
    {
        private Checkbox.Forms.Items.Configuration.ListData _listData = new Checkbox.Forms.Items.Configuration.ListData();

        protected SelectItemData()
        {
        }

        public void AddOption(string alias, bool isDefault, int position, double points, bool isOther)
        {
            this._listData.AddOption(-1, alias, isDefault, position, points, isOther);
        }

        protected override ItemData Copy()
        {
            SelectItemData data = (SelectItemData) base.Copy();
            if (data != null)
            {
                data.Randomize = this.Randomize;
                data.AllowOther = this.AllowOther;
                data._listData = (Checkbox.Forms.Items.Configuration.ListData) this._listData.Clone();
            }
            return data;
        }

        protected override void CreateDataRelationsInternal(DataSet ds)
        {
            base.CreateDataRelationsInternal(ds);
            string name = "Items_ItemOptions";
            if ((!ds.Relations.Contains(name) && ds.Tables.Contains("ItemOptions")) && ds.Tables.Contains("Items"))
            {
                DataRelation relation = new DataRelation(name, ds.Tables["Items"].Columns["ItemID"], ds.Tables["ItemOptions"].Columns["ItemID"]);
                ds.Relations.Add(relation);
            }
            name = "Items_ItemLists";
            if ((!ds.Relations.Contains(name) && ds.Tables.Contains("ItemLists")) && ds.Tables.Contains("Items"))
            {
                DataRelation relation2 = new DataRelation(name, ds.Tables["Items"].Columns["ItemID"], ds.Tables["ItemLists"].Columns["ItemID"]);
                ds.Relations.Add(relation2);
            }
            this._listData.CreateDataRelations(ds);
        }

        public override void CreateTextDataRelations(DataSet ds)
        {
            base.CreateTextDataRelations(ds);
            this._listData.CreateTextDataRelations(ds);
        }

        public override ItemTextDecorator CreateTextDecorator(string languageCode)
        {
            return new SelectItemTextDecorator(this, languageCode);
        }

        public virtual ListOptionData GetOption(int optionID)
        {
            return this._listData.ListOptions.FirstOrDefault<ListOptionData>(option => (option.OptionID == optionID));
        }

        public override void Load(DataSet data)
        {
            base.Load(data);
            this.LoadItemOptions(data);
        }

        protected virtual void LoadItemOptions(DataSet data)
        {
            if ((data != null) && data.Tables.Contains("ItemOptions"))
            {
                foreach (DataRow row in data.Tables["ItemOptions"].Select(this.IdentityColumnName + " = " + base.ID))
                {
                    if (row["ListID"] != DBNull.Value)
                    {
                        int num = Convert.ToInt32(row["ListID"]);
                        if (!this._listData.ID.HasValue)
                        {
                            this._listData.Initialize(new int?(num));
                            this._listData.Load(data);
                            return;
                        }
                    }
                }
            }
        }

        public override DataSet LoadTextData()
        {
            DataSet ds = base.LoadTextData();
            this.MergeTextData(ds, TextManager.GetTextData(this.OtherTextID), this.TextTableName, "otherText", this.TextIDPrefix, base.ID.Value);
            ds.Merge(this._listData.LoadTextData());
            return ds;
        }

        protected override void OnAbort(object sender, EventArgs e)
        {
            base.OnAbort(sender, e);
            this._listData.NotifyAbort(sender, e);
        }

        protected override void OnCommit(object sender, EventArgs e)
        {
            base.OnCommit(sender, e);
            this._listData.NotifyCommit(sender, e);
        }

        public void RemoveOption(int optionID)
        {
            this._listData.RemoveOption(optionID);
        }

        public override void SetConfigurationDataSetColumnMappings(DataSet data)
        {
            base.SetConfigurationDataSetColumnMappings(data);
            if (data.Tables.Contains("ItemOptions"))
            {
                DataTable table = data.Tables["ItemOptions"];
                table.Columns["OptionID"].ColumnMapping = MappingType.Attribute;
                table.Columns["IsDefault"].ColumnMapping = MappingType.Attribute;
                table.Columns["Position"].ColumnMapping = MappingType.Attribute;
            }
        }

        protected virtual void UpdateLists(IDbTransaction transaction)
        {
            ListOptionData otherOption = null;
            List<ListOptionData> list = new List<ListOptionData>();
            foreach (ListOptionData data2 in this._listData.ListOptions)
            {
                if (data2.IsOther)
                {
                    otherOption = data2;
                    if (!this.AllowOther)
                    {
                        list.Add(data2);
                        otherOption = null;
                    }
                }
            }
            foreach (ListOptionData data3 in list)
            {
                this._listData.RemoveOption(data3.OptionID);
            }
            this.UpdateOtherOption(otherOption);
            this._listData.ItemID = new int?(base.ID.Value);
            this._listData.Save(transaction);
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_AddListData");
            storedProcCommandWrapper.AddInParameter("ListID", DbType.Int32, this._listData.ID);
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            database.ExecuteNonQuery(storedProcCommandWrapper, transaction);
        }

        public void UpdateOption(int optionID, string alias, bool isDefault, int position, double points, bool isOther)
        {
            this._listData.UpdateOption(optionID, -1, alias, isDefault, position, points, isOther);
        }

        protected virtual void UpdateOtherOption(ListOptionData otherOption)
        {
            if (this.AllowOther)
            {
                if (otherOption == null)
                {
                    this._listData.AddOption(base.ID.Value, "other", false, this._listData.ListOptions.Count + 1, 0.0, true);
                }
                else
                {
                    this._listData.UpdateOption(otherOption.OptionID, -1, "other", false, this._listData.ListOptions.Count + 1, 0.0, true);
                }
            }
        }

        public virtual bool AllowOther { get; set; }

        protected virtual string[] ConfigurationDataTableNames
        {
            get
            {
                return new string[] { this.DataTableName, "ItemLists", "ItemOptions" };
            }
        }

        public override bool ItemIsIAnswerable
        {
            get
            {
                return true;
            }
        }

        public override bool ItemIsIScored
        {
            get
            {
                return true;
            }
        }

        public virtual Checkbox.Forms.Items.Configuration.ListData ListData
        {
            get
            {
                return this._listData;
            }
        }

        public virtual ReadOnlyCollection<ListOptionData> Options
        {
            get
            {
                return this._listData.ListOptions;
            }
        }

        public virtual string OtherTextID
        {
            get
            {
                return this.GetTextID("otherText");
            }
        }

        public virtual bool Randomize { get; set; }

        public override string TextIDPrefix
        {
            get
            {
                return "selectItemData";
            }
        }
    }
}

