﻿namespace Checkbox.Forms.Items.UI
{
    using Prezza.Framework.Data;
    using System;
    using System.Data;

    [Serializable]
    public class Matrix : LabelledItemAppearanceData
    {
        protected override void Create(IDbTransaction transaction)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Appearance_CreateMatrix");
            storedProcCommandWrapper.AddInParameter("AppearanceCode", DbType.String, this.AppearanceCode);
            storedProcCommandWrapper.AddInParameter("Width", DbType.Int32, this.TableWidth);
            storedProcCommandWrapper.AddInParameter("ItemPosition", DbType.String, base.ItemPosition);
            storedProcCommandWrapper.AddInParameter("LabelPosition", DbType.String, this.LabelPosition.ToString());
            storedProcCommandWrapper.AddInParameter("GridLines", DbType.String, this.GridLines);
            storedProcCommandWrapper.AddOutParameter("AppearanceID", DbType.Int32, 4);
            database.ExecuteNonQuery(storedProcCommandWrapper);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("AppearanceID");
            if ((parameterValue == null) || (parameterValue == DBNull.Value))
            {
                throw new Exception("Unable to save appearance data.");
            }
            base.ID = new int?((int) parameterValue);
        }

        protected override void Update(IDbTransaction transaction)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Appearance_UpdateMatrix");
            storedProcCommandWrapper.AddInParameter("AppearanceID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("Width", DbType.Int32, this.TableWidth);
            storedProcCommandWrapper.AddInParameter("ItemPosition", DbType.String, base.ItemPosition);
            storedProcCommandWrapper.AddInParameter("LabelPosition", DbType.String, this.LabelPosition.ToString());
            storedProcCommandWrapper.AddInParameter("GridLines", DbType.String, this.GridLines);
            database.ExecuteNonQuery(storedProcCommandWrapper);
        }

        public override string AppearanceCode
        {
            get
            {
                return "MATRIX";
            }
        }

        public int? TableWidth
        {
            get
            {
                return this.Width;
            }
            set
            {
                this.Width = value;
            }
        }
    }
}

