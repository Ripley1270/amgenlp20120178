﻿namespace Checkbox.Analytics.Items
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class CrossTabAnalysisData
    {
        private DataTable _data;
        private Dictionary<int, string> _questionTexts;
        private Dictionary<int, List<string>> _xAxisOptions;
        private Dictionary<int, List<string>> _yAxisOptions;

        public CrossTabAnalysisData()
        {
            this.InitializeData();
        }

        public virtual void AddAnswer(int xAxisItemID, string xAxisItem, int yAxisItemID, string yAxisItem, int answerCount)
        {
            string filterExpression = string.Format("XAxisItemID = {0} AND XAxisItem = '{1}'", xAxisItemID, xAxisItem.Replace("'", "''")) + string.Format(" AND  YAxisItemID = {0} AND YAxisItem = '{1}'", yAxisItemID, yAxisItem.Replace("'", "''"));
            DataRow[] rowArray = this._data.Select(filterExpression, null, DataViewRowState.CurrentRows);
            if (rowArray.Length == 0)
            {
                DataRow row = this._data.NewRow();
                row["XAxisItemID"] = xAxisItemID;
                row["XAxisItem"] = xAxisItem;
                row["YAxisItemID"] = yAxisItemID;
                row["YAxisItem"] = yAxisItem;
                row["AnswerCount"] = answerCount;
                this._data.Rows.Add(row);
            }
            else
            {
                int num = Convert.ToInt32(rowArray[0]["AnswerCount"]) + answerCount;
                rowArray[0]["AnswerCount"] = num;
            }
        }

        public virtual void AddQuestionText(int itemID, string questionText)
        {
            this._questionTexts[itemID] = questionText;
        }

        public virtual void AddXOptions(int itemID, List<string> options)
        {
            this._xAxisOptions[itemID] = options;
        }

        public virtual void AddYOptions(int itemID, List<string> options)
        {
            this._yAxisOptions[itemID] = options;
        }

        public Dictionary<int, List<string>> GetOptionTexts()
        {
            Dictionary<int, List<string>> dictionary = new Dictionary<int, List<string>>();
            foreach (int num in this._xAxisOptions.Keys)
            {
                dictionary[num] = this._xAxisOptions[num];
            }
            foreach (int num2 in this._yAxisOptions.Keys)
            {
                dictionary[num2] = this._yAxisOptions[num2];
            }
            return dictionary;
        }

        public Dictionary<int, string> GetXQuestionTexts()
        {
            Dictionary<int, string> dictionary = new Dictionary<int, string>();
            foreach (int num in this._questionTexts.Keys)
            {
                if (this._data.Select("XAxisItemID = " + num, null, DataViewRowState.CurrentRows).Length > 0)
                {
                    dictionary[num] = this._questionTexts[num];
                }
            }
            return dictionary;
        }

        public Dictionary<int, string> GetYQuestionTexts()
        {
            Dictionary<int, string> dictionary = new Dictionary<int, string>();
            foreach (int num in this._questionTexts.Keys)
            {
                if (this._data.Select("YAxisItemID = " + num, null, DataViewRowState.CurrentRows).Length > 0)
                {
                    dictionary[num] = this._questionTexts[num];
                }
            }
            return dictionary;
        }

        private void InitializeData()
        {
            DataTable table = new DataTable {
                TableName = "CrossTabData"
            };
            this._data = table;
            this._data.Columns.Add("XAxisItemID", typeof(int));
            this._data.Columns.Add("XAxisItem", typeof(string));
            this._data.Columns.Add("YAxisItemID", typeof(int));
            this._data.Columns.Add("YAxisItem", typeof(string));
            this._data.Columns.Add("AnswerCount", typeof(int));
            this._questionTexts = new Dictionary<int, string>();
            this._xAxisOptions = new Dictionary<int, List<string>>();
            this._yAxisOptions = new Dictionary<int, List<string>>();
        }

        public string KeyColumn
        {
            get
            {
                return "YAxisKey";
            }
        }

        public virtual int ResponseCount { get; set; }

        public virtual DataTable ResultsData
        {
            get
            {
                DataTable table = new DataTable();
                table.Columns.Add("YAxisItem", typeof(string));
                table.Columns.Add(this.KeyColumn, typeof(string));
                foreach (int num in this._xAxisOptions.Keys)
                {
                    foreach (string str in this._xAxisOptions[num])
                    {
                        string name = num + "_" + str;
                        for (int i = 1; table.Columns.Contains(name); i++)
                        {
                            name = string.Concat(new object[] { num, "_", str, i });
                        }
                        table.Columns.Add(name, typeof(string));
                        foreach (int num3 in this._yAxisOptions.Keys)
                        {
                            foreach (string str3 in this._yAxisOptions[num3])
                            {
                                string str4 = str.Replace("'", "''");
                                string str5 = str3.Replace("'", "''");
                                object obj2 = this._data.Compute("Sum(AnswerCount)", string.Format("XAxisItemID = {0} AND XAxisItem = '{1}' AND YAxisItemID = {2} AND YAxisItem = '{3}'", new object[] { num, str4, num3, str5 }));
                                double num4 = (obj2 != DBNull.Value) ? Convert.ToDouble(obj2) : 0.0;
                                double num5 = 0.0;
                                if (this.ResponseCount > 0)
                                {
                                    num5 = (num4 / ((double) this.ResponseCount)) * 100.0;
                                }
                                string str6 = string.Concat(new object[] { num4, "  (", num5.ToString("0.00"), "%)  " });
                                DataRow[] rowArray = table.Select(string.Concat(new object[] { this.KeyColumn, " = '", num3, "_", str5, "'" }), null, DataViewRowState.CurrentRows);
                                if (rowArray.Length == 0)
                                {
                                    DataRow row = table.NewRow();
                                    row[name] = str6;
                                    row["YAxisItem"] = str3;
                                    row[this.KeyColumn] = num3 + "_" + str3;
                                    table.Rows.Add(row);
                                }
                                else
                                {
                                    rowArray[0][name] = str6;
                                }
                            }
                        }
                    }
                }
                return table;
            }
        }
    }
}

