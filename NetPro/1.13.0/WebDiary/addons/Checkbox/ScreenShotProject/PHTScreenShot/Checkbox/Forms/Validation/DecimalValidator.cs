﻿using Checkbox.Common;
using Checkbox.Globalization.Text;
using System;
using System.Globalization;

namespace Checkbox.Forms.Validation
{
    //using Checkbox.Common;
    //using Checkbox.Globalization.Text;
    //using System;

    public class DecimalValidator : Validator<string>
    {
        public override string GetMessage(string languageCode)
        {
            return TextManager.GetText("/validationMessages/regex/decimal", languageCode);
        }

        public override bool Validate(string input)
        {
            if (!input.Contains(".") && !input.Contains(","))
            {
                return false;
            }
            int num = input.Length - 1;
            int num2 = input.LastIndexOf('.');
            int num3 = input.LastIndexOf(',');
            return (((num2 != num) && (num3 != num)) && Utilities.GetDouble(input, new CultureInfo[0]).HasValue);
        }
    }
}

