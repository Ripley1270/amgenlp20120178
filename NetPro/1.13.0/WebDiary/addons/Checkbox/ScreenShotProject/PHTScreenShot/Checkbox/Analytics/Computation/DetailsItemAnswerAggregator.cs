﻿namespace Checkbox.Analytics.Computation
{
    using Checkbox.Analytics.Data;
    using Checkbox.Common;
    using System;

    public class DetailsItemAnswerAggregator : ItemAnswerAggregator
    {
        public override void AddAnswer(long answerId, long responseID, Guid? responseGuid, int itemID, int? optionID, string answer)
        {
            Predicate<ItemAnswer> match = null;
            if ((optionID.HasValue && Utilities.IsNotNullOrEmpty(answer)) && base.ResponseAnswerDictionary.ContainsKey(responseID))
            {
                if (match == null)
                {
                    match = a => a.ItemId == itemID;
                }
                ItemAnswer answer2 = base.ListResponseAnswers(responseID).Find(match);
                if (answer2 != null)
                {
                    if (Utilities.IsNotNullOrEmpty(answer2.AnswerText))
                    {
                        answer2.AnswerText = answer2.AnswerText + ", " + answer;
                    }
                    else
                    {
                        answer2.AnswerText = answer;
                    }
                }
            }
            base.AddAnswer(answerId, responseID, responseGuid, itemID, optionID, answer);
        }
    }
}

