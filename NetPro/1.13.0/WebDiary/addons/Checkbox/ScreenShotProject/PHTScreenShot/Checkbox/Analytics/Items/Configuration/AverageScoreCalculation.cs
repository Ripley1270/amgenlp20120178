﻿namespace Checkbox.Analytics.Items.Configuration
{
    using System;

    public enum AverageScoreCalculation
    {
        Aggregate,
        AverageOfItemScores,
        ItemAverages
    }
}

