﻿namespace Checkbox.Forms.Validation
{
    using System;

    public class DoubleRangeValidator : RangeValidator<double>
    {
        public DoubleRangeValidator(double? min, double? max)
        {
            if (min.HasValue)
            {
                base.MinValue = min.Value;
            }
            if (max.HasValue)
            {
                base.MaxValue = max.Value;
            }
        }

        public override bool Validate(double input)
        {
            if (base.MinValueSet && (base.MinValue > input))
            {
                return false;
            }
            if (base.MaxValueSet && (base.MaxValue < input))
            {
                return false;
            }
            return true;
        }
    }
}

