﻿namespace Checkbox.Analytics.Items.UI
{
    using Prezza.Framework.Data;
    using System;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class StatisticsItemAppearanceData : AnalysisItemAppearanceData
    {
        private const string CUSTOM_APPEARANCE_DATA_TABLENAME = "StatisticsItemAppearance";

        public StatisticsItemAppearanceData()
        {
            this.ShowResponses = true;
            this.ShowMean = true;
            this.ShowMedian = true;
            this.ShowMode = true;
            this.ShowStandardDeviation = true;
        }

        protected override void Create(IDbTransaction transaction)
        {
            base.Create(transaction);
            this.UpSert(transaction);
        }

        protected override DataSet GetConcreteConfigurationDataSet()
        {
            if (!base.ID.HasValue || (base.ID <= 0))
            {
                throw new Exception("Unable to load data with no ID!");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Appearance_StatisticsTable_Get");
            storedProcCommandWrapper.AddInParameter("AppearanceId", DbType.Int32, base.ID);
            DataSet dataSet = new DataSet();
            database.LoadDataSet(storedProcCommandWrapper, dataSet, new string[] { this.DataTableName, "StatisticsItemAppearance" });
            return dataSet;
        }

        public override void Load(DataSet data)
        {
            base.Load(data);
            if (data.Tables.Contains("StatisticsItemAppearance"))
            {
                DataRow[] rowArray = data.Tables["StatisticsItemAppearance"].Select("AppearanceId = " + base.ID);
                if (rowArray.Length > 0)
                {
                    this.ShowResponses = DbUtility.GetValueFromDataRow<bool>(rowArray[0], "ShowResponses", false);
                    this.ShowMean = DbUtility.GetValueFromDataRow<bool>(rowArray[0], "ShowMean", false);
                    this.ShowMedian = DbUtility.GetValueFromDataRow<bool>(rowArray[0], "ShowMedian", false);
                    this.ShowMode = DbUtility.GetValueFromDataRow<bool>(rowArray[0], "ShowMode", false);
                    this.ShowStandardDeviation = DbUtility.GetValueFromDataRow<bool>(rowArray[0], "ShowStandardDeviation", false);
                }
            }
            if (((!this.ShowResponses && !this.ShowMean) && (!this.ShowMedian && !this.ShowMode)) && !this.ShowStandardDeviation)
            {
                this.ShowResponses = true;
            }
        }

        protected override void Update(IDbTransaction transaction)
        {
            base.Update(transaction);
            this.UpSert(transaction);
        }

        private void UpSert(IDbTransaction transaction)
        {
            if (!base.ID.HasValue || (base.ID <= 0))
            {
                throw new Exception("Unable to save custom appearance data!");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Appearance_StatisticsTable_UpSert");
            storedProcCommandWrapper.AddInParameter("AppearanceId", DbType.Int32, base.ID.Value);
            storedProcCommandWrapper.AddInParameter("ShowResponses", DbType.Boolean, this.ShowResponses);
            storedProcCommandWrapper.AddInParameter("ShowMean", DbType.Boolean, this.ShowMean);
            storedProcCommandWrapper.AddInParameter("ShowMedian", DbType.Boolean, this.ShowMedian);
            storedProcCommandWrapper.AddInParameter("ShowMode", DbType.Boolean, this.ShowMode);
            storedProcCommandWrapper.AddInParameter("ShowStandardDeviation", DbType.Boolean, this.ShowStandardDeviation);
            database.ExecuteNonQuery(storedProcCommandWrapper, transaction);
        }

        public override string AppearanceCode
        {
            get
            {
                return "STATISTICS_TABLE";
            }
        }

        public bool ShowMean { get; set; }

        public bool ShowMedian { get; set; }

        public bool ShowMode { get; set; }

        public bool ShowResponses { get; set; }

        public bool ShowStandardDeviation { get; set; }
    }
}

