﻿namespace Checkbox.Forms.Items.Configuration
{
    using Checkbox.Forms.Items;
    using Checkbox.Globalization.Text;
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class EmailItemData : LocalizableResponseItemData
    {
        protected override void Create(IDbTransaction t)
        {
            base.Create(t);
            if (base.ID <= 0)
            {
                throw new ApplicationException("No DataID specified for Create()");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_InsertEmail");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("MessageFormat", DbType.String, this.MessageFormat);
            storedProcCommandWrapper.AddInParameter("StyleTemplateID", DbType.Int32, this.StyleTemplateID);
            storedProcCommandWrapper.AddInParameter("FromAddress", DbType.String, this.From);
            storedProcCommandWrapper.AddInParameter("ToAddress", DbType.String, this.To);
            storedProcCommandWrapper.AddInParameter("Bcc", DbType.String, this.BCC);
            storedProcCommandWrapper.AddInParameter("SubjectTextID", DbType.String, this.SubjectTextID);
            storedProcCommandWrapper.AddInParameter("BodyTextID", DbType.String, this.BodyTextID);
            storedProcCommandWrapper.AddInParameter("SendOnce", DbType.Boolean, this.SendOnce);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        protected override Item CreateItem()
        {
            return new EmailItem();
        }

        public override ItemTextDecorator CreateTextDecorator(string languageCode)
        {
            return new EmailItemTextDecorator(this, languageCode);
        }

        protected override DataSet GetConcreteConfigurationDataSet()
        {
            if (base.ID <= 0)
            {
                throw new Exception("No DataID specified.");
            }
            try
            {
                Database database = DatabaseFactory.CreateDatabase();
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_GetEmail");
                storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
                DataSet concreteConfigurationDataSet = base.GetConcreteConfigurationDataSet();
                database.LoadDataSet(storedProcCommandWrapper, concreteConfigurationDataSet, this.DataTableName);
                return concreteConfigurationDataSet;
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
                return null;
            }
        }

        protected override void LoadFromDataRow(DataRow data)
        {
            if (data == null)
            {
                throw new Exception("Data Row cannot be null.");
            }
            try
            {
                this.MessageFormat = DbUtility.GetValueFromDataRow<string>(data, "MessageFormat", string.Empty);
                if (!base.IsImporting)
                {
                    this.StyleTemplateID = DbUtility.GetValueFromDataRow<int?>(data, "StyleTemplateId", null);
                }
                this.From = DbUtility.GetValueFromDataRow<string>(data, "FromAddress", string.Empty);
                this.To = DbUtility.GetValueFromDataRow<string>(data, "ToAddress", string.Empty);
                this.BCC = DbUtility.GetValueFromDataRow<string>(data, "BCC", string.Empty);
                this.SendOnce = DbUtility.GetValueFromDataRow<bool>(data, "SendOnce", false);
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
            }
        }

        public override DataSet LoadTextData()
        {
            DataSet ds = base.LoadTextData();
            this.MergeTextData(ds, TextManager.GetTextData(this.SubjectTextID), this.TextTableName, "subject", this.TextIDPrefix, base.ID.Value);
            this.MergeTextData(ds, TextManager.GetTextData(this.BodyTextID), this.TextTableName, "body", this.TextIDPrefix, base.ID.Value);
            return ds;
        }

        protected override void Update(IDbTransaction t)
        {
            base.Update(t);
            if (base.ID <= 0)
            {
                throw new ApplicationException("No DataID specified for Update()");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_UpdateEmail");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("MessageFormat", DbType.String, this.MessageFormat);
            storedProcCommandWrapper.AddInParameter("StyleTemplateID", DbType.Int32, this.StyleTemplateID);
            storedProcCommandWrapper.AddInParameter("FromAddress", DbType.String, this.From);
            storedProcCommandWrapper.AddInParameter("ToAddress", DbType.String, this.To);
            storedProcCommandWrapper.AddInParameter("Bcc", DbType.String, this.BCC);
            storedProcCommandWrapper.AddInParameter("SubjectTextID", DbType.String, this.SubjectTextID);
            storedProcCommandWrapper.AddInParameter("BodyTextID", DbType.String, this.BodyTextID);
            storedProcCommandWrapper.AddInParameter("SendOnce", DbType.Boolean, this.SendOnce);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        public virtual string BCC { get; set; }

        public virtual string BodyTextID
        {
            get
            {
                return this.GetTextID("body");
            }
        }

        public override string DataTableName
        {
            get
            {
                return "EmailItemData";
            }
        }

        public virtual string From { get; set; }

        public virtual string MessageFormat { get; set; }

        public virtual bool SendOnce { get; set; }

        public virtual int? StyleTemplateID { get; set; }

        public virtual string SubjectTextID
        {
            get
            {
                return this.GetTextID("subject");
            }
        }

        public override string TextIDPrefix
        {
            get
            {
                return "emailItemData";
            }
        }

        public virtual string To { get; set; }
    }
}

