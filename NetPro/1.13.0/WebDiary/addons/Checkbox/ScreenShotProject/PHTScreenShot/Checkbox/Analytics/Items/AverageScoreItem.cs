﻿namespace Checkbox.Analytics.Items
{
    using Checkbox.Analytics.Computation;
    using Checkbox.Analytics.Data;
    using Checkbox.Analytics.Items.Configuration;
    using Checkbox.Forms.Items.Configuration;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class AverageScoreItem : AnalysisItem
    {
        private AverageScoreCalculation _scoreOption;

        public override void Configure(ItemData itemData, string languageCode)
        {
            base.Configure(itemData, languageCode);
            this._scoreOption = ((AverageScoreItemData) itemData).AverageScoreCalculation;
        }

        protected override object GeneratePreviewData()
        {
            return this.ProcessData(true);
        }

        protected override object ProcessData()
        {
            return this.ProcessData(false);
        }

        protected virtual object ProcessData(bool isPreview)
        {
            ItemAnswerAggregator aggregator = new ItemAnswerAggregator();
            AverageScoreDataCalculator calculator = new AverageScoreDataCalculator(this.ScoreOption);
            foreach (int num in this.SourceItemIDs)
            {
                List<int> list = isPreview ? this.GetItemOptionIdsForPreview(num) : this.GetItemOptionIdsForReport(num);
                aggregator.AddItem(num, this.GetItemText(num), this.GetSourceItemTypeName(num));
                foreach (int num2 in list)
                {
                    aggregator.AddItemOption(num, num2, this.GetOptionText(num, num2), new double?(base.GetOptionPoints(num, num2)), base.GetOptionIsOther(num, num2));
                }
            }
            long answerId = 0x3e8L;
            foreach (int num4 in this.SourceItemIDs)
            {
                List<ItemAnswer> list2 = isPreview ? this.GetItemPreviewAnswers(num4, new long?(answerId), 0x3e8L) : base.GetItemAnswers(num4);
                foreach (ItemAnswer answer in list2)
                {
                    if (answer.AnswerId > answerId)
                    {
                        answerId = answer.AnswerId;
                    }
                    aggregator.AddAnswer(answer.AnswerId, answer.ResponseId, num4, answer.OptionId);
                }
                base.ResponseCounts[num4] = aggregator.GetResponseCount(new int?(num4));
                answerId += 1L;
            }
            base.DataProcessed = true;
            return calculator.GetData(aggregator);
        }

        public virtual AverageScoreCalculation ScoreOption
        {
            get
            {
                return this._scoreOption;
            }
        }
    }
}

