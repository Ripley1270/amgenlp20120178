﻿namespace Checkbox.Analytics.Security
{
    using Checkbox.Analytics;
    using Checkbox.Security;
    using System;

    public class AnalysisSecurityEditor : AccessControllablePDOSecurityEditor
    {
        public AnalysisSecurityEditor(AnalysisTemplate analysis) : base(analysis)
        {
        }

        protected override string RequiredEditorPermission
        {
            get
            {
                return "Analysis.Administer";
            }
        }
    }
}

