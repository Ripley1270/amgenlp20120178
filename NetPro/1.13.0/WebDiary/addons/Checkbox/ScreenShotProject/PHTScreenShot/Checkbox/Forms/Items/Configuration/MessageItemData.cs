﻿namespace Checkbox.Forms.Items.Configuration
{
    using Checkbox.Forms.Items;
    using Checkbox.Globalization.Text;
    using Prezza.Framework.Data;
    using System;
    using System.Data;

    [Serializable]
    public class MessageItemData : LocalizableResponseItemData
    {
        protected override void Create(IDbTransaction t)
        {
            base.Create(t);
            if (base.ID <= 0)
            {
                throw new ApplicationException("No DataID specified for Create()");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_InsertMessage");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("TextID", DbType.String, this.TextID);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        protected override Item CreateItem()
        {
            return new Message();
        }

        public override ItemTextDecorator CreateTextDecorator(string languageCode)
        {
            return new MessageItemTextDecorator(this, languageCode);
        }

        protected override DataSet GetConcreteConfigurationDataSet()
        {
            if (!base.ID.HasValue || (base.ID <= 0))
            {
                throw new ApplicationException("No DataID specified.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_GetMessage", new object[] { base.ID });
            DataSet dataSet = new DataSet();
            database.LoadDataSet(storedProcCommandWrapper, dataSet, this.DataTableName);
            return dataSet;
        }

        protected override void LoadFromDataRow(DataRow data)
        {
            if (data == null)
            {
                throw new Exception("DataRow cannot be null.");
            }
        }

        public override DataSet LoadTextData()
        {
            DataSet ds = base.LoadTextData();
            this.MergeTextData(ds, TextManager.GetTextData(this.TextID), this.TextTableName, "text", this.TextIDPrefix, base.ID.Value);
            return ds;
        }

        protected override void Update(IDbTransaction t)
        {
            base.Update(t);
            if (base.ID <= 0)
            {
                throw new ApplicationException("No DataID specified for Update()");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_UpdateMessage");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("TextId", DbType.String, this.TextID);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        public override string DataTableName
        {
            get
            {
                return "MessageItemData";
            }
        }

        public virtual string TextID
        {
            get
            {
                return this.GetTextID("text");
            }
        }

        public override string TextIDPrefix
        {
            get
            {
                return "messageItemData";
            }
        }
    }
}

