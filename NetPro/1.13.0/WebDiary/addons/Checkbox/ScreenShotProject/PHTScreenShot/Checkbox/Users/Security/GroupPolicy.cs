﻿namespace Checkbox.Users.Security
{
    using Prezza.Framework.Security;
    using System;

    [Serializable]
    internal class GroupPolicy : Policy
    {
        private static readonly string[] allowablePermissions = new string[] { "Group.View", "Group.Delete", "Group.Edit", "Group.ManageUsers" };

        public GroupPolicy() : base(allowablePermissions)
        {
        }

        public GroupPolicy(string[] permissions) : base(permissions)
        {
        }
    }
}

