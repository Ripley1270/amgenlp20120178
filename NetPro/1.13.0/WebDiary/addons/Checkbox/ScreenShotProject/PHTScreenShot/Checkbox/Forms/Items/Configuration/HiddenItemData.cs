﻿namespace Checkbox.Forms.Items.Configuration
{
    using Checkbox.Forms.Items;
    using Checkbox.Forms.Items.UI;
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class HiddenItemData : LabelledItemData
    {
        protected override void Create(IDbTransaction t)
        {
            base.Create(t);
            if (base.ID <= 0)
            {
                throw new ApplicationException("No DataID specified for Create()");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_InsertHidden");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("TextID", DbType.String, this.TextID);
            storedProcCommandWrapper.AddInParameter("SubTextID", DbType.String, this.SubTextID);
            storedProcCommandWrapper.AddInParameter("VariableName", DbType.String, this.VariableName);
            storedProcCommandWrapper.AddInParameter("VariableSource", DbType.String, this.VariableSource);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
            new AppearanceDataFactory().CreateAppearanceData("Checkbox.Forms.Items.UI.Hidden,Checkbox").Save(t, base.ID.Value);
        }

        protected override Item CreateItem()
        {
            return new HiddenItem();
        }

        public override ItemTextDecorator CreateTextDecorator(string languageCode)
        {
            return new LabelledItemTextDecorator(this, languageCode);
        }

        protected override DataSet GetConcreteConfigurationDataSet()
        {
            if (base.ID <= 0)
            {
                throw new ApplicationException("No DataID specified.");
            }
            try
            {
                Database database = DatabaseFactory.CreateDatabase();
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_GetHidden");
                storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
                DataSet concreteConfigurationDataSet = base.GetConcreteConfigurationDataSet();
                database.LoadDataSet(storedProcCommandWrapper, concreteConfigurationDataSet, this.DataTableName);
                return concreteConfigurationDataSet;
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessProtected"))
                {
                    throw;
                }
                return null;
            }
        }

        protected override void LoadFromDataRow(DataRow data)
        {
            if (data == null)
            {
                throw new Exception("DataRow cannot be null.");
            }
            try
            {
                this.VariableSource = (HiddenVariableSource) Enum.Parse(typeof(HiddenVariableSource), (string) data["VariableSource"]);
                this.VariableName = (string) data["Variablename"];
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessProtected"))
                {
                    throw;
                }
            }
        }

        protected override void Update(IDbTransaction t)
        {
            base.Update(t);
            if (base.ID <= 0)
            {
                throw new Exception("No DataID specified for Update()");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_UpdateHidden");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("TextID", DbType.String, this.TextID);
            storedProcCommandWrapper.AddInParameter("SubTextID", DbType.String, this.SubTextID);
            storedProcCommandWrapper.AddInParameter("VariableName", DbType.String, this.VariableName);
            storedProcCommandWrapper.AddInParameter("VariableSource", DbType.String, this.VariableSource);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        public override string DataTableName
        {
            get
            {
                return "HiddenItemData";
            }
        }

        public override bool ItemIsIAnswerable
        {
            get
            {
                return true;
            }
        }

        public override string TextIDPrefix
        {
            get
            {
                return "hiddenItemData";
            }
        }

        public string VariableName { get; set; }

        public HiddenVariableSource VariableSource { get; set; }
    }
}

