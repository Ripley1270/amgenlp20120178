﻿namespace Checkbox.Analytics.Items.Configuration
{
    using Checkbox.Analytics.Filters;
    using Checkbox.Analytics.Filters.Configuration;
    using Checkbox.Analytics.Items;
    using Checkbox.Forms.Items;
    using Checkbox.Forms.Items.Configuration;
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    public abstract class AnalysisItemData : ItemData
    {
        private AnalysisItemFilterCollection _filters;
        private Checkbox.Analytics.Items.Configuration.OtherOption _otherOption;
        private const string _responseTemplatesTableName = "ResponseTemplates";
        private const string _sourceItemsTableName = "SourceItems";

        protected AnalysisItemData()
        {
            this.InitializeData();
            this._filters = new AnalysisItemFilterCollection();
        }

        public virtual void AddFilter(FilterData filter)
        {
            this._filters.AddFilter(filter);
        }

        public virtual void AddFilter(int filterID)
        {
            this._filters.AddFilter(filterID);
        }

        public virtual void AddResponseTemplate(int responseTemplateID)
        {
            if ((responseTemplateID > 0) && (this.ResponseTemplatesTable.Select("ResponseTemplateID = " + responseTemplateID, null, DataViewRowState.CurrentRows).Length == 0))
            {
                DataRow row = this.ResponseTemplatesTable.NewRow();
                row["ResponseTemplateID"] = responseTemplateID;
                this.ResponseTemplatesTable.Rows.Add(row);
            }
        }

        public virtual void AddSourceItem(int sourceItemID)
        {
            if ((sourceItemID > 0) && (this.SourceItemsTable.Select("SourceItemID = " + sourceItemID, null, DataViewRowState.CurrentRows).Length == 0))
            {
                DataRow row = this.SourceItemsTable.NewRow();
                row["SourceItemID"] = sourceItemID;
                this.SourceItemsTable.Rows.Add(row);
            }
        }

        protected override void CreateDataRelationsInternal(DataSet ds)
        {
            base.CreateDataRelationsInternal(ds);
            string name = "Items_" + this.DataTableName;
            if (!ds.Relations.Contains(name))
            {
                DataRelation relation = new DataRelation(name, ds.Tables["Items"].Columns["ItemId"], ds.Tables[this.DataTableName].Columns[this.IdentityColumnName]);
                ds.Tables[this.DataTableName].Constraints.Add(new ForeignKeyConstraint(ds.Tables["Items"].Columns["ItemId"], ds.Tables[this.DataTableName].Columns[this.IdentityColumnName]));
                relation.Nested = true;
                ds.Relations.Add(relation);
            }
            name = "Items_SourceAnalysisItemIds";
            if (!ds.Relations.Contains(name))
            {
                DataRelation relation2 = new DataRelation(name, ds.Tables["Items"].Columns["ItemId"], ds.Tables["SourceItems"].Columns["AnalysisItemId"]);
                ds.Tables["SourceItems"].Constraints.Add(new ForeignKeyConstraint(ds.Tables["Items"].Columns["ItemId"], ds.Tables["SourceItems"].Columns["AnalysisItemId"]));
                ds.Relations.Add(relation2);
            }
            name = "Items_ResponseTemplateAnalysisItemIds";
            if (!ds.Relations.Contains(name))
            {
                DataRelation relation3 = new DataRelation(name, ds.Tables["Items"].Columns["ItemId"], ds.Tables["ResponseTemplates"].Columns["AnalysisItemId"]);
                ds.Tables["ResponseTemplates"].Constraints.Add(new ForeignKeyConstraint(ds.Tables["Items"].Columns["ItemId"], ds.Tables["ResponseTemplates"].Columns["AnalysisItemId"]));
                ds.Relations.Add(relation3);
            }
            if (ds.Tables.Contains("Rule"))
            {
                name = "Template_ResponseTemplates";
                if (!ds.Relations.Contains(name))
                {
                    DataRelation relation4 = new DataRelation(name, ds.Tables["TemplateData"].Columns["TemplateId"], ds.Tables["ResponseTemplates"].Columns["ResponseTemplateId"]);
                    ds.Tables["ResponseTemplates"].Constraints.Add(new ForeignKeyConstraint(ds.Tables["TemplateData"].Columns["TemplateId"], ds.Tables["ResponseTemplates"].Columns["ResponseTemplateId"]));
                    ds.Relations.Add(relation4);
                }
                name = "Items_SourceItems";
                if (!ds.Relations.Contains(name))
                {
                    DataRelation relation5 = new DataRelation(name, ds.Tables["Items"].Columns["ItemId"], ds.Tables["SourceItems"].Columns["SourceItemId"]);
                    ds.Tables["SourceItems"].Constraints.Add(new ForeignKeyConstraint(ds.Tables["Items"].Columns["ItemId"], ds.Tables["SourceItems"].Columns["SourceItemId"]));
                    ds.Relations.Add(relation5);
                }
            }
        }

        public virtual void DeleteFilter(FilterData filter)
        {
            if (filter != null)
            {
                this.DeleteFilter(filter.ID.Value);
            }
        }

        public virtual void DeleteFilter(int filterID)
        {
            this._filters.DeleteFilter(filterID);
        }

        protected DBCommandWrapper GetDeleteRTCommand(Database db)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_ItemData_DeleteAIRT");
            storedProcCommandWrapper.AddInParameter("AnalysisItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("ResponseTemplateID", DbType.Int32, "ResponseTemplateID", DataRowVersion.Current);
            return storedProcCommandWrapper;
        }

        protected DBCommandWrapper GetDeleteSourceItemCommand(Database db)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_ItemData_DeleteAISource");
            storedProcCommandWrapper.AddInParameter("SourceItemID", DbType.Int32, "SourceItemID", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("AnalysisItemID", DbType.Int32, base.ID);
            return storedProcCommandWrapper;
        }

        public virtual DataSet GetExportAppearance()
        {
            string str;
            string str2;
            return this.GetAppearanceDataForExport(out str, out str2);
        }

        protected DBCommandWrapper GetInsertRTCommand(Database db)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_ItemData_InsertAIRT");
            storedProcCommandWrapper.AddInParameter("AnalysisItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("ResponseTemplateID", DbType.Int32, "ResponseTemplateID", DataRowVersion.Current);
            return storedProcCommandWrapper;
        }

        protected DBCommandWrapper GetInsertSourceItemCommand(Database db)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_ItemData_InsertAISource");
            storedProcCommandWrapper.AddInParameter("SourceItemID", DbType.Int32, "SourceItemID", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("AnalysisItemID", DbType.Int32, base.ID);
            return storedProcCommandWrapper;
        }

        public void ImportFilters(DataSet ds, int oldItemID)
        {
            this._filters.Import(ds, oldItemID);
        }

        protected virtual void InitializeData()
        {
            DataTable table = new DataTable();
            table.Columns.Add("AnalysisItemID", typeof(int));
            table.Columns.Add("SourceItemID", typeof(int));
            table.TableName = "SourceItems";
            DataTable table2 = new DataTable();
            table2.Columns.Add("AnalysisItemID", typeof(int));
            table2.Columns.Add("ResponseTemplateID", typeof(int));
            table2.TableName = "ResponseTemplates";
            this.Data = new DataSet();
            this.Data.Tables.Add(table);
            this.Data.Tables.Add(table2);
        }

        protected override void InitializeItem(Item item, string languageCode)
        {
            if (item is AnalysisItem)
            {
                ((AnalysisItem) item).Configure(this, languageCode, this._filters);
            }
        }

        public override void Load(DataSet data)
        {
            base.Load(data);
            if (data == null)
            {
                throw new Exception("Unable to load analysis item from NULL data.");
            }
            if ((!data.Tables.Contains(this.DataTableName) || !data.Tables.Contains("SourceItems")) || !data.Tables.Contains("ResponseTemplates"))
            {
                throw new Exception("Provided dataset was missing one or more of the item data tables.");
            }
            if (data.Tables[this.DataTableName].Rows.Count == 0)
            {
                throw new Exception("Item data table has no rows.");
            }
            this.SourceItemsTable.Rows.Clear();
            foreach (DataRow row in data.Tables["SourceItems"].Select("AnalysisItemID = " + base.ID, null, DataViewRowState.CurrentRows))
            {
                this.SourceItemsTable.ImportRow(row);
            }
            this.ResponseTemplatesTable.Rows.Clear();
            foreach (DataRow row2 in data.Tables["ResponseTemplates"].Select("AnalysisItemID = " + base.ID, null, DataViewRowState.CurrentRows))
            {
                this.ResponseTemplatesTable.ImportRow(row2);
            }
            this._filters.ParentID = base.ID.Value;
            data.Merge(this._filters.Load(base.ID.Value));
        }

        protected override void LoadFromDataRow(DataRow data)
        {
            if (data != null)
            {
                this.UseAliases = DbUtility.GetValueFromDataRow<bool>(data, "UseAliases", false);
                if (data["OtherOption"] != DBNull.Value)
                {
                    try
                    {
                        this._otherOption = (Checkbox.Analytics.Items.Configuration.OtherOption) Enum.Parse(typeof(Checkbox.Analytics.Items.Configuration.OtherOption), Convert.ToString(data["OtherOption"]));
                    }
                    catch
                    {
                        this._otherOption = Checkbox.Analytics.Items.Configuration.OtherOption.Aggregate;
                    }
                }
            }
        }

        public virtual void RemoveResponseTemplate(int responseTemplateID)
        {
            DataRow[] rowArray = this.ResponseTemplatesTable.Select("ResponseTemplateID = " + responseTemplateID, null, DataViewRowState.CurrentRows);
            if (rowArray.Length > 0)
            {
                rowArray[0].Delete();
            }
        }

        public virtual void RemoveSourceItem(int sourceItemID)
        {
            DataRow[] rowArray = this.SourceItemsTable.Select("SourceItemID = " + sourceItemID, null, DataViewRowState.CurrentRows);
            if (rowArray.Length > 0)
            {
                rowArray[0].Delete();
            }
        }

        public virtual void SaveFilters()
        {
            if (this._filters != null)
            {
                using (IDbConnection connection = DatabaseFactory.CreateDatabase().GetConnection())
                {
                    try
                    {
                        connection.Open();
                        IDbTransaction t = connection.BeginTransaction();
                        try
                        {
                            this._filters.Save(t);
                            t.Commit();
                            this._filters = null;
                        }
                        catch (Exception)
                        {
                            t.Rollback();
                            throw;
                        }
                    }
                    catch (Exception exception)
                    {
                        if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                        {
                            throw;
                        }
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }

        protected virtual void UpdateDataTables(IDbTransaction t)
        {
            Database db = DatabaseFactory.CreateDatabase();
            this._filters.ParentID = base.ID.Value;
            this._filters.Save(t);
            db.UpdateDataSet(this.Data, "SourceItems", this.GetInsertSourceItemCommand(db), null, this.GetDeleteSourceItemCommand(db), t);
            db.UpdateDataSet(this.Data, "ResponseTemplates", this.GetInsertRTCommand(db), null, this.GetDeleteRTCommand(db), t);
        }

        protected DataSet Data { get; private set; }

        public override string DataTableName
        {
            get
            {
                return "AnalysisItemData";
            }
        }

        public List<FilterData> Filters
        {
            get
            {
                return this._filters.GetFilterDataObjects();
            }
        }

        public override bool IsExportable
        {
            get
            {
                return false;
            }
        }

        public Checkbox.Analytics.Items.Configuration.OtherOption OtherOption
        {
            get
            {
                return this._otherOption;
            }
            set
            {
                this._otherOption = value;
            }
        }

        public List<int> ResponseTemplateIDs
        {
            get
            {
                List<int> list = new List<int>();
                foreach (DataRow row in this.ResponseTemplatesTable.Select(null, null, DataViewRowState.CurrentRows))
                {
                    list.Add(Convert.ToInt32(row["ResponseTemplateID"]));
                }
                return list;
            }
        }

        protected DataTable ResponseTemplatesTable
        {
            get
            {
                return this.Data.Tables["ResponseTemplates"];
            }
        }

        protected static string ResponseTemplatesTableName
        {
            get
            {
                return "ResponseTemplates";
            }
        }

        public virtual List<int> SourceItemIDs
        {
            get
            {
                List<int> list = new List<int>();
                foreach (DataRow row in this.SourceItemsTable.Select(null, null, DataViewRowState.CurrentRows))
                {
                    if (row["SourceItemID"] != DBNull.Value)
                    {
                        list.Add(Convert.ToInt32(row["SourceItemID"]));
                    }
                }
                return list;
            }
        }

        protected DataTable SourceItemsTable
        {
            get
            {
                return this.Data.Tables["SourceItems"];
            }
        }

        protected static string SourceItemsTableName
        {
            get
            {
                return "SourceItems";
            }
        }

        public bool UseAliases { get; set; }
    }
}

