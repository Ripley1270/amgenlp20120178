﻿namespace Checkbox.Analytics.Items.Configuration
{
    using Checkbox.Analytics.Items;
    using Checkbox.Forms.Items;
    using System;

    [Serializable]
    public class CSVExportItemData : ExportItemData
    {
        protected override Item CreateItem()
        {
            return new CSVExportItem();
        }
    }
}

