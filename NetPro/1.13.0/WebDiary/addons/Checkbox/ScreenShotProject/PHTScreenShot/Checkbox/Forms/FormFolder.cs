﻿namespace Checkbox.Forms
{
    using Checkbox.Common;
    using Checkbox.Forms.Data;
    using Checkbox.Forms.Security;
    using Checkbox.Globalization.Text;
    using Prezza.Framework.Common;
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using Prezza.Framework.Security;
    using Prezza.Framework.Security.Principal;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Security.Principal;

    [Serializable]
    public class FormFolder : Folder
    {
        public FormFolder() : this(new string[] { "FormFolder.FullControl", "FormFolder.Read" }, new string[] { "FormFolder.FullControl", "FormFolder.Read" })
        {
        }

        public FormFolder(string[] supportedPermissionMasks, string[] supportedPermissions) : base(supportedPermissionMasks, supportedPermissions)
        {
        }

        public override void Add(int id)
        {
            this.MoveToFolder(this, id);
        }

        public override void Add(object form)
        {
            ArgumentValidation.CheckExpectedType(form, typeof(ResponseTemplate));
            this.MoveToFolder(this, ((ResponseTemplate) form).ID.Value);
        }

        public override Folder Copy(ExtendedPrincipal principal, string languageCode)
        {
            int? nullable2;
            FormFolder folder = new FormFolder();
            int num = 1;
            string folderName = string.Concat(new object[] { TextManager.GetText("/pageText/manageSurveys.aspx/copy", languageCode), " ", num, " ", TextManager.GetText("/pageText/manageSurveys.aspx/of", languageCode), " ", base.Name });
        Label_00C8:
            nullable2 = null;
            if (FolderExists(nullable2, folderName, principal))
            {
                num++;
                folderName = string.Concat(new object[] { TextManager.GetText("/pageText/manageSurveys.aspx/copy", languageCode), " ", num, " ", TextManager.GetText("/pageText/manageSurveys.aspx/of", languageCode), " ", base.Name });
                goto Label_00C8;
            }
            folder.Name = folderName;
            folder.Description = base.Description;
            folder.Save(principal);
            string query = "Select ItemID from ckbx_TemplatesAndFoldersView WHERE AncestorID = " + base.ID + " order by itemtype, itemname";
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query);
            IAuthorizationProvider authorizationProvider = AuthorizationFactory.GetAuthorizationProvider();
            using (IDataReader reader = database.ExecuteReader(sqlStringCommandWrapper))
            {
                try
                {
                    try
                    {
                        while (reader.Read())
                        {
                            int? defaultValue = null;
                            int? nullable = DbUtility.GetValueFromDataReader<int?>(reader, "ItemID", defaultValue);
                            if (nullable.HasValue)
                            {
                                LightweightResponseTemplate lightweightResponseTemplate = ResponseTemplateManager.GetLightweightResponseTemplate(nullable.Value);
                                if (authorizationProvider.Authorize(principal, lightweightResponseTemplate, "Form.Edit"))
                                {
                                    ResponseTemplate item = ResponseTemplateManager.CopyTemplate(nullable.Value, principal, languageCode, string.Empty);
                                    folder.Add(item);
                                }
                            }
                        }
                    }
                    catch (Exception exception)
                    {
                        if (ExceptionPolicy.HandleException(exception, "BusinessProtected"))
                        {
                            throw;
                        }
                    }
                    return folder;
                }
                finally
                {
                    reader.Close();
                }
            }
            return folder;
        }

        protected override void Create(IDbTransaction t)
        {
            throw new Exception("The method or operation is not implemented.  Call overloaded Create(...) instead.");
        }

        protected void Create(ExtendedPrincipal principal, IDbTransaction t)
        {
            string[] permissions = new string[0];
            Policy defaultPolicy = this.CreatePolicy(permissions);
            AccessControlList acl = new AccessControlList();
            Policy policy = this.CreatePolicy(this.SupportedPermissions);
            acl.Add(principal, policy);
            acl.Save();
            base.SetAccess(defaultPolicy, acl);
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Folder_Create");
            storedProcCommandWrapper.AddInParameter("FolderName", DbType.String, base.Name);
            storedProcCommandWrapper.AddInParameter("FolderDescription", DbType.String, base.Description);
            storedProcCommandWrapper.AddInParameter("CreatedBy", DbType.String, principal.Identity.Name);
            storedProcCommandWrapper.AddInParameter("AclID", DbType.Int32, base.AclID);
            storedProcCommandWrapper.AddInParameter("DefaultPolicyID", DbType.Int32, base.DefaultPolicyID);
            storedProcCommandWrapper.AddOutParameter("FolderID", DbType.Int32, 4);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("FolderID");
            if ((parameterValue == null) || (parameterValue == DBNull.Value))
            {
                throw new Exception("Unable to save folder.");
            }
            base.ID = new int?((int) parameterValue);
            DBCommandWrapper command = database.GetStoredProcCommandWrapper("ckbx_Folder_CreateFormFolder");
            command.AddInParameter("FolderID", DbType.Int32, (int) storedProcCommandWrapper.GetParameterValue("FolderID"));
            database.ExecuteNonQuery(command, t);
        }

        public override Policy CreatePolicy(string[] permissions)
        {
            return new FormFolderPolicy(permissions);
        }

        public void Delete(ExtendedPrincipal principal)
        {
            if (!AuthorizationFactory.GetAuthorizationProvider().Authorize(principal, "FormFolder.FullControl"))
            {
                throw new AuthorizationException();
            }
            Database database = DatabaseFactory.CreateDatabase();
            using (IDbConnection connection = database.GetConnection())
            {
                connection.Open();
                IDbTransaction t = connection.BeginTransaction();
                try
                {
                    DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Folder_GetTemplates");
                    storedProcCommandWrapper.AddInParameter("FolderID", DbType.Int32, base.ID.Value);
                    using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                int templateID = DbUtility.GetValueFromDataReader<int>(reader, "ItemID", -1);
                                if (!ResponseTemplateManager.CanBeDeleted(templateID, principal))
                                {
                                    throw new Exception("The folder contains one or more survey you do not have permission to delete.");
                                }
                                ResponseTemplateManager.DeleteResponseTemplate(templateID, t);
                            }
                        }
                        finally
                        {
                            reader.Close();
                        }
                    }
                    storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Folder_Delete");
                    storedProcCommandWrapper.AddInParameter("FolderID", DbType.Int32, base.ID);
                    database.ExecuteNonQuery(storedProcCommandWrapper, t);
                    t.Commit();
                }
                catch
                {
                    t.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public static bool FolderExists(int? folderId, string folderName, IPrincipal currentPrincipal)
        {
            if (folderName != null)
            {
                folderName = folderName.Trim();
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_RT_CheckFolderExists");
            storedProcCommandWrapper.AddInParameter("FolderName", DbType.String, folderName);
            storedProcCommandWrapper.AddInParameter("ItemId", DbType.Int32, folderId.GetValueOrDefault());
            if (currentPrincipal != null)
            {
                storedProcCommandWrapper.AddInParameter("CreatedBy", DbType.String, currentPrincipal.Identity.Name);
            }
            else
            {
                storedProcCommandWrapper.AddInParameter("CreatedBy", DbType.String, string.Empty);
            }
            return Convert.ToBoolean(Convert.ToInt32(database.ExecuteScalar(storedProcCommandWrapper)));
        }

        public static List<FormFolder> GetAllFolders(ExtendedPrincipal currentPrincipal, string permissionToCheck)
        {
            List<FormFolder> list = new List<FormFolder>();
            DataSet set = GetAllFolders(currentPrincipal, "Name", true);
            if ((set != null) && (set.Tables.Count > 0))
            {
                DataRow[] rowArray = set.Tables[0].Select();
                IAuthorizationProvider authorizationProvider = AuthorizationFactory.GetAuthorizationProvider();
                foreach (DataRow row in rowArray)
                {
                    int dataID = (int) row["FolderID"];
                    FormFolder resource = new FormFolder();
                    resource.Load(dataID);
                    if (authorizationProvider.Authorize(currentPrincipal, resource, permissionToCheck))
                    {
                        list.Add(resource);
                    }
                }
            }
            return list;
        }

        public static DataSet GetAllFolders(ExtendedPrincipal currentPrincipal, string sortProperty, bool sortAscending)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(QueryFactory.GetSelectAvailableFormFoldersQuery(currentPrincipal, sortProperty, sortAscending, new string[0]).ToString());
            return database.ExecuteDataSet(sqlStringCommandWrapper);
        }

        public override SecurityEditor GetEditor()
        {
            return new FormFolderSecurityEditor(this);
        }

        public static RootFormFolder GetRoot()
        {
            return new RootFormFolder();
        }

        public override void Remove(int id)
        {
        }

        public override void Remove(object form)
        {
            ArgumentValidation.CheckExpectedType(form, typeof(ResponseTemplate));
        }

        public void Save(ExtendedPrincipal principal)
        {
            if (!AuthorizationFactory.GetAuthorizationProvider().Authorize(principal, "FormFolder.FullControl"))
            {
                throw new AuthorizationException();
            }
            if (FolderExists(base.ID, base.Name, principal))
            {
                throw new ArgumentException("Folder name must be unique.  '" + base.Name + "' is currently in use by another folder.");
            }
            using (IDbConnection connection = DatabaseFactory.CreateDatabase().GetConnection())
            {
                connection.Open();
                IDbTransaction t = connection.BeginTransaction();
                try
                {
                    if (!base.ID.HasValue || (base.ID <= 0))
                    {
                        this.Create(principal, t);
                    }
                    else
                    {
                        this.Update(principal, t);
                    }
                    t.Commit();
                }
                catch
                {
                    t.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        protected override void Update(IDbTransaction t)
        {
            throw new Exception("The method or operation is not implemented.  Call overloaded Update(...) instead.");
        }

        protected void Update(ExtendedPrincipal principal, IDbTransaction t)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Folder_Update");
            storedProcCommandWrapper.AddInParameter("FolderID", DbType.Int32, base.ID.Value);
            storedProcCommandWrapper.AddInParameter("FolderName", DbType.String, base.Name);
            storedProcCommandWrapper.AddInParameter("FolderDescription", DbType.String, base.Description);
            storedProcCommandWrapper.AddInParameter("CreatedBy", DbType.String, base.CreatedBy);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }
    }
}

