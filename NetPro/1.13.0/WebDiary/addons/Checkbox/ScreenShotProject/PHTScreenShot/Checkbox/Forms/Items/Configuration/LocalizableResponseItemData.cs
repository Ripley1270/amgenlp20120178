﻿namespace Checkbox.Forms.Items.Configuration
{
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Data;

    [Serializable]
    public abstract class LocalizableResponseItemData : ResponseItemData
    {
        protected LocalizableResponseItemData()
        {
            this.InitializeTextDataTable();
        }

        protected override void CreateDataRelationsInternal(DataSet ds)
        {
            base.CreateDataRelationsInternal(ds);
            try
            {
                this.CreateTextDataRelations(ds);
            }
            catch (Exception exception)
            {
                Exception ex = new Exception(string.Format("An error occurred while creating TEXT DATA relations.  Entity ID = [{0}], DataTableName = [{1}], IdentityColumnName = [{2}]", base.ID, this.DataTableName, this.IdentityColumnName));
                exception.Data["ID"] = base.ID;
                exception.Data["DataTableName"] = this.DataTableName;
                exception.Data["IdentityColumnName"] = this.IdentityColumnName;
                ExceptionPolicy.HandleException(ex, "BusinessProtected");
                throw;
            }
        }

        public virtual void CreateTextDataRelations(DataSet ds)
        {
            string name = this.TextTableName + "_" + this.DataTableName;
            if ((!ds.Relations.Contains(name) && ds.Tables.Contains(this.TextTableName)) && ds.Tables.Contains(this.DataTableName))
            {
                DataRelation relation = new DataRelation(name, ds.Tables[this.DataTableName].Columns[this.IdentityColumnName], ds.Tables[this.TextTableName].Columns["ItemID"]);
                ds.Relations.Add(relation);
            }
        }

        protected virtual string GetTextID(string suffix)
        {
            if (base.ID.HasValue && (base.ID > 0))
            {
                return string.Concat(new object[] { "/", this.TextIDPrefix, "/", base.ID, "/", suffix });
            }
            return string.Empty;
        }

        protected virtual DataTable InitializeTextDataTable()
        {
            DataTable table = new DataTable {
                TableName = this.TextTableName
            };
            table.Columns.Add("ItemID", typeof(int));
            table.Columns.Add("TextIDSuffix", typeof(string));
            table.Columns.Add("TextIDPrefix", typeof(string));
            table.Columns.Add("ComputedTextID", typeof(string));
            table.Columns.Add("TextID", typeof(string));
            table.Columns.Add("LanguageCode", typeof(string));
            table.Columns.Add("TextValue", typeof(string));
            table.Columns["ComputedTextID"].Expression = "Iif(ItemID IS NULL OR TextIDSuffix IS NULL OR TextIDPrefix IS NULL, '', '/' + TextIDPrefix + '/' + ItemID + '/' + TextIDSuffix)";
            return table;
        }

        public virtual DataSet LoadTextData()
        {
            DataSet set = new DataSet();
            set.Tables.Add(this.InitializeTextDataTable());
            return set;
        }

        protected virtual void MergeTextData(DataSet ds, DataTable textData, string tableName, string suffix, string prefix, int itemID)
        {
            if ((ds != null) && (textData != null))
            {
                textData.TableName = tableName;
                if (!textData.Columns.Contains("ItemID"))
                {
                    textData.Columns.Add("ItemID", typeof(int));
                }
                if (!textData.Columns.Contains("TextIDSuffix"))
                {
                    textData.Columns.Add("TextIDSuffix", typeof(string));
                }
                if (!textData.Columns.Contains("TextIDPrefix"))
                {
                    textData.Columns.Add("TextIDPrefix", typeof(string));
                }
                foreach (DataRow row in textData.Select(null, null, DataViewRowState.CurrentRows))
                {
                    row["ItemID"] = itemID;
                    row["TextIDSuffix"] = suffix;
                    row["TextIDPrefix"] = prefix;
                }
                if (!textData.Columns.Contains("ComputedTextID"))
                {
                    textData.Columns.Add("ComputedTextID", typeof(string));
                    textData.Columns["ComputedTextID"].Expression = "Iif(ItemID IS NULL OR TextIDSuffix IS NULL OR TextIDPrefix IS NULL, '', '/' + TextIDPrefix + '/' + ItemID + '/' + TextIDSuffix)";
                }
                ds.Merge(textData);
            }
        }

        protected override DataSet ReloadItemData()
        {
            DataSet ds = base.ReloadItemData();
            ds.Merge(this.LoadTextData());
            this.CreateTextDataRelations(ds);
            return ds;
        }

        public abstract string TextIDPrefix { get; }

        public virtual string TextTableName
        {
            get
            {
                return ("ItemTexts_" + this.DataTableName);
            }
        }
    }
}

