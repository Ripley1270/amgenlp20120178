﻿namespace Checkbox.Styles
{
    using Checkbox.Globalization.Text;
    using Prezza.Framework.Data;
    using Prezza.Framework.Security;
    using Prezza.Framework.Security.Principal;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Runtime.InteropServices;
    using System.Xml;

    public static class StyleTemplateManager
    {
        public static StyleTemplate CopyTemplate(StyleTemplate template, string languageCode, ExtendedPrincipal currentPrincipal)
        {
            if (template == null)
            {
                return null;
            }
            if (!AuthorizationFactory.GetAuthorizationProvider().Authorize(currentPrincipal, "Form.Edit"))
            {
                throw new AuthorizationException();
            }
            string name = template.Name;
            if (IsStyleNameInUse(name))
            {
                int num = 1;
                name = string.Concat(new object[] { template.Name, " ", TextManager.GetText("/common/duplicate", languageCode), " ", num });
                while (IsStyleNameInUse(name))
                {
                    num++;
                    name = string.Concat(new object[] { template.Name, " ", TextManager.GetText("/common/duplicate", languageCode), " ", num });
                }
            }
            StyleTemplate template2 = new StyleTemplate();
            StyleTemplateTextDecorator decorator = new StyleTemplateTextDecorator(template, languageCode);
            new StyleTemplateTextDecorator(template2, languageCode).LoadTemplateFromXml(decorator.GetTemplateXml(), currentPrincipal);
            template2.Name = name;
            template2.CreatedBy = currentPrincipal.Identity.Name;
            template2.DateCreated = new DateTime?(DateTime.Now);
            template2.IsPublic = template.IsPublic;
            template2.Save();
            return template2;
        }

        public static StyleTemplate CreateStyleTemplate(ExtendedPrincipal currentPrincipal)
        {
            if (!AuthorizationFactory.GetAuthorizationProvider().Authorize(currentPrincipal, "Form.Edit"))
            {
                throw new AuthorizationException();
            }
            return new StyleTemplate { CreatedBy = currentPrincipal.Identity.Name };
        }

        public static StyleTemplate CreateStyleTemplate(XmlDocument xmlDoc, ExtendedPrincipal currentPrincipal)
        {
            if (!AuthorizationFactory.GetAuthorizationProvider().Authorize(currentPrincipal, "Form.Edit"))
            {
                throw new AuthorizationException();
            }
            XmlNode node = xmlDoc.SelectSingleNode("/CssDocument/TemplateName");
            if (node != null)
            {
                string innerText = node.InnerText;
                int num = 1;
                string styleName = innerText;
                while (IsStyleNameInUse(styleName))
                {
                    styleName = string.Concat(new object[] { innerText, " ", TextManager.GetText("/common/duplicate", TextManager.DefaultLanguage), " ", num });
                    num++;
                }
                node.InnerText = styleName;
            }
            StyleTemplate template = new StyleTemplate();
            new StyleTemplateTextDecorator(template, TextManager.DefaultLanguage).LoadTemplateFromXml(xmlDoc, currentPrincipal);
            template.CreatedBy = currentPrincipal.Identity.Name;
            template.DateCreated = new DateTime?(DateTime.Now);
            template.Save();
            return template;
        }

        public static void DeleteTemplate(StyleTemplate template, ExtendedPrincipal currentPrincipal)
        {
            if (!AuthorizationFactory.GetAuthorizationProvider().Authorize(currentPrincipal, "Form.Edit"))
            {
                throw new AuthorizationException();
            }
            if (template != null)
            {
                DeleteTemplate(template.TemplateID.Value, currentPrincipal);
            }
        }

        public static void DeleteTemplate(int styleTemplateID, ExtendedPrincipal currentPrincipal)
        {
            if (!AuthorizationFactory.GetAuthorizationProvider().Authorize(currentPrincipal, "Form.Edit"))
            {
                throw new AuthorizationException();
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Style_DeleteTemplate");
            storedProcCommandWrapper.AddInParameter("TemplateID", DbType.Int32, styleTemplateID);
            database.ExecuteNonQuery(storedProcCommandWrapper);
        }

        public static DataTable GetPagedStyleData(ExtendedPrincipal currentPrincipal, int pageSize, int currentPage, out int totalCount)
        {
            int num;
            int num2;
            DataTable styleData = GetStyleData(currentPrincipal);
            int num3 = 0;
            if ((pageSize > 0) && (currentPage > 0))
            {
                num = (currentPage - 1) * pageSize;
                num2 = (num + pageSize) - 1;
            }
            else
            {
                num = 0;
                num2 = styleData.Rows.Count - 1;
            }
            DataTable table2 = styleData.Clone();
            foreach (DataRow row in styleData.Rows)
            {
                if ((num3 >= num) && (num3 <= num2))
                {
                    table2.ImportRow(row);
                }
                num3++;
            }
            totalCount = num3;
            return table2;
        }

        public static DataTable GetStyleData(ExtendedPrincipal currentPrincipal)
        {
            DBCommandWrapper storedProcCommandWrapper;
            Database database = DatabaseFactory.CreateDatabase();
            if (currentPrincipal == null)
            {
                storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Style_ListForUser");
                storedProcCommandWrapper.AddInParameter("UniqueIdentifier", DbType.String, "[[NULL]]");
            }
            else if (currentPrincipal.IsInRole("System Administrator"))
            {
                storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Style_ListAll");
            }
            else
            {
                storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Style_ListForUser");
                storedProcCommandWrapper.AddInParameter("UniqueIdentifier", DbType.String, currentPrincipal.Identity.Name);
            }
            DataSet set = database.ExecuteDataSet(storedProcCommandWrapper);
            if (set.Tables.Count > 0)
            {
                return set.Tables[0];
            }
            return null;
        }

        public static StyleTemplate GetStyleTemplate(int templateID)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Style_GetTemplate");
            storedProcCommandWrapper.AddInParameter("TemplateID", DbType.Int32, templateID);
            DataSet ds = database.ExecuteDataSet(storedProcCommandWrapper);
            StyleTemplate template = new StyleTemplate();
            template.Load(ds);
            return template;
        }

        public static bool IsStyleInUse(int styleTemplateID)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Style_GetRTUsing");
            storedProcCommandWrapper.AddInParameter("TemplateID", DbType.Int32, styleTemplateID);
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    if (reader.Read())
                    {
                        return true;
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            DBCommandWrapper command = database.GetStoredProcCommandWrapper("ckbx_Style_GetATUsing");
            command.AddInParameter("TemplateID", DbType.Int32, styleTemplateID);
            using (IDataReader reader2 = database.ExecuteReader(command))
            {
                try
                {
                    if (reader2.Read())
                    {
                        return true;
                    }
                }
                finally
                {
                    reader2.Close();
                }
            }
            DBCommandWrapper wrapper3 = database.GetStoredProcCommandWrapper("ckbx_Style_GetEmailUsing");
            wrapper3.AddInParameter("TemplateID", DbType.Int32, styleTemplateID);
            using (IDataReader reader3 = database.ExecuteReader(wrapper3))
            {
                try
                {
                    if (reader3.Read())
                    {
                        return true;
                    }
                }
                finally
                {
                    reader3.Close();
                }
            }
            return false;
        }

        public static bool IsStyleNameInUse(string styleName)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Style_GetIDFromName");
            storedProcCommandWrapper.AddInParameter("Name", DbType.String, styleName);
            storedProcCommandWrapper.AddOutParameter("TemplateID", DbType.Int32, 4);
            database.ExecuteNonQuery(storedProcCommandWrapper);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("TemplateID");
            return (((parameterValue != null) && (parameterValue != DBNull.Value)) && (((int) parameterValue) > 0));
        }

        public static List<StyleTemplate> ListStyleTemplates(ExtendedPrincipal currentPrincipal)
        {
            List<StyleTemplate> list = new List<StyleTemplate>();
            foreach (DataRow row in GetStyleData(currentPrincipal).Rows)
            {
                StyleTemplate styleTemplate = GetStyleTemplate(Convert.ToInt32(row["TemplateID"]));
                list.Add(styleTemplate);
            }
            return list;
        }

        public static void SaveTemplate(StyleTemplate template, ExtendedPrincipal currentPrincipal)
        {
            if (!AuthorizationFactory.GetAuthorizationProvider().Authorize(currentPrincipal, "Form.Edit"))
            {
                throw new AuthorizationException();
            }
            template.Save();
        }
    }
}

