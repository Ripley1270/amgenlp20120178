﻿namespace Checkbox.Styles
{
    using Checkbox.Common;
    using Prezza.Framework.Common;
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Runtime.CompilerServices;
    using System.Text;
    using System.Xml;

    [Serializable]
    public class StyleTemplate
    {
        private DataSet _data;
        private bool? _isEditable;
        private bool? _isPublic;
        private int? _templateID;
        private const string CssPropTableName = "CssProp";
        private const string CssTableName = "Css";
        private const string TemplateTableName = "TemplateData";

        internal StyleTemplate()
        {
            this.InitializeData();
        }

        private void Create(IDbTransaction t)
        {
            Database db = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_Style_InsertTemplate");
            storedProcCommandWrapper.AddInParameter("Name", DbType.String, this.Name);
            storedProcCommandWrapper.AddInParameter("CreatedBy", DbType.String, this.CreatedBy);
            storedProcCommandWrapper.AddInParameter("Header", DbType.String, this.HeaderHTML);
            storedProcCommandWrapper.AddInParameter("Footer", DbType.String, this.FooterHTML);
            storedProcCommandWrapper.AddInParameter("IsPublic", DbType.Boolean, this.IsPublic);
            storedProcCommandWrapper.AddInParameter("IsEditable", DbType.Boolean, this.IsEditable);
            storedProcCommandWrapper.AddOutParameter("TemplateID", DbType.Int32, 4);
            db.ExecuteNonQuery(storedProcCommandWrapper);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("TemplateID");
            if ((parameterValue == null) || (parameterValue == DBNull.Value))
            {
                throw new Exception("Unable to save template data.");
            }
            this._templateID = new int?(Convert.ToInt32(parameterValue));
            db.UpdateDataSet(this._data, "Css", this.GetElementInsertCommand(db), this.GetElementUpdateCommand(db), GetElementDeleteCommand(db), t);
            db.UpdateDataSet(this._data, "CssProp", GetElementPropertyInsertCommand(db), GetElementPropertyUpdateCommand(db), GetElementPropertyDeleteCommand(db), t);
        }

        public string GetCss()
        {
            StringBuilder builder = new StringBuilder();
            foreach (DataRow row in this._data.Tables["Css"].Select(null, null, DataViewRowState.CurrentRows))
            {
                string str = DbUtility.GetValueFromDataRow<string>(row, "ElementName", string.Empty);
                int? defaultValue = null;
                int? nullable = DbUtility.GetValueFromDataRow<int?>(row, "ElementID", defaultValue);
                if (nullable.HasValue && Utilities.IsNotNullOrEmpty(str))
                {
                    string css = this.GetCss(nullable.Value);
                    if ((str != null) && (str.Trim() != string.Empty))
                    {
                        builder.Append(str);
                        builder.Append("{");
                        builder.Append(css);
                        builder.Append("}");
                        builder.AppendLine();
                        if ((string.Compare(str, "Item", true) == 0) || (string.Compare(str, "AlternatingItem", true) == 0))
                        {
                            builder.Append(str + " td");
                            builder.Append("{");
                            builder.Append(css);
                            builder.Append("}");
                            builder.AppendLine();
                        }
                    }
                }
            }
            return builder.ToString();
        }

        protected string GetCss(int elementID)
        {
            StringBuilder builder = new StringBuilder();
            foreach (DataRow row in this._data.Tables["CssProp"].Select(string.Format("ElementID = {0}", elementID), null, DataViewRowState.CurrentRows))
            {
                string str = DbUtility.GetValueFromDataRow<string>(row, "PropertyName", string.Empty);
                string str2 = DbUtility.GetValueFromDataRow<string>(row, "PropertyValue", string.Empty);
                if ((Utilities.IsNotNullOrEmpty(str) && Utilities.IsNotNullOrEmpty(str2)) && !str2.Contains("url(\"\")"))
                {
                    builder.Append(str);
                    builder.Append(":");
                    builder.Append(str2);
                    builder.Append(";");
                }
            }
            return builder.ToString();
        }

        public string GetCss(string element)
        {
            if ((element != null) && (element.Trim() != string.Empty))
            {
                DataRow[] rowArray = this._data.Tables["Css"].Select("ElementName = '" + Utilities.SqlEncode(element) + "'", null, DataViewRowState.CurrentRows);
                if (rowArray.Length > 0)
                {
                    int? nullable = DbUtility.GetValueFromDataRow<int?>(rowArray[0], "ElementID", null);
                    if (nullable.HasValue)
                    {
                        return this.GetCss(nullable.Value);
                    }
                }
            }
            return string.Empty;
        }

        private static DBCommandWrapper GetElementDeleteCommand(Database db)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_Style_DeleteElement");
            storedProcCommandWrapper.AddInParameter("ElementID", DbType.Int32, "ElementID", DataRowVersion.Current);
            return storedProcCommandWrapper;
        }

        private DBCommandWrapper GetElementInsertCommand(Database db)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_Style_InsertElement");
            storedProcCommandWrapper.AddInParameter("TemplateID", DbType.Int32, this._templateID);
            storedProcCommandWrapper.AddInParameter("ElementName", DbType.String, "ElementName", DataRowVersion.Current);
            storedProcCommandWrapper.AddParameter("ElementID", DbType.Int32, ParameterDirection.Output, "ElementID", DataRowVersion.Current, null);
            return storedProcCommandWrapper;
        }

        public string GetElementProperty(string element, string property)
        {
            if (((element == null) || (element.Trim() == string.Empty)) || ((property == null) || (property.Trim() == string.Empty)))
            {
                return string.Empty;
            }
            Dictionary<string, string> elementStyle = this.GetElementStyle(element);
            if (!elementStyle.ContainsKey(property))
            {
                return string.Empty;
            }
            return elementStyle[property];
        }

        private static DBCommandWrapper GetElementPropertyDeleteCommand(Database db)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_Style_DeleteProperty");
            storedProcCommandWrapper.AddInParameter("ElementID", DbType.Int32, "ElementID", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("PropertyName", DbType.String, "PropertyName", DataRowVersion.Current);
            return storedProcCommandWrapper;
        }

        private static DBCommandWrapper GetElementPropertyInsertCommand(Database db)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_Style_InsertProperty");
            storedProcCommandWrapper.AddInParameter("ElementID", DbType.Int32, "ElementID", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("PropertyName", DbType.String, "PropertyName", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("PropertyValue", DbType.String, "PropertyValue", DataRowVersion.Current);
            return storedProcCommandWrapper;
        }

        private static DBCommandWrapper GetElementPropertyUpdateCommand(Database db)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_Style_UpdateProperty");
            storedProcCommandWrapper.AddInParameter("ElementID", DbType.Int32, "ElementID", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("PropertyName", DbType.String, "PropertyName", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("PropertyValue", DbType.String, "PropertyValue", DataRowVersion.Current);
            return storedProcCommandWrapper;
        }

        public Dictionary<string, string> GetElementStyle(string element)
        {
            if ((element == null) || (element.Trim() == string.Empty))
            {
                return null;
            }
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            DataRow[] rowArray = this._data.Tables["Css"].Select(string.Format("ElementName = '{0}'", Utilities.SqlEncode(element)), null, DataViewRowState.CurrentRows);
            if (rowArray.Length > 0)
            {
                int? nullable = DbUtility.GetValueFromDataRow<int?>(rowArray[0], "ElementID", null);
                if (!nullable.HasValue)
                {
                    return dictionary;
                }
                foreach (DataRow row in this._data.Tables["CssProp"].Select(string.Format("ElementID = {0}", nullable), null, DataViewRowState.CurrentRows))
                {
                    string str = DbUtility.GetValueFromDataRow<string>(row, "PropertyName", string.Empty);
                    string str2 = DbUtility.GetValueFromDataRow<string>(row, "PropertyValue", string.Empty);
                    if (Utilities.IsNotNullOrEmpty(str))
                    {
                        dictionary[str] = str2;
                    }
                }
            }
            return dictionary;
        }

        private DBCommandWrapper GetElementUpdateCommand(Database db)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_Style_UpdateElement");
            storedProcCommandWrapper.AddInParameter("TemplateID", DbType.Int32, this._templateID);
            storedProcCommandWrapper.AddInParameter("ElementID", DbType.Int32, "ElementID", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("ElementName", DbType.String, "ElementName", DataRowVersion.Current);
            return storedProcCommandWrapper;
        }

        private void InitializeData()
        {
            DataTable table = new DataTable {
                TableName = "Css"
            };
            table.Columns.Add("ElementName", typeof(string));
            table.Columns.Add("ElementID", typeof(int));
            table.Columns["ElementID"].AutoIncrement = true;
            table.Columns["ElementID"].AutoIncrementSeed = -1L;
            table.Columns["ElementID"].AutoIncrementStep = -1L;
            DataTable table2 = new DataTable {
                TableName = "CssProp"
            };
            table2.Columns.Add("ElementID", typeof(int));
            table2.Columns.Add("PropertyName", typeof(string));
            table2.Columns.Add("PropertyValue", typeof(string));
            this._data = new DataSet();
            this._data.Tables.Add(table);
            this._data.Tables.Add(table2);
            DataRelation relation = new DataRelation("ElemID", table.Columns["ElementID"], table2.Columns["ElementID"]);
            this._data.Relations.Add(relation);
            relation.ChildKeyConstraint.AcceptRejectRule = AcceptRejectRule.None;
        }

        internal void Load(DataSet ds)
        {
            if (ds == null)
            {
                throw new Exception("DataSet was null.");
            }
            if (ds.Tables.Count < 3)
            {
                throw new Exception("DataSet did not contain the expected number of tables.");
            }
            ds.Tables[0].TableName = "TemplateData";
            ds.Tables[1].TableName = "Css";
            ds.Tables[2].TableName = "CssProp";
            if (ds.Tables["TemplateData"].Rows.Count <= 0)
            {
                throw new Exception("TemplateData table has no rows.");
            }
            DataRow tableRow = ds.Tables["TemplateData"].Rows[0];
            this._templateID = (int?) tableRow["TemplateID"];
            this.HeaderHTML = DbUtility.GetValueFromDataRow<string>(tableRow, "Header", null);
            this.FooterHTML = DbUtility.GetValueFromDataRow<string>(tableRow, "Footer", null);
            this.Name = DbUtility.GetValueFromDataRow<string>(tableRow, "Name", null);
            this.CreatedBy = DbUtility.GetValueFromDataRow<string>(tableRow, "CreatedBy", null);
            this._isPublic = new bool?(DbUtility.GetValueFromDataRow<bool>(tableRow, "IsPublic", false));
            this._isEditable = new bool?(DbUtility.GetValueFromDataRow<bool>(tableRow, "IsEditable", false));
            this.DateCreated = DbUtility.GetValueFromDataRow<DateTime?>(tableRow, "DateCreated", null);
            this._data.Tables["CssProp"].Rows.Clear();
            this._data.Tables["Css"].Rows.Clear();
            foreach (DataRow row2 in ds.Tables["Css"].Rows)
            {
                this._data.Tables["Css"].ImportRow(row2);
            }
            foreach (DataRow row3 in ds.Tables["CssProp"].Rows)
            {
                this._data.Tables["CssProp"].ImportRow(row3);
            }
        }

        internal void Load(string xml)
        {
            if ((xml != null) && (xml.Trim() != string.Empty))
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(xml);
                XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
                nsmgr.AddNamespace("prz", "http://www.prezzatech.com/schemas/StyleTemplateSchema.xsd");
                if (xmlDoc.SelectSingleNode("/prz:StyleTemplate", nsmgr) != null)
                {
                    this.LoadPreviousVersionExport(xmlDoc, nsmgr);
                }
                else
                {
                    this.Name = XmlUtility.GetNodeText(xmlDoc.SelectSingleNode("/CssDocument/TemplateName"));
                    this.HeaderHTML = XmlUtility.GetNodeText(xmlDoc.SelectSingleNode("/CssDocument/Header"));
                    this.FooterHTML = XmlUtility.GetNodeText(xmlDoc.SelectSingleNode("/CssDocument/Footer"));
                    this.LoadCssClasses(xmlDoc);
                }
            }
        }

        internal void LoadCssClasses(XmlDocument xmlDoc)
        {
            foreach (XmlNode node in xmlDoc.SelectNodes("/CssDocument/CssClass"))
            {
                string nodeText = XmlUtility.GetNodeText(node.SelectSingleNode("Name"));
                foreach (XmlNode node2 in node.SelectNodes("Style"))
                {
                    string property = XmlUtility.GetNodeText(node2.SelectSingleNode("Property"));
                    string str3 = XmlUtility.GetNodeText(node2.SelectSingleNode("Value"));
                    if ((nodeText != string.Empty) && (property != string.Empty))
                    {
                        this.SetElementStyleProperty(nodeText, property, str3);
                    }
                }
            }
        }

        private void LoadPreviousVersionExport(XmlDocument xmlDoc, XmlNamespaceManager ns)
        {
            this.Name = XmlUtility.GetNodeText(xmlDoc.SelectSingleNode("/prz:StyleTemplate/prz:TemplateName", ns));
            string str = XmlUtility.GetNodeText(xmlDoc.SelectSingleNode("/prz:StyleTemplate/prz:CustomHeader", ns)).Replace("&gt;", ">").Replace("&lt;", "<");
            this.HeaderHTML = str;
            string str2 = XmlUtility.GetNodeText(xmlDoc.SelectSingleNode("/prz:StyleTemplate/prz:CustomFooter", ns)).Replace("&gt;", ">").Replace("&lt;", "<");
            this.FooterHTML = str2;
            XmlNode node = xmlDoc.SelectSingleNode("/prz:StyleTemplate/prz:Css", ns);
            if (node != null)
            {
                XmlDocument document = new XmlDocument();
                string xml = node.InnerXml.Replace("&gt;", ">").Replace("&lt;", "<");
                document.LoadXml(xml);
                this.LoadCssClasses(document);
            }
        }

        public void RemoveElement(string element)
        {
            if ((element != null) && (element.Trim() != string.Empty))
            {
                foreach (DataRow row in this._data.Tables["Css"].Select("ElementName = '" + Utilities.SqlEncode(element) + "'", null, DataViewRowState.CurrentRows))
                {
                    row.Delete();
                }
            }
        }

        protected void RemoveElementStyleProperty(int elementID, string property)
        {
            if ((property != null) && (property.Trim() != string.Empty))
            {
                foreach (DataRow row in this._data.Tables["CssProp"].Select(string.Format("ElementID = {0} AND PropertyName = '{1}'", elementID, property), null, DataViewRowState.CurrentRows))
                {
                    row.Delete();
                }
            }
        }

        public void RemoveElementStyleProperty(string element, string property)
        {
            if (((element != null) && (element.Trim() != string.Empty)) && ((property != null) && (property.Trim() != string.Empty)))
            {
                DataRow[] rowArray = this._data.Tables["Css"].Select(string.Format("ElementName = '{0}'", Utilities.SqlEncode(element)), null, DataViewRowState.CurrentRows);
                if (rowArray.Length > 0)
                {
                    int? nullable = DbUtility.GetValueFromDataRow<int?>(rowArray[0], "ElementID", null);
                    if (nullable.HasValue)
                    {
                        this.RemoveElementStyleProperty(nullable.Value, property);
                    }
                }
            }
        }

        internal void Save()
        {
            try
            {
                using (IDbConnection connection = DatabaseFactory.CreateDatabase().GetConnection())
                {
                    connection.Open();
                    IDbTransaction t = connection.BeginTransaction();
                    try
                    {
                        if (this._templateID.HasValue && (this._templateID.Value > 0))
                        {
                            this.Update(t);
                        }
                        else
                        {
                            this.Create(t);
                        }
                        t.Commit();
                    }
                    catch
                    {
                        t.Rollback();
                        throw new Exception("Unable to save style data.");
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "UIProcess"))
                {
                    throw;
                }
            }
        }

        public void SetElementStyle(string element, Dictionary<string, string> styles)
        {
            if (((element != null) && (element.Trim() != string.Empty)) && (styles != null))
            {
                DataRow[] rowArray = this._data.Tables["Css"].Select("ElementName = '" + Utilities.SqlEncode(element) + "'", null, DataViewRowState.CurrentRows);
                if (rowArray.Length > 0)
                {
                    int? nullable = DbUtility.GetValueFromDataRow<int?>(rowArray[0], "ElementID", null);
                    if (nullable.HasValue)
                    {
                        foreach (DataRow row in this._data.Tables["CssProp"].Select(string.Format("ElementID = {0}", nullable), null, DataViewRowState.CurrentRows))
                        {
                            string str = DbUtility.GetValueFromDataRow<string>(row, "PropertyName", string.Empty);
                            if (Utilities.IsNotNullOrEmpty(str) && !styles.ContainsKey(str))
                            {
                                this.RemoveElementStyleProperty(nullable.Value, str);
                            }
                        }
                    }
                }
                foreach (string str2 in styles.Keys)
                {
                    this.SetElementStyleProperty(element, str2, styles[str2]);
                }
            }
        }

        public void SetElementStyleProperty(string element, string property, string value)
        {
            if (((element != null) && (element.Trim() != string.Empty)) && ((property != null) && (property.Trim() != string.Empty)))
            {
                DataRow row;
                DataRow[] rowArray = this._data.Tables["Css"].Select("ElementName = '" + Utilities.SqlEncode(element) + "'", null, DataViewRowState.CurrentRows);
                if (rowArray.Length > 0)
                {
                    row = rowArray[0];
                }
                else
                {
                    row = this._data.Tables["Css"].NewRow();
                    row["ElementName"] = element;
                    this._data.Tables["Css"].Rows.Add(row);
                }
                int? nullable = DbUtility.GetValueFromDataRow<int?>(row, "ElementID", null);
                if (nullable.HasValue)
                {
                    DataRow[] rowArray2 = this._data.Tables["CssProp"].Select(string.Format("ElementID = {0} AND PropertyName = '{1}'", nullable, property), null, DataViewRowState.CurrentRows);
                    if (rowArray2.Length == 0)
                    {
                        DataRow row2 = this._data.Tables["CssProp"].NewRow();
                        row2["ElementID"] = nullable;
                        row2["PropertyName"] = property;
                        row2["PropertyValue"] = value ?? string.Empty;
                        row2.SetParentRow(row);
                        this._data.Tables["CssProp"].Rows.Add(row2);
                    }
                    else if (DbUtility.GetValueFromDataRow<string>(rowArray2[0], "PropertyValue", string.Empty) != value)
                    {
                        rowArray2[0]["PropertyValue"] = value ?? string.Empty;
                    }
                }
            }
        }

        internal void SetTemplateID(int templateID)
        {
            this._templateID = new int?(templateID);
        }

        public XmlDocument ToXml()
        {
            Dictionary<string, Dictionary<string, string>> elements = this.Elements;
            XmlDocument document = new XmlDocument();
            XmlNode newChild = document.CreateElement("CssDocument");
            XmlNode node2 = document.CreateElement("TemplateName");
            node2.InnerText = this.Name;
            newChild.AppendChild(node2);
            XmlElement element = document.CreateElement("Header");
            XmlElement element2 = document.CreateElement("Footer");
            newChild.AppendChild(element);
            newChild.AppendChild(element2);
            foreach (string str in elements.Keys)
            {
                Dictionary<string, string> dictionary2 = elements[str];
                XmlNode node3 = document.CreateElement("CssClass");
                XmlNode node4 = document.CreateElement("Name");
                node4.InnerText = str;
                node3.AppendChild(node4);
                foreach (string str2 in dictionary2.Keys)
                {
                    XmlNode node5 = document.CreateElement("Style");
                    XmlNode node6 = document.CreateElement("Property");
                    XmlNode node7 = document.CreateElement("Value");
                    node6.InnerText = str2;
                    node7.InnerText = dictionary2[str2];
                    node5.AppendChild(node6);
                    node5.AppendChild(node7);
                    node3.AppendChild(node5);
                }
                newChild.AppendChild(node3);
            }
            document.AppendChild(newChild);
            return document;
        }

        private void Update(IDbTransaction t)
        {
            Database db = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_Style_UpdateTemplate");
            storedProcCommandWrapper.AddInParameter("TemplateID", DbType.Int32, this._templateID);
            storedProcCommandWrapper.AddInParameter("Name", DbType.String, this.Name);
            storedProcCommandWrapper.AddInParameter("CreatedBy", DbType.String, this.CreatedBy);
            storedProcCommandWrapper.AddInParameter("Header", DbType.String, this.HeaderHTML);
            storedProcCommandWrapper.AddInParameter("Footer", DbType.String, this.FooterHTML);
            storedProcCommandWrapper.AddInParameter("IsPublic", DbType.Boolean, this.IsPublic);
            storedProcCommandWrapper.AddInParameter("IsEditable", DbType.Boolean, this.IsEditable);
            db.ExecuteNonQuery(storedProcCommandWrapper);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("TemplateID");
            if ((parameterValue == null) || (parameterValue == DBNull.Value))
            {
                throw new Exception("Unable to save template data.");
            }
            this._templateID = new int?(Convert.ToInt32(parameterValue));
            db.UpdateDataSet(this._data, "Css", this.GetElementInsertCommand(db), this.GetElementUpdateCommand(db), GetElementDeleteCommand(db), t);
            db.UpdateDataSet(this._data, "CssProp", GetElementPropertyInsertCommand(db), GetElementPropertyUpdateCommand(db), GetElementPropertyDeleteCommand(db), t);
        }

        public string CreatedBy { get; set; }

        public DateTime? DateCreated { get; set; }

        public Dictionary<string, Dictionary<string, string>> Elements
        {
            get
            {
                Dictionary<string, Dictionary<string, string>> dictionary = new Dictionary<string, Dictionary<string, string>>();
                foreach (DataRow row in this._data.Tables["Css"].Select(null, null, DataViewRowState.CurrentRows))
                {
                    int? defaultValue = null;
                    int? nullable = DbUtility.GetValueFromDataRow<int?>(row, "ElementID", defaultValue);
                    string str = DbUtility.GetValueFromDataRow<string>(row, "ElementName", string.Empty);
                    if (nullable.HasValue && Utilities.IsNotNullOrEmpty(str))
                    {
                        Dictionary<string, string> dictionary2 = new Dictionary<string, string>();
                        foreach (DataRow row2 in this._data.Tables["CssProp"].Select("ElementID = " + nullable, null, DataViewRowState.CurrentRows))
                        {
                            string str2 = DbUtility.GetValueFromDataRow<string>(row2, "PropertyName", string.Empty);
                            string str3 = DbUtility.GetValueFromDataRow<string>(row2, "PropertyValue", string.Empty);
                            if (Utilities.IsNotNullOrEmpty(str2) && Utilities.IsNotNullOrEmpty(str3))
                            {
                                dictionary2[str2] = str3;
                            }
                        }
                        dictionary[str] = dictionary2;
                    }
                }
                return dictionary;
            }
        }

        [Obsolete("FooterHTML is localizable, so the TextManager should be used to get/set the text.")]
        public string FooterHTML { get; set; }

        public string FooterTextID
        {
            get
            {
                if (this.TemplateID.HasValue)
                {
                    return ("/styleTemplate/" + this.TemplateID + "/footer");
                }
                return null;
            }
        }

        [Obsolete("HeaderHTML is localizable, so the TextManager should be used to get/set the text.")]
        public string HeaderHTML { get; set; }

        public string HeaderTextID
        {
            get
            {
                if (this.TemplateID.HasValue)
                {
                    return ("/styleTemplate/" + this.TemplateID + "/header");
                }
                return null;
            }
        }

        public bool IsEditable
        {
            get
            {
                if (!this._isEditable.HasValue)
                {
                    return false;
                }
                return this._isEditable.Value;
            }
            set
            {
                this._isEditable = new bool?(value);
            }
        }

        public bool IsPublic
        {
            get
            {
                if (!this._isPublic.HasValue)
                {
                    return false;
                }
                return this._isPublic.Value;
            }
            set
            {
                this._isPublic = new bool?(value);
            }
        }

        public string Name { get; set; }

        public int? TemplateID
        {
            get
            {
                return new int?(this._templateID.Value);
            }
        }
    }
}

