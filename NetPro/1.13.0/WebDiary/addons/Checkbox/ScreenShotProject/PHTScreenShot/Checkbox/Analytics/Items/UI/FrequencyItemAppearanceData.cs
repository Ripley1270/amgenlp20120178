﻿namespace Checkbox.Analytics.Items.UI
{
    using System;

    [Serializable]
    public class FrequencyItemAppearanceData : AnalysisItemAppearanceData
    {
        public override string AppearanceCode
        {
            get
            {
                return "ANALYSIS_SUMMARY";
            }
        }
    }
}

