﻿namespace Checkbox.Forms.Items
{
    using Checkbox.Common;
    using Prezza.Framework.Data;
    using System;
    using System.Collections.Generic;
    using System.Data;

    public static class ItemFormatterFactory
    {
        private static Dictionary<string, Type> _typeCache;

        private static Type GetFormatterType(int itemTypeId, string format)
        {
            string key = itemTypeId + "_" + format;
            if (!TypeCache.ContainsKey(key))
            {
                return LookupFormatterType(itemTypeId, format);
            }
            return TypeCache[key];
        }

        public static IItemFormatter GetItemFormatter(int itemTypeId, string format)
        {
            Type formatterType = GetFormatterType(itemTypeId, format);
            if (formatterType != null)
            {
                return (Activator.CreateInstance(formatterType) as IItemFormatter);
            }
            return null;
        }

        private static Type LookupFormatterType(int itemTypeId, string format)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemFormatter_Get");
            storedProcCommandWrapper.AddInParameter("ItemTypeId", DbType.Int32, itemTypeId);
            storedProcCommandWrapper.AddInParameter("Format", DbType.String, format);
            Type type = null;
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    if (reader.Read())
                    {
                        string str = DbUtility.GetValueFromDataReader<string>(reader, "FormatterClassName", null);
                        string str2 = DbUtility.GetValueFromDataReader<string>(reader, "FormatterAssembly", null);
                        if (Utilities.IsNotNullOrEmpty(str) && Utilities.IsNotNullOrEmpty(str2))
                        {
                            type = Type.GetType(str + "," + str2);
                        }
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return type;
        }

        private static Dictionary<string, Type> TypeCache
        {
            get
            {
                if (_typeCache == null)
                {
                    lock (typeof(ItemFormatterFactory))
                    {
                        _typeCache = new Dictionary<string, Type>();
                    }
                }
                return _typeCache;
            }
        }
    }
}

