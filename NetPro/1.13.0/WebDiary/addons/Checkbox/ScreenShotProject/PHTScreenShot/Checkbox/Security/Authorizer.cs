﻿namespace Checkbox.Security
{
    using Prezza.Framework.Data;
    using Prezza.Framework.Security;
    using Prezza.Framework.Security.Principal;
    using System;
    using System.Collections.Specialized;
    using System.Data;
    using System.Runtime.InteropServices;

    public static class Authorizer
    {
        public static DataTable AuthorizeDataTable(ExtendedPrincipal currentPrincipal, DataTable inTable, int pageNumber, int resultsPerPage, string idColumnName, AccessControllableDelegate getControllable, string[] permissions, out int itemCount)
        {
            return AuthorizeDataTable(currentPrincipal, inTable, pageNumber, resultsPerPage, idColumnName, getControllable, permissions, null, out itemCount);
        }

        public static DataTable AuthorizeDataTable(ExtendedPrincipal currentPrincipal, DataTable inTable, int pageNumber, int resultsPerPage, string idColumnName, AccessControllableDelegate getControllable, string[] permissions, NameValueCollection permissionColumnMap, out int itemCount)
        {
            int? nullable = null;
            int? nullable2 = null;
            if ((pageNumber > 0) && (resultsPerPage > 0))
            {
                nullable = new int?((pageNumber - 1) * resultsPerPage);
                int? nullable3 = nullable;
                int num2 = resultsPerPage;
                nullable2 = (nullable3.HasValue ? new int?(nullable3.GetValueOrDefault() + num2) : 0) - 1;
            }
            IAuthorizationProvider authorizationProvider = AuthorizationFactory.GetAuthorizationProvider();
            itemCount = 0;
            DataTable table = inTable.Clone();
            if (permissionColumnMap != null)
            {
                foreach (string str in permissionColumnMap.AllKeys)
                {
                    if (!table.Columns.Contains(str))
                    {
                        table.Columns.Add(str, typeof(bool));
                    }
                }
            }
            foreach (DataRow row in inTable.Select())
            {
                int controllableId = DbUtility.GetValueFromDataRow<int>(row, idColumnName, -1);
                if (controllableId > 0)
                {
                    IAccessControllable resource = getControllable(controllableId);
                    if (resource != null)
                    {
                        if (nullable.HasValue)
                        {
                            if (itemCount < nullable)
                            {
                                continue;
                            }
                            if (itemCount > nullable2)
                            {
                                continue;
                            }
                        }
                        DataRow row2 = table.NewRow();
                        foreach (DataColumn column in inTable.Columns)
                        {
                            row2[column.ColumnName] = row[column.ColumnName];
                        }
                        if (permissionColumnMap != null)
                        {
                            foreach (string str2 in permissionColumnMap.AllKeys)
                            {
                                row2[str2] = authorizationProvider.Authorize(currentPrincipal, resource, permissionColumnMap[str2]);
                            }
                        }
                        table.Rows.Add(row2);
                        itemCount++;
                    }
                }
            }
            return table;
        }
    }
}

