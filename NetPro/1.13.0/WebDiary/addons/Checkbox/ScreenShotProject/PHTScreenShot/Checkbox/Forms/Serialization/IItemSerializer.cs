﻿namespace Checkbox.Forms.Serialization
{
    using Checkbox.Forms.Items;
    using Checkbox.Forms.Items.UI;
    using System;

    public interface IItemSerializer
    {
        string SerializeItem(Item item, AppearanceData itemAppearance);
    }
}

