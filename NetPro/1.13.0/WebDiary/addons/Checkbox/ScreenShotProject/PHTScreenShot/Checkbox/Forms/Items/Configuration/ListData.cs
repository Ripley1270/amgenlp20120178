﻿namespace Checkbox.Forms.Items.Configuration
{
    using Checkbox.Common;
    using Checkbox.Globalization.Text;
    using Prezza.Framework.Data;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class ListData : PersistedDomainObject, ICloneable, IEquatable<ListData>, IEquatable<int>
    {
        private DataSet _data;
        private bool _dirty = true;
        private bool _imported = false;
        private readonly List<ListOptionData> _listOptions = new List<ListOptionData>();
        private object _lockObject = new object();

        public void AddOption(int itemID, string alias, bool isDefault, int position, double points, bool isOther)
        {
            DataRow row = this.Data.Tables[this.OptionsTableName].NewRow();
            row["ItemID"] = itemID;
            row["Alias"] = alias;
            row["IsDefault"] = isDefault;
            row["Position"] = position;
            row["IsOther"] = isOther;
            row["Points"] = points;
            this.Data.Tables[this.OptionsTableName].Rows.Add(row);
            this._dirty = true;
        }

        public object Clone()
        {
            ListData data = new ListData();
            foreach (ListOptionData data2 in this.ListOptions)
            {
                data.AddOption(-1, data2.Alias, data2.IsDefault, data2.Position, data2.Points, data2.IsOther);
            }
            return data;
        }

        protected override void Create(IDbTransaction transaction)
        {
            Database db = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_ListData_Create");
            storedProcCommandWrapper.AddOutParameter("ListID", DbType.Int32, 4);
            db.ExecuteNonQuery(storedProcCommandWrapper, transaction);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("ListID");
            if ((parameterValue == null) || (parameterValue == DBNull.Value))
            {
                throw new Exception("Unable to create item options data.");
            }
            base.ID = new int?((int) parameterValue);
            db.UpdateDataSet(this.Data, this.OptionsTableName, this.GetCreateItemOptionCommand(db), GetUpdateItemOptionCommand(db), GetDeleteItemOptionCommand(db), transaction);
            if (this._imported)
            {
                this.SaveOptionTextTable();
            }
            this._dirty = true;
        }

        protected override void CreateDataRelationsInternal(DataSet ds)
        {
            base.CreateDataRelationsInternal(ds);
            string name = "ItemLists_" + this.OptionsTableName;
            if ((!ds.Relations.Contains(name) && ds.Tables.Contains("ItemLists")) && ds.Tables.Contains(this.OptionsTableName))
            {
                DataRelation relation = new DataRelation(name, ds.Tables["ItemLists"].Columns["ListID"], ds.Tables[this.OptionsTableName].Columns["ListID"]);
                ds.Relations.Add(relation);
            }
            this.CreateTextDataRelations(ds);
        }

        public virtual void CreateTextDataRelations(DataSet ds)
        {
            string name = this.OptionsTableName + "_OptionTexts";
            if ((!ds.Relations.Contains(name) && ds.Tables.Contains("OptionTexts")) && ds.Tables.Contains(this.OptionsTableName))
            {
                DataRelation relation = new DataRelation(name, ds.Tables[this.OptionsTableName].Columns["OptionID"], ds.Tables["OptionTexts"].Columns["OptionID"]);
                ds.Relations.Add(relation);
            }
        }

        public override void Delete(IDbTransaction t)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public bool Equals(ListData other)
        {
            return (other.ID == base.ID);
        }

        public bool Equals(int other)
        {
            int num = other;
            return (num == base.ID);
        }

        public override DataSet GetConfigurationDataSet()
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ListData_Get");
            storedProcCommandWrapper.AddInParameter("ListID", DbType.Int32, base.ID);
            DataSet ds = database.ExecuteDataSet(storedProcCommandWrapper);
            if (ds.Tables.Count == 2)
            {
                ds.Tables[0].TableName = this.DataTableName;
                ds.Tables[1].TableName = this.OptionsTableName;
            }
            foreach (DataRow row in ds.Tables[this.OptionsTableName].Rows)
            {
                if (row["TextID"] != DBNull.Value)
                {
                    DataTable textData = TextManager.GetTextData((string) row["TextID"]);
                    this.MergeTextData(ds, textData, Convert.ToInt32(row["OptionID"]), "text");
                }
            }
            return ds;
        }

        private DBCommandWrapper GetCreateItemOptionCommand(Database db)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_ListData_InsertOption");
            storedProcCommandWrapper.AddInParameter("ListID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, this.ItemID);
            storedProcCommandWrapper.AddInParameter("Alias", DbType.String, "Alias", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("IsDefault", DbType.Boolean, "IsDefault", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("Position", DbType.Int32, "Position", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("IsOther", DbType.Boolean, "IsOther", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("Points", DbType.Double, "Points", DataRowVersion.Current);
            storedProcCommandWrapper.AddParameter("OptionID", DbType.Int32, ParameterDirection.Output, "OptionID", DataRowVersion.Current, null);
            return storedProcCommandWrapper;
        }

        private static DBCommandWrapper GetDeleteItemOptionCommand(Database db)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_ListData_DeleteOption");
            storedProcCommandWrapper.AddInParameter("OptionID", DbType.Int32, "OptionID", DataRowVersion.Current);
            return storedProcCommandWrapper;
        }

        public ListOptionData GetOptionAt(int position)
        {
            DataRow[] rowArray = this.Data.Tables[this.OptionsTableName].Select("Position = " + position, null, DataViewRowState.CurrentRows);
            if (rowArray.Length > 0)
            {
                return new ListOptionData(rowArray[0]);
            }
            return null;
        }

        private static DBCommandWrapper GetUpdateItemOptionCommand(Database db)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_ListData_UpdateOption");
            storedProcCommandWrapper.AddInParameter("OptionID", DbType.Int32, "OptionID", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("TextID", DbType.String, "TextID", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("Alias", DbType.String, "Alias", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("IsDefault", DbType.Boolean, "IsDefault", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("Position", DbType.Int32, "Position", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("IsOther", DbType.Boolean, "IsOther", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("Points", DbType.Double, "Points", DataRowVersion.Current);
            return storedProcCommandWrapper;
        }

        public void Initialize(int? listId)
        {
            base.ID = listId;
            this.InitializeDataTable();
        }

        private void InitializeDataTable()
        {
            DataTable table = new DataTable {
                TableName = this.OptionsTableName
            };
            table.Columns.Add("OptionID", typeof(int));
            table.Columns.Add("ItemID", typeof(int));
            table.Columns.Add("TextID", typeof(string));
            table.Columns.Add("Alias", typeof(string));
            table.Columns.Add("IsDefault", typeof(bool));
            table.Columns.Add("Position", typeof(int));
            table.Columns.Add("IsOther", typeof(bool));
            table.Columns.Add("Points", typeof(double));
            table.Columns.Add("Deleted", typeof(bool));
            table.Columns.Add("ListID", typeof(int));
            table.Columns.Add("OptionText", typeof(string));
            DataColumn[] columnArray = new DataColumn[] { table.Columns["OptionID"] };
            table.Columns["OptionID"].AutoIncrement = true;
            table.Columns["OptionID"].AutoIncrementSeed = -1L;
            table.Columns["OptionID"].AutoIncrementStep = -1L;
            table.PrimaryKey = columnArray;
            DataTable table2 = this.InitializeTextTable();
            this._data = new DataSet();
            this._data.Tables.Add(table);
            this._data.Tables.Add(table2);
            DataRelation relation = new DataRelation("OptionData_OptionTexts", table.Columns["OptionID"], table2.Columns["OptionID"]);
            this._data.Relations.Add(relation);
        }

        protected virtual DataTable InitializeTextTable()
        {
            DataTable table = new DataTable {
                TableName = this.TextTableName
            };
            table.Columns.Add("OptionID", typeof(int));
            table.Columns.Add("TextIDSuffix", typeof(string));
            table.Columns.Add("ComputedTextID", typeof(string));
            table.Columns.Add("TextID", typeof(string));
            table.Columns.Add("LanguageCode", typeof(string));
            table.Columns.Add("TextValue", typeof(string));
            table.Columns["ComputedTextID"].Expression = "Iif(OptionID IS NULL OR TextIDSuffix IS NULL, '', '/listOption/' + OptionID + '/' + TextIDSuffix)";
            return table;
        }

        public override void Load(DataSet data)
        {
            base.Load(data);
            this._imported = base.ID <= 0;
            if ((data != null) && data.Tables.Contains(this.OptionsTableName))
            {
                this._dirty = true;
                foreach (DataRow row in data.Tables[this.OptionsTableName].Select("ListID = " + base.ID + " AND (Deleted IS NULL OR Deleted = 0)", "Position ASC", DataViewRowState.CurrentRows))
                {
                    if (this._imported)
                    {
                        DataRow row2 = this.Data.Tables[this.OptionsTableName].NewRow();
                        row2["Alias"] = row["Alias"];
                        row2["IsDefault"] = row["IsDefault"];
                        row2["Position"] = row["Position"];
                        row2["IsOther"] = row["IsOther"];
                        row2["Points"] = row["Points"];
                        row2["OptionID"] = row["OptionID"];
                        this.Data.Tables[this.OptionsTableName].Rows.Add(row2);
                    }
                    else
                    {
                        this.Data.Tables[this.OptionsTableName].ImportRow(row);
                    }
                    int num = Convert.ToInt32(row["OptionID"]);
                    DataRow[] rowArray2 = null;
                    if (data.Tables.Contains(this.TextTableName))
                    {
                        rowArray2 = data.Tables[this.TextTableName].Select("OptionID=" + num, null, DataViewRowState.CurrentRows);
                    }
                    if ((rowArray2 != null) && (rowArray2.Length > 0))
                    {
                        foreach (DataRow row3 in rowArray2)
                        {
                            this.Data.Tables[this.TextTableName].ImportRow(row3);
                        }
                    }
                }
            }
        }

        public virtual DataSet LoadTextData()
        {
            DataSet ds = new DataSet();
            ds.Tables.Add(this.InitializeTextTable());
            foreach (ListOptionData data in this.ListOptions)
            {
                if ((data.TextID != null) && (data.TextID.Trim() != string.Empty))
                {
                    this.MergeTextData(ds, TextManager.GetTextData(data.TextID), data.OptionID, "text");
                }
            }
            return ds;
        }

        protected virtual void MergeTextData(DataSet ds, DataTable data, int optionID, string suffix)
        {
            if (data != null)
            {
                data.TableName = this.TextTableName;
                if (!data.Columns.Contains("OptionID"))
                {
                    data.Columns.Add("OptionID", typeof(int));
                }
                if (!data.Columns.Contains("TextIDSuffix"))
                {
                    data.Columns.Add("TextIDSuffix", typeof(string));
                }
                if (!data.Columns.Contains("ComputedTextID"))
                {
                    data.Columns.Add("ComputedTextID", typeof(string));
                    data.Columns["ComputedTextID"].Expression = "Iif(OptionID IS NULL OR TextIDSuffix IS NULL, '', '/listOption/' + OptionID + '/' + TextIDSuffix)";
                }
                foreach (DataRow row in data.Rows)
                {
                    row["OptionID"] = optionID;
                    row["TextIDSuffix"] = suffix;
                }
                ds.Merge(data);
                if (ds.Tables.Contains(this.TextTableName) && ds.Tables[this.TextTableName].Columns.Contains("ComputedTextID"))
                {
                    ds.Tables[this.TextTableName].Columns["ComputedTextID"].Expression = "Iif(OptionID IS NULL OR TextIDSuffix IS NULL, '', '/listOption/' + OptionID + '/' + TextIDSuffix)";
                }
            }
        }

        public void RemoveOption(int optionID)
        {
            DataRow[] rowArray = this.Data.Tables[this.OptionsTableName].Select("OptionID = " + optionID, null, DataViewRowState.CurrentRows);
            for (int i = 0; i < rowArray.Length; i++)
            {
                rowArray[i].Delete();
                this._dirty = true;
            }
        }

        protected virtual void SaveOptionTextTable()
        {
            if (this.Data.Tables.Contains(this.TextTableName))
            {
                foreach (DataRow row in this.Data.Tables[this.TextTableName].Select(null, null, DataViewRowState.CurrentRows))
                {
                    if (((row["LanguageCode"] != DBNull.Value) && (row["TextValue"] != DBNull.Value)) && (row["ComputedTextID"] != DBNull.Value))
                    {
                        string languageCode = (string) row["LanguageCode"];
                        string textValue = (string) row["TextValue"];
                        string textIdentifier = (string) row["ComputedTextID"];
                        TextManager.SetText(textIdentifier, languageCode, textValue);
                    }
                }
            }
        }

        protected override void Update(IDbTransaction transaction)
        {
            Database db = DatabaseFactory.CreateDatabase();
            db.UpdateDataSet(this.Data, this.OptionsTableName, this.GetCreateItemOptionCommand(db), GetUpdateItemOptionCommand(db), GetDeleteItemOptionCommand(db), transaction);
            this._dirty = true;
        }

        public void UpdateOption(int optionID, int itemID, string alias, bool isDefault, int position, double points, bool isOther)
        {
            DataRow[] rowArray = this.Data.Tables[this.OptionsTableName].Select("OptionID = " + optionID, null, DataViewRowState.CurrentRows);
            if (rowArray.Length > 0)
            {
                rowArray[0]["ItemID"] = itemID;
                rowArray[0]["Alias"] = alias;
                rowArray[0]["IsDefault"] = isDefault;
                rowArray[0]["Position"] = position;
                rowArray[0]["IsOther"] = isOther;
                rowArray[0]["Points"] = points;
                this._dirty = true;
            }
        }

        private DataSet Data
        {
            get
            {
                if (this._data == null)
                {
                    this.Initialize(null);
                }
                return this._data;
            }
        }

        public override string DataTableName
        {
            get
            {
                return "ListData";
            }
        }

        public string DescriptionTextID
        {
            get
            {
                if (base.ID > 0)
                {
                    return ("/listOptionData/" + base.ID + "/description");
                }
                return string.Empty;
            }
        }

        public override string IdentityColumnName
        {
            get
            {
                return "ListId";
            }
        }

        public int? ItemID { get; set; }

        public ReadOnlyCollection<ListOptionData> ListOptions
        {
            get
            {
                if (this._dirty)
                {
                    lock (this._lockObject)
                    {
                        if (this._dirty)
                        {
                            this._listOptions.Clear();
                            foreach (DataRow row in this.Data.Tables[this.OptionsTableName].Select(null, "Position ASC", DataViewRowState.CurrentRows))
                            {
                                ListOptionData item = new ListOptionData(row);
                                this._listOptions.Add(item);
                            }
                        }
                        this._dirty = false;
                    }
                }
                return new ReadOnlyCollection<ListOptionData>(this._listOptions);
            }
        }

        public string NameTextID
        {
            get
            {
                if (base.ID > 0)
                {
                    return ("/listOptionData/" + base.ID + "/name");
                }
                return string.Empty;
            }
        }

        protected virtual string OptionsTableName
        {
            get
            {
                return "ItemOptions";
            }
        }

        protected virtual string TextTableName
        {
            get
            {
                return "OptionTexts";
            }
        }
    }
}

