﻿namespace Checkbox.Analytics.Items
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class MatrixSummaryAnalysisData
    {
        private string _averageKey;
        private Dictionary<int, List<string>> _columnOptions;
        private Dictionary<int, string> _columnTexts;
        private Dictionary<int, string> _defaultOtherTexts;
        private Dictionary<string, MatrixAnswer> _matrixAnswers;
        private DataTable _otherData;
        private Dictionary<string, string> _otherTexts;
        private Dictionary<int, string> _rowTexts;
        private Dictionary<string, double> _scaleAverages;

        public MatrixSummaryAnalysisData()
        {
            this.InitializeData();
        }

        public virtual void AddAnswer(int row, int column, string answer, int count)
        {
            string key = string.Concat(new object[] { row, "_", column, "_", answer });
            if (this._matrixAnswers.ContainsKey(key))
            {
                MatrixAnswer local1 = this._matrixAnswers[key];
                local1.Count += count;
            }
            else
            {
                MatrixAnswer answer2 = new MatrixAnswer {
                    Row = row,
                    Column = column,
                    Answer = answer,
                    Count = count
                };
                this._matrixAnswers[key] = answer2;
            }
        }

        public virtual void AddColumnOptionTexts(int column, List<string> optionTexts)
        {
            this._columnOptions[column] = optionTexts;
        }

        public virtual void AddColumnQuestionText(int column, string text)
        {
            this._columnTexts[column] = text;
        }

        public virtual void AddDefaultOtherText(int row, string otherText)
        {
            this._defaultOtherTexts[row] = otherText;
        }

        public virtual void AddOtherAnswer(long responseID, int row, int column, string answer)
        {
            DataRow row2 = this._otherData.NewRow();
            row2["ResponseID"] = responseID;
            row2["Row"] = row;
            row2["Column"] = column;
            row2["AnswerText"] = answer;
            this._otherData.Rows.Add(row2);
        }

        public virtual void AddOtherText(long responseID, int row, string otherText)
        {
            string str = responseID + "_" + row;
            this._otherTexts[str] = otherText;
        }

        public virtual void AddRowText(int row, string text)
        {
            this._rowTexts[row] = text;
        }

        public virtual void AddScaleAverage(int row, int column, string averageKey, double average)
        {
            string str = string.Concat(new object[] { row, "_", column, "_", averageKey });
            this._averageKey = averageKey;
            this._scaleAverages[str] = average;
        }

        public List<string> GetColumnOptions(int column)
        {
            if (this._columnOptions.ContainsKey(column))
            {
                return this._columnOptions[column];
            }
            return new List<string>();
        }

        public Dictionary<int, string> GetColumnTexts()
        {
            return this._columnTexts;
        }

        public string GetDefaultOtherText(int row)
        {
            if (this._defaultOtherTexts.ContainsKey(row))
            {
                return this._defaultOtherTexts[row];
            }
            return string.Empty;
        }

        protected virtual DataTable GetOtherDataTable()
        {
            DataTable table = new DataTable();
            foreach (int num in this._columnTexts.Keys)
            {
                table.Columns.Add(num.ToString(), typeof(string));
            }
            return table;
        }

        private void InitializeData()
        {
            this._rowTexts = new Dictionary<int, string>();
            this._columnOptions = new Dictionary<int, List<string>>();
            this._columnTexts = new Dictionary<int, string>();
            this._matrixAnswers = new Dictionary<string, MatrixAnswer>();
            this._otherTexts = new Dictionary<string, string>();
            this._defaultOtherTexts = new Dictionary<int, string>();
            this._scaleAverages = new Dictionary<string, double>();
            this._averageKey = string.Empty;
            DataTable table = new DataTable {
                TableName = "OtherData"
            };
            this._otherData = table;
            this._otherData.Columns.Add("ResponseID", typeof(long));
            this._otherData.Columns.Add("Row", typeof(int));
            this._otherData.Columns.Add("Column", typeof(int));
            this._otherData.Columns.Add("AnswerText", typeof(string));
        }

        public virtual int AnswerCount { get; set; }

        public string KeyColumn
        {
            get
            {
                return "RowText";
            }
        }

        public virtual DataTable OtherData
        {
            get
            {
                DataTable table = new DataTable();
                table.Columns.Add("Row", typeof(int));
                table.Columns.Add("ResponseID", typeof(long));
                table.Columns.Add("OtherText", typeof(string));
                foreach (int num in this._columnTexts.Keys)
                {
                    string name = num + "_" + this._columnTexts[num];
                    if (!table.Columns.Contains(name))
                    {
                        table.Columns.Add(name, typeof(string));
                    }
                }
                List<long> list = new List<long>();
                List<int> list2 = new List<int>();
                foreach (string str2 in this._otherTexts.Keys)
                {
                    string[] strArray = str2.Split(new char[] { '_' });
                    if (strArray.Length == 2)
                    {
                        long item = Convert.ToInt64(strArray[0]);
                        int num3 = Convert.ToInt32(strArray[1]);
                        if (!list.Contains(item))
                        {
                            list.Add(item);
                        }
                        if (!list2.Contains(num3))
                        {
                            list2.Add(num3);
                        }
                    }
                }
                foreach (long num4 in list)
                {
                    foreach (int num5 in list2)
                    {
                        DataRow[] rowArray = this._otherData.Select(string.Concat(new object[] { "ResponseID = ", num4, " AND Row = ", num5 }), null, DataViewRowState.CurrentRows);
                        DataRow row = table.NewRow();
                        string key = num4 + "_" + num5;
                        if (this._otherTexts.ContainsKey(key))
                        {
                            row["OtherText"] = this._otherTexts[key];
                        }
                        else
                        {
                            row["OtherText"] = string.Empty;
                        }
                        row["ResponseID"] = num4;
                        row["Row"] = num5;
                        foreach (DataRow row2 in rowArray)
                        {
                            if (row2["Column"] != DBNull.Value)
                            {
                                int num6 = (int) row2["Column"];
                                if (this._columnTexts.ContainsKey(num6))
                                {
                                    string str4 = num6 + "_" + this._columnTexts[num6];
                                    if (table.Columns.Contains(str4))
                                    {
                                        if ((row[str4] != DBNull.Value) && (((string) row[str4]) != string.Empty))
                                        {
                                            DataRow row3;
                                            string str5;
                                            (row3 = row)[str5 = str4] = row3[str5] + ", " + row2["AnswerText"];
                                        }
                                        else
                                        {
                                            row[str4] = row2["AnswerText"];
                                        }
                                    }
                                }
                            }
                        }
                        table.Rows.Add(row);
                    }
                }
                return table;
            }
        }

        public virtual int ResponseCount { get; set; }

        public virtual DataTable ResultsData
        {
            get
            {
                DataTable table = new DataTable {
                    TableName = "MatrixSummaryData"
                };
                table.Columns.Add("RowNumber", typeof(string));
                table.Columns.Add("RowText", typeof(string));
                foreach (int num in this._columnTexts.Keys)
                {
                    if (this._columnOptions.ContainsKey(num) && (this._columnOptions[num].Count > 0))
                    {
                        foreach (string str in this._columnOptions[num])
                        {
                            if (!table.Columns.Contains(num + "_" + str))
                            {
                                table.Columns.Add(num + "_" + str, typeof(string));
                            }
                        }
                    }
                    else if (!table.Columns.Contains(num + "_" + this._columnTexts[num]))
                    {
                        table.Columns.Add(num + "_", typeof(string));
                    }
                }
                foreach (MatrixAnswer answer in this._matrixAnswers.Values)
                {
                    DataRow[] rowArray = table.Select("RowNumber = " + answer.Row, null, DataViewRowState.CurrentRows);
                    if (rowArray.Length > 0)
                    {
                        if (table.Columns.Contains(answer.Column + "_" + answer.Answer))
                        {
                            double num2 = 0.0;
                            if (this.AnswerCount > 0)
                            {
                                num2 = 100.0 * (((double) answer.Count) / ((double) this.AnswerCount));
                            }
                            rowArray[0][answer.Column + "_" + answer.Answer] = string.Concat(new object[] { answer.Count, "  (", num2.ToString("N2"), "%)" });
                        }
                    }
                    else if (table.Columns.Contains(answer.Column + "_" + answer.Answer))
                    {
                        DataRow row = table.NewRow();
                        double num3 = 0.0;
                        if (this.AnswerCount > 0)
                        {
                            num3 = 100.0 * (((double) answer.Count) / ((double) this.AnswerCount));
                        }
                        row[answer.Column + "_" + answer.Answer] = string.Concat(new object[] { answer.Count, "  (", num3.ToString("N2"), "%)" });
                        row["RowNumber"] = answer.Row;
                        if (this._rowTexts.ContainsKey(answer.Row))
                        {
                            row["RowText"] = this._rowTexts[answer.Row];
                        }
                        table.Rows.Add(row);
                    }
                }
                foreach (DataColumn column in table.Columns)
                {
                    if (((this._averageKey != null) && (this._averageKey.Trim() != string.Empty)) && (column.ColumnName.Contains(this._averageKey) && column.ColumnName.Contains("_")))
                    {
                        string str2 = column.ColumnName.Substring(0, column.ColumnName.IndexOf('_'));
                        for (int i = 1; i <= table.Rows.Count; i++)
                        {
                            string key = string.Concat(new object[] { i, "_", str2, "_", this._averageKey });
                            if (this._scaleAverages.ContainsKey(key))
                            {
                                table.Rows[i - 1][column] = this._scaleAverages[key].ToString("N2");
                            }
                        }
                    }
                }
                table.Columns.Remove("RowNumber");
                return table;
            }
        }

        [Serializable]
        protected class MatrixAnswer
        {
            public string Answer;
            public int Column;
            public int Count;
            public int Row;
        }
    }
}

