﻿namespace Checkbox.Forms.Logic
{
    using Checkbox.Forms;
    using System;

    [Serializable]
    public class ResponseOperand : Operand
    {
        private readonly object _exclusiveValue;
        private readonly object _inclusiveValue;
        private readonly bool _isCurrentScore;
        private readonly object _value;

        public ResponseOperand(string key, Response response)
        {
            ResponseProperties properties = new ResponseProperties();
            properties.Initialize(response);
            this._value = properties.GetObjectValue(key);
            this._isCurrentScore = key.Equals("CurrentScore");
            if (this._isCurrentScore)
            {
                this._inclusiveValue = properties.GetObjectValue("CurrentScoreInclusive");
                this._exclusiveValue = properties.GetObjectValue("CurrentScoreExclusive");
            }
        }

        protected override object ProtectedGetValue(RuleEventTrigger trigger)
        {
            if (!this._isCurrentScore)
            {
                return this._value;
            }
            if (trigger != RuleEventTrigger.Load)
            {
                return this._inclusiveValue;
            }
            return this._exclusiveValue;
        }
    }
}

