﻿namespace Checkbox.Forms.Logic.Configuration
{
    using Checkbox.Common;
    using Checkbox.Forms;
    using Checkbox.Forms.Logic;
    using Checkbox.Globalization.Text;
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Runtime.CompilerServices;
    using System.Text;

    [Serializable]
    public sealed class RuleData : PersistedDomainObject
    {
        private List<ActionData> _actions;
        private readonly ResponseTemplate _context;
        private ExpressionData _expressionData;
        private bool _valid;
        private static readonly object EventValidationFailed = new object();

        internal event EventHandler ValidationFailed
        {
            add
            {
                base.Events.AddHandler(EventValidationFailed, value);
            }
            remove
            {
                base.Events.RemoveHandler(EventValidationFailed, value);
            }
        }

        public RuleData(ResponseTemplate context) : this(context, null, null)
        {
        }

        public RuleData(ResponseTemplate context, ExpressionData expression, List<ActionData> actions)
        {
            this._valid = true;
            this._context = context;
            this._expressionData = expression;
            this._expressionData.ValidationFailed += new EventHandler(this._expressionData_ValidationFailed);
            this._actions = actions;
        }

        private void _expressionData_Deleted(object sender, EventArgs e)
        {
            if (((CompositeExpressionData) this.Expression).Children.Count == 0)
            {
                foreach (ActionData data in this.Actions)
                {
                    this._context.RuleActionsTable.Select("ActionID=" + data.ID)[0].Delete();
                    data.Delete();
                }
                this.Delete();
                this.Expression.Delete();
            }
        }

        private void _expressionData_ValidationFailed(object sender, EventArgs e)
        {
            this.OnValidationFailed();
        }

        protected override void Create(IDbTransaction transaction)
        {
            DataRow row = this._context.RuleTable.NewRow();
            row["ExpressionID"] = this.Expression.ID.Value;
            this._context.RuleTable.Rows.Add(row);
            base.ID = new int?((int) row["RuleID"]);
        }

        public override void Delete(IDbTransaction t)
        {
            if (base.ID.HasValue)
            {
                this.Expression.Delete(t);
                foreach (ActionData data in this.Actions)
                {
                    data.Delete(t);
                }
                DataRow[] rowArray = this._context.RuleTable.Select(this.IdentityColumnName + "=" + base.ID.Value);
                if (rowArray.Length > 0)
                {
                    rowArray[0].Delete();
                }
            }
        }

        public override DataSet GetConfigurationDataSet()
        {
            if (this._context == null)
            {
                if (!base.ID.HasValue)
                {
                    throw new ApplicationException("No Identity specified.");
                }
                try
                {
                    string[] tableNames = new string[] { "Rule", "Expression", "Operand", "Action" };
                    Database database = DatabaseFactory.CreateDatabase();
                    DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Rules_GetData");
                    storedProcCommandWrapper.AddInParameter("RuleID", DbType.Int32, base.ID.Value);
                    DataSet dataSet = new DataSet();
                    database.LoadDataSet(storedProcCommandWrapper, dataSet, tableNames);
                    return dataSet;
                }
                catch (Exception exception)
                {
                    if (ExceptionPolicy.HandleException(exception, "BusinessProtected"))
                    {
                        throw;
                    }
                    return null;
                }
            }
            return new DataSet();
        }

        public override void Load()
        {
            if (!base.ID.HasValue)
            {
                this._expressionData = new CompositeExpressionData(this._context);
                this._actions = new List<ActionData>();
            }
        }

        public override void Load(DataSet data)
        {
            try
            {
                if (!data.Tables.Contains(this.DataTableName))
                {
                    throw new ApplicationException("Cannot find DataTable for RuleData");
                }
                this.LoadRuleExpression(data);
                this.LoadRuleActions(data);
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
            }
        }

        private void LoadRuleActions(DataSet data)
        {
            ActionDataFactory factory = new ActionDataFactory();
            DataRow[] rowArray = data.Tables["RuleActions"].Select(this.IdentityColumnName + " = " + base.ID);
            for (int i = 0; i < rowArray.Length; i++)
            {
                DataRow row = data.Tables["Action"].Select("ActionID = " + rowArray[i][1])[0];
                ActionData item = factory.CreateActionData(((string) row["ActionTypeName"]) + "," + ((string) row["ActionAssembly"]), new object[] { this._context, this.Parent });
                item.Identity = (int) row["ActionID"];
                item.Load(data);
                this._actions.Add(item);
            }
        }

        private void LoadRuleExpression(DataSet data)
        {
            DataRow[] rowArray = data.Tables[this.DataTableName].Select(this.IdentityColumnName + " = " + base.ID);
            if (rowArray.Length <= 0)
            {
                throw new ApplicationException("ExpressionData for this RuleData was not found");
            }
            DataRow row = rowArray[0];
            this._expressionData.SetID = (int) row["ExpressionID"];
            this._expressionData.Context = this._context;
            this._expressionData.Load(data);
        }

        internal void OnValidationFailed()
        {
            this._valid = false;
            EventHandler handler = (EventHandler) base.Events[EventValidationFailed];
            if (handler != null)
            {
                handler(this, null);
            }
        }

        public override void Save(IDbTransaction transaction)
        {
            this.Expression.Save(transaction);
            base.Save(transaction);
            foreach (ActionData data in this.Actions)
            {
                data.Save(transaction);
                if (this._context.RuleActionsTable.Select(string.Concat(new object[] { "RuleID=", base.ID, " AND ActionID=", data.ID })).Length == 0)
                {
                    DataRow row = this._context.RuleActionsTable.NewRow();
                    row["RuleID"] = base.ID.Value;
                    row["ActionID"] = data.ID.Value;
                    this._context.RuleActionsTable.Rows.Add(row);
                }
            }
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(TextManager.GetText("/logic/if", this._context.EditLanguage));
            builder.Append(" ");
            builder.Append(this.Expression.ToString());
            if ((this.Expression is CompositeExpressionData) && (((CompositeExpressionData) this.Expression).Children.Count > 0))
            {
                builder.Append(Environment.NewLine);
            }
            else
            {
                builder.Append(" ");
            }
            builder.Append(TextManager.GetText("/logic/then", this._context.EditLanguage));
            builder.Append(" ");
            for (int i = 0; i < this.Actions.Count; i++)
            {
                builder.Append(this.Actions[i].ToString());
                if (i < (this.Actions.Count - 1))
                {
                    builder.Append(" ");
                    builder.Append(TextManager.GetText("/logic/and", this._context.EditLanguage));
                    builder.Append(" ");
                }
            }
            return builder.ToString();
        }

        protected override void Update(IDbTransaction t)
        {
        }

        internal void Validate()
        {
            this.Expression.Validate();
        }

        public List<ActionData> Actions
        {
            get
            {
                return this._actions;
            }
            set
            {
                this._actions = value;
            }
        }

        public override string DataTableName
        {
            get
            {
                return "Rule";
            }
        }

        public ExpressionData Expression
        {
            get
            {
                return this._expressionData;
            }
            set
            {
                this._expressionData.Deleted += new EventHandler(this._expressionData_Deleted);
                this._expressionData = value;
            }
        }

        public override string IdentityColumnName
        {
            get
            {
                return "RuleID";
            }
        }

        internal object Parent { get; set; }

        internal int SetID
        {
            set
            {
                base.ID = new int?(value);
            }
        }

        public RuleEventTrigger Trigger { get; set; }

        internal bool Valid
        {
            get
            {
                return this._valid;
            }
        }
    }
}

