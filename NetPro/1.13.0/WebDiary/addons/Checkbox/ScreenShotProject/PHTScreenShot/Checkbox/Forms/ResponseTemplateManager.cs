﻿namespace Checkbox.Forms
{
    using Checkbox.Common;
    using Checkbox.Forms.Data;
    using Checkbox.Forms.Security.Principal;
    using Checkbox.Globalization.Text;
    using Checkbox.Management;
    using Checkbox.Security;
    using Prezza.Framework.Caching;
    using Prezza.Framework.Data;
    using Prezza.Framework.Data.QueryCriteria;
    using Prezza.Framework.Data.Sprocs;
    using Prezza.Framework.ExceptionHandling;
    using Prezza.Framework.Security;
    using Prezza.Framework.Security.Principal;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Security.Principal;
    using System.Xml.Serialization;

    public static class ResponseTemplateManager
    {
        private static readonly CacheManager _defaultCache;
        private static readonly CacheManager _templateCache;

        static ResponseTemplateManager()
        {
            lock (typeof(ResponseTemplateManager))
            {
                _templateCache = CacheFactory.GetCacheManager("templateCacheManager");
                _defaultCache = CacheFactory.GetCacheManager("defaultCacheManager");
            }
        }

        private static bool AuthorizeItem(ExtendedPrincipal currentPrincipal, string itemType, int itemId, IEnumerable<string> permissions)
        {
            IAccessControllable lightweightResponseTemplate;
            if (itemType.Equals("Folder", StringComparison.InvariantCultureIgnoreCase))
            {
                FormFolder folder = new FormFolder();
                folder.Load(itemId);
                lightweightResponseTemplate = folder;
            }
            else
            {
                lightweightResponseTemplate = GetLightweightResponseTemplate(itemId);
            }
            if (lightweightResponseTemplate != null)
            {
                IAuthorizationProvider authorizationProvider = AuthorizationFactory.GetAuthorizationProvider();
                foreach (string str in permissions)
                {
                    if (authorizationProvider.Authorize(currentPrincipal, lightweightResponseTemplate, str))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static void CacheResponseTemplate(ResponseTemplate rt)
        {
            if (ApplicationManager.AppSettings.CacheResponseTemplates)
            {
                _templateCache.Add(rt.ID.ToString(), rt);
            }
        }

        public static bool CanBeDeleted(int templateID, ExtendedPrincipal principal)
        {
            LightweightResponseTemplate lightweightResponseTemplate = GetLightweightResponseTemplate(templateID);
            bool flag = AuthorizationFactory.GetAuthorizationProvider().Authorize(principal, lightweightResponseTemplate, "Form.Delete");
            bool flag2 = ApplicationManager.AppSettings.AllowEditActiveSurvey && lightweightResponseTemplate.AllowEditWhileActive;
            return (((flag && lightweightResponseTemplate.IsActive) && flag2) || (flag && !lightweightResponseTemplate.IsActive));
        }

        public static bool ChangeResponseTemplateOwner(int rtId, string newOwner)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_RT_ChangeOwner");
            storedProcCommandWrapper.AddInParameter("ResponseTemplateId", DbType.Int32, rtId);
            storedProcCommandWrapper.AddInParameter("CreatedBy", DbType.String, newOwner);
            int num = database.ExecuteNonQuery(storedProcCommandWrapper);
            MarkTemplateUpdated(rtId);
            return (num > 0);
        }

        public static bool CheckTemplateUpdated(int templateID, DateTime? referenceDate)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Template_CheckUpdated");
            storedProcCommandWrapper.AddInParameter("TemplateId", DbType.Int32, templateID);
            storedProcCommandWrapper.AddInParameter("ReferenceDate", DbType.DateTime, referenceDate);
            storedProcCommandWrapper.AddOutParameter("Updated", DbType.Boolean, 4);
            database.ExecuteNonQuery(storedProcCommandWrapper);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("Updated");
            if ((parameterValue != null) && (parameterValue != DBNull.Value))
            {
                return (bool) parameterValue;
            }
            return true;
        }

        public static ResponseTemplate CopyTemplate(int templateID, ExtendedPrincipal owner, string languageCode, string progressKey)
        {
            DataSet ds = new DataSet();
            ResponseTemplate responseTemplate = GetResponseTemplate(templateID, false);
            MemoryStream stream = new MemoryStream();
            new XmlSerializer(typeof(ResponseTemplate)).Serialize((Stream) stream, responseTemplate);
            stream.Seek(0L, SeekOrigin.Begin);
            ds.ReadXml(stream, XmlReadMode.ReadSchema);
            ResponseTemplate template2 = CreateResponseTemplate(owner);
            template2.Import(ds, progressKey, languageCode);
            template2.Name = GetUniqueName(responseTemplate.Name, languageCode);
            template2.SecurityType = ApplicationManager.AppSettings.DefaultSurveySecurityType;
            int defaultStyleTemplate = ApplicationManager.AppSettings.DefaultStyleTemplate;
            if (defaultStyleTemplate > 0)
            {
                template2.StyleTemplateID = new int?(defaultStyleTemplate);
            }
            template2.IsActive = false;
            template2.Save();
            return template2;
        }

        public static ResponseTemplate CreateResponseTemplate(ExtendedPrincipal principal)
        {
            try
            {
                ResponseTemplate responseTemplate = new ResponseTemplate(principal.AclEntryIdentifier);
                Policy defaultPolicy = responseTemplate.CreatePolicy(new string[0]);
                AccessControlList acl = new AccessControlList();
                Policy policy = responseTemplate.CreatePolicy(responseTemplate.SupportedPermissions);
                acl.Add(principal, policy);
                acl.Save();
                responseTemplate.InitializeAccess(defaultPolicy, acl);
                using (IDbConnection connection = DatabaseFactory.CreateDatabase().GetConnection())
                {
                    connection.Open();
                    IDbTransaction t = connection.BeginTransaction();
                    try
                    {
                        responseTemplate.Save(t);
                        t.Commit();
                    }
                    catch
                    {
                        t.Rollback();
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
                responseTemplate.NotifyCommit(null, new EventArgs());
                responseTemplate.ReportSecurityType = ReportSecurityType.SummaryPrivate;
                responseTemplate.SecurityType = ApplicationManager.AppSettings.DefaultSurveySecurityType;
                InitializeResponseTemplateAclAndPolicy(responseTemplate, principal);
                responseTemplate.Load();
                return responseTemplate;
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
                return null;
            }
        }

        public static void DeleteResponseTemplate(int templateID)
        {
            DeleteResponseTemplate(templateID, null);
        }

        public static void DeleteResponseTemplate(int templateID, IDbTransaction t)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_RT_Delete");
            storedProcCommandWrapper.AddInParameter("ResponseTemplateID", DbType.Int32, templateID);
            storedProcCommandWrapper.AddInParameter("ModifiedDate", DbType.DateTime, DateTime.Now);
            if (t != null)
            {
                database.ExecuteNonQuery(storedProcCommandWrapper, t);
            }
            else
            {
                database.ExecuteNonQuery(storedProcCommandWrapper);
            }
        }

        private static DataSet FilterTakeableResponseTemplates(ExtendedPrincipal currentPrincipal, DataSet takeableTemplates, int pageNumber, int resultsPerPage, out int totalCount)
        {
            if ((currentPrincipal != null) && currentPrincipal.IsInRole("System Administrator"))
            {
                totalCount = takeableTemplates.Tables[0].Select().Length;
                return takeableTemplates;
            }
            int num = 0;
            DataSet set = new DataSet();
            if ((takeableTemplates != null) && (takeableTemplates.Tables.Count > 0))
            {
                int num2;
                DataTable table = takeableTemplates.Tables[0].Clone();
                DataRow[] rowArray = takeableTemplates.Tables[0].Select();
                if (pageNumber > 0)
                {
                    num2 = (pageNumber - 1) * resultsPerPage;
                }
                else
                {
                    num2 = 0;
                }
                int num3 = 0;
                for (int i = 0; i < rowArray.Length; i++)
                {
                    int templateID = (int) rowArray[i]["ItemID"];
                    int num6 = DbUtility.GetValueFromDataRow<int>(rowArray[i], "SecurityType", 1);
                    bool? nullable = DbUtility.GetValueFromDataRow<bool?>(rowArray[i], "AnonymizeResponses", false);
                    bool? defaultValue = null;
                    bool? nullable2 = DbUtility.GetValueFromDataRow<bool?>(rowArray[i], "IsActive", defaultValue);
                    if ((nullable2.HasValue && nullable2.Value) && (num6 != 5))
                    {
                        DateTime? nullable8 = null;
                        DateTime? nullable3 = DbUtility.GetValueFromDataRow<DateTime?>(rowArray[i], "ActivationStart", nullable8);
                        DateTime? nullable9 = null;
                        DateTime? nullable4 = DbUtility.GetValueFromDataRow<DateTime?>(rowArray[i], "ActivationEnd", nullable9);
                        DateTime now = DateTime.Now;
                        if ((!nullable3.HasValue || (now >= nullable3.Value)) && (!nullable4.HasValue || (now <= nullable4.Value)))
                        {
                            int? nullable10 = null;
                            int? maxTotalResponses = DbUtility.GetValueFromDataRow<int?>(rowArray[i], "MaxTotalResponses", nullable10);
                            int? nullable11 = null;
                            int? maxResponsesPerUser = DbUtility.GetValueFromDataRow<int?>(rowArray[i], "MaxResponsesPerUser", nullable11);
                            bool isAnonymized = !nullable.HasValue ? false : nullable.Value;
                            if (MoreResponsesAllowed(templateID, maxTotalResponses, maxResponsesPerUser, currentPrincipal, isAnonymized))
                            {
                                num++;
                                if (((resultsPerPage > 0) && (num > num2)) && (resultsPerPage > num3))
                                {
                                    table.ImportRow(rowArray[i]);
                                    num3++;
                                }
                            }
                        }
                    }
                }
                set.Tables.Add(table);
            }
            totalCount = num;
            return set;
        }

        private static DataSet FilterTemplatesAndFolders(DataSet templateDataSet, ExtendedPrincipal currentPrincipal, int pageSize, int currentPage, IEnumerable<string> permissions, out int totalCount)
        {
            int num;
            int num2;
            if ((pageSize > 0) && (currentPage > 0))
            {
                num = (currentPage - 1) * pageSize;
                num2 = (num + pageSize) - 1;
            }
            else
            {
                num = 0;
                num2 = templateDataSet.Tables[0].Rows.Count - 1;
            }
            DataSet set = new DataSet();
            DataTable table = templateDataSet.Tables[0].Clone();
            set.Tables.Add(table);
            DataRow[] rowArray = templateDataSet.Tables[0].Select();
            int num3 = 0;
            foreach (DataRow row in rowArray)
            {
                string str = DbUtility.GetValueFromDataRow<string>(row, "ItemType", null);
                int itemId = DbUtility.GetValueFromDataRow<int>(row, "ItemID", -1);
                if (((itemId > 0) && Utilities.IsNotNullOrEmpty(str)) && AuthorizeItem(currentPrincipal, str, itemId, permissions))
                {
                    if ((num3 >= num) && (num3 <= num2))
                    {
                        table.ImportRow(row);
                    }
                    num3++;
                }
            }
            totalCount = num3;
            return set;
        }

        public static DataSet GetAllResponseTemplates(string filterField, string filterText)
        {
            SelectQuery query = QueryFactory.GetAllResponseTemplatesQuery(filterField, filterText, null, false);
            query.AddParameter("CompleteResponseCount", string.Empty, "ckbx_TemplatesAndFoldersView");
            query.AddParameter("IncompleteResponseCount", string.Empty, "ckbx_TemplatesAndFoldersView");
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
            return database.ExecuteDataSet(sqlStringCommandWrapper);
        }

        public static DataSet GetAvailableResponseTemplates(ExtendedPrincipal currentPrincipal, int pageSize, int page, string filterField, string filterText, string sortField, bool descending, bool includeInactiveTemplates, string[] permissions, out int totalCount)
        {
            SelectQuery query = includeInactiveTemplates ? QueryFactory.GetAllResponseTemplatesQuery(filterField, filterText, sortField, descending) : QueryFactory.GetAllActiveResponseTemplatesQuery(filterField, filterText, sortField, descending);
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
            return FilterTemplatesAndFolders(database.ExecuteDataSet(sqlStringCommandWrapper), currentPrincipal, pageSize, page, permissions, out totalCount);
        }

        public static DataSet GetAvailableTemplatesAndFolders(ExtendedPrincipal currentPrincipal, int pagesize, int page, string filterField, string filterText, string sortField, bool descending, string[] permissions, out int totalCount)
        {
            SelectQuery query = QueryFactory.GetAllTemplatesAndFoldersQuery(filterField, filterText, sortField, descending);
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
            return FilterTemplatesAndFolders(database.ExecuteDataSet(sqlStringCommandWrapper), currentPrincipal, pagesize, page, permissions, out totalCount);
        }

        public static DataSet GetAvailableTemplatesAndFolders(ExtendedPrincipal currentPrincipal, int? parentFolderId, int pagesize, int page, string filterField, string filterText, string sortField, bool descending, string[] permissions, out int totalCount)
        {
            SelectQuery query = QueryFactory.GetAllTemplatesAndFoldersQuery(filterField, filterText, sortField, descending, parentFolderId);
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
            return FilterTemplatesAndFolders(database.ExecuteDataSet(sqlStringCommandWrapper), currentPrincipal, pagesize, page, permissions, out totalCount);
        }

        public static LightweightResponseTemplate GetLightweightResponseTemplate(Guid templateGuid)
        {
            int? responseTemplateIdFromGuid = GetResponseTemplateIdFromGuid(templateGuid);
            if (!responseTemplateIdFromGuid.HasValue)
            {
                return null;
            }
            LightweightResponseTemplate theObject = new LightweightResponseTemplate(responseTemplateIdFromGuid.Value);
            StoredProcedureCommandExtractor.ExecuteProcedure(ProcedureType.Select, theObject);
            return theObject;
        }

        public static LightweightResponseTemplate GetLightweightResponseTemplate(int templateID)
        {
            LightweightResponseTemplate theObject = new LightweightResponseTemplate(templateID);
            StoredProcedureCommandExtractor.ExecuteProcedure(ProcedureType.Select, theObject);
            return theObject;
        }

        public static SecurityEditor GetLightweightResponseTemplateSecurityEditor(int responseTemplateId)
        {
            LightweightResponseTemplate lightweightResponseTemplate = GetLightweightResponseTemplate(responseTemplateId);
            if (lightweightResponseTemplate != null)
            {
                return lightweightResponseTemplate.GetEditor();
            }
            return null;
        }

        public static ResponseTemplate GetResponseTemplate(Guid guid)
        {
            //return GetResponseTemplate(guid, true);
            // Turn off cache for template data 
            return GetResponseTemplate(guid, false);
        }

        public static ResponseTemplate GetResponseTemplate(int templateID)
        {
            //return GetResponseTemplate(templateID, true);
            return GetResponseTemplate(templateID, false);
        }

        public static ResponseTemplate GetResponseTemplate(string rtName)
        {
            return GetResponseTemplate(rtName, true);
        }

        public static ResponseTemplate GetResponseTemplate(Guid guid, bool useCache)
        {
            useCache = false;

            int? responseTemplateIdFromGuid = GetResponseTemplateIdFromGuid(guid);
            if (!responseTemplateIdFromGuid.HasValue)
            {
                throw new TemplateDoesNotExist(guid.ToString());
            }
            return GetResponseTemplate(responseTemplateIdFromGuid.Value, useCache);
        }

        public static ResponseTemplate GetResponseTemplate(int templateID, bool useCache)
        {
            useCache = false;

            ResponseTemplate rt = null;
            if (useCache)
            {
                rt = (ResponseTemplate) _templateCache[templateID.ToString()];
                if ((rt != null) && CheckTemplateUpdated(templateID, rt.LastModified))
                {
                    rt = null;
                }
            }
            if (rt == null)
            {
                rt = new ResponseTemplate();
                rt.Load(templateID);
                //CacheResponseTemplate(rt);
                MergeActiveSurveyLanguages(rt.SupportedLanguages);
            }
            return rt;
        }

        public static ResponseTemplate GetResponseTemplate(string rtName, bool useCache)
        {
            useCache = false;

            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_RT_GetResponseTemplateIDFromName");
            storedProcCommandWrapper.AddInParameter("ResponseTemplateName", DbType.String, rtName);
            storedProcCommandWrapper.AddOutParameter("ResponseTemplateID", DbType.Int32, 4);
            database.ExecuteNonQuery(storedProcCommandWrapper);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("ResponseTemplateID");
            if ((parameterValue == null) || (parameterValue == DBNull.Value))
            {
                throw new TemplateDoesNotExist(rtName);
            }
            return GetResponseTemplate((int) parameterValue, useCache);
        }

        public static DataSet GetResponseTemplateData(int templateID)
        {
            ArrayList list = new ArrayList();
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_RT_GetTableNames");
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    while (reader.Read())
                    {
                        list.Add(reader["TableName"]);
                    }
                }
                catch
                {
                }
                finally
                {
                    reader.Close();
                }
            }
            DataSet dataSet = new DataSet();
            DBCommandWrapper command = database.GetStoredProcCommandWrapper("ckbx_RT_GetResponseTemplate");
            command.AddInParameter("ResponseTemplateID", DbType.Int32, templateID);
            database.LoadDataSet(command, dataSet, (string[]) list.ToArray(typeof(string)));
            if (((dataSet.Tables.Count == 0) || !dataSet.Tables.Contains(ResponseTemplate.DataTableName)) || (dataSet.Tables[ResponseTemplate.DataTableName].Rows.Count <= 0))
            {
                throw new TemplateDoesNotExist(templateID);
            }
            return dataSet;
        }

        public static ResponseTemplate GetResponseTemplateFromResponseGUID(Guid responseGUID)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_RT_GetIDFromResponseGUID");
            storedProcCommandWrapper.AddInParameter("ResponseGUID", DbType.Guid, responseGUID);
            object obj2 = database.ExecuteScalar(storedProcCommandWrapper);
            if ((obj2 != null) && (obj2 != DBNull.Value))
            {
                return GetResponseTemplate((int) obj2);
            }
            return null;
        }

        public static Guid? GetResponseTemplateGUID(int templateID)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_RT_GetGUID");
            storedProcCommandWrapper.AddInParameter("ResponseTemplateID", DbType.Int32, templateID);
            Guid? nullable = null;
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    try
                    {
                        while (reader.Read())
                        {
                            nullable = new Guid?((Guid) reader["GUID"]);
                        }
                    }
                    catch
                    {
                    }
                    return nullable;
                }
                finally
                {
                    reader.Close();
                }
            }
            return nullable;
        }

        public static int? GetResponseTemplateId(string responseTemplateName)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ResponseTemplate_GetIdFromName");
            storedProcCommandWrapper.AddInParameter("TemplateName", DbType.String, responseTemplateName);
            storedProcCommandWrapper.AddOutParameter("TemplateId", DbType.Int32, 4);
            database.ExecuteNonQuery(storedProcCommandWrapper);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("TemplateId");
            if ((parameterValue != null) && (parameterValue != DBNull.Value))
            {
                return new int?((int) parameterValue);
            }
            return null;
        }

        public static int? GetResponseTemplateIdFromGuid(Guid responseTemplateGuid)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_RT_GetIDByGUID");
            storedProcCommandWrapper.AddInParameter("GUID", DbType.String, responseTemplateGuid.ToString());
            object obj2 = database.ExecuteScalar(storedProcCommandWrapper);
            if ((obj2 != null) && (obj2 != DBNull.Value))
            {
                return new int?((int) obj2);
            }
            return null;
        }

        public static int? GetResponseTemplateParentFolderId(int responseTemplateId)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_RT_GetParent");
            storedProcCommandWrapper.AddInParameter("ResponseTemplateId", DbType.Int32, responseTemplateId);
            storedProcCommandWrapper.AddOutParameter("FolderId", DbType.Int32, 4);
            database.ExecuteNonQuery(storedProcCommandWrapper);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("FolderId");
            if ((parameterValue != null) && (parameterValue != DBNull.Value))
            {
                return new int?((int) parameterValue);
            }
            return null;
        }

        public static string GetResponseTemplateParentFolderName(int responseTemplateId)
        {
            string str = string.Empty;
            int? responseTemplateParentFolderId = GetResponseTemplateParentFolderId(responseTemplateId);
            if (!responseTemplateParentFolderId.HasValue)
            {
                return str;
            }
            FormFolder folder = new FormFolder();
            folder.Load(responseTemplateParentFolderId.Value);
            return (folder.Name ?? string.Empty);
        }

        public static DataSet GetTakeableResponseTemplates(ExtendedPrincipal currentPrincipal, int pageNumber, int pageSize, string searchField, string searchText, string sortField, bool sortDescending, CriteriaCollection additionalFilterCriteria, out int totalCount)
        {
            DataSet takeableTemplates = GetAvailableResponseTemplates(currentPrincipal, -1, -1, searchField, searchText, sortField, sortDescending, true, new string[] { "Form.Fill" }, out totalCount);
            return FilterTakeableResponseTemplates(currentPrincipal, takeableTemplates, pageNumber, pageSize, out totalCount);
        }

        private static int GetTemplateResponseCount(int templateID)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Response_Count");
            storedProcCommandWrapper.AddInParameter("ResponseTemplateID", DbType.Int32, templateID);
            storedProcCommandWrapper.AddOutParameter("TotalCount", DbType.Int32, 4);
            database.ExecuteNonQuery(storedProcCommandWrapper);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("TotalCount");
            if (parameterValue != DBNull.Value)
            {
                return (int) parameterValue;
            }
            return 0;
        }

        public static int GetTestResponseCount(int templateID)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_RT_TestResponseCount");
            storedProcCommandWrapper.AddInParameter("ResponseTemplateID", DbType.Int32, templateID);
            storedProcCommandWrapper.AddOutParameter("Count", DbType.Int32, 4);
            database.ExecuteNonQuery(storedProcCommandWrapper);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("Count");
            if (parameterValue != DBNull.Value)
            {
                return (int) parameterValue;
            }
            return 0;
        }

        public static string GetUniqueName(string surveyName, string languageCode)
        {
            int num = 0;
            string responseTemplateName = surveyName;
            while (ResponseTemplateExists(responseTemplateName))
            {
                num++;
                responseTemplateName = string.Format("{0} {1} {2}", surveyName, TextManager.GetText("/common/duplicate", languageCode), num);
            }
            return responseTemplateName;
        }

        public static bool HasResponses(int responseTemplateId)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_RT_HasResponses");
            storedProcCommandWrapper.AddInParameter("ResponseTemplateID", DbType.String, responseTemplateId);
            storedProcCommandWrapper.AddOutParameter("Count", DbType.Int32, 4);
            database.ExecuteNonQuery(storedProcCommandWrapper);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("Count");
            return ((parameterValue != DBNull.Value) && (((int) parameterValue) > 0));
        }

        public static ResponseTemplate ImportResponseTemplate(DataSet templateData, ExtendedPrincipal creator, FormFolder folder, string progressKey, string progressLanguage)
        {
            ResponseTemplate item = CreateResponseTemplate(creator);
            item.Import(templateData, progressKey, progressLanguage);
            string uniqueName = GetUniqueName(item.Name, item.DefaultLanguage);
            item.Name = uniqueName;
            item.SecurityType = ApplicationManager.AppSettings.DefaultSurveySecurityType;
            int defaultStyleTemplate = ApplicationManager.AppSettings.DefaultStyleTemplate;
            if (defaultStyleTemplate > 0)
            {
                item.StyleTemplateID = new int?(defaultStyleTemplate);
            }
            item.Save();
            if (folder != null)
            {
                folder.Add(item);
            }
            return item;
        }

        public static void InitializeResponseTemplateAclAndPolicy(ResponseTemplate responseTemplate, ExtendedPrincipal principal)
        {
            ExtendedPrincipal principal2 = principal;
            if (!AuthorizationFactory.GetAuthorizationProvider().Authorize(principal, responseTemplate, "Form.Administer"))
            {
                principal2 = new ExtendedPrincipal(new GenericIdentity(principal.Identity.Name, principal.Identity.AuthenticationType), new string[] { "Form Administrator", "Survey Administrator" });
            }
            switch (responseTemplate.SecurityType)
            {
                case SecurityType.Public:
                case SecurityType.PasswordProtected:
                case SecurityType.InvitationOnly:
                    AccessManager.AddPermissionsToDefaultPolicy(responseTemplate, principal2, new string[] { "Form.Fill" });
                    AccessManager.AddPermissionsToAllAclEntries(responseTemplate, principal2, new string[] { "Form.Fill" });
                    break;

                case SecurityType.AccessControlList:
                    AccessManager.RemovePermissionsFromDefaultPolicy(responseTemplate, principal2, new string[] { "Form.Fill" });
                    break;

                case SecurityType.AllRegisteredUsers:
                    AccessManager.AddEveryoneGroupWithPermissions(responseTemplate, principal2, new string[] { "Form.Fill" });
                    AccessManager.AddPermissionsToAllAclEntries(responseTemplate, principal2, new string[] { "Form.Fill" });
                    AccessManager.RemovePermissionsFromDefaultPolicy(responseTemplate, principal2, new string[] { "Form.Fill" });
                    break;
            }
            switch (responseTemplate.ReportSecurityType)
            {
                case ReportSecurityType.SummaryPrivate:
                    AccessManager.RemovePermissionsFromDefaultPolicy(responseTemplate, principal2, new string[] { "Analysis.Create", "Analysis.Delete", "Analysis.Edit", "Analysis.ManageFilters", "Analysis.Responses.View", "Analysis.Responses.Export" });
                    AccessManager.RemovePermissionsFromAllAclEntries(responseTemplate, principal2, responseTemplate.CreatedBy, new string[] { "Analysis.Create", "Analysis.Delete", "Analysis.Edit", "Analsyis.ManageFilters", "Analysis.Responses.View", "Analysis.Responses.Export" });
                    return;

                case ReportSecurityType.DetailsPrivate:
                    AccessManager.RemovePermissionsFromDefaultPolicy(responseTemplate, principal2, new string[] { "Analysis.Create", "Analysis.Delete", "Analysis.Edit", "Analysis.ManageFilters" });
                    AccessManager.RemovePermissionsFromAllAclEntries(responseTemplate, principal2, responseTemplate.CreatedBy, new string[] { "Analysis.Create", "Analysis.Delete", "Analysis.Edit", "Analsyis.ManageFilters" });
                    AccessManager.AddPermissionsToAllAclEntries(responseTemplate, principal2, new string[] { "Analysis.Run", "Analysis.Responses.View", "Analysis.Responses.Export" });
                    AccessManager.AddPermissionsToDefaultPolicy(responseTemplate, principal2, new string[] { "Analysis.Run", "Analysis.Responses.View", "Analysis.Responses.Export" });
                    return;

                case ReportSecurityType.SummaryPublic:
                    AccessManager.AddPermissionsToDefaultPolicy(responseTemplate, principal2, new string[] { "Analysis.Run", "Analysis.Create", "Analysis.Delete", "Analysis.Edit", "Analysis.ManageFilters" });
                    AccessManager.AddPermissionsToAllAclEntries(responseTemplate, principal2, new string[] { "Analysis.Run", "Analysis.Create", "Analysis.Delete", "Analysis.Edit", "Analysis.ManageFilters" });
                    AccessManager.RemovePermissionsFromDefaultPolicy(responseTemplate, principal2, new string[] { "Analysis.Responses.View", "Analysis.Responses.Export" });
                    AccessManager.RemovePermissionsFromAllAclEntries(responseTemplate, principal2, responseTemplate.CreatedBy, new string[] { "Analysis.Responses.View", "Analysis.Responses.Export" });
                    return;

                case ReportSecurityType.DetailsPublic:
                    AccessManager.AddPermissionsToDefaultPolicy(responseTemplate, principal2, new string[] { "Analysis.Run", "Analysis.Create", "Analysis.Delete", "Analysis.Edit", "Analysis.ManageFilters", "Analysis.Responses.View", "Analysis.Responses.Export" });
                    AccessManager.AddPermissionsToAllAclEntries(responseTemplate, principal2, new string[] { "Analysis.Run", "Analysis.Create", "Analysis.Delete", "Analysis.Edit", "Analysis.ManageFilters", "Analysis.Responses.View", "Analysis.Responses.Export" });
                    return;

                case ReportSecurityType.SummaryAcl:
                    AccessManager.RemovePermissionsFromDefaultPolicy(responseTemplate, principal2, new string[] { "Analysis.Create", "Analysis.Delete", "Analysis.Edit", "Analysis.ManageFilters", "Analysis.Responses.View", "Analysis.Responses.Export" });
                    AccessManager.RemovePermissionsFromAllAclEntries(responseTemplate, principal2, responseTemplate.CreatedBy, new string[] { "Analysis.Responses.View", "Analysis.Responses.Export" });
                    return;

                case ReportSecurityType.DetailsAcl:
                    AccessManager.RemovePermissionsFromDefaultPolicy(responseTemplate, principal2, new string[] { "Analysis.Create", "Analysis.Delete", "Analysis.Edit", "Analysis.ManageFilters", "Analysis.Responses.View", "Analysis.Responses.Export" });
                    AccessManager.AddPermissionsToAllAclEntries(responseTemplate, principal2, new string[] { "Analysis.Responses.View", "Analysis.Responses.Export" });
                    return;

                case ReportSecurityType.SummaryRegisteredUsers:
                    AccessManager.AddEveryoneGroupWithPermissions(responseTemplate, principal2, new string[] { "Analysis.Run", "Analysis.Create", "Analysis.Delete", "Analysis.Edit", "Analysis.ManageFilters" });
                    AccessManager.AddPermissionsToAllAclEntries(responseTemplate, principal2, new string[] { "Analysis.Run", "Analysis.Create", "Analysis.Delete", "Analysis.Edit", "Analysis.ManageFilters" });
                    AccessManager.RemovePermissionsFromDefaultPolicy(responseTemplate, principal2, new string[] { "Analysis.Responses.View", "Analysis.Responses.Export" });
                    AccessManager.RemovePermissionsFromAllAclEntries(responseTemplate, principal2, responseTemplate.CreatedBy, new string[] { "Analysis.Responses.View", "Analysis.Responses.Export" });
                    return;

                case ReportSecurityType.DetailsRegisteredUsers:
                    AccessManager.AddEveryoneGroupWithPermissions(responseTemplate, principal2, new string[] { "Analysis.Run", "Analysis.Create", "Analysis.Delete", "Analysis.Edit", "Analysis.ManageFilters", "Analysis.Responses.View", "Analysis.Responses.Export" });
                    AccessManager.AddPermissionsToAllAclEntries(responseTemplate, principal2, new string[] { "Analysis.Run", "Analysis.Create", "Analysis.Delete", "Analysis.Edit", "Analysis.ManageFilters", "Analysis.Responses.View", "Analysis.Responses.Export" });
                    AccessManager.RemovePermissionsFromDefaultPolicy(responseTemplate, principal2, new string[] { "Analysis.Create", "Analysis.Delete", "Analysis.Edit", "Analysis.ManageFilters", "Analysis.Responses.View", "Analysis.Responses.Export" });
                    return;
            }
        }

        public static void MarkTemplateUpdated(int templateID)
        {
            _templateCache.Remove(templateID.ToString());
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Template_SetModifiedDate");
            storedProcCommandWrapper.AddInParameter("TemplateID", DbType.Int32, templateID);
            storedProcCommandWrapper.AddInParameter("ModifiedDate", DbType.DateTime, DateTime.Now);
            database.ExecuteNonQuery(storedProcCommandWrapper);
        }

        private static void MergeActiveSurveyLanguages(string[] languages)
        {
            List<string> list = new List<string>();
            string[] activeSurveyLanguages = ActiveSurveyLanguages;
            foreach (string str in languages)
            {
                if (!activeSurveyLanguages.Contains<string>(str, StringComparer.InvariantCultureIgnoreCase) && !list.Contains(str))
                {
                    list.Add(str);
                }
            }
            if (list.Count != 0)
            {
                list.AddRange(activeSurveyLanguages);
                lock (typeof(ResponseTemplateManager))
                {
                    ActiveSurveyLanguages = list.ToArray();
                }
            }
        }

        public static bool MoreResponsesAllowed(int templateID, int? maxTotalResponses, int? maxResponsesPerUser, ExtendedPrincipal principal, bool isAnonymized)
        {
            return (MoreResponsesAllowedForTemplate(templateID, maxTotalResponses) && MoreResponsesAllowedForUser(templateID, maxResponsesPerUser, principal, isAnonymized));
        }

        private static bool MoreResponsesAllowedForTemplate(int templateID, int? maxResponseCount)
        {
            if (maxResponseCount.HasValue && (GetTemplateResponseCount(templateID) >= maxResponseCount.Value))
            {
                return false;
            }
            return true;
        }

        private static bool MoreResponsesAllowedForUser(int templateID, int? maxResponsesPerUser, ExtendedPrincipal currentPrincipal, bool isAnonymized)
        {
            if (!maxResponsesPerUser.HasValue)
            {
                return true;
            }
            int num = 0;
            if (currentPrincipal is AnonymousRespondent)
            {
                Database database = DatabaseFactory.CreateDatabase();
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Response_CountForAnonRespondent");
                storedProcCommandWrapper.AddInParameter("RespondentGUID", DbType.Guid, ((AnonymousRespondent) currentPrincipal).Guid);
                storedProcCommandWrapper.AddInParameter("ResponseTemplateID", DbType.Int32, templateID);
                storedProcCommandWrapper.AddOutParameter("UserCount", DbType.Int32, 4);
                database.ExecuteNonQuery(storedProcCommandWrapper);
                object parameterValue = storedProcCommandWrapper.GetParameterValue("UserCount");
                if (parameterValue != DBNull.Value)
                {
                    num = (int) parameterValue;
                }
            }
            else if (isAnonymized)
            {
                Database database2 = DatabaseFactory.CreateDatabase();
                DBCommandWrapper command = database2.GetStoredProcCommandWrapper("ckbx_Response_CountForAnonymizedUser");
                command.AddInParameter("ResumeKey", DbType.String, Utilities.GetSaltedMD5Hash(currentPrincipal.Identity.Name));
                command.AddInParameter("ResponseTemplateID", DbType.Int32, templateID);
                command.AddOutParameter("UserCount", DbType.Int32, 4);
                database2.ExecuteNonQuery(command);
                object obj3 = command.GetParameterValue("UserCount");
                if (obj3 != DBNull.Value)
                {
                    num = (int) obj3;
                }
            }
            else
            {
                Database database3 = DatabaseFactory.CreateDatabase();
                DBCommandWrapper wrapper3 = database3.GetStoredProcCommandWrapper("ckbx_Response_CountForUser");
                wrapper3.AddInParameter("UniqueIdentifier", DbType.String, currentPrincipal.Identity.Name);
                wrapper3.AddInParameter("ResponseTemplateID", DbType.Int32, templateID);
                wrapper3.AddOutParameter("UserCount", DbType.Int32, 4);
                database3.ExecuteNonQuery(wrapper3);
                object obj4 = wrapper3.GetParameterValue("UserCount");
                if (obj4 != DBNull.Value)
                {
                    num = (int) obj4;
                }
            }
            return (num < maxResponsesPerUser.Value);
        }

        public static bool ResponseTemplateExists(string responseTemplateName)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_RT_GetResponseTemplateIDFromName");
            storedProcCommandWrapper.AddInParameter("ResponseTemplateName", DbType.String, responseTemplateName);
            storedProcCommandWrapper.AddOutParameter("ResponseTemplateID", DbType.Int32, 4);
            database.ExecuteNonQuery(storedProcCommandWrapper);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("ResponseTemplateID");
            return ((parameterValue != null) && (parameterValue != DBNull.Value));
        }

        public static string[] ActiveSurveyLanguages
        {
            get
            {
                if (!_defaultCache.Contains("ActiveSurveyLanguages"))
                {
                    lock (typeof(ResponseTemplateManager))
                    {
                        if (!_defaultCache.Contains("ActiveSurveyLanguages"))
                        {
                            _defaultCache.Add("ActiveSurveyLanguages", new string[0]);
                        }
                    }
                }
                return (string[]) _defaultCache["ActiveSurveyLanguages"];
            }
            set
            {
                _defaultCache.Add("ActiveSurveyLanguages", value);
            }
        }
    }
}

