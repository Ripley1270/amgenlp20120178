﻿namespace Checkbox.Analytics
{
    using Checkbox.Analytics.Filters;
    using Checkbox.Analytics.Filters.Configuration;
    using Checkbox.Analytics.Items;
    using Checkbox.Analytics.Items.Configuration;
    using Checkbox.Analytics.Security;
    using Checkbox.Forms;
    using Checkbox.Forms.Items;
    using Checkbox.Forms.Items.Configuration;
    using Checkbox.Globalization.Text;
    using Prezza.Framework.Common;
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using Prezza.Framework.Security;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class AnalysisTemplate : Template
    {
        private AnalysisFilterCollection _filters;

        public AnalysisTemplate() : base(new string[] { "Analysis.Administer", "Analysis.Edit", "Analysis.Run" }, new string[] { "Analysis.Administer", "Analysis.Edit", "Analysis.Delete", "Analysis.Run" })
        {
            this._filters = new AnalysisFilterCollection();
            this.GUID = Guid.NewGuid();
        }

        public void AddFilter(FilterData filter)
        {
            if (this._filters == null)
            {
                AnalysisFilterCollection filters = new AnalysisFilterCollection {
                    ParentID = base.ID.Value
                };
                this._filters = filters;
                this._filters.Load(base.ID.Value);
            }
            this._filters.AddFilter(filter);
        }

        public void AddFilter(int filterID)
        {
            if (this._filters == null)
            {
                AnalysisFilterCollection filters = new AnalysisFilterCollection {
                    ParentID = base.ID.Value
                };
                this._filters = filters;
                this._filters.Load(base.ID.Value);
            }
            this._filters.AddFilter(filterID);
        }

        protected override void Create(IDbTransaction t)
        {
            base.Create(t);
            if (base.ID <= 0)
            {
                throw new Exception("Unable to save analysis template data.  DataID <= 0.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_AnalysisTemplate_Insert");
            storedProcCommandWrapper.AddInParameter("AnalysisTemplateID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("StyleTemplateID", DbType.Int32, this.StyleTemplateID);
            storedProcCommandWrapper.AddInParameter("ResponseTemplateID", DbType.Int32, this.ResponseTemplateID);
            storedProcCommandWrapper.AddInParameter("AnalysisName", DbType.String, this.Name);
            storedProcCommandWrapper.AddInParameter("NameTextID", DbType.String, this.NameTextID);
            storedProcCommandWrapper.AddInParameter("GUID", DbType.Guid, this.GUID);
            storedProcCommandWrapper.AddInParameter("DateFilterStart", DbType.DateTime, this.FilterStartDate);
            storedProcCommandWrapper.AddInParameter("DateFilterEnd", DbType.DateTime, this.FilterEndDate);
            storedProcCommandWrapper.AddInParameter("ChartStyleID", DbType.Int32, this.ChartStyleID);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        public Analysis CreateAnalysis(string languageCode, bool includeIncompleteResponses, string progressKey)
        {
            Analysis analysis = new Analysis {
                Name = TextManager.GetText(this.NameTextID, languageCode)
            };
            if (base.ID.HasValue)
            {
                analysis.SetId(base.ID.Value);
            }
            foreach (ItemData data in base.Items)
            {
                Item item = data.CreateItem(languageCode);
                if (item is AnalysisItem)
                {
                    ((AnalysisItem) item).RunMode = true;
                }
                analysis.AddItem(item);
            }
            foreach (TemplatePage page in this.GetPages(null))
            {
                AnalysisPage page2 = new AnalysisPage(page.Identity.Value, page.Position, page.LayoutTemplateID) {
                    Parent = analysis
                };
                foreach (int num in this.GetPageItemDataIDs(page.Identity.Value))
                {
                    page2.AddItemID(num);
                }
                analysis.AddPage(page2);
            }
            analysis.AddResponseTemplateID(this.ResponseTemplateID);
            DateTime? startFilter = null;
            DateTime? endFilter = null;
            if (this.FilterStartDate.HasValue)
            {
                startFilter = new DateTime(this.FilterStartDate.Value.Year, this.FilterStartDate.Value.Month, this.FilterStartDate.Value.Day);
            }
            if (this.FilterEndDate.HasValue)
            {
                endFilter = new DateTime(this.FilterEndDate.Value.Year, this.FilterEndDate.Value.Month, this.FilterEndDate.Value.Day, 0x17, 0x3b, 0x3b, 0x3e7);
            }
            analysis.Initialize(this._filters, languageCode, startFilter, endFilter, includeIncompleteResponses, progressKey, base.LastModified);
            return analysis;
        }

        public override Policy CreatePolicy(string[] permissions)
        {
            return new AnalysisPolicy(permissions);
        }

        public override void Delete(IDbTransaction t)
        {
            if (base.ID <= 0)
            {
                throw new Exception("Unable to delete analysis template data.  DataID <= 0.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_AnalysisTemplate_Delete");
            storedProcCommandWrapper.AddInParameter("AnalysisTemplateID", DbType.Int32, base.ID);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        public void DeleteFilter(FilterData filter)
        {
            if (this._filters == null)
            {
                AnalysisFilterCollection filters = new AnalysisFilterCollection {
                    ParentID = base.ID.Value
                };
                this._filters = filters;
                this._filters.Load(base.ID.Value);
            }
            this._filters.DeleteFilter(filter);
        }

        public void DeleteFilter(int filterID)
        {
            if (this._filters == null)
            {
                AnalysisFilterCollection filters = new AnalysisFilterCollection {
                    ParentID = base.ID.Value
                };
                this._filters = filters;
                this._filters.Load(base.ID.Value);
            }
            this._filters.DeleteFilter(filterID);
        }

        public override DataSet GetConfigurationDataSet()
        {
            if (base.ID < 0)
            {
                throw new Exception("Unable to load analysis template.  DataID < 0.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_AnalysisTemplate_Get");
            storedProcCommandWrapper.AddInParameter("AnalysisTemplateID", DbType.Int32, base.ID);
            DataSet dataSet = new DataSet();
            string[] tableNames = new string[] { this.TemplateDataTableName, this.TemplateItemsTableName, this.PageDataTableName, this.PageItemsTableName, this.ItemAppearanceMapTableName, this.ItemAppearanceTableName };
            database.LoadDataSet(storedProcCommandWrapper, dataSet, tableNames);
            base._templateData = dataSet;
            return dataSet;
        }

        public override SecurityEditor GetEditor()
        {
            return new AnalysisSecurityEditor(this);
        }

        public List<FilterData> GetFilterDataObjects()
        {
            if (this._filters == null)
            {
                AnalysisFilterCollection filters = new AnalysisFilterCollection {
                    ParentID = base.ID.Value
                };
                this._filters = filters;
                this._filters.Load(base.ID.Value);
            }
            return this._filters.GetFilterDataObjects();
        }

        public void Import(DataSet ds, int oldTemplateID)
        {
            base.Import(ds);
            this._filters.ParentID = base.ID.Value;
            this.ImportFilters(ds, oldTemplateID);
        }

        protected virtual void ImportFilters(DataSet ds, int oldTemplateID)
        {
            this._filters.Import(ds, oldTemplateID);
        }

        protected override ItemData ImportItem(int itemID, DataSet ds)
        {
            ItemData data = base.ImportItem(itemID, ds);
            if (data is AnalysisItemData)
            {
                ((AnalysisItemData) data).ImportFilters(ds, Math.Abs(itemID));
            }
            return data;
        }

        protected override void ImportTemplateData(DataSet ds)
        {
            base.ImportTemplateData(ds);
            this.LoadTemplateData(ds, true);
        }

        internal void InitializeAccess(Policy defaultPolicy, AccessControlList acl)
        {
            if (base.ID.HasValue && (base.ID > 0))
            {
                throw new Exception("Access can only be initialized for a new analysis template.");
            }
            ArgumentValidation.CheckExpectedType(defaultPolicy, typeof(AnalysisPolicy));
            base.SetAccess(defaultPolicy, acl);
        }

        public override void Load(DataSet data)
        {
            base.Load(data);
            this._filters.ParentID = base.ID.Value;
            DataSet dataSet = this._filters.Load(base.ID.Value);
            data.Merge(dataSet);
            this.LoadTemplateData(data, false);
        }

        protected void LoadTemplateData(DataSet data, bool isImport)
        {
            base.LoadTemplateData(data);
            if (((data.Tables.Count > 0) && data.Tables.Contains(this.TemplateDataTableName)) && (data.Tables[this.TemplateDataTableName].Rows.Count > 0))
            {
                DataRow tableRow = data.Tables[this.TemplateDataTableName].Rows[0];
                this.ResponseTemplateID = DbUtility.GetValueFromDataRow<int>(tableRow, "ResponseTemplateID", -1);
                if (!isImport)
                {
                    base.AclID = DbUtility.GetValueFromDataRow<int?>(tableRow, "AclID", null);
                    base.DefaultPolicyID = DbUtility.GetValueFromDataRow<int?>(tableRow, "DefaultPolicy", null);
                    base.LastModified = DbUtility.GetValueFromDataRow<DateTime?>(tableRow, "ModifiedDate", null);
                    this.GUID = DbUtility.GetValueFromDataRow<Guid>(tableRow, "GUID", new Guid());
                    this.ChartStyleID = DbUtility.GetValueFromDataRow<int?>(tableRow, "ChartStyleID", null);
                }
                this.StyleTemplateID = DbUtility.GetValueFromDataRow<int?>(tableRow, "StyleTemplateID", null);
                this.Name = DbUtility.GetValueFromDataRow<string>(tableRow, "AnalysisName", string.Empty);
                this.FilterStartDate = DbUtility.GetValueFromDataRow<DateTime?>(tableRow, "DateFilterStart", null);
                this.FilterEndDate = DbUtility.GetValueFromDataRow<DateTime?>(tableRow, "DateFilterEnd", null);
                this.IsDeleted = DbUtility.GetValueFromDataRow<bool>(tableRow, "Deleted", false);
            }
        }

        public void SaveFilters()
        {
            if (this._filters != null)
            {
                using (IDbConnection connection = DatabaseFactory.CreateDatabase().GetConnection())
                {
                    try
                    {
                        connection.Open();
                        IDbTransaction t = connection.BeginTransaction();
                        try
                        {
                            this._filters.Save(t);
                            t.Commit();
                            this._filters = null;
                        }
                        catch (Exception)
                        {
                            t.Rollback();
                            throw;
                        }
                    }
                    catch (Exception exception)
                    {
                        if (ExceptionPolicy.HandleException(exception, "BusinessPrivate"))
                        {
                            throw;
                        }
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }

        protected override void Update(IDbTransaction t)
        {
            base.Update(t);
            if (base.ID <= 0)
            {
                throw new Exception("Unable to save analysis template data.  DataID <= 0.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_AnalysisTemplate_Update");
            storedProcCommandWrapper.AddInParameter("AnalysisTemplateID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("StyleTemplateID", DbType.Int32, this.StyleTemplateID);
            storedProcCommandWrapper.AddInParameter("ResponseTemplateID", DbType.Int32, this.ResponseTemplateID);
            storedProcCommandWrapper.AddInParameter("AnalysisName", DbType.String, this.Name);
            storedProcCommandWrapper.AddInParameter("NameTextID", DbType.String, this.NameTextID);
            storedProcCommandWrapper.AddInParameter("DateFilterStart", DbType.DateTime, this.FilterStartDate);
            storedProcCommandWrapper.AddInParameter("DateFilterEnd", DbType.DateTime, this.FilterEndDate);
            storedProcCommandWrapper.AddInParameter("ChartStyleID", DbType.Int32, this.ChartStyleID);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        public int? ChartStyleID { get; set; }

        public DateTime? FilterEndDate { get; set; }

        public DateTime? FilterStartDate { get; set; }

        public Guid GUID { get; private set; }

        public override string IdentityColumnName
        {
            get
            {
                return "TemplateID";
            }
        }

        public bool IsDeleted { get; private set; }

        public string Name { get; set; }

        public string NameTextID
        {
            get
            {
                if (base.ID.HasValue && (base.ID.Value > 0))
                {
                    return ("/analysisTemplate/" + base.ID + "/name");
                }
                return string.Empty;
            }
        }

        public int ResponseTemplateID { get; set; }

        public int? StyleTemplateID { get; set; }
    }
}

