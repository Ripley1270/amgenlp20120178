﻿namespace Checkbox.Security.Data
{
    using Checkbox.Security;
    using Prezza.Framework.Data;
    using Prezza.Framework.Data.QueryCriteria;
    using Prezza.Framework.Data.QueryParameters;
    using System;

    internal static class SelectACLWithPermission
    {
        internal static SelectQuery GetQuery(string entityTable, string entityTableAclIDColumn, string aclEntryType, string aclEntryTypeID, string permission)
        {
            SelectQuery query = new SelectQuery(entityTable);
            query.AddParameter(entityTableAclIDColumn, string.Empty, entityTable);
            query.AddTableJoin("ckbx_AccessControlEntries", QueryJoinType.Inner, "AclID", entityTable, entityTableAclIDColumn);
            query.AddTableJoin("ckbx_AccessControlEntry", QueryJoinType.Inner, "EntryID", "ckbx_AccessControlEntries", "EntryID");
            query.AddTableJoin("ckbx_PolicyPermissions", QueryJoinType.Inner, "PolicyID", "ckbx_AccessControlEntry", "PolicyID");
            query.AddTableJoin("ckbx_Permission", QueryJoinType.Inner, "PermissionID", "ckbx_PolicyPermissions", "PermissionID");
            query.AddCriterion(new QueryCriterion(new SelectParameter("EntryType", string.Empty, "ckbx_AccessControlEntry"), CriteriaOperator.EqualTo, new LiteralParameter("'" + aclEntryType + "'")));
            query.AddCriterion(new QueryCriterion(new SelectParameter("EntryIdentifier", string.Empty, "ckbx_AccessControlEntry"), CriteriaOperator.EqualTo, new LiteralParameter("'" + aclEntryTypeID + "'")));
            query.AddCriterion(new QueryCriterion(new SelectParameter("PermissionName", string.Empty, "ckbx_Permission"), CriteriaOperator.EqualTo, new LiteralParameter("'" + permission + "'")));
            return query;
        }

        internal static SelectQuery GetQuery(string entityTable, string entityTableAclIDColumn, string aclEntryType, string aclEntryTypeID, PermissionJoin permissionJoinType, params string[] permissions)
        {
            SelectQuery query = new SelectQuery(entityTable);
            query.AddParameter(entityTableAclIDColumn, string.Empty, entityTable);
            query.AddTableJoin("ckbx_AccessControlEntries", QueryJoinType.Inner, "AclID", entityTable, entityTableAclIDColumn);
            query.AddTableJoin("ckbx_AccessControlEntry", QueryJoinType.Inner, "EntryID", "ckbx_AccessControlEntries", "EntryID");
            query.AddTableJoin("ckbx_PolicyPermissions", QueryJoinType.Inner, "PolicyID", "ckbx_AccessControlEntry", "PolicyID");
            query.AddTableJoin("ckbx_Permission", QueryJoinType.Inner, "PermissionID", "ckbx_PolicyPermissions", "PermissionID");
            query.AddCriterion(new QueryCriterion(new SelectParameter("EntryType", string.Empty, "ckbx_AccessControlEntry"), CriteriaOperator.EqualTo, new LiteralParameter("'" + aclEntryType + "'")));
            query.AddCriterion(new QueryCriterion(new SelectParameter("EntryIdentifier", string.Empty, "ckbx_AccessControlEntry"), CriteriaOperator.EqualTo, new LiteralParameter("'" + aclEntryTypeID + "'")));
            string paramValue = string.Empty;
            for (int i = 0; i < permissions.Length; i++)
            {
                if (i > 0)
                {
                    paramValue = paramValue + ",";
                }
                paramValue = paramValue + "'" + permissions[i] + "'";
            }
            query.AddCriterion(new QueryCriterion(new SelectParameter("PermissionName", string.Empty, "ckbx_Permission"), CriteriaOperator.In, new LiteralParameter(paramValue, string.Empty, true)));
            return query;
        }
    }
}

