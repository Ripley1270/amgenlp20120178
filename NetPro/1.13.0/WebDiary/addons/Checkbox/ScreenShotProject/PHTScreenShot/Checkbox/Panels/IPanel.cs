﻿namespace Checkbox.Panels
{
    using System;
    using System.Collections.Generic;

    public interface IPanel
    {
        Panelist GetPanelist(string identifier);

        List<Panelist> Panelists { get; }
    }
}

