﻿namespace Checkbox.Forms.Logic.Configuration
{
    using System;

    public enum ValidationState
    {
        Invalid = 1,
        Unknown = 2,
        Valid = 3
    }
}

