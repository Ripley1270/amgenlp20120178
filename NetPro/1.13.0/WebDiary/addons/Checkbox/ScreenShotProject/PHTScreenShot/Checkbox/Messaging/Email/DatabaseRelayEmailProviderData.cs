﻿namespace Checkbox.Messaging.Email
{
    using Prezza.Framework.Common;
    using Prezza.Framework.Configuration;
    using System;
    using System.Runtime.CompilerServices;
    using System.Xml;

    public class DatabaseRelayEmailProviderData : ProviderData, IXmlConfigurationBase
    {
        public DatabaseRelayEmailProviderData(string providerName) : base(providerName, typeof(DatabaseRelayEmailProvider).AssemblyQualifiedName)
        {
        }

        public void LoadFromXml(XmlNode node)
        {
            if (node == null)
            {
                throw new Exception("Unable to load DatabaseRelayEmailProviderData from null Xml node.");
            }
            this.MailDbInstanceName = XmlUtility.GetNodeText(node.SelectSingleNode("/databaseRelayEmailProviderData/mailDbInstanceName"), true);
        }

        public string MailDbInstanceName { get; private set; }
    }
}

