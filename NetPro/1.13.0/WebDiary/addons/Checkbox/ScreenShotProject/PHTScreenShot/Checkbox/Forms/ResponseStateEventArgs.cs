﻿namespace Checkbox.Forms
{
    using System;

    public class ResponseStateEventArgs : EventArgs
    {
        private readonly ResponseState _state;

        public ResponseStateEventArgs(ResponseState state)
        {
            this._state = state;
        }

        public ResponseState State
        {
            get
            {
                return this._state;
            }
        }
    }
}

