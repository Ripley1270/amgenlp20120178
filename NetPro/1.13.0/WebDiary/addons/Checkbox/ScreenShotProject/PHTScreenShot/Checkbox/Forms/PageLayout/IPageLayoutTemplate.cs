﻿namespace Checkbox.Forms.PageLayout
{
    using System;
    using System.Collections.ObjectModel;

    public interface IPageLayoutTemplate
    {
        void ClearZones();

        ILayoutZone DefaultZone { get; }

        bool LayoutDesignMode { get; set; }

        ReadOnlyCollection<string> ReservedZones { get; }

        string TypeName { get; }

        ReadOnlyCollection<ILayoutZone> Zones { get; }
    }
}

