﻿namespace Checkbox.Forms.Items.Configuration
{
    using System;
    using System.Collections.Generic;
    using System.Data;

    public class MatrixItemImportHandler : DefaultItemImportHandler
    {
        public override ItemData ImportItem(int itemID, DataSet ds, bool doNotNegateRelationalIDs)
        {
            return this.ImportMatrixItem(itemID, ds, doNotNegateRelationalIDs);
        }

        protected virtual ItemData ImportMatrixItem(int itemID, DataSet ds, bool doNotNegateRelationalIDs)
        {
            if (ds.Tables.Contains("Items"))
            {
                DataRow[] rowArray = ds.Tables["Items"].Select("ItemID = " + itemID, null, DataViewRowState.CurrentRows);
                try
                {
                    if (((rowArray.Length > 0) && (rowArray[0]["ItemName"] != DBNull.Value)) && (string.Compare((string) rowArray[0]["ItemName"], "Matrix", true) == 0))
                    {
                        MatrixItemData itemData = (MatrixItemData) DefaultItemImportHandler.CreateItemFromDataRow(rowArray[0]);
                        if (itemData != null)
                        {
                            Dictionary<int, int> dictionary = new Dictionary<int, int>();
                            itemData.Import(ds, doNotNegateRelationalIDs);
                            if (ds.Tables.Contains("ColumnTypes"))
                            {
                                foreach (DataRow row in ds.Tables["ColumnTypes"].Select("MatrixID = " + itemData.ID, null, DataViewRowState.CurrentRows))
                                {
                                    int? nullable = (row["ColumnPrototypeID"] != DBNull.Value) ? ((int?) row["ColumnPrototypeID"]) : null;
                                    int? nullable2 = (row["Column"] != DBNull.Value) ? ((int?) row["Column"]) : null;
                                    if (nullable.HasValue && nullable2.HasValue)
                                    {
                                        DataRow[] rowArray3 = ds.Tables["Items"].Select("ItemID = " + nullable, null, DataViewRowState.CurrentRows);
                                        if (rowArray3.Length > 0)
                                        {
                                            ItemData data2 = DefaultItemImportHandler.CreateItemFromDataRow(rowArray3[0]);
                                            if (data2 != null)
                                            {
                                                data2.Import(ds, doNotNegateRelationalIDs);
                                                bool columnUniqueness = itemData.GetColumnUniqueness(nullable2.Value);
                                                dictionary[nullable2.Value] = data2.ID.Value;
                                                DefaultItemImportHandler.SaveImportedItem(data2, ds);
                                                itemData.UpdateColumn(data2, nullable2.Value);
                                                itemData.SetColumnUniqueness(nullable2.Value, columnUniqueness);
                                            }
                                        }
                                    }
                                }
                            }
                            DefaultItemImportHandler.SaveImportedItem(itemData, ds);
                            int rowCount = itemData.RowCount;
                            foreach (int num2 in dictionary.Keys)
                            {
                                if (num2 != itemData.PrimaryKeyColumnIndex)
                                {
                                    for (int i = 1; i <= rowCount; i++)
                                    {
                                        ItemData itemAt = itemData.GetItemAt(i, num2);
                                        if ((itemAt != null) && ((itemAt is SelectItemData) || (itemAt is TextItemData)))
                                        {
                                            DefaultItemImportHandler.SaveImportedItemText(itemAt, ds, dictionary[num2]);
                                        }
                                    }
                                }
                            }
                            return itemData;
                        }
                    }
                }
                catch
                {
                }
            }
            return null;
        }
    }
}

