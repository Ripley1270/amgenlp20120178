﻿namespace Checkbox.Security.Data
{
    using Checkbox.Security;
    using Prezza.Framework.Data;
    using Prezza.Framework.Security;
    using Prezza.Framework.Security.Principal;
    using System;

    public static class QueryFactory
    {
        internal static SelectQuery GetSelectAllIdentitiesNotInAclQuery(AccessControlList acl, string[] rolePermissions)
        {
            return SelectAllIdentitiesNotInAclQuery.GetQuery(acl, rolePermissions);
        }

        internal static SelectQuery GetSelectAvailableIdentitiesNotInAclQuery(IAccessControlList acl, ExtendedPrincipal currentPrincipal, string[] aclPermissions, string[] rolePermissions)
        {
            return SelectAvailableIdentitiesNotInAclQuery.GetQuery(acl, currentPrincipal, aclPermissions, rolePermissions);
        }

        internal static SelectQuery GetSelectEntitiesWithPermissionQuery(string entityTable, string entityDefaultPolicyIDColumn, string permission)
        {
            return SelectEntitiesWithPermissionQuery.GetQuery(entityTable, entityDefaultPolicyIDColumn, permission);
        }

        internal static SelectQuery GetSelectEntitiesWithPermissionQuery(string entityTable, string entityDefaultPolicyIDColumn, PermissionJoin permissionJoinType, params string[] permissions)
        {
            return SelectEntitiesWithPermissionQuery.GetQuery(entityTable, entityDefaultPolicyIDColumn, permissionJoinType, permissions);
        }

        internal static SelectQuery GetSelectEntitiesWithPermissionQuery(string entityTable, string entityIDColumn, string entityAclIDColumn, string aclEntryType, string aclEntryTypeID, string permission)
        {
            return SelectEntitiesWithPermissionQuery.GetQuery(entityTable, entityIDColumn, entityAclIDColumn, aclEntryType, aclEntryTypeID, permission);
        }

        internal static SelectQuery GetSelectEntitiesWithPermissionQuery(string entityTable, string entityIDColumn, string entityAclIDColumn, string aclEntryType, string aclEntryTypeID, PermissionJoin permissionJoinType, params string[] permissions)
        {
            return SelectEntitiesWithPermissionQuery.GetQuery(entityTable, entityAclIDColumn, aclEntryType, aclEntryTypeID, permissionJoinType, permissions);
        }

        internal static SelectQuery GetSelectEntitiesWithPermissionQuery(string entityTable, string entityIDColumn, string entityAclIDColumn, string aclEntryType, string aclEntryTypeID, string permission, string entityDefaultPolicyIDColumn)
        {
            return SelectEntitiesWithPermissionQuery.GetQuery(entityTable, entityAclIDColumn, entityDefaultPolicyIDColumn, aclEntryType, aclEntryTypeID, permission);
        }

        internal static SelectQuery GetSelectEntitiesWithPermissionQuery(string entityTable, string entityIDColumn, string entityAclIDColumn, string aclEntryType, string aclEntryTypeID, string entityDefaultPolicyIDColumn, PermissionJoin permissionJoinType, params string[] permissions)
        {
            return SelectEntitiesWithPermissionQuery.GetQuery(entityTable, entityAclIDColumn, entityDefaultPolicyIDColumn, aclEntryType, aclEntryTypeID, permissionJoinType, permissions);
        }

        internal static SelectQuery GetSelectGroupsWithPermissionAndNotInAclQuery(IAccessControlList acl, ExtendedPrincipal currentPrincipal)
        {
            return SelectGroupsWithPermissionAndNotInAclQuery.GetQuery(acl, currentPrincipal);
        }

        public static SelectQuery GetSelectPermissionMaskPermissionsQuery(string maskName)
        {
            return SelectMaskPermissions.GetQuery(maskName);
        }
    }
}

