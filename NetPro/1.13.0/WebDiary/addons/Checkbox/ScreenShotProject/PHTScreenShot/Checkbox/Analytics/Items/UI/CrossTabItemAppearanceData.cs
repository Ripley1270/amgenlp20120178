﻿namespace Checkbox.Analytics.Items.UI
{
    using System;

    [Serializable]
    public class CrossTabItemAppearanceData : AnalysisItemAppearanceData
    {
        public CrossTabItemAppearanceData()
        {
            this.GraphType = Checkbox.Analytics.Items.UI.GraphType.CrossTab;
        }

        public override string AppearanceCode
        {
            get
            {
                return "ANALYSIS_CROSSTAB";
            }
        }

        public override Checkbox.Analytics.Items.UI.GraphType GraphType
        {
            get
            {
                return Checkbox.Analytics.Items.UI.GraphType.CrossTab;
            }
            set
            {
            }
        }
    }
}

