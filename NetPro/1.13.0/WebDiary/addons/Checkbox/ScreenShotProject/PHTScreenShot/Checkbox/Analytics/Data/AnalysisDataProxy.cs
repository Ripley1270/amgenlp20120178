﻿namespace Checkbox.Analytics.Data
{
    using Checkbox.Analytics;
    using Prezza.Framework.Caching;
    using System;
    using System.Collections.Generic;

    public static class AnalysisDataProxy
    {
        private static readonly CacheManager _analysisItemResultCache;

        static AnalysisDataProxy()
        {
            lock (typeof(AnalysisDataProxy))
            {
                _analysisItemResultCache = CacheFactory.GetCacheManager("analysisItemResultCache");
            }
        }

        public static void AddResultToCache<T>(int analysisItemId, string languageCode, string key, T result)
        {
            AnalysisDataProxyCacheItem<T> item = new AnalysisDataProxyCacheItem<T> {
                Data = result,
                ReferenceDate = DateTime.Now
            };
            _analysisItemResultCache.Add(GenerateResultCacheKey(analysisItemId, languageCode, key), item);
        }

        private static string GenerateResultCacheKey(int itemId, string languageCode, string key)
        {
            return string.Format("{0}_{1}_{2}", itemId, languageCode, key);
        }

        public static AnalysisDataProxyCacheItem<T> GetResultFromCache<T>(int analysisItemId, string languageCode, string key)
        {
            string str = GenerateResultCacheKey(analysisItemId, languageCode, key);
            if (_analysisItemResultCache.Contains(str))
            {
                return (_analysisItemResultCache[str] as AnalysisDataProxyCacheItem<T>);
            }
            return null;
        }

        public static DateTime? GetSurveyReferenceDate(IEnumerable<int> sourceResponseTemplateIds)
        {
            DateTime? nullable = null;
            foreach (int num in sourceResponseTemplateIds)
            {
                DateTime? nullable2;
                DateTime? nullable3;
                int? invitationId = null;
                ResponseManager.GetMinMaxResponseDates(new int?(num), invitationId, out nullable2, out nullable3);
                if (nullable3.HasValue)
                {
                    if (nullable.HasValue)
                    {
                        DateTime? nullable5 = nullable3;
                        DateTime? nullable6 = nullable;
                        if (!((nullable5.HasValue & nullable6.HasValue) ? (nullable5.GetValueOrDefault() > nullable6.GetValueOrDefault()) : false))
                        {
                            continue;
                        }
                    }
                    nullable = nullable3;
                }
            }
            return nullable;
        }

        public static bool ValidateItemResultData(int analysisItemId, string languageCode, string dataKey, DateTime? referenceDate)
        {
            AnalysisDataProxyCacheItem<object> item = GetResultFromCache<object>(analysisItemId, languageCode, dataKey);
            if (item == null)
            {
                return false;
            }
            if (!referenceDate.HasValue)
            {
                return true;
            }
            DateTime time = item.ReferenceDate;
            DateTime? nullable = referenceDate;
            if (!nullable.HasValue)
            {
                return false;
            }
            return (time >= nullable.GetValueOrDefault());
        }
    }
}

