﻿namespace Checkbox.Management.Licensing.Limits
{
    using System;
    using System.Runtime.InteropServices;
    using Xheo.Licensing;

    public abstract class StaticLicenseLimit : ValueLimit<bool?>
    {
        private bool? _licenseFileLimitValue;

        protected StaticLicenseLimit()
        {
        }

        public override void Initialize(ExtendedLicense license)
        {
            if ((license != null) && (license.Values[this.LimitName] != null))
            {
                try
                {
                    this._licenseFileLimitValue = new bool?(Convert.ToBoolean(license.Values[this.LimitName]));
                }
                catch
                {
                    this._licenseFileLimitValue = null;
                }
            }
        }

        public override LimitValidationResult ProtectedValidate(out string message)
        {
            message = string.Empty;
            if (this.RuntimeLimitValue.HasValue && this.RuntimeLimitValue.Value)
            {
                return LimitValidationResult.LimitNotReached;
            }
            return LimitValidationResult.LimitExceeded;
        }

        public override bool? LicenseFileLimitValue
        {
            get
            {
                return this._licenseFileLimitValue;
            }
        }
    }
}

