﻿namespace Checkbox.Forms.Validation
{
    using Checkbox.Globalization.Text;
    using System;

    public class USCurrencyValidator : RegularExpressionValidator
    {
        public USCurrencyValidator()
        {
            base._regex = @"^\$?(\d{1,3},?(\d{3},?)*\d{3}(\.\d{1,3})?|\d{1,3}(\.\d{2})?)$";
        }

        public override string GetMessage(string languageCode)
        {
            return TextManager.GetText("/validationMessages/regex/usCurrency", languageCode);
        }
    }
}

