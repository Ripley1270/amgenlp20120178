﻿namespace Checkbox.Analytics.Items
{
    using Checkbox.Analytics.Computation;
    using Checkbox.Analytics.Data;
    using Checkbox.Analytics.Items.Configuration;
    using Checkbox.Forms.Items.Configuration;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class StatisticsItem : AnalysisItem
    {
        private long _idSeed = 0x3e8L;

        private object AggregateAndCompute(bool isPreview)
        {
            ItemAnswerAggregator aggregator = this.AggregateResults(isPreview);
            StatisticsItemDataCalculator calculator = new StatisticsItemDataCalculator(this.ReportOption);
            DataSet data = calculator.GetData(aggregator);
            this.MinPossibleAnswerValue = calculator.MinPossibleAnswerValue;
            this.MaxPossibleAnswerValue = calculator.MaxPossibleAnswerValue;
            return data;
        }

        private ItemAnswerAggregator AggregateResults(bool isPreview)
        {
            ItemAnswerAggregator aggregator = new ItemAnswerAggregator();
            foreach (int num in this.SourceItemIDs)
            {
                aggregator.AddItem(num, this.GetItemText(num), this.GetSourceItemTypeName(num));
                List<int> list = isPreview ? this.GetItemOptionIdsForPreview(num) : this.GetItemOptionIdsForReport(num);
                foreach (int num2 in list)
                {
                    aggregator.AddItemOption(num, num2, this.GetOptionText(num, num2), new double?(base.GetOptionPoints(num, num2)), base.GetOptionIsOther(num, num2));
                }
                List<ItemAnswer> list2 = isPreview ? this.GeneratePreviewAnswers(num) : base.GetItemAnswers(num);
                foreach (ItemAnswer answer in list2)
                {
                    aggregator.AddAnswer(answer.AnswerId, answer.ResponseId, answer.ItemId, answer.OptionId, answer.AnswerText);
                }
            }
            return aggregator;
        }

        public override void Configure(ItemData itemData, string languageCode)
        {
            base.Configure(itemData, languageCode);
            this.ReportOption = ((StatisticsItemData) itemData).ReportOption;
        }

        private List<ItemAnswer> GeneratePreviewAnswers(int sourceItemId)
        {
            List<ItemAnswer> list = new List<ItemAnswer>();
            List<int> itemOptionIdsForPreview = this.GetItemOptionIdsForPreview(sourceItemId);
            for (int i = 0; i < itemOptionIdsForPreview.Count; i++)
            {
                for (int j = 0; j <= i; j++)
                {
                    ItemAnswer item = new ItemAnswer {
                        AnswerId = this._idSeed += 1L,
                        ResponseId = this._idSeed,
                        ItemId = sourceItemId,
                        OptionId = new int?(itemOptionIdsForPreview[i])
                    };
                    list.Add(item);
                }
            }
            return list;
        }

        protected override object GeneratePreviewData()
        {
            return this.AggregateAndCompute(true);
        }

        protected override object ProcessData()
        {
            return this.AggregateAndCompute(false);
        }

        public double MaxPossibleAnswerValue { get; private set; }

        public double MinPossibleAnswerValue { get; private set; }

        public StatisticsItemReportingOption ReportOption { get; private set; }
    }
}

