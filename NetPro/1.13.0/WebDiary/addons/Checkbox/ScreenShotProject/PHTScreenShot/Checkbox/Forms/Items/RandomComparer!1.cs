﻿namespace Checkbox.Forms.Items
{
    using System;
    using System.Collections.Generic;

    public class RandomComparer<T> : IComparer<T>
    {
        private static readonly Random _random;

        static RandomComparer()
        {
            RandomComparer<T>._random = new Random();
        }

        public int Compare(T x, T y)
        {
            if (x.Equals(y))
            {
                return 0;
            }
            if (RandomComparer<T>.RandomGetNext(1, 2) == 1)
            {
                return 1;
            }
            return -1;
        }

        protected static int RandomGetNext(int minValue, int maxValue)
        {
            return RandomComparer<T>._random.Next(minValue, maxValue + 1);
        }
    }
}

