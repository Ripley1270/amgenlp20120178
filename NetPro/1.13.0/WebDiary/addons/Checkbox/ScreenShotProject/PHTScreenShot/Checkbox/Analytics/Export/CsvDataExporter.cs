﻿namespace Checkbox.Analytics.Export
{
    using Checkbox.Analytics;
    using Checkbox.Analytics.Items;
    using Checkbox.Common;
    using Checkbox.Forms;
    using Checkbox.Forms.Items;
    using Checkbox.Globalization.Text;
    using Checkbox.Management;
    using Checkbox.Progress;
    using Checkbox.Users;
    using Prezza.Framework.Security;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Security.Principal;

    public class CsvDataExporter : SurveyDataExporter
    {
        public CsvDataExporter()
        {
            this.NewLineReplacement = ApplicationManager.AppSettings.NewLineReplacement;
            this.ReplaceNewLines = !this.NewLineReplacement.Equals("NEW_LINE", StringComparison.InvariantCultureIgnoreCase);
        }

        protected virtual void CalculateStartAndEndIndices()
        {
            this.StartFieldIndex = 0;
            this.EndFieldIndex = base.AllFieldNames.Count - 1;
            if (base.Options.FileSet.HasValue && (base.Options.FileSet > 0))
            {
                int num = base.Options.FileSet.Value;
                int num2 = 0xff;
                if (num > 1)
                {
                    num2--;
                }
                int num3 = (num - 1) * 0xff;
                if (num > 2)
                {
                    num3 -= num - 2;
                }
                int num4 = (num3 + num2) - 1;
                if (num4 >= base.AllFieldNames.Count)
                {
                    num4 = base.AllFieldNames.Count - 1;
                }
                this.StartFieldIndex = num3;
                this.EndFieldIndex = num4;
            }
        }

        protected override AnalysisTemplate CreateAnalysisTemplate()
        {
            return AnalysisTemplateManager.GenerateCSVExportTemplate(ResponseTemplateManager.GetResponseTemplate(base.ResponseTemplateId), base.ProgressKey, base.LanguageCode, 0, 0x19);
        }

        protected static List<string> GetAnswerData(int expectedAnswerCount, long responseId, Analysis analysis)
        {
            List<string> rowAnswers = new List<string>();
            foreach (Item item in analysis.Items)
            {
                if (item is ExportItem)
                {
                    rowAnswers = ((ExportItem) item).GetRowAnswers(responseId);
                    break;
                }
            }
            if (rowAnswers.Count < expectedAnswerCount)
            {
                int num = expectedAnswerCount - rowAnswers.Count;
                for (int i = 0; i < num; i++)
                {
                    rowAnswers.Add(string.Empty);
                }
            }
            if (rowAnswers.Count > expectedAnswerCount)
            {
                int count = rowAnswers.Count - expectedAnswerCount;
                rowAnswers.RemoveRange(expectedAnswerCount, count);
            }
            return rowAnswers;
        }

        protected static List<string> GetResponseData(IList<string> responseHeaders, ResponseProperties props)
        {
            List<string> list = new List<string>();
            for (int i = 0; i < responseHeaders.Count; i++)
            {
                list.Add(SurveyDataExporter.FormatResponseData(responseHeaders[i], props));
            }
            return list;
        }

        protected static List<string> GetUserData(IList<string> profileHeaders, string uniqueIdentifier)
        {
            List<string> list = new List<string>();
            if (profileHeaders.Count != 0)
            {
                IProfile profile = null;
                if (Utilities.IsNotNullOrEmpty(uniqueIdentifier))
                {
                    IIdentity identity = new GenericIdentity(uniqueIdentifier);
                    profile = UserManager.GetProfile(identity);
                }
                for (int i = 0; i < profileHeaders.Count; i++)
                {
                    if (((profile != null) && profile.Contains(profileHeaders[i])) && (profile[profileHeaders[i]] != null))
                    {
                        list.Add(profile[profileHeaders[i]].ToString());
                    }
                    else
                    {
                        list.Add(string.Empty);
                    }
                }
            }
            return list;
        }

        protected override void PopulateFieldNames(bool forceQuestionReload)
        {
            base.PopulateFieldNames(forceQuestionReload);
            this.CalculateStartAndEndIndices();
        }

        protected override void WriteExportData(TextWriter writer)
        {
            this.WriteHeaders(writer);
            writer.Write(Environment.NewLine);
            this.WriteResponses(writer);
        }

        protected void WriteHeaders(TextWriter writer)
        {
            if (Utilities.IsNotNullOrEmpty(base.ProgressKey))
            {
                ProgressData progressData = new ProgressData {
                    CurrentItem = 0x55,
                    TotalItemCount = 100,
                    Status = ProgressStatus.Pending,
                    Message = TextManager.GetText("/controlText/exportManager/writingHeaders", base.LanguageCode)
                };
                ProgressProvider.SetProgress(base.ProgressKey, progressData);
            }
            if (base.Options.FileSet.HasValue && (base.Options.FileSet > 1))
            {
                this.WriteValue("ResponseId", writer);
                writer.Write(',');
            }
            for (int i = this.StartFieldIndex; i <= this.EndFieldIndex; i++)
            {
                int? maxLength = null;
                this.WriteValue(Utilities.StripHtml(base.AllFieldNames[i], maxLength), writer);
                if (i < this.EndFieldIndex)
                {
                    writer.Write(',');
                }
            }
        }

        protected void WriteResponses(TextWriter writer)
        {
            List<string> list = new List<string>();
            List<long> list2 = base.Analysis.Data.ListResponseIds();
            int count = list2.Count;
            string format = Utilities.IsNotNullOrEmpty(base.LanguageCode) ? TextManager.GetText("/controlText/exportManager/exportingAnswers", base.LanguageCode) : string.Empty;
            for (int i = 0; i < count; i++)
            {
                long responseId = list2[i];
                ResponseProperties responseProperties = base.Analysis.Data.GetResponseProperties(responseId);
                if (base.Options.IncludeScore)
                {
                    responseProperties["Score"] = base.Analysis.Data.CalculateResponseScore(responseId);
                }
                list.AddRange(GetResponseData(base.ResponseFieldNames, responseProperties));
                list.AddRange(GetUserData(base.UserFieldNames, (string) responseProperties["UniqueIdentifier"]));
                list.AddRange(GetAnswerData(base.QuestionFieldNames.Count, responseId, base.Analysis));
                if (base.Options.FileSet.HasValue && (base.Options.FileSet.Value > 1))
                {
                    this.WriteValue(list[0], writer);
                    writer.Write(',');
                }
                for (int j = this.StartFieldIndex; j <= this.EndFieldIndex; j++)
                {
                    this.WriteValue(list[j], writer);
                    if (j < this.EndFieldIndex)
                    {
                        writer.Write(',');
                    }
                }
                writer.Write(Environment.NewLine);
                list.Clear();
                if ((i % ApplicationManager.AppSettings.ResponseDataExportChunkSize) == 0)
                {
                    writer.Flush();
                }
                if (Utilities.IsNotNullOrEmpty(base.ProgressKey) && ((i % 0x19) == 0))
                {
                    string str2 = (format.Contains("{0}") && format.Contains("{1}")) ? string.Format(format, i, count) : format;
                    double num5 = 100.0 * (0.85 + (0.14 * (((double) i) / ((double) count))));
                    ProgressData progressData = new ProgressData {
                        CurrentItem = (int) num5,
                        Status = ProgressStatus.Running,
                        Message = str2,
                        TotalItemCount = 100
                    };
                    ProgressProvider.SetProgress(base.ProgressKey, progressData);
                }
            }
            writer.Flush();
        }

        protected void WriteValue(string value, TextWriter tw)
        {
            value = value.Replace("\"", "\"\"");
            if (this.ReplaceNewLines)
            {
                value = value.Replace(Environment.NewLine, this.NewLineReplacement);
            }
            value = string.Format("\"{0}\"", value);
            tw.Write(value);
        }

        protected int EndFieldIndex { get; set; }

        protected string NewLineReplacement { get; set; }

        protected bool ReplaceNewLines { get; set; }

        protected int StartFieldIndex { get; set; }
    }
}

