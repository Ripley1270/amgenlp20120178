﻿namespace Checkbox.Forms.Items.UI
{
    using System;

    [Serializable]
    public class Hidden : AppearanceData
    {
        public override string AppearanceCode
        {
            get
            {
                return "HIDDEN";
            }
        }
    }
}

