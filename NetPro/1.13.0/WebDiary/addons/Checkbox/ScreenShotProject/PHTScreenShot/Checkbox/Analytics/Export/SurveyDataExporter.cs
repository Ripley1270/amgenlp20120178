﻿namespace Checkbox.Analytics.Export
{
    using Checkbox.Analytics;
    using Checkbox.Analytics.Items;
    using Checkbox.Analytics.Items.Configuration;
    using Checkbox.Common;
    using Checkbox.Forms;
    using Checkbox.Forms.Items;
    using Checkbox.Forms.Items.Configuration;
    using Checkbox.Globalization.Text;
    using Checkbox.Management;
    using Checkbox.Progress;
    using Checkbox.Users;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Runtime.CompilerServices;

    public abstract class SurveyDataExporter
    {
        private Checkbox.Analytics.Analysis _analysis;
        public const int MAX_COLUMN_COUNT_PER_FILE = 0xff;

        protected SurveyDataExporter()
        {
        }

        private void ConfigureAnalysisTemplate(AnalysisTemplate analysisTemplate)
        {
            analysisTemplate.FilterStartDate = this.Options.StartDate;
            analysisTemplate.FilterEndDate = this.Options.EndDate;
            foreach (ItemData data in analysisTemplate.Items)
            {
                if (data is ExportItemData)
                {
                    ((ExportItemData) data).UseAliases = this.Options.UseAliases;
                    ((ExportItemData) data).MergeSelectMany = this.Options.MergeSelectMany;
                    ((ExportItemData) data).IncludeOpenEnded = this.Options.IncludeOpenEnded;
                    ((ExportItemData) data).IncludeHidden = this.Options.IncludeHidden;
                }
            }
        }

        protected abstract AnalysisTemplate CreateAnalysisTemplate();
        protected static string FormatResponseData(string headerValue, ResponseProperties responseProperties)
        {
            if (headerValue.Equals("TotalTime", StringComparison.InvariantCultureIgnoreCase))
            {
                if ((responseProperties["Started"] != null) && (responseProperties["Ended"] != null))
                {
                    DateTime time = (DateTime) responseProperties["Started"];
                    DateTime time2 = (DateTime) responseProperties["Ended"];
                    return time2.Subtract(time).ToString();
                }
                return string.Empty;
            }
            object obj2 = responseProperties[headerValue];
            if ((obj2 != null) && (obj2 != DBNull.Value))
            {
                return obj2.ToString();
            }
            return string.Empty;
        }

        public virtual void Initialize(int responseTemplateId, string languageCode, ExportOptions options, string progressKey)
        {
            this.ResponseTemplateId = responseTemplateId;
            this.Options = options;
            this.ProgressKey = progressKey;
            this.LanguageCode = languageCode;
        }

        public ReadOnlyCollection<string> ListAllFieldNames()
        {
            if ((this.AllFieldNames == null) || (this.AllFieldNames.Count == 0))
            {
                this.PopulateFieldNames(false);
            }
            return new ReadOnlyCollection<string>(this.AllFieldNames);
        }

        protected virtual List<string> ListAllQuestionFieldNames()
        {
            foreach (Item item in this.Analysis.Items)
            {
                if (item is ExportItem)
                {
                    return ((ExportItem) item).GetColumnNames(this.ProgressKey, this.LanguageCode, 0x19, 50);
                }
            }
            return new List<string>();
        }

        protected virtual List<string> ListAllResponseFieldNames()
        {
            List<string> list = new List<string>();
            if (this.Options.IncludeResponseId || (this.Options.FileSet.HasValue && (this.Options.FileSet > 0)))
            {
                list.Add("ResponseID");
            }
            if (this.Options.IncludeDetailedResponseInfo)
            {
                list.Add("ResponseGuid");
                list.Add("Started");
                list.Add("Ended");
                list.Add("TotalTime");
                list.Add("LastEdit");
                list.Add("IP");
                list.Add("Language");
                list.Add("UniqueIdentifier");
                list.Add("Invitee");
                if (ApplicationManager.AppSettings.LogNetworkUser)
                {
                    list.Add("NetworkUser");
                }
            }
            if (this.Options.IncludeScore)
            {
                list.Add("Score");
            }
            return list;
        }

        protected virtual List<string> ListAllUserFieldNames()
        {
            List<string> list = new List<string>();
            if (this.Options.IncludeDetailedUserInfo)
            {
                ArrayList list2 = new ArrayList(UserManager.GetProfilePropertyList());
                foreach (object obj2 in list2)
                {
                    list.Add(obj2.ToString());
                }
            }
            return list;
        }

        protected virtual void PopulateFieldNames(bool forceColumnReload)
        {
            this.ResponseFieldNames = this.ListAllResponseFieldNames();
            this.UserFieldNames = this.ListAllUserFieldNames();
            if (forceColumnReload)
            {
                foreach (Item item in this.Analysis.Items)
                {
                    if (item is ExportItem)
                    {
                        ((ExportItem) item).ClearColumns();
                    }
                }
            }
            this.QuestionFieldNames = this.ListAllQuestionFieldNames();
            this.AllFieldNames = new List<string>();
            this.AllFieldNames.AddRange(this.ListAllResponseFieldNames());
            this.AllFieldNames.AddRange(this.ListAllUserFieldNames());
            this.AllFieldNames.AddRange(this.ListAllQuestionFieldNames());
        }

        protected abstract void WriteExportData(TextWriter writer);
        public virtual void WriteToFile(string filePath)
        {
            using (StreamWriter writer = File.CreateText(filePath))
            {
                try
                {
                    this.WriteToTextWriter(writer);
                }
                catch (Exception exception)
                {
                    ExceptionPolicy.HandleException(exception, "UIProcess");
                    throw;
                }
                finally
                {
                    writer.Close();
                }
            }
        }

        public void WriteToTextWriter(TextWriter writer)
        {
            if (Utilities.IsNotNullOrEmpty(this.ProgressKey))
            {
                ProgressData progressData = new ProgressData {
                    TotalItemCount = 100,
                    CurrentItem = 0,
                    Status = ProgressStatus.Pending,
                    Message = TextManager.GetText("/controlText/exportManager/analyzingSurveyStructure", this.LanguageCode)
                };
                ProgressProvider.SetProgress(this.ProgressKey, progressData);
            }
            this.PopulateFieldNames(true);
            this.WriteExportData(writer);
            if (Utilities.IsNotNullOrEmpty(this.ProgressKey))
            {
                ProgressData data2 = new ProgressData {
                    CurrentItem = 0x63,
                    Status = ProgressStatus.Pending,
                    Message = TextManager.GetText("/controlText/exportManager/completed", this.LanguageCode),
                    TotalItemCount = 100
                };
                ProgressProvider.SetProgress(this.ProgressKey, data2);
            }
        }

        protected List<string> AllFieldNames { get; set; }

        protected Checkbox.Analytics.Analysis Analysis
        {
            get
            {
                if (this._analysis == null)
                {
                    AnalysisTemplate analysisTemplate = this.CreateAnalysisTemplate();
                    this.ConfigureAnalysisTemplate(analysisTemplate);
                    this._analysis = analysisTemplate.CreateAnalysis(this.LanguageCode, this.Options.IncludeIncomplete, this.ProgressKey);
                }
                return this._analysis;
            }
        }

        protected bool AnalysisDataLoaded { get; set; }

        protected string LanguageCode { get; set; }

        protected ExportOptions Options { get; set; }

        protected string ProgressKey { get; set; }

        protected List<string> QuestionFieldNames { get; set; }

        protected List<string> ResponseFieldNames { get; set; }

        protected int ResponseTemplateId { get; set; }

        protected List<string> UserFieldNames { get; set; }
    }
}

