﻿namespace Checkbox.Panels
{
    using Prezza.Framework.Data.Sprocs;
    using Prezza.Framework.Security;
    using System;
    using System.Data;

    [FetchProcedure("ckbx_Panel_GetAccessControllableResource")]
    public class LightweightPanel : LightweightAccessControllable
    {
        private int _panelID;

        internal LightweightPanel(int panelID)
        {
            this._panelID = panelID;
        }

        [FetchParameter(Name="PanelID", DbType=DbType.Int32, Direction=ParameterDirection.Input)]
        public int PanelID
        {
            get
            {
                return this._panelID;
            }
            set
            {
                this._panelID = value;
            }
        }
    }
}

