﻿namespace Checkbox.Content
{
    using System;
    using System.Collections.Generic;

    public abstract class ContentFolder
    {
        private Dictionary<string, ContentFolder> _contentFolders;
        private Dictionary<string, ContentItem> _contentItems;
        private string _folderName;
        private string _folderPath;
        private bool _isPublic;

        protected ContentFolder()
        {
        }

        public virtual void AddContentItem(ContentItem item)
        {
            this.StoreContentItem(item);
            this.Items[item.ItemName] = item;
        }

        protected abstract void DeleteContentItem(ContentItem item);
        protected abstract Dictionary<string, ContentFolder> LoadFolders();
        protected abstract Dictionary<string, ContentItem> LoadItems();
        public virtual void RemoveContentItem(ContentItem item)
        {
            this.DeleteContentItem(item);
            if (this.Items.ContainsKey(item.ItemName))
            {
                this.Items.Remove(item.ItemName);
            }
        }

        protected abstract void StoreContentItem(ContentItem item);

        public virtual string FolderName
        {
            get
            {
                return this._folderName;
            }
            set
            {
                this._folderName = value;
            }
        }

        public virtual string FolderPath
        {
            get
            {
                return this._folderPath;
            }
            set
            {
                this._folderPath = value;
            }
        }

        protected Dictionary<string, ContentFolder> Folders
        {
            get
            {
                if (this._contentFolders == null)
                {
                    this._contentFolders = this.LoadFolders();
                }
                return this._contentFolders;
            }
        }

        public virtual bool IsPublic
        {
            get
            {
                return this._isPublic;
            }
            set
            {
                this._isPublic = value;
            }
        }

        protected Dictionary<string, ContentItem> Items
        {
            get
            {
                if (this._contentItems == null)
                {
                    this._contentItems = this.LoadItems();
                }
                return this._contentItems;
            }
        }
    }
}

