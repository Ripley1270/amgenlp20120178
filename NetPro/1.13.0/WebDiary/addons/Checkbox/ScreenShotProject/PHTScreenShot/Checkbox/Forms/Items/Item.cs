﻿namespace Checkbox.Forms.Items
{
    using Checkbox.Forms.Items.Configuration;
    using Checkbox.Globalization.Text;
    using Prezza.Framework.Common;
    using System;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    using System.Xml;
    using System.Xml.Schema;
    using System.Xml.Serialization;

    [Serializable]
    public abstract class Item : IDisposable, IXmlSerializable
    {
        [NonSerialized]
        private EventHandlerList _eventHandlers;
        private bool _exclude;
        private static readonly object EventItemExcluded = new object();
        private static readonly object EventItemIncluded = new object();
        private static readonly object EventLoad = new object();

        public event EventHandler ItemExcluded
        {
            add
            {
                this.Events.AddHandler(EventItemExcluded, value);
            }
            remove
            {
                this.Events.RemoveHandler(EventItemExcluded, value);
            }
        }

        public event EventHandler ItemIncluded
        {
            add
            {
                this.Events.AddHandler(EventItemIncluded, value);
            }
            remove
            {
                this.Events.RemoveHandler(EventItemIncluded, value);
            }
        }

        public event EventHandler Load
        {
            add
            {
                this.Events.AddHandler(EventLoad, value);
            }
            remove
            {
                this.Events.RemoveHandler(EventLoad, value);
            }
        }

        protected Item()
        {
            this.Visible = true;
        }

        public virtual void Configure(ItemData configuration, string languageCode)
        {
            this.Alias = configuration.Alias;
            if (configuration.ID.HasValue)
            {
                this.ID = configuration.ID.Value;
            }
            this.LanguageCode = languageCode;
            this.TypeID = configuration.ItemTypeID;
            this.ItemTypeName = configuration.ItemTypeName;
            this.IsActive = configuration.IsActive;
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
        }

        protected virtual NameValueCollection GetInstanceDataValuesForXmlSerialization()
        {
            return new NameValueCollection();
        }

        protected virtual NameValueCollection GetMetaDataValuesForXmlSerialization()
        {
            return new NameValueCollection();
        }

        public XmlSchema GetSchema()
        {
            return null;
        }

        protected virtual string GetText(string textID)
        {
            return TextManager.GetText(textID, this.LanguageCode);
        }

        protected virtual void OnItemExcluded()
        {
            EventHandler handler = (EventHandler) this.Events[EventItemExcluded];
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        protected virtual void OnItemIncluded()
        {
            EventHandler handler = (EventHandler) this.Events[EventItemIncluded];
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        protected virtual void OnLoad()
        {
            EventHandler handler = (EventHandler) this.Events[EventLoad];
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        protected virtual void OnPageLoad()
        {
            this.OnLoad();
        }

        internal virtual void Page_Load(object sender, EventArgs e)
        {
            this.OnPageLoad();
        }

        public void ReadXml(XmlReader reader)
        {
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(this.XmlElementName);
            writer.WriteAttributeString("itemId", this.ID.ToString());
            writer.WriteAttributeString("type", this.ItemTypeName);
            writer.WriteAttributeString("alias", this.Alias ?? string.Empty);
            writer.WriteAttributeString("isVisible", this.Visible.ToString());
            writer.WriteAttributeString("isExcluded", this.Excluded.ToString());
            writer.WriteAttributeString("isActive", this.IsActive.ToString());
            writer.WriteAttributeString("enableScoring", this.EnableScoring.ToString());
            writer.WriteStartElement(this.XmlMetaDataElementName);
            this.WriteXmlMetaData(writer);
            writer.WriteEndElement();
            writer.WriteStartElement(this.XmlInstanceDataElementName);
            this.WriteXmlInstanceData(writer);
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        public virtual void WriteXmlInstanceData(XmlWriter writer)
        {
            XmlUtility.SerializeNameValueCollection(writer, this.GetInstanceDataValuesForXmlSerialization(), true);
        }

        public virtual void WriteXmlMetaData(XmlWriter writer)
        {
            XmlUtility.SerializeNameValueCollection(writer, this.GetMetaDataValuesForXmlSerialization(), true);
        }

        public virtual string Alias { get; protected set; }

        public int ContainingPagePosition { get; set; }

        public bool EnableScoring { get; set; }

        protected EventHandlerList Events
        {
            get
            {
                if (this._eventHandlers == null)
                {
                    this._eventHandlers = new EventHandlerList();
                }
                return this._eventHandlers;
            }
        }

        public bool Excluded
        {
            get
            {
                return this._exclude;
            }
            set
            {
                if (value != this._exclude)
                {
                    this._exclude = value;
                    if (this._exclude)
                    {
                        this.OnItemExcluded();
                    }
                    else
                    {
                        this.OnItemIncluded();
                    }
                }
            }
        }

        public int ID { get; set; }

        public bool IsActive { get; set; }

        public string ItemTypeName { get; protected set; }

        public string LanguageCode { get; private set; }

        public int TypeID { get; protected set; }

        public virtual bool Visible { get; protected set; }

        protected virtual string XmlElementName
        {
            get
            {
                return "item";
            }
        }

        protected virtual string XmlInstanceDataElementName
        {
            get
            {
                return "instanceData";
            }
        }

        protected virtual string XmlMetaDataElementName
        {
            get
            {
                return "metaData";
            }
        }
    }
}

