﻿namespace Checkbox.Forms.Items.UI
{
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Collections;
    using System.Data;

    public static class AppearanceDataManager
    {
        private static readonly Hashtable _defaultTypes;
        private static readonly Hashtable _typeAssembliesByCode;
        private static readonly Hashtable _typeAssembliesByID;
        private static readonly Hashtable _typeNamesByCode;
        private static readonly Hashtable _typeNamesByID;

        static AppearanceDataManager()
        {
            lock (typeof(AppearanceDataManager))
            {
                _defaultTypes = new Hashtable();
                _typeAssembliesByID = new Hashtable();
                _typeNamesByID = new Hashtable();
                _typeAssembliesByCode = new Hashtable();
                _typeNamesByCode = new Hashtable();
            }
        }

        public static AppearanceData CopyAppearance(AppearanceData appearanceToCopy)
        {
            return appearanceToCopy.Copy();
        }

        public static AppearanceData GetAppearanceData(int appearanceID)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_AppearanceData_Get");
            storedProcCommandWrapper.AddInParameter("AppearanceID", DbType.Int32, appearanceID);
            AppearanceData appearanceDataForCode = null;
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    try
                    {
                        if (reader.Read())
                        {
                            string appearanceCode = (string) reader["AppearanceCode"];
                            appearanceDataForCode = GetAppearanceDataForCode(appearanceCode);
                            appearanceDataForCode.ID = new int?(appearanceID);
                            appearanceDataForCode.Load();
                        }
                    }
                    catch (Exception exception)
                    {
                        if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                        {
                            throw;
                        }
                    }
                    return appearanceDataForCode;
                }
                finally
                {
                    reader.Close();
                }
            }
            return appearanceDataForCode;
        }

        public static AppearanceData GetAppearanceData(string typeName)
        {
            AppearanceDataFactory factory = new AppearanceDataFactory();
            return factory.CreateAppearanceData(typeName);
        }

        public static AppearanceData GetAppearanceData(string typeName, int id)
        {
            AppearanceData data = new AppearanceDataFactory().CreateAppearanceData(typeName);
            data.ID = new int?(id);
            data.Load();
            return data;
        }

        public static AppearanceData GetAppearanceData(int appearanceID, string appearanceCode, DataSet ds)
        {
            AppearanceData appearanceDataForCode = GetAppearanceDataForCode(appearanceCode);
            if (appearanceDataForCode != null)
            {
                appearanceDataForCode.ID = new int?(appearanceID);
                if (ds != null)
                {
                    appearanceDataForCode.Load(ds);
                    return appearanceDataForCode;
                }
                appearanceDataForCode.Load();
            }
            return appearanceDataForCode;
        }

        public static AppearanceData GetAppearanceDataForCode(string appearanceCode)
        {
            string str = null;
            string str2 = null;
            if (_typeNamesByCode.ContainsKey(appearanceCode.ToLower()))
            {
                str = (string) _typeNamesByCode[appearanceCode.ToLower()];
                str2 = (string) _typeAssembliesByCode[appearanceCode.ToLower()];
            }
            else
            {
                Database database = DatabaseFactory.CreateDatabase();
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Appearance_GetByCode");
                storedProcCommandWrapper.AddInParameter("AppearanceCode", DbType.String, appearanceCode);
                using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
                {
                    try
                    {
                        if (reader.Read())
                        {
                            str = (string) reader["DataTypeName"];
                            str2 = (string) reader["DataTypeAssembly"];
                        }
                    }
                    finally
                    {
                        reader.Close();
                        if ((str != null) && (str2 != null))
                        {
                            lock (_typeNamesByCode.SyncRoot)
                            {
                                _typeNamesByCode[appearanceCode.ToLower()] = str;
                                _typeAssembliesByCode[appearanceCode.ToLower()] = str2;
                            }
                        }
                    }
                }
            }
            return GetAppearanceData(str + "," + str2);
        }

        public static AppearanceData GetAppearanceDataForItem(int itemId)
        {
            return GetAppearanceDataForItem(itemId, null);
        }

        public static AppearanceData GetAppearanceDataForItem(int itemId, IDbTransaction t)
        {
            if (itemId <= 0)
            {
                return null;
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Item_GetAppearance");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, itemId);
            AppearanceData appearanceData = null;
            IDataReader reader = (t != null) ? database.ExecuteReader(storedProcCommandWrapper, t) : database.ExecuteReader(storedProcCommandWrapper);
            try
            {
                if (reader.Read())
                {
                    string str = (string) reader["DataTypeName"];
                    string str2 = (string) reader["DataTypeAssembly"];
                    int id = Convert.ToInt32(reader["AppearanceID"]);
                    appearanceData = GetAppearanceData(str + "," + str2, id);
                }
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
            }
            finally
            {
                reader.Close();
            }
            return appearanceData;
        }

        public static string GetDefaultAppearanceCodeForType(int itemTypeId)
        {
            string str = null;
            if (_defaultTypes.ContainsKey(itemTypeId))
            {
                return (string) _defaultTypes[itemTypeId];
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Appearance_GetItemDefault");
            storedProcCommandWrapper.AddInParameter("ItemTypeID", DbType.Int32, itemTypeId);
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    if (reader.Read())
                    {
                        str = (string) reader["DefaultAppearanceCode"];
                    }
                }
                finally
                {
                    reader.Close();
                    if (str != null)
                    {
                        lock (_defaultTypes.SyncRoot)
                        {
                            _defaultTypes[itemTypeId] = str;
                        }
                    }
                }
            }
            return str;
        }

        public static AppearanceData GetDefaultAppearanceDataForType(int itemTypeId)
        {
            string str = null;
            string str2 = null;
            if (_typeNamesByID.ContainsKey(itemTypeId))
            {
                str = (string) _typeNamesByID[itemTypeId];
                str2 = (string) _typeAssembliesByID[itemTypeId];
            }
            else
            {
                Database database = DatabaseFactory.CreateDatabase();
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Appearance_GetItemDefault");
                storedProcCommandWrapper.AddInParameter("ItemTypeID", DbType.Int32, itemTypeId);
                using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
                {
                    try
                    {
                        if (reader.Read())
                        {
                            str = (string) reader["DataTypeName"];
                            str2 = (string) reader["DataTypeAssembly"];
                        }
                    }
                    catch (Exception exception)
                    {
                        if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                        {
                            throw;
                        }
                    }
                    finally
                    {
                        reader.Close();
                        if ((str != null) && (str2 != null))
                        {
                            lock (_typeNamesByID.SyncRoot)
                            {
                                _typeNamesByID[itemTypeId] = str;
                                _typeAssembliesByID[itemTypeId] = str2;
                            }
                        }
                    }
                }
            }
            AppearanceData appearanceData = null;
            if (((str != null) && (str.Trim() != string.Empty)) && ((str2 != null) && (str2.Trim() != string.Empty)))
            {
                appearanceData = GetAppearanceData(str + "," + str2);
            }
            return appearanceData;
        }

        public static void UpdateAppearanceCode(int appearanceID, string newAppearanceCode)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Appearance_UpdateCode");
            storedProcCommandWrapper.AddInParameter("AppearanceID", DbType.Int32, appearanceID);
            storedProcCommandWrapper.AddInParameter("NewAppearanceCode", DbType.String, newAppearanceCode);
            database.ExecuteNonQuery(storedProcCommandWrapper);
        }
    }
}

