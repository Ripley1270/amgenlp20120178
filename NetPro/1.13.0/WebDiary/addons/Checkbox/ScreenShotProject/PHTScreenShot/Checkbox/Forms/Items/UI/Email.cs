﻿namespace Checkbox.Forms.Items.UI
{
    using System;

    [Serializable]
    public class Email : AppearanceData
    {
        public override string AppearanceCode
        {
            get
            {
                return "EMAIL";
            }
        }
    }
}

