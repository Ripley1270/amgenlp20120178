﻿using System;
using System.Runtime.CompilerServices;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Checkbox.Forms.Items
{
    //using System;
    //using System.Runtime.CompilerServices;
    //using System.Xml;
    //using System.Xml.Schema;
    //using System.Xml.Serialization;

    [Serializable]
    public class ListOption : IEquatable<ListOption>, IEquatable<int>, IXmlSerializable
    {
        private string _text;
        public GetOptionTextDelegate OptionTextDelegate;

        public bool Equals(ListOption other)
        {
            return (this.ID == other.ID);
        }

        public bool Equals(int other)
        {
            return (this.ID == other);
        }

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            throw new NotImplementedException();
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("listOption");
            writer.WriteAttributeString("optionId", this.ID.ToString());
            writer.WriteElementString("text", this.Text);
            writer.WriteElementString("alias", this.Alias);
            writer.WriteElementString("isSelected", this.IsSelected.ToString());
            writer.WriteElementString("isOther", this.IsOther.ToString());
            //writer.WriteElementString("isEnabled", !this.Disabled.ToString());
            bool disabled = !this.Disabled;
            writer.WriteElementString("isEnabled", disabled.ToString());
            writer.WriteElementString("points", this.Points.ToString());
            writer.WriteEndElement();
        }

        public string Alias { get; set; }

        public bool Disabled { get; set; }

        public int ID { get; set; }

        public bool IsDefault { get; set; }

        public bool IsOther { get; set; }

        public bool IsSelected { get; set; }

        public double Points { get; set; }

        public string Text
        {
            get
            {
                if (this.OptionTextDelegate != null)
                {
                    return this.OptionTextDelegate(this.ID, this._text);
                }
                return this._text;
            }
            set
            {
                this._text = value;
            }
        }
    }
}

