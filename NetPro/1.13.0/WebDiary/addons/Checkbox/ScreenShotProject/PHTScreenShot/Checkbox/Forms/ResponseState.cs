﻿namespace Checkbox.Forms
{
    using Checkbox.Common;
    using Prezza.Framework.Data;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Runtime.CompilerServices;
    using System.Runtime.Serialization;
    using System.Security.Permissions;

    [Serializable]
    public class ResponseState : DataSet, IAnswerData
    {
        public event EventHandler Saved;

        public ResponseState()
        {
            base.RemotingFormat = SerializationFormat.Binary;
        }

        public ResponseState(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            base.RemotingFormat = SerializationFormat.Binary;
        }

        public void AddAnswerRow(DataRow row)
        {
            this.AnswerData.Rows.Add(row);
        }

        internal void ClearPageLog()
        {
            DataRow[] rowArray = this.Log.Select(null, null, DataViewRowState.CurrentRows);
            List<DataRow> list = new List<DataRow>();
            foreach (DataRow row in rowArray)
            {
                list.Add(row);
            }
            foreach (DataRow row2 in list)
            {
                row2.Delete();
            }
        }

        public DataRow CreateAnswerRow(int itemID)
        {
            DataRow row = this.AnswerData.NewRow();
            row["ResponseID"] = this.ResponseID;
            row["ItemID"] = itemID;
            return row;
        }

        public void DeleteAnswerRow(long answerID)
        {
            DataRow answerRow = this.GetAnswerRow(answerID);
            if (answerRow != null)
            {
                answerRow.Delete();
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                base.Clear();
            }
        }

        public DataRow GetAnswerRow(long answerID)
        {
            return this.AnswerData.Rows.Find(answerID);
        }

        public DataRow[] GetAnswerRowsForItem(int itemID)
        {
            return this.AnswerData.Select("ItemID = " + itemID, null, DataViewRowState.CurrentRows);
        }

        private DBCommandWrapper GetInsertItemOptionOrderCommand(Database db)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_Response_InsertItemOptionOrder");
            storedProcCommandWrapper.AddInParameter("ResponseID", DbType.Int64, this.ResponseID);
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, "ItemID", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("OptionID", DbType.Int32, "OptionID", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("Position", DbType.Int32, "Position", DataRowVersion.Current);
            return storedProcCommandWrapper;
        }

        private DBCommandWrapper GetInsertPageItemOrderCommand(Database db)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_Response_InsertPageItemOrder");
            storedProcCommandWrapper.AddInParameter("ResponseID", DbType.Int64, this.ResponseID);
            storedProcCommandWrapper.AddInParameter("PageID", DbType.Int32, "PageID", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, "ItemID", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("Position", DbType.Int32, "Position", DataRowVersion.Current);
            return storedProcCommandWrapper;
        }

        internal List<int> GetItemOptionOrder(int itemID)
        {
            DataRow[] rowArray = base.Tables["ItemOptionOrder"].Select("ItemID=" + itemID, "Position ASC", DataViewRowState.CurrentRows);
            List<int> list = new List<int>();
            foreach (DataRow row in rowArray)
            {
                if (row["OptionID"] != DBNull.Value)
                {
                    list.Add((int) row["OptionID"]);
                }
            }
            return list;
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter=true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }

        internal List<int> GetPageItemOrder(int pageID)
        {
            DataRow[] rowArray = base.Tables["PageItemOrder"].Select("PageID=" + pageID, "Position ASC", DataViewRowState.CurrentRows);
            List<int> list = new List<int>();
            foreach (DataRow row in rowArray)
            {
                if (row["ItemID"] != DBNull.Value)
                {
                    list.Add((int) row["ItemID"]);
                }
            }
            return list;
        }

        internal void InsertResponseData(Guid guid, int responseTemplateID, int lastPageViewed, string IP, string uniqueIdentifier, string networkUser, string languageCode, Guid? respondentGuid, bool isTest, bool isAnonymized, string invitee)
        {
            if (this.ResponseData == null)
            {
                string str = string.Empty;
                if (isAnonymized)
                {
                    str = Utilities.GetSaltedMD5Hash(uniqueIdentifier);
                    uniqueIdentifier = "AnonymizedRespondent";
                    networkUser = string.Empty;
                    invitee = string.Empty;
                    IP = string.Empty;
                }
                DataRow row = this.ResponseTable.NewRow();
                row["GUID"] = guid;
                row["ResponseTemplateID"] = responseTemplateID;
                row["LastPageViewed"] = lastPageViewed;
                row["IP"] = IP;
                if (((uniqueIdentifier == null) || (uniqueIdentifier.Trim() == string.Empty)) && (networkUser != null))
                {
                    row["UniqueIdentifier"] = networkUser;
                }
                else
                {
                    row["UniqueIdentifier"] = uniqueIdentifier;
                }
                row["NetworkUser"] = networkUser;
                row["Language"] = languageCode;
                row["Started"] = DateTime.Now;
                row["LastEdit"] = DateTime.Now;
                row["IsComplete"] = false;
                if (respondentGuid.HasValue)
                {
                    row["RespondentGuid"] = respondentGuid;
                }
                row["IsTest"] = isTest;
                row["IsAnonymized"] = isAnonymized;
                row["ResumeKey"] = str;
                row["Invitee"] = invitee;
                this.ResponseTable.Rows.Add(row);
            }
        }

        internal void Load(Guid responseGUID)
        {
            base.EnforceConstraints = true;
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Response_GetState");
            storedProcCommandWrapper.AddInParameter("GUID", DbType.Guid, responseGUID);
            database.LoadDataSet(storedProcCommandWrapper, this, new string[] { "Response", "Answers", "Log", "PageItemOrder", "ItemOptionOrder" });
            this.UpdateSchema();
        }

        internal void LoadSchemaOnly()
        {
            this.Load(Guid.Empty);
        }

        internal void PopPageLog()
        {
            DataRow[] rowArray = this.Log.Select(null, null, DataViewRowState.CurrentRows);
            if (rowArray.Length > 0)
            {
                rowArray[rowArray.Length - 1].Delete();
            }
        }

        internal void PushPageLog(int pageID)
        {
            DataRow row = this.Log.NewRow();
            row["ResponseID"] = this.ResponseID;
            row["PageID"] = pageID;
            row["PageStartTime"] = DateTime.Now;
            DataRow[] rowArray = this.Log.Select(null, null, DataViewRowState.CurrentRows);
            if (rowArray.Length > 0)
            {
                DataRow row2 = rowArray[rowArray.Length - 1];
                row2["PageEndTime"] = DateTime.Now;
            }
            this.Log.Rows.Add(row);
        }

        internal void Save()
        {
            Database db = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_Response_Create");
            storedProcCommandWrapper.AddInParameter("RespondentID", DbType.String, "UniqueIdentifier", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("GUID", DbType.Guid, "GUID", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("ResponseTemplateID", DbType.Int32, "ResponseTemplateID", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("LastPageViewed", DbType.Int32, "LastPageViewed", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("IP", DbType.String, "IP", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("NetworkUser", DbType.String, "NetworkUser", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("StartDate", DbType.DateTime, "Started", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("RespondentGuid", DbType.Guid, "RespondentGuid", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("LanguageCode", DbType.String, "Language", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("IsTest", DbType.Boolean, "IsTest", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("IsAnonymized", DbType.Boolean, "IsAnonymized", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("ResumeKey", DbType.String, "ResumeKey", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("Invitee", DbType.String, "Invitee", DataRowVersion.Current);
            storedProcCommandWrapper.AddParameter("ResponseID", DbType.Int64, ParameterDirection.Output, "ResponseID", DataRowVersion.Current, null);
            DBCommandWrapper updateCommand = db.GetStoredProcCommandWrapper("ckbx_Response_Update");
            updateCommand.AddInParameter("GUID", DbType.Guid, "GUID", DataRowVersion.Current);
            updateCommand.AddInParameter("LastEdit", DbType.DateTime, "LastEdit", DataRowVersion.Current);
            updateCommand.AddInParameter("IsComplete", DbType.Boolean, "IsComplete", DataRowVersion.Current);
            updateCommand.AddInParameter("EndDate", DbType.DateTime, "Ended", DataRowVersion.Current);
            updateCommand.AddInParameter("LastPageViewed", DbType.Int32, "LastPageViewed", DataRowVersion.Current);
            updateCommand.AddInParameter("Language", DbType.String, "Language", DataRowVersion.Current);
            DBCommandWrapper insertCommand = db.GetStoredProcCommandWrapper("ckbx_Response_CreateAnswer");
            insertCommand.AddInParameter("ResponseID", DbType.Int64, "ResponseID", DataRowVersion.Current);
            insertCommand.AddInParameter("ItemID", DbType.Int32, "ItemID", DataRowVersion.Current);
            insertCommand.AddInParameter("AnswerText", DbType.String, "AnswerText", DataRowVersion.Current);
            insertCommand.AddInParameter("OptionID", DbType.Int32, "OptionID", DataRowVersion.Current);
            insertCommand.AddInParameter("DateCreated", DbType.DateTime, DateTime.Now);
            insertCommand.AddParameter("AnswerID", DbType.Int64, ParameterDirection.Output, "AnswerID", DataRowVersion.Current, null);
            DBCommandWrapper wrapper4 = db.GetStoredProcCommandWrapper("ckbx_Response_UpdateAnswer");
            wrapper4.AddInParameter("AnswerID", DbType.Int64, "AnswerID", DataRowVersion.Current);
            wrapper4.AddInParameter("AnswerText", DbType.String, "AnswerText", DataRowVersion.Current);
            wrapper4.AddInParameter("OptionID", DbType.Int32, "OptionID", DataRowVersion.Current);
            wrapper4.AddInParameter("DateCreated", DbType.DateTime, DateTime.Now);
            DBCommandWrapper deleteCommand = db.GetStoredProcCommandWrapper("ckbx_Response_DeleteAnswer");
            deleteCommand.AddInParameter("AnswerID", DbType.Int64, "AnswerID", DataRowVersion.Current);
            DBCommandWrapper wrapper6 = db.GetStoredProcCommandWrapper("ckbx_Response_InsertPageLog");
            wrapper6.AddInParameter("ResponseID", DbType.Int64, "ResponseID", DataRowVersion.Current);
            wrapper6.AddInParameter("PageID", DbType.Int32, "PageID", DataRowVersion.Current);
            wrapper6.AddInParameter("PageStartTime", DbType.DateTime, "PageStartTime", DataRowVersion.Current);
            wrapper6.AddInParameter("PageEndTime", DbType.DateTime, "PageEndTime", DataRowVersion.Current);
            wrapper6.AddParameter("PageLogID", DbType.Int64, ParameterDirection.Output, "PageLogID", DataRowVersion.Current, null);
            DBCommandWrapper wrapper7 = db.GetStoredProcCommandWrapper("ckbx_Response_UpdatePageLog");
            wrapper7.AddInParameter("PageLogID", DbType.Int64, "PageLogID", DataRowVersion.Current);
            wrapper7.AddInParameter("PageEndTime", DbType.DateTime, "PageEndTime", DataRowVersion.Current);
            DBCommandWrapper wrapper8 = db.GetStoredProcCommandWrapper("ckbx_Response_DeletePageLog");
            wrapper8.AddInParameter("PageLogID", DbType.Int32, "PageLogID", DataRowVersion.Current);
            using (IDbConnection connection = db.GetConnection())
            {
                connection.Open();
                IDbTransaction transaction = connection.BeginTransaction();
                try
                {
                    db.UpdateDataSet(this, "Response", storedProcCommandWrapper, updateCommand, null, transaction);
                    db.UpdateDataSet(this, "Answers", insertCommand, wrapper4, deleteCommand, transaction);
                    db.UpdateDataSet(this, "PageItemOrder", this.GetInsertPageItemOrderCommand(db), null, null, transaction);
                    db.UpdateDataSet(this, "ItemOptionOrder", this.GetInsertItemOptionOrderCommand(db), null, null, transaction);
                    try
                    {
                        db.UpdateDataSet(this, "Log", wrapper6, wrapper7, wrapper8, transaction);
                    }
                    catch (DBConcurrencyException)
                    {
                        base.Tables["Log"].AcceptChanges();
                    }
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
            if (this.Saved != null)
            {
                this.Saved(this, new EventArgs());
            }
        }

        internal void SaveItemOptionOrder(int itemID, int optionID, int position)
        {
            DataRow row = base.Tables["ItemOptionOrder"].NewRow();
            row["ItemID"] = itemID;
            row["OptionID"] = optionID;
            row["Position"] = position;
            base.Tables["ItemOptionOrder"].Rows.Add(row);
        }

        internal void SavePageItemOrder(int pageID, int itemID, int position)
        {
            DataRow row = base.Tables["PageItemOrder"].NewRow();
            row["PageID"] = pageID;
            row["ItemID"] = itemID;
            row["Position"] = position;
            base.Tables["PageItemOrder"].Rows.Add(row);
        }

        private void UpdateSchema()
        {
            DataTable table = base.Tables["Response"];
            DataColumn parentColumn = table.Columns["ResponseID"];
            parentColumn.AutoIncrement = true;
            parentColumn.AutoIncrementSeed = -1L;
            parentColumn.AutoIncrementStep = -1L;
            parentColumn.AllowDBNull = false;
            DataColumn column2 = table.Columns["GUID"];
            column2.AllowDBNull = false;
            table.PrimaryKey = new DataColumn[] { parentColumn };
            DataTable table2 = base.Tables["Answers"];
            DataColumn column3 = table2.Columns["AnswerID"];
            column3.AutoIncrement = true;
            column3.AutoIncrementSeed = -1L;
            column3.AutoIncrementStep = -1L;
            column3.AllowDBNull = false;
            table2.PrimaryKey = new DataColumn[] { column3 };
            DataColumn column4 = this.Log.Columns["PageLogID"];
            column4.AutoIncrement = true;
            column4.AutoIncrementSeed = -1L;
            column4.AutoIncrementStep = -1L;
            column4.AllowDBNull = false;
            this.Log.PrimaryKey = new DataColumn[] { column4 };
            if (!base.Relations.Contains("Response_Answers"))
            {
                base.Relations.Add(new DataRelation("Response_Answers", parentColumn, table2.Columns["ResponseID"]));
            }
            if (!base.Relations.Contains("Response_PageLog"))
            {
                base.Relations.Add(new DataRelation("Response_PageLog", parentColumn, this.Log.Columns["ResponseID"]));
            }
            if (!base.Relations.Contains("Response_PageItemOrder"))
            {
                base.Relations.Add(new DataRelation("Response_PageItemOrder", parentColumn, base.Tables["PageItemOrder"].Columns["ResponseID"]));
            }
            if (!base.Relations.Contains("Response_ItemOptionOrder"))
            {
                base.Relations.Add(new DataRelation("Response_ItemOptionOrder", parentColumn, base.Tables["ItemOptionOrder"].Columns["ResponseID"]));
            }
        }

        private DataTable AnswerData
        {
            get
            {
                return base.Tables[1];
            }
        }

        internal DateTime? DateCompleted
        {
            get
            {
                return DbUtility.GetValueFromDataRow<DateTime?>(this.ResponseData, "Ended", null);
            }
            set
            {
                this.ResponseData["Ended"] = value;
            }
        }

        internal DateTime? DateCreated
        {
            get
            {
                return DbUtility.GetValueFromDataRow<DateTime?>(this.ResponseData, "Started", null);
            }
        }

        internal Guid? GUID
        {
            get
            {
                return DbUtility.GetValueFromDataRow<Guid?>(this.ResponseData, "GUID", null);
            }
        }

        internal string Invitee
        {
            get
            {
                return DbUtility.GetValueFromDataRow<string>(this.ResponseData, "Invitee", string.Empty);
            }
        }

        internal string IPAddress
        {
            get
            {
                return DbUtility.GetValueFromDataRow<string>(this.ResponseData, "IP", null);
            }
        }

        internal bool IsComplete
        {
            get
            {
                return DbUtility.GetValueFromDataRow<bool>(this.ResponseData, "IsComplete", false);
            }
        }

        public string LanguageCode
        {
            get
            {
                return DbUtility.GetValueFromDataRow<string>(this.ResponseData, "Language", null);
            }
            set
            {
                this.ResponseData["Language"] = value;
            }
        }

        public DateTime? LastModified
        {
            get
            {
                return DbUtility.GetValueFromDataRow<DateTime?>(this.ResponseData, "LastEdit", null);
            }
            internal set
            {
                this.ResponseData["LastEdit"] = value;
            }
        }

        public int? LastPageViewed
        {
            get
            {
                return DbUtility.GetValueFromDataRow<int?>(this.ResponseData, "LastPageViewed", null);
            }
            internal set
            {
                this.ResponseData["LastPageViewed"] = value;
            }
        }

        internal DataTable Log
        {
            get
            {
                return base.Tables[2];
            }
        }

        internal string NetworkUser
        {
            get
            {
                return DbUtility.GetValueFromDataRow<string>(this.ResponseData, "NetworkUser", null);
            }
        }

        internal Guid? RespondentGuid
        {
            get
            {
                return DbUtility.GetValueFromDataRow<Guid?>(this.ResponseData, "RespondentGuid", null);
            }
        }

        internal DataRow ResponseData
        {
            get
            {
                if (this.ResponseTable.Rows.Count == 0)
                {
                    return null;
                }
                return this.ResponseTable.Rows[0];
            }
        }

        public long? ResponseID
        {
            get
            {
                return DbUtility.GetValueFromDataRow<long?>(this.ResponseData, "ResponseID", null);
            }
        }

        private DataTable ResponseTable
        {
            get
            {
                return base.Tables[0];
            }
        }

        internal string UniqueIdentifier
        {
            get
            {
                return DbUtility.GetValueFromDataRow<string>(this.ResponseData, "UniqueIdentifier", null);
            }
        }

        internal int[] VisitedPages
        {
            get
            {
                List<int> list = new List<int>();
                foreach (DataRow row in this.Log.Select(null, null, DataViewRowState.CurrentRows))
                {
                    list.Add((int) row["PageID"]);
                }
                return list.ToArray();
            }
        }
    }
}

