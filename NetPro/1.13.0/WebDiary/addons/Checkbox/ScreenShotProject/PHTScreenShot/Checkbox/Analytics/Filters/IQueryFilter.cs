﻿namespace Checkbox.Analytics.Filters
{
    using System;

    internal interface IQueryFilter
    {
        string FilterString { get; }

        bool UseNotIn { get; }
    }
}

