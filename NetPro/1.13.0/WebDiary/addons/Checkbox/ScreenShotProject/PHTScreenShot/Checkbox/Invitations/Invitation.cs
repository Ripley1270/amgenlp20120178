﻿namespace Checkbox.Invitations
{
    using Checkbox.Common;
    using Checkbox.Invitations.Data;
    using Checkbox.Messaging.Email;
    using Checkbox.Panels;
    using Checkbox.Security;
    using Checkbox.Security.IdentityFilters;
    using Checkbox.Users;
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using Prezza.Framework.Security.Principal;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Security.Principal;

    [Serializable]
    public class Invitation
    {
        private List<int> _addedPanels;
        private AdHocEmailListPanel _adHocPanel;
        private int _nextPanelTempId;
        private Dictionary<int, Panel> _panels;
        private bool _recipientDataLoaded;
        private Dictionary<long, Recipient> _recipientDictionary;
        private List<int> _removedPanels;
        private InvitationTemplate _template;
        private UserPanel _userPanel;

        public Invitation()
        {
            this._recipientDataLoaded = false;
            this._nextPanelTempId = -1;
        }

        public Invitation(int id) : this()
        {
            this.ID = new int?(id);
            this._recipientDataLoaded = false;
        }

        public void AddPanel(Panel panel)
        {
            if (!panel.ID.HasValue)
            {
                panel.ID = new int?(this._nextPanelTempId);
                this._nextPanelTempId--;
            }
            if (!this.PanelDictionary.ContainsKey(panel.ID.Value))
            {
                this.AddedPanels.Add(panel.ID.Value);
            }
            this.PanelDictionary[panel.ID.Value] = panel;
        }

        public void AddPanel(Group group)
        {
            GroupPanel panel = (GroupPanel) PanelManager.CreatePanel(2);
            panel.Name = group.Name + " Panel";
            panel.Description = group.Description;
            panel.Group = group;
            this.AddPanel(panel);
        }

        public void AddPanel(List<IIdentity> identities)
        {
            this.UsersPanel.AddIdentities(identities);
        }

        public void AddPanel(List<string> addresses)
        {
            this.AdHocEmailPanel.AddEmailAddresses(addresses);
        }

        private void AddPanelMapping(int panelId, Database db, IDbTransaction transaction)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_Invitation_AddPanel");
            storedProcCommandWrapper.AddInParameter("InvitationID", DbType.Int32, this.ID.Value);
            storedProcCommandWrapper.AddInParameter("PanelID", DbType.Int32, panelId);
            db.ExecuteNonQuery(storedProcCommandWrapper, transaction);
        }

        protected virtual void Create(ExtendedPrincipal principal)
        {
            Database db = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_Invitation_Create");
            storedProcCommandWrapper.AddOutParameter("InvitationID", DbType.Int32, 4);
            storedProcCommandWrapper.AddInParameter("GUID", DbType.Guid, Guid.NewGuid());
            storedProcCommandWrapper.AddInParameter("ResponseTemplateID", DbType.Int32, this.ParentID);
            storedProcCommandWrapper.AddInParameter("EmailFormat", DbType.String, this.Template.Format.ToString());
            if (principal != null)
            {
                storedProcCommandWrapper.AddInParameter("CreatedBy", DbType.String, principal.Identity.Name);
            }
            else
            {
                storedProcCommandWrapper.AddInParameter("CreatedBy", DbType.String, string.Empty);
            }
            storedProcCommandWrapper.AddInParameter("IsPublic", DbType.Int32, 0);
            storedProcCommandWrapper.AddInParameter("DateCreated", DbType.DateTime, DateTime.Now);
            storedProcCommandWrapper.AddInParameter("Subject", DbType.String, this.Template.Subject);
            storedProcCommandWrapper.AddInParameter("Body", DbType.String, this.Template.Body);
            storedProcCommandWrapper.AddInParameter("FromAddress", DbType.String, this.Template.FromAddress);
            storedProcCommandWrapper.AddInParameter("FromName", DbType.String, this.Template.FromName);
            storedProcCommandWrapper.AddInParameter("LinkText", DbType.String, this.Template.LinkText);
            storedProcCommandWrapper.AddInParameter("LoginOption", DbType.String, this.Template.LoginOption);
            storedProcCommandWrapper.AddInParameter("IncludeOptOutLink", DbType.Boolean, this.Template.IncludeOptOutLink);
            storedProcCommandWrapper.AddInParameter("OptOutText", DbType.String, this.Template.OptOutText);
            try
            {
                using (IDbConnection connection = db.GetConnection())
                {
                    connection.Open();
                    IDbTransaction transaction = connection.BeginTransaction();
                    try
                    {
                        db.ExecuteNonQuery(storedProcCommandWrapper, transaction);
                        this.ID = new int?((int) storedProcCommandWrapper.GetParameterValue("InvitationID"));
                        this.SavePanels(principal, db, transaction);
                        transaction.Commit();
                    }
                    catch (Exception exception)
                    {
                        transaction.Rollback();
                        throw new ApplicationException("Unable to save Invitation", exception);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            catch (Exception exception2)
            {
                if (ExceptionPolicy.HandleException(exception2, "BusinessPublic"))
                {
                    throw;
                }
            }
        }

        protected virtual Recipient CreateRecipient(Panelist panelist, int panelID, int panelTypeId)
        {
            Recipient recipient;
            if (panelist.GetType() == typeof(UserPanelist))
            {
                recipient = new UserPanelistRecipient(panelist);
            }
            else
            {
                recipient = new Recipient(panelist);
            }
            this.SetRecipientProperties(recipient, panelID, panelTypeId);
            return recipient;
        }

        private static IdentityFilterCollection CreateUserFilter(string filterField, string filterValue)
        {
            if (Utilities.IsNullOrEmpty(filterValue) || Utilities.IsNullOrEmpty(filterField))
            {
                return null;
            }
            IdentityFilterCollection filters = new IdentityFilterCollection(IdentityFilterJoin.And);
            filters.AddFilter(new LikeFilter(filterField, filterValue, filterField.Equals("UniqueIdentifier", StringComparison.InvariantCultureIgnoreCase) ? IdentityFilterPropertyType.IdentityProperty : IdentityFilterPropertyType.ProfileProperty));
            return filters;
        }

        public int GetAvailableEmailListsCount(ExtendedPrincipal principal, string filterField, string filterValue)
        {
            return EmailListPanel.GetEmailListsCount(principal, filterField, filterValue, InvitationQueryFactory.CreateInvitationEmailListPanelFilter(this));
        }

        public DataSet GetAvailableEmailListsDataSet(ExtendedPrincipal principal, int pageSize, int pageNumber, string filterField, string filterValue, string sortField, bool sortDescending)
        {
            return EmailListPanel.GetAvailableEmailLists(principal, pageSize, pageNumber, filterField, filterValue, sortField, sortDescending, InvitationQueryFactory.CreateInvitationEmailListPanelFilter(this));
        }

        public int GetAvailableGroupsCount(ExtendedPrincipal principal, string filterField, string filterValue)
        {
            return Group.GetGroupsCount(principal, filterField, filterValue, InvitationQueryFactory.CreateInvitationGroupPanelFilter(principal, this.ID.Value, this.ParentID), PermissionJoin.Any, new string[] { "Group.View" });
        }

        public DataSet GetAvailableGroupsDataSet(ExtendedPrincipal principal, int pageSize, int pageNumber, string filterField, string filterValue, string sortField, bool sortDescending)
        {
            return Group.GetGroupsDataSet(principal, pageNumber, pageSize, filterField, filterValue, sortField, sortDescending, InvitationQueryFactory.CreateInvitationGroupPanelFilter(principal, this.ID.Value, this.ParentID), PermissionJoin.Any, new string[] { "Group.View" });
        }

        public DataSet GetAvailableUsersDataSet(ExtendedPrincipal callingPrincipal, int pageSize, int pageNumber, string filterField, string filterValue, string sortField, bool sortDescending, out int usersCount)
        {
            return UserManager.GetUsersDataSet(callingPrincipal, pageSize, pageNumber, filterField, filterValue, sortField, false, InvitationQueryFactory.CreateInvitationUserPanelFilter(callingPrincipal, this.ID.Value, this.ParentID), PermissionJoin.Any, new string[] { "Group.View" }, out usersCount);
        }

        public List<Recipient> GetPagedRecipientData(RecipientFilter filter, string sortField, bool sortAscending, int pageNumber, int resultsPerPage, out int count)
        {
            List<Recipient> listToPage = this.GetRecipients(filter, sortField, sortAscending);
            count = listToPage.Count;
            return Utilities.GetListDataPage<Recipient>(listToPage, pageNumber, resultsPerPage);
        }

        public virtual List<Recipient> GetPendingRecipients(bool groupRecipientsByPanel)
        {
            List<Recipient> list = new List<Recipient>();
            List<Recipient> recipients = this.GetRecipients(RecipientFilter.All);
            Hashtable hashtable = new Hashtable();
            foreach (Recipient recipient in recipients)
            {
                hashtable[recipient.ComputeHashKey()] = true;
            }
            foreach (Panel panel in this.PanelDictionary.Values)
            {
                if (panel.ID.HasValue)
                {
                    foreach (Panelist panelist in panel.Panelists)
                    {
                        string key = panelist.ComputeHashKey(panel.ID.Value);
                        if (!hashtable.ContainsKey(key))
                        {
                            if (groupRecipientsByPanel && ((panel is EmailListPanel) || (panel is GroupPanel)))
                            {
                                string str2 = (panel is EmailListPanel) ? panel.Name : ((GroupPanel) panel).Group.Name;
                                Recipient recipient2 = new Recipient {
                                    PanelTypeId = panel.PanelTypeID,
                                    UniqueIdentifier = str2,
                                    PanelID = panel.ID.Value
                                };
                                list.Add(recipient2);
                                break;
                            }
                            Recipient item = this.CreateRecipient(panelist, panel.ID.Value, panel.PanelTypeID);
                            if (Utilities.IsNullOrEmpty(item.UniqueIdentifier))
                            {
                                item.UniqueIdentifier = item.EmailToAddress;
                            }
                            list.Add(item);
                        }
                    }
                }
            }
            return list;
        }

        public List<Recipient> GetRecipients(RecipientFilter filter)
        {
            return this.GetRecipients(filter, "EmailAddress", true);
        }

        public List<Recipient> GetRecipients(IEnumerable<long> recipientIDs)
        {
            if (!this._recipientDataLoaded)
            {
                this.LoadRecipientData();
            }
            List<Recipient> list = new List<Recipient>();
            foreach (long num in recipientIDs)
            {
                if (this.RecipientDictionary.ContainsKey(num))
                {
                    list.Add(this.RecipientDictionary[num]);
                }
            }
            return list;
        }

        public List<Recipient> GetRecipients(RecipientFilter filter, string sortField, bool sortAscending)
        {
            if (!this._recipientDataLoaded)
            {
                this.LoadRecipientData();
            }
            if (filter == RecipientFilter.Pending)
            {
                return this.GetPendingRecipients(false);
            }
            List<Recipient> list = new List<Recipient>();
            foreach (Recipient recipient in this.RecipientDictionary.Values)
            {
                bool deleted = false;
                if (filter == RecipientFilter.NotResponded)
                {
                    deleted = (!recipient.Deleted && !recipient.OptedOut) && !recipient.HasResponded;
                }
                if (filter == RecipientFilter.Responded)
                {
                    deleted = (!recipient.Deleted && !recipient.OptedOut) && recipient.HasResponded;
                }
                if (filter == RecipientFilter.Current)
                {
                    deleted = !recipient.Deleted && !recipient.OptedOut;
                }
                if (filter == RecipientFilter.Deleted)
                {
                    deleted = recipient.Deleted;
                }
                if (filter == RecipientFilter.OptOut)
                {
                    deleted = recipient.OptedOut;
                }
                if (filter == RecipientFilter.All)
                {
                    deleted = !recipient.Deleted;
                }
                if (deleted)
                {
                    list.Add(recipient);
                }
            }
            return list;
        }

        public List<IIdentity> ListAvailableUsers(ExtendedPrincipal callingPrincipal, int pageSize, int pageNumber, string filterField, string filterValue, string sortField, bool sortDescending, out int totalUsersCount)
        {
            List<Panelist> panelists = this.UsersPanel.Panelists;
            List<string> list2 = new List<string>();
            foreach (Panelist panelist in panelists)
            {
                if (panelist is UserPanelist)
                {
                    list2.Add(((UserPanelist) panelist).UniqueIdentifier);
                }
            }
            List<IIdentity> list3 = UserManager.GetUserIdentities(callingPrincipal, CreateUserFilter(filterField, filterValue), PermissionJoin.Any, new string[] { "Group.View" });
            list2.Sort();
            List<IIdentity> listToPage = new List<IIdentity>();
            foreach (IIdentity identity in list3)
            {
                if (!list2.Contains(identity.Name))
                {
                    listToPage.Add(identity);
                }
            }
            totalUsersCount = listToPage.Count;
            return Utilities.GetListDataPage<IIdentity>(listToPage, pageNumber, pageSize);
        }

        public bool Load()
        {
            bool flag = false;
            this.PanelDictionary.Clear();
            this.RecipientDictionary.Clear();
            this.RemovedPanels.Clear();
            this.AddedPanels.Clear();
            this._recipientDataLoaded = false;
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Invitation_GetInvitation");
            storedProcCommandWrapper.AddInParameter("InvitationID", DbType.Int32, this.ID.Value);
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    while (reader.Read())
                    {
                        DateTime time;
                        this.GUID = (Guid) reader["GUID"];
                        this.Template.Format = (MailFormat) Enum.Parse(typeof(MailFormat), (string) reader["EmailFormat"]);
                        this.Template.FromAddress = DbUtility.GetValueFromDataReader<string>(reader, "FromAddress", string.Empty);
                        this.Template.FromName = DbUtility.GetValueFromDataReader<string>(reader, "FromName", string.Empty);
                        this.Template.Subject = DbUtility.GetValueFromDataReader<string>(reader, "Subject", string.Empty);
                        this.Template.Body = DbUtility.GetValueFromDataReader<string>(reader, "Body", string.Empty);
                        this.Template.OptOutText = DbUtility.GetValueFromDataReader<string>(reader, "OptOutText", string.Empty);
                        this.Template.IncludeOptOutLink = DbUtility.GetValueFromDataReader<bool>(reader, "IncludeOptOutLink", false);
                        this.Template.LinkText = DbUtility.GetValueFromDataReader<string>(reader, "LinkText", string.Empty);
                        if ((reader["LoginOption"] != DBNull.Value) && (((string) reader["LoginOption"]).ToLower() == "auto"))
                        {
                            this.Template.LoginOption = LoginOption.Auto;
                        }
                        else
                        {
                            this.Template.LoginOption = LoginOption.None;
                        }
                        this.Created = DbUtility.GetValueFromDataReader<DateTime>(reader, "DateCreated", DateTime.Now);
                        this.CreatedBy = DbUtility.GetValueFromDataReader<string>(reader, "CreatedBy", string.Empty);
                        if (DateTime.TryParse(reader["LastSentOn"].ToString(), out time))
                        {
                            this.LastSent = new DateTime?(time);
                        }
                        else
                        {
                            this.LastSent = null;
                        }
                        this.ParentID = (int) reader["ResponseTemplateID"];
                        flag = true;
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Invitation_GetPanels");
            storedProcCommandWrapper.AddInParameter("InvitationID", DbType.Int32, this.ID.Value);
            using (IDataReader reader2 = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    while (reader2.Read())
                    {
                        int panelID = (int) reader2["PanelID"];
                        Panel panel = PanelManager.GetPanel(panelID);
                        if (panel is UserPanel)
                        {
                            this.UsersPanel = (UserPanel) panel;
                        }
                        if (panel is AdHocEmailListPanel)
                        {
                            this.AdHocEmailPanel = (AdHocEmailListPanel) panel;
                        }
                        this.PanelDictionary[panel.ID.Value] = panel;
                    }
                }
                finally
                {
                    reader2.Close();
                }
            }
            return flag;
        }

        private void LoadRecipientData()
        {
            this.RecipientDictionary.Clear();
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Invitation_GetRecipients");
            storedProcCommandWrapper.AddInParameter("InvitationID", DbType.Int32, this.ID);
            storedProcCommandWrapper.AddInParameter("Filter", DbType.String, "All");
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    while (reader.Read())
                    {
                        Recipient recipient = new Recipient(reader);
                        if (recipient.ID.HasValue)
                        {
                            this.RecipientDictionary[recipient.ID.Value] = recipient;
                        }
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            this._recipientDataLoaded = true;
        }

        public void OptOutRecipient(long recipientID)
        {
            if (this.RecipientDictionary.ContainsKey(recipientID))
            {
                this.RecipientDictionary[recipientID].OptedOut = true;
                this.RecipientDictionary[recipientID].Modified = true;
            }
        }

        public static void RecordResponse(long recipientID, long responseID)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Invitation_InsertResponse");
            storedProcCommandWrapper.AddInParameter("RecipientID", DbType.Int64, recipientID);
            storedProcCommandWrapper.AddInParameter("ResponseID", DbType.Int64, responseID);
            database.ExecuteNonQuery(storedProcCommandWrapper);
        }

        public void RemovePanel(int panelId)
        {
            if (!this.RemovedPanels.Contains(panelId))
            {
                this.RemovedPanels.Add(panelId);
            }
            if (this.PanelDictionary.ContainsKey(panelId))
            {
                this.PanelDictionary.Remove(panelId);
            }
            foreach (Recipient recipient in this.RecipientDictionary.Values)
            {
                if (recipient.PanelID == panelId)
                {
                    recipient.Deleted = true;
                    recipient.Modified = true;
                }
            }
        }

        public void RemovePanelists(IEnumerable<IIdentity> userPanelists)
        {
            if (this._userPanel != null)
            {
                this._userPanel.RemoveIdentities(userPanelists);
            }
        }

        public void RemovePanelists(IEnumerable<string> emailPanelists)
        {
            if (this._adHocPanel != null)
            {
                this._adHocPanel.RemoveEmailAddresses(emailPanelists);
            }
        }

        private void RemovePanelMapping(int panelId, Database db, IDbTransaction transaction)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_Invitation_RemovePanel");
            storedProcCommandWrapper.AddInParameter("InvitationID", DbType.Int32, this.ID.Value);
            storedProcCommandWrapper.AddInParameter("PanelID", DbType.Int32, panelId);
            db.ExecuteNonQuery(storedProcCommandWrapper, transaction);
        }

        public void RemoveRecipient(long recipientID)
        {
            if (this.RecipientDictionary.ContainsKey(recipientID))
            {
                this.RecipientDictionary[recipientID].Deleted = true;
                this.RecipientDictionary[recipientID].Modified = true;
            }
        }

        public void RemoveRecipients(IEnumerable<long> recipients)
        {
            foreach (long num in recipients)
            {
                if (this.RecipientDictionary.ContainsKey(num))
                {
                    this.RecipientDictionary[num].Deleted = true;
                    this.RecipientDictionary[num].Modified = true;
                }
            }
        }

        public void RemoveRecipients(IEnumerable<IIdentity> recipients)
        {
            if (this._userPanel != null)
            {
                foreach (IIdentity identity in recipients)
                {
                    foreach (Recipient recipient in this.RecipientDictionary.Values)
                    {
                        int panelID = recipient.PanelID;
                        if ((panelID == this.UsersPanel.ID) && identity.Name.Equals(recipient.UniqueIdentifier, StringComparison.InvariantCultureIgnoreCase))
                        {
                            recipient.Deleted = true;
                            recipient.Modified = true;
                        }
                    }
                }
            }
        }

        public void RemoveRecipients(List<string> emailRecipients)
        {
            if (this._adHocPanel != null)
            {
                foreach (string str in emailRecipients)
                {
                    foreach (Recipient recipient in this.RecipientDictionary.Values)
                    {
                        int panelID = recipient.PanelID;
                        if ((panelID == this.AdHocEmailPanel.ID) && str.Equals(recipient.EmailToAddress, StringComparison.InvariantCultureIgnoreCase))
                        {
                            recipient.Deleted = true;
                            recipient.Modified = true;
                        }
                    }
                }
            }
        }

        public void Save(ExtendedPrincipal principal)
        {
            if (!this._recipientDataLoaded)
            {
                this.LoadRecipientData();
            }
            if (!this.ID.HasValue || (this.ID.Value <= 0))
            {
                this.Create(principal);
            }
            else
            {
                this.Update(principal);
            }
        }

        private void SavePanels(ExtendedPrincipal principal, Database db, IDbTransaction transaction)
        {
            List<Panel> list = new List<Panel>(this.PanelDictionary.Values);
            List<Recipient> recipients = this.GetRecipients(RecipientFilter.All);
            foreach (Panel panel in list)
            {
                int item = panel.ID.Value;
                panel.Save(principal, transaction);
                int num2 = panel.ID.Value;
                if (num2 != item)
                {
                    foreach (Recipient recipient in recipients)
                    {
                        if (recipient.PanelID == item)
                        {
                            recipient.PanelID = num2;
                            recipient.Modified = true;
                        }
                        if (recipient.Modified)
                        {
                            SaveRecipient(recipient, db, transaction);
                        }
                    }
                    if (this.AddedPanels.Contains(item))
                    {
                        this.AddedPanels.Remove(item);
                        this.AddedPanels.Add(num2);
                    }
                    if (this.RemovedPanels.Contains(item))
                    {
                        this.RemovedPanels.Remove(item);
                        this.RemovedPanels.Add(num2);
                    }
                    if (this.PanelDictionary.ContainsKey(item))
                    {
                        this.PanelDictionary.Remove(item);
                    }
                    this.PanelDictionary[num2] = panel;
                }
            }
            foreach (int num3 in this.AddedPanels)
            {
                this.AddPanelMapping(num3, db, transaction);
            }
            foreach (int num4 in this.RemovedPanels)
            {
                this.RemovePanelMapping(num4, db, transaction);
            }
            foreach (Recipient recipient2 in recipients)
            {
                if (recipient2.Modified)
                {
                    SaveRecipient(recipient2, db, transaction);
                }
            }
            foreach (Recipient recipient3 in this.GetRecipients(RecipientFilter.Deleted))
            {
                if (recipient3.Modified)
                {
                    SaveRecipient(recipient3, db, transaction);
                }
            }
        }

        private static void SaveRecipient(Recipient recipient, Database db, IDbTransaction transaction)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_Invitation_UpdateRecipient");
            storedProcCommandWrapper.AddInParameter("RecipientID", DbType.Int64, recipient.ID);
            storedProcCommandWrapper.AddInParameter("Deleted", DbType.Boolean, recipient.Deleted);
            storedProcCommandWrapper.AddInParameter("OptOut", DbType.Boolean, recipient.OptedOut);
            db.ExecuteNonQuery(storedProcCommandWrapper, transaction);
            if (recipient.BatchMessageId.HasValue && EmailGateway.ProviderSupportsBatches)
            {
                InvitationManager.RemoveRecipientMessagesFromEmailQueue(recipient.ID.Value, transaction);
            }
            recipient.Modified = false;
        }

        protected virtual void SetRecipientProperties(Recipient r, int panelID, int panelTypeId)
        {
            r.InvitationID = this.ID.Value;
            r.GUID = Guid.NewGuid();
            r.PanelID = panelID;
            r.PanelTypeId = panelTypeId;
        }

        protected virtual void Update(ExtendedPrincipal principal)
        {
            Database db = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_Invitation_Update");
            storedProcCommandWrapper.AddInParameter("InvitationID", DbType.Int32, this.ID.Value);
            storedProcCommandWrapper.AddInParameter("EmailFormat", DbType.String, this.Template.Format.ToString());
            storedProcCommandWrapper.AddInParameter("Subject", DbType.String, this.Template.Subject);
            storedProcCommandWrapper.AddInParameter("Body", DbType.String, this.Template.Body);
            storedProcCommandWrapper.AddInParameter("FromAddress", DbType.String, this.Template.FromAddress);
            storedProcCommandWrapper.AddInParameter("FromName", DbType.String, this.Template.FromName);
            storedProcCommandWrapper.AddInParameter("LinkText", DbType.String, this.Template.LinkText);
            storedProcCommandWrapper.AddInParameter("LoginOption", DbType.String, this.Template.LoginOption);
            storedProcCommandWrapper.AddInParameter("IncludeOptOutLink", DbType.Boolean, this.Template.IncludeOptOutLink);
            storedProcCommandWrapper.AddInParameter("OptOutText", DbType.String, this.Template.OptOutText);
            using (IDbConnection connection = db.GetConnection())
            {
                connection.Open();
                IDbTransaction transaction = connection.BeginTransaction();
                try
                {
                    db.ExecuteNonQuery(storedProcCommandWrapper);
                    this.SavePanels(principal, db, transaction);
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        protected List<int> AddedPanels
        {
            get
            {
                if (this._addedPanels == null)
                {
                    this._addedPanels = new List<int>();
                }
                return this._addedPanels;
            }
        }

        protected AdHocEmailListPanel AdHocEmailPanel
        {
            get
            {
                if (this._adHocPanel == null)
                {
                    this._adHocPanel = (AdHocEmailListPanel) PanelManager.CreatePanel(4);
                    this._adHocPanel.Name = "Invitation[" + this.ID.Value + "] AdHocEmailPanel";
                    this._adHocPanel.Description = "This Panel was autogenerated for this Invitation";
                    this.AddPanel(this._adHocPanel);
                }
                return this._adHocPanel;
            }
            set
            {
                this._adHocPanel = value;
                if (this._adHocPanel.ID.HasValue && !this.PanelDictionary.ContainsKey(this._adHocPanel.ID.Value))
                {
                    this.AddPanel(this._adHocPanel);
                }
            }
        }

        public DateTime Created { get; private set; }

        public string CreatedBy { get; private set; }

        public Guid GUID { get; private set; }

        public int? ID { get; private set; }

        public DateTime? LastSent { get; private set; }

        public Dictionary<int, Panel> PanelDictionary
        {
            get
            {
                if (this._panels == null)
                {
                    this._panels = new Dictionary<int, Panel>();
                }
                return this._panels;
            }
        }

        public int ParentID { get; set; }

        protected Dictionary<long, Recipient> RecipientDictionary
        {
            get
            {
                if (this._recipientDictionary == null)
                {
                    this._recipientDictionary = new Dictionary<long, Recipient>();
                }
                return this._recipientDictionary;
            }
        }

        protected List<int> RemovedPanels
        {
            get
            {
                if (this._removedPanels == null)
                {
                    this._removedPanels = new List<int>();
                }
                return this._removedPanels;
            }
        }

        public InvitationTemplate Template
        {
            get
            {
                if (this._template == null)
                {
                    this._template = new InvitationTemplate();
                }
                return this._template;
            }
        }

        protected UserPanel UsersPanel
        {
            get
            {
                if (this._userPanel == null)
                {
                    this._userPanel = (UserPanel) PanelManager.CreatePanel(1);
                    this._userPanel.Name = "Invitation[" + this.ID.Value + "] UserPanel";
                    this._userPanel.Description = "This Panel was autogenerated for this Invitation";
                    this.AddPanel(this._userPanel);
                }
                return this._userPanel;
            }
            set
            {
                this._userPanel = value;
                if (this._userPanel.ID.HasValue && !this.PanelDictionary.ContainsKey(this._userPanel.ID.Value))
                {
                    this.AddPanel(this._userPanel);
                }
            }
        }
    }
}

