﻿namespace Checkbox.Analytics.Items
{
    using Checkbox.Analytics.Computation;
    using Checkbox.Analytics.Data;
    using Checkbox.Analytics.Items.Configuration;
    using Checkbox.Forms.Data;
    using Checkbox.Forms.Items.Configuration;
    using Prezza.Framework.Common;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;

    [Serializable]
    public class MatrixSummaryItem : AnalysisItem
    {
        private int _matrixSourceItem;
        private SummaryItemProcessedData _sourceData;

        public override void Configure(ItemData itemData, string languageCode)
        {
            if (((MatrixSummaryItemData) itemData).MatrixSourceItem.HasValue)
            {
                this._matrixSourceItem = ((MatrixSummaryItemData) itemData).MatrixSourceItem.Value;
            }
            base.Configure(itemData, languageCode);
        }

        protected override object GeneratePreviewData()
        {
            return this.ProcessData(true);
        }

        public ReadOnlyCollection<int> GetColumnOptions(int column)
        {
            if (this.ProcessedData.ColumnItems.ContainsKey(column))
            {
                int key = this.ProcessedData.ColumnItems[column];
                if (this.ProcessedData.ItemOptions.ContainsKey(key))
                {
                    return new ReadOnlyCollection<int>(this.ProcessedData.ItemOptions[key]);
                }
            }
            return new ReadOnlyCollection<int>(new List<int>());
        }

        public int GetColumnProtoypeItemID(int column)
        {
            if (this.ProcessedData.ColumnItems.ContainsKey(column))
            {
                return this.ProcessedData.ColumnItems[column];
            }
            return -1;
        }

        public string GetColumnText(int column)
        {
            if (this.ProcessedData.ColumnItems.ContainsKey(column))
            {
                return this.GetItemText(this.ProcessedData.ColumnItems[column]);
            }
            return string.Empty;
        }

        public int GetItemIDAt(int row, int column)
        {
            Coordinate key = new Coordinate(column, row);
            if (this.ProcessedData.MatrixItems.ContainsKey(key))
            {
                return this.ProcessedData.MatrixItems[key];
            }
            return -1;
        }

        public override string GetItemText(int itemID)
        {
            if (!this.ProcessedData.RowTexts.ContainsKey(itemID))
            {
                return base.GetItemText(itemID);
            }
            return this.ProcessedData.RowTexts[itemID];
        }

        public string GetRowText(int row)
        {
            if (this.ProcessedData.RowItems.ContainsKey(row))
            {
                return this.GetItemText(this.ProcessedData.RowItems[row]);
            }
            return string.Empty;
        }

        public bool IsColumnRatingScale(int column)
        {
            return this.ProcessedData.RatingScaleColumns.Contains(column);
        }

        public bool IsColumnSumTotal(int column)
        {
            return this.ProcessedData.SumTotalColumns.Contains(column);
        }

        public bool IsRowOther(int row)
        {
            return this.ProcessedData.OtherRows.Contains(row);
        }

        public bool IsRowSubheading(int row)
        {
            return this.ProcessedData.HeaderRows.Contains(row);
        }

        private void LoadChildItemData(LightweightItemMetaData matrixItemData, SummaryItemProcessedData processedData)
        {
            processedData.ColumnCount = matrixItemData.ColumnCount;
            processedData.RowCount = matrixItemData.RowCount;
            processedData.MatrixItems.Clear();
            foreach (int num in matrixItemData.Children)
            {
                Coordinate childCoordinate = matrixItemData.GetChildCoordinate(num);
                if (childCoordinate != null)
                {
                    processedData.MatrixItems[childCoordinate] = num;
                }
            }
            processedData.RatingScaleColumns.Clear();
            processedData.SumTotalColumns.Clear();
            for (int i = 1; i <= processedData.ColumnCount; i++)
            {
                this.LoadColumnPrototype(i, matrixItemData.GetColumnPrototypeID(i), processedData);
            }
            for (int j = 1; j <= processedData.RowCount; j++)
            {
                this.LoadRowPkItem(j, matrixItemData.GetRowPkItemId(j), matrixItemData.GetRowType(j), processedData);
            }
        }

        private void LoadColumnPrototype(int columnNumber, int? columnPrototypeId, SummaryItemProcessedData processedData)
        {
            if (columnPrototypeId.HasValue && (columnNumber > 0))
            {
                LightweightItemMetaData itemData = SurveyMetaDataProxy.GetItemData(columnPrototypeId.Value, this.TemplatesValidated);
                if (itemData != null)
                {
                    if ("RadioButtonScale".Equals(itemData.ItemType, StringComparison.InvariantCultureIgnoreCase))
                    {
                        processedData.RatingScaleColumns.Add(columnNumber);
                    }
                    if ("MatrixSumTotal".Equals(itemData.ItemType, StringComparison.InvariantCultureIgnoreCase))
                    {
                        processedData.SumTotalColumns.Add(columnNumber);
                    }
                    if (!"SingleLineText".Equals(itemData.ItemType, StringComparison.InvariantCultureIgnoreCase))
                    {
                        processedData.ColumnItems[columnNumber] = columnPrototypeId.Value;
                        processedData.ItemOptions[itemData.ItemId] = itemData.Options;
                    }
                }
            }
        }

        private void LoadRowPkItem(int rowNumber, int? rowItemId, string rowType, SummaryItemProcessedData processedData)
        {
            if (rowItemId.HasValue)
            {
                string text = SurveyMetaDataProxy.GetItemText(rowItemId.Value, base.LanguageCode, base.UseAliases, this.TemplatesValidated);
                if ("Other".Equals(rowType, StringComparison.InvariantCultureIgnoreCase))
                {
                    processedData.OtherRows.Add(rowNumber);
                    text = this.GetText("/controlText/matrixSummaryItem/other");
                }
                if ("Subheading".Equals(rowType, StringComparison.InvariantCultureIgnoreCase))
                {
                    processedData.HeaderRows.Add(rowNumber);
                }
                processedData.RowTexts[rowItemId.Value] = text;
                processedData.RowItems[rowNumber] = rowItemId.Value;
            }
        }

        protected override object ProcessData()
        {
            return this.ProcessData(false);
        }

        protected virtual object ProcessData(bool isPreview)
        {
            ItemAnswerAggregator aggregator = new ItemAnswerAggregator();
            MatrixReportDataCalculator calculator = new MatrixReportDataCalculator();
            this.ProcessedData = this.ProcessSourceData();
            List<int> list = new List<int>(this.ProcessedData.ColumnItems.Keys);
            for (int i = 1; i <= this.ProcessedData.RowCount; i++)
            {
                foreach (int num2 in list)
                {
                    Coordinate key = new Coordinate(num2, i);
                    if (this.ProcessedData.MatrixItems.ContainsKey(key))
                    {
                        int itemId = this.ProcessedData.MatrixItems[key];
                        List<int> list2 = isPreview ? this.GetItemOptionIdsForPreview(itemId) : this.GetItemOptionIdsForReport(itemId);
                        aggregator.AddItem(itemId, string.Empty, this.GetSourceItemTypeName(itemId));
                        foreach (int num4 in list2)
                        {
                            aggregator.AddItemOption(itemId, num4, string.Empty, new double?(base.GetOptionPoints(itemId, num4)), base.GetOptionIsOther(itemId, num4));
                        }
                        if (this.IsColumnRatingScale(num2))
                        {
                            calculator.AddRatingScaleItem(itemId);
                        }
                        else if (this.IsColumnSumTotal(num2))
                        {
                            calculator.AddSumTotalItem(itemId);
                        }
                    }
                }
            }
            long num5 = 0x3e8L;
            for (int j = 1; j <= this.ProcessedData.RowCount; j++)
            {
                foreach (int num7 in list)
                {
                    Coordinate coordinate2 = new Coordinate(num7, j);
                    if (this.ProcessedData.MatrixItems.ContainsKey(coordinate2))
                    {
                        int itemID = this.ProcessedData.MatrixItems[coordinate2];
                        List<ItemAnswer> list3 = isPreview ? this.GetItemPreviewAnswers(itemID, new long?(num5), 0x3e8L) : base.GetItemAnswers(itemID);
                        foreach (ItemAnswer answer in list3)
                        {
                            if (answer.OptionId.HasValue)
                            {
                                aggregator.AddAnswer(answer.AnswerId, answer.ResponseId, itemID, answer.OptionId);
                            }
                            else
                            {
                                aggregator.AddAnswer(answer.AnswerId, answer.ResponseId, itemID, answer.AnswerText);
                            }
                        }
                    }
                }
                num5 += 1L;
            }
            DataSet data = calculator.GetData(aggregator);
            if (data.Tables.Contains("ResponseCounts") && (data.Tables["ResponseCounts"].Rows.Count > 0))
            {
                DataRow row = data.Tables["ResponseCounts"].NewRow();
                row["ItemId"] = this._matrixSourceItem;
                row["ResponseCount"] = aggregator.GetResponseCount(null);
                data.Tables["ResponseCounts"].Rows.Add(row);
                data.Tables["ResponseCounts"].AcceptChanges();
            }
            base.ResponseCounts[this._matrixSourceItem] = aggregator.GetResponseCount(null);
            base.DataProcessed = true;
            return data;
        }

        protected virtual SummaryItemProcessedData ProcessSourceData()
        {
            SummaryItemProcessedData processedData = new SummaryItemProcessedData();
            if (this.SourceItemIDs.Count > 0)
            {
                LightweightItemMetaData itemData = SurveyMetaDataProxy.GetItemData(this._matrixSourceItem, this.TemplatesValidated);
                if (itemData == null)
                {
                    return processedData;
                }
                this.LoadChildItemData(itemData, processedData);
                List<int> list = new List<int>(processedData.ColumnItems.Keys);
                for (int i = 1; i <= processedData.RowCount; i++)
                {
                    foreach (int num2 in list)
                    {
                        Coordinate key = new Coordinate(num2, i);
                        if (processedData.MatrixItems.ContainsKey(key))
                        {
                            int itemId = processedData.MatrixItems[key];
                            processedData.ItemOptions[itemId] = this.PreviewMode ? this.GetItemOptionIdsForPreview(itemId) : this.GetItemOptionIdsForReport(itemId);
                        }
                    }
                }
            }
            return processedData;
        }

        public int ColumnCount
        {
            get
            {
                return this.ProcessedData.ColumnCount;
            }
        }

        private SummaryItemProcessedData ProcessedData
        {
            get
            {
                if (this._sourceData == null)
                {
                    AnalysisDataProxyCacheItem<SummaryItemProcessedData> item = AnalysisDataProxy.GetResultFromCache<SummaryItemProcessedData>(base.ID, base.LanguageCode, base.GenerateDataKey() + "_SourceData");
                    if (item != null)
                    {
                        this._sourceData = item.Data;
                    }
                }
                if (this._sourceData == null)
                {
                    this._sourceData = this.ProcessSourceData();
                    if (this._sourceData != null)
                    {
                        AnalysisDataProxy.AddResultToCache<SummaryItemProcessedData>(base.ID, base.LanguageCode, base.GenerateDataKey() + "_SourceData", this._sourceData);
                    }
                }
                return this._sourceData;
            }
            set
            {
                this._sourceData = value;
                if (this._sourceData != null)
                {
                    AnalysisDataProxy.AddResultToCache<SummaryItemProcessedData>(base.ID, base.LanguageCode, base.GenerateDataKey() + "_SourceData", this._sourceData);
                }
            }
        }

        public int RowCount
        {
            get
            {
                return this.ProcessedData.RowCount;
            }
        }

        public override ReadOnlyCollection<int> SourceItemIDs
        {
            get
            {
                return new ReadOnlyCollection<int>(new List<int> { this._matrixSourceItem });
            }
        }

        [Serializable]
        public class SummaryItemProcessedData
        {
            public int ColumnCount;
            public readonly Dictionary<int, int> ColumnItems = new Dictionary<int, int>();
            public readonly List<int> HeaderRows = new List<int>();
            public readonly Dictionary<int, List<int>> ItemOptions = new Dictionary<int, List<int>>();
            public readonly Dictionary<Coordinate, int> MatrixItems = new Dictionary<Coordinate, int>(new CoordinateComparer());
            public readonly List<int> OtherRows = new List<int>();
            public readonly List<int> RatingScaleColumns = new List<int>();
            public int RowCount;
            public readonly Dictionary<int, int> RowItems = new Dictionary<int, int>();
            public readonly Dictionary<int, string> RowTexts = new Dictionary<int, string>();
            public readonly List<int> SumTotalColumns = new List<int>();
        }
    }
}

