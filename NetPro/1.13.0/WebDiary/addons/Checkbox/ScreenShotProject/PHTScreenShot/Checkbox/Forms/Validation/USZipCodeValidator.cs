﻿namespace Checkbox.Forms.Validation
{
    using Checkbox.Globalization.Text;
    using System;

    public class USZipCodeValidator : RegularExpressionValidator
    {
        public USZipCodeValidator()
        {
            base._regex = @"^\d{5}-\d{4}$|^\d{5}$";
        }

        public override string GetMessage(string languageCode)
        {
            return TextManager.GetText("/validationMessages/regex/usPostal", languageCode);
        }
    }
}

