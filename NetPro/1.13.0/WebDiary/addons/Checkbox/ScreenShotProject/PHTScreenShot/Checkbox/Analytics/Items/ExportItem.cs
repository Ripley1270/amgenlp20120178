﻿namespace Checkbox.Analytics.Items
{
    using Checkbox.Analytics.Data;
    using Checkbox.Common;
    using Checkbox.Forms;
    using Checkbox.Forms.Items.Configuration;
    using Checkbox.Globalization.Text;
    using Checkbox.Progress;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    [Serializable]
    public abstract class ExportItem : AnalysisItem
    {
        private List<string> _columnNames;
        private bool _columnsAdded;
        private Dictionary<int, ColumnData> _columnToIdMapping;
        private List<int> _multiselectOtherOptions;
        private List<int> _naOptions;

        protected ExportItem()
        {
        }

        protected virtual void AddColumn(int columnNumber, string columnName, int itemId, int? optionId)
        {
            this._columnNames.Add(columnName);
            ColumnData data = new ColumnData {
                ItemId = itemId,
                OptionId = optionId
            };
            this._columnToIdMapping[columnNumber] = data;
        }

        protected virtual void AddColumns(string progressKey, string languageCode, int startProgress, int endProgress)
        {
            this.ClearColumns();
            foreach (int num in base.SourceResponseTemplateIDs)
            {
                this.AddColumnsForResponseTemplate(num, progressKey, languageCode, startProgress, endProgress);
            }
        }

        protected virtual void AddColumnsForCompositeItem(ICompositeItemData itemData, string prefix)
        {
            string str = this.DetermineColumnTextForItem((ItemData) itemData, prefix);
            foreach (ItemData data in itemData.GetChildItemDatas())
            {
                this.AddColumnsForItem(data, str, string.Empty);
            }
        }

        protected virtual void AddColumnsForItem(ItemData itemData, string prefix, string textOverride)
        {
            if (itemData.ItemIsIAnswerable)
            {
                if (itemData is MatrixItemData)
                {
                    this.AddColumnsForMatrixItem((MatrixItemData) itemData, prefix);
                }
                else if (itemData is ICompositeItemData)
                {
                    this.AddColumnsForCompositeItem((ICompositeItemData) itemData, prefix);
                }
                else if (itemData is UploadItemData)
                {
                    this.AddColumnsForUploadItem(itemData, prefix, textOverride);
                }
                else if (itemData is SelectItemData)
                {
                    this.AddColumnsForSelectItem((SelectItemData) itemData, prefix, textOverride);
                }
                else if (((itemData is TextItemData) && this.IncludeOpenEnded) || ((itemData is HiddenItemData) && this.IncludeHidden))
                {
                    string str;
                    if ((textOverride != null) && (textOverride.Trim() != string.Empty))
                    {
                        str = textOverride;
                    }
                    else
                    {
                        str = this.DetermineColumnTextForItem(itemData, prefix);
                    }
                    this.AddColumn(this.ColumnCount, str, itemData.ID.Value, null);
                }
            }
        }

        protected virtual void AddColumnsForMatrixItem(MatrixItemData itemData, string prefix)
        {
            string str = this.DetermineColumnTextForItem(itemData, prefix);
            MatrixItemTextDecorator decorator = (MatrixItemTextDecorator) itemData.CreateTextDecorator(base.LanguageCode);
            for (int i = 1; i <= itemData.RowCount; i++)
            {
                if (!itemData.IsRowSubheading(i))
                {
                    string rowAlias = string.Empty;
                    if (base.UseAliases)
                    {
                        rowAlias = decorator.Data.GetRowAlias(i);
                    }
                    if (Utilities.IsNullOrEmpty(rowAlias))
                    {
                        rowAlias = decorator.GetRowText(i);
                    }
                    if (Utilities.IsNullOrEmpty(rowAlias))
                    {
                        rowAlias = "Row" + i;
                    }
                    for (int j = 1; j <= itemData.ColumnCount; j++)
                    {
                        ItemData itemAt = itemData.GetItemAt(i, j);
                        if (j == itemData.PrimaryKeyColumnIndex)
                        {
                            if (((itemAt != null) && itemAt.ItemIsIAnswerable) && this.IncludeOpenEnded)
                            {
                                this.AddColumnsForItem(itemAt, string.Empty, str + "_" + rowAlias + "_Other");
                            }
                        }
                        else
                        {
                            string textOverride = string.Empty;
                            if (base.UseAliases && (itemData.GetColumnPrototype(j) != null))
                            {
                                textOverride = itemData.GetColumnPrototype(j).Alias;
                            }
                            if ((textOverride == null) || (textOverride.Trim() == string.Empty))
                            {
                                textOverride = decorator.GetColumnText(j);
                            }
                            if ((textOverride == null) || (textOverride.Trim() == string.Empty))
                            {
                                textOverride = "Column" + j;
                            }
                            textOverride = str + "_" + rowAlias + "_" + textOverride;
                            if ((itemAt is TextItemData) && this.IncludeOpenEnded)
                            {
                                this.AddColumnsForItem(itemAt, string.Empty, textOverride);
                            }
                            else if (itemAt is SelectItemData)
                            {
                                if (!(itemAt is SelectManyData) || this.MergeSelectMany)
                                {
                                    this.AddColumnsForItem(itemAt, string.Empty, textOverride);
                                }
                                this.AddColumnsForSelectItemOptions((SelectItemData) itemAt, textOverride + "_");
                            }
                        }
                    }
                }
            }
        }

        protected virtual void AddColumnsForResponseTemplate(int responseTemplateID, string progressKey, string languageCode, int startProgress, int endProgress)
        {
            ResponseTemplate responseTemplate = ResponseTemplateManager.GetResponseTemplate(responseTemplateID);
            if (responseTemplate != null)
            {
                bool flag = Utilities.IsNotNullOrEmpty(progressKey) && Utilities.IsNotNullOrEmpty(languageCode);
                string format = flag ? TextManager.GetText("/controlText/exportManager/loadingItems", languageCode) : string.Empty;
                int num = 1;
                foreach (TemplatePage page in responseTemplate.TemplatePages)
                {
                    foreach (ItemData data in page.Items)
                    {
                        if (this.SourceItemIDs.Contains(data.ID.Value))
                        {
                            this.AddColumnsForItem(data, string.Empty, string.Empty);
                            if (flag)
                            {
                                string message = (format.Contains("{0}") && format.Contains("{1}")) ? string.Format(format, num, responseTemplate.ItemCount) : format;
                                int currentItem = startProgress + ((int) ((((double) num) / ((double) responseTemplate.ItemCount)) * (endProgress - startProgress)));
                                ProgressProvider.SetProgress(progressKey, message, string.Empty, ProgressStatus.Running, currentItem, 100);
                            }
                            num++;
                        }
                    }
                }
            }
        }

        protected virtual void AddColumnsForSelectItem(SelectItemData itemData, string prefix, string textOverride)
        {
            string str;
            if ((textOverride == null) || (textOverride.Trim() == string.Empty))
            {
                str = this.DetermineColumnTextForItem(itemData, prefix);
            }
            else
            {
                str = textOverride;
            }
            if (!(itemData is SelectManyData) || this.MergeSelectMany)
            {
                this.AddColumn(this.ColumnCount, str, itemData.ID.Value, null);
            }
            this.AddColumnsForSelectItemOptions(itemData, str + "_");
        }

        protected virtual void AddColumnsForSelectItemOptions(SelectItemData itemData, string prefix)
        {
            bool flag = itemData is SelectManyData;
            bool flag2 = itemData is RatingScaleItemData;
            if ((flag2 || !flag) || !this.MergeSelectMany)
            {
                foreach (ListOptionData data in itemData.Options)
                {
                    if (flag)
                    {
                        string columnName = prefix + this.GetOptionText(itemData.ID.Value, data.OptionID);
                        this.AddColumn(this.ColumnCount, columnName, itemData.ID.Value, new int?(data.OptionID));
                    }
                    if (data.IsOther)
                    {
                        if (flag)
                        {
                            this._multiselectOtherOptions.Add(data.OptionID);
                        }
                        if (flag2)
                        {
                            this._naOptions.Add(data.OptionID);
                        }
                    }
                }
            }
        }

        protected virtual void AddColumnsForUploadItem(ItemData itemData, string prefix, string textOverride)
        {
            string str;
            if ((textOverride == null) || (textOverride.Trim() == string.Empty))
            {
                str = this.DetermineColumnTextForItem(itemData, prefix);
            }
            else
            {
                str = textOverride;
            }
            this.AddColumn(this.ColumnCount, str, itemData.ID.Value, null);
        }

        public void ClearColumns()
        {
            this._columnNames.Clear();
            this._columnToIdMapping.Clear();
            this._multiselectOtherOptions.Clear();
            this._naOptions.Clear();
            this._columnsAdded = false;
        }

        public override void Configure(ItemData itemData, string languageCode)
        {
            base.Configure(itemData, languageCode);
            this._columnNames = new List<string>();
            this._columnToIdMapping = new Dictionary<int, ColumnData>();
            this._multiselectOtherOptions = new List<int>();
            this._naOptions = new List<int>();
        }

        protected virtual string DetermineColumnTextForItem(ItemData itemData, string prefix)
        {
            return (prefix + this.GetItemText(itemData.ID.Value));
        }

        private void EnsureColumnsAdded(string progressKey, string languageCode, int startProgress, int endProgress)
        {
            if (!this._columnsAdded)
            {
                this.AddColumns(progressKey, languageCode, startProgress, endProgress);
                this._columnsAdded = true;
            }
        }

        public virtual string GetAnswerColumnValue(long responseID, int column)
        {
            if (!this._columnToIdMapping.ContainsKey(column))
            {
                return string.Empty;
            }
            ColumnData data = this._columnToIdMapping[column];
            if (data.OptionId.HasValue)
            {
                ItemAnswer answerDataObject = base.GetAnalysisData().GetOptionAnswer(responseID, data.ItemId, data.OptionId.Value);
                if (this._multiselectOtherOptions.Contains(data.OptionId.Value))
                {
                    if (answerDataObject != null)
                    {
                        if (this.IncludeOpenEnded)
                        {
                            return this.GetAnswerText(answerDataObject);
                        }
                        return "1";
                    }
                    if (this.IncludeOpenEnded)
                    {
                        return string.Empty;
                    }
                    return "0";
                }
                if (answerDataObject != null)
                {
                    return "1";
                }
                return "0";
            }
            string str = string.Empty;
            List<ItemAnswer> list = base.GetAnalysisData().ListItemResponseAnswers(responseID, data.ItemId);
            for (int i = 0; i < list.Count; i++)
            {
                if (i > 0)
                {
                    str = str + ", ";
                }
                str = str + this.GetAnswerText(list[i]);
            }
            return str;
        }

        protected virtual string GetAnswerText(ItemAnswer answerDataObject)
        {
            if (!answerDataObject.OptionId.HasValue)
            {
                return answerDataObject.AnswerText;
            }
            if (this._naOptions.Contains(answerDataObject.OptionId.Value))
            {
                return "n/a";
            }
            if (answerDataObject.IsOther)
            {
                return (answerDataObject.AnswerText ?? string.Empty);
            }
            string optionText = this.GetOptionText(answerDataObject.ItemId, answerDataObject.OptionId.Value);
            if (Utilities.IsNullOrEmpty(optionText))
            {
                return ("Option " + answerDataObject.OptionId);
            }
            return optionText;
        }

        public virtual List<string> GetColumnNames(string progressKey, string languageCode, int startProgress, int endProgress)
        {
            this.EnsureColumnsAdded(progressKey, languageCode, startProgress, endProgress);
            return new List<string>(this._columnNames);
        }

        public virtual List<string> GetRowAnswers(long responseID)
        {
            int count = this._columnNames.Count;
            List<string> list = new List<string>(this._columnNames.Count);
            for (int i = 0; i < count; i++)
            {
                list.Add(this.GetAnswerColumnValue(responseID, i));
            }
            return list;
        }

        public virtual int ColumnCount
        {
            get
            {
                return this._columnNames.Count;
            }
        }

        public abstract bool IncludeHidden { get; }

        protected abstract bool IncludeOpenEnded { get; }

        protected abstract bool MergeSelectMany { get; }

        public override bool TemplatesValidated
        {
            get
            {
                return true;
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct ColumnData
        {
            public int ItemId;
            public int? OptionId;
        }
    }
}

