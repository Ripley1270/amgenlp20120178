﻿namespace Checkbox.Forms.Logic
{
    using Prezza.Framework.Data;
    using System;
    using System.Collections.Generic;
    using System.Data;

    internal class ExpressionBuilder
    {
        public static List<Type> GetComparableOperandTypes(Operand left, LogicalOperator operation)
        {
            List<Type> list = new List<Type>();
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Rules_ComparableOperands");
            storedProcCommandWrapper.AddInParameter("LeftOperandTypeName", DbType.String, left.GetType().FullName);
            storedProcCommandWrapper.AddInParameter("LeftOperandTypeAssembly", DbType.String, left.GetType().Assembly.GetName().Name);
            storedProcCommandWrapper.AddInParameter("LogicalOperation", DbType.String, operation.ToString());
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    try
                    {
                        while (reader.Read())
                        {
                            string str = (string) reader[0];
                            string str2 = (string) reader[1];
                            list.Add(Type.GetType(str + "," + str2));
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    return list;
                }
                finally
                {
                    reader.Close();
                }
            }
            return list;
        }

        public static List<Type> GetSupportedOperandTypes(Type subject)
        {
            List<Type> list = new List<Type>();
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Rules_SupportedOperands");
            storedProcCommandWrapper.AddInParameter("TypeName", DbType.String, subject.FullName);
            storedProcCommandWrapper.AddInParameter("TypeAssembly", DbType.String, subject.Assembly.GetName().Name);
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    try
                    {
                        while (reader.Read())
                        {
                            string str = (string) reader[0];
                            string str2 = (string) reader[1];
                            list.Add(Type.GetType(str + "," + str2));
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    return list;
                }
                finally
                {
                    reader.Close();
                }
            }
            return list;
        }
    }
}

