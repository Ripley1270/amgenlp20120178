﻿namespace Checkbox.Forms.Items
{
    using System;
    using System.Runtime.CompilerServices;

    public interface IAnswerable
    {
        event EventHandler AnswerChanged;

        void DeleteAnswers();
        string GetAnswer();
        void SetAnswer(object answer);

        bool HasAnswer { get; }
    }
}

