﻿namespace Checkbox.Security.Data
{
    using Checkbox.Security;
    using Checkbox.Users;
    using Prezza.Framework.Data;
    using Prezza.Framework.Data.QueryCriteria;
    using Prezza.Framework.Data.QueryParameters;
    using Prezza.Framework.Security;
    using Prezza.Framework.Security.Principal;
    using System;

    internal static class SelectAvailableIdentitiesNotInAclQuery
    {
        public static SelectQuery GetQuery(IAccessControlList acl, ExtendedPrincipal currentPrincipal, string[] aclPermissions, string[] rolePermissions)
        {
            SelectQuery query = new SelectQuery("ckbx_Credential");
            query.AddAllParameter("ckbx_Credential");
            SelectQuery subQuery = SelectIdentitiesInAclQuery.GetQuery(acl, PermissionJoin.All, aclPermissions);
            SelectQuery query3 = SelectRolesWithPermissionQuery.GetQuery(PermissionJoin.Any, rolePermissions);
            query.AddCriterion(new QueryCriterion(new SelectParameter("UniqueIdentifier", string.Empty, "ckbx_Credential"), CriteriaOperator.NotIn, new SubqueryParameter(subQuery)));
            query.AddCriterion(new QueryCriterion(new SelectParameter("UniqueIdentifier", string.Empty, "ckbx_Credential"), CriteriaOperator.In, new SubqueryParameter(query3)));
            SelectQuery query4 = new SelectQuery("ckbx_Credential");
            query4.AddParameter("UniqueIdentifier", string.Empty, "ckbx_Credential");
            if (!currentPrincipal.IsInRole("System Administrator") && !AuthorizationFactory.GetAuthorizationProvider().Authorize(currentPrincipal, Group.Everyone, "Group.View"))
            {
                query4.AddSelectDistinctCriterion();
                query4.AddTableJoin("ckbx_GroupMembers", QueryJoinType.Inner, "MemberUniqueIdentifier", "ckbx_Credential", "UniqueIdentifier");
                SelectQuery query5 = QueryFactory.GetSelectEntitiesWithPermissionQuery("ckbx_Group", "GroupID", "AclID", currentPrincipal.AclTypeIdentifier, currentPrincipal.AclEntryIdentifier, "DefaultPolicy", PermissionJoin.Any, new string[] { "Group.View" });
                query5.SelectParameters = null;
                query5.AddParameter("GroupID", string.Empty, "ckbx_Group");
                query4.AddCriterion(new QueryCriterion(new SelectParameter("GroupID", string.Empty, "ckbx_GroupMembers"), CriteriaOperator.In, new SubqueryParameter(query5)));
            }
            query.AddCriterion(new QueryCriterion(new SelectParameter("UniqueIdentifier", string.Empty, "ckbx_Credential"), CriteriaOperator.In, new SubqueryParameter(query4)));
            query.AddSortField(new SortOrder("UniqueIdentifier"));
            return query;
        }
    }
}

