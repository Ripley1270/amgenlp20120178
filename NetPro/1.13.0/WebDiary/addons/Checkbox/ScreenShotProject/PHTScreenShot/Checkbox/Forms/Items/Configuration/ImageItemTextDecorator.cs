﻿namespace Checkbox.Forms.Items.Configuration
{
    using Prezza.Framework.Common;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class ImageItemTextDecorator : LocalizableResponseItemTextDecorator
    {
        private string _altText;
        private bool _altTextModified;

        public ImageItemTextDecorator(ImageItemData itemData, string language) : base(itemData, language)
        {
            this._altText = string.Empty;
            this._altTextModified = false;
        }

        protected override void CopyLocalizedText(ItemData data)
        {
            base.CopyLocalizedText(data);
            ArgumentValidation.CheckExpectedType(data, typeof(ImageItemData));
            string alternateTextID = ((ImageItemData) data).AlternateTextID;
            if ((alternateTextID != null) && (alternateTextID.Trim() != string.Empty))
            {
                Dictionary<string, string> allTexts = this.GetAllTexts(alternateTextID);
                foreach (string str2 in allTexts.Keys)
                {
                    this.SetText(this.Data.AlternateTextID, allTexts[str2], str2);
                }
            }
        }

        protected override void SetLocalizedTexts()
        {
            if (this.Data.AlternateTextID != string.Empty)
            {
                this.SetText(this.Data.AlternateTextID, this.AlternateText);
            }
        }

        public string AlternateText
        {
            get
            {
                if ((this.Data.AlternateTextID != string.Empty) && !this._altTextModified)
                {
                    return this.GetText(this.Data.AlternateTextID);
                }
                return this._altText;
            }
            set
            {
                this._altText = value;
                this._altTextModified = true;
            }
        }

        public ImageItemData Data
        {
            get
            {
                return (ImageItemData) base.Data;
            }
        }
    }
}

