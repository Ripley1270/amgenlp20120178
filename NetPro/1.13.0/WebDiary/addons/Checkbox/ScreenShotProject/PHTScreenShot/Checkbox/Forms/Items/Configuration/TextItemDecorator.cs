﻿namespace Checkbox.Forms.Items.Configuration
{
    using Prezza.Framework.Common;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class TextItemDecorator : LabelledItemTextDecorator
    {
        private string _defaultText;
        private bool _defaultTextModified;

        public TextItemDecorator(TextItemData data, string language) : base(data, language)
        {
            this._defaultText = string.Empty;
            this._defaultTextModified = false;
        }

        protected override void CopyLocalizedText(ItemData data)
        {
            base.CopyLocalizedText(data);
            ArgumentValidation.CheckExpectedType(data, typeof(TextItemData));
            string defaultTextID = ((TextItemData) data).DefaultTextID;
            if ((defaultTextID != null) && (defaultTextID != string.Empty))
            {
                Dictionary<string, string> allTexts = this.GetAllTexts(defaultTextID);
                if (allTexts != null)
                {
                    foreach (string str2 in allTexts.Keys)
                    {
                        this.SetText(this.Data.DefaultTextID, allTexts[str2], str2);
                    }
                }
            }
        }

        protected override void SetLocalizedTexts()
        {
            base.SetLocalizedTexts();
            if (this._defaultTextModified && (this.Data.DefaultTextID != string.Empty))
            {
                this.SetText(this.Data.DefaultTextID, this._defaultText);
            }
        }

        public TextItemData Data
        {
            get
            {
                return (TextItemData) base.Data;
            }
        }

        public string DefaultText
        {
            get
            {
                if (!this._defaultTextModified)
                {
                    return this.GetText(this.Data.DefaultTextID);
                }
                return this._defaultText;
            }
            set
            {
                this._defaultText = value;
                this._defaultTextModified = true;
            }
        }
    }
}

