﻿namespace Checkbox.Forms.Items
{
    using Checkbox.Forms.Items.Configuration;
    using Prezza.Framework.Common;
    using System;

    [Serializable]
    public class MultiLineTextBoxItem : TextItem
    {
        public override void Configure(ItemData configuration, string languageCode)
        {
            ArgumentValidation.CheckExpectedType(configuration, typeof(MultiLineTextItemData));
            base.Configure(configuration, languageCode);
            base.Format = AnswerFormat.None;
            base.MaxLength = null;
        }
    }
}

