﻿namespace Checkbox.Forms.Items.UI
{
    using System;

    [Serializable]
    public abstract class MatrixRatingScale : RatingScale
    {
        protected MatrixRatingScale()
        {
        }

        public override Layout LayoutDirection
        {
            get
            {
                return Layout.Horizontal;
            }
            set
            {
            }
        }

        public override bool ShowNumberLabels
        {
            get
            {
                return false;
            }
            set
            {
            }
        }
    }
}

