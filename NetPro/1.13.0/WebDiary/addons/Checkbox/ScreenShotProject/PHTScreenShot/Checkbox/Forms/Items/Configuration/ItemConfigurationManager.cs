﻿namespace Checkbox.Forms.Items.Configuration
{
    using Checkbox.Common;
    using Checkbox.Forms.Data;
    using Checkbox.Forms.Items.UI;
    using Checkbox.Globalization.Text;
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Reflection;
    using System.Xml.Serialization;

    public static class ItemConfigurationManager
    {
        private static readonly Hashtable _typeAssembliesByID;
        private static readonly Hashtable _typeAssembliesByName;
        private static readonly Hashtable _typeClassesByID;
        private static readonly Hashtable _typeClassesByName;
        private static readonly Hashtable _typeIDsByName;
        private static readonly Hashtable _typeNamesByID;

        static ItemConfigurationManager()
        {
            lock (typeof(ItemConfigurationManager))
            {
                _typeClassesByName = new Hashtable();
                _typeAssembliesByName = new Hashtable();
                _typeIDsByName = new Hashtable();
                _typeClassesByID = new Hashtable();
                _typeAssembliesByID = new Hashtable();
                _typeNamesByID = new Hashtable();
            }
        }

        public static SelectItemData ChangeSelectItemType(SelectItemData existingItem, string newItemName)
        {
            Dictionary<string, string> allTexts = TextManager.GetAllTexts(existingItem.TextID);
            Dictionary<string, string> dictionary2 = TextManager.GetAllTexts(existingItem.SubTextID);
            Dictionary<string, string> dictionary3 = TextManager.GetAllTexts(existingItem.OtherTextID);
            SelectItemData newItem = CreateConfigurationData(newItemName) as SelectItemData;
            if (newItem == null)
            {
                return null;
            }
            string defaultAppearanceCodeForType = AppearanceDataManager.GetDefaultAppearanceCodeForType(newItem.ItemTypeID);
            newItem.IsRequired = existingItem.IsRequired;
            newItem.AllowOther = existingItem.AllowOther;
            newItem.Randomize = existingItem.Randomize;
            newItem.Save();
            UpdateSelectItemID(existingItem, newItem);
            SelectLayout appearanceDataForItem = AppearanceDataManager.GetAppearanceDataForItem(existingItem.ID.Value) as SelectLayout;
            if (appearanceDataForItem != null)
            {
                AppearanceDataManager.UpdateAppearanceCode(appearanceDataForItem.ID.Value, defaultAppearanceCodeForType);
                SelectLayout appearanceCopy = AppearanceDataManager.GetAppearanceDataForItem(newItem.ID.Value) as SelectLayout;
                if (appearanceCopy != null)
                {
                    appearanceDataForItem.CopyTo(appearanceCopy);
                    appearanceCopy.Save(newItem.ID.Value);
                }
            }
            newItem = GetConfigurationData(existingItem.ID.Value) as SelectItemData;
            if (newItem == null)
            {
                return null;
            }
            foreach (string str2 in allTexts.Keys)
            {
                TextManager.SetText(newItem.TextID, str2, allTexts[str2]);
            }
            foreach (string str3 in dictionary2.Keys)
            {
                TextManager.SetText(newItem.SubTextID, str3, dictionary2[str3]);
            }
            foreach (string str4 in dictionary3.Keys)
            {
                TextManager.SetText(newItem.OtherTextID, str4, dictionary3[str4]);
            }
            return newItem;
        }

        public static bool CheckItemDataUpdated(int itemId, DateTime? referenceDate)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_CheckUpdated");
            storedProcCommandWrapper.AddInParameter("ItemId", DbType.Int32, itemId);
            storedProcCommandWrapper.AddInParameter("ReferenceDate", DbType.DateTime, referenceDate);
            storedProcCommandWrapper.AddOutParameter("Updated", DbType.Boolean, 4);
            database.ExecuteNonQuery(storedProcCommandWrapper);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("Updated");
            if ((parameterValue != null) && (parameterValue != DBNull.Value))
            {
                return (bool) parameterValue;
            }
            return true;
        }

        public static ItemData CopyItemData(ItemData itemData)
        {
            MemoryStream stream = new MemoryStream();
            new XmlSerializer(itemData.GetType()).Serialize((Stream) stream, itemData);
            stream.Seek(0L, SeekOrigin.Begin);
            DataSet ds = new DataSet();
            ds.ReadXml(stream, XmlReadMode.ReadSchema);
            return ImportItem(ds);
        }

        public static ItemData CreateConfigurationData(int itemTypeID)
        {
            string str = null;
            string str2 = null;
            string str3 = null;
            if (_typeClassesByID.ContainsKey(itemTypeID))
            {
                str = (string) _typeClassesByID[itemTypeID];
                str2 = (string) _typeAssembliesByID[itemTypeID];
                str3 = (string) _typeNamesByID[itemTypeID];
            }
            else
            {
                Database database = DatabaseFactory.CreateDatabase();
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemType_Get");
                storedProcCommandWrapper.AddInParameter("ItemTypeID", DbType.Int32, itemTypeID);
                using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
                {
                    try
                    {
                        while (reader.Read())
                        {
                            str2 = (string) reader["ItemDataAssemblyName"];
                            str = (string) reader["ItemDataClassName"];
                            str3 = (string) reader["ItemName"];
                        }
                    }
                    finally
                    {
                        reader.Close();
                        if (((str != null) && (str2 != null)) && (str3 != null))
                        {
                            lock (_typeClassesByID.SyncRoot)
                            {
                                _typeClassesByID[itemTypeID] = str;
                                _typeAssembliesByID[itemTypeID] = str2;
                                _typeNamesByID[itemTypeID] = str3;
                            }
                        }
                    }
                }
            }
            ItemData data = new ItemConfigurationFactory().CreateItemData(str + ", " + str2);
            data.ItemTypeID = itemTypeID;
            data.ItemTypeName = str3;
            return data;
        }

        public static ItemData CreateConfigurationData(string itemTypeName)
        {
            string str = null;
            string str2 = null;
            int num = -1;
            if (_typeClassesByName.ContainsKey(itemTypeName.ToLower()))
            {
                str = (string) _typeClassesByName[itemTypeName.ToLower()];
                str2 = (string) _typeAssembliesByName[itemTypeName.ToLower()];
                num = (int) _typeIDsByName[itemTypeName.ToLower()];
            }
            else
            {
                Database database = DatabaseFactory.CreateDatabase();
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemType_GetByName");
                storedProcCommandWrapper.AddInParameter("ItemName", DbType.String, itemTypeName);
                using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
                {
                    try
                    {
                        while (reader.Read())
                        {
                            str2 = (string) reader["ItemDataAssemblyName"];
                            str = (string) reader["ItemDataClassName"];
                            num = (int) reader["ItemTypeID"];
                        }
                    }
                    finally
                    {
                        reader.Close();
                        if ((str != null) && (str2 != null))
                        {
                            lock (_typeClassesByName.SyncRoot)
                            {
                                _typeClassesByName[itemTypeName.ToLower()] = str;
                                _typeAssembliesByName[itemTypeName.ToLower()] = str2;
                                _typeIDsByName[itemTypeName.ToLower()] = num;
                            }
                        }
                    }
                }
            }
            ItemData data = new ItemConfigurationFactory().CreateItemData(str + ", " + str2);
            data.ItemTypeID = num;
            data.ItemTypeName = itemTypeName;
            return data;
        }

        public static ItemData GetConfigurationData(int identity)
        {
            DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_Item_GetTypeInfo").AddInParameter("ItemID", DbType.Int32, identity);
            return GetConfigurationData(GetItemTypeName(identity), identity);
        }

        public static ItemData GetConfigurationData(int identity, DataSet ds)
        {
            string itemName = null;
            if (((ds != null) && ds.Tables.Contains("Items")) && ds.Tables["Items"].Columns.Contains("ItemName"))
            {
                itemName = DbUtility.GetValueFromDataTable<string>(ds.Tables["Items"], "ItemName", "ItemID = " + identity, null, null);
            }
            if (itemName == null)
            {
                itemName = GetItemTypeName(identity);
            }
            return GetConfigurationData(itemName, identity, ds);
        }

        public static ItemData GetConfigurationData(string itemType, int identity)
        {
            return GetConfigurationData(itemType, identity, null);
        }

        public static ItemData GetConfigurationData(string itemName, int identity, DataSet ds)
        {
            if ((itemName == null) || (itemName.Trim() == string.Empty))
            {
                return null;
            }
            ItemData data = CreateConfigurationData(itemName);
            if (data != null)
            {
                data.ID = new int?(identity);
                if (ds != null)
                {
                    data.Load(ds);
                    return data;
                }
                data.Load();
            }
            return data;
        }

        private static IItemImportHandler GetImportHandler(string itemTypeName)
        {
            object obj2;
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_GetImportHandlerForType");
            storedProcCommandWrapper.AddInParameter("ItemTypeName", DbType.String, itemTypeName);
            string str = string.Empty;
            string str2 = string.Empty;
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    if (reader.Read())
                    {
                        str = (string) reader["HandlerTypeName"];
                        str2 = (string) reader["HandlerTypeAssembly"];
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            if ((str.Trim() == string.Empty) || (str2.Trim() == string.Empty))
            {
                return new DefaultItemImportHandler();
            }
            Type c = Type.GetType(str + "," + str2, true, false);
            if (!typeof(IItemImportHandler).IsAssignableFrom(c))
            {
                throw new Exception("Type mismatch between IItemImportHandler type [" + typeof(IItemImportHandler).AssemblyQualifiedName + "] and requested type [" + c.AssemblyQualifiedName);
            }
            ConstructorInfo constructor = c.GetConstructor(new Type[0]);
            if (constructor == null)
            {
                throw new Exception("ItemImportHandler does not have a constructor: " + c.FullName);
            }
            try
            {
                obj2 = constructor.Invoke(null);
            }
            catch (MethodAccessException exception)
            {
                throw new Exception(exception.Message, exception);
            }
            catch (TargetInvocationException exception2)
            {
                throw new Exception(exception2.Message, exception2);
            }
            catch (TargetParameterCountException exception3)
            {
                throw new Exception(exception3.Message, exception3);
            }
            return (IItemImportHandler) obj2;
        }

        public static DataTable GetItemCategoryList()
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemType_ListCategories");
            DataSet set = database.ExecuteDataSet(storedProcCommandWrapper);
            if ((set != null) && (set.Tables.Count > 0))
            {
                return set.Tables[0];
            }
            return null;
        }

        public static string GetItemOptionText(ItemData itemData, ListOptionData optionData, string languageCode, int? maxLength, bool preferAlias)
        {
            if (preferAlias && Utilities.IsNotNullOrEmpty(optionData.Alias))
            {
                return Utilities.TruncateText(optionData.Alias, maxLength);
            }
            ItemTextDecorator decorator = itemData.CreateTextDecorator(languageCode);
            decorator.AddAlternateLanguages(new List<string>(TextManager.SurveyLanguages));
            string optionText = string.Empty;
            if (decorator is SelectItemTextDecorator)
            {
                optionText = ((SelectItemTextDecorator) decorator).GetOptionText(optionData.Position);
            }
            if (Utilities.IsNullOrEmpty(optionText))
            {
                optionText = optionData.Alias;
            }
            if ((Utilities.IsNullOrEmpty(optionText) && (itemData is RatingScaleItemData)) && optionData.IsOther)
            {
                optionText = TextManager.GetText("/controlText/ratingScale/notApplicableDefault", languageCode);
                if (Utilities.IsNullOrEmpty(optionText))
                {
                    optionText = "n/a";
                }
            }
            if ((Utilities.IsNullOrEmpty(optionText) && (itemData is SelectItemData)) && optionData.IsOther)
            {
                optionText = TextManager.GetText("/common/otherTextDefault", languageCode);
                if (Utilities.IsNullOrEmpty(optionText))
                {
                    optionText = "Other";
                }
            }
            if (Utilities.IsNullOrEmpty(optionText))
            {
                optionText = optionData.OptionID.ToString();
            }
            return Utilities.TruncateText(optionText, maxLength);
        }

        public static string GetItemOptionText(int itemId, int optionId, string languageCode, int? maxLength, bool preferAlias)
        {
            ItemData configurationData = GetConfigurationData(itemId);
            if ((configurationData != null) && (configurationData is SelectItemData))
            {
                ListOptionData option = ((SelectItemData) configurationData).GetOption(optionId);
                if (option != null)
                {
                    return GetItemOptionText(configurationData, option, languageCode, maxLength, preferAlias);
                }
            }
            return string.Empty;
        }

        public static int? GetItemParent(int childItemId)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_GetMatrixParentId");
            storedProcCommandWrapper.AddInParameter("ChildItemId", DbType.Int32, childItemId);
            int? nullable = null;
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    if (reader.Read())
                    {
                        nullable = DbUtility.GetValueFromDataReader<int?>(reader, "MatrixId", null);
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return nullable;
        }

        public static string GetItemText(int itemId, string languageCode, int? maxLength, bool preferAlias, bool lookupParentText)
        {
            return GetItemText(itemId, languageCode, maxLength, preferAlias, lookupParentText ? GetItemParent(itemId) : null);
        }

        public static string GetItemText(int itemId, string languageCode, int? maxLength, bool preferAlias, int? parentItemId)
        {
            if (parentItemId.HasValue)
            {
                SurveyMetaDataProxy.GetItemData(parentItemId.Value, false);
            }
            return Utilities.TruncateText(SurveyMetaDataProxy.GetItemText(itemId, languageCode, preferAlias, parentItemId.HasValue), maxLength);
        }

        public static DataTable GetItemTypeList()
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemType_List");
            DataSet set = database.ExecuteDataSet(storedProcCommandWrapper);
            if ((set != null) && (set.Tables.Count > 0))
            {
                return set.Tables[0];
            }
            return null;
        }

        public static string GetItemTypeName(int itemId)
        {
            string str = string.Empty;
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Item_GetTypeInfo");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, itemId);
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    if (reader.Read())
                    {
                        str = (string) reader["ItemName"];
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return str;
        }

        private static string GetOptionText(ListOptionData option, SelectItemTextDecorator decorator, int? maxLength)
        {
            string alias = option.IsOther ? decorator.OtherText : decorator.GetOptionText(option.Position);
            if (Utilities.IsNullOrEmpty(alias))
            {
                alias = option.Alias;
            }
            if (Utilities.IsNullOrEmpty(alias))
            {
                alias = option.Position.ToString();
            }
            return Utilities.TruncateText(alias, maxLength);
        }

        public static Dictionary<int, string> GetOptionTexts(SelectItemData itemData, string languageCode, int? maxLength)
        {
            Dictionary<int, string> dictionary = new Dictionary<int, string>();
            SelectItemTextDecorator decorator = itemData.CreateTextDecorator(languageCode) as SelectItemTextDecorator;
            if (decorator != null)
            {
                foreach (ListOptionData data in itemData.Options)
                {
                    if (itemData is RatingScaleItemData)
                    {
                        dictionary[data.OptionID] = data.IsOther ? TextManager.GetText(((RatingScaleItemData) itemData).NotApplicableTextID, languageCode) : data.Points.ToString();
                    }
                    else
                    {
                        dictionary[data.OptionID] = GetOptionText(data, decorator, maxLength);
                    }
                }
            }
            return dictionary;
        }

        public static ItemData ImportItem(DataSet ds)
        {
            try
            {
                if ((ds.Tables.Contains("Items") && (ds.Tables["Items"].Rows.Count > 0)) && (ds.Tables["Items"].Rows[0]["ItemID"] != DBNull.Value))
                {
                    return ImportItem((int) ds.Tables["Items"].Rows[0]["ItemID"], ds);
                }
            }
            catch (Exception exception)
            {
                ExceptionPolicy.HandleException(exception, "BusinessPublic");
            }
            return null;
        }

        public static ItemData ImportItem(int itemID, DataSet ds)
        {
            return ImportItem(itemID, ds, false);
        }

        public static ItemData ImportItem(int itemID, DataSet ds, bool doNotNegateRelationalIDs)
        {
            try
            {
                if (ds.Tables.Contains("Items"))
                {
                    DataRow[] rowArray = ds.Tables["Items"].Select("ItemID = " + itemID, null, DataViewRowState.CurrentRows);
                    if ((rowArray.Length > 0) && (rowArray[0]["ItemName"] != DBNull.Value))
                    {
                        IItemImportHandler importHandler = GetImportHandler((string) rowArray[0]["ItemName"]);
                        if (importHandler != null)
                        {
                            return importHandler.ImportItem(itemID, ds, doNotNegateRelationalIDs);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                ExceptionPolicy.HandleException(exception, "BusinessPublic");
            }
            return null;
        }

        private static void UpdateSelectItemID(ItemData existingItem, ItemData newItem)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Item_UpdateType");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, existingItem.ID.Value);
            storedProcCommandWrapper.AddInParameter("ItemTypeID", DbType.Int32, newItem.ItemTypeID);
            storedProcCommandWrapper.AddInParameter("ModifiedDate", DbType.DateTime, DateTime.Now);
            database.ExecuteNonQuery(storedProcCommandWrapper);
            DBCommandWrapper command = database.GetStoredProcCommandWrapper("ckbx_ItemData_UpdateSelectItemID");
            command.AddInParameter("ExistingItemID", DbType.Int32, existingItem.ID.Value);
            command.AddInParameter("ExistingItemName", DbType.String, existingItem.ItemTypeName);
            command.AddInParameter("NewItemID", DbType.Int32, newItem.ID.Value);
            command.AddInParameter("NewItemName", DbType.String, newItem.ItemTypeName);
            database.ExecuteNonQuery(command);
        }
    }
}

