﻿namespace Checkbox.Forms.Piping.Tokens
{
    using System;

    [Serializable]
    public class ResponseTemplateToken : Token
    {
        public ResponseTemplateToken(string token) : base(token, TokenType.ResponseTemplate)
        {
        }
    }
}

