﻿namespace Checkbox.Analytics.Filters.Configuration
{
    using Checkbox.Analytics.Filters;
    using Checkbox.Common;
    using Checkbox.Forms.Logic;
    using Checkbox.Globalization.Text;
    using Prezza.Framework.Data;
    using System;
    using System.Data;
    using System.Runtime.CompilerServices;
    using System.Text;

    [Serializable]
    public abstract class FilterData : PersistedDomainObject, IEquatable<FilterData>
    {
        protected FilterData()
        {
        }

        protected override void Create(IDbTransaction t)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Filter_Insert");
            storedProcCommandWrapper.AddInParameter("Operator", DbType.String, this.Operator.ToString());
            storedProcCommandWrapper.AddInParameter("Value", DbType.String, this.Value);
            storedProcCommandWrapper.AddInParameter("FilterTypeName", DbType.String, this.FilterTypeName);
            storedProcCommandWrapper.AddOutParameter("FilterID", DbType.Int32, 4);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("FilterID");
            if ((parameterValue == null) || (parameterValue == DBNull.Value))
            {
                throw new Exception("Unable to save filter.");
            }
            base.ID = new int?((int) parameterValue);
        }

        protected override void CreateDataRelationsInternal(DataSet ds)
        {
            base.CreateDataRelationsInternal(ds);
            if ((ds.Relations.Contains("FilterMap_Filters") && ds.Tables.Contains("FilterMap")) && ds.Tables.Contains(this.DataTableName))
            {
                DataRelation relation = new DataRelation("FilterMap_Filters", ds.Tables["FilterMap"].Columns["FilterID"], ds.Tables[this.DataTableName].Columns[this.IdentityColumnName]);
                ds.Tables[this.DataTableName].Constraints.Add(new ForeignKeyConstraint(ds.Tables["FilterMap"].Columns["FilterID"], ds.Tables[this.DataTableName].Columns[this.IdentityColumnName]));
                ds.Relations.Add(relation);
            }
        }

        public virtual Checkbox.Analytics.Filters.Filter CreateFilter(string languageCode)
        {
            Checkbox.Analytics.Filters.Filter filter = this.CreateFilterObject();
            if (filter != null)
            {
                filter.Configure(this, languageCode);
            }
            return filter;
        }

        protected abstract Checkbox.Analytics.Filters.Filter CreateFilterObject();
        public override void Delete(IDbTransaction t)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Filter_Delete");
            storedProcCommandWrapper.AddInParameter("FilterID", DbType.Int32, base.ID.Value);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        public bool Equals(FilterData other)
        {
            return (base.ID == other.ID);
        }

        protected abstract string GetFilterLeftOperandText(string languageCode);
        protected virtual string GetFilterRightOperandText(string languageCode)
        {
            if (this.Value != null)
            {
                return this.Value.ToString();
            }
            return string.Empty;
        }

        public void Initialize(int filterId)
        {
            base.ID = new int?(filterId);
        }

        public void Initialize(string filterTypeName)
        {
            this.FilterTypeName = filterTypeName;
        }

        protected override void Load(DataRow data)
        {
            base.Load(data);
            this.FilterTypeName = DbUtility.GetValueFromDataRow<string>(data, "FilterTypeName", string.Empty);
            if (DbUtility.GetValueFromDataRow<string>(data, "Operator", null) != null)
            {
                this.Operator = (LogicalOperator) Enum.Parse(typeof(LogicalOperator), (string) data["Operator"]);
            }
            this.Value = DbUtility.GetValueFromDataRow<object>(data, "Value", null);
        }

        public virtual string ToString(string languageCode)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(this.GetFilterLeftOperandText(languageCode));
            builder.Append(" ");
            builder.Append(TextManager.GetText("/enum/logicalOperator/" + this.Operator, languageCode, this.Operator.ToString(), TextManager.ApplicationLanguages));
            builder.Append(" ");
            builder.Append(this.GetFilterRightOperandText(languageCode));
            return builder.ToString();
        }

        protected override void Update(IDbTransaction t)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Filter_Update");
            storedProcCommandWrapper.AddInParameter("FilterID", DbType.Int32, base.ID.Value);
            storedProcCommandWrapper.AddInParameter("Operator", DbType.String, this.Operator.ToString());
            if (this.Value != null)
            {
                storedProcCommandWrapper.AddInParameter("Value", DbType.String, this.Value.ToString());
            }
            else
            {
                storedProcCommandWrapper.AddInParameter("Value", DbType.String, null);
            }
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        public string FilterTypeName { get; private set; }

        public virtual LogicalOperator Operator { get; set; }

        public virtual object Value { get; set; }
    }
}

