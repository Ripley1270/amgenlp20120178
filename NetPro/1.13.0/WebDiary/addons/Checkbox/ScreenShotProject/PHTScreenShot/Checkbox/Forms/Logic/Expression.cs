﻿namespace Checkbox.Forms.Logic
{
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class Expression
    {
        private readonly Operand _left;
        private readonly LogicalOperator _operator;
        private readonly Operand _right;

        public Expression(Operand left, Operand right, LogicalOperator operation)
        {
            this._left = left;
            this._right = right;
            this._operator = operation;
        }

        internal virtual bool Evaluate(RuleEventTrigger trigger)
        {
            return OperandComparer.Compare(this._left, this._operator, this._right, trigger);
        }

        public int? Identity { get; set; }

        public CompositeExpression Parent { get; set; }
    }
}

