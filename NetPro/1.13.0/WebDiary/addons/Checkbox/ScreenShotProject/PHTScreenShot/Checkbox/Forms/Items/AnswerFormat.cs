﻿namespace Checkbox.Forms.Items
{
    using System;

    [Serializable]
    public enum AnswerFormat
    {
        Alpha = 11,
        AlphaNumeric = 0x11,
        Custom = 0x12,
        Date = 14,
        Date_ROTW = 0x10,
        Date_USA = 15,
        Decimal = 5,
        Email = 2,
        Integer = 3,
        Lowercase = 12,
        Money = 6,
        None = 1,
        Numeric = 4,
        Phone = 7,
        Postal = 10,
        SSN = 8,
        Uppercase = 13,
        URL = 9
    }
}

