﻿namespace Checkbox.Analytics.Items
{
    using Checkbox.Analytics.Computation;
    using Checkbox.Analytics.Data;
    using Checkbox.Analytics.Items.Configuration;
    using Checkbox.Forms.Items.Configuration;
    using Prezza.Framework.Common;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class CrossTabItem : AnalysisItem
    {
        public override void Configure(ItemData itemData, string languageCode)
        {
            base.Configure(itemData, languageCode);
            this.InitializeData(itemData);
        }

        protected override object GeneratePreviewData()
        {
            return this.ProcessData(true);
        }

        protected virtual void InitializeData(ItemData itemData)
        {
            this.XAxisItemIDs = ((CrossTabItemData) itemData).XAxisItemIDs;
            this.YAxisItemIDs = ((CrossTabItemData) itemData).YAxisItemIDs;
        }

        protected override object ProcessData()
        {
            return this.ProcessData(false);
        }

        protected virtual object ProcessData(bool isPreview)
        {
            try
            {
                ItemAnswerAggregator aggregator = new ItemAnswerAggregator();
                CrossTabReportDataCalculator calculator = new CrossTabReportDataCalculator();
                int x = 2;
                foreach (int num2 in this.XAxisItemIDs)
                {
                    List<int> list = isPreview ? this.GetItemOptionIdsForPreview(num2) : this.GetItemOptionIdsForReport(num2);
                    if (list.Count > 0)
                    {
                        aggregator.AddItem(num2, this.GetItemText(num2), this.GetSourceItemTypeName(num2));
                        foreach (int num3 in list)
                        {
                            aggregator.AddItemOption(num2, num3, this.GetOptionText(num2, num3));
                            calculator.SetOptionResultsPosition(num3, new Coordinate(x, 1));
                            x++;
                        }
                    }
                }
                int y = 2;
                foreach (int num5 in this.YAxisItemIDs)
                {
                    List<int> list2 = isPreview ? this.GetItemOptionIdsForPreview(num5) : this.GetItemOptionIdsForReport(num5);
                    if (list2.Count > 0)
                    {
                        aggregator.AddItem(num5, this.GetItemText(num5), this.GetSourceItemTypeName(num5));
                        foreach (int num6 in list2)
                        {
                            aggregator.AddItemOption(num5, num6, this.GetOptionText(num5, num6));
                            aggregator.AddItemOption(num5, num6, this.GetOptionText(num5, num6));
                            calculator.SetOptionResultsPosition(num6, new Coordinate(1, y));
                            y++;
                        }
                    }
                }
                long answerId = 0x3e8L;
                foreach (int num8 in this.YAxisItemIDs)
                {
                    List<ItemAnswer> list3 = isPreview ? this.GetItemPreviewAnswers(num8, new long?(answerId), 0x3e8L) : base.GetItemAnswers(num8);
                    foreach (ItemAnswer answer in list3)
                    {
                        if (answer.AnswerId > answerId)
                        {
                            answerId = answer.AnswerId;
                        }
                        if (answer.OptionId.HasValue)
                        {
                            aggregator.AddAnswer(answer.AnswerId, answer.ResponseId, num8, answer.OptionId);
                        }
                        answerId += 1L;
                    }
                    base.ResponseCounts[num8] = aggregator.GetResponseCount(new int?(num8));
                }
                foreach (int num9 in this.XAxisItemIDs)
                {
                    List<ItemAnswer> list4 = isPreview ? this.GetItemPreviewAnswers(num9, new long?(answerId), 0x3e8L) : base.GetItemAnswers(num9);
                    foreach (ItemAnswer answer2 in list4)
                    {
                        if (answer2.AnswerId > answerId)
                        {
                            answerId = answer2.AnswerId;
                        }
                        if (answer2.OptionId.HasValue)
                        {
                            aggregator.AddAnswer(answer2.AnswerId, answer2.ResponseId, num9, answer2.OptionId);
                        }
                        answerId += 1L;
                    }
                    base.ResponseCounts[num9] = aggregator.GetResponseCount(new int?(num9));
                }
                base.DataProcessed = true;
                return calculator.GetData(aggregator);
            }
            catch (Exception exception)
            {
                ExceptionPolicy.HandleException(exception, "BusinessProtected");
                return null;
            }
        }

        public ReadOnlyCollection<int> XAxisItemIDs { get; private set; }

        public ReadOnlyCollection<int> YAxisItemIDs { get; private set; }
    }
}

