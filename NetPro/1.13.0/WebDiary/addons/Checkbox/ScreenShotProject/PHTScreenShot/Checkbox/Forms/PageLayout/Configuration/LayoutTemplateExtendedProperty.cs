﻿namespace Checkbox.Forms.PageLayout.Configuration
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LayoutTemplateExtendedProperty
    {
        public double? MaxValue { get; set; }

        public double? MinValue { get; set; }

        public string Name { get; set; }

        public List<string> PossibleValues { get; set; }

        public LayoutTemplateExtendedPropertyType Type { get; set; }

        public object Value { get; set; }
    }
}

