﻿namespace Checkbox.Analytics.Filters
{
    using Checkbox.Analytics.Data;
    using Checkbox.Analytics.Filters.Configuration;
    using Checkbox.Forms;
    using System;
    using System.Runtime.InteropServices;

    [Serializable]
    public class ItemFilter : AnswerDataObjectFilter
    {
        private int _itemID;

        public override void Configure(FilterData filterData, string languageCode)
        {
            base.Configure(filterData, languageCode);
            if (filterData is ItemFilterData)
            {
                this._itemID = ((ItemFilterData) filterData).ItemId;
            }
        }

        public override bool EvaluateFilter(ItemAnswer answer, ResponseProperties responseProperties, out bool answerHasValue)
        {
            answerHasValue = answer.ItemId == this.ItemID;
            if (!answerHasValue)
            {
                return !this.ValueRequired;
            }
            if (!answer.OptionId.HasValue)
            {
                return this.CompareValue(answer.AnswerText);
            }
            return this.CompareValue(answer.OptionId.Value);
        }

        public virtual int ItemID
        {
            get
            {
                return this._itemID;
            }
        }
    }
}

