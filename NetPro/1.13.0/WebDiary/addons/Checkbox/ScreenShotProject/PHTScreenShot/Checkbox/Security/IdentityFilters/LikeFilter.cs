﻿namespace Checkbox.Security.IdentityFilters
{
    using System;

    public class LikeFilter : IdentityFilter
    {
        private object propertyValue;

        public LikeFilter(string filterProperty) : base(IdentityFilterType.Like, IdentityFilterPropertyType.IdentityProperty, filterProperty)
        {
        }

        public LikeFilter(string filterProperty, object propertyValue) : base(IdentityFilterType.Like, IdentityFilterPropertyType.IdentityProperty, filterProperty)
        {
            this.propertyValue = propertyValue;
        }

        public LikeFilter(string filterProperty, object propertyValue, IdentityFilterPropertyType filterPropertyType) : base(IdentityFilterType.Like, filterPropertyType, filterProperty)
        {
            this.propertyValue = propertyValue;
        }

        public object PropertyValue
        {
            get
            {
                return this.propertyValue;
            }
        }
    }
}

