﻿namespace Checkbox.Security.IdentityFilters
{
    using System;

    public class NotNullFilter : IdentityFilter
    {
        public NotNullFilter(string filterProperty) : base(IdentityFilterType.IsNotNull, IdentityFilterPropertyType.IdentityProperty, filterProperty)
        {
        }

        public NotNullFilter(string filterProperty, IdentityFilterPropertyType filterPropertyType) : base(IdentityFilterType.IsNotNull, filterPropertyType, filterProperty)
        {
        }
    }
}

