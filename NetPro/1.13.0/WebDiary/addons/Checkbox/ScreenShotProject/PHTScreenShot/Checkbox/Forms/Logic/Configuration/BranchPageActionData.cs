﻿namespace Checkbox.Forms.Logic.Configuration
{
    using Checkbox.Forms;
    using Checkbox.Globalization.Text;
    using Prezza.Framework.Common;
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Data;
    using System.Text;

    [Serializable]
    public class BranchPageActionData : ActionData
    {
        private int? _targetPage;

        public BranchPageActionData(ResponseTemplate context, TemplatePage branchFromPage) : base(context, branchFromPage)
        {
        }

        protected override void Create(IDbTransaction t)
        {
            base.Create(t);
            DataRow row = base.Context.BranchActionTable.NewRow();
            row["ActionID"] = base.ID;
            if (this._targetPage.HasValue)
            {
                row["GoToPageID"] = this._targetPage.Value;
            }
            base.Context.BranchActionTable.Rows.Add(row);
        }

        public override void Delete(IDbTransaction t)
        {
            DataRow[] rowArray = base.Context.BranchActionTable.Select(this.IdentityColumnName + "=" + base.ID.Value);
            if (rowArray.Length > 0)
            {
                rowArray[0].Delete();
            }
            base.Delete(t);
        }

        protected override DataSet GetConcreteConfigurationDataSet()
        {
            if (base.ID <= 0)
            {
                throw new ApplicationException("No DataID specified.");
            }
            try
            {
                Database database = DatabaseFactory.CreateDatabase();
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_BranchAction_GetAction");
                storedProcCommandWrapper.AddInParameter("ActionID", DbType.Int32, base.ID.Value);
                DataSet dataSet = new DataSet();
                database.LoadDataSet(storedProcCommandWrapper, dataSet, new string[] { this.DataTableName });
                return dataSet;
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessProtected"))
                {
                    throw;
                }
                return null;
            }
        }

        protected override void LoadFromDataRow(DataRow data)
        {
            ArgumentValidation.CheckForNullReference(data, "data");
            try
            {
                if (data["GoToPageID"] != DBNull.Value)
                {
                    this._targetPage = new int?((int) data["GoToPageID"]);
                }
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessProtected"))
                {
                    throw;
                }
            }
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            if (base.Context.TemplatePages.IndexOf(base.Context.GetPage(this.GoToPageID.Value)) == (base.Context.TemplatePages.Count - 1))
            {
                builder.Append(TextManager.GetText("/pageBranch/completeSurvey", base.Context.EditLanguage));
            }
            else
            {
                builder.Append(TextManager.GetText("/pageBranch/goToPage", base.Context.EditLanguage));
                builder.Append(" ");
                if (this._targetPage.HasValue)
                {
                    builder.Append(base.Context.GetPage(this._targetPage.Value).Position);
                }
            }
            return builder.ToString();
        }

        protected override void Update(IDbTransaction t)
        {
            base.Update(t);
            DataRow row = base.Context.BranchActionTable.Select(this.IdentityColumnName + "=" + base.ID.Value)[0];
            if (this._targetPage.HasValue)
            {
                row["GoToPageID"] = this._targetPage.Value;
            }
        }

        public override string DataTableName
        {
            get
            {
                return "BranchAction";
            }
        }

        public int? GoToPageID
        {
            get
            {
                return this._targetPage;
            }
            set
            {
                this._targetPage = value;
            }
        }

        public string GoToPagePositionAsString
        {
            get
            {
                if (this.GoToPageID.HasValue && (base.Context != null))
                {
                    TemplatePage page = base.Context.GetPage(this.GoToPageID.Value);
                    if (page != null)
                    {
                        if (page.Position == base.Context.TemplatePages[base.Context.TemplatePages.Count - 1].Position)
                        {
                            return TextManager.GetText("/pageBranch/completeSurvey", base.Context.EditLanguage);
                        }
                        return page.Position.ToString();
                    }
                }
                return string.Empty;
            }
        }
    }
}

