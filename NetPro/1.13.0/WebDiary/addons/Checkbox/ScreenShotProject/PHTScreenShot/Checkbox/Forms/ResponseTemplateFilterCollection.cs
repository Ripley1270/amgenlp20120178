﻿namespace Checkbox.Forms
{
    using Checkbox.Analytics.Filters;
    using Checkbox.Analytics.Filters.Configuration;
    using Prezza.Framework.Data;
    using System;

    [Serializable]
    public class ResponseTemplateFilterCollection : FilterDataCollection
    {
        public override void DeleteFilter(FilterData filter)
        {
            base.DeleteFilter(filter);
            Database db = DatabaseFactory.CreateDatabase();
            db.UpdateDataSet(base.FilterData, this.FilterMapTableName, this.GetInsertFilterMapCommand(db), null, this.GetDeleteFilterMapCommand(db), UpdateBehavior.Standard);
            filter.Delete();
        }

        public override string ParentType
        {
            get
            {
                return "ResponseTemplate";
            }
            set
            {
            }
        }
    }
}

