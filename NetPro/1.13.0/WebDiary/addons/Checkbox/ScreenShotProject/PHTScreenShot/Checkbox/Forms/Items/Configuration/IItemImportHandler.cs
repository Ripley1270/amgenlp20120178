﻿namespace Checkbox.Forms.Items.Configuration
{
    using System;
    using System.Data;

    internal interface IItemImportHandler
    {
        ItemData ImportItem(int itemID, DataSet ds, bool doNotNegateRelationalIDs);
    }
}

