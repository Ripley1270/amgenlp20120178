﻿namespace Checkbox.Messaging.Email
{
    using Checkbox.Messaging.Configuration;
    using Prezza.Framework.Common;
    using Prezza.Framework.Configuration;
    using Prezza.Framework.ExceptionHandling;
    using System;

    internal class EmailProviderFactory : ProviderFactory
    {
        internal EmailProviderFactory(string factoryName) : base(factoryName, typeof(IEmailProvider))
        {
            try
            {
                ArgumentValidation.CheckForEmptyString(factoryName, "factoryName");
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessInternal"))
                {
                    throw;
                }
            }
        }

        internal EmailProviderFactory(string factoryName, MessagingConfiguration config) : base(factoryName, typeof(IEmailProvider), config)
        {
            try
            {
                ArgumentValidation.CheckForEmptyString(factoryName, "factoryName");
                ArgumentValidation.CheckForNullReference(config, "config");
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessInternal"))
                {
                    throw;
                }
            }
        }

        protected override ConfigurationBase GetConfigurationObject(string providerName)
        {
            ArgumentValidation.CheckForEmptyString(providerName, "providerName");
            MessagingConfiguration config = (MessagingConfiguration) base.Config;
            return config.GetEmailProviderConfig(providerName);
        }

        protected override Type GetConfigurationType(string emailProviderName)
        {
            ArgumentValidation.CheckForEmptyString(emailProviderName, "emailProviderName");
            ProviderData configurationObject = (ProviderData) this.GetConfigurationObject(emailProviderName);
            return base.GetType(configurationObject.TypeName);
        }

        protected override string GetDefaultInstanceName()
        {
            MessagingConfiguration config = (MessagingConfiguration) base.Config;
            return config.DefaultEmailProvider;
        }

        internal IEmailProvider GetEmailProvider()
        {
            try
            {
                return (IEmailProvider) base.CreateDefaultInstance();
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessInternal"))
                {
                    throw;
                }
                return null;
            }
        }

        internal IEmailProvider GetEmailProvider(string providerName)
        {
            try
            {
                ArgumentValidation.CheckForEmptyString(providerName, "providerName");
                return (IEmailProvider) base.CreateInstance(providerName);
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessInternal"))
                {
                    throw;
                }
                return null;
            }
        }
    }
}

