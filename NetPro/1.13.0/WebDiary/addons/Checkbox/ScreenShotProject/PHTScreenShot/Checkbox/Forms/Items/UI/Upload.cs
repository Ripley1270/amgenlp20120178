﻿namespace Checkbox.Forms.Items.UI
{
    using System;

    [Serializable]
    public class Upload : LabelledItemAppearanceData
    {
        public override string AppearanceCode
        {
            get
            {
                return "FILE_UPLOAD";
            }
        }
    }
}

