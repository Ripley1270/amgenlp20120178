﻿namespace Checkbox.Security
{
    using Checkbox.Security.Configuration;
    using Prezza.Framework.Common;
    using Prezza.Framework.Configuration;
    using Prezza.Framework.ExceptionHandling;
    using System;

    internal class IdentityProviderFactory : ProviderFactory
    {
        internal IdentityProviderFactory(string factoryName) : base(factoryName, typeof(IIdentityProvider))
        {
            try
            {
                ArgumentValidation.CheckForEmptyString(factoryName, "factoryName");
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessInternal"))
                {
                    throw;
                }
            }
        }

        internal IdentityProviderFactory(string factoryName, SecurityConfiguration config) : base(factoryName, typeof(IIdentityProvider), config)
        {
            try
            {
                ArgumentValidation.CheckForEmptyString(factoryName, "factoryName");
                ArgumentValidation.CheckForNullReference(config, "config");
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessInternal"))
                {
                    throw;
                }
            }
        }

        protected override ConfigurationBase GetConfigurationObject(string providerName)
        {
            ArgumentValidation.CheckForEmptyString(providerName, "providerName");
            SecurityConfiguration config = (SecurityConfiguration) base.Config;
            return config.GetIdentityProviderConfig(providerName);
        }

        protected override Type GetConfigurationType(string identityProviderName)
        {
            ArgumentValidation.CheckForEmptyString(identityProviderName, "identityProviderName");
            ProviderData configurationObject = (ProviderData) this.GetConfigurationObject(identityProviderName);
            return base.GetType(configurationObject.TypeName);
        }

        protected override string GetDefaultInstanceName()
        {
            SecurityConfiguration config = (SecurityConfiguration) base.Config;
            return config.DefaultIdentityProvider;
        }

        internal IIdentityProvider GetIdentityProvider()
        {
            try
            {
                return (IIdentityProvider) base.CreateDefaultInstance();
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessInternal"))
                {
                    throw;
                }
                return null;
            }
        }

        internal IIdentityProvider GetIdentityProvider(string providerName)
        {
            try
            {
                ArgumentValidation.CheckForEmptyString(providerName, "providerName");
                return (IIdentityProvider) base.CreateInstance(providerName);
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessInternal"))
                {
                    throw;
                }
                return null;
            }
        }
    }
}

