﻿namespace Checkbox.Forms.Validation
{
    using Checkbox.Globalization.Text;
    using System;

    public class USPhoneValidator : RegularExpressionValidator
    {
        public USPhoneValidator()
        {
            base._regex = @"^1?[-\. ]?(\(\d{3}\)?[-\. ]?|\d{3}?[-\. ]?)?\d{3}?[-\. ]?\d{4}$";
        }

        public override string GetMessage(string languageCode)
        {
            return TextManager.GetText("/validationMessages/regex/phoneNumber", languageCode);
        }
    }
}

