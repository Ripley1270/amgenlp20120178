﻿namespace Checkbox.Security.Providers
{
    using Checkbox.Common;
    using Checkbox.Security;
    using Checkbox.Security.IdentityFilters;
    using Prezza.Framework.Caching;
    using Prezza.Framework.Caching.Expirations;
    using Prezza.Framework.Configuration;
    using Prezza.Framework.Data;
    using Prezza.Framework.Data.QueryCriteria;
    using Prezza.Framework.Data.QueryParameters;
    using Prezza.Framework.Security.Providers;
    using System;
    using System.Collections;
    using System.Data;
    using System.Security.Principal;

    public class ProfileAndIdentityProvider : DbProfileProvider, IIdentityProvider
    {
        private CacheManager _cacheManager;
        private ProfileAndIdentityProviderData _config;

        private SelectQuery AddPagingAndFilteringToQuery(SelectQuery query, PageFilter pageFilter, IdentityFilterCollection identityFilters, string tableName)
        {
            if ((identityFilters != null) && ((identityFilters.FilterCollections.Length > 0) || (identityFilters.Filters.Length > 0)))
            {
                query.AddCriteriaCollection(IdentityFilterToQueryCriteriaInterpreter.Translate(identityFilters, tableName, tableName));
            }
            if (((pageFilter != null) && (pageFilter.PageNumber > 0)) && (pageFilter.ResultsPerPage > 0))
            {
                query.AddSelectTopCriterion(pageFilter.ResultsPerPage);
                query.CriteriaJoinType = CriteriaJoinType.And;
                if (pageFilter.PageNumber > 1)
                {
                    query.AddCriterion(new QueryCriterion(new SelectParameter(this._config.IdentityNameColumn, string.Empty, tableName), CriteriaOperator.NotIn, CreatePagingSubQueryParameter(query, this._config.IdentityNameColumn, tableName, pageFilter.PageNumber, pageFilter.ResultsPerPage)));
                }
            }
            return query;
        }

        private static SubqueryParameter CreatePagingSubQueryParameter(SelectQuery query, string selectFieldName, string selectFieldTable, int pageNumber, int resultsPerPage)
        {
            int count = (pageNumber - 1) * resultsPerPage;
            SelectQuery subQuery = (SelectQuery) query.Clone();
            subQuery.SelectParameters = null;
            subQuery.AddParameter(selectFieldName, string.Empty, selectFieldTable);
            if (count > 0)
            {
                subQuery.AddSelectTopCriterion(count);
            }
            return new SubqueryParameter(subQuery);
        }

        public IIdentity[] GetIdentities(PageFilter pageFilter, IdentitySortOrder sortOrder, IdentityFilterCollection identityFilters)
        {
            ArrayList list = new ArrayList();
            SelectQuery query = this.GetIdentityQuery(pageFilter, sortOrder, identityFilters);
            string type = base.GetType().ToString();
            Database database = DatabaseFactory.CreateDatabase("DBProfileProvider");
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
            using (IDataReader reader = database.ExecuteReader(sqlStringCommandWrapper))
            {
                try
                {
                    while (reader.Read())
                    {
                        list.Add(new GenericIdentity((string) reader[this._config.IdentityNameColumn], type));
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return (IIdentity[]) list.ToArray(typeof(IIdentity));
        }

        public IIdentity GetIdentity(string uniqueIdentifier)
        {
            if (!string.IsNullOrEmpty(uniqueIdentifier))
            {
                if ((this._config.CachingEnabled && (this._cacheManager != null)) && this._cacheManager.Contains("//IDENTITY/" + uniqueIdentifier))
                {
                    return (IIdentity) this._cacheManager["//IDENTITY/" + uniqueIdentifier];
                }
                SelectQuery query = new SelectQuery(this._config.IdentityTableName);
                query.AddParameter(this._config.IdentityNameColumn, string.Empty, this._config.IdentityTableName);
                query.AddCriterion(new QueryCriterion(new SelectParameter(this._config.IdentityNameColumn), CriteriaOperator.EqualTo, new LiteralParameter("'" + uniqueIdentifier + "'")));
                Database database = DatabaseFactory.CreateDatabase("DBProfileProvider");
                DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
                using (IDataReader reader = database.ExecuteReader(sqlStringCommandWrapper))
                {
                    try
                    {
                        if (reader.Read() && (reader[this._config.IdentityNameColumn] != null))
                        {
                            if (this._config.CachingEnabled && (this._cacheManager != null))
                            {
                                ICacheItemExpiration[] expirations = new ICacheItemExpiration[] { new AbsoluteTime(new TimeSpan(0, 0, this._config.CacheExpiration, 0, 0)) };
                                this._cacheManager.Add("//IDENTITY/" + uniqueIdentifier, new GenericIdentity(uniqueIdentifier), CacheItemPriority.Normal, null, expirations);
                            }
                            return new GenericIdentity(uniqueIdentifier);
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
            }
            return null;
        }

        public int GetIdentityCount(IdentityFilterCollection identityFilters)
        {
            SelectQuery query = this.GetIdentityQuery(null, null, identityFilters);
            query.SelectParameters = null;
            query.AddCountParameter("UniqueIdentifier", string.Empty);
            Database database = DatabaseFactory.CreateDatabase("DBProfileProvider");
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
            using (IDataReader reader = database.ExecuteReader(sqlStringCommandWrapper))
            {
                try
                {
                    if (reader.Read() && (reader[0] != null))
                    {
                        return (int) reader[0];
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return 0;
        }

        private SelectQuery GetIdentityQuery(PageFilter pageFilter, IdentitySortOrder sortOrder, IdentityFilterCollection identityFilters)
        {
            string identityTableName;
            if (IsFilterCollectionIdentityOnly(identityFilters) && ((sortOrder == null) || (sortOrder.PropertyType == IdentityFilterPropertyType.IdentityProperty)))
            {
                identityTableName = this._config.IdentityTableName;
            }
            else
            {
                identityTableName = this._config.IdentityProfileTable;
            }
            SelectQuery query = new SelectQuery(identityTableName);
            query.AddParameter(this._config.IdentityNameColumn, string.Empty, identityTableName);
            if (sortOrder != null)
            {
                query.AddSortField(new SortOrder(identityTableName + "." + sortOrder.SortProperty, sortOrder.SortAscending));
                if (sortOrder.SortProperty.ToLower().IndexOf(this._config.IdentityNameColumn.ToLower()) < 0)
                {
                    query.AddSortField(new SortOrder(identityTableName + "." + this._config.IdentityNameColumn, sortOrder.SortAscending));
                }
            }
            return this.AddPagingAndFilteringToQuery(query, pageFilter, identityFilters, identityTableName);
        }

        public override void Initialize(ConfigurationBase config)
        {
            base.Initialize(config);
            this._config = (ProfileAndIdentityProviderData) config;
            this._cacheManager = CacheFactory.GetCacheManager(this._config.CacheManager);
        }

        private static bool IsFilterCollectionIdentityOnly(IdentityFilterCollection filterCollection)
        {
            if (filterCollection != null)
            {
                foreach (IdentityFilter filter in filterCollection.Filters)
                {
                    if (filter.PropertyType != IdentityFilterPropertyType.IdentityProperty)
                    {
                        return false;
                    }
                }
                foreach (IdentityFilterCollection filters in filterCollection.FilterCollections)
                {
                    if (!IsFilterCollectionIdentityOnly(filters))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public void PreLoadProfilesForTemplateResponses(int responseTemplateId)
        {
            Database database = DatabaseFactory.CreateDatabase("DBProfileProvider");
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Profile_LoadForResponseTemplate");
            storedProcCommandWrapper.AddInParameter("ResponseTemplateId", DbType.Int32, responseTemplateId);
            string uniqueIdentifier = string.Empty;
            GenericProfile profile = null;
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    while (reader.Read())
                    {
                        string str2 = DbUtility.GetValueFromDataReader<string>(reader, "UniqueIdentifier", string.Empty);
                        if (!string.IsNullOrEmpty(str2))
                        {
                            if (!uniqueIdentifier.Equals(str2, StringComparison.InvariantCultureIgnoreCase))
                            {
                                if (profile != null)
                                {
                                    base.CacheProfile(uniqueIdentifier, profile);
                                }
                                uniqueIdentifier = str2;
                                profile = new GenericProfile();
                            }
                            string str3 = DbUtility.GetValueFromDataReader<string>(reader, "CustomUserFieldName", string.Empty);
                            if (!string.IsNullOrEmpty(str3) && (profile != null))
                            {
                                profile[str3] = DbUtility.GetValueFromDataReader<string>(reader, "Value", string.Empty);
                            }
                        }
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
        }
    }
}

