﻿namespace Checkbox.Progress
{
    using Checkbox.Common;
    using Checkbox.Management;
    using Prezza.Framework.Caching;
    using Prezza.Framework.Caching.Expirations;
    using System;

    public static class ProgressProvider
    {
        private static readonly CacheManager _progressCache;

        static ProgressProvider()
        {
            lock (typeof(ProgressProvider))
            {
                _progressCache = CacheFactory.GetCacheManager();
            }
        }

        public static void ClearProgress(string key)
        {
            _progressCache.Remove(GetCacheKey(key));
        }

        private static string GetCacheKey(string progressKey)
        {
            if (Utilities.IsNotNullOrEmpty(ApplicationManager.ApplicationDataContext))
            {
                return string.Format("PROGRESS::{0}::{1}", ApplicationManager.ApplicationDataContext, progressKey);
            }
            return string.Format("PROGRESS::{0}", progressKey);
        }

        public static ProgressData GetProgress(string key)
        {
            return (_progressCache[GetCacheKey(key)] as ProgressData);
        }

        public static void SetProgress(string key, ProgressData progressData)
        {
            _progressCache.Add(GetCacheKey(key), progressData, CacheItemPriority.Normal, null, new ICacheItemExpiration[] { new SlidingTime(new TimeSpan(0, 10, 0)) });
        }

        public static void SetProgress(string progressKey, string message, int currentItem)
        {
            if (!string.IsNullOrEmpty(progressKey))
            {
                SetProgress(progressKey, message, string.Empty, ProgressStatus.Running, currentItem, 100);
            }
        }

        public static void SetProgress(string key, string message, string errorMessage, ProgressStatus status, int currentItem, int itemCount)
        {
            ProgressData progressData = new ProgressData {
                Message = message,
                ErrorMessage = errorMessage,
                Status = status,
                CurrentItem = currentItem,
                TotalItemCount = itemCount
            };
            SetProgress(key, progressData);
        }

        public static void SetProgressCounter(string progressKey, string baseMessage, int currentCounterItem, int totalCounterItems, int stepMagnitude, int stepCompletePercent)
        {
            if (!string.IsNullOrEmpty(progressKey) && !string.IsNullOrEmpty(baseMessage))
            {
                string message = (baseMessage.Contains("{0}") && baseMessage.Contains("{1}")) ? string.Format(baseMessage, currentCounterItem, totalCounterItems) : baseMessage;
                int currentItem = (totalCounterItems > 0) ? (stepCompletePercent - ((int) (((double) (stepMagnitude * (totalCounterItems - currentCounterItem))) / ((double) totalCounterItems)))) : stepCompletePercent;
                SetProgress(progressKey, message, currentItem);
            }
        }
    }
}

