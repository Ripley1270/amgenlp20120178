﻿namespace Checkbox.Forms.Validation
{
    using Checkbox.Globalization.Text;
    using System;

    public class UpperCaseAlphaValidator : RegularExpressionValidator
    {
        public UpperCaseAlphaValidator()
        {
            base._regex = @"^[A-Z\s]+$";
        }

        public override string GetMessage(string languageCode)
        {
            return TextManager.GetText("/validationMessages/regex/uppercase", languageCode);
        }
    }
}

