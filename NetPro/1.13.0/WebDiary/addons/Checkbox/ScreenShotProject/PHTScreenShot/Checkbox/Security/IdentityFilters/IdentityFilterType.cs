﻿namespace Checkbox.Security.IdentityFilters
{
    using System;

    public enum IdentityFilterType
    {
        Equal,
        NotEqual,
        In,
        NotIn,
        Like,
        NotLike,
        IsNull,
        IsNotNull
    }
}

