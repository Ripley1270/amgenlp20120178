﻿namespace Checkbox.Forms.Items.Configuration
{
    using Checkbox.Forms.Items;
    using Checkbox.Globalization.Text;
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class RedirectItemData : LocalizableResponseItemData
    {
        protected override void Create(IDbTransaction t)
        {
            base.Create(t);
            if (base.ID <= 0)
            {
                throw new ApplicationException("No DataID specified.  Unable to insert data for redirect item.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_InsertRedirect");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("Url", DbType.String, this.URL);
            storedProcCommandWrapper.AddInParameter("UrlTextID", DbType.String, this.URLTextID);
            storedProcCommandWrapper.AddInParameter("AutoRedirect", DbType.Boolean, this.RedirectAutomatically);
            storedProcCommandWrapper.AddInParameter("RestartSurvey", DbType.Boolean, this.RestartSurvey);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        protected override Item CreateItem()
        {
            return new RedirectItem();
        }

        public override ItemTextDecorator CreateTextDecorator(string languageCode)
        {
            return new RedirectItemTextDecorator(this, languageCode);
        }

        protected override DataSet GetConcreteConfigurationDataSet()
        {
            if (base.ID <= 0)
            {
                throw new ApplicationException("No DataID specified.  Unable to get for redirect item.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_GetRedirect");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            DataSet concreteConfigurationDataSet = base.GetConcreteConfigurationDataSet();
            database.LoadDataSet(storedProcCommandWrapper, concreteConfigurationDataSet, this.DataTableName);
            return concreteConfigurationDataSet;
        }

        protected override void LoadFromDataRow(DataRow data)
        {
            if (data == null)
            {
                throw new Exception("DataRow cannot be null.");
            }
            try
            {
                this.URL = data["Url"] as string;
                this.RedirectAutomatically = (data["AutoRedirect"] != DBNull.Value) ? ((bool) data["AutoRedirect"]) : false;
                this.RestartSurvey = (data["RestartSurvey"] != DBNull.Value) ? ((bool) data["RestartSurvey"]) : false;
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessProtected"))
                {
                    throw;
                }
            }
        }

        public override DataSet LoadTextData()
        {
            DataSet ds = base.LoadTextData();
            this.MergeTextData(ds, TextManager.GetTextData(this.URLTextID), this.TextTableName, "urlText", this.TextIDPrefix, base.ID.Value);
            return ds;
        }

        protected override void Update(IDbTransaction t)
        {
            base.Update(t);
            if (base.ID <= 0)
            {
                throw new ApplicationException("No DataID specified.  Unable to update data for redirect item.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_UpdateRedirect");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("Url", DbType.String, this.URL);
            storedProcCommandWrapper.AddInParameter("UrlTextID", DbType.String, this.URLTextID);
            storedProcCommandWrapper.AddInParameter("AutoRedirect", DbType.Boolean, this.RedirectAutomatically);
            storedProcCommandWrapper.AddInParameter("RestartSurvey", DbType.Boolean, this.RestartSurvey);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        public override string DataTableName
        {
            get
            {
                return "RedirectItemData";
            }
        }

        public virtual bool RedirectAutomatically { get; set; }

        public virtual bool RestartSurvey { get; set; }

        public override string TextIDPrefix
        {
            get
            {
                return "redirectItemData";
            }
        }

        public virtual string URL { get; set; }

        public virtual string URLTextID
        {
            get
            {
                return this.GetTextID("urlText");
            }
        }
    }
}

