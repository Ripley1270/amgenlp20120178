﻿namespace Checkbox.Security
{
    using Checkbox.Common;
    using System;
    using System.Security.Principal;

    public interface IIdentityProvider
    {
        IIdentity[] GetIdentities(PageFilter pageFilter, IdentitySortOrder sortOrder, IdentityFilterCollection identityFilters);
        IIdentity GetIdentity(string uniqueIdentifier);
        int GetIdentityCount(IdentityFilterCollection identityFilters);
    }
}

