﻿namespace Checkbox.Configuration
{
    using Prezza.Framework.Common;
    using Prezza.Framework.Configuration;
    using System;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Runtime.Remoting;
    using System.Text;
    using System.Xml;

    public class ConfigurationValidator
    {
        private ConfigurationValidator()
        {
        }

        private static bool LoadClass(string typeName, out string loadStatus)
        {
            ObjectHandle handle;
            if (typeName.IndexOf(",") < 0)
            {
                loadStatus = "Invalid type name.";
                return false;
            }
            string str = typeName.Split(new char[] { ',' })[0];
            string str2 = typeName.Split(new char[] { ',' })[1];
            if (string.IsNullOrEmpty(str2) || string.IsNullOrEmpty(str))
            {
                loadStatus = "Invalid type name.";
                return false;
            }
            try
            {
                handle = Activator.CreateInstance(str2, str);
            }
            catch (MissingMethodException)
            {
                loadStatus = string.Empty;
                return true;
            }
            catch (Exception exception)
            {
                loadStatus = string.Format("{0}  {1}", exception.GetType(), exception.Message);
                return false;
            }
            if (handle == null)
            {
                loadStatus = "CreateInstance failed.";
                return false;
            }
            loadStatus = string.Empty;
            return true;
        }

        public static bool ValidateConfiguration(string webconfigPath, out string status)
        {
            StringBuilder builder = new StringBuilder();
            bool flag = true;
            try
            {
                XmlNamespaceManager manager;
                ArgumentValidation.CheckForNullReference(webconfigPath, "webconfigPath");
                XmlDocument document = new XmlDocument();
                if (!File.Exists(webconfigPath))
                {
                    status = "Specified web.config path [" + webconfigPath + "] does not exist.";
                    return false;
                }
                try
                {
                    document.Load(webconfigPath);
                    manager = new XmlNamespaceManager(document.NameTable);
                    manager.AddNamespace("ms", "http://schemas.microsoft.com/.NetConfiguration/v2.0");
                }
                catch (Exception exception)
                {
                    status = "Unable to load web.config. Error was [" + exception.Message + "].";
                    return false;
                }
                foreach (XmlNode node in document.SelectNodes("/ms:configuration/ms:configSections/ms:section", manager))
                {
                    bool flag2 = true;
                    string attributeText = XmlUtility.GetAttributeText(node, "name");
                    if (string.IsNullOrEmpty(attributeText))
                    {
                        builder.Append(Environment.NewLine + "FAILED -- Found a configSection in the web.config with no name attribute.");
                        flag2 = false;
                        attributeText = string.Empty;
                    }
                    string str2 = XmlUtility.GetAttributeText(node, "type");
                    if (string.IsNullOrEmpty(str2))
                    {
                        builder.Append(Environment.NewLine + "FAILED -- Found a configSection in the web.config with no type attribute.  ConfigSection name was [" + attributeText + "].");
                        flag2 = false;
                    }
                    else
                    {
                        string str3;
                        if (!LoadClass(str2, out str3))
                        {
                            builder.Append(Environment.NewLine + "FAILED -- Unable to load section handler for configSection.  Name was [" + attributeText + "] and type was [" + str2 + "].  Load status was [" + str3 + "].");
                            flag2 = false;
                        }
                    }
                    if (attributeText != string.Empty)
                    {
                        XmlNode node2 = document.SelectSingleNode("/ms:configuration/ms:" + attributeText, manager);
                        if (node2 == null)
                        {
                            builder.Append(Environment.NewLine + "FAILED -- Unable to find named configuration section in web.confg.  Name was [" + attributeText + "].");
                            flag2 = false;
                        }
                        else
                        {
                            string str4 = XmlUtility.GetAttributeText(node2, "filePath");
                            string str5 = XmlUtility.GetAttributeText(node2, "configDataType");
                            if (string.IsNullOrEmpty(str4) && string.IsNullOrEmpty(str5))
                            {
                                continue;
                            }
                            if (string.IsNullOrEmpty(str4))
                            {
                                builder.Append(Environment.NewLine + "FAILED -- The configuration path for the section [" + attributeText + "] was not specified.");
                                flag2 = false;
                            }
                            else if (string.IsNullOrEmpty(str5))
                            {
                                builder.Append(Environment.NewLine + "FAILED -- The configuration data type for the section [" + attributeText + "] was not specified.");
                                flag2 = false;
                            }
                            else
                            {
                                string str6;
                                if (!ValidateFile(str4, manager, out str6))
                                {
                                    builder.Append(str6);
                                    flag2 = false;
                                }
                            }
                        }
                    }
                    if (!flag2)
                    {
                        builder.Append(Environment.NewLine + "FAILED -- Failed to validate configuration for section [" + attributeText + "].");
                    }
                    flag = flag && flag2;
                }
            }
            catch (Exception exception2)
            {
                builder.Append("An unhandled exception occurred while validating the application configuration. [" + exception2.Message + "].");
                status = builder.ToString();
                return false;
            }
            status = builder.ToString();
            return flag;
        }

        private static bool ValidateFile(string filePath, XmlNamespaceManager ns, out string fileStatus)
        {
            fileStatus = string.Empty;
            if (!filePath.Contains("/") && !filePath.Contains(@"\"))
            {
                filePath = ConfigurationManager.DetermineAppConfigPath() + @"\" + filePath;
            }
            if (!File.Exists(filePath))
            {
                fileStatus = Environment.NewLine + "FAILED -- Specified file path [" + filePath + "] does not exist.";
                return false;
            }
            XmlDocument document = new XmlDocument();
            try
            {
                document.Load(filePath);
            }
            catch (Exception exception)
            {
                fileStatus = Environment.NewLine + "FAILED -- Unable to load [" + filePath + "].  Exception was [" + exception.Message + "]";
                return false;
            }
            XmlNodeList list = document.SelectNodes("//ms:*", ns);
            bool flag = true;
            foreach (XmlNode node in list)
            {
                bool flag2 = false;
                foreach (XmlAttribute attribute in node.Attributes)
                {
                    bool flag3;
                    if (string.Compare(attribute.Name, "filePath", true) == 0)
                    {
                        flag3 = true;
                        if (string.Compare(attribute.Name, "filePath", false) != 0)
                        {
                            string str5 = fileStatus;
                            fileStatus = str5 + Environment.NewLine + "FAILED -- filePath attribute in node [" + node.Name + "] is the wrong case.  It is [" + attribute.Name + "] when it should be [filePath]";
                            flag = false;
                        }
                    }
                    else
                    {
                        flag3 = false;
                    }
                    if (string.Compare(attribute.Name, "configDataType", true) == 0)
                    {
                        if (string.Compare(attribute.Name, "configDataType", false) != 0)
                        {
                            string str6 = fileStatus;
                            fileStatus = str6 + Environment.NewLine + "FAILED -- configDataType attribute in node [" + node.Name + "] is the wrong case.  It is [" + attribute.Name + "] when it should be [configDataType]";
                            flag = false;
                        }
                    }
                    else
                    {
                        flag2 = true;
                    }
                    if (!flag2)
                    {
                        string str7 = fileStatus;
                        fileStatus = str7 + Environment.NewLine + "FAILED -- Node [" + node.Name + "] has a filePath attribute but no configDataType attribute.";
                        flag = false;
                    }
                    else if (!flag3)
                    {
                        string str8 = fileStatus;
                        fileStatus = str8 + Environment.NewLine + "FAILED -- Node [" + node.Name + "] has a configDataType attribute but no filePath attribute.";
                        flag = false;
                    }
                }
            }
            foreach (XmlNode node2 in document.SelectNodes("//ms:*[@filePath]", ns))
            {
                string str2;
                string str4;
                string attributeText = XmlUtility.GetAttributeText(node2, "configDataType");
                if (!string.IsNullOrEmpty(attributeText) && !LoadClass(attributeText, out str2))
                {
                    string str9 = fileStatus;
                    fileStatus = str9 + Environment.NewLine + "FAILED -- [" + filePath + "] Unable to load class.  Load status was [" + str2 + "].";
                    flag = false;
                }
                string str3 = XmlUtility.GetAttributeText(node2, "filePath");
                if (!string.IsNullOrEmpty(str3) && !ValidateFile(str3, ns, out str4))
                {
                    fileStatus = fileStatus + Environment.NewLine + str4;
                    flag = false;
                }
            }
            return flag;
        }
    }
}

