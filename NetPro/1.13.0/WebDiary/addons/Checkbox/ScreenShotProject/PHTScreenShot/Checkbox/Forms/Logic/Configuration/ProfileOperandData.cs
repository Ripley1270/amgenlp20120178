﻿namespace Checkbox.Forms.Logic.Configuration
{
    using Checkbox.Forms;
    using Checkbox.Forms.Logic;
    using Checkbox.Users;
    using Prezza.Framework.Common;
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class ProfileOperandData : OperandData
    {
        public ProfileOperandData(ResponseTemplate context) : this(context, null)
        {
        }

        public ProfileOperandData(ResponseTemplate context, string key) : base(context)
        {
            this.ProfileKey = key;
        }

        protected override void Create(IDbTransaction t)
        {
            base.Create(t);
            DataRow row = base.Context.ProfileOperandTable.NewRow();
            row["OperandID"] = base.ID.Value;
            row["ProfileKey"] = this.ProfileKey;
            base.Context.ProfileOperandTable.Rows.Add(row);
        }

        public override Operand CreateOperand(Response context, string languageCode)
        {
            if (context.Respondent != null)
            {
                return new ProfileOperand(this.ProfileKey, context.Respondent.Identity);
            }
            return new ProfileOperand(this.ProfileKey, null);
        }

        public override void Delete(IDbTransaction transaction)
        {
            DataRow[] rowArray = base.Context.ProfileOperandTable.Select(this.IdentityColumnName + "=" + base.ID.Value);
            if (rowArray.Length > 0)
            {
                rowArray[0].Delete();
            }
            base.Delete(transaction);
        }

        protected override DataSet GetConcreteConfigurationDataSet()
        {
            if (base.ID <= 0)
            {
                throw new ApplicationException("No DataID specified.");
            }
            try
            {
                Database database = DatabaseFactory.CreateDatabase();
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ProfileOperand_GetOperand");
                storedProcCommandWrapper.AddInParameter("OperandID", DbType.Int32, base.ID);
                DataSet dataSet = new DataSet();
                database.LoadDataSet(storedProcCommandWrapper, dataSet, new string[] { this.DataTableName });
                return dataSet;
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessProtected"))
                {
                    throw;
                }
                return null;
            }
        }

        protected override void LoadFromDataRow(DataRow data)
        {
            ArgumentValidation.CheckForNullReference(data, "data");
            try
            {
                this.ProfileKey = DbUtility.GetValueFromDataRow<string>(data, "ProfileKey", string.Empty);
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessProtected"))
                {
                    throw;
                }
            }
        }

        public override string ToString()
        {
            return this.ProfileKey;
        }

        public override void Validate()
        {
            foreach (object obj2 in UserManager.GetProfilePropertyList())
            {
                if (obj2.ToString() == this.ProfileKey)
                {
                    return;
                }
            }
            this.OnValidationFailed();
        }

        public override string DataTableName
        {
            get
            {
                return "ProfileOperand";
            }
        }

        public override string OperandTypeName
        {
            get
            {
                return "ProfileOperand";
            }
        }

        public string ProfileKey { get; set; }
    }
}

