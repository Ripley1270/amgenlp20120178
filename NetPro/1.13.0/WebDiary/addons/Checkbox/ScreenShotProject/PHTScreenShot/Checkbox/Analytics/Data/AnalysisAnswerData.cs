﻿namespace Checkbox.Analytics.Data
{
    using Checkbox.Analytics.Filters;
    using Checkbox.Common;
    using Checkbox.Forms;
    using Checkbox.Forms.Data;
    using Checkbox.Globalization.Text;
    using Checkbox.Progress;
    using Checkbox.Security.Providers;
    using Prezza.Framework.Data;
    using Prezza.Framework.Data.QueryParameters;
    using Prezza.Framework.Security;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Globalization;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Text;

    [Serializable]
    public class AnalysisAnswerData
    {
        private bool _answerDataLoaded;
        private Dictionary<string, int> _answerReaderOrdinals;
        private Dictionary<long, ItemAnswer> _answersDictionary;
        private Dictionary<int, List<long>> _filterPassedResponseIds;
        private Dictionary<int, List<ItemAnswer>> _itemAnswersDictionary;
        private Dictionary<Triplet<int, int, long>, long> _itemOptionResponseAnswersDictionary;
        private Dictionary<Pair<int, long>, List<long>> _itemResponseAnswersDictionary;
        private List<int> _preloadedProfileRtIds;
        private Dictionary<long, List<ItemAnswer>> _responseAnswerObjectsDictionary;
        private Dictionary<long, ResponseProperties> _responsePropertiesDictionary;
        private ReadOnlyCollection<string> _responsePropertyNames;

        public AnalysisAnswerData(bool skipDateModifiedChecks)
        {
            this.SkipFreshnessChecks = skipDateModifiedChecks;
        }

        private void AddAnswerToDictionaries(ItemAnswer answer)
        {
            this.AnswerDictionary[answer.AnswerId] = answer;
            if (!this.ItemAnswersDictionary.ContainsKey(answer.ItemId))
            {
                this.ItemAnswersDictionary[answer.ItemId] = new List<ItemAnswer>();
            }
            this.ItemAnswersDictionary[answer.ItemId].Add(answer);
            if (!this.ResponseAnswerObjectDictionary.ContainsKey(answer.ResponseId))
            {
                this.ResponseAnswerObjectDictionary[answer.ResponseId] = new List<ItemAnswer>();
            }
            this.ResponseAnswerObjectDictionary[answer.ResponseId].Add(answer);
            Pair<int, long> key = new Pair<int, long>(answer.ItemId, answer.ResponseId);
            if (!this.ItemResponseAnswersDictionary.ContainsKey(key))
            {
                this.ItemResponseAnswersDictionary[key] = new List<long>(1);
            }
            this.ItemResponseAnswersDictionary[key].Add(answer.AnswerId);
            if (answer.OptionId.HasValue)
            {
                Triplet<int, int, long> triplet = new Triplet<int, int, long>(answer.ItemId, answer.OptionId.Value, answer.ResponseId);
                this.ItemOptionResponseAnswersDictionary[triplet] = answer.AnswerId;
            }
        }

        private void AddItemAnswerObject(IDataReader reader)
        {
            this.PopulateResponseProperties(reader);
            ItemAnswer answer = this.CreateItemAnswerObject(reader);
            if (answer != null)
            {
                this.AddAnswerToDictionaries(answer);
            }
        }

        private static string BuildAnswerSelectQuery(IList<int> rtIDs, AnalysisFilterCollection filterCollection, DateTime? startDate, DateTime? endDate, bool includeIncompleteResponses)
        {
            StringBuilder builder = new StringBuilder();
            SelectQuery query = new SelectQuery("ckbx_Response");
            query.AddParameter(new SelectParameter("ResponseID", string.Empty, "ckbx_Response"));
            query.AddParameter(new SelectParameter("AnswerID", string.Empty, "ckbx_ResponseAnswers"));
            query.AddParameter(new SelectParameter("GUID", string.Empty, "ckbx_Response"));
            query.AddParameter(new SelectParameter("ItemID", string.Empty, "ckbx_ResponseAnswers"));
            query.AddParameter(new SelectParameter("AnswerText", string.Empty, "ckbx_ResponseAnswers"));
            query.AddParameter(new SelectParameter("OptionID", string.Empty, "ckbx_ResponseAnswers"));
            query.AddParameter(new SelectParameter("DateCreated", string.Empty, "ckbx_ResponseAnswers"));
            query.AddParameter(new SelectParameter("ResponseTemplateID", string.Empty, "ckbx_Response"));
            query.AddParameter(new SelectParameter("IsComplete", string.Empty, "ckbx_Response"));
            query.AddParameter(new SelectParameter("LastPageViewed", string.Empty, "ckbx_Response"));
            query.AddParameter(new SelectParameter("Started", string.Empty, "ckbx_Response"));
            query.AddParameter(new SelectParameter("Ended", string.Empty, "ckbx_Response"));
            query.AddParameter(new SelectParameter("IP", string.Empty, "ckbx_Response"));
            query.AddParameter(new SelectParameter("LastEdit", string.Empty, "ckbx_Response"));
            query.AddParameter(new SelectParameter("NetworkUser", string.Empty, "ckbx_Response"));
            query.AddParameter(new SelectParameter("Language", string.Empty, "ckbx_Response"));
            query.AddParameter(new SelectParameter("UniqueIdentifier", string.Empty, "ckbx_Response"));
            query.AddParameter(new SelectParameter("RespondentGUID", string.Empty, "ckbx_Response"));
            query.AddParameter(new SelectParameter("Invitee", string.Empty, "ckbx_Response"));
            query.AddTableJoin("ckbx_ResponseAnswers", QueryJoinType.Left, "ResponseID", "ckbx_Response", "ResponseID");
            query.AddTableJoin("ckbx_Item", QueryJoinType.Left, "ItemID", "ckbx_ResponseAnswers", "ItemID");
            query.AddTableJoin("ckbx_ItemOptions", QueryJoinType.Left, "OptionID", "ckbx_ResponseAnswers", "OptionID");
            builder.Append(query.ToString());
            builder.Append(" WHERE (ckbx_ResponseAnswers.Deleted IS NULL OR (ckbx_ResponseAnswers.Deleted IS NOT NULL AND ckbx_ResponseAnswers.Deleted = 0))");
            builder.Append(" AND (ckbx_Response.Deleted IS NULL OR (ckbx_Response.Deleted IS NOT NULL AND ckbx_Response.Deleted = 0))");
            builder.Append(" AND (ckbx_Item.Deleted IS NULL OR (ckbx_Item.Deleted IS NOT NULL AND ckbx_Item.Deleted = 0))");
            builder.Append(" AND (ckbx_ResponseAnswers.OptionID IS NULL OR (ckbx_ItemOptions.Deleted IS NULL OR (ckbx_ItemOptions.Deleted IS NOT NULL AND ckbx_ItemOptions.Deleted = 0)))");
            if (includeIncompleteResponses)
            {
                builder.Append(" ");
                if (startDate.HasValue && endDate.HasValue)
                {
                    builder.AppendFormat("AND ( (ckbx_Response.Ended IS NULL AND ckbx_Response.Started BETWEEN '{0}' AND '{1}') OR ckbx_Response.Ended BETWEEN '{0}' AND '{1}')", startDate.Value.ToString("s", DateTimeFormatInfo.InvariantInfo), endDate.Value.ToString("s", DateTimeFormatInfo.InvariantInfo));
                }
                else if (startDate.HasValue)
                {
                    builder.AppendFormat("AND ( (ckbx_Response.Ended IS NULL AND ckbx_Response.Started >= '{0}') OR ckbx_Response.Ended >= '{0}')", startDate.Value.ToString("s", DateTimeFormatInfo.InvariantInfo));
                }
                else if (endDate.HasValue)
                {
                    builder.AppendFormat("AND ( (ckbx_Response.Ended IS NULL AND ckbx_Response.Started <= '{0}') OR ckbx_Response.Ended <= '{0}')", endDate.Value.ToString("s", DateTimeFormatInfo.InvariantInfo));
                }
            }
            else
            {
                builder.Append(" AND (ckbx_Response.IsComplete = 1)");
                if (startDate.HasValue && endDate.HasValue)
                {
                    builder.Append(" AND (ckbx_Response.Ended BETWEEN '" + startDate.Value.ToString("s", DateTimeFormatInfo.InvariantInfo) + "' AND '" + endDate.Value.ToString("s", DateTimeFormatInfo.InvariantInfo) + "')");
                }
                else if (startDate.HasValue)
                {
                    builder.Append(" AND (ckbx_Response.Ended >= '" + startDate.Value.ToString("s", DateTimeFormatInfo.InvariantInfo) + "')");
                }
                else if (endDate.HasValue)
                {
                    builder.Append(" AND (ckbx_Response.Ended <= '" + endDate.Value.ToString("s", DateTimeFormatInfo.InvariantInfo) + "')");
                }
            }
            if (rtIDs.Count > 0)
            {
                builder.Append(" AND (");
                if (rtIDs.Count == 1)
                {
                    builder.Append(" ckbx_Response.ResponseTemplateID = " + rtIDs[0]);
                }
                else
                {
                    builder.Append(" (");
                    for (int i = 0; i < rtIDs.Count; i++)
                    {
                        if (i > 0)
                        {
                            builder.Append(",");
                        }
                        builder.Append(rtIDs[i].ToString());
                    }
                }
                builder.Append(")");
            }
            string filterString = filterCollection.FilterString;
            if (!string.IsNullOrEmpty(filterString))
            {
                builder.Append(" AND ");
                builder.Append("(" + filterString + ")");
            }
            return builder.ToString();
        }

        public double CalculateResponseScore(long responseId)
        {
            List<ItemAnswer> responseAnswers = this.GetResponseAnswers(responseId);
            double num = 0.0;
            foreach (ItemAnswer answer in responseAnswers)
            {
                if (answer.OptionId.HasValue)
                {
                    num += SurveyMetaDataProxy.GetOptionPoints(answer.OptionId.Value, this.SkipFreshnessChecks);
                }
            }
            return num;
        }

        private ItemAnswer CreateItemAnswerObject(IDataReader reader)
        {
            if (!DbUtility.GetValueFromDataReader<long?>(reader, "AnswerId", null).HasValue)
            {
                return null;
            }
            ItemAnswer answer = new ItemAnswer {
                ResponseId = (long) reader["ResponseId"],
                ResponseGuid = DbUtility.GetValueFromDataReader<Guid?>(reader, "GUID", null),
                AnswerId = (long) reader["AnswerID"],
                AnswerText = DbUtility.GetValueFromDataReader<string>(reader, "AnswerText", string.Empty).Trim(),
                ItemId = (int) reader["ItemId"],
                OptionId = DbUtility.GetValueFromDataReader<int?>(reader, "OptionId", null)
            };
            if (answer.OptionId.HasValue)
            {
                answer.IsOther = SurveyMetaDataProxy.GetOptionIsOther(answer.OptionId.Value, this.SkipFreshnessChecks);
            }
            return answer;
        }

        public void EnsureProfileDataPreloaded(IList<int> rtIds)
        {
            if (ProfileFactory.GetProfileProvider() is ProfileAndIdentityProvider)
            {
                if (this._preloadedProfileRtIds == null)
                {
                    this._preloadedProfileRtIds = new List<int>();
                }
                foreach (int num in rtIds)
                {
                    if (!this._preloadedProfileRtIds.Contains(num))
                    {
                        ((ProfileAndIdentityProvider) ProfileFactory.GetProfileProvider()).PreLoadProfilesForTemplateResponses(num);
                        this._preloadedProfileRtIds.Add(num);
                    }
                }
            }
        }

        protected void FilterResponseData(List<Checkbox.Analytics.Filters.Filter> filters)
        {
            List<long> list = new List<long>();
            foreach (long num in this.ResponseAnswerObjectDictionary.Keys)
            {
                if (!this.ValidateResponse(num, filters))
                {
                    list.Add(num);
                }
            }
            foreach (long num2 in list)
            {
                this.RemoveResponsesFromDictionaries(num2);
            }
        }

        public ItemAnswer GetOptionAnswer(long responseId, int itemId, int optionId)
        {
            Triplet<int, int, long> key = new Triplet<int, int, long>(itemId, optionId, responseId);
            if (this.ItemOptionResponseAnswersDictionary.ContainsKey(key))
            {
                return this.AnswerDictionary[this.ItemOptionResponseAnswersDictionary[key]];
            }
            return null;
        }

        public List<ItemAnswer> GetResponseAnswers(long responseId)
        {
            if (this.ResponseAnswerObjectDictionary.ContainsKey(responseId))
            {
                return this.ResponseAnswerObjectDictionary[responseId];
            }
            return new List<ItemAnswer>();
        }

        public ResponseProperties GetResponseProperties(long responseId)
        {
            if (this.ResponsePropertiesDictionary.ContainsKey(responseId))
            {
                return this.ResponsePropertiesDictionary[responseId];
            }
            return null;
        }

        public void Initialize(string languageCode)
        {
            this.AnswerDataLoaded = false;
            this.LanguageCode = languageCode;
        }

        public List<long> ListAnswerIds()
        {
            return new List<long>(this.AnswerDictionary.Keys);
        }

        public List<ItemAnswer> ListItemAnswers(int itemId)
        {
            if (this.ItemAnswersDictionary.ContainsKey(itemId))
            {
                return this.ItemAnswersDictionary[itemId];
            }
            return new List<ItemAnswer>();
        }

        public List<ItemAnswer> ListItemAnswers(int itemId, List<int> responseTemplateIds, List<Checkbox.Analytics.Filters.Filter> filtersToApply)
        {
            List<ItemAnswer> list = this.ListItemAnswers(itemId);
            if ((filtersToApply == null) || (filtersToApply.Count == 0))
            {
                return list;
            }
            foreach (Checkbox.Analytics.Filters.Filter filter in filtersToApply)
            {
                if (!this.FilterPassedResponseIds.ContainsKey(filter.FilterId))
                {
                    this.FilterPassedResponseIds[filter.FilterId] = new List<long>();
                    if (!(filter is IAnswerDataObjectFilter))
                    {
                        this.FilterPassedResponseIds[filter.FilterId] = new List<long>(this.ResponsePropertiesDictionary.Keys);
                    }
                    else
                    {
                        if (filter is ProfileFilter)
                        {
                            this.EnsureProfileDataPreloaded(responseTemplateIds);
                        }
                        foreach (long num in this.ResponsePropertiesDictionary.Keys)
                        {
                            if (((IAnswerDataObjectFilter) filter).EvaluateFilter(this.GetResponseAnswers(num), this.ResponsePropertiesDictionary))
                            {
                                this.FilterPassedResponseIds[filter.FilterId].Add(num);
                            }
                        }
                    }
                }
            }
            List<long> list2 = new List<long>();
            List<long> list3 = new List<long>();
            List<ItemAnswer> list4 = new List<ItemAnswer>();
            foreach (ItemAnswer answer in list)
            {
                if (!list2.Contains(answer.ResponseId) && !list3.Contains(answer.ResponseId))
                {
                    if (this.ValidateResponse(answer.ResponseId, filtersToApply))
                    {
                        list2.Add(answer.ResponseId);
                        list4.Add(answer);
                    }
                    else
                    {
                        list3.Add(answer.ResponseId);
                    }
                }
                else if (list2.Contains(answer.ResponseId))
                {
                    list4.Add(answer);
                }
            }
            return list4;
        }

        public List<ItemAnswer> ListItemResponseAnswers(long responseId, int itemId)
        {
            List<ItemAnswer> list = new List<ItemAnswer>();
            Pair<int, long> key = new Pair<int, long>(itemId, responseId);
            if (this.ItemResponseAnswersDictionary.ContainsKey(key))
            {
                foreach (long num in this.ItemResponseAnswersDictionary[key])
                {
                    if (this.AnswerDictionary.ContainsKey(num))
                    {
                        list.Add(this.AnswerDictionary[num]);
                    }
                }
            }
            return list;
        }

        public List<long> ListResponseIds()
        {
            List<long> list = new List<long>(this.ResponsePropertiesDictionary.Keys);
            list.Sort();
            return list;
        }

        public void Load(IList<int> rtIDs, AnalysisFilterCollection filterCollection, DateTime? startDate, DateTime? endDate, bool includeIncompleteResponses, string progressKey)
        {
            this.AnswerDictionary.Clear();
            this.AnswerReaderOrdinals.Clear();
            this.ResponseAnswerObjectDictionary.Clear();
            this.ItemAnswersDictionary.Clear();
            this.ItemResponseAnswersDictionary.Clear();
            this.ItemOptionResponseAnswersDictionary.Clear();
            this.ProgressKey = progressKey;
            bool flag = Utilities.IsNotNullOrEmpty(progressKey);
            if (flag)
            {
                ProgressProvider.SetProgress(progressKey, TextManager.GetText("/controlText/analysisData/loadingAnswersFromDatabase", this.LanguageCode), string.Empty, ProgressStatus.Running, 50, 100);
            }
            string query = BuildAnswerSelectQuery(rtIDs, filterCollection, startDate, endDate, includeIncompleteResponses);
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query);
            using (IDataReader reader = database.ExecuteReader(sqlStringCommandWrapper))
            {
                try
                {
                    if (flag)
                    {
                        ProgressProvider.SetProgress(progressKey, TextManager.GetText("/controlText/analysisData/processingAnswers", this.LanguageCode), string.Empty, ProgressStatus.Running, 60, 100);
                    }
                    while (reader.Read())
                    {
                        this.AddItemAnswerObject(reader);
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            if (flag)
            {
                ProgressProvider.SetProgress(progressKey, TextManager.GetText("/controlText/analysisData/applyingFilters", this.LanguageCode), string.Empty, ProgressStatus.Running, 70, 100);
            }
            List<Checkbox.Analytics.Filters.Filter> filters = filterCollection.GetFilters(string.Empty);
            foreach (Checkbox.Analytics.Filters.Filter filter in filters)
            {
                if (filter is ProfileFilter)
                {
                    this.EnsureProfileDataPreloaded(rtIDs);
                }
            }
            foreach (Checkbox.Analytics.Filters.Filter filter2 in filters)
            {
                if (filter2 is IAnswerDataObjectFilter)
                {
                    this.FilterResponseData(filters);
                    break;
                }
            }
            if (flag)
            {
                ProgressProvider.SetProgress(progressKey, TextManager.GetText("/controlText/analysisData/answersLoaded", this.LanguageCode), string.Empty, ProgressStatus.Running, 0x55, 100);
            }
            this._answerDataLoaded = true;
        }

        private void PopulateResponseProperties(IDataReader reader)
        {
            long key = DbUtility.GetValueFromDataReader<long>(reader, "ResponseId", -1L);
            if ((key > 0L) && !this.ResponsePropertiesDictionary.ContainsKey(key))
            {
                if (this.AnswerReaderOrdinals.Count == 0)
                {
                    foreach (string str in ResponseProperties.PropertyNames)
                    {
                        try
                        {
                            this.AnswerReaderOrdinals[str] = reader.GetOrdinal(str);
                        }
                        catch
                        {
                        }
                    }
                }
                ResponseProperties properties = new ResponseProperties();
                foreach (string str2 in this.ResponsePropertyNames)
                {
                    if (this.AnswerReaderOrdinals.ContainsKey(str2))
                    {
                        object obj2 = reader[this.AnswerReaderOrdinals[str2]];
                        if (obj2 == DBNull.Value)
                        {
                            properties.SetValue(str2, null);
                        }
                        else
                        {
                            properties.SetValue(str2, obj2);
                        }
                    }
                }
                properties.SetValue("ResponseGuid", DbUtility.GetValueFromDataReader<Guid>(reader, "GUID", Guid.Empty));
                this.ResponsePropertiesDictionary[key] = properties;
            }
        }

        private void RemoveAnswerFromDictionaries(ItemAnswer answer)
        {
            this.AnswerDictionary.Remove(answer.AnswerId);
            if (this.ItemAnswersDictionary.ContainsKey(answer.ItemId))
            {
                this.ItemAnswersDictionary[answer.ItemId].Remove(answer);
            }
            if (this.ResponseAnswerObjectDictionary.ContainsKey(answer.ResponseId))
            {
                this.ResponseAnswerObjectDictionary[answer.ResponseId].Remove(answer);
            }
            Pair<int, long> key = new Pair<int, long>(answer.ItemId, answer.ResponseId);
            if (this.ItemResponseAnswersDictionary.ContainsKey(key))
            {
                this.ItemResponseAnswersDictionary[key].Remove(answer.AnswerId);
            }
            if (answer.OptionId.HasValue)
            {
                Triplet<int, int, long> triplet = new Triplet<int, int, long>(answer.ItemId, answer.OptionId.Value, answer.ResponseId);
                this.ItemOptionResponseAnswersDictionary.Remove(triplet);
            }
        }

        private void RemoveResponsesFromDictionaries(long responseId)
        {
            foreach (ItemAnswer answer in (from answer in this.AnswerDictionary.Values
                where answer.ResponseId == responseId
                select answer).ToList<ItemAnswer>())
            {
                this.RemoveAnswerFromDictionaries(answer);
            }
            this.ResponseAnswerObjectDictionary.Remove(responseId);
        }

        protected bool ValidateResponse(long responseId, List<Checkbox.Analytics.Filters.Filter> filters)
        {
            foreach (Checkbox.Analytics.Filters.Filter filter in filters)
            {
                if (this.FilterPassedResponseIds.ContainsKey(filter.FilterId))
                {
                    if (!this.FilterPassedResponseIds[filter.FilterId].Contains(responseId))
                    {
                        return false;
                    }
                }
                else if ((filter is IAnswerDataObjectFilter) && !((IAnswerDataObjectFilter) filter).EvaluateFilter(this.GetResponseAnswers(responseId), this.ResponsePropertiesDictionary))
                {
                    return false;
                }
            }
            return true;
        }

        public bool AnswerDataLoaded
        {
            get
            {
                return this._answerDataLoaded;
            }
            private set
            {
                this._answerDataLoaded = value;
            }
        }

        private Dictionary<long, ItemAnswer> AnswerDictionary
        {
            get
            {
                if (this._answersDictionary == null)
                {
                    this._answersDictionary = new Dictionary<long, ItemAnswer>();
                }
                return this._answersDictionary;
            }
        }

        private Dictionary<string, int> AnswerReaderOrdinals
        {
            get
            {
                if (this._answerReaderOrdinals == null)
                {
                    this._answerReaderOrdinals = new Dictionary<string, int>();
                }
                return this._answerReaderOrdinals;
            }
        }

        public DateTime? DateRangeEnd { get; set; }

        public DateTime? DateRangeStart { get; set; }

        private Dictionary<int, List<long>> FilterPassedResponseIds
        {
            get
            {
                if (this._filterPassedResponseIds == null)
                {
                    this._filterPassedResponseIds = new Dictionary<int, List<long>>();
                }
                return this._filterPassedResponseIds;
            }
        }

        private Dictionary<int, List<ItemAnswer>> ItemAnswersDictionary
        {
            get
            {
                if (this._itemAnswersDictionary == null)
                {
                    this._itemAnswersDictionary = new Dictionary<int, List<ItemAnswer>>();
                }
                return this._itemAnswersDictionary;
            }
        }

        private Dictionary<Triplet<int, int, long>, long> ItemOptionResponseAnswersDictionary
        {
            get
            {
                if (this._itemOptionResponseAnswersDictionary == null)
                {
                    this._itemOptionResponseAnswersDictionary = new Dictionary<Triplet<int, int, long>, long>();
                }
                return this._itemOptionResponseAnswersDictionary;
            }
        }

        private Dictionary<Pair<int, long>, List<long>> ItemResponseAnswersDictionary
        {
            get
            {
                if (this._itemResponseAnswersDictionary == null)
                {
                    this._itemResponseAnswersDictionary = new Dictionary<Pair<int, long>, List<long>>();
                }
                return this._itemResponseAnswersDictionary;
            }
        }

        public string LanguageCode { get; private set; }

        public string ProgressKey { get; private set; }

        private Dictionary<long, List<ItemAnswer>> ResponseAnswerObjectDictionary
        {
            get
            {
                if (this._responseAnswerObjectsDictionary == null)
                {
                    this._responseAnswerObjectsDictionary = new Dictionary<long, List<ItemAnswer>>();
                }
                return this._responseAnswerObjectsDictionary;
            }
        }

        protected Dictionary<long, ResponseProperties> ResponsePropertiesDictionary
        {
            get
            {
                if (this._responsePropertiesDictionary == null)
                {
                    this._responsePropertiesDictionary = new Dictionary<long, ResponseProperties>();
                }
                return this._responsePropertiesDictionary;
            }
        }

        private ReadOnlyCollection<string> ResponsePropertyNames
        {
            get
            {
                if (this._responsePropertyNames == null)
                {
                    this._responsePropertyNames = ResponseProperties.PropertyNames;
                }
                return this._responsePropertyNames;
            }
        }

        public bool SkipFreshnessChecks { get; private set; }
    }
}

