﻿using Checkbox.Forms.Items;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Checkbox.Forms
{
    //using System;
    //using System.Collections.Generic;
    //using System.ComponentModel;
    //using System.Runtime.CompilerServices;

    [Serializable]
    public abstract class Page
    {
        [NonSerialized]
        private EventHandlerList _eventHandlers;

        protected Page(int templatePageID, int position)
        {
            this.PageId = templatePageID;
            this.Position = position;
            this.ItemIDs = new List<int>();
        }

        internal virtual void AddItemID(int itemID)
        {
            this.ItemIDs.Add(itemID);
        }

        protected EventHandlerList EventHandlers
        {
            get
            {
                return this._eventHandlers;
            }
            set
            {
                this._eventHandlers = value;
            }
        }

        protected List<int> ItemIDs { get; private set; }

        public abstract List<Item> Items { get; }

        public int PageId { get; private set; }

        public int Position { get; private set; }
    }
}

