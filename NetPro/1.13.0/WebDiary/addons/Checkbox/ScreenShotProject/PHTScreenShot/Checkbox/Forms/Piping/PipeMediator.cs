﻿namespace Checkbox.Forms.Piping
{
    using Checkbox.Common;
    using Checkbox.Forms;
    using Checkbox.Forms.Items;
    using Checkbox.Forms.Piping.Tokens;
    using Checkbox.Management;
    using Prezza.Framework.Security.Principal;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Runtime.CompilerServices;
    using System.Text.RegularExpressions;

    [Serializable]
    public class PipeMediator : IDisposable
    {
        private ExtendedPrincipal _currentPrincipal;
        private bool _initialized;
        private Regex _regExp;
        private readonly ResponseProperties _responseProperties;

        internal PipeMediator()
        {
            this.ProcessedTexts = new Dictionary<string, ProcessedText>();
            this.TokenValues = new Dictionary<string, TokenValue>();
            this.StaticTexts = new Dictionary<string, string>();
            this.ResponsePipes = new List<ItemToken>();
            this._responseProperties = new ResponseProperties();
            this._regExp = this.CreateRegEx();
        }

        protected void CacheText(string key, ProcessedText text)
        {
            this.ProcessedTexts[key] = text;
        }

        protected void CacheValue(string key, TokenValue value)
        {
            this.TokenValues[key] = value;
        }

        protected virtual Regex CreateRegEx()
        {
            return new Regex(ApplicationManager.AppSettings.PipePrefix + @"\w*\b", RegexOptions.IgnoreCase);
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
        }

        public virtual string GetText(int itemID, string key)
        {
            return this.GetText(itemID, key, true);
        }

        public virtual string GetText(int itemID, string key, bool removeEmpty)
        {
            if (!Utilities.IsNullOrEmpty(key))
            {
                string str = itemID + "_" + key;
                if (this.StaticTexts.ContainsKey(str))
                {
                    return this.StaticTexts[str];
                }
                ProcessedText textFromCache = this.GetTextFromCache(str);
                if (textFromCache != null)
                {
                    this.ProcessTokens(textFromCache.TokenValues, removeEmpty);
                    return textFromCache.Text;
                }
            }
            return string.Empty;
        }

        public virtual string GetText(int itemId, string key, string initialValue)
        {
            string str = itemId + "_" + key;
            if (!this.StaticTexts.ContainsKey(str) && !this.ProcessedTexts.ContainsKey(key))
            {
                this.RegisterText(itemId, key, initialValue);
            }
            return this.GetText(itemId, key);
        }

        protected ProcessedText GetTextFromCache(string key)
        {
            if (this.ProcessedTexts.ContainsKey(key))
            {
                return this.ProcessedTexts[key];
            }
            return null;
        }

        protected TokenValue GetValueFromCache(string key)
        {
            if (this.TokenValues.ContainsKey(key))
            {
                return this.TokenValues[key];
            }
            return null;
        }

        internal virtual void Initialize(Response response, ExtendedPrincipal principal)
        {
            this._responseProperties.Initialize(response);
            this._currentPrincipal = principal;
            if (!this._initialized)
            {
                this.ResponsePipes.Clear();
                foreach (ItemToken token in response.ResponsePipes)
                {
                    this.ResponsePipes.Add(token);
                    if (response.Items.ContainsKey(token.ItemID) && !this.TokenValues.ContainsKey(token.TokenName))
                    {
                        Item item = response.Items[token.ItemID];
                        if (item is IAnswerable)
                        {
                            ((IAnswerable) item).AnswerChanged += new EventHandler(this.item_AnswerChanged);
                            TokenValue value2 = new TokenValue(token) {
                                Value = null,
                                IsDirty = true
                            };
                            this.CacheValue(token.TokenName, value2);
                        }
                    }
                }
                this._initialized = true;
            }
        }

        protected void item_AnswerChanged(object sender, EventArgs e)
        {
            if ((sender is ResponseItem) && (sender is IAnswerable))
            {
                List<Token> list = new List<Token>();
                foreach (TokenValue value2 in this.TokenValues.Values)
                {
                    if ((value2.Token.Type == TokenType.Answer) && (((ItemToken) value2.Token).ItemID == ((Item) sender).ID))
                    {
                        list.Add(value2.Token);
                    }
                }
                foreach (Token token in list)
                {
                    if (this.TokenValues.ContainsKey(token.TokenName))
                    {
                        this.TokenValues[token.TokenName].Value = PipeManager.GetTokenValue(token, ((ResponseItem) sender).Response);
                    }
                }
            }
        }

        protected virtual void ProcessTokens(ReadOnlyCollection<TokenValue> tokenValues, bool removeEmpty)
        {
            foreach (TokenValue value2 in tokenValues)
            {
                if (value2.IsDirty)
                {
                    if ((value2.Token.Type == TokenType.Response) || (value2.Token.Type == TokenType.ResponseTemplate))
                    {
                        value2.Value = PipeManager.GetTokenValue(value2.Token, this._responseProperties);
                    }
                    else if (value2.Token.Type == TokenType.Profile)
                    {
                        value2.Value = PipeManager.GetTokenValue(value2.Token, this._currentPrincipal);
                    }
                    else
                    {
                        value2.Value = string.Empty;
                    }
                }
                if (!removeEmpty && Utilities.IsNullOrEmpty(value2.Value))
                {
                    value2.Value = value2.Token.TokenName;
                }
            }
        }

        public virtual void RegisterText(int itemID, string key, string text)
        {
            if (Utilities.IsNotNullOrEmpty(key) && Utilities.IsNotNullOrEmpty(text))
            {
                string str = itemID + "_" + key;
                if (this.TextCacheContainsKey(str))
                {
                    this.GetTextFromCache(str).OriginalText = text;
                }
                else
                {
                    bool flag = false;
                    ProcessedText text3 = new ProcessedText();
                    for (Match match = this._regExp.Match(text); match.Success; match = match.NextMatch())
                    {
                        flag = true;
                        if (this.ValueCacheContainsKey(match.Value))
                        {
                            text3.AddTokenValue(this.GetValueFromCache(match.Value));
                        }
                        else
                        {
                            Token token = PipeManager.GetToken(match.Value, null);
                            if ((token != null) && (token.Type != TokenType.Other))
                            {
                                this.TokenValues[match.Value] = new TokenValue(token);
                                text3.AddTokenValue(this.TokenValues[match.Value]);
                            }
                            else
                            {
                                foreach (ItemToken token2 in this.ResponsePipes)
                                {
                                    if (string.Compare(token2.TokenName, match.Value, true) == 0)
                                    {
                                        this.TokenValues[match.Value] = new TokenValue(token2);
                                        text3.AddTokenValue(this.TokenValues[match.Value]);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    if (flag)
                    {
                        text3.OriginalText = text;
                        this.CacheText(str, text3);
                    }
                    else
                    {
                        this.StaticTexts[str] = text;
                    }
                }
            }
        }

        protected bool TextCacheContainsKey(string key)
        {
            return this.ProcessedTexts.ContainsKey(key);
        }

        protected bool ValueCacheContainsKey(string key)
        {
            return this.TokenValues.ContainsKey(key);
        }

        protected Dictionary<string, ProcessedText> ProcessedTexts { get; set; }

        protected Regex RegExp
        {
            get
            {
                return this._regExp;
            }
            set
            {
                this._regExp = value;
            }
        }

        protected List<ItemToken> ResponsePipes { get; private set; }

        protected Dictionary<string, string> StaticTexts { get; set; }

        protected Dictionary<string, TokenValue> TokenValues { get; set; }

        [Serializable]
        protected class ProcessedText
        {
            private string _text;
            private readonly List<PipeMediator.TokenValue> _tokenValues;

            public ProcessedText()
            {
                this.OriginalText = string.Empty;
                this._text = string.Empty;
                this.IsDirty = true;
                this._tokenValues = new List<PipeMediator.TokenValue>();
            }

            public void AddTokenValue(PipeMediator.TokenValue tokenValue)
            {
                tokenValue.ValueUpdated += new TokenValueUpdated(this.tokenValue_ValueUpdated);
                this._tokenValues.Add(tokenValue);
            }

            private void ProcessText()
            {
                this._text = this.OriginalText;
                foreach (PipeMediator.TokenValue value2 in this._tokenValues)
                {
                    this._text = this._text.Replace(value2.Token.TokenName, value2.Value);
                }
                this.IsDirty = false;
            }

            private void tokenValue_ValueUpdated(object sender, TokenValueUpdatedEventArgs e)
            {
                this.IsDirty = true;
            }

            public bool IsDirty { get; set; }

            public string OriginalText { get; set; }

            public string Text
            {
                get
                {
                    if (this.IsDirty)
                    {
                        this.ProcessText();
                    }
                    return this._text;
                }
                set
                {
                    this._text = value;
                    this.IsDirty = false;
                }
            }

            public ReadOnlyCollection<PipeMediator.TokenValue> TokenValues
            {
                get
                {
                    return new ReadOnlyCollection<PipeMediator.TokenValue>(this._tokenValues);
                }
            }
        }

        [Serializable]
        protected class TokenValue
        {
            private string _value;

            public event TokenValueUpdated ValueUpdated;

            public TokenValue(Checkbox.Forms.Piping.Tokens.Token token)
            {
                this.Token = token;
                this.IsDirty = true;
            }

            public TokenValue(Checkbox.Forms.Piping.Tokens.Token token, string value)
            {
                this.Token = token;
                this._value = value;
                this.IsDirty = false;
            }

            public bool IsDirty { get; set; }

            public Checkbox.Forms.Piping.Tokens.Token Token { get; private set; }

            public string Value
            {
                get
                {
                    return this._value;
                }
                set
                {
                    this._value = value;
                    if (this.Token.Type != TokenType.Response)
                    {
                        this.IsDirty = false;
                    }
                    if (this.ValueUpdated != null)
                    {
                        this.ValueUpdated(this, new TokenValueUpdatedEventArgs(this.Token.TokenName, value));
                    }
                }
            }
        }
    }
}

