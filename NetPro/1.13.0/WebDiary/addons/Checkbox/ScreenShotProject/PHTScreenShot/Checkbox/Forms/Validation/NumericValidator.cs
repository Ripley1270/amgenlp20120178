﻿using Checkbox.Common;
using Checkbox.Globalization.Text;
using System;
using System.Globalization;

namespace Checkbox.Forms.Validation
{
    //using Checkbox.Common;
    //using Checkbox.Globalization.Text;
    //using System;

    public class NumericValidator : Validator<string>
    {
        public override string GetMessage(string languageCode)
        {
            return TextManager.GetText("/validationMessages/regex/numeric", languageCode);
        }

        public override bool Validate(string input)
        {
            return Utilities.GetDouble(input, new CultureInfo[0]).HasValue;
        }
    }
}

