﻿namespace Checkbox.Forms.Piping
{
    using Checkbox.Common;
    using Checkbox.Forms;
    using Checkbox.Forms.Piping.PipeHandlers;
    using Checkbox.Forms.Piping.Tokens;
    using Checkbox.Management;
    using Checkbox.Users;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    public static class PipeManager
    {
        public static ReadOnlyCollection<string> GetProfilePipeNames(List<string> customUserFieldNames)
        {
            if (customUserFieldNames == null)
            {
                customUserFieldNames = new List<string>();
                foreach (object obj2 in UserManager.GetProfilePropertyList())
                {
                    customUserFieldNames.Add(obj2.ToString());
                }
            }
            if (!customUserFieldNames.Contains("UserName"))
            {
                customUserFieldNames.Add("UserName");
            }
            if (!customUserFieldNames.Contains("Password"))
            {
                customUserFieldNames.Add("Password");
            }
            if (!customUserFieldNames.Contains("GUID"))
            {
                customUserFieldNames.Add("GUID");
            }
            return new ReadOnlyCollection<string>(customUserFieldNames);
        }

        public static ReadOnlyCollection<string> GetResponsePipeNames()
        {
            return ResponseProperties.PropertyNames;
        }

        public static ReadOnlyCollection<string> GetResponseTemplatePipeNames()
        {
            List<string> list = new List<string> { "ResponseTemplateGUID" };
            if (ApplicationManager.AppSettings.AllowResponseTemplateIDLookup)
            {
                list.Add("ResponseTemplateID");
            }
            return new ReadOnlyCollection<string>(list);
        }

        public static Token GetToken(string token, List<string> customUserFieldNames)
        {
            switch (GetTokenType(token, customUserFieldNames))
            {
                case TokenType.Profile:
                    return new ProfileToken(token);

                case TokenType.Response:
                    return new ResponseToken(token);

                case TokenType.ResponseTemplate:
                    return new ResponseTemplateToken(token);
            }
            return new Token(token);
        }

        private static TokenType GetTokenType(string token, List<string> customUserFieldNames)
        {
            if (!Utilities.IsNotNullOrEmpty(token))
            {
                throw new Exception("Unable to determine type for NULL token.");
            }
            string strB = token.Replace(ApplicationManager.AppSettings.PipePrefix, string.Empty);
            foreach (string str2 in GetProfilePipeNames(customUserFieldNames))
            {
                if ((string.Compare(str2, strB, true) == 0) || (string.Compare(str2, strB.Replace("_", " "), true) == 0))
                {
                    return TokenType.Profile;
                }
                if (string.Compare(str2, strB.Replace("_", " "), true) == 0)
                {
                    return TokenType.Profile;
                }
            }
            foreach (string str3 in GetResponsePipeNames())
            {
                if (string.Compare(str3, strB, true) == 0)
                {
                    return TokenType.Response;
                }
            }
            foreach (string str4 in GetResponseTemplatePipeNames())
            {
                if (string.Compare(str4, strB, true) == 0)
                {
                    return TokenType.ResponseTemplate;
                }
            }
            return TokenType.Answer;
        }

        public static string GetTokenValue(Token token, object context)
        {
            if (token.Type == TokenType.Answer)
            {
                return new AnswerPipeHandler().GetTokenValue(token, context);
            }
            if (token.Type == TokenType.Profile)
            {
                return new ProfilePipeHandler().GetTokenValue(token, context);
            }
            if (token.Type == TokenType.Response)
            {
                return new ResponsePipeHandler().GetTokenValue(token, context);
            }
            if (token.Type == TokenType.ResponseTemplate)
            {
                return new ResponseTemplatePipeHandler().GetTokenValue(token, context);
            }
            return string.Empty;
        }
    }
}

