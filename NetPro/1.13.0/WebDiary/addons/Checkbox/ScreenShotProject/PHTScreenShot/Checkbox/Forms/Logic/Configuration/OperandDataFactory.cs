﻿namespace Checkbox.Forms.Logic.Configuration
{
    using Prezza.Framework.Common;
    using System;
    using System.Reflection;

    public class OperandDataFactory
    {
        private OperandData CreateObject(Type type, object[] args)
        {
            object obj2;
            ArgumentValidation.CheckForNullReference(type, "type");
            ValidateTypeIsOperandData(type);
            Type[] types = new Type[args.Length];
            for (int i = 0; i < types.Length; i++)
            {
                types[i] = args[i].GetType();
            }
            ConstructorInfo constructor = type.GetConstructor(types);
            if (constructor == null)
            {
                throw new Exception("Operand does not have a constructor: " + type.FullName);
            }
            try
            {
                obj2 = constructor.Invoke(args);
            }
            catch (MethodAccessException exception)
            {
                throw new Exception(exception.Message, exception);
            }
            catch (TargetInvocationException exception2)
            {
                throw new Exception(exception2.Message, exception2);
            }
            catch (TargetParameterCountException exception3)
            {
                throw new Exception(exception3.Message, exception3);
            }
            return (OperandData) obj2;
        }

        internal OperandData CreateOperandData(string typeName, params object[] args)
        {
            Type type = GetType(typeName);
            return this.CreateObject(type, args);
        }

        private static Type GetType(string typeName)
        {
            Type type;
            ArgumentValidation.CheckForEmptyString(typeName, "typeName");
            try
            {
                type = Type.GetType(typeName, true, false);
            }
            catch (TypeLoadException exception)
            {
                throw new Exception("A type-loading error occurred.  Type was: " + typeName, exception);
            }
            return type;
        }

        private static void ValidateTypeIsOperandData(Type type)
        {
            ArgumentValidation.CheckForNullReference(type, "type");
            if (!typeof(OperandData).IsAssignableFrom(type))
            {
                throw new Exception("Type mismatch between OperandData type [" + typeof(OperandData).AssemblyQualifiedName + "] and requested type [" + type.AssemblyQualifiedName);
            }
        }
    }
}

