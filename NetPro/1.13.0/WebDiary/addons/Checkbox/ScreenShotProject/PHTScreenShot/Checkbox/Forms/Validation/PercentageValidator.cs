﻿namespace Checkbox.Forms.Validation
{
    using System;

    public class PercentageValidator : RegularExpressionValidator
    {
        public PercentageValidator()
        {
            base._regex = @"^\d{0,2}(\.\d{1,4})? *%?$";
        }
    }
}

