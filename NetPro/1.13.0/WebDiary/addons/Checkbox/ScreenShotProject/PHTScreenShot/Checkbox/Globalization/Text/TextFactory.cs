﻿namespace Checkbox.Globalization.Text
{
    using Checkbox.Globalization.Configuration;
    using Prezza.Framework.Configuration;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Collections;

    internal sealed class TextFactory
    {
        private static readonly Hashtable _providers;

        static TextFactory()
        {
            lock (typeof(TextFactory))
            {
                _providers = new Hashtable();
            }
        }

        private static void ConfigurationManager_ConfigurationChanged(object sender, ConfigurationChangedEventArgs e)
        {
            if (_providers != null)
            {
                _providers.Clear();
            }
        }

        public static ITextProvider GetTextProvider()
        {
            try
            {
                lock (_providers.SyncRoot)
                {
                    if (_providers.Contains("[DEFAULT]"))
                    {
                        return (ITextProvider) _providers["[DEFAULT]"];
                    }
                }
                ITextProvider textProvider = new TextProviderFactory("textProviderFactory", (GlobalizationConfiguration) ConfigurationManager.GetConfiguration("globalizationConfiguration")).GetTextProvider();
                lock (_providers.SyncRoot)
                {
                    if (!_providers.Contains("[DEFAULT]"))
                    {
                        _providers.Add("[DEFAULT]", textProvider);
                    }
                }
                return textProvider;
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
                return null;
            }
        }

        public static ITextProvider GetTextProvider(string textProvider)
        {
            try
            {
                lock (_providers.SyncRoot)
                {
                    if (_providers.Contains(textProvider))
                    {
                        return (ITextProvider) _providers[textProvider];
                    }
                }
                ITextProvider provider = new TextProviderFactory("textProviderFactory", (GlobalizationConfiguration) ConfigurationManager.GetConfiguration("globalizationConfiguration")).GetTextProvider(textProvider);
                lock (_providers.SyncRoot)
                {
                    if (!_providers.Contains(textProvider))
                    {
                        _providers.Add(textProvider, provider);
                    }
                }
                return provider;
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
                return null;
            }
        }

        internal static string[] ApplicationLanguages
        {
            get
            {
                GlobalizationConfiguration configuration = (GlobalizationConfiguration) ConfigurationManager.GetConfiguration("globalizationConfiguration");
                if (configuration != null)
                {
                    return (string[]) configuration.ApplicationLanguages.ToArray(typeof(string));
                }
                return new string[0];
            }
        }

        internal static string DefaultLanguage
        {
            get
            {
                GlobalizationConfiguration configuration = (GlobalizationConfiguration) ConfigurationManager.GetConfiguration("globalizationConfiguration");
                if (configuration != null)
                {
                    return configuration.DefaultLanguage;
                }
                return string.Empty;
            }
        }

        internal static string[] SurveyLanguages
        {
            get
            {
                GlobalizationConfiguration configuration = (GlobalizationConfiguration) ConfigurationManager.GetConfiguration("globalizationConfiguration");
                if (configuration != null)
                {
                    return (string[]) configuration.SurveyLanguages.ToArray(typeof(string));
                }
                return new string[0];
            }
        }
    }
}

