﻿namespace Checkbox.ActiveDirectory.Providers
{
    using Prezza.Framework.Common;
    using Prezza.Framework.Configuration;
    using System;
    using System.Xml;

    public class LDAPAuthenticationProviderData : ProviderData, IXmlConfigurationBase
    {
        public LDAPAuthenticationProviderData(string providerName) : base(providerName, typeof(LDAPAuthenticationProvider).AssemblyQualifiedName)
        {
            ArgumentValidation.CheckForEmptyString(providerName, "providerName");
        }

        public void LoadFromXml(XmlNode providerNode)
        {
        }
    }
}

