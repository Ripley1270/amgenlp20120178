﻿namespace Checkbox.Forms
{
    using System;
    using System.Collections.Generic;

    public class TemplatePageComparer : IComparer<TemplatePage>
    {
        public int Compare(TemplatePage x, TemplatePage y)
        {
            if (x.Position < y.Position)
            {
                return -1;
            }
            if (x.Position > y.Position)
            {
                return 1;
            }
            return 0;
        }
    }
}

