﻿namespace Checkbox.Forms.Validation
{
    using Checkbox.Forms.Items;
    using System;

    public class RatingScaleValidator : SelectItemValidator
    {
        public override bool Validate(SelectItem input)
        {
            base.ErrorMessage = string.Empty;
            if (!this.ValidateRequired(input))
            {
                return false;
            }
            return true;
        }
    }
}

