﻿namespace Checkbox.Panels
{
    using Checkbox.Common;
    using Prezza.Framework.Data;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Security.Principal;

    [Serializable]
    public class UserPanel : Panel
    {
        public UserPanel() : base(new string[0], new string[0])
        {
            base.PanelType = PanelType.UserPanel;
        }

        public void AddIdentities(IList<IIdentity> identities)
        {
            foreach (IIdentity identity in identities)
            {
                this.AddIdentity(identity.Name);
            }
        }

        public void AddIdentity(string identity)
        {
            if (!Utilities.IsNullOrEmpty(identity) && (base.Panelists.Find(p => ((UserPanelist) p).UniqueIdentifier.Equals(identity, StringComparison.InvariantCultureIgnoreCase)) == null))
            {
                UserPanelist item = new UserPanelist {
                    UniqueIdentifier = identity
                };
                base.Panelists.Add(item);
                UserPanelist panelist2 = new UserPanelist {
                    UniqueIdentifier = identity
                };
                base.AddedPanelists.Add(panelist2);
            }
        }

        protected DBCommandWrapper GetDeletePanelistCommand(Database db, string panelistIdentifier)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_Panel_DeleteUser");
            storedProcCommandWrapper.AddInParameter("PanelID", DbType.Int32, base.ID.Value);
            storedProcCommandWrapper.AddInParameter("UniqueIdentifier", DbType.String, panelistIdentifier);
            return storedProcCommandWrapper;
        }

        protected DBCommandWrapper GetInsertPanelistCommand(Database db, string panelistIdentifier)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_Panel_InsertUser");
            storedProcCommandWrapper.AddInParameter("PanelID", DbType.Int32, base.ID.Value);
            storedProcCommandWrapper.AddInParameter("UniqueIdentifier", DbType.String, panelistIdentifier);
            return storedProcCommandWrapper;
        }

        public override Panelist GetPanelist(string identifier)
        {
            return base.Panelists.Find(p => ((UserPanelist) p).UniqueIdentifier.Equals(identifier, StringComparison.InvariantCultureIgnoreCase));
        }

        protected override List<Panelist> GetPanelists()
        {
            List<Panelist> list = new List<Panelist>();
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Panel_GetUserPanelIDs");
            storedProcCommandWrapper.AddInParameter("PanelID", DbType.Int32, base.ID.Value);
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    while (reader.Read())
                    {
                        string str = DbUtility.GetValueFromDataReader<string>(reader, "UserIdentifier", string.Empty);
                        if (Utilities.IsNotNullOrEmpty(str))
                        {
                            UserPanelist item = new UserPanelist {
                                UniqueIdentifier = str
                            };
                            list.Add(item);
                        }
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return list;
        }

        public void RemoveIdentities(IEnumerable<IIdentity> identities)
        {
            foreach (IIdentity identity in identities)
            {
                this.RemoveIdentity(identity.Name);
            }
        }

        public void RemoveIdentity(string identity)
        {
            if (!Utilities.IsNullOrEmpty(identity))
            {
                int index = base.Panelists.FindIndex(p => ((UserPanelist) p).UniqueIdentifier.Equals(identity, StringComparison.InvariantCultureIgnoreCase));
                if (index >= 0)
                {
                    base.RemovedPanelists.Add(base.Panelists[index]);
                    base.Panelists.RemoveAt(index);
                }
            }
        }

        protected override void UpdatePanelists(IDbTransaction t)
        {
            Database db = DatabaseFactory.CreateDatabase();
            foreach (Panelist panelist in base.RemovedPanelists)
            {
                DBCommandWrapper deletePanelistCommand = this.GetDeletePanelistCommand(db, ((UserPanelist) panelist).UniqueIdentifier);
                db.ExecuteNonQuery(deletePanelistCommand, t);
            }
            foreach (Panelist panelist2 in base.AddedPanelists)
            {
                DBCommandWrapper insertPanelistCommand = this.GetInsertPanelistCommand(db, ((UserPanelist) panelist2).UniqueIdentifier);
                db.ExecuteNonQuery(insertPanelistCommand, t);
            }
            base.RemovedPanelists.Clear();
            base.AddedPanelists.Clear();
        }
    }
}

