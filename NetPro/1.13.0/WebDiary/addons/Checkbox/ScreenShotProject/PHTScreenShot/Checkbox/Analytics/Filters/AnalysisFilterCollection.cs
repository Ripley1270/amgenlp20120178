﻿namespace Checkbox.Analytics.Filters
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    [Serializable]
    public class AnalysisFilterCollection : FilterDataCollection
    {
        public string AppendFilterInClause(string existingFilterString, string clause, bool negated)
        {
            if ((clause != null) && (clause.Trim() == string.Empty))
            {
                return existingFilterString;
            }
            StringBuilder builder = new StringBuilder();
            builder.Append(existingFilterString);
            if ((existingFilterString != null) && (existingFilterString.Trim() != string.Empty))
            {
                builder.Append(" AND ");
            }
            builder.Append("( ckbx_Response.ResponseID");
            if (negated)
            {
                builder.Append(" NOT IN ");
            }
            else
            {
                builder.Append(" IN ");
            }
            builder.Append("(Select ResponseID FROM ckbx_ResponseAnswers WHERE ");
            builder.Append(clause);
            builder.Append("))");
            return builder.ToString();
        }

        public string AppendFilterString(string existingFilterString, string toAppend)
        {
            if ((existingFilterString != null) && (existingFilterString.Trim() != string.Empty))
            {
                existingFilterString = existingFilterString + " AND ";
            }
            return (existingFilterString + toAppend);
        }

        public virtual string FilterString
        {
            get
            {
                List<Checkbox.Analytics.Filters.Filter> filters = base.GetFilters(string.Empty);
                string existingFilterString = string.Empty;
                foreach (Checkbox.Analytics.Filters.Filter filter in filters)
                {
                    if (filter is IQueryFilter)
                    {
                        existingFilterString = this.AppendFilterInClause(existingFilterString, ((IQueryFilter) filter).FilterString, ((IQueryFilter) filter).UseNotIn);
                    }
                }
                return existingFilterString;
            }
        }

        public override string ParentType
        {
            get
            {
                return "AnalysisTemplate";
            }
        }
    }
}

