﻿namespace Checkbox.Forms.Items.Configuration
{
    using Checkbox.Globalization.Text;
    using System;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class ItemTextDecorator : TextDecorator
    {
        public ItemTextDecorator(ItemData data, string language) : base(language)
        {
            this.Data = data;
        }

        public void Copy(ItemData data)
        {
            if (this.Data != null)
            {
                this.Data.Save();
                if (this.Data.ID > 0)
                {
                    this.CopyLocalizedText(data);
                }
            }
        }

        protected virtual void CopyLocalizedText(ItemData data)
        {
        }

        public virtual void ImportText(DataSet data, int oldItemID)
        {
        }

        protected override void OnAbort(object sender, EventArgs e)
        {
            if (this.Data != null)
            {
                this.Data.NotifyAbort(sender, e);
            }
        }

        protected override void OnCommit(object sender, EventArgs e)
        {
            if (this.Data != null)
            {
                this.Data.NotifyCommit(sender, e);
            }
        }

        protected override void OnRollback()
        {
            if (this.Data != null)
            {
                this.Data.Rollback();
            }
        }

        public override void Save()
        {
            if (this.Data != null)
            {
                this.Data.Save();
                if (this.Data.ID > 0)
                {
                    this.SetLocalizedTexts();
                }
            }
        }

        public override void Save(IDbTransaction t)
        {
            if (this.Data != null)
            {
                this.Data.Save(t);
                if (this.Data.ID > 0)
                {
                    this.SetLocalizedTexts();
                }
            }
        }

        protected override void SetLocalizedTexts()
        {
        }

        public ItemData Data { get; private set; }
    }
}

