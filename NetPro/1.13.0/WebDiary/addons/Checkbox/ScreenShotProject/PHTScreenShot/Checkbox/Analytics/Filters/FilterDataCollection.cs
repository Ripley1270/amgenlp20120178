﻿namespace Checkbox.Analytics.Filters
{
    using Checkbox.Analytics.Filters.Configuration;
    using Prezza.Framework.Data;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    public abstract class FilterDataCollection
    {
        private DataSet _filterData;
        private List<Checkbox.Analytics.Filters.Configuration.FilterData> _filterDataObjects;

        protected FilterDataCollection()
        {
            this.InitializeData();
        }

        public virtual void AddFilter(Checkbox.Analytics.Filters.Configuration.FilterData filter)
        {
            if (this._filterDataObjects != null)
            {
                if (this._filterDataObjects.Contains(filter))
                {
                    this._filterDataObjects.Remove(filter);
                    this._filterDataObjects.Add(filter);
                }
                else
                {
                    if (!filter.ID.HasValue || (filter.ID <= 0))
                    {
                        filter.Save();
                    }
                    DataRow row = this.FilterMapTable.NewRow();
                    row["FilterID"] = filter.ID;
                    this.FilterMapTable.Rows.Add(row);
                    this._filterDataObjects.Add(filter);
                }
            }
        }

        public virtual void AddFilter(int filterId)
        {
            Checkbox.Analytics.Filters.Configuration.FilterData filterData = FilterFactory.GetFilterData(filterId);
            if (filterData != null)
            {
                this.AddFilter(filterData);
            }
        }

        public virtual void DeleteFilter(Checkbox.Analytics.Filters.Configuration.FilterData filter)
        {
            if (filter != null)
            {
                this.DeleteFilterMap(filter.ID.Value);
                if (this._filterDataObjects.Contains(filter))
                {
                    this._filterDataObjects.Remove(filter);
                }
            }
        }

        public virtual void DeleteFilter(int filterID)
        {
            Checkbox.Analytics.Filters.Configuration.FilterData filterData = FilterFactory.GetFilterData(filterID);
            this.DeleteFilter(filterData);
        }

        protected virtual void DeleteFilterMap(int filterID)
        {
            DataRow[] rowArray = this.FilterMapTable.Select("FilterID = " + filterID, null, DataViewRowState.CurrentRows);
            if (rowArray.Length > 0)
            {
                rowArray[0].Delete();
            }
        }

        protected virtual DBCommandWrapper GetDeleteFilterMapCommand(Database db)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_Filter_DeleteMap");
            storedProcCommandWrapper.AddInParameter("FilterID", DbType.Int32, "FilterID", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("Parentid", DbType.Int32, this.ParentID);
            storedProcCommandWrapper.AddInParameter("ParentType", DbType.String, this.ParentType);
            return storedProcCommandWrapper;
        }

        public List<Checkbox.Analytics.Filters.Configuration.FilterData> GetFilterDataObjects()
        {
            return this._filterDataObjects;
        }

        public List<Checkbox.Analytics.Filters.Filter> GetFilters(string languageCode)
        {
            List<Checkbox.Analytics.Filters.Filter> list = new List<Checkbox.Analytics.Filters.Filter>();
            foreach (Checkbox.Analytics.Filters.Configuration.FilterData data in this._filterDataObjects)
            {
                list.Add(data.CreateFilter(languageCode));
            }
            return list;
        }

        protected virtual DBCommandWrapper GetInsertFilterMapCommand(Database db)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_Filter_InsertMap");
            storedProcCommandWrapper.AddInParameter("FilterID", DbType.Int32, "FilterID", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("ParentID", DbType.Int32, this.ParentID);
            storedProcCommandWrapper.AddInParameter("ParentType", DbType.String, this.ParentType);
            return storedProcCommandWrapper;
        }

        public void Import(DataSet ds, int parentID)
        {
            this._filterDataObjects.Clear();
            this.FilterMapTable.Clear();
            if ((ds != null) && ds.Tables.Contains(this.FilterMapTableName))
            {
                foreach (DataRow row in ds.Tables[this.FilterMapTableName].Select(string.Concat(new object[] { "ParentID = ", parentID, " AND ParentType = '", this.ParentType, "'" }), null, DataViewRowState.CurrentRows))
                {
                    int? defaultValue = null;
                    int? nullable = DbUtility.GetValueFromDataRow<int?>(row, "FilterID", defaultValue);
                    if (nullable.HasValue)
                    {
                        Checkbox.Analytics.Filters.Configuration.FilterData filterData = FilterFactory.GetFilterData(nullable.Value);
                        if (filterData != null)
                        {
                            this.AddFilter(filterData);
                        }
                    }
                }
            }
        }

        protected virtual void InitializeData()
        {
            this._filterDataObjects = new List<Checkbox.Analytics.Filters.Configuration.FilterData>();
            this._filterData = new DataSet();
            DataTable table = new DataTable();
            table.Columns.Add("FilterID", typeof(int));
            table.Columns.Add("ParentID", typeof(int));
            table.Columns.Add("ParentType", typeof(string));
            table.TableName = this.FilterMapTableName;
            this._filterData.Tables.Add(table);
        }

        public DataSet Load(int parentID)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Filters_Get");
            storedProcCommandWrapper.AddInParameter("ParentID", DbType.Int32, parentID);
            storedProcCommandWrapper.AddInParameter("ParentType", DbType.String, this.ParentType);
            DataSet set = database.ExecuteDataSet(storedProcCommandWrapper);
            if (set.Tables.Count == 1)
            {
                set.Tables[0].TableName = this.FilterMapTableName;
            }
            this._filterDataObjects.Clear();
            this.FilterMapTable.Clear();
            if (set.Tables.Contains(this.FilterMapTableName))
            {
                foreach (DataRow row in set.Tables[this.FilterMapTableName].Select("ParentID = " + parentID, null, DataViewRowState.CurrentRows))
                {
                    if (row["FilterID"] != DBNull.Value)
                    {
                        Checkbox.Analytics.Filters.Configuration.FilterData item = FilterFactory.CreateFilterData(DbUtility.GetValueFromDataRow<int>(row, "FilterID", -1));
                        if (item != null)
                        {
                            DataSet configurationDataSet = item.GetConfigurationDataSet();
                            item.Load(configurationDataSet);
                            this._filterDataObjects.Add(item);
                            this.FilterMapTable.ImportRow(row);
                            set.Merge(configurationDataSet);
                        }
                    }
                }
            }
            return set;
        }

        public void Save(IDbTransaction t)
        {
            Database db = DatabaseFactory.CreateDatabase();
            foreach (Checkbox.Analytics.Filters.Configuration.FilterData data in this._filterDataObjects)
            {
                data.Save(t);
            }
            db.UpdateDataSet(this._filterData, this.FilterMapTableName, this.GetInsertFilterMapCommand(db), null, this.GetDeleteFilterMapCommand(db), t);
        }

        protected DataSet FilterData
        {
            get
            {
                return this._filterData;
            }
        }

        protected virtual DataTable FilterMapTable
        {
            get
            {
                if (this._filterData.Tables.Contains(this.FilterMapTableName))
                {
                    return this._filterData.Tables[this.FilterMapTableName];
                }
                return null;
            }
        }

        public virtual string FilterMapTableName
        {
            get
            {
                return "FilterMap";
            }
        }

        public int ParentID { get; set; }

        public virtual string ParentType { get; set; }
    }
}

