﻿namespace Checkbox.Security.Data
{
    using Checkbox.Security;
    using Prezza.Framework.Data;
    using Prezza.Framework.Data.QueryCriteria;
    using Prezza.Framework.Data.QueryParameters;
    using Prezza.Framework.Security;
    using System;

    internal static class SelectAllIdentitiesNotInAclQuery
    {
        public static SelectQuery GetQuery(AccessControlList acl)
        {
            SelectQuery query = new SelectQuery("ckbx_Credential");
            query.AddAllParameter("ckbx_Credential");
            SelectQuery subQuery = SelectIdentitiesInAclQuery.GetQuery(acl);
            SelectQuery query3 = SelectRolesWithPermissionQuery.GetQuery(PermissionJoin.Any, new string[] { "Group.View" });
            query.AddCriterion(new QueryCriterion(new SelectParameter("UniqueIdentifier", string.Empty, "ckbx_Credential"), CriteriaOperator.NotIn, new SubqueryParameter(subQuery)));
            query.AddCriterion(new QueryCriterion(new SelectParameter("UniqueIdentifier", string.Empty, "ckbx_Credential"), CriteriaOperator.In, new SubqueryParameter(query3)));
            return query;
        }

        public static SelectQuery GetQuery(AccessControlList acl, string[] rolePermissions)
        {
            SelectQuery query = new SelectQuery("ckbx_Credential");
            query.AddAllParameter("ckbx_Credential");
            SelectQuery subQuery = SelectIdentitiesInAclQuery.GetQuery(acl);
            SelectQuery query3 = SelectRolesWithPermissionQuery.GetQuery(PermissionJoin.Any, rolePermissions);
            query.AddCriterion(new QueryCriterion(new SelectParameter("UniqueIdentifier", string.Empty, "ckbx_Credential"), CriteriaOperator.NotIn, new SubqueryParameter(subQuery)));
            query.AddCriterion(new QueryCriterion(new SelectParameter("UniqueIdentifier", string.Empty, "ckbx_Credential"), CriteriaOperator.In, new SubqueryParameter(query3)));
            return query;
        }
    }
}

