﻿namespace Checkbox.Forms.Logic.Configuration
{
    using Checkbox.Common;
    using Checkbox.Forms;
    using Checkbox.Forms.Items.Configuration;
    using Prezza.Framework.Common;
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Data;
    using System.Text;

    [Serializable]
    public class MatrixItemOperandData : ItemOperandData
    {
        private int? _parentItemID;

        public MatrixItemOperandData(ResponseTemplate context) : this(context, null, null)
        {
        }

        public MatrixItemOperandData(ResponseTemplate context, int? itemID, int? parentItemID) : base(context, itemID)
        {
            this._parentItemID = parentItemID;
        }

        protected override void Create(IDbTransaction t)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Rules_InsertOperand");
            storedProcCommandWrapper.AddInParameter("TypeName", DbType.String, base.GetType().FullName);
            storedProcCommandWrapper.AddInParameter("TypeAssembly", DbType.String, base.GetType().Assembly.GetName().Name);
            storedProcCommandWrapper.AddOutParameter("OperandID", DbType.Int32, 4);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
            base.ID = new int?((int) storedProcCommandWrapper.GetParameterValue("OperandID"));
            storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemOperand_Create");
            storedProcCommandWrapper.AddInParameter("OperandID", DbType.Int32, base.ID.Value);
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ItemId);
            storedProcCommandWrapper.AddInParameter("ParentItemID", DbType.Int32, this._parentItemID);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        protected override void LoadFromDataRow(DataRow data)
        {
            ArgumentValidation.CheckForNullReference(data, "data");
            base.LoadFromDataRow(data);
            try
            {
                this._parentItemID = new int?((int) data["ParentItemID"]);
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessProtected"))
                {
                    throw;
                }
            }
        }

        public override string ToString()
        {
            ItemData item = base.Context.GetItem(this._parentItemID.Value);
            StringBuilder builder = new StringBuilder();
            StringBuilder builder2 = new StringBuilder();
            foreach (TemplatePage page in base.Context.TemplatePages)
            {
                if (page.Items.Contains(item))
                {
                    builder.Append(page.Position.ToString());
                    builder.Append(".");
                    builder.Append((page.Items.IndexOf(item) + 1).ToString());
                    builder.Append(" ");
                    break;
                }
            }
            builder2.Append(builder);
            try
            {
                MatrixItemTextDecorator decorator = (MatrixItemTextDecorator) item.CreateTextDecorator(base.Context.EditLanguage);
                if (Utilities.IsNullOrEmpty(decorator.Text))
                {
                    builder2.Append(Utilities.StripHtml(item.Alias, 0x40));
                }
                else
                {
                    builder2.Append(Utilities.StripHtml(decorator.Text, 0x40));
                }
                DataRow row = ((MatrixItemData) item).MatrixItems.Select("ItemID=" + base.ItemId.Value)[0];
                int num = (int) row["Row"];
                int column = (int) row["Column"];
                string str = Utilities.StripHtml(decorator.GetRowText(num), 0x40);
                string str2 = Utilities.StripHtml(decorator.GetColumnText(column), 0x40);
                builder2.Append(" -- ");
                builder2.Append(str);
                builder2.Append(" -- ");
                builder2.Append(str2);
            }
            catch
            {
                return builder2.ToString();
            }
            return builder2.ToString();
        }
    }
}

