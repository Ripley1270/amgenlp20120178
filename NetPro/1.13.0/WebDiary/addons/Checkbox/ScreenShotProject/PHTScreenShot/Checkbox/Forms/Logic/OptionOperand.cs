﻿namespace Checkbox.Forms.Logic
{
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class OptionOperand : Operand
    {
        public OptionOperand(int optionId)
        {
            this.OptionId = optionId;
        }

        public override OperandValue CreateOperandValue()
        {
            return new SelectOperandValue();
        }

        protected override object ProtectedGetValue(RuleEventTrigger trigger)
        {
            return this.OptionId;
        }

        public int OptionId { get; private set; }
    }
}

