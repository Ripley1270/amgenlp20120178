﻿namespace Checkbox.Forms.Validation
{
    using Checkbox.Forms.Items;
    using Checkbox.Globalization.Text;
    using System;

    public class AnswerLengthValidator : Validator<TextItem>
    {
        private int? _maxLength;

        public override string GetMessage(string languageCode)
        {
            string text = TextManager.GetText("/validationMessages/text/length", languageCode);
            if ((text != null) && this.MaxLength.HasValue)
            {
                text = text.Replace("{max}", this.MaxLength.ToString());
            }
            return text;
        }

        public override bool Validate(TextItem input)
        {
            if (input.HasAnswer)
            {
                string answer = input.GetAnswer();
                if ((answer != null) && input.MaxLength.HasValue)
                {
                    this._maxLength = new int?(input.MaxLength.Value);
                    return (answer.Length <= input.MaxLength.Value);
                }
            }
            return true;
        }

        public int? MaxLength
        {
            get
            {
                return this._maxLength;
            }
        }
    }
}

