﻿namespace Checkbox.Forms.Items.Configuration
{
    using Checkbox.Forms.Items;
    using Checkbox.Globalization.Text;
    using Prezza.Framework.Data;
    using System;
    using System.Data;

    [Serializable]
    public class ScoreMessageItemData : MessageItemData
    {
        private DataSet _containerSet;

        public ScoreMessageItemData()
        {
            this.InitializeData();
        }

        public void AddScoreRange(double? lowScore, double? highScore)
        {
            if (!lowScore.HasValue && !highScore.HasValue)
            {
                this.ScoreRangesTable.Clear();
            }
            else
            {
                DataRow row = this.ScoreRangesTable.NewRow();
                if (!lowScore.HasValue)
                {
                    row["LowScore"] = DBNull.Value;
                }
                else
                {
                    row["lowScore"] = lowScore;
                }
                if (!highScore.HasValue)
                {
                    row["HighScore"] = DBNull.Value;
                }
                else
                {
                    row["HighScore"] = highScore;
                }
                this.ScoreRangesTable.Rows.Add(row);
                this.AdjustScores();
            }
        }

        public void AdjustScores()
        {
        }

        protected override void Create(IDbTransaction t)
        {
            base.Create(t);
            if (base.ID <= 0)
            {
                throw new ApplicationException("No DataID specified to create score message item data.");
            }
            Database db = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_ItemData_InsertScoreMessage");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            db.ExecuteNonQuery(storedProcCommandWrapper, t);
            this.UpdateScoreMessages(db, t);
        }

        protected override void CreateDataRelationsInternal(DataSet ds)
        {
            base.CreateDataRelationsInternal(ds);
            if ((ds.Tables.Contains(ScoreRangeTableName) && ds.Tables.Contains(this.DataTableName)) && !ds.Tables[ScoreRangeTableName].Constraints.Contains("FK_" + this.DataTableName + "_" + ScoreRangeTableName))
            {
                DataRelation relation = new DataRelation(this.DataTableName + "_" + ScoreRangeTableName, ds.Tables[this.DataTableName].Columns[this.IdentityColumnName], ds.Tables[ScoreRangeTableName].Columns["ItemID"]);
                ds.Tables[ScoreRangeTableName].Constraints.Add(new ForeignKeyConstraint("FK_" + this.DataTableName + "_" + ScoreRangeTableName, ds.Tables[this.DataTableName].Columns[this.IdentityColumnName], ds.Tables[ScoreRangeTableName].Columns["ItemID"]));
                relation.Nested = true;
                ds.Relations.Add(relation);
            }
        }

        protected override Item CreateItem()
        {
            return new ScoreMessageItem();
        }

        public override ItemTextDecorator CreateTextDecorator(string languageCode)
        {
            return new ScoreMessageTextDecorator(this, languageCode);
        }

        protected override DataSet GetConcreteConfigurationDataSet()
        {
            if (!base.ID.HasValue || (base.ID <= 0))
            {
                throw new Exception("Unable to get score message item data.  DataID is NULL or <= 0.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_GetScoreMessage");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            string[] tableNames = new string[] { this.DataTableName, ScoreRangeTableName };
            DataSet dataSet = new DataSet();
            database.LoadDataSet(storedProcCommandWrapper, dataSet, tableNames);
            return dataSet;
        }

        protected virtual DBCommandWrapper GetDeleteScoreMessageCommand(Database db)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_ItemData_SM_DeleteMessage");
            storedProcCommandWrapper.AddInParameter("ScoreMessageID", DbType.Int32, "ScoreMessageID", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            return storedProcCommandWrapper;
        }

        protected virtual DBCommandWrapper GetInsertScoreMessageCommand(Database db)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_ItemData_SM_AddMessage");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("LowScore", DbType.Int32, "LowScore", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("HighScore", DbType.Int32, "HighScore", DataRowVersion.Current);
            storedProcCommandWrapper.AddParameter("ScoreMessageID", DbType.Int32, ParameterDirection.Output, "ScoreMessageID", DataRowVersion.Current, null);
            return storedProcCommandWrapper;
        }

        public string GetMessageTextID(int scoreMessageID)
        {
            DataRow[] rowArray = this.ScoreRangesTable.Select("ScoreMessageID = " + scoreMessageID, null, DataViewRowState.CurrentRows);
            if ((rowArray.Length > 0) && (rowArray[0]["ScoreMessageID"] != DBNull.Value))
            {
                return this.GetTextID(rowArray[0]["ScoreMessageID"].ToString());
            }
            return string.Empty;
        }

        public string GetMessageTextID(double? lowScore, double? highScore)
        {
            string str;
            if (!lowScore.HasValue && !highScore.HasValue)
            {
                return this.GetTextID("default");
            }
            if (lowScore.HasValue && highScore.HasValue)
            {
                str = string.Concat(new object[] { "LowScore = ", lowScore, " AND HighScore = ", highScore });
            }
            else if (!lowScore.HasValue)
            {
                str = "LowScore IS NULL AND HighScore = " + highScore;
            }
            else
            {
                str = "LowScore = " + lowScore + " AND HighScore IS NULL";
            }
            DataRow[] rowArray = this.ScoreRangesTable.Select(str, "LowScore DESC", DataViewRowState.CurrentRows);
            if ((rowArray.Length > 0) && (rowArray[0]["ScoreMessageID"] != DBNull.Value))
            {
                int num = Convert.ToInt32(rowArray[0]["ScoreMessageID"]);
                return this.GetTextID(num.ToString());
            }
            return string.Empty;
        }

        protected virtual DBCommandWrapper GetUpdateScoreMessageCommand(Database db)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_ItemData_SM_UpdateMessage");
            storedProcCommandWrapper.AddInParameter("ScoreMessageID", DbType.Int32, "ScoreMessageID", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("LowScore", DbType.Int32, "LowScore", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("HighScore", DbType.Int32, "HighScore", DataRowVersion.Current);
            return storedProcCommandWrapper;
        }

        protected virtual void InitializeData()
        {
            DataTable table = new DataTable {
                TableName = ScoreRangeTableName
            };
            table.Columns.Add("ScoreMessageID", typeof(int));
            table.Columns.Add("ItemID", typeof(int));
            table.Columns.Add("LowScore", typeof(double));
            table.Columns.Add("HighScore", typeof(double));
            table.Columns["ScoreMessageID"].AutoIncrement = true;
            table.Columns["ScoreMessageID"].AutoIncrementSeed = -1L;
            table.Columns["ScoreMessageID"].AutoIncrementStep = -1L;
            this._containerSet = new DataSet();
            this._containerSet.Tables.Add(table);
        }

        public override void Load(DataSet data)
        {
            base.Load(data);
            if ((data != null) && data.Tables.Contains(ScoreRangeTableName))
            {
                this.ScoreRangesTable.Clear();
                if (base.ID.HasValue)
                {
                    foreach (DataRow row in data.Tables[ScoreRangeTableName].Select("ItemID = " + base.ID, "LowScore ASC", DataViewRowState.CurrentRows))
                    {
                        this.ScoreRangesTable.ImportRow(row);
                    }
                }
            }
        }

        public override DataSet LoadTextData()
        {
            DataSet ds = base.LoadTextData();
            foreach (DataRow row in this.ScoreRangesTable.Select(null, null, DataViewRowState.CurrentRows))
            {
                if (row["ScoreMessageID"] != DBNull.Value)
                {
                    int scoreMessageID = DbUtility.GetValueFromDataRow<int>(row, "ScoreMessageId", -1);
                    double? defaultValue = null;
                    double? lowScore = DbUtility.GetValueFromDataRow<double?>(row, "LowScore", defaultValue);
                    double? nullable4 = null;
                    double? highScore = DbUtility.GetValueFromDataRow<double?>(row, "HighScore", nullable4);
                    this.MergeTextData(ds, TextManager.GetTextData(this.GetMessageTextID(scoreMessageID)), scoreMessageID, lowScore, highScore);
                }
            }
            return ds;
        }

        protected virtual void MergeTextData(DataSet ds, DataTable data, int scoreMessageID, double? lowScore, double? highScore)
        {
            if (data != null)
            {
                if (!data.Columns.Contains("LowScore"))
                {
                    data.Columns.Add("LowScore", typeof(double));
                }
                if (!data.Columns.Contains("HighScore"))
                {
                    data.Columns.Add("HighScore", typeof(double));
                }
                foreach (DataRow row in data.Select(null, null, DataViewRowState.CurrentRows))
                {
                    if (lowScore.HasValue)
                    {
                        row["LowScore"] = lowScore;
                    }
                    if (highScore.HasValue)
                    {
                        row["HighScore"] = highScore;
                    }
                }
                this.MergeTextData(ds, data, this.TextTableName, scoreMessageID.ToString(), this.TextIDPrefix, base.ID.Value);
            }
        }

        public void RemoveScoreRange(int scoreMessageID)
        {
            DataRow[] rowArray = this.ScoreRangesTable.Select("ScoreMessageID = " + scoreMessageID, null, DataViewRowState.CurrentRows);
            if (rowArray.Length > 0)
            {
                rowArray[0].Delete();
                this.AdjustScores();
            }
        }

        protected override void Update(IDbTransaction t)
        {
            base.Update(t);
            if (base.ID <= 0)
            {
                throw new ApplicationException("No DataID specified to update score message item data.");
            }
            Database db = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_ItemData_UpdateScoreMessage");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            db.ExecuteNonQuery(storedProcCommandWrapper, t);
            this.UpdateScoreMessages(db, t);
        }

        protected virtual void UpdateScoreMessages(Database db, IDbTransaction t)
        {
            db.UpdateDataSet(this._containerSet, ScoreRangeTableName, this.GetInsertScoreMessageCommand(db), this.GetUpdateScoreMessageCommand(db), this.GetDeleteScoreMessageCommand(db), t);
        }

        public void UpdateScoreRange(int scoreMessageID, double? lowScore, double? highScore)
        {
            DataRow[] rowArray = this.ScoreRangesTable.Select("ScoreMessageID = " + scoreMessageID, null, DataViewRowState.CurrentRows);
            if (rowArray.Length > 0)
            {
                rowArray[0]["LowScore"] = lowScore.HasValue ? ((object) lowScore) : ((object) DBNull.Value);
                rowArray[0]["HighScore"] = highScore.HasValue ? ((object) highScore) : ((object) DBNull.Value);
            }
        }

        public DataTable ScoreRanges
        {
            get
            {
                DataTable table = this.ScoreRangesTable.Copy();
                table.Columns.Add("ScoreText", typeof(string));
                foreach (DataRow row in table.Select(null, null, DataViewRowState.CurrentRows))
                {
                    if ((row["LowScore"] != DBNull.Value) || (row["HighScore"] != DBNull.Value))
                    {
                        if (row["LowScore"] == DBNull.Value)
                        {
                            row["ScoreText"] = " <= " + row["HighScore"];
                        }
                        else if (row["HighScore"] == DBNull.Value)
                        {
                            row["ScoreText"] = " >= " + row["LowScore"];
                        }
                        else
                        {
                            row["ScoreText"] = row["LowScore"] + " - " + row["HighScore"];
                        }
                    }
                }
                return table;
            }
        }

        protected DataTable ScoreRangesTable
        {
            get
            {
                if (this._containerSet.Tables.Contains(ScoreRangeTableName))
                {
                    return this._containerSet.Tables[ScoreRangeTableName];
                }
                return null;
            }
        }

        protected static string ScoreRangeTableName
        {
            get
            {
                return "ScoreRanges";
            }
        }

        public override string TextIDPrefix
        {
            get
            {
                return "scoreMessageData";
            }
        }
    }
}

