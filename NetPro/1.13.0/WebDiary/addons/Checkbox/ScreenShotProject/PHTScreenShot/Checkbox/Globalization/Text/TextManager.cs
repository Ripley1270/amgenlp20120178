﻿namespace Checkbox.Globalization.Text
{
    using Checkbox.Common;
    using Checkbox.Management;
    using Prezza.Framework.Common;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;

    public static class TextManager
    {
        public static void ExportAllTexts(TextWriter writer)
        {
            ((IImportExportTextProvider) ExportTextBase(writer)).ExportAllTexts(writer);
        }

        public static void ExportFilteredTexts(TextWriter writer, string[] languageCodes, params string[] partialTextId)
        {
            ((IImportExportTextProvider) ExportTextBase(writer)).ExportFilteredTexts(writer, languageCodes, partialTextId);
        }

        private static ITextProvider ExportTextBase(TextWriter writer)
        {
            ArgumentValidation.CheckForNullReference(writer, "reader");
            ITextProvider textProvider = TextFactory.GetTextProvider();
            if (!MultiLanguageEnabled)
            {
                throw new Exception("Unable to export.  Text provider does not support exporting data.");
            }
            return textProvider;
        }

        public static void ExportTextsById(TextWriter writer, string[] languageCodes, params string[] textIds)
        {
            ((IImportExportTextProvider) ExportTextBase(writer)).ExportTextsById(writer, languageCodes, textIds);
        }

        public static DataTable GetAllMatchingTexts(params string[] matchExpressions)
        {
            return TextFactory.GetTextProvider().GetMatchingTextData(matchExpressions);
        }

        public static Dictionary<string, string> GetAllTexts(string textIdentifier)
        {
            return TextFactory.GetTextProvider().GetAllTexts(textIdentifier);
        }

        public static Dictionary<string, string> GetEnumLocalizedValues(Type enumType, string languageCode)
        {
            List<string> list = Utilities.ListEnumValues(enumType);
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            foreach (string str in list)
            {
                dictionary[str] = GetText("/enum/" + enumType.Name + "/" + str, languageCode);
            }
            return dictionary;
        }

        public static string GetText(string textIdentifier, string languageCode)
        {
            if (!Utilities.IsNullOrEmpty(textIdentifier) && !Utilities.IsNullOrEmpty(languageCode))
            {
                return TextFactory.GetTextProvider().GetText(textIdentifier, languageCode);
            }
            return string.Empty;
        }

        public static string GetText(string textIdentifier, string languageCode, string defaultText, params string[] alternateLanguages)
        {
            if (!Utilities.IsNullOrEmpty(textIdentifier) && !Utilities.IsNullOrEmpty(languageCode))
            {
                string text = TextFactory.GetTextProvider().GetText(textIdentifier, languageCode);
                if (!Utilities.IsNullOrEmpty(text))
                {
                    return text;
                }
                foreach (string str2 in alternateLanguages)
                {
                    text = TextFactory.GetTextProvider().GetText(textIdentifier, str2);
                    if (Utilities.IsNotNullOrEmpty(text))
                    {
                        return text;
                    }
                }
            }
            return defaultText;
        }

        public static DataTable GetTextData(string textIdentifier)
        {
            return TextFactory.GetTextProvider().GetTextData(textIdentifier);
        }

        public static void ImportTexts(TextReader reader)
        {
            ArgumentValidation.CheckForNullReference(reader, "reader");
            ITextProvider textProvider = TextFactory.GetTextProvider();
            if (!MultiLanguageEnabled)
            {
                throw new Exception("Unable to import.  Text provider does not support exporting data.");
            }
            ((IImportExportTextProvider) textProvider).ImportTexts(reader);
        }

        public static void SetText(string textIdentifier, string languageCode, string textValue)
        {
            ArgumentValidation.CheckForEmptyString(textIdentifier, "textIdentifier");
            ArgumentValidation.CheckForEmptyString(languageCode, "languageCode");
            if (textValue == null)
            {
                textValue = string.Empty;
            }
            TextFactory.GetTextProvider().SetText(textIdentifier, languageCode, textValue);
        }

        public static string[] ApplicationLanguages
        {
            get
            {
                if (MultiLanguageEnabled)
                {
                    return TextFactory.ApplicationLanguages;
                }
                return new string[] { DefaultLanguage };
            }
        }

        public static string DefaultLanguage
        {
            get
            {
                return TextFactory.DefaultLanguage;
            }
        }

        public static bool MultiLanguageEnabled
        {
            get
            {
                return ((TextFactory.GetTextProvider() is IImportExportTextProvider) && ApplicationManager.AppSettings.AllowMultiLanguage);
            }
        }

        public static string[] SurveyLanguages
        {
            get
            {
                if (MultiLanguageEnabled)
                {
                    return TextFactory.SurveyLanguages;
                }
                return new string[] { DefaultLanguage };
            }
        }
    }
}

