﻿namespace Checkbox.Analytics.Items.Configuration
{
    using System;

    [Serializable]
    public enum OtherOption
    {
        Aggregate = 1,
        AggregateAndDisplay = 3,
        Display = 2
    }
}

