﻿namespace Checkbox.Forms.Items
{
    using System;

    [Serializable]
    public enum HiddenVariableSource
    {
        Cookie = 3,
        QueryString = 1,
        Session = 2
    }
}

