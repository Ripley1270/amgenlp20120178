﻿namespace Checkbox.Forms.Validation
{
    using Checkbox.Common;
    using Checkbox.Globalization.Text;
    using Checkbox.Management;
    using System;
    using System.Collections.Generic;

    public class EmailValidator : RegularExpressionValidator
    {
        public EmailValidator()
        {
            base._regex = @"^([a-zA-Z0-9]+)@([a-zA-Z0-9\-]+\.)+[a-zA-Z]{2,}$";
        }

        private bool ContainsConsecutive(IEnumerable<char> source, char pattern)
        {
            int num = 0;
            foreach (char ch in source)
            {
                if (num > 1)
                {
                    return true;
                }
                if (ch == pattern)
                {
                    num++;
                }
                else
                {
                    num = 0;
                }
            }
            return false;
        }

        public override string GetMessage(string languageCode)
        {
            return TextManager.GetText("/validationMessages/regex/email", languageCode);
        }

        public override bool Validate(string input)
        {
            if (!ApplicationManager.AppSettings.EnableEmailAddressValidation)
            {
                return true;
            }
            if (Utilities.IsNullOrEmpty(input))
            {
                return false;
            }
            string[] strArray = input.Split(new char[] { '@' });
            if (strArray.Length != 2)
            {
                return false;
            }
            string source = strArray[0] ?? string.Empty;
            string str2 = strArray[1] ?? string.Empty;
            foreach (string str3 in ApplicationManager.AppSettings.EmailAddressOptionalCharacters.Split(new char[] { ',' }))
            {
                if (".".Equals(str3) && ((source.StartsWith(".") || source.EndsWith(".")) || this.ContainsConsecutive(source, '.')))
                {
                    return false;
                }
                source = source.Replace(str3, string.Empty);
            }
            if (str2.StartsWith("[") && !str2.EndsWith("]"))
            {
                return false;
            }
            str2 = str2.Replace("[", string.Empty).Replace("]", string.Empty);
            return this.RegularExpression.IsMatch(string.Format("{0}@{1}", source, str2));
        }
    }
}

