﻿namespace Checkbox.Security
{
    using Checkbox.Globalization.Text;
    using Checkbox.Management;
    using Checkbox.Users;
    using Prezza.Framework.Common;
    using Prezza.Framework.Data;
    using Prezza.Framework.Data.QueryCriteria;
    using Prezza.Framework.Data.QueryParameters;
    using Prezza.Framework.Security;
    using Prezza.Framework.Security.Principal;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Security.Principal;

    public static class RoleManager
    {
        private static readonly Dictionary<string, List<Role>> _identityRoles;
        private static readonly Dictionary<string, int> _roleIds = new Dictionary<string, int>();
        private static readonly Dictionary<string, List<string>> _rolePermissions;

        static RoleManager()
        {
            if (ApplicationManager.AppSettings.CacheVolatileDataInApplication)
            {
                _identityRoles = new Dictionary<string, List<Role>>(StringComparer.InvariantCultureIgnoreCase);
            }
            if (ApplicationManager.AppSettings.CacheRolePermissions)
            {
                _rolePermissions = new Dictionary<string, List<string>>(StringComparer.InvariantCultureIgnoreCase);
            }
        }

        public static void AddIdentityRole(IIdentity identity, int roleID)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Identity_AddToRole");
            storedProcCommandWrapper.AddInParameter("UniqueIdentifier", DbType.String, identity.Name);
            storedProcCommandWrapper.AddInParameter("RoleID", DbType.Int32, roleID);
            database.ExecuteNonQuery(storedProcCommandWrapper);
            if (ApplicationManager.AppSettings.CacheVolatileDataInApplication && _identityRoles.ContainsKey(identity.Name))
            {
                _identityRoles.Remove(identity.Name);
            }
        }

        public static string[] GetAllRoles()
        {
            if (!ApplicationManager.UseSimpleSecurity)
            {
                SelectQuery query = new SelectQuery("ckbx_Role");
                query.AddParameter("RoleName", string.Empty, string.Empty);
                query.AddSortField(new SortOrder("RoleName"));
                ArrayList list = new ArrayList();
                Database database = DatabaseFactory.CreateDatabase();
                DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
                using (IDataReader reader = database.ExecuteReader(sqlStringCommandWrapper))
                {
                    try
                    {
                        while (reader.Read())
                        {
                            list.Add(reader["RoleName"]);
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
                return (string[]) list.ToArray(typeof(string));
            }
            ArrayList list2 = new ArrayList();
            list2.Add(TextManager.GetText("/simpleSecurity/systemAdministrator", TextManager.DefaultLanguage));
            list2.Add(TextManager.GetText("/simpleSecurity/respondent", TextManager.DefaultLanguage));
            list2.Add(TextManager.GetText("/simpleSecurity/surveyEditor", TextManager.DefaultLanguage));
            return (string[]) list2.ToArray(typeof(string));
        }

        public static DataTable GetAllRolesTable()
        {
            if (!ApplicationManager.UseSimpleSecurity)
            {
                SelectQuery query = new SelectQuery("ckbx_Role");
                query.AddAllParameter("ckbx_Role");
                query.AddSortField(new SortOrder("RoleName"));
                Database database = DatabaseFactory.CreateDatabase();
                DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
                DataSet set = database.ExecuteDataSet(sqlStringCommandWrapper);
                if ((set != null) && (set.Tables.Count > 0))
                {
                    return set.Tables[0];
                }
                return null;
            }
            DataTable table = new DataTable();
            table.Columns.Add("RoleID", typeof(int));
            table.Columns.Add("RoleName", typeof(string));
            table.Columns.Add("RoleDescription", typeof(string));
            table.Rows.Add(new object[] { 1, TextManager.GetText("/simpleSecurity/systemAdministrator", TextManager.DefaultLanguage), "Super User" });
            table.Rows.Add(new object[] { 4, TextManager.GetText("/simpleSecurity/respondent", TextManager.DefaultLanguage), "Takes surveys" });
            table.Rows.Add(new object[] { 7, TextManager.GetText("/simpleSecurity/surveyEditor", TextManager.DefaultLanguage), "Creates and edits surveys" });
            return table;
        }

        public static List<string> GetAllUserNamesWithPermissions(ExtendedPrincipal currentUser, string[] permissions)
        {
            SelectQuery query = new SelectQuery("ckbx_IdentityRoles");
            query.AddTableJoin("ckbx_Role", QueryJoinType.Inner, "RoleID", "ckbx_IdentityRoles", "RoleID");
            query.AddTableJoin("ckbx_RolePermissions", QueryJoinType.Left, "RoleID", "ckbx_Role", "RoleID");
            query.AddTableJoin("ckbx_Permission", QueryJoinType.Left, "PermissionID", "ckbx_RolePermissions", "PermissionID");
            query.AddLiteralParameter("Distinct UniqueIdentifier", "UniqueIdentifier");
            foreach (string str in permissions)
            {
                query.AddCriterion(new QueryCriterion(new SelectParameter("PermissionName", null, "ckbx_Permission"), CriteriaOperator.EqualTo, new LiteralParameter("'" + str + "'")));
            }
            if (currentUser.IsInRole("System Administrator"))
            {
                query.AddCriterion(new QueryCriterion(new SelectParameter("RoleName", null, "ckbx_Role"), CriteriaOperator.EqualTo, new LiteralParameter("'System Administrator'")));
            }
            query.CriteriaJoinType = CriteriaJoinType.Or;
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
            List<string> list = new List<string>();
            using (IDataReader reader = database.ExecuteReader(sqlStringCommandWrapper))
            {
                while (reader.Read())
                {
                    list.Add(DbUtility.GetValueFromDataReader<string>(reader, "UniqueIdentifier", string.Empty));
                }
            }
            return list;
        }

        public static List<string> GetIdentityRoleNames(IIdentity identity)
        {
            ArgumentValidation.CheckForNullReference(identity, "identity");
            return GetIdentityRoleNames(identity.AuthenticationType, GetIdentityRoles(identity));
        }

        private static List<string> GetIdentityRoleNames(string authenticationType, List<Role> identityRoles)
        {
            if (UserManager.IsNetworkUser(authenticationType) && (identityRoles.Count <= 0))
            {
                return new List<string>(ApplicationManager.AppSettings.DefaultRolesForUnAuthenticatedNetworkUsers);
            }
            List<string> list = new List<string>();
            foreach (Role role in identityRoles)
            {
                list.Add(role.Name);
            }
            return list;
        }

        public static List<Role> GetIdentityRoles(IIdentity identity)
        {
            ArgumentValidation.CheckForNullReference(identity, "identity");
            if ((ApplicationManager.AppSettings.CacheVolatileDataInApplication && _identityRoles.ContainsKey(identity.Name)) && (_identityRoles[identity.Name] != null))
            {
                return _identityRoles[identity.Name];
            }
            List<Role> list = LoadIdentityRoles(identity);
            if (ApplicationManager.AppSettings.CacheVolatileDataInApplication)
            {
                _identityRoles[identity.Name] = list;
            }
            return list;
        }

        public static Role GetRole(string roleName)
        {
            List<string> list;
            if (ApplicationManager.AppSettings.CacheRolePermissions && _rolePermissions.ContainsKey(GetRoleCacheKey(roleName)))
            {
                list = _rolePermissions[GetRoleCacheKey(roleName)];
            }
            else
            {
                list = new List<string>();
                Database database = DatabaseFactory.CreateDatabase();
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Role_GetPermissions");
                storedProcCommandWrapper.AddInParameter("RoleName", DbType.String, roleName);
                using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
                {
                    try
                    {
                        while (reader.Read())
                        {
                            list.Add(DbUtility.GetValueFromDataReader<string>(reader, "PermissionName", string.Empty));
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
                if (ApplicationManager.AppSettings.CacheRolePermissions)
                {
                    _rolePermissions[GetRoleCacheKey(roleName)] = list;
                }
            }
            return new Role(list.ToArray()) { Name = roleName, Id = GetRoleID(roleName) };
        }

        private static string GetRoleCacheKey(string roleName)
        {
            return (ApplicationManager.ApplicationDataContext + "_" + roleName).ToLower();
        }

        public static int GetRoleID(string roleName)
        {
            if (_roleIds.ContainsKey(roleName))
            {
                return _roleIds[roleName];
            }
            SelectQuery query = new SelectQuery("ckbx_Role");
            query.AddParameter("RoleID", string.Empty, string.Empty);
            query.AddCriterion(new QueryCriterion(new SelectParameter("RoleName"), CriteriaOperator.EqualTo, new LiteralParameter("'" + roleName + "'")));
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
            using (IDataReader reader = database.ExecuteReader(sqlStringCommandWrapper))
            {
                try
                {
                    if (reader.Read() && (reader["RoleID"] != DBNull.Value))
                    {
                        _roleIds[roleName] = (int) reader["RoleID"];
                        return (int) reader["RoleID"];
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return -1;
        }

        public static bool HasRole(IIdentity identity, string roleNameToCheck)
        {
            ArgumentValidation.CheckForEmptyString(roleNameToCheck, "roleName");
            foreach (string str in GetIdentityRoleNames(identity))
            {
                if (str.Equals(roleNameToCheck, StringComparison.InvariantCultureIgnoreCase))
                {
                    return true;
                }
            }
            return false;
        }

        public static bool HasRoleWithPermission(IIdentity identity, string permission)
        {
            ArgumentValidation.CheckForEmptyString(permission, "permission");
            foreach (Role role in GetIdentityRoles(identity))
            {
                if (role.HasPermission(permission))
                {
                    return true;
                }
            }
            return false;
        }

        public static List<Role> LoadIdentityRoles(IIdentity identity)
        {
            SelectQuery query = new SelectQuery("ckbx_IdentityRoles");
            query.AddAllParameter("ckbx_IdentityRoles");
            query.AddAllParameter("ckbx_Role");
            query.AddTableJoin("ckbx_Role", QueryJoinType.Inner, "RoleID", "ckbx_IdentityRoles", "RoleID");
            query.AddCriterion(new QueryCriterion(new SelectParameter("UniqueIdentifier", string.Empty, "ckbx_IdentityRoles"), CriteriaOperator.EqualTo, new LiteralParameter("'" + identity.Name + "'")));
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
            List<string> list = new List<string>();
            using (IDataReader reader = database.ExecuteReader(sqlStringCommandWrapper))
            {
                try
                {
                    while (reader.Read())
                    {
                        list.Add(DbUtility.GetValueFromDataReader<string>(reader, "RoleName", string.Empty));
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            List<Role> list2 = new List<Role>();
            foreach (string str in list)
            {
                list2.Add(GetRole(str));
            }
            return list2;
        }

        public static void RemoveIdentityRole(IIdentity identity, int roleID)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Identity_RemoveFromRole");
            storedProcCommandWrapper.AddInParameter("UniqueIdentifier", DbType.String, identity.Name);
            storedProcCommandWrapper.AddInParameter("RoleID", DbType.Int32, roleID);
            database.ExecuteNonQuery(storedProcCommandWrapper);
            if (ApplicationManager.AppSettings.CacheVolatileDataInApplication && _identityRoles.ContainsKey(identity.Name))
            {
                _identityRoles.Remove(identity.Name.ToLower());
            }
        }
    }
}

