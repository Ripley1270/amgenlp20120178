﻿namespace Checkbox.Forms.Items.Configuration
{
    using Checkbox.Common.Captcha;
    using Checkbox.Common.Captcha.Image;
    using Checkbox.Forms.Items;
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class CaptchaItemData : LabelledItemData
    {
        private string _textStyles = string.Empty;

        public CaptchaItemData()
        {
            this.AddTextStyle(TextStyleEnum.AncientMosaic.ToString());
            this.MinCodeLength = 5;
            this.MaxCodeLength = 5;
            this.CodeType = Checkbox.Common.Captcha.CodeType.AlphaNumeric;
            this.ImageHeight = 200;
            this.ImageWidth = 250;
            this.ImageFormat = ImageFormatEnum.Gif;
            this.EnableSound = false;
        }

        private void AddParams(DBCommandWrapper command)
        {
            command.AddInParameter("ItemID", DbType.Int32, base.ID);
            command.AddInParameter("MaxCodeLength", DbType.Int32, this.MaxCodeLength);
            command.AddInParameter("MinCodeLength", DbType.Int32, this.MinCodeLength);
            command.AddInParameter("CodeType", DbType.String, this.CodeType.ToString());
            command.AddInParameter("ImageHeight", DbType.Int32, this.ImageHeight);
            command.AddInParameter("ImageWidth", DbType.Int32, this.ImageWidth);
            command.AddInParameter("TextStyles", DbType.String, this._textStyles);
            command.AddInParameter("ImageFormat", DbType.String, this.ImageFormat);
            command.AddInParameter("EnableSound", DbType.Boolean, this.EnableSound);
        }

        public void AddTextStyle(TextStyleEnum textStyle)
        {
            this.AddTextStyle(textStyle.ToString());
        }

        public void AddTextStyle(string textStyleName)
        {
            if (this._textStyles.Trim() == string.Empty)
            {
                this._textStyles = textStyleName;
            }
            else if (!this._textStyles.Contains("," + textStyleName) && !this._textStyles.Contains(textStyleName + ","))
            {
                this._textStyles = this._textStyles + "," + textStyleName;
            }
        }

        protected override void Create(IDbTransaction t)
        {
            base.Create(t);
            if (!base.ID.HasValue || (base.ID <= 0))
            {
                throw new Exception("Unable to create configuration data data for CaptchaItem with null or negative id.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_InsertCaptcha");
            this.AddParams(storedProcCommandWrapper);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        protected override Item CreateItem()
        {
            return new CaptchaItem();
        }

        protected override DataSet GetConcreteConfigurationDataSet()
        {
            if (!base.ID.HasValue || (base.ID <= 0))
            {
                throw new Exception("Unable to load concrete configuration data for CaptchaItem with null or negative id.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_GetCaptcha");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            DataSet concreteConfigurationDataSet = base.GetConcreteConfigurationDataSet();
            database.LoadDataSet(storedProcCommandWrapper, concreteConfigurationDataSet, this.DataTableName);
            return concreteConfigurationDataSet;
        }

        protected override void LoadFromDataRow(DataRow data)
        {
            if (data == null)
            {
                throw new Exception("Unable to load Captcha item data from a null data row.");
            }
            this.MaxCodeLength = DbUtility.GetValueFromDataRow<int>(data, "MaxCodeLength", 5);
            this.MinCodeLength = DbUtility.GetValueFromDataRow<int>(data, "MinCodeLength", 5);
            this.ImageHeight = DbUtility.GetValueFromDataRow<int>(data, "ImageHeight", 200);
            this.ImageWidth = DbUtility.GetValueFromDataRow<int>(data, "ImageWidth", 250);
            this.EnableSound = DbUtility.GetValueFromDataRow<bool>(data, "EnableSound", false);
            if (data["CodeType"] != DBNull.Value)
            {
                this.CodeType = (Checkbox.Common.Captcha.CodeType) Enum.Parse(typeof(Checkbox.Common.Captcha.CodeType), (string) data["CodeType"]);
            }
            if (data["TextStyles"] != DBNull.Value)
            {
                List<string> textStyleNames = new List<string>(((string) data["TextStyles"]).Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries));
                this.SetTextStyles(textStyleNames);
            }
            if (data["ImageFormat"] != DBNull.Value)
            {
                this.ImageFormat = (ImageFormatEnum) Enum.Parse(typeof(ImageFormatEnum), (string) data["ImageFormat"]);
            }
        }

        public void RemoveTextStyle(TextStyleEnum textStyle)
        {
            this.RemoveTextStyle(textStyle.ToString());
        }

        public void RemoveTextStyle(string textStyleName)
        {
            if (string.Compare(textStyleName, this._textStyles, true) == 0)
            {
                this._textStyles = string.Empty;
            }
            else if (this._textStyles.Contains(textStyleName + ","))
            {
                this._textStyles.Replace(textStyleName + ",", string.Empty);
            }
            else if (this._textStyles.Contains("," + textStyleName))
            {
                this._textStyles.Replace("," + textStyleName, string.Empty);
            }
        }

        public void SetTextStyles(List<TextStyleEnum> textStyles)
        {
            this._textStyles = string.Empty;
            foreach (TextStyleEnum enum2 in textStyles)
            {
                this.AddTextStyle(enum2);
            }
        }

        public void SetTextStyles(List<string> textStyleNames)
        {
            this._textStyles = string.Empty;
            foreach (string str in textStyleNames)
            {
                this.AddTextStyle(str);
            }
        }

        protected override void Update(IDbTransaction t)
        {
            base.Update(t);
            if (!base.ID.HasValue || (base.ID <= 0))
            {
                throw new Exception("Unable to update configuration data data for CaptchaItem with null or negative id.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_UpdateCaptcha");
            this.AddParams(storedProcCommandWrapper);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        public Checkbox.Common.Captcha.CodeType CodeType { get; set; }

        public override string DataTableName
        {
            get
            {
                return "CaptchaItemData";
            }
        }

        public bool EnableSound { get; set; }

        public ImageFormatEnum ImageFormat { get; set; }

        public int ImageHeight { get; set; }

        public int ImageWidth { get; set; }

        public int MaxCodeLength { get; set; }

        public int MinCodeLength { get; set; }

        public override string TextIDPrefix
        {
            get
            {
                return "captchaItemData";
            }
        }

        public List<TextStyleEnum> TextStyles
        {
            get
            {
                List<TextStyleEnum> list = new List<TextStyleEnum>();
                foreach (string str in this._textStyles.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries))
                {
                    try
                    {
                        list.Add((TextStyleEnum) Enum.Parse(typeof(TextStyleEnum), str, true));
                    }
                    catch (Exception exception)
                    {
                        ExceptionPolicy.HandleException(exception, "BusinessPublic");
                    }
                }
                return list;
            }
        }
    }
}

