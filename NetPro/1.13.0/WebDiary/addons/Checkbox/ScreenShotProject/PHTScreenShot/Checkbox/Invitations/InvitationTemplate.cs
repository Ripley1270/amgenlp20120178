﻿namespace Checkbox.Invitations
{
    using Checkbox.Management;
    using Checkbox.Messaging.Email;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class InvitationTemplate
    {
        public InvitationTemplate()
        {
            this.FromAddress = ApplicationManager.AppSettings.DefaultFromEmailAddress;
            this.FromName = ApplicationManager.AppSettings.DefaultEmailFromName;
            this.Body = ApplicationManager.AppSettings.DefaultEmailInvitationBody;
            this.Subject = ApplicationManager.AppSettings.DefaultEmailInvitationSubject;
            this.ResendBody = this.Body;
            this.ResendSubject = this.Subject;
        }

        public InvitationTemplate Copy()
        {
            return new InvitationTemplate { FromAddress = this.FromAddress, FromName = this.FromName, Subject = this.Subject, Body = this.Body, LinkText = this.LinkText, LoginOption = this.LoginOption, Format = this.Format, IncludeOptOutLink = this.IncludeOptOutLink, OptOutText = this.OptOutText };
        }

        public string Body { get; set; }

        public MailFormat Format { get; set; }

        public string FromAddress { get; set; }

        public string FromName { get; set; }

        public bool IncludeOptOutLink { get; set; }

        public string LinkText { get; set; }

        public Checkbox.Invitations.LoginOption LoginOption { get; set; }

        public string OptOutText { get; set; }

        public string OptOutURL
        {
            get
            {
                return string.Format("{0}{1}/OptOut.aspx?i={2}recipientGuid", ApplicationManager.ApplicationURL, ApplicationManager.ApplicationRoot, ApplicationManager.AppSettings.PipePrefix);
            }
        }

        public string ResendBody { get; set; }

        public string ResendSubject { get; set; }

        public string Subject { get; set; }
    }
}

