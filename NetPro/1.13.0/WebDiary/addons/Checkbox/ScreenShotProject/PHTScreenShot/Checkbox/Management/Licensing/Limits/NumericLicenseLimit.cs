﻿namespace Checkbox.Management.Licensing.Limits
{
    using Checkbox.Management;
    using System;
    using Xheo.Licensing;

    public abstract class NumericLicenseLimit : ValueLimit<long?>
    {
        private long? _licenseFileLimitValue;

        protected NumericLicenseLimit()
        {
        }

        protected abstract long GetCurrentCount();
        public override void Initialize(ExtendedLicense license)
        {
            if ((license != null) && (license.Values[this.LimitName] != null))
            {
                try
                {
                    this._licenseFileLimitValue = new long?(Convert.ToInt64(license.Values[this.LimitName]));
                }
                catch
                {
                    this._licenseFileLimitValue = null;
                }
            }
        }

        public long CurrentCount
        {
            get
            {
                return this.GetCurrentCount();
            }
        }

        public override long? LicenseFileLimitValue
        {
            get
            {
                if (ApplicationManager.AppSettings.LimitDebugMode)
                {
                    return 1L;
                }
                return this._licenseFileLimitValue;
            }
        }
    }
}

