﻿namespace Checkbox.Forms.Items.UI
{
    using System;

    [Serializable]
    public class RadioButtons : SelectLayout
    {
        public override string AppearanceCode
        {
            get
            {
                return "RADIO_BUTTONS";
            }
        }
    }
}

