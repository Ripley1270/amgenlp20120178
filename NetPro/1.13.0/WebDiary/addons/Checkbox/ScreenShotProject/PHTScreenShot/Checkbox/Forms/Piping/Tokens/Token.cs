﻿namespace Checkbox.Forms.Piping.Tokens
{
    using Checkbox.Management;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class Token : IEquatable<string>, IEquatable<Token>
    {
        public Token(string token)
        {
            this.TokenName = token;
            this.Type = TokenType.Other;
        }

        protected Token(string token, TokenType type)
        {
            this.TokenName = token;
            this.Type = type;
        }

        public bool Equals(Token other)
        {
            return (string.Compare(other.TokenName, this.TokenName, true) == 0);
        }

        public bool Equals(string other)
        {
            return (string.Compare(other, this.TokenName, true) == 0);
        }

        public string CleanName
        {
            get
            {
                return this.TokenName.Replace(ApplicationManager.AppSettings.PipePrefix, string.Empty);
            }
        }

        public string TokenName { get; set; }

        public TokenType Type { get; set; }
    }
}

