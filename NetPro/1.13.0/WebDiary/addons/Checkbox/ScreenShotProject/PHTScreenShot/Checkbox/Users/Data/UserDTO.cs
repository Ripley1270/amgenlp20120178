﻿namespace Checkbox.Users.Data
{
    using System;
    using System.Security.Principal;

    internal class UserDTO
    {
        private string _domain;
        private IIdentity _identity;
        private string _password;
        private string _uniqueIdentifier;

        public UserDTO(string uniqueIdentifier, IIdentity identity, string domain, string password)
        {
            this.UniqueIdentifier = uniqueIdentifier;
            this.Identity = identity;
            this.Domain = domain;
            this.Password = password;
        }

        public string Domain
        {
            get
            {
                return this._domain;
            }
            set
            {
                this._domain = value;
            }
        }

        public IIdentity Identity
        {
            get
            {
                return this._identity;
            }
            set
            {
                this._identity = value;
            }
        }

        public string IdentityName
        {
            get
            {
                return this.Identity.Name;
            }
        }

        public string Password
        {
            get
            {
                return this._password;
            }
            set
            {
                this._password = value;
            }
        }

        public string UniqueIdentifier
        {
            get
            {
                return this._uniqueIdentifier;
            }
            set
            {
                this._uniqueIdentifier = value;
            }
        }
    }
}

