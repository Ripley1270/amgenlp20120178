﻿namespace Checkbox.Management.Licensing.Limits.Static
{
    using Checkbox.Management.Licensing.Limits;
    using System;

    public class SpssLimit : StaticLicenseLimit
    {
        public override string LimitName
        {
            get
            {
                return "AllowNativeSpssExport";
            }
        }
    }
}

