﻿namespace Checkbox.Forms.Items.UI
{
    using Checkbox.Analytics.Items.UI;
    using Checkbox.Common;
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Collections.Specialized;
    using System.Data;
    using System.Runtime.CompilerServices;
    using System.Xml;
    using System.Xml.Schema;
    using System.Xml.Serialization;

    [Serializable]
    public abstract class AppearanceData : AbstractPersistedDomainObject, IXmlSerializable
    {
        protected AppearanceData()
        {
            this.ItemPosition = "Left";
            this.Layout = Checkbox.Forms.Items.UI.Layout.Vertical;
        }

        private void AddBaseAppearanceValuesForXmlSerialization(NameValueCollection values)
        {
            values.Add("graphType", this.GraphType.ToString());
            values.Add("itemPosition", this.ItemPosition);
        }

        protected virtual void AddCustomAppearanceValuesForXmlSerialization(NameValueCollection values)
        {
        }

        public virtual AppearanceData Copy()
        {
            return null;
        }

        protected override void Create(IDbTransaction transaction)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Appearance_Create");
            storedProcCommandWrapper.AddInParameter("AppearanceCode", DbType.String, this.AppearanceCode);
            storedProcCommandWrapper.AddInParameter("ItemPosition", DbType.String, this.ItemPosition);
            storedProcCommandWrapper.AddOutParameter("AppearanceID", DbType.Int32, 4);
            database.ExecuteNonQuery(storedProcCommandWrapper, transaction);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("AppearanceID");
            if ((parameterValue == null) || (parameterValue == DBNull.Value))
            {
                throw new Exception("Unable to save appearance data.");
            }
            base.ID = new int?((int) parameterValue);
        }

        public override void Delete(IDbTransaction t)
        {
        }

        protected override void Load(DataRow data)
        {
            if (data == null)
            {
                throw new Exception("DataRow cannot be null.");
            }
            if (this.DataTableName == data.Table.TableName)
            {
                this.LoadFromDataRow(data);
            }
            if (data.Table.TableName == this.ParentDataTableName)
            {
                this.LoadAbstractTableDataFromDataRow(data);
            }
        }

        protected void LoadAbstractTableDataFromDataRow(DataRow data)
        {
            try
            {
                this.GraphType = (Checkbox.Analytics.Items.UI.GraphType) Enum.Parse(typeof(Checkbox.Analytics.Items.UI.GraphType), DbUtility.GetValueFromDataRow<string>(data, "GraphType", Checkbox.Analytics.Items.UI.GraphType.SummaryTable.ToString()));
                this.ItemPosition = DbUtility.GetValueFromDataRow<string>(data, "ItemPosition", string.Empty);
                this.LabelPosition = new Checkbox.Forms.Items.UI.LabelPosition?((Checkbox.Forms.Items.UI.LabelPosition) Enum.Parse(typeof(Checkbox.Forms.Items.UI.LabelPosition), DbUtility.GetValueFromDataRow<string>(data, "LabelPosition", Checkbox.Forms.Items.UI.LabelPosition.Top.ToString())));
                this.FontColor = DbUtility.GetValueFromDataRow<string>(data, "FontColor", string.Empty);
                this.FontSize = DbUtility.GetValueFromDataRow<string>(data, "FontSize", string.Empty);
                this.Rows = DbUtility.GetValueFromDataRow<int?>(data, "Rows", null);
                this.Columns = DbUtility.GetValueFromDataRow<int?>(data, "Columns", null);
                this.Height = DbUtility.GetValueFromDataRow<int?>(data, "Height", null);
                this.Width = DbUtility.GetValueFromDataRow<int?>(data, "Width", null);
                this.GridLines = DbUtility.GetValueFromDataRow<string>(data, "GridLines", "None");
                this.ShowNumberLabels = DbUtility.GetValueFromDataRow<int>(data, "ShowNumberLabels", 0) == 1;
                string str = DbUtility.GetValueFromDataRow<string>(data, "LayoutStyle", "Vertical");
                try
                {
                    this.Layout = (Checkbox.Forms.Items.UI.Layout) Enum.Parse(typeof(Checkbox.Forms.Items.UI.Layout), str);
                }
                catch (Exception)
                {
                    this.Layout = Checkbox.Forms.Items.UI.Layout.Vertical;
                }
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessProtected"))
                {
                    throw;
                }
            }
        }

        public void ReadXml(XmlReader reader)
        {
            throw new NotImplementedException();
        }

        public virtual void Save(int itemId)
        {
            try
            {
                using (IDbConnection connection = DatabaseFactory.CreateDatabase().GetConnection())
                {
                    connection.Open();
                    IDbTransaction transaction = connection.BeginTransaction();
                    try
                    {
                        this.Save(transaction, itemId);
                        transaction.Commit();
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
            }
        }

        public virtual void Save(IDbTransaction transaction, int itemId)
        {
            if (base.ID > 0)
            {
                this.Update(transaction);
            }
            else
            {
                this.Create(transaction);
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemAppearances_Add");
            storedProcCommandWrapper.AddInParameter("AppearanceID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, itemId);
            database.ExecuteNonQuery(storedProcCommandWrapper, transaction);
        }

        XmlSchema IXmlSerializable.GetSchema()
        {
            return null;
        }

        protected override void Update(IDbTransaction t)
        {
            if (base.ID <= 0)
            {
                throw new Exception("DataID must be set to Update appearance.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Appearance_Update");
            storedProcCommandWrapper.AddInParameter("AppearanceID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("ItemPosition", DbType.String, this.ItemPosition);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        public void WriteXml(XmlWriter writer)
        {
            NameValueCollection values = new NameValueCollection();
            this.AddBaseAppearanceValuesForXmlSerialization(values);
        }

        public abstract string AppearanceCode { get; }

        public virtual int? Columns { get; set; }

        protected override DBCommandWrapper ConfigurationDataSetCommand
        {
            get
            {
                DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_AppearanceData_Get");
                storedProcCommandWrapper.AddInParameter("AppearanceID", DbType.Int32, base.ID);
                return storedProcCommandWrapper;
            }
        }

        public override string DataTableName
        {
            get
            {
                return "AppearanceData";
            }
        }

        public virtual string FontColor { get; set; }

        public virtual string FontSize { get; set; }

        public virtual Checkbox.Analytics.Items.UI.GraphType GraphType { get; set; }

        public virtual string GridLines { get; set; }

        public virtual int? Height { get; set; }

        public override string IdentityColumnName
        {
            get
            {
                return "AppearanceId";
            }
        }

        public string ItemPosition { get; set; }

        public virtual Checkbox.Forms.Items.UI.LabelPosition? LabelPosition { get; set; }

        public virtual Checkbox.Forms.Items.UI.Layout Layout { get; set; }

        public override string ParentDataTableName
        {
            get
            {
                return "AppearanceData";
            }
        }

        public virtual int? Rows { get; set; }

        public virtual bool ShowNumberLabels { get; set; }

        public virtual int? Width { get; set; }
    }
}

