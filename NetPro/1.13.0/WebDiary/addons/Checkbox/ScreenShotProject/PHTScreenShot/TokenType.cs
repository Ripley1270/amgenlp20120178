﻿using System;

public enum TokenType
{
    Answer,
    Profile,
    Response,
    ResponseTemplate,
    Other
}

