﻿// Assembly Checkbox, Version 4.7.1.60

[assembly: System.Reflection.AssemblyProduct("Checkbox\x00ae")]
[assembly: System.Reflection.AssemblyCopyright("Copyright \x00a9 2002-2010 Prezza Technologies, Inc.")]
[assembly: System.Reflection.AssemblyTrademark("")]
[assembly: System.Runtime.CompilerServices.CompilationRelaxations(8)]
[assembly: System.Runtime.CompilerServices.RuntimeCompatibility(WrapNonExceptionThrows=true)]
[assembly: System.Reflection.AssemblyTitle("Checkbox")]
[assembly: System.Reflection.AssemblyDescription("Primary Checkbox\x00ae application logic")]
[assembly: System.Reflection.AssemblyConfiguration("")]
[assembly: System.Reflection.AssemblyCompany("Prezza Technologies, Inc.")]

