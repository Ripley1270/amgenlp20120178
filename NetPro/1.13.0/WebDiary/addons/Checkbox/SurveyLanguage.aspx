<%@ Page Language="C#" AutoEventWireup="true" Inherits="WebDiary.SurveyTextExport.CustomSurveyLanguage" MasterPageFile="~/CheckboxAdmin.master" Theme="Default" Codebehind="CustomSurveyLanguage.cs" %>
<%@ Register TagPrefix="cc1" Namespace="WebDiary.SurveyTextExport" Assembly="WebDiary.SurveyTextExport" %>
<%@ MasterType VirtualPath="~/CheckboxAdmin.master" %>

<asp:Content ID="language" runat="server" ContentPlaceHolderID="_titleCustomPlace">
    <asp:Panel ID="_editLanguagePlace" runat="server">
        <cc1:MultiLanguageLabel ID="_editLanguageLbl" runat="server" CssClass="PrezzaLabel" TextId="/pageText/formEditor.aspx/editLanguage"></cc1:MultiLanguageLabel>
        <asp:DropDownList runat="server" CssClass="PrezzaNormal" AutoPostBack="true" ID="editLanguage" />
    </asp:Panel>
</asp:Content>
<asp:Content ID="content" runat="server" ContentPlaceHolderID="pageContentPlace">
  <br />
  <cc1:MultiLanguageLabel runat="server" CssClass="PrezzaSubTitle" ID="languagesLabel" TextId="/pageText/surveyLanguage.aspx/languageOptions">Languages Options</cc1:MultiLanguageLabel>
  <div style="background:url(<%=Checkbox.Management.ApplicationManager.ApplicationRoot%>/Images/ckbx_General_HorizDots.gif);height:1px;overflow:hidden"></div>
  <table cellspacing="2" cellpadding="2">
    <!--<tr>
      <td width="50"></td>
      <td><cc1:MultiLanguageLabel runat="server" CssClass="PrezzaNormal" ID="currentLanguageLabel" Text="Current Edit Language" TextId="/pageText/SurveyLanguage.aspx/CurrentLanguage" /></td>
      <td><asp:DropDownList runat="server" CssClass="PrezzaNormal" ID="editLanguage2" /></td>
    </tr> -->
    <tr valign="top">
      <td width="50"></td>
      <td><cc1:MultiLanguageLabel runat="server" CssClass="PrezzaNormal" ID="defaultLanguageLabel" Text="Default Language" TextId="/pageText/SurveyLanguage.aspx/DefaultLanguage" /></td>
      <td><asp:DropDownList CssClass="PrezzaNormal" runat="server" id="defaultLanguage" /></td>
    </tr>
    <tr valign="top">
      <td width="50"></td>
      <td><cc1:MultiLanguageLabel runat="server" ID="supportedLanguagesLabel" CssClass="PrezzaNormal" TextId="/pageText/SurveyLanguage.aspx/supportedLanguages">Supported Languages</cc1:MultiLanguageLabel></td>
      <td>
        <asp:GridView ID="_languagesGrid" runat="server" CssClass="PrezzaDataGrid" CellPadding="2" CellSpacing="2">
          <RowStyle CssClass="EvenRow" />
          <AlternatingRowStyle CssClass="OddRow" />
          <HeaderStyle CssClass="Header" />
        </asp:GridView>
      </td>
    </tr>
    <tr>
      <td width="50" />
      <td><cc1:MultiLanguageLabel runat="server" ID="addLanguageLbl" CssClass="PrezzaNormal" TextId="/pageText/surveyLanguage.aspx/addLanguage" /></td>
      <td>
        <cc1:MultiLanguageLabel ID="_noAvailableLanguagesLbl" runat="server" CssClass="PrezzaNormal" TextId="/pageText/surveyLanguage.aspx/noAvailableLanguages" />
        <asp:DropDownList ID="_newLanguageSelect" runat="server" CssClass="PrezzaNormal" />
        <cc1:MultiLanguageImageButton runat="server" ID="_addLanguageButton" ToolTipTextID="/pageText/surveyLanguage.aspx/addLanguage" ImageUrl="../Images/add16.gif" />
      </td>
    </tr>
    <tr valign="top">
      <td width="50"></td>
      <td><cc1:MultiLanguageLabel runat="server" CssClass="PrezzaNormal" ID="languageSelectionLabel" Text="Language Selection" TextId="/pageText/SurveyLanguage.aspx/languageSelection" /></td>
      <td>
        <cc1:MultiLanguageDropDownList runat="server" CssClass="PrezzaNormal" ID="languageSelectionMethod" AutoPostBack="true">
          <asp:ListItem value="Prompt" TextId="/enum/formLanguageSource/prompt">Prompt User</asp:ListItem>
          <asp:ListItem value="QueryString" TextId="/enum/formLanguageSource/queryString">Query String</asp:ListItem>
          <asp:ListItem value="Session" TextId="/enum/formLanguageSource/session">Session Variable</asp:ListItem>
          <asp:ListItem value="User" TextId="/enum/formLanguageSource/user">User Attribute</asp:ListItem>
        </cc1:MultiLanguageDropDownList>
      </td>
    </tr>
    <tr valign="top">
      <td width="50"></td>
      <asp:PlaceHolder runat="server" ID="variableNamePanel" Visible="false">
        <td><cc1:MultiLanguageLabel runat="server" CssClass="PrezzaNormal" ID="variableNameLabel" Text="Variable Name" TextId="/pageText/SurveyLanguage.aspx/variableName" /></td>
        <td><cc1:MultiLanguageTextBox runat="server" CssClass="PrezzaNormal" ID="variableName" /></td>
      </asp:PlaceHolder>
      <asp:PlaceHolder runat="server" ID="userAttributePanel" Visible="false">
        <td><cc1:MultiLanguageLabel runat="server" CssClass="PrezzaNormal" ID="attributeNameLabel" Text="User Attribute" TextId="/pageText/SurveyLanguage.aspx/userAttribute" /></td>
        <td><asp:DropDownList runat="server" CssClass="PrezzaNormal" ID="attributeName" /></td>
      </asp:PlaceHolder>
    </tr>
    <tr>
      <td width="50"></td>
      <td></td>
      <td align="left"><cc1:MultiLanguageButton runat="server" ID="updateSettings" CssClass="PrezzaButton" Text="Update Settings" TextId="/pageText/SurveyLanguage.aspx/updateSettings" /></td>
    </tr>
  </table>
  
  <br /><br />
    
  <cc1:MultiLanguageLabel runat="server" ID="importExportLbl" CssClass="PrezzaSubTitle" TextId="/pageText/surveyLanguage.aspx/importExport">Import / Export</cc1:MultiLanguageLabel>
  <div style="background:url(<%=Checkbox.Management.ApplicationManager.ApplicationRoot%>/Images/ckbx_General_HorizDots.gif);height:1px;overflow:hidden"></div>
  <br />
  <table cellpadding="2" cellspacing="2" style="padding-left:20px">
    <tr>
      <td width="50"></td>
      <td><cc1:MultiLanguageButton runat="server" ID="exportBtn" CssClass="PrezzaButton" TextId="/pageText/surveyLanguage.aspx/export" Text="Export Texts"/></td>
    </tr>
    <tr> 
      <td width="50"></td>
      <td>
        <table>
          <tr>
            <td><br /><br /></td>
          </tr>
          <tr>
            <td><asp:PlaceHolder id="importFilePlace" runat="server" /></td>
          </tr>
          <tr>
            <td><cc1:MultiLanguageButton runat="server" ID="importBtn" CssClass="PrezzaButton" TextId="/pageText/surveyLanguage.aspx/import" Text="Import Texts" /></td>
          </tr>
          <tr>
            <td><asp:Label runat="server" id="importMsgLbl" CssClass="Message" Visible="false" /></td>
          </tr>          
        </table>
      </td>
    </tr>
    <tr><td><br /></td></tr>
  </table>
  <div style="background:url(<%=Checkbox.Management.ApplicationManager.ApplicationRoot%>/Images/ckbx_General_HorizDots.gif);height:1px;overflow:hidden"></div>
  
</asp:Content>