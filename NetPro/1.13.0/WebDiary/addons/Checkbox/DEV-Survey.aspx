<%@ Page Language="C#" AutoEventWireup="true" Inherits="Survey" Codebehind="Survey.aspx.cs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:prezza >
  <head id="Head1" runat="server">
    <title><asp:PlaceHolder id="_pageTitlePlace" runat="server" /></title>
    <script type="text/javascript">
        var isMobile = false;
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini|Mobile|Windows CE|Palm/i.test(navigator.userAgent)) {
            isMobile = true;
        }
        if (isMobile) {
            document.write('<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />');
            document.write('<meta name="apple-mobile-web-app-capable" content="yes" />');
            document.write('<link rel="stylesheet" type="text/css" href="Styles/iPad-form.css" />');
        }
    </script>   
    <style type="text/css">
      <!--
        <asp:PlaceHolder id="_stylePlace" runat="server" />
      -->
    </style>
    <link id="dynamicStyle" rel="stylesheet" type="text/css" />
    <link id="jqueryStyle" rel="stylesheet" type="text/css" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/themes/base/jquery-ui.css" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>        
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>  
    
    <script type="text/javascript">
        var returnUrl = '[URL]';
        var secCheck = false;
        var isReloaded = true;
        var isClicked = false;
        var notOnlineMessage = "";
		
        if (getParam('psd_id').length == 0)
            location.href = returnUrl;

        function getParam(name) {
            name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
            var regexS = "[\\?&]" + name + "=([^&#]*)";
            var regex = new RegExp(regexS);
            var results = regex.exec(window.location.href);
            if (results == null)
                return "";
            else
                return results[1];
        }
		
		function adjustStyle(width) {
			width = parseInt(width);
			if (width < 701) {
				$("#dynamicStyle").attr("href", "Styles/narrow.css");
			} else if ((width >= 701) && (width < 900)) {
				$("#dynamicStyle").attr("href", "Styles/medium.css");
			} else {
			   $("#dynamicStyle").attr("href", "Styles/wide.css");
			}
        }

        $(document).ready(function () {
           if (isMobile) {
                $('input[type="checkbox"]').after('<span class="cb"></span>');
                if($('input[type="radio"]').next('span').length == 0)
					$('input[type="radio"]').after('<span class="rd"></span>');
                $('label').bind('click', function () { $('input#' + $(this).attr('for')).click(); return false; });
                $('span.cb, span.rd').bind('click', function () { $(this).prev().click(); return false; });
            }

            $('select').bind('change', function () { isReloaded = false; });
            $('#survey').submit(function () { isReloaded = false; });

            //prevent submitting form more than once
            $(':submit').click(function (e) {
                if (isClicked)
                    e.preventDefault();
                else {
                    isClicked = true;
                    if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
                        if (!navigator.onLine) {
                            isClicked = false;
                            e.preventDefault();
                            alert(notOnlineMessage);
                        }
                    }
                }
            });

            adjustStyle($(this).width());
        });
    </script>
	<script src="Resources/ToolTips.js"></script>
    <link rel="stylesheet" type="text/css" href="Styles/CustomSurveyStyles.css" />	
    <script type="text/javascript" src="Resources/ToolTips.js"></script>
  </head>
    <body class="survey">
	<script src="<%= "[URL]/getStudyURL.aspx?psd_id=" + Request.QueryString["psd_id"]%>"></script>    	
	<script type="text/javascript">
	    if (!secCheck)
	        location.href = returnUrl;
	</script>
    <form id="survey" runat="server">
        <div data-role="page">
        <%-- Add a dummy textbox because IE will not always submit the name/value of the button --%>
        <%-- when the screen contains exactly one textbox --%>
        <asp:TextBox ID="_dummyBox" runat="server" style="visibility:hidden;display:none;" />

        <%-- Last Chance Error (used only when error can't be displayed via response view) --%>
        <asp:Panel ID="_errorPanel" runat="server" Visible="false" EnableViewState="false" />
        
        <%-- Response View --%>
        <asp:Panel ID="_responseViewPlace" runat="server" />
        </div>
    </form>

  </body>
</html>
