!include nsDialogs.nsh
; MUI 1.67 compatible ------
!include "MUI.nsh"
!include XML.nsh
!include MSWAuthenticate.nsh

; MUI Settings
!define MUI_ABORTWARNING
!define MUI_ICON "${NSISDIR}\Contrib\Graphics\Icons\modern-install.ico"
!define MUI_UNICON "${NSISDIR}\Contrib\Graphics\Icons\modern-uninstall.ico"

!define SOURCE_ROOT "$%WORKSPACE%"
!define JOB_NAME "$%JOB_NAME%"
!define BUILD_NUMBER "$%BUILD_NUMBER%"
!define PRODUCT_VERSION "1.13.0"
!define PRODUCT_NAME "NetPro_WebDiaryService"

Var lblInstallDir
Var txtInstallDir
Var InstallDir
Var InstallDialog

Var browse
Var server

; Domain dialog variables
Var DomainDialog
Var lblDomainLabel
Var lblDomainName
Var lblDomainUser
Var lblDomainUserPassword
Var txtDomainName
Var txtDomainUser
Var pwdDomainUserPassword
Var DomainName
Var DomainUser
Var DomainUserPassword

!include ZipDLL.nsh

!insertmacro MUI_PAGE_WELCOME

Page custom nsDomainUser nsDomainUserLeave
Page custom nsInstallDir nsInstallDirLeave

!insertmacro MUI_PAGE_INSTFILES

!insertmacro MUI_PAGE_FINISH

; Uninstaller pages
!insertmacro MUI_UNPAGE_INSTFILES

; Language files
!insertmacro MUI_LANGUAGE "English"

Name "${PRODUCT_NAME} ${PRODUCT_VERSION}"

OutFile "..\..\WebDiary\WebDiaryService_install.${BUILD_NUMBER}.exe"

ShowInstDetails show
ShowUnInstDetails show

Section "Main"

InitPluginsDir
SetOutPath "$InstallDir\WebDiary.SchedulerService"
SetOverwrite ifnewer
File /r "${SOURCE_ROOT}\WebDiary\src\WebDiary.SchedulerService\"
File "${SOURCE_ROOT}\installer\WebDiaryService\ntrights.exe"
####File "${SOURCE_ROOT}\WebDiary\${JOB_NAME}.sql.${BUILD_NUMBER}.zip"
####ZipDLL::extractall "$PLUGINSDIR\${JOB_NAME}.sql.${BUILD_NUMBER}.zip" "$PLUGINSDIR"
####${If} $0 == "success"
#### detailprint "successfully unzipped ${JOB_NAME}.sql.${BUILD_NUMBER}.zip"
####${ELSE}
#### MessageBox MB_OK "Failed to unzip webdiary sql zip file Aborting"
#### Abort ;Do not leave the page until SQL completes successfully
####${EndIf}

###nsSCM::Stop /NOUNLOAD "WebDiary.SchedulerService"

###nsSCM::Remove /NOUNLOAD "WebDiary.SchedulerService"

nsexec::ExecToStack 'sc stop "WebDiary.SchedulerService"'
nsexec::ExecToStack 'sc delete "WebDiary.SchedulerService"'


 CopyFiles $InstallDir\WebDiary.SchedulerService\bin\Release\WebDiary.SchedulerService.exe.config $InstallDir\WebDiary.SchedulerService\bin\Release\WebDiary.SchedulerService.exe.config.tmp
  pop $0
  ;${LogText} "the return value for copying WebDiary.TestPages\DEV-Web.config to WebDiary.TestPages\Web.config.tmp is $0"
  
  CopyFiles $InstallDir\WebDiary.SchedulerService\DEV-log4net.config $InstallDir\WebDiary.SchedulerService\bin\Release\log4net.config
  pop $0

  ReadRegStr $server HKLM "System\CurrentControlSet\Control\ComputerName\ActiveComputerName" "ComputerName"
  ;StrCmp $0 "" win9x
  ;StrCpy $1 $0 4 3
  ;MessageBox MB_OK "Your ComputerName : $server"

   ${xml::LoadFile} "$InstallDir\WebDiary.SchedulerService\bin\Release\WebDiary.SchedulerService.exe.config.tmp" $0
  ${xml::RootElement} $0 $1
  ${xml::XPathNode} "//connectionStrings/add[@name='default']" $0
  ${xml::SetAttribute} "connectionString" "data source=$server;initial catalog=WebDiary;integrated security=SSPI;" $0
  ${xml::SaveFile} "$InstallDir\WebDiary.SchedulerService\bin\Release\WebDiary.SchedulerService.exe.config" $0
  ${xml::Unload}
  
  ;nsSCM::Stop /NOUNLOAD "WebDiary.SchedulerService"
  ;nsSCM::Remove /NOUNLOAD "WebDiary.SchedulerService"
  
  #############################################################################
  # Formatted the sql command call
  # ###########################################################################
  ####StrCpy $0 '$\"$sql_cmd\sqlcmd.EXE$\" -U $username -P $password'
  ####StrCpy $1 '-S $server -i $\"$PLUGINSDIR\add_domain_user.sql$\"'
  ####StrCpy $2 '-o c:\temp\domain_user.txt -e -b'

 #### nsExec::ExecToStack "$0 $1 $2"
 #### Pop $0
 #### Pop $1
 #### ${If} $0 == 0
  ####${Else}
  ####  MessageBox MB_OK "The SQL script call did not run succesfully. The error is : $0$\n$\r$1"
  ####  Abort
  ####${EndIf}
  nsexec::ExecToStack '$InstallDir\WebDiary.SchedulerService\ntrights.exe -u $DomainName\$DomainUser +r SeServiceLogonRight'
  pop $0
  pop $1
 # MessageBox mb_ok $0
  #MessageBox mb_ok $1
  nsexec::ExecToStack 'sc create "WebDiary.SchedulerService" start= delayed-auto binPath= "$InstallDir\WebDiary.SchedulerService\bin\Release\WebDiary.SchedulerService.exe"'
  pop $0
  pop $1
  #MessageBox MB_OK $1
  nsexec::ExecToStack 'sc description "WebDiary.SchedulerService" "NetPro SchedulerService 1.13.0"'
  nsexec::ExecToStack 'sc config  "WebDiary.SchedulerService" depend= "MSSQLSERVER" obj= "$DomainName\$DomainUser" password= "$DomainUserPassword"'
  nsexec::ExecToStack 'sc failure "WebDiary.SchedulerService" reset= 60 actions= restart/5000/restart/5000//'
  nsexec::ExecToStack 'sc start "WebDiary.SchedulerService"'
  #nsSCM::Install /NOUNLOAD "WebDiary.SchedulerService" "WebDiary.SchedulerService" 16 2 "$InstallDir\WebDiary.SchedulerService\bin\Release\WebDiary.SchedulerService.exe" "" "MSSQLSERVER" "" ""
  
  #nsSCM::Start /NOUNLOAD "WebDiary.SchedulerService"


  #StrCpy $0 'icacls $InstallDir\WebDiary.SchedulerService\bin /grant "$DomainName\$DomainUser:(F)"'
  #nsExec::ExecToStack "$0"
  nsExec::ExecToStack 'c:\WINDOWS\system32\cacls.exe "$InstallDir\WebDiary.SchedulerService\bin" /E /T /G "$DomainName\$DomainUser":F'
  pop $0
  pop $1
  
  Delete "$InstallDir\WebDiary.SchedulerService\ntrights.exe"

SectionEnd

Function un.onUninstSuccess
  HideWindow
  MessageBox MB_ICONINFORMATION|MB_OK "$(^Name) was successfully removed from your computer."
FunctionEnd

Function un.onInit
  MessageBox MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2 "Are you sure you want to completely remove $(^Name) and all of its components?" IDYES +2
  Abort
FunctionEnd

Section Uninstall
;  Delete "$INSTDIR\${PRODUCT_NAME}.url"
  Delete "$INSTDIR\uninst.exe"
  ;Delete "$INSTDIR\5.3.0.zip"

  Delete "$SMPROGRAMS\test\Uninstall.lnk"
  Delete "$SMPROGRAMS\test\Website.lnk"

  RMDir "$SMPROGRAMS\test"
  RMDir "$INSTDIR"

  nsSCM::Stop /NOUNLOAD "WebDiary.SchedulerService"

  nsSCM::Remove /NOUNLOAD "WebDiary.SchedulerService"

;;  DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}"
  SetAutoClose true
SectionEnd


Function nsDomainUser
 nsDialogs::Create /NOUNLOAD 1018
 Pop $DomainDialog

 ${If} $DomainDialog == error
  Abort
 ${EndIf}

 ${NSD_CreateLabel} 0 0 100% 24u "Please specify the domain, domain user and the password for the domain user."
 Pop $lblDomainLabel

 ${NSD_CreateLabel} 0 24u 100u 12u "Domain Name:"
 Pop $lblDomainName

 ${NSD_CreateLabel} 0 38u 100u 12u "Username:"
 Pop $lblDomainUser

 ${NSD_CreateLabel} 0 52u 100u 12u "Password:"
 Pop $lblDomainUserPassword

 ${NSD_CreateText} 100u 24u 100% 12u ""
 Pop $txtDomainName

 ${NSD_CreateText} 100u 38u 100% 12u ""
 Pop $txtDomainUser

 ${NSD_CreatePassword} 100u 52u 100% 12u ""
 Pop $pwdDomainUserPassword

 nsDialogs::Show

FunctionEnd

Function nsDomainUserLeave
 ${NSD_GetText} $txtDomainName $DomainName
 ${NSD_GetText} $txtDomainUser $DomainUser
 ${NSD_GetText} $pwdDomainUserPassword $DomainUserPassword
 push $DomainUser
 push $DomainName
 push $DomainUserPassword

 call MSWAuthenticate
 pop $0
 ${If} $0 == "success"
 ${else} 
    MessageBox MB_OK $0
    Abort
 ${EndIf} 
FunctionEnd


Function nsInstallDir

    nsDialogs::Create 1018
    Pop $InstallDialog

   ${NSD_CreateLabel} 0 0 100% 24u "Please specify the Directory Service File should be installed"
   Pop $0

   ${NSD_CreateLabel} 0 24u 100u 12u "Install Directory"
   Pop $lblInstallDir

   ${NSD_CreateText} 100u 24u 120u 12u $InstallDir
   Pop $txtInstallDir

   ${NSD_CreateBrowseButton} 224u 24u 40u 12u "Browse"
   Pop $browse
   ${NSD_OnClick} $browse OnDirBrowseButton

   nsDialogs::Show

 FunctionEnd
 
  Function OnDirBrowseButton
        Pop $R0
        ${If} $R0 == $browse
              ${NSD_GetText} $txtInstallDir $R0
              nsDialogs::SelectFolderDialog /NOUNLOAD "browse" $R0
              Pop $R0

              ${If} $R0 != error
                    ${NSD_SetText} $txtInstallDir "$R0"
              ${EndIf}
        ${EndIf}
FunctionEnd

Function nsInstallDirLeave
   ${NSD_GetText} $txtInstallDir $InstallDir
FunctionEnd
