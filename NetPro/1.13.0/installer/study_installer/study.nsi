!include nsDialogs.nsh
!include LogicLib.nsh
!include StrRep.nsh
!include ReplaceInFile.nsh
!include FileFunc.nsh
!include ZipDLL.nsh
!include StrContains.nsh
!include XML.nsh
!include ..\TextLog.nsh
!include MSWAuthenticate.nsh
!include x64.nsh
!include "WordFunc.nsh"
!include "TextFunc.nsh"
!include splice.nsh
!include trim.nsh
!insertmacro GetParameters
!insertmacro GetOptions

; HM NIS Edit Wizard helper defines
!define PRODUCT_NAME "NetPro"
!define PRODUCT_VERSION "1.13.0"
!define PRODUCT_PUBLISHER "PHT Corp"
!define DEFAULT_ANSWER_FILE default.ini
; Variables
Var WebSiteDir
Var WebDialog
Var lblWebSiteDir
Var txtWebSiteDir
Var Browse
Var SiteName
Var SurveyDialog
Var lblLabel
Var lblSurveyUrl
Var lblSurveyServer
Var lblSurveyUserUrl
Var lblSurveyPasswordUrl
Var lblSurveyDatabase
Var lblSurveyUsername
Var lblSurveyPassword
Var txtSurveyUrl
Var txtSurveyServer
Var txtSurveyUserUrl
Var txtSurveyPasswordUrl
Var txtSurveyDatabase
Var txtSurveyUsername
Var SurveyUrl
Var SurveyUrlUser
Var SurveyUrlPassword
Var pwdSurveyPassword
Var Surveyserver
Var Surveydatabase
Var Surveyusername
Var Surveypassword
Var Dialog
Var lblStudyLabel
Var lblWebServer
Var lblStudyIp
Var txtWebServer
Var txtStudyIp
Var Webserver
Var StudyIp
Var lblDbServer
Var txtDbServer
Var DbServer
Var NoHttpCluster
Var AppDir

# Domain User page Variables
Var DomainUserDialog
Var lblDomainWelcome
Var lblDomainName
Var lblDomainUser
Var lblDomainPassword
Var txtDomainName
Var txtDomainUser
Var pwdDomainUserPassword
Var DomainName
Var DomainUser
Var DomainUserPassword

# WebDiary database page
Var DatabaseDialog
Var lblDatabaseWelcome
Var lblDatabaseServer
Var lblDatabaseName
Var lblDatabaseAdmin
Var lblDatabaseAdminpass
Var txtDatabaseServer
Var txtDatabaseAdmin
Var txtDatabaseAdminPassword
Var DatabaseUser
Var DatabasePassword
Var DatabaseServer

#Install Type page Variables
Var InstallTypeDialog
Var DropList
Var lblDropList
Var Type
Var installType

#Resource Validation page Variables
Var ResoucesValidationDialog
Var lblResourceQuestion
Var lblProductionSelectionMsg
Var txtResourceValidationAnswer
Var ResourceValidationAnswer_cb
Var ResourceValidationAnswer_st

Var FinishPage

Var SaveDefault_cb
Var SaveDefault_st

Var AnswerFile
Var sql_cmd

Var dll_version
Var dll_version_new
Var minor_version
Var major_version
Var revision_version
Var build_version
Var study_id
Var SiteName1
Var study_env

; MUI 1.67 compatible ------
!include "MUI.nsh"

; MUI Settings
!define MUI_ABORTWARNING
!define MUI_ICON "${NSISDIR}\Contrib\Graphics\Icons\modern-install.ico"

!define SOURCE_ROOT "$%WORKSPACE%\Netpro\1.13.0"
!define JOB_NAME "$%JOB_NAME%"
!define BUILD_NUMBER "$%BUILD_NUMBER%"

!insertmacro MUI_PAGE_WELCOME

#Page custom nsInstallType nsInstallTypeLeave

#Page custom nsResoucesValidation nsResourcesValidationLeave

Page custom nsDomainUser nsDomainUserLeave

Page custom nsDatabase nsDatabaseLeave


Page custom nsInstallWeb nsInstallWebLeave

Page custom Page PageLeave

Page custom SurveyPage SurveyPageLeave

; Instfiles page
!insertmacro MUI_PAGE_INSTFILES
; Finish page
Page custom nsFinishPage nsFinisPageLeave
;!define MUI_FINISHPAGE_RUN "test"
;!insertmacro MUI_PAGE_FINISH

; Uninstaller pages
;;!insertmacro MUI_UNPAGE_INSTFILES

; Language files
!insertmacro MUI_LANGUAGE "English"

; MUI end ------


!macro IndexOf Var Str Char
Push "${Char}"
Push "${Str}"
Call IndexOf
Pop "${Var}"
!macroend
!define IndexOf "!insertmacro IndexOf"

Name "${PRODUCT_NAME} ${PRODUCT_VERSION}"
OutFile "..\..\WebDiary\${JOB_NAME}.${BUILD_NUMBER}_study_install.exe"
ShowInstDetails show
;;ShowUnInstDetails show

Section "MainSection"
  InitPluginsDir
  SetOutPath "$PLUGINSDIR"
  ;MessageBox MB_OK "$PLUGINSDIR"
  SetOverwrite on
  File "${SOURCE_ROOT}\WebDiary\${JOB_NAME}.${BUILD_NUMBER}.Study.zip"
  File ".\Studycmd\*"
  File "${SOURCE_ROOT}\installer\study_installer\iis_id.vbs"
  File "${SOURCE_ROOT}\WebDiary\data\sql\add_domain_user.sql"
  File "${SOURCE_ROOT}\WebDiary\data\sql\core_version.sql"
  SetOutPath "$PLUGINSDIR\Config"
  SetOverwrite on
  File "${SOURCE_ROOT}\WebDiary\addons\checkbox\Config\*"
SectionEnd

Function .onInit
${If} ${FileExists} "C:\Program Files\Microsoft SQL Server\100\Tools\Binn\sqlcmd.EXE"
    strcpy $sql_cmd "C:\Program Files\Microsoft SQL Server\100\Tools\Binn"
    ;MessageBox MB_OK "path to sql is $sql_cmd"
${ElseIf} ${FileExists} "C:\Program Files\Microsoft SQL Server\90\Tools\Binn\sqlcmd.EXE"
    strcpy $sql_cmd "C:\Program Files\Microsoft SQL Server\90\Tools\Binn"
     ;MessageBox MB_OK "path to sql is $sql_cmd"
${Else}
     MessageBox MB_OK "can not find sqlcmd on this system"
     Abort
${EndIf}

${If} ${FileExists} "c:\temp"
${Else}
   createdirectory "c:\temp"
${EndIf}
	StrCpy $AnswerFile ${DEFAULT_ANSWER_FILE}
 # Initialize the name of the answer file, either from
  #  the command line, or to the default.
  ${GetParameters} $R0
  ${GetOptions} $R0 /ANSWERS= $AnswerFile

  # If the option isn't available, the error flag is set
  #  clears the error and sets $AnswerFile to the default
  ${If} ${Errors}
    ClearErrors
    StrCpy $AnswerFile ${DEFAULT_ANSWER_FILE}
  ${EndIf}

  call "setDefaultAnswers"
	;${LogText} "In .onInit"
FunctionEnd

Section -Post_Questions

#Get the type of install from database to determing resources validation
nsExec::ExecToStack '"$sql_cmd\sqlcmd.exe" -U $DatabaseUser -P $DatabasePassword -S $DatabaseServer -d WebDiary -Q "select environment from dbo.VERSIONS"'
 pop $0
   pop $study_env
   ${StrReplace} $1 "$\r$\n" "|" $study_env
   StrCpy $study_env $1
   ${WordFind} $study_env "|" "+2}" $study_env
   ${WordFind} $study_env "|" "+1{" $study_env
   ${trim} $study_env $study_env 

   ${If} $Study_env = 0
       MessageBox MB_OK "Production Install Type. Resource validation will be performed."
	   
	   RMDir /r "C:\temp\WebDiary.StudyPortal\"
       ZipDLL::extractall "$PLUGINSDIR\${JOB_NAME}.${BUILD_NUMBER}.Study.zip" "C:\temp\"
       WriteRegStr HKLM "Software\Microsoft\PowerShell\1\ShellIds\Microsoft.PowerShell" 'ExecutionPolicy' "Unrestricted" 
       execwait  "powershell.exe -WindowStyle Hidden -File C:\temp\WebDiary.StudyPortal\ResourcesValidation.ps1" $0

       IfFileExists "C:\temp\WebDiary.StudyPortal\resourcevalidation.txt" file_found file_not_found 
       file_found:
       #MessageBox MB_OK "Errors found in resources validation! Click to Abort installation"
       Abort
       file_not_found:
       #MessageBox MB_OK "Resource Validation Successful. Click to continue installation"		   	   
   ${Else}
        MessageBox MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2 "Do you want to perform resource validation?" IDYES true IDNO false
        true:  
            RMDir /r "C:\temp\WebDiary.StudyPortal\"
            ZipDLL::extractall "$PLUGINSDIR\${JOB_NAME}.${BUILD_NUMBER}.Study.zip" "C:\temp\"
            WriteRegStr HKLM "Software\Microsoft\PowerShell\1\ShellIds\Microsoft.PowerShell" 'ExecutionPolicy' "Unrestricted" 
            execwait  "powershell.exe -WindowStyle Hidden -File C:\temp\WebDiary.StudyPortal\ResourcesValidation.ps1" $0

            IfFileExists "C:\temp\WebDiary.StudyPortal\resourcevalidation.txt" file_found1 file_not_found1 
            file_found1:
            #MessageBox MB_OK "Errors found in resources validation! Click to Abort installation"
            Abort
            file_not_found1:
            #MessageBox MB_OK "Resource Validation Successful. Click to continue installation"
         false:           	
   ${ENDIF}

Delete "C:\temp\MyInstallLog.txt"
${LogSetFileName} "C:\temp\MyInstallLog.txt"
${LogSetOn}
ZipDLL::extractall "$PLUGINSDIR\${JOB_NAME}.${BUILD_NUMBER}.Study.zip" "$WebSiteDir"

${GetFileVersion} "$WebSiteDir\WebDiary.StudyPortal\bin\WebDiary.Core.dll" $R0
strcpy $dll_version $R0

${splice} $dll_version "0" "." $R0
pop $dll_version_new
pop $R0
${trim} $dll_version_new $dll_version_new
strcpy $major_version $dll_version_new

${splice} $dll_version "1" "." $R0
pop $dll_version_new
pop $R0
${trim} $dll_version_new $dll_version_new
strcpy $minor_version $dll_version_new

${splice} $dll_version "2" "." $R0
pop $dll_version_new
pop $R0
${trim} $dll_version_new $dll_version_new
strcpy $revision_version $dll_version_new

${splice} $dll_version "3" "." $R0
pop $dll_version_new
pop $R0
${trim} $dll_version_new $dll_version_new
strcpy $build_version $dll_version_new


;;GetFullPathName /SHORT $0 "\Config"
StrCpy $0 $PLUGINSDIR
System::Call 'kernel32::GetLongPathName(t r0, t .r1, i ${NSIS_MAX_STRLEN}) i .r2'
StrCmp $2 error +2
StrCpy $0 $1
StrCpy $AppDir $0

!insertmacro ReplaceInFile "$PLUGINSDIR\config\Cache.xml" "[APP_PATH]" "$AppDir"
!insertmacro ReplaceInFile "$PLUGINSDIR\config\CheckboxSecurity.xml" "[APP_PATH]" "$AppDir"
!insertmacro ReplaceInFile "$PLUGINSDIR\config\ExceptionHandling.xml" "[APP_PATH]" "$AppDir"
!insertmacro ReplaceInFile "$PLUGINSDIR\config\GlobalizationConfiguration.xml" "[APP_PATH]" "$AppDir"
!insertmacro ReplaceInFile "$PLUGINSDIR\config\Logging.xml" "[APP_PATH]" "$AppDir"
!insertmacro ReplaceInFile "$PLUGINSDIR\config\Messaging.xml" "[APP_PATH]" "$AppDir"
!insertmacro ReplaceInFile "$PLUGINSDIR\config\Security.xml" "[APP_PATH]" "$AppDir"
!insertmacro ReplaceInFile "$PLUGINSDIR\add_domain_user.sql" "<domain>" \
                           "$DomainName"
!insertmacro ReplaceInFile "$PLUGINSDIR\add_domain_user.sql" "<domain_user>" \
                           "$DomainUser"
;!insertmacro ReplaceInFile "$PLUGINSDIR\app.config" "[APP_PATH]" "$AppDir"

  #############################################################################
  # Formatted the sql command call
  # ###########################################################################
  StrCpy $0 '$\"$sql_cmd\sqlcmd.EXE$\" -U $DatabaseUser -P $DatabasePassword'
  StrCpy $1 '-S $DatabaseServer -i $\"$PLUGINSDIR\add_domain_user.sql$\"'
  StrCpy $2 '-o c:\temp\domain_user.txt -e -b'

  nsExec::ExecToStack "$0 $1 $2"
  Pop $0
  Pop $1
  ${If} $0 == 0
  ${Else}
    MessageBox MB_OK "The SQL script add_domain_user did not run successfully:$\n$\rPlease look at c:\temp\domain_user.txt for errors"
    Abort
  ${EndIf}

 MessageBox MB_OK "Please update the study.xml in $WebSiteDir\WebDiary.StudyPortal\studyconfig"

 ${xml::LoadFile} "$WebSiteDir\WebDiary.StudyPortal\studyconfig\study.xml" $0
 ${xml::GotoPath} "/study" $0
 ${xml::GetAttribute} "name" $SiteName $2
 ;MessageBox MB_OK $SiteName
 ${xml::Unload}
 
 CopyFiles "$WebSiteDir\WebDiary.StudyPortal\DEV-Web.config" "$WebSiteDir\WebDiary.StudyPortal\Web.config.tmp"
  pop $0
  
  ;;CopyFiles "$WebSiteDir\WebDiary.StudyPortal\DEV-log4net.config" "$WebSiteDir\WebDiary.StudyPortal\log4net.config.tmp"
  ;pop $0
  
  CopyFiles $PLUGINSDIR\WebDiary.StudySetupCmd.exe.config $PLUGINSDIR\WebDiary.StudySetupCmd.exe.config.tmp
  pop $0
  
  !insertmacro ReplaceInFile "$PLUGINSDIR\WebDiary.StudySetupCmd.exe.config.tmp" "[APP_PATH]" "$AppDir"
  ;;${LogText} "the return value for copying WebDiary.StudyPortal\DEV-Web.config to WebDiary.StudyPortal\Web.config is $0"
 
  #${xml::LoadFile} "$WebSiteDir\WebDiary.StudyPortal\Web.config.tmp" $0
  ;;MessageBox MB_OK "opened log4j log $0"
  #${xml::RootElement} $0 $1
  #${xml::XPathNode} "/log4net/appender[@name='AdoNetAppender']/connectionString" $0
  ;;MessageBox MB_OK "found the sting to replace $0"
  #${xml::SetAttribute} "value" "data source=$Dbserver;initial catalog=webdiary;integrated security=SSPI;" $0
  ;;MessageBox MB_OK "replaced the string $0"
  #${xml::SaveFile} "$WebSiteDir\WebDiary.StudyPortal\Web.config" $0
  ;;MessageBox MB_OK "just saved file $WebSiteDir\WebDiary.StudyPortal\log4net.config $0"
  #${xml::Unload}
  
  ${xml::LoadFile} "$WebSiteDir\WebDiary.StudyPortal\Web.config.tmp" $0
  ${xml::RootElement} $0 $1
  ${xml::XPathNode} "/configuration/connectionStrings/add[@name='default']" $0
  ${xml::SetAttribute} "connectionString" "data source=$Dbserver;initial catalog=WebDiary;integrated security=SSPI;" $0
  ${xml::RootElement} $0 $1
  ${xml::XPathNode} "/configuration/connectionStrings/add[@name='checkboxdb']" $0
  ${xml::SetAttribute} "connectionString" "Data Source=$Surveyserver;Initial Catalog=$Surveydatabase;Trusted_Connection=Yes" $0
  ${xml::RootElement} $0 $1
  ${xml::XPathNode} "/configuration/system.web/sessionState[@mode='SQLServer']" $0
  ${xml::SetAttribute} "sqlConnectionString" "data source=$Dbserver;initial catalog=WebDiary;integrated security=SSPI;" $0
  ${xml::RootElement} $0 $1
  ${xml::XPathNode} "/configuration/log4net/appender[@name='AdoNetAppender']/connectionString" $0
  ${xml::SetAttribute} "value" "data source=$Dbserver;initial catalog=webdiary;integrated security=SSPI;" $0
  ${xml::SaveFile} "$WebSiteDir\WebDiary.StudyPortal\Web.config" $0
  ${xml::Unload}
  
  ${xml::LoadFile} "$PLUGINSDIR\WebDiary.StudySetupCmd.exe.config.tmp" $0
  ;;MessageBox MB_OK "opened log4j log $0"
  ${xml::RootElement} $0 $1
  ${xml::XPathNode} "/configuration/connectionStrings/add[@name='checkboxdb']" $0
  ${xml::SetAttribute} "connectionString" "Data Source=$Surveyserver;Initial Catalog=$Surveydatabase;Trusted_Connection=Yes;" $0
  ${xml::RootElement} $0 $1
  ${xml::XPathNode} "/configuration/connectionStrings/add[@name='default']" $0
  ;; was not sure what to do with this one as it wasn't in the examples
  ${xml::SetAttribute} "connectionString" "Data Source=$Dbserver;Initial Catalog=WebDiary;integrated security=SSPI;" $0
  ${xml::RootElement} $0 $1
  ${xml::XPathNode} "/configuration/connectionStrings/add[@name='DefaultConnectionString']" $0
  ${xml::SetAttribute} "connectionString" "server=$Surveyserver;database=$Surveydatabase;uid=$Surveyusername;pwd=$Surveypassword;Application Name=StudySetupCmd;" $0
  ${xml::SaveFile} "$PLUGINSDIR\WebDiary.StudySetupCmd.exe.config" $0
  ;;MessageBox MB_OK "saved file $0"
  ${xml::Unload}
  
  ;;!insertmacro ReplaceInFile "$WebSiteDir\WebDiary.StudyPortal\Web.config" "Source=checkbox-survey;Initial Catalog=Checkbox_47;User ID=admin;Password=admin" "Source=$Surveyserver;Initial Catalog=$Surveydatabase;User ID=$Surveyusername;Password=$Surveypassword"
  ;;!insertmacro ReplaceInFile "$PLUGINSDIR\WebDiary.StudySetupCmd.exe.config" "Source=checkbox-survey;Initial Catalog=Checkbox_47;User ID=admin;Password=admin" "Source=$Surveyserver;Initial Catalog=$Surveydatabase;User ID=$Surveyusername;Password=$Surveypassword"
  ;;!insertmacro ReplaceInFile "$PLUGINSDIR\WebDiary.StudySetupCmd.exe.config" "Source=JSALESI-T500\SQLEXPRESS;Initial Catalog=WebDiary;User ID=wda;Password=PH7_C0rp" "Source=$Webserver;Initial Catalog=WebDiary;User ID=wda;Password=PH7_C0rp"
  ;;!insertmacro ReplaceInFile "$PLUGINSDIR\WebDiary.StudyPortal\Web.config" "Source=JSALESI-T500\SQLEXPRESS;Initial Catalog=WebDiary;User ID=wda;Password=PH7_C0rp" "Source=$Webserver;Initial Catalog=WebDiary;User ID=wda;Password=PH7_C0rp"

  execwait '$PLUGINSDIR\WebDiary.StudySetupCmd.exe /silent "$WebSiteDir\WebDiary.StudyPortal\studyconfig\study.xml" $Webserver $SurveyUrl $SurveyUrlUser $SurveyUrlPassword' $0
  ${IF} $0 == 1
      MessageBox MB_OK "Argument is null or empty for the StudySetupCmd.exe"
      Abort
  ${ELSEIF} $0 == 2
     MessageBox MB_OK "Unable to open study.xml for the StudySetupCmd.exe"
     Abort
  ${ELSEIF} $0 == 3
     MessageBox MB_OK "A problem occurred while trying to read the study xml file for StudySetupCmd.exe"
     Abort
  ${ELSEIF} $0 == 4
      MessageBox MB_OK "Study.xml is missing some required attributes for StudySetupCmd.exe"
      Abort
  ${ELSEIF} $0 == 5
     MessageBox MB_OK "A Study version entry not found for this study for StudySetupCmd.exe"
      Abort
  ${ELSEIF} $0 == 6
     MessageBox MB_OK "No surveys were found in study.xml for StudySetupCmd.exe"
      Abort
   ${ELSEIF} $0 == 7
     MessageBox MB_OK "Unable to access the WebDiary database for StudySetupCmd.exe"
      Abort
   ${ELSEIF} $0 == 8
     MessageBox MB_OK "Unable to access the survey database for StudySetupCmd.exe"
      Abort
   ${ELSEIF} $0 == 9
     MessageBox MB_OK "Invalid mode for StudySetupCmd.exe"
      Abort
   ${ELSEIF} $0 == 10
     MessageBox MB_OK "Too many arguments for StudySetupCmd.exe"
      Abort
	${ELSEIF} $0 == 11
     MessageBox MB_OK "Missing some arguments for StudySetupCmd.exe"
      Abort
   ${ELSEIF} $0 == 12
     MessageBox MB_OK "A problem occurred while trying to use Checkbox web services for StudySetupCmd.exe. See c:\temp\study_setup_error.txt for more info."
      Abort
   ${ELSEIF} $0 == 13
     MessageBox MB_OK "The study url for this study already exists in the database. for StudySetupCmd.exe"
      Abort
	${ELSEIF} $0 == 14
     MessageBox MB_OK "Cannot convert current study to non-PII study due to existing subjects"
      Abort
	${ELSEIF} $0 == 15
     MessageBox MB_OK "Cannot convert current study to PII study due to existing subjects"
      Abort
	${ELSEIF} $0 == 16
     MessageBox MB_OK "There was an error installing external survey"
      Abort
	${ELSEIF} $0 == 17
     MessageBox MB_OK "Invalid format for subject id"
      Abort
	${ELSEIF} $0 == 18
     MessageBox MB_OK "Unable to create c:\temp\ directory."
      Abort
        ${ELSEIF} $0 == 19
     MessageBox MB_OK "Sortorder in surveys is not valid."
      Abort
        ${ELSEIF} $0 == 20
     MessageBox MB_OK "Duplicate sortorder in surveys for subject submit."
      Abort
        ${ELSEIF} $0 == 21
     MessageBox MB_OK "Duplicate sortorder in surveys for site submit."
      Abort
        ${ELSEIF} $0 == 22
     MessageBox MB_OK "Invalid value for collect subject initials."
      Abort
        ${ELSEIF} $0 == 23
     MessageBox MB_OK "Study.xml is missing authorized_browsers block."
      Abort
   ${EndIf}

   nsExec::ExecToStack '"$sql_cmd\sqlcmd.exe" -U $DatabaseUser -P $DatabasePassword -S $DatabaseServer -d WebDiary -Q "select environment from dbo.VERSIONS"'
   pop $0
   pop $study_env
   ${StrReplace} $1 "$\r$\n" "|" $study_env
   StrCpy $study_env $1
   ${WordFind} $study_env "|" "+2}" $study_env
   ${WordFind} $study_env "|" "+1{" $study_env
   ${trim} $study_env $study_env 

   ${If} $Study_env = 1
       strcpy $SiteName1 "$SiteName_UST"
   ${Else}
       strcpy $SiteName1 $SiteName     
   ${ENDIF}

   nsExec::ExecToStack '"$sql_cmd\sqlcmd.exe" -U $DatabaseUser -P $DatabasePassword -S $DatabaseServer -d WebDiary -Q "select study_id from dbo.STUDIES where study_name=$\'$SiteName1$\'"'
   Pop $0
   Pop $study_id
   ${StrReplace} $1 "$\r$\n" "|" $study_id
   StrCpy $study_id $1
   ${WordFind} $study_id "|" "+2}" $study_id
   ${WordFind} $study_id "|" "+1{" $study_id
   ${trim} $study_id $study_id


####################################################################################
# Sql to add core version to database
####################################################################################
 
  !insertmacro ReplaceInFile "$PLUGINSDIR\core_version.sql" "SET @majorVersion = majorVersion;" "SET @majorVersion = $major_version;"
  !insertmacro ReplaceInFile "$PLUGINSDIR\core_version.sql" "SET @minorVersion = minorVersion;" "SET @minorVersion = $minor_version;"
  !insertmacro ReplaceInFile "$PLUGINSDIR\core_version.sql" "SET @revisionVersion = revisionVersion;" "SET @revisionVersion = $revision_version;"
  !insertmacro ReplaceInFile "$PLUGINSDIR\core_version.sql" "SET @buildNumber = buildNumber;" "SET @buildNumber = $build_version;"
  !insertmacro ReplaceInFile "$PLUGINSDIR\core_version.sql" "SET @study_id = study_id;" "SET @study_id = $study_id;"
  
  nsExec::ExecToStack '"$sql_cmd\sqlcmd.exe" -U $DatabaseUser -P $DatabasePassword -S $DatabaseServer -d WebDiary -i "$PLUGINSDIR\core_version.sql" -o C:\temp\core_version.txt -e -b'
  pop $0
  pop $1
   ${If} $0 == 0
  ${Else}
    MessageBox MB_OK "The SQL script core_version did not run successfully:$\n$\rPlease look at c:\temp\core_version.txt for errors"
    Abort
  ${EndIf}

   nsExec::ExecToStack "C:\Windows\System32\inetsrv\appcmd.exe delete apppool /apppool.name:$SiteName.${BUILD_NUMBER}"

   nsexec::ExecToStack "C:\Windows\System32\inetsrv\appcmd add apppool /name:$SiteName.${BUILD_NUMBER} /managedRuntimeVersion:v4.0 /managedPipelineMode:Integrated"
   pop $0
   pop $1
   ${LogText} "Just added apppoll for $SiteName.${BUILD_NUMBER} the return was $0$\r$\n the trace was$\r$\n $1"
  
  nsexec::ExecToStack "C:\Windows\System32\inetsrv\appcmd.exe set config -section:applicationPools /[name='$SiteName.${BUILD_NUMBER}'].processModel.loadUserProfile:true"
  pop $0
  pop $1
  ${LogText} "Just configured apppoll for $SiteName.${BUILD_NUMBER} the return was $0$\r$\n the trace was$\r$\n $1"
      
  nsExec::ExecToStack 'c:\WINDOWS\system32\cacls.exe "$WebSiteDir" /E /T /G "NETWORK SERVICE":F'
  pop $0
  pop $1
  ${LogText} "Just changed permissions on $WebSiteDir $0$\r$\n the trace was$\r$\n $1"
  
  nsexec::ExecToStack 'C:\Windows\System32\inetsrv\appcmd list site "$SiteName.${BUILD_NUMBER}"'
  pop $0
  
  ${IF} $0 <> 0
  ${IF} $StudyIp == ""
  nsExec::ExecToStack 'C:\Windows\System32\inetsrv\appcmd.exe add site /name:$SiteName.${BUILD_NUMBER} /physicalPath:"$WebSiteDir\WebDiary.StudyPortal" /bindings:http/*:80:$NoHttpCluster'
  #nsExec::Exec 'c:\WINDOWS\system32\cscript.exe "c:\WINDOWS\system32\iisweb.vbs" /create $WebSiteDir\WebDiary.StudyPortal "$SiteName.${BUILD_NUMBER}" /d $NoHttpCluster'
  pop $0
  pop $1
  ${ELSE}
  nsExec::ExecToStack 'C:\Windows\System32\inetsrv\appcmd.exe add site /name:$SiteName.${BUILD_NUMBER} /physicalPath:"$WebSiteDir\WebDiary.StudyPortal" /bindings:http/$StudyIp:80:$NoHttpCluster'
  #nsExec::Exec 'c:\WINDOWS\system32\cscript.exe "c:\WINDOWS\system32\iisweb.vbs" /create $WebSiteDir\WebDiary.StudyPortal "$SiteName.${BUILD_NUMBER}" /i $StudyIp /d $NoHttpCluster'
  pop $0
  pop $1
  ${EndIF}
  ${IF} $0 == 0
  ${ELSE}
  MessageBox MB_OK "Failed to create website please verify info and try again Error is $\r$\n$1"
  Abort
  ${EndIf}
  ${EndIf}

  nsExec::ExecToStack "C:\Windows\System32\inetsrv\appcmd.exe set site /site.name:$SiteName.${BUILD_NUMBER} /[path='/'].applicationPool:$SiteName.${BUILD_NUMBER}"
  pop $0
  pop $1
  ${LogText} "Just add apppool to $SiteName the return was $0$\r$\n the trace was$\r$\n $1"
#############################################################################
  # Adding the custom identity to the apppool
  #############################################################################
  StrCpy $0 `C:\Windows\System32\inetsrv\appcmd.exe set config `
  StrCpy $1 `/section:applicationPools $\"/[name='$SiteName.${BUILD_NUMBER}']`
  StrCpy $2 `.processModel.identityType:SpecificUser$\" $\"/[name='$SiteName.${BUILD_NUMBER}']`
  StrCpy $3 `.processModel.userName:$DomainName\$DomainUser$\" $\"/[name='$SiteName.${BUILD_NUMBER}']`
  StrCpy $4 `.processModel.password:$DomainUserPassword$\"`
  #############################################################################
  # appcmd call
  #############################################################################
  nsExec::ExecToStack "$0$1$2$3$4"
  pop $0
  pop $1
  ${LogText} "Just add custom identity to apppool the return was $0$\r$\n the trace was$\r$\n $1"
  
  #############################################################################
  # Giving the domain user the permission to read site folder
  # ###########################################################################
  StrCpy $0 'icacls $WebSiteDir /grant "$DomainName\$DomainUser:(R)"'
  nsExec::ExecToStack "$0"
  pop $0
  pop $1
  ${LogText} "Just changed permissions on $WebSiteDir $0$\r$\n the trace was$\r$\n $1"


  #nsExec::Exec 'c:\WINDOWS\system32\cscript.exe "$PLUGINSDIR\iis_id.vbs" $SiteName.${BUILD_NUMBER}'
  #Pop $0
  ;;MessageBox MB_OK "you have found the correct website the value is $0"
  
  nsexec::ExecToStack "C:\Windows\System32\inetsrv\appcmd.exe set config $SiteName.${BUILD_NUMBER} /section:defaultDocument /enabled:true /+files.[value='index.aspx']"
  pop $0
  pop $1
  ${LogText} "Just added index.aspx to default list of $SiteName $0$\r$\n the trace was$\r$\n $1"
  nsexec::ExecToStack "C:\Windows\System32\inetsrv\appcmd.exe set config $SiteName.${BUILD_NUMBER} /section:defaultDocument /enabled:true /+files.[value='Default.aspx']"
  pop $0
  pop $1
  ${LogText} "Just added Default.aspx to default list of $SiteName $0$\r$\n the trace was$\r$\n $1"
  nsexec::ExecToStack "C:\Windows\System32\inetsrv\appcmd.exe set config $SiteName.${BUILD_NUMBER} /section:defaultDocument /enabled:true /+files.[value='Default.htm']"
  pop $0
  pop $1
  ${LogText} "Just added Default.html to default list of $SiteName $0$\r$\n the trace was$\r$\n $1"
  nsexec::ExecToStack "C:\Windows\System32\inetsrv\appcmd.exe set config $SiteName.${BUILD_NUMBER} /section:defaultDocument /enabled:true /+files.[value='Default.asp']"
  pop $0
  pop $1
  ${LogText} "Just added Default.asp to default list of $SiteName $0$\r$\n the trace was$\r$\n $1"
  nsexec::ExecToStack "C:\Windows\System32\inetsrv\appcmd.exe set config $SiteName.${BUILD_NUMBER} /section:defaultDocument /enabled:true /+files.[value='index.htm']"
  pop $0
  pop $1
  ${LogText} "Just added index.htm to default list of $SiteName $0$\r$\n the trace was$\r$\n $1"
  #nsExec::Exec 'c:\WINDOWS\system32\cscript.exe "C:\Inetpub\AdminScripts\adsutil.vbs" set w3svc/$0/defaultdoc index.aspx,Default.aspx,Default.htm,Default.asp,index.htm'
  nsexec::ExecToStack "C:\Windows\System32\inetsrv\appcmd.exe set site /site.name:$SiteName.${BUILD_NUMBER} /+bindings.[protocol='https',bindingInformation='*:443:$NoHttpCluster']"
  pop $0
  pop $1
  ${LogText} "Just added index.aspx to default list of $SiteName $0$\r$\n the trace was$\r$\n $1"
  #nsExec::Exec 'c:\WINDOWS\system32\cscript.exe "C:\Inetpub\AdminScripts\adsutil.vbs" set w3svc/$0/SecureBindings ":443:$NoHttpCluster"'
  nsExec::ExecToStack 'sqlcmd.exe -U $DomainUser -P -E -S test_install -d WebDiary -Q "SELECT [description] FROM [WebDiary].[dbo].[ENVIRONMENTS] where [environment_id] = (SELECT [environment] FROM [WebDiary].[dbo].[VERSIONS])"' ;-o c:\temp\database_sql.txt -e -b'
  Pop $0 ;return value
  pop $1 ;stack returned from command

  ${StrContains} $0 "Staging" $1

  ${If} $0 == ""
     ${DisableX64FSRedirection}
     nsexec::ExecToStack 'C:\WINDOWS\Microsoft.NET\Framework64\v4.030319\aspnet_regiis -pe "connectionStrings" -app "/$SiteName.${BUILD_NUMBER}" -prov "RsaProtectedConfigurationProvider"'
     nsexec::ExecToStack 'C:\WINDOWS\Microsoft.NET\Framework64\v4.030319\aspnet_regiis -pe "system.web/sessionState" -app "/$SiteName.${BUILD_NUMBER}" -prov "RsaProtectedConfigurationProvider"'
     ${EnableX64FSRedirection}
  ${EndIf}

  CopyFIles "C:\temp\MyInstallLog.txt" "$WebSiteDir\WebDiary.StudyPortal\study_install.txt"

SectionEnd



Function nsInstallWeb

  ;StrCpy $WebSiteDir "C:\Inetpub\WebDiary"
  nsDialogs::Create 1018
  Pop $WebDialog

  ${if} $WebDialog == error
     MessageBox MB_OK "An error was found"
     Abort
   ${EndIf}

   ${NSD_CreateLabel} 0 0 100% 24u "Please specify the Study Webserver Home directory"
   Pop $0

   ${NSD_CreateLabel} 0 30u 100u 12u "WebSite Home Directory"
   Pop $lblWebSiteDir

   ${NSD_CreateText} 100u 30u 120u 12u $WebSiteDir
   Pop $txtWebSiteDir

   ${NSD_CreateBrowseButton} 224u 30u 40u 12u "Browse"
   Pop $browse
   ${NSD_OnClick} $browse OnDirBrowseButton

   nsDialogs::Show

 FunctionEnd
 
Function nsInstallWebLeave
   ${NSD_GetText} $txtWebSiteDir $WebSiteDir
FunctionEnd

 Function OnDirBrowseButton
        Pop $R0
        ${If} $R0 == $browse
              ${NSD_GetText} $txtWebSiteDir $R0
              nsDialogs::SelectFolderDialog /NOUNLOAD "browse" $R0
              Pop $R0
              ${If} $R0 != error
                    ${NSD_SetText} $txtWebSiteDir "$R0"
              ${EndIf}
        ${EndIf}
FunctionEnd

Function SurveyPage
 nsDialogs::Create /NOUNLOAD 1018
 Pop $SurveyDialog

 ${If} $SurveyDialog == error
  Abort
 ${EndIf}

 ${NSD_CreateLabel} 0 0 100% 24u "Please specify the Survey Server, Database, Username, and Password for your SQL Database Authentication."
 Pop $lblLabel

 ${NSD_CreateLabel} 0 24u 100u 12u "Survey URL"
 Pop $lblSurveyUrl
 
 ${NSD_CreateLabel} 0 38u 100u 12u "Survey URL UserName"
 Pop $lblSurveyUserUrl
 
 ${NSD_CreateLabel} 0 52u 100u 12u "Survey URL Password"
 Pop $lblSurveyPasswordUrl
 
 ${NSD_CreateLabel} 0 66u 100u 12u "Server"
 Pop $lblSurveyServer

 ${NSD_CreateLabel} 0 80u 100u 12u "Database"
 Pop $lblSurveyDatabase

 ${NSD_CreateLabel} 0 94u 100u 12u "Username"
 Pop $lblSurveyUsername

 ${NSD_CreateLabel} 0 108u 100u 12u "Password"
 Pop $lblSurveyPassword

${NSD_CreateText} 100u 24u 100% 12u $SurveyUrl
 Pop $txtSurveyUrl
 
 ${NSD_CreateText} 100u 38u 100% 12u $SurveyUrlUser
 Pop $txtSurveyUserUrl
 
 ${NSD_CreatePassword} 100u 52u 100% 12u $SurveyUrlPassword
 Pop $txtSurveyPasswordUrl
 
 ${NSD_CreateText} 100u 66u 100% 12u $SurveyServer
 Pop $txtSurveyServer

 ${NSD_CreateText} 100u 80u 100% 12u $SurveyDatabase
 Pop $txtSurveyDatabase

 ${NSD_CreateText} 100u 94u 100% 12u $SurveyUsername
 Pop $txtSurveyUsername

 ${NSD_CreatePassword} 100u 108u 100% 12u $SurveyPassword
 Pop $pwdSurveyPassword

 nsDialogs::Show
FunctionEnd

Function SurveyPageLeave
 ${NSD_GetText} $txtSurveyUrl $SurveyUrl
 ${NSD_GetText} $txtSurveyUserUrl $SurveyUrlUser
 ${NSD_GetText} $txtSurveyPasswordUrl $SurveyUrlPassword
 ${NSD_GetText} $txtSurveyServer $Surveyserver
 ${NSD_GetText} $txtSurveyDatabase $Surveydatabase
 ${NSD_GetText} $txtSurveyUsername $Surveyusername
 ${NSD_GetText} $pwdSurveyPassword $Surveypassword
FunctionEnd



###############################################################################
# Domain User Page - custom page for collectin domain user domain and username
# #############################################################################

Function nsDomainUser
 nsDialogs::Create /NOUNLOAD 1018
 Pop $DomainUserDialog

 ${If} $DomainUserDialog == error
  Abort
 ${EndIf}

 ${NSD_CreateLabel} 0 0 100% 24u "Please Specify the domain and domain user you want to use to install this Study"
 Pop $lblDomainWelcome

 ${NSD_CreateLabel} 0 24u 100u 12u "Domain:"
 Pop $lblDomainName

 ${NSD_CreateLabel} 0 38u 100u 12u "Domain User:"
 Pop $lblDomainUser

 ${NSD_CreateLabel} 0 52u 100u 12u "Domain User Password:"
 Pop $lblDomainPassword

 ${NSD_CreateText} 100u 24u 100% 12u $DomainName
 Pop $txtDomainName

 ${NSD_CreateText} 100u 38u 100% 12u $DomainUser
 Pop $txtDomainUser

 ${NSD_CreatePassword} 100u 52u 100% 12u $DomainUserPassword
 Pop $pwdDomainUserPassword

 nsDialogs::Show
FunctionEnd

###############################################################################
# Domain User Leave Page - custom page used to collect the values from the
# previous page
# #############################################################################

Function nsDomainUserLeave
 ${NSD_GetText} $txtDomainName $DomainName
 ${NSD_GetText} $txtDomainUser $DomainUser
 ${NSD_GetText} $pwdDomainUserPassword $DomainUserPassword
 push $DomainUser
 push $DomainName
 push $DomainUserPassword

 call MSWAuthenticate
 pop $0
 ${If} $0 == "success"
 ${else} 
    MessageBox MB_OK $0
    Abort
 ${EndIf} 
FunctionEnd


###############################################################################
# Install Type Page - custom page for Install Type
# #############################################################################
Function nsInstallType
nsDialogs::Create /NOUNLOAD 1018
 Pop $InstallTypeDialog
 ${If} $InstallTypeDialog == error
  Abort
 ${EndIf}
 
 ${NSD_CreateLabel} 0 24u 100u 12u "Type of Install"
 Pop $lblDropList
 
 ${NSD_CreateDropList} 100u 24u 67% 12u ""
 Pop $DropList
 
 ${NSD_CB_AddString} $DropList Production
 ${NSD_CB_AddString} $DropList UST
 ${NSD_CB_AddString} $DropList Staging
 ${NSD_CB_AddString} $DropList Developer
 ${NSD_CB_SelectString} $DropList Production 
  
 nsDialogs::Show
FunctionEnd

###############################################################################
# Install Type Page Leave - custom page used to collect the values from the
# previous page
# #############################################################################
Function nsInstallTypeLeave
${NSD_GetText} $DropList $Type
FunctionEnd


###############################################################################
# Resource Validation Page - custom page for ResourceValidation
# #############################################################################
Function nsResoucesValidation
nsDialogs::Create /NOUNLOAD 1018
 Pop $ResoucesValidationDialog
 ${If} $ResoucesValidationDialog == error
  Abort
 ${EndIf}
 
  ${NSD_CreateLabel} 0 0 100% 24u "Do you want to perform resource validation?"
 Pop $lblResourceQuestion
 
  ${NSD_CreateLabel} 0 24u 100% 24u "Resource validation will happen regardless of your selection in Production Install type"
 Pop $lblProductionSelectionMsg
 
 #${NSD_CreateText} 100u 24u 100% 12u $ResourceValidationAnswer
 #Pop $txtResourceValidationAnswer  
 
  ${NSD_CreateCheckbox} 0 117u 48% 12u "Click for Yes"
  Pop $ResourceValidationAnswer_cb # Checkbox   
  
 nsDialogs::Show
FunctionEnd

###############################################################################
# Resource Validation Leave Page - custom page used to collect the values from the
# previous page
# #############################################################################

Function nsResourcesValidationLeave
 #${NSD_GetText} $txtResourceValidationAnswer $ResourceValidationAnswer
 ${NSD_GetState} $ResourceValidationAnswer_cb $ResourceValidationAnswer_st
FunctionEnd

###############################################################################
# Database Page - custom page used to collect the values need in order to gain
# access to the NetPro database
# #############################################################################

Function nsDatabase
 nsDialogs::Create /NOUNLOAD 1018
 Pop $DatabaseDialog

 ${If} $DatabaseDialog == error
  Abort
 ${EndIf}

 ${NSD_CreateLabel} 0 0 100% 24u "Please Specify the databse server, username and password for accessing the NetPro database"
 Pop $lblDatabaseWelcome

 ${NSD_CreateLabel} 0 24u 100u 12u "Database server:"
 Pop $lblDatabaseServer

 ${NSD_CreateLabel} 0 38u 100u 12u "Database User"
 Pop $lblDatabaseAdmin

 ${NSD_CreateLabel} 0 52u 100u 12u "Database User Password"
 Pop $lblDatabaseAdminpass

 ${NSD_CreateText} 100u 24u 100% 12u $DatabaseServer
 Pop $txtDatabaseServer

 ${NSD_CreateText} 100u 38u 100% 12u $DatabaseUser
 Pop $txtDatabaseAdmin

 ${NSD_CreatePassword} 100u 52u 100% 12u $DatabasePassword
 Pop $txtDatabaseAdminPassword

 nsDialogs::Show
FunctionEnd

###############################################################################
# Domain User Leave Page - custom page used to collect the values from the
# previous page
# #############################################################################

Function nsDatabaseLeave
 ${NSD_GetText} $txtDatabaseServer $DatabaseServer
 ${NSD_GetText} $txtDatabaseAdmin $DatabaseUser
 ${NSD_GetText} $txtDatabaseAdminPassword $DatabasePassword
FunctionEnd
Function Page
 nsDialogs::Create /NOUNLOAD 1018
 Pop $Dialog

 ${If} $Dialog == error
  Abort
 ${EndIf}

 ${NSD_CreateLabel} 0 0 100% 24u "Please specify the Study WebSite information"
 Pop $lblStudyLabel

 ${NSD_CreateLabel} 0 30u 100u 12u "NetPro Study Cluster Name"
 Pop $lblWebServer

 ${NSD_CreateLabel} 0 44u 100u 12u "NetPro Study ip"
 Pop $lblStudyIP
 
 ${NSD_CreateLabel} 0 58u 100u 12u "Study Database server"
 Pop $lblDbServer

 ${NSD_CreateText} 100u 30u 100% 12u $WebServer
 Pop $txtWebServer

 ${NSD_CreateText} 100u 44u 100% 12u $StudyIP
 Pop $txtStudyIP
 
 ${NSD_CreateText} 100u 58u 100% 12u $DbServer
 Pop $txtDbServer

 nsDialogs::Show
FunctionEnd

Function PageLeave
 ${NSD_GetText} $txtWebServer $Webserver
 ${IndexOf} $R0 $Webserver "/"
 IntOp $R0 $R0 + 2
 StrCpy $NoHttpCluster $Webserver "" $R0
 ${NSD_GetText} $txtStudyIp $StudyIp
 ${NSD_GetText} $txtDbServer $Dbserver
FunctionEnd



Function IndexOf
Exch $R0
Exch
Exch $R1
Push $R2
Push $R3

 StrCpy $R3 $R0
 StrCpy $R0 -1
 IntOp $R0 $R0 + 1
  StrCpy $R2 $R3 1 $R0
  StrCmp $R2 "" +2
  StrCmp $R2 $R1 +2 -3

 StrCpy $R0 -1

Pop $R3
Pop $R2
Pop $R1
Exch $R0
FunctionEnd

Function nsFinishPage

  nsDialogs::Create 1018
  Pop $FinishPage

  ${If} $FinishPage == error
    Abort
  ${EndIf}

  ${NSD_CreateLabel} 0 0 48% 12u "Installation is complete."
  Pop $0
  ${NSD_CreateCheckbox} 0 117u 48% 12u "Save Default"
  Pop $SaveDefault_cb # Checkbox

  nsDialogs::Show

FunctionEnd

Function nsFinisPageLeave
  ${NSD_GetState} $SaveDefault_cb $SaveDefault_st

  ${If} $SaveDefault_st == ${BST_CHECKED}
    # If SaveDefault_st is checked, write all of the values
    #  from server information to the ini file.
    
    ;;MessageBox MB_OK $EXEDIR

    ;;;DetailPrint "$EXEDIR\$AnswerFile"

    WriteINIStr "$EXEDIR\$AnswerFile" "Study Settings" WebSiteDir $WebSiteDir
    WriteINIStr "$EXEDIR\$AnswerFile" "Study Settings" Webserver $Webserver
    WriteINIstr "$EXEDIR\$AnswerFile" "Study Settings" StudyIp $StudyIp
    WriteINIStr "$EXEDIR\$AnswerFile" "Study Settings" Dbserver $Dbserver
    WriteINIStr "$EXEDIR\$AnswerFile" "Study Settings" SurveyUrl $SurveyUrl
    WriteINIStr "$EXEDIR\$AnswerFile" "Study Settings" SurveyUrlUser $SurveyUrlUser
    WriteINIStr "$EXEDIR\$AnswerFile" "Study Settings" Surveyserver $Surveyserver
    WriteINIStr "$EXEDIR\$AnswerFile" "Study Settings" Surveydatabase $Surveydatabase
    WriteINIStr "$EXEDIR\$AnswerFile" "Study Settings" Surveyusername $Surveyusername

   FlushINI "$EXEDIR\$AnswerFile"
 ${EndIf}
FunctionEnd

Function "setDefaultAnswers"
   ;;MessageBox MB_OK "$EXEDIR\$AnswerFile"
  # Verify that the file exists.
  ${If} ${FileExists} "$EXEDIR\$AnswerFile"
  ;;MessageBox MB_OK "found answer file"
  # Initialize values For nsServerSettings
    ReadINIStr $WebSiteDir "$EXEDIR\$AnswerFile" "Study Settings" WebSiteDir
    ReadINIStr $Webserver "$EXEDIR\$AnswerFile" "Study Settings" Webserver
    ReadINIStr $StudyIp "$EXEDIR\$AnswerFile" "Study Settings" StudyIp
    ReadINIStr $Dbserver "$EXEDIR\$AnswerFile" "Study Settings" Dbserver
    ReadINIStr $SurveyUrl "$EXEDIR\$AnswerFile" "Study Settings" SurveyUrl
    ReadINIStr $SurveyUrlUser "$EXEDIR\$AnswerFile" "Study Settings" SurveyUrlUser
    ReadINIStr $Surveyserver "$EXEDIR\$AnswerFile" "Study Settings" Surveyserver
    ReadINIStr $Surveydatabase "$EXEDIR\$AnswerFile" "Study Settings" Surveydatabase
    ReadINIStr $Surveyusername  "$EXEDIR\$AnswerFile" "Study Settings" Surveyusername
  ${EndIf}
  ${If} ${Errors}
    MessageBox MB_OK "Errors Found while loading default answers."
  ${EndIf}
FunctionEnd
