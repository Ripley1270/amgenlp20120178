//Ongoing headache Resolution
//Page 3
$(document).ready(function () {
    function activateDateTimePickerTWO () {
        //Set min and max dates
        var minDate = new Date($('#startDate').text());
        var maxDate = new Date();
        //Get information
        var visitDateName = 'endDate';
        var visitDateID = 'endDate';
        var tempVal = medDate.value;
        //Move textArea outside of the matrix
        $(medDate).remove();
        $('#dateContainer').append('<br/><input class="datepicker" name= "' + visitDateName + '" type="text" id= "' + visitDateID + '" />');
        $('#' + visitDateID).datetimepicker({
            controlType : 'select',
            minDate     : minDate,
            maxDate     : maxDate,
            defaultDate : minDate,
            dateFormat  : 'dd M yy',
            stepMinute  : 1
        });
        $('#' + visitDateID).attr('readOnly', 'true');
        $('#' + visitDateID).datepicker("setDate", minDate);
        //Hide Old Widgets
        /*
         $($('input[type=text]')[1]).hide()
         $($('img')[0]).hide();
         */
        //copy current value into new date picker
        $('#' + visitDateID).val(tempVal);
        //set default time
        $('#' + visitDateID).unbind('click');
        $('#' + visitDateID).click(function () {
            if(tempVal == "" && $('#' + visitDateID).val() == ""){
                $('#' + visitDateID).datetimepicker("setDate", minDate);
            }
        });
    }

    WaitAndLoad();
    function WaitAndLoad () {
        if(typeof activateDateTimePickerTWO != 'function' ||
            typeof $.timepicker != 'object'){
            setTimeout(WaitAndLoad, 100);
            return;
        }
        activateDateTimePickerTWO();
    }

    // Override the Next Button
    $('#ResponseView_LayoutTemplate__nextZone__nextBtn').unbind('click');
    $('#ResponseView_LayoutTemplate__nextZone__nextBtn').bind('click', function () {
        //var visitDateID = medDate.id;
        //$($('input[type=text]')[1]).val($('#' + visitDateID).val());
        return true;
    });
});
